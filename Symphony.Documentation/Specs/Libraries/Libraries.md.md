# Libraries
### Requirements
* We need large groupings of courses (4000+ courses)
* Courses can be taken in any order at any time
* Access can be granted to a large group of courses to a student
* Access can expire. 
* No need for any of our advanced training program features:
  * Instructors/Leaders - Need not apply since these will never be instructor lead courses. They are courses that a user can take whenever they wish. 
  * Assignments - Since we don't have instructors, we wont have anyone to mark assignments. You could potentially still add a course with an assignment to a library, but the student would never receive feedback. 
  * Sessions - Start and end time will be based on the link between a student and a library. No need for any additional dates. 
  * DueDates/ActiveAfter/ExpiresAfter - No need - all library courses are available at any time. 
  * Optional/Elective/Finals - No need
* Courses within a library are not grouped in any particular way. A course stands on it's own. 
* Robust searching/management on the student end.

### Design
Libraries will be introduced as a new Symphony entity. There are two alternative approaches that have been considered:
* Use Training Programs
  * As noted in the requirements, training programs offer many advanced features that really would not be required for a library. 
  * We are unsure of how training programs would behave once 4000+ courses are added. We do a lot of processing when loading training programs, doing this with 4000 courses could be problematic. 
  * We need additional features in the library (Searching/Favorites) that we would need to toggle on or off depending on how the training program is being used. Seems best not to complicate something that is already quite complex.
* Use Categories
  * Libraries are sort of like categories, if we were able to assign a student access to a category containing public courses. However, a library may end up containing courses from multiple categories. These categories may not all be from the same tree. 

Libraries will exist within a customer where students can be granted access for a period of time. Within the library a student will be able to organize their courses by favoriting the ones they want to access. 
##### Database
![](https://i.gyazo.com/877ab2f4cfca06e0b40f68a1dae39ff0.png)

##### User Interface
* **Library Portal Module**
  
  A Simple grid to display in our portal. Will just list the libraries assigned to the student just like the Training Programs module. Clicking a library will launch the library details view.

  ![](https://i.gyazo.com/89b5f1bed0bc5362c7f252f7674f0414.png) 
  
 * **Library Details View - Category Listing**
  
   Library details window will be shown after launch a library. 
  
   ![](https://i.gyazo.com/4e63fc572eb30e6f136833044e3479ba.png)

   * Categories will automatically be configured in the library based on the categories assigned to the course.
   * The library grid will initially display categories. The student can drill down into the category they want to access courses in. 
   *  Details and advanced search can be hidden in order to view a larger grid of results.
   *  Displaying some summary details about library progress and the current subscription.
   *  Content at the bottom right will be editable per library. Likely it would be useful to allow students to easily link back to a library they are registered in. 
   *  In this mode, useing the advanced search, or the course filter on the the right of the grid, will filter categories only. Only categories with courses will be shown.
   * Double click on the category will load the course listing.

* **Library Details View - Course Listing**

    Details window will switch to course mode after a category is loaded.
    
    ![](https://i.gyazo.com/503a1138f017037fd9e562cee4f958c2.png)
    
    * Advanced search is hidden in this screen purely to maximize space. (It will behave the same as the category advanced saerch, only apply to courses.
    * Course filters on the right will filter the courses.
    * Displaying course image, description, and authors as part of the course name column.
    * Courses can be launched directly from the link. 
    * Clicking the star icon will add the course to the student's favorites.
    * Course hours will be displayed. 
    * Will use breadcrumbs to navigate back to the parent category.

* **Library Editor - Details**
    
    Library editor will exist in the Course Management section of Symphony. 
    ![](https://i.gyazo.com/e001e73d0a7cd004c71adfbc4af9d3f9.png)
    * Details will just require Name, Description and Company Details
    * Allowing company details as just a small snippet of HTML to allow students an easy way to get help or further details about the library. We may want to define what actually might be required here and have them as specific fields instead of allowing arbitrary HTML

* **Library Editor - Courses**

    Courses tab for the library editor will behave similarily to the student view of the library. 
    ![](https://i.gyazo.com/5f89cbf0e73cd9a0f46e11f6c9494b7b.png)
    * Courses can be searched for using hte advanced search functionality
    * Courses can be selected and added directly to the library
    * Grid will indicate if a course is in the library or not
    * Filters will be available to show All Courses, Courses in the library, and courses outside the library

* **Library Editor - Registrations**

    Student registrations for a library will be managed in an additional tab. 
    ![](https://i.gyazo.com/dd56e8c5009791ed90ad0890f44c7f1d.png)
    * Adding students will be done using our usual student selector when "Add Students" is clicked
    * Registrations can be revoked using remove selected or the trash can.

