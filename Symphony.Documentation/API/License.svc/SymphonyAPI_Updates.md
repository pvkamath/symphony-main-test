# New Symphony API's

#### root: /services/license.svc

```
GET     /reports/expiration/{entityType}
    required: entityType ("user", "location", "jobrole", "audience" "all")
    returns a generic array of key/value pairs: 
    {
        data : [
            { key: <key value>, value: <value> }
        ]
    }
```
```
GET     /reports/status/{entityType}
    required: entityType ("user", "location", "jobrole", "audience" "all")
    returns a generic array of key/value pairs: 
    {
        data : [
            { key: <key value>, value: <value> }
        ]
    }
```
```
GET     /list
    returns a list of licenses:
    [
        { 
            id: <int>,
            name : <string>
        }
    ]
```
```
POST    /selfsave/{licenseId}
    required params - <int> licenseId 
    required data - JSON object: 
    {
        startDate: <string "Date(<timestamp>)",
        expiryDate: <string "Date(<timestamp>)", 
        assignmentStatusId: <int>
    }
```
```
DELETE  /assignment/delete/{licenseAssignmentId}
    required params: <int> licenseAssignmentId
```

```javascript
POST /notes
    required params: JSON Object {
        licenseAssignmentId: <int>,
        body: <text>
    }
```

```javascript
GET /notes/{licenseAssignmentId}
    required params: <int> licenseAssignmentId
    returns Array of JSON Note objects:
        [
            {
                id: <int>,
                createdBy: <int>,
                created: <timestamp>
                body:<string>
            }[, ...]
        ]
```
#### License Report Object
This is the common object returned by all entity-based license detail reports:
```javascript
{
    entityName: <string>,
    entityType: <string> ("user", "location", "jobrole", "audience"),
    entityId: <int>,
    licenseName: <string>,
    licenseId: <int>,
    licenseDescription: <string>,
    expirationRuleId: <int>, 
    expirationRuleAfterDays: <int>,
    startDate: <timestamp>,
    expiryDate: <timestamp>,
    licenseId: <int>,
    daysUntilExpiry: <int>
}
```
## Reporting Endpoints
The above reporting API is a simplified version to obtain thdata marked in the mock ups. We have also designed several endpoint to further drill down into the data and get a fully realized report for Users, Locations, Job Roles, and Audiences. These can be further refined as the granularity emerges from use and design needs.


```javascript
GET /reports/{entityType}/{entityId}
    required params: 
        entityType: string ['location, 'jobrole, 'audience']
        entityId:Use '0' to get all records of the entity type, otherwise provide the entity id
    returns: array of license report objects (see definition above)
```

```javascript
GET /reports/users/{entityType}/{entityId}
    required params: 
        entityType: string ['location, 'jobrole, 'audience']
        entityId: the entity id
    returns: array of license report objects for all users associated with a given license (see definition above)
```
