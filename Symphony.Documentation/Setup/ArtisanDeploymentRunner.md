# Deployment Runner
The deployment runner is a console app set up to run one deployment at a time. This app runs externally from IIS to avoid any potential errors that might trigger an IIS reset.

The deployment runner is in the Symphony solution as `Symphony.DeploymentRunner`

### Deploying the Deployment Runner

The compiled DLLs for the DeploymentRunner are in `SVN /symphony2-build/tools/DeploymentRunner`. Symphony DLLs are also included in this folder. 

#### Deployment Steps Plan

**Build**
1. Build project `/symphony2/Symphony.DeploymentRunner`
2. Copy dlls to `/symphony2-build/tools/DeploymentRunner`
3. Include DeploymentRunner in autocommit. 

**Deploy**
1. Update C:/Symphony/DeploymentRunner (Assuming DeploymentRunner will always live in the root Symphony directory on server.
2. Handle potential required `appsettings.deploymentrunner.config`
   * Need to merge appsettings.production.config with appsettings.deploymentrunner.config, preserverving whatever is in appsettings.deploymentrunner.config ArtisanAssetUploadDirectory

#### Setup

Should only be required when configuring a new instance.

* Checkout `/symphony2-build/tools/DeploymentRunner` - best to place this in C:/{SymphonyFileRoot}/DeploymentRunner
* Copy `appsettings.production.config` rename to `appsettings.deploymentrunner.config
* Update `appsettings.deploymentrunner.config`
  * Set `ArtisanAssetsUploadDirectory` to absolute path to ArtisanAssetFiles (e.g. C:\{SymphonyFileRoot}\Symphony.Web\Uploads\ArtisanAssetFiles)
* Update `C:\{SymphonyFileRoot}\DeploymentRunner\Symphony.DeploymentRunner.exe.config`
  * Set `connectionStrings` to production symphony db config file
  * Set `appSettings` file to `appsettings.deploymentrunner.config` config file
* Create file `C:\{SymphonyFileRoot}\Symphony.Web\runDeployments.bat`with the following:
    ```
    C:/{SymphonyFileRoot}/DeploymentRunner/Symphony.DeploymentRunner.exe
    ```
