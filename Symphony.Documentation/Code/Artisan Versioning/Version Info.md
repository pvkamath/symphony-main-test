## Versioning

Versioning has been added to Symphony in order gain visibility to historical deployments. We can now map all previously deployed Artisan courses back to the current Online Course, and view the version of Symphony used to generate them. 

#### ArtisanAudit
An entry is created in this table every time an Artisan course is deployed. 
* OnlineCourseID
* ArtisanCourseID
* OnlineCourseVersion
* PackageGuid (Extracted from the file path)
* CoreVersion (Version of Symphony.Core)

In order to get details about a particular version of a course, all we need the OnlineCourseID and the ArtisanCourseID, or the OnlineCourseID and the PackageGuid. Indexes have been added for both combinations.

#### OnlineCourseRollup
CoreVersion and ArtisanAuditID have been added to the OnlineCourseRollup table. This records the version of Symphony.RusticiIntegration (Configured to match Symphony.Core, but may very depending on build time). This allows us to link back directly to the specific version of the course the user last received.

#### Artisan
Artisan playback now displays version information on the course:

![](https://i.gyazo.com/eb226b44ed7a0d29faf74b82e116157b.png)

#### CSR Admin Tool
Version info is displayed in the CSR admin tool:
![](https://i.gyazo.com/12b186d8ca1e63280f5254315722590a.png)
#### Feature Toggles on Artisan Version
With versions we can now add features and display them based on the version of the Artisan course. Assignment launch links will now show up for all courses that have assignments and the minor version is greator than 2733.

#### Version Numbering
Version numbering is setup through a template file (`AssemblyInfoTemplate.cs`) This file is coppied to `AssemblyInfo.cs` on build. This is configured for Symphony.Core and Symphony.RusticiIntegration. 

Version template is: `1.$WCREV$.*`

This will auto increment our version to: 1.{SVN Revision Number}.{Days since Jan 1 200}.{Seconds since midnight / 2}

This will allow us to restrict certain items based on the commit they were introduced. 


