# Server Side Display Logic Documentation

Since this is a fairly big piece, and adds some functionality that will change how we handle are models in the future this certainly warrants some internal documentation. This will be placed in the repo. 

## Symphony.Core.DisplayValues (/Symphony.Core/DisplayValues/DisplayValue.cs)

DisplayValue is a simple model that is basically a key/value pair between a code and a value to display. This is the base of any display value properties used in a model. Unique codes are generated for each value used. These codes can be used in AppSettings to override a particular value, or we could extend this to use the DB to pull in values for particular customers (Or just customer specific configs). Both the value and the code are returned for each display value. So should a customer using an api decide they want to change a particular value on the fly, the could do so by mapping a value to a particular code when they render their ui. 

The constructor is protected, so this class is extended and constructed as static properties. This allows setting the default value directly in code, and allows the value to behave like an Enum. The value's code is automatically generated based on the name of the subtype and the name of the property. 

Some display values do allow configuring dynamic values. This is mostly done to keep the resulting json consistent for all displayable values. If the value is a dynamic value the code will include `_Dynamic`.

Display values are all aggregated by DisplayModels which will create a specific representation of something we display in Symphony (e.g. **DisplayDate:** `DisplayValues.DateFormat` + `DateTime` + `List<DisplayValues.Text>` - Allows a date time to be automatically formatted, and include possible messages)

* *private void `BuildLookupDictionaries()`* - Generates static dictionaries for the display value type. This loops through the static properties in a display value subtype and creates a static dictionary of codes and a static dictionary of values. The values will use anything configured in AppSettings if available. These dictionaries will only be built if they don't exist, or if the AppConfig `rebuild_displayvalue_cache` is set.

* *public string `Value`* - Returns the value configured in the value lookup dictionary, or the default value.

* *public string `Code`* - Returns the code for the value. Will be in the format `{DisplayValueSubTypeClassName}_{DisplayValuePropertyName}` (e.g. Text_Session_RegistrationRequired) 

* *protected virtual bool `IsValid(string value)`* - Allows validation of any over ridden parameters. Default implementation is to return true. If a sub type requires a specific format we can use this to fall back to the default if an override value is not correct. (For now this is only used in `JsonString` where the value needs to be JSON. We could also validate URLs)

##### DisplayValue Sub Types

For now, all sub types are contained in a single file to keep a central file of all things we display. 

* **DateFormat** - All date formats used in Symphony (Date, DateTimeTz, DateTime... etc.) 
  
  **Example JSON:** `DateFormat.DateTimeTz` (Example JSON omitted in subsequent sub types as this should be consistent for all types)
  ```
  {
     code: 'DateFormat_DateTimeTz',
     value: 'm/d/y g:i a T'
  }
  ```
  * *public bool `HasTimeComponent`* - Returns True if the default format contains "g:i". This is used when calculating times. If there is a time component any associated DateTime with this format will have it's Kind specified as UTC.

* **IconState** - All icons we display in Symphony. Maps a code, to an icon file name. File names will likely make no sense when consuming the UI outside of Symphony and will need to be overridden with specific files. (Or just match our file names to keep it easy... up to them!)

* **Text** - (Allows Dynamic) All text strings displayed in Symphony. Naming convention is used to organize these fields: `Text.TrainingProgram_NoSurvey`, `Text.Session_DueDatePassed` ... etc.
  * *public static Text `Dynamic(string value)`* - Creates an on the fly dynamic string. Code will be `Text_Dynamic`

* **DateText** - Extends Text - includes a DisplayDate associated with the text. This can be used for a last minute replacement of dates in a messages just before rendering.

* **DynamicDateText** - Used when creating a DateText. These will include either `{!date_relative}` or `{!date}` to allow rendering a date within a message.

* **Url** - (Allows Dynamic) All urls used for links displayed in Symphony. 
  * *public static Url `Dynamic(string value)`* - Creates on the fly dynamic string. Code will be `Url_Dynamic`

* **JsonString** - (Allows Dynamic) Any JSON string we might need to return. This is currently solely used by our course launch steps. Since we have a complicated procedure for launching a course, it's hard to represent this consistently with just a single value. Each launch step is built in JSON, the JSON matches the basic Ext configuration required for a launch step window. An external entity consuming our api can use the JSON here to determine any required fields that need to be submitted, names of the fields, and any messages required (Affidavit)

  Dynamic values here are used for the affidavit JSON we have stored in our db, so we wont need to change how that is handled. 

  Validation is implemented here to ensure the JSON value is valid.

* **TextFormat** - Used mostly for links. This allows setting a format when generating a particular display value.

  
## Symphony.Core.Models.DisplayModel : Model (/Symphony.Core/Models/Display/DisplayModel.cs)

This is the base class that will contain display values. Will be returned as a property within an existing Symphony model. 

* *public List<Text> `Messages`* - Nearly all of our display logic is determining if some value should be displayed, or a message should be displayed in it's place. Multiple messages implies the additional messages are displayed in an associated tooltip. 

##### DisplayModel Sub Types

All sub types are located in `/Symphony.Core/Models/Display/` as separate files. 

* **DisplayDate** - Aggregates a `DateTime` with a `DisplayValue.DateFormat` and `List<DisplayValue.Text>`

  * *public bool* `IsAutoSpecifyKind`** [IgnoreDataMember] - Allows internally toggling auto kind specification off. (Based on "g:i" contained in the DateFormat default")

  * *public DateTime? `Date`* - The DateTime to display. Will automatically be specified in UTC if there is a time component, or Unspecified if there is no time component. If required, this can be turned off with `IsAutoSpecifyKind`

  * *public bool `AddLocalOffset`* - Used to determine when rendering if we need to add the local offset to the time. Will be true when there is no time component.

  * *public long `Ticks`* - Tick version of the date time so consumer isn't forced to use the /Date('2340194321-800')/ 

  * *public long `RelativeTicks`* - The number of ticks from the current date. Sometimes we display time in relative mode ("Available in 1 hour") this will allow easily calculating these time spans. 

  * *public DateFormat `Format`* - The format to use for the date. 

  * *public bool `IsActive`* - Flag that indicates if the relative time has started. For example, we have fields that represent a time duration after a specific event (Course Start, Training Program start) if IsActive it means the time can continue to count down client side if necessary (So subsequent requests will display a smaller value) if not active, then subsequent requests will return the same value (no need to count down)

  **Example JSON**

  ```
  {
     date: "/Date(34290843209-800)/",
     addLocalOffset: false,
     ticks: 5249208454,
     relativeTicks: 1839040,
     dateFormat: {
         code: "DateFormat_DateTimeTz",
         value: "m/d/y g:i a T"
     },
     isActive: false,
     messages: [{
         Code: "Text_Session_DueDatePassed",
         Value: "The due date for this {!course} has passed. Contact your training program leader for more time if needed."
     }]
  }
  ```

* **DisplayLink** - Represents a link using DisplayValue.TextFormat and DisplayValue.Url. Allows class/id/url formats to pass parameters to the link. 

  * *public bool `IsEmpty`* - Returns True if the BaseUrl == Url.Empty

  * *public bool `IsNewWindow`* - Parameter to indicate if the url should just be a regular target='_blank' link. Otherwise it likely requires an ajax handler of some sort.

  * *public Text `Title`* - The text to display within the link, will be null when used within a DisplayIconState as the icon will be the content of the link.

  * *public Url `BaseUrl`* - The base url to be used when generating the full url. 

  * *public KeyValueList `QueryParams`* - Any params to be passed to this link. Either as query parameters, or as parameters to be replaced using a link format. KeyValueList is used here instead of a regular dictionary since there were issues with serializing dictionaries. 

  * *public TextFormat UrlFormat* - The format of the url. Can be null. If null, `FullUrl = BaseUrl?QueryParam[key]=QueryParam[value]&...` otherwise query params in the format will be replaced, and the base url will be placed accordingly. (e.g. `"{base}&registration=UserId|{UserID}!CourseId|{CourseID}!TrainingProgramId|{TrainingProgramID}"`)

  * *public TextFormat `IdFormat`* - Allows generating an ID with possible query params. 
    > Included as a convience for rendering Symphony links to include parameters as part of the ID. This is not required when rendering links in external UI. 
  * *public TextFormat `ClassFormat`*  - Allows generating a class with possible query params. For Symphony, we include as part of the class the name of the method used to handle the link (e.g "method_launch") this replaces our old method of symphony:method="launch" symphony:params="3,1,4,5,true,false,3,1". This just passes the entire link, along with all associated data in the record so there isn't a need to output all the params in the html.
    > Included as a convience for rendering Symphony links in order to include any parameters or function names in the class. This is not required for external UI.
  * *public string `DomId`* - The generated ID using the IdFormat (Will be empty if no IdFormat provided)
    > Not required for rendering links in external UI
  * *public string `DomClass`* - The generated Class using the ClassFormat (Will be empty if no ClassFormat provided)
    > Not required for rendering links in external UI
  * *public string `FullUrl`* - The generated full URL of the link. This is either concatenates the query params and appends to the base url, or uses them in the format specified by UrlFormat.

  **Example JSON**
  ```
	{
		"messages": [
		  
		],
		"baseUrl": {
		  "code": "Url_Launch",
		  "value": "http:\/\/localhost:82\/defaultui\/launch.aspx?configuration="
		},
		"classFormat": {
		  "code": "TextFormat_LaunchLinkClassFormat",
		  "value": "method_launch course-launch-link" // Sticking the symphony method to handle the link in the class. 
		},
		"domClass": "method_launch course-launch-link",
		"domId": "launch-link-256-951",
		"fullUrl": "http:\/\/localhost:82\/defaultui\/launch.aspx?configuration=&registration=UserId|9758!CourseId|256!TrainingProgramId|951",
		"idFormat": {
		  "code": "TextFormat_LaunchLinkIdFormat",
		  "value": "launch-link-{CourseID}-{TrainingProgramID}"
		},
		"isEmpty": false,
		"isNewWindow": false,
		"queryParams": [
		  {
			"key": "CourseID",
			"value": "256"
		  },
		  {
			"key": "TrainingProgramID",
			"value": "951"
		  },
		  {
			"key": "UserID",
			"value": "9758"
		  },
		  {
			"key": "AffidavitID",
			"value": "0"
		  },
		  {
			"key": "ProctorRequiredIndicator",
			"value": "True"
		  },
		  {
			"key": "IsCourseWorkAllowed",
			"value": "True"
		  },
		  {
			"key": "IsUserValidationAllowed",
			"value": "True"
		  },
		  {
			"key": "IsInactiveSession",
			"value": "True"
		  },
		  {
			"key": "IsCourseComplete",
			"value": "False"
		  }
		],
		"title": {
		  "code": "Text_Launch",
		  "value": "Launch"
		},
		"urlFormat": {
		  "code": "TextFormat_LaunchLinkUrlFormat",
		  "value": "{base}&registration=UserId|{UserID}!CourseId|{CourseID}!TrainingProgramId|{TrainingProgramID}"
		}
	  }
  ```

* **DisplayIconState** - Aggregates `DisplayValue.IconState`, `DisplayModel.DisplayLink`, and `List<DisplayValue.Text>`. This is used for any icon based state. (Certificate, Assignment State, ...). The DisplayLink allows the icon to be clickable, like in the case of a certificate. The list of DisplayValue.Text allows any tooltips to be included to indicate why the state has occurred. 

  * *public IconState `Icon`* - The icon state to use.
 
  * *public DisplayLink `Link`* - The link to place on the icon, if any.

  * *public bool `IsEmpty`* - Flag to indicate whether the icon is not provided. Returns true if the IconState == IconState.Empty. This would mean we just want to display the first message. (For example when rendering an assignment status and there are no assignments for the course we would render the message which for us would be "N/A")

  **Example JSON**
  ```
  {
     icon: {
         code: "IconState_Certificate",
         value: "/images/medal_gold_3.png"
     },
     isEmpty: false,
     messages: [{
         code: "Text_ViewCertificate",
         value: "View Certificate"
     }]
     link: {
         baseUrl: {
             code: "Url_ViewCertificate",
             value: "/Handlers/CertificateHandler.ashx"
         }
         classFormat: null,
         domClass: "",
         domId: "",
         fullUrl: "/Handlers/CertificateHandler.ashx/?UserID=9758&CertificateTypeID=TrainingProgram&TrainingProgramID=951&CourseOrClassID=0",
         idFormat: null,
         isEmpty: false,
         isNewWindow: false,
         messages: [],
         queryParams: [
             {key: "UserID", value: "9758"}, 
             {key: "CertificateTypeID", value: "TrainingProgram"}
             {key: "TrainingProgramID", value: "951"}
             {key: "CourseOrClassID", value: "0"}
         ]
         title: null,
         urlFormat: null
     }
  }
  ```
* **DisplayLaunchStep** - This represents a step in a series of steps that may be required when launching a course. Our current steps are: 
  >`CourseWorkLaunchStep` - Message to indicate that a student has exceeded the amount of daily activity allowed in a course.
  >     
  >`SessionWorkingLaunchStep` - Message to indicate the student's score will not be recorded.
  >
  >`ProctorRequiredLaunchStep` - Form to capture proctor information
  >
  >`ValidationRequiredLaunchStep` - Form to capture personal student information used to periodically validate that the same student is still taking the course.
  >
  >`AffidavitRequiredLaunchStep` - Form to capture any additional information about the student required for the current course, and confirm that the student abides by the rules of conduct for the institution.

  * *public string `Code`* - Since there are only 5 possibilities for this DisplayModel, a code is provided to distinguish the steps. Will be one of the values listed above.

  * *public Text `Title`* - The title of the launch step. (To use as a window title for example)

  * *public JsonString `Content`* - Contains all the fields/messages required to build the step.

  * *public bool `IsBlocker`* - Should stop any additional steps from occurring and prevent launch of the course. Most importantly this prevents launching of the course. Additional steps should not even be included after a blocker is hit anyway.  Currently the only blocker is CourseWorkLaunchStep.

  * *public string `JsonFieldName`* - For simplicity each step should store all responses in a json string. When all steps are complete POST the responses to `"/launchmeta/{userId}/{courseId}/{trainingProgramId}/"`
  
  **Example JSON**
  ```
	  "messages": [
		
	  ],
	  "code": "ProctorRequiredLaunchStep",
	  "content": {
		"code": "JsonString_LaunchStep_ProctorRequired",
		"value": "{\u000d\u000a  \"items\": [\u000d\u000a    {\u000d\u000a      \"xtype\": \"label\",\u000d\u000a      \"text\": \"Please have your proctor enter their information\",\u000d\u000a      \"textCode\": \"Text_LaunchStep_ProctorRequired_Content\",\u000d\u000a      \"cls\": \"x-form-item\"\u000d\u000a    },\u000d\u000a    {\u000d\u000a      \"xtype\": \"textfield\",\u000d\u000a      \"fieldLabel\": \"Name\",\u000d\u000a      \"name\": \"name\",\u000d\u000a      \"allowBlank\": false,\u000d\u000a      \"itemCls\": \"required\"\u000d\u000a    },\u000d\u000a    {\u000d\u000a      \"xtype\": \"textfield\",\u000d\u000a      \"fieldLabel\": \"Code\",\u000d\u000a      \"name\": \"proctorCode\",\u000d\u000a      \"allowBlank\": true\u000d\u000a    },\u000d\u000a    {\u000d\u000a      \"xtype\": \"textfield\",\u000d\u000a      \"fieldLabel\": \"Location\",\u000d\u000a      \"name\": \"location\",\u000d\u000a      \"allowBlank\": false,\u000d\u000a      \"itemCls\": \"required\"\u000d\u000a    },\u000d\u000a    {\u000d\u000a      \"xtype\": \"checkbox\",\u000d\u000a      \"fieldLabel\": \"\",\u000d\u000a      \"boxLabel\": \"I am an authorized proctor.\",\u000d\u000a      \"name\": \"isAuthorized\",\u000d\u000a      \"allowBlank\": false\u000d\u000a    }\u000d\u000a  ],\u000d\u000a  \"width\": 330,\u000d\u000a  \"height\": 205\u000d\u000a}"
	  },
	  "isBlocker": false,
	  "jsonFieldName": "proctorJsonString",
	  "title": {
		"code": "Text_LaunchStep_ProctorRequired_Title",
		"value": "Proctor Required"
	  }
	}
   ```

   **Launch Step Implementation**

   When launch steps are encountered it will be up to the API consumer to display them in whatever format they desire. They will be listed in the array in the same order that they are presented in Symphony. The syntax of the steps follows ExtJS syntax. xtype will indicate what field to use (checkbox/textfield - we don't use anything else), name will indicate the what we expect for this field, and fieldLabel is the suggested label. If xtypes like panel or label are encountered these are just instructions or messages. The content will be found in html or text fields. 

   One call should be made once the data is collected from all steps:

   >`POST /launchmeta/{userId}/{courseId}/{trainingProgramId}/`
   >```
   >{
   >  proctorJsonString: "{name: \"Test Student\", proctorCode: \"Code\", location: \"The location\", isAuthorized: true}",
   >  affidavitJsonString: "{ name: \"Test Student\", nmlsID: \"f892jaf1\"... whatever fields are in the affidavit...}"
   >}
   >```
   >  
   >If the course is being launched in a popup, go ahead and launch the course without waiting for a response.
 ## Usage

Symphony models currently using DisplayModels:

#### TrainingProgram.cs

* *public DisplayDate `DisplayDueDate`* - Displays either the TP due date, or the sesison due date, or session registration required message.
* *public DisplayIconState `DisplayCompleted`* - Displays pale dot if incomplete with messages to indicate what is outstanding, or a certificate icon if the training program is complete with a link to the certificate.
* *public DisplayIconState `DisplaySurvey`* - Displays a link to take the survey if the training program is complete, or a pale dot if there is no survey or if it is on available. Messages are included to indicate why it is unavailable. 
* *public DisplayModel `DisplayIsElectivesAvailable`* - Just a simple list of messages to indicating if and why the electives are unavailable.
* *public DisplayModel `DisplayIsOptionalAvailable`* - List of messages indicating if and why the optional courses are unavailable.
* *public DisplayModel `DisplayIsFinalAvailable`* - List of messages indicating if and why the final is unavailable

#### TrainingProgramCourse.cs

* *public DisplayDate `DisplayDueDate`* - Displays the due date for the course as specified by training program settings. Includes any messages about the due date being expired.
* *public override DisplayLink `GetDisplayLink(DateTime? minDate, DateTime? maxDate, List<Text> unavailableReasons)`* - Override of the base Course model GetDisplayLink to include training program logic when generating the link for the course. (Includes things like previous courses being required/order...)
* *public List<DisplayLaunchSteps> `DisplayLaunchSteps`* - A list of launch steps to use when launching this course. 

#### Class.cs
* *public DisplayDate `DisplayStartDate`* - Start date of the class
* *public DisplayModel `DisplayLocation`* - Displays Online/Unspecified/ActualLocation
* *public DisplayLink `DisplayRegisterLink`* - Displays a register link for the specific class, or registration closed if it is unavailable

#### Course.cs

* *public Text `DisplayScore`* - Will either contain the score, or a - if it is not valid to currently display the score. 
* *public DisplayIconState `DisplayCertificate`* - Will either be empty, or contain a certificate link/icon to the users certificate for the course. Will include messages indicating why the certificate is unavailable.
* *public DisplayIconState `DisplayAssignmentStatus`* - Indicate whether the assignments are complete/incomplete/failed
* *public DisplayLink `DisplayCourseLink`* - Grabs the course link from GetDisplayCourseLink()
* *public DisplayLink `GetDisplayLink(DateTime? minDate, DateTime? maxDate, List<Text> unavailableReasons)* - Will return the course link. Could be Launch, Register, Unregister, -, or messages indicating why the course is unavailable. 

#### Assignment.cs
* *public DisplayIconState `DisplayAssignmentStatus`* - Displays the status of the assignment (Resubmitted, Incomplete, Unmarked, Passed, Failed)
* *public DisplayDate `DisplaySubmitDate`* - Submitted date of the assignment

#### UpcomingEvent.cs
* *public DisplayDate `DisplayStartDate`* - Start date of the event
* *public DisplayLink `DisplayEventLink`* - Link to the event. Links either the class details link, or the GTM link
* *public DisplayLink `DisplayCourseLink`* - Link to register for a class

## Client Side Rendering

Before rendering a display value (Something in the format `{ code: "code", value: "value" }`) there are a few things that should take place:

1. **Replace Aliases** - Symphony has aliases that may be used in values. They can be configured per customer. You can use whatever values you like. This is the default configuration:

  ```
  aliases = [
        { key: '{!Audience}', val: Symphony.Aliases.audience || "Audience" },
        { key: '{!Audiences}', val: Symphony.Aliases.audiences || "Audience" },
        { key: '{!Course}', val: Symphony.Aliases.course || "Audience" },
        { key: '{!Courses}', val: Symphony.Aliases.courses || "Audience" },
        { key: '{!JobRole}', val: Symphony.Aliases.jobRole || "Audience" },
        { key: '{!JobRoles}', val: Symphony.Aliases.jobRoles || "Audience" },
        { key: '{!Location}', val: Symphony.Aliases.location || "Audience" },
        { key: '{!Locations}', val: Symphony.Aliases.locations || "Audience" },
        { key: '{!Username}', val: Symphony.Aliases.username || "Audience" },
        { key: '{!audience}', val: (Symphony.Aliases.audience || "Audience").toLowerCase() },
        { key: '{!audiences}', val: (Symphony.Aliases.audiences || "Audiences").toLowerCase() },
        { key: '{!course}', val: (Symphony.Aliases.course || "Course").toLowerCase() },
        { key: '{!courses}', val: (Symphony.Aliases.courses || "Courses").toLowerCase() },
        { key: '{!jobrole}', val: (Symphony.Aliases.jobRole || "Job Role").toLowerCase() },
        { key: '{!jobroles}', val: (Symphony.Aliases.jobRoles || "Job Roles").toLowerCase() },
        { key: '{!location}', val: (Symphony.Aliases.location || "Location").toLowerCase() },
        { key: '{!locations}', val: (Symphony.Aliases.locations || "Locations").toLowerCase() },
        { key: '{!username}', val: (Symphony.Aliases.username || "Username").toLowerCase() }
    ];
  ```
 
2. **Replace Dates** - It's possible that the display value will include a date to display as part of the value. In this case the format will be:

   `{ code: "code", value: "value", messageDate: { // DisplayDate // } }`

   At this point you will want to replace `{!date_relative}` with the number of hours/minutes represented by RelativeTicks in DisplayDate, and `{!date}` with the rendered DisplayDate.

3. **Make any other replacements** - If there are specific values you want to use instead of our defaults, replace them based on the code.


The rendering of each display model should be similar:

**DisplayDate**
* If there are messages, display the messages. There could be multiple messages. Otherwise, display the date using the format provided.

**DisplayIcon** 
* IsEmpty && Messages - At least display first message. Display additional messages in tooltip or however it works best for your system.
* Link != null && !Link.IsEmpty - Render the link, with the file specified in IconState as the link content, or whatever file you map to the code
* Else - display file specified by IconState or whatever file you map to the code.

**DisplayLink**
* IsEmpty && Messages = At least display first message. Display additional messages in tooltip or however it works best for your system. Do not link to anything.
*  Generate a link with `href=fullURl`, `id=domId`, `class=domClass`, `title=title`

   > DomID and DomClass are not required. These are designed as helpers for rendering Symphony links. You may use any id/class that works best for your system.

**DisplayModel** (Only has messages property)
* Concatenate all messages into one and display.

## Sample Rendering Implementation

This is our implementation. Feel free to use it as a starting point and modify as needed:
```
(function () {
    var aliases = [
        { key: '{!Audience}', val: Symphony.Aliases.audience || "Audience" },
        { key: '{!Audiences}', val: Symphony.Aliases.audiences || "Audience" },
        { key: '{!Course}', val: Symphony.Aliases.course || "Audience" },
        { key: '{!Courses}', val: Symphony.Aliases.courses || "Audience" },
        { key: '{!JobRole}', val: Symphony.Aliases.jobRole || "Audience" },
        { key: '{!JobRoles}', val: Symphony.Aliases.jobRoles || "Audience" },
        { key: '{!Location}', val: Symphony.Aliases.location || "Audience" },
        { key: '{!Locations}', val: Symphony.Aliases.locations || "Audience" },
        { key: '{!Username}', val: Symphony.Aliases.username || "Audience" },
        { key: '{!audience}', val: (Symphony.Aliases.audience || "Audience").toLowerCase() },
        { key: '{!audiences}', val: (Symphony.Aliases.audiences || "Audiences").toLowerCase() },
        { key: '{!course}', val: (Symphony.Aliases.course || "Course").toLowerCase() },
        { key: '{!courses}', val: (Symphony.Aliases.courses || "Courses").toLowerCase() },
        { key: '{!jobrole}', val: (Symphony.Aliases.jobRole || "Job Role").toLowerCase() },
        { key: '{!jobroles}', val: (Symphony.Aliases.jobRoles || "Job Roles").toLowerCase() },
        { key: '{!location}', val: (Symphony.Aliases.location || "Location").toLowerCase() },
        { key: '{!locations}', val: (Symphony.Aliases.locations || "Locations").toLowerCase() },
        { key: '{!username}', val: (Symphony.Aliases.username || "Username").toLowerCase() }
    ];

    var _dv = function (displayValue) {
        // For getting the value with any value
        // that has an associated code.
        //
        // This will allow us to do any last minute overrides
        // based on the code or any settings. In most cases
        // the value should be exactly what we want from 
        // the server, but since we have the option
        // why not.
        var result = "-";

        if (displayValue && displayValue.value) {
            result = displayValue.value;
        }
        if (typeof(displayValue) == 'string') {
            result = displayValue;
        }

        for (var i = 0; i < aliases.length; i++) {
            result = result.replace(new RegExp(aliases[i].key, "g"), aliases[i].val);
        }

        if (displayValue && displayValue.messageDate) {
            result = result.replace("{!date}", Symphony.Renderer.displayDateRenderer(displayValue.messageDate));
            
            var minutes = displayValue.messageDate.relativeTicks / 1000 / 60; 
            result = result.replace("{!date_relative}", Symphony.Classroom.durationRenderer(minutes));
        }

        return result;
    }

    Symphony.Renderer = {
        
        getMessageTooltip: function (displayMessages) {
            var tooltipText = "";
            var messageArray = [];
            
            if (typeof (displayMessages) == 'string') {
                messageArray.push(displayMessages);
            } else if (displayMessages.length) {
                for (var i = 0; i < displayMessages.length; i++) {
                    messageArray.push(_dv(displayMessages[i]));
                }

                if (messageArray.length > 0) {
                    tooltipText = messageArray.length > 1 ?
                        "<ul><li>" + messageArray.join("</li><li>") + "</li></ul>" :
                        messageArray[0];
                }
            }

            return tooltipText;
        },
        displayTextRenderer: function(display) {
            var messages = Symphony.Renderer.displayMessageRenderer(display);

            if (messages !== false) {
                return messages;
            }

            return _dv(display);
        },
        displayIconRenderer: function (display) {
            var tooltip = Symphony.Renderer.getMessageTooltip(display.messages);
            var icon = !display.isEmpty ? '<img ext:qtip="{0}" src="{1}"/>'.format(tooltip, _dv(display.icon)) : "";

            if (display.isEmpty && display.messages.length > 0) {
                var displayMessage = display.messages.shift();
                tooltip = display.messages.length > 0 ? Symphony.Renderer.getMessageTooltip(display.messages) : "";
                return '<span ext:qtip="{0}">{1}</span>'.format(tooltip, _dv(displayMessage));
            }

            if (display.link && !display.link.isEmpty) {
                display.link.title = icon;
                icon = Symphony.Renderer.displayLinkRenderer(display.link);
            }

            return icon;
        },
        displayDateRenderer: function (display) {
            var messages = Symphony.Renderer.displayMessageRenderer(display);
            
            if (messages !== false) {
                return messages;
            }

            var dt = Symphony.parseDate(display.date, display.addLocalOffset);

            return dt.formatSymphony(_dv(display.format));
        },
        displayMessageRenderer: function (display) {
            if (display.messages && display.messages.length > 0) {
                if (!display.messageValues.length) {
                    return false;
                }

                var text = display.title ? _dv(display.title) : _dv(display.messages[0]);

                if (text == _dv(display.messages[0])) {
                    display.messages.shift();
                }

                var helpText = Symphony.Renderer.getMessageTooltip(display.messages);

                if (text && !helpText) {
                    return text;
                }

                return Symphony.qtipRenderer(text, helpText, 'help-icon');
            }

            return false;
        },
        displayLinkRenderer: function (display) {
            var messages = Symphony.Renderer.displayMessageRenderer(display);

            if (messages !== false && display.isEmpty) {
                return messages;
            }

            // extract query params
            var params = {};
            for (var i = 0; i < display.queryParams.length; i++) {
                params[display.queryParams[i].key] = display.queryParams[i].value;
            }

            // render the link
            var link = new Ext.Element(document.createElement("a"))
                .set({
                    id: display.domId,
                    "class": display.domClass,
                    href: display.fullUrl
                })
                .update(messages !== false ? messages : _dv(display.title))

            if (display.isNewWindow) {
                link.set({ target: '_blank' });
            }

            return link.dom.outerHTML;
        },
        displayModelRenderer: function (display) {
            if (!display || !display.messages) {
                return "";
            }

            var message = "";
            for (var i = 0; i < display.messages.length; i++) {
                message += _dv(display.messages[i]);
            }

            return message;
        }
       
    }
})()
```










 









