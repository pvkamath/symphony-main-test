﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symphony.ThemeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] subdomains = new string[]{
                "hawaiibor",
                "kauai",
                //"ralphgoulgers",
                "vitousek",
                "abelee",
                "realclass",
                "ralphfoulgers",
                "RETCCO",
                "russgoode",
                "academyre",
                "hudsongateway",
                "cbpacificproperties"
            };

            string root = @"c:\temp\out";
            string imagesFolder = @"C:\temp\reseller_logos";

            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }



            string templateAppFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Template", "SymphonyApp", "be");
            string templateCourseFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Template", "SymphonyCourse", "be");
            string loginCss = File.ReadAllText(Path.Combine(templateAppFolder, "login.css"));
            string mainCss = File.ReadAllText(Path.Combine(templateAppFolder, "main.css"));

            foreach (var subdomain in subdomains) //Directory.GetFiles(@"c:\temp\reseller_logos\"))
            {
                string appTarget = Path.Combine(root, "Template", "SymphonyApp", subdomain);
                DirectoryInfo di = Directory.CreateDirectory(appTarget);
                string image = string.Empty;

                foreach (var ext in new string[] { "png", "jpeg", "jpg" })
                {
                    string file = subdomain + "." + ext;
                    if (File.Exists(Path.Combine(imagesFolder, file)))
                    {
                        image = file;
                        break;
                    }
                }

                if (string.IsNullOrEmpty(image))
                {
                    throw new Exception("No image found for " + subdomain);
                }

                File.WriteAllText(Path.Combine(appTarget, "login.css"), loginCss.Replace("{logo}", image));
                File.WriteAllText(Path.Combine(appTarget, "main.css"), mainCss.Replace("{logo}", image));
                
                // copy course theme
                // add course theme logo

                //site_logo.png
                string courseTarget = Path.Combine(root, "Template", "SymphonyCourse", subdomain);
                CopyFolder(templateCourseFolder, courseTarget);

                // add course theme logo
                using (var bmp = new System.Drawing.Bitmap(Path.Combine(imagesFolder, image)))
                {
                    bmp.Save(Path.Combine(courseTarget, "img", "site_logo.png"), ImageFormat.Png);
                }
            }
        }

        public static void CopyFolder(string sourceFolder, string destFolder)
        {
            if (Path.GetFileName(destFolder).StartsWith("."))
            {
                return;
            }

            if (!Directory.Exists(destFolder))
                Directory.CreateDirectory(destFolder);
            string[] files = Directory.GetFiles(sourceFolder);
            foreach (string file in files)
            {
                string name = Path.GetFileName(file);
                string dest = Path.Combine(destFolder, name);
                File.Copy(file, dest, true);
                // make sure the file shows a change in it's write time
                File.SetLastWriteTime(dest, DateTime.Now);
            }
            string[] folders = Directory.GetDirectories(sourceFolder);
            foreach (string folder in folders)
            {
                string name = Path.GetFileName(folder);
                string dest = Path.Combine(destFolder, name);
                CopyFolder(folder, dest);
            }
        }
    }


}
