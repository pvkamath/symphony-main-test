﻿var Connection = require('tedious').Connection,
    Q = require('q'),
    LZ = require('lz-string'),
    squel = require('squel'),
    mssql = squel.useFlavour("mssql"),
    Request = require('tedious').Request,
    TYPES = require('tedious').TYPES,
    fs = require('fs'),
    // Prod config
    configProd = {
        userName: 'iis-sql',
        password: 'pa55w0rd!',
        server: '10.68.80.93',
        options: { encrypt: false, database: 'Symphony', requestTimeout: 120000 }
    },
    // Sandbox config
    configSandbox = {
        userName: 'ocl-qasym-test',
        password: 'qasym123!',
        server: '10.68.80.125',
        options: { encrypt: false, database: 'Symphony' }
    },
    connection = new Connection(configProd),
    colMap = {};
    

connection.on('connect', function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log("Connected");
        run();
    }
});

function getColumnValue(columns, colName) {
    if (typeof (colMap[colName]) === 'undefined') {
        for (var i = 0; i < columns.length; i++) {
            colMap[columns[i].metadata.colName] = i;
        }
    }

    return columns[colMap[colName]].value;
}

function createTempTable() {
    console.log("creating temp table");
    return new Promise(function (resolve, reject) {
        try {
            var sql = [
                "if (OBJECT_ID('Symphony.dbo.tempCourseRecords')is not null)",
                "BEGIN",
                "    drop table Symphony.dbo.tempCourseRecords",
                "end",
                "create table Symphony.dbo.tempCourseRecords",
                "(",
                "    OnlineCourseRollupID int,",
                "    OnlineCourseRollupRegistrationID nvarchar(50), ",
                "    NavigationPercentage int, ",
                "    UserID int,",
                "    CourseID int,",
                "    TrainingProgramID int,",
                "    Bookmark nvarchar(50),",
                "    CustomerID int,",
                "    DataChunk nvarchar(max),",
                "    ArtisanCourseID int,",
                "    FirstName nvarchar(80),",
                "    LastName nvarchar(80),",
                "    PagesVisited int,",
                "    LearningObjectsVisited int,",
                "    ScosVisited int,",
                "    BookmarkPercentage int,",
                "    HighScoreDate datetime,",
                "    LastLaunchedOn datetime,",
                "    ModifiedOn datetime",
                ")"
            ],
                request = new Request(sql.join("\r\n"), function (err) {
                    if (err) {
                        console.log(err);
                        reject(err);
                    }
                });
            
            request.on('requestCompleted', function () {
                resolve();
                console.log("Created temporary table");
            });
            
            connection.execSql(request);
        } catch(e) {
            reject(e);
        }
    });
}

function getCourseIdsWithQuizzes() {
    console.log("getting course ids with quizzes");
    return new Promise(function (resolve, reject) {
        try {
            var result = [],
                sql = mssql.select()
                    .field("s.CourseID")
                    .from("ArtisanSections", "s")
                    .join("ArtisanCourses", "a", "a.ID = s.CourseID")
                    .where("s.IsReRandomizeOrder = 0 and s.IsQuiz = 1 and a.IsPublished = 1")
            
            request = new Request(sql.toString(), function (err) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
            });
            
            request.on("row", function (columns) {
                result.push(columns[0].value);
            });
            
            request.on('requestCompleted', function () {
                console.log(result.length + ' rows returned');
                resolve(result);  
            });
            
            connection.execSql(request);
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });   
}

function getCourseRecordsWithParsedDataChunks(artisanCourseIds) {
    console.log("Getting course records for datachunk parsing");
    return new Promise(function (resolve, reject) {
        try {
            var result = [],
                sql = mssql.select({ separator: "\n" })
            .field("u.FirstName")
            .field("u.LastName")
            .field("u.ID", "UserID")
            .field("ro.CourseID")
            .field("ro.TrainingProgramID")
            .field("rt.suspend_data")
            .field("rt.location")
            .field("c.PublishedArtisanCourseID", "ArtisanCourseID")
            .field("ro.ID", "OnlineCourseRollupID")
            .field("r.scorm_registration_id", "OnlineCourseRollupRegistrationID")
            .field("ro.NavigationPercentage")
            .field("u.CustomerID")
            .field("ro.HighScoreDate")
            .field("ro.LastLaunchedOn")
            .field("ro.ModifiedOn")
            .field("ro.ResetNotes")
            .from("OnlineTraining.dbo.ScormRegistration", "r")
            .join("OnlineTraining.dbo.ScormActivity", "a", "a.scorm_registration_id = r.scorm_registration_id")
            .join("OnlineTraining.dbo.ScormActivityRT", "rt", "rt.scorm_activity_id = a.scorm_activity_id")
            .join("Symphony.dbo.OnlineCourseRollup", "ro", "ro.UserID = r.user_id and ro.CourseID = r.course_id and ro.TrainingProgramID = r.training_program_id")
            .join("Symphony.dbo.[User]", "u", "u.ID = ro.UserID")
            .join("Symphony.dbo.OnlineCourse", "c", "c.ID = ro.CourseID")
            .where("ro.HighScoreDate > '2016-09-29' and c.PublishedArtisanCourseID in ?", artisanCourseIds);
            
            request = new Request(sql.toString(), function (err) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
            });
            var count = 0;

            request.on("row", function (columns) {
                if (count % 100 == 0) {
                    console.log("Parsed " + count + " data chunks");   
                }

                var record = {
                    userId: getColumnValue(columns, "UserID"),
                    firstName: getColumnValue(columns, "FirstName"),
                    lastName: getColumnValue(columns, "LastName"),
                    courseId: getColumnValue(columns, "CourseID"),
                    customerId: getColumnValue(columns, "CustomerID"),
                    navigationPercentage: getColumnValue(columns, "NavigationPercentage"),
                    trainingProgramId: getColumnValue(columns, "TrainingProgramID"),
                    suspendData: getColumnValue(columns, "suspend_data"),
                    dataChunkJson: "",
                    dataChunk: {},
                    location: getColumnValue(columns, "location"),
                    pagesVisited: 0,
                    learningObjectsVisited: 0,
                    scosVisited: 0,
                    artisanCourseId: getColumnValue(columns, "ArtisanCourseID"),
                    onlineCourseRollupId: getColumnValue(columns, "OnlineCourseRollupID"),
                    onlineCourseRollupRegistrationId: getColumnValue(columns, "OnlineCourseRollupRegistrationID"),
                    locationPercentage: 0,
                    highScoreDate: getColumnValue(columns, "HighScoreDate"),
                    lastLaunchedOn: getColumnValue(columns, "LastLaunchedOn"),
                    modifiedOn: getColumnValue(columns, "ModifiedOn")
                }

                if (record.suspendData) {
                    
                    if (record.suspendData.startsWith("gz:")) {
                        record.dataChunkJson = LZ.decompressFromEncodedURIComponent(record.suspendData.replace("gz:", ""));
                    } else {
                        record.dataChunkJson = record.suspendData;
                    }
                    
                    record.dataChunk = JSON.parse(record.dataChunkJson);

                    record.learningObjectsVisited = record.dataChunk.vl && Object.keys(record.dataChunk.vl).length ? Object.keys(record.dataChunk.vl).length : 0;
                    record.scosVisited = record.dataChunk.v && Object.keys(record.dataChunk.v).length ? Object.keys(record.dataChunk.v).length : 0;

                    // Exclude dynamic pages since we can't determine these from db. 
                    record.pagesVisited = 0;
                    if (record.dataChunk.vp && Object.keys(record.dataChunk.vp.length > 0)) {
                        for (var key in record.dataChunk.vp) {
                            if (record.dataChunk.vp.hasOwnProperty(key)) {
                                if (parseInt(key, 10) > 0) {
                                    record.pagesVisited++;
                                }
                            }
                        }
                    }

                }

                if (record.location) {
                    var params = record.location.split('|');
                    record.locationPercentage = params[4] ? Number(params[4]) : 0
                }
                                 
                result.push(record);
                count++;
            });
            
            request.on('requestCompleted', function () {
                resolve(result);
                console.log(count + ' rows returned');
            });
            
            connection.execSql(request);
        } catch (e) {
            reject(e);
        }
    });
}

function insertRecordsToTemp(records) {
    console.log("inserting data");
    return new Promise(function (resolve, reject) {
        try {
            var sql,
                data = [],
                i = 0,
                insertStatements = [],
                resolveMe = resolve,
                rejectMe = reject;
            console.log("Inserting : " + records.length + " records");
 
            while (i < records.length) {
                for (i; i < records.length && data.length < 1000; i++) {
                    data.push({
                        OnlineCourseRollupID: records[i].onlineCourseRollupId,
                        OnlineCourseRollupRegistrationID: records[i].onlineCourseRollupRegistrationId,
                        NavigationPercentage: records[i].navigationPercentage,
                        UserID: records[i].userId,
                        CourseID: records[i].courseId,
                        TrainingProgramID: records[i].trainingProgramId,
                        Bookmark: records[i].location,
                        CustomerID: records[i].customerId,
                        DataChunk: records[i].dataChunkJson,
                        ArtisanCourseID: records[i].artisanCourseId,
                        FirstName: records[i].firstName,
                        LastName: records[i].lastName,
                        PagesVisited: records[i].pagesVisited,
                        LearningObjectsVisited: records[i].learningObjectsVisited,
                        ScosVisited: records[i].scosVisited,
                        BookmarkPercentage: records[i].locationPercentage,
                        HighScoreDate: records[i].highScoreDate,
                        LastLaunchedOn: records[i].lastLaunchedOn,
                        ModifiedOn: records[i].modifiedOn
                    });
                }

                sql = mssql.insert()
                    .into("Symphony.dbo.tempCourseRecords")
                    .setFieldsRows(data)
                
                //console.log("Adding sql statement: " + sql.toString());
                insertStatements.push(sql.toString());
                data = [];
            }
            
            insertStatements.push("finish");
            
            insertStatements.reduce(function (promise, sql) {
                return promise.then(function () {
                    return new Promise(function (resolve, reject) {
                        if (sql === "finish") {
                            resolve();
                            resolveMe();
                            return;  
                        }
                        try {
                            console.log("Inserting block of 1000 records to temp table...");
                            var request = new Request(sql, function (err) {
                                if (err) {
                                    console.log(err);
                                    reject(err);
                                }
                            });
                            request.on('requestCompleted', function (rowCount, more) {
                                resolve();
                                console.log('Data insert complete');
                            });
                            
                            connection.execSql(request);
                        } catch (ex) {
                            console.log(ex);
                            reject(ex);
                        }
                    });
                }, function (ex) {
                    console.log("Rejected");
                    console.log(ex);
                    rejectMe(ex);               
                });
            }, Q());

        } catch (e) {
            reject(e);
        }
    });
}

function checkForSkips() {
    console.log("checking for skips");
    return new Promise(function (resolve, reject) {
        try {
            
            var sql = mssql.select({ separator: "\n" })
                .from(mssql.select({ separator: "\n" })
                    .field("c.Name", "CustomerName")
                    .field("tp.Name", "TrainingProgramName")
                    .field("o.Name", "CourseName")
                    .field("r.LastName")
                    .field("r.FirstName")
                    .field("r.CustomerID")
                    .field("r.TrainingProgramID")
                    .field("r.CourseID")
                    .field("r.ArtisanCourseID")
                    .field("r.UserID")
                    .field("r.Bookmark")
                    .field("r.DataChunk")
                    .field("r.NavigationPercentage")
                    .field("r.BookmarkPercentage")
                    .field("case when co.TotalPages > 0 then (cast(r.PagesVisited as float) / cast(co.TotalPages as float)) * 100 else -1 end", "DataChunkCalculatedPercentage")
                    .field("co.TotalPages")
                    .field("r.PagesVisited")
                    .field("r.LearningObjectsVisited")
                    .field("r.ScosVisited")
                    .field("r.OnlineCourseRollupID")
                    .field("r.OnlineCourseRollupRegistrationID")
                    .field("assign.AssignmentsRequired")
                    .field("assign.AssignmentsComplete")
                    .field("tpr.DateCompleted", "TrainingProgramCompletionDate")
                    .field("tpr.IsComplete", "IsTrainingProgramComplete")
                    .field("tpr.PercentComplete", "TrainingProgramPercentComplete")
                    .field("tpr.FinalExamScore", "TrainingProgramFinalExamScore")
                    .field("r.HighScoreDate")
                    .field("r.LastLaunchedOn")
                    .field("r.ModifiedOn")
                    .field("ro.ResetNotes")
                    .from("tempCourseRecords", "r")
                    .join("OnlineCourseRollup", "ro", "ro.ID = r.OnlineCourseRollupID")
                    .join("Customer", "c", "c.ID = r.CustomerID")
                    .join("TrainingProgram", "tp", "tp.ID = r.TrainingProgramID")
                    .join("OnlineCourse", "o", "o.ID = r.CourseID")
                    .join("TrainingProgramRollup", "tpr", "tpr.TrainingProgramID = ro.TrainingProgramID and tpr.UserID = ro.UserID")
                    .left_join(mssql.select({ separator: "\n" })
                        .field("a.OnlineCourseRollupID")
                        .field("a.TrainingProgramID")
                        .field("a.CourseID")
                        .field("a.UserID")
                        .field("a.ArtisanCourseID")
                        .field("a.AssignmentsRequired")
                        .field("count(sa.ID)", "AssignmentsComplete")
                        .from(mssql.select({ separator: "\n" })
                            .field("ro.ID", "OnlineCourseRollupID")
                            .field("ro.TrainingProgramID")
                            .field("ro.CourseID")
                            .field("ro.UserID")
                            .field("aa.ArtisanCourseID")
                            .field("count(s.ID)", "AssignmentsRequired")
                            .from("OnlineCourseRollup", "ro")
                            .join("ArtisanAudit", "aa", "aa.ID = ro.ArtisanAuditID")
                            .join("AssignmentSummary", "s", "s.ArtisanCourseID = aa.ArtisanCourseID")
                            .group("ro.TrainingProgramID")
                            .group("ro.CourseID")
                            .group("ro.UserID")
                            .group("ro.ID")
                            .group("aa.ArtisanCourseID"), "a")
                        .join("AssignmentSummary", "s", "s.ArtisanCourseID = a.ArtisanCourseID")
                        .left_join("AssignmentSummaryAttempts", "sa", [
                            "sa.TrainingProgramID = a.TrainingProgramID and",
                            "sa.OnlineCourseID = a.CourseID and",
                            "sa.UserID = a.UserID and",
                            "sa.AssignmentSummaryID = s.ID and",
                            "sa.IsComplete = 1 and",
                            "sa.IsMarked = 1"].join(" "))
                        .group("a.OnlineCourseRollupID")
                        .group("a.TrainingProgramID")
                        .group("a.CourseID")
                        .group("a.UserID")
                        .group("a.ArtisanCourseID")
                        .group("a.AssignmentsRequired")
                    , "assign", "assign.OnlineCourseRollupID = ro.ID")
                    .join(mssql.select({ separator: "\n" })
                        .field("c.ArtisanCourseID")
                        .field("sum(case when c.MaxQuestions > 0 and c.MaxQuestions < c.Pages then c.MaxQuestions else c.Pages end)", "TotalPages")
                        .from(mssql.select({ separator: "\n" })
                            .field("c.ID", "ArtisanCourseID")
                            .field("s.ID", "SectionID")
                            .field("case when s.TestType > 0 or s.IsQuiz = 1 then s.MaxQuestions else 0 end as MaxQuestions")
                            .field("count(sp.ID) as Pages")
                            .from("ArtisanCourses", "c")
                            .join("ArtisanSections", "s", "s.CourseID = c.ID")
                            .join("ArtisanSectionPages", "sp", "sp.SectionID = s.ID")
                            .where("c.IsPublished = 1 and c.NavigationMethod = 3 and c.CompletionMethod = 1")
                            .group("c.ID")
                            .group("s.ID")
                            .group("s.MaxQuestions")
                            .group("s.TestType")
                            .group("s.IsQuiz"), "c")
                        .group("c.ArtisanCourseID"), "co", "co.ArtisanCourseID = o.PublishedArtisanCourseID")
                    .where("ro.Success = 1 and ro.Completion = 1 and ro.ResetNotes not like '%Bumped Forward%' and ro.ResetNotes not like '%Changed Passed to true%' and ro.ResetNotes not like '%Changed Success Flag to true%'"), "r")
                 .where("r.TotalPages > r.PagesVisited")
                 .order("r.CustomerName")
                 .order("r.TrainingProgramName")
                 .order("r.CourseName")
                 .order("r.LastName")
                 .order("r.FirstName")

            request = new Request(sql.toString(), function (err) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
            });
            
            var resultData = "";
            var columnHeader = "";
            var rowCount = 0;

            request.on("row", function (columns) {
                if (rowCount % 100 === 0) {
                    console.log(rowCount + " rows read");
                }
                var header = "";
                var row = "";

                columns.forEach(function (col) {
                    if (col.metadata.colName === 'HighScoreDate' ||
                       col.metadata.colName === 'LastLaunchedOn' ||
                        col.metadata.colName === 'ModifiedOn') {
                        var x = Date.parse(col.value);
                        var d = new Date(x);
                        col.value = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    }


                    row += col.value + "\t";
                    header += col.metadata.colName + "\t";
                });
                
                if (!columnHeader) {
                    columnHeader = header + "\r\n";   
                }
                
                resultData += row + "\r\n";

                rowCount++;
            })
            
            request.on('requestCompleted', function () {
                resolve();
                
                resultData = columnHeader + resultData;
                
                fs.writeFile("/working/logs/output.txt", resultData, function (err) {
                    if (err) {
                        console.log("could not output to file");
                        console.log(err);
                    } else {
                        console.log("The file was saved");
                    }
                });

                console.log(rowCount + ' skipped records found');
            });
            
            connection.execSql(request);
        } catch (e) {
            reject(e);
        }
    });     
}

function dropTempTable() {
    console.log("deleting temp table");
    return new Promise(function (resolve, reject) {
        try {
            var sql = ["if (OBJECT_ID('Symphony.dbo.tempCourseRecords')is not null)",
                "BEGIN",
                "   drop table Symphony.dbo.tempCourseRecords",
                "end"];
            request = new Request(sql.join("\r\n"), function (err) {
                if (err) {
                    console.log(err);
                    reject();
                }
            });
            
            request.on('requestCompleted', function (rowCount, more) {
                console.log('temp table deleted');
                resolve();
            });
            
            connection.execSql(request);
        } catch (e) {
            reject(e);
        }
    });
}

function run() {
    createTempTable()
        .then(getCourseIdsWithQuizzes)
        .then(getCourseRecordsWithParsedDataChunks)
        .then(insertRecordsToTemp)
        .then(checkForSkips)
        .then(dropTempTable)
        .then(function () {
            console.log("complete "); 
        })
        .catch(function (e) {
            console.log(e);
        });
}



