﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Symphony.Saml.Test._Default" %>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <form id="myform" runat="server" defaultbutton="btnIdPLogin">
        <div class="templatecontent">
            <% if (string.IsNullOrEmpty(Context.User.Identity.Name)) { %>
                <p>Login Customer:</p>
                <asp:TextBox runat="server" ID="customer" Text="be"></asp:TextBox>
                <p><asp:Button runat="server" CssClass="okbutton" ID="btnIdPLogin" Text="Login to Symphony" OnClick="btnIdPLogin_Click" /></p>
            <% } else { %>
                <h1>Welcome to Test Service B!!!</h1>
                <p> Logged in as: <%=Context.User.Identity.Name %></p>
                <asp:Button runat="server" CssClass="button" ID="btnLogout" Text="Logout from Symphony" OnClick="btnLogout_Click" />
            <% } %>

            

            
        </div>
    </form>
</asp:Content>
