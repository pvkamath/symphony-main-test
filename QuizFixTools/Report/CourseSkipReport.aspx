﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="System.Data.SqlClient" %>

<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name, filename) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }

                document.getElementById("dlink").href = uri + base64(format(template, ctx));
                document.getElementById("dlink").download = filename;
                document.getElementById("dlink").click();

            }
        })()
    </script>
<style>
    body {
        font-size: 11px;
        font-family: Arial;
    }

    table {
        border-collapse:collapse;
        font-size: 10px;
    }
    table th,
    table td {
        border: 1px solid #ccc;
    }
    table tr.row-complete td {
        background: #fe8345;
    }
    td.reset {
        padding: 10px;
    }
</style>
</head>
<body>
    <a id="dlink"  style="display:none;"></a>

<input type="button" onclick="tableToExcel('IncompleteUsers', 'IncompleteUsers', 'IncompleteUsers.xls')" value="Export to Excel">
    <table id="IncompleteUsers">
        <tr>
            <th>CustomerName</th>
            <th>TrainingProgramID</th>
            <th>TrainingProgramName</th>
            <th>TrainingProgramRollupID</th>
            <th>DateCompleted</th>
            <th>Sku</th>
            <th>UserID</th>
            <th>Username</th>
            <th>NMLS #</th>
            <th>FirstName</th>
            <th>LastName</th>
            <th>CourseID</th>
            <th>ArtisanCourseID</th>
            <th>totaltime</th>
            <th>CourseName</th>
            <th>requiredTime</th>
            <th>ModifiedOn</th>
            <th>OnlineCourseRollup</th>
            <th>ScormRegistrationID</th>
            <th>timepercent</th>
        </tr>

    
<%
    List<int> userIds = new List<int>();
    string conString = ConfigurationManager.ConnectionStrings["Symphony"].ConnectionString;
    using (SqlConnection connection = new SqlConnection(conString))
    {
        connection.Open();



        using (SqlCommand command = new SqlCommand(@"
                select 
                    x.CustomerName,
                    x.TrainingProgramID,
                    x.TrainingProgramName,
                    x.TrainingProgramRollupID,
                    x.DateCompleted,
                    x.Sku,
                    x.UserID,
                    x.Username, 
                    x.NmlsNumber,
                    x.FirstName,
                    x.LastName,
                    x.CourseID,
                    x.ArtisanCourseID,
                    x.totaltime,  
                    x.CourseName,
                    x.requiredTime,
                    x.ModifiedOn,
                    x.OnlineCourseRollupID,
                    x.ScormRegistrationID,
                    x.timepercent,
                    tpr.IsComplete,
                    ro.ResetNotes
                from 
                    fGetInvalidCompletions_ExcludeCompleteTP(null) x
                    join TrainingProgramRollup tpr on tpr.ID = x.TrainingProgramRollupID
                    join OnlineCourseRollup ro on ro.ID = x.OnlineCourseRollupID
                ", connection))
        {
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    try
                    {
                        DateTime? dateCompleted = null;

                        string customerName = reader.GetString(0);
                        int trainingProgramId = reader.GetInt32(1);
                        string trainingProgramName = reader.GetString(2);
                        int trainingProgramRollupId = reader.GetInt32(3);
                        if (!reader.IsDBNull(4))
                        {
                            dateCompleted = reader.GetDateTime(4);
                        }
                        string sku = !reader.IsDBNull(5) ? reader.GetString(5) : "";
                        int userId = reader.GetInt32(6);
                        string username = reader.GetString(7);
                        string nmlsNumber = !reader.IsDBNull(8) ? reader.GetString(8) : "";
                        string firstName = reader.GetString(9);
                        string lastName = reader.GetString(10);
                        int courseId = reader.GetInt32(11);
                        int artisanCourseId = reader.GetInt32(12);
                        int totaltime = reader.GetInt32(13);
                        string courseName = reader.GetString(14);
                        int requiredTime = reader.GetInt32(15);
                        DateTime modifiedOn = reader.GetDateTime(16);
                        int onlineCourseRollupId = reader.GetInt32(17);
                        int scormRegistrationId = reader.GetInt32(18);
                        double timePercent = reader.GetDouble(19);
                        bool isTpComplete = reader.GetBoolean(20);
                        string resetNotes = reader.GetString(21);

                        userIds.Add(userId);

                        string row = string.Format(@"<tr class='row{20}'>
                            <td>{0}</td>
                            <td>{1}</td>
                            <td>{2}</td>
                            <td>{3}</td>
                            <td>{4}</td>
                            <td>{5}</td>
                            <td>{6}</td>
                            <td>{7}</td>
                            <td>{8}</td>
                            <td>{9}</td>
                            <td>{10}</td>
                            <td>{11}</td>
                            <td>{12}</td>
                            <td>{13}</td>
                            <td>{14}</td>
                            <td>{15}</td>
                            <td>{16}</td>
                            <td>{17}</td>
                            <td>{18}</td>
                            <td>{19}</td>
                        </tr>", customerName, trainingProgramId, trainingProgramName, trainingProgramRollupId, dateCompleted.HasValue ? dateCompleted.Value.ToString() : "", sku, userId, username, nmlsNumber,
                        firstName, lastName, courseId, artisanCourseId, totaltime, courseName, requiredTime, modifiedOn.ToString(), onlineCourseRollupId, scormRegistrationId, timePercent * 100,
                        isTpComplete ? "-complete" : "-incomplete");

                        Response.Write(row);
                        if (!string.IsNullOrWhiteSpace(resetNotes))
                        {
                            Response.Write("<tr><td colspan='20' class='reset'><strong>Reset Notes:</strong><br/>" + resetNotes.Replace(";", "<br/>") + "</td></tr>");
                        }

                    } catch (Exception e)
                    {
                        Response.Write("<tr><td colspan='20'>" + e.ToString() + "</td></tr>");
                    }
                }
            }
        }
    }

%>
        </table>

    <%
        string userIdsString = string.Join("<br/>", userIds.Distinct().ToList());
        Response.Write("<div><h4>UserIDs List: </h4>" + userIdsString + "</div>");
    %>
    </body>
    </html>