﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<html>
    <body>
        <form runat="server" id="form">
            <textarea cols="50" rows="30" id="input" runat="server"></textarea>
            <button>Submit</button>
        </form>
    </body>
</html>
<% 
    if (Request.HttpMethod == "POST")
    {
        %>

<table>
    <tr>
        <th>Email</th>
        <th>Valid User</th>
        <th>Invalid Completions</th>
    </tr>

<%

        string conString = ConfigurationManager.ConnectionStrings["Symphony"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(conString))
        {
            connection.Open();
            
            string[] emails = this.input.Value.Split('\n'); // new string[] { "dina.pierson@supremelending.com", "jvenema@gmail.com" };

            foreach (var temp in emails)
            {
                var email = temp.Trim();
                bool isValidCompletion = true;
                bool isValidUser = false;
                
                using (SqlCommand command = new SqlCommand("select top 1 * from [user] where username = @email", connection))
                {
                    command.Parameters.AddWithValue("@email", email);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            isValidUser = true;
                            break;
                        }
                    }
                }

                string invalidList = "";
                using (SqlCommand command = new SqlCommand("select sku, coursename from dbo.[fGetInvalidCompletions2](@email)", connection))
                {
                    command.Parameters.AddWithValue("@email", email);

                    List<string> invalid = new List<string>();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            /*for (int i = 0; i < reader.FieldCount; i++)
                            {
                                Console.WriteLine(reader.GetValue(i));
                            }
                            Console.WriteLine();*/
                            isValidCompletion = false;
                            string sku = reader.GetString(0); // sku
                            string course = reader.GetString(1); //course

                            invalid.Add("SKU: " + sku + ", Module: " + course);
                        }
                    }

                    if (invalid.Count > 0)
                    {
                        invalidList = string.Join("<br/>", invalid.ToArray());
                    }
                    else
                    {
                        invalidList = " - ";
                    }
                }

                Response.Write("<tr>");
                Response.Write("<td>" + email + "</td>");

                if (isValidUser)
                {
                    Response.Write("<td>Yes</td>");
                }
                else
                {
                    Response.Write("<td>No</td>");
                }
                
                if (isValidCompletion)
                {
                    Response.Write("<td> - </td>");
                }
                else
                {
                    Response.Write("<td><b>" + invalidList + "</b></td>");
                }
                Response.Write("</tr>");
            }
        }
        
        %>
    
</table>
    <%
    }
%>