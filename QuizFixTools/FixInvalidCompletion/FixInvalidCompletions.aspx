﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <% 
            if (Request.HttpMethod == "POST")
            {%>
        <script type="text/javascript">
            $(document).ready(function () {

                $('a').each(function (i, o) {
                    var a = $(o);
                    var x = window.open(a.attr('href'));
                    x.addEventListener('open', function (event) {

                        setTimeout(function () {
                            event.target.close();
                        }, 30000)
                    });
                });


            });
        </script>
<%            }%>
    </head>
    <body>
        <form runat="server" id="form">
            <textarea cols="50" rows="30" id="input" runat="server"></textarea>
            <button>Submit</button>
        </form>
    </body>
</html>
<%
    if (Request.HttpMethod == "POST")
            {
                string conString = ConfigurationManager.ConnectionStrings["Symphony"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(conString))
                {
                    connection.Open();

                    string[] ids = this.input.Value.Split('\n'); // new string[] { "dina.pierson@supremelending.com", "jvenema@gmail.com" };

                    string sqlQueryIds = string.Join(",", ids);

                    List<string> urls = new List<string>();
                    using (SqlCommand command = new SqlCommand("select UserID, CourseID, TrainingProgramID, OnlineCourseRollupID, TrainingProgramRollupID, ScormRegistrationID from fGetInvalidCompletions_ExcludeCompleteTP(null) where UserID in (" + sqlQueryIds + ")",connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int userID = reader.GetInt32(0);
                                int courseID = reader.GetInt32(1);
                                int trainingProgramID = reader.GetInt32(2);

                                urls.Add(string.Format("https://symphony.ocltraining.com/scorm/defaultui/launch.aspx?configuration=&amp;registration=UserId|{0}!CourseId|{1}!TrainingProgramId|{2}", userID, courseID, trainingProgramID));
                            }
                        }
                    }

                    string fixCommandSql = string.Format(@"
                    if (OBJECT_ID('tempdb..#tempIds') is not null) 
	                    begin
		                    drop table #tempIds
	                    end

	                    create table #tempIds
	                    (
		                    OnlineCourseRollupID int,
		                    TrainingProgramRollupID int,
		                    ScormRegistrationID int
	                    )

	                    insert into #tempIds
                        select OnlineCourseRollupID, TrainingProgramRollupID, ScormRegistrationID
	                    from fGetInvalidCompletions_ExcludeCompleteTP(null) where UserID in ({0})

	                    --- Update scorm
	                    update OnlineTraining.dbo.ScormActivityrt 
	                    set 
	                        completion_status = 1,
	                        score_raw = null,
	                        score_scaled = null,
	                        score_max = null,
	                        score_min = null,
	                        progress_measure = null,
	                        success_status = 1,
		                    location = null
	                    where scorm_activity_id in 
	                    (
		                    select rt.scorm_activity_id from OnlineTraining.dbo.ScormRegistration reg 
		                    join OnlineTraining.dbo.ScormActivity a on a.scorm_registration_id = reg.scorm_registration_id
		                    join OnlineTraining.dbo.ScormActivityRT rt on a.scorm_activity_id = rt.scorm_activity_id
		                    join #tempIds t on t.ScormRegistrationID = reg.scorm_registration_id
	                    )
	
	                    update OnlineTraining.dbo.ScormActivity
	                    set 
	                        attempt_progress_status = 0, attempt_Completion_status = 0, first_completion_timestamp_utc = null, attempted_during_this_attempt = 0
	                    where scorm_registration_id in 
	                    (
		                    select t.ScormRegistrationID from #tempIds t
	                    )

	                    update 
		                     OnlineTraining.dbo.ScormActivityObjective
	                    set objective_satisfied_Status = 0, objective_measure_status = 0, first_success_timestamp_utc = null, first_obj_normalized_measure = 0, completion_status_value = 0, score_raw = null, score_max = null, score_min = null
	                    where scorm_activity_id in  (
		                    select rt.scorm_activity_id from OnlineTraining.dbo.ScormRegistration reg 
		                    join OnlineTraining.dbo.ScormActivity a on a.scorm_registration_id = reg.scorm_registration_id
		                    join OnlineTraining.dbo.ScormActivityRT rt on a.scorm_activity_id = rt.scorm_activity_id
		                    join #tempIds t on t.ScormRegistrationID = reg.scorm_registration_id
	                    )


	                    --- Update Symphony online course rollup
	                     Update OnlineCourseRollup
	                    set 
	                        Score = Null, Completion = 0, Success = 0, NavigationPercentage = 0
	                     --delete from OnlineCourseRollup
	                    where ID in (select OnlineCourseRollupID from  #tempIds)

	                    --- Update Symphony training program rollup
	                     Update TrainingProgramRollup
	                     set
			                    IsCompleteOverride = 0,
			                    DateCompleted = null,
			                    IsRequiredComplete = 0,
			                    IsComplete = 0,
			                    PercentComplete = 0
	                    where ID in (select TrainingProgramRollupID from #tempIds)


	                    if (OBJECT_ID('tempdb..#tempIds') is not null) 
	                    begin
		                    drop table #tempIds
	                    end
                    ", sqlQueryIds);

                    using (SqlCommand command = new SqlCommand(fixCommandSql, connection))
                    {
                        int rows = command.ExecuteNonQuery();
                        Response.Write("<br/><br/>Updated " + rows);
                    }

                    foreach (string url in urls)
                    {
                        Response.Write(string.Format("<a class='courselink incomplete' href='{0}' target='_blank'>{1}</a><br/>", url, url));
                    }


                    //foreach ()

                    //foreach (var temp in ids)
                    //{
                    //    var email = temp.Trim();
                    //    bool isValidCompletion = true;
                    //    bool isValidUser = false;

                    //    using (SqlCommand command = new SqlCommand("select top 1 * from [user] where username = @email", connection))
                    //    {
                    //        command.Parameters.AddWithValue("@email", email);
                    //        using (SqlDataReader reader = command.ExecuteReader())
                    //        {
                    //            while (reader.Read())
                    //            {
                    //                isValidUser = true;
                    //                break;
                    //            }
                    //        }
                    //    }

                    //    string invalidList = "";
                    //    using (SqlCommand command = new SqlCommand("select sku, coursename from dbo.[fGetInvalidCompletions2](@email)", connection))
                    //    {
                    //        command.Parameters.AddWithValue("@email", email);

                    //        List<string> invalid = new List<string>();
                    //        using (SqlDataReader reader = command.ExecuteReader())
                    //        {
                    //            while (reader.Read())
                    //            {
                    //                /*for (int i = 0; i < reader.FieldCount; i++)
                    //                {
                    //                    Console.WriteLine(reader.GetValue(i));
                    //                }
                    //                Console.WriteLine();*/
                    //                isValidCompletion = false;
                    //                string sku = reader.GetString(0); // sku
                    //                string course = reader.GetString(1); //course

                    //                invalid.Add("SKU: " + sku + ", Module: " + course);
                    //            }
                    //        }

                    //        if (invalid.Count > 0)
                    //        {
                    //            invalidList = string.Join("<br/>", invalid.ToArray());
                    //        }
                    //        else
                    //        {
                    //            invalidList = " - ";
                    //        }
                    //    }

                    //    Response.Write("<tr>");
                    //    Response.Write("<td>" + email + "</td>");

                    //    if (isValidUser)
                    //    {
                    //        Response.Write("<td>Yes</td>");
                    //    }
                    //    else
                    //    {
                    //        Response.Write("<td>No</td>");
                    //    }

                    //    if (isValidCompletion)
                    //    {
                    //        Response.Write("<td> - </td>");
                    //    }
                    //    else
                    //    {
                    //        Response.Write("<td><b>" + invalidList + "</b></td>");
                    //    }
                    //    Response.Write("</tr>");
                }
            }
%>