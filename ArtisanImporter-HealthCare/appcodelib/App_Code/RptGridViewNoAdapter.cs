﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;

/// <summary>
/// Summary description for RptGridViewNoAdapter
/// </summary>
public class RptGridViewNoAdapter : Page
{
    private Hashtable _adaptersOrig = new Hashtable();
    private bool _compatible = false;
    public bool Compatible
    {
        get { return _compatible; }

    }
    public RptGridViewNoAdapter()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    protected override void OnPreInit(EventArgs e)
    {
        if (IsPostBack && (Request.Form["__EVENTTARGET"] != null) && (Request.Form["__EVENTTARGET"].IndexOf("adaptersenabled", StringComparison.OrdinalIgnoreCase) > -1))
        {
            //CSSFriendly.Context.Enabled = !CSSFriendly.Context.Enabled;
        }

        DetermineCompatibility();
        if (_compatible)
        {
            //&& (!CSSFriendly.Context.Enabled)
            DisableAdapters();
        }

        base.OnPreInit(e);
    }

    protected override void OnUnload(EventArgs e)
    {
        base.OnUnload(e);

        if (_compatible)
        {
            RestoreAdapters();
        }
    }
    protected void DisableAdapters()
    {
        _adaptersOrig.Clear();

        foreach (DictionaryEntry entry in HttpContext.Current.Request.Browser.Adapters)
        {
            _adaptersOrig.Add(entry.Key, entry.Value);
        }

        HttpContext.Current.Request.Browser.Adapters.Clear();
    }

    protected void RestoreAdapters()
    {
        foreach (DictionaryEntry entry in _adaptersOrig)
        {
            HttpContext.Current.Request.Browser.Adapters.Add(entry.Key, entry.Value);
        }

        _adaptersOrig.Clear();
    }

    private bool DetermineCompatibility()
    {
        _compatible = false;
        foreach (DictionaryEntry entry in HttpContext.Current.Request.Browser.Adapters)
        {
            if (entry.Value.ToString().IndexOf("CSSFriendly", StringComparison.OrdinalIgnoreCase) > -1)
            {
                _compatible = true;
                break;
            }
        }

        return _compatible;
    }
}
