﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PearlsReview.BLL;
using System.Data;
using Westwind.WebStore;
using System.Net.Mail;
using System.IO;

/// <summary>
/// Summary description for PRDailyRun
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class PRDailyRun : System.Web.Services.WebService {
   
    MailAddress toAddress, bccAddress;
    int iPaymentIdsdue = 0;
    int iSuccessCount = 0;
    decimal dSuccessAmount = 0;
    int iFailCount = 0;
    String strUserUpdatefail = string.Empty;
    String strFailPaymentUserId = string.Empty;
    String strUserGracePeriod = string.Empty;
    string strException = string.Empty;
   string cServerLocation = String.Empty;
   private string strUserCheckedForAutoRenewPayment=string.Empty;
   private string strUserWithAutoRenewSuccessfullyRenewedOnPayPal=string.Empty;
   private string FailAutoRenewAccountUpdateCount=string.Empty;
   private string strUserWithAutoRenewExpireThreeDays=string.Empty;
   private string strUserWithoutAutoRenewExpireTomorrow=string.Empty;
   private string CancelledAccount = string.Empty;
   private string CancelledAccountCount=string.Empty;
   private string strUserCheckedForCancelRenewal;
   private string strUserWithCancelProfileSuccessfullyOnPayPal;
   private string strFailCancelUserIds;
   private string strCancelException;
   private string strNewPublishedCourses;
   private int iCancelIdsDue=0;
   private int iCancelSucCount=0;
   private int iCancelFailCount=0;


    public PRDailyRun () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public void Expire()
    {
        
        try
        {
            System.Data.DataSet ds = UserAccount1.GetPRUserAccountExpireDate();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    strUserWithAutoRenewExpireThreeDays += ", " + ds.Tables[0].Rows[i]["iid"].ToString();
                    //Sending a recurring email
                    SendEmail(Convert.ToString(ds.Tables[0].Rows[i]["cemail"]), "PRRecurRemind", "PearlsReview Account Renewal", Convert.ToDateTime(ds.Tables[0].Rows[i]["ExpDate"]),Convert.ToString(ds.Tables[0].Rows[i]["CCLast"]), PRSubscription.SubValue);
                }
            }
            //Give the user a grace period of 3 days
                strUserGracePeriod = strUserGracePeriod + UserAccount1.DailyPRUserAccountExpireDate() + " Users got a grace period of 3 days";
           
        }
        catch(Exception ex)
        {
            strException = strException + " - " + ex.Message;
        }

        //Cancel profile of users who remove the auto renew option
        DataSet dsNoRenews = UserAccount1.GetPRUserAccountExpireDateWithoutRenew();
        ccPayFlowPro_Net_v4 cc = new ccPayFlowPro_Net_v4();
        if(dsNoRenews.Tables[0].Rows.Count>0)
        {
            iCancelIdsDue = dsNoRenews.Tables[0].Rows.Count;
            for(int i=0;i<dsNoRenews.Tables[0].Rows.Count;i++)
            {
                try 
                {
                    string profileID = dsNoRenews.Tables[0].Rows[i]["ProfileID"].ToString();
                    strUserCheckedForCancelRenewal += "," + dsNoRenews.Tables[0].Rows[i]["iid"].ToString();
                   
                    //Check if the status of the profile is active
                    if (cc.CheckRecurringProfile(profileID))
                    {
                        //cancel the profile in Paypal
                        if (cc.CancelProfileWithTransaction(profileID))
                        {
                            iCancelSucCount++;
                            strUserWithCancelProfileSuccessfullyOnPayPal += "," + dsNoRenews.Tables[0].Rows[i]["iid"].ToString();
                        }
                        else
                        {
                            iCancelFailCount++;
                            strFailCancelUserIds += "," + dsNoRenews.Tables[0].Rows[i]["iid"].ToString();
                        }
                    }
                }
                catch(Exception ex)
                {
                    strException += "_" + ex.Message;
                }
            }
        }



        List<Payment> payments;
        //Get the payments of users whose auto payment done 3 days ago. 
        payments = Payment.DailyPRPaymentUpdate();
        ccPayFlowPro_Net_v4 objPayflow = new ccPayFlowPro_Net_v4();
        try
        {
            if (payments.Count > 0)
            {
                iPaymentIdsdue = payments.Count;
                foreach (Payment objpayment in payments)
                {
                    strUserCheckedForAutoRenewPayment += "," + objpayment.UserID;
                    // Check the status of the profile.
                    try
                    {
                        if (objPayflow.CheckRecurringProfile(objpayment.ProfileID))
                        {
                            //If status is true
                            try
                            {
                                iSuccessCount = iSuccessCount + 1;
                                dSuccessAmount = dSuccessAmount + objpayment.PayAmount;
                                strUserWithAutoRenewSuccessfullyRenewedOnPayPal += "," + objpayment.UserID;
                                // Update the previous payment row with active and renew and create a new row in payment table with all the values.
                                //And update UserAccount expire date too
                                if (PearlsReview.BLL.Payment.DailyPRUserAccountUpdate(objpayment.UserID))
                                {
                                    UserAccount objAccount = UserAccount.GetUserAccountByID(objpayment.UserID);
                                    if (objAccount != null)
                                    {
                                        SendEmail(objAccount.cEmail, "PRSUCESSRENEW", "PearlsReview Account Renewal", objpayment.ExpDate, objpayment.CCLast, objpayment.PayAmount);
                                    }

                                }
                                else
                                {
                                    FailAutoRenewAccountUpdateCount += 1;
                                    strUserUpdatefail = strUserUpdatefail + "," + objpayment.UserID;
                                }
                            }
                            catch (Exception ex)
                            {
                                strException = strException + " - " + ex.Message;

                            }

                        }
                        else
                        {
                            //If status is false, update the payment table and useraccount table.
                            strFailPaymentUserId = strFailPaymentUserId + "," + objpayment.UserID;
                            iFailCount = iFailCount + 1;
                            objpayment.Active = false;
                            objpayment.Renew = false;
                            objpayment.PayDesc = objpayment.PayDesc + "RENew failed";
                            objpayment.Update();
                            UserAccount objUser = UserAccount.GetUserAccountByID(objpayment.UserID);
                            objUser.lPrimary = false;
                            objUser.Update();
                            SendEmail(objUser.cEmail, "PRFAILEDRENEW", "PearlsReview Account Expiration", Convert.ToDateTime(objpayment.ExpDate), objpayment.CCLast, objpayment.PayAmount);
                        }
                    }
                    catch (Exception ex) 
                    {
                        sendExceptionEmail("profile id: " + objpayment.ProfileID + ", exception: " + ex.Message); 
                        continue;
                    }
                }

            }
            else
            {
                iPaymentIdsdue = 0;
            }

            List<Payment> CancelledPayments;
            //Get the payments of users whose don't have auto renew and membership is going to expire tomorrow
            //expiredate>=CAST(FLOOR(CAST(DATEADD(day,0,GETDATE()) AS FLOAT)) AS DATETIME) 
            //and expiredate <= CAST(FLOOR(CAST(DATEADD(day,1,GETDATE()) AS FLOAT)) AS DATETIME) 
            //renew = 0
            CancelledPayments = Payment.DailyPRPaymentCancelledUpdate();
            foreach(Payment CancelledPayment in CancelledPayments)
            {
                if (CancelledPayment != null)
                {
                    UserAccount cancelledUser = UserAccount.GetUserAccountByID(CancelledPayment.UserID);
                    if (cancelledUser != null)
                    {
                        strUserWithoutAutoRenewExpireTomorrow = strUserWithoutAutoRenewExpireTomorrow + ", " + CancelledPayment.UserID;
                        SendEmail(cancelledUser.cEmail, "PRNOTRENEW", "PearlsReview Account Expiration", Convert.ToDateTime(CancelledPayment.ExpDate), "", CancelledPayment.PayAmount);
                        
                    }

                }

            }

            

            //send an email to the notify the results
            sendEmail();

        }

        catch (Exception ex)
        {
            strException = strException + " - " + ex.Message;

        }


        try
        {
            getNewPublishedCourses();
            sendEmailtoVera();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
   
    private void getNewPublishedCourses()
    {
        DataSet topics = Topic.GetNewPublishedTopics();
        if (topics.Tables[0].Rows.Count != 0)
        {
            foreach(DataRow row in topics.Tables[0].Rows)
            {
                strNewPublishedCourses += row["course_number"] + " " + row["topicname"] + "</br>";
            }
        }
    }

    public bool SendEmail(string toAddress, string Type, string Subject,DateTime Expdate, string ccnum, decimal Amount)
    {
        MailMessage objMM = new MailMessage();
        string cServerLocation = System.Configuration.ConfigurationManager.AppSettings["serverLocation"];
        string cMailServerHost = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServer"];
        string cMailServerUserName = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServerUserName"];
        string cMailServerPassword = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServerPassword"];
        MailAddress fromAddress = new MailAddress(System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateFromAddress"]);
        bccAddress = new MailAddress(System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCEBrokerCCAddress"]);
        objMM.From = fromAddress;
        objMM.IsBodyHtml =true;
        objMM.Priority = MailPriority.Normal;
        objMM.To.Add(toAddress);
        //objMM.Bcc.Add("btumurbaat@gannetthg.com");
        EmailDefinition objEmail = EmailDefinition.GetEmailDefinitionByType(Type);
        if (objEmail != null)
        {
            if (string.IsNullOrEmpty(objEmail.EDefText))
            {
                return false;
            }
            else
            {
                string EmailBody = objEmail.EDefText;
                EmailBody=EmailBody.Replace("#EXPCHECK#", Convert.ToString(Expdate));
                EmailBody=EmailBody.Replace("#CCNUM#", ccnum);
                EmailBody=EmailBody.Replace("#AMOUNT#", "$" + Convert.ToString(Amount));
                objMM.Body = EmailBody;
            }
        }
        objMM.Subject = Subject;
      
        if (cMailServerHost == null)
        {
            cMailServerHost = "smtp";
        }
        if (cMailServerUserName == null)
        {
            cMailServerUserName = "";
        }
        if (cMailServerPassword == null)
        {
            cMailServerPassword = "";
        }
        SmtpClient objSMTP = new SmtpClient();
        objSMTP.Host = cMailServerHost;
        if (!string.IsNullOrEmpty(cMailServerUserName) & !string.IsNullOrEmpty(cMailServerPassword))
        {
            System.Net.NetworkCredential objSMTPCredentials = new System.Net.NetworkCredential(cMailServerUserName, cMailServerPassword);
            objSMTP.UseDefaultCredentials = false;
            objSMTP.Credentials = objSMTPCredentials;
        }

        else
        {
            objSMTP.UseDefaultCredentials = true;

        }
        try
        {
            objSMTP.Send(objMM);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    //Send the newly published nurse or pr courses to Vera via email
    public void sendEmailtoVera()
    {
        MailMessage objMM = new MailMessage();
        string cServerLocation = System.Configuration.ConfigurationManager.AppSettings["serverLocation"];
        string cMailServerHost = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServer"];
        string cMailServerUserName = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServerUserName"];
        string cMailServerPassword = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServerPassword"];
        MailAddress fromAddress = new MailAddress(System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateFromAddress"]);
        MailAddress toAddress = new MailAddress("vaverina@gannetthg.com");
       MailAddress  bccAddress = new MailAddress(System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCEBrokerCCAddress"]);
        objMM.From = fromAddress;
        objMM.IsBodyHtml = true;
        objMM.Priority = MailPriority.Normal;
        objMM.To.Add(toAddress);
       // objMM.Bcc.Add("fyang@gannetthg.com");
        objMM.Subject = "Courses Publish Today "+System.DateTime.Today.ToLongDateString();
        objMM.Body = "<html>"+strNewPublishedCourses+"</html>";
           
        if (cMailServerHost == null)
            {
                cMailServerHost = "smtp";
            }
            if (cMailServerUserName == null)
            {
                cMailServerUserName = "";
            }
            if (cMailServerPassword == null)
            {
                cMailServerPassword = "";
            }
            SmtpClient objSMTP = new SmtpClient();
            objSMTP.Host = cMailServerHost;
            if (!string.IsNullOrEmpty(cMailServerUserName) & !string.IsNullOrEmpty(cMailServerPassword))
            {
                System.Net.NetworkCredential objSMTPCredentials = new System.Net.NetworkCredential(cMailServerUserName, cMailServerPassword);
                objSMTP.UseDefaultCredentials = false;
                objSMTP.Credentials = objSMTPCredentials;
            }

            else
            {
                objSMTP.UseDefaultCredentials = true;

            }
            objSMTP.Send(objMM);
    }

    public void sendExceptionEmail(string errormessage)
    {
        MailMessage objMM = new MailMessage();
        string cServerLocation = System.Configuration.ConfigurationManager.AppSettings["serverLocation"];
        string cMailServerHost = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServer"];
        string cMailServerUserName = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServerUserName"];
        string cMailServerPassword = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServerPassword"];
        MailAddress fromAddress = new MailAddress(System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateFromAddress"]);
        objMM.From = fromAddress;
        objMM.IsBodyHtml = true;
        objMM.Priority = MailPriority.Normal;
        objMM.To.Add("btumurbaat@gannetthg.com");        
        objMM.Subject = "Daily PR Run (exception occured) for " + System.DateTime.Today.ToLongDateString();
        objMM.Body = errormessage;
        if (cMailServerHost == null)
        {
            cMailServerHost = "smtp";
        }
        if (cMailServerUserName == null)
        {
            cMailServerUserName = "";
        }
        if (cMailServerPassword == null)
        {
            cMailServerPassword = "";
        }
        SmtpClient objSMTP = new SmtpClient();
        objSMTP.Host = cMailServerHost;
        if (!string.IsNullOrEmpty(cMailServerUserName) & !string.IsNullOrEmpty(cMailServerPassword))
        {
            System.Net.NetworkCredential objSMTPCredentials = new System.Net.NetworkCredential(cMailServerUserName, cMailServerPassword);
            objSMTP.UseDefaultCredentials = false;
            objSMTP.Credentials = objSMTPCredentials;
        }

        else
        {
            objSMTP.UseDefaultCredentials = true;

        }
        objSMTP.Send(objMM);
    }


    public void sendEmail()
    {
        MailMessage objMM = new MailMessage();
        string cServerLocation = System.Configuration.ConfigurationManager.AppSettings["serverLocation"];
        string cMailServerHost = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServer"];
        string cMailServerUserName = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServerUserName"];
        string cMailServerPassword = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServerPassword"];
        MailAddress fromAddress = new MailAddress(System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateFromAddress"]);
        toAddress = new MailAddress(System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCEBrokerTOAddress"]);
        bccAddress = new MailAddress(System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCEBrokerCCAddress"]);
        objMM.From = fromAddress;
        objMM.IsBodyHtml = true;
        objMM.Priority = MailPriority.Normal;
        objMM.To.Add(toAddress);
        objMM.Bcc.Add("btumurbaat@gannetthg.com");
        objMM.Subject = "Daily PR Run for " + System.DateTime.Today.ToLongDateString() ;
        objMM.Body = "<html>Users will be checked for auto renew Payment (" + iPaymentIdsdue.ToString() + "): " + strUserCheckedForAutoRenewPayment + "<br/><br/>" +
                     "Users have successfully renewed on PayPal (" + iSuccessCount.ToString() + "): " + strUserWithAutoRenewSuccessfullyRenewedOnPayPal + "<br /><br/>" +
                     "Users have successfully renewed on PayPal but failed updating their account (" + FailAutoRenewAccountUpdateCount + "): " + strUserUpdatefail + "<br/><br/>" +
                     "Total of success auto renew amount : " + String.Format("{0:C}", dSuccessAmount) + " <br/><br/> " +
                     "Auto renew failed on PayPal, account has been deactivated (" + iFailCount.ToString() + "): " + strFailPaymentUserId + "<br/><br/>" +
                     "Auto renew reminder emails sent to: " + strUserWithAutoRenewExpireThreeDays + "<br /><br/>" +
                     "Account expiration reminder emails sent to (no auto renew): " + strUserWithoutAutoRenewExpireTomorrow + "<br /><br/>" +                  
                     strUserGracePeriod + "<br/><br/>" +
                     "Users will be checked for paypal profile removal (" + iCancelIdsDue.ToString() + "): " + strUserCheckedForCancelRenewal + "<br/><br/>" +
                     "Users whoes profiles have successfully removed on PayPal (" + iCancelSucCount.ToString() + "): " + strUserWithCancelProfileSuccessfullyOnPayPal + "<br /><br/>" +
                     "Profile removal failed on PayPal (" + iCancelFailCount.ToString() + "): " + strFailCancelUserIds + "<br/><br/>" +
                     " Exception : " + strException + "</html>";
          if (cMailServerHost == null)
            {
                cMailServerHost = "smtp";
            }
            if (cMailServerUserName == null)
            {
                cMailServerUserName = "";
            }
            if (cMailServerPassword == null)
            {
                cMailServerPassword = "";
            }
            SmtpClient objSMTP = new SmtpClient();
            objSMTP.Host = cMailServerHost;
            if (!string.IsNullOrEmpty(cMailServerUserName) & !string.IsNullOrEmpty(cMailServerPassword))
            {
                System.Net.NetworkCredential objSMTPCredentials = new System.Net.NetworkCredential(cMailServerUserName, cMailServerPassword);
                objSMTP.UseDefaultCredentials = false;
                objSMTP.Credentials = objSMTPCredentials;
            }

            else
            {
                objSMTP.UseDefaultCredentials = true;

            }
            objSMTP.Send(objMM);
    }

    
    [WebMethod]
    public void Test()
    {
       try {
	//Dim binLogString As Byte() = Encoding.[Default].GetBytes((DateTime.Now.ToString() & " - ") & vbCr & vbLf)
	//Dim loFile As New IO.FileStream(System.Web.HttpContext.Current.Server.MapPath(".\\XML\\cache.txt"), FileMode.OpenOrCreate, FileAccess.Write, FileShare.Write)
	//loFile.Seek(0, SeekOrigin.[End])
	//loFile.Write(binLogString, 0, binLogString.Length)
	//loFile.Close()

	string FILENAME = System.Web.HttpContext.Current.Server.MapPath("date.txt");

	// Get a StreamReader class that can be used to read the file  
	StreamWriter objStreamWriter = default(StreamWriter);
	objStreamWriter = File.AppendText(FILENAME);

	// Append the the end of the string, "A user viewed this demo at: "  
	// followed by the current date and time  
	objStreamWriter.WriteLine((DateTime.Now.ToString() + " - ") );

	objStreamWriter.Close();

	
} catch (Exception ex) {
	
}
    }
    
    
}

