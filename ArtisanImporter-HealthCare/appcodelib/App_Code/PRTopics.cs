﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PearlsReview.BLL;

/// <summary>
/// Summary description for PRTopics
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class PRTopics : System.Web.Services.WebService
{

    public PRTopics()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public List<Topic> GetTopic(string SCategory)
    {
        List<Topic> list = new List<Topic>();
        if (!(string.IsNullOrEmpty(SCategory)))
        {
            if ((SCategory == "Webinar"))
            {
                list = Topic.GetWebinarTopicsByFacID(3, "TopicName");
            }
            else
            {
                list = Topic.GetTopicsByCategoryWebsite(SCategory, "TopicName");
            }
        }
        return list;
    }
    [WebMethod]
    public Category GetCategory(string SCategory)
    {
        return Category.GetPearlsCategoryByWebSite(SCategory);
    }
    [WebMethod]
    public string GetAuthorsByTopicID(int TopicID)
    {
        return ResourcePerson.GetAuthorsByTopicID(TopicID, false);
    }
    [WebMethod]
    public Topic GetTopicByID(int TopicID)
    {
        return Topic.GetTopicByID(TopicID);

    }
}

    


