﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI.WebControls

Namespace Gannett.Controls
    ' NOTE: These two classes related to Row-clickable GridView subclass are from:
    ' http://www.mostlydevelopers.com/mostlydevelopers/blog/post/2008/06/02/Row-Clickable-GridView.aspx
    '
    Public Class RowClickableGridView
        Inherits GridView

        'key that is used for our RowClicked event handler
        Private Shared ReadOnly RowClickedEventKey As Object = New Object()

        Public Property RowClick_Enabled() As Boolean
            Get
                If Not IsNothing(ViewState("RowClick_Enabled")) Then
                    Return DirectCast(ViewState("RowClick_Enabled"), Boolean)
                Else
                    Return False
                End If
            End Get
            Set(ByVal value As Boolean)
                ViewState("RowClick_Enabled") = value
            End Set
        End Property

        Public Property RowClick_ToolTip() As String
            Get
                If Not IsNothing(ViewState("RowClick_ToolTip")) Then
                    Return ViewState("RowClick_ToolTip").ToString()
                Else
                    Return String.Empty
                End If
            End Get
            Set(ByVal value As String)
                ViewState("RowClick_ToolTip") = value
            End Set
        End Property

        Protected Overrides Sub PrepareControlHierarchy()
            MyBase.PrepareControlHierarchy()

            'wire RowClick event & RowClick tooltip for rows if RowClicked is enabled
            If Me.RowClick_Enabled Then
                Dim argsData As String = String.Empty
                For Each row As GridViewRow In Me.Rows
                    'if row is a data row & it's not the edit row
                    If row.RowType = DataControlRowType.DataRow And Me.EditIndex <> row.RowIndex Then
                        'create argument - pass the row index
                        argsData = "rc" & row.RowIndex.ToString()

                        'wire javascript to the row for the RowClicked event
                        row.Attributes.Add("onClick", Me.Page.ClientScript.GetPostBackEventReference(Me, argsData))

                        'if the RowClick ToolTip is supplied
                        If Not String.IsNullOrEmpty(Me.RowClick_ToolTip) Then
                            'apply tooltip to row
                            row.ToolTip = Me.RowClick_ToolTip
                        End If
                    End If
                Next
            End If
        End Sub

        Protected Overrides Sub RaisePostBackEvent(ByVal eventArgument As String)
            'if a row clicked event
            If eventArgument.StartsWith("rc") Then
                'get the index of the row that raised the event
                Dim index As Integer = Int32.Parse(eventArgument.Substring(2))

                'create our instance of GridViewRowClickedEventArgs passing the GridViewRow clicked
                Dim args As GridViewRowClickedEventArgs = New GridViewRowClickedEventArgs(Me.Rows(index))

                'raise the RowClicked event
                OnRowClicked(args)
            Else
                'raise any other event (not row clicked)
                MyBase.RaisePostBackEvent(eventArgument)
            End If
        End Sub

        Protected Overridable Sub OnRowClicked(ByVal e As GridViewRowClickedEventArgs)
            'raise the RowClicked event
            RaiseEvent RowClicked(Me, e)
        End Sub

        'setup our EventHandler for RowClicked
        Public Custom Event RowClicked As EventHandler(Of GridViewRowClickedEventArgs)
            AddHandler(ByVal value As EventHandler(Of GridViewRowClickedEventArgs))
                Events.AddHandler(RowClickedEventKey, value)
            End AddHandler

            RemoveHandler(ByVal value As EventHandler(Of GridViewRowClickedEventArgs))
                Events.RemoveHandler(RowClickedEventKey, value)
            End RemoveHandler

            RaiseEvent(ByVal sender As Object, ByVal e As GridViewRowClickedEventArgs)
                Dim ev As EventHandler(Of GridViewRowClickedEventArgs) = TryCast(Events(RowClickedEventKey), EventHandler(Of GridViewRowClickedEventArgs))
                If Not IsNothing(ev) Then
                    ev(sender, e)
                End If
            End RaiseEvent
        End Event

    End Class

    'our custom event argument class for the RowClicked event
    Public Class GridViewRowClickedEventArgs
        Inherits EventArgs

        Private _row As GridViewRow

        Public ReadOnly Property Row() As GridViewRow
            Get
                Return _row
            End Get
        End Property

        Public Sub New(ByVal row As GridViewRow)
            _row = row
        End Sub
    End Class




End Namespace
