﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.IO
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Xml
Imports System.Xml.Linq

Namespace PearlsReview.QTI.VB

    Public Class QTIQuestionObject
        Private _cQuestionID As String = ""
        Public Property cQuestionID() As String
            Get
                Return _cQuestionID
            End Get
            Set(ByVal Value As String)
                _cQuestionID = Value
            End Set
        End Property

        'Bsk New
        Private _cHeaderID As String = ""
        Public Property cHeaderID() As String
            Get
                Return _cHeaderID
            End Get
            Set(ByVal Value As String)
                _cHeaderID = Value
            End Set
        End Property
        'Bsk New
        Private _cHeaderName As String = ""
        Public Property cHeaderName() As String
            Get
                Return _cHeaderName
            End Get
            Set(ByVal Value As String)
                _cHeaderName = Value
            End Set
        End Property

        Private _cQuestionTitle As String = ""
        Public Property cQuestionTitle() As String
            Get
                Return _cQuestionTitle
            End Get
            Set(ByVal Value As String)
                _cQuestionTitle = Value
            End Set
        End Property

        Private _cResponseID As String = ""
        Public Property cResponseID() As String
            Get
                Return _cResponseID
            End Get
            Set(ByVal Value As String)
                _cResponseID = Value
            End Set
        End Property

        Private _cQuestionText As String = ""
        Public Property cQuestionText() As String
            Get
                Return _cQuestionText
            End Get
            Set(ByVal Value As String)
                _cQuestionText = Value
            End Set
        End Property

        Private _cAnswer_A_Text As String = ""
        Public Property cAnswer_A_Text() As String
            Get
                Return _cAnswer_A_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_A_Text = Value
            End Set
        End Property

        Private _cAnswer_B_Text As String = ""
        Public Property cAnswer_B_Text() As String
            Get
                Return _cAnswer_B_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_B_Text = Value
            End Set
        End Property

        Private _cAnswer_C_Text As String = ""
        Public Property cAnswer_C_Text() As String
            Get
                Return _cAnswer_C_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_C_Text = Value
            End Set
        End Property

        Private _cAnswer_D_Text As String = ""
        Public Property cAnswer_D_Text() As String
            Get
                Return _cAnswer_D_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_D_Text = Value
            End Set
        End Property

        Private _cAnswer_E_Text As String = ""
        Public Property cAnswer_E_Text() As String
            Get
                Return _cAnswer_E_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_E_Text = Value
            End Set
        End Property

        Private _cAnswer_F_Text As String = ""
        Public Property cAnswer_F_Text() As String
            Get
                Return _cAnswer_F_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_F_Text = Value
            End Set
        End Property

        Private _cCorrectAnswer As String = ""
        Public Property cCorrectAnswer() As String
            Get
                Return _cCorrectAnswer
            End Get
            Set(ByVal Value As String)
                _cCorrectAnswer = Value
            End Set
        End Property

        Private _cCorrectAnswerFeedback As String = ""
        Public Property cCorrectAnswerFeedback() As String
            Get
                Return _cCorrectAnswerFeedback
            End Get
            Set(ByVal Value As String)
                _cCorrectAnswerFeedback = Value
            End Set
        End Property

        Private _cUserAnswer As String = ""
        Public Property cUserAnswer() As String
            Get
                Return _cUserAnswer
            End Get
            Set(ByVal Value As String)
                _cUserAnswer = Value
            End Set
        End Property

        'Added new (Bhaskar)
        Private _cAnswer_A_Count As Integer = 0
        Public Property cAnswerACount() As Integer
            Get
                Return _cAnswer_A_Count
            End Get
            Set(ByVal Value As Integer)
                _cAnswer_A_Count = Value
            End Set
        End Property
        'Added new (Bhaskar)
        Private _cAnswer_B_Count As Integer = 0
        Public Property cAnswerBCount() As Integer
            Get
                Return _cAnswer_B_Count
            End Get
            Set(ByVal Value As Integer)
                _cAnswer_B_Count = Value
            End Set
        End Property
        'Added new (Bhaskar)
        Private _cAnswer_C_Count As Integer = 0
        Public Property cAnswerCCount() As Integer
            Get
                Return _cAnswer_C_Count
            End Get
            Set(ByVal Value As Integer)
                _cAnswer_C_Count = Value
            End Set
        End Property
        'Added new (Bhaskar)
        Private _cAnswer_D_Count As Integer = 0
        Public Property cAnswerDCount() As Integer
            Get
                Return _cAnswer_D_Count
            End Get
            Set(ByVal Value As Integer)
                _cAnswer_D_Count = Value
            End Set
        End Property
        'Added new (Bhaskar)
        Private _cAnswer_E_Count As Integer = 0
        Public Property cAnswerECount() As Integer
            Get
                Return _cAnswer_E_Count
            End Get
            Set(ByVal Value As Integer)
                _cAnswer_E_Count = Value
            End Set
        End Property

        'Added new (Bhaskar)
        Private _cAnswer_F_Count As Integer = 0
        Public Property cAnswerFCount() As Integer
            Get
                Return _cAnswer_F_Count
            End Get
            Set(ByVal Value As Integer)
                _cAnswer_F_Count = Value
            End Set
        End Property

        'Added new (Bhaskar)
        Private _cAnswer_Total_Count As Integer = 0
        Public Property cAnswerTotalCount() As Integer
            Get
                Return _cAnswer_Total_Count
            End Get
            Set(ByVal Value As Integer)
                _cAnswer_Total_Count = Value
            End Set
        End Property


        'Private Sub New()
        '    Me.
        'End Sub
    End Class

    Public Class QTIUtils

        Public Sub New()
            ' nothing to do
        End Sub

        Public Function ConvertQTIQuestionXMLToQTIQuestionObject(ByVal cQTIQuestionXML As String) As QTIQuestionObject
            Dim oQTIQuestion As QTIQuestionObject = New QTIQuestionObject()
            ' parse out parts of the XML to populate properties of the QTIQuestionObject

            Return oQTIQuestion
        End Function

        Public Function ConvertQTIQuestionObjectToQTIQuestionXML(ByVal oQuestionObject As QTIQuestionObject) _
            As XElement
            ' use XML literals to create QTI-formatted XML Question from Question object passed in
            '<item title=<%= "" + oQuestionObject.cQuestionTitle + "" %> ident=<%= "" + oQuestionObject.cQuestionID + "" %>>
            Dim cQTIQuestionXML As XElement = _
            <item title=<%= "" + oQuestionObject.cQuestionTitle + "" %> ident=<%= "" + oQuestionObject.cQuestionID + "" %> identHeader=<%= "" + oQuestionObject.cHeaderID + "" %>>
                <presentation label=<%= "" + oQuestionObject.cQuestionID + "" %>>
                    <material>
                        <mattext><%= CreateCDataSection(oQuestionObject.cQuestionText) %></mattext>
                    </material>_
                    <response_lid ident=<%= "" + oQuestionObject.cResponseID + "" %> rcardinality="Single" rtiming="No">
                        <render_choice shuffle="No">
                            <response_label ident="A">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_A_Text) %></mattext></material>
                            </response_label>
                            <response_label ident="B">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_B_Text) %></mattext></material>
                            </response_label>
                            <response_label ident="C">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_C_Text) %></mattext></material>
                            </response_label>
                            <response_label ident="D">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_D_Text) %></mattext></material>
                            </response_label>
                            <response_label ident="E">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_E_Text) %></mattext></material>
                            </response_label>
                            <response_label ident="F">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_F_Text) %></mattext></material>
                            </response_label>
                        </render_choice>
                    </response_lid>
                </presentation>
                <resprocessing>
                    <outcomes>
                        <decvar vartype="Integer" defaultval="0"/>
                    </outcomes>
                    <respcondition title="Correct">
                        <conditionvar>
                            <varequal respident=<%= "" + oQuestionObject.cResponseID + "" %>><%= oQuestionObject.cCorrectAnswer %></varequal>
                        </conditionvar>
                        <setvar action="Set">1</setvar>
                        <displayfeedback feedbacktype="Response" linkrefid="Correct"/>
                    </respcondition>
                    <respcondition title="Incorrect">
                        <conditionvar>
                            <not><varequal respident=<%= "" + oQuestionObject.cResponseID + "" %>><%= oQuestionObject.cCorrectAnswer %></varequal></not>
                        </conditionvar>
                        <setvar action="Set">-1</setvar>
                    </respcondition>
                </resprocessing>
                <itemfeedback ident="Correct">
                    <material><mattext><%= CreateCDataSection(oQuestionObject.cCorrectAnswerFeedback) %></mattext></material>
                </itemfeedback>
            </item>

            Return cQTIQuestionXML
        End Function

        Private Shared Function CreateCDataSection(ByVal ContentText As String) As String
            If ContentText Is Nothing Then
                Return ""
            Else
                Return "<![CDATA[" & ContentText & "]]>"
            End If
        End Function


        Public Function GetUserAnswersFromXML(ByVal cXML As String) As List(Of String)
            Dim AnswerList As List(Of String) = New List(Of String)
            If cXML.Trim().Length > 0 Then

                Dim XAnswers As XDocument = XDocument.Parse(cXML)
                Dim XAnswerList = From xAnswer In XAnswers...<answer> _
                    Select cAnswer = xAnswer.Value
                For Each XAnswer In XAnswerList
                    AnswerList.Add(XAnswer.ToString())
                Next
            End If

            Return AnswerList
        End Function


        Public Function XConvertQTITestXMLToQTIQuestionObjectList(ByVal cQTITestXML As String) As List(Of QTIQuestionObject)
            Dim QTIQuestionList As List(Of QTIQuestionObject) = New List(Of QTIQuestionObject)
            Dim oQTIQuestion As QTIQuestionObject
            ' parse out parts of the XML to populate properties of the multiple QTIQuestionObjects
            Dim XTest As XDocument = XDocument.Parse(cQTITestXML)

            Dim QuestionList = From xQuestion In XTest...<item> _
                                Select QuestionID = xQuestion.@ident, _
                                QuestionTitle = xQuestion.@title, _
                                ResponseID = xQuestion.<presentation>.<response_lid>.@ident, _
                                QuestionText = xQuestion.<presentation>.<material>.<mattext>.Value, _
                                Answer_A_Text = (From xResponseLabel In xQuestion.<presentation>.<response_lid>.<render_choice>.<response_label> _
                                    Where xResponseLabel.@ident = "A" _
                                    Select xResponseLabel.<material>.<mattext>.Value), _
                                Answer_B_Text = (From xResponseLabel In xQuestion.<presentation>.<response_lid>.<render_choice>.<response_label> _
                                    Where xResponseLabel.@ident = "B" _
                                    Select xResponseLabel.<material>.<mattext>.Value), _
                                Answer_C_Text = (From xResponseLabel In xQuestion.<presentation>.<response_lid>.<render_choice>.<response_label> _
                                    Where xResponseLabel.@ident = "C" _
                                    Select xResponseLabel.<material>.<mattext>.Value), _
                                Answer_D_Text = (From xResponseLabel In xQuestion.<presentation>.<response_lid>.<render_choice>.<response_label> _
                                    Where xResponseLabel.@ident = "D" _
                                    Select xResponseLabel.<material>.<mattext>.Value), _
                                Answer_E_Text = (From xResponseLabel In xQuestion.<presentation>.<response_lid>.<render_choice>.<response_label> _
                                    Where xResponseLabel.@ident = "E" _
                                    Select xResponseLabel.<material>.<mattext>.Value), _
                                Answer_F_Text = (From xResponseLabel In xQuestion.<presentation>.<response_lid>.<render_choice>.<response_label> _
                                    Where xResponseLabel.@ident = "F" _
                                    Select xResponseLabel.<material>.<mattext>.Value), _
                                Correct_Answer = (From xRespCondition In xQuestion.<respprocessing>.<resp_condition> _
                                    Where xRespCondition.@title = "Correct" _
                                    Select xRespCondition.<conditionvar>.<varequal>.Value), _
                                Correct_Answer_Feedback = (From xItemFeedback In xQuestion.<itemfeedback> _
                                    Where xItemFeedback.@ident = "Correct" _
                                    Select xItemFeedback.<material>.<mattext>.Value)

            For Each Question In QuestionList
                oQTIQuestion = New QTIQuestionObject
                oQTIQuestion.cQuestionID = Question.QuestionID.ToString()
                oQTIQuestion.cQuestionTitle = Question.QuestionTitle.ToString()
                oQTIQuestion.cResponseID = Question.ResponseID.ToString()
                oQTIQuestion.cQuestionText = Question.QuestionText.ToString()
                oQTIQuestion.cAnswer_A_Text = Question.Answer_A_Text.ToString()
                oQTIQuestion.cAnswer_B_Text = Question.Answer_B_Text.ToString()
                oQTIQuestion.cAnswer_C_Text = Question.Answer_C_Text.ToString()
                oQTIQuestion.cAnswer_D_Text = Question.Answer_D_Text.ToString()
                oQTIQuestion.cAnswer_E_Text = Question.Answer_E_Text.ToString()
                oQTIQuestion.cAnswer_F_Text = Question.Answer_F_Text.ToString()
                oQTIQuestion.cCorrectAnswer = Question.Correct_Answer.ToString()
                oQTIQuestion.cCorrectAnswerFeedback = Question.Correct_Answer_Feedback.ToString()
                ' more here

                QTIQuestionList.Add(oQTIQuestion)
                oQTIQuestion = Nothing
            Next

            Return QTIQuestionList
        End Function

        Public Function ConvertQTITestXMLToQTIQuestionObjectList(ByVal cQTITestXML As String) As List(Of QTIQuestionObject)
            Dim QTIQuestionList As List(Of QTIQuestionObject) = New List(Of QTIQuestionObject)
            Dim oQTIQuestion As QTIQuestionObject
            ' parse out parts of the XML to populate properties of the multiple QTIQuestionObjects
            Dim XTest As XDocument = XDocument.Parse(cQTITestXML)

            Dim QuestionList = (From xQuestion In XTest...<item> _
                                Select QuestionText = xQuestion.<presentation>.<material>.<mattext>.Value, _
                                QuestionID = xQuestion.<presentation>.<response_lid>.@ident, _
                                HeaderID = xQuestion.@identHeader, _
                                AnswerOptions = (From xRespCondition In xQuestion.<resprocessing>.<respcondition> _
                                    Select Answer_Option_Title = xRespCondition.@title, _
                                    Correct_Answer = xRespCondition.<conditionvar>.<varequal>.Value).ToList(), _
                                Correct_Answer_Feedback = xQuestion.<itemfeedback>.<material>.<mattext>.Value, _
                                AnswerTexts = (From xResponse_Label In xQuestion.<presentation>.<response_lid>.<render_choice>.<response_label> _
                                    Select AnswerTextOrdinal = xResponse_Label.@ident, _
                                    AnswerText = xResponse_Label.<material>.<mattext>.Value).ToList())


            For Each Question In QuestionList
                oQTIQuestion = New QTIQuestionObject
                oQTIQuestion.cQuestionID = Question.QuestionID.ToString()
                If (Question.HeaderID Is Nothing Or Question.HeaderID = "") Then
                    oQTIQuestion.cHeaderID = "0" 'bsk new
                Else
                    oQTIQuestion.cHeaderID = Question.HeaderID.ToString() 'bsk new
                End If

                oQTIQuestion.cQuestionText = Question.QuestionText.ToString()

                For Each AnswerOption In Question.AnswerOptions
                    If AnswerOption.Answer_Option_Title = "Correct" Then
                        oQTIQuestion.cCorrectAnswer = AnswerOption.Correct_Answer
                    End If
                Next

                oQTIQuestion.cCorrectAnswerFeedback = Question.Correct_Answer_Feedback.ToString()

                For Each AnswerText In Question.AnswerTexts
                    If AnswerText.AnswerTextOrdinal.ToString() = "A" Then
                        oQTIQuestion.cAnswer_A_Text = AnswerText.AnswerText.ToString()
                    End If
                    If AnswerText.AnswerTextOrdinal.ToString() = "B" Then
                        oQTIQuestion.cAnswer_B_Text = AnswerText.AnswerText.ToString()
                    End If
                    If AnswerText.AnswerTextOrdinal.ToString() = "C" Then
                        oQTIQuestion.cAnswer_C_Text = AnswerText.AnswerText.ToString()
                    End If
                    If AnswerText.AnswerTextOrdinal.ToString() = "D" Then
                        oQTIQuestion.cAnswer_D_Text = AnswerText.AnswerText.ToString()
                    End If
                    If AnswerText.AnswerTextOrdinal.ToString() = "E" Then
                        oQTIQuestion.cAnswer_E_Text = AnswerText.AnswerText.ToString()
                    End If
                    If AnswerText.AnswerTextOrdinal.ToString() = "F" Then
                        oQTIQuestion.cAnswer_F_Text = AnswerText.AnswerText.ToString()
                    End If
                Next

                ' more here

                QTIQuestionList.Add(oQTIQuestion)
                oQTIQuestion = Nothing
            Next

            Return QTIQuestionList
        End Function

        Public Function ConvertQTIQuestionObjectListToQTITestXMLString(ByVal QTIQuestionList As List(Of QTIQuestionObject)) _
             As String

            ' use XML literals to create QTI-formatted XML Question from Question object passed in
            Dim cQTITestXML As XElement = _
            <questestinterop>
                <%= From QTIQuestionObject As QTIQuestionObject In QTIQuestionList _
                    Select _
                    ConvertQTIQuestionObjectToQTIQuestionXML(QTIQuestionObject) %>
            </questestinterop>

            Dim cQTITestXMLString As String = cQTITestXML.ToString()
            ' fix up some problems with starting and ending < and > around CDATA sections
            ' and some other items inside CDATA sections that are encoded when they should not be
            cQTITestXMLString = System.Web.HttpUtility.HtmlDecode(cQTITestXMLString)
            ' once more to take care of some items that are "double-encoded"
            ' (&amp;quot;) - the first htmldecode takes care of the &amp; but
            ' then we need a second one to turn the resulting &quot; into a quote mark 
            cQTITestXMLString = System.Web.HttpUtility.HtmlDecode(cQTITestXMLString)
            Return cQTITestXMLString

        End Function

        '' Bhaskar...Only Question Text (Testing)
        'Public Function ConvertOnlyQTIQuestionObjectToQTITestXMLString(ByVal QTIQuestionList As List(Of QTIQuestionObject)) _
        '     As String

        '    ' use XML literals to create QTI-formatted XML Question from Question object passed in
        '    Dim cQTITestXML As XElement = _
        '    <questestinterop>
        '        <%= From QTIQuestionObject As QTIQuestionObject In QTIQuestionList _
        '            Select _
        '            ConvertOnlyQTIQuestionObjectToQTIQuestionXML(QTIQuestionObject) %>
        '    </questestinterop>
        '    Dim cQTITestXMLString As String = cQTITestXML.ToString()
        '    cQTITestXMLString = System.Web.HttpUtility.HtmlDecode(cQTITestXMLString)
        '    cQTITestXMLString = System.Web.HttpUtility.HtmlDecode(cQTITestXMLString)
        '    Return cQTITestXMLString
        'End Function

        '' Bhaskar...Only Question Text 
        'Public Function ConvertOnlyQTIQuestionObjectToQTIQuestionXML(ByVal oQuestionObject As PearlsReview.QTI.QTIQuestionObject) _
        '   As XElement
        '    Dim cQTIQuestionXML As XElement

        '    If oQuestionObject.cAnswer_A_Text <> "" Then
        '        ' use XML literals to create QTI-formatted XML Question from Question object passed in
        '        cQTIQuestionXML = _
        '        <item title=<%= "" + oQuestionObject.cQuestionTitle + "" %> ident=<%= "" + oQuestionObject.cQuestionID + "" %>>
        '            <presentation label=<%= "" + oQuestionObject.cQuestionID + "" %>>
        '                <material>
        '                    <mattext><%= CreateCDataSection(oQuestionObject.cQuestionText) %></mattext>
        '                </material>_
        '            <response_lid ident=<%= "" + oQuestionObject.cResponseID + "" %> rcardinality="Single" rtiming="No">
        '                    <render_choice shuffle="No">
        '                        <response_label ident="A">
        '                            <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_A_Text) %></mattext></material>
        '                        </response_label>
        '                        <response_label ident="B">
        '                            <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_B_Text) %></mattext></material>
        '                        </response_label>
        '                        <response_label ident="C">
        '                            <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_C_Text) %></mattext></material>
        '                        </response_label>
        '                        <response_label ident="D">
        '                            <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_D_Text) %></mattext></material>
        '                        </response_label>
        '                        <response_label ident="E">
        '                            <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_E_Text) %></mattext></material>
        '                        </response_label>
        '                        <response_label ident="F">
        '                            <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_F_Text) %></mattext></material>
        '                        </response_label>
        '                    </render_choice>
        '                </response_lid>
        '            </presentation>
        '            <resprocessing>
        '                <outcomes>
        '                    <decvar vartype="Integer" defaultval="0"/>
        '                </outcomes>
        '                <respcondition title="Correct">
        '                    <conditionvar>
        '                        <varequal respident=<%= "" + oQuestionObject.cResponseID + "" %>><%= oQuestionObject.cCorrectAnswer %></varequal>
        '                    </conditionvar>
        '                    <setvar action="Set">1</setvar>
        '                    <displayfeedback feedbacktype="Response" linkrefid="Correct"/>
        '                </respcondition>
        '                <respcondition title="Incorrect">
        '                    <conditionvar>
        '                        <not><varequal respident=<%= "" + oQuestionObject.cResponseID + "" %>><%= oQuestionObject.cCorrectAnswer %></varequal></not>
        '                    </conditionvar>
        '                    <setvar action="Set">-1</setvar>
        '                </respcondition>
        '            </resprocessing>
        '            <itemfeedback ident="Correct">
        '                <material><mattext><%= CreateCDataSection(oQuestionObject.cCorrectAnswerFeedback) %></mattext></material>
        '            </itemfeedback>
        '        </item>
        '    Else
        '        cQTIQuestionXML = _
        '        <item title=<%= "" + oQuestionObject.cQuestionTitle + "" %> ident=<%= "" + oQuestionObject.cQuestionID + "" %>>
        '            <presentation label=<%= "" + oQuestionObject.cQuestionID + "" %>>
        '                <material>
        '                    <mattext><%= CreateCDataSection(oQuestionObject.cQuestionText) %></mattext>
        '                </material>
        '                <response_lid ident=<%= "" + oQuestionObject.cResponseID + "" %> rcardinality="Single" rtiming="No">
        '                </response_lid>
        '            </presentation>
        '        </item>
        '    End If

        '    Return cQTIQuestionXML
        'End Function


    End Class

End Namespace


