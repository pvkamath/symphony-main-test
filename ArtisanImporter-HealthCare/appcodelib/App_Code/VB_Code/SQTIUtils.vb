﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.IO
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Xml
Imports System.Xml.Linq

Namespace PearlsReview.QTI.VB

    Public Class SQTIQuestionObject

        Private _cQuestionID As String = ""
        Public Property cQuestionID() As String
            Get
                Return _cQuestionID
            End Get
            Set(ByVal Value As String)
                _cQuestionID = Value
            End Set
        End Property

        Private _cQuestionTitle As String = ""
        Public Property cQuestionTitle() As String
            Get
                Return _cQuestionTitle
            End Get
            Set(ByVal Value As String)
                _cQuestionTitle = Value
            End Set
        End Property

        Private _cQuestionText As String = ""
        Public Property cQuestionText() As String
            Get
                Return _cQuestionText
            End Get
            Set(ByVal Value As String)
                _cQuestionText = Value
            End Set
        End Property

        Private _cAnswer_A_Text As String = ""
        Public Property cAnswer_A_Text() As String
            Get
                Return _cAnswer_A_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_A_Text = Value
            End Set
        End Property

        Private _cAnswer_B_Text As String = ""
        Public Property cAnswer_B_Text() As String
            Get
                Return _cAnswer_B_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_B_Text = Value
            End Set
        End Property

        Private _cAnswer_C_Text As String = ""
        Public Property cAnswer_C_Text() As String
            Get
                Return _cAnswer_C_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_C_Text = Value
            End Set
        End Property

        Private _cAnswer_D_Text As String = ""
        Public Property cAnswer_D_Text() As String
            Get
                Return _cAnswer_D_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_D_Text = Value
            End Set
        End Property

        Private _cAnswer_E_Text As String = ""
        Public Property cAnswer_E_Text() As String
            Get
                Return _cAnswer_E_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_E_Text = Value
            End Set
        End Property

        Private _cAnswer_F_Text As String = ""
        Public Property cAnswer_F_Text() As String
            Get
                Return _cAnswer_F_Text
            End Get
            Set(ByVal Value As String)
                _cAnswer_F_Text = Value
            End Set
        End Property

        Private _cRecordOrder As String = ""
        Public Property cRecordOrder() As String
            Get
                Return _cRecordOrder
            End Get
            Set(ByVal Value As String)
                _cRecordOrder = Value
            End Set
        End Property

        Private _cUserAnswer As String = ""
        Public Property cUserAnswer() As String
            Get
                Return _cUserAnswer
            End Get
            Set(ByVal Value As String)
                _cUserAnswer = Value
            End Set
        End Property

        Private _cControl_Type As String = ""
        Public Property cControl_Type() As String
            Get
                Return _cControl_Type
            End Get
            Set(ByVal Value As String)
                _cControl_Type = Value
            End Set
        End Property


        'Private Sub New()
        '    Me.
        'End Sub
    End Class

    Public Class SQTIUtils

        Public Sub New()
            ' nothing to do
        End Sub

        Public Function ConvertSQTIQuestionObjectListToQTITestXMLString(ByVal QTIQuestionList As List(Of SQTIQuestionObject)) _
           As String

            ' use XML literals to create QTI-formatted XML Question from Question object passed in
            Dim cQTITestXML As XElement = _
            <questestinterop>
                <%= From SQTIQuestionObject As SQTIQuestionObject In QTIQuestionList _
                    Select _
                    ConvertSQTIQuestionObjectToQTIQuestionXML(SQTIQuestionObject) %>
            </questestinterop>

            Dim cQTITestXMLString As String = cQTITestXML.ToString()
            ' fix up some problems with starting and ending < and > around CDATA sections
            ' and some other items inside CDATA sections that are encoded when they should not be
            cQTITestXMLString = System.Web.HttpUtility.HtmlDecode(cQTITestXMLString)
            ' once more to take care of some items that are "double-encoded"
            ' (&amp;quot;) - the first htmldecode takes care of the &amp; but
            ' then we need a second one to turn the resulting &quot; into a quote mark 
            cQTITestXMLString = System.Web.HttpUtility.HtmlDecode(cQTITestXMLString)
            Return cQTITestXMLString
        End Function

        Public Function ConvertSQTIQuestionObjectToQTIQuestionXML(ByVal oQuestionObject As SQTIQuestionObject) _
            As XElement
            ' use XML literals to create QTI-formatted XML Question from Question object passed in
            Dim cQTIQuestionXML As XElement = _
            <item title=<%= "" + oQuestionObject.cQuestionTitle + "" %> ident=<%= "" + oQuestionObject.cQuestionID + "" %>>
                <presentation label=<%= "" + oQuestionObject.cQuestionID + "" %>>
                    <material>
                        <mattext><%= CreateCDataSection(oQuestionObject.cQuestionText) %></mattext>
                    </material>_
                    <control_type ident=<%= "" + oQuestionObject.cControl_Type + "" %>></control_type>
                    <response_lid ident="0" rcardinality="Single" rtiming="No">
                        <render_choice shuffle="No">
                            <response_label ident="A">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_A_Text) %></mattext></material>
                            </response_label>
                            <response_label ident="B">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_B_Text) %></mattext></material>
                            </response_label>
                            <response_label ident="C">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_C_Text) %></mattext></material>
                            </response_label>
                            <response_label ident="D">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_D_Text) %></mattext></material>
                            </response_label>
                            <response_label ident="E">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_E_Text) %></mattext></material>
                            </response_label>
                            <response_label ident="F">
                                <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_F_Text) %></mattext></material>
                            </response_label>
                        </render_choice>
                    </response_lid>
                </presentation>

                <itemfeedback ident="Correct">
                    <material><mattext><%= CreateCDataSection(oQuestionObject.cRecordOrder) %></mattext></material>
                </itemfeedback>
            </item>
            Return cQTIQuestionXML
        End Function

        Private Shared Function CreateCDataSection(ByVal ContentText As String) As String
            If ContentText Is Nothing Then
                Return ""
            Else
                Return "<![CDATA[" & ContentText & "]]>"
            End If
        End Function


        Public Function ConvertQTITestXMLToSQTIQuestionObjectList(ByVal cQTITestXML As String) As List(Of SQTIQuestionObject)
            Dim QTIQuestionList As List(Of SQTIQuestionObject) = New List(Of SQTIQuestionObject)
            Dim oQTIQuestion As SQTIQuestionObject
            ' parse out parts of the XML to populate properties of the multiple SQTIQuestionObjects
            Dim XTest As XDocument = XDocument.Parse(cQTITestXML)

            Dim QuestionList = (From xQuestion In XTest...<item> _
                                Select QuestionText = xQuestion.<presentation>.<material>.<mattext>.Value, _
                                QuestionID = xQuestion.<presentation>.<response_lid>.@ident, _
                                cControl_Type = xQuestion.<presentation>.<control_type>.@ident, _
                                AnswerOptions = (From xRespCondition In xQuestion.<resprocessing>.<respcondition> _
                                    Select Answer_Option_Title = xRespCondition.@title).ToList(), _
                                RecordOrder = xQuestion.<itemfeedback>.<material>.<mattext>.Value, _
                                AnswerTexts = (From xResponse_Label In xQuestion.<presentation>.<response_lid>.<render_choice>.<response_label> _
                                    Select AnswerTextOrdinal = xResponse_Label.@ident, _
                                    AnswerText = xResponse_Label.<material>.<mattext>.Value).ToList())
            For Each Question In QuestionList
                oQTIQuestion = New SQTIQuestionObject
                oQTIQuestion.cQuestionID = Question.QuestionID.ToString()

                If (Question.cControl_Type Is Nothing Or Question.cControl_Type = "") Then
                    oQTIQuestion.cControl_Type = "Single"
                Else
                    oQTIQuestion.cControl_Type = Question.cControl_Type.ToString()
                End If
                oQTIQuestion.cQuestionText = Question.QuestionText.ToString()
                oQTIQuestion.cRecordOrder = Question.RecordOrder.ToString()

                For Each AnswerText In Question.AnswerTexts
                    If AnswerText.AnswerTextOrdinal.ToString() = "A" Then
                        oQTIQuestion.cAnswer_A_Text = AnswerText.AnswerText.ToString()
                    End If
                    If AnswerText.AnswerTextOrdinal.ToString() = "B" Then
                        oQTIQuestion.cAnswer_B_Text = AnswerText.AnswerText.ToString()
                    End If
                    If AnswerText.AnswerTextOrdinal.ToString() = "C" Then
                        oQTIQuestion.cAnswer_C_Text = AnswerText.AnswerText.ToString()
                    End If
                    If AnswerText.AnswerTextOrdinal.ToString() = "D" Then
                        oQTIQuestion.cAnswer_D_Text = AnswerText.AnswerText.ToString()
                    End If
                    If AnswerText.AnswerTextOrdinal.ToString() = "E" Then
                        oQTIQuestion.cAnswer_E_Text = AnswerText.AnswerText.ToString()
                    End If
                    If AnswerText.AnswerTextOrdinal.ToString() = "F" Then
                        oQTIQuestion.cAnswer_F_Text = AnswerText.AnswerText.ToString()
                    End If
                Next

                ' more here
                QTIQuestionList.Add(oQTIQuestion)
                oQTIQuestion = Nothing
            Next

            Return QTIQuestionList
        End Function


    End Class
End Namespace