﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for CEBroker
/// </summary>
/// 
namespace PearlsReview.DAL
{
    public class CEBroker : PRProvider3
    {
        string CEBrokerString = "<?xml version=\"1.0\"?><rosters id_parent_provider=\"1489\" upload_key=\"742D729V\">";
        string TXTBrokerString = "<?xml version=\"1.0\"?><courses id_parent_provider=\"1489\" upload_key=\"742D729V\">";
        string PearlsCEBrokerString = "<?xml version=\"1.0\"?><rosters id_parent_provider=\"1768\" upload_key=\"377N833B\">";
        public CEBroker()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string GetCEBrokerList()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_CEBroker";
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CEBrokerString = CEBrokerString + (reader["cebrokercolumn"].ToString());
                    }
                    CEBrokerString = CEBrokerString + " </rosters>";
                    return CEBrokerString;
                }
                else
                {
                    return "";
                }

            }
        }



        public string GetPearlsReviewCEBrokerList()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_Pearls_CEBroker";
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        PearlsCEBrokerString = PearlsCEBrokerString + (reader["cebrokercolumn"].ToString());
                    }
                    PearlsCEBrokerString = PearlsCEBrokerString + " </rosters>";
                    return PearlsCEBrokerString;
                }
                else
                {
                    return "";
                }

            }
        }


        public string GetCEBrokerByUniqueID(int UniqueID,string TopicIds)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_CEBrokerByUniqueID";
                cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = UniqueID;
                cmd.Parameters.Add("@tid", SqlDbType.VarChar).Value = TopicIds;
                cmd.CommandType = CommandType.StoredProcedure; 
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CEBrokerString = CEBrokerString + (reader["cebrokercolumn"].ToString());
                    }
                    CEBrokerString = CEBrokerString + " </rosters>";
                    return CEBrokerString;
                }
                else
                {
                    return "";
                }

            }
        }

        public string GetCEBrokerByUniqueIDOnly(int UniqueID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_CEBrokerBy_UniqueID_Only";
                cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = UniqueID;
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CEBrokerString = CEBrokerString + (reader["cebrokercolumn"].ToString());
                    }
                    CEBrokerString = CEBrokerString + " </rosters>";
                    return CEBrokerString;
                }
                else
                {
                    return "";
                }

            }
        }


        public DataSet GetCEBrokerSubmitList(int IID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlDataAdapter adapter;
                System.Data.DataSet ds = new DataSet();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_CEBroker_SubmittedByUniqueID";
                cmd.Parameters.Add("@IID", SqlDbType.Int).Value = IID;
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
                return ds;
            }
        }

        public string GetCEBrokerCourseList()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_CEBroker_textbook";
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TXTBrokerString = TXTBrokerString + (reader["cebrokercolumn"].ToString());
                    }
                    TXTBrokerString = TXTBrokerString + " </courses>";
                    return TXTBrokerString;
                }
                else
                {
                    return "";
                }

            }
        }


        public bool Deleterosterbyrosterid(string rosterids)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from rosters where rid in ( " + rosterids + " ) ", cn);
                //cmd.Parameters.Add("@rosterids", SqlDbType.VarChar).Value = rosterids;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret >= 1);
            }
        }
    }
}

