﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
/// <summary>
/// Summary description for Facebook
/// </summary>
/// 
 namespace SocialNetwork
{
    public class Facebook
    {
        //public string AppId = "382430615161630";
        public string AppId = "195356297309455";
       // public string Appsecret = "21444b8ec7b1615ef28f9736be2e5497";
        public string Appsecret = "f39febf6267b5187759116adbaeec117";
        public string Scope = "email, user_work_history, user_birthday, user_website";
        public string RedirectUri = "http://cestage.nurse.com:8095/facebook/FacebookCallBack.aspx";
        public string ReturnURL = "http://cestage.nurse.com:8095/aspx/LoginPage.aspx";

	    public Facebook()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }

        public bool BindFBUserAccount(FBUserAccount u, Dictionary<string, Object> d)
        {
            if (u == null || d == null)
                return false;
            foreach (PropertyInfo pro in u.GetType().GetProperties())
            {
                foreach (KeyValuePair<string, Object> pair in d)
                {
                    if (pair.Key.Equals(pro.Name))
                    {
                        pro.SetValue(u, pair.Value.ToString(), null);
                    }
                }
            }
            return true;
        }
    }
    public class FBWorkHistory
    {
        public string id { get; set; }
        public string employer { get; set; }
        public string name { get; set; }
    }
    public class FBUserAccount
    {
        public string name { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string work { get; set; }
        public string id { get; set; }
        public string birthday { get; set; }
        public string link { get; set; }
    }
}
