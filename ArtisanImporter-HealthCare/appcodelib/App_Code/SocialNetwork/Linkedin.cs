﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Text;
using System.IO;

using System.Net;
using Newtonsoft.Json;
using System.Reflection;
/// <summary>
/// Summary description for Linkedin
/// </summary>
namespace SocialNetwork
{
    public class Linkedin
    {
        public string AUTHLINK = "https://www.linkedin.com/uas/oauth2/authorization?response_type=code";
        public string TOKENLINK = "https://www.linkedin.com/uas/oauth2/accessToken";
        public string APPKEY = "gjqv9h3dfmvm";//"zure1059r9y8";
        public string SECRETKEY = "quf4pw21GJdXrt8C";//"gWKWaRtcbhOIUF1D";
        public string STATE = "TEST";
        public string PROFILELINK = "https://api.linkedin.com/v1/people/~?oauth2_access_token=";
        public string EMAILLINK = "https://api.linkedin.com/v1/people/~/email-address?oauth2_access_token=";
        public string REDIRECTURI = "http://cestage.nurse.com:8095/aspx/LoginPage.aspx"; 

        public Linkedin()
        {
            //
            // TODO: Add constructor logic here
            //
        }

       public  string HttpPost(string uri, string methodType)
        {
            var request = WebRequest.Create(uri);
            WebResponse webResponse = null;
            request.Method = methodType;
            try
            {
                webResponse = request.GetResponse();
            }
            catch (WebException ex)
            {
                // lblInfo.Text = ex.Message;
                return null;
            }
            try
            {
                // get the response           
                if (webResponse == null)
                {
                    return null;
                }
                var data = GetResponseByte(webResponse);
                using (var reader = new StreamReader(new MemoryStream(data)))
                {
                    var resultString = reader.ReadToEnd();
                    //Session["expires_in"] = value["expires_in"];
                    //Session["expires_at"] = DateTime.Now.AddSeconds(Convert.ToInt32(value["expires_in"].ToString()));
                    return resultString;
                }
            }
            catch (WebException ex)
            {
                //lblInfo.Text = ex.Message;
                return null;
            }
        }

        public byte[] GetResponseByte(WebResponse response)
        {
            byte[] resultData = null;
            var ms = new MemoryStream();
            var binaryWriter = new BinaryWriter(ms);

            using (var binaryReader = new BinaryReader(response.GetResponseStream()))
            {
                int readedByteCount;
                const int bufferLength = 65536;
                byte[] data;
                do
                {
                    data = new byte[bufferLength];
                    readedByteCount = binaryReader.Read(data, 0, data.Length);

                    if (readedByteCount == 0)
                        break;
                    else
                        binaryWriter.Write(data, 0, readedByteCount);
                }
                while (readedByteCount <= bufferLength);
            }

            ms.Position = 0;
            resultData = ms.ToArray();
            return resultData;
        }

    }
    public class LinkedinUserAccount
    {
        public string name { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string emailaddress { get; set; }
        public string work { get; set; }
        public string id { get; set; }
        public string link { get; set; }
        public LinkedinUserAccount() { }
    }
}
