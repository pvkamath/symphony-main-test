﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.Routing;
using System.Web.Compilation;
using PearlsReview.BLL;

/// <summary>
/// Summary description for ProfessionsAndPagesRouteHandler
/// </summary>
public class ProfessionsAndPagesRouteHandler : IRouteHandler
{
	public ProfessionsAndPagesRouteHandler()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
        string value = requestContext.RouteData.Values["statecerequirements"] as string;
        //if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("/CourseImages/Init/" + value + ".html")))
        if (value == "state-ce-requirements")
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            context.Items.Add("statecerequirements", value);
            return BuildManager.CreateInstanceFromVirtualPath("~/Aspx/StateMandate.aspx", typeof(Page)) as Page;
        }
        if (value == "forgotpassword")
        {            
            return BuildManager.CreateInstanceFromVirtualPath("~/Aspx/ForgotPassword.aspx", typeof(Page)) as Page;
        }
        if (value == "signup")
        {
            return BuildManager.CreateInstanceFromVirtualPath("~/Aspx/LoginPage.aspx", typeof(Page)) as Page;
        }  
        MicrositeDomain Domain = MicrositeDomain.GetMicrositeDomainIDByName(value);
        if (Domain != null)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            context.Items.Add("profession", value);
            return BuildManager.CreateInstanceFromVirtualPath("~/Profession.aspx", typeof(Page)) as Page;
        }
        HttpContext.Current.Response.Redirect("~/Aspx/404.aspx");
        return null;
    } 
}
