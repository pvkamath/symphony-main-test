﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PearlsReview.BLL;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for CoursePanel
/// </summary>
public class CoursePanel
{
	public CoursePanel()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string GenerateCoursePanelHtml(string ContextKey)
    {
        Topic objTopic = Topic.GetTopicByID(Convert.ToInt32(ContextKey));
        Literal litContent = new Literal();
        litContent.Text = Convert.ToString(objTopic.Objectives);
        if ((litContent.Text == string.Empty | Convert.ToString(litContent.Text).Length < 200))
        {
        }
        else
        {
            litContent.Text = litContent.Text.Substring(0, 200) + "...";
        }
        Literal lblFacilityAuthors = new Literal();
        string cAuthors = PearlsReview.BLL.ResourcePerson.GetAuthorsByTopicID(Convert.ToInt32(ContextKey), false);
        if (!string.IsNullOrEmpty(cAuthors))
        {
            lblFacilityAuthors.Text = "<b>by </b>" + cAuthors;

            if ((lblFacilityAuthors.Text.Length < 100))
            {
            }
            else
            {
                lblFacilityAuthors.Text = lblFacilityAuthors.Text.Substring(0, 100) + "...";
            }
        }
        bool bUnlimted = false;
        string imgUnlimted = string.Empty;
        if ((objTopic.Online_Ind == true))
        {
            bUnlimted = true;
            if ((objTopic.Hours <= (decimal)1.5 | objTopic.uce_Ind == true))
            {
                imgUnlimted = "/Images/FreeUnlimitedCESubscriber3.PNG";
            }
            else
            {
                imgUnlimted = "/Images/UnlimitedCE_50off2.PNG";
            }
        }
        string ReturnText = "<table><tr><td valign=\"top\"><img id=\"imgCourse\" src=\"/CourseImages/CourseImages/" + objTopic.Img_Name.ToString().Trim() + "\" style=\"height:50px;width:50px;border-width:0px;\"" + " /></td><td style=\"padding-left:5px; padding-right:2px;\"> " + "<span style=\"color:Black;font-size:10pt;font-weight:bold; width:500px;\">" + objTopic.TopicName.ToString() + " </span>&nbsp&nbsp&nbsp<br/>";
        if ((string.IsNullOrEmpty(objTopic.Subtitle)))
        {
        }
        else
        {
            ReturnText = ReturnText + " <div><span style=\"color:#333333;\">" + objTopic.Subtitle + "</span><br/></div>";
        }
        if ((string.IsNullOrEmpty(lblFacilityAuthors.Text)))
        {
            ReturnText = ReturnText + "";
        }
        else
        {
            ReturnText = ReturnText + "<span style=\"color:#666666;font-weight:normal;font-style:italic;\">" + lblFacilityAuthors.Text + "</span><br/> ";
        }
        ReturnText = ReturnText + "<span style=\"color:Black;font-size:Smaller;font-weight:bold;\">" + objTopic.Course_Number.ToString() + "</span>" + "<span style=\"color:#333333;font-size:smaller;font-weight:normal;\">&nbsp&nbsp;l&nbsp;" + objTopic.Hours.ToString() + " contact hr</span>" + "<br/><br/></td></tr></table><table><tr width=\"100%\"><td ><span style=\"font-size:small; font-weight:bold;\">Price: </span>$" + objTopic.online_Cost.ToString() + " ";
        if ((bUnlimted == true))
        {
            ReturnText = ReturnText + " </td><td><span style=\"font-size: small; font-weight:bold;color:#666666;\">&nbsp&nbsp Or &nbsp</span> " + "<a href=\"http://www.nurse.com/unlimitedce/\" target=\"_blank\"><img src=\"" + imgUnlimted.ToString() + "\" align=\"top\" style=\"border-width:0px;\"/></a></td>";
        }
        ReturnText = ReturnText + "</tr></table><div style=\"text-align:left;\"><br/><span style=\"font-size:small; font-weight:bold;\">Objectives:</span>" + "<table style=\"font-size:12px;\"><tr><td valign=\"top\" style=\"font-family:verdana; font-size: 12px;\"><div>" + litContent.Text + "</div></td></tr><tr><td><br/></td></tr></table></div><div style=\"float:right;\"><a href=\"/course/" + objTopic.Course_Number.ToString() + "/" + objTopic.urlmark.ToString() + "/\"><img src=\"Images/Go_Course.gif\" style=\"border-width:0px;\"/>" + "</a></div>";
        return ReturnText;


    }
}
