﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for EMRCourseCompletions
/// </summary>
namespace PearlsReview.DAL
{
    public class EMRCourseCompletions : PRProvider3
    {
        string EMRString = "<?xml version=\"1.0\" standalone=\"yes\"?>";       

	    public EMRCourseCompletions()
	    {
		    //
		    // TODO: Add constructor logic here
		    //

            
	    }
        public DataSet GetEMRCourseCompletionsByDate(string DateStart, string DateEnd, string cSortExpression)
        {
            
            if ((cSortExpression.Length == 0) || (cSortExpression == null))
            {
                cSortExpression = "";
            }
            else
            {
                    cSortExpression = " order by " + cSortExpression;              
            }
            
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlDataAdapter adapter;
                System.Data.DataSet ds = new DataSet();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_EMR_Course_Completions";
                cmd.Parameters.Add("@date_start", SqlDbType.VarChar).Value = DateStart;
                cmd.Parameters.Add("@date_end", SqlDbType.VarChar).Value = DateEnd;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = cSortExpression;
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
                return ds;
            }
        }

        public string GetEMRCourseCompletionsXmlByDate(string DateStart, string DateEnd, char IsQualified)
        {
            EMRString = GetEMRString();
            //EMRString = EMRString + "<dsXmlSummary>";
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_EMR_Course_Completions_Xml";
                cmd.Parameters.Add("@date_start", SqlDbType.VarChar).Value = DateStart;
                cmd.Parameters.Add("@date_end", SqlDbType.VarChar).Value = DateEnd;
                cmd.Parameters.Add("@isQualified", SqlDbType.Char).Value = IsQualified;
                
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        EMRString = EMRString + (reader["emrcolumn"].ToString());
                    }
                    EMRString = EMRString + " </dsXmlSummary>";
                    return EMRString;
                }
                else
                {
                    return "";
                }

            }
        }
        public string GetEMRString()
        {
            EMRString = EMRString + "<dsXmlSummary xmlns=\"http://www.tempuri.org/dsXmlSummary.xsd\"> " +
                        " <xs:schema id=\"dsXmlSummary\" targetNamespace=\"http://www.tempuri.org/dsXmlSummary.xsd\" " +
                        " xmlns:mstns=\"http://www.tempuri.org/dsXmlSummary.xsd\" xmlns=\"http://www.tempuri.org/ " +
                        " dsXmlSummary.xsd \" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:msdata=\"urn: " +
                        " schemas-microsoft-com:xml-msdata\" attributeFormDefault=\"qualified\" elementFormDefault= \"qualified\"> " +
                        " <xs:element name=\"dsXmlSummary\" msdata:IsDataSet=\"true\"> " +
                        " <xs:complexType> " +
                        " <xs:choice maxOccurs=\"unbounded\"> " +
                        " <xs:element name=\"admin_reports_xmlReports\"> " +
                        " <xs:complexType> " +
                        " <xs:sequence> " +
                        " <xs:element name=\"emailAddress\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"licenseType\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"stateIssued\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"licenseNo\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"expiration\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"dateOfCompletion\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"provider\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"city\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"nameLast\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"nameFirst\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"nameMiddle\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"street\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"nremtReReg\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"state\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"zip\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"nremtNo\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"phone\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"courseNo\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"units\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " <xs:element name=\"Type\" type=\"xs:string\" minOccurs=\"0\"/> " +
                        " </xs:sequence> " +
                        " </xs:complexType> " +
                        " </xs:element> " +
                        " </xs:choice> " +
                        " </xs:complexType> " +
                        " </xs:element>" + 
                        " </xs:schema>";
            return EMRString;
        }
    }
}