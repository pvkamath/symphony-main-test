﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;

/// <summary>
/// Summary description for ValidationSummaryFixWithModelPopup
/// </summary>
namespace CustomControl.ValidationSummaryFix
{
    public sealed class ValidationSummaryFixWithModelPopup : ValidationSummary
    {
        public ValidationSummaryFixWithModelPopup()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), this.ClientID, ";", true);
        }
    }
}
