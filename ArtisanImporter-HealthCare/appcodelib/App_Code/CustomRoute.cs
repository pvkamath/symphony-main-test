﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Compilation;
using System.Web.UI;

namespace PearlsReview.route
{ 
public class CustomRouteHandler : IRouteHandler
{
    public CustomRouteHandler(string virtualPath)
    {
        this.VirtualPath = virtualPath;
    }

    public string VirtualPath { get; private set; }

    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
        foreach (var urlParm in requestContext.RouteData.Values)
        {
            requestContext.HttpContext.Items[urlParm.Key] = urlParm.Value;
        }
        var page = BuildManager.CreateInstanceFromVirtualPath
             (VirtualPath, typeof(Page)) as IHttpHandler;
        return page;
    }
}

public class RedirectRouteHandler : IRouteHandler
{
    private string newUrl;

    public RedirectRouteHandler(string newUrl)
    {
        this.newUrl = newUrl;
    }

    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
        return new RedirectHandler(newUrl);
    }
}
public class RedirectHandler : IHttpHandler
{
    private string newUrl;

    public RedirectHandler(string newUrl)
    {
        this.newUrl = newUrl;
    }

    public bool IsReusable
    {
        get { return true; }
    }

    public void ProcessRequest(HttpContext httpContext)
    {
        httpContext.Response.Status = "301 Moved Permanently";
        httpContext.Response.StatusCode = 301;
        httpContext.Response.AppendHeader("Location", newUrl);
        return;
    }
}
}