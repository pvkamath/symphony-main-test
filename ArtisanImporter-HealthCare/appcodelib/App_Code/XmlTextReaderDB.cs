﻿using System;
using System.Xml;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;

namespace XmlTextReaderDB.Component
{    
    public class SQLGenerator
    {
        public bool CreateSQLStatement(string xmlPath)
        {
            XmlTextReader reader = null;
            Hashtable fieldNamesValues = new Hashtable();
            StringBuilder sqlStatements = new StringBuilder();
            bool error = false;
            //Create Return Object
            SQLInfo sqlInfo = new SQLInfo();
            try
            {
                reader = new XmlTextReader(xmlPath);
                //Read through the XML stream and find proper tokens
                while (reader.Read())
                {
                    if (!error)
                    { //Stop parsing if problem is encountered
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            //Get the name of the XML token
                            switch (reader.Name.ToLower())
                            {
                                case "roster":
                                    if (reader.HasAttributes)
                                    {
                                        fieldNamesValues.Add("course_id", "'" + reader.GetAttribute("id_course") + "'");
                                        fieldNamesValues.Add("roster_id", "'" + reader.GetAttribute("id_roster") + "'");
                                       
                                    }
                                    else
                                    {
                                        sqlInfo.Status = 1;
                                        sqlInfo.StatusMessage = "No attributes on customer element";
                                        sqlInfo.SQL = null;
                                        error = true;
                                    }
                                    break;
                                case "attendee":
                                  
                                    if (reader.HasAttributes)
                                    {
                                        fieldNamesValues.Add("licensecode", "'" + reader.GetAttribute("licensee_profession") + "'");
                                        fieldNamesValues.Add("license_number", "'" + reader.GetAttribute("licensee_number") + "'");
                                        fieldNamesValues.Add("cb_time", "'" + reader.GetAttribute("TimeStamp") + "'");
                                        if (string.IsNullOrEmpty(reader.GetAttribute("message")))
                                        {
                                            fieldNamesValues.Add("cb_message", "'Submitted'");
                                        }
                                        else
                                        {
                                            fieldNamesValues.Add("cb_message", "'" + reader.GetAttribute("message").ToString().Replace("'","''") + "'");
                                        }
                                      
                                    }
                                    break;
                               
                            } //switch
                        } //XmlNodeType check
                    }
                    else
                    {
                        break;
                    }
                    if (reader.NodeType == XmlNodeType.EndElement)
                    {
                        if (reader.Name.ToLower() == "roster")
                        {
                            string[] FVArray = AddSeparator(fieldNamesValues, ',');
                            string fields = FVArray[0];
                            string fieldVals = FVArray[1];
                            sqlStatements.Append("INSERT INTO rosters (" + fields + ") VALUES (" + fieldVals + ");");
                            //Clear out ArrayLists to handle multiple XML records
                            fieldNamesValues.Clear();
                        }
                    }
                } //End While
                if (!error)
                {
                    sqlInfo.Status = 0;
                    sqlInfo.StatusMessage = String.Empty;
                    sqlInfo.SQL = sqlStatements.ToString();
                }

                //return sqlInfo;
                if (sqlInfo.Status == 0)
                {

                    //Execute the SQL statement
                    SqlConnection dataConn = null;
                    try
                    {
                        string connStr = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
                        dataConn = new SqlConnection(connStr);
                        SqlCommand cmd = new SqlCommand(sqlInfo.SQL, dataConn);
                        dataConn.Open();
                        cmd.ExecuteNonQuery();
                        //Response.Write("<b>Database updated!</b>");
                        return true;
                    }
                    catch 
                    {
                        //Response.Write("<b>Error: </b>" + exp.Message);
                        return false;
                    }
                    finally
                    {
                        dataConn.Close();
                    }

                }
                else
                {
                    //Response.Write("<b>Error Occurred:</b><p />" + sqlInfo.StatusMessage);
                    return false;
                }            

            }
            catch (Exception exp)
            {
                sqlInfo.Status = 1;
                sqlInfo.StatusMessage = exp.Message + "\n\n" + exp.StackTrace;
                sqlInfo.SQL = null;
                //return sqlInfo;
                return false;
            }
            finally
            {
                if (reader != null) reader.Close();
            }
        }

        public bool SubmitBroker(SQLInfo sqlInfo)
        {
            if (sqlInfo.Status == 0)
              {
                            
                //Execute the SQL statement
                SqlConnection dataConn = null;
                try
                {
                    string connStr = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
                    dataConn = new SqlConnection(connStr);
                    SqlCommand cmd = new SqlCommand(sqlInfo.SQL, dataConn);
                    dataConn.Open();
                    cmd.ExecuteNonQuery();
                    //Response.Write("<b>Database updated!</b>");
                    return true;
                }
                catch 
                {
                    //Response.Write("<b>Error: </b>" + exp.Message);
                    return false;
                }
                finally
                {
                    dataConn.Close();
                }

            }
            else
            {
                //Response.Write("<b>Error Occurred:</b><p />" + sqlInfo.StatusMessage);
                return false;
            }            
        }

        private string[] AddSeparator(Hashtable fv, char sep)
        {
            int len = fv.Count;
            int i = 0;
            StringBuilder sbFields = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();
            IDictionaryEnumerator fvEnum = fv.GetEnumerator();
            while (fvEnum.MoveNext())
            {
                sbFields.Append(fvEnum.Key);
                if (i < len - 1) sbFields.Append(sep);
                sbValues.Append(fvEnum.Value);
                if (i < len - 1) sbValues.Append(sep);
                i++;
            }
            string[] output = { sbFields.ToString(), sbValues.ToString() };
            return output;
        }

    }

    public struct SQLInfo
    {
        int _status;
        string _statusMessage;
        string _sql;

        public int Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }
        public string StatusMessage
        {
            get
            {
                return _statusMessage;
            }
            set
            {
                _statusMessage = value;
            }
        }
        public string SQL
        {
            get
            {
                return _sql;
            }
            set
            {
                _sql = value;
            }
        }
    }



}

