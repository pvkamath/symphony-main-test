﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Unlimited
{

    /// <summary>
    /// Summary description for Unlimited
    /// </summary>
    public class Unlimited
    {
        public Unlimited()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static decimal UnlimitedValue
        {
            get
            {
                return (decimal)44.95;

                // Stored Procs need to change
                //[sp_r_unlimited][sp_r_unlimited_Order][sp_r_ul_shop_cart][sp_r_ul_order_summary]

            }
        }
    }
}
