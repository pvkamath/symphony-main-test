﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL;
using PearlsReview.Security.Membership;
/// <summary>
/// Summary description for checkuser
/// </summary>
/// 
namespace ValidUser
{
    public class checkuser
    {
        int rnID = 0;
        public int RNID { get { return rnID; } }
        public int Validuser(HttpCookie Userid, HttpCookie Password)
        {

            if (Userid!= null & Password != null )
            {

                int iuserid = PearlsReview.BLL.UserAccount.GetUserAccountByUniqueid(Userid.Value.ToString(), Password.Value.ToString());
                if ((iuserid > 0))
                {
                    return iuserid;
                }
                else
                {
                    return (int)0;
                }
            }
            else
            {
                return (int)0;
            }
        }

        public int Validuser(HttpCookie loginHash, HttpCookie loginGUID, HttpRequest request)
        {
          int userid = 0;
          CEDirectMembershipSQLProvider provider;
          try
          {
              provider = (CEDirectMembershipSQLProvider)Membership.Provider;
              string[] ipandbrowserinfo = PearlsReview.Helpers.GetUserHostAndIPAddress(request);
              userid = provider.ValidateUser(loginHash, loginGUID, 2, ipandbrowserinfo[0], ipandbrowserinfo[1]);
              this.rnID = provider.loginNurseID;
              return userid;
          }
          catch (Exception ex) { return 0; }
          finally { provider = null; }          
        }

        public checkuser()
        {

        }
    }
}