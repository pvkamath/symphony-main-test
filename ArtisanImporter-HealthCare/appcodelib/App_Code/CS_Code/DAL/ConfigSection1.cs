﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PearlsReview
{
    ///// <summary>
    ///// Summary description for ConfigSection1
    ///// </summary>
    //public class PRElement1 : ConfigurationElement
    //{
    //    [ConfigurationProperty("connectionStringName")]
    //    public string ConnectionStringName
    //    {
    //        get { return (string)base["connectionStringName"]; }
    //        set { base["connectionStringName"] = value; }
    //    }

    //    public string ConnectionString
    //    {
    //        get
    //        {
    //            // Return the base class' ConnectionString property.
    //            // The name of the connection string to use is retrieved from the site's 
    //            // custom config section and is used to read the setting from the <connectionStrings> section
    //            // If no connection string name is defined for the <articles> element, the
    //            // parent section's DefaultConnectionString prop is used.
    //            string connStringName = (string.IsNullOrEmpty(this.ConnectionStringName) ?
    //               Globals.Settings.DefaultConnectionStringName : this.ConnectionStringName);
    //            return WebConfigurationManager.ConnectionStrings[connStringName].ConnectionString;
    //        }
    //    }

    //    [ConfigurationProperty("providerType", DefaultValue = "PearlsReview.DAL.SQLClient.SQLPRProvider1")]
    //    public string ProviderType
    //    {
    //        get { return (string)base["providerType"]; }
    //        set { base["providerType"] = value; }
    //    }

    //    [ConfigurationProperty("pageSize", DefaultValue = "10")]
    //    public int PageSize
    //    {
    //        get { return (int)base["pageSize"]; }
    //        set { base["pageSize"] = value; }
    //    }

    //    [ConfigurationProperty("enableCaching", DefaultValue = "false")]
    //    public bool EnableCaching
    //    {
    //        get { return (bool)base["enableCaching"]; }
    //        set { base["enableCaching"] = value; }
    //    }

    //    [ConfigurationProperty("cacheDuration")]
    //    public int CacheDuration
    //    {
    //        get
    //        {
    //            int duration = (int)base["cacheDuration"];
    //            return (duration > 0 ? duration : Globals.Settings.DefaultCacheDuration);
    //        }
    //        set { base["cacheDuration"] = value; }
    //    }
    //}
}