﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for SiteProvider1
    /// </summary>
    public static class SiteProvider1
    {
        public static PRProvider1 PR
        {
            get { return PRProvider1.Instance; }
        }
    }
}