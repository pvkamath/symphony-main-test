using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

namespace PearlsReview.DAL
{
   public abstract partial class PRProvider : DataAccess
   {
      static private PRProvider _instance = null;
      /// <summary>
      /// Returns an instance of the provider type specified in the config file
      /// </summary>
      static public PRProvider Instance
      {
          get
          {
              if (_instance == null)
                  _instance = (PRProvider)Activator.CreateInstance(
                     Type.GetType(Globals.Settings.PR.ProviderType));
              return _instance;
          }
      }

      public PRProvider()
      {
          this.ConnectionString = Globals.Settings.PR.ConnectionString;
          this.EnableCaching = Globals.Settings.PR.EnableCaching;
          this.CacheDuration = Globals.Settings.PR.CacheDuration;
          this.RConnectionString = Globals.Settings.PR.RConnectionString;
          
      }
//      /////////////////////////////////////////////////////////
//      // methods that work with AdClicks
//      public abstract int GetAdClickCount();
//      public abstract List<AdClickInfo> GetAdClicks(string cSortExpression);
//      public abstract int GetUserAccountByUniqueid(string uniqueid,string password);
//      public abstract decimal GetTaxRateByUserId(int iID);
//      public abstract AdClickInfo GetAdClickByID(int AdClickID);
//      public abstract bool DeleteAdClick(int AdClickID);
//      public abstract bool UpdateAdClick(AdClickInfo AdClick);
//      public abstract int InsertAdClick(AdClickInfo AdClick);

//      /// <summary>
//      /// Returns a new AdClickInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual AdClickInfo GetAdClickFromReader(IDataReader reader)
//      {
//          return GetAdClickFromReader(reader, true);
//      }
//      protected virtual AdClickInfo GetAdClickFromReader(IDataReader reader, bool readMemos)
//      {
//          AdClickInfo AdClick = new AdClickInfo(
//            (int)reader["AdClickID"],
//            (int)reader["AdID"],
//            (DateTime)reader["Date"],
//            reader["PageName"].ToString());


//          return AdClick;
//      }

//      /// <summary>
//      /// Returns a collection of AdClickInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<AdClickInfo> GetAdClickCollectionFromReader(IDataReader reader)
//      {
//          return GetAdClickCollectionFromReader(reader, true);
//      }
//      protected virtual List<AdClickInfo> GetAdClickCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<AdClickInfo> AdClicks = new List<AdClickInfo>();
//          while (reader.Read())
//              AdClicks.Add(GetAdClickFromReader(reader, readMemos));
//          return AdClicks;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with Advertisements
//      public abstract int GetAdvertisementCount();
//      public abstract List<AdvertisementInfo> GetAdvertisements(string cSortExpression);
//      public abstract AdvertisementInfo GetAdvertisementByID(int AdID);
//      public abstract bool DeleteAdvertisement(int AdID);
//      public abstract bool UpdateAdvertisement(AdvertisementInfo Advertisement);
//      public abstract int InsertAdvertisement(AdvertisementInfo Advertisement);

//      /// <summary>
//      /// Returns a new AdvertisementInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual AdvertisementInfo GetAdvertisementFromReader(IDataReader reader)
//      {
//          return GetAdvertisementFromReader(reader, true);
//      }
//      protected virtual AdvertisementInfo GetAdvertisementFromReader(IDataReader reader, bool readMemos)
//      {
//          AdvertisementInfo Advertisement = new AdvertisementInfo(
//            (int)reader["AdID"],
//            reader["AdName"].ToString(),
//            reader["AdType"].ToString(),
//            reader["AdDest"].ToString(),
//            reader["AdText"].ToString(),
//            reader["AdFilename"].ToString(),
//            reader["AdUnitType"].ToString());


//          return Advertisement;
//      }

//      /// <summary>
//      /// Returns a collection of AdvertisementInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<AdvertisementInfo> GetAdvertisementCollectionFromReader(IDataReader reader)
//      {
//          return GetAdvertisementCollectionFromReader(reader, true);
//      }
//      protected virtual List<AdvertisementInfo> GetAdvertisementCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<AdvertisementInfo> Advertisements = new List<AdvertisementInfo>();
//          while (reader.Read())
//              Advertisements.Add(GetAdvertisementFromReader(reader, readMemos));
//          return Advertisements;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with Categories
//      public abstract int GetCategoryCount();
//      public abstract int GetTextbookCategoryCount();
//      public abstract int GetNonTextbookCategoryCount();
//      public abstract List<CategoryInfo> GetCategories(string cSortExpression);
//      public abstract List<CategoryInfo> GetComplianceCategories(string cSortExpression);
//      public abstract List<CategoryInfo> GetTextbookCategories(string cSortExpression);
//      public abstract List<CategoryInfo> GetNonTextbookCategories(string cSortExpression);
//      public abstract List<CategoryInfo> GetTextbookCategoriesForDisplay(string cSortExpression);
//      public abstract List<CategoryInfo> GetNonTextbookCategoriesForDisplay(string cSortExpression);
//      public abstract CategoryInfo GetCategoryByID(int ID);
//      public abstract bool DeleteCategory(int ID);
//      public abstract bool UpdateCategory(CategoryInfo Category);
//      public abstract int InsertCategory(CategoryInfo Category);
//      public abstract DataSet GetCategoryByWebSite(string website);
//      public abstract CategoryInfo GetCatNameByTopicId(int TopicId);
//      public abstract CategoryInfo GetPrimaryCatNameByTopicId(int TopicId);

//      public abstract List<CategoryTopicCountInfo> GetNonTextbookCategoriesWithTopicCount(string cSortExpression);
//      public abstract List<CategoryTopicCountInfo> GetRNonTextbookCategoriesWithTopicCount(string cSortExpression);
//      public abstract List<CategoryTopicCountInfo> GetRNonTextbookStateCategoriesWithTopicCount();
//      public abstract List<CategoryTopicCountInfo> GetNonTextbookCategoriesWithTopicCountByFacilityId(int facilityid,string cSortExpression);
//      public abstract List<CategoryTopicCountInfo> GetTextbookCategoriesWithTopicCountByFacilityId(int facilityid, string cSortExpression);
//      public abstract string CheckCatNameByTopicId(int TopicId, string CatName);


//      /// <summary>
//      /// Returns a new CategoryInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual CategoryInfo GetCategoryFromReader(IDataReader reader)
//      {
//          return GetCategoryFromReader(reader, true);
//      }
//      protected virtual CategoryInfo GetCategoryFromReader(IDataReader reader, bool readMemos)
//      {
//          CategoryInfo Category = new CategoryInfo(
//            (int)reader["ID"],
//            reader["Title"].ToString(),
//            (bool)reader["BuildPage"],
//            reader["MetaKW"].ToString(),
//            reader["MetaDesc"].ToString(),
//            reader["MetaTitle"].ToString(),
//            reader["PageText"].ToString(),
//            (bool)reader["Textbook"],
//            (bool)reader["ShowInList"],
//            reader["CertName"].ToString(),
//            reader["CredAward"].ToString(),
//            reader["ExamType"].ToString(),
//            reader["ExamCost"].ToString(),
//            reader["AdminOrg"].ToString(),
//            reader["Website"].ToString(),
//            reader["Reqments"].ToString(),
//            reader["EligCrit"].ToString(),
//            reader["Image"].ToString(),
//            (int)reader["FacilityID"]);

//          return Category;
//      }

//      /// <summary>
//      /// Returns a collection of CategoryInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<CategoryInfo> GetCategoryCollectionFromReader(IDataReader reader)
//      {
//          return GetCategoryCollectionFromReader(reader, true);
//      }
//      protected virtual List<CategoryInfo> GetCategoryCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<CategoryInfo> Categories = new List<CategoryInfo>();
//          while (reader.Read())
//              Categories.Add(GetCategoryFromReader(reader, readMemos));
//          return Categories;
//      }

//       /// NOTE: These variations are for CategoryTopicCountInfo lists
//       /// <summary>
//      /// Returns a new CategoryTopicCountInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual CategoryTopicCountInfo GetCategoryTopicCountFromReader(IDataReader reader)
//      {
//          return GetCategoryTopicCountFromReader(reader, true);
//      }
//      protected virtual CategoryTopicCountInfo GetCategoryTopicCountFromReader(IDataReader reader, bool readMemos)
//      {
//          CategoryTopicCountInfo Category = new CategoryTopicCountInfo(
//            (int)reader["ID"],
//            reader["Title"].ToString(),
//            (int)reader["TopicCount"],
//            reader["DTitle"].ToString());

//          return Category;
//      }

//      /// <summary>
//      /// Returns a collection of CategoryTopicCountInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<CategoryTopicCountInfo> GetCategoryTopicCountCollectionFromReader(IDataReader reader)
//      {
//          return GetCategoryTopicCountCollectionFromReader(reader, true);
//      }
//      protected virtual List<CategoryTopicCountInfo> GetCategoryTopicCountCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<CategoryTopicCountInfo> Categories = new List<CategoryTopicCountInfo>();
//          while (reader.Read())
//              Categories.Add(GetCategoryTopicCountFromReader(reader, readMemos));
//          return Categories;
//      }

//      /// <summary>
//      /// Returns a new CheckBoxListCategoryInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual CheckBoxListCategoryInfo GetCheckBoxListCategoryFromReader(IDataReader reader)
//      {
//          CheckBoxListCategoryInfo CheckBoxListCategory = new CheckBoxListCategoryInfo(
//            (int)reader["ID"],
//            reader["Title"].ToString(),
//            (bool)reader["Selected"]);

//          return CheckBoxListCategory;
//      }

//      /// <summary>
//      /// Returns a collection of CheckBoxListCategoryInfo objects with the data read from the input DataReader
//      /// </summary>
//       protected virtual List<CheckBoxListCategoryInfo> GetCheckBoxListCategoryCollectionFromReader(IDataReader reader)
//      {
//          List<CheckBoxListCategoryInfo> CheckBoxListCategories = new List<CheckBoxListCategoryInfo>();
//          while (reader.Read())
//              CheckBoxListCategories.Add(GetCheckBoxListCategoryFromReader(reader));
//          return CheckBoxListCategories;
//      }

//       public abstract int GetCategoryCountByTopicID(int TopicID);
//      //public abstract List<CategoryInfo> GetCategoriesX(string cSortExpression);
//      public abstract List<CategoryInfo> GetCategoriesByTopicID(int TopicID, string cSortExpression);
//      public abstract List<CategoryInfo> GetTextbookCategoriesNotAssignedToTopicID(int TopicID, string cSortExpression);
//      public abstract List<CategoryInfo> GetNonTextbookCategoriesNotAssignedToTopicID(int TopicID, string cSortExpression);
//      public abstract List<CheckBoxListCategoryInfo> GetAllCategoriesPlusTopicAssignments(int TopicID, string cSortExpression);
//      public abstract List<CheckBoxListCategoryInfo> GetAllNonTextbookCategoriesPlusTopicAssignments(int TopicID, string cSortExpression);
//      public abstract List<CheckBoxListCategoryInfo> GetAllTextbookCategoriesPlusTopicAssignments(int TopicID, string cSortExpression);
//      public abstract bool UpdateCategoryTopicAssignments(int CategoryID, string TopicIDAssignments);

//      /////////////////////////////////////////////////////////
//      // methods that work with CategoryLinks
//      public abstract int GetCategoryLinkCount();
//      public abstract List<CategoryLinkInfo> GetCategoryLinks(string cSortExpression);
//      public abstract CategoryLinkInfo GetCategoryLinkByID(int CatTopicID);
//      public abstract bool DeleteCategoryLink(int CatTopicID);
//      public abstract bool UpdateCategoryLink(CategoryLinkInfo CategoryLink);
//      public abstract int InsertCategoryLink(CategoryLinkInfo CategoryLink);

//      /// <summary>
//      /// Returns a new CategoryLinkInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual CategoryLinkInfo GetCategoryLinkFromReader(IDataReader reader)
//      {
//          return GetCategoryLinkFromReader(reader, true);
//      }
//      protected virtual CategoryLinkInfo GetCategoryLinkFromReader(IDataReader reader, bool readMemos)
//      {
//          CategoryLinkInfo CategoryLink = new CategoryLinkInfo(
//            (int)reader["CatTopicID"],
//            (int)reader["CategoryID"],
//            (int)reader["TopicID"]);


//          return CategoryLink;
//      }

//      /// <summary>
//      /// Returns a collection of CategoryLinkInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<CategoryLinkInfo> GetCategoryLinkCollectionFromReader(IDataReader reader)
//      {
//          return GetCategoryLinkCollectionFromReader(reader, true);
//      }
//      protected virtual List<CategoryLinkInfo> GetCategoryLinkCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<CategoryLinkInfo> CategoryLinks = new List<CategoryLinkInfo>();
//          while (reader.Read())
//              CategoryLinks.Add(GetCategoryLinkFromReader(reader, readMemos));
//          return CategoryLinks;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with CertificateDefinition
//      public abstract int GetCertificateDefinitionCount();
//      public abstract List<CertificateDefinitionInfo> GetCertificateDefinitions(string cSortExpression);
//      public abstract List<CertificateDefinitionInfo> GetCertificateDefinitionsByCertType(string cCertType, string cSortExpression);
//      public abstract CertificateDefinitionInfo GetCertificateDefinitionByID(int CertID);
//      public abstract bool DeleteCertificateDefinition(int CertID);
//      public abstract bool UpdateCertificateDefinition(CertificateDefinitionInfo CertificateDefinition);
//      public abstract int InsertCertificateDefinition(CertificateDefinitionInfo CertificateDefinition);

//      /// <summary>
//      /// Returns a new CertificateDefinitionInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual CertificateDefinitionInfo GetCertificateDefinitionFromReader(IDataReader reader)
//      {
//          return GetCertificateDefinitionFromReader(reader, true);
//      }
//      protected virtual CertificateDefinitionInfo GetCertificateDefinitionFromReader(IDataReader reader, bool readMemos)
//      {
//          CertificateDefinitionInfo CertificateDefinition = new CertificateDefinitionInfo(
//            (int)reader["CertID"],
//            reader["CertName"].ToString(),
//            reader["CertBody"].ToString(),
//            reader["CertType"].ToString(),
//            (int)reader["FacilityID"]);

//          return CertificateDefinition;
//      }

//      /// <summary>
//      /// Returns a collection of CertificateDefinitionInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<CertificateDefinitionInfo> GetCertificateDefinitionCollectionFromReader(IDataReader reader)
//      {
//          return GetCertificateDefinitionCollectionFromReader(reader, true);
//      }
//      protected virtual List<CertificateDefinitionInfo> GetCertificateDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<CertificateDefinitionInfo> CertificateDefinitions = new List<CertificateDefinitionInfo>();
//          while (reader.Read())
//              CertificateDefinitions.Add(GetCertificateDefinitionFromReader(reader, readMemos));
//          return CertificateDefinitions;
//      }



//      ///// <summary>
//      ///// Returns a new DiscountInfo instance filled with the DataReader's current record data
//      ///// </summary>
//      //protected virtual DiscountInfo GetDiscountFromReader(IDataReader reader)
//      //{
//      //    return GetDiscountFromReader(reader, true);
//      //}
//      //protected virtual DiscountInfo GetDiscountFromReader(IDataReader reader, bool readMemos)
//      //{
//      //    DiscountInfo Discount = new DiscountInfo(
//      //          (int)reader["discountid"],
//      //          (int)reader["topicid"],
//      //          reader["tagline"].ToString(),
//      //          (DateTime)reader["startdate"],
//      //          (DateTime)reader["enddate"],
//      //          (decimal)reader["discount_price"],
//      //          reader["discount_type"].ToString(),
//      //          reader["sponsor_text"].ToString(),
//      //          reader["img_file"].ToString(),
//      //          reader["discount_url"].ToString(),
//      //          (bool)reader["survey_ind"],
//      //          (int)reader["impression"],
//      //          (int)reader["clickthrough"],
//      //          reader["username"].ToString(),
//      //          reader["userpassword"].ToString(),
//      //          (bool)reader["section_ind"],
//      //          (decimal)reader["offline_price"],
//      //          (bool)reader["virtualurl_ind"]);
//      //    return Discount;
//      //}



//      ///// <summary>
//      ///// Returns a collection of DiscountInfo objects with the data read from the input DataReader
//      ///// </summary>
//      //protected virtual List<DiscountInfo> GetDiscountCollectionFromReader(IDataReader reader)
//      //{
//      //    return GetDiscountCollectionFromReader(reader, true);
//      //}
//      //protected virtual List<DiscountInfo> GetDiscountCollectionFromReader(IDataReader reader, bool readMemos)
//      //{
//      //    List<DiscountInfo> Discounts = new List<DiscountInfo>();
//      //    while (reader.Read())
//      //        Discounts.Add(GetDiscountFromReader(reader, readMemos));
//      //    return Discounts;
//      //}



//      /////////////////////////////////////////////////////////
//      // methods that work with EmailDefinitions
//      public abstract int GetEmailDefinitionCount();
//      public abstract List<EmailDefinitionInfo> GetEmailDefinitions(string cSortExpression);
//      public abstract EmailDefinitionInfo GetEmailDefinitionByID(int EDefID);
//      public abstract bool DeleteEmailDefinition(int EDefID);
//      public abstract bool UpdateEmailDefinition(EmailDefinitionInfo EmailDefinition);
//      public abstract int InsertEmailDefinition(EmailDefinitionInfo EmailDefinition);

//      /// <summary>
//      /// Returns a new EmailDefinitionInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual EmailDefinitionInfo GetEmailDefinitionFromReader(IDataReader reader)
//      {
//          return GetEmailDefinitionFromReader(reader, true);
//      }
//      protected virtual EmailDefinitionInfo GetEmailDefinitionFromReader(IDataReader reader, bool readMemos)
//      {
//          EmailDefinitionInfo EmailDefinition = new EmailDefinitionInfo(
//            (int)reader["EDefID"],
//            reader["EDefName"].ToString(),
//            reader["EDefText"].ToString());


//          return EmailDefinition;
//      }

//      /// <summary>
//      /// Returns a collection of EmailDefinitionInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<EmailDefinitionInfo> GetEmailDefinitionCollectionFromReader(IDataReader reader)
//      {
//          return GetEmailDefinitionCollectionFromReader(reader, true);
//      }
//      protected virtual List<EmailDefinitionInfo> GetEmailDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<EmailDefinitionInfo> EmailDefinitions = new List<EmailDefinitionInfo>();
//          while (reader.Read())
//              EmailDefinitions.Add(GetEmailDefinitionFromReader(reader, readMemos));
//          return EmailDefinitions;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with EmailQueue
//      public abstract int GetEmailQueueCount();
//      public abstract List<EmailQueueInfo> GetEmailQueue(string cSortExpression);
//      public abstract EmailQueueInfo GetEmailQueueByID(int EQueueID);
//      public abstract bool DeleteEmailQueue(int EQueueID);
//      public abstract bool UpdateEmailQueue(EmailQueueInfo EmailQueue);
//      public abstract int InsertEmailQueue(EmailQueueInfo EmailQueue);

//      /// <summary>
//      /// Returns a new EmailQueueInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual EmailQueueInfo GetEmailQueueFromReader(IDataReader reader)
//      {
//          return GetEmailQueueFromReader(reader, true);
//      }
//      protected virtual EmailQueueInfo GetEmailQueueFromReader(IDataReader reader, bool readMemos)
//      {
//          EmailQueueInfo EmailQueue = new EmailQueueInfo(
//            (int)reader["EQueueID"],
//            (int)reader["UserID"],
//            (int)reader["EDefID"],
//            (Convert.IsDBNull(reader["SentDate"])
//                ? "" : ((DateTime)reader["SentDate"]).ToShortDateString()) );

//          return EmailQueue;
//      }

//      /// <summary>
//      /// Returns a collection of EmailQueueInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<EmailQueueInfo> GetEmailQueueCollectionFromReader(IDataReader reader)
//      {
//          return GetEmailQueueCollectionFromReader(reader, true);
//      }
//      protected virtual List<EmailQueueInfo> GetEmailQueueCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<EmailQueueInfo> EmailQueue = new List<EmailQueueInfo>();
//          while (reader.Read())
//              EmailQueue.Add(GetEmailQueueFromReader(reader, readMemos));
//          return EmailQueue;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with FacilityGroups
//      // ******** CEDIRECT VERSION ************
//       public abstract int GetFacilityGroupCount();
//      public abstract List<FacilityGroupInfo> GetFacilityGroups(string cSortExpression);
//      public abstract List<FacilityGroupInfo> GetFacilityGroupsExceptGiftCard(String cSortExpression);
//      public abstract List<FacilityGroupInfo> GetFacilityGroupsByParentOfFacilityID(int FacilityID, string cSortExpression);
//      public abstract List<FacilityGroupInfo> GetFacilityGroupsByParentID(int ParentID, string cSortExpression);
//      public abstract FacilityGroupInfo GetFacilityGroupByID(int FacID);
//      public abstract FacilityGroupInfo GetFacilityGroupByFacilityCode(string FacCode);
//      public abstract FacilityGroupInfo GetFacilityGroupByFacName(string FacName);
//      public abstract bool DeleteFacilityGroup(int FacID);
//      public abstract bool UpdateFacilityGroup(FacilityGroupInfo FacilityGroup);
//      public abstract int InsertFacilityGroup(FacilityGroupInfo FacilityGroup);

//      /// <summary>
//      /// Returns a new FacilityGroupInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual FacilityGroupInfo GetFacilityGroupFromReader(IDataReader reader)
//      {
//          return GetFacilityGroupFromReader(reader, true);
//      }
//      protected virtual FacilityGroupInfo GetFacilityGroupFromReader(IDataReader reader, bool readMemos)
//      {
//          FacilityGroupInfo FacilityGroup = new FacilityGroupInfo(
//            (int)reader["FacID"],
//            reader["FacCode"].ToString(),
//            reader["FacName"].ToString(),
//            (int)reader["MaxUsers"],
//            (DateTime)reader["Started"],
//            (DateTime)reader["Expires"],
//            (int)(Convert.IsDBNull(reader["RepID"]) ? (int)0 : (int)reader["RepID"]),
//            reader["Comment"].ToString(),
//            (bool)reader["Compliance"],
//            (bool)reader["CustomTopics"],
//            (bool)reader["QuizBowl"],
//            reader["homeurl"].ToString(),
//            reader["tagline"].ToString(),
//            reader["address1"].ToString(),
//            reader["address2"].ToString(),
//            reader["city"].ToString(),
//            reader["state"].ToString(),
//            reader["zipcode"].ToString(),
//            reader["phone"].ToString(),
//            reader["contact_name"].ToString(),
//            reader["contact_phone"].ToString(),
//            reader["contact_ext"].ToString(),
//            reader["contact_email"].ToString(),
//            (int)(Convert.IsDBNull(reader["parent_id"]) ? (int)0 : (int)reader["parent_id"]),
//            reader["welcome_pg"].ToString(),
//            reader["login_help"].ToString(),
//            reader["logo_img"].ToString(),
//            (bool)reader["active"],
//            reader["support_email"].ToString(),
//            reader["feedback_email"].ToString(),
//            reader["default_password"].ToString() );
          
//          return FacilityGroup;
//      }

//      /// <summary>
//      /// Returns a collection of FacilityGroupInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<FacilityGroupInfo> GetFacilityGroupCollectionFromReader(IDataReader reader)
//      {
//          return GetFacilityGroupCollectionFromReader(reader, true);
//      }
//      protected virtual List<FacilityGroupInfo> GetFacilityGroupCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<FacilityGroupInfo> FacilityGroups = new List<FacilityGroupInfo>();
//          while (reader.Read())
//              FacilityGroups.Add(GetFacilityGroupFromReader(reader, readMemos));
//          return FacilityGroups;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with FieldsOfInterest
//      public abstract int GetFieldOfInterestCount();
//      public abstract List<FieldOfInterestInfo> GetFieldsOfInterest(string cSortExpression);
//      public abstract FieldOfInterestInfo GetFieldOfInterestByID(int InterestID);
//      public abstract bool DeleteFieldOfInterest(int InterestID);
//      public abstract bool UpdateFieldOfInterest(FieldOfInterestInfo FieldOfInterest);
//      public abstract int InsertFieldOfInterest(FieldOfInterestInfo FieldOfInterest);
//      public abstract List<FieldOfInterestInfo> GetFieldsOfInterestByUserID(int iID, string cSortExpression);
//      public abstract bool UpdateUserInterestAssignments(int UserID, string InterestIDAssignments);
//       public abstract bool AssignInterestToUser(int UserID, int InterestID);

//       /// <summary>
//      /// Returns a new FieldOfInterestInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual FieldOfInterestInfo GetFieldOfInterestFromReader(IDataReader reader)
//      {
//          return GetFieldOfInterestFromReader(reader, true);
//      }
//      protected virtual FieldOfInterestInfo GetFieldOfInterestFromReader(IDataReader reader, bool readMemos)
//      {
//          FieldOfInterestInfo FieldOfInterest = new FieldOfInterestInfo(
//            (int)reader["InterestID"],
//            reader["IntDescr"].ToString());


//          return FieldOfInterest;
//      }

//      /// <summary>
//      /// Returns a collection of FieldOfInterestInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<FieldOfInterestInfo> GetFieldOfInterestCollectionFromReader(IDataReader reader)
//      {
//          return GetFieldOfInterestCollectionFromReader(reader, true);
//      }
//      protected virtual List<FieldOfInterestInfo> GetFieldOfInterestCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<FieldOfInterestInfo> FieldsOfInterest = new List<FieldOfInterestInfo>();
//          while (reader.Read())
//              FieldsOfInterest.Add(GetFieldOfInterestFromReader(reader, readMemos));
//          return FieldsOfInterest;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with LectureAttendees
//      public abstract int GetLectureAttendeeCount();
//      public abstract List<LectureAttendeeInfo> GetLectureAttendees(string cSortExpression);
//      public abstract LectureAttendeeInfo GetLectureAttendeeByID(int LectAttID);
//      public abstract bool DeleteLectureAttendee(int LectAttID);
//      public abstract bool UpdateLectureAttendee(LectureAttendeeInfo LectureAttendee);
//      public abstract int InsertLectureAttendee(LectureAttendeeInfo LectureAttendee);

//      /// <summary>
//      /// Returns a new LectureAttendeeInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual LectureAttendeeInfo GetLectureAttendeeFromReader(IDataReader reader)
//      {
//          return GetLectureAttendeeFromReader(reader, true);
//      }
//      protected virtual LectureAttendeeInfo GetLectureAttendeeFromReader(IDataReader reader, bool readMemos)
//      {
//          LectureAttendeeInfo LectureAttendee = new LectureAttendeeInfo(
//            (int)reader["LectAttID"],
//            (int)reader["LectEvtID"],
//            (int)(Convert.IsDBNull(reader["UserID"]) ? (int)0 : (int)reader["UserID"]),
//            reader["UserName"].ToString(),
//            reader["PW"].ToString(),
//            reader["FirstName"].ToString(),
//            reader["LastName"].ToString(),
//            reader["MI"].ToString(),
//            reader["Address1"].ToString(),
//            reader["Address2"].ToString(),
//            reader["City"].ToString(),
//            reader["State"].ToString(),
//            reader["Zip"].ToString(),
//            reader["Email"].ToString(),
//            (DateTime)reader["DateAdded"],
//            (bool)reader["Completed"]);


//          return LectureAttendee;
//      }

//      /// <summary>
//      /// Returns a collection of LectureAttendeeInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<LectureAttendeeInfo> GetLectureAttendeeCollectionFromReader(IDataReader reader)
//      {
//          return GetLectureAttendeeCollectionFromReader(reader, true);
//      }
//      protected virtual List<LectureAttendeeInfo> GetLectureAttendeeCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<LectureAttendeeInfo> LectureAttendees = new List<LectureAttendeeInfo>();
//          while (reader.Read())
//              LectureAttendees.Add(GetLectureAttendeeFromReader(reader, readMemos));
//          return LectureAttendees;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with LectureDefinitions
//      public abstract int GetLectureDefinitionCount();
//      public abstract List<LectureDefinitionInfo> GetLectureDefinitions(string cSortExpression);
//      public abstract LectureDefinitionInfo GetLectureDefinitionByID(int LectureID);
//      public abstract bool DeleteLectureDefinition(int LectureID);
//      public abstract bool UpdateLectureDefinition(LectureDefinitionInfo LectureDefinition);
//      public abstract int InsertLectureDefinition(LectureDefinitionInfo LectureDefinition);

//      /// <summary>
//      /// Returns a new LectureDefinitionInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual LectureDefinitionInfo GetLectureDefinitionFromReader(IDataReader reader)
//      {
//          return GetLectureDefinitionFromReader(reader, true);
//      }
//      protected virtual LectureDefinitionInfo GetLectureDefinitionFromReader(IDataReader reader, bool readMemos)
//      {
//          LectureDefinitionInfo LectureDefinition = new LectureDefinitionInfo(
//            (int)reader["LectureID"],
//            reader["LectTitle"].ToString(),
//            (int)reader["CertID"],
//            reader["Comment"].ToString());


//          return LectureDefinition;
//      }

//      /// <summary>
//      /// Returns a collection of LectureDefinitionInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<LectureDefinitionInfo> GetLectureDefinitionCollectionFromReader(IDataReader reader)
//      {
//          return GetLectureDefinitionCollectionFromReader(reader, true);
//      }
//      protected virtual List<LectureDefinitionInfo> GetLectureDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<LectureDefinitionInfo> LectureDefinitions = new List<LectureDefinitionInfo>();
//          while (reader.Read())
//              LectureDefinitions.Add(GetLectureDefinitionFromReader(reader, readMemos));
//          return LectureDefinitions;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with LectureEvents
//      public abstract int GetLectureEventCount();
//      public abstract List<LectureEventInfo> GetLectureEvents(string cSortExpression);
//      public abstract LectureEventInfo GetLectureEventByID(int LectEvtID);
//      public abstract bool DeleteLectureEvent(int LectEvtID);
//      public abstract bool UpdateLectureEvent(LectureEventInfo LectureEvent);
//      public abstract int InsertLectureEvent(LectureEventInfo LectureEvent);

//      /// <summary>
//      /// Returns a new LectureEventInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual LectureEventInfo GetLectureEventFromReader(IDataReader reader)
//      {
//          return GetLectureEventFromReader(reader, true);
//      }
//      protected virtual LectureEventInfo GetLectureEventFromReader(IDataReader reader, bool readMemos)
//      {
//          LectureEventInfo LectureEvent = new LectureEventInfo(
//            (int)reader["LectEvtID"],
//            (int)reader["LectureID"],
//            (DateTime)reader["StartDate"],
//            (int)reader["Capacity"],
//            (int)reader["Attendees"],
//            reader["Comment"].ToString(),
//            reader["Facility"].ToString(),
//            reader["City"].ToString(),
//            reader["State"].ToString(),
//           (bool) reader["Finalized"]);


//          return LectureEvent;
//      }

//      /// <summary>
//      /// Returns a collection of LectureEventInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<LectureEventInfo> GetLectureEventCollectionFromReader(IDataReader reader)
//      {
//          return GetLectureEventCollectionFromReader(reader, true);
//      }
//      protected virtual List<LectureEventInfo> GetLectureEventCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<LectureEventInfo> LectureEvents = new List<LectureEventInfo>();
//          while (reader.Read())
//              LectureEvents.Add(GetLectureEventFromReader(reader, readMemos));
//          return LectureEvents;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with LectureTopicLinks
//      public abstract int GetLectureTopicLinkCount();
//      public abstract List<LectureTopicLinkInfo> GetLectureTopicLinks(string cSortExpression);
//      public abstract LectureTopicLinkInfo GetLectureTopicLinkByID(int LectTopID);
//      public abstract bool DeleteLectureTopicLink(int LectTopID);
//      public abstract bool UpdateLectureTopicLink(LectureTopicLinkInfo LectureTopicLink);
//      public abstract int InsertLectureTopicLink(LectureTopicLinkInfo LectureTopicLink);
//       public abstract bool UpdateLectureTopicLinkAssignments(int LectureID, string TopicIDAssignments, int Sequence);

//      /// <summary>
//      /// Returns a new LectureTopicLinkInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual LectureTopicLinkInfo GetLectureTopicLinkFromReader(IDataReader reader)
//      {
//          return GetLectureTopicLinkFromReader(reader, true);
//      }
//      protected virtual LectureTopicLinkInfo GetLectureTopicLinkFromReader(IDataReader reader, bool readMemos)
//      {
//          LectureTopicLinkInfo LectureTopicLink = new LectureTopicLinkInfo(
//            (int)reader["LectTopID"],
//            (int)reader["LectureID"],
//            (int)reader["TopicID"],
//            (int)reader["Sequence"]);


//          return LectureTopicLink;
//      }

//      /// <summary>
//      /// Returns a collection of LectureTopicLinkInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<LectureTopicLinkInfo> GetLectureTopicLinkCollectionFromReader(IDataReader reader)
//      {
//          return GetLectureTopicLinkCollectionFromReader(reader, true);
//      }
//      protected virtual List<LectureTopicLinkInfo> GetLectureTopicLinkCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<LectureTopicLinkInfo> LectureTopicLinks = new List<LectureTopicLinkInfo>();
//          while (reader.Read())
//              LectureTopicLinks.Add(GetLectureTopicLinkFromReader(reader, readMemos));
//          return LectureTopicLinks;
//      }

//       //////////////////////////////////////////////////////////
//       // Methods that work with AreaType
//      public abstract List<AreaTypeInfo> GetAreaTypes(string cSortExpression);




//      /////////////////////////////////////////////////////////
//      // methods that work with LicenseTypes
//      public abstract int GetLicenseTypeCount();
//      public abstract List<LicenseTypeInfo> GetLicenseTypes(string cSortExpression);
//      public abstract LicenseTypeInfo GetLicenseTypeByID(int License_Type_ID);
//      public abstract bool DeleteLicenseType(int License_Type_ID);
//      public abstract bool UpdateLicenseType(LicenseTypeInfo LicenseType);
//      public abstract int InsertLicenseType(LicenseTypeInfo LicenseType);

//      /// <summary>
//      /// Returns a new LicenseTypeInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual LicenseTypeInfo GetLicenseTypeFromReader(IDataReader reader)
//      {
//          return GetLicenseTypeFromReader(reader, true);
//      }
//      protected virtual LicenseTypeInfo GetLicenseTypeFromReader(IDataReader reader, bool readMemos)
//      {
//          LicenseTypeInfo LicenseType = new LicenseTypeInfo(
//            (int)reader["License_Type_ID"],
//            reader["Description"].ToString());


//          return LicenseType;
//      }

//      /// <summary>
//      /// Returns a collection of LicenseTypeInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<LicenseTypeInfo> GetLicenseTypeCollectionFromReader(IDataReader reader)
//      {
//          return GetLicenseTypeCollectionFromReader(reader, true);
//      }
//      protected virtual List<LicenseTypeInfo> GetLicenseTypeCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<LicenseTypeInfo> LicenseTypes = new List<LicenseTypeInfo>();
//          while (reader.Read())
//              LicenseTypes.Add(GetLicenseTypeFromReader(reader, readMemos));
//          return LicenseTypes;
//      }

//      /// <summary>
//      /// Returns a collection of AreaTypeInfo objects with the data read from the input DataReader
//      /// </summary>
//      /// 

//      protected virtual AreaTypeInfo GetAreaTypeFromReader(IDataReader reader)
//      {
//          return GetAreaTypeFromReader(reader, true);
//      }
//      protected virtual AreaTypeInfo GetAreaTypeFromReader(IDataReader reader, bool readMemos)
//      {
//          AreaTypeInfo AreaType = new AreaTypeInfo(
//            (int)reader["areaid"],
//            reader["areaname"].ToString(),
//            reader["tabname"].ToString());
          
//          return AreaType;
//      }

//      protected virtual List<AreaTypeInfo> GetAreaTypeCollectionFromReader(IDataReader reader)
//      {
//          return GetAreaTypeCollectionFromReader(reader, true);
//      }
//      protected virtual List<AreaTypeInfo> GetAreaTypeCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<AreaTypeInfo> AreaTypes = new List<AreaTypeInfo>();
//          while (reader.Read())
//              AreaTypes.Add(GetAreaTypeFromReader(reader, readMemos));
//          return AreaTypes;
//      }



//      /////////////////////////////////////////////////////////
//      // methods that work with TopicPages
//      public abstract int GetTopicPageCount();
//      public abstract int GetTopicPageCountByTopicID(int TopicID);
//      public abstract List<TopicPageInfo> GetTopicPages(string cSortExpression);
//      public abstract List<TopicPageInfo> GetTopicPagesByTopicID(int TopicID, string cSortExpression);
//      public abstract TopicPageInfo GetTopicPageByTopicIDAndPageNum(int TopicID, int Page_Num);
//      public abstract TopicPageInfo GetTopicPageByID(int PageID);
//      public abstract bool DeleteTopicPage(int PageID);
//      public abstract bool UpdateTopicPage(TopicPageInfo TopicPage);
//      public abstract int InsertTopicPage(TopicPageInfo TopicPage);

//      /// <summary>
//      /// Returns a new TopicPageInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual TopicPageInfo GetTopicPageFromReader(IDataReader reader)
//      {
//          return GetTopicPageFromReader(reader, true);
//      }
//      protected virtual TopicPageInfo GetTopicPageFromReader(IDataReader reader, bool readMemos)
//      {
//          TopicPageInfo TopicPage = new TopicPageInfo(
//            (int)reader["PageID"],
//            (int)reader["TopicID"],
//            (int)reader["Page_Num"],
//            reader["Page_Content"].ToString());


//          return TopicPage;
//      }

//      /// <summary>
//      /// Returns a collection of TopicPageInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<TopicPageInfo> GetTopicPageCollectionFromReader(IDataReader reader)
//      {
//          return GetTopicPageCollectionFromReader(reader, true);
//      }
//      protected virtual List<TopicPageInfo> GetTopicPageCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<TopicPageInfo> TopicPages = new List<TopicPageInfo>();
//          while (reader.Read())
//              TopicPages.Add(GetTopicPageFromReader(reader, readMemos));
//          return TopicPages;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with Promotions
//      public abstract int GetPromotionCount();
//      public abstract List<PromotionInfo> GetPromotions(string cSortExpression);
//      public abstract PromotionInfo GetPromotionByID(int ID);
//      public abstract bool DeletePromotion(int ID);
//      public abstract bool UpdatePromotion(PromotionInfo Promotion);
//      public abstract int InsertPromotion(PromotionInfo Promotion);

//      /// <summary>
//      /// Returns a new PromotionInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual PromotionInfo GetPromotionFromReader(IDataReader reader)
//      {
//          return GetPromotionFromReader(reader, true);
//      }
//      protected virtual PromotionInfo GetPromotionFromReader(IDataReader reader, bool readMemos)
//      {
//          PromotionInfo Promotion = new PromotionInfo(
//            (int)reader["ID"],
//            reader["cCode"].ToString(),
//            reader["PromoType"].ToString(),
//            reader["PromoName"].ToString(),
//            (bool)reader["lActive"],
//            (bool)reader["lOneTime"],
//            (decimal)reader["nDollars"],
//            reader["Notes"].ToString(),
//            (decimal)reader["nPercent"],
//            (decimal)reader["nUnits"],
//            (DateTime)reader["tExpires"],
//            (DateTime)reader["tStart"],
//            (int)(Convert.IsDBNull(reader["RepID"]) ? (int)0 : (int)reader["RepID"]) );

//          return Promotion;
//      }

//      /// <summary>
//      /// Returns a collection of PromotionInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<PromotionInfo> GetPromotionCollectionFromReader(IDataReader reader)
//      {
//          return GetPromotionCollectionFromReader(reader, true);
//      }
//      protected virtual List<PromotionInfo> GetPromotionCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<PromotionInfo> Promotions = new List<PromotionInfo>();
//          while (reader.Read())
//              Promotions.Add(GetPromotionFromReader(reader, readMemos));
//          return Promotions;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with UserAccounts
//      public abstract int GetUserAccountCount();
//      public abstract int GetUserIDByUserName(string UserName);
//      public abstract int GetUserIDByShortUserNameAndFacilityID(string ShortUserName, int FacilityID);
//      public abstract List<UserAccountInfo> GetUserAccounts(string cSortExpression);
//      public abstract List<UserAccountInfo> GetRecentUserAccounts();
//      public abstract UserAccountInfo GetUserAccountByID(int iID);
//      public abstract UserAccountInfo GetUserAccountByShortUserNameAndFacilityID(string ShortUserName, int FacilityID);
//      public abstract bool DeleteUserAccount(int iID);
//      public abstract bool UpdateUserAccount(UserAccountInfo UserAccount);
//      public abstract int InsertUserAccount(UserAccountInfo UserAccount);

//      /// <summary>
//      /// Returns a new UserAccountInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual UserAccountInfo GetUserAccountFromReader(IDataReader reader)
//      {
//          return GetUserAccountFromReader(reader, true);
//      }
//      protected virtual UserAccountInfo GetUserAccountFromReader(IDataReader reader, bool readMemos)
//      {
//          UserAccountInfo UserAccount = new UserAccountInfo(
//            (int)reader["iID"],
//            reader["cAddress1"].ToString(),
//            reader["cAddress2"].ToString(),
//            (bool)reader["lAdmin"],
//            (bool)reader["lPrimary"],
//            reader["cBirthDate"].ToString(),
//            reader["cCity"].ToString(),
//            reader["cEmail"].ToString(),
//            reader["cExpires"].ToString(),
//            reader["cFirstName"].ToString(),
//            reader["cFloridaNo"].ToString(),
//            reader["cInstitute"].ToString(),
//            reader["cLastName"].ToString(),
//            reader["cLectDate"].ToString(),
//            reader["cMiddle"].ToString(),
//            reader["cPhone"].ToString(),
//            reader["cPromoCode"].ToString(),
//            (int)(Convert.IsDBNull(reader["PromoID"]) ? (int)0 : (int)reader["PromoID"]),
//            (int)(Convert.IsDBNull(reader["FacilityID"]) ? (int)0 : (int)reader["FacilityID"]),
//            reader["cPW"].ToString(),
//            reader["cRefCode"].ToString(),
//            reader["cRegType"].ToString(),
//            reader["cSocial"].ToString(),
//            reader["cState"].ToString(),
//            reader["cUserName"].ToString().ToLower(),
//            reader["cZipCode"].ToString(),
//            (int)(Convert.IsDBNull(reader["SpecID"]) ? (int)0 : (int)reader["SpecID"]),
//            reader["LCUserName"].ToString().ToLower(),
//            (DateTime)(Convert.IsDBNull(reader["LastAct"]) 
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastAct"]),
//            reader["PassFmt"].ToString(),
//            reader["PassSalt"].ToString(),
//            reader["MobilePIN"].ToString(),
//            reader["LCEmail"].ToString(),
//            reader["PWQuest"].ToString(),
//            reader["PWAns"].ToString(),
//            (bool)reader["IsApproved"],
//            (bool)reader["IsOnline"],
//            (bool)reader["IsLocked"],
//            (DateTime)(Convert.IsDBNull(reader["LastLogin"]) 
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLogin"]),
//            (DateTime)(Convert.IsDBNull(reader["LastPWChg"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastPWChg"]),
//           (DateTime)(Convert.IsDBNull(reader["LastLock"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLock"]),
//            (int)reader["XPWAtt"],
//            (DateTime)(Convert.IsDBNull(reader["XPWAttSt"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAttSt"]),
//            (int)reader["XPWAnsAtt"],
//            (DateTime)(Convert.IsDBNull(reader["XPWAnsSt"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAnsSt"]),
//            reader["Comment"].ToString(),
//            (DateTime)(Convert.IsDBNull(reader["Created"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Created"]),
//            (int)(Convert.IsDBNull(reader["AgeGroup"]) ? (int)0 : (int)reader["AgeGroup"]),
//            (int)(Convert.IsDBNull(reader["RegTypeID"]) ? (int)0 : (int)reader["RegTypeID"]),
//            (int)(Convert.IsDBNull(reader["RepID"]) ? (int)0 : (int)reader["RepID"]) ,
//            reader["Work_phone"].ToString(),
//            reader["Badge_ID"].ToString(),
//            (DateTime)(Convert.IsDBNull(reader["Hire_Date"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Hire_Date"]),
//            (DateTime)(Convert.IsDBNull(reader["Termin_Date"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Termin_Date"]),
//            (int)(Convert.IsDBNull(reader["dept_ID"]) ? (int)0 : (int)reader["dept_ID"]) ,
//            (int)(Convert.IsDBNull(reader["Position_ID"]) ? (int)0 : (int)reader["Position_ID"]),
//            (int)(Convert.IsDBNull(reader["Clinical_Ind"]) ? 0 : Convert.ToInt32(reader["Clinical_Ind"])),
//            reader["Title"].ToString(),
//            (bool)(Convert.IsDBNull(reader["Giftcard_ind"]) ? false : Convert.ToBoolean(reader["Giftcard_Ind"])),
//            (decimal)(Convert.IsDBNull(reader["Giftcard_Chour"]) ? (decimal)0 : (decimal)reader["Giftcard_Chour"]),
//            (decimal)(Convert.IsDBNull(reader["Giftcard_Uhour"]) ? (decimal)0 : (decimal)reader["Giftcard_Uhour"]),
//            (int)(Convert.IsDBNull(reader["UniqueID"]) ? (int)0 : (int)reader["UniqueID"]),
//            (bool)reader["FirstLogin_Ind"] );
             

//          return UserAccount;
//      }

//      /// <summary>
//      /// Returns a collection of UserAccountInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<UserAccountInfo> GetUserAccountCollectionFromReader(IDataReader reader)
//      {
//          return GetUserAccountCollectionFromReader(reader, true);
//      }
//      protected virtual List<UserAccountInfo> GetUserAccountCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<UserAccountInfo> UserAccounts = new List<UserAccountInfo>();
//          while (reader.Read())
//              UserAccounts.Add(GetUserAccountFromReader(reader, readMemos));
//          return UserAccounts;
//      }

//       public abstract int GetUserAccountCountByPromoCode(string cPromoCode);
//       public abstract List<UserAccountInfo> GetUserAccountsByPromoCode(string cPromoCode, string cSortExpression);
//       public abstract List<UserAccountInfo> GetUserAccountsByLastName(string cLastName, string cSortExpression);
//       public abstract List<UserAccountInfo> GetUserAccountsByShortUserName(string ShortUserName, string cSortExpression);
//       public abstract List<UserAccountInfo> GetUserAccountsByEmail(string cEmail, string cSortExpression);
//       public abstract int GetUserAccountCountByPromoID(int PromoID);
//       public abstract List<UserAccountInfo> GetUserAccountsByPromoID(int PromoID, string cSortExpression);
//       public abstract int GetUserAccountCountByFacilityID(int FacilityID);
//       public abstract List<UserAccountInfo> GetUserAccountsByFacilityID(int FacilityID, string cSortExpression);
//       public abstract List<UserAccountInfo> GetAdminUserAccountsByFacilityID(int FacilityID, string cSortExpression);
//       public abstract bool UpdateUserAccountAdminStatus(int iID, bool lAdmin);

//       //method to check the uniqueness of gift card user
//       public abstract bool GetGiftCardUsersFromUserAccountInfoList(string cGiftUsername);

//      /////////////////////////////////////////////////////////
//      // methods that work with RegistrationTypes
//      public abstract int GetRegistrationTypeCount();
//      public abstract List<RegistrationTypeInfo> GetRegistrationTypes(string cSortExpression);
//      public abstract RegistrationTypeInfo GetRegistrationTypeByID(int RegTypeID);
//      public abstract bool DeleteRegistrationType(int RegTypeID);
//      public abstract bool UpdateRegistrationType(RegistrationTypeInfo RegistrationType);
//      public abstract int InsertRegistrationType(RegistrationTypeInfo RegistrationType);

//      /// <summary>
//      /// Returns a new RegistrationTypeInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual RegistrationTypeInfo GetRegistrationTypeFromReader(IDataReader reader)
//      {
//          return GetRegistrationTypeFromReader(reader, true);
//      }
//      protected virtual RegistrationTypeInfo GetRegistrationTypeFromReader(IDataReader reader, bool readMemos)
//      {
//          RegistrationTypeInfo RegistrationType = new RegistrationTypeInfo(
//            (int)reader["RegTypeID"],
//            reader["RegType"].ToString());


//          return RegistrationType;
//      }

//      /// <summary>
//      /// Returns a collection of RegistrationTypeInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<RegistrationTypeInfo> GetRegistrationTypeCollectionFromReader(IDataReader reader)
//      {
//          return GetRegistrationTypeCollectionFromReader(reader, true);
//      }
//      protected virtual List<RegistrationTypeInfo> GetRegistrationTypeCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<RegistrationTypeInfo> RegistrationTypes = new List<RegistrationTypeInfo>();
//          while (reader.Read())
//              RegistrationTypes.Add(GetRegistrationTypeFromReader(reader, readMemos));
//          return RegistrationTypes;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with ResourcePersons
//      public abstract int GetResourcePersonCount();
//      public abstract List<ResourcePersonInfo> GetResourcePersons(string cSortExpression);
//      public abstract List<ResourcePersonInfo> GetResourcePersonsByTopicIDAndType(int TopicID, 
//            string ResourceType, string cSortExpression);

//      public abstract ResourcePersonInfo GetResourcePersonByID(int ResourceID);
//      public abstract List<ResourcePersonInfo> GetResourcePersonByFacilityID(int FacilityID);
//      public abstract bool DeleteResourcePerson(int ResourceID);
//      public abstract bool UpdateResourcePerson(ResourcePersonInfo ResourcePerson);
//      public abstract int InsertResourcePerson(ResourcePersonInfo ResourcePerson);

//      /// <summary>
//      /// Returns a new ResourcePersonInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual ResourcePersonInfo GetResourcePersonFromReader(IDataReader reader)
//      {
//          return GetResourcePersonFromReader(reader, true);
//      }
//      protected virtual ResourcePersonInfo GetResourcePersonFromReader(IDataReader reader, bool readMemos)
//      {
//          // NOTE: We are forcing a default value for the Disclosure statement if blank in the database
//          ResourcePersonInfo ResourcePerson = new ResourcePersonInfo(
//            (int)reader["ResourceID"],
//            reader["Fullname"].ToString(),
//            reader["LastFirst"].ToString(),
//            reader["Address"].ToString(),
//            reader["City"].ToString(),
//            reader["State"].ToString(),
//            reader["Zip"].ToString(),
//            reader["Country"].ToString(),
//            reader["Phone"].ToString(),
//            reader["Email"].ToString(),
//            reader["SSN"].ToString(),
//            reader["Specialty"].ToString(),
//            reader["OtherNotes"].ToString(),
//            (bool)reader["ContrRecd"],
//            (Convert.IsDBNull(reader["DateDiscl"]) 
//                ? "" : ((DateTime)reader["DateDiscl"]).ToShortDateString() ),
//            ( reader["Disclosure"].ToString().Trim().Length == 0 
//                ? "I have nothing to disclose." : reader["Disclosure"].ToString() ),
//            reader["StateLic"].ToString(),
//            reader["Degrees"].ToString(),
//            reader["Position"].ToString(),
//            reader["Experience"].ToString(),
//            (int)reader["FacilityID"]);

//            //(DateTime)(Convert.IsDBNull(reader["DateDiscl"]) 
//            //    ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["DateDiscl"]),


//          return ResourcePerson;
//      }

//      /// <summary>
//      /// Returns a collection of ResourcePersonInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<ResourcePersonInfo> GetResourcePersonCollectionFromReader(IDataReader reader)
//      {
//          return GetResourcePersonCollectionFromReader(reader, true);
//      }
//      protected virtual List<ResourcePersonInfo> GetResourcePersonCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<ResourcePersonInfo> ResourcePersons = new List<ResourcePersonInfo>();
//          while (reader.Read())
//              ResourcePersons.Add(GetResourcePersonFromReader(reader, readMemos));
//          return ResourcePersons;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with Roles
//      public abstract int GetRoleCount();
//      public abstract List<RoleInfo> GetRoles(string cSortExpression);
//      public abstract RoleInfo GetRoleByID(int RoleID);
//      public abstract bool DeleteRole(int RoleID);
//      public abstract bool UpdateRole(RoleInfo Role);
//      public abstract int InsertRole(RoleInfo Role);

//      /// <summary>
//      /// Returns a new RoleInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual RoleInfo GetRoleFromReader(IDataReader reader)
//      {
//          return GetRoleFromReader(reader, true);
//      }
//      protected virtual RoleInfo GetRoleFromReader(IDataReader reader, bool readMemos)
//      {
//          RoleInfo Role = new RoleInfo(
//            (int)reader["RoleID"],
//            reader["RoleName"].ToString());


//          return Role;
//      }

//      /// <summary>
//      /// Returns a collection of RoleInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<RoleInfo> GetRoleCollectionFromReader(IDataReader reader)
//      {
//          return GetRoleCollectionFromReader(reader, true);
//      }
//      protected virtual List<RoleInfo> GetRoleCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<RoleInfo> Roles = new List<RoleInfo>();
//          while (reader.Read())
//              Roles.Add(GetRoleFromReader(reader, readMemos));
//          return Roles;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with SalesReps
//      public abstract int GetSalesRepCount();
//      public abstract List<SalesRepInfo> GetSalesReps(string cSortExpression);
//      public abstract SalesRepInfo GetSalesRepByID(int RepID);
//      public abstract bool DeleteSalesRep(int RepID);
//      public abstract bool UpdateSalesRep(SalesRepInfo SalesRep);
//      public abstract int InsertSalesRep(SalesRepInfo SalesRep);

//      /// <summary>
//      /// Returns a new SalesRepInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual SalesRepInfo GetSalesRepFromReader(IDataReader reader)
//      {
//          return GetSalesRepFromReader(reader, true);
//      }
//      protected virtual SalesRepInfo GetSalesRepFromReader(IDataReader reader, bool readMemos)
//      {
//          SalesRepInfo SalesRep = new SalesRepInfo(
//            (int)reader["RepID"],
//            reader["RepCode"].ToString(),
//            reader["RepName"].ToString(),
//            reader["RepComment"].ToString());


//          return SalesRep;
//      }

//      /// <summary>
//      /// Returns a collection of SalesRepInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<SalesRepInfo> GetSalesRepCollectionFromReader(IDataReader reader)
//      {
//          return GetSalesRepCollectionFromReader(reader, true);
//      }
//      protected virtual List<SalesRepInfo> GetSalesRepCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<SalesRepInfo> SalesReps = new List<SalesRepInfo>();
//          while (reader.Read())
//              SalesReps.Add(GetSalesRepFromReader(reader, readMemos));
//          return SalesReps;
//      }

//      /////////////////////////////////////////////////////////
//      // methods that work with Sidebars
//      public abstract int GetSidebarCount();
//      public abstract List<SidebarInfo> GetSidebars(string cSortExpression);
//      public abstract SidebarInfo GetSidebarByID(int TestDefID);
//      public abstract List<SidebarInfo> GetSidebarsByTopicID(int TopicID, string cSortExpression);
//      public abstract int GetSidebarCountByTopicID(int TopicID);
//      public abstract bool DeleteSidebar(int TestDefID);
//      public abstract bool UpdateSidebar(SidebarInfo Sidebar);
//      public abstract int InsertSidebar(SidebarInfo Sidebar);

      


//      /// <summary>
//      /// Returns a new SidebarInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual SidebarInfo GetSidebarFromReader(IDataReader reader)
//      {
//          return GetSidebarFromReader(reader, true);
//      }
//      protected virtual SidebarInfo GetSidebarFromReader(IDataReader reader, bool readMemos)
//      {
//          SidebarInfo Sidebar = new SidebarInfo(
//            (int)reader["SB_ID"],
//            (int)reader["TopicID"],
//            reader["Title"].ToString(),
//            reader["Body"].ToString());

//          return Sidebar;
//      }

//      /// <summary>
//      /// Returns a collection of SidebarInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<SidebarInfo> GetSidebarCollectionFromReader(IDataReader reader)
//      {
//          return GetSidebarCollectionFromReader(reader, true);
//      }
//      protected virtual List<SidebarInfo> GetSidebarCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<SidebarInfo> Sidebars = new List<SidebarInfo>();
//          while (reader.Read())
//              Sidebars.Add(GetSidebarFromReader(reader, readMemos));
//          return Sidebars;
//      }





//      /////////////////////////////////////////////////////////
//      // methods that work with Snippets
//      public abstract int GetSnippetCount();
//      public abstract List<SnippetInfo> GetSnippets(string cSortExpression);
//      public abstract SnippetInfo GetSnippetByID(int SnippetID);
//      public abstract SnippetInfo GetSnippetBySnippetKey(string SnippetKey);
//      public abstract bool DeleteSnippet(int SnippetID);
//      public abstract bool UpdateSnippet(SnippetInfo Snippet);
//      public abstract int InsertSnippet(SnippetInfo Snippet);

//      /// <summary>
//      /// Returns a new SnippetInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual SnippetInfo GetSnippetFromReader(IDataReader reader)
//      {
//          return GetSnippetFromReader(reader, true);
//      }
//      protected virtual SnippetInfo GetSnippetFromReader(IDataReader reader, bool readMemos)
//      {
//          SnippetInfo Snippet = new SnippetInfo(
//            (int)reader["SnippetID"],
//            reader["SnippetKey"].ToString(),
//            reader["SnippetText"].ToString());


//          return Snippet;
//      }

//      /// <summary>
//      /// Returns a collection of SnippetInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<SnippetInfo> GetSnippetCollectionFromReader(IDataReader reader)
//      {
//          return GetSnippetCollectionFromReader(reader, true);
//      }
//      protected virtual List<SnippetInfo> GetSnippetCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<SnippetInfo> Snippets = new List<SnippetInfo>();
//          while (reader.Read())
//              Snippets.Add(GetSnippetFromReader(reader, readMemos));
//          return Snippets;
//      }

//      /////////////////////////////////////////////////////////
//      // methods that work with Specialties
//      public abstract int GetSpecialtyCount();
//      public abstract List<SpecialtyInfo> GetSpecialties(string cSortExpression);
//      public abstract SpecialtyInfo GetSpecialtyByID(int SpecID);
//      public abstract bool DeleteSpecialty(int SpecID);
//      public abstract bool UpdateSpecialty(SpecialtyInfo Specialty);
//      public abstract int InsertSpecialty(SpecialtyInfo Specialty);

//      /// <summary>
//      /// Returns a new SpecialtyInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual SpecialtyInfo GetSpecialtyFromReader(IDataReader reader)
//      {
//          return GetSpecialtyFromReader(reader, true);
//      }
//      protected virtual SpecialtyInfo GetSpecialtyFromReader(IDataReader reader, bool readMemos)
//      {
//          SpecialtyInfo Specialty = new SpecialtyInfo(
//            (int)reader["SpecID"],
//            reader["SpecTitle"].ToString());


//          return Specialty;
//      }

//      /// <summary>
//      /// Returns a collection of SpecialtyInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<SpecialtyInfo> GetSpecialtyCollectionFromReader(IDataReader reader)
//      {
//          return GetSpecialtyCollectionFromReader(reader, true);
//      }
//      protected virtual List<SpecialtyInfo> GetSpecialtyCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<SpecialtyInfo> Specialties = new List<SpecialtyInfo>();
//          while (reader.Read())
//              Specialties.Add(GetSpecialtyFromReader(reader, readMemos));
//          return Specialties;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with SurveyDefinitions
//      public abstract int GetSurveyDefinitionCount();
//      public abstract List<SurveyDefinitionInfo> GetSurveyDefinitions(string cSortExpression);
//      public abstract SurveyDefinitionInfo GetSurveyDefinitionByID(int SurveyID);
//      public abstract SurveyDefinitionInfo GetSurveyDefinitionByTopicID(int TopicID); //Bhaskar
//      public abstract bool DeleteSurveyDefinition(int SurveyID);
//      public abstract bool UpdateSurveyDefinition(SurveyDefinitionInfo SurveyDefinition);
//      public abstract bool UpdateSurveyDefinition(int SurveyID, string XMLSurvey, int Version, int FacilityID);
//      public abstract int InsertSurveyDefinition(SurveyDefinitionInfo SurveyDefinition);

//      /// <summary>
//      /// Returns a new SurveyDefinitionInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual SurveyDefinitionInfo GetSurveyDefinitionFromReader(IDataReader reader)
//      {
//          return GetSurveyDefinitionFromReader(reader, true);
//      }
//      protected virtual SurveyDefinitionInfo GetSurveyDefinitionFromReader(IDataReader reader, bool readMemos)
//      {
//          SurveyDefinitionInfo SurveyDefinition = new SurveyDefinitionInfo(
//            (int)reader["SurveyID"],
//            reader["SurveyName"].ToString(),
//            reader["XMLSurvey"].ToString(),
//            (int)reader["Version"],
//            (int)reader["FacilityID"]);


//          return SurveyDefinition;
//      }

//      /// <summary>
//      /// Returns a collection of SurveyDefinitionInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<SurveyDefinitionInfo> GetSurveyDefinitionCollectionFromReader(IDataReader reader)
//      {
//          return GetSurveyDefinitionCollectionFromReader(reader, true);
//      }
//      protected virtual List<SurveyDefinitionInfo> GetSurveyDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<SurveyDefinitionInfo> SurveyDefinitions = new List<SurveyDefinitionInfo>();
//          while (reader.Read())
//              SurveyDefinitions.Add(GetSurveyDefinitionFromReader(reader, readMemos));
//          return SurveyDefinitions;
//      }


//       /////////////////////////////////////////////////////////
//       //methods that work with NewGiftCardInfo

//      public abstract int InsertNewGiftcard(NewGiftcardInfo NewGiftcard);
//      public abstract List<NewGiftcardInfo> GetNewGiftcardByGcNumber(string GcNumber);
//      protected virtual List<NewGiftcardInfo> GetNewGiftcardCollectionFromReader(IDataReader reader)
//      {
//          return GetNewGiftcardCollectionFromReader(reader, true);
//      }
//      protected virtual List<NewGiftcardInfo> GetNewGiftcardCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<NewGiftcardInfo> NewGiftcards = new List<NewGiftcardInfo>();
//          while (reader.Read())
//              NewGiftcards.Add(GetNewGiftcardFromReader(reader, readMemos));
//          return NewGiftcards;
//      }
//      protected virtual NewGiftcardInfo GetNewGiftcardFromReader(IDataReader reader)
//      {
//          return GetNewGiftcardFromReader(reader, true);
//      }

//      protected virtual NewGiftcardInfo GetNewGiftcardFromReader(IDataReader reader, bool readMemos)
//      {
//          // NOTE: NULL integer foreign key LectEvtID is replaced with value of
//          // zero, which will trigger "Please select..." or "(Not Selected)"
//          // message in UI dropdown for that field
//          NewGiftcardInfo NewGiftcard = new NewGiftcardInfo(
//            (int)reader["GcID"],
//            reader["GcNumber"].ToString(),
//            (int)reader["GiftCard_CHour"],
//            (int)reader["FacilityID"],
//            reader["FacName"].ToString());

//          return NewGiftcard;
//      }


//     //////////////////////////////////////////////////////////
//       // methods that work with FacilityAreaLink

//      public abstract List<FacilityAreaLinkInfo> GetFacilityAreaLinkByFacilityId(int facilityid);
//      public abstract int UpdateFacilityAreaLink(FacilityAreaLinkInfo FacilityAreaLink);
//      protected virtual List<FacilityAreaLinkInfo> GetFacilityAreaLinkCollectionFromReader(IDataReader reader)
//      {
//          return GetFacilityAreaLinkCollectionFromReader(reader, true);
//      }
//      protected virtual List<FacilityAreaLinkInfo> GetFacilityAreaLinkCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<FacilityAreaLinkInfo> FacilityAreaLinks = new List<FacilityAreaLinkInfo>();
//          while (reader.Read())
//              FacilityAreaLinks.Add(GetFacilityAreaLinkFromReader(reader, readMemos));
//          return FacilityAreaLinks;
//      }
//      protected virtual FacilityAreaLinkInfo GetFacilityAreaLinkFromReader(IDataReader reader)
//      {
//          return GetFacilityAreaLinkFromReader(reader, true);
//      }
//      public abstract bool DeleteFacilityAreaLink(int facilityid,int areaid);
//      protected virtual FacilityAreaLinkInfo GetFacilityAreaLinkFromReader(IDataReader reader, bool readMemos)
//      {
//          // NOTE: NULL integer foreign key LectEvtID is replaced with value of
//          // zero, which will trigger "Please select..." or "(Not Selected)"
//          // message in UI dropdown for that field
//          FacilityAreaLinkInfo FacilityAreaLink = new FacilityAreaLinkInfo(
//            (int)reader["facilityid"],
//            (int)reader["areaid"]);

//          return FacilityAreaLink;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with Tests
//      public abstract int GetTestCount();
//      public abstract List<TestInfo> GetTests(string cSortExpression);
//      public abstract TestInfo GetTestByID(int TestID);
//      public abstract List<TestInfo> GetAllTestsByTopicId(int TopicId, string FromDate, string Todate, string SortExpression); //Bhaskar N
//      public abstract bool DeleteTest(int TestID);
//      public abstract bool UpdateTest(TestInfo Test);
//      public abstract bool UpdateTestTopicNameByTopicID(int TopicID, string TopicName);
//      public abstract int InsertTest(TestInfo Test);

//      /////////////////////////////////////////////////////////
//      // methods that work with Cart
//      public abstract int InsertCart(CartInfo Cart);
//      public abstract bool UpdateCart(CartInfo Cart);
//      public abstract int CheckAllOnline(int UserId);
//      public abstract Decimal GetCouponAmountByUserIdandCode(int UserId, string CouponCode);
//      public abstract DataSet GetCartPriceByUserId(int UserId);
//      public abstract DataSet GetUnlimCartPriceByUserId(int UserId);
//      public abstract Decimal GetCartPriceByCartId(int CartId);
//      public abstract int CheckNumberOfPharmacy(int UserId);
//      public abstract DataSet GetOrderPriceByUserId(int UserId);
//      public abstract DataSet GetUnlimOrderPriceByUserId(int UserId);
//      public abstract List<CartInfo> GetCartsByUserID(int UserID);
//      protected virtual CartInfo GetCartFromReader(IDataReader reader)
//      {
//          return GetCartFromReader(reader, true);
//      }
//      public abstract bool DeleteCart(int CartID);
//      public abstract List<CartInfo> GetIncompleteCartsByUserIDAndTopicID(int UserID, int TopicID);
//      public abstract List<CartInfo> GetcompleteCartsByUserIDAndTopicID(int UserID, int TopicID);
//      protected virtual CartInfo GetCartFromReader(IDataReader reader, bool readMemos)
//      {
//          // NOTE: NULL integer foreign key LectEvtID is replaced with value of
//          // zero, which will trigger "Please select..." or "(Not Selected)"
//          // message in UI dropdown for that field
//          CartInfo Cart = new CartInfo(
//            (int)reader["CartID"],
//            (int)reader["TopicID"],
//            (int)reader["UserID"],
//            reader["Media_Type"].ToString(),
//            (int)reader["Score"],
//            (DateTime)reader["Lastmod"],
//            reader["XmlTest"].ToString(),
//            reader["XmlSurvey"].ToString(),
//            (int)reader["Quantity"],
//            (int)reader["DiscountId"],
//            (bool)reader["Process_Ind"],
//            (bool)reader["Optin_Ind"]);
//                    return Cart;
//      }
//      public abstract CartInfo GetCartByID(int CartID);

//      /// <summary>
//      /// Returns a collection of TestInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<CartInfo> GetCartCollectionFromReader(IDataReader reader)
//      {
//          return GetCartCollectionFromReader(reader, true);
//      }
//      protected virtual List<CartInfo> GetCartCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<CartInfo> Carts = new List<CartInfo>();
//          while (reader.Read())
//              Carts.Add(GetCartFromReader(reader, readMemos));
//          return Carts;
//      }


//      /// <summary>
//      /// Returns a new TestInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual TestInfo GetTestFromReader(IDataReader reader)
//      {
//          return GetTestFromReader(reader, true);
//      }
//      protected virtual TestInfo GetTestFromReader(IDataReader reader, bool readMemos)
//      {
//          // NOTE: NULL integer foreign key LectEvtID is replaced with value of
//          // zero, which will trigger "Please select..." or "(Not Selected)"
//          // message in UI dropdown for that field
//          TestInfo Test = new TestInfo(
//            (int)reader["TestID"],
//            reader["OldTestID"].ToString(),
//            (int)reader["TopicID"],
//            (int)reader["UserID"],
//            (DateTime)reader["BuyDate"],
//            (decimal)reader["Credits"],
//            (DateTime)reader["LastMod"],
//            reader["Name"].ToString(),
//            (bool)reader["NoSurvey"],
//            reader["Status"].ToString(),
//            reader["UserName"].ToString(),
//            reader["XMLTest"].ToString(),
//            reader["XMLSurvey"].ToString(),
//            (int)reader["TestVers"],
//            (int)(Convert.IsDBNull(reader["LectEvtID"]) ? (int)0 : (int)reader["LectEvtID"]),
//            (bool)reader["SurveyComplete"],
//            (bool)reader["VignetteComplete"],
//            //reader["XMLVignette"].ToString(),
//            //(int)reader["VignetteVersion"],
//            //(int)(Convert.IsDBNull(reader["VignetteScore"]) ? (int)0 : (int)reader["VignetteScore"]),
//            (int)(Convert.IsDBNull(reader["Score"]) ? (int)0 : (int)reader["Score"]),
//            //(DateTime)(Convert.IsDBNull(reader["Printed_Date"])
//            //    ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Printed_Date"]),
//            //(DateTime)(Convert.IsDBNull(reader["Emailed_Date"])
//            //    ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Emailed_Date"]),
//            //(DateTime)(Convert.IsDBNull(reader["Reported_Date"])
//                //? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Reported_Date"]),
//            (DateTime)(Convert.IsDBNull(reader["View_Date"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["View_Date"]),
//            (int)(Convert.IsDBNull(reader["UniqueID"]) ? (int)0 : (int)reader["UniqueID"]),
//            (int)(Convert.IsDBNull(reader["AlterID"]) ? (int)0 : (int)reader["AlterID"]),
//            (bool)reader["GCCharged"]);

//          return Test;
//      }

//      /// <summary>
//      /// Returns a collection of TestInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<TestInfo> GetTestCollectionFromReader(IDataReader reader)
//      {
//          return GetTestCollectionFromReader(reader, true);
//      }
//      protected virtual List<TestInfo> GetTestCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<TestInfo> Tests = new List<TestInfo>();
//          while (reader.Read())
//              Tests.Add(GetTestFromReader(reader, readMemos));
//          return Tests;
//      }
//      public abstract int GetFacilityIDByTestID(int TestID);
//      public abstract int GetTestCountByTopicID(int TopicID);
//       public abstract int GetTestCountByUserName(string UserName);
//      public abstract List<TestInfo> GetTestsByUserName(string UserName);
//      public abstract List<TestInfo> GetIncompleteTestsByUserName(string UserName);
//      public abstract List<TestInfo> GetCompletedTestsByUserName(string UserName);
//      public abstract int GetTestCountByUserID(int UserID);
//      public abstract List<TestInfo> GetTestsByUserID(int UserID);
//      public abstract List<TestInfo> GetIncompleteTestsByUserID(int UserID);
//      public abstract List<TestInfo> GetIncompleteTestsByUserIDAndTopicID(int UserID, int TopicID);

//       public abstract System.Data.DataSet GetCompletedTestsByUserID(int UserID);
//       public abstract TestInfo GetCompletedTestByTestIDUserID(int UserID, int TestID);
//       public abstract System.Data.DataSet GetNeedSurveyTestsByUserID(int UserID);
//       public abstract int GetCompletedTestCountByFacilityID(int FacilityID);

//      /////////////////////////////////////////////////////////
//      // methods that work with TestDefinitions
//      public abstract int GetTestDefinitionCount();
//      public abstract List<TestDefinitionInfo> GetTestDefinitions(string cSortExpression);
//      public abstract TestDefinitionInfo GetTestDefinitionByID(int TestDefID);
//      public abstract TestDefinitionInfo GetTestDefinitionByTopicID(int TopicID);
//       public abstract bool DeleteTestDefinition(int TestDefID);
//      public abstract bool UpdateTestDefinition(TestDefinitionInfo TestDefinition);
//      public abstract int InsertTestDefinition(TestDefinitionInfo TestDefinition);

//      /// <summary>
//      /// Returns a new TestDefinitionInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual TestDefinitionInfo GetTestDefinitionFromReader(IDataReader reader)
//      {
//          return GetTestDefinitionFromReader(reader, true);
//      }
//      protected virtual TestDefinitionInfo GetTestDefinitionFromReader(IDataReader reader, bool readMemos)
//      {
//          TestDefinitionInfo TestDefinition = new TestDefinitionInfo(
//            (int)reader["TestDefID"],
//            (int)reader["TopicID"],
//            reader["XMLTest"].ToString(),
//            (int)reader["Version"]);


//          return TestDefinition;
//      }

//      /// <summary>
//      /// Returns a collection of TestDefinitionInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<TestDefinitionInfo> GetTestDefinitionCollectionFromReader(IDataReader reader)
//      {
//          return GetTestDefinitionCollectionFromReader(reader, true);
//      }
//      protected virtual List<TestDefinitionInfo> GetTestDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<TestDefinitionInfo> TestDefinitions = new List<TestDefinitionInfo>();
//          while (reader.Read())
//              TestDefinitions.Add(GetTestDefinitionFromReader(reader, readMemos));
//          return TestDefinitions;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with ViDefinitions
//      public abstract int GetViDefinitionCount();
//      public abstract List<ViDefinitionInfo> GetViDefinitions(string cSortExpression);
//      public abstract ViDefinitionInfo GetViDefinitionByID(int ViDefID);
//      public abstract ViDefinitionInfo GetViDefinitionByTopicID(int TopicID);
//      public abstract bool DeleteViDefinition(int ViDefID);
//      public abstract bool UpdateViDefinition(ViDefinitionInfo ViDefinition);
//      public abstract int InsertViDefinition(ViDefinitionInfo ViDefinition);

//      /// <summary>
//      /// Returns a new ViDefinitionInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual ViDefinitionInfo GetViDefinitionFromReader(IDataReader reader)
//      {
//          return GetViDefinitionFromReader(reader, true);
//      }
//      protected virtual ViDefinitionInfo GetViDefinitionFromReader(IDataReader reader, bool readMemos)
//      {
//          ViDefinitionInfo ViDefinition = new ViDefinitionInfo(
//            (int)reader["ViDefID"],
//            (int)reader["TopicID"],
//            reader["XMLTest"].ToString(),
//            reader["Vignette_Desc"].ToString(),
//            (int)reader["Version"]);


//          return ViDefinition;
//      }

//      /// <summary>
//      /// Returns a collection of ViDefinitionInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<ViDefinitionInfo> GetViDefinitionCollectionFromReader(IDataReader reader)
//      {
//          return GetViDefinitionCollectionFromReader(reader, true);
//      }
//      protected virtual List<ViDefinitionInfo> GetViDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<ViDefinitionInfo> ViDefinitions = new List<ViDefinitionInfo>();
//          while (reader.Read())
//              ViDefinitions.Add(GetViDefinitionFromReader(reader, readMemos));
//          return ViDefinitions;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with Topics
//      public abstract int GetTopicCount();
//      public abstract int GetTextbookTopicCount();
//      public abstract int GetNonTextbookTopicCount();
//      public abstract int GetObsoleteTopicCount();
//      public abstract int GetObsoleteTextbookTopicCount();
//      public abstract int GetObsoleteNonTextbookTopicCount();
//      public abstract List<TopicInfo> GetTopics(string cSortExpression);
//      public abstract List<TopicInfo> GetFeaturedTopics(string cSortExpression);
//      public abstract List<TopicInfo> GetAudioTopics(string cSortExpression);
//      public abstract List<TopicInfo> GetFeaturedTopicsByFacilityId(int facilityid, string cSortExpression);
//      public abstract List<TopicInfo> GetAllChaptersByTopicId(int topicId); //Bhaskar N

//      public abstract List<TopicInfo> GetTextbookTopics(string cSortExpression);
//      public abstract List<TopicInfo> GetNonTextbookTopics(string cSortExpression);
//      public abstract List<TopicInfo> GetNonTextbookTopicsByFacilityId(int facilityid, string cSortExpression);
//      public abstract List<TopicInfo> GetAnthologyTopicsByTopicId(int TopicID, string cSortExpression);
//      public abstract List<TopicInfo> GetObsoleteTopics(string cSortExpression);
//      public abstract List<TopicInfo> GetFacilityTopics();
//      public abstract List<TopicInfo> GetObsoleteTextbookTopics(string cSortExpression);
//      public abstract List<TopicInfo> GetTopicsByType(string CourseNumber, string Title, string category, string active, string cSortExpression);
//      public abstract List<TopicInfo> GetObsoleteNonTextbookTopics(string cSortExpression);
//      public abstract List<TopicInfo> GetComplianceTopics(string cSortExpression);
//      public abstract TopicInfo GetTopicByID(int TopicID);
//      public abstract TopicInfo GetTopicByCourseNumber(string Course_Number); //Bsk
//      public abstract TopicInfo GetTopicByUserName(string UserName); //Bsk
//      public abstract int GetSectionDiscountIdByTopicId(int TopicID);
//      public abstract bool DeleteTopic(int TopicID);
//      public abstract bool UpdateTopic(TopicInfo Topic);
//      public abstract bool UpdateTopicActiveStatus(bool status, int topicid);
//      public abstract int UpdateTopicInProgressByUserID(int UserID, int TopicID);
//      public abstract int InsertTopic(TopicInfo Topic);
//      public abstract DataSet GetRecommendationTopicsByTopicId(int TopicID);
//      public abstract int IsPTOTTopic(int TopicId);

      

//      /// <summary>
//      /// Returns a new TopicInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual TopicInfo GetTopicFromReader(IDataReader reader)
//      {
//          return GetTopicFromReader(reader, true);
//      }
//      protected virtual TopicInfo GetTopicFromReader(IDataReader reader, bool readMemos)
//      {
//          // NOTE: NULL integer foreign key SurveyID is replaced with value of
//          // zero, which will trigger "Please select..." or "(Not Selected)" or "(None)"
//          // message in UI dropdown for that field
//          // NOTE: CertID is a required FK value, so null is not allowed.
//          //TopicInfo Topic = new TopicInfo(
//          //  (bool)reader["CorLecture"],
//          //  reader["Fla_CEType"].ToString(),
//          //  reader["Fla_IDNo"].ToString(),
//          //  reader["FolderName"].ToString(),
//          //  reader["HTML"].ToString(),
//          //  reader["LastUpdate"].ToString(),
//          //  (bool)reader["NoSurvey"],
//          //  (int)reader["TopicID"],
//          //  reader["TopicName"].ToString(),
//          //  (int)(Convert.IsDBNull(reader["SurveyID"]) ? (int)0 : (int)reader["SurveyID"]),
//          //  (bool)reader["Compliance"],
//          //  reader["MetaKW"].ToString(),
//          //  reader["MetaDesc"].ToString(),
//          //  (decimal)reader["Hours"],
//          //  reader["MediaType"].ToString(),
//          //  reader["Objectives"].ToString(),
//          //  reader["Content"].ToString(),
//          //  (bool)reader["Textbook"],
//          //  (int)reader["CertID"],
//          //  reader["Method"].ToString(),
//          //  reader["GrantBy"].ToString(),
//          //  reader["DOCXFile"].ToString(),
//          //  reader["DOCXHoFile"].ToString(),
//          //  (bool)reader["Obsolete"],
//          //  (int)reader["FacilityID"],
//          //  reader["Course_Number"].ToString(),
//          //  reader["CeRef"].ToString(),
//          //  reader["Sidebar_Html"].ToString(),
//          //  reader["Cert_Text"].ToString(),
//          //  (DateTime)(Convert.IsDBNull(reader["Release_Date"])
//          //      ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Release_Date"]),
//          //  (int)reader["Minutes"],
//          //  (bool)reader["Audio_Ind"],
//          //  (bool)reader["Apn_Ind"],
//          //  (bool)reader["Icn_Ind"],
//          //  (bool)reader["Jcaho_Ind"],
//          //  (bool)reader["Magnet_Ind"],
//          //  (bool)reader["Active_Ind"],
//          //  (bool)reader["Video_Ind"],
//          //  (bool)reader["Online_Ind"],
//          //  (bool)reader["Ebp_Ind"],
//          //  (bool)reader["Ccm_Ind"],
//          //  (bool)reader["Avail_Ind"],
//          //  reader["Cert_Cerp"].ToString(),
//          //  reader["Ref_Html"].ToString(),
//          //  reader["Vignette_Name"].ToString(),
//          //  reader["Vignette_Html"].ToString(),
//          //  reader["Topic_Xml"].ToString(),
//          //  (int)reader["AlterID"],
//          //  (int)reader["Pass_Score"],
//          //  reader["Subtitle"].ToString(),
//          //  (decimal)reader["Cost"],
//          //  reader["Topic_Type"].ToString(),
//          //  reader["Img_Name"].ToString(),
//          //  reader["Img_Credit"].ToString(),
//          //  reader["Img_Caption"].ToString(),
//          //  reader["Accreditation"].ToString(),
//          //  (int)reader["Views"],
//          //  (int)reader["PrimaryViews"],
//          //  (int)reader["PageReads"],
//          //  (bool)reader["Featured"] );


//          TopicInfo Topic = new TopicInfo(
//            (bool)reader["CorLecture"],
//            reader["Fla_CEType"].ToString(),
//            reader["Fla_IDNo"].ToString(),
//            reader["FolderName"].ToString(),
//            reader["HTML"].ToString(),
//            reader["LastUpdate"].ToString(),
//            (bool)reader["NoSurvey"],
//            (int)reader["TopicID"],
//            reader["TopicName"].ToString(),
//            (int)(Convert.IsDBNull(reader["SurveyID"]) ? (int)0 : (int)reader["SurveyID"]),
//            (bool)reader["Compliance"],
//            reader["MetaKW"].ToString(),
//            reader["MetaDesc"].ToString(),
//            (decimal)reader["Hours"],
//            reader["MediaType"].ToString(),
//            reader["Objectives"].ToString(),
//            reader["Content"].ToString(),
//            (bool)reader["Textbook"],
//            (int)reader["CertID"],
//            reader["Method"].ToString(),
//            reader["GrantBy"].ToString(),
//            reader["DOCXFile"].ToString(),
//            reader["DOCXHoFile"].ToString(),
//            (bool)reader["Obsolete"],
//            (int)reader["FacilityID"],
//            (Convert.IsDBNull(reader["Course_Number"]) ? "" : reader["Course_Number"].ToString()),
//            //(Convert.IsDBNull(reader["CeRef"]) ? "" : reader["CeRef"].ToString()),
//            //(DateTime)(Convert.IsDBNull(reader["Release_Date"])
//            //    ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Release_Date"]),
//            (int)(Convert.IsDBNull(reader["Minutes"]) ? (int)0 : (int)reader["Minutes"]),
//            (Convert.IsDBNull(reader["Audio_Ind"]) ? false : Convert.ToBoolean(reader["Audio_Ind"])),
//            (bool)(Convert.IsDBNull(reader["Apn_Ind"]) ? false : (bool)reader["Apn_Ind"]),
//            (Convert.IsDBNull(reader["Icn_Ind"]) ? false : Convert.ToBoolean(reader["Icn_Ind"])),
//            (Convert.IsDBNull(reader["Jcaho_Ind"]) ? false : Convert.ToBoolean(reader["Jcaho_Ind"])),
//            (Convert.IsDBNull(reader["Magnet_Ind"]) ? false : Convert.ToBoolean(reader["Magnet_Ind"])),
//            (bool)reader["Active_Ind"],
//            (Convert.IsDBNull(reader["Video_Ind"]) ? false : Convert.ToBoolean(reader["Video_Ind"])),
//            (Convert.IsDBNull(reader["Online_Ind"]) ? false : Convert.ToBoolean(reader["Online_Ind"])),
//            (Convert.IsDBNull(reader["Ebp_Ind"]) ? false : Convert.ToBoolean(reader["Ebp_Ind"])),
//            (Convert.IsDBNull(reader["Ccm_Ind"]) ? false : Convert.ToBoolean(reader["Ccm_Ind"])),
//            (Convert.IsDBNull(reader["Avail_Ind"]) ? false : Convert.ToBoolean(reader["Avail_Ind"])),
//            reader["Cert_Cerp"].ToString(),
//            reader["Ref_Html"].ToString(),
//            (int)reader["AlterID"],
//            (int)(Convert.IsDBNull(reader["Pass_Score"]) ? (int)0 : (int)reader["Pass_Score"]),
//            reader["Subtitle"].ToString(),
//            //(decimal)(Convert.IsDBNull(reader["Cost"]) ? (decimal)0 : (decimal)reader["Cost"]),
//            reader["Topic_Type"].ToString(),
//            reader["Img_Name"].ToString(),
//            reader["Img_Credit"].ToString(),
//            reader["Img_Caption"].ToString(),
//            reader["Accreditation"].ToString(),
//            (int)(Convert.IsDBNull(reader["Views"]) ? (int)0 : (int)reader["Views"]),
//            (int)(Convert.IsDBNull(reader["PrimaryViews"]) ? (int)0 : (int)reader["PrimaryViews"]),
//            (int)(Convert.IsDBNull(reader["PageReads"]) ? (int)0 : (int)reader["PageReads"]),
//            (Convert.IsDBNull(reader["Featured"]) ? false : Convert.ToBoolean(reader["Featured"])),
//            reader["rev"].ToString(),
//            reader["urlmark"].ToString(),
//            (decimal)(Convert.IsDBNull(reader["offline_Cost"]) ? (decimal)0 : (decimal)reader["offline_Cost"]),
//            (decimal)(Convert.IsDBNull(reader["online_Cost"]) ? (decimal)0 : (decimal)reader["online_Cost"]),
//            (bool)(Convert.IsDBNull(reader["notest"]) ? false : (bool)reader["notest"]),
//            (bool)(Convert.IsDBNull(reader["uce_Ind"]) ? false : (bool)reader["uce_Ind"]),
//            (Convert.IsDBNull(reader["shortname"]) ? "" : reader["shortname"].ToString()),
//            (int)(Convert.IsDBNull(reader["categoryid"]) ? (int)0 : (int)reader["categoryid"]),
//            (bool)(Convert.IsDBNull(reader["hold_ind"]) ? false : (bool)reader["hold_ind"]));
          
//          return Topic;
//      }

//      /// <summary>
//      /// Returns a collection of TopicInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<TopicInfo> GetTopicCollectionFromReader(IDataReader reader)
//      {
//          return GetTopicCollectionFromReader(reader, true);
//      }
//      protected virtual List<TopicInfo> GetTopicCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<TopicInfo> Topics = new List<TopicInfo>();
//          while (reader.Read())
//              Topics.Add(GetTopicFromReader(reader, readMemos));
//          return Topics;
//      }


//      /// <summary>
//      /// Returns a collection of TopicInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<MediaContentInfo> GetMediaContentCollectionFromReader(IDataReader reader)
//      {
//          return GetMediaContentCollectionFromReader(reader, true);
//      }
//      protected virtual List<MediaContentInfo> GetMediaContentCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<MediaContentInfo> MediaContents = new List<MediaContentInfo>();
//          while (reader.Read())
//              MediaContents.Add(GetMediaContentFromReader(reader, readMemos));
//          return MediaContents;
//      }



//      protected virtual MediaContentInfo GetMediaContentFromReader(IDataReader reader)
//      {
//          return GetMediaContentFromReader(reader, true);
//      }
//      protected virtual MediaContentInfo GetMediaContentFromReader(IDataReader reader, bool readMemos)
//      {
//          MediaContentInfo MediaContent = new MediaContentInfo(
//              (int)reader["mcid"],
//              (int)reader["topicid"],
//               reader["mediatype"].ToString(),
//            reader["asseturl"].ToString(),
//            reader["description"].ToString());

//          return MediaContent;
//      }


//      /// <summary>
//      /// Returns a new CheckBoxListTopicInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual CheckBoxListTopicInfo GetCheckBoxListTopicFromReader(IDataReader reader)
//      {
//          CheckBoxListTopicInfo CheckBoxListTopic = new CheckBoxListTopicInfo(
//            (int)reader["TopicID"],
//            reader["TopicName"].ToString(),
//            (bool)reader["Selected"]);

//          return CheckBoxListTopic;
//      }

//      /// <summary>
//      /// Returns a collection of CheckBoxListTopicInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<CheckBoxListTopicInfo> GetCheckBoxListTopicCollectionFromReader(IDataReader reader)
//      {
//          List<CheckBoxListTopicInfo> CheckBoxListTopics = new List<CheckBoxListTopicInfo>();
//          while (reader.Read())
//              CheckBoxListTopics.Add(GetCheckBoxListTopicFromReader(reader));
//          return CheckBoxListTopics;
//      }

//       public abstract int GetTopicCountByCategoryID(int CategoryID);
//       public abstract List<MediaContentInfo> GetMediaContentByTopicID(int TopicID, string cSortExpression);
//      public abstract List<TopicInfo> GetTopicsByCategoryID(int CategoryID, string cSortExpression);
//      public abstract List<TopicInfo> GetTopicsByCategoryName(string CategoryName, string cSortExpression);
//      public abstract List<TopicInfo> GetTextBookTopicsByCategoryId(int CategoryID, string cSortExpression);
//      public abstract List<TopicInfo> GetTextAndNonTextTopicsByCategoryId(int CategoryId, string cSortExpression);
//      public abstract List<TopicInfo> GetTopicsInProgressByUserID(int UserID, string cSortExpression);
//      public abstract List<TopicInfo> GetTopicsBySearchText(string cSearchText, string cSortExpression);
//      public abstract List<TopicInfo> GetTopicsBySearchTextandFacility(string cSearchText, int facilityid, string cSortExpression);
//      public abstract List<TopicInfo> GetRetailTopicsBySearchTextandFacility(string cSearchText, int facilityid, string cSortExpression);
//      //public abstract List<TopicInfo> GetAvailableTopicsByCategoryIDAndUserName(int CategoryID, string UserName, string cSortExpression);
//      public abstract List<TopicInfo> GetAvailableTopicsByCategoryIDAndUserID(int CategoryID, int UserID, string cSortExpression);
//      public abstract List<TopicInfo> GetTextbookTopicsWithNoCategories(string cSortExpression);
//      public abstract List<TopicInfo> GetNonTextbookTopicsWithNoCategories(string cSortExpression);
//      public abstract List<TopicInfo> GetTopicsByLectureID(int LectureID, string cSortExpression);
//      public abstract bool UpdateTopicHTML(int TopicID, string HTML, string LastUpdate, string FolderName);
//      public abstract bool UpdateTopicImage(int TopicID,string image);
//      public abstract bool UpdateTextbookTopicHTML(int TopicID, string HTML, string LastUpdate, string FolderName, 
//          string DOCXFile, string DOCXHoFile);
//      public abstract bool UpdateTopicEditableFields(int TopicID, bool CorLecture,
//           string Fla_CEType, string Fla_IDNo, string LastUpdate, bool NoSurvey,
//           int SurveyID, bool Compliance, string MetaKW, string MetaDesc,
//           decimal Hours, string MediaType, string Objectives, string Content,
//           bool Textbook, int CertID, string Method, string GrantBy, bool Obsolete, string TopicName);
//      public abstract int GetHighestTopicID();
//      public abstract bool AssignTopicToCategory(int TopicID, int CategoryID);
//      public abstract bool DetachTopicFromCategory(int TopicID, int CategoryID);
//      public abstract bool DetachTopicFromAllCategories(int TopicID);
//      public abstract List<CheckBoxListTopicInfo> GetAllTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression);
//      public abstract List<CheckBoxListTopicInfo> GetAllNonTextbookTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression);
//      public abstract List<CheckBoxListTopicInfo> GetAllTextbookTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression);
//      public abstract bool UpdateTopicCategoryAssignments(int TopicID, string CategoryIDAssignments);

//      /////////////////////////////////////////////////////////
//      // methods that work with TopicComplianceLinks
//      public abstract int GetTopicComplianceLinkCount();
//      public abstract List<TopicComplianceLinkInfo> GetTopicComplianceLinks(string cSortExpression);
//      public abstract List<TopicComplianceLinkInfo> GetTopicComplianceLinksByFacilityID(int FacilityID, string cSortExpression);
//      public abstract List<TopicCollectionInfo> GetTopicCollectionsByTopicID(int TopicID, string cSortExpression);
//      public abstract bool UpdateTopicCollections(int chapterid, int chapternumber,int tcid);
//      public abstract DataSet GetTopicCollectionsDataSetByTopicID(int TopicID, string cSortExpression);
//      public abstract TopicComplianceLinkInfo GetTopicComplianceLinkByID(int TopCompID);
//      public abstract bool DeleteTopicComplianceLink(int TopCompID);
//      public abstract bool UpdateTopicComplianceLink(TopicComplianceLinkInfo TopicComplianceLink);
//      public abstract int InsertTopicComplianceLink(TopicComplianceLinkInfo TopicComplianceLink);
//      public abstract int InsertMediaContent(MediaContentInfo MediaContent);
//      public abstract bool UpdateMediaContent(MediaContentInfo MediaContent);
//      public abstract bool DeleteMediaContent(int mcid);
//      public abstract int InsertTopicCollection(TopicCollectionInfo TopicCollection);
//      public abstract bool DeleteTopicCollection(int TopicID);


//      /// <summary>
//      /// Returns a new TopicComplianceLinkInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual TopicComplianceLinkInfo GetTopicComplianceLinkFromReader(IDataReader reader)
//      {
//          return GetTopicComplianceLinkFromReader(reader, true);
//      }
//      protected virtual TopicComplianceLinkInfo GetTopicComplianceLinkFromReader(IDataReader reader, bool readMemos)
//      {
//          TopicComplianceLinkInfo TopicComplianceLink = new TopicComplianceLinkInfo(
//            (int)reader["TopCompID"],
//            (int)reader["TopicID"],
//            (int)reader["FacID"],
//            reader["ComplFile"].ToString());


//          return TopicComplianceLink;
//      }

//      /// <summary>
//      /// Returns a collection of TopicComplianceLinkInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<TopicComplianceLinkInfo> GetTopicComplianceLinkCollectionFromReader(IDataReader reader)
//      {
//          return GetTopicComplianceLinkCollectionFromReader(reader, true);
//      }
//      protected virtual List<TopicComplianceLinkInfo> GetTopicComplianceLinkCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<TopicComplianceLinkInfo> TopicComplianceLinks = new List<TopicComplianceLinkInfo>();
//          while (reader.Read())
//              TopicComplianceLinks.Add(GetTopicComplianceLinkFromReader(reader, readMemos));
//          return TopicComplianceLinks;
//      }




//      /// <summary>
//      /// Returns a new TopicCollectionInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual TopicCollectionInfo GetTopicCollectionTFromReader(IDataReader reader)
//      {
//          return GetTopicCollectionTFromReader(reader, true);
//      }
//      protected virtual TopicCollectionInfo GetTopicCollectionTFromReader(IDataReader reader, bool readMemos)
//      {
//          TopicCollectionInfo TopicCollection = new TopicCollectionInfo(
//            (int)reader["TopicID"],
//            (int)reader["ChapterID"],
//            (int)reader["ChapterNumber"],
//            (int)reader["TCID"]);
          
//          return TopicCollection;
//      }

//      /// <summary>
//      /// Returns a collection of TopicComplianceLinkInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<TopicCollectionInfo> GetTopicCollectionCollectionFromReader(IDataReader reader)
//      {
//          return GetTopicCollectionCollectionFromReader(reader, true);
//      }
//      protected virtual List<TopicCollectionInfo> GetTopicCollectionCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<TopicCollectionInfo> TopicCollections = new List<TopicCollectionInfo>();
//          while (reader.Read())
//              TopicCollections.Add(GetTopicCollectionTFromReader(reader, readMemos));
//          return TopicCollections;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with TopicResourceLinks
//      public abstract int GetTopicResourceLinkCount();
//      public abstract int GetTopicResourceLinkCountByTopicID(int TopicID);
//      public abstract List<TopicResourceLinkInfo> GetTopicResourceLinks(string cSortExpression);
//      public abstract TopicResourceLinkInfo GetTopicResourceLinkByID(int TopicResID);
//      public abstract bool DeleteTopicResourceLink(int TopicResID);
//      public abstract bool UpdateTopicResourceLink(TopicResourceLinkInfo TopicResourceLink);
//      public abstract int InsertTopicResourceLink(TopicResourceLinkInfo TopicResourceLink);
//      public abstract bool UpdateResourcePersonAssignments(int TopicID, string ResourceIDAssignments, string ResourceType);
 
//       /// <summary>
//      /// Returns a new TopicResourceLinkInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual TopicResourceLinkInfo GetTopicResourceLinkFromReader(IDataReader reader)
//      {
//          return GetTopicResourceLinkFromReader(reader, true);
//      }
//      protected virtual TopicResourceLinkInfo GetTopicResourceLinkFromReader(IDataReader reader, bool readMemos)
//      {
//          TopicResourceLinkInfo TopicResourceLink = new TopicResourceLinkInfo(
//            (int)reader["TopicResID"],
//            (int)reader["TopicID"],
//            (int)reader["ResourceID"],
//            reader["ResType"].ToString());


//          return TopicResourceLink;
//      }

//      /// <summary>
//      /// Returns a collection of TopicResourceLinkInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<TopicResourceLinkInfo> GetTopicResourceLinkCollectionFromReader(IDataReader reader)
//      {
//          return GetTopicResourceLinkCollectionFromReader(reader, true);
//      }
//      protected virtual List<TopicResourceLinkInfo> GetTopicResourceLinkCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<TopicResourceLinkInfo> TopicResourceLinks = new List<TopicResourceLinkInfo>();
//          while (reader.Read())
//              TopicResourceLinks.Add(GetTopicResourceLinkFromReader(reader, readMemos));
//          return TopicResourceLinks;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with UserInterestLinks
//      public abstract int GetUserInterestLinkCount();
//      public abstract List<UserInterestLinkInfo> GetUserInterestLinks(string cSortExpression);
//      public abstract UserInterestLinkInfo GetUserInterestLinkByID(int UserIntID);
//      public abstract bool DeleteUserInterestLink(int UserIntID);
//      public abstract bool UpdateUserInterestLink(UserInterestLinkInfo UserInterestLink);
//      public abstract int InsertUserInterestLink(UserInterestLinkInfo UserInterestLink);

//      /// <summary>
//      /// Returns a new UserInterestLinkInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual UserInterestLinkInfo GetUserInterestLinkFromReader(IDataReader reader)
//      {
//          return GetUserInterestLinkFromReader(reader, true);
//      }
//      protected virtual UserInterestLinkInfo GetUserInterestLinkFromReader(IDataReader reader, bool readMemos)
//      {
//          UserInterestLinkInfo UserInterestLink = new UserInterestLinkInfo(
//            (int)reader["UserIntID"],
//            (int)reader["UserID"],
//            (int)reader["InterestID"]);


//          return UserInterestLink;
//      }

//      /// <summary>
//      /// Returns a collection of UserInterestLinkInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<UserInterestLinkInfo> GetUserInterestLinkCollectionFromReader(IDataReader reader)
//      {
//          return GetUserInterestLinkCollectionFromReader(reader, true);
//      }
//      protected virtual List<UserInterestLinkInfo> GetUserInterestLinkCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<UserInterestLinkInfo> UserInterestLinks = new List<UserInterestLinkInfo>();
//          while (reader.Read())
//              UserInterestLinks.Add(GetUserInterestLinkFromReader(reader, readMemos));
//          return UserInterestLinks;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with UserLicenses
//      public abstract int GetUserLicenseCount();
//      public abstract List<UserLicenseInfo> GetUserLicenses(string cSortExpression);
//      public abstract List<UserLicenseInfo> GetUserLicensesByUserID(int UserID, string cSortExpression);
//      public abstract UserLicenseInfo  GetUserLicenseByUserID(int UserID);
//      public abstract UserLicenseInfo GetUserLicenseByID(int LicenseID);
//      public abstract bool DeleteUserLicense(int LicenseID);
//      public abstract bool UpdateUserLicense(UserLicenseInfo UserLicense);
//      public abstract int InsertUserLicense(UserLicenseInfo UserLicense);

//      /// <summary>
//      /// Returns a new UserLicenseInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual UserLicenseInfo GetUserLicenseFromReader(IDataReader reader)
//      {
//          return GetUserLicenseFromReader(reader, true);
//      }
//      protected virtual UserLicenseInfo GetUserLicenseFromReader(IDataReader reader, bool readMemos)
//      {
//          UserLicenseInfo UserLicense = new UserLicenseInfo(
//            (int)reader["LicenseID"],
//            (int)reader["UserID"],
//            (int)reader["License_Type_ID"],
//            reader["State"].ToString(),
//            reader["License_Number"].ToString(),
//            (Convert.IsDBNull(reader["Issue_Date"]) 
//                ? "" : ((DateTime)reader["Issue_Date"]).ToShortDateString() ),
//            (Convert.IsDBNull(reader["Expire_Date"]) 
//                ? "" : ((DateTime)reader["Expire_Date"]).ToShortDateString() ),
//            (Convert.IsDBNull(reader["Report_To_State"])? false:(bool)reader["Report_To_State"]));

//          return UserLicense;
//      }

//      /// <summary>
//      /// Returns a collection of UserLicenseInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<UserLicenseInfo> GetUserLicenseCollectionFromReader(IDataReader reader)
//      {
//          return GetUserLicenseCollectionFromReader(reader, true);
//      }
//      protected virtual List<UserLicenseInfo> GetUserLicenseCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<UserLicenseInfo> UserLicenses = new List<UserLicenseInfo>();
//          while (reader.Read())
//              UserLicenses.Add(GetUserLicenseFromReader(reader, readMemos));
//          return UserLicenses;
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with UsersInRoles
//      public abstract int GetUsersInRolesCount();
//      public abstract List<UsersInRolesInfo> GetUsersInRoles(string cSortExpression);
//      public abstract UsersInRolesInfo GetUsersInRolesByID(int UserRoleID);
//      public abstract bool DeleteUsersInRoles(int UserRoleID);
//      public abstract bool DeleteUsersInRolesByUserName(string UserName);
//      public abstract bool UpdateUsersInRoles(UsersInRolesInfo UsersInRoles);
//      public abstract int InsertUsersInRoles(UsersInRolesInfo UsersInRoles);

//      /// <summary>
//      /// Returns a new UsersInRolesInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual UsersInRolesInfo GetUsersInRolesFromReader(IDataReader reader)
//      {
//          return GetUsersInRolesFromReader(reader, true);
//      }
//      protected virtual UsersInRolesInfo GetUsersInRolesFromReader(IDataReader reader, bool readMemos)
//      {
//          UsersInRolesInfo UsersInRoles = new UsersInRolesInfo(
//            (int)reader["UserRoleID"],
//            reader["RoleName"].ToString(),
//            reader["UserName"].ToString().ToLower(),
//            (int)reader["FacilityId"]);


//          return UsersInRoles;
//      }

//      /// <summary>
//      /// Returns a collection of UsersInRolesInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<UsersInRolesInfo> GetUsersInRolesCollectionFromReader(IDataReader reader)
//      {
//          return GetUsersInRolesCollectionFromReader(reader, true);
//      }
//      protected virtual List<UsersInRolesInfo> GetUsersInRolesCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<UsersInRolesInfo> UsersInRoles = new List<UsersInRolesInfo>();
//          while (reader.Read())
//              UsersInRoles.Add(GetUsersInRolesFromReader(reader, readMemos));
//          return UsersInRoles;
//      }


//      ////////////////////////////////////////////////////
//      // ADDED AT THE BOTTOM FROM AddToProv.txt///////////
//      ////////////////////////////////////////////////////

//      /////////////////////////////////////////////////////////
//      // methods that work with TopicDetails
//      public abstract int GetTopicDetailCount();
//      public abstract int GetTopicDetailCountByTopicID(int TopicID);
//      public abstract List<TopicDetailInfo> GetTopicDetailsByTopicID(int TopicID);
//      public abstract bool DeleteTopicDetailsByTopicID(int TopicID);
//      public abstract bool InsertTopicDetail(TopicDetailInfo TopicDetail);
//      public abstract bool InsertTopicDetailsFromTopicDetailList(
//                 List<TopicDetailInfo> TopicDetails);
//      public abstract bool UpdateUserAccountDeptByuserID(int userID, int deptID);
    
//      /// <summary>
//      /// Returns a new TopicDetailInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual TopicDetailInfo GetTopicDetailFromReader(IDataReader reader)
//      {
//          return GetTopicDetailFromReader(reader, true);
//      }
//      protected virtual TopicDetailInfo GetTopicDetailFromReader(IDataReader reader, bool readMemos)
//      {
//          TopicDetailInfo TopicDetail = new TopicDetailInfo(
//           (int)reader["TopicID"],
//           reader["Type"].ToString(),
//           reader["Data"].ToString(),
//           reader["CorrectAns"].ToString(),
//           (int)reader["Order"],
//           reader["Discussion"].ToString());

//          //         if (readMemos)
//          //            article.Body = reader["Body"].ToString();

//          return TopicDetail;
//      }

//      /// <summary>
//      /// Returns a collection of TopicDetailInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<TopicDetailInfo> GetTopicDetailCollectionFromReader(IDataReader reader)
//      {
//          return GetTopicDetailCollectionFromReader(reader, true);
//      }
//      protected virtual List<TopicDetailInfo> GetTopicDetailCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<TopicDetailInfo> TopicDetails = new List<TopicDetailInfo>();
//          while (reader.Read())
//              TopicDetails.Add(GetTopicDetailFromReader(reader, readMemos));
//          return TopicDetails;
//      }

//       //////////////////////////////////// 
//       /// Methods that work with Nurse
//       //////////////////////////////////
        
//      public abstract int InsertNurse(string FirstName, string LastName, string UserName, string Email, string Password);
//      public abstract bool UpdateNurse(NurseInfo Nurse);
//      public abstract NurseInfo GetNurseByID(int rnID);
//      protected virtual NurseInfo GetNurseFromReader(IDataReader reader)
//      {
//          return GetNurseFromReader(reader, true);
//      }
//      protected virtual NurseInfo GetNurseFromReader(IDataReader reader, bool readMemos)
//      {       

//          NurseInfo Nurse = new NurseInfo(
//            (int)(Convert.IsDBNull(reader["rnID"]) ? (int)0 : (int)reader["rnID"]),
//            reader["UserName"].ToString(), reader["Password"].ToString(), (bool)(Convert.IsDBNull(reader["Registered"])) ? false : Convert.ToBoolean(reader["Registered"]),
//            reader["FirstName"].ToString(), reader["LastName"].ToString(), reader["Address1"].ToString(),
//            reader["Address2"].ToString(),  reader["City"].ToString(), reader["State"].ToString(),
//            reader["Zip"].ToString(), reader["Country"].ToString(), reader["S_Address1"].ToString(),
//            reader["S_Address2"].ToString(), reader["S_City"].ToString(), reader["S_State"].ToString(),
//            reader["S_Zip"].ToString(), reader["S_Country"].ToString(),  reader["Email"].ToString(),
//            reader["EmailPref"].ToString(), (bool)reader["OptIn"], (bool)(Convert.IsDBNull(reader["Offers"])) ? false : Convert.ToBoolean(reader["Offers"]),
//            reader["RNstate"].ToString(), reader["RNnum"].ToString(), reader["RNCode"].ToString(),
//            reader["RNnumX"].ToString(), reader["RNstate2"].ToString(), reader["RNnum2"].ToString(),
//            reader["RNCode2"].ToString(), reader["RNnumX2"].ToString(),    //(int)(Convert.IsDBNull(reader["SpecialityID"],
//            (int)(Convert.IsDBNull(reader["SpecialtyID"]) ? (int)0 : (int)reader["SpecialtyID"]),
//          //  1,"",
//            (reader["SpecialtyIDlist"] == null)? String.Empty :reader["SpecialtyIDlist"].ToString(),
//            reader["ImportedFrom"].ToString(),
//           (int)(Convert.IsDBNull(reader["FieldID"]) ? (int)0 : (int)reader["FieldID"]),
//           (int)(Convert.IsDBNull(reader["AgeGroup"]) ? (int)0 : (int)reader["AgeGroup"]),
//            reader["BrowserAgent"].ToString(), reader["Phone"].ToString(),  reader["PhoneType"].ToString(),
//            reader["Specialty"].ToString().ToLower(), reader["Education"].ToString(), reader["Position"].ToString(),
//            (DateTime)(Convert.IsDBNull(reader["DateEntered"]) 
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["DateEntered"]),
//                (DateTime)(Convert.IsDBNull(reader["UCEend"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["UCEend"]),
//                (DateTime)(Convert.IsDBNull(reader["Updated"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Updated"]),
//                (DateTime)(Convert.IsDBNull(reader["LastLogin"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLogin"]),
//            reader["IP"].ToString(),
//                     (bool)(Convert.IsDBNull(reader["Disabled"]) ? false : Convert.ToBoolean(reader["Disabled"])));
//           return Nurse;
//      }

//       /////////////////////////////////////////////////////////
//       // methods that work with HistTranscript

//      public abstract List<HistTranscriptInfo> GetHistTranscriptRecords(string cSortExpression, int TranscriptID, int ItemID, int Score, DateTime DateComplete,
//            string FirstName, string LastName, string RNState, string RNNum, string RNCode, string RNState2, string RNNum2, string RNCode2, string CourseName,
//            string CourseNum, decimal Credits);



//      /// <summary>
//      /// Returns a new CategoryLinkInfo instance filled with the DataReader's current record data
//      /// </summary>
//      protected virtual MetaInfo GetMetaFromReader(IDataReader reader)
//      {
//          return GetMetaFromReader(reader, true);
//      }
//      protected virtual MetaInfo GetMetaFromReader(IDataReader reader, bool readMemos)
//      {
//          MetaInfo Meta = new MetaInfo(
//            (int)reader["metaID"],
//            reader["path"].ToString(),
//              reader["title"].ToString(),
//                reader["description"].ToString(),
//                  reader["keyword"].ToString(),
//                    reader["h1"].ToString());


//          return Meta;
//      }

//      /// <summary>
//      /// Returns a collection of CategoryLinkInfo objects with the data read from the input DataReader
//      /// </summary>
//      protected virtual List<MetaInfo> GetMetaCollectionFromReader(IDataReader reader)
//      {
//          return GetMetaCollectionFromReader(reader, true);
//      }
//      protected virtual List<MetaInfo> GetMetaCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<MetaInfo> Metas = new List<MetaInfo>();
//          while (reader.Read())
//              Metas.Add(GetMetaFromReader(reader, readMemos));
//          return Metas;
//      }

//      public abstract MetaInfo GetMetaByPath(String Path);


//      #region Methods that work with CategoryAreaLink

//      public abstract List<CategoryAreaLinkInfo> GetCategoryAreaLinkByCategoryId(int Categoryid);
//      public abstract int UpdateCategoryAreaLink(CategoryAreaLinkInfo CategoryAreaLink);
//      protected virtual List<CategoryAreaLinkInfo> GetCategoryAreaLinkCollectionFromReader(IDataReader reader)
//      {
//          return GetCategoryAreaLinkCollectionFromReader(reader, true);
//      }
//      protected virtual List<CategoryAreaLinkInfo> GetCategoryAreaLinkCollectionFromReader(IDataReader reader, bool readMemos)
//      {
//          List<CategoryAreaLinkInfo> CategoryAreaLinks = new List<CategoryAreaLinkInfo>();
//          while (reader.Read())
//              CategoryAreaLinks.Add(GetCategoryAreaLinkFromReader(reader, readMemos));
//          return CategoryAreaLinks;
//      }
//      protected virtual CategoryAreaLinkInfo GetCategoryAreaLinkFromReader(IDataReader reader)
//      {
//          return GetCategoryAreaLinkFromReader(reader, true);
//      }
//      public abstract bool DeleteCategoryAreaLink(int Categoryid, int areaid);
//      protected virtual CategoryAreaLinkInfo GetCategoryAreaLinkFromReader(IDataReader reader, bool readMemos)
//      {
//          // NOTE: NULL integer foreign key LectEvtID is replaced with value of
//          // zero, which will trigger "Please select..." or "(Not Selected)"
//          // message in UI dropdown for that field
//          CategoryAreaLinkInfo CategoryAreaLink = new CategoryAreaLinkInfo(
//            (int)reader["Categoryid"],
//            (int)reader["areaid"]);

//          return CategoryAreaLink;
//      }
//      #endregion
   }
}