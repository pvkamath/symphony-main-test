﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region MicrositePicklistInfo

namespace PearlsReview.DAL
{

    /// <summary> 
    /// Summary description for MicrositeDomainInfo
    /// Bhaskar N
    /// </summary>
    public class MicrositePicklistInfo
    {

        public MicrositePicklistInfo() { }


        public MicrositePicklistInfo(int msid, int topicid, string pick_type, int pick_order)
        {
            this.Msid = msid;
            this.Topicid = topicid;
            this.Pick_type = pick_type;
            this.Pick_order = pick_order;
        }

        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }
        private int _topicid = 0;
        public int Topicid
        {
            get { return _topicid; }
            private set { _topicid = value; }
        }
        private string _pick_type = "";
        public string Pick_type
        {
            get { return _pick_type; }
            private set { _pick_type = value; }
        }
        private int _pick_order = 0;
        public int Pick_order
        {
            get { return _pick_order; }
            private set { _pick_order = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // Methods that work with MicrositeDomain
        // Bhaskar N

        /// <summary>
        /// Retrieves all MicrositeDomains
        /// </summary>
        public List<MicrositePicklistInfo> GetMicrositePicklist(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from MicrositePicklist ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetMicrositePicklistCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public DataTable GetMicrositePicklistByType(int Msid, string PickType, string cSortExpression)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT t.*, pl.* from Topic t right outer join MicrositePicklist pl on pl.topicid = t.topicid where pl.msid = @msid and pl.pick_type = @pick_type and t.active_ind='1' and t.obsolete='0' ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                cmd.Parameters.Add("@pick_type", SqlDbType.VarChar).Value = PickType;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
        public DataTable GetMicrositePicklistByTypes(int Msid, string[] PickTypes, string cSortExpression)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                string pick_types = "'" + string.Join("','", PickTypes) + "'";
                cSQLCommand = "SELECT t.*, pl.* from Topic t right outer join MicrositePicklist pl on pl.topicid = t.topicid where pl.msid = @msid and pl.pick_type in (" + pick_types + ") and t.active_ind='1' and t.obsolete='0' ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
        public DataTable GetMicrositePicklistByMsid(int Msid, string cSortExpression)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT t.*, pl.* from Topic t right outer join MicrositePicklist pl on pl.topicid = t.topicid where pl.msid = @msid and t.active_ind='1' and t.obsolete='0'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
        public MicrositePicklistInfo GetMicrositePicklist(int msid, int topicid, string PickType)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositePicklist where msid=@msid and topicid = @topicid and pick_type=@pick_type", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@pick_type", SqlDbType.VarChar).Value = PickType;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositePicklistFromReader(reader, true);
                else
                    return null;
            }
        }
        /// <summary>
        /// Retrieves the MicrositeDomain with the specified ID
        /// </summary>
        public MicrositePicklistInfo GetMicrositePicklistByID(int msid, int topicid, string pick_type)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositePicklist where msid=@msid and topicid = @topicid and pick_type=@pick_type", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@pick_type", SqlDbType.VarChar).Value = pick_type;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositePicklistFromReader(reader, true);
                else
                    return null;
            }
        }
        public int GetMicrositePicklistMaxOrder(int msid, string pick_type)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select count(*) as max " +
                        "from MicrositePicklist where msid=@msid and pick_type = @pick_type", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cmd.Parameters.Add("@pick_type", SqlDbType.VarChar).Value = pick_type;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return int.Parse(reader["max"].ToString());
                else
                    return 0;
            }
        }
        public void RefreshTopRated(int msid)
        {
             using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_r_microsite_refresh_toprated";
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }
        public void RefreshMostPopular(int msid)
        {
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_r_microsite_refresh_mostpopular";
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// Inserts a new Discount
        /// </summary>
        public int InsertMicrositePicklist(MicrositePicklistInfo MicrositePicklist)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into MicrositePicklist " +
                  "(msid , " +
                  "topicid, " +
                  "pick_type, " +
                  "pick_order ) SET @msid = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositePicklist.Msid;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = MicrositePicklist.Topicid;
                cmd.Parameters.Add("@pick_type", SqlDbType.VarChar).Value = MicrositePicklist.Pick_type;
                cmd.Parameters.Add("@pick_order", SqlDbType.Int).Value = MicrositePicklist.Pick_order;

                SqlParameter IDParameter = new SqlParameter("@msid", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }
        public bool UpdateMicrositePickListLists(List<MicrositePicklistInfo> Lists, int Msid, string PickType)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                cn.Open();
                SqlTransaction transaction;
                SqlCommand cmd = cn.CreateCommand();
                transaction = cn.BeginTransaction();
                cmd.Connection = cn;
                cmd.Transaction = transaction;

                try
                {
                    cmd.CommandText = "delete from MicrositePicklist where msid=@msid and pick_type=@pick_type";
                    cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                    cmd.Parameters.Add("@pick_type", SqlDbType.VarChar).Value = PickType;
                    cmd.ExecuteNonQuery();

                    foreach (MicrositePicklistInfo PickList in Lists)
                    {
                        cmd.CommandText = "insert into MicrositePicklist (msid , topicid, pick_type, pick_order) values (@msid, @topicid, @pick_type, @pick_order)";
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                        cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = PickList.Topicid;
                        cmd.Parameters.Add("@pick_type", SqlDbType.VarChar).Value = PickList.Pick_type;
                        cmd.Parameters.Add("@pick_order", SqlDbType.Int).Value = PickList.Pick_order;
                        cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Updates a Discount
        /// </summary>
        public bool UpdateMicrositePicklist(MicrositePicklistInfo MicrositePicklist)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update MicrositePicklist set " +
              "msid = @msid, " +
              "topicid = @topicid, " +
              "pick_type = @pick_type, " +
              "pick_order = @pick_order where msid = @msid and topicid = @topicid and pick_type = @pick_type", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositePicklist.Msid;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = MicrositePicklist.Topicid;
                cmd.Parameters.Add("@pick_type", SqlDbType.VarChar).Value = MicrositePicklist.Pick_type;
                cmd.Parameters.Add("@pick_order", SqlDbType.Int).Value = MicrositePicklist.Pick_order;
                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a Discount
        /// </summary>
        public bool DeleteMicrositePicklist(int Msid, int topicid, string PickType)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from MicrositePicklist where msid=@msid and topicid = @topicid and pick_type=@pick_type", cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@pick_type", SqlDbType.VarChar).Value = PickType;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public bool IsUnlimitedByTopicIdAndDomainId(int TopicId, int DomainId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select topicid from micrositepicklist where pick_type='unlimited-ce' and topicid = @TopicId and msid=@DomainId", cn);
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return true;
                else
                    return false;
               
            }
        }

        public DataTable GetLimitedMicrositePicklistByType(int Msid, string PickType, string cSortExpression)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT top 15 t.*, pl.* from Topic t right outer join MicrositePicklist pl on pl.topicid = t.topicid where pl.msid = @msid and pl.pick_type = @pick_type and t.active_ind='1' and t.obsolete='0' ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                cmd.Parameters.Add("@pick_type", SqlDbType.VarChar).Value = PickType;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }

        #endregion

        #region PRProvider
        /////////////////////////////////////////////////////////
        // methods that work with Discount  

        /// <summary>
        /// Returns a new DiscountInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual MicrositePicklistInfo GetMicrositePicklistFromReader(IDataReader reader)
        {
            return GetMicrositePicklistFromReader(reader, true);
        }
        protected virtual MicrositePicklistInfo GetMicrositePicklistFromReader(IDataReader reader, bool readMemos)
        {
            MicrositePicklistInfo MicrositePicklist = new MicrositePicklistInfo(
                  (int)reader["msid"],
                  (int)reader["topicid"],
                  reader["pick_type"].ToString(),
                  (int)reader["pick_order"]);
            return MicrositePicklist;
        }


        /// <summary>
        /// Returns a collection of DiscountInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<MicrositePicklistInfo> GetMicrositePicklistCollectionFromReader(IDataReader reader)
        {
            return GetMicrositePicklistCollectionFromReader(reader, true);
        }
        protected virtual List<MicrositePicklistInfo> GetMicrositePicklistCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<MicrositePicklistInfo> MicrositePicklist = new List<MicrositePicklistInfo>();
            while (reader.Read())
                MicrositePicklist.Add(GetMicrositePicklistFromReader(reader, readMemos));
            return MicrositePicklist;
        }

        #endregion

    }
}
#endregion
