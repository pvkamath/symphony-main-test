﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region MicrositeStateReqInfo

namespace PearlsReview.DAL
{

    /// <summary> 
    /// Summary description for MicrositeDomainInfo
    /// Bhaskar N
    /// </summary>
    public class MicrositeStateReqInfo
    {

        public MicrositeStateReqInfo() { }


        public MicrositeStateReqInfo(int msrID, int msid, string stateabr, string req_text, string req_header)
        {
            this.MsrID = msrID;
            this.Msid = msid;
            this.Stateabr = stateabr;
            this.Req_text = req_text;
            this.Req_header = req_header;
        }

        private int _msrID = 0;
        public int MsrID
        {
            get { return _msrID; }
            set { _msrID = value; }
        }

        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }
        private string _stateabr = "";
        public string Stateabr
        {
            get { return _stateabr; }
            private set { _stateabr = value; }
        }
        private string _req_text = "";
        public string Req_text
        {
            get { return _req_text; }
            private set { _req_text = value; }
        }
        private string _req_header = "";
        public string Req_header
        {
            get { return _req_header; }
            private set { _req_header = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // Methods that work with MicrositeDomain
        // Bhaskar N

        /// <summary>
        /// Retrieves all MicrositeDomains
        /// </summary>
        public List<MicrositeStateReqInfo> GetMicrositeStateReq(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from MicrositeStateReq ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetMicrositeStateReqCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the MicrositeDomain with the specified ID
        /// </summary>
        public MicrositeStateReqInfo GetMicrositeStateReqByID(int msid, string stateabr)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositeStateReq where msid=@msid and stateabr = @stateabr", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cmd.Parameters.Add("@stateabr", SqlDbType.Char).Value = stateabr;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositeStateReqFromReader(reader, true);
                else
                    return null;
            }
        }
        public List<MicrositeStateReqInfo> GetMicrositeStateReqByMsid(int msid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT m.*, s.statename from MicrositeStateReq as m join Statereq as s on m.stateabr = s.stateabr where m.msid = @msid";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;

                cn.Open();
                return GetMicrositeStateReqCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<MicrositeStateReqInfo> GetMicrositeStateReqsByMsid(int msid, string stateAbr)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from MicrositeStateReq where msid = " + msid + " and stateabr = '" + stateAbr + "'";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetMicrositeStateReqCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public DataTable GetStateReqTexts(int msid, string cSortExpression)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "select m.msid, m.stateabr, s.statename, m.req_text, m.req_header from MicrositeStateReq m " +
                "join  "+
                "StateReq s on m.stateabr = s.stateabr where msid = " + msid;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
        public DataTable GetMicrositeStateReqDTByMsid(int msid, string cSortExpression)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT ms.*, sr.statename from MicrositeStateReq ms left outer join statereq sr on ms.stateabr = sr.stateabr where msid = " + msid;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
         /// <summary>
        /// Inserts a new Discount
        /// </summary>
        public int InsertMicrositeStateReq(MicrositeStateReqInfo MicrositeStateReq)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into MicrositeStateReq " +
                  "(msid , " +
                  "stateabr, " +
                  "req_text, "+
                  "req_header) VALUES (@msid, @stateabr, @req_text, @req_header) SET @msid = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeStateReq.Msid;
                cmd.Parameters.Add("@stateabr", SqlDbType.Char).Value = MicrositeStateReq.Stateabr;
                cmd.Parameters.Add("@req_text", SqlDbType.VarChar).Value = MicrositeStateReq.Req_text;
                cmd.Parameters.Add("@req_header", SqlDbType.VarChar).Value = MicrositeStateReq.Req_header;
                
                SqlParameter IDParameter = new SqlParameter("@msid", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }
        public bool UpdateMicrositeStateReqLists(List<MicrositeStateReqInfo> StateReqs, int Msid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                cn.Open();
                SqlTransaction transaction;
                SqlCommand cmd = cn.CreateCommand();
                transaction = cn.BeginTransaction();
                cmd.Connection = cn;
                cmd.Transaction = transaction;

                try
                {
                    cmd.CommandText = "delete from micrositestatereq where msid=@msid";
                    cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                    cmd.ExecuteNonQuery();

                    foreach (MicrositeStateReqInfo stateReq in StateReqs)
                    {
                        cmd.CommandText = "insert into micrositestatereq (msid , stateabr, req_text, req_header) values (@msid, @stateabr, @req_text, @req_header)";
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                        cmd.Parameters.Add("@stateabr", SqlDbType.Char).Value = stateReq.Stateabr;
                        cmd.Parameters.Add("@req_text", SqlDbType.VarChar).Value = stateReq.Req_text;
                        cmd.Parameters.Add("@req_header", SqlDbType.VarChar).Value = stateReq.Req_header;
                        cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Updates a Discount
        /// </summary>
        public bool UpdateMicrositeStateReq(MicrositeStateReqInfo MicrositeStateReq)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update MicrositeStateReq set " +
              "msid = @msid, " +
              "stateabr = @stateabr, " +
              "req_text = @req_text, "+
              "req_header = @req_header where msid = @msid and stateabr = @stateabr", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeStateReq.Msid;
                cmd.Parameters.Add("@stateabr", SqlDbType.VarChar).Value = MicrositeStateReq.Stateabr;
                cmd.Parameters.Add("@req_text", SqlDbType.VarChar).Value = MicrositeStateReq.Req_text;
                cmd.Parameters.Add("@req_header", SqlDbType.VarChar).Value = MicrositeStateReq.Req_header;

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a Discount
        /// </summary>
        public bool DeleteMicrositeStateReq(int Msid, string Stateabr)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from MicrositeStateReq where msid=@msid and stateabr = @stateabr", cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                cmd.Parameters.Add("@stateabr", SqlDbType.VarChar).Value = Stateabr;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider
        /////////////////////////////////////////////////////////
        // methods that work with Discount  

        /// <summary>
        /// Returns a new DiscountInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual MicrositeStateReqInfo GetMicrositeStateReqFromReader(IDataReader reader)
        {
            return GetMicrositeStateReqFromReader(reader, true);
        }
        protected virtual MicrositeStateReqInfo GetMicrositeStateReqFromReader(IDataReader reader, bool readMemos)
        {
            MicrositeStateReqInfo MicrositeStateReq = new MicrositeStateReqInfo(
                (int)reader["msrid"], 
                (int)reader["msid"],
                reader["stateabr"].ToString(),
                reader["req_text"].ToString(),
                reader["req_header"].ToString());
            return MicrositeStateReq;
        }


        /// <summary>
        /// Returns a collection of DiscountInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<MicrositeStateReqInfo> GetMicrositeStateReqCollectionFromReader(IDataReader reader)
        {
            return GetMicrositeStateReqCollectionFromReader(reader, true);
        }
        protected virtual List<MicrositeStateReqInfo> GetMicrositeStateReqCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<MicrositeStateReqInfo> MicrositeStateReq = new List<MicrositeStateReqInfo>();
            while (reader.Read())
                MicrositeStateReq.Add(GetMicrositeStateReqFromReader(reader, readMemos));
            return MicrositeStateReq;
        }

        #endregion

    }
}
#endregion