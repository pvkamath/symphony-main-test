﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;


#region ImportHistSQL
public class ImportHistInfo
{
    #region Variables and Properties
    private DateTime _loadtime = System.DateTime.MinValue;
    public DateTime Loadtime
    {
        get { return _loadtime; }
        set { _loadtime = value; }
    }
    private string _verify_comment = "";
    public string Verify_comment
    {
        get { return _verify_comment; }
        set { _verify_comment = value; }
    }
    private string _upload_comment = "";
    public string Upload_comment
    {
        get { return _upload_comment; }
        set { _upload_comment = value; }
    }
    private string _facilityname = "";
    public string Facilityname
    {
        get { return _facilityname; }
        set { _facilityname = value; }
    }
    private string _importtype = "";
    public string Importtype
    {
        get { return _importtype; }
        set { _importtype = value; }
    }
    private int _ihid;
    public int Ihid
    {
        get { return _ihid; }
        set { _ihid = value; }
    }
    private int _loadrows;
    public int Loadrows
    {
        get { return _loadrows; }
        set { _loadrows = value; }
    }
    private int _ignorerows;
    public int Ignorerows
    {
        get { return _ignorerows; }
        set { _ignorerows = value; }
    }
    #endregion
    public ImportHistInfo()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public ImportHistInfo(DateTime loadtime, int loadrows, int ignorerows, int ihid, string verify_comment, string upload_comment, string facilityname, string importtype)
    {
        this.Loadtime = loadtime;
        this.Loadrows = loadrows;
        this.Ignorerows = ignorerows;
        this.Ihid = ihid;
        this.Verify_comment = verify_comment;
        this.Upload_comment = upload_comment;
        this.Facilityname = facilityname;
        this.Importtype = importtype;
    }
}
#endregion
#region SQLPRProvider and PRProvider
namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLProvider
        public List<ImportHistInfo> GetImportHist(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from ImportHist";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    // default to TaskID order if none specified
                    cSQLCommand = cSQLCommand +
                        " order by ihid ";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetImportHistCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        #endregion
        #region PRProvider
        /////////////////////////////////////////////////////////
        // methods that work with CETaskSQL

        /// <summary>
        /// Returns a new CETaskInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual ImportHistInfo GetImportHistFromReader(IDataReader reader)
        {
            return GetImportHistFromReader(reader, true);
        }
        protected virtual ImportHistInfo GetImportHistFromReader(IDataReader reader, bool readMemos)
        {
            ImportHistInfo ImportHist = new ImportHistInfo(
                (DateTime)(Convert.IsDBNull(reader["loadtime"])
                  ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["loadtime"]),
              (int)reader["loadrows"],
            (int)reader["ignorerows"],
            (int)reader["ihid"],
              reader["verify_comment"].ToString(),
              reader["upload_comment"].ToString(),
              reader["facilityname"].ToString(),
              reader["importtype"].ToString());
            return ImportHist;
        }

        /// <summary>
        /// Returns a collection of CETaskInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<ImportHistInfo> GetImportHistCollectionFromReader(IDataReader reader)
        {
            return GetImportHistCollectionFromReader(reader, true);
        }
        protected virtual List<ImportHistInfo> GetImportHistCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<ImportHistInfo> ImportHist = new List<ImportHistInfo>();
            while (reader.Read())
                ImportHist.Add(GetImportHistFromReader(reader, readMemos));
            return ImportHist;
        }

        #endregion
    }
}
#endregion
