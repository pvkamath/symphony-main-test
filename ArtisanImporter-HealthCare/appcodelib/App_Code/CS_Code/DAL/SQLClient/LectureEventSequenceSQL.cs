﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Collections.Generic;
using System.Data.SqlClient;


#region LectureEventSequenceInfo

namespace PearlsReview.DAL
{
    public class LectureEventSequenceInfo
    {
        public LectureEventSequenceInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public LectureEventSequenceInfo(int LectSeqID, int LectEvtID, int Sequence, int TopicID, int ResourceID1, 
            int ResourceID2, int ResourceID3, int ResourceID4, int ResourceID5, int resourceID6, int resourceID7, 
            int resourceID8, int resourceID9, int resourceID10)
        {
            this.LectSeqID = LectSeqID;
            this.LectEvtID = LectEvtID;
            this.Sequence = Sequence;
            this.TopicID = TopicID;
            this.ResourceID1 = ResourceID1;
            this.ResourceID2 = ResourceID2;
            this.ResourceID3 = ResourceID3;
            this.ResourceID4 = ResourceID4;
            this.ResourceID5 = ResourceID5;
            this.ResourceID6 = resourceID6;
            this.ResourceID7 = resourceID7;
            this.ResourceID8 = resourceID8;
            this.ResourceID9 = resourceID9;
            this.ResourceID10 = resourceID10;
        }

        private int _LectSeqID = 0;
        public int LectSeqID
        {
            get { return _LectSeqID; }
            set { _LectSeqID = value; }
        }

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            protected set { _LectEvtID = value; }
        }

        private int _Sequence = 0;
        public int Sequence
        {
            get { return _Sequence; }
            set { _Sequence = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _ResourceID1 = 0;
        public int ResourceID1
        {
            get { return _ResourceID1; }
            set { _ResourceID1 = value; }
        }

        private int _ResourceID2 = 0;
        public int ResourceID2
        {
            get { return _ResourceID2; }
            set { _ResourceID2 = value; }
        }

        private int _ResourceID3 = 0;
        public int ResourceID3
        {
            get { return _ResourceID3; }
            set { _ResourceID3 = value; }
        }

        private int _ResourceID4 = 0;
        public int ResourceID4
        {
            get { return _ResourceID4; }
            set { _ResourceID4 = value; }
        }

        private int _ResourceID5 = 0;
        public int ResourceID5
        {
            get { return _ResourceID5; }
            set { _ResourceID5 = value; }
        }

        private int _ResourceID6 = 0;
        public int ResourceID6
        {
            get { return _ResourceID6; }
            set { _ResourceID6 = value; }
        }

        private int _ResourceID7 = 0;
        public int ResourceID7
        {
            get { return _ResourceID7; }
            set { _ResourceID7 = value; }
        }

        private int _ResourceID8 = 0;
        public int ResourceID8
        {
            get { return _ResourceID8; }
            set { _ResourceID8 = value; }
        }

        private int _ResourceID9 = 0;
        public int ResourceID9
        {
            get { return _ResourceID9; }
            set { _ResourceID9 = value; }
        }

        private int _ResourceID10 = 0;
        public int ResourceID10
        {
            get { return _ResourceID10; }
            set { _ResourceID10 = value; }
        }
    }
}
#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        public List<LectureEventSequenceInfo> GetLectureEventSequenceDetailsByTopicID(int topicID)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                String cSQLCommand = "select * From LectureEventSequence " +
                            "where topicID =  @topicID ";

             
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@topicID", SqlDbType.Int).Value = topicID;

                cn.Open();
                return GetLectureEventSequenceCollectionFromReader(ExecuteReader(cmd), false);

            }
        }

        public List<LectureEventSequenceInfo> GetLectureEventSequenceDetailsBylectevtid(int lectevtid)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                String cSQLCommand = "select * From LectureEventSequence " +
                            "where lectevtid =  @lectevtid ";


                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@lectevtid", SqlDbType.Int).Value = lectevtid;

                cn.Open();
                return GetLectureEventSequenceCollectionFromReader(ExecuteReader(cmd), false);

            }
        }


        public DataSet GetLectureTopicAndResourceDetailsByEventID(int lectEvtID)
        {
            DataSet myDataset = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                String cSQLCommand = "SELECT * from LectureEventSequence where lectevtid = @lectEvtID order by LectureEventSequence.sequence";

                //String cSQLCommand = "SELECT     LectureEventTopicLink.lectevttopid, LectureEventTopicLink.lectevtid, LectureEventTopicLink.topicid, LectureEventSequence.sequence, " +
                //                    " LectureEventSequence.resourceid1, LectureEventSequence.resourceid2, LectureEventSequence.resourceid3, LectureEventSequence.lectseqid " +
                //                    " FROM         LectureEventTopicLink LEFT OUTER JOIN " +
                //                    " LectureEventSequence ON LectureEventTopicLink.lectevtid = LectureEventSequence.lectevtid" +
                //                    " WHERE     (LectureEventTopicLink.lectevtid = @lectEvtID)";

                        SqlCommand cmd = new SqlCommand(cSQLCommand, cn);         
                        cmd.Parameters.Add("@lectEvtID", SqlDbType.Int).Value = lectEvtID;
                        cn.Open();
                        SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd);      
                        sqlAdapter.Fill(myDataset);
            
            }
        
            return myDataset;
        }


        public DataSet GetLectureTopicAndResourceInfoByLecEvtID(int lectEvtID)
        {
            DataSet myDataset = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                String cSQLCommand = "SELECT les.*, t.topicname, t.course_number from LectureEventSequence les " +
                    " join Topic t on les.topicid = t.topicid where lectevtid = @lectEvtID order by les.sequence";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@lectEvtID", SqlDbType.Int).Value = lectEvtID;
                cn.Open();
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd);
                sqlAdapter.Fill(myDataset);

            }

            return myDataset;
        }

        /// <summary>
        /// Inserts a new LectureEventSequence
        /// </summary>
        public int InsertLectureEventSequence(LectureEventSequenceInfo LectureEventSequence)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into LectureEventSequence " +
                    "(lectevtid, " + 
                    "sequence, " +
                    "topicid, " +
                    "resourceid1, " +
                    "resourceid2, " +
                    "resourceid3," +
                    "resourceid4, " +
                    "resourceid5, " +
                    "resourceid6, " +
                    "resourceid7, " +
                    "resourceid8, " +
                    "resourceid9, " +
                    "resourceid10)" +
                    "VALUES (" +
                    "@LectEvtID, " + 
                    "@Sequence, " +
                    "@TopicID, " +
                    "@ResourceID1, " +
                    "@ResourceID2, " +
                    "@ResourceID3, " +
                    "@ResourceID4, " +
                    "@ResourceID5, " +
                    "@ResourceID6, " +
                    "@ResourceID7, " +
                    "@ResourceID8, " +
                    "@ResourceID9, " +
                    "@ResourceID10 ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@lectevtid", SqlDbType.Int).Value = LectureEventSequence.LectEvtID;
                cmd.Parameters.Add("@sequence", SqlDbType.Int).Value = LectureEventSequence.Sequence;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = LectureEventSequence.TopicID;
                cmd.Parameters.Add("@resourceid1", SqlDbType.Int).Value = LectureEventSequence.ResourceID1;
                cmd.Parameters.Add("@resourceid2", SqlDbType.Int).Value = LectureEventSequence.ResourceID2;
                cmd.Parameters.Add("@resourceid3", SqlDbType.Int).Value = LectureEventSequence.ResourceID3;
                cmd.Parameters.Add("@resourceid4", SqlDbType.Int).Value = LectureEventSequence.ResourceID4;
                cmd.Parameters.Add("@resourceid5", SqlDbType.Int).Value = LectureEventSequence.ResourceID5;
                cmd.Parameters.Add("@resourceid6", SqlDbType.Int).Value = LectureEventSequence.ResourceID6;
                cmd.Parameters.Add("@resourceid7", SqlDbType.Int).Value = LectureEventSequence.ResourceID7;
                cmd.Parameters.Add("@resourceid8", SqlDbType.Int).Value = LectureEventSequence.ResourceID8;
                cmd.Parameters.Add("@resourceid9", SqlDbType.Int).Value = LectureEventSequence.ResourceID9;
                cmd.Parameters.Add("@resourceid10", SqlDbType.Int).Value = LectureEventSequence.ResourceID10;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

         /// <summary>
        /// Updates a LectureEventSequence
        /// </summary>
        
          public  bool UpdateLectureEventSequence(LectureEventSequenceInfo LectureEventSequence)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update LectureEventSequence set " +
                    " lectevtid = @lectevtid, " + 
                    "sequence = @Sequence, " +                   
                    "resourceid1 = @ResourceID1, " +
                    "resourceid2 = @ResourceID2, " +
                    "resourceid3 = @ResourceID3, " +
                    "resourceid4 = @ResourceID4, " +
                    "resourceid5 = @ResourceID5, " +
                    "resourceid6 = @ResourceID6, " +
                    "resourceid7 = @ResourceID7, " +
                    "resourceid8 = @ResourceID8, " +
                    "resourceid9 = @ResourceID9, " +
                    "resourceid10 = @ResourceID10 " +
                    "where lectseqid = @LectSeqID  and topicid = @TopicID ", cn);

                cmd.Parameters.Add("@lectevtid", SqlDbType.Int).Value = LectureEventSequence.LectEvtID;
                cmd.Parameters.Add("@sequence", SqlDbType.Int).Value = LectureEventSequence.Sequence;
                cmd.Parameters.Add("@resourceid1", SqlDbType.Int).Value = LectureEventSequence.ResourceID1;
                cmd.Parameters.Add("@resourceid2", SqlDbType.Int).Value = LectureEventSequence.ResourceID2;
                cmd.Parameters.Add("@resourceid3", SqlDbType.Int).Value = LectureEventSequence.ResourceID3;
                cmd.Parameters.Add("@resourceid4", SqlDbType.Int).Value = LectureEventSequence.ResourceID4;
                cmd.Parameters.Add("@resourceid5", SqlDbType.Int).Value = LectureEventSequence.ResourceID5;
                cmd.Parameters.Add("@resourceid6", SqlDbType.Int).Value = LectureEventSequence.ResourceID6;
                cmd.Parameters.Add("@resourceid7", SqlDbType.Int).Value = LectureEventSequence.ResourceID7;
                cmd.Parameters.Add("@resourceid8", SqlDbType.Int).Value = LectureEventSequence.ResourceID8;
                cmd.Parameters.Add("@resourceid9", SqlDbType.Int).Value = LectureEventSequence.ResourceID9;
                cmd.Parameters.Add("@resourceid10", SqlDbType.Int).Value = LectureEventSequence.ResourceID10;

                cmd.Parameters.Add("@LectSeqID", SqlDbType.Int).Value = LectureEventSequence.LectSeqID;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = LectureEventSequence.TopicID;
             

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);

            }

        }


          public bool DeleteLectureSequence(int lectSeqID)
          {
              using (SqlConnection cn = new SqlConnection(this.ConnectionString))
              {
                  SqlCommand cmd = new SqlCommand("delete from LectureEventSequence " +                     
                      "where lectseqid = @LectSeqID ", cn);


                  cmd.Parameters.Add("@LectSeqID", SqlDbType.Int).Value = lectSeqID;


               
                  cn.Open();
                  int ret = ExecuteNonQuery(cmd);
                  return (ret == 1);

              }
          }

          public bool DeleteLectureEventSequenceByTopicID(int TopicID)
          {
              using (SqlConnection cn = new SqlConnection(this.ConnectionString))
              {
                  SqlCommand cmd = new SqlCommand("delete from LectureEventSequence " +
                      "where TopicID = @TopicID ", cn);


                  cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;



                  cn.Open();
                  int ret = ExecuteNonQuery(cmd);
                  return (ret == 1);

              }
          }

          public bool UnAssignResourcesForLectEvtID(int ResourceID)
          {
              using (SqlConnection cn = new SqlConnection(this.ConnectionString))
              {
                  SqlCommand cmd = new SqlCommand("UPDATE LectureEventSequence " +
                                " SET resourceid1 =  case  when resourceid1 =@ResourceID then null else resourceid1 end , " +
                                "     resourceid2 =  case  when resourceid2 = @ResourceID then null else resourceid2 end , " +
                                "     resourceid3 =  case  when resourceid3 = @ResourceID then null else resourceid3 end ,  " +
                                "     resourceid4 =  case  when resourceid4 = @ResourceID then null else resourceid4 end ,  " +
                                "     resourceid5 =  case  when resourceid5 = @ResourceID then null else resourceid5 end,   " +
                                "     resourceid6 =  case  when resourceid6 =@ResourceID then null else resourceid6 end , " +
                                "     resourceid7 =  case  when resourceid7 = @ResourceID then null else resourceid7 end , " +
                                "     resourceid8 =  case  when resourceid8 = @ResourceID then null else resourceid8 end ,  " +
                                "     resourceid9 =  case  when resourceid9 = @ResourceID then null else resourceid9 end ,  " +
                                "     resourceid10 =  case  when resourceid10 = @ResourceID then null else resourceid10 end   " +
                                " WHERE resourceid1 = @ResourceID or resourceid2 = @ResourceID or resourceid3 = @ResourceID " + 
                                " or resourceid4 = @ResourceID or resourceid5 = @ResourceID " +
                                " or resourceid6 = @ResourceID or resourceid7 = @ResourceID or resourceid8 = @ResourceID " +
                                " or resourceid9 = @ResourceID or resourceid10 = @ResourceID ", cn);


                 // cmd.Parameters.Add("@LectSeqID", SqlDbType.Int).Value = LectSeqID;
                  cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = ResourceID;



                  cn.Open();
                  int ret = ExecuteNonQuery(cmd);
                  return (ret == 1);

              }
          }

        #endregion

        #region PRProvider

        /////////////////////////////////////////////////////////
        // methods that work with LectureSequence  

        /// <summary>
        /// Returns a new LectureEventSequenceInfo instance filled with the DataReader's current record data
        /// </summary>

          protected virtual LectureEventSequenceInfo GetLectureEventSequenceFromReader(IDataReader reader)
        {
            return GetLectureEventSequenceFromReader(reader,true);
        }

        protected virtual LectureEventSequenceInfo GetLectureEventSequenceFromReader(IDataReader reader, bool readMemos)
        {
            LectureEventSequenceInfo LectureEventSequence = new LectureEventSequenceInfo(
            (int)reader["LectSeqID"],
            (int)reader["LectEvtID"],
            (int)reader["Sequence"],
            (int)reader["TopicID"],
            (int)reader["ResourceID1"],
            (int)reader["ResourceID2"],
            (int)reader["ResourceID3"],
            (int)reader["ResourceID4"],
            (int)reader["ResourceID5"], 
            (int)reader["ResourceID6"],
            (int)reader["ResourceID7"],
            (int)reader["ResourceID8"],
            (int)reader["ResourceID9"],
            (int)reader["ResourceID10"]            
            );
            return LectureEventSequence;
        }

        /// <summary>
        /// Returns a collection of LectureEventSequenceInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<LectureEventSequenceInfo> GetLectureEventSequenceCollectionFromReader(IDataReader reader)
        {
            return GetLectureEventSequenceCollectionFromReader(reader, true);
        }
        protected virtual List<LectureEventSequenceInfo> GetLectureEventSequenceCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<LectureEventSequenceInfo> LectureEventSequences = new List<LectureEventSequenceInfo>();
            while (reader.Read())
                LectureEventSequences.Add(GetLectureEventSequenceFromReader(reader, readMemos));
            return LectureEventSequences;
        }

        #endregion



    }
}
#endregion