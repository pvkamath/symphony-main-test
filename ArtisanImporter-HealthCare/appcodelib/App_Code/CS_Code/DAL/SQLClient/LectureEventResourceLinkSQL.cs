﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region LectureEventResourceLinkSQL

namespace PearlsReview.DAL
{
    public class LectureEventResourceLinkInfo
    {
        public LectureEventResourceLinkInfo() { }

        public LectureEventResourceLinkInfo(int LectEvtResID, int LectEvtID, int ResourceID)
        {
            this.LectEvtResID = LectEvtResID;
            this.LectEvtID = LectEvtID;
            this.ResourceID = ResourceID;
        }

        private int _LectEvtResID = 0;
        public int LectEvtResID
        {
            get { return _LectEvtResID; }
            protected set { _LectEvtResID = value; }
        }

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            private set { _LectEvtID = value; }
        }

        private int _ResourceID = 0;
        public int ResourceID
        {
            get { return _ResourceID; }
            private set { _ResourceID = value; }
        }

    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider


        /////////////////////////////////////////////////////////
        // methods that work with LectureEventResourceLinks

        /// <summary>
        /// Returns the total number of LectureEventResourceLinks
        /// </summary>
        public int GetLectureEventResourceLinkCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from LectureEventResourceLink", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all LectureEventResourceLinks
        /// </summary>
        public List<LectureEventResourceLinkInfo> GetLectureEventResourceLinks(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from LectureEventResourceLink";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetLectureEventResourceLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<LectureEventResourceLinkInfo> GetLectureEventResourceLinksByLectEvtID(int LectEvtID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from LectureEventResourceLink where LectEvtID = @LectEvtID";

               SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectEvtID;
            
                cn.Open();
                return GetLectureEventResourceLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

       
        /// <summary>
        /// Retrieves the LectureEventResourceLink with the specified ID
        /// </summary>
        public LectureEventResourceLinkInfo GetLectureEventResourceLinkByID(int LectEvtResID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from LectureEventResourceLink where LectEvtResID=@LectEvtResID ", cn);
                cmd.Parameters.Add("@LectEvtResID", SqlDbType.Int).Value = LectEvtResID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetLectureEventResourceLinkFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a LectureEventResourceLink
        /// </summary>
        public bool DeleteLectureEventResourceLink(int LectEvtResID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from LectureEventResourceLink where LectEvtResID=@LectEvtResID", cn);
                cmd.Parameters.Add("@LectEvtResID", SqlDbType.Int).Value = LectEvtResID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new LectureEventResourceLink
        /// </summary>
        public int InsertLectureEventResourceLink(LectureEventResourceLinkInfo LectureEventResourceLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into LectureEventResourceLink " +
              "(LectEvtID, " +
              "ResourceID) " +
              "VALUES (" +
              "@LectEvtID, " +
              "@ResourceID) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectureEventResourceLink.LectEvtID;
                cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = LectureEventResourceLink.ResourceID;
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        public int InsertLectureEventResourceLink(int lectEvtID ,int resourceID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into LectureEventResourceLink " +
              "(LectEvtID, " +
              "ResourceID) " +
              "VALUES (" +
              "@LectEvtID, " +
              "@ResourceID) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = lectEvtID;
                cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = resourceID;
               
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }
        /// <summary>
        /// Updates a LectureEventResourceLink
        /// </summary>
        public bool UpdateLectureEventResourceLink(LectureEventResourceLinkInfo LectureEventResourceLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update LectureEventResourceLink set " +
              "LectEvtID = @LectEvtID, " +
              "ResourceID = @ResourceID " +
              "where LectEvtResID = @LectEvtResID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectureEventResourceLink.LectEvtID;
                cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = LectureEventResourceLink.ResourceID;
                cmd.Parameters.Add("@LectEvtResID", SqlDbType.Int).Value = LectureEventResourceLink.LectEvtResID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        public bool UpdateLectureEventResourcePersonAssignments(int LectEvtID,
            string ResourceIDAssignments)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {


                // NOTE; For now, just delete them all
                SqlCommand cmd = new SqlCommand(
                    "DELETE FROM LectureEventResourceLink " +
                    "where LectEvtID = @LectEvtID ", cn);

                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectEvtID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                //              return (ret == 1);

                if (ResourceIDAssignments.Trim().Length < 1)
                    // no assignments, so nothing more to do
                    return true;

                // now step through all ResourceIDs in the list passed in and call
                // this.InsertLectureEventResourceLink() for each, which will insert the assignment
                // if it does not already exist
                string[] ResourceIDs = ResourceIDAssignments.Split(',');

                LectureEventResourceLinkInfo LectureEventResourceLink;

                foreach (string cResourceID in ResourceIDs)
                {
                    LectureEventResourceLink = new LectureEventResourceLinkInfo(0, LectEvtID, Int32.Parse(cResourceID));
                    this.InsertLectureEventResourceLink(LectureEventResourceLink);
                }

                return true;
            }
        }

        public int CheckForLectureEventResource(string LectEvtID)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string str = "SELECT  COUNT(LectEvtID) FROM  LectureEventResourceLink WHERE (LectEvtID = " + LectEvtID + ")";

                SqlCommand cmd = new SqlCommand(str, cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }

        }

        #endregion

        #region PRProvider

        /////////////////////////////////////////////////////////
        // methods that work with LectureEventResourceLinks
      
        /// <summary>
        /// Returns a new LectureEventResourceLinkInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual LectureEventResourceLinkInfo GetLectureEventResourceLinkFromReader(IDataReader reader)
        {
            return GetLectureEventResourceLinkFromReader(reader, true);
        }
        protected virtual LectureEventResourceLinkInfo GetLectureEventResourceLinkFromReader(IDataReader reader, bool readMemos)
        {
            LectureEventResourceLinkInfo LectureEventResourceLink = new LectureEventResourceLinkInfo(
              (int)reader["LectEvtResID"],
              (int)reader["LectEvtID"],
              (int)reader["ResourceID"]);


            return LectureEventResourceLink;
        }

        /// <summary>
        /// Returns a collection of LectureEventResourceLinkInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<LectureEventResourceLinkInfo> GetLectureEventResourceLinkCollectionFromReader(IDataReader reader)
        {
            return GetLectureEventResourceLinkCollectionFromReader(reader, true);
        }
        protected virtual List<LectureEventResourceLinkInfo> GetLectureEventResourceLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<LectureEventResourceLinkInfo> LectureEventResourceLinks = new List<LectureEventResourceLinkInfo>();
            while (reader.Read())
                LectureEventResourceLinks.Add(GetLectureEventResourceLinkFromReader(reader, readMemos));
            return LectureEventResourceLinks;
        }


        #endregion
    }
}
#endregion
