﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region LectureDefinitionInfo

namespace PearlsReview.DAL
{
    public class LectureDefinitionInfo
    {
        public LectureDefinitionInfo() { }

        public LectureDefinitionInfo(int LectureID, string LectTitle, int CertID, string Comment)
        {
            this.LectureID = LectureID;
            this.LectTitle = LectTitle;
            this.CertID = CertID;
            this.Comment = Comment;
        }

        private int _LectureID = 0;
        public int LectureID
        {
            get { return _LectureID; }
            protected set { _LectureID = value; }
        }

        private string _LectTitle = "";
        public string LectTitle
        {
            get { return _LectTitle; }
            private set { _LectTitle = value; }
        }

        private int _CertID = 0;
        public int CertID
        {
            get { return _CertID; }
            private set { _CertID = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            private set { _Comment = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with LectureDefinitions

        /// <summary>
        /// Returns the total number of LectureDefinitions
        /// </summary>
        public  int GetLectureDefinitionCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from LectureDefinition", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all LectureDefinitions
        /// </summary>
        public  List<LectureDefinitionInfo> GetLectureDefinitions(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from LectureDefinition";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetLectureDefinitionCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the LectureDefinition with the specified ID
        /// </summary>
        public  LectureDefinitionInfo GetLectureDefinitionByID(int LectureID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from LectureDefinition where LectureID=@LectureID", cn);
                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetLectureDefinitionFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a LectureDefinition
        /// </summary>
        public  bool DeleteLectureDefinition(int LectureID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from LectureDefinition where LectureID=@LectureID", cn);
                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new LectureDefinition
        /// </summary>
        public  int InsertLectureDefinition(LectureDefinitionInfo LectureDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into LectureDefinition " +
              "(LectTitle, " +
              "CertID, " +
              "Comment) " +
              "VALUES (" +
              "@LectTitle, " +
              "@CertID, " +
              "@Comment) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@LectTitle", SqlDbType.VarChar).Value = LectureDefinition.LectTitle;
                cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = LectureDefinition.CertID;
                cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = LectureDefinition.Comment;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a LectureDefinition
        /// </summary>
        public  bool UpdateLectureDefinition(LectureDefinitionInfo LectureDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update LectureDefinition set " +
              "LectTitle = @LectTitle, " +
              "CertID = @CertID, " +
              "Comment = @Comment " +
              "where LectureID = @LectureID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@LectTitle", SqlDbType.VarChar).Value = LectureDefinition.LectTitle;
                cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = LectureDefinition.CertID;
                cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = LectureDefinition.Comment;
                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureDefinition.LectureID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public int CheckForLectureTopic(string LectureID)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string str = "SELECT  COUNT(lectureid) FROM  LectureTopicLink WHERE (lectureid = " + LectureID + ")";

                SqlCommand cmd = new SqlCommand(str, cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }

        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new LectureDefinitionInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual LectureDefinitionInfo GetLectureDefinitionFromReader(IDataReader reader)
        {
            return GetLectureDefinitionFromReader(reader, true);
        }
        protected virtual LectureDefinitionInfo GetLectureDefinitionFromReader(IDataReader reader, bool readMemos)
        {
            LectureDefinitionInfo LectureDefinition = new LectureDefinitionInfo(
              (int)reader["LectureID"],
              reader["LectTitle"].ToString(),
              (int)reader["CertID"],
              reader["Comment"].ToString());


            return LectureDefinition;
        }

        /// <summary>
        /// Returns a collection of LectureDefinitionInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<LectureDefinitionInfo> GetLectureDefinitionCollectionFromReader(IDataReader reader)
        {
            return GetLectureDefinitionCollectionFromReader(reader, true);
        }
        protected virtual List<LectureDefinitionInfo> GetLectureDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<LectureDefinitionInfo> LectureDefinitions = new List<LectureDefinitionInfo>();
            while (reader.Read())
                LectureDefinitions.Add(GetLectureDefinitionFromReader(reader, readMemos));
            return LectureDefinitions;
        }
        #endregion
    }
}
#endregion