﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region CategoryAreaLinkInfo

namespace PearlsReview.DAL
{
    public class CategoryAreaLinkInfo
    {
        public CategoryAreaLinkInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private int _areaid = 0;
        public int areaid
        {
            get { return _areaid; }
            protected set { _areaid = value; }
        }

        private int _categoryid = 0;
        public int categoryid
        {
            get { return _categoryid; }
            protected set { _categoryid = value; }
        }

        public CategoryAreaLinkInfo(int categoryid, int areaid)
        {
            this.areaid = areaid;
            this.categoryid = categoryid;
        }
    }
}
#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /// <summary>        
        /// Retrieves all  Categoryarealink Info for the specified Category id
        /// </summary>
        public  List<CategoryAreaLinkInfo> GetCategoryAreaLinkByCategoryId(int Categoryid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from CategoryAreaLink " +
                    "where Categoryid = @CategoryID " +
                    "order by areaid DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@CategoryID", SqlDbType.VarChar).Value = Categoryid;
                cn.Open();
                return GetCategoryAreaLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public  int UpdateCategoryAreaLink(CategoryAreaLinkInfo CategoryAreaLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into CategoryAreaLink " +
                    "(Categoryid, " +
                    "areaid )" +
                    "VALUES (" +
                    "@CategoryId, " +
                    "@AreaId)", cn);
                cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = CategoryAreaLink.categoryid;
                cmd.Parameters.Add("@AreaId", SqlDbType.Int).Value = CategoryAreaLink.areaid;

                //SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                //IDParameter.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                //int NewID = (int)IDParameter.Value;
                return ret;
            }
            throw new NotImplementedException();
        }

        public  bool DeleteCategoryAreaLink(int Categoryid, int areaid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Categoryarealink where Categoryid=@CategoryId And areaid=@AreaId", cn);
                cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = Categoryid;
                cmd.Parameters.Add("@AreaId", SqlDbType.Int).Value = areaid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider

        protected virtual List<CategoryAreaLinkInfo> GetCategoryAreaLinkCollectionFromReader(IDataReader reader)
        {
            return GetCategoryAreaLinkCollectionFromReader(reader, true);
        }

        protected virtual List<CategoryAreaLinkInfo> GetCategoryAreaLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<CategoryAreaLinkInfo> CategoryAreaLinks = new List<CategoryAreaLinkInfo>();
            while (reader.Read())
                CategoryAreaLinks.Add(GetCategoryAreaLinkFromReader(reader, readMemos));
            return CategoryAreaLinks;
        }

        protected virtual CategoryAreaLinkInfo GetCategoryAreaLinkFromReader(IDataReader reader)
        {
            return GetCategoryAreaLinkFromReader(reader, true);
        }
        
        protected virtual CategoryAreaLinkInfo GetCategoryAreaLinkFromReader(IDataReader reader, bool readMemos)
        {
            // NOTE: NULL integer foreign key LectEvtID is replaced with value of
            // zero, which will trigger "Please select..." or "(Not Selected)"
            // message in UI dropdown for that field
            CategoryAreaLinkInfo CategoryAreaLink = new CategoryAreaLinkInfo(
              (int)reader["Categoryid"],
              (int)reader["areaid"]);

            return CategoryAreaLink;
        }
        #endregion
    }
}
#endregion


