﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region SnippetInfo

namespace PearlsReview.DAL
{
    public class SnippetInfo
    {
        public SnippetInfo() { }

        public SnippetInfo(int SnippetID, string SnippetKey, string SnippetText)
        {
            this.SnippetID = SnippetID;
            this.SnippetKey = SnippetKey;
            this.SnippetText = SnippetText;
        }

        private int _SnippetID = 0;
        public int SnippetID
        {
            get { return _SnippetID; }
            protected set { _SnippetID = value; }
        }

        private string _SnippetKey = "";
        public string SnippetKey
        {
            get { return _SnippetKey; }
            private set { _SnippetKey = value; }
        }

        private string _SnippetText = "";
        public string SnippetText
        {
            get { return _SnippetText; }
            private set { _SnippetText = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider


        /////////////////////////////////////////////////////////
        // methods that work with Snippets

        /// <summary>
        /// Returns the total number of Snippets
        /// </summary>
        public  int GetSnippetCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Snippets", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Snippets
        /// </summary>
        public  List<SnippetInfo> GetSnippets(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Snippets";

                // add on ORDER BY if provided
                if (cSortExpression.Length == 0)
                {
                    cSortExpression = "snippetkey";
                }

                cSQLCommand = cSQLCommand +
                    " order by " + cSortExpression;

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetSnippetCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Snippet with the specified ID
        /// </summary>
        public  SnippetInfo GetSnippetByID(int SnippetID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Snippets where SnippetID=@SnippetID", cn);
                cmd.Parameters.Add("@SnippetID", SqlDbType.Int).Value = SnippetID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSnippetFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the Snippet with the specified Message Key
        /// </summary>
        public  SnippetInfo GetSnippetBySnippetKey(string SnippetKey)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Snippets where snippetkey=@SnippetKey", cn);
                cmd.Parameters.Add("@SnippetKey", SqlDbType.VarChar).Value = SnippetKey;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSnippetFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a Snippet
        /// </summary>
        public  bool DeleteSnippet(int SnippetID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Snippets where SnippetID=@SnippetID", cn);
                cmd.Parameters.Add("@SnippetID", SqlDbType.Int).Value = SnippetID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Snippet
        /// </summary>
        public  int InsertSnippet(SnippetInfo Snippet)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Snippets " +
              "(SnippetKey, " +
              "SnippetText) " +
              "VALUES (" +
              "@SnippetKey, " +
              "@SnippetText) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@SnippetKey", SqlDbType.VarChar).Value = Snippet.SnippetKey;
                cmd.Parameters.Add("@SnippetText", SqlDbType.Text).Value = Snippet.SnippetText;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Snippet
        /// </summary>
        public  bool UpdateSnippet(SnippetInfo Snippet)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Snippets set " +
              "SnippetKey = @SnippetKey, " +
               "SnippetText = @SnippetText " +
              "where SnippetID = @SnippetID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SnippetKey", SqlDbType.VarChar).Value = Snippet.SnippetKey;
                cmd.Parameters.Add("@SnippetText", SqlDbType.Text).Value = Snippet.SnippetText;
                cmd.Parameters.Add("@SnippetID", SqlDbType.Int).Value = Snippet.SnippetID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new SnippetInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual SnippetInfo GetSnippetFromReader(IDataReader reader)
        {
            return GetSnippetFromReader(reader, true);
        }
        protected virtual SnippetInfo GetSnippetFromReader(IDataReader reader, bool readMemos)
        {
            SnippetInfo Snippet = new SnippetInfo(
              (int)reader["SnippetID"],
              reader["SnippetKey"].ToString(),
              reader["SnippetText"].ToString());


            return Snippet;
        }

        /// <summary>
        /// Returns a collection of SnippetInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<SnippetInfo> GetSnippetCollectionFromReader(IDataReader reader)
        {
            return GetSnippetCollectionFromReader(reader, true);
        }
        protected virtual List<SnippetInfo> GetSnippetCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<SnippetInfo> Snippets = new List<SnippetInfo>();
            while (reader.Read())
                Snippets.Add(GetSnippetFromReader(reader, readMemos));
            return Snippets;
        }
        #endregion
    }
}
#endregion