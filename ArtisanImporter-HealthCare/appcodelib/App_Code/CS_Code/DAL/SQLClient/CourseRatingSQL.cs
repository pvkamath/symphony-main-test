﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

# region CourseRatingInfo

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for CourseRatingSQL
    /// </summary>
    public class CourseRatingInfo
    {
        public CourseRatingInfo()
        {
           
        }
        public CourseRatingInfo(int crid, int topicid, int userid, DateTime ratingdate, int rating, bool active_ind, string comment,
            bool recommend_ind, int helpful_yes, int helpful_no, string location, string flag_status, string editor_response, int editorid, DateTime editor_cdate, int flagid)
        {
            this.crid = crid;
            this.topicid = topicid;
            this.userid = userid;
            this.ratingdate = ratingdate;
            this.rating = rating;
            this.active_ind = active_ind;
            this.comment = comment;
            this.recommend_ind = recommend_ind;
            this.helpful_yes = helpful_yes;
            this.helpful_no = helpful_no;
            this.location = location;
            this.flag_status = flag_status;
            this.editor_response = editor_response;
            this.editorid = editorid;
            this.editor_cdate = editor_cdate;
            this.flagid = flagid;
        }
        private int _crid = 0;
        public int crid
        {
            get { return _crid; }
            protected set { _crid = value; }
        }

        private int _topicid = 0;
        public int topicid
        {
            get { return _topicid; }
            protected set { _topicid = value; }
        }
        private int _userid = 0;
        public int userid
        {
            get { return _userid; }
            protected set { _userid = value; }
        }

        private DateTime _ratingdate = System.DateTime.Now;
        public DateTime ratingdate
        {
            get { return _ratingdate; }
            private set { _ratingdate = value; }
        }

        private int _rating = 5;
        public int rating
        {
            get { return _rating; }
            protected set { _rating = value; }
        }

        private bool _active_ind = true;
        public bool active_ind
        {
            get { return _active_ind; }
            private set { _active_ind = value; }
        }

        private string _comment = "";
        public string comment
        {
            get { return _comment; }
            private set { _comment = value; }
        }

        private bool _recommend_ind = true;
        public bool recommend_ind
        {
            get { return _recommend_ind; }
            private set { _recommend_ind = value; }
        }

        private int _helpful_yes = 0;
        public int helpful_yes
        {
            get { return _helpful_yes; }
            protected set { _helpful_yes = value; }
        }

        private int _helpful_no = 0;
        public int helpful_no
        {
            get { return _helpful_no; }
            protected set { _helpful_no = value; }
        }
        
        private string _location = "";
        public string location
        {
            get { return _location; }
            private set { _location = value; }
        }

        private string _flag_status = "I";
        public string flag_status
        {
            get { return _flag_status; }
            private set { _flag_status = value; }
        }

        private string _editor_response = "";
        public string editor_response
        {
            get { return _editor_response; }
            private set { _editor_response = value; }
        }


        private int _editorid = 0;
        public int editorid
        {
            get { return _editorid; }
            protected set { _editorid = value; }
        }
        private DateTime _editor_cdate = DateTime.Now;
        public DateTime editor_cdate
        {
           get { return _editor_cdate; }
           set { _editor_cdate = value; }
        }

        private int _flagid = 0;
        public int flagid
        {
            get { return _flagid; }
            set { _flagid = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        /// <summary>
        /// Retrieves all CourseRating
        /// </summary>
        public List<CourseRatingInfo> GetCourseRating(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from CourseRating ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCourseRatingCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public DataTable GetCourseRatingSearchList(string coursenumber, string topicname, string flag_status, string active_ind, string startdate, string enddate, string cSortExpression)
        {
            DataTable dt = new DataTable();
            List<string> WhereCond = new List<string>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                if (!String.IsNullOrEmpty(coursenumber)) WhereCond.Add(String.Format("t.course_number like '%{0}%'", coursenumber.Replace("'","''")));
                if (!String.IsNullOrEmpty(topicname)) WhereCond.Add(String.Format("t.topicname like '%{0}%'", topicname.Replace("'", "''")));
                if (!String.IsNullOrEmpty(flag_status) && flag_status != "ALL") WhereCond.Add(String.Format("c.flag_status like '{0}'", flag_status.Replace("'", "''")));
                if (!String.IsNullOrEmpty(active_ind) && active_ind != "ALL") WhereCond.Add(String.Format("c.active_ind = '{0}'", bool.Parse(active_ind)));
                if (!String.IsNullOrEmpty(startdate) && startdate != "ALL") WhereCond.Add(String.Format("c.ratingdate >= '{0}'", startdate));
                if (!String.IsNullOrEmpty(enddate) && enddate != "ALL") WhereCond.Add(String.Format("c.ratingdate <= '{0}'", enddate));
                
                StringBuilder cSQLCommand = new StringBuilder();
                cSQLCommand.Append("select c.*, t.topicname, t.course_number from CourseRating c join Topic t on c.topicid = t.topicid ");

                if (WhereCond.Count > 0)
                    cSQLCommand.AppendFormat(" Where ({0})", String.Join(") and (", WhereCond.ToArray()));
                if (cSortExpression.Length > 0) cSQLCommand.AppendFormat(" order by {0}", cSortExpression);
                SqlCommand cmd = new SqlCommand(cSQLCommand.ToString(), cn);

                cn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
        
        /// <summary>
        /// Retrieves the CourseRating with the specified ID
        /// </summary>
        public CourseRatingInfo GetCourseRatingByID(int CrId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from CourseRating where CrId=@CrId", cn);
                cmd.Parameters.Add("@CrId", SqlDbType.Int).Value = CrId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCourseRatingFromReader(reader, true);
                else
                    return null;
            }
        }
        public DataTable GetCourseRatingDTByID(int CrId)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select C.*, t.topicname, t.course_number " +
                        "from CourseRating c join Topic t on c.topicid = t.topicid where c.CrId=@CrId", cn);
                cmd.Parameters.Add("@CrId", SqlDbType.Int).Value = CrId;
                cn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
        /// <summary>
        /// Retrieves the DiscountID with the specified TopicID
        /// </summary>
        public int  GetSumCourseRatingByTopicId(int TopicId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                //SqlCommand cmd = new SqlCommand("select * " +
                //        "from Discount where discountid=@DiscountId", cn);

                SqlCommand cmd = new SqlCommand("SELECT sum(rating) from courserating" +
                     " where topicid=@TopicId ", cn);

                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;               

                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }
        /// <summary>
        /// Retrieves all CourseRating
        /// </summary>
        public List<CourseRatingInfo> GetCourseRatingByTopicID(int topicid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand ="select * " +
                         "from CourseRating where topicid=@topicid and active_ind = 1";


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;

                cn.Open();
                return GetCourseRatingCollectionFromReader(ExecuteReader(cmd), false);
               
            }
        }

        /// <summary>
        /// Retrieves all CourseRating
        /// </summary>
        public List<CourseRatingInfo> GetCourseRatingByTopicIDAndComment(int topicid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                         "from CourseRating where topicid=@topicid and active_ind = 1 and len(comment) > 10";


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;

                cn.Open();
                return GetCourseRatingCollectionFromReader(ExecuteReader(cmd), false);

            }
        }
        /// <summary>
        /// Retrieves all CourseRating by userid and topicid
        /// </summary>
        public List<CourseRatingInfo> GetCourseRatingByTopicIDAndUserID(int topicid, int userid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                         "from CourseRating where topicid=@topicid and userid = @userid";


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cn.Open();
                return GetCourseRatingCollectionFromReader(ExecuteReader(cmd), false);

            }
        }
        /// <summary>
        /// Deletes a CourseRating
        /// </summary>
        public bool DeleteCourseRating(int CrId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from CourseRating where CrId=@CrId", cn);
                cmd.Parameters.Add("@CrId", SqlDbType.Int).Value = CrId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new CourseRating
        /// </summary>
        public int InsertCourseRating(CourseRatingInfo CourseRating)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into CourseRating " +
              "(topicid, " +
              "userid, " +
              "ratingdate, " +
              "rating, " +
              "active_ind, " +
              "comment, " +
              "recommend_ind, " +
              "helpful_yes, " +
               "helpful_no, " +
              "location, " +
              "flag_status, " +
              "editor_response, " +
              "editorid, " +
              "editor_cdate, " +
              "flagid) " +
              "VALUES (" +
              "@topicid, " +
              "@userid, " +
              "@ratingdate, " +
              "@rating, " +
              "@active_ind, " +
              "@comment, " +
              "@recommend_ind, " +
              "@helpful_yes, " +
              "@helpful_no, " +
              "@location, " +
              "@flag_status, " +
              "@editor_response, " +              
              "@editorid, "+
              "@editor_cdate, "+
              "@flagid) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = CourseRating.topicid;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = CourseRating.userid;
                cmd.Parameters.Add("@ratingdate", SqlDbType.DateTime).Value = CourseRating.ratingdate;
                cmd.Parameters.Add("@rating", SqlDbType.Int).Value = CourseRating.rating;
                cmd.Parameters.Add("@active_ind", SqlDbType.Bit).Value = CourseRating.active_ind;
                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = CourseRating.comment;
                cmd.Parameters.Add("@recommend_ind", SqlDbType.Bit).Value = CourseRating.recommend_ind;
                cmd.Parameters.Add("@helpful_yes", SqlDbType.Int).Value = CourseRating.helpful_yes;
                cmd.Parameters.Add("@helpful_no", SqlDbType.Int).Value = CourseRating.helpful_no;
                cmd.Parameters.Add("@location", SqlDbType.VarChar).Value = CourseRating.location;
                cmd.Parameters.Add("@flag_status", SqlDbType.Char).Value = CourseRating.flag_status;
                cmd.Parameters.Add("@editor_response", SqlDbType.VarChar).Value = CourseRating.editor_response;
                cmd.Parameters.Add("@editorid", SqlDbType.Int).Value = CourseRating.editorid;
                cmd.Parameters.Add("@editor_cdate", SqlDbType.DateTime).Value = CourseRating.editor_cdate;
                cmd.Parameters.Add("@flagid", SqlDbType.Int).Value = CourseRating.flagid;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a CourseRating
        /// </summary>
        public bool UpdateCourseRating(CourseRatingInfo CourseRating)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update CourseRating set " +
              "topicid = @topicid, " +
              "userid = @userid, " +
              "ratingdate = @ratingdate, " +
              "rating = @rating, " +
              "active_ind = @active_ind, " +
              "comment = @comment, " +
              "recommend_ind = @recommend_ind, " +
              "helpful_yes = @helpful_yes, " +
              "helpful_no = @helpful_no, " +
              "location = @location, " +
              "flag_status = @flag_status, " +
              "editor_response = @editor_response, " +
              "editorid = @editorid, " +
              "editor_cdate = @editor_cdate, " +
              "flagid = @flagid " +
              "where crid = @crid ", cn);

                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = CourseRating.topicid;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = CourseRating.userid;
                cmd.Parameters.Add("@ratingdate", SqlDbType.DateTime).Value = CourseRating.ratingdate;
                cmd.Parameters.Add("@rating", SqlDbType.Int).Value = CourseRating.rating;
                cmd.Parameters.Add("@active_ind", SqlDbType.Bit).Value = CourseRating.active_ind;
                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = CourseRating.comment;
                cmd.Parameters.Add("@recommend_ind", SqlDbType.Bit).Value = CourseRating.recommend_ind;
                cmd.Parameters.Add("@helpful_yes", SqlDbType.Int).Value = CourseRating.helpful_yes;
                cmd.Parameters.Add("@helpful_no", SqlDbType.Int).Value = CourseRating.helpful_no;
                cmd.Parameters.Add("@location", SqlDbType.VarChar).Value = CourseRating.location;
                cmd.Parameters.Add("@flag_status", SqlDbType.Char).Value = CourseRating.flag_status;
                cmd.Parameters.Add("@editor_response", SqlDbType.VarChar).Value = CourseRating.editor_response;
                cmd.Parameters.Add("@editorid", SqlDbType.Int).Value = CourseRating.editorid;
                cmd.Parameters.Add("@crid", SqlDbType.Int).Value = CourseRating.crid;
                cmd.Parameters.Add("@editor_cdate", SqlDbType.DateTime).Value = CourseRating.editor_cdate;
                cmd.Parameters.Add("@flagid", SqlDbType.Int).Value = CourseRating.flagid;
                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new CourseRatingInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual CourseRatingInfo GetCourseRatingFromReader(IDataReader reader)
        {
            return GetCourseRatingFromReader(reader, true);
        }
        protected virtual CourseRatingInfo GetCourseRatingFromReader(IDataReader reader, bool readMemos)
        {
            CourseRatingInfo CourseRating = new CourseRatingInfo(
              (int)reader["crid"],
              (int)(Convert.IsDBNull(reader["topicid"]) ? (int)0 : (int)reader["topicid"]),
              (int)(Convert.IsDBNull(reader["userid"]) ? (int)0 : (int)reader["userid"]),
              (DateTime)reader["ratingdate"],
              (int)(Convert.IsDBNull(reader["rating"]) ? (int)5 : (int)reader["rating"]),
              (bool)reader["active_ind"],
              reader["comment"].ToString(),
              (bool)reader["recommend_ind"],
              (int)(Convert.IsDBNull(reader["helpful_yes"]) ? (int)0 : (int)reader["helpful_yes"]),
              (int)(Convert.IsDBNull(reader["helpful_no"]) ? (int)0 : (int)reader["helpful_no"]),
              reader["location"].ToString(),
              reader["flag_status"].ToString(),
              reader["editor_response"].ToString(),
              (int)(Convert.IsDBNull(reader["editorid"]) ? (int)0 : (int)reader["editorid"]),
              !String.IsNullOrEmpty(reader["editor_cdate"].ToString()) ? (DateTime)reader["editor_cdate"] : DateTime.Now,
              (int)(Convert.IsDBNull(reader["flagid"]) ? (int)0 : (int)reader["flagid"]));

            return CourseRating;
        }

        /// <summary>
        /// Returns a collection of CourseRatingInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<CourseRatingInfo> GetCourseRatingCollectionFromReader(IDataReader reader)
        {
            return GetCourseRatingCollectionFromReader(reader, true);
        }
        protected virtual List<CourseRatingInfo> GetCourseRatingCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<CourseRatingInfo> CourseRating = new List<CourseRatingInfo>();
            while (reader.Read())
                CourseRating.Add(GetCourseRatingFromReader(reader, readMemos));
            return CourseRating;
        }
        #endregion
    }
}
#endregion