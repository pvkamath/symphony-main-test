﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>
    public partial class SQLPRProvider : PRProvider
    {

//        #region Methods that work with Discount

        
//        /////////////////////////////////////////////////////////
//        // Methods that work with Discount
//        // Bhaskar N

//        /// <summary>
//        /// Retrieves all Discounts
//        /// </summary>
//        public override List<DiscountInfo> GetDiscounts(string cSortExpression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                //string cSQLCommand = "select * " +
//                //    "from Discount";

//                string cSQLCommand = "SELECT Discount.* , Topic.course_number " +
//                        "FROM Discount LEFT OUTER JOIN Topic ON Topic.Topicid = Discount.Topicid ";
//                // add on ORDER BY if provided
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetDiscountCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Retrieves the Discount with the specified ID
//        /// </summary>
//        public override DiscountInfo GetDiscountByID(int DiscountId)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                //SqlCommand cmd = new SqlCommand("select * " +
//                //        "from Discount where discountid=@DiscountId", cn);

//                SqlCommand cmd = new SqlCommand("SELECT Discount.* , Topic.course_number " +
//                     "FROM Discount LEFT OUTER JOIN Topic ON Topic.Topicid = Discount.Topicid" +
//                     " where Discount.discountid=@DiscountId ", cn);

//                cmd.Parameters.Add("@DiscountId", SqlDbType.Int).Value = DiscountId;
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//                if (reader.Read())
//                    return GetDiscountFromReader(reader, true);
//                else
//                    return null;
//            }
//        }


//        /// <summary>
//        /// Retrieves the DiscountID with the specified TopicID
//        /// </summary>
//        public override int GetDiscountIdByTopicId(int TopicId)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                //SqlCommand cmd = new SqlCommand("select * " +
//                //        "from Discount where discountid=@DiscountId", cn);

//                SqlCommand cmd = new SqlCommand("SELECT discountid from discount" +
//                     " where topicid=@TopicId ", cn);

//                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//                if (reader.Read())
//                    return (int)reader["discountId"];
//                else
//                    return 0;
//            }
//        }

//        /// <summary>
//        /// Retrieves the Discount with the specified TopicID
//        /// </summary>
//        public override System.Data.DataSet GetDiscountByTopicID(string TopicID, int UrlIdentity,string UserName)
//        {
//            System.Data.DataSet ds = new DataSet();
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                string cSQLCommand = "select * from Discount where (topicid = " + TopicID + ") ";
//                if (UrlIdentity == 1)
//                {
//                    //cSQLCommand = cSQLCommand + " and virtualurl_ind= 0 ";
//                    cSQLCommand = cSQLCommand + " and section_ind = 1 ";
//                }
//                else if (UrlIdentity == 2)
//                {
//                    cSQLCommand = cSQLCommand + "  and virtualurl_ind= 1 and  username = '" + UserName + "'";                    
//                }
//                //cSQLCommand = cSQLCommand + " and enddate >= '" + EndDate + "'";
//                cSQLCommand = cSQLCommand + " and enddate >= '" + System.DateTime.Now + "'";
                    

//                //else if (UrlIdentity == 2)
//                //{
//                //    cSQLCommand = cSQLCommand + " and section_ind= 1 " ;
//                //}
                
//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                //cn.Open();
//                SqlDataAdapter adap = new SqlDataAdapter(cmd);
//                adap.Fill(ds, "sponsor_text");
//            }
//            return ds;
//        }



//        /// <summary>
//        /// Inserts a new Discount
//        /// </summary>
//        public override int InsertDiscount(DiscountInfo Discount)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("insert into Discount " +
//                  "(topicid , " +
//                  "tagline, " +
//                  "startdate , " +
//                  "enddate, " +
//                  "discount_price, " +
//                  "discount_type , " +
//                  "sponsor_text , " +
//                  "img_file, " +
//                  "discount_url, " +
//                  "survey_ind, " +
//                  "impression, " +
//                  "clickthrough , " +
//                  "username, " +
//                  "userpassword,  " +
//                  "section_ind, " +
//                  "offline_price, " +
//                  "virtualurl_ind ) " +
//                  "VALUES (" +
//                  "@TopicId, " +
//                  "@TagLine, " +
//                  "@StartDate, " +
//                  "@EndDate, " +
//                  "@discount_price, " +
//                  "@discount_type, " +
//                  "@sponsor_text , " +
//                  "@Img_File, " +
//                  "@Discount_Url, " +
//                  "@Survey_Ind, " +
//                  "@Impression, " +
//                  "@ClickThrough, " +
//                  "@UserName, " +
//                  "@UserPassword, " +
//                  "@Section_Ind, " +
//                  "@Offline_Price, " +
//                  "@Virtualurl_Ind ) SET @ID = SCOPE_IDENTITY()", cn);

//                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = Discount.TopicId;
//                cmd.Parameters.Add("@TagLine", SqlDbType.VarChar).Value = Discount.TagLine;
//                cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = Discount.StartDate;
//                cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = Discount.EndDate;
//                cmd.Parameters.Add("@discount_price", SqlDbType.Decimal).Value = Discount.discount_price;
//                cmd.Parameters.Add("@discount_type", SqlDbType.VarChar).Value = Discount.discount_type;
//                cmd.Parameters.Add("@sponsor_text", SqlDbType.VarChar).Value = Discount.Sponsor_Text;
//                cmd.Parameters.Add("@Img_File", SqlDbType.VarChar).Value = Discount.Img_File;
//                cmd.Parameters.Add("@Discount_Url", SqlDbType.VarChar).Value = Discount.Discount_Url;
//                cmd.Parameters.Add("@Survey_Ind", SqlDbType.Bit).Value = Discount.Survey_Ind;
//                cmd.Parameters.Add("@Impression", SqlDbType.Int).Value = Discount.Impression;
//                cmd.Parameters.Add("@ClickThrough", SqlDbType.Int).Value = Discount.ClickThrough;
//                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Discount.UserName;
//                cmd.Parameters.Add("@UserPassword", SqlDbType.VarChar).Value = Discount.UserPassword;
//                cmd.Parameters.Add("@Section_Ind", SqlDbType.Bit).Value = Discount.Section_Ind;
//                cmd.Parameters.Add("@Offline_Price", SqlDbType.VarChar).Value = Discount.Offline_Price;
//                cmd.Parameters.Add("@Virtualurl_Ind", SqlDbType.Bit).Value = Discount.Virtualurl_Ind;

//                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//                IDParameter.Direction = ParameterDirection.Output;
//                cmd.Parameters.Add(IDParameter);

//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                cmd.ExecuteNonQuery();

//                int NewID = (int)IDParameter.Value;
//                return NewID;

//            }
//        }


//        /// <summary>
//        /// Updates a Discount
//        /// </summary>
//        public override bool UpdateDiscount(DiscountInfo Discount)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update Discount set " +
//              "topicid = @TopicId, " +
//              "tagline = @TagLine, " +
//              "startdate = @StartDate, " +
//              "enddate = @EndDate, " +
//              "discount_price = @discount_price, " +
//              "discount_type = @discount_type, " +
//              "sponsor_text = @Sponsor_Text, " +
//              "img_file = @Img_File, " +
//              "discount_url = @Discount_Url, " +
//              "survey_ind = @Survey_Ind, " +
//              "impression = @Impression, " +
//              "clickthrough = @ClickThrough, " +
//              "username = @UserName, " +
//              "userpassword = @UserPassword, " +
//              "section_ind = @Section_Ind, " +
//              "offline_price = @Offline_Price, " +
//              "virtualurl_ind = @Virtualurl_Ind " +
//              "where discountid = @DiscountId ", cn);
//                //            cmd.CommandType = CommandType.StoredProcedure;

//                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = Discount.TopicId;
//                cmd.Parameters.Add("@TagLine", SqlDbType.VarChar).Value = Discount.TagLine;
//                cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = Discount.StartDate;
//                cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = Discount.EndDate;
//                cmd.Parameters.Add("@discount_price", SqlDbType.Decimal).Value = Discount.discount_price;
//                cmd.Parameters.Add("@discount_type", SqlDbType.VarChar).Value = Discount.discount_type;
//                cmd.Parameters.Add("@sponsor_text", SqlDbType.VarChar).Value = Discount.Sponsor_Text;
//                cmd.Parameters.Add("@Img_File", SqlDbType.VarChar).Value = Discount.Img_File;
//                cmd.Parameters.Add("@Discount_Url", SqlDbType.VarChar).Value = Discount.Discount_Url;
//                cmd.Parameters.Add("@Survey_Ind", SqlDbType.Bit).Value = Discount.Survey_Ind;
//                cmd.Parameters.Add("@Impression", SqlDbType.Int).Value = Discount.Impression;
//                cmd.Parameters.Add("@ClickThrough", SqlDbType.Int).Value = Discount.ClickThrough;
//                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Discount.UserName;
//                cmd.Parameters.Add("@UserPassword", SqlDbType.VarChar).Value = Discount.UserPassword;
//                cmd.Parameters.Add("@Section_Ind", SqlDbType.Bit).Value = Discount.Section_Ind;
//                cmd.Parameters.Add("@Offline_Price", SqlDbType.Decimal).Value = Discount.Offline_Price;
//                cmd.Parameters.Add("@Virtualurl_Ind", SqlDbType.Bit).Value = Discount.Virtualurl_Ind;

//                cmd.Parameters.Add("@DiscountId", SqlDbType.Int).Value = Discount.DiscountId;


//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }

//        /// <summary>
//        /// Deletes a Discount
//        /// </summary>
//        public override bool DeleteDiscount(int DiscountId)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("delete from Discount where discountid=@DiscountId", cn);
//                cmd.Parameters.Add("@DiscountId", SqlDbType.Int).Value = DiscountId;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }


//        /// <summary>
//        /// Updates a Discount
//        /// </summary>
//        public override bool UpdateDiscountByImpression(int Discountid, int Impression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update discount set " +
//                "impression = @impression where discountid = @discountid ", cn);

//                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = Discountid;
//                cmd.Parameters.Add("@impression", SqlDbType.Int).Value = Impression;                

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }

//        /// <summary>
//        /// Updates a Discount
//        /// </summary>
//        public override bool UpdateDiscountByClickThrough(int Discountid, int ClickThrough)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update discount set " +
//                "clickthrough = @clickthrough where discountid = @discountid ", cn);

//                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = Discountid;
//                cmd.Parameters.Add("@clickthrough", SqlDbType.Int).Value = ClickThrough;

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }
//        #endregion

//        #region Methods that work with Positions

        


//        /////////////////////////////////////////////////////////
//        // methods that work with Positions

//        /// <summary>
//        /// Returns the total number of Positions
//        /// </summary>
//        public override int GetPositionCount(int FacID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("select Count(*) from Position where facid=@facid", cn);
//                cmd.Parameters.Add("@facid", SqlDbType.Int).Value = FacID;
//                cn.Open();
//                return (int)ExecuteScalar(cmd);
//            }
//        }

//        /// <summary>
//        /// Retrieves all Positions
//        /// </summary>
//        public override List<PositionInfo> GetPositions(string fac_id,string cSortExpression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select * " +
//                    "from Position";

//                if (fac_id.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand + " where facilityid= " + fac_id;
//                }

//                // add on ORDER BY if provided
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }
//                else
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by position_name";
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetPositionCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Retrieves the Position with the specified ID
//        /// </summary>
//        public override PositionInfo GetPositionByID(int PositionID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                SqlCommand cmd = new SqlCommand("select * " +
//                        "from Position where position_id=@PositionID", cn);
//                cmd.Parameters.Add("@PositionID", SqlDbType.Int).Value = PositionID;
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//                if (reader.Read())
//                    return GetPositionFromReader(reader, true);
//                else
//                    return null;
//            }
//        }

//        /// <summary>
//        /// Deletes a Position
//        /// </summary>
//        public override bool DeletePosition(int PositionID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("delete from Position where position_id=@PositionID", cn);
//                cmd.Parameters.Add("@PositionID", SqlDbType.Int).Value = PositionID;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }

//        /// <summary>
//        /// Inserts a new Position
//        /// </summary>
//        public override int InsertPosition(PositionInfo Position)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("insert into Position " +
//              "(facilityid,position_name,position_active_ind) " +
//              "VALUES (" +
//              "@facid,@position_name,@position_active_ind) SET @ID = SCOPE_IDENTITY()", cn);

//                cmd.Parameters.Add("@facid", SqlDbType.Int).Value = Position.FacID;
//                cmd.Parameters.Add("@position_name", SqlDbType.VarChar).Value = Position.Position_name;
//                cmd.Parameters.Add("@position_active_ind", SqlDbType.Bit).Value = Position.Position_active_ind;

//                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//                IDParameter.Direction = ParameterDirection.Output;
//                cmd.Parameters.Add(IDParameter);

//                cn.Open();
//                cmd.ExecuteNonQuery();

//                int NewID = (int)IDParameter.Value;
//                return NewID;

//            }
//        }

//        /// <summary>
//        /// Updates a Position
//        /// </summary>
//        public override bool UpdatePosition(PositionInfo Position)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update Position set " +
//              "position_name = @position_name, position_active_ind=@position_active_ind " +
//              "where position_id = @positionID ", cn);
//                //            cmd.CommandType = CommandType.StoredProcedure;
//                cmd.Parameters.Add("@position_name", SqlDbType.VarChar).Value = Position.Position_name;
//                cmd.Parameters.Add("@position_active_ind", SqlDbType.Bit).Value = Position.Position_active_ind;
//                cmd.Parameters.Add("@positionID", SqlDbType.Int).Value = Position.PositionID;


//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }


//        #endregion

//        #region Methods that work with Parent Organizations

       
//        /////////////////////////////////////////////////////////
//        // methods that work with ParentOrganizations
//        /// ***** CEDIRECT VERSION ************

//        /// <summary>
//        /// Returns the total number of ParentOrganizations
//        /// </summary>
//        public override int GetParentOrganizationCount()
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("select Count(*) from ParentOrganization", cn);
//                cn.Open();
//                return (int)ExecuteScalar(cmd);
//            }
//        }

//        /// <summary>
//        /// Retrieves all ParentOrganizations
//        /// </summary>
//        public override List<ParentOrganizationInfo> GetParentOrganizations(string cSortExpression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select * " +
//                    "from ParentOrganization";

//                // add on ORDER BY if provided
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetParentOrganizationCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Retrieves the ParentOrganization with the specified ID
//        /// </summary>
//        public override ParentOrganizationInfo GetParentOrganizationByID(int Parent_id)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                SqlCommand cmd = new SqlCommand("select * " +
//                        "from ParentOrganization where Parent_id=@Parent_id", cn);
//                cmd.Parameters.Add("@Parent_id", SqlDbType.Int).Value = Parent_id;
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//                if (reader.Read())
//                    return GetParentOrganizationFromReader(reader, true);
//                else
//                    return null;
//            }
//        }

//        /// <summary>
//        /// Returns the total number of UserAccounts for the specified FacilityID
//        /// </summary>
//        public override int GetUserAccountCountByParentFacilityID(int FacilityID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("select Count(*) from UserAccount u join FacilityGroup f on u.facilityid=f.facid where u.isapproved=1 and f.parent_id = @FacilityID", cn);
//                //              cmd.CommandType = CommandType.StoredProcedure;
//                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
//                cn.Open();
//                return (int)ExecuteScalar(cmd);
//            }
//        }


//        /// <summary>
//        /// Deletes a ParentOrganization
//        /// </summary>
//        public override bool DeleteParentOrganization(int Parent_id)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("delete from ParentOrganization where Parent_id=@Parent_id", cn);
//                cmd.Parameters.Add("@Parent_id", SqlDbType.Int).Value = Parent_id;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }

//        /// <summary>
//        /// Inserts a new ParentOrganization
//        /// </summary>
//        public override int InsertParentOrganization(ParentOrganizationInfo ParentOrganization)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("insert into ParentOrganization " +
//              "(Parent_name, " +
//              "address1, " +
//              "address2, " +
//              "city, " +
//              "state, " +
//              "zipcode, " +
//              "phone, " +
//              "contact_name, " +
//              "contact_phone, " +
//              "contact_ext, " +
//              "contact_email, " +
//              "active, " +
//              "comment ) " +
//              "VALUES (" +
//              "@Parent_name, " +
//              "@address1, " +
//              "@address2, " +
//              "@city, " +
//              "@state, " +
//              "@zipcode, " +
//              "@phone, " +
//              "@contact_name, " +
//              "@contact_phone, " +
//              "@contact_ext, " +
//              "@contact_email, " +
//              "@active, " +
//              "MaxUsers, " +
//              "Started, " +
//              "Expires, " +
//              "@comment ) SET @ID = SCOPE_IDENTITY()", cn);

//                cmd.Parameters.Add("@Parent_name", SqlDbType.VarChar).Value = ParentOrganization.Parent_name;
//                cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = ParentOrganization.address1;
//                cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = ParentOrganization.address2;
//                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = ParentOrganization.city;
//                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = ParentOrganization.state;
//                cmd.Parameters.Add("@zipcode", SqlDbType.VarChar).Value = ParentOrganization.zipcode;
//                cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = ParentOrganization.phone;
//                cmd.Parameters.Add("@contact_name", SqlDbType.VarChar).Value = ParentOrganization.contact_name;
//                cmd.Parameters.Add("@contact_phone", SqlDbType.VarChar).Value = ParentOrganization.contact_phone;
//                cmd.Parameters.Add("@contact_ext", SqlDbType.VarChar).Value = ParentOrganization.contact_ext;
//                cmd.Parameters.Add("@contact_email", SqlDbType.VarChar).Value = ParentOrganization.contact_email;
//                cmd.Parameters.Add("@active", SqlDbType.Bit).Value = ParentOrganization.active;
//                cmd.Parameters.Add("@MaxUsers", SqlDbType.Int).Value = ParentOrganization.MaxUsers;
//                cmd.Parameters.Add("@Started", SqlDbType.DateTime).Value = ParentOrganization.Started;
//                cmd.Parameters.Add("@Expires", SqlDbType.DateTime).Value = ParentOrganization.Expires;
//                cmd.Parameters.Add("@Comment", SqlDbType.Text).Value = ParentOrganization.Comment;

//                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//                IDParameter.Direction = ParameterDirection.Output;
//                cmd.Parameters.Add(IDParameter);

//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                cmd.ExecuteNonQuery();

//                int NewID = (int)IDParameter.Value;
//                return NewID;

//            }
//        }

//        /// <summary>
//        /// Updates a ParentOrganization
//        /// </summary>
//        public override bool UpdateParentOrganization(ParentOrganizationInfo ParentOrganization)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update ParentOrganization set " +
//              "Parent_name = @Parent_name, " +
//              "address1 = @address1, " +
//              "address2 = @address2, " +
//              "city = @city, " +
//              "state = @state, " +
//              "zipcode = @zipcode, " +
//              "phone = @phone, " +
//              "contact_name = @contact_name, " +
//              "contact_phone = @contact_phone, " +
//              "contact_ext = @contact_ext, " +
//              "contact_email = @contact_email, " +
//              "active = @active, " +
//            "MaxUsers = @MaxUsers, " +
//            "Started = @Started, " +
//            "Expires = @Expires, " +
//              "comment = @comment " +
//              "where Parent_id = @Parent_id ", cn);
//                //            cmd.CommandType = CommandType.StoredProcedure;

//                cmd.Parameters.Add("@Parent_name", SqlDbType.VarChar).Value = ParentOrganization.Parent_name;
//                cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = ParentOrganization.address1;
//                cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = ParentOrganization.address2;
//                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = ParentOrganization.city;
//                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = ParentOrganization.state;
//                cmd.Parameters.Add("@zipcode", SqlDbType.VarChar).Value = ParentOrganization.zipcode;
//                cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = ParentOrganization.phone;
//                cmd.Parameters.Add("@contact_name", SqlDbType.VarChar).Value = ParentOrganization.contact_name;
//                cmd.Parameters.Add("@contact_phone", SqlDbType.VarChar).Value = ParentOrganization.contact_phone;
//                cmd.Parameters.Add("@contact_ext", SqlDbType.VarChar).Value = ParentOrganization.contact_ext;
//                cmd.Parameters.Add("@contact_email", SqlDbType.VarChar).Value = ParentOrganization.contact_email;
//                cmd.Parameters.Add("@active", SqlDbType.Bit).Value = ParentOrganization.active;
//                cmd.Parameters.Add("@MaxUsers", SqlDbType.Int).Value = ParentOrganization.MaxUsers;
//                cmd.Parameters.Add("@Started", SqlDbType.DateTime).Value = ParentOrganization.Started;
//                cmd.Parameters.Add("@Expires", SqlDbType.DateTime).Value = ParentOrganization.Expires;
//                cmd.Parameters.Add("@comment", SqlDbType.Text).Value = ParentOrganization.Comment;

//                cmd.Parameters.Add("@Parent_id", SqlDbType.Int).Value = ParentOrganization.Parent_id;


//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }
//        #endregion

//        #region Methods that work with SponsorSurveyQue


//        /////////////////////////////////////////////////////////
//        // Methods that work with SponsorSurveyQue
//        // Bhaskar N

//        /// <summary>
//        /// Retrieves all SponsorSurveyQue
//        /// </summary>
//        public override List<SponsorSurveyQueInfo> GetSponsorSurveyQues(string cSortExpression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {                
//                string cSQLCommand = "select * " +
//                        "FROM SponsorSurveyQue";

//                // add on ORDER BY if provided
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetSponsorSurveyQueCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Retrieves all SponsorSurveyQue
//        /// </summary>
//        public override List<SponsorSurveyQueInfo> GetSponsorSurveyQuesByDiscountId(string cSortExpression, int discountid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                //string cSQLCommand = "select * " +
//                //        "FROM GetSponsorSurveyQue";

//                string cSQLCommand = "SELECT " +
//                        "SponsorSurveyQue.questionid, " +
//                        "SponsorSurveyQue.survey_text, " +
//                        "SponsorSurveyQue.optional_ind " +
//                    "FROM SponsorSurveyQue " +
//                    "JOIN SponsorSurveys ON SponsorSurveys.QuestionID=SponsorSurveyQue.QuestionID " +
//                    "WHERE SponsorSurveys.discountid=@discountid ";
                
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;

//                cn.Open();
//                return GetSponsorSurveyQueCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Retrieves the SponsorSurveyQue with the specified ID
//        /// </summary>
//        public override SponsorSurveyQueInfo GetSponsorSurveyQueByID(int questionid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                SqlCommand cmd = new SqlCommand("select * " +
//                        "from SponsorSurveyQue where questionid=@questionid", cn);

//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//                if (reader.Read())
//                    return GetSponsorSurveyQueFromReader(reader, true);
//                else
//                    return null;
//            }
//        }


//        /// <summary>
//        /// Inserts a new SponsorSurveyQue
//        /// </summary>
//        public override int InsertSponsorSurveyQue(SponsorSurveyQueInfo SponsorSurveyQue)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("insert into SponsorSurveyQue " +
//                  "(survey_text , " +                  
//                  "optional_ind ) " +
//                  "VALUES (" +
//                  "@survey_text, " + 
//                  "@optional_ind ) SET @ID = SCOPE_IDENTITY()", cn);

//                //cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = SponsorSurveyQue.questionid;
//                cmd.Parameters.Add("@survey_text", SqlDbType.VarChar).Value = SponsorSurveyQue.survey_text;
//                cmd.Parameters.Add("@optional_ind", SqlDbType.Bit).Value = SponsorSurveyQue.optional_ind;

//                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//                IDParameter.Direction = ParameterDirection.Output;
//                cmd.Parameters.Add(IDParameter);

//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                cmd.ExecuteNonQuery();

//                int NewID = (int)IDParameter.Value;
//                return NewID;

//            }
//        }


//        /// <summary>
//        /// Updates a SponsorSurveyQue
//        /// </summary>
//        public override bool UpdateSponsorSurveyQue(SponsorSurveyQueInfo SponsorSurveyQue)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update SponsorSurveyQue set " +
//                  "survey_text = @survey_text, " +
//                  "optional_ind = @optional_ind " +
//                  "where questionid = @questionid ", cn);                

//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = SponsorSurveyQue.questionid;
//                cmd.Parameters.Add("@survey_text", SqlDbType.VarChar).Value = SponsorSurveyQue.survey_text;
//                cmd.Parameters.Add("@optional_ind", SqlDbType.Bit).Value = SponsorSurveyQue.optional_ind;


//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }

//        /// <summary>
//        /// Deletes a SponsorSurveyQue
//        /// </summary>
//        public override bool DeleteSponsorSurveyQue(int questionid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("delete from SponsorSurveyQue where questionid=@questionid", cn);
//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }

//        #endregion

//        #region Methods that work with SponsorSurveyAns


//        /////////////////////////////////////////////////////////
//        // Methods that work with SponsorSurveyAns
//        // Bhaskar N

//        /// <summary>
//        /// Retrieves all Discounts
//        /// </summary>
//        public override List<SponsorSurveyAnsInfo> GetSponsorSurveyAns(string cSortExpression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select * " +
//                        "FROM SponsorSurveyAns";

//                // add on ORDER BY if provided
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetSponsorSurveyAnsCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Retrieves all Discounts
//        /// </summary>
//        public override List<SponsorSurveyAnsInfo> GetSponsorSurveyAnsByQuestionId(int questionid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select * " +
//                        "FROM SponsorSurveyAns where questionid=@questionid " +
//                        " order by answerid";                

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;

//                cn.Open();
//                return GetSponsorSurveyAnsCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Retrieves the SponsorSurveyAns with the specified ID
//        /// </summary>
//        public override SponsorSurveyAnsInfo GetSponsorSurveyAnsByID(int questionid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                SqlCommand cmd = new SqlCommand("select * " +
//                        "from SponsorSurveyAns where questionid=@questionid", cn);

//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//                if (reader.Read())
//                    return GetSponsorSurveyAnsFromReader(reader, true);
//                else
//                    return null;
//            }
//        }


//        /// <summary>
//        /// Inserts a new SponsorSurveyAns
//        /// </summary>
//        public override int InsertSponsorSurveyAns(SponsorSurveyAnsInfo SponsorSurveyAns)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("insert into SponsorSurveyAns " +
//                  "(questionid , " +
//                  "answer_text , " +
//                  "share_ind ) " +                  
//                  "VALUES (" +
//                  "@questionid, " +
//                  "@answer_text, " +                  
//                  "@share_ind ) SET @ID = SCOPE_IDENTITY()", cn);

//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = SponsorSurveyAns.questionid;
//                cmd.Parameters.Add("@answer_text", SqlDbType.VarChar).Value = SponsorSurveyAns.answer_text;
//                cmd.Parameters.Add("@share_ind", SqlDbType.Bit).Value = SponsorSurveyAns.share_ind;

//                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//                IDParameter.Direction = ParameterDirection.Output;
//                cmd.Parameters.Add(IDParameter);

//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                cmd.ExecuteNonQuery();

//                int NewID = (int)IDParameter.Value;
//                return NewID;

//            }
//        }


//        /// <summary>
//        /// Updates a SponsorSurveyAns
//        /// </summary>
//        public override bool UpdateSponsorSurveyAns(SponsorSurveyAnsInfo SponsorSurveyAns)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update SponsorSurveyAns set " +
//                  "questionid = @questionid, " +
//                  "answer_text = @answer_text, " +
//                  "share_ind = @share_ind " +
//                  "where answerid = @answerid ", cn);

//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = SponsorSurveyAns.questionid;
//                cmd.Parameters.Add("@answer_text", SqlDbType.VarChar).Value = SponsorSurveyAns.answer_text;
//                cmd.Parameters.Add("@share_ind", SqlDbType.Bit).Value = SponsorSurveyAns.share_ind;


//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }

//        /// <summary>
//        /// Deletes a SponsorSurveyAns
//        /// </summary>
//        public override bool DeleteSponsorSurveyAns(int questionid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("delete from SponsorSurveyAns where questionid=@questionid", cn);
//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }

//        #endregion

//        #region Methods that work with SponsorSurveys


//        /////////////////////////////////////////////////////////
//        // Methods that work with SponsorSurveys
//        // Bhaskar N

//        /// <summary>
//        /// Retrieves all Discounts
//        /// </summary>
//        public override List<SponsorSurveysInfo> GetSponsorSurveys(string cSortExpression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select * " +
//                        "FROM SponsorSurveys";

//                // add on ORDER BY if provided
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetSponsorSurveysCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Retrieves all Discounts
//        /// </summary>
//        public override List<SponsorSurveysInfo> GetSponsorSurveysByDiscountIdAndQuestionId(int discountid, int questionid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select * " +
//                        "FROM SponsorSurveys where discountid=@discountid and questionid=@questionid " +
//                        " order by questionid";

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;
//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;

//                cn.Open();
//                return GetSponsorSurveysCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Retrieves all Discounts
//        /// </summary>
//        public override List<SponsorSurveysInfo> GetSponsorSurveysByDiscountId(int discountid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select * " +
//                        "FROM SponsorSurveys where discountid=@discountid  " +
//                        " order by questionid";

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;               

//                cn.Open();
//                return GetSponsorSurveysCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Inserts a new SponsorSurveys
//        /// </summary>
//        public override bool InsertSponsorSurveys(SponsorSurveysInfo SponsorSurveys)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("insert into SponsorSurveys " +
//                  "(discountid , " +                  
//                  "questionid ) " +
//                  "VALUES (" +
//                  "@discountid, " +                  
//                  "@questionid )", cn);

//                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = SponsorSurveys.discountid;
//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = SponsorSurveys.questionid;
     
//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);

//            }
//        }

//        /// <summary>
//        /// Deletes a SponsorSurveys
//        /// </summary>
//        public override bool DeleteSponsorSurveysByDiscountidAndQuestionid(int discountid, int questionid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("delete from SponsorSurveys where discountid=@discountid and questionid=@questionid", cn);
//                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;
//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }

//        #endregion

//        #region Methods that work with SponsorSurveyRsp
//        /// <summary>
//        /// Insert Sponsor Survey Rsp
//        /// </summary>
//        public override bool InsertSponsorSurveyRsp(int userid, int orderitemid, int questionid, int answerid, int discountid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                SqlCommand cmd = new SqlCommand("insert into SponsorSurveyRsp " +
//                    "(userid, " +
//                    "orderitemid, " +
//                    "questionid, " +
//                    "answerid, " +
//                    "discountid) " +
//                    "VALUES (" +
//                    "@userid, " +
//                    "@orderitemid, " +
//                    "@questionid, " +
//                    "@answerid, " +
//                    "@discountid)", cn);

//                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
//                cmd.Parameters.Add("@orderitemid", SqlDbType.Int).Value = orderitemid;
//                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
//                cmd.Parameters.Add("@answerid", SqlDbType.Int).Value = answerid;
//                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }

//        /// <summary>
//        /// Updates a StateReq
//        /// </summary>
//        public override bool UpdateSponsorSurveyRspByOrderitemId(int userid, int orderitemid, int discountid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update SponsorSurveyRsp set " +
//                  "orderitemid = @orderitemid " +
//                  "where userid = @userid and discountid = @discountid And (OrderItemID Is Null OR OrderItemID = 0) ", cn);

//                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
//                cmd.Parameters.Add("@orderitemid", SqlDbType.Int).Value = orderitemid;
//                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;

//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }
//        #endregion

//        #region Methods that work with StateReq


//        /////////////////////////////////////////////////////////
//        // Methods that work with StateReq
//        // Bhaskar N

//        /// <summary>
//        /// Retrieves all StateReq
//        /// </summary>
//        public override List<StateReqInfo> GetStateReqs(string cSortExpression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                //string cSQLCommand = "select * " +
//                //    "from StateReq";

//                string cSQLCommand = "SELECT * FROM StateReq " +
//                        "Left Outer Join Categories ON Categories.id =StateReq.CategoryID ";
//                // add on ORDER BY if provided
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetStateReqCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Retrieves the StateReq with the specified StateAbr
//        /// </summary>
//        public override StateReqInfo GetStateReqByStateAbr(string StateAbr)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                //SqlCommand cmd = new SqlCommand("select * " +
//                //        "from StateReq where StateReqid=@StateReqId", cn);

//                SqlCommand cmd = new SqlCommand("SELECT * FROM StateReq " +
//                     " where stateabr = @StateAbr ", cn);

//                cmd.Parameters.Add("@StateAbr", SqlDbType.VarChar).Value = StateAbr;
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//                if (reader.Read())
//                    return GetStateReqFromReader(reader, true);
//                else
//                    return null;
//            }
//        }

//        public override StateReqInfo GetStateReqByCategoryId(int CategoryId)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                SqlCommand cmd = new SqlCommand("SELECT * FROM StateReq " +
//                     " where categoryid = @categoryid ", cn);

//                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = CategoryId;
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//                if (reader.Read())
//                    return GetStateReqFromReader(reader, true);
//                else
//                    return null;
//            }
//        }


//        public override List<StateReqInfo> GetStateReq()
//        {
//            List<StateReqInfo> statereq = new List<StateReqInfo>();
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
                
//                //SqlCommand cmd = new SqlCommand("select * " +
//                //        "from StateReq where StateReqid=@StateReqId", cn);

//                SqlCommand cmd = new SqlCommand("SELECT * FROM StateReq where inter_ind= 0" ,cn);
//                //+    " where stateabr = @StateAbr ", cn);

//               // cmd.Parameters.Add("@StateAbr", SqlDbType.VarChar).Value = StateAbr;
//                cn.Open();
//                StateReqInfo sqi = new StateReqInfo();
//                IDataReader reader = ExecuteReader(cmd);
//                while (reader.Read())
//                {
//                    sqi = GetStateReqFromReader(reader, true);
//                    statereq.Add(sqi);
//                }
               
//                //else
//                    return statereq;
//            }
//        }

//        /// <summary>
//        /// Inserts a new StateReq
//        /// </summary>
//        public override bool InsertStateReq(StateReqInfo StateReq)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("insert into StateReq " +
//                  "(stateabr , " +
//                  "statename, " +
//                  "cereq , " +
//                  "licagency, " +
//                  "permlic, " +
//                  "templic , " +
//                  "extralic , " +
//                  "email, " +
//                  "url, " +
//                  "lpn, " +
//                  "lastupdate, " +
//                  "categoryid , " +
//                  "taxrate, " +
//                  "inter_ind, " +
//                  "req_header, " +
//                  "course_recommend ) " +
//                  "VALUES (" +
//                  "@stateabr, " +
//                  "@statename, " +
//                  "@cereq, " +
//                  "@licagency, " +
//                  "@permlic, " +
//                  "@templic, " +
//                  "@extralic , " +
//                  "@email, " +
//                  "@url, " +
//                  "@lpn, " +
//                  "@lastupdate, " +
//                  "@categoryid, " +
//                  "@taxrate, " +
//                  "@inter_ind, " +
//                  "@req_header, " +
//                  "@course_recommend )", cn);


//                cmd.Parameters.Add("@stateabr", SqlDbType.VarChar).Value = StateReq.StateAbr;
//                cmd.Parameters.Add("@statename", SqlDbType.VarChar).Value = StateReq.StateName;
//                cmd.Parameters.Add("@cereq", SqlDbType.VarChar).Value = StateReq.CeReq;
//                cmd.Parameters.Add("@licagency", SqlDbType.VarChar).Value = StateReq.LicAgency;
//                cmd.Parameters.Add("@permlic", SqlDbType.VarChar).Value = StateReq.Permlic;
//                cmd.Parameters.Add("@templic", SqlDbType.VarChar).Value = StateReq.TempLic;
//                cmd.Parameters.Add("@extralic", SqlDbType.VarChar).Value = StateReq.ExtraLic;
//                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = StateReq.Email;
//                cmd.Parameters.Add("@url", SqlDbType.VarChar).Value = StateReq.Url;
//                cmd.Parameters.Add("@lpn", SqlDbType.VarChar).Value = StateReq.Lpn;
//                cmd.Parameters.Add("@lastupdate", SqlDbType.DateTime).Value = StateReq.LastUpdate;
//                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = StateReq.CategoryId;
//                cmd.Parameters.Add("@taxrate", SqlDbType.Decimal).Value = StateReq.TaxRate;
//                cmd.Parameters.Add("@inter_ind", SqlDbType.Bit).Value = StateReq.Inter_Ind;
//                cmd.Parameters.Add("@req_header", SqlDbType.VarChar).Value = StateReq.req_header;
//                cmd.Parameters.Add("@course_recommend", SqlDbType.VarChar).Value = StateReq.course_recommend;

//                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//                IDParameter.Direction = ParameterDirection.Output;
//                cmd.Parameters.Add(IDParameter);

//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                //cmd.ExecuteNonQuery();

//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);

//            }
//        }


//        /// <summary>
//        /// Updates a StateReq
//        /// </summary>
//        public override bool UpdateStateReq(StateReqInfo StateReq)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update StateReq set " +
//                  "statename = @statename, " +
//                  "cereq = @cereq, " +
//                  "licagency = @licagency, " +
//                  "permlic = @permlic, " +
//                  "templic = @templic, " +
//                  "extralic = @extralic, " +
//                  "email = @email, " +
//                  "url = @url, " +
//                  "lpn = @lpn, " +
//                  "lastupdate = @lastupdate, " +
//                  "categoryid = @categoryid, " +
//                  "taxrate = @taxrate, " +
//                  "req_header = @req_header, " +
//                  "course_recommend = @course_recommend, " +
//                  "inter_ind = @inter_ind " +
//                  "where stateabr = @stateabr ", cn);

//                cmd.Parameters.Add("@statename", SqlDbType.VarChar).Value = StateReq.StateName;
//                cmd.Parameters.Add("@cereq", SqlDbType.VarChar).Value = StateReq.CeReq;
//                cmd.Parameters.Add("@licagency", SqlDbType.VarChar).Value = StateReq.LicAgency;
//                cmd.Parameters.Add("@permlic", SqlDbType.VarChar).Value = StateReq.Permlic;
//                cmd.Parameters.Add("@templic", SqlDbType.VarChar).Value = StateReq.TempLic;
//                cmd.Parameters.Add("@extralic", SqlDbType.VarChar).Value = StateReq.ExtraLic;
//                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = StateReq.Email;
//                cmd.Parameters.Add("@url", SqlDbType.VarChar).Value = StateReq.Url;
//                cmd.Parameters.Add("@lpn", SqlDbType.VarChar).Value = StateReq.Lpn;
//                cmd.Parameters.Add("@lastupdate", SqlDbType.DateTime).Value = StateReq.LastUpdate;
//                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = StateReq.CategoryId;
//                cmd.Parameters.Add("@taxrate", SqlDbType.Decimal).Value = StateReq.TaxRate;
//                cmd.Parameters.Add("@inter_ind", SqlDbType.Bit).Value = StateReq.Inter_Ind;

//                cmd.Parameters.Add("@req_header", SqlDbType.VarChar).Value = StateReq.req_header;
//                cmd.Parameters.Add("@course_recommend", SqlDbType.VarChar).Value = StateReq.course_recommend;

//                cmd.Parameters.Add("@stateabr", SqlDbType.VarChar).Value = StateReq.StateAbr;


//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }


//        /// <summary>
//        /// Deletes a StateReq
//        /// </summary>
//        public override bool DeleteStateReq(string StateAbr)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("delete from StateReq where StateAbr=@StateAbr", cn);
//                cmd.Parameters.Add("@StateAbr", SqlDbType.Int).Value = StateAbr;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }


//        /// <summary>
//        /// Updates a StateReq
//        /// </summary>
//        public override bool UpdateStateReqByStateAbr(string Cereq, string Email, string Url, string LicAgency, string Lpn, string Stateabr, int CategoryId, string req_header, string course_recommend)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update StateReq set " +
//                  "cereq = @cereq, " +
//                  "email = @email, " +
//                  "url = @url, " +
//                  "licagency = @LicAgency, " +
//                  "lpn = @lpn, " +
//                  "lastupdate = @lastupdate, " +
//                  "CategoryId = @CategoryId, " +
//                  "req_header = @req_header, " +
//                  "course_recommend = @course_recommend " +
//                  "where stateabr = @stateabr ", cn);

//                cmd.Parameters.Add("@cereq", SqlDbType.VarChar).Value = Cereq;
//                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = Email;
//                cmd.Parameters.Add("@url", SqlDbType.VarChar).Value = Url;
//                cmd.Parameters.Add("@LicAgency", SqlDbType.VarChar).Value = LicAgency;
//                cmd.Parameters.Add("@lpn", SqlDbType.VarChar).Value = Lpn;
//                cmd.Parameters.Add("@lastupdate", SqlDbType.DateTime).Value = System.DateTime.Now;
//                cmd.Parameters.Add("@stateabr", SqlDbType.VarChar).Value = Stateabr;
//                cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = CategoryId;
//                cmd.Parameters.Add("@req_header", SqlDbType.VarChar).Value = req_header;
//                cmd.Parameters.Add("@course_recommend", SqlDbType.VarChar).Value = course_recommend;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }
//        #endregion
    }      
}