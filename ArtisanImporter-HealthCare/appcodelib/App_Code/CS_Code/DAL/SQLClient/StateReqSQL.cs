﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region PositionInfo

namespace PearlsReview.DAL
{
    /// <summary> 
    /// Summary description for StateReqInfo
    /// Bhaskar N
    /// </summary>
    public class StateReqInfo
    {
        public StateReqInfo() { }


        public StateReqInfo(string stateabr, string statename, string cereq, string licagency, string permlic, string templic,
            string extralic, string email, string url, string lpn, DateTime lastupdate, int categoryid,
            decimal taxrate, bool inter_ind, string req_header, string course_recommend, string region)
        {
            this.StateAbr = stateabr;
            this.StateName = statename;
            this.CeReq = cereq;
            this.LicAgency = licagency;
            this.Permlic = permlic;
            this.TempLic = templic;
            this.ExtraLic = extralic;
            this.Email = email;
            this.Url = url;
            this.Lpn = lpn;
            this.LastUpdate = lastupdate;
            this.CategoryId = categoryid;
            this.TaxRate = taxrate;
            this.Inter_Ind = inter_ind;
            this.req_header = req_header;
            this.course_recommend = course_recommend;
            this.Region = region;
        }


        private string _stateabr;
        public string StateAbr
        {
            get { return _stateabr; }
            private set { _stateabr = value; }
        }

        private string _statename = "";
        public string StateName
        {
            get { return _statename; }
            private set { _statename = value; }
        }

        private string _cereq = "";
        public string CeReq
        {
            get { return _cereq; }
            private set { _cereq = value; }
        }

        private string _licagency = "";
        public string LicAgency
        {
            get { return _licagency; }
            private set { _licagency = value; }
        }

        private string _permlic = "";
        public string Permlic
        {
            get { return _permlic; }
            private set { _permlic = value; }
        }

        private string _templic = "";
        public string TempLic
        {
            get { return _templic; }
            private set { _templic = value; }
        }

        private string _extralic = "";
        public string ExtraLic
        {
            get { return _extralic; }
            private set { _extralic = value; }
        }

        private string _email = "";
        public string Email
        {
            get { return _email; }
            private set { _email = value; }
        }

        private string _url = "";
        public string Url
        {
            get { return _url; }
            private set { _url = value; }
        }

        private string _lpn = "";
        public string Lpn
        {
            get { return _lpn; }
            private set { _lpn = value; }
        }

        private DateTime _lastupdate = System.DateTime.Now;
        public DateTime LastUpdate
        {
            get { return _lastupdate; }
            private set { _lastupdate = value; }
        }

        private int _categoryid = 0;
        public int CategoryId
        {
            get { return _categoryid; }
            protected set { _categoryid = value; }
        }

        private decimal _taxrate = 0.00M;
        public decimal TaxRate
        {
            get { return _taxrate; }
            private set { _taxrate = value; }
        }

        private bool _inter_ind = true;
        public bool Inter_Ind
        {
            get { return _inter_ind; }
            private set { _inter_ind = value; }
        }

        private string _req_header = "";
        public string req_header
        {
            get { return _req_header; }
            private set { _req_header = value; }
        }

        private string _course_recommend = "";
        public string course_recommend
        {
            get { return _course_recommend; }
            private set { _course_recommend = value; }
        }
        private string _region = "";
        public string Region
        {
            get { return _region; }
            private set { _region = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // Methods that work with StateReq
        // Bhaskar N

        /// <summary>
        /// Retrieves all StateReq
        /// </summary>
        public List<StateReqInfo> GetStateReqs(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select * " +
                //    "from StateReq";

                string cSQLCommand = "SELECT * FROM StateReq " +
                        "Left Outer Join Categories ON Categories.id =StateReq.CategoryID ";
                // add on ORDER BY if provided

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetStateReqCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<StateReqInfo> GetAllStates(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select * " +
                //    "from StateReq";

                string cSQLCommand = "SELECT * FROM StateReq ";
                // add on ORDER BY if provided

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetStateReqCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public List<StateReqInfo> GetMicrositeStatesReq(int msid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select * " +
                //    "from StateReq";

                string cSQLCommand = "SELECT s.* from MicrositeStateReq as m join Statereq as s on m.stateabr = s.stateabr where m.msid = @msid";
                // add on ORDER BY if provided

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cn.Open();
                return GetStateReqCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public List<StateReqInfo> GetStates(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select * " +
                //    "from StateReq";

                string cSQLCommand = "SELECT * FROM StateReq where inter_ind = 0";
                // add on ORDER BY if provided

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetStateReqCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public List<StateReqInfo> GetStatesByRegion(string Region)
        {
            List<StateReqInfo> list = new List<StateReqInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_region_statelist";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@region", SqlDbType.VarChar).Value = Region;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    StateReqInfo info = new StateReqInfo(reader["stateabr"].ToString(), "", "", "", "", "", "", "", "", "", DateTime.Today, 0, 0, false, "", "", Region);
                    list.Add(info);
                }
                return list;
            }
        }
        /// <summary>
        /// Retrieves all StateReq
        /// </summary>
        public List<StateReqInfo> GetMicrositeStateReqsWithLink(int msid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select * " +
                //    "from StateReq";

                //string cSQLCommand = "SELECT * FROM StateReq " +
                //        "Left Outer Join Categories ON Categories.id =StateReq.CategoryID ";
                // add on ORDER BY if provided

                string cSQLCommand = "select " +
                    "case categories.showinlist when 1 then statereq.statename + ' <html><br></br><a href= \"/' + categories.website + '\">View Courses</a></html>'  else statereq.statename end as statename, " +
                 "statereq.stateabr,cereq,licagency,permlic,templic,extralic,email,url,lpn,lastupdate,categoryid,taxrate,inter_ind,req_header,course_recommend, region " +
                     " FROM StateReq Right Outer Join MicrositeStateReq ON StateReq.stateabr = MicrositeStateReq.stateabr and msid = " + msid +
                        "Left Outer Join Categories ON Categories.id =StateReq.CategoryID ";
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetStateReqCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves all StateReq
        /// </summary>
        public List<StateReqInfo> GetStateReqsWithLink(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select * " +
                //    "from StateReq";

                //string cSQLCommand = "SELECT * FROM StateReq " +
                //        "Left Outer Join Categories ON Categories.id =StateReq.CategoryID ";
                // add on ORDER BY if provided

                string cSQLCommand = "select " +
                     "case categories.showinlist when 1 then statereq.statename + ' <html><br></br><a href= \"/' + categories.website + '\">View Courses</a></html>'  else statereq.statename end as statename, " +
                  "stateabr,cereq,licagency,permlic,templic,extralic,email,url,lpn,lastupdate,categoryid,taxrate,inter_ind,req_header,course_recommend, region " +
                      " FROM StateReq " +
                         "Left Outer Join Categories ON Categories.id =StateReq.CategoryID ";
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetStateReqCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the StateReq with the specified StateAbr
        /// </summary>
        public StateReqInfo GetStateReqByStateAbr(string StateAbr)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                //SqlCommand cmd = new SqlCommand("select * " +
                //        "from StateReq where StateReqid=@StateReqId", cn);

                SqlCommand cmd = new SqlCommand("SELECT * FROM StateReq " +
                     " where stateabr = @StateAbr ", cn);

                cmd.Parameters.Add("@StateAbr", SqlDbType.VarChar).Value = StateAbr;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetStateReqFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves the StateReq with the specified StateAbr
        /// </summary>
        public StateReqInfo GetStateReqByStateName(string Statename)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                //SqlCommand cmd = new SqlCommand("select * " +
                //        "from StateReq where StateReqid=@StateReqId", cn);

                SqlCommand cmd = new SqlCommand("SELECT * FROM StateReq " +
                     " where statename = @Statename ", cn);

                cmd.Parameters.Add("@Statename", SqlDbType.VarChar).Value = Statename.Replace("-", " ");
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetStateReqFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the StateReq with the specified StateAbr or name
        /// </summary>
        public StateReqInfo GetStateReqByStateNameorAbr(string StatenameOrAbr)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                //SqlCommand cmd = new SqlCommand("select * " +
                //        "from StateReq where StateReqid=@StateReqId", cn);

                SqlCommand cmd = new SqlCommand("SELECT * FROM StateReq " +
                     " where replace(statename,' ','') = @Statename  or replace(stateabr,' ','')=@Statename", cn);

                cmd.Parameters.Add("@Statename", SqlDbType.VarChar).Value = StatenameOrAbr.Replace("-", "");
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetStateReqFromReader(reader, true);
                else
                    return null;
            }
        }



        /// <summary>
        /// Inserts a new StateReq
        /// </summary>
        public bool InsertStateReq(StateReqInfo StateReq)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into StateReq " +
                  "(stateabr , " +
                  "statename, " +
                  "cereq , " +
                  "licagency, " +
                  "permlic, " +
                  "templic , " +
                  "extralic , " +
                  "email, " +
                  "url, " +
                  "lpn, " +
                  "lastupdate, " +
                  "categoryid , " +
                  "taxrate, " +
                  "inter_ind, " +
                  "req_header, " +
                  "course_recommend, " +
                  "region )" +
                  "VALUES (" +
                  "@stateabr, " +
                  "@statename, " +
                  "@cereq, " +
                  "@licagency, " +
                  "@permlic, " +
                  "@templic, " +
                  "@extralic , " +
                  "@email, " +
                  "@url, " +
                  "@lpn, " +
                  "@lastupdate, " +
                  "@categoryid, " +
                  "@taxrate, " +
                  "@inter_ind, " +
                  "@req_header, " +
                  "@course_recommend, " +
                  "@region )", cn);


                cmd.Parameters.Add("@stateabr", SqlDbType.VarChar).Value = StateReq.StateAbr;
                cmd.Parameters.Add("@statename", SqlDbType.VarChar).Value = StateReq.StateName;
                cmd.Parameters.Add("@cereq", SqlDbType.VarChar).Value = StateReq.CeReq;
                cmd.Parameters.Add("@licagency", SqlDbType.VarChar).Value = StateReq.LicAgency;
                cmd.Parameters.Add("@permlic", SqlDbType.VarChar).Value = StateReq.Permlic;
                cmd.Parameters.Add("@templic", SqlDbType.VarChar).Value = StateReq.TempLic;
                cmd.Parameters.Add("@extralic", SqlDbType.VarChar).Value = StateReq.ExtraLic;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = StateReq.Email;
                cmd.Parameters.Add("@url", SqlDbType.VarChar).Value = StateReq.Url;
                cmd.Parameters.Add("@lpn", SqlDbType.VarChar).Value = StateReq.Lpn;
                cmd.Parameters.Add("@lastupdate", SqlDbType.DateTime).Value = StateReq.LastUpdate;
                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = StateReq.CategoryId;
                cmd.Parameters.Add("@taxrate", SqlDbType.Decimal).Value = StateReq.TaxRate;
                cmd.Parameters.Add("@inter_ind", SqlDbType.Bit).Value = StateReq.Inter_Ind;
                cmd.Parameters.Add("@req_header", SqlDbType.VarChar).Value = StateReq.req_header;
                cmd.Parameters.Add("@course_recommend", SqlDbType.VarChar).Value = StateReq.course_recommend;
                cmd.Parameters.Add("@region", SqlDbType.VarChar).Value = StateReq.Region;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                //cmd.ExecuteNonQuery();

                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);

            }
        }


        /// <summary>
        /// Updates a StateReq
        /// </summary>
        public bool UpdateStateReq(StateReqInfo StateReq)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update StateReq set " +
                  "statename = @statename, " +
                  "cereq = @cereq, " +
                  "licagency = @licagency, " +
                  "permlic = @permlic, " +
                  "templic = @templic, " +
                  "extralic = @extralic, " +
                  "email = @email, " +
                  "url = @url, " +
                  "lpn = @lpn, " +
                  "lastupdate = @lastupdate, " +
                  "categoryid = @categoryid, " +
                  "taxrate = @taxrate, " +
                  "req_header = @req_header, " +
                  "course_recommend = @course_recommend, " +
                  "region = @region, " +
                  "inter_ind = @inter_ind " +
                  "where stateabr = @stateabr ", cn);

                cmd.Parameters.Add("@statename", SqlDbType.VarChar).Value = StateReq.StateName;
                cmd.Parameters.Add("@cereq", SqlDbType.VarChar).Value = StateReq.CeReq;
                cmd.Parameters.Add("@licagency", SqlDbType.VarChar).Value = StateReq.LicAgency;
                cmd.Parameters.Add("@permlic", SqlDbType.VarChar).Value = StateReq.Permlic;
                cmd.Parameters.Add("@templic", SqlDbType.VarChar).Value = StateReq.TempLic;
                cmd.Parameters.Add("@extralic", SqlDbType.VarChar).Value = StateReq.ExtraLic;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = StateReq.Email;
                cmd.Parameters.Add("@url", SqlDbType.VarChar).Value = StateReq.Url;
                cmd.Parameters.Add("@lpn", SqlDbType.VarChar).Value = StateReq.Lpn;
                cmd.Parameters.Add("@lastupdate", SqlDbType.DateTime).Value = StateReq.LastUpdate;
                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = StateReq.CategoryId;
                cmd.Parameters.Add("@taxrate", SqlDbType.Decimal).Value = StateReq.TaxRate;
                cmd.Parameters.Add("@inter_ind", SqlDbType.Bit).Value = StateReq.Inter_Ind;

                cmd.Parameters.Add("@req_header", SqlDbType.VarChar).Value = StateReq.req_header;
                cmd.Parameters.Add("@course_recommend", SqlDbType.VarChar).Value = StateReq.course_recommend;
                cmd.Parameters.Add("@region", SqlDbType.VarChar).Value = StateReq.Region;
                cmd.Parameters.Add("@stateabr", SqlDbType.VarChar).Value = StateReq.StateAbr;


                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a StateReq
        /// </summary>
        public bool DeleteStateReq(string StateAbr)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from StateReq where StateAbr=@StateAbr", cn);
                cmd.Parameters.Add("@StateAbr", SqlDbType.Int).Value = StateAbr;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        /// <summary>
        /// Updates a StateReq
        /// </summary>
        public bool UpdateStateReqByStateAbr(string Cereq, string Email, string Url, string LicAgency, string Lpn, string Stateabr, int CategoryId, string req_header, string course_recommend, string region)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update StateReq set " +
                  "cereq = @cereq, " +
                  "email = @email, " +
                  "url = @url, " +
                  "licagency = @LicAgency, " +
                  "lpn = @lpn, " +
                  "lastupdate = @lastupdate, " +
                  "CategoryId = @CategoryId, " +
                  "req_header = @req_header, " +
                  "region = @region, " +
                  "course_recommend = @course_recommend " +
                  "where stateabr = @stateabr ", cn);

                cmd.Parameters.Add("@cereq", SqlDbType.VarChar).Value = Cereq;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = Email;
                cmd.Parameters.Add("@url", SqlDbType.VarChar).Value = Url;
                cmd.Parameters.Add("@LicAgency", SqlDbType.VarChar).Value = LicAgency;
                cmd.Parameters.Add("@lpn", SqlDbType.VarChar).Value = Lpn;
                cmd.Parameters.Add("@lastupdate", SqlDbType.DateTime).Value = System.DateTime.Now;
                cmd.Parameters.Add("@stateabr", SqlDbType.VarChar).Value = Stateabr;
                cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = CategoryId;
                cmd.Parameters.Add("@req_header", SqlDbType.VarChar).Value = req_header;
                cmd.Parameters.Add("@region", SqlDbType.VarChar).Value = region;
                cmd.Parameters.Add("@course_recommend", SqlDbType.VarChar).Value = course_recommend;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public StateReqInfo GetStateReqByCategoryId(int CategoryId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("SELECT * FROM StateReq " +
                     " where categoryid = @categoryid ", cn);

                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = CategoryId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetStateReqFromReader(reader, true);
                else
                    return null;
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new StateReqInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual StateReqInfo GetStateReqFromReader(IDataReader reader)
        {
            return GetStateReqFromReader(reader, true);
        }
        protected virtual StateReqInfo GetStateReqFromReader(IDataReader reader, bool readMemos)
        {
            StateReqInfo StateReq = new StateReqInfo(
                (Convert.IsDBNull(reader["StateAbr"]) ? "" : reader["StateAbr"].ToString()),
                (Convert.IsDBNull(reader["StateName"]) ? "" : reader["StateName"].ToString()),
                (Convert.IsDBNull(reader["CeReq"]) ? "" : reader["CeReq"].ToString()),
                (Convert.IsDBNull(reader["LicAgency"]) ? "" : reader["LicAgency"].ToString()),
                (Convert.IsDBNull(reader["Permlic"]) ? "" : reader["Permlic"].ToString()),
                (Convert.IsDBNull(reader["TempLic"]) ? "" : reader["TempLic"].ToString()),
                (Convert.IsDBNull(reader["ExtraLic"]) ? "" : reader["ExtraLic"].ToString()),
                (Convert.IsDBNull(reader["Email"]) ? "" : reader["Email"].ToString()),
                (Convert.IsDBNull(reader["Url"]) ? "" : reader["Url"].ToString()),
                (Convert.IsDBNull(reader["Lpn"]) ? "" : reader["Lpn"].ToString()),
                (DateTime)(Convert.IsDBNull(reader["LastUpdate"]) ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastUpdate"]),
                (int)(Convert.IsDBNull(reader["CategoryId"]) ? (int)0 : (int)reader["CategoryId"]),
                (decimal)(Convert.IsDBNull(reader["TaxRate"]) ? (decimal)0 : (decimal)reader["TaxRate"]),
                (bool)(Convert.IsDBNull(reader["Inter_Ind"]) ? false : (bool)reader["Inter_Ind"]),
                (Convert.IsDBNull(reader["req_header"]) ? "" : reader["req_header"].ToString()),
                (Convert.IsDBNull(reader["course_recommend"]) ? "" : reader["course_recommend"].ToString()),
                (Convert.IsDBNull(reader["region"]) ? "" : reader["region"].ToString()));
            return StateReq;

        }



        /// <summary>
        /// Returns a collection of StateReqInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<StateReqInfo> GetStateReqCollectionFromReader(IDataReader reader)
        {
            return GetStateReqCollectionFromReader(reader, true);
        }
        protected virtual List<StateReqInfo> GetStateReqCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<StateReqInfo> StateReqs = new List<StateReqInfo>();
            while (reader.Read())
                StateReqs.Add(GetStateReqFromReader(reader, readMemos));
            return StateReqs;
        }

        #endregion
    }
}
#endregion