﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region PositionInfo

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for Test
    /// </summary>
    public class SponsorSurveyRspInfo
    {
        public SponsorSurveyRspInfo() { }

        public SponsorSurveyRspInfo(int responseid, int userid, int orderitemid, int questionid, int answerid, int discountid)
        {
            this.ResponseID  = responseid;
            this.UserID = userid;
            this.OrderitemID = orderitemid;
            this.QuestionID = questionid;
            this.AnswerID = answerid;
            this.DiscountID = discountid;
        }

        private int _responseid = 0;
        public int ResponseID
        {
            get { return _responseid; }
            set { _responseid = value; }
        }

        private int _userid = 0;
        public int UserID
        {
            get { return _userid; }
            set { _userid = value; }
        }

        private int _orderitemid = 0;
        public int OrderitemID
        {
            get { return _orderitemid; }
            set { _orderitemid = value; }
        }

        private int _questionid = 0;
        public int QuestionID
        {
            get { return _questionid; }
            set { _questionid = value; }
        }

        private int _answerid = 0;
        public int AnswerID
        {
            get { return _answerid; }
            set { _answerid = value; }
        }

        private int _discountid = 0;
        public int DiscountID
        {
            get { return _discountid; }
            set { _discountid = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        /// <summary>
        /// Insert Sponsor Survey Rsp
        /// </summary>
        public  bool InsertSponsorSurveyRsp(int userid, int orderitemid, int questionid, int answerid, int discountid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("insert into SponsorSurveyRsp " +
                    "(userid, " +
                    "orderitemid, " +
                    "questionid, " +
                    "answerid, " +
                    "discountid) " +
                    "VALUES (" +
                    "@userid, " +
                    "@orderitemid, " +
                    "@questionid, " +
                    "@answerid, " +
                    "@discountid)", cn);

                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@orderitemid", SqlDbType.Int).Value = orderitemid;
                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
                cmd.Parameters.Add("@answerid", SqlDbType.Int).Value = answerid;
                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        ///
        /// </summary>
        public  bool UpdateSponsorSurveyRspByOrderitemId(int userid, int orderitemid, int discountid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update SponsorSurveyRsp set " +
                  "orderitemid = @orderitemid " +
                  "where userid = @userid and discountid = @discountid And (OrderItemID Is Null OR OrderItemID = 0) ", cn);

                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@orderitemid", SqlDbType.Int).Value = orderitemid;
                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        
        #endregion

        #region PRProvider

        /////////////////////////////////////////////////////////
        // methods that work with SponsorSurveys
        
        //public abstract bool InsertSponsorSurveyRsp(int userid, int orderitemid, int questionid, int answerid, int discountid);

        
        #endregion
    }
}
#endregion