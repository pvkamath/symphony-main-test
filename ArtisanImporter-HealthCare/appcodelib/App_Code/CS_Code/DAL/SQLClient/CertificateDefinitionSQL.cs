﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region CertificateDefinitionInfo

namespace PearlsReview.DAL
{
    public class CertificateDefinitionInfo
    {
        public CertificateDefinitionInfo() { }

        public CertificateDefinitionInfo(int CertID, string CertName, string CertBody, string CertType,
             int FacilityID)
        {
            this.CertID = CertID;
            this.CertName = CertName;
            this.CertBody = CertBody;
            this.CertType = CertType;
            this.FacilityID = FacilityID;
        }

        private int _CertID = 0;
        public int CertID
        {
            get { return _CertID; }
            protected set { _CertID = value; }
        }

        private string _CertName = "";
        public string CertName
        {
            get { return _CertName; }
            private set { _CertName = value; }
        }

        private string _CertBody = "";
        public string CertBody
        {
            get { return _CertBody; }
            private set { _CertBody = value; }
        }

        private string _CertType = "";
        public string CertType
        {
            get { return _CertType; }
            private set { _CertType = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            private set { _FacilityID = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with CertificateDefinitions

        /// <summary>
        /// Returns the total number of CertificateDefinitions
        /// </summary>
        public  int GetCertificateDefinitionCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from CertificateDefinition where FacilityID=0 ", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all CertificateDefinitions
        /// </summary>
        public  List<CertificateDefinitionInfo> GetCertificateDefinitions(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from CertificateDefinition";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCertificateDefinitionCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all CertificateDefinitions for a given CertType
        /// </summary>
        public  List<CertificateDefinitionInfo> GetCertificateDefinitionsByCertType(string cCertType, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from CertificateDefinition where CertType=@CertType ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@CertType", SqlDbType.VarChar).Value = cCertType;

                cn.Open();
                return GetCertificateDefinitionCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the CertificateDefinition with the specified ID
        /// </summary>
        public  CertificateDefinitionInfo GetCertificateDefinitionByID(int CertID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from CertificateDefinition where CertID=@CertID", cn);
                cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = CertID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCertificateDefinitionFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a CertificateDefinition
        /// </summary>
        public  bool DeleteCertificateDefinition(int CertID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from CertificateDefinition where CertID=@CertID", cn);
                cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = CertID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new CertificateDefinition
        /// </summary>
        public  int InsertCertificateDefinition(CertificateDefinitionInfo CertificateDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into CertificateDefinition " +
              "(CertName, " +
              "CertBody, " +
              "CertType, " +
                "FacilityID ) " +
              "VALUES (" +
              "@CertName, " +
              "@CertBody, " +
              "@CertType, " +
                "@FacilityID ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@CertName", SqlDbType.VarChar).Value = CertificateDefinition.CertName;
                cmd.Parameters.Add("@CertBody", SqlDbType.VarChar).Value = CertificateDefinition.CertBody;
                cmd.Parameters.Add("@CertType", SqlDbType.Char).Value = CertificateDefinition.CertType;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = CertificateDefinition.FacilityID;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a CertificateDefinition
        /// </summary>
        public  bool UpdateCertificateDefinition(CertificateDefinitionInfo CertificateDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update CertificateDefinition set " +
              "CertName = @CertName, " +
              "CertBody = @CertBody, " +
              "CertType = @CertType, " +
              "FacilityID = @FacilityID " +
              "where CertID = @CertID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CertName", SqlDbType.VarChar).Value = CertificateDefinition.CertName;
                cmd.Parameters.Add("@CertBody", SqlDbType.Text).Value = CertificateDefinition.CertBody;
                cmd.Parameters.Add("@CertType", SqlDbType.Char).Value = CertificateDefinition.CertType;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = CertificateDefinition.FacilityID;
                cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = CertificateDefinition.CertID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new CertificateDefinitionInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual CertificateDefinitionInfo GetCertificateDefinitionFromReader(IDataReader reader)
        {
            return GetCertificateDefinitionFromReader(reader, true);
        }
        protected virtual CertificateDefinitionInfo GetCertificateDefinitionFromReader(IDataReader reader, bool readMemos)
        {
            CertificateDefinitionInfo CertificateDefinition = new CertificateDefinitionInfo(
              (int)reader["CertID"],
              reader["CertName"].ToString(),
              reader["CertBody"].ToString(),
              reader["CertType"].ToString(),
              (int)reader["FacilityID"]);

            return CertificateDefinition;
        }

        /// <summary>
        /// Returns a collection of CertificateDefinitionInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<CertificateDefinitionInfo> GetCertificateDefinitionCollectionFromReader(IDataReader reader)
        {
            return GetCertificateDefinitionCollectionFromReader(reader, true);
        }
        protected virtual List<CertificateDefinitionInfo> GetCertificateDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<CertificateDefinitionInfo> CertificateDefinitions = new List<CertificateDefinitionInfo>();
            while (reader.Read())
                CertificateDefinitions.Add(GetCertificateDefinitionFromReader(reader, readMemos));
            return CertificateDefinitions;
        }
        #endregion
    }
}
#endregion