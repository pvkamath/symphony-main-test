﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

/// <summary>
/// Summary description for ProMembershipSQL
/// </summary>
/// 

#region ProMembershipInfo
namespace PearlsReview.DAL
{
    public class ProMembershipInfo
    {
        public ProMembershipInfo(int memberid, int userid, DateTime paydate, DateTime expiredate, decimal payamount,
                int facilityid, string authcode, string profileid, bool active, bool renew, decimal couponamount, int couponid,
                string cclast, string comment, DateTime lastupdate)
        {
            this.Memberid = memberid;
            this.Userid = userid;
            this.Paydate = paydate;
            this.Expiredate = expiredate;
            this.Payamount = payamount;
            this.Facilityid = facilityid;
            this.Authcode = authcode;
            this.profileid = profileid;
            this.Active = active;
            this.Renew = renew;
            this.Couponamount = couponamount;
            this.Couponid = couponid;
            this.Cclast = cclast;
            this.Comment = comment;
            this.Lastupdate = lastupdate;
        }

        private int _memberid = 0;
        public int Memberid
        {
            get { return _memberid; }
            set { _memberid = value; }
        }
        private int _userid = 0;
        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }
        private DateTime _paydate = System.DateTime.MinValue;
        public DateTime Paydate
        {
            get { return _paydate; }
            set { _paydate = value; }
        }
        private DateTime _expiredate = System.DateTime.MinValue;
        public DateTime Expiredate
        {
            get { return _expiredate; }
            set { _expiredate = value; }
        }
        private decimal _payamount = 0;
        public decimal Payamount
        {
            get { return _payamount; }
            set { _payamount = value; }
        }
        private int _facilityid = 0;
        public int Facilityid
        {
            get { return _facilityid; }
            set { _facilityid = value; }
        }
        private string _authcode;
        public string Authcode
        {
            get { return _authcode; }
            set { _authcode = value; }
        }
        private string _profileid;
        public string profileid
        {
            get { return _profileid; }
            set { _profileid = value; }
        }
        private Boolean _active = false;
        public Boolean Active
        {
            get { return _active; }
            set { _active = value; }
        }
        private Boolean _renew = false;
        public Boolean Renew
        {
            get { return _renew; }
            set { _renew = value; }
        }
        private decimal _couponamount = 0;
        public decimal Couponamount
        {
            get { return _couponamount; }
            set { _couponamount = value; }
        }
        private int _couponid = 0;
        public int Couponid
        {
            get { return _couponid; }
            set { _couponid = value; }
        }
        private string _cclast;
        public string Cclast
        {
            get { return _cclast; }
            set { _cclast = value; }
        }
        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }
        private DateTime _lastupdate = System.DateTime.MinValue;
        public DateTime Lastupdate
        {
            get { return _lastupdate; }
            set { _lastupdate = value; }
        }
    }
}
#endregion
#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        public int insertProMembership(ProMembershipInfo promembership)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into ProMembership " +
                    "(userid, " +
                    "paydate, " +
                    "expiredate, " +
                    "payamount, " +
                    "facilityid, " +
                    "authcode, " +
                    "profileid, " +
                    "active, " +
                    "renew, " +
                    "couponamount, " +
                    "couponid, " +
                    "cclast, " +
                    "comment, " +
                    "lastupdate) " +
                    "VALUES " +
                    "(@userid, " +
                    "@paydate, " +
                    "@expiredate, " +
                    "@payamount, " +
                    "@facilityid, " +
                    "@authcode, " +
                    "@profileid, " +
                    "@active, " +
                    "@renew, " +
                    "@couponamount, " +
                    "@couponid, " +
                    "@cclast, " +
                    "@comment, " +
                    "@lastupdate) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@userid", SqlDbType.Int).Value =
                    (promembership.Userid == null ? (Object)DBNull.Value : (Object)promembership.Userid);
                cmd.Parameters.Add("@paydate", SqlDbType.DateTime).Value = promembership.Paydate;
                cmd.Parameters.Add("@expiredate", SqlDbType.DateTime).Value = promembership.Expiredate;
                cmd.Parameters.Add("@payamount", SqlDbType.Decimal).Value = promembership.Payamount;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value =
                    (promembership.Facilityid == null ? (Object)DBNull.Value : (Object)promembership.Facilityid);
                cmd.Parameters.Add("@authcode", SqlDbType.VarChar).Value =
                    (promembership.Authcode == null ? (Object)DBNull.Value : (Object)promembership.Authcode);
                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value =
                    (promembership.profileid == null ? (Object)DBNull.Value : (Object)promembership.profileid);
                cmd.Parameters.Add("@active", SqlDbType.Bit).Value = promembership.Active;
                cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = promembership.Renew;
                cmd.Parameters.Add("@couponamount", SqlDbType.Decimal).Value = promembership.Couponamount;
                cmd.Parameters.Add("@couponid", SqlDbType.Int).Value =
                    (promembership.Couponid == null ? (Object)DBNull.Value : (Object)promembership.Couponid);
                cmd.Parameters.Add("@cclast", SqlDbType.VarChar).Value =
                    (promembership.Cclast == null ? (Object)DBNull.Value : (Object)promembership.Cclast);
                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value =
                    (promembership.Comment == null ? (Object)DBNull.Value : (Object)promembership.Comment);
                cmd.Parameters.Add("@lastupdate", SqlDbType.DateTime).Value = promembership.Lastupdate;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                return (int)IDParameter.Value;
            }
        }

        public bool updateProMembership(ProMembershipInfo promembership)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update ProMembership set " +
                    "userid = @userid, " +
                    "paydate = @paydate, " +
                    "expiredate = @expiredate, " +
                    "payamount = @payamount, " +
                    "facilityid = @facilityid, " +
                    "authcode = @authcode, " +
                    "profileid = @profileid, " +
                    "active = @active, " +
                    "renew = @renew, " +
                    "couponamount = @couponamount, " +
                    "couponid  = @couponid, " +
                    "cclast = @cclast, " +
                    "comment = @comment, " +
                    "lastupdate = @lastupdate " +
                    "where memberid = @memberid ", cn);
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value =
                    (promembership.Userid == null ? (Object)DBNull.Value : (Object)promembership.Userid);
                cmd.Parameters.Add("@paydate", SqlDbType.DateTime).Value = promembership.Paydate;
                cmd.Parameters.Add("@expiredate", SqlDbType.DateTime).Value = promembership.Expiredate;
                cmd.Parameters.Add("@payamount", SqlDbType.Decimal).Value = promembership.Payamount;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value =
                    (promembership.Facilityid == null ? (Object)DBNull.Value : (Object)promembership.Facilityid);
                cmd.Parameters.Add("@authcode", SqlDbType.VarChar).Value =
                    (promembership.Authcode == null ? (Object)DBNull.Value : (Object)promembership.Authcode);
                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value =
                    (promembership.profileid == null ? (Object)DBNull.Value : (Object)promembership.profileid);
                cmd.Parameters.Add("@active", SqlDbType.Bit).Value = promembership.Active;
                cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = promembership.Renew;
                cmd.Parameters.Add("@couponamount", SqlDbType.Decimal).Value = promembership.Couponamount;
                cmd.Parameters.Add("@couponid", SqlDbType.Int).Value =
                   (promembership.Couponid == null ? (Object)DBNull.Value : (Object)promembership.Couponid);
                cmd.Parameters.Add("@cclast", SqlDbType.VarChar).Value =
                    (promembership.Cclast == null ? (Object)DBNull.Value : (Object)promembership.Cclast);
                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value =
                    (promembership.Comment == null ? (Object)DBNull.Value : (Object)promembership.Comment);
                cmd.Parameters.Add("@lastupdate", SqlDbType.DateTime).Value = promembership.Lastupdate;
                cmd.Parameters.Add("@memberid", SqlDbType.Int).Value = promembership.Memberid;

                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public bool deleteProMembership(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from ProMembership where userid = @userid", cn);
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = UserID;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public ProMembershipInfo GetProMemeberShipByMemberID(int MemberID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From ProMembership Where memberid = @memberid", cn);
                cmd.Parameters.Add("@memberid", SqlDbType.Int).Value = MemberID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetProMembershipFromReader(reader);
                else
                    return null;
            }
        }
        public ProMembershipInfo GetProMemeberShipByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From ProMembership Where userid = @UserID", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetProMembershipFromReader(reader);
                else
                    return null;
            }
        }
        public ProMembershipInfo GetExpiredMemeberShipByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From ProMembership Where userid = @UserID and (expiredate < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME) or active = 0)", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetProMembershipFromReader(reader);
                else
                    return null;
            }
        }
        public decimal GetCEPROSavedTotalByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select sum(isnull(couponamount, 0)) as TotalSaved From orders Where userid = @UserID and facilityid <> 2 and couponid = 999", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return (!String.IsNullOrEmpty(reader["TotalSaved"].ToString())) ? decimal.Parse(reader["TotalSaved"].ToString()) : 0;
                else
                    return 0;
            }
        }
        public decimal GetCEPROMissedSavedTotalByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select sum(isnull((t.cost * 0.5)*t.quantity, 0)) as TotalMissedSaved From orders o join orderitem t" +
                        " on o.orderid = t.orderid Where t.media_type <> 'cepro' and o.userid = @UserID and o.facilityid <> 2 and o.couponid <> 999", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return (!String.IsNullOrEmpty(reader["TotalMissedSaved"].ToString())) ? decimal.Parse(reader["TotalMissedSaved"].ToString()) : 0;
                else
                    return 0;
            }
        }
        public ProMembershipInfo GetActiveProMemeberShipByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from ProMembership where userid = @UserID and active = 1", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetProMembershipFromReader(reader);
                else
                    return null;
            }
        }
        public List<ProMembershipInfo> DailyCEmembershipUpdate()
        {
            List<ProMembershipInfo> proMembershipInfo = new List<ProMembershipInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                    "from ProMembership where expiredate <= CAST(FLOOR(CAST(DATEADD(day,-3,GETDATE()) AS FLOAT)) AS DATETIME) " +
                    " and renew=1 and active=1", cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    ProMembershipInfo pinfo = GetProMembershipFromReader(reader);
                    proMembershipInfo.Add(pinfo);
                }
                return proMembershipInfo;
            }
        }
        public bool DailyCEUserAccountUpdate(int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_Daily_CEPayment_Update";
                cmd.Connection = cn;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret >= 1);
            }
        }
        public DataSet GetCEUserAccountExpireDate()
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "select U.cemail, U.iid, p.expiredate, P.cclast from useraccount U inner join ProMembership P on U.iid=P.userid and P.active=1 and P.renew=1 " +
                    " where p.expiredate >= CAST(FLOOR(CAST(DATEADD(day,2,GETDATE()) AS FLOAT)) AS DATETIME) and p.expiredate < CAST(FLOOR(CAST(DATEADD(day,3,GETDATE()) AS FLOAT)) AS DATETIME) and P.facilityid=5";
                SqlCommand cmd = new SqlCommand(qry, cn);
                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds);

            }
            return ds;
        }

        public DataSet GetCEUserAccountExpireDateWithoutRenew()
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "select U.cemail, U.iid, p.expiredate, P.cclast,P.ProfileId from useraccount U inner join ProMembership P on U.iid=P.userid and P.renew=0 " +
                    " where p.expiredate >= CAST(FLOOR(CAST(DATEADD(day,2,GETDATE()) AS FLOAT)) AS DATETIME) and p.expiredate < CAST(FLOOR(CAST(DATEADD(day,3,GETDATE()) AS FLOAT)) AS DATETIME) and P.facilityid=5";
                SqlCommand cmd = new SqlCommand(qry, cn);
                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds);

            }
            return ds;
        }

        public int UpdateDailyCEMembershipGracePeriodExpireDate()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_Update_Daily_CEMembership_Grace_Period";
                cmd.Connection = cn;
                cn.Open();
                return (int)cmd.ExecuteNonQuery();

            }
        }
        public int UpdateFailedRenewDailyCEMembership(int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_Update_Failed_Renew_Daily_CEMembership";
                cmd.Connection = cn;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cn.Open();
                return (int)cmd.ExecuteNonQuery();
            }
        }
        public int CEMembershipCancelAutoRenew(int userid, string comment)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_CEPro_Membership_Cancel_Renew";
                cmd.Connection = cn;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = comment;
                cn.Open();
                return (int)cmd.ExecuteNonQuery();
            }
        }

        public int CEMembershipCancelMembership(int userid, string comment)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_CEPro_Membership_Cancel";
                cmd.Connection = cn;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = comment;
                cn.Open();
                return (int)cmd.ExecuteNonQuery();
            }
        }
        public int CEMembershipActiveAutoRenewNewProfile(int userid, string ProfileID, string CCLast)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_CEPro_Membership_active_auto_Renew_New_Profile";
                cmd.Connection = cn;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@ProfileID", SqlDbType.VarChar).Value = ProfileID;
                cmd.Parameters.Add("@CCLast", SqlDbType.VarChar).Value = CCLast;
                cn.Open();
                return (int)cmd.ExecuteNonQuery();
            }
        }
        public int CEMembershipActiveAutoRenew(int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_CEPro_Membership_active_auto_Renew";
                cmd.Connection = cn;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cn.Open();
                return (int)cmd.ExecuteNonQuery();
            }
        }
        public int CEMembershipActiveAutoRenew(int userid, string profileid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_CEPro_Membership_active_auto_Renew_profile";
                cmd.Connection = cn;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = profileid;
                cn.Open();
                return (int)cmd.ExecuteNonQuery();
            }
        }
        public int RenewCEMembership(int userid, string AuthorizationCode, string ProfileId, string cCCRightFour, 
            decimal CEPROPrice, DateTime ExpireDate, decimal couponAmount, int couponID, bool renew)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_CEPro_Membership_Renew";
                cmd.Connection = cn;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@AutoCode", SqlDbType.VarChar).Value = AuthorizationCode;
                cmd.Parameters.Add("@ProfileID", SqlDbType.VarChar).Value = ProfileId;
                cmd.Parameters.Add("@CCLastFour", SqlDbType.VarChar).Value = cCCRightFour;
                cmd.Parameters.Add("@CEProPrice", SqlDbType.Decimal).Value = CEPROPrice;
                cmd.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value = ExpireDate;
                cmd.Parameters.Add("@couponAmount", SqlDbType.Decimal).Value = couponAmount;
                cmd.Parameters.Add("@couponID", SqlDbType.Int).Value = couponID;
                cmd.Parameters.Add("@renew", SqlDbType.Int).Value = renew;
                cn.Open();
                return (int)cmd.ExecuteNonQuery();
            }
        }
        public List<ProMembershipInfo> DailyCEMembershipCancelledAutoRenewUpdate()
        {
            List<ProMembershipInfo> proMembershipInfoInfo = new List<ProMembershipInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                "from ProMembership where expiredate>=CAST(FLOOR(CAST(DATEADD(day,-1,GETDATE()) AS FLOAT)) AS DATETIME) and expiredate <CAST(FLOOR(CAST(DATEADD(day,0,GETDATE()) AS FLOAT)) AS DATETIME) " +
                    " and renew=0 and active=1", cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    ProMembershipInfo pinfo = GetProMembershipFromReader(reader);
                    proMembershipInfoInfo.Add(pinfo);
                }
                return proMembershipInfoInfo;
            }
        }
        public List<ProMembershipInfo> DailyCEMembershipExpiredMembershipNoAutoRenew()
        {
            List<ProMembershipInfo> proMembershipInfoInfo = new List<ProMembershipInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * from Promembership where expiredate < CAST(FLOOR(CAST(DATEADD(day,0,GETDATE()) AS FLOAT)) AS DATETIME) and renew=0 and active=1", cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    ProMembershipInfo pinfo = GetProMembershipFromReader(reader);
                    proMembershipInfoInfo.Add(pinfo);
                }
                return proMembershipInfoInfo;
            }
        }
        public DataTable GetCEProMembershipSearchResult(int UserID, string FirstName, string LastName, string Paydate, string ExpireDate, string Active, string Renew, string OrderBy)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "microsite_CEPro_Membership_Search";
                cmd.Connection = cn;
                cmd.Parameters.Add("@userID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = FirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = LastName;
                cmd.Parameters.Add("@paydate", SqlDbType.VarChar).Value = Paydate;
                cmd.Parameters.Add("@expiredate", SqlDbType.VarChar).Value = ExpireDate;
                cmd.Parameters.Add("@active", SqlDbType.VarChar).Value = Active;
                cmd.Parameters.Add("@renew", SqlDbType.VarChar).Value = Renew;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = OrderBy;
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(dt);
            }
            return dt;
        }
        #endregion


        #region PRProvider

        protected virtual ProMembershipInfo GetProMembershipFromReader(IDataReader reader)
        {
            return GetProMembershipFromReader(reader, true);
        }
        protected virtual ProMembershipInfo GetProMembershipFromReader(IDataReader reader, bool readMemos)
        {
            ProMembershipInfo info = new ProMembershipInfo(
                Convert.IsDBNull(reader["memberid"]) ? (int)0 : (int)reader["memberid"],
                Convert.IsDBNull(reader["userid"]) ? (int)0 : (int)reader["userid"],
                Convert.IsDBNull(reader["paydate"]) ? DateTime.MinValue : Convert.ToDateTime(reader["paydate"]),
                Convert.IsDBNull(reader["expiredate"]) ? DateTime.MinValue : Convert.ToDateTime(reader["expiredate"]),
                Convert.IsDBNull(reader["payamount"]) ? (decimal)0 : (decimal)reader["payamount"],
                Convert.IsDBNull(reader["facilityid"]) ? (int)0 : (int)reader["facilityid"],
                Convert.ToString(reader["authcode"].ToString()),
                Convert.ToString(reader["profileid"].ToString()),
                Convert.IsDBNull(reader["active"]) ? (Boolean)false : (Boolean)reader["active"],
                Convert.IsDBNull(reader["renew"]) ? (Boolean)false : (Boolean)reader["renew"],
                Convert.IsDBNull(reader["couponamount"]) ? (decimal)0 : (decimal)reader["couponamount"],
                Convert.IsDBNull(reader["couponid"]) ? (int)0 : (int)reader["couponid"],
                Convert.ToString(reader["cclast"].ToString()),
                Convert.ToString(reader["comment"].ToString()),
                Convert.IsDBNull(reader["lastupdate"]) ? DateTime.MinValue : Convert.ToDateTime(reader["lastupdate"]));

            return info;
        }

        protected virtual List<ProMembershipInfo> GetProMembershipCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<ProMembershipInfo> ProMembershipList = new List<ProMembershipInfo>();
            while (reader.Read())
            {
                ProMembershipList.Add(GetProMembershipFromReader(reader, readMemos));
            }
            return ProMembershipList;
        }
        protected virtual List<ProMembershipInfo> GetProMembershipCollectionFromReader(IDataReader reader)
        {
            return GetProMembershipCollectionFromReader(reader, true);
        }

        #endregion

    }
}
#endregion

