﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.DAL.SQLClient
{

    // This code..copied frm PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        static private SQL2PRProvider _instance = null;
        /// <summary>
        /// Returns an instance of the provider type specified in the config file
        /// </summary>
        static public SQL2PRProvider Instance
        {
            get
            {
                if (_instance == null)
                    _instance = (SQL2PRProvider)Activator.CreateInstance(Type.GetType("PearlsReview.DAL.SQLClient.SQL2PRProvider"));
                    
                    //_instance = (PRProvider)Activator.CreateInstance(Type.GetType(Globals.Settings.PR.ProviderType));  //Note: IN ConfigSection.cs defaul provider=[ConfigurationProperty("providerType", DefaultValue = "PearlsReview.DAL.SQLClient.SQLPRProvider")]
                return _instance;
            }
        }
        public SQL2PRProvider()
        {
            this.ConnectionString = ConfigurationManager.ConnectionStrings["HealthCare"] != null ? ConfigurationManager.ConnectionStrings["HealthCare"].ToString() : ""; 
            //"Server=66.195.143.62;Database=ECE_LS;User=GHC_Reader;Password=D@tA,US@R!;";

            this.EnableCaching = false;
            this.CacheDuration = 0;
            this.RConnectionString = "";
            /*
            this.ConnectionString = Globals.Settings.PR.ConnectionString;
            this.EnableCaching = Globals.Settings.PR.EnableCaching;
            this.CacheDuration = Globals.Settings.PR.CacheDuration;
            this.RConnectionString = Globals.Settings.PR.RConnectionString;
            */ 
        }
    }
}
