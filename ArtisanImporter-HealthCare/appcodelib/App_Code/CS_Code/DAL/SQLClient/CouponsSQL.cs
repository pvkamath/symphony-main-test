﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region CouponInfo

namespace PearlsReview.DAL
{
    public class CouponsInfo
    {
        public CouponsInfo(int cid, string couponCode, string couponDesc, decimal cAmount, DateTime expireDate,
            int? facilityID, string couponType, int? topicID, string mediaType, decimal? hours, int? uLimit, int? userID, 
            bool oneTimePerUserInd)
        {
            this.CID = cid;
            this.CouponCode = couponCode;
            this.CouponDesc = couponDesc;
            this.CAmount = cAmount;
            this.ExpireDate = expireDate;
            this.FacilityID = facilityID;
            this.CouponType = couponType;
            this.TopicID = topicID;
            this.MediaType = mediaType;
            this.Hours = hours;
            this.ULimit = uLimit;
            this.UserID = userID;
            this.OneTimePerUserInd = oneTimePerUserInd;
        }

        private int _CID = 0;
        public int CID
        {
            get { return _CID; }
            set { _CID = value; }
        }

        private string _CouponCode = "";
        public string CouponCode
        {
            get { return _CouponCode; }
            set { _CouponCode = value; }
        }

        private string _CouponDesc = "";
        public string CouponDesc
        {
            get { return _CouponDesc; }
            set { _CouponDesc = value; }
        }

        private decimal _CAmount = 0;
        public decimal CAmount
        {
            get { return _CAmount; }
            set { _CAmount = value; }
        }

        private DateTime _ExpireDate = System.DateTime.MinValue;
        public DateTime ExpireDate
        {
            get { return _ExpireDate; }
            set { _ExpireDate = value; }
        }

        private int? _FacilityID = null;
        public int? FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }

        private string _CouponType = "";
        public string CouponType
        {
            get { return _CouponType; }
            set { _CouponType = value; }
        }

        private int? _TopicID = null;
        public int? TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }


        private string _MediaType = null;
        public string MediaType
        {
            get { return _MediaType; }
            set { _MediaType = value; }
        }

        private decimal? _hours = null;
        public decimal? Hours
        {
            get { return _hours; }
            set { _hours = value; }
        }

        private int? _ulimit = null;
        public int? ULimit
        {
            get { return _ulimit; }
            set { _ulimit = value; }
        }

        private int? _userid = null;
        public int? UserID
        {
            get { return _userid; }
            set { _userid = value; }
        }







        private Boolean _oneTimePerUserInd = false;
        public Boolean OneTimePerUserInd
        {
            get { return _oneTimePerUserInd; }
            set { _oneTimePerUserInd = value; }
        }
        public string UserDesc { get; set; }
    }

}

#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        public int insertCoupon(CouponsInfo coupon)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Coupons " +
                    "(couponcode, " +
                    "coupondesc, " +
                    "camount, " +
                    "expiredate, " +
                    "facilityid, " +
                    "coupontype, " +
                    "topicid, " +
                    "mediatype, " +
                    "hours, " +
                    "ulimit, " +
                    "userid, " + 
                    "userDesc," +
                    "onetimeperuserind) " +
                    "VALUES " +
                    "(@couponcode, " +
                    "@coupondesc, " +
                    "@camount, " +
                    "@expiredate, " +
                    "@facilityid, " +
                    "@coupontype, " +
                    "@topicid, " +
                    "@mediatype, " +
                    "@hours, " +
                    "@ulimit, " +
                    "@userid, " + 
                    "@userDesc," +
                    "@onetimeperuserind) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@couponcode", SqlDbType.VarChar).Value = coupon.CouponCode;
                cmd.Parameters.Add("@coupondesc", SqlDbType.VarChar).Value = coupon.CouponDesc;
                cmd.Parameters.Add("@camount", SqlDbType.Decimal).Value = coupon.CAmount;
                cmd.Parameters.Add("@expiredate", SqlDbType.DateTime).Value = coupon.ExpireDate;
                //cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = coupon.FacilityID;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = 
                    (coupon.FacilityID == null ? (Object)DBNull.Value : (Object)coupon.FacilityID);
                cmd.Parameters.Add("@coupontype", SqlDbType.Char).Value = coupon.CouponType;
                //cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = coupon.TopicID;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = 
                    (coupon.TopicID == null ? (Object)DBNull.Value : (Object)coupon.TopicID);
                //cmd.Parameters.Add("@mediatype", SqlDbType.Char).Value = coupon.MediaType;
                cmd.Parameters.Add("@mediatype", SqlDbType.Char).Value =
                    (coupon.MediaType == null ? (Object)DBNull.Value : (Object)coupon.MediaType);
                //cmd.Parameters.Add("@hours", SqlDbType.Decimal).Value = coupon.Hours;
                cmd.Parameters.Add("@hours", SqlDbType.Decimal).Value = 
                    (coupon.Hours == null ? (Object)DBNull.Value : (Object)coupon.Hours);
                //cmd.Parameters.Add("@ulimit", SqlDbType.Int).Value = coupon.ULimit;
                cmd.Parameters.Add("@ulimit", SqlDbType.Int).Value = 
                    (coupon.ULimit == null ? (Object)DBNull.Value : (Object)coupon.ULimit);
                //cmd.Parameters.Add("@userid", SqlDbType.Int).Value = coupon.UserID;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value =
                    (coupon.UserID == null ? (Object)DBNull.Value : (Object)coupon.UserID);
                cmd.Parameters.Add("@onetimeperuserind", SqlDbType.Bit).Value = coupon.OneTimePerUserInd;


                cmd.Parameters.Add("@userDesc", SqlDbType.VarChar).Value = coupon.UserDesc;



                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                return (int)IDParameter.Value;
            }
        }

        public bool updateCoupon(CouponsInfo coupon)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Coupons set " +
                    "couponcode = @couponcode, " +
                    "coupondesc = @coupondesc, " +
                    "camount = @camount, " +
                    "expiredate = @expiredate, " +
                    "facilityid = @facilityid, " +
                    "coupontype = @coupontype, " +
                    "topicid = @topicid, " +
                    "mediatype = @mediatype, " +
                    "hours = @hours, " +
                    "ulimit = @ulimit, " +
                    "userid  = @userid, " +
                    "userDesc =@userDesc, " +
                    "onetimeperuserind = @onetimeperuserind " +
                    "where cid = @cid ", cn);

                cmd.Parameters.Add("@couponcode", SqlDbType.VarChar).Value = coupon.CouponCode;
                cmd.Parameters.Add("@coupondesc", SqlDbType.VarChar).Value = coupon.CouponDesc;
                cmd.Parameters.Add("@camount", SqlDbType.Decimal).Value = coupon.CAmount;
                cmd.Parameters.Add("@expiredate", SqlDbType.DateTime).Value = coupon.ExpireDate;
                //cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = coupon.FacilityID;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value =
                    (coupon.FacilityID == null ? (Object)DBNull.Value : (Object)coupon.FacilityID);
                cmd.Parameters.Add("@coupontype", SqlDbType.Char).Value = coupon.CouponType;
                //cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = coupon.TopicID;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value =
                    (coupon.TopicID == null ? (Object)DBNull.Value : (Object)coupon.TopicID);
                //cmd.Parameters.Add("@mediatype", SqlDbType.Char).Value = coupon.MediaType;
                cmd.Parameters.Add("@mediatype", SqlDbType.Char).Value =
                    (coupon.MediaType == null ? (Object)DBNull.Value : (Object)coupon.MediaType);
                //cmd.Parameters.Add("@hours", SqlDbType.Decimal).Value = coupon.Hours;
                cmd.Parameters.Add("@hours", SqlDbType.Decimal).Value =
                    (coupon.Hours == null ? (Object)DBNull.Value : (Object)coupon.Hours);
                //cmd.Parameters.Add("@ulimit", SqlDbType.Int).Value = coupon.ULimit;
                cmd.Parameters.Add("@ulimit", SqlDbType.Int).Value =
                    (coupon.ULimit == null ? (Object)DBNull.Value : (Object)coupon.ULimit);
                //cmd.Parameters.Add("@userid", SqlDbType.Int).Value = coupon.UserID;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value =
                    (coupon.UserID == null ? (Object)DBNull.Value : (Object)coupon.UserID);


                cmd.Parameters.Add("@userDesc", SqlDbType.VarChar).Value = coupon.UserDesc;


                cmd.Parameters.Add("@onetimeperuserind", SqlDbType.Bit).Value = coupon.OneTimePerUserInd;
                cmd.Parameters.Add("@cid", SqlDbType.Int).Value = coupon.CID;

                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public bool deleteCoupon(int couponID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Coupons where cid = @cid", cn);
                cmd.Parameters.Add("@cid", SqlDbType.Int).Value = couponID;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public List<CouponsInfo> GetCouponsBySearchCriteria(string cid, string couponCode, string couponDesc, string facilityID,
            string topicID, string userID, string expiredAfter, string orderBy)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_get_coupons_by_search_criteria", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@cid", SqlDbType.VarChar).Value = cid;
                cmd.Parameters.Add("@couponCode", SqlDbType.VarChar).Value = couponCode;
                cmd.Parameters.Add("@couponDesc", SqlDbType.VarChar).Value = couponDesc;
                cmd.Parameters.Add("@facilityID", SqlDbType.VarChar).Value = facilityID;
                cmd.Parameters.Add("@topicID", SqlDbType.VarChar).Value = topicID;
                cmd.Parameters.Add("@userID", SqlDbType.VarChar).Value = userID;
                cmd.Parameters.Add("@expireAfter", SqlDbType.VarChar).Value = expiredAfter;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = orderBy;
                cn.Open();
                return GetCouponCollectionFromReader(cmd.ExecuteReader());
            }
        }

        public CouponsInfo GetCouponByID(int cid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From Coupons Where cid = @cid", cn);
                cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCouponsFromReader(reader);
                else
                    return null;
            }
        }

        public List<CouponsInfo> GetCoupons(string facilityid, string sortExpression)
        {
            //List<CouponsInfo> coups = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "SELECT * FROM Coupons";

                if (facilityid.Length > 0)
                {
                    cSQLCommand = cSQLCommand + " where facilityid= " + facilityid;
                }

                // add on ORDER BY if provided
                if (sortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + sortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by couponcode";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                List<CouponsInfo> coups = GetCouponCollectionFromReader(reader);
                return coups;
                //coups = new List<CouponsInfo>();
                //while (reader.Read())
                //{
                //    CouponsInfo coup = new CouponsInfo();
                //    coup.CID = reader["cid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["cid"]);
                //    coup.CouponCode = reader["couponcode"].ToString();
                //    coup.CouponDesc = reader["coupondesc"].ToString();
                //    coup.CAmount = reader["camount"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["camount"]);
                //    coup.ExpireDate = reader["expiredate"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(reader["expiredate"]);
                //    coup.FacilityID = reader["facilityid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["facilityid"]);
                //    coup.CouponType = reader["coupontype"].ToString();
                //    coup.TopicID = reader["topicid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["topicid"]);
                //    coup.MediaType = reader["mediatype"].ToString();
                //    coups.Add(coup);

                //}
            }
            //return coups;
        }

        public CouponsInfo GetCouponByFacID(int facilityid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Coupons where facilityid=@facilityid", cn);
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCouponsFromReader(reader, true);
                else
                    return null;
            }
        }

        public CouponsInfo GetCouponByCode(string Code)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                          "from Coupons where couponcode=@Code", cn);
                cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = Code;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCouponsFromReader(reader, true);
                else
                    return null;

            }
        }

        public CouponsInfo GetUCECouponByCode(string Code)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                          "from Coupons where couponcode=@Code and coupontype='M' and (facilityid is null or facilityid=2) and expiredate>=getdate()", cn);
                cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = Code;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCouponsFromReader(reader, true);
                else
                    return null;
            }
        }

        public int GetCouponIdByCode(string Code)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select cid from coupons where couponcode=@Code and expiredate >= getdate()", cn);
                cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = Code.Trim();
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                if (reader.Read())
                {
                    return (int)reader[0];
                }
                else
                {
                    return 0;
                }
            }
        }

        public int GetCouponIdByCode(string Code, int FacilityId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select cid from coupons where couponcode=@Code and FacilityId = @FacilityId and expiredate >=getdate()", cn);
                cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = Code.Trim();
                cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = FacilityId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                if (reader.Read())
                {
                    return (int)reader[0];
                }
                else
                {
                    return 0;
                }
            }
        }

        #endregion


        #region PRProvider

        protected virtual CouponsInfo GetCouponsFromReader(IDataReader reader)
        {
            return GetCouponsFromReader(reader, true);
        }
        protected virtual CouponsInfo GetCouponsFromReader(IDataReader reader, bool readMemos)
        {
            CouponsInfo Coupons = new CouponsInfo(
                Convert.IsDBNull(reader["CID"]) ? (int)0 : (int)reader["CID"],
                Convert.ToString(reader["CouponCode"].ToString()),
                Convert.ToString(reader["CouponDesc"].ToString()),
                Convert.IsDBNull(reader["CAmount"]) ? (decimal)0 : (decimal)reader["CAmount"],
                Convert.IsDBNull(reader["ExpireDate"]) ? DateTime.MinValue : Convert.ToDateTime(reader["ExpireDate"]),
                Convert.IsDBNull(reader["FacilityID"]) ? (int?)null : (int?)reader["FacilityID"],
                Convert.ToString(reader["CouponType"].ToString()),
                Convert.IsDBNull(reader["TopicID"]) ? (int?)null : (int?)reader["TopicID"],
                Convert.IsDBNull(reader["MediaType"]) ? (String)null : (String)reader["MediaType"],
                Convert.IsDBNull(reader["hours"]) ? (decimal?)null : (decimal?)reader["hours"],
                Convert.IsDBNull(reader["ulimit"]) ? (int?)null : (int?)reader["ulimit"],
                Convert.IsDBNull(reader["userid"]) ? (int?)null : (int?)reader["userid"], 
                Convert.IsDBNull(reader["onetimeperuserind"]) ? (Boolean)false : (Boolean)reader["onetimeperuserind"]
                );

            Coupons.UserDesc = Convert.IsDBNull(reader["userdesc"]) ? "" : (string)reader["userdesc"];
            return Coupons;
        }

        protected virtual List<CouponsInfo> GetCouponCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<CouponsInfo> couponList = new List<CouponsInfo>();
            while (reader.Read())
            {
                couponList.Add(GetCouponsFromReader(reader, readMemos));
            }
            return couponList;
        }

        protected virtual List<CouponsInfo> GetCouponCollectionFromReader(IDataReader reader)
        {
            return GetCouponCollectionFromReader(reader, true);
        }

        #endregion

    }
}
#endregion
