﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for MicrositeInterestSQL
/// </summary>
/// 
 #region MicrositeInterestInfo

namespace PearlsReview.DAL
{

public class MicrositeInterestInfo
{
    public MicrositeInterestInfo()
	{
		//
		// TODO: Add constructor logic here
		//
	}          

       #region Variables and Properties

        private int _miid= 0;
        public int miid
        {
            get { return _miid; }
            protected set { _miid = value; }
        }
        public int _msid = 0;
        public int msid
        {
            get { return _msid; }
            set { _msid = value; }
        }
        public string _interest_desc = "";
        public string interest_desc
        {
            get { return _interest_desc; }
            set { _interest_desc = value; }
        }        
        
        public MicrositeInterestInfo(int miid, int msid, string interest_desc)
        {
            this.miid = miid;
            this.msid = msid;
            this.interest_desc = interest_desc;            
        }

       #endregion
    }
}

#endregion
#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        /// <summary>
        /// Inserts a new MicrositeInterest
        /// </summary>
        public int InsertMicrositeInterest(MicrositeInterestInfo MicrositeInterest)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into MicrositeInterest " +
                "( msid, " +                
                "interest_desc ) " +
                "VALUES ( " +
                "@msid, " +                
                "@interest_desc ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeInterest.msid;
                cmd.Parameters.Add("@interest_desc", SqlDbType.VarChar).Value = MicrositeInterest.interest_desc;                
                cn.Open();
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;
            }
        }


        /// <summary>
        /// Updates a MicrositeInterest
        /// </summary>
        public bool UpdateMicrositeInterest(MicrositeInterestInfo MicrositeInterest)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update MicrositeInterest set " +
              "msid = @msid, " +             
              "interest_desc = @interest_desc " +
              " where miid = @miid ", cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeInterest.msid;
                cmd.Parameters.Add("@interest_desc", SqlDbType.VarChar ).Value = MicrositeInterest.interest_desc;               
                cmd.Parameters.Add("@miid", SqlDbType.Int).Value = MicrositeInterest.miid;
               
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Retrieves the MicrositeInterest with the specified ID
        /// </summary>
        public MicrositeInterestInfo GetMicrositeInterestByID(int id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositeInterest where miid=@id", cn);

                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositeInterestFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the MicrositeInterest with the specified ID and Name
        /// </summary>
        public MicrositeInterestInfo GetMicrositeInterestBymsMsIdAndName(int msid, string Interest_desc)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositeInterest where msid=@msid and interest_desc=@interest_desc", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cmd.Parameters.Add("@interest_desc", SqlDbType.VarChar).Value = Interest_desc;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositeInterestFromReader(reader, true);
                else
                    return null;
            }
        }
        
        /// <summary>
        /// Retrieves all MicrositeInterest for the specified msid
        /// </summary>
        public List<MicrositeInterestInfo> GetMicrositeInterestsBymsid(int msid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "miid, " +
                    "msid, " +                    
                    "interest_desc " +
                    "from MicrositeInterest " +
                    "where msid = @msid " +
                    "order by " + cSortExpression;

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cn.Open();
                return GetMicrositeInterestCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Deletes a MicrositeInterest
        /// </summary>
        public bool DeleteMicrositeInterestById(int miid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from MicrositeInterest where miid=@miid", cn);
                cmd.Parameters.Add("@miid", SqlDbType.Int).Value = miid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

         #region PRProvider

        protected virtual MicrositeInterestInfo GetMicrositeInterestFromReader(IDataReader reader)
        {
            return GetMicrositeInterestFromReader(reader, true);
        }
        protected virtual MicrositeInterestInfo GetMicrositeInterestFromReader(IDataReader reader, bool readMemos)
        {
            MicrositeInterestInfo MicrositeInterest = new MicrositeInterestInfo(
              (int)reader["miid"],
              (int)reader["msid"],              
              (string)reader["interest_desc"]);

            return MicrositeInterest;
        }

        protected virtual List<MicrositeInterestInfo> GetMicrositeInterestCollectionFromReader(IDataReader reader)
        {
            return GetMicrositeInterestCollectionFromReader(reader, true);
        }
        protected virtual List<MicrositeInterestInfo> GetMicrositeInterestCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<MicrositeInterestInfo> MicrositeInterests = new List<MicrositeInterestInfo>();
            while (reader.Read())
                MicrositeInterests.Add(GetMicrositeInterestFromReader(reader, readMemos));
            return MicrositeInterests;
        }

        #endregion
    
    }
}
#endregion