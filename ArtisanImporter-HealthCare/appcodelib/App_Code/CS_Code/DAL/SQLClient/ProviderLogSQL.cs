﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ProviderLogSQL
/// </summary>
/// 

#region OrdersInfo
namespace PearlsReview.DAL
{
    public class ProviderLogInfo
    {
        public ProviderLogInfo() { }

        public ProviderLogInfo(int plid, int userid, int orderitemid, int discountid, DateTime feeddate, string provider, string responsetype, string responsetext)
        {
            this.plid = plid;
            this.userid = userid;
            this.orderitemid = orderitemid;
            this.discountid = discountid;
            this.feeddate = feeddate;
            this.provider = provider;
            this.responsetype = responsetype;
            this.responsetext = responsetext;
        }

        private int _plid;
        public int plid
        {
            get
            {
                return _plid;
            }
            private set
            {
                _plid = value;
            }
        }

        private int _userid;

        public int userid
        {
            get
            {
                return _userid;
            }
            set
            {
                _userid = value;
            }
        }

        private int _orderitemid;

        public int orderitemid
        {
            get
            {
                return _orderitemid;
            }
            set
            {
                _orderitemid = value;
            }
        }

        private int _discountid;

        public int discountid
        {
            get
            {
                return _discountid;
            }
            set
            {
                _discountid = value;
            }
        }


        private DateTime _feeddate;
        public DateTime feeddate
        {
            get
            {
                return _feeddate;
            }

            set
            {
                _feeddate = value;
            }
        }

        private string _provider;
        public string provider
        {
            get
            {
                return _provider;
            }
            set
            {
                _provider = value;
            }
        }

        private string _responsetype;
        public string responsetype
        {
            get
            {
                return _responsetype;
            }
            set
            {
                _responsetype = value;
            }
        }

        private string _responsetext;
        public string responsetext
        {
            get
            {
                return _responsetext;
            }
            set
            {
                _responsetext = value;
            }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider
namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    { 
        #region SQLPRProvider
        /// <summary>
        /// Inserts a new Orders
        /// </summary>
        public int InsertProviderLog(ProviderLogInfo ProviderLog)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into providerlog values ( " +
              " @userid, " +
              " @orderitemid, " +
              " @discountid, " +
              " @feeddate, " +
              " @provider, " +
              " @responsetype, " +
              " @responsetext ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = ProviderLog.userid;
                cmd.Parameters.Add("@orderitemid", SqlDbType.Int).Value = ProviderLog.orderitemid;
                cmd.Parameters.Add("@discountid", SqlDbType.VarChar).Value = ProviderLog.discountid;
                cmd.Parameters.Add("@feeddate", SqlDbType.VarChar).Value = ProviderLog.feeddate;
                cmd.Parameters.Add("@provider", SqlDbType.VarChar).Value = ProviderLog.provider == null ? DBNull.Value as Object : ProviderLog.provider as Object;
                cmd.Parameters.Add("@responsetype", SqlDbType.VarChar).Value = ProviderLog.responsetype == null ? DBNull.Value as Object : ProviderLog.responsetype as Object;
                cmd.Parameters.Add("@responsetext", SqlDbType.VarChar).Value = ProviderLog.responsetext == null ? DBNull.Value as Object : ProviderLog.responsetext as Object;
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }
         #endregion
    }
}
#endregion

public class ProviderLogSQL
    {
        public ProviderLogSQL()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }

