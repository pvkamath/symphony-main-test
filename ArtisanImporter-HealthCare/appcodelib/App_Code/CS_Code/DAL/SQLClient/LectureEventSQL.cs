﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;
using PearlsReview.BLL;

#region LectureEventInfo

namespace PearlsReview.DAL
{
    public class LectureEventInfo
    {
        public LectureEventInfo() { }

        public LectureEventInfo(int LectEvtID, int LectureID, DateTime StartDate, int Capacity, int Attendees, string Comment,
            string Facility, string City, string State, bool Finalized, decimal Hours, string EventType, DateTime EndDate,bool IsActive)
        {
            this.LectEvtID = LectEvtID;
            this.LectureID = LectureID;
            this.StartDate = StartDate;
            this.Capacity = Capacity;
            this.Attendees = Attendees;
            this.Comment = Comment;
            this.Facility = Facility;
            this.City = City;
            this.State = State;
            this.Finalized = Finalized;
            this.Hours = Hours;
            this.EventType = EventType;
            this.EndDate = EndDate;
            this.IsActive = IsActive;
        }

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            protected set { _LectEvtID = value; }
        }

        private int _LectureID = 0;
        public int LectureID
        {
            get { return _LectureID; }
            private set { _LectureID = value; }
        }

        private DateTime _StartDate = System.DateTime.Now;
        public DateTime StartDate
        {
            get { return _StartDate; }
            private set { _StartDate = value; }
        }

        private int _Capacity = 0;
        public int Capacity
        {
            get { return _Capacity; }
            private set { _Capacity = value; }
        }

        private int _Attendees = 0;
        public int Attendees
        {
            get { return _Attendees; }
            private set { _Attendees = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            private set { _Comment = value; }
        }

        private string _Facility = "";
        public string Facility
        {
            get { return _Facility; }
            private set { _Facility = value; }
        }

        private string _City = "";
        public string City
        {
            get { return _City; }
            private set { _City = value; }
        }

        private string _State = "";
        public string State
        {
            get { return _State; }
            private set { _State = value; }
        }

        private bool _Finalized = false;
        public bool Finalized
        {
            get { return _Finalized; }
            set { _Finalized = value; }
        }

        private decimal _Hours = 0;
        public decimal Hours
        {
            get { return _Hours; }
            set { _Hours = value; }
        }

        private string _EventType = "";
        public string EventType
        {
            get { return _EventType; }
            set { _EventType = value; }
        }

        private DateTime _EndDate = System.DateTime.Now;
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        private bool _IsActive = false;
        public bool IsActive
        {
            get { return _IsActive; }
            private set { _IsActive = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with LectureEvents

        /// <summary>
        /// Returns the total number of LectureEvents
        /// </summary>
        public  int GetLectureEventCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from LectureEvent", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all LectureEvents
        /// </summary>
        public  List<LectureEventInfo> GetLectureEvents(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from LectureEvent ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                { 
                        
                    cSQLCommand = cSQLCommand + "order by IsActive desc";
                
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetLectureEventCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all LectureEvents
        /// </summary>
        public List<LectureEventInfo> GetLectureEventsByLectureID(int LectureID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from LectureEvent where lectureid = @LectureID";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID;

                cn.Open();
                return GetLectureEventCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public DataSet GetAllLectureEvents(string cSortExpression)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "SELECT LectureDefinition.lecttitle, * FROM  "+
                    " LectureEvent INNER JOIN   LectureDefinition ON LectureEvent.lectureid = LectureDefinition.lectureid where isactive='true'"
;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                  else
                { 
                        
                    cSQLCommand = cSQLCommand + "order by startdate desc";
                
                }

                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = cSQLCommand;
           
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }

        public DataSet GetInactiveLectureEvents(string cSortExpression)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "SELECT LectureDefinition.lecttitle, * FROM  " +
                    " LectureEvent INNER JOIN   LectureDefinition ON LectureEvent.lectureid = LectureDefinition.lectureid where isactive='false'"
;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {

                    cSQLCommand = cSQLCommand + "order by startdate desc";

                }

                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = cSQLCommand;

                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }   
   
        /// <summary>
        /// Retrieves the LectureEvent with the specified ID
        /// </summary>
        public  LectureEventInfo GetLectureEventByID(int LectEvtID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from LectureEvent where LectEvtID=@LectEvtID", cn);
                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectEvtID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetLectureEventFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a LectureEvent
        /// </summary>
        public  bool DeleteLectureEvent(int LectEvtID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_DeleteLectureEvent", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectEvtID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }

        }

        /// <summary>
        /// Inserts a new LectureEvent
        /// </summary>
        public int InsertLectureEvent(LectureEventInfo LectureEvent)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into LectureEvent " +
              "(LectureID, " +
              "StartDate, " +
              "Capacity, " +
              "Attendees, " +
              "Comment, " +
              "Facility, " +
              "City, " +
              "State, " +
              "Finalized, " +
              "Hours, " +
              "EventType, " +
              "EndDate, " +
              "IsActive ) " +
              "VALUES (" +
              "@LectureID, " +
              "@StartDate, " +
              "@Capacity, " +
              "@Attendees, " +
              "@Comment, " +
              "@Facility, " +
              "@City, " +
              "@State, " +
              "@Finalized, " +
              "@Hours, " +
              "@EventType, " +
              "@EndDate, " +
              "@IsActive ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureEvent.LectureID;
                cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = LectureEvent.StartDate;
                cmd.Parameters.Add("@Capacity", SqlDbType.Int).Value = LectureEvent.Capacity;
                cmd.Parameters.Add("@Attendees", SqlDbType.Int).Value = LectureEvent.Attendees;
                cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = LectureEvent.Comment;
                cmd.Parameters.Add("@Facility", SqlDbType.VarChar).Value = LectureEvent.Facility;
                cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = LectureEvent.City;
                cmd.Parameters.Add("@State", SqlDbType.Char).Value = LectureEvent.State;
                cmd.Parameters.Add("@Finalized", SqlDbType.Bit).Value = LectureEvent.Finalized;
                cmd.Parameters.Add("@Hours", SqlDbType.Decimal).Value = LectureEvent.Hours;
                cmd.Parameters.Add("@EventType", SqlDbType.VarChar).Value = String.IsNullOrEmpty(LectureEvent.EventType) ? String.Empty : LectureEvent.EventType;
                cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = LectureEvent.EndDate;
                cmd.Parameters.Add("@IsActive", SqlDbType.Bit).Value = LectureEvent.IsActive;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;

                return NewID;
            }
        }

        /// <summary>
        /// Updates a LectureEvent
        /// </summary>
        public  bool UpdateLectureEvent(LectureEventInfo LectureEvent)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update LectureEvent set " +
              "LectureID = @LectureID, " +
              "StartDate = @StartDate, " +
              "Capacity = @Capacity, " +
              "Attendees = @Attendees, " +
              "Comment = @Comment, " +
              "Facility = @Facility, " +
              "City = @City, " +
              "State = @State, " +
              "Finalized = @Finalized, " +
              "Hours = @Hours, " +
              "EventType = @EventType, " + 
              "EndDate = @EndDate, " +
              "IsActive = @IsActive " +
              "where LectEvtID = @LectEvtID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureEvent.LectureID;
                cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = LectureEvent.StartDate;
                cmd.Parameters.Add("@Capacity", SqlDbType.Int).Value = LectureEvent.Capacity;
                cmd.Parameters.Add("@Attendees", SqlDbType.Int).Value = LectureEvent.Attendees;
                cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = LectureEvent.Comment;
                cmd.Parameters.Add("@Facility", SqlDbType.VarChar).Value = LectureEvent.Facility;
                cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = LectureEvent.City;
                cmd.Parameters.Add("@State", SqlDbType.Char).Value = LectureEvent.State;
                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectureEvent.LectEvtID;
                cmd.Parameters.Add("@Finalized", SqlDbType.Bit).Value = LectureEvent.Finalized;
                cmd.Parameters.Add("@Hours", SqlDbType.Decimal).Value = LectureEvent.Hours;
                cmd.Parameters.Add("@EventType", SqlDbType.VarChar).Value = String.IsNullOrEmpty(LectureEvent.EventType) ? String.Empty : LectureEvent.EventType;
                cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = LectureEvent.EndDate;
                cmd.Parameters.Add("@IsActive", SqlDbType.Bit).Value = LectureEvent.IsActive;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public int CheckForLectureEventTopic(string LectEvtID)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string str = "SELECT  COUNT(LectEvtID) FROM  LectureEventTopicLink WHERE (LectEvtID = " + LectEvtID + ")";

                SqlCommand cmd = new SqlCommand(str, cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }

        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new LectureEventInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual LectureEventInfo GetLectureEventFromReader(IDataReader reader)
        {
            return GetLectureEventFromReader(reader, true);
        }
        protected virtual LectureEventInfo GetLectureEventFromReader(IDataReader reader, bool readMemos)
        {
            LectureEventInfo LectureEvent = new LectureEventInfo(
              (int)reader["LectEvtID"],
              (int)reader["LectureID"],
              (DateTime)reader["StartDate"],
              (int)reader["Capacity"],
              (int)reader["Attendees"],
              reader["Comment"].ToString(),
              reader["Facility"].ToString(),
              reader["City"].ToString(),
              reader["State"].ToString(),
              (bool)reader["Finalized"],
              (decimal)(Convert.IsDBNull(reader["Hours"]) ? (decimal)0 : (decimal)reader["Hours"]),
              reader["EventType"].ToString(),
              (DateTime)reader["EndDate"],
             (bool)(Convert.IsDBNull(reader["IsActive"]) == null ? true : (bool)reader["IsActive"]));
            return LectureEvent;
        }

        /// <summary>
        /// Returns a collection of LectureEventInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<LectureEventInfo> GetLectureEventCollectionFromReader(IDataReader reader)
        {
            return GetLectureEventCollectionFromReader(reader, true);
        }
        protected virtual List<LectureEventInfo> GetLectureEventCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<LectureEventInfo> LectureEvents = new List<LectureEventInfo>();
            while (reader.Read())
                LectureEvents.Add(GetLectureEventFromReader(reader, readMemos));
            return LectureEvents;
        }

        #endregion
    }
}
#endregion