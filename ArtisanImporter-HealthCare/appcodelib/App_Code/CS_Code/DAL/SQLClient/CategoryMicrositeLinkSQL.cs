﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region CategoryMicrositeLinkInfo

namespace PearlsReview.DAL
{

    /// <summary> 
    /// Summary description for MicrositeDomainInfo
    /// Bhaskar N
    /// </summary>
    public class CategoryMicrositeLinkInfo
    {

        public CategoryMicrositeLinkInfo() { }


        public CategoryMicrositeLinkInfo(int msid, int categoryid)
        {
            this.Msid = msid;
            this.Categoryid = categoryid;
        }

        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }
        private int _categoryid = 0;
        public int Categoryid
        {
            get { return _categoryid; }
            protected set { _categoryid = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // Methods that work with MicrositeDomain
        // Bhaskar N

        /// <summary>
        /// Retrieves all MicrositeDomains
        /// </summary>
        public List<CategoryMicrositeLinkInfo> GetCategoryMicrositeLink(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from CategoryMicrositeLink ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryMicrositeLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the MicrositeDomain with the specified ID
        /// </summary>
        public CategoryMicrositeLinkInfo GetCategoryMicrositeLinkByID(int msid, int categoryid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from CategoryMicrositeLink where msid=@msid and categoryid = @categoryid", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = categoryid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCategoryMicrositeLinkFromReader(reader, true);
                else
                    return null;
            }
        }
        public List<CategoryMicrositeLinkInfo> GetCategoryMicrositeLinkByMsidID(int msid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from CategoryMicrositeLink where msid=@msid", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                
                cn.Open();
                return GetCategoryMicrositeLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public DataTable GetCategoryMicrositeLinkDTByMsidID(int msid)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select ml.*, c.id, c.title, c.facilityid " +
                        "from CategoryMicrositeLink ml left outer join categories c on ml.categoryid = c.id where msid=@msid", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;

                cn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
        /// <summary>
        /// Inserts a new Discount
        /// </summary>
        public int InsertCategoryMicrositeLink(CategoryMicrositeLinkInfo CategoryMicrositeLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into CategoryMicrositeLink " +
                  "(msid , " +
                  "categoryid ) VALUES (@msid, @categoryid)", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = CategoryMicrositeLink.Msid;
                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = CategoryMicrositeLink.Categoryid;
                
                SqlParameter IDParameter = new SqlParameter("@msid", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        public bool UpdateCategoryMicrositeLinkLists(List<string> CategoryIds, int Msid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                cn.Open();
                SqlTransaction transaction;
                SqlCommand cmd = cn.CreateCommand();
                transaction = cn.BeginTransaction();
                cmd.Connection = cn;
                cmd.Transaction = transaction;

                try
                {
                    cmd.CommandText = "delete from CategoryMicrositeLink where msid=@msid";
                    cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                    cmd.ExecuteNonQuery();

                    foreach (string CategoryID in CategoryIds)
                    {
                        cmd.CommandText = "insert into CategoryMicrositeLink (msid , categoryid) values (@msid, @categoryid)";
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                        cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = int.Parse(CategoryID);
                        cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                catch(Exception ex)
                {
                    transaction.Rollback();
                    return false;
                }
                return true;
            }
        }
            
        /// <summary>
        /// Updates a CategoryMicrositeLink
        /// </summary>
        public bool UpdateCategoryMicrositeLink(CategoryMicrositeLinkInfo CategoryMicrositeLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update CategoryMicrositeLink set " +
              "msid = @msid, " +
              "categoryid = @categoryid where msid = @msid and categoryid = @categoryid", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = CategoryMicrositeLink.Msid;
                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = CategoryMicrositeLink.Categoryid;
                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a Discount
        /// </summary>
        public bool DeleteCategoryMicrositeLink(int Msid, int categoryid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from CategoryMicrositeLink where msid=@msid and categoryid = @categoryid", cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = categoryid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider
        /////////////////////////////////////////////////////////
        // methods that work with Discount  

        /// <summary>
        /// Returns a new DiscountInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual CategoryMicrositeLinkInfo GetCategoryMicrositeLinkFromReader(IDataReader reader)
        {
            return GetCategoryMicrositeLinkFromReader(reader, true);
        }
        protected virtual CategoryMicrositeLinkInfo GetCategoryMicrositeLinkFromReader(IDataReader reader, bool readMemos)
        {
            CategoryMicrositeLinkInfo CategoryMicrositeLink = new CategoryMicrositeLinkInfo(
                  (int)reader["msid"],
                  (int)reader["categoryid"]);
            return CategoryMicrositeLink;
        }


        /// <summary>
        /// Returns a collection of DiscountInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<CategoryMicrositeLinkInfo> GetCategoryMicrositeLinkCollectionFromReader(IDataReader reader)
        {
            return GetCategoryMicrositeLinkCollectionFromReader(reader, true);
        }
        protected virtual List<CategoryMicrositeLinkInfo> GetCategoryMicrositeLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<CategoryMicrositeLinkInfo> CategoryMicrositeLink = new List<CategoryMicrositeLinkInfo>();
            while (reader.Read())
                CategoryMicrositeLink.Add(GetCategoryMicrositeLinkFromReader(reader, readMemos));
            return CategoryMicrositeLink;
        }

        #endregion

    }
}
#endregion
