﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region RolesInfo

namespace PearlsReview.DAL
{
    public class RoleInfo
    {
        public RoleInfo() { }

        public RoleInfo(int RoleID, string RoleName)
        {
            this.RoleID = RoleID;
            this.RoleName = RoleName;
        }

        public RoleInfo(int RoleID, string RoleName, string DisplayName)
        {
            this.RoleID = RoleID;
            this.RoleName = RoleName;
            this.DisplayName = DisplayName;
        }

        private int _RoleID = 0;
        public int RoleID
        {
            get { return _RoleID; }
            protected set { _RoleID = value; }
        }

        private string _RoleName = "";
        public string RoleName
        {
            get { return _RoleName; }
            private set { _RoleName = value; }
        }

        private string _DisplayName = "";
        public string DisplayName
        {
            get { return _DisplayName; }
            private set { _DisplayName = value; }
        }
    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Roles

        /// <summary>
        /// Returns the total number of Roles
        /// </summary>
        public int GetRoleCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Roles", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Roles
        /// </summary>
        public List<RoleInfo> GetRoles(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Roles";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    // default to RoleID order if none specified
                    cSQLCommand = cSQLCommand +
                        " order by RoleID ";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetRoleCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all system Roles
        /// </summary>
        public List<RoleInfo> GetSystemRoles(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Roles where priorder > 0";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    // default to RoleID order if none specified
                    cSQLCommand = cSQLCommand +
                        " order by RoleID ";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetRoleCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Role with the specified ID
        /// </summary>
        public RoleInfo GetRoleByID(int RoleID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Roles where RoleID=@RoleID", cn);
                cmd.Parameters.Add("@RoleID", SqlDbType.Int).Value = RoleID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetRoleFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a Role
        /// </summary>
        public bool DeleteRole(int RoleID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Roles where RoleID=@RoleID", cn);
                cmd.Parameters.Add("@RoleID", SqlDbType.Int).Value = RoleID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Role
        /// </summary>
        public int InsertRole(RoleInfo Role)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Roles " +
              "(RoleName) " +
              "VALUES (" +
              "@RoleName) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@RoleName", SqlDbType.VarChar).Value = Role.RoleName;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Role
        /// </summary>
        public bool UpdateRole(RoleInfo Role)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Roles set " +
              "RoleName = @RoleName " +
              "where RoleID = @RoleID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@RoleName", SqlDbType.VarChar).Value = Role.RoleName;
                cmd.Parameters.Add("@RoleID", SqlDbType.Int).Value = Role.RoleID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider


        /////////////////////////////////////////////////////////
        // methods that work with Roles

        /// <summary>
        /// Returns a new RoleInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual RoleInfo GetRoleFromReader(IDataReader reader)
        {
            return GetRoleFromReader(reader, true);
        }
        protected virtual RoleInfo GetRoleFromReader(IDataReader reader, bool readMemos)
        {
            RoleInfo Role = new RoleInfo(
              (int)reader["RoleID"],
              reader["RoleName"].ToString(),
              reader["DisplayName"].ToString());


            return Role;
        }

        /// <summary>
        /// Returns a collection of RoleInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<RoleInfo> GetRoleCollectionFromReader(IDataReader reader)
        {
            return GetRoleCollectionFromReader(reader, true);
        }
        protected virtual List<RoleInfo> GetRoleCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<RoleInfo> Roles = new List<RoleInfo>();
            while (reader.Read())
                Roles.Add(GetRoleFromReader(reader, readMemos));
            return Roles;
        }

        #endregion
    }
}
#endregion

