﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region LectureAttendeeInfo

namespace PearlsReview.DAL
{
    public class LectureAttendeeInfo
    {
        public LectureAttendeeInfo() { }

        public LectureAttendeeInfo(int LectAttID, int LectEvtID, int UserID, string UserName, string PW, string FirstName, string LastName, string MI, string Address1, string Address2, string City, string State, string Zip, string Email, DateTime DateAdded, bool Completed)
        {
            this.LectAttID = LectAttID;
            this.LectEvtID = LectEvtID;
            this.UserID = UserID;
            this.UserName = UserName;
            this.PW = PW;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.MI = MI;
            this.Address1 = Address1;
            this.Address2 = Address2;
            this.City = City;
            this.State = State;
            this.Zip = Zip;
            this.Email = Email;
            this.DateAdded = DateAdded;
            this.Completed = Completed;
        }

        private int _LectAttID = 0;
        public int LectAttID
        {
            get { return _LectAttID; }
            protected set { _LectAttID = value; }
        }

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            private set { _LectEvtID = value; }
        }

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            private set { _UserID = value; }
        }

        private string _UserName = "";
        public string UserName
        {
            get { return _UserName; }
            private set { _UserName = value; }
        }

        private string _PW = "";
        public string PW
        {
            get { return _PW; }
            private set { _PW = value; }
        }

        private string _FirstName = "";
        public string FirstName
        {
            get { return _FirstName; }
            private set { _FirstName = value; }
        }

        private string _LastName = "";
        public string LastName
        {
            get { return _LastName; }
            private set { _LastName = value; }
        }

        private string _MI = "";
        public string MI
        {
            get { return _MI; }
            private set { _MI = value; }
        }

        private string _Address1 = "";
        public string Address1
        {
            get { return _Address1; }
            private set { _Address1 = value; }
        }

        private string _Address2 = "";
        public string Address2
        {
            get { return _Address2; }
            private set { _Address2 = value; }
        }

        private string _City = "";
        public string City
        {
            get { return _City; }
            private set { _City = value; }
        }

        private string _State = "";
        public string State
        {
            get { return _State; }
            private set { _State = value; }
        }

        private string _Zip = "";
        public string Zip
        {
            get { return _Zip; }
            private set { _Zip = value; }
        }

        private string _Email = "";
        public string Email
        {
            get { return _Email; }
            private set { _Email = value; }
        }

        private DateTime _DateAdded = System.DateTime.Now;
        public DateTime DateAdded
        {
            get { return _DateAdded; }
            private set { _DateAdded = value; }
        }

        private bool _Completed = false;
        public bool Completed
        {
            get { return _Completed; }
            private set { _Completed = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with LectureAttendees

        /// <summary>
        /// Returns the total number of LectureAttendees
        /// </summary>
        public  int GetLectureAttendeeCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from LectureAttendee", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all LectureAttendees
        /// </summary>
        public  List<LectureAttendeeInfo> GetLectureAttendees(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from LectureAttendee";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetLectureAttendeeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the LectureAttendee with the specified ID
        /// </summary>
        public  LectureAttendeeInfo GetLectureAttendeeByID(int LectAttID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from LectureAttendee where LectAttID=@LectAttID", cn);
                cmd.Parameters.Add("@LectAttID", SqlDbType.Int).Value = LectAttID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetLectureAttendeeFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a LectureAttendee
        /// </summary>
        public  bool DeleteLectureAttendee(int LectAttID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from LectureAttendee where LectAttID=@LectAttID", cn);
                cmd.Parameters.Add("@LectAttID", SqlDbType.Int).Value = LectAttID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new LectureAttendee
        /// </summary>
        public  int InsertLectureAttendee(LectureAttendeeInfo LectureAttendee)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into LectureAttendee " +
              "(LectEvtID, " +
              "UserID, " +
              "UserName, " +
              "PW, " +
              "FirstName, " +
              "LastName, " +
              "MI, " +
              "Address1, " +
              "Address2, " +
              "City, " +
              "State, " +
              "Zip, " +
              "Email, " +
              "DateAdded, " +
              "Completed) " +
              "VALUES (" +
              "@LectEvtID, " +
              "@UserID, " +
              "@UserName, " +
              "@PW, " +
              "@FirstName, " +
              "@LastName, " +
              "@MI, " +
              "@Address1, " +
              "@Address2, " +
              "@City, " +
              "@State, " +
              "@Zip, " +
              "@Email, " +
              "@DateAdded, " +
              "@Completed) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectureAttendee.LectEvtID;

                // pass null to database if value is zero (nothing selected yet)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                // NOTE: This situation will possibly occur when setting up a new potential user who is
                // registering for a lecture. When the user's account has been set up, this link will be
                // put into place, but until then, it will be stored as a null in the database.
                if (LectureAttendee.UserID == 0)
                {
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = LectureAttendee.UserID;
                }

                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = LectureAttendee.UserName;
                cmd.Parameters.Add("@PW", SqlDbType.VarChar).Value = LectureAttendee.PW;
                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = LectureAttendee.FirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = LectureAttendee.LastName;
                cmd.Parameters.Add("@MI", SqlDbType.VarChar).Value = LectureAttendee.MI;
                cmd.Parameters.Add("@Address1", SqlDbType.VarChar).Value = LectureAttendee.Address1;
                cmd.Parameters.Add("@Address2", SqlDbType.VarChar).Value = LectureAttendee.Address2;
                cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = LectureAttendee.City;
                cmd.Parameters.Add("@State", SqlDbType.Char).Value = LectureAttendee.State;
                cmd.Parameters.Add("@Zip", SqlDbType.VarChar).Value = LectureAttendee.Zip;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = LectureAttendee.Email;
                cmd.Parameters.Add("@DateAdded", SqlDbType.DateTime).Value = LectureAttendee.DateAdded == DateTime.MinValue ? DateTime.Now : LectureAttendee.DateAdded;
                cmd.Parameters.Add("@Completed", SqlDbType.Bit).Value = LectureAttendee.Completed;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a LectureAttendee
        /// </summary>
        public  bool UpdateLectureAttendee(LectureAttendeeInfo LectureAttendee)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update LectureAttendee set " +
              "LectEvtID = @LectEvtID, " +
              "UserID = @UserID, " +
              "UserName = @UserName, " +
              "PW = @PW, " +
              "FirstName = @FirstName, " +
              "LastName = @LastName, " +
              "MI = @MI, " +
              "Address1 = @Address1, " +
              "Address2 = @Address2, " +
              "City = @City, " +
              "State = @State, " +
              "Zip = @Zip, " +
              "Email = @Email, " +
              "DateAdded = @DateAdded, " +
              "Completed = @Completed " +
              "where LectAttID = @LectAttID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectureAttendee.LectEvtID;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = LectureAttendee.UserID;
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = LectureAttendee.UserName;
                cmd.Parameters.Add("@PW", SqlDbType.VarChar).Value = LectureAttendee.PW;
                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = LectureAttendee.FirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = LectureAttendee.LastName;
                cmd.Parameters.Add("@MI", SqlDbType.VarChar).Value = LectureAttendee.MI;
                cmd.Parameters.Add("@Address1", SqlDbType.VarChar).Value = LectureAttendee.Address1;
                cmd.Parameters.Add("@Address2", SqlDbType.VarChar).Value = LectureAttendee.Address2;
                cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = LectureAttendee.City;
                cmd.Parameters.Add("@State", SqlDbType.Char).Value = LectureAttendee.State;
                cmd.Parameters.Add("@Zip", SqlDbType.VarChar).Value = LectureAttendee.Zip;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = LectureAttendee.Email;
                cmd.Parameters.Add("@DateAdded", SqlDbType.DateTime).Value = LectureAttendee.DateAdded == DateTime.MinValue?DateTime.Now:LectureAttendee.DateAdded;
                cmd.Parameters.Add("@Completed", SqlDbType.Bit).Value = LectureAttendee.Completed;
                cmd.Parameters.Add("@LectAttID", SqlDbType.Int).Value = LectureAttendee.LectAttID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        /// <summary>
        /// Retrieves all LectureAttendees for a Lecture Event
        /// </summary>
        public List<LectureAttendeeInfo> GetLectureAttendeesByLectEvtID(int LectEvtID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from LectureAttendee where LectEvtID = @LectEvtID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectEvtID;

                cn.Open();
                return GetLectureAttendeeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new LectureAttendeeInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual LectureAttendeeInfo GetLectureAttendeeFromReader(IDataReader reader)
        {
            return GetLectureAttendeeFromReader(reader, true);
        }
        protected virtual LectureAttendeeInfo GetLectureAttendeeFromReader(IDataReader reader, bool readMemos)
        {
            LectureAttendeeInfo LectureAttendee = new LectureAttendeeInfo(
              (int)reader["LectAttID"],
              (int)reader["LectEvtID"],
              (int)(Convert.IsDBNull(reader["UserID"]) ? (int)0 : (int)reader["UserID"]),
              reader["UserName"].ToString(),
              reader["PW"].ToString(),
              reader["FirstName"].ToString(),
              reader["LastName"].ToString(),
              reader["MI"].ToString(),
              reader["Address1"].ToString(),
              reader["Address2"].ToString(),
              reader["City"].ToString(),
              reader["State"].ToString(),
              reader["Zip"].ToString(),
              reader["Email"].ToString(),
              (DateTime)reader["DateAdded"],
              (bool)reader["Completed"]);


            return LectureAttendee;
        }

        /// <summary>
        /// Returns a collection of LectureAttendeeInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<LectureAttendeeInfo> GetLectureAttendeeCollectionFromReader(IDataReader reader)
        {
            return GetLectureAttendeeCollectionFromReader(reader, true);
        }
        protected virtual List<LectureAttendeeInfo> GetLectureAttendeeCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<LectureAttendeeInfo> LectureAttendees = new List<LectureAttendeeInfo>();
            while (reader.Read())
                LectureAttendees.Add(GetLectureAttendeeFromReader(reader, readMemos));
            return LectureAttendees;
        }
        #endregion
    }
}
#endregion