﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region UserLicenseInfo

namespace PearlsReview.DAL
{
    public class UserLicenseInfo
    {
        public UserLicenseInfo() { }

        public UserLicenseInfo(int LicenseID, int UserID, int License_Type_ID, string State,
            string License_Number, string Issue_Date, string Expire_Date, bool Report_To_State, 
            string comment1, string comment2)
        {
            this.LicenseID = LicenseID;
            this.UserID = UserID;
            this.License_Type_ID = License_Type_ID;
            this.State = State;
            this.License_Number = License_Number;
            this.Issue_Date = Issue_Date;
            this.Expire_Date = Expire_Date;
            this.Report_To_State = Report_To_State;
            this.Comment1 = comment1;
            this.Comment2 = comment2;
        }

        private int _LicenseID = 0;
        public int LicenseID
        {
            get { return _LicenseID; }
            protected set { _LicenseID = value; }
        }

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            private set { _UserID = value; }
        }

        private int _License_Type_ID = 0;
        public int License_Type_ID
        {
            get { return _License_Type_ID; }
            private set { _License_Type_ID = value; }
        }

        private string _State = "";
        public string State
        {
            get { return _State; }
            private set { _State = value; }
        }

        private string _License_Number = "";
        public string License_Number
        {
            get { return _License_Number; }
            private set { _License_Number = value; }
        }

        private string _Issue_Date = "";
        public string Issue_Date
        {
            get { return _Issue_Date; }
            private set { _Issue_Date = value; }
        }

        private string _Expire_Date = "";
        public string Expire_Date
        {
            get { return _Expire_Date; }
            private set { _Expire_Date = value; }
        }

        private bool _Report_To_State = false;
        public bool Report_To_State
        {
            get { return _Report_To_State; }
            private set { _Report_To_State = value; }
        }

        private string _comment1 = "";
        public string Comment1
        {
            get { return _comment1; }
            set { _comment1 = value; }
        }

        private string _comment2 = "";
        public string Comment2
        {
            get { return _comment2; }
            set { _comment2 = value; }
        }
    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider


        /////////////////////////////////////////////////////////
        // methods that work with UserLicenses

        /// <summary>
        /// Returns the total number of UserLicenses
        /// </summary>
        public int GetUserLicenseCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from UserLicense", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all UserLicenses
        /// </summary>
        public List<UserLicenseInfo> GetUserLicenses(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from UserLicense";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetUserLicenseCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all UserLicenses for a UserID
        /// </summary>
        public List<UserLicenseInfo> GetUserLicensesByUserID(int UserID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from UserLicense where UserID=@UserID ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

                cn.Open();
                return GetUserLicenseCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public DataSet GetUserLicensesDatasetByUserID(int UserID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select lt.description, lt.licensecode, ul.* from UserLicense ul " +
                    " join LicenseType lt on ul.license_type_id =lt.license_type_id " +
                    "where UserID=@UserID ";                   

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                
                SqlDataAdapter dap = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("UserLicense");
                dap.Fill(ds);
                return ds;
            }
        }

        /// <summary>
        /// Retrieves all UserLicenses for a UserID
        /// </summary>
        public List<UserLicenseInfo> GetTop3UserLicensesByUserID(int UserID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select Top 3 * " +
                    "from UserLicense where UserID=@UserID ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

                cn.Open();
                return GetUserLicenseCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves  UserLicense for a UserID
        /// </summary>
        public UserLicenseInfo GetUserLicenseByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from UserLicense where UserID=@UserID ";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

                cn.Open();


                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUserLicenseFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves  UserLicense Count for a UserID
        /// </summary>
        public int GetUserLicenseCountByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select Count(*) " +
                    "from UserLicense where UserID=@UserID ";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

                cn.Open();
                return (int)ExecuteScalar(cmd);

            }
        }

        /// <summary>
        /// Retrieves the UserLicense with the specified ID
        /// </summary>
        public UserLicenseInfo GetUserLicenseByID(int LicenseID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from UserLicense where LicenseID=@LicenseID", cn);
                cmd.Parameters.Add("@LicenseID", SqlDbType.Int).Value = LicenseID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUserLicenseFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a UserLicense
        /// </summary>
        public bool DeleteUserLicense(int LicenseID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from UserLicense where LicenseID=@LicenseID", cn);
                cmd.Parameters.Add("@LicenseID", SqlDbType.Int).Value = LicenseID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new UserLicense
        /// </summary>
        public int InsertUserLicense(UserLicenseInfo UserLicense)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into UserLicense " +
              "(UserID, " +
              "License_Type_ID, " +
              "State, " +
              "License_Number, " +
              "Issue_Date, " +
              "Expire_Date, " +
              "Report_To_State, " +
              "comment1, " +
              "comment2" +
              ") " +
              "VALUES (" +
              "@UserID, " +
              "@License_Type_ID, " +
              "@State, " +
              "@License_Number, " +
              "@Issue_Date, " +
              "@Expire_Date, " +
              "@Report_To_State, " +
              "@comment1, " +
              "@comment2  ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserLicense.UserID;
                cmd.Parameters.Add("@License_Type_ID", SqlDbType.Int).Value = UserLicense.License_Type_ID;
                cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = UserLicense.State;
                cmd.Parameters.Add("@License_Number", SqlDbType.VarChar).Value = UserLicense.License_Number;
                cmd.Parameters.Add("@comment1", SqlDbType.VarChar).Value = UserLicense.Comment1;
                cmd.Parameters.Add("@comment2", SqlDbType.VarChar).Value = UserLicense.Comment2;

                if (UserLicense.Issue_Date == null
                    || UserLicense.Issue_Date.Trim().Length == 0)
                {
                    cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
                    try
                    {
                        DateTime dIssue_Date = Convert.ToDateTime(UserLicense.Issue_Date);
                        cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = dIssue_Date;
                    }
                    catch
                    {
                        // something went wrong with convert, so store NULL instead
                        cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                }

                if (UserLicense.Expire_Date == null
                    || UserLicense.Expire_Date.Trim().Length == 0)
                {
                    cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
                    try
                    {
                        DateTime dExpire_Date = Convert.ToDateTime(UserLicense.Expire_Date);
                        cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = dExpire_Date;
                    }
                    catch
                    {
                        // something went wrong with convert, so store NULL instead
                        cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                }

                cmd.Parameters.Add("@Report_To_State", SqlDbType.Bit).Value = UserLicense.Report_To_State;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a UserLicense
        /// </summary>
        public bool UpdateUserLicense(UserLicenseInfo UserLicense)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserLicense set " +
              "UserID = @UserID, " +
              "License_Type_ID = @License_Type_ID, " +
              "State = @State, " +
              "License_Number = @License_Number, " +
              "Issue_Date = @Issue_Date, " +
              "Expire_Date = @Expire_Date, " +
              "Report_To_State = @Report_To_State, " +
              "comment1 = @comment1, " +
              "comment2 = @comment2 " +
              "where LicenseID = @LicenseID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserLicense.UserID;
                cmd.Parameters.Add("@License_Type_ID", SqlDbType.Int).Value = UserLicense.License_Type_ID;
                cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = UserLicense.State;
                cmd.Parameters.Add("@License_Number", SqlDbType.VarChar).Value = UserLicense.License_Number;
                cmd.Parameters.Add("@comment1", SqlDbType.VarChar).Value = UserLicense.Comment1;
                cmd.Parameters.Add("@comment2", SqlDbType.VarChar).Value = UserLicense.Comment2;

                if (UserLicense.Issue_Date == null
                    || UserLicense.Issue_Date.Trim().Length == 0)
                {
                    cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
                    try
                    {
                        DateTime dIssue_Date = Convert.ToDateTime(UserLicense.Issue_Date);
                        cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = dIssue_Date;
                    }
                    catch
                    {
                        // something went wrong with convert, so store NULL instead
                        cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                }

                if (UserLicense.Expire_Date == null
                    || UserLicense.Expire_Date.Trim().Length == 0)
                {
                    cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
                    try
                    {
                        DateTime dExpire_Date = Convert.ToDateTime(UserLicense.Expire_Date);
                        cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = dExpire_Date;
                    }
                    catch
                    {
                        // something went wrong with convert, so store NULL instead
                        cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                }

                cmd.Parameters.Add("@Report_To_State", SqlDbType.Bit).Value = UserLicense.Report_To_State;

                cmd.Parameters.Add("@LicenseID", SqlDbType.Int).Value = UserLicense.LicenseID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        public bool UpdateUserLicenseUseridByNewId(int newId, int OldId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserLicense set " +
              "UserId = @newId " +
              "where UserId = @OldId ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@newId", SqlDbType.Int).Value = newId;
                cmd.Parameters.Add("@OldId", SqlDbType.Int).Value = OldId;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public string GetUserLicenseTypeHours(int testID)
        {
            System.Text.StringBuilder typeHours = new System.Text.StringBuilder();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_get_LicenseTypeHours");
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = testID;                

                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    typeHours.Append(reader["LicenseCode"].ToString() + " ");
                    typeHours.Append(reader["Th_Hours"].ToString() + " ");

                    if (!string.IsNullOrEmpty(reader["Th_Provider"].ToString()))
                    {
                        typeHours.Append(reader["UnitName"].ToString() + ", ");
                        typeHours.Append(" Provider# ");
                        typeHours.Append(reader["Th_Provider"].ToString());
                    }
                    else
                        typeHours.Append(reader["UnitName"].ToString());
                    typeHours.Append("<br />");
                }
                return typeHours.ToString();
            }
        }        
        /// <summary>
        /// Retrieves all licenses by licensenumber
        /// </summary>
        public List<UserLicenseInfo> GetUserLicenseByLicenseNumber(string LicenseNumber)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from UserLicense a join Useraccount b on a.userid=b.iid "+
                     " where license_number=@LicenseNumber and (b.facilityid=5 or b.facilityid=2)";            

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@LicenseNumber", SqlDbType.VarChar).Value = LicenseNumber;

                cn.Open();
                return GetUserLicenseCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        #endregion

        #region PRProvider

        /////////////////////////////////////////////////////////
        // methods that work with UserLicenses
       
        /// <summary>
        /// Returns a new UserLicenseInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual UserLicenseInfo GetUserLicenseFromReader(IDataReader reader)
        {
            return GetUserLicenseFromReader(reader, true);
        }
        protected virtual UserLicenseInfo GetUserLicenseFromReader(IDataReader reader, bool readMemos)
        {
            UserLicenseInfo UserLicense = new UserLicenseInfo(
              (int)reader["LicenseID"],
              (int)reader["UserID"],
              (int)reader["License_Type_ID"],
              reader["State"].ToString(),
              reader["License_Number"].ToString(),
              (Convert.IsDBNull(reader["Issue_Date"])
                  ? "" : ((DateTime)reader["Issue_Date"]).ToShortDateString()),
              (Convert.IsDBNull(reader["Expire_Date"])
                  ? "" : ((DateTime)reader["Expire_Date"]).ToShortDateString()),
              (Convert.IsDBNull(reader["Report_To_State"]) ? false : (bool)reader["Report_To_State"]), 
              (Convert.IsDBNull(reader["comment1"]) ? "" : reader["comment1"].ToString()), 
              (Convert.IsDBNull(reader["comment2"]) ? "" : reader["comment2"].ToString()));

            return UserLicense;
        }

        /// <summary>
        /// Returns a collection of UserLicenseInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<UserLicenseInfo> GetUserLicenseCollectionFromReader(IDataReader reader)
        {
            return GetUserLicenseCollectionFromReader(reader, true);
        }
        protected virtual List<UserLicenseInfo> GetUserLicenseCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<UserLicenseInfo> UserLicenses = new List<UserLicenseInfo>();
            while (reader.Read())
                UserLicenses.Add(GetUserLicenseFromReader(reader, readMemos));
            return UserLicenses;
        }


        #endregion
    }
}
#endregion
