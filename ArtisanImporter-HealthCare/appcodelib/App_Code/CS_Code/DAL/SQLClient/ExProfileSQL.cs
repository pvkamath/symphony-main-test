﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;



namespace PearlsReview.DAL
{
    #region ExProfileInfo

    public class ExProfileInfo
    {
        public ExProfileInfo() { }

        public ExProfileInfo(int epID, int userID, string profileType, string profileID, DateTime? addDate, bool curStatus, DateTime? disableDate)
        {
            this.EpID = epID;
            this.UserID = userID;
            this.ProfileType = profileType;
            this.ProfileID = profileID;
            this.AddDate = addDate;
            this.CurStatus = curStatus;
            this.DisableDate = disableDate;
        }

        public int EpID { get; set; }
        public int UserID { get; set; }
        public string ProfileType { get; set; }
        public string ProfileID { get; set; }
        public DateTime? AddDate { get; set; }
        public bool CurStatus { get; set; }
        public DateTime? DisableDate { get; set; }
    }

    #endregion
}
    #region SQLPRProvider and PRProvider

    namespace PearlsReview.DAL.SQLClient
    {
        public partial class SQL2PRProvider : DataAccess
        {
            #region SQLPRProvider

            /// <summary>
            /// Retrieves all Categories
            /// </summary>
            public List<ExProfileInfo> GetExProfile(string cSortExpression)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    string cSQLCommand = "select * from ExProfile";

                    // add on ORDER BY if provided
                    if (cSortExpression.Length > 0)
                    {
                        cSQLCommand = cSQLCommand +
                            " order by " + cSortExpression;
                    }

                    SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                    cn.Open();
                    return GetExProfileCollectionFromReader(ExecuteReader(cmd), false);
                }
            }

            /// <summary>
            /// Updates a ExProfile
            /// </summary>
            public bool UpdateExProfile(ExProfileInfo ExProfile)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("update ExProfile set " +
                  "userid = @userid, " +
                  "profiletype = @profiletype, " +
                  "profileid = @profileid, " +
                  "adddate = @adddate, " +
                  "curstatus = @curstatus, " +
                  "disabledate = @disabledate where profileid=@profileid ", cn);

                    cmd.Parameters.Add("@userid", SqlDbType.Int).Value = ExProfile.UserID;
                    cmd.Parameters.Add("@profiletype", SqlDbType.VarChar).Value = ExProfile.ProfileType;
                    cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = ExProfile.ProfileID;
                    cmd.Parameters.Add("@adddate", SqlDbType.DateTime).Value = ExProfile.AddDate;
                    cmd.Parameters.Add("@curstatus", SqlDbType.Int).Value = ExProfile.CurStatus;
                    cmd.Parameters.Add("@disabledate", SqlDbType.DateTime).Value = ExProfile.DisableDate;

                    cn.Open();
                    int ret = ExecuteNonQuery(cmd);
                    return (ret == 1);
                }
            }
            #endregion

            #region PRProvider

            protected virtual ExProfileInfo GetExProflieFromReader(IDataReader reader)
            {
                return GetExProflieFromReader(reader, true);
            }
            protected virtual ExProfileInfo GetExProflieFromReader(IDataReader reader, bool readMemos)
            {
                ExProfileInfo ExProfile = new ExProfileInfo(
                  (int)reader["epid"],
                  (int)reader["userid"],
                  reader["profiletype"].ToString(),
                  reader["profileid"].ToString(),
                  (String.IsNullOrEmpty(reader["adddate"].ToString())) ? null : (DateTime?)reader["adddate"],
                  (bool)reader["curstatus"],
                  (String.IsNullOrEmpty(reader["disabledate"].ToString())) ? null : (DateTime?)reader["disabledate"]);

                return ExProfile;
            }

            /// <summary>
            /// Returns a collection of ExProfileInfo objects with the data read from the input DataReader
            /// </summary>
            protected virtual List<ExProfileInfo> GetExProfileCollectionFromReader(IDataReader reader)
            {
                return GetExProfileCollectionFromReader(reader, true);
            }
            protected virtual List<ExProfileInfo> GetExProfileCollectionFromReader(IDataReader reader, bool readMemos)
            {
                List<ExProfileInfo> ExProfiles = new List<ExProfileInfo>();
                while (reader.Read())
                    ExProfiles.Add(GetExProflieFromReader(reader, readMemos));
                return ExProfiles;
            }

            #endregion
        }
    }

    #endregion