﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Alex Chen
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for TopicCMEAuditSQL
/// </summary>

#region TopicCMEAuditInfo
namespace PearlsReview.DAL
{
    public class TopicCMEAuditInfo
    {
        private int _topicID = 0;
        public int TopicID
        {
            get { return _topicID; }
            set { _topicID = value; }
        }

        private bool _cmecdEvidenceInd = false;
        public bool CMEcdEvidenceInd
        {
            get { return _cmecdEvidenceInd; }
            set { _cmecdEvidenceInd = value; }
        }

        private bool _cmecdPlannerInd = false;
        public bool CMEcdPlannerInd
        {
            get { return _cmecdPlannerInd; }
            set { _cmecdPlannerInd = value; }
        }

        private bool _cmecdActivityInd = false;
        public bool CMEcdActivityInd
        {
            get { return _cmecdActivityInd; }
            set { _cmecdActivityInd = value; }
        }

        private bool _cmecdRecommendationInd = false;
        public bool CMEcdRecommendationInd
        {
            get { return _cmecdRecommendationInd; }
            set { _cmecdRecommendationInd = value; }
        }

        private bool _cmecdResearchInd = false;
        public bool CMEcdResearchInd
        {
            get { return _cmecdResearchInd; }
            set { _cmecdResearchInd = value; }
        }

        private string _cmecdTargetAudience = "";
        public string CMEcdTargetAudience
        {
            get { return _cmecdTargetAudience; }
            set { _cmecdTargetAudience = value; }
        }

        private string _cmecdContent = "";
        public string CMEcdContent
        {
            get { return _cmecdContent; }
            set { _cmecdContent = value; }
        }

        private string _cmecdPeer1 = "";
        public string CMEcdPeer1
        {
            get { return _cmecdPeer1; }
            set { _cmecdPeer1 = value; }
        }

        private string _cmecdPeer2 = "";
        public string CMEcdPeer2
        {
            get { return _cmecdPeer2; }
            set { _cmecdPeer2 = value; }
        }

        private string _cmecdPeer3 = "";
        public string CMEcdPeer3
        {
            get { return _cmecdPeer3; }
            set { _cmecdPeer3 = value; }
        }

        private bool _cmeprClinicalInd = false;
        public bool CMEprClinicalInd
        {
            get { return _cmeprClinicalInd; }
            set { _cmeprClinicalInd = value; }
        }

        private bool _cmeprQualityInd = false;
        public bool CMEprQualityInd
        {
            get { return _cmeprQualityInd; }
            set { _cmeprQualityInd = value; }
        }

        private bool _cmeprRequirementInd = false;
        public bool CMEprRequirementInd
        {
            get { return _cmeprRequirementInd; }
            set { _cmeprRequirementInd = value; }
        }

        private bool _cmeprEvaluationInd = false;
        public bool CMEprEvaluationInd
        {
            get { return _cmeprEvaluationInd; }
            set { _cmeprEvaluationInd = value; }
        }

        private bool _cmeprOutcomeInd = false;
        public bool CMEprOutcomeInd
        {
            get { return _cmeprOutcomeInd; }
            set { _cmeprOutcomeInd = value; }
        }

        private bool _cmeprSearchInd = false;
        public bool CMEprSearchInd
        {
            get { return _cmeprSearchInd; }
            set { _cmeprSearchInd = value; }
        }

        private bool _cmeprCaseInd = false;
        public bool CMEprCaseInd
        {
            get { return _cmeprCaseInd; }
            set { _cmeprCaseInd = value; }
        }

        private bool _cmeprOtherInd = false;
        public bool CMEprOtherInd
        {
            get { return _cmeprOtherInd; }
            set { _cmeprOtherInd = value; }
        }

        private string _cmeprDescribe = "";
        public string CMEprDescribe
        {
            get { return _cmeprDescribe; }
            set { _cmeprDescribe = value; }
        }

        private string _cmeusdLearner = "";
        public string CMEusdLearner
        {
            get { return _cmeusdLearner; }
            set { _cmeusdLearner = value; }
        }

        private string _cmeusdProcedure = "";
        public string CMEusdProcedure
        {
            get { return _cmeusdProcedure; }
            set { _cmeusdProcedure = value; }
        }

        private string _cmeusdResult = "";
        public string CMEusdResult
        {
            get { return _cmeusdResult; }
            set { _cmeusdResult = value; }
        }

        private string _cmeusdCause = "";
        public string CMEusdCause
        {
            get { return _cmeusdCause; }
            set { _cmeusdCause = value; }
        }

        private string _cmeusdExplain = "";
        public string CMEusdExplain
        {
            get { return _cmeusdExplain; }
            set { _cmeusdExplain = value; }
        }

        private bool _cmecpPatientInd = false;
        public bool CMEcpPatientInd
        {
            get { return _cmecpPatientInd; }
            set { _cmecpPatientInd = value; }
        }

        private string _cmecpPatient = "";
        public string CMEcpPatient
        {
            get { return _cmecpPatient; }
            set { _cmecpPatient = value; }
        }

        private bool _cmecpKnowledgeInd = false;
        public bool CMEcpKnowledgeInd
        {
            get { return _cmecpKnowledgeInd; }
            set { _cmecpKnowledgeInd = value; }
        }

        private string _cmecpKnowledge = "";
        public string CMEcpKnowledge
        {
            get { return _cmecpKnowledge; }
            set { _cmecpKnowledge = value; }
        }

        private bool _cmecpPracticeInd = false;
        public bool CMEcpPracticeInd
        {
            get { return _cmecpPracticeInd; }
            set { _cmecpPracticeInd = value; }
        }

        private bool _cmecpSystemPracticeInd = false;
        public bool CMEcpSystemPracticeInd
        {
            get { return _cmecpSystemPracticeInd; }
            set { _cmecpSystemPracticeInd = value; }
        }

        private string _cmecpPractice = "";
        public string CMEcpPractice
        {
            get { return _cmecpPractice; }
            set { _cmecpPractice = value; }
        }

        private bool _cmecpProfessionInd = false;
        public bool CMEcpProfessionInd
        {
            get { return _cmecpProfessionInd; }
            set { _cmecpProfessionInd = value; }
        }

        private string _cmecpProfession = "";
        public string CMEcpProfession
        {
            get { return _cmecpProfession; }
            set { _cmecpProfession = value; }
        }

        private bool _cmecpSkillInd = false;
        public bool CMEcpSkillInd
        {
            get { return _cmecpSkillInd; }
            set { _cmecpSkillInd = value; }
        }

        private string _cmecpSkill = "";
        public string CMEcpSkill
        {
            get { return _cmecpSkill; }
            set { _cmecpSkill = value; }
        }

        private bool _cmecpCareInd = false;
        public bool CMEcpCareInd
        {
            get { return _cmecpCareInd; }
            set { _cmecpCareInd = value; }
        }

        private string _cmecpCare = "";
        public string CMEcpCare
        {
            get { return _cmecpCare; }
            set { _cmecpCare = value; }
        }

        private bool _cmecpTeamInd = false;
        public bool CMEcpTeamInd
        {
            get { return _cmecpTeamInd; }
            set { _cmecpTeamInd = value; }
        }

        private string _cmecpTeam = "";
        public string CMEcpTeam
        {
            get { return _cmecpTeam; }
            set { _cmecpTeam = value; }
        }

        private bool _cmecpEvidenceInd = false;
        public bool CMEcpEvidenceInd
        {
            get { return _cmecpEvidenceInd; }
            set { _cmecpEvidenceInd = value; }
        }

        private string _cmecpEvidence = "";
        public string CMEcpEvidence
        {
            get { return _cmecpEvidence; }
            set { _cmecpEvidence = value; }
        }

        private bool _cmecpQualityInd = false;
        public bool CMEcpQualityInd
        {
            get { return _cmecpQualityInd; }
            set { _cmecpQualityInd = value; }
        }

        private string _cmecpQuality = "";
        public string CMEcpQuality
        {
            get { return _cmecpQuality; }
            set { _cmecpQuality = value; }
        }

        private bool _cmecpUtilizeInd = false;
        public bool CMEcpUtilizeInd
        {
            get { return _cmecpUtilizeInd; }
            set { _cmecpUtilizeInd = value; }
        }

        private string _cmecpUtilize = "";
        public string CMEcpUtilize
        {
            get { return _cmecpUtilize; }
            set { _cmecpUtilize = value; }
        }

        private string _cmecpContent = "";
        public string CMEcpContent
        {
            get { return _cmecpContent; }
            set { _cmecpContent = value; }
        }

        private string _cmecpBarrier = "";
        public string CMEcpBarrier
        {
            get { return _cmecpBarrier; }
            set { _cmecpBarrier = value; }
        }

        private string _cmecpStrategy = "";
        public string CMEcpStrategy
        {
            get { return _cmecpStrategy; }
            set { _cmecpStrategy = value; }
        }

        private string _cmecpNonStrategy = "";
        public string CMEcpNonStrategy
        {
            get { return _cmecpNonStrategy; }
            set { _cmecpNonStrategy = value; }
        }

        private string _cmecpPurpose = "";
        public string CMEcpPurpose
        {
            get { return _cmecpPurpose; }
            set { _cmecpPurpose = value; }
        }

        private string _cmecpFactors = "";
        public string CMEcpFactors
        {
            get { return _cmecpFactors; }
            set { _cmecpFactors = value; }
        }

        private string _cmepgPg1 = "";
        public string CMEpgPg1
        {
            get { return _cmepgPg1; }
            set { _cmepgPg1 = value; }
        }

        private string _cmepgPg2 = "";
        public string CMEpgPg2
        {
            get { return _cmepgPg2; }
            set { _cmepgPg2 = value; }
        }

        private string _cmepgPg3 = "";
        public string CMEpgPg3
        {
            get { return _cmepgPg3; }
            set { _cmepgPg3 = value; }
        }

        public TopicCMEAuditInfo(int topicID, bool cmecdEvidenceInd, bool cmecdPlannerInd, bool cmecdActivityInd,
            bool cmecdRecommendationInd, bool cmecdResearchInd, string cmecdTargetAudience, string cmecdContent,
            string cmecdPeer1, string cmecdPeer2, string cmecdPeer3, bool cmeprClinicalInd, bool cmeprQualityInd,
            bool cmeprRequirementInd, bool cmeprEvaluationInd, bool cmeprOutcomeInd, bool cmeprSearchInd,
            bool cmeprCaseInd, bool cmeprOtherInd, string cmeprDescribe, string cmeusdLearner, string cmeusdProcedure,
            string cmeusdResult, string cmeusdCause, string cmeusdExplain, bool cmecpPatientInd, string cmecpPatient,
            bool cmecpKnowledgeInd, string cmecpKnowledge, bool cmecpPracticeInd, bool cmecpSystemPracticeInd,
            string cmecpPractice, bool cmecpProfessionInd, string cmecpProfession, bool cmecpSkillInd, string cmecpSkill,
            bool cmecpCareInd, string cmecpCare, bool cmecpTeamInd, string cmecpTeam, bool cmecpEvidenceInd,
            string cmecpEvidence, bool cmecpQualityInd, string cmecpQuality, bool cmecpUtilizeInd, string cmecpUtilize,
            string cmecpContent, string cmecpBarrier, string cmecpStrategy, string cmecpNonStrategy,
            string cmecpPurpose, string cmecpFactors, string cmepgPg1, string cmepgPg2, string cmepgPg3
            )
        {
            this.TopicID = topicID;
            this.CMEcdEvidenceInd = cmecdEvidenceInd;
            this.CMEcdPlannerInd = cmecdPlannerInd;
            this.CMEcdActivityInd = cmecdActivityInd;
            this.CMEcdRecommendationInd = cmecdRecommendationInd;
            this.CMEcdResearchInd = cmecdResearchInd;
            this.CMEcdTargetAudience = cmecdTargetAudience;
            this.CMEcdContent = cmecdContent;
            this.CMEcdPeer1 = cmecdPeer1;
            this.CMEcdPeer2 = cmecdPeer2;
            this.CMEcdPeer3 = cmecdPeer3;
            this.CMEprClinicalInd = cmeprClinicalInd;
            this.CMEprQualityInd = cmeprQualityInd;
            this.CMEprRequirementInd = cmeprRequirementInd;
            this.CMEprEvaluationInd = cmeprEvaluationInd;
            this.CMEprOutcomeInd = cmeprOutcomeInd;
            this.CMEprSearchInd = cmeprSearchInd;
            this.CMEprCaseInd = cmeprCaseInd;
            this.CMEprOtherInd = cmeprOtherInd;
            this.CMEprDescribe = cmeprDescribe;
            this.CMEusdLearner = cmeusdLearner;
            this.CMEusdProcedure = cmeusdProcedure;
            this.CMEusdResult = cmeusdResult;
            this.CMEusdCause = cmeusdCause;
            this.CMEusdExplain = cmeusdExplain;
            this.CMEcpPatientInd = cmecpPatientInd;
            this.CMEcpPatient = cmecpPatient;
            this.CMEcpKnowledgeInd = cmecpKnowledgeInd;
            this.CMEcpKnowledge = cmecpKnowledge;
            this.CMEcpPracticeInd = cmecpPracticeInd;
            this.CMEcpSystemPracticeInd = cmecpSystemPracticeInd;
            this.CMEcpPractice = cmecpPractice;
            this.CMEcpProfessionInd = cmecpPracticeInd;
            this.CMEcpProfession = cmecpProfession;
            this.CMEcpSkillInd = cmecpSkillInd;
            this.CMEcpSkill = cmecpSkill;
            this.CMEcpCareInd = cmecpCareInd;
            this.CMEcpCare = cmecpCare;
            this.CMEcpTeamInd = cmecpTeamInd;
            this.CMEcpTeam = cmecpTeam;
            this.CMEcpEvidenceInd = cmecpEvidenceInd;
            this.CMEcpEvidence = cmecpEvidence;
            this.CMEcpQualityInd = cmecpQualityInd;
            this.CMEcpQuality = cmecpQuality;
            this.CMEcpUtilizeInd = cmecpUtilizeInd;
            this.CMEcpUtilize = cmecpUtilize;
            this.CMEcpContent = cmecpContent;
            this.CMEcpBarrier = cmecpBarrier;
            this.CMEcpStrategy = cmecpStrategy;
            this.CMEcpNonStrategy = cmecpNonStrategy;
            this.CMEcpPurpose = cmecpPurpose;
            this.CMEcpFactors = cmecpFactors;
            this.CMEpgPg1 = cmepgPg1;
            this.CMEpgPg2 = cmepgPg2;
            this.CMEpgPg3 = cmepgPg3;
        }
    }
}
#endregion TopicCMEAuditInfo

#region SQLPRProvider and PRProvider
namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <returns>All TopicCME Audits</returns>
        public List<TopicCMEAuditInfo> GetTopicCMEAudits(string sortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string sql = "select * from TopicCMEAudit ";
                if (!String.IsNullOrEmpty(sortExpression))
                {
                    sql += " order by " + sortExpression;
                }

                SqlCommand cmd = new SqlCommand(sql, cn);
                cn.Open();
                return GetTopicCMEAuditCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="topicID"></param>
        /// <returns>TopicCMEAudit</returns>
        public TopicCMEAuditInfo GetTopicCMEAuditByID(int topicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from TopicCMEAudit where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = topicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicCMEAuditFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="topicID"></param>
        /// <returns></returns>
        public bool DeleteTopicCMEAudit(int topicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from TopicCMEAudit where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = topicID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public int InsertTopicCMEAudit(TopicCMEAuditInfo topicCMEAudit)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into TopicCMEAudit " + 
                    "(topicid, " + 
                    "cme_cd_evidence_ind, " + 
                    "cme_cd_planner_ind, " + 
                    "cme_cd_activity_ind, " + 
                    "cme_cd_recommendation_ind, " +
                    "cme_cd_research_ind, " + 
                    "cme_cd_targetaudience, " +
                    "cme_cd_content, " +
                    "cme_cd_peer1, " + 
                    "cme_cd_peer2, " + 
                    "cme_cd_peer3, " + 
                    "cme_pr_clinical_ind, " + 
                    "cme_pr_quality_ind, " + 
                    "cme_pr_requirement_ind, " + 
                    "cme_pr_evaluation_ind, " + 
                    "cme_pr_outcome_ind, " + 
                    "cme_pr_search_ind, " + 
                    "cme_pr_case_ind, " + 
                    "cme_pr_other_ind, " + 
                    "cme_pr_describe, " + 
                    "cme_usd_learner, " + 
                    "cme_usd_procedure, " + 
                    "cme_usd_result, " + 
                    "cme_usd_cause, " + 
                    "cme_usd_explain, " + 
                    "cme_cp_patient_ind, " +
                    "cme_cp_patient, " + 
                    "cme_cp_knowledge_ind, " + 
                    "cme_cp_knowledge, " + 
                    "cme_cp_practice_ind, " +
                    "cme_cp_systempractice_ind, " +
                    "cme_cp_practice, " +
                    "cme_cp_profession_ind, " +
                    "cme_cp_profession, " + 
                    "cme_cp_skill_ind, " +
                    "cme_cp_skill, " +
                    "cme_cp_care_ind, " +
                    "cme_cp_care, " +
                    "cme_cp_team_ind, " +
                    "cme_cp_team, " +
                    "cme_cp_evidence_ind, " +
                    "cme_cp_evidence, " +
                    "cme_cp_quality_ind, " +
                    "cme_cp_quality, " +
                    "cme_cp_utilize_ind, " +
                    "cme_cp_utilize, " +
                    "cme_cp_content, " +
                    "cme_cp_barrier, " +
                    "cme_cp_strategy, " +
                    "cme_cp_nonstrategy, " +
                    "cme_cp_purpose, " +
                    "cme_cp_factors, " +
                    "cme_pg_pg1, " +
                    "cme_pg_pg2, " +
                    "cme_pg_pg3) " + 
                    "VALUES (@topicid, " + 
                    "@cme_cd_evidence_ind, " + 
                    "@cme_cd_planner_ind, " + 
                    "@cme_cd_activity_ind, " + 
                    "@cme_cd_recommendation_ind, " +
                    "@cme_cd_research_ind, " + 
                    "@cme_cd_targetaudience, " +
                    "@cme_cd_content, " +
                    "@cme_cd_peer1, " + 
                    "@cme_cd_peer2, " + 
                    "@cme_cd_peer3, " + 
                    "@cme_pr_clinical_ind, " + 
                    "@cme_pr_quality_ind, " + 
                    "@cme_pr_requirement_ind, " + 
                    "@cme_pr_evaluation_ind, " + 
                    "@cme_pr_outcome_ind, " + 
                    "@cme_pr_search_ind, " + 
                    "@cme_pr_case_ind, " + 
                    "@cme_pr_other_ind, " + 
                    "@cme_pr_describe, " + 
                    "@cme_usd_learner, " + 
                    "@cme_usd_procedure, " + 
                    "@cme_usd_result, " + 
                    "@cme_usd_cause, " + 
                    "@cme_usd_explain, " + 
                    "@cme_cp_patient_ind, " +
                    "@cme_cp_patient, " + 
                    "@cme_cp_knowledge_ind, " + 
                    "@cme_cp_knowledge, " + 
                    "@cme_cp_practice_ind, " +
                    "@cme_cp_systempractice_ind, " +
                    "@cme_cp_practice, " +
                    "@cme_cp_profession_ind, " +
                    "@cme_cp_profession, " + 
                    "@cme_cp_skill_ind, " +
                    "@cme_cp_skill, " +
                    "@cme_cp_care_ind, " +
                    "@cme_cp_care, " +
                    "@cme_cp_team_ind, " +
                    "@cme_cp_team, " +
                    "@cme_cp_evidence_ind, " +
                    "@cme_cp_evidence, " +
                    "@cme_cp_quality_ind, " +
                    "@cme_cp_quality, " +
                    "@cme_cp_utilize_ind, " +
                    "@cme_cp_utilize, " +
                    "@cme_cp_content, " +
                    "@cme_cp_barrier, " +
                    "@cme_cp_strategy, " +
                    "@cme_cp_nonstrategy, " +
                    "@cme_cp_purpose, " +
                    "@cme_cp_factors, " +
                    "@cme_pg_pg1, " +
                    "@cme_pg_pg2, " +
                    "@cme_pg_pg3) ", cn);

                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicCMEAudit.TopicID;
                cmd.Parameters.Add("@cme_cd_evidence_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcdEvidenceInd;
                cmd.Parameters.Add("@cme_cd_planner_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcdPlannerInd;
                cmd.Parameters.Add("@cme_cd_activity_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcdActivityInd;
                cmd.Parameters.Add("@cme_cd_recommendation_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcdRecommendationInd;
                cmd.Parameters.Add("@cme_cd_research_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcdResearchInd;
                cmd.Parameters.Add("@cme_cd_targetaudience", SqlDbType.VarChar).Value = topicCMEAudit.CMEcdTargetAudience;
                cmd.Parameters.Add("@cme_cd_content", SqlDbType.VarChar).Value = topicCMEAudit.CMEcdContent;
                cmd.Parameters.Add("@cme_cd_peer1", SqlDbType.VarChar).Value = topicCMEAudit.CMEcdPeer1;
                cmd.Parameters.Add("@cme_cd_peer2", SqlDbType.VarChar).Value = topicCMEAudit.CMEcdPeer2;
                cmd.Parameters.Add("@cme_cd_peer3", SqlDbType.VarChar).Value = topicCMEAudit.CMEcdPeer3;
                cmd.Parameters.Add("@cme_pr_clinical_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprClinicalInd;
                cmd.Parameters.Add("@cme_pr_quality_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprQualityInd;
                cmd.Parameters.Add("@cme_pr_requirement_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprRequirementInd;
                cmd.Parameters.Add("@cme_pr_evaluation_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprEvaluationInd;
                cmd.Parameters.Add("@cme_pr_outcome_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprOutcomeInd;
                cmd.Parameters.Add("@cme_pr_search_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprSearchInd;
                cmd.Parameters.Add("@cme_pr_case_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprCaseInd;
                cmd.Parameters.Add("@cme_pr_other_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprOtherInd;
                cmd.Parameters.Add("@cme_pr_describe", SqlDbType.VarChar).Value = topicCMEAudit.CMEprDescribe;
                cmd.Parameters.Add("@cme_usd_learner", SqlDbType.VarChar).Value = topicCMEAudit.CMEusdLearner;
                cmd.Parameters.Add("@cme_usd_procedure", SqlDbType.VarChar).Value = topicCMEAudit.CMEusdProcedure;
                cmd.Parameters.Add("@cme_usd_result", SqlDbType.VarChar).Value = topicCMEAudit.CMEusdResult;
                cmd.Parameters.Add("@cme_usd_cause", SqlDbType.VarChar).Value = topicCMEAudit.CMEusdCause;
                cmd.Parameters.Add("@cme_usd_explain", SqlDbType.VarChar).Value = topicCMEAudit.CMEusdExplain;
                cmd.Parameters.Add("@cme_cp_patient_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpPatientInd;
                cmd.Parameters.Add("@cme_cp_patient", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpPatient;
                cmd.Parameters.Add("@cme_cp_knowledge_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpKnowledgeInd;
                cmd.Parameters.Add("@cme_cp_knowledge", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpKnowledge;
                cmd.Parameters.Add("@cme_cp_practice_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpPracticeInd;
                cmd.Parameters.Add("@cme_cp_systempractice_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpSystemPracticeInd;
                cmd.Parameters.Add("@cme_cp_practice", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpPractice;
                cmd.Parameters.Add("@cme_cp_profession_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpProfessionInd;
                cmd.Parameters.Add("@cme_cp_profession", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpProfession;
                cmd.Parameters.Add("@cme_cp_skill_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpSkillInd;
                cmd.Parameters.Add("@cme_cp_skill", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpSkill;
                cmd.Parameters.Add("@cme_cp_care_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpCareInd;
                cmd.Parameters.Add("@cme_cp_care", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpCare;
                cmd.Parameters.Add("@cme_cp_team_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpTeamInd;
                cmd.Parameters.Add("@cme_cp_team", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpTeam;
                cmd.Parameters.Add("@cme_cp_evidence_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpEvidenceInd;
                cmd.Parameters.Add("@cme_cp_evidence", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpEvidence;
                cmd.Parameters.Add("@cme_cp_quality_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpQualityInd;
                cmd.Parameters.Add("@cme_cp_quality", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpQuality;
                cmd.Parameters.Add("@cme_cp_utilize_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpUtilizeInd;
                cmd.Parameters.Add("@cme_cp_utilize", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpUtilize;
                cmd.Parameters.Add("@cme_cp_content", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpContent;
                cmd.Parameters.Add("@cme_cp_barrier", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpBarrier;
                cmd.Parameters.Add("@cme_cp_strategy", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpStrategy;
                cmd.Parameters.Add("@cme_cp_nonstrategy", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpNonStrategy;
                cmd.Parameters.Add("@cme_cp_purpose", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpPurpose;
                cmd.Parameters.Add("@cme_cp_factors", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpFactors;
                cmd.Parameters.Add("@cme_pg_pg1", SqlDbType.VarChar).Value = topicCMEAudit.CMEpgPg1;
                cmd.Parameters.Add("@cme_pg_pg2", SqlDbType.VarChar).Value = topicCMEAudit.CMEpgPg2;
                cmd.Parameters.Add("@cme_pg_pg3", SqlDbType.VarChar).Value = topicCMEAudit.CMEpgPg3;

                cn.Open();
                cmd.ExecuteNonQuery();
                return topicCMEAudit.TopicID;
            }
        }

        public bool UpdateTopicCMEAudit(TopicCMEAuditInfo topicCMEAudit)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("UPDATE TopicCMEAudit SET " + 
                    "cme_cd_evidence_ind = @cme_cd_evidence_ind, " + 
                    "cme_cd_planner_ind = @cme_cd_planner_ind, " + 
                    "cme_cd_activity_ind = @cme_cd_activity_ind, " + 
                    "cme_cd_recommendation_ind = @cme_cd_recommendation_ind, " +
                    "cme_cd_research_ind = @cme_cd_research_ind, " + 
                    "cme_cd_targetaudience = @cme_cd_targetaudience, " +
                    "cme_cd_content = @cme_cd_content, " +
                    "cme_cd_peer1 = @cme_cd_peer1, " + 
                    "cme_cd_peer2 = @cme_cd_peer2, " + 
                    "cme_cd_peer3 = @cme_cd_peer3, " + 
                    "cme_pr_clinical_ind = @cme_pr_clinical_ind, " + 
                    "cme_pr_quality_ind = @cme_pr_quality_ind, " + 
                    "cme_pr_requirement_ind = @cme_pr_requirement_ind, " + 
                    "cme_pr_evaluation_ind = @cme_pr_evaluation_ind, " + 
                    "cme_pr_outcome_ind = @cme_pr_outcome_ind, " + 
                    "cme_pr_search_ind = @cme_pr_search_ind, " + 
                    "cme_pr_case_ind = @cme_pr_case_ind, " + 
                    "cme_pr_other_ind = @cme_pr_other_ind, " + 
                    "cme_pr_describe = @cme_pr_describe, " + 
                    "cme_usd_learner = @cme_usd_learner, " + 
                    "cme_usd_procedure = @cme_usd_procedure, " + 
                    "cme_usd_result = @cme_usd_result, " + 
                    "cme_usd_cause = @cme_usd_cause, " + 
                    "cme_usd_explain = @cme_usd_explain, " + 
                    "cme_cp_patient_ind = @cme_cp_patient_ind, " +
                    "cme_cp_patient = @cme_cp_patient, " + 
                    "cme_cp_knowledge_ind = @cme_cp_knowledge_ind, " + 
                    "cme_cp_knowledge = @cme_cp_knowledge, " + 
                    "cme_cp_practice_ind = @cme_cp_practice_ind, " +
                    "cme_cp_systempractice_ind = @cme_cp_systempractice_ind, " +
                    "cme_cp_practice = @cme_cp_practice, " +
                    "cme_cp_profession_ind = @cme_cp_profession_ind, " +
                    "cme_cp_profession = @cme_cp_profession, " + 
                    "cme_cp_skill_ind = @cme_cp_skill_ind, " +
                    "cme_cp_skill = @cme_cp_skill, " +
                    "cme_cp_care_ind = @cme_cp_care_ind, " +
                    "cme_cp_care = @cme_cp_care, " +
                    "cme_cp_team_ind = @cme_cp_team_ind, " +
                    "cme_cp_team = @cme_cp_team, " +
                    "cme_cp_evidence_ind = @cme_cp_evidence_ind, " +
                    "cme_cp_evidence = @cme_cp_evidence, " +
                    "cme_cp_quality_ind = @cme_cp_quality_ind, " +
                    "cme_cp_quality = @cme_cp_quality, " +
                    "cme_cp_utilize_ind = @cme_cp_utilize_ind, " +
                    "cme_cp_utilize = @cme_cp_utilize, " +
                    "cme_cp_content = @cme_cp_content, " +
                    "cme_cp_barrier = @cme_cp_barrier, " +
                    "cme_cp_strategy = @cme_cp_strategy, " +
                    "cme_cp_nonstrategy = @cme_cp_nonstrategy, " +
                    "cme_cp_purpose = @cme_cp_purpose, " +
                    "cme_cp_factors = @cme_cp_factors, " +
                    "cme_pg_pg1 = @cme_pg_pg1, " +
                    "cme_pg_pg2 = @cme_pg_pg2, " +
                    "cme_pg_pg3 = @cme_pg_pg3 " + 
                    "where topicid = @topicid", cn);

                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicCMEAudit.TopicID;
                cmd.Parameters.Add("@cme_cd_evidence_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcdEvidenceInd;
                cmd.Parameters.Add("@cme_cd_planner_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcdPlannerInd;
                cmd.Parameters.Add("@cme_cd_activity_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcdActivityInd;
                cmd.Parameters.Add("@cme_cd_recommendation_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcdRecommendationInd;
                cmd.Parameters.Add("@cme_cd_research_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcdResearchInd;
                cmd.Parameters.Add("@cme_cd_targetaudience", SqlDbType.VarChar).Value = topicCMEAudit.CMEcdTargetAudience;
                cmd.Parameters.Add("@cme_cd_content", SqlDbType.VarChar).Value = topicCMEAudit.CMEcdContent;
                cmd.Parameters.Add("@cme_cd_peer1", SqlDbType.VarChar).Value = topicCMEAudit.CMEcdPeer1;
                cmd.Parameters.Add("@cme_cd_peer2", SqlDbType.VarChar).Value = topicCMEAudit.CMEcdPeer2;
                cmd.Parameters.Add("@cme_cd_peer3", SqlDbType.VarChar).Value = topicCMEAudit.CMEcdPeer3;
                cmd.Parameters.Add("@cme_pr_clinical_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprClinicalInd;
                cmd.Parameters.Add("@cme_pr_quality_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprQualityInd;
                cmd.Parameters.Add("@cme_pr_requirement_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprRequirementInd;
                cmd.Parameters.Add("@cme_pr_evaluation_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprEvaluationInd;
                cmd.Parameters.Add("@cme_pr_outcome_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprOutcomeInd;
                cmd.Parameters.Add("@cme_pr_search_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprSearchInd;
                cmd.Parameters.Add("@cme_pr_case_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprCaseInd;
                cmd.Parameters.Add("@cme_pr_other_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEprOtherInd;
                cmd.Parameters.Add("@cme_pr_describe", SqlDbType.VarChar).Value = topicCMEAudit.CMEprDescribe;
                cmd.Parameters.Add("@cme_usd_learner", SqlDbType.VarChar).Value = topicCMEAudit.CMEusdLearner;
                cmd.Parameters.Add("@cme_usd_procedure", SqlDbType.VarChar).Value = topicCMEAudit.CMEusdProcedure;
                cmd.Parameters.Add("@cme_usd_result", SqlDbType.VarChar).Value = topicCMEAudit.CMEusdResult;
                cmd.Parameters.Add("@cme_usd_cause", SqlDbType.VarChar).Value = topicCMEAudit.CMEusdCause;
                cmd.Parameters.Add("@cme_usd_explain", SqlDbType.VarChar).Value = topicCMEAudit.CMEusdExplain;
                cmd.Parameters.Add("@cme_cp_patient_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpPatientInd;
                cmd.Parameters.Add("@cme_cp_patient", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpPatient;
                cmd.Parameters.Add("@cme_cp_knowledge_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpKnowledgeInd;
                cmd.Parameters.Add("@cme_cp_knowledge", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpKnowledge;
                cmd.Parameters.Add("@cme_cp_practice_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpPracticeInd;
                cmd.Parameters.Add("@cme_cp_systempractice_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpSystemPracticeInd;
                cmd.Parameters.Add("@cme_cp_practice", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpPractice;
                cmd.Parameters.Add("@cme_cp_profession_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpProfessionInd;
                cmd.Parameters.Add("@cme_cp_profession", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpProfession;
                cmd.Parameters.Add("@cme_cp_skill_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpSkillInd;
                cmd.Parameters.Add("@cme_cp_skill", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpSkill;
                cmd.Parameters.Add("@cme_cp_care_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpCareInd;
                cmd.Parameters.Add("@cme_cp_care", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpCare;
                cmd.Parameters.Add("@cme_cp_team_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpTeamInd;
                cmd.Parameters.Add("@cme_cp_team", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpTeam;
                cmd.Parameters.Add("@cme_cp_evidence_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpEvidenceInd;
                cmd.Parameters.Add("@cme_cp_evidence", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpEvidence;
                cmd.Parameters.Add("@cme_cp_quality_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpQualityInd;
                cmd.Parameters.Add("@cme_cp_quality", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpQuality;
                cmd.Parameters.Add("@cme_cp_utilize_ind", SqlDbType.Bit).Value = topicCMEAudit.CMEcpUtilizeInd;
                cmd.Parameters.Add("@cme_cp_utilize", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpUtilize;
                cmd.Parameters.Add("@cme_cp_content", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpContent;
                cmd.Parameters.Add("@cme_cp_barrier", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpBarrier;
                cmd.Parameters.Add("@cme_cp_strategy", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpStrategy;
                cmd.Parameters.Add("@cme_cp_nonstrategy", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpNonStrategy;
                cmd.Parameters.Add("@cme_cp_purpose", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpPurpose;
                cmd.Parameters.Add("@cme_cp_factors", SqlDbType.VarChar).Value = topicCMEAudit.CMEcpFactors;
                cmd.Parameters.Add("@cme_pg_pg1", SqlDbType.VarChar).Value = topicCMEAudit.CMEpgPg1;
                cmd.Parameters.Add("@cme_pg_pg2", SqlDbType.VarChar).Value = topicCMEAudit.CMEpgPg2;
                cmd.Parameters.Add("@cme_pg_pg3", SqlDbType.VarChar).Value = topicCMEAudit.CMEpgPg3;

                cn.Open();
                return (cmd.ExecuteNonQuery() == 1);
            }
        }
        #endregion SQLPRProvider

        #region PRProvider

        /// <summary>
        /// Returns a new TopicAuditInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual TopicCMEAuditInfo GetTopicCMEAuditFromReader(IDataReader reader)
        {
            return GetTopicCMEAuditFromReader(reader, true);
        }

        protected virtual TopicCMEAuditInfo GetTopicCMEAuditFromReader(IDataReader reader, bool readMemos)
        {
            TopicCMEAuditInfo TopicCMEAudit = new TopicCMEAuditInfo(
                (int)reader["topicid"], 
                (bool)reader["cme_cd_evidence_ind"],
                (bool)reader["cme_cd_planner_ind"],
                (bool)reader["cme_cd_activity_ind"],
                (bool)reader["cme_cd_recommendation_ind"],
                (bool)reader["cme_cd_research_ind"],
                reader["cme_cd_targetaudience"].ToString(),
                reader["cme_cd_content"].ToString(),
                reader["cme_cd_peer1"].ToString(),
                reader["cme_cd_peer2"].ToString(),
                reader["cme_cd_peer3"].ToString(),
                (bool)reader["cme_pr_clinical_ind"],
                (bool)reader["cme_pr_quality_ind"],
                (bool)reader["cme_pr_requirement_ind"],
                (bool)reader["cme_pr_evaluation_ind"],
                (bool)reader["cme_pr_outcome_ind"],
                (bool)reader["cme_pr_search_ind"],
                (bool)reader["cme_pr_case_ind"],
                (bool)reader["cme_pr_other_ind"],
                reader["cme_pr_describe"].ToString(),
                reader["cme_usd_learner"].ToString(),
                reader["cme_usd_procedure"].ToString(),
                reader["cme_usd_result"].ToString(),
                reader["cme_usd_cause"].ToString(),
                reader["cme_usd_explain"].ToString(),
                (bool)reader["cme_cp_patient_ind"],
                reader["cme_cp_patient"].ToString(),
                (bool)reader["cme_cp_knowledge_ind"],
                reader["cme_cp_knowledge"].ToString(),
                (bool)reader["cme_cp_practice_ind"],
                (bool)reader["cme_cp_systempractice_ind"],
                reader["cme_cp_practice"].ToString(),
                (bool)reader["cme_cp_profession_ind"],
                reader["cme_cp_profession"].ToString(),
                (bool)reader["cme_cp_skill_ind"],
                reader["cme_cp_skill"].ToString(),
                (bool)reader["cme_cp_care_ind"],
                reader["cme_cp_care"].ToString(),
                (bool)reader["cme_cp_team_ind"],
                reader["cme_cp_team"].ToString(),
                (bool)reader["cme_cp_evidence_ind"],
                reader["cme_cp_evidence"].ToString(),
                (bool)reader["cme_cp_quality_ind"],
                reader["cme_cp_quality"].ToString(),
                (bool)reader["cme_cp_utilize_ind"],
                reader["cme_cp_utilize"].ToString(),
                reader["cme_cp_content"].ToString(),
                reader["cme_cp_barrier"].ToString(),
                reader["cme_cp_strategy"].ToString(),
                reader["cme_cp_nonstrategy"].ToString(),
                reader["cme_cp_purpose"].ToString(),
                reader["cme_cp_factors"].ToString(),
                reader["cme_pg_pg1"].ToString(),
                reader["cme_pg_pg2"].ToString(),
                reader["cme_pg_pg3"].ToString()
                );

            return TopicCMEAudit;
        }

        /// <summary>
        /// Returns a collection of TopicAuditInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<TopicCMEAuditInfo> GetTopicCMEAuditCollectionFromReader(IDataReader reader)
        {
            return GetTopicCMEAuditCollectionFromReader(reader, true);
        }

        protected virtual List<TopicCMEAuditInfo> GetTopicCMEAuditCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TopicCMEAuditInfo> TopicCMEAudits = new List<TopicCMEAuditInfo>();
            while (reader.Read())
                TopicCMEAudits.Add(GetTopicCMEAuditFromReader(reader, readMemos));
            return TopicCMEAudits;
        }

        #endregion
    }
}
#endregion SQLPRProvider and PRProvider

