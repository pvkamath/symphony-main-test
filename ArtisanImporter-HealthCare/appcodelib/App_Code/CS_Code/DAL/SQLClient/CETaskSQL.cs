﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;


#region CETaskSQL

namespace PearlsReview.DAL
{
    public class CETaskInfo
    {
        public CETaskInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public CETaskInfo(int TaskID, DateTime RequestDate, DateTime ReviewDate, DateTime CompleteDate,
                             string TaskType, string TaskArea, string TaskName, string TaskDetail, string ReviewComment,
                             string CompleteComment)
        {
            this.TaskID = TaskID;
            this.RequestDate = RequestDate;
            this.ReviewDate = ReviewDate;
            this.CompleteDate = CompleteDate;
            this.TaskType = TaskType;
            this.TaskArea = TaskArea;
            this.TaskName = TaskName;
            this.TaskDetail = TaskDetail;
            this.ReviewComment = ReviewComment;
            this.CompleteComment = CompleteComment;
        }

        public int _TaskID = 0;
        public int TaskID
        {
            get { return _TaskID; }
            set { _TaskID = value; }
        }

        private DateTime _RequestDate = System.DateTime.MinValue;
        public DateTime RequestDate
        {
            get { return _RequestDate; }
            set { _RequestDate = value; }
        }

        private DateTime _ReviewDate = System.DateTime.MinValue;
        public DateTime ReviewDate
        {
            get { return _ReviewDate; }
            set { _ReviewDate = value; }
        }

        private DateTime _CompleteDate = System.DateTime.MinValue;
        public DateTime CompleteDate
        {
            get { return _CompleteDate; }
            set { _CompleteDate = value; }
        }

        private string _TaskType = "";
        public string TaskType
        {
            get { return _TaskType; }
            set { _TaskType = value; }
        }

        private string _TaskArea = "";
        public string TaskArea
        {
            get { return _TaskArea; }
            set { _TaskArea = value; }
        }

        private string _TaskName = "";
        public string TaskName
        {
            get { return _TaskName; }
            set { _TaskName = value; }
        }

        private string _TaskDetail = "";
        public string TaskDetail
        {
            get { return _TaskDetail; }
            set { _TaskDetail = value; }
        }

        private string _ReviewComment = "";
        public string ReviewComment
        {
            get { return _ReviewComment; }
            set { _ReviewComment = value; }
        }

        private string _CompleteComment = "";
        public string CompleteComment
        {
            get { return _CompleteComment; }
            set { _CompleteComment = value; }
        }
    }

}
#endregion 


#region SQLPRProvider and PRProvider
namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLProvider

        /////////////////////////////////////////////////////////
        // methods that work with CETask

        /// <summary>
        /// Returns the total number of CETasks
        /// </summary>
        public int GetCETaskCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from CETask", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves top 5 CETasks
        /// </summary>
        public List<CETaskInfo> GetCETask(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select  top 3 * " +
                    "from CETask";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    // default to TaskID order if none specified
                    cSQLCommand = cSQLCommand +
                        " order by TaskID ";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCETaskCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all available CETasks
        /// </summary>
        public List<CETaskInfo> GetAllCETask(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from CETask";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    // default to TaskID order if none specified
                    cSQLCommand = cSQLCommand +
                        " order by TaskID ";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCETaskCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the CETask with the specified ID
        /// </summary>
        public CETaskInfo GetCETaskByID(int TaskID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from CETask where TaskID=@TaskID", cn);
                cmd.Parameters.Add("@TaskID", SqlDbType.Int).Value = TaskID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCETaskFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a CETask
        /// </summary>
        public bool DeleteCETask(int TaskID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from CETask where TaskID=@TaskID", cn);
                cmd.Parameters.Add("@TaskID", SqlDbType.Int).Value = TaskID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new CETask
        /// </summary>
        public int InsertCETask(CETaskInfo CETask)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into CETask " +
              "(requestdate, " +
              "reviewdate, " +
              "completedate, " +
              "tasktype, " +
              "taskarea, " +
              "taskname, " +
              "taskdetail, " +
              "reviewcomment, " +
              "completecomment) " +
              "VALUES (" +
              "@RequestDate, " +
              "@ReviewDate, " +
              "@CompleteDate, " +
              "@TaskType, " +
              "@TaskArea, " +
              "@TaskName, " +
              "@TaskDetail, " +
              "@ReviewComment, " +
              "@CompleteComment ) SET @ID = SCOPE_IDENTITY()", cn);


                cmd.Parameters.Add("@RequestDate", SqlDbType.VarChar).Value = CETask.RequestDate.ToShortDateString();
                cmd.Parameters.Add("@ReviewDate", SqlDbType.VarChar).Value = CETask.ReviewDate.ToShortDateString();
                cmd.Parameters.Add("@CompleteDate", SqlDbType.VarChar).Value = CETask.CompleteDate.ToShortDateString();
                cmd.Parameters.Add("@TaskType", SqlDbType.VarChar).Value = CETask.TaskType;
                cmd.Parameters.Add("@TaskArea", SqlDbType.VarChar).Value = CETask.TaskArea;
                cmd.Parameters.Add("@TaskName", SqlDbType.VarChar).Value = CETask.TaskName;
                cmd.Parameters.Add("@TaskDetail", SqlDbType.VarChar).Value = CETask.TaskDetail;
                cmd.Parameters.Add("@ReviewComment", SqlDbType.VarChar).Value = CETask.ReviewComment;
                cmd.Parameters.Add("@CompleteComment", SqlDbType.VarChar).Value = CETask.CompleteComment;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a CETask
        /// </summary>
        public bool UpdateCETask(CETaskInfo CETask)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update CETask set " +
               "RequestDate = @RequestDate, " +
               "ReviewDate = @ReviewDate, " +
               "CompleteDate = @CompleteDate, " +
               "TaskType = @TaskType, " +
               "TaskArea = @TaskArea, " +
               "TaskName = @TaskName, " +
               "TaskDetail = @TaskDetail, " +
               "ReviewComment = @ReviewComment, " +
               "CompleteComment = @CompleteComment " +
               "where TaskID = @TaskID ", cn);

                cmd.Parameters.Add("@TaskID", SqlDbType.Int).Value = CETask.TaskID;
                cmd.Parameters.Add("@RequestDate", SqlDbType.VarChar).Value = CETask.RequestDate;
                cmd.Parameters.Add("@ReviewDate", SqlDbType.VarChar).Value = CETask.ReviewDate;
                cmd.Parameters.Add("@CompleteDate", SqlDbType.VarChar).Value = CETask.CompleteDate;
                cmd.Parameters.Add("@TaskType", SqlDbType.VarChar).Value = CETask.TaskType;
                cmd.Parameters.Add("@TaskArea", SqlDbType.VarChar).Value = CETask.TaskArea;
                cmd.Parameters.Add("@TaskName", SqlDbType.VarChar).Value = CETask.TaskName;
                cmd.Parameters.Add("@TaskDetail", SqlDbType.VarChar).Value = CETask.TaskDetail;
                cmd.Parameters.Add("@ReviewComment", SqlDbType.VarChar).Value = CETask.ReviewComment;
                cmd.Parameters.Add("@CompleteComment", SqlDbType.VarChar).Value = CETask.CompleteComment;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        #endregion

        #region PRProvider
        /////////////////////////////////////////////////////////
        // methods that work with CETaskSQL

        /// <summary>
        /// Returns a new CETaskInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual CETaskInfo GetCETaskFromReader(IDataReader reader)
        {
            return GetCETaskFromReader(reader, true);
        }
        protected virtual CETaskInfo GetCETaskFromReader(IDataReader reader, bool readMemos)
        {
            CETaskInfo CETask = new CETaskInfo(
              (int)reader["TaskID"],
            (DateTime)(Convert.IsDBNull(reader["RequestDate"])
                  ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["RequestDate"]),
            (DateTime)(Convert.IsDBNull(reader["ReviewDate"])
                    ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["ReviewDate"]),
            (DateTime)(Convert.IsDBNull(reader["CompleteDate"])
                    ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["CompleteDate"]),
              reader["TaskType"].ToString(),
              reader["TaskArea"].ToString(),
              reader["TaskName"].ToString(),
              reader["TaskDetail"].ToString(),
              reader["ReviewComment"].ToString(),
              reader["CompleteComment"].ToString());
            return CETask;
        }

        /// <summary>
        /// Returns a collection of CETaskInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<CETaskInfo> GetCETaskCollectionFromReader(IDataReader reader)
        {
            return GetCETaskCollectionFromReader(reader, true);
        }
        protected virtual List<CETaskInfo> GetCETaskCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<CETaskInfo> CETasks = new List<CETaskInfo>();
            while (reader.Read())
                CETasks.Add(GetCETaskFromReader(reader, readMemos));
            return CETasks;
        }

        #endregion


    }

}
#endregion