﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region AreaTypeInfo

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for AreaTypeInfo
    /// </summary>
    public class AreaTypeInfo
    {
        public AreaTypeInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public AreaTypeInfo(int areaid, string areaname, string tabname,string instruction)
        {
            this.areaid = areaid;
            this.areaname = areaname;
            this.tabname = tabname;
            this.instruction = instruction;
        }

        private int _areaid = 0;
        public int areaid
        {
            get { return _areaid; }
            protected set { _areaid = value; }
        }

        private string _areaname = "";
        public string areaname
        {
            get { return _areaname; }
            private set { _areaname = value; }
        }

        private string _tabname = "";
        public string tabname
        {
            get { return _tabname; }
            private set { _tabname = value; }
        }
        private string _instruction = "";
        public string instruction
        {
            get { return _instruction; }
            private set { _instruction = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /// <summary>
        /// Retrieves all AreaTypes
        /// </summary>
        public  List<AreaTypeInfo> GetAreaTypes(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from AreaType";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all AreaTypes
        /// </summary>
        public  List<AreaTypeInfo> GetFacilityAreaTypes(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from AreaType where areaid < 1";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all AreaTypes for dropdown dispaly
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypesForDropDown(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select areaid, areaname+ ' - ' + tabname as tabname,areaname,instruction  " +
                    "from AreaType";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        

            
        /// <summary>
        /// Retrieves all AreaTypes for dropdown dispaly
        /// </summary>
        public List<AreaTypeInfo> GetCategoryAreaTypesForDropDown(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select areaid, areaname+ ' - ' + tabname as tabname,areaname , instruction  " +
                    "from AreaType where areaname not in ('CE Retail','Facility') ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all AreaTypes for depends on areaid
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypesByAreaID(int AreaID,string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from AreaType where areaid = " + AreaID ;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all AreaTypes for depends on areatype
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypesByAreaName(String AreaName, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from AreaType where areaname = '" + AreaName + "'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all AreaTypes for depends on areatype
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypesByAreaNames(String AreaNames, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from AreaType where areaname in ( " + AreaNames + ") and areaid not in (24,25,26,31)";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all AreaTypes for depends on areatype
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypesByAreaNamesForCEDirect(String AreaNames, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from AreaType where areaname in ( " + AreaNames + ") and areaid not in (24,25,26,31) and tabname not in ('CE.COM Review','CME Review','Not Listed')";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all AreaTypes for depends on areatype
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypesAndTabNamesByAreaName(String AreaName, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select a.areaid," +
                    " a.tabname as areaname,a.tabname+ ' ('+convert(varchar(3),count(*)) +')' as tabname , a.instruction  " +
                    " from Categories c " +
                    " inner join categoryarealink ca on c.id=ca.categoryid  " +
                    " join areatype a on ca.areaid=a.areaid " +
                    "where showinlist='1' and a.areaname = '" + AreaName + "'" +
                    " group by a.areaid,a.areaname,a.tabname ";                
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by a.tabname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Returns a collection with all Facility AreaTypes
        /// </summary>
        public List<AreaTypeInfo> GetFacilityAreaTypesForDropDown(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from areatype ";
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression + " desc";
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by a.tabname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<AreaTypeInfo> GetUniqueAreaTypes(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct areaname,'' as tabname, areaid, '' as instruction from areatype where areaname not in ('CE Courses','CE Retail')";
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression + " asc";
                }
              
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all AreaTypes for depends on areatype
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypesByAreaNameAndFacId(String AreaName,int FacId, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select AreaType.* " +
                    "from AreaType join facilityarealink on areatype.areaid=facilityarealink.areaid where areaname = '" + AreaName + "' and facilityid = " + FacId;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves all AreaTypes for depends on areatype
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypesAndAreaNamesByFacId(int FacId, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct areaname, 0 as areaid , '' as instruction,'' as tabname  " +
                    "from AreaType join facilityarealink on areatype.areaid=facilityarealink.areaid where facilityid = " + FacId;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all AreaTypes Instructions
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypeInstructions(string cSortExpression)
        {
            List<AreaTypeInfo> lst = null;

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select AreaType.* " +
                    "from AreaType where areaid >= 0 and areaid<>2 and areaid<>15 ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                lst = GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);

                cn.Close();
                cSQLCommand = "select AreaType.* " +
                   "from AreaType where  areaid=15 ";


                cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                lst.AddRange(GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false));
            }

            return lst;
        }

        /// <summary>
        /// Retrieves all AreaTypes for depends on areatype
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypesByDomainId(int DomainId, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select at.* from AreaType at " +
                        " join MicrositeSpeciality ms on at.areaid=ms.disciplineid  where ms.msid = " + DomainId;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all AreaTypes for depends on DomainId And UserId
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypesByDomainIdAndUserId(int DomainId, int UserId, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select at.* from AreaType at " +
                        " join MicrositeSpeciality ms on at.areaid=ms.disciplineid  where ms.msid = " + DomainId + 
                " and areaid in (select disciplineid from UserDiscipline where UserID = " + UserId + " )";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all AreaTypes Instructions
        /// </summary>
        public List<AreaTypeInfo> GetAreaTypeInstructionsByUserId(int UserId, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select AreaType.* " +
                    "from AreaType join Userdiscipline on areatype.areaid=userdiscipline.disciplineid where userdiscipline.userid= " + UserId;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by areaname";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        #endregion

        #region PRProvider

        protected virtual AreaTypeInfo GetAreaTypeFromReader(IDataReader reader)
        {
            return GetAreaTypeFromReader(reader, true);
        }
        protected virtual AreaTypeInfo GetAreaTypeFromReader(IDataReader reader, bool readMemos)
        {
            AreaTypeInfo AreaType = new AreaTypeInfo(
              (int)reader["areaid"],
              reader["areaname"].ToString(),
              reader["tabname"].ToString(),
              reader["instruction"].ToString());

            return AreaType;
        }

        protected virtual List<AreaTypeInfo> GetAreaTypeCollectionFromReader(IDataReader reader)
        {
            return GetAreaTypeCollectionFromReader(reader, true);
        }
        protected virtual List<AreaTypeInfo> GetAreaTypeCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<AreaTypeInfo> AreaTypes = new List<AreaTypeInfo>();
            while (reader.Read())
                AreaTypes.Add(GetAreaTypeFromReader(reader, readMemos));
            return AreaTypes;
        }

        #endregion
    }
}
#endregion