﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region DocumentInfo

namespace PearlsReview.DAL
{
    public class DocumentInfo
    {
        public DocumentInfo() { }

        public DocumentInfo(int DocumentID, string FileName, string Description,
            string Created, DateTime Uploaded, string UploadedBy)
        {
            this.DocumentID = DocumentID;
            this.FileName = FileName;
            this.Description = Description;
            this.Created = Created;
            this.Uploaded = Uploaded;
            this.UploadedBy = UploadedBy;
        }

        private int _DocumentID = 0;
        public int DocumentID
        {
            get { return _DocumentID; }
            protected set { _DocumentID = value; }
        }

        private string _FileName = "";
        public string FileName
        {
            get { return _FileName; }
            private set { _FileName = value; }
        }

        private string _Description = "";
        public string Description
        {
            get { return _Description; }
            private set { _Description = value; }
        }

        private string _Created = "";
        public string Created
        {
            get { return _Created; }
            private set { _Created = value; }
        }

        private DateTime _Uploaded = System.DateTime.Now;
        public DateTime Uploaded
        {
            get { return _Uploaded; }
            private set { _Uploaded = value; }
        }

        private string _UploadedBy = "";
        public string UploadedBy
        {
            get { return _UploadedBy; }
            private set { _UploadedBy = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Documents        
        /// <summary>
        /// Returns the total number of Documents
        /// </summary>
        public int GetDocumentCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Document", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Documents
        /// </summary>
        public  List<DocumentInfo> GetDocuments(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Document";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetDocumentCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Documents for a RelationID and Type
        /// (Types: resource person="R", topic="T")
        /// </summary>
        public  List<DocumentInfo> GetDocumentsByRelationIDAndType(int RelationID, string RelationType, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Document " +
                    "join DocumentLink on Document.DocumentID = DocumentLink.DocumentID " +
                    "where DocumentLink.RelationID = @RelationID " +
                    "and DocumentLink.RelationType = @RelationType ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@RelationID", SqlDbType.Int).Value = RelationID;
                cmd.Parameters.Add("@RelationType", SqlDbType.VarChar).Value = RelationType;

                cn.Open();
                return GetDocumentCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Document with the specified ID
        /// </summary>
        public  DocumentInfo GetDocumentByID(int DocumentID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Document where DocumentID=@DocumentID", cn);
                cmd.Parameters.Add("@DocumentID", SqlDbType.Int).Value = DocumentID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetDocumentFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a Document
        /// </summary>
        public  bool DeleteDocument(int DocumentID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Document where DocumentID=@DocumentID", cn);
                cmd.Parameters.Add("@DocumentID", SqlDbType.Int).Value = DocumentID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Document
        /// </summary>
        public  int InsertDocument(DocumentInfo Document)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Document " +
              "(FileName, " +
              "Description, " +
              "Created, " +
              "Uploaded, " +
              "UploadedBy) " +
              "VALUES (" +
              "@FileName, " +
              "@Description, " +
              "@Created, " +
              "@Uploaded, " +
              "@UploadedBy) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@FileName", SqlDbType.VarChar).Value = Document.FileName;
                cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = Document.Description;
                cmd.Parameters.Add("@Created", SqlDbType.VarChar).Value = Document.Created;
                cmd.Parameters.Add("@Uploaded", SqlDbType.DateTime).Value = Document.Uploaded;
                cmd.Parameters.Add("@UploadedBy", SqlDbType.VarChar).Value = Document.UploadedBy;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Document
        /// </summary>
        public  bool UpdateDocument(DocumentInfo Document)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Document set " +
              "FileName = @FileName, " +
              "Description = @Description, " +
              "Created = @Created, " +
              "Uploaded = @Uploaded, " +
              "UploadedBy = @UploadedBy " +
              "where DocumentID = @DocumentID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@FileName", SqlDbType.VarChar).Value = Document.FileName;
                cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = Document.Description;
                cmd.Parameters.Add("@Created", SqlDbType.VarChar).Value = Document.Created;
                cmd.Parameters.Add("@Uploaded", SqlDbType.DateTime).Value = Document.Uploaded;
                cmd.Parameters.Add("@UploadedBy", SqlDbType.VarChar).Value = Document.UploadedBy;
                cmd.Parameters.Add("@DocumentID", SqlDbType.Int).Value = Document.DocumentID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new DocumentInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual DocumentInfo GetDocumentFromReader(IDataReader reader)
        {
            return GetDocumentFromReader(reader, true);
        }
        protected virtual DocumentInfo GetDocumentFromReader(IDataReader reader, bool readMemos)
        {
            DocumentInfo Document = new DocumentInfo(
              (int)reader["DocumentID"],
              reader["FileName"].ToString(),
              reader["Description"].ToString(),
              reader["Created"].ToString(),
              (DateTime)reader["Uploaded"],
              reader["UploadedBy"].ToString());


            return Document;
        }

        /// <summary>
        /// Returns a collection of DocumentInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<DocumentInfo> GetDocumentCollectionFromReader(IDataReader reader)
        {
            return GetDocumentCollectionFromReader(reader, true);
        }
        protected virtual List<DocumentInfo> GetDocumentCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<DocumentInfo> Documents = new List<DocumentInfo>();
            while (reader.Read())
                Documents.Add(GetDocumentFromReader(reader, readMemos));
            return Documents;
        }

        #endregion
    }
}
#endregion