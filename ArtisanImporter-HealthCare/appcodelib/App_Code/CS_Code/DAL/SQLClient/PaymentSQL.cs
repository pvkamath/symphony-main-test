﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region PaymentInfo

namespace PearlsReview.DAL
{
    public class PaymentInfo
    {
        public PaymentInfo() { }

        public PaymentInfo(int PaymentID, DateTime PayDate, decimal PayAmount, string PayStatus,
            string PayType, string PayDesc, int UserID, int FacilityID, string CCError, string TransID, string AuthCode,DateTime ExpDate, string ProfileID, bool Renew, bool Active,
            decimal CouponAmount, int CouponId, String CCLast)
        {
            this.PaymentID = PaymentID;
            this.PayDate = PayDate;
            this.PayAmount = PayAmount;
            this.PayStatus = PayStatus;
            this.PayType = PayType;
            this.PayDesc = PayDesc;
            this.UserID = UserID;
            this.FacilityID = FacilityID;
            this.CCError = CCError;
            this.TransID = TransID;
            this.AuthCode = AuthCode;
            this.ExpDate = ExpDate;
            this.ProfileID = ProfileID;
            this.Renew = Renew;
            this.Active = Active;
            this.CouponAmount = CouponAmount;
            this.CouponId = CouponId;
            this.CCLast = CCLast;
        }

        private int _PaymentID = 0;
        public int PaymentID
        {
            get { return _PaymentID; }
            protected set { _PaymentID = value; }
        }

        private DateTime _PayDate = System.DateTime.Now;
        public DateTime PayDate
        {
            get { return _PayDate; }
            private set { _PayDate = value; }
        }

        private decimal _PayAmount = 0.00M;
        public decimal PayAmount
        {
            get { return _PayAmount; }
            private set { _PayAmount = value; }
        }

        private string _PayStatus = "";
        public string PayStatus
        {
            get { return _PayStatus; }
            private set { _PayStatus = value; }
        }

        private string _PayType = "";
        public string PayType
        {
            get { return _PayType; }
            private set { _PayType = value; }
        }

        private string _PayDesc = "";
        public string PayDesc
        {
            get { return _PayDesc; }
            private set { _PayDesc = value; }
        }

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            protected set { _UserID = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            protected set { _FacilityID = value; }
        }

        private string _CCError = "";
        public string CCError
        {
            get { return _CCError; }
            private set { _CCError = value; }
        }

        private string _TransID = "";
        public string TransID
        {
            get { return _TransID; }
            private set { _TransID = value; }
        }

        private string _AuthCode = "";
        public string AuthCode
        {
            get { return _AuthCode; }
            private set { _AuthCode = value; }
        }
        private DateTime _ExpDate = System.DateTime.Now;
        public DateTime ExpDate
        {
            get { return _ExpDate; }
            set { _ExpDate = value; }
        }
        private string _ProfileID = "";
        public string ProfileID
        {
            get { return _ProfileID; }
            set { _ProfileID = value; }
        }
        private bool _Renew = false;
        public bool Renew
        {
            get { return _Renew; }
            set { _Renew = value; }
        }
        private bool _Active = false;
        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }
        public decimal _CouponAmount = (decimal)0;
        public decimal CouponAmount
        {
            get { return _CouponAmount; }
            set { _CouponAmount = value; }
        }

        public int _CouponId = 0;
        public int CouponId
        {
            get { return _CouponId; }
            set { _CouponId = value; }
        }

        public string _CCLast = string.Empty;
        public string CCLast
        {
            get { return _CCLast; }
            set { _CCLast = value; }
        }

    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        /////////////////////////////////////////////////////////
        // methods that work with Payments

        /// <summary>
        /// Returns the total number of Payments
        /// </summary>
        public int GetPaymentCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Payment", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Payments
        /// </summary>
        public List<PaymentInfo> GetPayments(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Payment";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetPaymentCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Payment with the specified ID
        /// </summary>
        public PaymentInfo GetPaymentByID(int PaymentID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Payment where PaymentID=@PaymentID", cn);
                cmd.Parameters.Add("@PaymentID", SqlDbType.Int).Value = PaymentID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetPaymentFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves the Payment with the specified ID
        /// </summary>
        public PaymentInfo GetPaymentByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Payment where userid=@UserID and active='1' order by paydate desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetPaymentFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the Payment with the specified AuthCode
        /// </summary>
        public PaymentInfo GetPaymentByAuthcode(string AuthCode)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Payment where authcode=@AuthCode and active='1' order by paydate desc", cn);
                cmd.Parameters.Add("@AuthCode", SqlDbType.VarChar).Value = AuthCode;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetPaymentFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the Payment with the specified ID
        /// </summary>
        public List<PaymentInfo> GetPaymentsByUserID(int UserID)
        {
            List<PaymentInfo> paymentInfo = new List<PaymentInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Payment where userid=@UserID order by paydate desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
               return GetPaymentCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<PaymentInfo> GetActivePaymentsByUserID(int UserID)
        {
            List<PaymentInfo> paymentInfo = new List<PaymentInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Payment where userid=@UserID and active = 1 order by paydate desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return GetPaymentCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<PaymentInfo> DailyPRPaymentUpdate()
        {
            List<PaymentInfo> paymentInfo = new List<PaymentInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                    "from payment where expdate <= CONVERT(VARCHAR(10),DATEADD(day,-3,GETDATE()),21) " +
                    " and Renew=1 and Active=1 and facilityid=3", cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    PaymentInfo pinfo = GetPaymentFromReader(reader);
                    paymentInfo.Add(pinfo);
                }
                return paymentInfo;
            }
        }
        
        public List<PaymentInfo> DailyPRPaymentCancelledUpdate()
        {
            List<PaymentInfo> PaymentInfo = new List<PaymentInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                "from payment where expdate>=CONVERT(VARCHAR(10),DATEADD(day,0,GETDATE()),21) and expdate <CONVERT(VARCHAR(10),DATEADD(day,1,GETDATE()),21) " +
                    " and Renew=0 and Active=1 and facilityid=3", cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    PaymentInfo pinfo = GetPaymentFromReader(reader);
                    PaymentInfo.Add(pinfo);
                }
                return PaymentInfo;
            }
        }


        public bool DailyPRUserAccountUpdate(int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_Daily_PRPayment_Update";
                cmd.Connection = cn;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret >= 1);
            }
        }

        public PaymentInfo GetActivePaymentByUserIdandDomainId(int UserId, int DomainId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * from payment " +
                    " where userid = @UserId and active='1' and expdate >= getdate() and facilityid=@DomainId order by paydate desc", cn);
                cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
                cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetPaymentFromReader(reader, true);
                else
                    return null;
            }
        }

        public List<PaymentInfo> GetBDSPaymentByUserIdandFacid(int UserId, int Facid)
        {
            List<PaymentInfo> paymentInfo = new List<PaymentInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from payment " +
                   " where userid = @UserId  and facilityid=@Facid order by paydate desc", cn);
                cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
                cmd.Parameters.Add("@Facid", SqlDbType.Int).Value = Facid;
                cn.Open();
                return GetPaymentCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public PaymentInfo GetPaymentByUserIDAndDomainId(int UserId, int DomainId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * from payment " +
                    " where userid=@UserId and facilityid=@DomainId order by paydate desc", cn);
                cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
                cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetPaymentFromReader(reader, true);
                else
                    return null;
                
            }
        }

        /// <summary>
        /// Get Active PR Payment Records which are expiring this month
        /// </summary>
        /// <returns></returns>
        public List<PaymentInfo> GetActivePRPaymentsByExpDateInThisMonth()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * from payment " +
                    " where expdate between GETDATE() AND DATEADD(m,1,GETDATE()) and Renew=1 and Active=1 and facilityid=3 Order by ExpDate", cn);
                cn.Open();
                return GetPaymentCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Get Expired Credit Card users List
        /// </summary>
        /// <returns></returns>
        public DataSet GetCCExpiredUsers()
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select U.cemail,p.ExpDate,P.ReminderDate,P.CCLast,U.CFirstName from payment P join useraccount U on P.userid=U.iid where (P.expDate Between GETDATE() AND DATEADD(m,1,GETDATE())) and "+
                "Renew=1 and Active=1 and p.facilityid=3 and DATEADD(mm,1,P.ReminderDate) <=ExpDate ",cn);
                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "ExpUsers");
            }
            return ds;
            
        }

        public bool UpdatePaymentReminderDate(DateTime reminderDate, int paymentID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update payment set ReminderDate=@reminderDate where paymentid=@paymentID", cn);
                cmd.Parameters.Add("@paymentID", SqlDbType.Int).Value = paymentID;
                cmd.Parameters.Add("@reminderDate", SqlDbType.DateTime).Value = reminderDate;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
       

        /// <summary>
        /// Deletes a Payment
        /// </summary>
        public bool DeletePayment(int PaymentID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Payment set remwhere PaymentID=@PaymentID", cn);
                cmd.Parameters.Add("@PaymentID", SqlDbType.Int).Value = PaymentID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Payment
        /// </summary>
        public int InsertPayment(PaymentInfo Payment)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Payment " +
              "(PayDate, " +
              "PayAmount, " +
              "PayStatus, " +
              "PayType, " +
              "PayDesc, " +
              "UserID, " +
              "FacilityID, " +
              "CCError, " +
              "TransID, " +
              "AuthCode," +
              "ExpDate, " +
              "ProfileID, " +
              "Renew, " +
              "Active, " +
              "CouponAmount, " +
              "CouponId, " +
              "CCLast ) " +
              "VALUES (" +
              "@PayDate, " +
              "@PayAmount, " +
              "@PayStatus, " +
              "@PayType, " +
              "@PayDesc, " +
              "@UserID, " +
              "@FacilityID, " +
              "@CCError, " +
              "@TransID, " +
              "@AuthCode, " +
               "@ExpDate, " +
                "@ProfileID, " +
                "@Renew, " +
                "@Active, " +
                "@CouponAmount, " +
                "@CouponId, " +
                "@CCLast ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@PayDate", SqlDbType.DateTime).Value = Payment.PayDate;
                cmd.Parameters.Add("@PayAmount", SqlDbType.Decimal).Value = Payment.PayAmount;
                cmd.Parameters.Add("@PayStatus", SqlDbType.Char).Value = Payment.PayStatus;
                cmd.Parameters.Add("@PayType", SqlDbType.VarChar).Value = Payment.PayType;
                cmd.Parameters.Add("@PayDesc", SqlDbType.VarChar).Value = Payment.PayDesc;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Payment.UserID == 0)
                {
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Payment.UserID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Payment.FacilityID == 0)
                {
                    cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = Payment.FacilityID;
                }

                cmd.Parameters.Add("@CCError", SqlDbType.VarChar).Value = Payment.CCError;
                cmd.Parameters.Add("@TransID", SqlDbType.VarChar).Value = Payment.TransID;
                cmd.Parameters.Add("@AuthCode", SqlDbType.VarChar).Value = Payment.AuthCode;
                cmd.Parameters.Add("@ExpDate", SqlDbType.DateTime).Value = Payment.ExpDate;
                cmd.Parameters.Add("@ProfileID", SqlDbType.VarChar).Value = Payment.ProfileID;
                cmd.Parameters.Add("@Renew", SqlDbType.Bit).Value = Payment.Renew;
                cmd.Parameters.Add("@Active", SqlDbType.Bit).Value = Payment.Active;
                cmd.Parameters.Add("@CouponAmount", SqlDbType.Decimal).Value = Payment.CouponAmount;
                cmd.Parameters.Add("@CouponId", SqlDbType.Int).Value = Payment.CouponId;
                cmd.Parameters.Add("@CCLast", SqlDbType.VarChar).Value = Payment.CCLast;
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Payment
        /// </summary>
        public bool UpdatePayment(PaymentInfo Payment)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Payment set " +
              "PayDate = @PayDate, " +
              "PayAmount = @PayAmount, " +
              "PayStatus = @PayStatus, " +
              "PayType = @PayType, " +
              "PayDesc = @PayDesc, " +
              "UserID = @UserID, " +
              "FacilityID = @FacilityID, " +
              "CCError = @CCError, " +
              "TransID = @TransID, " +
              "AuthCode = @AuthCode, " +
                "ExpDate=@ExpDate, " +
                "ProfileID=@ProfileID, " +
                "Renew=@Renew, " +
                "CouponAmount= @CouponAmount, " +
                "CouponId = @CouponId, " +
                "CCLast = @CCLast, " +
                "Active=@Active where PaymentID = @PaymentID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PayDate", SqlDbType.DateTime).Value = Payment.PayDate;
                cmd.Parameters.Add("@PayAmount", SqlDbType.Decimal).Value = Payment.PayAmount;
                cmd.Parameters.Add("@PayStatus", SqlDbType.Char).Value = Payment.PayStatus;
                cmd.Parameters.Add("@PayType", SqlDbType.VarChar).Value = Payment.PayType;
                cmd.Parameters.Add("@PayDesc", SqlDbType.VarChar).Value = Payment.PayDesc;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Payment.UserID == 0)
                {
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Payment.UserID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Payment.FacilityID == 0)
                {
                    cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = Payment.FacilityID;
                }

                cmd.Parameters.Add("@CCError", SqlDbType.VarChar).Value = Payment.CCError;
                cmd.Parameters.Add("@TransID", SqlDbType.VarChar).Value = Payment.TransID;
                cmd.Parameters.Add("@AuthCode", SqlDbType.VarChar).Value = Payment.AuthCode;
                cmd.Parameters.Add("@ExpDate", SqlDbType.DateTime).Value = Payment.ExpDate;
                cmd.Parameters.Add("@ProfileID", SqlDbType.VarChar).Value = Payment.ProfileID;
                cmd.Parameters.Add("@Renew", SqlDbType.Bit).Value = Payment.Renew;
                cmd.Parameters.Add("@Active", SqlDbType.Bit).Value = Payment.Active;
                cmd.Parameters.Add("@CouponAmount", SqlDbType.Decimal).Value = Payment.CouponAmount;
                cmd.Parameters.Add("@CouponId", SqlDbType.Int).Value = Payment.CouponId;
                cmd.Parameters.Add("@CCLast", SqlDbType.VarChar).Value = Payment.CCLast;
                cmd.Parameters.Add("@PaymentID", SqlDbType.Int).Value = Payment.PaymentID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        #endregion
        #region PRProvider


        /////////////////////////////////////////////////////////
        // methods that work with Payments

        
        /// <summary>
        /// Returns a new PaymentInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual PaymentInfo GetPaymentFromReader(IDataReader reader)
        {
            return GetPaymentFromReader(reader, true);
        }
        protected virtual PaymentInfo GetPaymentFromReader(IDataReader reader, bool readMemos)
        {
            PaymentInfo Payment = new PaymentInfo(
              (int)reader["PaymentID"],
              (DateTime)reader["PayDate"],
              (decimal)reader["PayAmount"],
              reader["PayStatus"].ToString(),
              reader["PayType"].ToString(),
              reader["PayDesc"].ToString(),
              (int)(Convert.IsDBNull(reader["UserID"]) ? (int)0 : (int)reader["UserID"]),
              (int)(Convert.IsDBNull(reader["FacilityID"]) ? (int)0 : (int)reader["FacilityID"]),
              reader["CCError"].ToString(),
              reader["TransID"].ToString(),
              reader["AuthCode"].ToString(),
             (DateTime)(Convert.IsDBNull(reader["ExpDate"])
                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["ExpDate"]),
              reader["ProfileID"].ToString(),
              (bool)reader["Renew"],
              (bool)reader["Active"],
              (decimal)(Convert.IsDBNull(reader["CouponAmount"]) ? (decimal)0.0:(decimal)reader["CouponAmount"]),
              (int)(Convert.IsDBNull(reader["CouponId"]) ? (int)0: (int) reader["CouponId"]),
              reader["CCLast"].ToString());

            return Payment;
        }

        /// <summary>
        /// Returns a collection of PaymentInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<PaymentInfo> GetPaymentCollectionFromReader(IDataReader reader)
        {
            return GetPaymentCollectionFromReader(reader, true);
        }
        protected virtual List<PaymentInfo> GetPaymentCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<PaymentInfo> Payments = new List<PaymentInfo>();
            while (reader.Read())
                Payments.Add(GetPaymentFromReader(reader, readMemos));
            return Payments;
        }

        #endregion
    }
}
#endregion
