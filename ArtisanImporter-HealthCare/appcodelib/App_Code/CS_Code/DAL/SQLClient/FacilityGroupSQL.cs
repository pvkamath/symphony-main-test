﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region FacilityGroupInfo

namespace PearlsReview.DAL
{
    public class FacilityGroupInfo
    {
        public FacilityGroupInfo() { }

        public FacilityGroupInfo(int FacID, string FacCode, string FacName, int MaxUsers, DateTime Started,
            DateTime Expires, int RepID, string Comment, bool Compliance, bool CustomTopics, bool QuizBowl,
            string homeurl, string tagline, string address1, string address2, string city, string state,
            string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
            string contact_email, int parent_id, string welcome_pg, string login_help, string logo_img,
            bool active, string support_email, string feedback_email, string default_password,
             bool forceinfocheck, int salemanager_id, int accountmanager_id, bool dataupload_ind, bool datadownload_ind)
        {
            this.FacID = FacID;
            this.FacCode = FacCode;
            this.FacName = FacName;
            this.MaxUsers = MaxUsers;
            this.Started = Started;
            this.Expires = Expires;
            this.RepID = RepID;
            this.Comment = Comment;
            this.Compliance = Compliance;
            this.CustomTopics = CustomTopics;
            this.QuizBowl = QuizBowl;
            this.homeurl = homeurl;
            this.tagline = tagline;
            this.address1 = address1;
            this.address2 = address2;
            this.city = city;
            this.state = state;
            this.zipcode = zipcode;
            this.phone = phone;
            this.contact_name = contact_name;
            this.contact_phone = contact_phone;
            this.contact_ext = contact_ext;
            this.contact_email = contact_email;
            this.parent_id = parent_id;
            this.welcome_pg = welcome_pg;
            this.login_help = login_help;
            this.logo_img = logo_img;
            this.active = active;
            this.support_email = support_email;
            this.feedback_email = feedback_email;
            this.default_password = default_password;
            this.forceinfocheck = forceinfocheck;
            this.salemanager_id = salemanager_id;
            this.accountmanager_id = accountmanager_id;
            this.datadownload_ind = datadownload_ind;
            this.dataupload_ind = dataupload_ind;
        }

        private int _FacID = 0;
        public int FacID
        {
            get { return _FacID; }
            protected set { _FacID = value; }
        }

        private string _FacCode = "";
        public string FacCode
        {
            get { return _FacCode; }
            private set { _FacCode = value; }
        }

        private string _FacName = "";
        public string FacName
        {
            get { return _FacName; }
            private set { _FacName = value; }
        }

        private int _MaxUsers = 1000;
        public int MaxUsers
        {
            get { return _MaxUsers; }
            protected set { _MaxUsers = value; }
        }

        private DateTime _Started = System.DateTime.Now;
        public DateTime Started
        {
            get { return _Started; }
            private set { _Started = value; }
        }

        private DateTime _Expires = System.DateTime.Now.AddYears(1);
        public DateTime Expires
        {
            get { return _Expires; }
            private set { _Expires = value; }
        }

        private int _RepID = 0;
        public int RepID
        {
            get { return _RepID; }
            protected set { _RepID = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            private set { _Comment = value; }
        }

        private bool _Compliance = false;
        public bool Compliance
        {
            get { return _Compliance; }
            private set { _Compliance = value; }
        }

        private bool _CustomTopics = false;
        public bool CustomTopics
        {
            get { return _CustomTopics; }
            private set { _CustomTopics = value; }
        }

        private bool _QuizBowl = false;
        public bool QuizBowl
        {
            get { return _QuizBowl; }
            private set { _QuizBowl = value; }
        }

        private string _homeurl = "";
        public string homeurl
        {
            get { return _homeurl; }
            private set { _homeurl = value; }
        }

        private string _tagline = "";
        public string tagline
        {
            get { return _tagline; }
            private set { _tagline = value; }
        }

        private string _address1 = "";
        public string address1
        {
            get { return _address1; }
            private set { _address1 = value; }
        }

        private string _address2 = "";
        public string address2
        {
            get { return _address2; }
            private set { _address2 = value; }
        }

        private string _city = "";
        public string city
        {
            get { return _city; }
            private set { _city = value; }
        }

        private string _state = "";
        public string state
        {
            get { return _state; }
            private set { _state = value; }
        }

        private string _zipcode = "";
        public string zipcode
        {
            get { return _zipcode; }
            private set { _zipcode = value; }
        }

        private string _phone = "";
        public string phone
        {
            get { return _phone; }
            private set { _phone = value; }
        }

        private string _contact_name = "";
        public string contact_name
        {
            get { return _contact_name; }
            private set { _contact_name = value; }
        }

        private string _contact_phone = "";
        public string contact_phone
        {
            get { return _contact_phone; }
            private set { _contact_phone = value; }
        }

        private string _contact_ext = "";
        public string contact_ext
        {
            get { return _contact_ext; }
            private set { _contact_ext = value; }
        }

        private string _contact_email = "";
        public string contact_email
        {
            get { return _contact_email; }
            private set { _contact_email = value; }
        }

        private int _parent_id = 0;
        public int parent_id
        {
            get { return _parent_id; }
            private set { _parent_id = value; }
        }

        private string _welcome_pg = "";
        public string welcome_pg
        {
            get { return _welcome_pg; }
            private set { _welcome_pg = value; }
        }

        private string _login_help = "";
        public string login_help
        {
            get { return _login_help; }
            private set { _login_help = value; }
        }

        private string _logo_img = "";
        public string logo_img
        {
            get { return _logo_img; }
            private set { _logo_img = value; }
        }

        private bool _active = true;
        public bool active
        {
            get { return _active; }
            private set { _active = value; }
        }

        private string _support_email = "";
        public string support_email
        {
            get { return _support_email; }
            private set { _support_email = value; }
        }

        private string _feedback_email = "";
        public string feedback_email
        {
            get { return _feedback_email; }
            private set { _feedback_email = value; }
        }

        private string _default_password = "";
        public string default_password
        {
            get { return _default_password; }
            private set { _default_password = value; }
        }

        private bool _forceinfocheck = false;
        public bool forceinfocheck
        {
            get { return _forceinfocheck; }
            set { _forceinfocheck = value; }
        }

        private int _salemanager_id = 0;
        public int salemanager_id
        {
            get { return _salemanager_id; }
            set { _salemanager_id = value; }
        }

        private int _accountmanager_id = 0;
        public int accountmanager_id
        {
            get { return _accountmanager_id; }
            set { _accountmanager_id = value; }
        }

        private bool _dataupload_ind = false;
        public bool dataupload_ind
        {
            get { return _dataupload_ind; }
            set { _dataupload_ind = value; }
        }

        private bool _datadownload_ind = false;
        public bool datadownload_ind
        {
            get { return _datadownload_ind; }
            set { _datadownload_ind = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with FacilityGroups
        /// ***** CEDIRECT VERSION ************

        /// <summary>
        /// Returns the total number of FacilityGroups
        /// </summary>
        public int GetFacilityGroupCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from FacilityGroup", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }
        public DataTable GetFacilitiesByOrgID(int OrgID)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_get_ChildFacilitiesByOrgID";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrgID", SqlDbType.Int).Value = OrgID;
                cn.Open();
                using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    adapter.Fill(dt);
            }
            return dt;
        }

        /// <summary>
        /// Retrieves all FacilityGroups
        /// </summary>
        public List<FacilityGroupInfo> GetFacilityGroups(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from FacilityGroup";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        ///<summary>
        ///Retrieves all facilityGroups except gift card 
        ///</summary>
        ///

        public List<FacilityGroupInfo> GetFacilityGroupsExceptGiftCard(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from facilitygroup where (parent_id is null or parent_id not " +
                    "in (select parent_id from parentorganization where parent_name ='CE Gift Card')) and facid<>2 and active=1 or (active=0 and forceinfocheck=1)";

                //add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all FacilityGroups
        /// </summary>
        public List<FacilityGroupInfo> GetFacilityGroupsByParentOfFacilityID(int FacilityID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from FacilityGroup where (Parent_ID is null and FacID=@FacilityID) or " +
                    "( Parent_ID>0 and Parent_ID IN " +
                    "( select Parent_ID from FacilityGroup where FacID=@FacilityID ) ) ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;

                cn.Open();
                return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public DataSet GetExpireRetailFacility()
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "select A.*, B.cclast, B.expiredate, B.recuramount from facilitygroup A join retailfacility B on A.facid=b.facid and active=1 and renew=1 " +
                    " where expiredate between DATEADD(day,2,GETDATE()) and DATEADD(day,3,GETDATE())";
                SqlCommand cmd = new SqlCommand(qry, cn);
                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds);
            }
            return ds;
        }


        /// <summary>
        /// Retrieves all FacilityGroups
        /// </summary>
        public List<FacilityGroupInfo> GetFacilityGroupsByParentID(int ParentID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from FacilityGroup where Parent_id=@ParentID ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@ParentID", SqlDbType.Int).Value = ParentID;

                cn.Open();
                return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the FacilityGroup with the specified ID
        /// </summary>
        public FacilityGroupInfo GetFacilityGroupByID(int FacID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from FacilityGroup where FacID=@FacID", cn);
                cmd.Parameters.Add("@FacID", SqlDbType.Int).Value = FacID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetFacilityGroupFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the FacilityGroup with the specified IP
        /// </summary>
        public FacilityGroupInfo GetFacilityGroupByIP(string ip)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                        "from FacilityGroup where charindex(@IP, IP) > 0", cn);
                cmd.Parameters.Add("@IP", SqlDbType.VarChar).Value = ip;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetFacilityGroupFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the FacilityGroup with the specified Domain
        /// </summary>
        public FacilityGroupInfo GetFacilityGroupByDomain(string domain)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                        "from FacilityGroup where @Domain LIKE '%' + Domain + '%'", cn);
                cmd.Parameters.Add("@Domain", SqlDbType.VarChar).Value = domain;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetFacilityGroupFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the FacilityGroup with the specified ID
        /// </summary>
        public FacilityGroupInfo GetFacilityGroupByFacilityCode(string FacCode)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from FacilityGroup where FacCode=@FacCode", cn);
                cmd.Parameters.Add("@FacCode", SqlDbType.VarChar).Value = FacCode;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetFacilityGroupFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves the FacilityGroup with the specified Name
        /// </summary>
        public FacilityGroupInfo GetFacilityGroupByFacName(string FacName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from FacilityGroup where Facname=@FacName", cn);
                cmd.Parameters.Add("@FacName", SqlDbType.VarChar).Value = FacName;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetFacilityGroupFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the sub facilities of parentorganization by parentorgadmin
        /// </summary>
        public List<FacilityGroupInfo> GetFacilityGroupByOrgAdmin(int ParentOrgAdminID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * from facilitygroup " +
                    "where FacilityGroup.facid in (select facilityid from useraccount where useraccount.iid=@ParentOrgAdmin) or parent_id " +
                    "in (select FacilityGroup.parent_id from Facilitygroup inner join useraccount on useraccount.facilityid=facilitygroup.facid where Useraccount.iid=@ParentOrgAdmin and facilitygroup.parent_id<>0)", cn);
                cmd.Parameters.Add("@ParentOrgAdmin", SqlDbType.Int).Value = ParentOrgAdminID;
                cn.Open();
                return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Deletes a FacilityGroup
        /// </summary>
        public bool DeleteFacilityGroup(int FacID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from FacilityGroup where FacID=@FacID", cn);
                cmd.Parameters.Add("@FacID", SqlDbType.Int).Value = FacID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new FacilityGroup
        /// </summary>
        public int InsertFacilityGroup(FacilityGroupInfo FacilityGroup)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into FacilityGroup " +
              "(FacCode, " +
              "FacName, " +
              "MaxUsers, " +
              "Started, " +
              "Expires, " +
              "RepID, " +
              "Comment, " +
              "Compliance, " +
              "CustomTopics, " +
              "QuizBowl, " +
              "homeurl, " +
              "tagline, " +
              "address1, " +
              "address2, " +
              "city, " +
              "state, " +
              "zipcode, " +
              "phone, " +
              "contact_name, " +
              "contact_phone, " +
              "contact_ext, " +
              "contact_email, " +
              "parent_id, " +
              "welcome_pg, " +
              "login_help, " +
              "logo_img, " +
              "active, " +
              "support_email, " +
              "feedback_email, " +
              "default_password, " +
              "forceinfocheck, " +
               "salemanager_id, " +
              "accountmanager_id, " +
              "dataupload_ind, " +
              "datadownload_ind )  " +
              "VALUES (" +
              "@FacCode, " +
              "@FacName, " +
              "@MaxUsers, " +
              "@Started, " +
              "@Expires, " +
              "@RepID, " +
              "@Comment, " +
              "@Compliance, " +
              "@CustomTopics, " +
              "@QuizBowl, " +
              "@homeurl, " +
              "@tagline, " +
              "@address1, " +
              "@address2, " +
              "@city, " +
              "@state, " +
              "@zipcode, " +
              "@phone, " +
              "@contact_name, " +
              "@contact_phone, " +
              "@contact_ext, " +
              "@contact_email, " +
              "@parent_id, " +
              "@welcome_pg, " +
              "@login_help, " +
              "@logo_img, " +
              "@active, " +
              "@support_email, " +
              "@feedback_email, " +
              "@default_password, " +
              "@forceinfocheck, " +
              "@salemanager_id, " +
              "@accountmanager_id," +
              "@dataupload_ind, " +
              "@datadownload_ind) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@FacCode", SqlDbType.VarChar).Value = FacilityGroup.FacCode;
                cmd.Parameters.Add("@FacName", SqlDbType.VarChar).Value = FacilityGroup.FacName;
                cmd.Parameters.Add("@MaxUsers", SqlDbType.Int).Value = FacilityGroup.MaxUsers;
                cmd.Parameters.Add("@Started", SqlDbType.DateTime).Value = FacilityGroup.Started;
                cmd.Parameters.Add("@Expires", SqlDbType.DateTime).Value = FacilityGroup.Expires;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (FacilityGroup.RepID == 0)
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = FacilityGroup.RepID;
                }

                cmd.Parameters.Add("@Comment", SqlDbType.Text).Value = FacilityGroup.Comment;
                cmd.Parameters.Add("@Compliance", SqlDbType.Bit).Value = FacilityGroup.Compliance;
                cmd.Parameters.Add("@CustomTopics", SqlDbType.Bit).Value = FacilityGroup.CustomTopics;
                cmd.Parameters.Add("@QuizBowl", SqlDbType.Bit).Value = FacilityGroup.QuizBowl;
                cmd.Parameters.Add("@homeurl", SqlDbType.VarChar).Value = FacilityGroup.homeurl;
                cmd.Parameters.Add("@tagline", SqlDbType.VarChar).Value = FacilityGroup.tagline;
                cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = FacilityGroup.address1;
                cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = FacilityGroup.address2;
                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = FacilityGroup.city;
                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = FacilityGroup.state;
                cmd.Parameters.Add("@zipcode", SqlDbType.VarChar).Value = FacilityGroup.zipcode;
                cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = FacilityGroup.phone;
                cmd.Parameters.Add("@contact_name", SqlDbType.VarChar).Value = FacilityGroup.contact_name;
                cmd.Parameters.Add("@contact_phone", SqlDbType.VarChar).Value = FacilityGroup.contact_phone;
                cmd.Parameters.Add("@contact_ext", SqlDbType.VarChar).Value = FacilityGroup.contact_ext;
                cmd.Parameters.Add("@contact_email", SqlDbType.VarChar).Value = FacilityGroup.contact_email;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (FacilityGroup.parent_id == 0)
                {
                    cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = FacilityGroup.parent_id;
                }

                cmd.Parameters.Add("@welcome_pg", SqlDbType.Text).Value = FacilityGroup.welcome_pg;
                cmd.Parameters.Add("@login_help", SqlDbType.VarChar).Value = FacilityGroup.login_help;
                cmd.Parameters.Add("@logo_img", SqlDbType.VarChar).Value = FacilityGroup.logo_img;
                cmd.Parameters.Add("@active", SqlDbType.Bit).Value = FacilityGroup.active;
                cmd.Parameters.Add("@support_email", SqlDbType.VarChar).Value = FacilityGroup.support_email;
                cmd.Parameters.Add("@feedback_email", SqlDbType.VarChar).Value = FacilityGroup.feedback_email;
                cmd.Parameters.Add("@default_password", SqlDbType.VarChar).Value = FacilityGroup.default_password;
                cmd.Parameters.Add("@forceinfocheck", SqlDbType.Bit).Value = FacilityGroup.forceinfocheck;
                if (FacilityGroup.salemanager_id == 0)
                {
                    cmd.Parameters.Add("@salemanager_id", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@salemanager_id", SqlDbType.Int).Value = FacilityGroup.salemanager_id;
                }
                if (FacilityGroup.accountmanager_id == 0)
                {
                    cmd.Parameters.Add("@accountmanager_id", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@accountmanager_id", SqlDbType.Int).Value = FacilityGroup.accountmanager_id;
                }
                cmd.Parameters.Add("@dataupload_ind", SqlDbType.Bit).Value = FacilityGroup.dataupload_ind;
                cmd.Parameters.Add("@datadownload_ind", SqlDbType.Bit).Value = FacilityGroup.datadownload_ind;
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        //Update the users' default password when the firstlogin_ind==1
        private void UpdateUserDefaultPW(int facid, string changedPassword)
        {
            FacilityGroupInfo facility = GetFacilityGroupByID(facid);
            string dpw = facility.default_password.Trim();

            if (changedPassword!=dpw)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_update_default_password",cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facid;
                    cmd.Parameters.Add("@defaultpassword",SqlDbType.VarChar).Value=changedPassword;

                    cn.Open();

                    cmd.ExecuteNonQuery();
                }

            }
            
             
        }

        /// <summary>
        /// Updates a FacilityGroup
        /// </summary>
        public bool UpdateFacilityGroup(FacilityGroupInfo FacilityGroup)
        {
            UpdateUserDefaultPW(FacilityGroup.FacID,FacilityGroup.default_password);
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {


                SqlCommand cmd = new SqlCommand("update FacilityGroup set " +
              "FacCode = @FacCode, " +
              "FacName = @FacName, " +
              "MaxUsers = @MaxUsers, " +
              "Started = @Started, " +
              "Expires = @Expires, " +
              "RepID = @RepID, " +
              "Comment = @Comment, " +
              "Compliance = @Compliance, " +
              "CustomTopics = @CustomTopics, " +
              "QuizBowl = @QuizBowl, " +
              "homeurl = @homeurl, " +
              "tagline = @tagline, " +
              "address1 = @address1, " +
              "address2 = @address2, " +
              "city = @city, " +
              "state = @state, " +
              "zipcode = @zipcode, " +
              "phone = @phone, " +
              "contact_name = @contact_name, " +
              "contact_phone = @contact_phone, " +
              "contact_ext = @contact_ext, " +
              "contact_email = @contact_email, " +
              "parent_id = @parent_id, " +
              "welcome_pg = @welcome_pg, " +
              "login_help = @login_help, " +
              "logo_img = @logo_img, " +
              "active = @active, " +
              "support_email = @support_email, " +
              "feedback_email = @feedback_email, " +
              "default_password = @default_password, " +
              "forceinfocheck = @forceinfocheck, " +
              "salemanager_id = @salemanager_id, " +
              "accountmanager_id = @accountmanager_id, " +
              "dataupload_ind = @dataupload_ind, " +
              "datadownload_ind = @datadownload_ind " +
              "where FacID = @FacID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FacCode", SqlDbType.VarChar).Value = FacilityGroup.FacCode;
                cmd.Parameters.Add("@FacName", SqlDbType.VarChar).Value = FacilityGroup.FacName;
                cmd.Parameters.Add("@MaxUsers", SqlDbType.Int).Value = FacilityGroup.MaxUsers;
                cmd.Parameters.Add("@Started", SqlDbType.DateTime).Value = FacilityGroup.Started;
                cmd.Parameters.Add("@Expires", SqlDbType.DateTime).Value = FacilityGroup.Expires;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (FacilityGroup.RepID == 0)
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = FacilityGroup.RepID;
                }

                cmd.Parameters.Add("@Comment", SqlDbType.Text).Value = FacilityGroup.Comment;
                cmd.Parameters.Add("@Compliance", SqlDbType.Bit).Value = FacilityGroup.Compliance;
                cmd.Parameters.Add("@CustomTopics", SqlDbType.Bit).Value = FacilityGroup.CustomTopics;
                cmd.Parameters.Add("@QuizBowl", SqlDbType.Bit).Value = FacilityGroup.QuizBowl;
                cmd.Parameters.Add("@homeurl", SqlDbType.VarChar).Value = FacilityGroup.homeurl;
                cmd.Parameters.Add("@tagline", SqlDbType.VarChar).Value = FacilityGroup.tagline;
                cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = FacilityGroup.address1;
                cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = FacilityGroup.address2;
                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = FacilityGroup.city;
                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = FacilityGroup.state;
                cmd.Parameters.Add("@zipcode", SqlDbType.VarChar).Value = FacilityGroup.zipcode;
                cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = FacilityGroup.phone;
                cmd.Parameters.Add("@contact_name", SqlDbType.VarChar).Value = FacilityGroup.contact_name;
                cmd.Parameters.Add("@contact_phone", SqlDbType.VarChar).Value = FacilityGroup.contact_phone;
                cmd.Parameters.Add("@contact_ext", SqlDbType.VarChar).Value = FacilityGroup.contact_ext;
                cmd.Parameters.Add("@contact_email", SqlDbType.VarChar).Value = FacilityGroup.contact_email;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (FacilityGroup.parent_id == 0)
                {
                    cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = FacilityGroup.parent_id;
                }

                cmd.Parameters.Add("@welcome_pg", SqlDbType.Text).Value = FacilityGroup.welcome_pg;
                cmd.Parameters.Add("@login_help", SqlDbType.VarChar).Value = FacilityGroup.login_help;
                cmd.Parameters.Add("@logo_img", SqlDbType.VarChar).Value = FacilityGroup.logo_img;
                cmd.Parameters.Add("@active", SqlDbType.Bit).Value = FacilityGroup.active;
                cmd.Parameters.Add("@support_email", SqlDbType.VarChar).Value = FacilityGroup.support_email;
                cmd.Parameters.Add("@feedback_email", SqlDbType.VarChar).Value = FacilityGroup.feedback_email;
                cmd.Parameters.Add("@default_password", SqlDbType.VarChar).Value = FacilityGroup.default_password;
                cmd.Parameters.Add("@forceinfocheck", SqlDbType.Bit).Value = FacilityGroup.forceinfocheck;
                if (FacilityGroup.salemanager_id == 0)
                {
                    cmd.Parameters.Add("@salemanager_id", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@salemanager_id", SqlDbType.Int).Value = FacilityGroup.salemanager_id;
                }
                if (FacilityGroup.accountmanager_id == 0)
                {
                    cmd.Parameters.Add("@accountmanager_id", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@accountmanager_id", SqlDbType.Int).Value = FacilityGroup.accountmanager_id;
                }
                cmd.Parameters.Add("@dataupload_ind", SqlDbType.Bit).Value = FacilityGroup.dataupload_ind;
                cmd.Parameters.Add("@datadownload_ind", SqlDbType.Bit).Value = FacilityGroup.datadownload_ind;
                cmd.Parameters.Add("@FacID", SqlDbType.Int).Value = FacilityGroup.FacID;


                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
               
                return (ret == 1);
            }
        }

        //edit by chuan
        #region banner
        //almost the same as Function "GetFacilityGroupsExceptGiftCard", but adds the condition "parent_id <> BANNERPARENTID" in the SQL Query
        public List<FacilityGroupInfo> GetFacilityGroupsExceptGiftCardBanner(int bannerPID, string cSortExpression)
        {
            //insert a row to the table facilityGroup with the facName "Banner Health"
            //insert into facilitygroup(facname, faccode, maxusers, active, started, expires, compliance, quizbowl, comment) 
            //values ('Banner Health', 0, 0, 0, 0, 0, 0, 0, 0)

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //select * from facilitygroup where (parent_id is null or parent_id<>26 and parent_id not in (select parent_id from parentorganization where parent_name ='CE Gift Card')) and facid<>2 and active=1 or (active=0 and forceinfocheck=1)
                string cSQLCommand = "select * from facilitygroup " +
                    "where (parent_id is null or parent_id<>@bannerpid and parent_id not in (select parent_id from parentorganization where parent_name ='CE Gift Card')) " +
                    "and facid<>2 and active=1 or (active=0 and forceinfocheck=1) or (facname='Banner Health' and active=0)";

                //add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@bannerpid", SqlDbType.Int).Value = bannerPID; //banner parent id constant
                cn.Open();
                return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        ///<summary>
        ///Random facilities
        ///</summary>      
        public List<FacilityGroupInfo> GetRandomFacilityGroupsExceptGiftCard()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top 9 * " +
                    "from facilitygroup where (parent_id is null or " +
                "parent_id<>21) and facid>4 and (active=1 or (active=0 and forceinfocheck=1)) order by newid()";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        //try to find the facility called "Banner Health"
        public int GetBannerFacilityID()
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            string cSQLCommand =
                "select facid from facilitygroup where facname='Banner Health' and active=0";

            SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
            cn.Open();
            IDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
                return (int)reader["facid"];
            else
                return -1;
        }

        public int GetBannerFacilityIDFromUsername(string username, int bannerPID)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            //select top 1 facilityid from useraccount where cusername='nsadmin' and facilityid in (select facid from facilitygroup where parent_id = 26)
            //string cSQLCommand = "select top 1 facilityid from useraccount where cusername=@username and facilityid in (select facid from facilitygroup where parent_id = @bannerPID)";
            //select top 1 facilityid from useraccount ua join facilitygroup fg on ua.facilityid=fg.facid where ua.cusername='nsadmin' and fg.parent_id=26 order by ua.facilityid
            string cSQLCommand = "select top 1 facilityid from useraccount ua join facilitygroup fg on ua.facilityid=fg.facid where ua.cusername=@username and fg.parent_id=@bannerPID order by ua.facilityid";

            SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
            cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = username;
            cmd.Parameters.Add("@bannerpid", SqlDbType.Int).Value = bannerPID; //banner parent id constant
            cn.Open();
            IDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
                return (int)reader["facilityid"];
            else
                return -1;
        }

        public int GetBannerFacilityIDFromUserNamePassWD(string username, string passwd, int bannerPID)
        {
            SqlConnection cn = new SqlConnection(this.ConnectionString);
            //select * from useraccount ua join facilitygroup fg on ua.facilityid=fg.facid where ua.cusername='nsadmin' and ua.cpw='G#Dr0cks' and fg.parent_id=26 order by ua.facilityid
            string cSQLCommand =
                "select facilityid from useraccount ua join facilitygroup fg on ua.facilityid=fg.facid where ua.cusername=@username and ua.cpw=@passwd and fg.parent_id=@bannerpid order by ua.facilityid";

            SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
            cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = username;
            cmd.Parameters.Add("@passwd", SqlDbType.VarChar).Value = passwd;
            cmd.Parameters.Add("@bannerpid", SqlDbType.Int).Value = bannerPID; //banner parent id constant
            cn.Open();
            IDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
                return (int)reader["facilityid"];
            else
                return -1;
        }

        /// <summary>
        /// Retrieves the FacilityGroup with the username
        /// </summary>
        public List<FacilityGroupInfo> GetFacilityGroupByUserName(string username)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * from facilitygroup fg join useraccount ua on " +
                    "ua.facilityid=fg.facid where (fg.parent_id is null or " +
                "fg.parent_id<>21) and fg.facid>4 and (fg.active=1 or (fg.active=0 and fg.forceinfocheck=1)) and ua.cusername=@username", cn);
                cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = username;
                cn.Open();
                return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        #endregion

        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new FacilityGroupInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual FacilityGroupInfo GetFacilityGroupFromReader(IDataReader reader)
        {
            return GetFacilityGroupFromReader(reader, true);
        }
        protected virtual FacilityGroupInfo GetFacilityGroupFromReader(IDataReader reader, bool readMemos)
        {
            FacilityGroupInfo FacilityGroup = new FacilityGroupInfo(
              (int)reader["FacID"],
              reader["FacCode"].ToString(),
              reader["FacName"].ToString(),
              (int)reader["MaxUsers"],
              (DateTime)reader["Started"],
              (DateTime)reader["Expires"],
              (int)(Convert.IsDBNull(reader["RepID"]) ? (int)0 : (int)reader["RepID"]),
              reader["Comment"].ToString(),
              (bool)reader["Compliance"],
              (bool)reader["CustomTopics"],
              (bool)reader["QuizBowl"],
              reader["homeurl"].ToString(),
              reader["tagline"].ToString(),
              reader["address1"].ToString(),
              reader["address2"].ToString(),
              reader["city"].ToString(),
              reader["state"].ToString(),
              reader["zipcode"].ToString(),
              reader["phone"].ToString(),
              reader["contact_name"].ToString(),
              reader["contact_phone"].ToString(),
              reader["contact_ext"].ToString(),
              reader["contact_email"].ToString(),
              (int)(Convert.IsDBNull(reader["parent_id"]) ? (int)0 : (int)reader["parent_id"]),
              reader["welcome_pg"].ToString(),
              reader["login_help"].ToString(),
              reader["logo_img"].ToString(),
              (bool)reader["active"],
              reader["support_email"].ToString(),
              reader["feedback_email"].ToString(),
              reader["default_password"].ToString(),
              (bool)reader["forceinfocheck"],
              (int)(Convert.IsDBNull(reader["salemanager_id"]) ? (int)0 : (int)reader["salemanager_id"]),
              (int)(Convert.IsDBNull(reader["accountmanager_id"]) ? (int)0 : (int)reader["accountmanager_id"]),
              (reader["dataupload_ind"] != DBNull.Value ? (bool)reader["dataupload_ind"] : false),
              (reader["datadownload_ind"] != DBNull.Value ? (bool)reader["datadownload_ind"] : false));

            return FacilityGroup;
        }

        /// <summary>
        /// Returns a collection of FacilityGroupInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<FacilityGroupInfo> GetFacilityGroupCollectionFromReader(IDataReader reader)
        {
            return GetFacilityGroupCollectionFromReader(reader, true);
        }
        protected virtual List<FacilityGroupInfo> GetFacilityGroupCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<FacilityGroupInfo> FacilityGroups = new List<FacilityGroupInfo>();
            while (reader.Read())
                FacilityGroups.Add(GetFacilityGroupFromReader(reader, readMemos));
            return FacilityGroups;
        }
        #endregion
    }
}
#endregion