﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region DocumentLinkInfo

namespace PearlsReview.DAL
{
    public class DocumentLinkInfo
    {
        public DocumentLinkInfo() { }

        public DocumentLinkInfo(int DocumentLinkID, int DocumentID, int RelationID, string RelationType)
        {
            this.DocumentLinkID = DocumentLinkID;
            this.DocumentID = DocumentID;
            this.RelationID = RelationID;
            this.RelationType = RelationType;
        }

        private int _DocumentLinkID = 0;
        public int DocumentLinkID
        {
            get { return _DocumentLinkID; }
            protected set { _DocumentLinkID = value; }
        }

        private int _DocumentID = 0;
        public int DocumentID
        {
            get { return _DocumentID; }
            private set { _DocumentID = value; }
        }

        private int _RelationID = 0;
        public int RelationID
        {
            get { return _RelationID; }
            private set { _RelationID = value; }
        }

        private string _RelationType = "";
        public string RelationType
        {
            get { return _RelationType; }
            private set { _RelationType = value; }
        }

    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with DocumentLinks

        /// <summary>
        /// Returns the total number of DocumentLinks
        /// </summary>
        public  int GetDocumentLinkCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from DocumentLink", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of DocumentLinks for a DocumentID
        /// </summary>
        public  int GetDocumentLinkCountByDocumentID(int DocumentID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from DocumentLink where DocumentID=@DocumentID", cn);
                cmd.Parameters.Add("@DocumentID", SqlDbType.Int).Value = DocumentID;

                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all DocumentLinks
        /// </summary>
        public  List<DocumentLinkInfo> GetDocumentLinks(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from DocumentLink";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetDocumentLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<DocumentLinkInfo> GetDocumentsLinksByTopicId(int ceTopicId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from DocumentLink where RelationType='T' and RelationID=" + ceTopicId.ToString();

                // add on ORDER BY if provided
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetDocumentLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
 
        }

        /// <summary>
        /// Retrieves the DocumentLink with the specified ID
        /// </summary>
        public  DocumentLinkInfo GetDocumentLinkByID(int DocumentLinkID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from DocumentLink where DocumentLinkID=@DocumentLinkID ", cn);
                cmd.Parameters.Add("@DocumentLinkID", SqlDbType.Int).Value = DocumentLinkID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetDocumentLinkFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a DocumentLink
        /// </summary>
        public  bool DeleteDocumentLink(int DocumentLinkID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from DocumentLink where DocumentLinkID=@DocumentLinkID", cn);
                cmd.Parameters.Add("@DocumentLinkID", SqlDbType.Int).Value = DocumentLinkID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new DocumentLink
        /// </summary>
        public  int InsertDocumentLink(DocumentLinkInfo DocumentLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into DocumentLink " +
              "(DocumentID, " +
              "RelationID, " +
              "RelationType) " +
              "VALUES (" +
              "@DocumentID, " +
              "@RelationID, " +
              "@RelationType) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@DocumentID", SqlDbType.Int).Value = DocumentLink.DocumentID;
                cmd.Parameters.Add("@RelationID", SqlDbType.Int).Value = DocumentLink.RelationID;
                cmd.Parameters.Add("@RelationType", SqlDbType.Char).Value = DocumentLink.RelationType;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a DocumentLink
        /// </summary>
        public  bool UpdateDocumentLink(DocumentLinkInfo DocumentLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update DocumentLink set " +
              "DocumentID = @DocumentID, " +
              "RelationID = @RelationID, " +
              "RelationType = @RelationType " +
              "where DocumentLinkID = @DocumentLinkID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@DocumentID", SqlDbType.Int).Value = DocumentLink.DocumentID;
                cmd.Parameters.Add("@RelationID", SqlDbType.Int).Value = DocumentLink.RelationID;
                cmd.Parameters.Add("@RelationType", SqlDbType.Char).Value = DocumentLink.RelationType;
                cmd.Parameters.Add("@DocumentLinkID", SqlDbType.Int).Value = DocumentLink.DocumentLinkID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new DocumentLinkInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual DocumentLinkInfo GetDocumentLinkFromReader(IDataReader reader)
        {
            return GetDocumentLinkFromReader(reader, true);
        }
        protected virtual DocumentLinkInfo GetDocumentLinkFromReader(IDataReader reader, bool readMemos)
        {
            DocumentLinkInfo DocumentLink = new DocumentLinkInfo(
              (int)reader["DocumentLinkID"],
              (int)reader["DocumentID"],
              (int)reader["RelationID"],
              reader["RelationType"].ToString());


            return DocumentLink;
        }

        /// <summary>
        /// Returns a collection of DocumentLinkInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<DocumentLinkInfo> GetDocumentLinkCollectionFromReader(IDataReader reader)
        {
            return GetDocumentLinkCollectionFromReader(reader, true);
        }
        protected virtual List<DocumentLinkInfo> GetDocumentLinkCollectionFromReader(IDataReader reader, bool  readMemos)
        {
            List<DocumentLinkInfo> DocumentLinks = new List<DocumentLinkInfo>();
            while (reader.Read())
                DocumentLinks.Add(GetDocumentLinkFromReader(reader, readMemos));
            return DocumentLinks;
        }
        #endregion
    }
}
#endregion