﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region DiscountInfo

namespace PearlsReview.DAL
{

    /// <summary> 
    /// Summary description for DiscountInfo
    /// Bhaskar N
    /// </summary>
    public class DiscountInfo
    {
        public DiscountInfo() { }


        public DiscountInfo(int discountid, int facilityid, int topicid, string course_number, string coursename, string tagline, DateTime startdate, DateTime enddate, decimal discount_price,
            string discount_type, string sponsor_text, string free_html, string img_file, string discount_url, bool survey_ind, int impression,
            int clickthrough, string username, string userpassword, bool section_ind, decimal offline_price,int InitID, bool virtualurl_ind, string sponsor_ad_html)
        {
            this.DiscountId = discountid;
            this.FacilityId = facilityid;
            this.TopicId = topicid;
            this.course_number = course_number;
            this.coursename = coursename;
            this.TagLine = tagline;
            this.StartDate = startdate;
            this.EndDate = enddate;
            this.discount_price = discount_price;
            this.discount_type = discount_type;
            this.Sponsor_Text = sponsor_text;
            this.Free_Html = free_html;
            this.Img_File = img_file;
            this.Discount_Url = discount_url;
            this.Survey_Ind = survey_ind;
            this.Impression = impression;
            this.ClickThrough = clickthrough;
            this.UserName = username;
            this.UserPassword = userpassword;
            this.Section_Ind = section_ind;
            this.Offline_Price = offline_price;
            this.Virtualurl_Ind = virtualurl_ind;
            this.InitID = InitID;
            this.Sponsor_Ad_Html = sponsor_ad_html;
        }

        private int _discountid = 0;
        public int DiscountId
        {
            get { return _discountid; }
            protected set { _discountid = value; }
        }
        private int _facilityid = 0;
        public int FacilityId
        {
            get { return _facilityid; }
            protected set { _facilityid = value; }
        }
        private int _topicid = 0;
        public int TopicId
        {
            get { return _topicid; }
            protected set { _topicid = value; }
        }

        private string _course_number = "";
        public string course_number
        {
            get { return _course_number; }
            private set { _course_number = value; }
        }
        private string _coursename = "";
        public string coursename
        {
            get { return _coursename; }
            private set { _coursename = value; }
        }
        private string _tagline = "";
        public string TagLine
        {
            get { return _tagline; }
            private set { _tagline = value; }
        }

        private DateTime _startdate = System.DateTime.Now;
        public DateTime StartDate
        {
            get { return _startdate; }
            private set { _startdate = value; }
        }

        private DateTime _enddate = System.DateTime.Now;
        public DateTime EndDate
        {
            get { return _enddate; }
            private set { _enddate = value; }
        }


        private decimal _discount_price = 0.00M;
        public decimal discount_price
        {
            get { return _discount_price; }
            private set { _discount_price = value; }
        }

        private string _discount_type = "";
        public string discount_type
        {
            get { return _discount_type; }
            private set { _discount_type = value; }
        }

        private string _sponsor_text = "";
        public string Sponsor_Text
        {
            get { return _sponsor_text; }
            private set { _sponsor_text = value; }
        }

        private string _free_html = "";
        public string Free_Html
        {
            get { return _free_html; }
            private set { _free_html = value; }
        }


        private string _img_file = "";
        public string Img_File
        {
            get { return _img_file; }
            private set { _img_file = value; }
        }

        private string _discount_url = "";
        public string Discount_Url
        {
            get { return _discount_url; }
            private set { _discount_url = value; }
        }

        private bool _survey_ind = true;
        public bool Survey_Ind
        {
            get { return _survey_ind; }
            private set { _survey_ind = value; }
        }

        private int _impression = 0;
        public int Impression
        {
            get { return _impression; }
            protected set { _impression = value; }
        }

         int _clickthrough = 0;
        public int ClickThrough
        {
            get { return _clickthrough; }
             set { _clickthrough = value; }
        }

        private string _username = "";
        public string UserName
        {
            get { return _username; }
            private set { _username = value; }
        }

        private string _userpassword = "";
        public string UserPassword
        {
            get { return _userpassword; }
            private set { _userpassword = value; }
        }


        private bool _section_ind = true;
        public bool Section_Ind
        {
            get { return _section_ind; }
            private set { _section_ind = value; }
        }


        //decimal(6, 2)..promotionsifo
        private decimal _offline_price = 0.00M;
        public decimal Offline_Price
        {
            get { return _offline_price; }
            private set { _offline_price = value; }
        }

        private bool _virtualurl_ind = true;
        public bool Virtualurl_Ind
        {
            get { return _virtualurl_ind; }
            private set { _virtualurl_ind = value; }
        }
        private int _initid = 0;
        public int InitID
        {
            get { return _initid; }
            private set { _initid = value; }
        }
        private string _sponsor_ad_html = "";
        public string Sponsor_Ad_Html
        {
            get { return _sponsor_ad_html; }
            private set { _sponsor_ad_html = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider  

        /////////////////////////////////////////////////////////
        // Methods that work with Discount
        // Bhaskar N

        /// <summary>
        /// Retrieves all Discounts
        /// </summary>
        public  List<DiscountInfo> GetDiscounts(string cSortExpression, int Active)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {                
                string cSQLCommand;
                if (Active == 1)
                {
                    cSQLCommand = "SELECT Discount.* , Topic.course_number, Topic.topicname " +
                      "FROM Discount LEFT OUTER JOIN Topic ON Topic.Topicid = Discount.Topicid " +
                      "WHERE  Discount.StartDate <= '" + System.DateTime.Now + "' AND Discount.EndDate >= '" + System.DateTime.Now + "'";
                }
                else
                {
                    cSQLCommand = "SELECT Discount.* , Topic.course_number, Topic.topicname " +
                         "FROM Discount LEFT OUTER JOIN Topic ON Topic.Topicid = Discount.Topicid ";
                }


                //Discount.virtualurl_ind = 1 or Discount.section_ind = 1  ";
                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetDiscountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Discount with the specified ID
        /// </summary>
        public  DiscountInfo GetDiscountByID(int DiscountId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                //SqlCommand cmd = new SqlCommand("select * " +
                //        "from Discount where discountid=@DiscountId", cn);

                SqlCommand cmd = new SqlCommand("SELECT Discount.* , Topic.course_number, Topic.topicname " +
                     "FROM Discount LEFT OUTER JOIN Topic ON Topic.Topicid = Discount.Topicid" +
                     " where Discount.discountid=@DiscountId ", cn);

                cmd.Parameters.Add("@DiscountId", SqlDbType.Int).Value = DiscountId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetDiscountFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the Discount with the specified ID
        /// </summary>
        public  DiscountInfo GetDiscountByUserNameAndPassword(string UserName, string UserPassword)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT Discount.* , Topic.course_number, Topic.topicname " +
                     "FROM Discount LEFT OUTER JOIN Topic ON Topic.Topicid = Discount.Topicid" +
                     " WHERE Discount.UserName=@UserName AND Discount.UserPassword=@UserPassword  ", cn);

                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName;
                cmd.Parameters.Add("@UserPassword", SqlDbType.VarChar).Value = UserPassword;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetDiscountFromReader(reader, true);
                else
                    return null;
            }
        }

        // To work with microsite domainId, where the facilityid >2

        public DiscountInfo GetDiscountByTopicIdAndMicroSiteId(int TopicId, int MicroSiteId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd;
                if (MicroSiteId < 20)
                {
                    cmd = new SqlCommand("SELECT Discount.* , Topic.course_number, Topic.topicname " +
                "from discount LEFT OUTER JOIN Topic ON Topic.TopicId=Discount.TopicId" +
                " where Discount.TopicId=@TopicId and discount.facilityid > 2  and Discount.StartDate <= '" + System.DateTime.Now + "' AND Discount.EndDate >= '" + System.DateTime.Now + "'", cn);
                }
                else
                {
                    cmd = new SqlCommand("SELECT Discount.* , Topic.course_number, Topic.topicname " +
                    "from discount LEFT OUTER JOIN Topic ON Topic.TopicId=Discount.TopicId " +
                    " where Discount.TopicId=@TopicId and discount.facilityid=@facilityId  and Discount.StartDate <= '" + System.DateTime.Now + "' AND Discount.EndDate >= '" + System.DateTime.Now + "'", cn);
                }
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cmd.Parameters.Add("@facilityId", SqlDbType.Int).Value = MicroSiteId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetDiscountFromReader(reader, true);
                else
                    return null;
            }

        }

        /// <summary>
        /// Retrieves the Discount with the specified TopicID
        /// </summary>
        public  System.Data.DataSet GetDiscountByTopicID(string TopicID, int UrlIdentity)
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                string cSQLCommand = "select * from Discount where (topicid = " + TopicID + ") ";
                if (UrlIdentity == 1)
                {
                    cSQLCommand = cSQLCommand + " and section_ind= 1 ";
                }
                else if (UrlIdentity == 2)
                {
                    cSQLCommand = cSQLCommand + "  and virtualurl_ind= 1 ";
                }
                //cSQLCommand = cSQLCommand + " and enddate >= '" + EndDate + "'";
                cSQLCommand = cSQLCommand + " and StartDate <= '" + System.DateTime.Now + "'";
                cSQLCommand = cSQLCommand + " and enddate >= '" + System.DateTime.Now + "'";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                //cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "sponsor_text");
            }
            return ds;
        }

        /// <summary>
        /// Retrieves the Discount with the specified SponsorID
        /// </summary>
        public DataTable GetDiscountBySponsorID(string SponsorID, string cSortExpression)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = String.Format("select " +
                    "DiscountID, Topic.facilityid, topicname, Tagline, " +
                    " convert(varchar(25),Topic.course_number,101)+' - '+ convert(varchar(10),StartDate,101)+' to '+ convert(varchar(10),EndDate,101)+' - '+ topicname As DiscountText " +
                    " FROM Discount LEFT OUTER JOIN Topic ON Topic.Topicid = Discount.Topicid " +
                    " where initid in (select distinct initid from SponsorList where sponsorid in ({0}))", SponsorID);

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                //cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(dt);
            }
            return dt;
        }
        public System.Data.DataSet GetDiscountByInitiativeIDs(string initIDs, string cSortExpression)
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = String.Format("select " +
                    "DiscountID, Topic.facilityid, topicname, Tagline, " +
                    " convert(varchar(25),Topic.course_number,101)+' - '+ convert(varchar(10),StartDate,101)+' to '+ convert(varchar(10),EndDate,101)+' - '+ topicname As DiscountText " +
                    " FROM Discount LEFT OUTER JOIN Topic ON Topic.Topicid = Discount.Topicid " +
                    " where initid in ({0})", initIDs);

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                //cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "Discount_Text");
            }
            return ds;
        }

        public void UpdateUCEIndicatorByTopicid(bool hide, int topicid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
           {
               string cSQLCommand = "update discount set hideuce_ind=@hide where topicid=@topicid";
               SqlCommand cmd = new SqlCommand(cSQLCommand,cn);
               cmd.Parameters.Add("@hide", SqlDbType.Bit).Value = hide;
               cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
               cn.Open();
               cmd.ExecuteNonQuery();
           }
        }

        /// <summary>
        /// Retrieves all Discounts
        /// </summary>
        public List<DiscountInfo> GetSponsorDiscounts(string cSortExpression, int SponsorID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "SELECT Discount.* , Topic.course_number, Topic.topicname " +
                     "FROM Discount LEFT OUTER JOIN Topic ON Topic.Topicid = Discount.Topicid " +
                   " where Discount.DiscountID in (select discountid from sponsorlist where SponsorID= " + SponsorID + ")";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetDiscountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves all Discounts
        /// </summary>
        public List<DiscountInfo> GetSponsorDiscountsByInitiativeID(string cSortExpression, int InitID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = String.Format("SELECT Discount.* , Topic.course_number, Topic.topicname " +
                     "FROM Discount LEFT OUTER JOIN Topic ON Topic.Topicid = Discount.Topicid " +
                   " where Discount.initid = {0}", InitID);

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetDiscountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Inserts a new Discount
        /// </summary>
        public  int InsertDiscount(DiscountInfo Discount)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Discount " +
                  "(topicid , " +
                  "facilityid, " +
                  "tagline, " +
                  "startdate , " +
                  "enddate, " +
                  "discount_price, " +
                  "discount_type , " +
                  "sponsor_text , " +
                  "free_html , " +
                  "img_file, " +
                  "discount_url, " +
                  "survey_ind, " +
                  "impression, " +
                  "clickthrough , " +
                  "username, " +
                  "userpassword,  " +
                  "section_ind, " +
                  "offline_price, " +
                  "initid, " +
                  "sponsor_ad_html, " +
                  "virtualurl_ind ) " +
                  "VALUES (" +
                  "@TopicId, " +
                  "@facilityid, " +
                  "@TagLine, " +
                  "@StartDate, " +
                  "@EndDate, " +
                  "@discount_price, " +
                  "@discount_type, " +
                  "@sponsor_text , " +
                  "@free_html , " +
                  "@Img_File, " +
                  "@Discount_Url, " +
                  "@Survey_Ind, " +
                  "@Impression, " +
                  "@ClickThrough, " +
                  "@UserName, " +
                  "@UserPassword, " +
                  "@Section_Ind, " +
                  "@Offline_Price, "+
                  "@initid, " +
                  "@Sponsor_Ad_Html, " +
                  "@Virtualurl_Ind ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = Discount.TopicId;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = Discount.FacilityId;
                cmd.Parameters.Add("@TagLine", SqlDbType.VarChar).Value = Discount.TagLine;
                cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = Discount.StartDate;
                cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = Discount.EndDate;
                cmd.Parameters.Add("@discount_price", SqlDbType.Decimal).Value = Discount.discount_price;
                cmd.Parameters.Add("@discount_type", SqlDbType.VarChar).Value = Discount.discount_type;
                cmd.Parameters.Add("@sponsor_text", SqlDbType.VarChar).Value = Discount.Sponsor_Text;
                cmd.Parameters.Add("@free_html", SqlDbType.VarChar).Value = Discount.Free_Html;
                cmd.Parameters.Add("@Img_File", SqlDbType.VarChar).Value = Discount.Img_File;
                cmd.Parameters.Add("@Discount_Url", SqlDbType.VarChar).Value = Discount.Discount_Url;
                cmd.Parameters.Add("@Survey_Ind", SqlDbType.Bit).Value = Discount.Survey_Ind;
                cmd.Parameters.Add("@Impression", SqlDbType.Int).Value = Discount.Impression;
                cmd.Parameters.Add("@ClickThrough", SqlDbType.Int).Value = Discount.ClickThrough;
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Discount.UserName;
                cmd.Parameters.Add("@UserPassword", SqlDbType.VarChar).Value = Discount.UserPassword;
                cmd.Parameters.Add("@Section_Ind", SqlDbType.Bit).Value = Discount.Section_Ind;
                cmd.Parameters.Add("@Offline_Price", SqlDbType.VarChar).Value = Discount.Offline_Price;
                cmd.Parameters.Add("@initid", SqlDbType.Int).Value = Discount.InitID;
                cmd.Parameters.Add("@Sponsor_Ad_Html", SqlDbType.VarChar).Value = Discount.Sponsor_Ad_Html;
                cmd.Parameters.Add("@Virtualurl_Ind", SqlDbType.Bit).Value = Discount.Virtualurl_Ind;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }


        /// <summary>
        /// Updates a Discount
        /// </summary>
        public  bool UpdateDiscount(DiscountInfo Discount)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Discount set " +
              "topicid = @TopicId, " +
              "facilityid = @facilityid, " +
              "tagline = @TagLine, " +
              "startdate = @StartDate, " +
              "enddate = @EndDate, " +
              "discount_price = @discount_price, " +
              "discount_type = @discount_type, " +
              "sponsor_text = @Sponsor_Text, " +
              "free_html = @Free_Html, " +
              "img_file = @Img_File, " +
              "discount_url = @Discount_Url, " +
              "survey_ind = @Survey_Ind, " +
              "impression = @Impression, " +
              "clickthrough = @ClickThrough, " +
              "username = @UserName, " +
              "userpassword = @UserPassword, " +
              "section_ind = @Section_Ind, " +
              "offline_price = @Offline_Price, " +
              "initid = @initid, " +
              "sponsor_ad_html = @Sponsor_Ad_Html, " +
              "virtualurl_ind = @Virtualurl_Ind " +
              "where discountid = @DiscountId ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = Discount.FacilityId;
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = Discount.TopicId;
                cmd.Parameters.Add("@TagLine", SqlDbType.VarChar).Value = Discount.TagLine;
                cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = Discount.StartDate;
                cmd.Parameters.Add("@EndDate", SqlDbType.DateTime).Value = Discount.EndDate;
                cmd.Parameters.Add("@discount_price", SqlDbType.Decimal).Value = Discount.discount_price;
                cmd.Parameters.Add("@discount_type", SqlDbType.VarChar).Value = Discount.discount_type;
                cmd.Parameters.Add("@sponsor_text", SqlDbType.VarChar).Value = Discount.Sponsor_Text;
                cmd.Parameters.Add("@free_html", SqlDbType.VarChar).Value = Discount.Free_Html;
                cmd.Parameters.Add("@Img_File", SqlDbType.VarChar).Value = Discount.Img_File;
                cmd.Parameters.Add("@Discount_Url", SqlDbType.VarChar).Value = Discount.Discount_Url;
                cmd.Parameters.Add("@Survey_Ind", SqlDbType.Bit).Value = Discount.Survey_Ind;
                cmd.Parameters.Add("@Impression", SqlDbType.Int).Value = Discount.Impression;
                cmd.Parameters.Add("@ClickThrough", SqlDbType.Int).Value = Discount.ClickThrough;
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Discount.UserName;
                cmd.Parameters.Add("@UserPassword", SqlDbType.VarChar).Value = Discount.UserPassword;
                cmd.Parameters.Add("@Section_Ind", SqlDbType.Bit).Value = Discount.Section_Ind;
                cmd.Parameters.Add("@Offline_Price", SqlDbType.Decimal).Value = Discount.Offline_Price;
                cmd.Parameters.Add("@initid", SqlDbType.Int).Value = Discount.InitID;
                cmd.Parameters.Add("@Virtualurl_Ind", SqlDbType.Bit).Value = Discount.Virtualurl_Ind;
                cmd.Parameters.Add("@Sponsor_Ad_Html", SqlDbType.VarChar).Value = Discount.Sponsor_Ad_Html;
                cmd.Parameters.Add("@DiscountId", SqlDbType.Int).Value = Discount.DiscountId;


                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a Discount
        /// </summary>
        public  bool DeleteDiscount(int DiscountId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Discount where discountid=@DiscountId", cn);
                cmd.Parameters.Add("@DiscountId", SqlDbType.Int).Value = DiscountId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public bool UpdateInitiativeField(int DiscountId, int InitID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Discount set initid = @initid where discountid=@DiscountId", cn);
                cmd.Parameters.Add("@initid", SqlDbType.Int).Value = InitID;
                cmd.Parameters.Add("@DiscountId", SqlDbType.Int).Value = DiscountId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        /// <summary>
        /// Updates a Discount
        /// </summary>
        public  bool UpdateDiscountByImpression(int Discountid, int Impression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update discount set " +
                "impression = @impression where discountid = @discountid ", cn);

                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = Discountid;
                cmd.Parameters.Add("@impression", SqlDbType.Int).Value = Impression;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Updates a Discount
        /// </summary>
        public  bool UpdateDiscountByClickThrough(int Discountid, int ClickThrough)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update discount set " +
                "clickthrough = @clickthrough where discountid = @discountid ", cn);

                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = Discountid;
                cmd.Parameters.Add("@clickthrough", SqlDbType.Int).Value = ClickThrough;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Retrieves the DiscountID with the specified TopicID
        /// </summary>
        public  int GetDiscountIdByTopicId(int TopicId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                //SqlCommand cmd = new SqlCommand("select * " +
                //        "from Discount where discountid=@DiscountId", cn);

                SqlCommand cmd = new SqlCommand("SELECT discountid from discount" +
                     " where topicid=@TopicId ", cn);

                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return (int)reader["discountId"];
                else
                    return 0;
            }
        }



        public Boolean GetUCEIndcatorByTopicID(int Discountid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                //SqlCommand cmd = new SqlCommand("select * " +
                //        "from Discount where discountid=@DiscountId", cn);

                SqlCommand cmd = new SqlCommand("SELECT hideuce_ind from discount" +
                     " where discountid=@Discountid ", cn);

                cmd.Parameters.Add("@Discountid", SqlDbType.Int).Value = Discountid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    String ind = reader["hideuce_ind"].ToString();
                    if (ind=="True" )
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
                
            }
        }



        
        /// <summary>
        /// Retrieves the Discount with the specified TopicID
        /// </summary>
        public  System.Data.DataSet GetDiscountByTopicID(string TopicID, int UrlIdentity, string UserName)
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                string cSQLCommand = "select * from Discount where (topicid = " + TopicID + ") ";
                if (UrlIdentity == 1)
                {
                    //cSQLCommand = cSQLCommand + " and virtualurl_ind= 0 ";
                    cSQLCommand = cSQLCommand + " and section_ind = 1 ";
                }
                else if (UrlIdentity == 2)
                {
                    cSQLCommand = cSQLCommand + "  and virtualurl_ind= 1 ";

                    if (UserName == string.Empty)
                    {

                    }
                    else
                    {
                    cSQLCommand = cSQLCommand + " and  username = '" + UserName + "'";
                    }
                }
                //cSQLCommand = cSQLCommand + " and enddate >= '" + EndDate + "'";
                cSQLCommand = cSQLCommand + " and StartDate <= '" + System.DateTime.Now + "'";
                cSQLCommand = cSQLCommand + " and enddate >= '" + System.DateTime.Now + "'";


                //else if (UrlIdentity == 2)
                //{
                //    cSQLCommand = cSQLCommand + " and section_ind= 1 " ;
                //}

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                //cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "sponsor_text");
            }
            return ds;
        }


        public int CheckDiscountWithTopicId(int discountId, int TopicId, bool section_ind, bool virtualurl_ind)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select count(*) from Discount where topicid = " + TopicId + " and discountid = " + discountId +
                    " and StartDate <= '" + System.DateTime.Now + "' and enddate >= '" + System.DateTime.Now + "' ";

                if (section_ind == true)
                {
                    cSQLCommand = cSQLCommand + " and section_ind = '" + section_ind + "'";
                }

                if (virtualurl_ind == true)
                {
                    cSQLCommand = cSQLCommand + " and virtualurl_ind = '" + virtualurl_ind + "'";
                }
                SqlCommand cmd = new SqlCommand(cSQLCommand,cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                if (reader.Read())
                    return (int)reader[0];
                else
                    return 0;
              
            }
        }


        #endregion

        #region PRProvider 
        /////////////////////////////////////////////////////////
        // methods that work with Discount  

        /// <summary>
        /// Returns a new DiscountInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual DiscountInfo GetDiscountFromReader(IDataReader reader)
        {
            return GetDiscountFromReader(reader, true);
        }
        protected virtual DiscountInfo GetDiscountFromReader(IDataReader reader, bool readMemos)
        {
            int initid = 0;
            if (!String.IsNullOrEmpty(reader["initid"].ToString()))
                initid = (int)reader["initid"];
            DiscountInfo Discount = new DiscountInfo(
                  (int)reader["discountid"],
                  (int)reader["facilityid"],
                  (int)reader["topicid"],
                  reader["course_number"].ToString(),
                  reader["topicname"].ToString(),
                  reader["tagline"].ToString(),
                  (DateTime)reader["startdate"],
                  (DateTime)reader["enddate"],
                  (decimal)reader["discount_price"],
                  reader["discount_type"].ToString(),
                  reader["sponsor_text"].ToString(),
                  reader["free_html"].ToString(),
                  reader["img_file"].ToString(),
                  reader["discount_url"].ToString(),
                  (bool)reader["survey_ind"],
                  (int)reader["impression"],
                  (int)reader["clickthrough"],
                  reader["username"].ToString(),
                  reader["userpassword"].ToString(),
                  (bool)reader["section_ind"],
                  (decimal)reader["offline_price"],
                  initid,
                  (bool)reader["virtualurl_ind"],
                  reader["sponsor_ad_html"].ToString());
            return Discount;
        }


        /// <summary>
        /// Returns a collection of DiscountInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<DiscountInfo> GetDiscountCollectionFromReader(IDataReader reader)
        {
            return GetDiscountCollectionFromReader(reader, true);
        }
        protected virtual List<DiscountInfo> GetDiscountCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<DiscountInfo> Discounts = new List<DiscountInfo>();
            while (reader.Read())
                Discounts.Add(GetDiscountFromReader(reader, readMemos));
            return Discounts;
        }

        #endregion
       
    }
}
#endregion
