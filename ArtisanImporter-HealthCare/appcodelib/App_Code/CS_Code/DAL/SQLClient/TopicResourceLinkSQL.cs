﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region TopicResourceLinkInfo

namespace PearlsReview.DAL
{
    public class TopicResourceLinkInfo
    {
        public TopicResourceLinkInfo() { }

        public TopicResourceLinkInfo(int TopicResID, int TopicID, int ResourceID, string ResType)
        {
            this.TopicResID = TopicResID;
            this.TopicID = TopicID;
            this.ResourceID = ResourceID;
            this.ResType = ResType;
        }

        private int _TopicResID = 0;
        public int TopicResID
        {
            get { return _TopicResID; }
            protected set { _TopicResID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

        private int _ResourceID = 0;
        public int ResourceID
        {
            get { return _ResourceID; }
            private set { _ResourceID = value; }
        }

        private string _ResType = "";
        public string ResType
        {
            get { return _ResType; }
            private set { _ResType = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with TopicResourceLinks

        /// <summary>
        /// Returns the total number of TopicResourceLinks
        /// </summary>
        public int GetTopicResourceLinkCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from TopicResourceLink", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of TopicResourceLinks for a TopicID
        /// </summary>
        public int GetTopicResourceLinkCountByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from TopicResourceLink where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all TopicResourceLinks
        /// </summary>
        public List<TopicResourceLinkInfo> GetTopicResourceLinks(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from TopicResourceLink";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicResourceLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all ReTypes
        /// </summary>

        public System.Data.DataSet GetResTypesFromTopicResourceLink()
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "select distinct restype from topicresourcelink group by restype";
                SqlCommand cmd = new SqlCommand(qry, cn);

                //cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "ResTypes");
            }
            return ds;
        }

        /// <summary>
        /// Retrieves the TopicResourceLink with the specified ID
        /// </summary>
        public TopicResourceLinkInfo GetTopicResourceLinkByID(int TopicResID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from TopicResourceLink where TopicResID=@TopicResID ", cn);
                cmd.Parameters.Add("@TopicResID", SqlDbType.Int).Value = TopicResID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicResourceLinkFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a TopicResourceLink
        /// </summary>
        public bool DeleteTopicResourceLink(int TopicResID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from TopicResourceLink where TopicResID=@TopicResID", cn);
                cmd.Parameters.Add("@TopicResID", SqlDbType.Int).Value = TopicResID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new TopicResourceLink
        /// </summary>
        public int InsertTopicResourceLink(TopicResourceLinkInfo TopicResourceLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.CommandText = "sp_IncrementSortOnInsertTopicRes";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicResourceLink.TopicID;
                    cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = TopicResourceLink.ResourceID;
                    cmd.Parameters.Add("@ResType", SqlDbType.Char).Value = TopicResourceLink.ResType;
                    SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                    IDParameter.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(IDParameter);
                    cmd.ExecuteNonQuery();
                    int NewID = (int)IDParameter.Value;
                    return NewID;



                }



            }
        }

        /// <summary>
        /// Updates a TopicResourceLink
        /// </summary>
        public bool UpdateTopicResourceLink(TopicResourceLinkInfo TopicResourceLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update TopicResourceLink set " +
              "TopicID = @TopicID, " +
              "ResourceID = @ResourceID, " +
              "ResType = @ResType " +
              "where TopicResID = @TopicResID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicResourceLink.TopicID;
                cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = TopicResourceLink.ResourceID;
                cmd.Parameters.Add("@ResType", SqlDbType.Char).Value = TopicResourceLink.ResType;
                cmd.Parameters.Add("@TopicResID", SqlDbType.Int).Value = TopicResourceLink.TopicResID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public bool UpdateTopicResourceLinkSortOrder(int TopicResID, int SortOrder)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update TopicResourceLink set " +
              "SortOrder = @SortOrder " +
              "where TopicResID = @TopicResID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TopicResID", SqlDbType.Int).Value = TopicResID;
                cmd.Parameters.Add("@SortOrder", SqlDbType.Int).Value = SortOrder;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public TopicResourceLinkInfo GetTopicrResourcesByDetails(int TopicID, int ResourceID, String resType)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select  * " +
                    "from TopicResourceLink where TopicID =@TopicID and ResourceID =@ResourceID and ResType = @ResType ";

                //// add on ORDER BY if provided
                //if (cSortExpression.Length > 0)
                //{
                //    cSQLCommand = cSQLCommand +
                //        " order by " + cSortExpression;
                //}

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = ResourceID;
                cmd.Parameters.Add("@ResType", SqlDbType.VarChar).Value = resType;

                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicResourceLinkFromReader(reader, true);
                else
                    return null;
            }
        }

        public int DeleteTopicResourceLinkByUnselection(int TopicID, String ResourceType, String ResourceIDAssignments)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string str = "DELETE FROM TopicResourceLink " +
                    "where TopicID = @TopicID " +
                    "and ResType = @ResourceType ";

                if (!String.IsNullOrEmpty(ResourceIDAssignments))
                {
                    str = str + " and ResourceID not in ( " + @ResourceIDAssignments + " ) ";
                }

                SqlCommand cmd = new SqlCommand(str, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@ResourceType", SqlDbType.VarChar).Value = ResourceType;
                //cmd.Parameters.Add("@ResourceIDAssignments", SqlDbType.VarChar).Value = ResourceIDAssignments;
                //              cmd.Parameters.Add("@CategoryIDAssignments", SqlDbType.VarChar).Value = CategoryIDAssignments;
                cn.Open();
                return ExecuteNonQuery(cmd);
            }
        }

        public bool UpdateResourcePersonAssignments(int TopicID,
            string ResourceIDAssignments, string ResourceType)
        {



            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                // first delete any assignments that are not in the list passed in
                //              SqlCommand cmd = new SqlCommand(
                //                  "DELETE FROM categorylink " +
                //                  "where TopicID = ? and CategoryID NOT IN (?)", cn);

                // NOTE; For now, just delete them all
                SqlCommand cmd = new SqlCommand(
                    "DELETE FROM TopicResourceLink " +
                    "where TopicID = @TopicID " +
                    "and ResType = @ResourceType ", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@ResourceType", SqlDbType.VarChar).Value = ResourceType;
                //              cmd.Parameters.Add("@CategoryIDAssignments", SqlDbType.VarChar).Value = CategoryIDAssignments;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                //              return (ret == 1);

                if (ResourceIDAssignments.Trim().Length < 1)
                    // no assignments, so nothing more to do
                    return true;

                // now step through all ResourceIDs in the list passed in and call
                // this.InsertTopicResourceLink() for each, which will insert the assignment
                // if it does not already exist
                string[] ResourceIDs = ResourceIDAssignments.Split(',');

                TopicResourceLinkInfo TopicResourceLink;

                foreach (string cResourceID in ResourceIDs)
                {
                    TopicResourceLink = new TopicResourceLinkInfo(0, TopicID, Int32.Parse(cResourceID), ResourceType);
                    this.InsertTopicResourceLink(TopicResourceLink);
                }

                return true;
            }
        }

        /// <summary>
        /// Bhaskar N
        /// </summary>
        /// <param name="TopicID"></param>
        /// <param name="ResourceIDAssignments"></param>
        /// <param name="ResourceType"></param>
        /// <returns></returns>
        /// 
        public bool AddResourcePersonAssignments(int TopicID, string ResourceIDAssignments, string ResourceType)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                if (ResourceIDAssignments.Trim().Length < 1)
                    // no assignments, so nothing more to do
                    return true;

                // now step through all ResourceIDs in the list passed in and call
                // this.InsertTopicResourceLink() for each, which will insert the assignment
                // if it does not already exist
                string[] ResourceIDs = ResourceIDAssignments.Split(',');

                TopicResourceLinkInfo TopicResourceLink;

                foreach (string cResourceID in ResourceIDs)
                {
                    TopicResourceLink = new TopicResourceLinkInfo(0, TopicID, Int32.Parse(cResourceID), ResourceType);
                    this.InsertTopicResourceLink(TopicResourceLink);
                }

                return true;
            }
        }

        public DataSet GetTopicDetailsByResID(int ResourceID)
        {

            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "SELECT TopicResourceLink.topicresid,TopicResourceLink.ResType, TopicResourceLink.topicid, Topic.topicname, Topic.course_number, TopicResourceLink.resourceid " +
                " FROM TopicResourceLink INNER JOIN " +
                " Topic ON TopicResourceLink.topicid = Topic.topicid " +
                " WHERE (TopicResourceLink.resourceid = @ResourceID)";

                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = ResourceID;

                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = cSQLCommand;

                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;

        }

        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new TopicResourceLinkInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual TopicResourceLinkInfo GetTopicResourceLinkFromReader(IDataReader reader)
        {
            return GetTopicResourceLinkFromReader(reader, true);
        }
        protected virtual TopicResourceLinkInfo GetTopicResourceLinkFromReader(IDataReader reader, bool readMemos)
        {
            TopicResourceLinkInfo TopicResourceLink = new TopicResourceLinkInfo(
              (int)reader["TopicResID"],
              (int)reader["TopicID"],
              (int)reader["ResourceID"],
              reader["ResType"].ToString());


            return TopicResourceLink;
        }

        /// <summary>
        /// Returns a collection of TopicResourceLinkInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<TopicResourceLinkInfo> GetTopicResourceLinkCollectionFromReader(IDataReader reader)
        {
            return GetTopicResourceLinkCollectionFromReader(reader, true);
        }
        protected virtual List<TopicResourceLinkInfo> GetTopicResourceLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TopicResourceLinkInfo> TopicResourceLinks = new List<TopicResourceLinkInfo>();
            while (reader.Read())
                TopicResourceLinks.Add(GetTopicResourceLinkFromReader(reader, readMemos));
            return TopicResourceLinks;
        }
        #endregion
    }
}
#endregion