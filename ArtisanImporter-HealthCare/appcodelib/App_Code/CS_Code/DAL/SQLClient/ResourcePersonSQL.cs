﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;


#region ResourcePersonInfo

namespace PearlsReview.DAL
{
    public class ResourcePersonInfo
    {
        public ResourcePersonInfo() { }

        public ResourcePersonInfo(int ResourceID, string Fullname, string LastFirst, string Address, string City,
            string State, string Zip, string Country, string Phone, string Email, string SSN, string Specialty,
            string OtherNotes, bool ContrRecd, string DateDiscl, string Disclosure, string StateLic,
            string Degrees, string Position, string Experience, int FacilityID, string RFirstName, string RLastName, string RMiddle,string Photo)
        {
            this.ResourceID = ResourceID;
            this.Fullname = Fullname;
            this.LastFirst = LastFirst;
            this.Address = Address;
            this.City = City;
            this.State = State;
            this.Zip = Zip;
            this.Country = Country;
            this.Phone = Phone;
            this.Email = Email;
            this.SSN = SSN;
            this.Specialty = Specialty;
            this.OtherNotes = OtherNotes;
            this.ContrRecd = ContrRecd;
            this.DateDiscl = DateDiscl;
            this.Disclosure = Disclosure;
            this.StateLic = StateLic;
            this.Degrees = Degrees;
            this.Position = Position;
            this.Experience = Experience;
            this.FacilityID = FacilityID;
            this.RFirstName = RFirstName;
            this.RLastName = RLastName;
            this.RMiddle = RMiddle;
            this.Photo = Photo;
        }

        public ResourcePersonInfo(int TopicResID, int SortOrder, int ResourceID, string Fullname, string LastFirst, string Address, string City,
          string State, string Zip, string Country, string Phone, string Email, string SSN, string Specialty,
          string OtherNotes, bool ContrRecd, string DateDiscl, string Disclosure, string StateLic,
          string Degrees, string Position, string Experience, int FacilityID, string RFirstName, string RLastName, string RMiddle,string Photo)
        {
            this.TopicResID = TopicResID;
            this.SortOrder = SortOrder;
            this.ResourceID = ResourceID;
            this.Fullname = Fullname;
            this.LastFirst = LastFirst;
            this.Address = Address;
            this.City = City;
            this.State = State;
            this.Zip = Zip;
            this.Country = Country;
            this.Phone = Phone;
            this.Email = Email;
            this.SSN = SSN;
            this.Specialty = Specialty;
            this.OtherNotes = OtherNotes;
            this.ContrRecd = ContrRecd;
            this.DateDiscl = DateDiscl;
            this.Disclosure = Disclosure;
            this.StateLic = StateLic;
            this.Degrees = Degrees;
            this.Position = Position;
            this.Experience = Experience;
            this.FacilityID = FacilityID;
            this.RFirstName = RFirstName;
            this.RLastName = RLastName;
            this.RMiddle = RMiddle;
            this.Photo = Photo;
        }

        private string _photo = "";
        public string Photo
        {
            get { return _photo; }
            private set { _photo = value; }
        }

        private int _topicResID = 0;
        public int TopicResID
        {
            get { return _topicResID; }
            protected set { _topicResID = value; }
        }

        private int _sortOrder = 0;
        public int SortOrder
        {
            get { return _sortOrder; }
            protected set { _sortOrder = value; }
        }

        private int _ResourceID = 0;
        public int ResourceID
        {
            get { return _ResourceID; }
            protected set { _ResourceID = value; }
        }

        private string _Fullname = "";
        public string Fullname
        {
            get { return _Fullname; }
            private set { _Fullname = value; }
        }

        //private string _LastFirst = "";
        //public string LastFirst
        //{
        //    get { return _LastFirst.ToLower(); }
        //    private set { _LastFirst = value.ToLower(); }
        //}

        private string _LastFirst = "";
        public string LastFirst
        {
            get { return _LastFirst; }
            private set { _LastFirst = value; }
        }
        private string _Address = "";
        public string Address
        {
            get { return _Address; }
            private set { _Address = value; }
        }

        private string _City = "";
        public string City
        {
            get { return _City; }
            private set { _City = value; }
        }

        private string _State = "";
        public string State
        {
            get { return _State; }
            private set { _State = value; }
        }

        private string _Zip = "";
        public string Zip
        {
            get { return _Zip; }
            private set { _Zip = value; }
        }

        private string _Country = "";
        public string Country
        {
            get { return _Country; }
            private set { _Country = value; }
        }

        private string _Phone = "";
        public string Phone
        {
            get { return _Phone; }
            private set { _Phone = value; }
        }

        private string _Email = "";
        public string Email
        {
            get { return _Email; }
            private set { _Email = value; }
        }

        private string _SSN = "";
        public string SSN
        {
            get { return _SSN; }
            private set { _SSN = value; }
        }

        private string _Specialty = "";
        public string Specialty
        {
            get { return _Specialty.Replace(" href=", " target=\"_blank\" href="); }
            private set { _Specialty = value; }
        }

        private string _OtherNotes = "";
        public string OtherNotes
        {
            get { return _OtherNotes; }
            private set { _OtherNotes = value; }
        }

        private bool _ContrRecd = false;
        public bool ContrRecd
        {
            get { return _ContrRecd; }
            private set { _ContrRecd = value; }
        }

        private String _DateDiscl = "";
        public String DateDiscl
        {
            get { return _DateDiscl; }
            private set { _DateDiscl = value; }
        }

        private string _Disclosure = "I have nothing to disclose.";
        public string Disclosure
        {
            get { return _Disclosure; }
            private set { _Disclosure = value; }
        }
        private string _StateLic = "";
        public string StateLic
        {
            get { return _StateLic; }
            private set { _StateLic = value; }
        }
        private string _Degrees = "";
        public string Degrees
        {
            get { return _Degrees; }
            private set { _Degrees = value; }
        }
        private string _Position = "";
        public string Position
        {
            get { return _Position; }
            private set { _Position = value; }
        }
        private string _Experience = "";
        public string Experience
        {
            get { return _Experience; }
            private set { _Experience = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            private set { _FacilityID = value; }
        }

        private string _RFirstName = "";
        public string RFirstName
        {
            get { return _RFirstName; }
            set { _RFirstName = value; }
        }

        private string _RLastName = "";
        public string RLastName
        {
            get { return _RLastName; }
            set { _RLastName = value; }
        }

        private string _RMiddle = "";
        public string RMiddle
        {
            get { return _RMiddle; }
            set { _RMiddle = value; }
        }


    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with ResourcePersons

        /// <summary>
        /// Returns the total number of ResourcePersons
        /// </summary>
        public int GetResourcePersonCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from ResourcePerson where FacilityID=0", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all ResourcePersons
        /// </summary>
        public List<ResourcePersonInfo> GetResourcePersons(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from ResourcePerson";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetResourcePersonCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all ResourcePersons
        /// </summary>
        public List<ResourcePersonInfo> GetUnAssignedResourcesForLectEventID(int LectEvtID, String searchText)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top 50 * from resourceperson where not resourceid in (select resourceid from lectureeventresourcelink where lectevtid = @LectEvtID) ";

                if (!string.IsNullOrEmpty(searchText))
                {
                    cSQLCommand = cSQLCommand + " and fullname like '%" + searchText.Replace("'", "''").ToLower() + "%' ";

                }

                //// add on ORDER BY if provided
                //if (cSortExpression.Length > 0)
                //{
                //    cSQLCommand = cSQLCommand +
                //        " order by " + cSortExpression;
                //}

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectEvtID;
                //cmd.Parameters.Add("@searchText", SqlDbType.VarChar).Value = searchText;

                cn.Open();
                return GetResourcePersonCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        public DataSet GetLectureEventsDetails(string cSortExpression)
        {
            DataSet ds = new DataSet();
         
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select * " +
                //    "from ResourcePerson";
                string cSQLCommand = "SELECT  DISTINCT LectureEventResourceLink.resourceid, ResourcePerson.fullname, "+ 
                                    " ResourcePerson.lastfirst  FROM         LectureEventResourceLink INNER JOIN " +
                                      " ResourcePerson ON LectureEventResourceLink.resourceid = ResourcePerson.resourceid ";
                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                              
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = cSQLCommand;
              
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }

        /// <summary>
        /// Retrieves all ResourcePersons for a TopicID and Resource Type
        /// (author="A", editor="E", nurse planner="N")
        /// </summary>
        public List<ResourcePersonInfo> GetResourcePersonsByTopicIDAndType(int TopicID,
       string ResourceType, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select  TopicResourceLink.SortOrder as SortOrder,* " +
                    "from ResourcePerson " +
                    "join TopicResourceLink on ResourcePerson.ResourceID = TopicResourceLink.ResourceID " +
                    "where TopicResourceLink.TopicID = @TopicID " +
                    "and TopicResourceLink.ResType = @ResourceType order by TopicResourceLink.SortOrder";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@ResourceType", SqlDbType.VarChar).Value = ResourceType;

                cn.Open();
                return GetResourcePersonCollectionInfoFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all ResourcePersons for a TopicID
        /// <summary>
        /// 
        public DataSet GetResourcePersonsByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select  DISTINCT fullname, degrees from ResourcePerson " +
                                   " join TopicResourceLink on ResourcePerson.ResourceID = TopicResourceLink.ResourceID " +
                                    "where TopicResourceLink.TopicID = @TopicID";
                    
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
               
                cn.Open();

                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();

                ad.Fill(ds,"ResourcePerson");
                return ds;
            }
        }
        /// Retrieves the ResourcePerson with the specified ID
        /// </summary>
        public ResourcePersonInfo GetResourcePersonByID(int ResourceID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from ResourcePerson where ResourceID=@ResourceID", cn);
                cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = ResourceID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetResourcePersonFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the ResourcePerson with the Facility ID
        /// </summary>
        public List<ResourcePersonInfo> GetResourcePersonByFacilityID(int FacilityID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from ResourcePerson where facilityid=@FacilityID order by fullname ", cn);
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cn.Open();
                return GetResourcePersonCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Bhaskar N
        /// Retrieves all Selected ResourcePersons for a TopicID 
        /// </summary>
        public List<ResourcePersonInfo> GetAllSelectedResourcePersonsByTopicID(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                
                string cSQLCommand = "select * " +
                  "from ResourcePerson " +
                  " where ResourceID in (select distinct TopicResourceLink.ResourceID from ResourcePerson  " +
                  " join TopicResourceLink on ResourcePerson.ResourceID = TopicResourceLink.ResourceID " +
                  "where TopicResourceLink.TopicID = @TopicID ) ";


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }               

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;              

                cn.Open();
                return GetResourcePersonCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public DataSet GetAllSelectedResourcePersonInfoByTopicID(int TopicID, string cSortExpression)
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select * " +
                //    "from ResourcePerson";
                string cSQLCommand = "select TopicResourceLink.SortOrder as SortOrder,  * " +
                      "from ResourcePerson " +
                        "join TopicResourceLink on ResourcePerson.ResourceID = TopicResourceLink.ResourceID " +
                      "where TopicResourceLink.TopicID = @TopicID " +
                   " and ResourcePerson.ResourceID in (select distinct TopicResourceLink.ResourceID from ResourcePerson  " +
                      " join TopicResourceLink on ResourcePerson.ResourceID = TopicResourceLink.ResourceID " +
                      "where TopicResourceLink.TopicID = @TopicID ) ";
                
                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = cSQLCommand;

                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }

        /// <summary>
        /// Bhaskar N
        /// Retrieves all not Selected ResourcePersons for a TopicID 
        /// </summary>
        public List<ResourcePersonInfo> GetAllNotSelectedResourcePersonsByTopicID(int TopicID,string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from ResourcePerson " +
                    " where ResourceID not in (select distinct TopicResourceLink.ResourceID from ResourcePerson  " +
                    " join TopicResourceLink on ResourcePerson.ResourceID = TopicResourceLink.ResourceID " +
                    "where TopicResourceLink.TopicID = @TopicID ) ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }                

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;                

                cn.Open();
                return GetResourcePersonCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Deletes a ResourcePerson
        /// </summary>
        public bool DeleteResourcePerson(int ResourceID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from ResourcePerson where ResourceID=@ResourceID", cn);
                cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = ResourceID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public int IsResourcePersonExists(string ResourceID)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string str = "SELECT     COUNT(resourceid) FROM  TopicResourceLink WHERE     (resourceid = " + ResourceID + ")";

                //string str = "Select count(ResourceID) from TopicResourceLink where ResourceID= " + ResourceID.ToString().Trim();
                SqlCommand cmd = new SqlCommand(str, cn);
                //cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = Username;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }

        }

        /// <summary>
        /// Inserts a new ResourcePerson
        /// </summary>
        public int InsertResourcePerson(ResourcePersonInfo ResourcePerson)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into ResourcePerson " +
              "(Fullname, " +
              "LastFirst, " +
              "Address, " +
              "City, " +
              "State, " +
              "Zip, " +
              "Country, " +
              "Phone, " +
              "Email, " +
              "SSN, " +
              "Specialty, " +
              "OtherNotes, " +
              "ContrRecd, " +
              "DateDiscl, " +
              "Disclosure, " +
              "StateLic, " +
              "Degrees, " +
              "Position, " +
              "Experience, " +
              "FacilityID, " +
              "RFirstName, " +
              "RLastName, " +
              "RMiddle, " +
              "Photo ) " +
              "VALUES (" +
              "@Fullname, " +
              "@LastFirst, " +
              "@Address, " +
              "@City, " +
              "@State, " +
              "@Zip, " +
              "@Country, " +
              "@Phone, " +
              "@Email, " +
              "@SSN, " +
              "@Specialty, " +
              "@OtherNotes, " +
              "@ContrRecd, " +
              "@DateDiscl, " +
              "@Disclosure, " +
              "@StateLic, " +
              "@Degrees, " +
              "@Position, " +
              "@Experience, " +
              "@FacilityID, " +
              "@RFirstName, " +
              "@RLastName, " +
              "@RMiddle, " +
              "@Photo ) SET @ID = SCOPE_IDENTITY()", cn);

                String FullName = String.Empty;
                String LastFirst= String.Empty;

                FullName = ResourcePerson.RFirstName;
                LastFirst = ResourcePerson.RLastName;

                if (!String.IsNullOrEmpty(ResourcePerson.RMiddle))
                {
                    FullName = FullName + " " + ResourcePerson.RMiddle;
                    LastFirst = LastFirst + " " + ResourcePerson.RMiddle;
                }

                if (!String.IsNullOrEmpty(ResourcePerson.RLastName))
                {
                    if ((!String.IsNullOrEmpty(FullName)) && (!string.IsNullOrEmpty(ResourcePerson.RMiddle)))
                    {
                        FullName = FullName + " " + ResourcePerson.RLastName;
                       
                    }
                    else if (!String.IsNullOrEmpty(FullName))
                    {
                        FullName = FullName + " " + ResourcePerson.RLastName;
                    }
                }

                if (!String.IsNullOrEmpty(ResourcePerson.RFirstName))
                {
                    if ((!String.IsNullOrEmpty(LastFirst)) && (!string.IsNullOrEmpty(ResourcePerson.RMiddle)))
                    {
                        LastFirst = LastFirst + " " + ResourcePerson.RFirstName;
                    }
                    else if (!String.IsNullOrEmpty(LastFirst))
                    {
                        LastFirst = LastFirst + " " + ResourcePerson.RFirstName;
                    }
                }
                cmd.Parameters.Add("@Fullname", SqlDbType.VarChar).Value = FullName;
                cmd.Parameters.Add("@LastFirst", SqlDbType.VarChar).Value = LastFirst;
                cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = ResourcePerson.Address;
                cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = ResourcePerson.City;
                cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = ResourcePerson.State;
                cmd.Parameters.Add("@Zip", SqlDbType.VarChar).Value = ResourcePerson.Zip;
                cmd.Parameters.Add("@Country", SqlDbType.VarChar).Value = ResourcePerson.Country;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ResourcePerson.Phone;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ResourcePerson.Email;
                cmd.Parameters.Add("@SSN", SqlDbType.VarChar).Value = ResourcePerson.SSN;
                cmd.Parameters.Add("@Specialty", SqlDbType.VarChar).Value = ResourcePerson.Specialty;
                cmd.Parameters.Add("@OtherNotes", SqlDbType.VarChar).Value = ResourcePerson.OtherNotes;
                cmd.Parameters.Add("@ContrRecd", SqlDbType.Bit).Value = ResourcePerson.ContrRecd;
                

                if (ResourcePerson.DateDiscl == null
                    || ResourcePerson.DateDiscl.Trim().Length == 0)
                {
                    cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
                    try
                    {
                        DateTime dDateDiscl = Convert.ToDateTime(ResourcePerson.DateDiscl);
                        cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = dDateDiscl;
                    }
                    catch
                    {
                        // something went wrong with convert, so store NULL instead
                        cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                }

                cmd.Parameters.Add("@Disclosure", SqlDbType.VarChar).Value = ResourcePerson.Disclosure;
                cmd.Parameters.Add("@StateLic", SqlDbType.VarChar).Value = ResourcePerson.StateLic;
                cmd.Parameters.Add("@Degrees", SqlDbType.VarChar).Value = ResourcePerson.Degrees;
                cmd.Parameters.Add("@Position", SqlDbType.VarChar).Value = ResourcePerson.Position;
                cmd.Parameters.Add("@Experience", SqlDbType.VarChar).Value = ResourcePerson.Experience;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = ResourcePerson.FacilityID;
                cmd.Parameters.Add("@RFirstName", SqlDbType.VarChar).Value = ResourcePerson.RFirstName;
                cmd.Parameters.Add("@RLastName", SqlDbType.VarChar).Value = ResourcePerson.RLastName;
                cmd.Parameters.Add("@RMiddle", SqlDbType.VarChar).Value = String.IsNullOrEmpty(ResourcePerson.RMiddle) ? String.Empty : ResourcePerson.RMiddle;
                cmd.Parameters.Add("@Photo", SqlDbType.VarChar).Value = String.IsNullOrEmpty(ResourcePerson.Photo) ? String.Empty : ResourcePerson.Photo; 

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a ResourcePerson
        /// </summary>
        public bool UpdateResourcePerson(ResourcePersonInfo ResourcePerson)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update ResourcePerson set " +
              "Fullname = @Fullname, " +
              "LastFirst = @LastFirst, " +
              "Address = @Address, " +
              "City = @City, " +
              "State = @State, " +
              "Zip = @Zip, " +
              "Country = @Country, " +
              "Phone = @Phone, " +
              "Email = @Email, " +
              "SSN = @SSN, " +
              "Specialty = @Specialty, " +
              "OtherNotes = @OtherNotes, " +
              "ContrRecd = @ContrRecd, " +
              "DateDiscl = @DateDiscl, " +
              "Disclosure = @Disclosure, " +
              "StateLic = @StateLic, " +
              "Degrees = @Degrees, " +
              "Position = @Position, " +
              "Experience = @Experience, " +
              "FacilityID = @FacilityID, " +
              "RFirstName = @RFirstName, " +
              "RLastName = @RLastName, " +
              "RMiddle = @RMiddle, " +
              "Photo = @Photo " +
              "where ResourceID = @ResourceID ", cn);

                String FullName = String.Empty;
                String LastFirst = String.Empty;

                FullName = ResourcePerson.RFirstName;
                LastFirst = ResourcePerson.RLastName;

                if (!String.IsNullOrEmpty(ResourcePerson.RMiddle))
                {
                    FullName = FullName + " " + ResourcePerson.RMiddle;
                    LastFirst = LastFirst + " " + ResourcePerson.RMiddle;
                }

                if (!String.IsNullOrEmpty(ResourcePerson.RLastName))
                {
                    if ((!String.IsNullOrEmpty(FullName)) && (!string.IsNullOrEmpty(ResourcePerson.RMiddle)))
                    {
                        FullName = FullName + " " + ResourcePerson.RLastName;

                    }
                    else if (!String.IsNullOrEmpty(FullName))
                    {
                        FullName = FullName + " " + ResourcePerson.RLastName;
                    }
                }

                if (!String.IsNullOrEmpty(ResourcePerson.RFirstName))
                {
                    if ((!String.IsNullOrEmpty(LastFirst)) && (!string.IsNullOrEmpty(ResourcePerson.RMiddle)))
                    {
                        LastFirst = LastFirst + " " + ResourcePerson.RFirstName;
                    }
                    else if (!String.IsNullOrEmpty(LastFirst))
                    {
                        LastFirst = LastFirst + " " + ResourcePerson.RFirstName;
                    }
                }
                cmd.Parameters.Add("@Fullname", SqlDbType.VarChar).Value = FullName;
                cmd.Parameters.Add("@LastFirst", SqlDbType.VarChar).Value = LastFirst;
                cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = ResourcePerson.Address;
                cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = ResourcePerson.City;
                cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = ResourcePerson.State;
                cmd.Parameters.Add("@Zip", SqlDbType.VarChar).Value = ResourcePerson.Zip;
                cmd.Parameters.Add("@Country", SqlDbType.VarChar).Value = ResourcePerson.Country;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ResourcePerson.Phone;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ResourcePerson.Email;
                cmd.Parameters.Add("@SSN", SqlDbType.VarChar).Value = ResourcePerson.SSN;
                cmd.Parameters.Add("@Specialty", SqlDbType.VarChar).Value = ResourcePerson.Specialty;
                cmd.Parameters.Add("@OtherNotes", SqlDbType.VarChar).Value = ResourcePerson.OtherNotes;
                cmd.Parameters.Add("@ContrRecd", SqlDbType.Bit).Value = ResourcePerson.ContrRecd;

                if (ResourcePerson.DateDiscl == null
                    || ResourcePerson.DateDiscl.Trim().Length == 0)
                {
                    cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
                    try
                    {
                        DateTime dDateDiscl = Convert.ToDateTime(ResourcePerson.DateDiscl);
                        cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = dDateDiscl;
                    }
                    catch
                    {
                        // something went wrong with convert, so store NULL instead
                        cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = DBNull.Value;
                    }
                }

                cmd.Parameters.Add("@Disclosure", SqlDbType.VarChar).Value = ResourcePerson.Disclosure;
                cmd.Parameters.Add("@StateLic", SqlDbType.VarChar).Value = ResourcePerson.StateLic;
                cmd.Parameters.Add("@Degrees", SqlDbType.VarChar).Value = ResourcePerson.Degrees;
                cmd.Parameters.Add("@Position", SqlDbType.VarChar).Value = ResourcePerson.Position;
                cmd.Parameters.Add("@Experience", SqlDbType.VarChar).Value = ResourcePerson.Experience;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = ResourcePerson.FacilityID;
                cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = ResourcePerson.ResourceID;
                cmd.Parameters.Add("@RFirstName", SqlDbType.VarChar).Value = ResourcePerson.RFirstName;
                cmd.Parameters.Add("@RLastName", SqlDbType.VarChar).Value = ResourcePerson.RLastName;
                cmd.Parameters.Add("@RMiddle", SqlDbType.VarChar).Value =String.IsNullOrEmpty( ResourcePerson.RMiddle) ? String.Empty: ResourcePerson.RMiddle ;
                cmd.Parameters.Add("@Photo", SqlDbType.VarChar).Value = String.IsNullOrEmpty(ResourcePerson.Photo) ? String.Empty : ResourcePerson.Photo; 

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Retrieves all ResourcePersons for a LectEvtID and Sequence Number
        /// (1 - 8, indicating up to eight lecture topics for a lecture day)
        /// </summary>
        public List<ResourcePersonInfo> GetResourcePersonsByLectEvtIDAndSequence(int LectEvtID,
             int Sequence, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from ResourcePerson " +
                    "join LectureEventResourceLink on ResourcePerson.ResourceID = LectureEventResourceLink.ResourceID " +
                    "where LectureEventResourceLink.LectEvtID = @LectEvtID " +
                    "and LectureEventResourceLink.Sequence = @Sequence ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectEvtID;
                cmd.Parameters.Add("@Sequence", SqlDbType.Int).Value = Sequence;

                cn.Open();
                return GetResourcePersonCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        public List<ResourcePersonInfo> GetResourcePersonsByLectEvtID(int LectEvtID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from ResourcePerson " +
                    "join LectureEventResourceLink on ResourcePerson.ResourceID = LectureEventResourceLink.ResourceID " +
                    "where LectureEventResourceLink.LectEvtID = @LectEvtID ";

               

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectEvtID;
             
                cn.Open();
                return GetResourcePersonCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<ResourcePersonInfo> GetResourcePersonsByResID(int ResourceID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                   "from ResourcePerson " +
                   "join TopicResourceLink on ResourcePerson.ResourceID = TopicResourceLink.ResourceID " +
                   "where TopicResourceLink.ResourceID = @ResourceID "; 

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = ResourceID;

                cn.Open();
                return GetResourcePersonCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        #endregion

        #region PRProvider


        /////////////////////////////////////////////////////////
        // methods that work with ResourcePersons
       
        /// <summary>
        /// Returns a new ResourcePersonInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual ResourcePersonInfo GetResourcePersonFromReader(IDataReader reader)
        {
            return GetResourcePersonFromReader(reader, true);
        }
        protected virtual ResourcePersonInfo GetResourcePersonFromReader(IDataReader reader, bool readMemos)
        {
            // NOTE: We are forcing a default value for the Disclosure statement if blank in the database
            ResourcePersonInfo ResourcePerson = new ResourcePersonInfo(
              (int)reader["ResourceID"],
              reader["Fullname"].ToString(),
              reader["LastFirst"].ToString(),
              reader["Address"].ToString(),
              reader["City"].ToString(),
              reader["State"].ToString(),
              reader["Zip"].ToString(),
              reader["Country"].ToString(),
              reader["Phone"].ToString(),
              reader["Email"].ToString(),
              reader["SSN"].ToString(),
              reader["Specialty"].ToString(),
              reader["OtherNotes"].ToString(),
              (bool)reader["ContrRecd"],
              (Convert.IsDBNull(reader["DateDiscl"])
                  ? "" : ((DateTime)reader["DateDiscl"]).ToShortDateString()),
              (reader["Disclosure"].ToString().Trim().Length == 0
                  ? "I have nothing to disclose." : reader["Disclosure"].ToString()),
              reader["StateLic"].ToString(),
              reader["Degrees"].ToString(),
              reader["Position"].ToString(),
              reader["Experience"].ToString(),
              (int)reader["FacilityID"],
              reader["RFirstName"].ToString(),
              reader["RLastName"].ToString(),
              reader["RMiddle"].ToString(),
              reader["Photo"].ToString());


            //(DateTime)(Convert.IsDBNull(reader["DateDiscl"]) 
            //    ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["DateDiscl"]),


            return ResourcePerson;
        }

        protected virtual ResourcePersonInfo GetResourcePersonInfoFromReader(IDataReader reader)
        {
            return GetResourcePersonInfoFromReader(reader, true);
        }
        protected virtual ResourcePersonInfo GetResourcePersonInfoFromReader(IDataReader reader, bool readMemos)
        {
            // NOTE: We are forcing a default value for the Disclosure statement if blank in the database
            ResourcePersonInfo ResourcePerson = new ResourcePersonInfo(
              (int)reader["TopicResId"],
              (int)(Convert.IsDBNull(reader["SortOrder"]) ? (int)0 : (int)reader["SortOrder"]),
                //(int)reader["SortOrder"],
              (int)reader["ResourceID"],
              reader["Fullname"].ToString(),
              reader["LastFirst"].ToString(),
              reader["Address"].ToString(),
              reader["City"].ToString(),
              reader["State"].ToString(),
              reader["Zip"].ToString(),
              reader["Country"].ToString(),
              reader["Phone"].ToString(),
              reader["Email"].ToString(),
              reader["SSN"].ToString(),
              reader["Specialty"].ToString(),
              reader["OtherNotes"].ToString(),
              (bool)reader["ContrRecd"],
              (Convert.IsDBNull(reader["DateDiscl"])
                  ? "" : ((DateTime)reader["DateDiscl"]).ToShortDateString()),
              (reader["Disclosure"].ToString().Trim().Length == 0
                  ? "I have nothing to disclose." : reader["Disclosure"].ToString()),
              reader["StateLic"].ToString(),
              reader["Degrees"].ToString(),
              reader["Position"].ToString(),
              reader["Experience"].ToString(),
              (int)reader["FacilityID"],
               reader["RFirstName"].ToString(),
              reader["RLastName"].ToString(),
              reader["RMiddle"].ToString(),
              reader["Photo"].ToString());
            //(DateTime)(Convert.IsDBNull(reader["DateDiscl"]) 
            //    ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["DateDiscl"]),


            return ResourcePerson;
        }
        /// <summary>
        /// Returns a collection of ResourcePersonInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<ResourcePersonInfo> GetResourcePersonCollectionFromReader(IDataReader reader)
        {
            return GetResourcePersonCollectionFromReader(reader, true);
        }
        protected virtual List<ResourcePersonInfo> GetResourcePersonCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<ResourcePersonInfo> ResourcePersons = new List<ResourcePersonInfo>();
            while (reader.Read())
                ResourcePersons.Add(GetResourcePersonFromReader(reader, readMemos));
            return ResourcePersons;
        }

        protected virtual List<ResourcePersonInfo> GetResourcePersonCollectionInfoFromReader(IDataReader reader)
        {
            return GetResourcePersonCollectionInfoFromReader(reader, true);
        }
        protected virtual List<ResourcePersonInfo> GetResourcePersonCollectionInfoFromReader(IDataReader reader, bool readMemos)
        {
            List<ResourcePersonInfo> ResourcePersons = new List<ResourcePersonInfo>();
            while (reader.Read())
                ResourcePersons.Add(GetResourcePersonInfoFromReader(reader, readMemos));
            return ResourcePersons;
        }


        #endregion
    }
}
#endregion