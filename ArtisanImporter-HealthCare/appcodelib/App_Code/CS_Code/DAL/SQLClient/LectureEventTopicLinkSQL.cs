﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;


#region LectureEventTopicLinkSQL

namespace PearlsReview.DAL
{
    public class LectureEventTopicLinkInfo
    {
        public LectureEventTopicLinkInfo() { }

        public LectureEventTopicLinkInfo(int LectEvtTopID, int LectEvtID, int TopicID)
        {
            this.LectEvtTopID = LectEvtTopID;
            this.LectEvtID = LectEvtID;
            this.TopicID = TopicID;
        }

        private int _LectEvtTopID = 0;
        public int LectEvtTopID
        {
            get { return _LectEvtTopID; }
            protected set { _LectEvtTopID = value; }
        }

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            private set { _LectEvtID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

    }
}

#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider


        /////////////////////////////////////////////////////////
        // methods that work with LectureEventTopicLinks

        /// <summary>
        /// Returns the total number of LectureEventTopicLinks
        /// </summary>
        public int GetLectureEventTopicLinkCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from LectureEventTopicLink", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all LectureEventTopicLinks
        /// </summary>
        public List<LectureEventTopicLinkInfo> GetLectureEventTopicLinks(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from LectureEventTopicLink";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetLectureEventTopicLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the LectureEventTopicLink with the specified ID
        /// </summary>
        public LectureEventTopicLinkInfo GetLectureEventTopicLinkByID(int LectEvtTopID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from LectureEventTopicLink where LectEvtTopID=@LectEvtTopID", cn);
                cmd.Parameters.Add("@LectEvtTopID", SqlDbType.Int).Value = LectEvtTopID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetLectureEventTopicLinkFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a LectureEventTopicLink
        /// </summary>
        public bool DeleteLectureEventTopicLink(int LectEvtTopID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from LectureEventTopicLink where LectEvtTopID=@LectEvtTopID", cn);
                cmd.Parameters.Add("@LectEvtTopID", SqlDbType.Int).Value = LectEvtTopID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public bool DeleteLectureEventTopicLinkByTopicID(int topicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from LectureEventTopicLink where topicID=@topicID", cn);
                cmd.Parameters.Add("@topicID", SqlDbType.Int).Value = topicID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        /// <summary>
        /// Inserts a new LectureEventTopicLink
        /// </summary>
        public int InsertLectureEventTopicLink(LectureEventTopicLinkInfo LectureEventTopicLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into LectureEventTopicLink " +
              "(LectEvtID, " +
              "TopicID) " +
              "VALUES (" +
              "@LectEvtID, " +
              "@TopicID) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectureEventTopicLink.LectEvtID;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = LectureEventTopicLink.TopicID;
               
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a LectureEventTopicLink
        /// </summary>
        public bool UpdateLectureEventTopicLink(LectureEventTopicLinkInfo LectureEventTopicLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update LectureEventTopicLink set " +
              "LectEvtID = @LectEvtID, " +
              "TopicID = @TopicID " +
              "where LectEvtTopID = @LectEvtTopID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectureEventTopicLink.LectEvtID;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = LectureEventTopicLink.TopicID;
               cmd.Parameters.Add("@LectEvtTopID", SqlDbType.Int).Value = LectureEventTopicLink.LectEvtTopID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider

        /////////////////////////////////////////////////////////
        // methods that work with LectureEventTopicLinks
      
        /// <summary>
        /// Returns a new LectureEventTopicLinkInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual LectureEventTopicLinkInfo GetLectureEventTopicLinkFromReader(IDataReader reader)
        {
            return GetLectureEventTopicLinkFromReader(reader, true);
        }
        protected virtual LectureEventTopicLinkInfo GetLectureEventTopicLinkFromReader(IDataReader reader, bool readMemos)
        {
            LectureEventTopicLinkInfo LectureEventTopicLink = new LectureEventTopicLinkInfo(
              (int)reader["LectEvtTopID"],
              (int)reader["LectEvtID"],
              (int)reader["TopicID"]);


            return LectureEventTopicLink;
        }

        /// <summary>
        /// Returns a collection of LectureEventTopicLinkInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<LectureEventTopicLinkInfo> GetLectureEventTopicLinkCollectionFromReader(IDataReader reader)
        {
            return GetLectureEventTopicLinkCollectionFromReader(reader, true);
        }
        protected virtual List<LectureEventTopicLinkInfo> GetLectureEventTopicLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<LectureEventTopicLinkInfo> LectureEventTopicLinks = new List<LectureEventTopicLinkInfo>();
            while (reader.Read())
                LectureEventTopicLinks.Add(GetLectureEventTopicLinkFromReader(reader, readMemos));
            return LectureEventTopicLinks;
        }
        #endregion
    }
}
#endregion
