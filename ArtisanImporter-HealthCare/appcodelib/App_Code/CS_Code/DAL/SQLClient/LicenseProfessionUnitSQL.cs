﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for LicenseProfUnitSQL
/// </summary>
/// 
#region LicenseProfessionUnitInfo
namespace PearlsReview.DAL
{
    public class LicenseProfessionUnitInfo
    {       
        public LicenseProfessionUnitInfo()
        {
        }

        public LicenseProfessionUnitInfo(int pLicenseProfUnitID, int pLicense_Type_ID, int pLicense_Profession_MSID, string pUnitName, string pLicense_Desc, string pProf_Title, string pLpuDesc)
        {
            this.LicenseProfUnitID = pLicenseProfUnitID;
            this.License_Type_ID = pLicense_Type_ID;
            this.License_Profession_MSID = pLicense_Profession_MSID;
            this.UnitName = pUnitName;
            this.Prof_Title = pProf_Title;            
            this.License_Desc = pLicense_Desc;            
            this.LpuDesc = pLpuDesc; 
        }

        public LicenseProfessionUnitInfo(int pLicenseProfUnitID, int pLicense_Type_ID, int pLicense_Profession_MSID, string pUnitName)
        {
            this.LicenseProfUnitID = pLicenseProfUnitID;
            this.License_Type_ID = pLicense_Type_ID;
            this.License_Profession_MSID = pLicense_Profession_MSID;
            this.UnitName = pUnitName;            
        }

        private int _licenseProfUnitID = 0;
        public int LicenseProfUnitID
        {
            get { return _licenseProfUnitID; }
            protected set { _licenseProfUnitID = value; }
        }

        private int _license_Type_ID = 0;
        public int License_Type_ID
        {
            get { return _license_Type_ID; }
            private set { _license_Type_ID = value; }
        }

        private int _license_Profession_MSID = 0;
        public int License_Profession_MSID
        {
            get { return _license_Profession_MSID; }
            set { _license_Profession_MSID = value; }
        }

        private string _unitName = string.Empty;
        public string UnitName
        {
            get { return _unitName; }
            set { _unitName = value; }
        }

        private string _license_Desc = string.Empty;
        public string License_Desc
        {
            get { return _license_Desc; }
            set { _license_Desc = value; }
        }

        private string _prof_Title = string.Empty;
        public string Prof_Title
        {
            get { return _prof_Title; }
            set { _prof_Title = value; }
        }

        private string _lpuDesc = string.Empty;
        public string LpuDesc
        {
            get { return _lpuDesc; }
            set { _lpuDesc = value; }
        }
    }
}
#endregion 
    
#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with LicenseProfessionUnits

        /// <summary>
        /// Returns the total number of LicenseProfessionUnits
        /// </summary>
        public  int GetLicenseProfUnitCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(LicenseProfUnitID) from LicenseProfUnit", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all LicenseProfessionUnits
        /// </summary>
        public  List<LicenseProfessionUnitInfo> GetLicenseProfUnits(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "sp_get_LicenseProfUnit";                
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SortExpression", SqlDbType.VarChar).Value = cSortExpression;                
                cn.Open();
                return GetLicenseProfUnitCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<LicenseProfessionUnitInfo> GetLicenseProfUnits(string cSortExpression, int pLicenseTypeID, int pProfessionMsID, string pUnitName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "sp_get_LicenseProfUnit";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                if (pUnitName == null)
                    pUnitName = string.Empty;
                cmd.Parameters.Add("@SortExpression", SqlDbType.VarChar).Value = cSortExpression;                
                cmd.Parameters.Add("@LicenseTypeID", SqlDbType.Int).Value = pLicenseTypeID;
                cmd.Parameters.Add("@ProfessionMsID", SqlDbType.Int).Value = pProfessionMsID;
                cmd.Parameters.Add("@UnitName", SqlDbType.VarChar).Value = pUnitName;
                cn.Open();
                return GetLicenseProfUnitCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves the LicenseProfUnit with the specified ID
        /// </summary>
        public  LicenseProfessionUnitInfo GetLicenseProfUnitByID(int pLicenseProfUnitID)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "sp_get_LicenseProfUnit";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.CommandType = CommandType.StoredProcedure;                
                cmd.Parameters.Add("@LicenseProfUnitID", SqlDbType.Int).Value = pLicenseProfUnitID;               
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetLicenseProfUnitFromReader(reader, true);
                else
                    return null;
            }          
        }

        /// <summary>
        /// Deletes a LicenseProfessionUnitInfo
        /// </summary>
        public  bool DeleteLicenseProfessionUnit(int pLicenseProfUnitID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from LicenseProfUnit where lpuid=@lpuid", cn);
                cmd.Parameters.Add("@lpuid", SqlDbType.Int).Value = pLicenseProfUnitID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new LicenseProfessionUnitInfo
        /// </summary>
        public  int InsertLicenseProfessionUnit(LicenseProfessionUnitInfo pLicenseProfessionUnit)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into LicenseProfUnit " +
              "(Lpu_license_type_id, Lpu_MSID, UnitName) " +
              "VALUES (" +
              "@Lpu_license_type_id, @Lpu_MSID, @UnitName) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@Lpu_license_type_id", SqlDbType.Int).Value = pLicenseProfessionUnit.License_Type_ID;
                cmd.Parameters.Add("@Lpu_MSID", SqlDbType.Int).Value = pLicenseProfessionUnit.License_Profession_MSID;
                cmd.Parameters.Add("@UnitName", SqlDbType.VarChar).Value = pLicenseProfessionUnit.UnitName;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;            
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;
            }
        }

        /// <summary>
        /// Updates a LicenseProfessionUnitInfo
        /// </summary>
        public  bool UpdateLicenseProfessionUnit(LicenseProfessionUnitInfo pLicenseProfessionUnit)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update LicenseProfUnit set " +
              "Lpu_license_type_id = @Lpu_license_type_id, " +
              "Lpu_MSID = @Lpu_MSID, " +
              "UnitName = @UnitName" +
              " where lpuid = @Lpuid ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Lpuid", SqlDbType.Int).Value = pLicenseProfessionUnit.LicenseProfUnitID;
                cmd.Parameters.Add("@Lpu_license_type_id", SqlDbType.Int).Value = pLicenseProfessionUnit.License_Type_ID;
                cmd.Parameters.Add("@Lpu_MSID", SqlDbType.Int).Value = pLicenseProfessionUnit.License_Profession_MSID;
                cmd.Parameters.Add("@UnitName", SqlDbType.VarChar).Value = pLicenseProfessionUnit.UnitName;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new LicenseProfessionUnitInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual LicenseProfessionUnitInfo GetLicenseProfUnitFromReader(IDataReader reader)
        {
            return GetLicenseProfUnitFromReader(reader, true);
        }

        protected virtual LicenseProfessionUnitInfo GetLicenseProfUnitFromReader(IDataReader reader, bool readMemos)
        {
            LicenseProfessionUnitInfo LicenseProfUnit = new LicenseProfessionUnitInfo(
              (int)reader["lpuid"],
              (int)reader["lpu_license_type_id"],
              (int)reader["lpu_msid"],
              reader["UnitName"].ToString(),
              reader["License_Desc"].ToString(),
              reader["Prof_Title"].ToString(),
              reader["LpuDesc"].ToString());
            return LicenseProfUnit;
        }

        /// <summary>
        /// Returns a collection of LicenseTypeInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<LicenseProfessionUnitInfo> GetLicenseProfUnitCollectionFromReader(IDataReader reader)
        {
            return GetLicenseProfUnitCollectionFromReader(reader, true);
        }

        protected virtual List<LicenseProfessionUnitInfo> GetLicenseProfUnitCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<LicenseProfessionUnitInfo> LicenseProfUnits = new List<LicenseProfessionUnitInfo>();
            while (reader.Read())
                LicenseProfUnits.Add(GetLicenseProfUnitFromReader(reader, readMemos));
            return LicenseProfUnits;
        }
        #endregion
    }
}
#endregion

