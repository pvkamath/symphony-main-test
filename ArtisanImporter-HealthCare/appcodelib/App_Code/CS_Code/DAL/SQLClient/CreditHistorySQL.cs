﻿/*************************************************************************
 * OnCourse Learning,Inc
 * 
 * File Description:  
 * 
 * Author   :   Budgerel T
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region CreditHistoryInfo

namespace PearlsReview.DAL
{
    public class CreditHistoryInfo
    {
        public CreditHistoryInfo() { }

        public CreditHistoryInfo(int ChID, int CreditID, int r_UserID, DateTime LogDate, string HistoryType, int CreditNumber, string Comment)
        {
            this.ChID = ChID;
            this.CreditID = CreditID;
            this.r_UserID = r_UserID;
            this.LogDate = LogDate;
            this.HistoryType = HistoryType;
            this.CreditNumber = CreditNumber;
            this.Comment = Comment;
        }

        private int _chID = 0;
        public int ChID
        {
            get { return _chID; }
            protected set { _chID = value; }
        }

        private int _creditID = 0;
        public int CreditID
        {
            get { return _creditID; }
            protected set { _creditID = value; }
        }

        private int _ruserID;
        public int r_UserID
        {
            get { return _ruserID; }
            private set { _ruserID = value; }
        }

        private DateTime _logDate;
        public DateTime LogDate
        {
            get { return _logDate; }
            private set { _logDate = value; }
        }

        private string _historyType;
        public string HistoryType
        {
            get { return _historyType; }
            private set { _historyType = value; }
        }

        private int _creditNumber;
        public int CreditNumber
        {
            get { return _creditNumber; }
            private set { _creditNumber = value; }
        }
        
        private string _comment;
        public string Comment
        {
            get { return _comment; }
            private set { _comment = value; }
        }
    }
#endregion
}
    #region SQLPRProvider and PRProvider

    namespace PearlsReview.DAL.SQLClient
    {
        public partial class SQL2PRProvider : DataAccess
        {
            #region SQLPRProvider

            /////////////////////////////////////////////////////////
            // methods that work with CreditHistory

            /// <summary>
            /// Returns the total number of CreditHistory
            /// </summary>
            public int GetCreditHistoryCount()
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("select Count(Chid) from CreditHistory", cn);
                    cn.Open();
                    return (int)ExecuteScalar(cmd);
                }
            }

            /// <summary>
            /// Retrieves all CreditHistory
            /// </summary>
            public List<CreditHistoryInfo> GetCreditHistory(string cSortExpression)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    string cSQLCommand = "select * " +
                        "from CreditHistory";

                    // add on ORDER BY if provided
                    if (cSortExpression.Length > 0)
                    {
                        cSQLCommand = cSQLCommand +
                            " order by " + cSortExpression;
                    }

                    SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                    cn.Open();
                    return GetCreditHistoryCollectionFromReader(ExecuteReader(cmd), false);
                }
            }
            /// <summary>
            /// Retrieves CreditHistory by User
            /// </summary>
            public List<CreditHistoryInfo> GetCreditHistoryByUserId(int UserId)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    string cSQLCommand = "select * " +
                        "from CreditHistory join Credits where creditnumber >0 and expiredate >= getdate() and UserID=@UserID order by cmsid,hourlimit";
                    SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserId;
                    cn.Open();
                    return GetCreditHistoryCollectionFromReader(ExecuteReader(cmd), false);
                }
            }

            /// <summary>
            /// Retrieves the CreditHistory with the specified Credit
            /// </summary>
            public List<CreditHistoryInfo> GetCreditHistoryByCredit(int CreditId)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    string cSQLCommand = "select * " +
                        "from CreditHistory where CreditID=@CreditID";
                    SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                    cmd.Parameters.Add("@CreditID", SqlDbType.Int).Value = CreditId;
                    cn.Open();
                    return GetCreditHistoryCollectionFromReader(ExecuteReader(cmd), false);
                }
            }
            public DataSet GetCreditHistoryByCreditID(int CreditId)
            {
                SqlDataAdapter adapter;
                System.Data.DataSet dsCreditHistory = new DataSet();
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    string cSQLCommand = "SELECT A.*, CASE WHEN B.iid IS NULL THEN '' ELSE B.clastname +', '+ B.cfirstname END RUserFullName "
                    +" FROM CreditHistory A left join UserAccount B on A.r_userid=b.iid WHERE CreditID=@CreditID ORDER BY LogDate Desc";

                    SqlCommand cmd = new SqlCommand(cSQLCommand, cn);                    
                    cmd.Parameters.Add("@CreditId", SqlDbType.Int).Value = CreditId;                    
                    adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dsCreditHistory);
                }
                return dsCreditHistory;
            }
            /// <summary>
            /// Retrieves the CreditHistory with the specified ID
            /// </summary>
            public CreditHistoryInfo GetCreditHistoryByID(int ChID)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand("select * " +
                            "from CreditHistory where ChID=@ChID", cn);
                    cmd.Parameters.Add("@ChID", SqlDbType.Int).Value = ChID;
                    cn.Open();
                    IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                    if (reader.Read())
                        return GetCreditHistoryFromReader(reader, true);
                    else
                        return null;
                }
            }

            /// <summary>
            /// Deletes a CreditHistory
            /// </summary>
            public bool DeleteCreditHistory(int ChID)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("delete from CreditHistory where ChID=@ChID", cn);
                    cmd.Parameters.Add("@ChID", SqlDbType.Int).Value = ChID;
                    cn.Open();
                    int ret = ExecuteNonQuery(cmd);
                    return (ret == 1);
                }
            }

            /// <summary>
            /// Inserts a new Credits
            /// </summary>
            public int InsertCreditHistory(CreditHistoryInfo CreditHistory)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("insert into CreditHistory " +
                  "(CreditID, " +
                  "r_userID, " +
                  "logdate, " +
                  "historyType, " +
                  "creditNumber, " +
                  "comment) " +
                  "VALUES (" +
                  "@CreditID, " +
                  "@r_userID, " +
                  "@logdate, " +
                  "@historyType, " +
                  "@creditNumber, " +
                  "@comment) SET @ID = SCOPE_IDENTITY()", cn);

                    cmd.Parameters.Add("@CreditID", SqlDbType.Int).Value = CreditHistory.CreditID;
                    cmd.Parameters.Add("@r_userID", SqlDbType.Int).Value = CreditHistory.r_UserID;
                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value =
                        (CreditHistory.LogDate == null ? (Object)DBNull.Value : (Object)CreditHistory.LogDate);
                    cmd.Parameters.Add("@historyType", SqlDbType.VarChar).Value = CreditHistory.HistoryType;
                    cmd.Parameters.Add("@creditNumber", SqlDbType.Int).Value = CreditHistory.CreditNumber;
                    cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = CreditHistory.Comment;
                    SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                    IDParameter.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(IDParameter);

                    cn.Open();
                    cmd.ExecuteNonQuery();

                    int NewID = (int)IDParameter.Value;
                    return NewID;

                }
            }


            ///// <summary>
            ///// Inserts a new CreditHistory
            ///// </summary>
            //public int InsertCreditHistory(CreditHistoryInfo CreditHistory)
            //{
            //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            //    {
            //        SqlCommand cmd = new SqlCommand("sp_insert_CreditHistory");
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        cmd.Parameters.Add("@CreditID", SqlDbType.Int).Value = CreditHistory.CreditID;
            //        cmd.Parameters.Add("@r_userid", SqlDbType.Int).Value = CreditHistory.r_UserID;
            //        cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = CreditHistory.LogDate;
            //        cmd.Parameters.Add("@historytype", SqlDbType.VarChar).Value = CreditHistory.HistoryType.Trim();
            //        cmd.Parameters.Add("@creditNumber", SqlDbType.Int).Value = CreditHistory.CreditNumber;
            //        cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = CreditHistory.Comment;                    
            //        SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
            //        IDParameter.Direction = ParameterDirection.InputOutput;
            //        cmd.Parameters.Add(IDParameter);

            //        cn.Open();
            //        cmd.ExecuteNonQuery();

            //        int NewID = (int)IDParameter.Value;
            //        return NewID;
            //        SqlCommand cmd = new SqlCommand("insert into CreditHistory " +
            //"(CreditID, " +
            //"CmsID, " +
            //"HourLimit, " +
            //"Expiredate, " +
            //"CreditNumber) " +
            //"VALUES (" +
            //"@UserID, " +
            //"@CmsID, " +
            //"@HourLimit, " +
            //"@Expiredate, " +
            //"@CreditNumber) SET @ID = SCOPE_IDENTITY()", cn);

            //        cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Credits.UserID;
            //        cmd.Parameters.Add("@CmsID", SqlDbType.Int).Value = Credits.CmsID;
            //        cmd.Parameters.Add("@HourLimit", SqlDbType.Int).Value = Credits.HourLimit;
            //        cmd.Parameters.Add("@Expiredate", SqlDbType.DateTime).Value =
            //            (Credits.Expiredate == null ? (Object)DBNull.Value : (Object)Credits.Expiredate);
            //        cmd.Parameters.Add("@CreditNumber", SqlDbType.Int).Value = Credits.CreditNumber;

            //        SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
            //        IDParameter.Direction = ParameterDirection.Output;
            //        cmd.Parameters.Add(IDParameter);

            //        cn.Open();
            //        cmd.ExecuteNonQuery();

            //        int NewID = (int)IDParameter.Value;
            //        return NewID;
            //    }
            //}



            /// <summary>
            /// Updates a CreditHistory
            /// </summary>
            public bool UpdateCreditHistory(CreditHistoryInfo CreditHistory)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("update CreditHistory set " +
                  "CreditID = @CreditID, " +
                  "r_userID = @r_userID, " +
                  "logdate = @logdate, " +
                  "historyType = @historyType, " +
                  "creditNumber = @creditNumber, " +
                  "comment = @comment " +
                  "where ChID = @ChID ", cn);
                    //            cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Chid", SqlDbType.Int).Value = CreditHistory.ChID;
                    cmd.Parameters.Add("@CreditID", SqlDbType.Int).Value = CreditHistory.CreditID;
                    cmd.Parameters.Add("@r_userID", SqlDbType.Int).Value = CreditHistory.r_UserID;
                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = CreditHistory.LogDate;
                    cmd.Parameters.Add("@historyType", SqlDbType.VarChar).Value = CreditHistory.HistoryType;
                    cmd.Parameters.Add("@creditNumber", SqlDbType.VarChar).Value = CreditHistory.CreditNumber;
                    cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = CreditHistory.Comment;

                    cn.Open();
                    int ret = ExecuteNonQuery(cmd);
                    return (ret == 1);
                }
            }
            #endregion

            #region PRProvider

            /// <summary>
            /// Returns a new CreditHistoryInfo instance filled with the DataReader's current record data
            /// </summary>
            protected virtual CreditHistoryInfo GetCreditHistoryFromReader(IDataReader reader)
            {  
                return GetCreditHistoryFromReader(reader, true);
            }
            protected virtual CreditHistoryInfo GetCreditHistoryFromReader(IDataReader reader, bool readMemos)
            {
                CreditHistoryInfo CreditHistory = new CreditHistoryInfo(
                  (int)reader["ChID"],
                  (int)reader["CreditID"],
                  (int)reader["r_userID"],
                  (DateTime)reader["logdate"],
                  (string)reader["HistoryType"],
                  (int)reader["CreditNumber"],
                  (string)reader["Comment"]);
                return CreditHistory;
            }

            /// <summary>
            /// Returns a collection of CreditHistoryInfo objects with the data read from the input DataReader
            /// </summary>
            protected virtual List<CreditHistoryInfo> GetCreditHistoryCollectionFromReader(IDataReader reader)
            {
                return GetCreditHistoryCollectionFromReader(reader, true);
            }
            protected virtual List<CreditHistoryInfo> GetCreditHistoryCollectionFromReader(IDataReader reader, bool readMemos)
            {
                List<CreditHistoryInfo> CreditHistory = new List<CreditHistoryInfo>();
                while (reader.Read())
                    CreditHistory.Add(GetCreditHistoryFromReader(reader, readMemos));
                return CreditHistory;
            }
            #endregion
        }
    }
#endregion