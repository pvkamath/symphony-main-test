﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;


#region SponsorInfo

namespace PearlsReview.DAL
{
    public class SponsorInfo
    {
        public SponsorInfo() { }

        public SponsorInfo(int SponsorID, string SponsorName, string UserName, string UserPassword, bool Survey_Ind)
        {
            this.SponsorID = SponsorID;
            this.SponsorName = SponsorName;
            this.UserName = UserName;
            this.UserPassword = UserPassword;
            this.Survey_Ind = Survey_Ind;
        }

        private int _SponsorID = 0;
        public int SponsorID
        {
            get { return _SponsorID; }
            protected set { _SponsorID = value; }
        }

        private string _SponsorName = "";
        public string SponsorName
        {
            get { return _SponsorName; }
            private set { _SponsorName = value; }
        }

        private string _UserName = "";
        public string UserName
        {
            get { return _UserName; }
            private set { _UserName = value; }
        }

        private string _UserPassword = "";
        public string UserPassword
        {
            get { return _UserPassword; }
            private set { _UserPassword = value; }
        }

        private bool _Survey_Ind =  true;
        public bool Survey_Ind
        {
            get { return _Survey_Ind; }
            private set { _Survey_Ind = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Sponsors

        

        /// <summary>
        /// Retrieves all Sponsors
        /// </summary>
        public List<SponsorInfo> GetSponsors(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Sponsor";
                
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetSponsorCollectionFromReader(ExecuteReader(cmd), false);
            }
        }        

        /// <summary>
        /// Retrieves the Sponsor with the specified ID
        /// </summary>
        public SponsorInfo GetSponsorByID(int SponsorID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Sponsor where SponsorID=@SponsorID", cn);
                cmd.Parameters.Add("@SponsorID", SqlDbType.Int).Value = SponsorID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSponsorFromReader(reader, true);
                else
                    return null;
            }
        }
        public List<SponsorInfo> GetUnSelectedSponsorByInitID(int InitID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = String.Format("select * from sponsor where sponsorid not in (select distinct sponsorid from SponsorList where initid = {0})", InitID);
                string cSQLCommand = "select * from sponsor";
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetSponsorCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public List<SponsorInfo> GetSponsorByInitiatvieID(int InitID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = String.Format("select * from Sponsor where SponsorID in (select distinct sponsorid from sponsorlist where initid = {0})", InitID);

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetSponsorCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Deletes a Sponsor
        /// </summary>
        public bool DeleteSponsor(int SponsorID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Sponsor where SponsorID=@SponsorID", cn);
                cmd.Parameters.Add("@SponsorID", SqlDbType.Int).Value = SponsorID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);

                SqlCommand cmd1 = new SqlCommand("delete from SponsorList where SponsorID=@SponsorID", cn);
                cmd1.Parameters.Add("@SponsorID", SqlDbType.Int).Value = SponsorID;                
                ExecuteNonQuery(cmd1);

                return (ret == 1);

                
            }
        }

        /// <summary>
        /// Inserts a new Sponsor
        /// </summary>
        public int InsertSponsor(SponsorInfo Sponsor)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Sponsor " +
                "(SponsorName, " +
                "UserName, " +
                "UserPassword, " +
                "Survey_Ind ) " +
                "VALUES (" +
                "@SponsorName, " +
                "@UserName, " +
                "@UserPassword, " +
                "@Survey_Ind ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@SponsorName", SqlDbType.VarChar).Value = Sponsor.SponsorName;
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Sponsor.UserName;
                cmd.Parameters.Add("@UserPassword", SqlDbType.Char).Value = Sponsor.UserPassword;
                cmd.Parameters.Add("@Survey_Ind", SqlDbType.Bit).Value = Sponsor.Survey_Ind;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Sponsor
        /// </summary>
        public bool UpdateSponsor(SponsorInfo Sponsor)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Sponsor set " +
              "SponsorName = @SponsorName, " +
              "UserName = @UserName, " +
              "UserPassword = @UserPassword, " +
              "Survey_Ind = @Survey_Ind " +
              "where SponsorID = @SponsorID ", cn);
               
                cmd.Parameters.Add("@SponsorName", SqlDbType.VarChar).Value = Sponsor.SponsorName;
                cmd.Parameters.Add("@UserName", SqlDbType.Text).Value = Sponsor.UserName;
                cmd.Parameters.Add("@UserPassword", SqlDbType.Char).Value = Sponsor.UserPassword;
                cmd.Parameters.Add("@Survey_Ind", SqlDbType.Bit).Value = Sponsor.Survey_Ind;
                cmd.Parameters.Add("@SponsorID", SqlDbType.Int).Value = Sponsor.SponsorID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new SponsorInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual SponsorInfo GetSponsorFromReader(IDataReader reader)
        {
            return GetSponsorFromReader(reader, true);
        }
        protected virtual SponsorInfo GetSponsorFromReader(IDataReader reader, bool readMemos)
        {
            SponsorInfo Sponsor = new SponsorInfo(
              (int)reader["SponsorID"],
              reader["SponsorName"].ToString(),
              reader["UserName"].ToString(),
              reader["UserPassword"].ToString(),
              (bool)reader["Survey_Ind"]);

            return Sponsor;
        }

        /// <summary>
        /// Returns a collection of SponsorInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<SponsorInfo> GetSponsorCollectionFromReader(IDataReader reader)
        {
            return GetSponsorCollectionFromReader(reader, true);
        }
        protected virtual List<SponsorInfo> GetSponsorCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<SponsorInfo> Sponsors = new List<SponsorInfo>();
            while (reader.Read())
                Sponsors.Add(GetSponsorFromReader(reader, readMemos));
            return Sponsors;
        }
        #endregion
    }
}
#endregion
