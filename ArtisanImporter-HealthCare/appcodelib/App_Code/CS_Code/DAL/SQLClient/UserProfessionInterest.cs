﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for UserProfessionInterestSQL
/// </summary>
#region UserProfessionInterestInfo
/// 
namespace PearlsReview.DAL
{
    public class UserProfessionInterestInfo
    {
        public UserProfessionInterestInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Variables and Properties

        private int _userpiid = 0;
        public int userpiid
        {
            get { return _userpiid; }
            protected set { _userpiid = value; }
        }
        public int _userdspid = 0;
        public int userdspid
        {
            get { return _userdspid; }
            set { _userdspid = value; }
        }
        public int _miid = 0;
        public int miid
        {
            get { return _miid; }
            set { _miid = value; }
        }
      
        public UserProfessionInterestInfo(int userpiid, int userdspid, int miid)
        {
            this.userpiid = userpiid;
            this.userdspid = userdspid;
            this.miid = miid;            
        }

        #endregion
    }
}

#endregion
#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        /// <summary>
        /// Inserts a new MicrositeInterest
        /// </summary>
        public int InsertUserProfessionInterest(UserProfessionInterestInfo UserProfessionInterest)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into UserProfessionInterest " +
                "( userdspid, " +                
                "miid ) " +
                "VALUES ( " +
                "@userdspid, " +                
                "@miid ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@userdspid", SqlDbType.Int).Value = UserProfessionInterest.userdspid;
                cmd.Parameters.Add("@miid", SqlDbType.Int).Value = UserProfessionInterest.miid;               
                cn.Open();
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;
            }
        }


        /// <summary>
        /// Updates a UserProfessionInterest
        /// </summary>
        public bool UpdateUserProfessionInterest(UserProfessionInterestInfo UserProfessionInterest)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserProfessionInterest set " +
              "userdspid = @userdspid, " +              
              "miid = @miid " +
              " where userpiid = @userpiid ", cn);
                cmd.Parameters.Add("@userdspid", SqlDbType.Int).Value = UserProfessionInterest.userdspid;
                cmd.Parameters.Add("@miid", SqlDbType.Int).Value = UserProfessionInterest.miid;                
                cmd.Parameters.Add("@userpiid", SqlDbType.Int).Value = UserProfessionInterest.userpiid;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        /// <summary>
        /// Retrieves all UserProfessionInterest for the specified userdspid
        /// </summary>
        public List<UserProfessionInterestInfo> GetUserProfessionInterestsByUserdspid(int userdspid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "userpiid, " +
                    "userdspid, " +                    
                    "miid " +
                    "from UserProfessionInterest " +
                    "where userdspid = @userdspid " +
                    "order by " + cSortExpression;

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@userdspid", SqlDbType.Int).Value = userdspid;
                cn.Open();
                return GetUserProfessionInterestCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Deletes a UserProfessionInterest
        /// </summary>
        public bool DeleteAllUserProfessionInterestsByUserdspid(int userdspid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from UserProfessionInterest where userdspid=@userdspid", cn);
                cmd.Parameters.Add("@userdspid", SqlDbType.Int).Value = userdspid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider

        protected virtual UserProfessionInterestInfo GetUserProfessionInterestFromReader(IDataReader reader)
        {
            return GetUserProfessionInterestFromReader(reader, true);
        }
        protected virtual UserProfessionInterestInfo GetUserProfessionInterestFromReader(IDataReader reader, bool readMemos)
        {
            UserProfessionInterestInfo UserProfessionInterest = new UserProfessionInterestInfo(
              (int)reader["userpiid"],
              (int)reader["userdspid"],              
              (int)reader["miid"]);

            return UserProfessionInterest;
        }

        protected virtual List<UserProfessionInterestInfo> GetUserProfessionInterestCollectionFromReader(IDataReader reader)
        {
            return GetUserProfessionInterestCollectionFromReader(reader, true);
        }
        protected virtual List<UserProfessionInterestInfo> GetUserProfessionInterestCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<UserProfessionInterestInfo> UserProfessionInterests = new List<UserProfessionInterestInfo>();
            while (reader.Read())
                UserProfessionInterests.Add(GetUserProfessionInterestFromReader(reader, readMemos));
            return UserProfessionInterests;
        }

        #endregion

    }
}
#endregion