using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Caching;

namespace PearlsReview.DAL.SQLClient
{
   public partial class SQLPRProvider : PRProvider
   {
       /////////////////////////////////////////
       /// <summary>
       /// / checks the status of connection
       /// </summary>
       /// <returns>
       /// true if connection is successfull , false if the connection is broken
       /// </returns>

       public bool CheckConnection()
       {
           using (SqlConnection cn = new SqlConnection(this.RConnectionString))
           {
               cn.Open();
               if (cn.State == ConnectionState.Open)
               {
                   cn.Close();
                   return true;
               }
               else
               {
                   cn.Close();
                   return false;
               }
               
           }
       }
//      /////////////////////////////////////////////////////////
//      // methods that work with AdClicks

//      /// <summary>
//      /// Returns the total number of AdClicks
//      /// </summary>
//      public override int GetAdClickCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from AdClick", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all AdClicks
//      /// </summary>
//      public override List<AdClickInfo> GetAdClicks(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from AdClick";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetAdClickCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the AdClick with the specified ID
//      /// </summary>
//      public override AdClickInfo GetAdClickByID(int AdClickID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from AdClick where AdClickID=@AdClickID", cn);
//            cmd.Parameters.Add("@AdClickID", SqlDbType.Int).Value = AdClickID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetAdClickFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a AdClick
//      /// </summary>
//      public override bool DeleteAdClick(int AdClickID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from AdClick where AdClickID=@AdClickID", cn);
//            cmd.Parameters.Add("@AdClickID", SqlDbType.Int).Value = AdClickID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new AdClick
//      /// </summary>
//      public override int InsertAdClick(AdClickInfo AdClick)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into AdClick " +
//            "(AdID, " +
//            "Date, " +
//            "PageName) " +
//            "VALUES (" +
//            "@AdID, " +
//            "@Date, " +
//            "@PageName) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = AdClick.AdID ;
//            cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = AdClick.Date ;
//            cmd.Parameters.Add("@PageName", SqlDbType.VarChar).Value = AdClick.PageName ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a AdClick
//      /// </summary>
//      public override bool UpdateAdClick(AdClickInfo AdClick)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update AdClick set " +
//            "AdID = @AdID, " +
//            "Date = @Date, " +
//            "PageName = @PageName " +
//            "where AdClickID = @AdClickID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = AdClick.AdID ;
//            cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = AdClick.Date ;
//            cmd.Parameters.Add("@PageName", SqlDbType.VarChar).Value = AdClick.PageName ;
//            cmd.Parameters.Add("@AdClickID", SqlDbType.Int).Value = AdClick.AdClickID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with Advertisements

//      /// <summary>
//      /// Returns the total number of Advertisements
//      /// </summary>
//      public override int GetAdvertisementCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Advertisement", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Advertisements
//      /// </summary>
//      public override List<AdvertisementInfo> GetAdvertisements(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Advertisement";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetAdvertisementCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the Advertisement with the specified ID
//      /// </summary>
//      public override AdvertisementInfo GetAdvertisementByID(int AdID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from Advertisement where AdID=@AdID", cn);
//            cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = AdID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetAdvertisementFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a Advertisement
//      /// </summary>
//      public override bool DeleteAdvertisement(int AdID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from Advertisement where AdID=@AdID", cn);
//            cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = AdID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new Advertisement
//      /// </summary>
//      public override int InsertAdvertisement(AdvertisementInfo Advertisement)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into Advertisement " +
//            "(AdName, " +
//            "AdType, " +
//            "AdDest, " +
//            "AdText, " +
//            "AdFilename, " +
//            "AdUnitType) " +
//            "VALUES (" +
//            "@AdName, " +
//            "@AdType, " +
//            "@AdDest, " +
//            "@AdText, " +
//            "@AdFilename, " +
//            "@AdUnitType) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@AdName", SqlDbType.VarChar).Value = Advertisement.AdName ;
//            cmd.Parameters.Add("@AdType", SqlDbType.VarChar).Value = Advertisement.AdType ;
//            cmd.Parameters.Add("@AdDest", SqlDbType.VarChar).Value = Advertisement.AdDest ;
//            cmd.Parameters.Add("@AdText", SqlDbType.VarChar).Value = Advertisement.AdText ;
//            cmd.Parameters.Add("@AdFilename", SqlDbType.VarChar).Value = Advertisement.AdFilename ;
//            cmd.Parameters.Add("@AdUnitType", SqlDbType.VarChar).Value = Advertisement.AdUnitType ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a Advertisement
//      /// </summary>
//      public override bool UpdateAdvertisement(AdvertisementInfo Advertisement)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Advertisement set " +
//            "AdName = @AdName, " +
//            "AdType = @AdType, " +
//            "AdDest = @AdDest, " +
//            "AdText = @AdText, " +
//            "AdFilename = @AdFilename, " +
//            "AdUnitType = @AdUnitType " +
//            "where AdID = @AdID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@AdName", SqlDbType.VarChar).Value = Advertisement.AdName ;
//            cmd.Parameters.Add("@AdType", SqlDbType.VarChar).Value = Advertisement.AdType ;
//            cmd.Parameters.Add("@AdDest", SqlDbType.VarChar).Value = Advertisement.AdDest ;
//            cmd.Parameters.Add("@AdText", SqlDbType.VarChar).Value = Advertisement.AdText ;
//            cmd.Parameters.Add("@AdFilename", SqlDbType.VarChar).Value = Advertisement.AdFilename ;
//            cmd.Parameters.Add("@AdUnitType", SqlDbType.VarChar).Value = Advertisement.AdUnitType ;
//            cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = Advertisement.AdID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//       /////////////////////////////////////////////////////////
//      // methods that work with Categories

//      /// <summary>
//      /// Returns the total number of Categories
//      /// </summary>
//      public override int GetCategoryCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Categories where FacilityID=0 ", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of Textbook Categories
//      /// </summary>
//      public override int GetTextbookCategoryCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Categories where Textbook='1' and FacilityID=0 ", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of Non-Textbook Categories
//      /// </summary>
//      public override int GetNonTextbookCategoryCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Categories where Textbook='0' and FacilityID=0 ", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Categories
//      /// </summary>
//      public override List<CategoryInfo> GetCategories(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Categories where FacilityID=0 ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }
      

//      /// <summary>
//      /// Retrieves all ComplianceCategories
//      /// </summary>
//      public override List<CategoryInfo> GetComplianceCategories(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Categories where title like 'Compliance%' and FacilityID=0 ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//       /// <summary>
//      /// Retrieves all Textbook Categories
//      /// </summary>
//      public override List<CategoryInfo> GetTextbookCategories(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Categories where textbook='1' and FacilityID=0 ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Non-Textbook Categories
//      /// </summary>
//      public override List<CategoryInfo> GetNonTextbookCategories(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Categories where textbook='0' ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Textbook Categories that should be displayed to the website users
//      /// (those with ShowInList true)
//      /// </summary>
//      public override List<CategoryInfo> GetTextbookCategoriesForDisplay(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Categories where textbook='1' and showinlist='1' and FacilityID=0 ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Non-Textbook Categories that should be displayed to the website users
//      /// (those with ShowInList true)
//      /// </summary>
//      public override List<CategoryInfo> GetNonTextbookCategoriesForDisplay(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Categories where textbook='0' and showinlist='1' and FacilityID=0 ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Non-Textbook Categories with topic counts that should be displayed to the website users
//      /// (those with ShowInList true)
//      /// </summary>
//      public override List<CategoryTopicCountInfo> GetNonTextbookCategoriesWithTopicCount(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "categories.id, " +
//                  "categories.title, " +
//                  "categories.title As DTitle, " +
//                  "(select Cast(count(*) as int)" +
//                  "   from topic " +
//                  "   join categorylink on Topic.topicid = categorylink.topicid " +
//                  "   where categorylink.CategoryID = Categories.ID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
//                  "   group by categorylink.categoryid )  As topiccount " + 
//                  "from Categories where textbook='0' and showinlist='1' and FacilityID=0 " +
//                  " and id in (select categoryid from categorylink " +
//                  "    join topic on categorylink.topicid = topic.topicid " +
//                  "    where Topic.Obsolete = '0' and Topic.Active_Ind = '1' ) " ;

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }



//      /// <summary>
//      /// Retrieves all Non-Textbook Categories with topic counts that should be displayed to the website users
//      /// (those with ShowInList true)
//      /// </summary>
//      public override List<CategoryTopicCountInfo> GetRNonTextbookCategoriesWithTopicCount(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "c.website As DTitle ," +
//                  "c.id,c.title+ '  ('+convert(varchar(3),count(*)) +')' as title,count(*) As topiccount" +
//                  "   from categories c  join categorylink cl on c.id=cl.categoryid  " +
//                 "   join topic t on cl.topicid=t.topicid " +
//                 " join categoryarealink ca on cl.categoryid=ca.categoryid" +
//                  "   where c.textbook='0' and c.showinlist='1' and c.FacilityID=0 and ca.areaid='2'  " +
//                  "and t.Obsolete = '0' and t.avail_ind = '1' and c.buildpage <> '1'" +
//                  "  group by c.id ,c.website,c.title";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//          /// <summary>
//      /// Retrieves all Non-Textbook State Specific Categories with topic counts that should be displayed to the website users
//      /// (those with ShowInList true)
//      /// </summary>
//      public override List<CategoryTopicCountInfo> GetRNonTextbookStateCategoriesWithTopicCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand();
//              cmd.Connection = cn;
//              cn.Open();
//              cmd.CommandText = "sp_State_CE";
//              cmd.CommandType = CommandType.StoredProcedure;
//              return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }
            

//        /// <summary>
//      /// Retrieves all Non-Textbook Categories with topic counts that should be displayed to the website users by facility id
//      /// (those with ShowInList true)
//      /// </summary>
//      public override List<CategoryTopicCountInfo> GetNonTextbookCategoriesWithTopicCountByFacilityId(int facilityid,string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "categories.id, " +
//                  "categories.title, " +
//                  "categories.title as DTitle, " +
//                  "(select Cast(count(*) as int)" +
//                  "   from topic " +
//                  "   join categorylink on Topic.topicid = categorylink.topicid " +
//                  "   where categorylink.CategoryID = Categories.ID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
//                  "   group by categorylink.categoryid )  As topiccount " +
//                  "from Categories where textbook='0' and showinlist='1' and FacilityID=@facilityid " +
//                  " and id in (select categoryid from categorylink " +
//                  "    join topic on categorylink.topicid = topic.topicid " +
//                  "    where Topic.Obsolete = '0' and Topic.Active_Ind = '1' ) " ;

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
              
//              cn.Open();
//              return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }



//      /// <summary>
//      /// Retrieves all Non-Textbook Categories with topic counts that should be displayed to the website users by facility id
//      /// (those with ShowInList true)
//      /// </summary>
//      public override List<CategoryTopicCountInfo> GetTextbookCategoriesWithTopicCountByFacilityId(int facilityid, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "categories.id, " +
//                  "categories.title, " +
//                  "(select Cast(count(*) as int)" +
//                  "   from topic " +
//                  "   join categorylink on Topic.topicid = categorylink.topicid " +
//                  "   where categorylink.CategoryID = Categories.ID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
//                  "   group by categorylink.categoryid )  As topiccount " +
//                  "from Categories where textbook='1' and showinlist='1' and FacilityID=@facilityid " +
//                  " and id in (select categoryid from categorylink " +
//                  "    join topic on categorylink.topicid = topic.topicid " +
//                  "    where Topic.Obsolete = '0' and Topic.Active_Ind = '1' ) ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;

//              cn.Open();
//              return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


      

//      /// <summary>
//      /// Retrieves the Category with the specified ID
//      /// </summary>
//      public override CategoryInfo GetCategoryByID(int ID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from Categories where ID=@ID", cn);
//            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetCategoryFromReader(reader, true);
//              else
//                  return null;
//          }
//      }


//       public override CategoryInfo GetCatNameByTopicId(int TopicId)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * from categories inner join categorylink on categories.id=categorylink.categoryid where categorylink.topicid=@TopicId", cn);
//            cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetCategoryFromReader(reader, true);
//              else
//                  return null;
//          }

//       }

//       public override CategoryInfo GetPrimaryCatNameByTopicId(int TopicId)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {

//               SqlCommand cmd = new SqlCommand("select * from categories where id in ( select categoryid from topic where topicid=@TopicId )", cn);
//               cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
//               cn.Open();
//               IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//               if (reader.Read())
//                   return GetCategoryFromReader(reader, true);
//               else
//                   return null;
//           }

//       }

//       public override string CheckCatNameByTopicId(int TopicId, string CatName)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {

//               SqlCommand cmd = new SqlCommand("select website from categories inner join categorylink on categories.id=categorylink.categoryid where categorylink.topicid=@TopicId and categories.website=@Title", cn);
//               cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
//               cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = CatName;
//               cn.Open();
//               IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//               if (reader.Read())
//                   return reader[0].ToString();
//               else
//                   return null;
//           }
//       }


//      public override DataSet GetCategoryByWebSite(string website)
//      {
//          DataSet ds= new DataSet();
//          SqlDataAdapter da ;
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from Categories inner join categoryarealink on categories.id=categoryarealink.categoryid where website=@website and areaid=2", cn);
//              cmd.Parameters.Add("@website", SqlDbType.VarChar).Value = website;
//              cn.Open();
//              cmd.CommandType = CommandType.Text;
//              da = new SqlDataAdapter(cmd);
//              da.Fill(ds);
//              if (ds == null)
//              {
//                  return null;
//              }
//              else
//              {
//                  return ds;
//              }
//          }
//      }

//      /// <summary>
//      /// Deletes a Category
//      /// </summary>
//      public override bool DeleteCategory(int ID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from Categories where ID=@ID", cn);
//            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new Category
//      /// </summary>
//      public override int InsertCategory(CategoryInfo Category)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into Categories " +
//            "(Title, " +
//            "BuildPage, " +
//            "MetaKW, " +
//            "MetaDesc, " +
//            "MetaTitle, " +
//            "PageText, " +
//            "Textbook, " +
//            "ShowInList, " +
//            "CertName, " +
//            "CredAward, " +
//            "ExamType, " +
//            "ExamCost, " +
//            "AdminOrg, " +
//            "Website, " +
//            "Reqments, " +
//            "EligCrit, " +
//              "FacilityID) " +
//            "VALUES (" +
//            "@Title, " +
//            "@BuildPage, " +
//            "@MetaKW, " +
//            "@MetaDesc, " +
//            "@MetaTitle, " +
//            "@PageText, " +
//            "@Textbook, " +
//            "@ShowInList, " +
//            "@CertName, " +
//            "@CredAward, " +
//            "@ExamType, " +
//            "@ExamCost, " +
//            "@AdminOrg, " +
//            "@Website, " +
//            "@Reqments, " +
//            "@EligCrit, " +
//              "@FacilityID ) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = Category.Title ;
//            cmd.Parameters.Add("@BuildPage", SqlDbType.Bit).Value = Category.BuildPage ;
//            cmd.Parameters.Add("@MetaKW", SqlDbType.VarChar).Value = Category.MetaKW ;
//            cmd.Parameters.Add("@MetaDesc", SqlDbType.VarChar).Value = Category.MetaDesc ;
//            cmd.Parameters.Add("@MetaTitle", SqlDbType.VarChar).Value = Category.MetaTitle ;
//            cmd.Parameters.Add("@PageText", SqlDbType.VarChar).Value = Category.PageText ;
//            cmd.Parameters.Add("@Textbook", SqlDbType.Bit).Value = Category.Textbook;
//            cmd.Parameters.Add("@ShowInList", SqlDbType.Bit).Value = Category.ShowInList;
//            cmd.Parameters.Add("@CertName", SqlDbType.VarChar).Value = Category.CertName;
//            cmd.Parameters.Add("@CredAward", SqlDbType.VarChar).Value = Category.CredAward;
//            cmd.Parameters.Add("@ExamType", SqlDbType.VarChar).Value = Category.ExamType;
//            cmd.Parameters.Add("@ExamCost", SqlDbType.VarChar).Value = Category.ExamCost;
//            cmd.Parameters.Add("@AdminOrg", SqlDbType.VarChar).Value = Category.AdminOrg;
//            cmd.Parameters.Add("@Website", SqlDbType.VarChar).Value = Category.Website;
//            cmd.Parameters.Add("@Reqments", SqlDbType.VarChar).Value = Category.Reqments;
//            cmd.Parameters.Add("@EligCrit", SqlDbType.VarChar).Value = Category.EligCrit;
//            cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = Category.FacilityID;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a Category
//      /// </summary>
//      public override bool UpdateCategory(CategoryInfo Category)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Categories set " +
//            "Title = @Title, " +
//            "BuildPage = @BuildPage, " +
//            "MetaKW = @MetaKW, " +
//            "MetaDesc = @MetaDesc, " +
//            "MetaTitle = @MetaTitle, " +
//            "PageText = @PageText, " +
//            "Textbook = @Textbook, " +
//            "ShowInList = @ShowInList, " +
//            "CertName = @CertName, " +
//            "CredAward = @CredAward, " +
//            "ExamType = @ExamType, " +
//            "ExamCost = @ExamCost, " +
//            "AdminOrg = @AdminOrg, " +
//            "Website = @Website, " +
//            "Reqments = @Reqments, " +
//            "EligCrit = @EligCrit, " +
//            "FacilityID = @FacilityID " +
//            "where ID = @ID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = Category.Title;
//              cmd.Parameters.Add("@BuildPage", SqlDbType.Bit).Value = Category.BuildPage;
//              cmd.Parameters.Add("@MetaKW", SqlDbType.VarChar).Value = Category.MetaKW;
//              cmd.Parameters.Add("@MetaDesc", SqlDbType.VarChar).Value = Category.MetaDesc;
//              cmd.Parameters.Add("@MetaTitle", SqlDbType.VarChar).Value = Category.MetaTitle;
//              cmd.Parameters.Add("@PageText", SqlDbType.VarChar).Value = Category.PageText;
//              cmd.Parameters.Add("@Textbook", SqlDbType.Bit).Value = Category.Textbook;
//              cmd.Parameters.Add("@ShowInList", SqlDbType.Bit).Value = Category.ShowInList;
//              cmd.Parameters.Add("@CertName", SqlDbType.VarChar).Value = Category.CertName;
//              cmd.Parameters.Add("@CredAward", SqlDbType.VarChar).Value = Category.CredAward;
//              cmd.Parameters.Add("@ExamType", SqlDbType.VarChar).Value = Category.ExamType;
//              cmd.Parameters.Add("@ExamCost", SqlDbType.VarChar).Value = Category.ExamCost;
//              cmd.Parameters.Add("@AdminOrg", SqlDbType.VarChar).Value = Category.AdminOrg;
//              cmd.Parameters.Add("@Website", SqlDbType.VarChar).Value = Category.Website;
//              cmd.Parameters.Add("@Reqments", SqlDbType.VarChar).Value = Category.Reqments;
//              cmd.Parameters.Add("@EligCrit", SqlDbType.VarChar).Value = Category.EligCrit;
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = Category.FacilityID;
//              cmd.Parameters.Add("@ID", SqlDbType.Int).Value = Category.ID;

//          //    string cBuildPage = "'0'";
//          //  if (Category.BuildPage == true) 
//          //  {cBuildPage = "'1'";}

//          //    SqlCommand cmd = new SqlCommand("update Categories set " +
//          //"Title = '" + Category.Title + "', " +
//          //"Order = " + Category.Order.ToString() + ", " +
//          //"BuildPage = " + cBuildPage + ", " +
//          //"MetaKW = '" + Category.MetaKW + "', " +
//          //"MetaDesc = '" + Category.MetaDesc + "', " +
//          //"MetaTitle = '" + Category.MetaTitle + "', " +
//          //"PageText = '" + Category.PageText + "' " + 
//          //"where ID = " + Category.ID.ToString(), cn);

//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of categories for a TopicID
//      /// </summary>
//      public override int GetCategoryCountByTopicID(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from categories " +
//                  "join categorylink on categories.id = categorylink.categoryid " +
//                  "where categorylink.TopicID = @TopicID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      ///// <summary>
//      ///// Retrieves all categories, then selects just 4th to 6th items
//      ///// </summary>
//      //public override List<CategoryInfo> GetCategoriesX(string cSortExpression)
//      //{
//      //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//      //    {
//      //        string cSQLCommand = "select * from (select " +
//      //            "ID, " +
//      //            "Title, " +
//      //            "Order " +
//      //            "from categories";

//      //        // add on ORDER BY if provided
//      //        if (cSortExpression.Length > 0)
//      //        {
//      //            cSQLCommand = cSQLCommand +
//      //                " order by " + cSortExpression;
//      //        }

//      //        cSQLCommand = cSQLCommand +
//      //            ") catlist where recno() > 3 and recno() < 7 " ;

//      //        SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//      //        cn.Open();
//      //        return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
//      //    }
//      //}

//      /// <summary>
//      /// Retrieves all categories for a TopicID
//      /// </summary>
//      public override List<CategoryInfo> GetCategoriesByTopicID(int TopicID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * from categories " +
//                  "join categorylink on categories.id = categorylink.categoryid " +
//                  "where categorylink.TopicID = @TopicID";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all categories, plus topics assignments for a TopicID
//      /// </summary>
//      public override List<CheckBoxListCategoryInfo> GetAllCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select ca.ID, ca.Title, " +
//                  "CAST( (CASE WHEN (cl.TopicID IS NULL) THEN '0' ELSE '1' END) As bit ) AS Selected from categories ca " +
//                  "left outer join categorylink cl on ca.id = cl.categoryid " +
//                  "and cl.TopicID = @TopicID where ca.FacilityID=0 ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return GetCheckBoxListCategoryCollectionFromReader(ExecuteReader(cmd));
//          }
//      }

//      /// <summary>
//      /// Retrieves all non-textbook categories, plus topics assignments for a TopicID
//      /// </summary>
//      public override List<CheckBoxListCategoryInfo> GetAllNonTextbookCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select ca.ID, ca.Title, " +
//                  "CAST( (CASE WHEN (cl.TopicID IS NULL) THEN '0' ELSE '1' END) As bit ) AS Selected from categories ca " +
//                  "left outer join categorylink cl on ca.id = cl.categoryid " +
//                  "and cl.TopicID = @TopicID where ca.textbook='0' and ca.FacilityID=(select topic.facilityid from " +
//                  "topic where topic.topicid=@TopicID)";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return GetCheckBoxListCategoryCollectionFromReader(ExecuteReader(cmd));
//          }
//      }

//      /// <summary>
//      /// Retrieves all textbook categories, plus topics assignments for a TopicID
//      /// </summary>
//      public override List<CheckBoxListCategoryInfo> GetAllTextbookCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              //string cSQLCommand = "select ca.ID, ca.Title, " +
//              //    "IIF(ISNULL(cl.TopicID), '0', '1') AS Selected from categories ca " +
//              //    "left outer join categorylink cl on ca.id = cl.categoryid " +
//              //    "and cl.TopicID = ? where ca.textbook='1'";
//              string cSQLCommand = "select ca.ID, ca.Title, " +
//                  "CAST( (CASE WHEN (cl.TopicID IS NULL) THEN '0' ELSE '1' END) As bit ) AS Selected from categories ca " +
//                  "left outer join categorylink cl on ca.id = cl.categoryid " +
//                  "and cl.TopicID = @TopicID where ca.textbook='1' and ca.FacilityID=0 ";


//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return GetCheckBoxListCategoryCollectionFromReader(ExecuteReader(cmd));
//          }
//      }

//      /// <summary>
//      /// Retrieves all textbook categories NOT assigned to a specific TopicID
//      /// </summary>
//      public override List<CategoryInfo> GetTextbookCategoriesNotAssignedToTopicID(int TopicID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * from categories " +
//                  "where textbook='1' and ca.FacilityID=0 and id not in " +
//                  "(select categoryid from categorylink " +
//                  "where categorylink.TopicID = @TopicID)";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all non-textbook categories NOT assigned to a specific TopicID
//      /// </summary>
//      public override List<CategoryInfo> GetNonTextbookCategoriesNotAssignedToTopicID(int TopicID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * from categories " +
//                  "where textbook='0' and ca.FacilityID=0 and id not in " +
//                  "(select categoryid from categorylink " +
//                  "where categorylink.TopicID = @TopicID)";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Assign a a topic to a category
//      /// </summary>
//      public override bool AssignTopicToCategory(int TopicID, int CategoryID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand(
//                  "select Count(*) from categorylink " +
//                  "where TopicID = @TopicID and CategoryID = @CategoryID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//              cn.Open();
//              int ResultCount = (int)ExecuteScalar(cmd);

//              if (ResultCount > 1)
//                  // link already exists, so nothing to do
//                  return true;

//              // link does not exist, so insert it
//              SqlCommand cmd2 = new SqlCommand("insert into categorylink " +
//                  "(CategoryID, " +
//                  "TopicID) " +
//                  "VALUES (" +
//                  "@CategoryID, " +
//                  "@TopicID)", cn);

//              cmd2.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//              cmd2.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

//              int ret = ExecuteNonQuery(cmd2);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Detach a topic assignment from a category
//      /// </summary>
//      public override bool DetachTopicFromCategory(int TopicID, int CategoryID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand(
//                  "delete from categorylink " +
//                  "where TopicID = @TopicID and CategoryID = @CategoryID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Assign topics to category from comma-delimited list
//      /// </summary>
//      public override bool UpdateCategoryTopicAssignments(int CategoryID, string TopicIDAssignments)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              // first delete any assignments that are not in the list passed in
////              SqlCommand cmd = new SqlCommand(
////                  "DELETE FROM categorylink " +
////                  "where CategoryID = ? and TopicID NOT IN ("+TopicIDAssignments.Trim()+")", cn);

//              // NOTE: For now, just delete them all
//              SqlCommand cmd = new SqlCommand(
//                  "DELETE FROM categorylink " +
//                  "where CategoryID = @CategoryID ", cn);
              
//              cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;

////              cmd.Parameters.Add("@TopicIDAssignments", SqlDbType.VarChar).Value = TopicIDAssignments;

//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
////              return (ret == 1);

//              if (TopicIDAssignments.Trim().Length < 1)
//                  // no assignments, so nothing to do
//                  return true;

//              // now step through all TopicIDs in the list passed in and call
//              // this.AssignTopicToCategory() for each, which will insert the assignment
//              // if it does not already exist
//              string[] TopicIDs = TopicIDAssignments.Split(',');

//              foreach (string cTopicID in TopicIDs)
//              {
//                  this.AssignTopicToCategory(Int32.Parse(cTopicID), CategoryID);
//              }

//              return true;
//          }
//      }

//      /// <summary>
//      /// Assign topics to category from comma-delimited list
//      /// </summary>
//      public override bool UpdateTopicCategoryAssignments(int TopicID, string CategoryIDAssignments)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              // first delete any assignments that are not in the list passed in
////              SqlCommand cmd = new SqlCommand(
////                  "DELETE FROM categorylink " +
////                  "where TopicID = ? and CategoryID NOT IN (?)", cn);

//              // NOTE; For now, just delete them all
//              SqlCommand cmd = new SqlCommand(
//                  "DELETE FROM categorylink " +
//                  "where TopicID = @TopicID ", cn);
              
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
////              cmd.Parameters.Add("@CategoryIDAssignments", SqlDbType.VarChar).Value = CategoryIDAssignments;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              //              return (ret == 1);

//              if (CategoryIDAssignments.Trim().Length < 1)
//                  // no assignments, so nothing to do
//                  return true;

//              // now step through all CategoryIDs in the list passed in and call
//              // this.AssignTopicToCategory() for each, which will insert the assignment
//              // if it does not already exist
//              string[] CategoryIDs = CategoryIDAssignments.Split(',');

//              foreach (string cCategoryID in CategoryIDs)
//              {
//                  this.AssignTopicToCategory(TopicID, Int32.Parse(cCategoryID));
//              }

//              return true;
//          }
//      }

      
//       /// <summary>
//      /// Detach a topic from ALL categories
//      /// </summary>
//      public override bool DetachTopicFromAllCategories(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand(
//                  "delete from categorylink " +
//                  "where TopicID = @TopicID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with CategoryLinks

//      /// <summary>
//      /// Returns the total number of CategoryLinks
//      /// </summary>
//      public override int GetCategoryLinkCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from CategoryLink", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all CategoryLinks
//      /// </summary>
//      public override List<CategoryLinkInfo> GetCategoryLinks(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from CategoryLink";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetCategoryLinkCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the CategoryLink with the specified ID
//      /// </summary>
//      public override CategoryLinkInfo GetCategoryLinkByID(int CatTopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from CategoryLink where CatTopicID=@CatTopicID", cn);
//            cmd.Parameters.Add("@CatTopicID", SqlDbType.Int).Value = CatTopicID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetCategoryLinkFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a CategoryLink
//      /// </summary>
//      public override bool DeleteCategoryLink(int CatTopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from CategoryLink where CatTopicID=@CatTopicID", cn);
//            cmd.Parameters.Add("@CatTopicID", SqlDbType.Int).Value = CatTopicID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new CategoryLink
//      /// </summary>
//      public override int InsertCategoryLink(CategoryLinkInfo CategoryLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into CategoryLink " +
//            "(CategoryID, " +
//            "TopicID) " +
//            "VALUES (" +
//            "@CategoryID, " +
//            "@TopicID) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryLink.CategoryID ;
//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = CategoryLink.TopicID ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a CategoryLink
//      /// </summary>
//      public override bool UpdateCategoryLink(CategoryLinkInfo CategoryLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update CategoryLink set " +
//            "CategoryID = @CategoryID, " +
//            "TopicID = @TopicID " +
//            "where CatTopicID = @CatTopicID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryLink.CategoryID ;
//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = CategoryLink.TopicID ;
//            cmd.Parameters.Add("@CatTopicID", SqlDbType.Int).Value = CategoryLink.CatTopicID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      public override int UpdateTopicInProgressByUserID(int UserID, int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "Delete from Test where UserID = @UserID and TopicID = @TopicID and Status<>'C'";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//              cn.Open();
//              return ExecuteNonQuery(cmd);
//              //return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }
//      /////////////////////////////////////////////////////////
//      // methods that work with CertificateDefinitions

//      /// <summary>
//      /// Returns the total number of CertificateDefinitions
//      /// </summary>
//      public override int GetCertificateDefinitionCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from CertificateDefinition where FacilityID=0 ", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all CertificateDefinitions
//      /// </summary>
//      public override List<CertificateDefinitionInfo> GetCertificateDefinitions(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from CertificateDefinition";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetCertificateDefinitionCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all CertificateDefinitions for a given CertType
//      /// </summary>
//      public override List<CertificateDefinitionInfo> GetCertificateDefinitionsByCertType(string cCertType, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from CertificateDefinition where CertType=@CertType ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@CertType", SqlDbType.VarChar).Value = cCertType;

//              cn.Open();
//              return GetCertificateDefinitionCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the CertificateDefinition with the specified ID
//      /// </summary>
//      public override CertificateDefinitionInfo GetCertificateDefinitionByID(int CertID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from CertificateDefinition where CertID=@CertID", cn);
//            cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = CertID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetCertificateDefinitionFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a CertificateDefinition
//      /// </summary>
//      public override bool DeleteCertificateDefinition(int CertID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from CertificateDefinition where CertID=@CertID", cn);
//            cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = CertID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new CertificateDefinition
//      /// </summary>
//      public override int InsertCertificateDefinition(CertificateDefinitionInfo CertificateDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into CertificateDefinition " +
//            "(CertName, " +
//            "CertBody, " +
//            "CertType, " +
//              "FacilityID ) " +
//            "VALUES (" +
//            "@CertName, " +
//            "@CertBody, " +
//            "@CertType, " +
//              "@FacilityID ) SET @ID = SCOPE_IDENTITY()", cn);

//              cmd.Parameters.Add("@CertName", SqlDbType.VarChar).Value = CertificateDefinition.CertName;
//              cmd.Parameters.Add("@CertBody", SqlDbType.VarChar).Value = CertificateDefinition.CertBody;
//              cmd.Parameters.Add("@CertType", SqlDbType.Char).Value = CertificateDefinition.CertType;
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = CertificateDefinition.FacilityID;

//              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              IDParameter.Direction = ParameterDirection.Output;
//              cmd.Parameters.Add(IDParameter);

//              cn.Open();
//              cmd.ExecuteNonQuery();

//              int NewID = (int)IDParameter.Value;
//              return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a CertificateDefinition
//      /// </summary>
//      public override bool UpdateCertificateDefinition(CertificateDefinitionInfo CertificateDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update CertificateDefinition set " +
//            "CertName = @CertName, " +
//            "CertBody = @CertBody, " +
//            "CertType = @CertType, " +
//            "FacilityID = @FacilityID " +
//            "where CertID = @CertID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@CertName", SqlDbType.VarChar).Value = CertificateDefinition.CertName;
//              cmd.Parameters.Add("@CertBody", SqlDbType.Text).Value = CertificateDefinition.CertBody;
//              cmd.Parameters.Add("@CertType", SqlDbType.Char).Value = CertificateDefinition.CertType;
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = CertificateDefinition.FacilityID;
//              cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = CertificateDefinition.CertID;
               
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }     

//      /////////////////////////////////////////////////////////
//      // methods that work with EmailDefinitions

//      /// <summary>
//      /// Returns the total number of EmailDefinitions
//      /// </summary>
//      public override int GetEmailDefinitionCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from EmailDefinition", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all EmailDefinitions
//      /// </summary>
//      public override List<EmailDefinitionInfo> GetEmailDefinitions(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from EmailDefinition";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetEmailDefinitionCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the EmailDefinition with the specified ID
//      /// </summary>
//      public override EmailDefinitionInfo GetEmailDefinitionByID(int EDefID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from EmailDefinition where EDefID=@EDefID", cn);
//            cmd.Parameters.Add("@EDefID", SqlDbType.Int).Value = EDefID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetEmailDefinitionFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a EmailDefinition
//      /// </summary>
//      public override bool DeleteEmailDefinition(int EDefID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from EmailDefinition where EDefID=@EDefID", cn);
//            cmd.Parameters.Add("@EDefID", SqlDbType.Int).Value = EDefID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new EmailDefinition
//      /// </summary>
//      public override int InsertEmailDefinition(EmailDefinitionInfo EmailDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into EmailDefinition " +
//            "(EDefName, " +
//            "EDefText) " +
//            "VALUES (" +
//            "@EDefName, " +
//            "@EDefText) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@EDefName", SqlDbType.VarChar).Value = EmailDefinition.EDefName ;
//            cmd.Parameters.Add("@EDefText", SqlDbType.VarChar).Value = EmailDefinition.EDefText ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a EmailDefinition
//      /// </summary>
//      public override bool UpdateEmailDefinition(EmailDefinitionInfo EmailDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update EmailDefinition set " +
//            "EDefName = @EDefName, " +
//            "EDefText = @EDefText " +
//            "where EDefID = @EDefID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@EDefName", SqlDbType.VarChar).Value = EmailDefinition.EDefName ;
//            cmd.Parameters.Add("@EDefText", SqlDbType.VarChar).Value = EmailDefinition.EDefText ;
//            cmd.Parameters.Add("@EDefID", SqlDbType.Int).Value = EmailDefinition.EDefID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with EmailQueue

//      /// <summary>
//      /// Returns the total number of EmailQueue
//      /// </summary>
//      public override int GetEmailQueueCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from EmailQueue", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all EmailQueue
//      /// </summary>
//      public override List<EmailQueueInfo> GetEmailQueue(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from EmailQueue";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetEmailQueueCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the EmailQueue with the specified ID
//      /// </summary>
//      public override EmailQueueInfo GetEmailQueueByID(int EQueueID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from EmailQueue where EQueueID=@EQueueID", cn);
//            cmd.Parameters.Add("@EQueueID", SqlDbType.Int).Value = EQueueID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetEmailQueueFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a EmailQueue
//      /// </summary>
//      public override bool DeleteEmailQueue(int EQueueID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from EmailQueue where EQueueID=@EQueueID", cn);
//            cmd.Parameters.Add("@EQueueID", SqlDbType.Int).Value = EQueueID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new EmailQueue
//      /// </summary>
//      public override int InsertEmailQueue(EmailQueueInfo EmailQueue)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into EmailQueue " +
//            "(UserID, " +
//            "EDefID, " +
//            "SentDate) " +
//            "VALUES (" +
//            "@UserID, " +
//            "@EDefID, " +
//            "@SentDate) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = EmailQueue.UserID ;
//            cmd.Parameters.Add("@EDefID", SqlDbType.Int).Value = EmailQueue.EDefID ;
//            cmd.Parameters.Add("@SentDate", SqlDbType.DateTime).Value = DBNull.Value ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a EmailQueue
//      /// </summary>
//      public override bool UpdateEmailQueue(EmailQueueInfo EmailQueue)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update EmailQueue set " +
//            "UserID = @UserID, " +
//            "EDefID = @EDefID, " +
//            "SentDate = @SentDate " +
//            "where EQueueID = @EQueueID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = EmailQueue.UserID ;
//            cmd.Parameters.Add("@EDefID", SqlDbType.Int).Value = EmailQueue.EDefID ;
//            cmd.Parameters.Add("@SentDate", SqlDbType.DateTime).Value = EmailQueue.SentDate ;
//            cmd.Parameters.Add("@EQueueID", SqlDbType.Int).Value = EmailQueue.EQueueID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with FacilityGroups
//      /// ***** CEDIRECT VERSION ************

//      /// <summary>
//      /// Returns the total number of FacilityGroups
//      /// </summary>
//      public override int GetFacilityGroupCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from FacilityGroup", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all FacilityGroups
//      /// </summary>
//      public override List<FacilityGroupInfo> GetFacilityGroups(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from FacilityGroup";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//       ///<summary>
//       ///Retrieves all facilityGroups except gift card 
//       ///</summary>
//       ///

//      public override List<FacilityGroupInfo> GetFacilityGroupsExceptGiftCard(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from facilitygroup where (parent_id is null or parent_id not " +
//                  "in (select parent_id from parentorganization where parent_name ='CE Gift Card')) and active='1'";

//              //add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cn.Open();
//              return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all FacilityGroups
//      /// </summary>
//      public override List<FacilityGroupInfo> GetFacilityGroupsByParentOfFacilityID(int FacilityID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from FacilityGroup where (Parent_ID=0 and FacID=@FacilityID) or " + 
//                  "( Parent_ID>0 and Parent_ID IN " +
//                  "( select Parent_ID from FacilityGroup where FacID=@FacilityID ) ) ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;

//              cn.Open();
//              return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all FacilityGroups
//      /// </summary>
//      public override List<FacilityGroupInfo> GetFacilityGroupsByParentID(int ParentID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from FacilityGroup where Parent_id=@ParentID ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@ParentID", SqlDbType.Int).Value = ParentID;

//              cn.Open();
//              return GetFacilityGroupCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//       /// <summary>
//      /// Retrieves the FacilityGroup with the specified ID
//      /// </summary>
//      public override FacilityGroupInfo GetFacilityGroupByID(int FacID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from FacilityGroup where FacID=@FacID", cn);
//            cmd.Parameters.Add("@FacID", SqlDbType.Int).Value = FacID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetFacilityGroupFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Retrieves the FacilityGroup with the specified ID
//      /// </summary>
//      public override FacilityGroupInfo GetFacilityGroupByFacilityCode(string FacCode)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from FacilityGroup where FacCode=@FacCode", cn);
//              cmd.Parameters.Add("@FacCode", SqlDbType.VarChar).Value = FacCode;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetFacilityGroupFromReader(reader, true);
//              else
//                  return null;
//          }
//      }


//      /// <summary>
//      /// Retrieves the FacilityGroup with the specified Name
//      /// </summary>
//      public override FacilityGroupInfo GetFacilityGroupByFacName(string FacName)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from FacilityGroup where Facname=@FacName", cn);
//              cmd.Parameters.Add("@FacName", SqlDbType.VarChar).Value = FacName;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetFacilityGroupFromReader(reader, true);
//              else
//                  return null;
//          }
//      }


//      /// <summary>
//      /// Deletes a FacilityGroup
//      /// </summary>
//      public override bool DeleteFacilityGroup(int FacID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from FacilityGroup where FacID=@FacID", cn);
//            cmd.Parameters.Add("@FacID", SqlDbType.Int).Value = FacID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new FacilityGroup
//      /// </summary>
//      public override int InsertFacilityGroup(FacilityGroupInfo FacilityGroup)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into FacilityGroup " +
//            "(FacCode, " +
//            "FacName, " +
//            "MaxUsers, " +
//            "Started, " +
//            "Expires, " +
//            "RepID, " +
//            "Comment, " +
//            "Compliance, " +
//            "CustomTopics, " +
//            "QuizBowl, " +
//            "homeurl, " +
//            "tagline, " +
//            "address1, " +
//            "address2, " +
//            "city, " +
//            "state, " +
//            "zipcode, " +
//            "phone, " +
//            "contact_name, " +
//            "contact_phone, " +
//            "contact_ext, " +
//            "contact_email, " +
//            "parent_id, " +
//            "welcome_pg, " +
//            "login_help, " +
//            "logo_img, " +
//            "active, " +
//            "support_email, " +
//            "feedback_email, " +
//            "default_password ) " +
//            "VALUES (" +
//            "@FacCode, " +
//            "@FacName, " +
//            "@MaxUsers, " +
//            "@Started, " +
//            "@Expires, " +
//            "@RepID, " +
//            "@Comment, " +
//            "@Compliance, " +
//            "@CustomTopics, " +
//            "@QuizBowl, " +
//            "@homeurl, " +
//            "@tagline, " +
//            "@address1, " +
//            "@address2, " +
//            "@city, " +
//            "@state, " +
//            "@zipcode, " +
//            "@phone, " +
//            "@contact_name, " +
//            "@contact_phone, " +
//            "@contact_ext, " +
//            "@contact_email, " +
//            "@parent_id, " +
//            "@welcome_pg, " +
//            "@login_help, " +
//            "@logo_img, " +
//            "@active, " +
//            "@support_email, " +
//            "@feedback_email, " +
//            "@default_password ) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@FacCode", SqlDbType.VarChar).Value = FacilityGroup.FacCode ;
//            cmd.Parameters.Add("@FacName", SqlDbType.VarChar).Value = FacilityGroup.FacName;
//            cmd.Parameters.Add("@MaxUsers", SqlDbType.Int).Value = FacilityGroup.MaxUsers;
//            cmd.Parameters.Add("@Started", SqlDbType.DateTime).Value = FacilityGroup.Started;
//            cmd.Parameters.Add("@Expires", SqlDbType.DateTime).Value = FacilityGroup.Expires;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (FacilityGroup.RepID == 0)
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = FacilityGroup.RepID;
//            }

//            cmd.Parameters.Add("@Comment", SqlDbType.Text).Value = FacilityGroup.Comment;
//            cmd.Parameters.Add("@Compliance", SqlDbType.Bit).Value = FacilityGroup.Compliance;
//            cmd.Parameters.Add("@CustomTopics", SqlDbType.Bit).Value = FacilityGroup.CustomTopics;
//            cmd.Parameters.Add("@QuizBowl", SqlDbType.Bit).Value = FacilityGroup.QuizBowl;
//            cmd.Parameters.Add("@homeurl", SqlDbType.VarChar).Value = FacilityGroup.homeurl;
//            cmd.Parameters.Add("@tagline", SqlDbType.VarChar).Value = FacilityGroup.tagline;
//            cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = FacilityGroup.address1;
//            cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = FacilityGroup.address2;
//            cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = FacilityGroup.city;
//            cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = FacilityGroup.state;
//            cmd.Parameters.Add("@zipcode", SqlDbType.VarChar).Value = FacilityGroup.zipcode;
//            cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = FacilityGroup.phone;
//            cmd.Parameters.Add("@contact_name", SqlDbType.VarChar).Value = FacilityGroup.contact_name;
//            cmd.Parameters.Add("@contact_phone", SqlDbType.VarChar).Value = FacilityGroup.contact_phone;
//            cmd.Parameters.Add("@contact_ext", SqlDbType.VarChar).Value = FacilityGroup.contact_ext;
//            cmd.Parameters.Add("@contact_email", SqlDbType.VarChar).Value = FacilityGroup.contact_email;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (FacilityGroup.parent_id == 0)
//            {
//                cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = FacilityGroup.parent_id;
//            }

//            cmd.Parameters.Add("@welcome_pg", SqlDbType.Text).Value = FacilityGroup.welcome_pg;
//            cmd.Parameters.Add("@login_help", SqlDbType.VarChar).Value = FacilityGroup.login_help;
//            cmd.Parameters.Add("@logo_img", SqlDbType.VarChar).Value = FacilityGroup.logo_img;
//            cmd.Parameters.Add("@active", SqlDbType.Bit).Value = FacilityGroup.active;
//            cmd.Parameters.Add("@support_email", SqlDbType.VarChar).Value = FacilityGroup.support_email;
//            cmd.Parameters.Add("@feedback_email", SqlDbType.VarChar).Value = FacilityGroup.feedback_email;
//            cmd.Parameters.Add("@default_password", SqlDbType.VarChar).Value = FacilityGroup.default_password;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            foreach (IDataParameter param in cmd.Parameters)
//            {
//                if (param.Value == null)
//                    param.Value = DBNull.Value;
//            }

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a FacilityGroup
//      /// </summary>
//      public override bool UpdateFacilityGroup(FacilityGroupInfo FacilityGroup)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update FacilityGroup set " +
//            "FacCode = @FacCode, " +
//            "FacName = @FacName, " +
//            "MaxUsers = @MaxUsers, " +
//            "Started = @Started, " +
//            "Expires = @Expires, " +
//            "RepID = @RepID, " +
//            "Comment = @Comment, " +
//            "Compliance = @Compliance, " +
//            "CustomTopics = @CustomTopics, " +
//            "QuizBowl = @QuizBowl, " +
//            "homeurl = @homeurl, " +
//            "tagline = @tagline, " +
//            "address1 = @address1, " +
//            "address2 = @address2, " +
//            "city = @city, " +
//            "state = @state, " +
//            "zipcode = @zipcode, " +
//            "phone = @phone, " +
//            "contact_name = @contact_name, " +
//            "contact_phone = @contact_phone, " +
//            "contact_ext = @contact_ext, " +
//            "contact_email = @contact_email, " +
//            "parent_id = @parent_id, " +
//            "welcome_pg = @welcome_pg, " +
//            "login_help = @login_help, " +
//            "logo_img = @logo_img, " +
//            "active = @active, " +
//            "support_email = @support_email, " +
//            "feedback_email = @feedback_email, " +
//            "default_password = @default_password " +
//            "where FacID = @FacID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@FacCode", SqlDbType.VarChar).Value = FacilityGroup.FacCode ;
//            cmd.Parameters.Add("@FacName", SqlDbType.VarChar).Value = FacilityGroup.FacName;
//            cmd.Parameters.Add("@MaxUsers", SqlDbType.Int).Value = FacilityGroup.MaxUsers;
//            cmd.Parameters.Add("@Started", SqlDbType.DateTime).Value = FacilityGroup.Started;
//            cmd.Parameters.Add("@Expires", SqlDbType.DateTime).Value = FacilityGroup.Expires;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (FacilityGroup.RepID == 0)
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = FacilityGroup.RepID;
//            }

//            cmd.Parameters.Add("@Comment", SqlDbType.Text).Value = FacilityGroup.Comment;
//            cmd.Parameters.Add("@Compliance", SqlDbType.Bit).Value = FacilityGroup.Compliance;
//            cmd.Parameters.Add("@CustomTopics", SqlDbType.Bit).Value = FacilityGroup.CustomTopics;
//            cmd.Parameters.Add("@QuizBowl", SqlDbType.Bit).Value = FacilityGroup.QuizBowl;
//            cmd.Parameters.Add("@homeurl", SqlDbType.VarChar).Value = FacilityGroup.homeurl;
//            cmd.Parameters.Add("@tagline", SqlDbType.VarChar).Value = FacilityGroup.tagline;
//            cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = FacilityGroup.address1;
//            cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = FacilityGroup.address2;
//            cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = FacilityGroup.city;
//            cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = FacilityGroup.state;
//            cmd.Parameters.Add("@zipcode", SqlDbType.VarChar).Value = FacilityGroup.zipcode;
//            cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = FacilityGroup.phone;
//            cmd.Parameters.Add("@contact_name", SqlDbType.VarChar).Value = FacilityGroup.contact_name;
//            cmd.Parameters.Add("@contact_phone", SqlDbType.VarChar).Value = FacilityGroup.contact_phone;
//            cmd.Parameters.Add("@contact_ext", SqlDbType.VarChar).Value = FacilityGroup.contact_ext;
//            cmd.Parameters.Add("@contact_email", SqlDbType.VarChar).Value = FacilityGroup.contact_email;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (FacilityGroup.parent_id == 0)
//            {
//                cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = FacilityGroup.parent_id;
//            }

//            cmd.Parameters.Add("@welcome_pg", SqlDbType.Text).Value = FacilityGroup.welcome_pg;
//            cmd.Parameters.Add("@login_help", SqlDbType.VarChar).Value = FacilityGroup.login_help;
//            cmd.Parameters.Add("@logo_img", SqlDbType.VarChar).Value = FacilityGroup.logo_img;
//            cmd.Parameters.Add("@active", SqlDbType.Bit).Value = FacilityGroup.active;
//            cmd.Parameters.Add("@support_email", SqlDbType.VarChar).Value = FacilityGroup.support_email;
//            cmd.Parameters.Add("@feedback_email", SqlDbType.VarChar).Value = FacilityGroup.feedback_email;
//            cmd.Parameters.Add("@default_password", SqlDbType.VarChar).Value = FacilityGroup.default_password;

//            cmd.Parameters.Add("@FacID", SqlDbType.Int).Value = FacilityGroup.FacID;


//            foreach (IDataParameter param in cmd.Parameters)
//            {
//                if (param.Value == null)
//                    param.Value = DBNull.Value;
//            }

//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with FieldsOfInterest

//      /// <summary>
//      /// Returns the total number of FieldsOfInterest
//      /// </summary>
//      public override int GetFieldOfInterestCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from FieldOfInterest", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all FieldsOfInterest
//      /// </summary>
//      public override List<FieldOfInterestInfo> GetFieldsOfInterest(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from FieldOfInterest";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }
//              else
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by intDescr";
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetFieldOfInterestCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the FieldOfInterest with the specified ID
//      /// </summary>
//      public override FieldOfInterestInfo GetFieldOfInterestByID(int InterestID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from FieldOfInterest where InterestID=@InterestID", cn);
//              cmd.Parameters.Add("@InterestID", SqlDbType.Int).Value = InterestID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetFieldOfInterestFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Retrieves a list of FieldsOfInterest with the specified UserID
//      /// </summary>
//       public override List<FieldOfInterestInfo> GetFieldsOfInterestByUserID(int UserID, string cSortExpression)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * from FieldOfInterest " +
//                  "join UserInterestLink on FieldOfinterest.InterestID = UserInterestLink.InterestID " +
//                  "where UserInterestLink.UserID = @UserID";


//               // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }
//              else
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by intDescr";
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID ;

//              cn.Open();
//              return GetFieldOfInterestCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Deletes a FieldOfInterest
//      /// </summary>
//      public override bool DeleteFieldOfInterest(int InterestID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from FieldOfInterest where InterestID=@InterestID", cn);
//            cmd.Parameters.Add("@InterestID", SqlDbType.Int).Value = InterestID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new FieldOfInterest
//      /// </summary>
//      public override int InsertFieldOfInterest(FieldOfInterestInfo FieldOfInterest)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into FieldOfInterest " +
//            "(IntDescr) " +
//            "VALUES (" +
//            "@IntDescr) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@IntDescr", SqlDbType.VarChar).Value = FieldOfInterest.IntDescr ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a FieldOfInterest
//      /// </summary>
//      public override bool UpdateFieldOfInterest(FieldOfInterestInfo FieldOfInterest)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update FieldOfInterest set " +
//            "IntDescr = @IntDescr " +
//            "where InterestID = @InterestID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@IntDescr", SqlDbType.VarChar).Value = FieldOfInterest.IntDescr ;
//            cmd.Parameters.Add("@InterestID", SqlDbType.Int).Value = FieldOfInterest.InterestID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//       /// <summary>
//       /// Assign fields of interest to user from comma-delimited list
//       /// </summary>
//       public override bool UpdateUserInterestAssignments(int UserID, string InterestIDAssignments)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {

//               // first delete any assignments that are not in the list passed in
//               //              SqlCommand cmd = new SqlCommand(
//               //                  "DELETE FROM categorylink " +
//               //                  "where TopicID = ? and CategoryID NOT IN (?)", cn);

//               // NOTE; For now, just delete them all
//               SqlCommand cmd = new SqlCommand(
//                   "DELETE FROM UserInterestLink " +
//                   "where UserID = @UserID ", cn);

//               cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

//               cn.Open();
//               int ret = ExecuteNonQuery(cmd);
//               //              return (ret == 1);

//               if (InterestIDAssignments.Trim().Length < 1)
//                   // no assignments, so nothing else to do
//                   return true;

//               // now step through all InterestIDs in the list passed in and call
//               // this.AssignInterestToUser() for each, which will insert the assignment
//               // if it does not already exist
//               string[] InterestIDs = InterestIDAssignments.Split(',');

//               foreach (string cInterestID in InterestIDs)
//               {
//                   this.AssignInterestToUser(UserID, Int32.Parse(cInterestID));
//               }

//               return true;
//           }
//       }

//       /// <summary>
//       /// Assign a field of interest to a user
//       /// </summary>
//       public override bool AssignInterestToUser(int UserID, int InterestID)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               //SqlCommand cmd = new SqlCommand(
//                 //  "select Count(*) from categorylink " +
//                   //"where TopicID = ? and CategoryID = ?", cn);
//               //cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//               //cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//               cn.Open();
//               //int ResultCount = (int)ExecuteScalar(cmd);

//               //if (ResultCount > 1)
//                   // link already exists, so nothing to do
//                 //  return true;

//               // link does not exist, so insert it
//               SqlCommand cmd2 = new SqlCommand("insert into UserInterestLink " +
//                   "(UserID, " +
//                   "InterestID) " +
//                   "VALUES (" +
//                   "@UserID, " +
//                   "@InterestID)", cn);

//               cmd2.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//               cmd2.Parameters.Add("@InterestID", SqlDbType.Int).Value = InterestID;

//               int ret = ExecuteNonQuery(cmd2);
//               return (ret == 1);
//           }
//       }

       


//      /////////////////////////////////////////////////////////
//      // methods that work with LectureAttendees

//      /// <summary>
//      /// Returns the total number of LectureAttendees
//      /// </summary>
//      public override int GetLectureAttendeeCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from LectureAttendee", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all LectureAttendees
//      /// </summary>
//      public override List<LectureAttendeeInfo> GetLectureAttendees(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from LectureAttendee";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetLectureAttendeeCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the LectureAttendee with the specified ID
//      /// </summary>
//      public override LectureAttendeeInfo GetLectureAttendeeByID(int LectAttID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from LectureAttendee where LectAttID=@LectAttID", cn);
//            cmd.Parameters.Add("@LectAttID", SqlDbType.Int).Value = LectAttID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetLectureAttendeeFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a LectureAttendee
//      /// </summary>
//      public override bool DeleteLectureAttendee(int LectAttID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from LectureAttendee where LectAttID=@LectAttID", cn);
//            cmd.Parameters.Add("@LectAttID", SqlDbType.Int).Value = LectAttID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new LectureAttendee
//      /// </summary>
//      public override int InsertLectureAttendee(LectureAttendeeInfo LectureAttendee)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into LectureAttendee " +
//            "(LectEvtID, " +
//            "UserID, " +
//            "UserName, " +
//            "PW, " +
//            "FirstName, " +
//            "LastName, " +
//            "MI, " +
//            "Address1, " +
//            "Address2, " +
//            "City, " +
//            "State, " +
//            "Zip, " +
//            "Email, " +
//            "DateAdded, " +
//            "Completed) " +
//            "VALUES (" +
//            "@LectEvtID, " +
//            "@UserID, " +
//            "@UserName, " +
//            "@PW, " +
//            "@FirstName, " +
//            "@LastName, " +
//            "@MI, " +
//            "@Address1, " +
//            "@Address2, " +
//            "@City, " +
//            "@State, " +
//            "@Zip, " +
//            "@Email, " +
//            "@DateAdded, " +
//            "@Completed) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectureAttendee.LectEvtID ;

//            // pass null to database if value is zero (nothing selected yet)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            // NOTE: This situation will possibly occur when setting up a new potential user who is
//            // registering for a lecture. When the user's account has been set up, this link will be
//            // put into place, but until then, it will be stored as a null in the database.
//            if (LectureAttendee.UserID == 0)
//            {
//                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = LectureAttendee.UserID;
//            }

//            cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = LectureAttendee.UserName ;
//            cmd.Parameters.Add("@PW", SqlDbType.VarChar).Value = LectureAttendee.PW ;
//            cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = LectureAttendee.FirstName ;
//            cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = LectureAttendee.LastName ;
//            cmd.Parameters.Add("@MI", SqlDbType.VarChar).Value = LectureAttendee.MI ;
//            cmd.Parameters.Add("@Address1", SqlDbType.VarChar).Value = LectureAttendee.Address1 ;
//            cmd.Parameters.Add("@Address2", SqlDbType.VarChar).Value = LectureAttendee.Address2 ;
//            cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = LectureAttendee.City ;
//            cmd.Parameters.Add("@State", SqlDbType.Char).Value = LectureAttendee.State ;
//            cmd.Parameters.Add("@Zip", SqlDbType.VarChar).Value = LectureAttendee.Zip ;
//            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = LectureAttendee.Email ;
//            cmd.Parameters.Add("@DateAdded", SqlDbType.DateTime).Value = LectureAttendee.DateAdded ;
//            cmd.Parameters.Add("@Completed", SqlDbType.Bit).Value = LectureAttendee.Completed ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a LectureAttendee
//      /// </summary>
//      public override bool UpdateLectureAttendee(LectureAttendeeInfo LectureAttendee)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update LectureAttendee set " +
//            "LectEvtID = @LectEvtID, " +
//            "UserID = @UserID, " +
//            "UserName = @UserName, " +
//            "PW = @PW, " +
//            "FirstName = @FirstName, " +
//            "LastName = @LastName, " +
//            "MI = @MI, " +
//            "Address1 = @Address1, " +
//            "Address2 = @Address2, " +
//            "City = @City, " +
//            "State = @State, " +
//            "Zip = @Zip, " +
//            "Email = @Email, " +
//            "DateAdded = @DateAdded, " +
//            "Completed = @Completed " +
//            "where LectAttID = @LectAttID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectureAttendee.LectEvtID ;
//            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = LectureAttendee.UserID ;
//            cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = LectureAttendee.UserName ;
//            cmd.Parameters.Add("@PW", SqlDbType.VarChar).Value = LectureAttendee.PW ;
//            cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = LectureAttendee.FirstName ;
//            cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = LectureAttendee.LastName ;
//            cmd.Parameters.Add("@MI", SqlDbType.VarChar).Value = LectureAttendee.MI ;
//            cmd.Parameters.Add("@Address1", SqlDbType.VarChar).Value = LectureAttendee.Address1 ;
//            cmd.Parameters.Add("@Address2", SqlDbType.VarChar).Value = LectureAttendee.Address2 ;
//            cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = LectureAttendee.City ;
//            cmd.Parameters.Add("@State", SqlDbType.Char).Value = LectureAttendee.State ;
//            cmd.Parameters.Add("@Zip", SqlDbType.VarChar).Value = LectureAttendee.Zip ;
//            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = LectureAttendee.Email ;
//            cmd.Parameters.Add("@DateAdded", SqlDbType.DateTime).Value = LectureAttendee.DateAdded ;
//            cmd.Parameters.Add("@Completed", SqlDbType.Bit).Value = LectureAttendee.Completed ;
//            cmd.Parameters.Add("@LectAttID", SqlDbType.Int).Value = LectureAttendee.LectAttID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with LectureDefinitions

//      /// <summary>
//      /// Returns the total number of LectureDefinitions
//      /// </summary>
//      public override int GetLectureDefinitionCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from LectureDefinition", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all LectureDefinitions
//      /// </summary>
//      public override List<LectureDefinitionInfo> GetLectureDefinitions(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from LectureDefinition";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetLectureDefinitionCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the LectureDefinition with the specified ID
//      /// </summary>
//      public override LectureDefinitionInfo GetLectureDefinitionByID(int LectureID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from LectureDefinition where LectureID=@LectureID", cn);
//            cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetLectureDefinitionFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a LectureDefinition
//      /// </summary>
//      public override bool DeleteLectureDefinition(int LectureID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from LectureDefinition where LectureID=@LectureID", cn);
//            cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new LectureDefinition
//      /// </summary>
//      public override int InsertLectureDefinition(LectureDefinitionInfo LectureDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into LectureDefinition " +
//            "(LectTitle, " +
//            "CertID, " +
//            "Comment) " +
//            "VALUES (" +
//            "@LectTitle, " +
//            "@CertID, " +
//            "@Comment) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@LectTitle", SqlDbType.VarChar).Value = LectureDefinition.LectTitle ;
//            cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = LectureDefinition.CertID ;
//            cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = LectureDefinition.Comment ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a LectureDefinition
//      /// </summary>
//      public override bool UpdateLectureDefinition(LectureDefinitionInfo LectureDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update LectureDefinition set " +
//            "LectTitle = @LectTitle, " +
//            "CertID = @CertID, " +
//            "Comment = @Comment " +
//            "where LectureID = @LectureID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@LectTitle", SqlDbType.VarChar).Value = LectureDefinition.LectTitle ;
//            cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = LectureDefinition.CertID ;
//            cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = LectureDefinition.Comment ;
//            cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureDefinition.LectureID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with LectureEvents

//      /// <summary>
//      /// Returns the total number of LectureEvents
//      /// </summary>
//      public override int GetLectureEventCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from LectureEvent", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all LectureEvents
//      /// </summary>
//      public override List<LectureEventInfo> GetLectureEvents(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from LectureEvent";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetLectureEventCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the LectureEvent with the specified ID
//      /// </summary>
//      public override LectureEventInfo GetLectureEventByID(int LectEvtID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from LectureEvent where LectEvtID=@LectEvtID", cn);
//            cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectEvtID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetLectureEventFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a LectureEvent
//      /// </summary>
//      public override bool DeleteLectureEvent(int LectEvtID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from LectureEvent where LectEvtID=@LectEvtID", cn);
//            cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectEvtID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new LectureEvent
//      /// </summary>
//      public override int InsertLectureEvent(LectureEventInfo LectureEvent)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into LectureEvent " +
//            "(LectureID, " +
//            "StartDate, " +
//            "Capacity, " +
//            "Attendees, " +
//            "Comment, " +
//            "Facility, " +
//            "City, " +
//            "State) " +
//            "VALUES (" +
//            "@LectureID, " +
//            "@StartDate, " +
//            "@Capacity, " +
//            "@Attendees, " +
//            "@Comment, " +
//            "@Facility, " +
//            "@City, " +
//            "@State) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureEvent.LectureID ;
//            cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = LectureEvent.StartDate ;
//            cmd.Parameters.Add("@Capacity", SqlDbType.Int).Value = LectureEvent.Capacity ;
//            cmd.Parameters.Add("@Attendees", SqlDbType.Int).Value = LectureEvent.Attendees ;
//            cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = LectureEvent.Comment ;
//            cmd.Parameters.Add("@Facility", SqlDbType.VarChar).Value = LectureEvent.Facility;
//            cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = LectureEvent.City;
//            cmd.Parameters.Add("@State", SqlDbType.Char).Value = LectureEvent.State;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a LectureEvent
//      /// </summary>
//      public override bool UpdateLectureEvent(LectureEventInfo LectureEvent)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update LectureEvent set " +
//            "LectureID = @LectureID, " +
//            "StartDate = @StartDate, " +
//            "Capacity = @Capacity, " +
//            "Attendees = @Attendees, " +
//            "Comment = @Comment, " +
//            "Facility = @Facility, " +
//            "City = @City, " +
//            "State = @State " +
//            "where LectEvtID = @LectEvtID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureEvent.LectureID ;
//            cmd.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = LectureEvent.StartDate ;
//            cmd.Parameters.Add("@Capacity", SqlDbType.Int).Value = LectureEvent.Capacity ;
//            cmd.Parameters.Add("@Attendees", SqlDbType.Int).Value = LectureEvent.Attendees ;
//            cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = LectureEvent.Comment ;
//            cmd.Parameters.Add("@Facility", SqlDbType.VarChar).Value = LectureEvent.Facility;
//            cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = LectureEvent.City;
//            cmd.Parameters.Add("@State", SqlDbType.Char).Value = LectureEvent.State;
//            cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectureEvent.LectEvtID;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with LectureTopicLinks

//      /// <summary>
//      /// Returns the total number of LectureTopicLinks
//      /// </summary>
//      public override int GetLectureTopicLinkCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from LectureTopicLink", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all LectureTopicLinks
//      /// </summary>
//      public override List<LectureTopicLinkInfo> GetLectureTopicLinks(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from LectureTopicLink";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetLectureTopicLinkCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the LectureTopicLink with the specified ID
//      /// </summary>
//      public override LectureTopicLinkInfo GetLectureTopicLinkByID(int LectTopID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from LectureTopicLink where LectTopID=@LectTopID", cn);
//            cmd.Parameters.Add("@LectTopID", SqlDbType.Int).Value = LectTopID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetLectureTopicLinkFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a LectureTopicLink
//      /// </summary>
//      public override bool DeleteLectureTopicLink(int LectTopID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from LectureTopicLink where LectTopID=@LectTopID", cn);
//            cmd.Parameters.Add("@LectTopID", SqlDbType.Int).Value = LectTopID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new LectureTopicLink
//      /// </summary>
//      public override int InsertLectureTopicLink(LectureTopicLinkInfo LectureTopicLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into LectureTopicLink " +
//            "(LectureID, " +
//            "TopicID) " +
//            "VALUES (" +
//            "@LectureID, " +
//            "@TopicID) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureTopicLink.LectureID ;
//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = LectureTopicLink.TopicID ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a LectureTopicLink
//      /// </summary>
//      public override bool UpdateLectureTopicLink(LectureTopicLinkInfo LectureTopicLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update LectureTopicLink set " +
//            "LectureID = @LectureID, " +
//            "TopicID = @TopicID " +
//            "where LectTopID = @LectTopID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureTopicLink.LectureID ;
//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = LectureTopicLink.TopicID ;
//            cmd.Parameters.Add("@LectTopID", SqlDbType.Int).Value = LectureTopicLink.LectTopID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Assign topics to Lecture Definition from comma-delimited list
//      /// </summary>
//      public override bool UpdateLectureTopicLinkAssignments(int LectureID, string TopicIDAssignments, int Sequence)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              // first delete any assignments that are not in the list passed in
//              //              SqlCommand cmd = new SqlCommand(
//              //                  "DELETE FROM categorylink " +
//              //                  "where CategoryID = ? and TopicID NOT IN ("+TopicIDAssignments.Trim()+")", cn);

//              // NOTE: For now, just delete them all
//              SqlCommand cmd = new SqlCommand(
//                  "DELETE FROM LectureTopicLink " +
//                  "where LectureID = @LectureID and Sequence = @Sequence", cn);

//              cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID;
//              cmd.Parameters.Add("@Sequence", SqlDbType.Int).Value = Sequence;

//              //              cmd.Parameters.Add("@TopicIDAssignments", SqlDbType.VarChar).Value = TopicIDAssignments;

//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              //              return (ret == 1);

//              if (TopicIDAssignments.Trim().Length < 1)
//                  // no assignments, so nothing to do
//                  return true;

//              // now step through all TopicIDs in the list passed in and call
//              // this.AssignTopicToCategory() for each, which will insert the assignment
//              // if it does not already exist
//              string[] TopicIDs = TopicIDAssignments.Split(',');

//              LectureTopicLinkInfo LectureTopicLink;

//              foreach (string cTopicID in TopicIDs)
//              {
//                  LectureTopicLink = new LectureTopicLinkInfo(0, LectureID, Int32.Parse(cTopicID), Sequence);
//                  this.InsertLectureTopicLink(LectureTopicLink);
//              }

//              return true;
//          }
//      }

//       /////////////////////////////////////////////////////////
//       // methods that work with LicenseTypes

//       /// <summary>
//       /// Returns the total number of LicenseTypes
//       /// </summary>
//       public override int GetLicenseTypeCount()
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("select Count(*) from LicenseType", cn);
//               cn.Open();
//               return (int)ExecuteScalar(cmd);
//           }
//       }

//       /// <summary>
//       /// Retrieves all LicenseTypes
//       /// </summary>
//       public override List<LicenseTypeInfo> GetLicenseTypes(string cSortExpression)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               string cSQLCommand = "select * " +
//                   "from LicenseType";

//               // add on ORDER BY if provided
//               if (cSortExpression.Length > 0)
//               {
//                   cSQLCommand = cSQLCommand +
//                       " order by " + cSortExpression;
//               }
//               else
//               {
//                   cSQLCommand = cSQLCommand +
//                       " order by Description";
//               }

//               SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//               cn.Open();
//               return GetLicenseTypeCollectionFromReader(ExecuteReader(cmd), false);
//           }
//       }



//       /// <summary>
//       /// Retrieves all AreaTypes
//       /// </summary>
//       public override List<AreaTypeInfo> GetAreaTypes(string cSortExpression)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               string cSQLCommand = "select * " +
//                   "from AreaType";

//               // add on ORDER BY if provided
//               if (cSortExpression.Length > 0)
//               {
//                   cSQLCommand = cSQLCommand +
//                       " order by " + cSortExpression;
//               }
//               else
//               {
//                   cSQLCommand = cSQLCommand +
//                       " order by areaname";
//               }

//               SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//               cn.Open();
//               return GetAreaTypeCollectionFromReader(ExecuteReader(cmd), false);
//           }
//       }






//       /// <summary>
//       /// Retrieves the LicenseType with the specified ID
//       /// </summary>
//       public override LicenseTypeInfo GetLicenseTypeByID(int License_Type_ID)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {

//               SqlCommand cmd = new SqlCommand("select * " +
//                       "from LicenseType where License_Type_ID=@License_Type_ID", cn);
//               cmd.Parameters.Add("@License_Type_ID", SqlDbType.Int).Value = License_Type_ID;
//               cn.Open();
//               IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//               if (reader.Read())
//                   return GetLicenseTypeFromReader(reader, true);
//               else
//                   return null;
//           }
//       }

//       /// <summary>
//       /// Deletes a LicenseType
//       /// </summary>
//       public override bool DeleteLicenseType(int License_Type_ID)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("delete from LicenseType where License_Type_ID=@License_Type_ID", cn);
//               cmd.Parameters.Add("@License_Type_ID", SqlDbType.Int).Value = License_Type_ID;
//               cn.Open();
//               int ret = ExecuteNonQuery(cmd);
//               return (ret == 1);
//           }
//       }

//       /// <summary>
//       /// Inserts a new LicenseType
//       /// </summary>
//       public override int InsertLicenseType(LicenseTypeInfo LicenseType)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("insert into LicenseType " +
//             "(Description) " +
//             "VALUES (" +
//             "@Description) SET @ID = SCOPE_IDENTITY()", cn);

//               cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = LicenseType.Description;

//               SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//               IDParameter.Direction = ParameterDirection.Output;
//               cmd.Parameters.Add(IDParameter);

//               cn.Open();
//               cmd.ExecuteNonQuery();

//               int NewID = (int)IDParameter.Value;
//               return NewID;

//           }
//       }

//       /// <summary>
//       /// Updates a LicenseType
//       /// </summary>
//       public override bool UpdateLicenseType(LicenseTypeInfo LicenseType)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("update LicenseType set " +
//             "Description = @Description " +
//             "where License_Type_ID = @License_Type_ID ", cn);
//               //            cmd.CommandType = CommandType.StoredProcedure;
//               cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = LicenseType.Description;
//               cmd.Parameters.Add("@License_Type_ID", SqlDbType.Int).Value = LicenseType.License_Type_ID;


//               cn.Open();
//               int ret = ExecuteNonQuery(cmd);
//               return (ret == 1);
//           }
//       }

//       /////////////////////////////////////////////////////////
//       // methods that work with TopicPages

//       /// <summary>
//       /// Returns the total number of TopicPages
//       /// </summary>
//       public override int GetTopicPageCount()
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("select Count(*) from Pages", cn);
//               cn.Open();
//               return (int)ExecuteScalar(cmd);
//           }
//       }

//       /// <summary>
//       /// Returns the total number of TopicPages for a TopicID
//       /// </summary>
//       public override int GetTopicPageCountByTopicID(int TopicID)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("select Count(*) from Pages where TopicID=@TopicID", cn);
//               cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

//               cn.Open();
//               return (int)ExecuteScalar(cmd);
//           }
//       }

//       /// <summary>
//       /// Retrieves all TopicPages
//       /// </summary>
//       public override List<TopicPageInfo> GetTopicPages(string cSortExpression)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               string cSQLCommand = "select * " +
//                   "from Pages";

//               // add on ORDER BY if provided
//               if (cSortExpression.Length > 0)
//               {
//                   cSQLCommand = cSQLCommand +
//                       " order by " + cSortExpression;
//               }

//               SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//               cn.Open();
//               return GetTopicPageCollectionFromReader(ExecuteReader(cmd), false);
//           }
//       }

//       /// <summary>
//       /// Retrieves all TopicPages
//       /// </summary>
//       public override List<TopicPageInfo> GetTopicPagesByTopicID(int TopicID, string cSortExpression)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {

//               string cSQLCommand = "select * from Pages where TopicID=@TopicID";

//               // add on ORDER BY if provided
//               if (cSortExpression.Length > 0)
//               {
//                   cSQLCommand = cSQLCommand +
//                       " order by " + cSortExpression;
//               }

//               SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//               cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

//               cn.Open();
//               return GetTopicPageCollectionFromReader(ExecuteReader(cmd), false);
//           }
//       }

//       /// <summary>
//       /// Retrieves the TopicPage with the specified TopicID and Page_Num
//       /// </summary>
//       public override TopicPageInfo GetTopicPageByTopicIDAndPageNum(int TopicID, int Page_Num)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {

//               SqlCommand cmd = new SqlCommand("select * " +
//                       "from Pages where TopicID=@TopicID and Page_Num=@Page_Num ", cn);
//               cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//               cmd.Parameters.Add("@Page_Num", SqlDbType.Int).Value = Page_Num;
//               cn.Open();
//               IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//               if (reader.Read())
//                   return GetTopicPageFromReader(reader, true);
//               else
//                   return null;
//           }
//       }

//       /// <summary>
//       /// Retrieves the TopicPage with the specified ID
//       /// </summary>
//       public override TopicPageInfo GetTopicPageByID(int PageID)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {

//               SqlCommand cmd = new SqlCommand("select * " +
//                       "from Pages where PageID=@PageID ", cn);
//               cmd.Parameters.Add("@PageID", SqlDbType.Int).Value = PageID;
//               cn.Open();
//               IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//               if (reader.Read())
//                   return GetTopicPageFromReader(reader, true);
//               else
//                   return null;
//           }
//       }

//       /// <summary>
//       /// Deletes a TopicPage
//       /// </summary>
//       public override bool DeleteTopicPage(int PageID)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("delete from Pages where PageID=@PageID", cn);
//               cmd.Parameters.Add("@PageID", SqlDbType.Int).Value = PageID;
//               cn.Open();
//               int ret = ExecuteNonQuery(cmd);

//               return (ret == 1);
                             
//           }
//       }

//       /// <summary>
//       /// Inserts a new TopicPage
//       /// </summary>
//       public override int InsertTopicPage(TopicPageInfo TopicPage)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("insert into Pages " +
//             "(TopicID, " +
//             "Page_Num, " +
//             "Page_Content) " +
//             "VALUES (" +
//             "@TopicID, " +
//             "@Page_Num, " +
//             "@Page_Content) SET @ID = SCOPE_IDENTITY()", cn);

//               cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicPage.TopicID;
//               cmd.Parameters.Add("@Page_Num", SqlDbType.Int).Value = TopicPage.Page_Num;
//               cmd.Parameters.Add("@Page_Content", SqlDbType.Text).Value = TopicPage.Page_Content;

//               SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//               IDParameter.Direction = ParameterDirection.Output;
//               cmd.Parameters.Add(IDParameter);

//               cn.Open();
//               cmd.ExecuteNonQuery();

//               int NewID = (int)IDParameter.Value;
//               return NewID;

//           }
//       }

//       /// <summary>
//       /// Updates a TopicPage
//       /// </summary>
//       public override bool UpdateTopicPage(TopicPageInfo TopicPage)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("update Pages set " +
//             "TopicID = @TopicID, " +
//             "Page_Num = @Page_Num, " +
//             "Page_Content = @Page_Content " +
//             "where PageID = @PageID ", cn);
//               //            cmd.CommandType = CommandType.StoredProcedure;
//               cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicPage.TopicID;
//               cmd.Parameters.Add("@Page_Num", SqlDbType.Int).Value = TopicPage.Page_Num;
//               cmd.Parameters.Add("@Page_Content", SqlDbType.Text).Value = TopicPage.Page_Content;
//               cmd.Parameters.Add("@PageID", SqlDbType.Int).Value = TopicPage.PageID;


//               cn.Open();
//               int ret = ExecuteNonQuery(cmd);
//               return (ret == 1);
//           }
//       }

//       /////////////////////////////////////////////////////////
//      // methods that work with Promotions

//      /// <summary>
//      /// Returns the total number of Promotions
//      /// </summary>
//      public override int GetPromotionCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Promotions", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Promotions
//      /// </summary>
//      public override List<PromotionInfo> GetPromotions(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Promotions";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetPromotionCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the Promotion with the specified ID
//      /// </summary>
//      public override PromotionInfo GetPromotionByID(int ID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from Promotions where ID=@ID", cn);
//            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetPromotionFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a Promotion
//      /// </summary>
//      public override bool DeletePromotion(int ID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from Promotions where ID=@ID", cn);
//            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new Promotion
//      /// </summary>
//      public override int InsertPromotion(PromotionInfo Promotion)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into Promotions " +
//            "(cCode, " +
//            "PromoType, " +
//            "PromoName, " +
//            "lOneTime, " +
//            "lActive, " +
//            "nDollars, " +
//            "Notes, " +
//            "nPercent, " +
//            "nUnits, " +
//            "tExpires, " +
//            "tStart, " +
//            "RepID) " +
//            "VALUES (" +
//            "@cCode, " +
//            "@PromoType, " +
//            "@PromoName, " +
//            "@lActive, " +
//            "@lOneTime, " +
//            "@nDollars, " +
//            "@Notes, " +
//            "@nPercent, " +
//            "@nUnits, " +
//            "@tExpires, " +
//            "@tStart, " +
//            "@RepID) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@cCode", SqlDbType.VarChar).Value = Promotion.cCode ;
//            cmd.Parameters.Add("@PromoType", SqlDbType.Char).Value = Promotion.PromoType;
//            cmd.Parameters.Add("@PromoName", SqlDbType.VarChar).Value = Promotion.PromoName;
//            cmd.Parameters.Add("@lActive", SqlDbType.Bit).Value = Promotion.lActive;
//            cmd.Parameters.Add("@lOneTime", SqlDbType.Bit).Value = Promotion.lOneTime ;
//            cmd.Parameters.Add("@nDollars", SqlDbType.Decimal).Value = Promotion.nDollars ;
//            cmd.Parameters.Add("@Notes", SqlDbType.VarChar).Value = Promotion.Notes ;
//            cmd.Parameters.Add("@nPercent", SqlDbType.Decimal).Value = Promotion.nPercent ;
//            cmd.Parameters.Add("@nUnits", SqlDbType.Decimal).Value = Promotion.nUnits ;
//            cmd.Parameters.Add("@tExpires", SqlDbType.DateTime).Value = Promotion.tExpires ;
//            cmd.Parameters.Add("@tStart", SqlDbType.DateTime).Value = Promotion.tStart ;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (Promotion.RepID == 0)
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = Promotion.RepID;
//            }


//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;

//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a Promotion
//      /// </summary>
//      public override bool UpdatePromotion(PromotionInfo Promotion)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Promotions set " +
//            "cCode = @cCode, " +
//            "PromoType = @PromoType, " +
//            "PromoName = @PromoName, " +
//            "lActive = @lActive, " +
//            "lOneTime = @lOneTime, " +
//            "nDollars = @nDollars, " +
//            "Notes = @Notes, " +
//            "nPercent = @nPercent, " +
//            "nUnits = @nUnits, " +
//            "tExpires = @tExpires, " +
//            "tStart = @tStart, " +
//            "RepID = @RepID " +
//            "where ID = @ID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@cCode", SqlDbType.VarChar).Value = Promotion.cCode ;
//            cmd.Parameters.Add("@PromoType", SqlDbType.Char).Value = Promotion.PromoType;
//            cmd.Parameters.Add("@PromoName", SqlDbType.VarChar).Value = Promotion.PromoName;
//            cmd.Parameters.Add("@lActive", SqlDbType.Bit).Value = Promotion.lActive;
//            cmd.Parameters.Add("@lOneTime", SqlDbType.Bit).Value = Promotion.lOneTime ;
//            cmd.Parameters.Add("@nDollars", SqlDbType.Decimal).Value = Promotion.nDollars ;
//            cmd.Parameters.Add("@Notes", SqlDbType.VarChar).Value = Promotion.Notes ;
//            cmd.Parameters.Add("@nPercent", SqlDbType.Decimal).Value = Promotion.nPercent ;
//            cmd.Parameters.Add("@nUnits", SqlDbType.Decimal).Value = Promotion.nUnits ;
//            cmd.Parameters.Add("@tExpires", SqlDbType.DateTime).Value = Promotion.tExpires ;
//            cmd.Parameters.Add("@tStart", SqlDbType.DateTime).Value = Promotion.tStart ;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (Promotion.RepID == 0)
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = Promotion.RepID;
//            }

//            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = Promotion.ID;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with UserAccounts

//      /// <summary>
//      /// Returns the total number of UserAccounts
//      /// </summary>
//      public override int GetUserAccountCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from UserAccount", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      public override int GetUserAccountByUniqueid(string uniqueid, string password)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select cpw,iid from UserAccount where uniqueid=@uniqueid and facilityid=2", cn);
//              cmd.Parameters.Add("@uniqueid", SqlDbType.Int).Value = uniqueid;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//              {
//                  if (reader["cpw"] != null)
//                  {
//                      if (password.ToString() == encrypt(encrypt(encrypt(reader["cpw"].ToString()) + "abc") + "VBF2D8EDB0"))
//                      {
//                          return (int)reader["iid"];
//                      }
//                      else
//                      {
//                          return 0;
//                      }

//                  }
//                  else
//                  {
//                      return 0;
//                  }
//              }
//              else
//              {
//                  return 0;
//              }

//          }
//      }

//      public string encrypt(string text)
//      {

//          System.Security.Cryptography.MD5CryptoServiceProvider md5Obj = new System.Security.Cryptography.MD5CryptoServiceProvider();

//          byte[] bytesToHash = md5Obj.ComputeHash(System.Text.Encoding.ASCII.GetBytes(text));

//          string strResult = "";

//          foreach (byte b in bytesToHash)
//          {


//              strResult += b.ToString("x2");
//          }
//          return strResult;
//      }


//      /// Returns the UserID for a UserName
//      /// </summary>
//      public override int GetUserIDByUserName(string UserName)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select iID from UserAccount where lcUserName=@cUserName", cn);
//              cmd.Parameters.Add("@cUserName", SqlDbType.VarChar).Value = UserName.ToLower();
//              cn.Open();

//              //TODO: Handle NULL result
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//     /// Returns the taxrate for UserId
//     /// 
//      public override decimal GetTaxRateByUserId(int iID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.RConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select S_State from nurse where rniD=@iID", cn);
//              cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//              {
//                  using (SqlConnection Retailcn = new SqlConnection(this.ConnectionString))
//                  {
//                      SqlCommand cmd1 = new SqlCommand("select taxrate from StateReq where stateabr=@state", Retailcn);
//                      cmd1.Parameters.Add("@state", SqlDbType.VarChar).Value = reader[0].ToString();
//                      Retailcn.Open();
//                      IDataReader reader1 = ExecuteReader(cmd1);
//                      if (reader1.Read())
//                      {
//                          return (decimal)reader1[0];
//                      }
//                      else
//                      {
//                          return (decimal)-1.0;
//                      }
//                  }
//              }
//              else
//              {
//                  return (decimal)-1.0;
//              }

//          }
//      }


//      /// Returns the UserID for a ShortUserName and FacilityID
//      /// </summary>
//      public override int GetUserIDByShortUserNameAndFacilityID(string ShortUserName, int FacilityID)
//      {
//          int ret = 0;
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select iID from UserAccount where cUserName=@ShortUserName and FacilityID=@FacilityID", cn);
//              cmd.Parameters.Add("@ShortUserName", SqlDbType.VarChar).Value = ShortUserName.ToLower();
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
//              cn.Open();
//              try
//              {
//                  //TODO: Handle NULL result
//                  ret = (int)ExecuteScalar(cmd);
//              }
//              catch (Exception ex)
//              {
//              }
//          }
//              return ret;
//          }


//      public override bool GetGiftCardUsersFromUserAccountInfoList(string cGiftUsername)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select * from useraccount where " +
//                  "giftcard_ind='1' and cusername= @UserName", cn);
//              cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = cGiftUsername;           

//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//              {
//                  return true;
//              }
//              else
//              {
//                  return false;
//              }              
//          }
//      }
      

//      /// <summary>
//      /// Retrieves all UserAccounts
//      /// </summary>
//      public override List<UserAccountInfo> GetUserAccounts(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                  "cExpires, cFirstName, cFloridaNo, " +
//                  "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                  "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                  "SpecID, LCUserName, " +
//                  "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                  "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                  "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                  "RegTypeID, RepID, Work_Phone, Badge_ID, " +
//                  "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                  "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                  "from UserAccount";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves most recent 300 UserAccounts
//      /// </summary>
//      public override List<UserAccountInfo> GetRecentUserAccounts()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select top 300 " +
//                  "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                  "cExpires, cFirstName, cFloridaNo, " +
//                  "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                  "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                  "SpecID, LCUserName, " +
//                  "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                  "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                  "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                  "RegTypeID, RepID,Work_Phone, Badge_ID, " +
//                  "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                  "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " + 
//                  "from UserAccount order by created desc ";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the UserAccount with the specified ID
//      /// </summary>
//      public override UserAccountInfo GetUserAccountByID(int iID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select " +
//                  "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                  "cExpires, cFirstName, cFloridaNo, " +
//                  "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                  "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                  "SpecID, LCUserName, " +
//                  "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                  "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                  "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                  "RegTypeID, RepID,Work_Phone, Badge_ID, " +
//                  "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                  "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                  "from UserAccount where iID=@iID", cn);
//            cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetUserAccountFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Retrieves the UserAccount with the specified username and facilityid
//      /// </summary>
//      public override UserAccountInfo GetUserAccountByShortUserNameAndFacilityID(string ShortUserName, int FacilityID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select " +
//                  "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                  "cExpires, cFirstName, cFloridaNo, " +
//                  "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                  "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                  "SpecID, LCUserName, " +
//                  "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                  "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                  "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                  "RegTypeID, RepID,Work_Phone, Badge_ID, " +
//                  "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                  "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                  "from UserAccount where cUserName=@ShortUserName and FacilityID = @FacilityID ", cn);
//              cmd.Parameters.Add("@ShortUserName", SqlDbType.VarChar).Value = ShortUserName.ToLower();
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;

//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetUserAccountFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a UserAccount
//      /// </summary>
//      public override bool DeleteUserAccount(int iID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from UserAccount where iID=@iID", cn);
//            cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new UserAccount
//      /// </summary>
//      public override int InsertUserAccount(UserAccountInfo UserAccount)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into UserAccount " +
//            "(cAddress1, " +
//            "cAddress2, " +
//            "lAdmin, " +
//            "lPrimary, " +
//            "cBirthDate, " +
//            "cCity, " +
//            "cEmail, " +
//            "cExpires, " +
//            "cFirstName, " +
//            "cFloridaNo, " +
//            "cInstitute, " +
//            "cLastName, " +
//            "cLectDate, " +
//            "cMiddle, " +
//            "cPhone, " +
//            "cPromoCode, " +
//            "PromoID, " +
//            "FacilityID, " +
//            "cPW, " +
//            "cRefCode, " +
//            "cRegType, " +
//            "cSocial, " +
//            "cState, " +
//            "cUserName, " +
//            "cZipCode, " +
//            "SpecID, " +
//            "LCUserName, " +
//            "LastAct, " +
//            "PassFmt, " +
//            "PassSalt, " +
//            "MobilePIN, " +
//            "LCEmail, " +
//            "PWQuest, " +
//            "PWAns, " +
//            "IsApproved, " +
//            "IsOnline, " +
//            "IsLocked, " +
//            "LastLogin, " +
//            "LastPWChg, " +
//            "LastLock, " +
//            "XPWAtt, " +
//            "XPWAttSt, " +
//            "XPWAnsAtt, " +
//            "XPWAnsSt, " +
//            "Comment, " +
//            "Created, " +
//            "AgeGroup, " +
//            "RegTypeID, " +
//            "RepID, " +
//            "Work_Phone," +
//            "Badge_ID," +
//            "Hire_Date," +
//            "Termin_Date," +
//            "Dept_ID," +
//            "Position_ID," +
//            "Clinical_Ind," +
//            "Title," +
//            "Giftcard_Ind," +
//            "Giftcard_Chour," +
//            "Giftcard_Uhour," +
//            "UniqueID," +
//            "FirstLogin_Ind ) " +
//            "VALUES (" +
//              "@cAddress1, " +
//              "@cAddress2, " +
//              "@lAdmin, " +
//              "@lPrimary, " +
//              "@cBirthDate, " +
//              "@cCity, " +
//              "@cEmail, " +
//              "@cExpires, " +
//              "@cFirstName, " +
//              "@cFloridaNo, " +
//              "@cInstitute, " +
//              "@cLastName, " +
//              "@cLectDate, " +
//              "@cMiddle, " +
//              "@cPhone, " +
//            "@cPromoCode, " +
//            "@PromoID, " +
//            "@FacilityID, " +
//            "@cPW, " +
//            "@cRefCode, " +
//            "@cRegType, " +
//            "@cSocial, " +
//            "@cState, " +
//            "@cUserName, " +
//            "@cZipCode, " +
//            "@SpecID, " +
//            "@LCUserName, " +
//            "@LastAct, " +
//            "@PassFmt, " +
//            "@PassSalt, " +
//            "@MobilePIN, " +
//            "@LCEmail, " +
//            "@PWQuest, " +
//            "@PWAns, " +
//            "@IsApproved, " +
//            "@IsOnline, " +
//            "@IsLocked, " +
//            "@LastLogin, " +
//            "@LastPWChg, " +
//            "@LastLock, " +
//            "@XPWAtt, " +
//            "@XPWAttSt, " +
//            "@XPWAnsAtt, " +
//            "@XPWAnsSt, " +
//            "@Comment, " +
//            "@Created, " +
//            "@AgeGroup, " +
//            "@RegTypeID, " +
//            "@RepID, " +
//              "@Work_Phone, " +
//              "@Badge_ID, " +
//              "@Hire_Date, " +
//              "@Termin_Date, " +
//              "@Dept_ID, " +
//              "@Position_ID, " +
//              "@Clinical_Ind, " +
//              "@Title, " +
//              "@Giftcard_Ind, " +
//              "@Giftcard_Chour, " +
//              "@Giftcard_Uhour, " +
//              "@UniqueID, " +
//              "@FirstLogin_Ind) SET @ID = SCOPE_IDENTITY()", cn);
//            cmd.Parameters.Add("@cAddress1", SqlDbType.VarChar).Value = UserAccount.cAddress1;
//            cmd.Parameters.Add("@cAddress2", SqlDbType.VarChar).Value = UserAccount.cAddress2 ;
//            cmd.Parameters.Add("@lAdmin", SqlDbType.Bit).Value = UserAccount.lAdmin ;
//            cmd.Parameters.Add("@lPrimary", SqlDbType.Bit).Value = UserAccount.lPrimary;
//            cmd.Parameters.Add("@cBirthDate", SqlDbType.VarChar).Value = UserAccount.cBirthDate;
//            cmd.Parameters.Add("@cCity", SqlDbType.VarChar).Value = UserAccount.cCity ;
//            cmd.Parameters.Add("@cEmail", SqlDbType.VarChar).Value = UserAccount.cEmail ;
//            cmd.Parameters.Add("@cExpires", SqlDbType.VarChar).Value = UserAccount.cExpires ;
//            cmd.Parameters.Add("@cFirstName", SqlDbType.VarChar).Value = UserAccount.cFirstName ;
//            cmd.Parameters.Add("@cFloridaNo", SqlDbType.VarChar).Value = UserAccount.cFloridaNo ;
//            cmd.Parameters.Add("@cInstitute", SqlDbType.VarChar).Value = UserAccount.cInstitute ;
//            cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = UserAccount.cLastName ;
//            cmd.Parameters.Add("@cLectDate", SqlDbType.VarChar).Value = UserAccount.cLectDate ;
//            cmd.Parameters.Add("@cMiddle", SqlDbType.VarChar).Value = UserAccount.cMiddle ;
//            cmd.Parameters.Add("@cPhone", SqlDbType.VarChar).Value = UserAccount.cPhone ;
//            cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = UserAccount.cPromoCode ;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.PromoID == 0)
//            {
//                cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = UserAccount.PromoID;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.FacilityID == 0)
//            {
//                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = UserAccount.FacilityID;
//            }

//            cmd.Parameters.Add("@cPW", SqlDbType.VarChar).Value = UserAccount.cPW.ToLower();
//            cmd.Parameters.Add("@cRefCode", SqlDbType.VarChar).Value = UserAccount.cRefCode ;
//            cmd.Parameters.Add("@cRegType", SqlDbType.VarChar).Value = UserAccount.cRegType ;
//            cmd.Parameters.Add("@cSocial", SqlDbType.VarChar).Value = UserAccount.cSocial ;
//            cmd.Parameters.Add("@cState", SqlDbType.Char).Value = UserAccount.cState ;
//            cmd.Parameters.Add("@cUserName", SqlDbType.VarChar).Value = UserAccount.cUserName.ToLower();
//            cmd.Parameters.Add("@cZipCode", SqlDbType.VarChar).Value = UserAccount.cZipCode ;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.SpecID == 0)
//            {
//                cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = UserAccount.SpecID;
//            }

//            cmd.Parameters.Add("@LCUserName", SqlDbType.VarChar).Value = UserAccount.LCUserName.ToLower();
//            cmd.Parameters.Add("@PassFmt", SqlDbType.VarChar).Value = UserAccount.PassFmt ;
//            cmd.Parameters.Add("@PassSalt", SqlDbType.VarChar).Value = UserAccount.PassSalt ;
//            cmd.Parameters.Add("@MobilePIN", SqlDbType.VarChar).Value = UserAccount.MobilePIN;
//            cmd.Parameters.Add("@LCEmail", SqlDbType.VarChar).Value = UserAccount.LCEmail ;
//            cmd.Parameters.Add("@PWQuest", SqlDbType.VarChar).Value = UserAccount.PWQuest ;
//            cmd.Parameters.Add("@PWAns", SqlDbType.VarChar).Value = UserAccount.PWAns.ToLower();
//            cmd.Parameters.Add("@IsApproved", SqlDbType.Bit).Value = UserAccount.IsApproved ;
//            cmd.Parameters.Add("@IsOnline", SqlDbType.Bit).Value = UserAccount.IsOnline ;
//            cmd.Parameters.Add("@IsLocked", SqlDbType.Bit).Value = UserAccount.IsLocked ;
//            cmd.Parameters.Add("@XPWAtt", SqlDbType.Int).Value = UserAccount.XPWAtt ;
//            cmd.Parameters.Add("@XPWAnsAtt", SqlDbType.Int).Value = UserAccount.XPWAnsAtt ;
//            cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = UserAccount.Comment ;
//            cmd.Parameters.Add("@Created", SqlDbType.DateTime).Value = UserAccount.Created ;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.AgeGroup == 0)
//            {
//                cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = UserAccount.AgeGroup;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.RegTypeID == 0)
//            {
//                cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = UserAccount.RegTypeID;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.RepID == 0)
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = UserAccount.RepID;
//            }


//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (UserAccount.Work_Phone == "0")
//            //{
//            //    cmd.Parameters.Add("@Work_Phone", SqlDbType.VarChar).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            cmd.Parameters.Add("@Work_Phone", SqlDbType.VarChar).Value = UserAccount.Work_Phone;
//            //}

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (UserAccount.Badge_ID == "0")
//            //{
//            //    cmd.Parameters.Add("@Badge_ID", SqlDbType.VarChar).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            cmd.Parameters.Add("@Badge_ID", SqlDbType.VarChar).Value = UserAccount.Badge_ID;
//            //}

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.LastAct == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@LastAct", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@LastAct", SqlDbType.DateTime).Value = UserAccount.LastAct;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.LastLogin == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = UserAccount.LastLogin;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.LastPWChg == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@LastPWChg", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@LastPWChg", SqlDbType.DateTime).Value = UserAccount.LastPWChg;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.LastLock == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@LastLock", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@LastLock", SqlDbType.DateTime).Value = UserAccount.LastLock;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.XPWAttSt == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@XPWAttSt", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@XPWAttSt", SqlDbType.DateTime).Value = UserAccount.XPWAttSt;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.XPWAnsSt == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@XPWAnsSt", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@XPWAnsSt", SqlDbType.DateTime).Value = UserAccount.XPWAnsSt;
//            }

//              // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.Hire_Date == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@Hire_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Hire_Date", SqlDbType.DateTime).Value = UserAccount.Hire_Date;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.Termin_Date == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@Termin_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Termin_Date", SqlDbType.DateTime).Value = UserAccount.Termin_Date;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.Dept_ID == 0)
//            {
//                cmd.Parameters.Add("@Dept_ID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Dept_ID", SqlDbType.Int).Value = UserAccount.Dept_ID;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.Position_ID == 0)
//            {
//                cmd.Parameters.Add("@Position_ID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Position_ID", SqlDbType.Int).Value = UserAccount.Position_ID;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.Clinical_Ind == 0)
//            {
//                cmd.Parameters.Add("@Clinical_Ind", SqlDbType.Bit).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Clinical_Ind", SqlDbType.Bit).Value = UserAccount.Clinical_Ind;
//            }

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (UserAccount.Title == "0")
//            //{
//            //    cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = UserAccount.Title;
//            //}

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.Giftcard_Ind == false)
//            {
//                cmd.Parameters.Add("@Giftcard_Ind", SqlDbType.Bit).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Giftcard_Ind", SqlDbType.Bit).Value = UserAccount.Giftcard_Ind;
//            }

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (UserAccount.Giftcard_Chour == 0)
//            //{
//            //    cmd.Parameters.Add("@Giftcard_Chour", SqlDbType.Decimal).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            cmd.Parameters.Add("@Giftcard_Chour", SqlDbType.Decimal).Value = UserAccount.Giftcard_Chour;
//            //}

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (UserAccount.Giftcard_Uhour == 0)
//            //{
//            //    cmd.Parameters.Add("@Giftcard_Uhour", SqlDbType.Decimal).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            cmd.Parameters.Add("@Giftcard_Uhour", SqlDbType.Decimal).Value = UserAccount.Giftcard_Uhour;
//            //}

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (UserAccount.UniqueID == 0)
//            //{
//            //    cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = UserAccount.UniqueID;
//            //}
//            cmd.Parameters.Add("@FirstLogin_Ind", SqlDbType.Bit).Value = UserAccount.FirstLogin_Ind;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }


//      // /// TODO: Check on this!!!!!

//      // /// <summary>
//      ///// Returns the highest UserAccountID
//      ///// (used by the front-end to find the last ID inserted because
//      ///// all other methods via ObjectDataSource are failing)
//      ///// </summary>
//      //public override int GetNextUserAccountID()
//      //{
//      //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//      //    {
//      //        SqlCommand cmd = new SqlCommand("select top 1 cast(iID as Integer) as iID from UserAccount order by iID Desc", cn);
//      //        cn.Open();
//      //        int iID;
//      //        iID = (int)ExecuteScalar(cmd);
//      //        return ++iID;
//      //    }
//      //}

//       /// <summary>
//      /// Updates a UserAccount
//      /// </summary>
//      public override bool UpdateUserAccount(UserAccountInfo UserAccount)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update UserAccount set " +
//            "cAddress1 = @cAddress1, " +
//            "cAddress2 = @cAddress2, " +
//            "lAdmin = @lAdmin, " +
//            "lPrimary = @lPrimary, " +
//            "cBirthDate = @cBirthDate, " +
//            "cCity = @cCity, " +
//            "cEmail = @cEmail, " +
//            "cExpires = @cExpires, " +
//            "cFirstName = @cFirstName, " +
//            "cFloridaNo = @cFloridaNo, " +
//            "cInstitute = @cInstitute, " +
//            "cLastName = @cLastName, " +
//            "cLectDate = @cLectDate, " +
//            "cMiddle = @cMiddle, " +
//            "cPhone = @cPhone, " +
//            "cPromoCode = @cPromoCode, " +
//            "PromoID = @PromoID, " +
//            "FacilityID = @FacilityID, " +
//            "cPW = @cPW, " +
//            "cRefCode = @cRefCode, " +
//            "cRegType = @cRegType, " +
//            "cSocial = @cSocial, " +
//            "cState = @cState, " +
//            "cUserName = @cUserName, " +
//            "cZipCode = @cZipCode, " +
//            "SpecID = @SpecID, " +
//            "LCUserName = @LCUserName, " +
//            "LastAct = @LastAct, " +
//            "PassFmt = @PassFmt, " +
//            "PassSalt = @PassSalt, " +
//            "MobilePIN = @MobilePIN, " +
//            "LCEmail = @LCEmail, " +
//            "PWQuest = @PWQuest, " +
//            "PWAns = @PWAns, " +
//            "IsApproved = @IsApproved, " +
//            "IsOnline = @IsOnline, " +
//            "IsLocked = @IsLocked, " +
//            "LastLogin = @LastLogin, " +
//            "LastPWChg = @LastPWChg, " +
//            "LastLock = @LastLock, " +
//            "XPWAtt = @XPWAtt, " +
//            "XPWAttSt = @XPWAttSt, " +
//            "XPWAnsAtt = @XPWAnsAtt, " +
//            "XPWAnsSt = @XPWAnsSt, " +
//            "Comment = @Comment, " +
//            "Created = @Created, " +
//            "AgeGroup = @AgeGroup, " +
//            "RegTypeID = @RegTypeID, " +
//            "RepID = @RepID, " +
//            "Work_Phone = @Work_Phone, " +
//            "Badge_ID = @Badge_ID, " +
//            "Hire_Date = @Hire_Date, " +
//            "Termin_Date = @Termin_Date, " +
//            "Dept_ID = @Dept_ID, " +
//            "Position_ID = @Position_ID, " +
//            "Clinical_Ind = @Clinical_Ind, " +
//            "Title = @Title, " +
//            "Giftcard_Ind = @Giftcard_Ind, " +
//            "Giftcard_Chour = @Giftcard_Chour, " +
//            "Giftcard_Uhour = @Giftcard_Uhour, " +
//            "UniqueID = @UniqueID, " +
//            "FirstLogin_Ind = @FirstLogin_Ind " +
//            "where iID = @iID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;

              
//              cmd.Parameters.Add("@cAddress1", SqlDbType.VarChar).Value = UserAccount.cAddress1 ;
//            cmd.Parameters.Add("@cAddress2", SqlDbType.VarChar).Value = UserAccount.cAddress2 ;
//            cmd.Parameters.Add("@lAdmin", SqlDbType.Bit).Value = UserAccount.lAdmin ;
//            cmd.Parameters.Add("@lPrimary", SqlDbType.Bit).Value = UserAccount.lPrimary;
//            cmd.Parameters.Add("@cBirthDate", SqlDbType.VarChar).Value = UserAccount.cBirthDate;
//            cmd.Parameters.Add("@cCity", SqlDbType.VarChar).Value = UserAccount.cCity ;
//            cmd.Parameters.Add("@cEmail", SqlDbType.VarChar).Value = UserAccount.cEmail ;
//            cmd.Parameters.Add("@cExpires", SqlDbType.VarChar).Value = UserAccount.cExpires ;
//            cmd.Parameters.Add("@cFirstName", SqlDbType.VarChar).Value = UserAccount.cFirstName ;
//            cmd.Parameters.Add("@cFloridaNo", SqlDbType.VarChar).Value = UserAccount.cFloridaNo ;
//            cmd.Parameters.Add("@cInstitute", SqlDbType.VarChar).Value = UserAccount.cInstitute ;
//            cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = UserAccount.cLastName ;
//            cmd.Parameters.Add("@cLectDate", SqlDbType.VarChar).Value = UserAccount.cLectDate ;
//            cmd.Parameters.Add("@cMiddle", SqlDbType.VarChar).Value = UserAccount.cMiddle ;
//            cmd.Parameters.Add("@cPhone", SqlDbType.VarChar).Value = UserAccount.cPhone ;
//            cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = UserAccount.cPromoCode ;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.PromoID == 0)
//            {
//                cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = UserAccount.PromoID;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.FacilityID == 0)
//            {
//                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = UserAccount.FacilityID;
//            }

//            cmd.Parameters.Add("@cPW", SqlDbType.VarChar).Value = UserAccount.cPW.ToLower();
//            cmd.Parameters.Add("@cRefCode", SqlDbType.VarChar).Value = UserAccount.cRefCode ;
//            cmd.Parameters.Add("@cRegType", SqlDbType.VarChar).Value = UserAccount.cRegType ;
//            cmd.Parameters.Add("@cSocial", SqlDbType.VarChar).Value = UserAccount.cSocial ;
//            cmd.Parameters.Add("@cState", SqlDbType.Char).Value = UserAccount.cState ;
//            cmd.Parameters.Add("@cUserName", SqlDbType.VarChar).Value = UserAccount.cUserName.ToLower();
//            cmd.Parameters.Add("@cZipCode", SqlDbType.VarChar).Value = UserAccount.cZipCode ;

//              // pass null to database if value is zero (nothing selected by user)
//              // this allows the database to have a NULL foreign key and still enforce relational integrity
//              if (UserAccount.SpecID == 0)
//              {
//                  cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = System.DBNull.Value;
//              }
//              else
//              {
//                  cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = UserAccount.SpecID;
//              }

//              cmd.Parameters.Add("@LCUserName", SqlDbType.VarChar).Value = UserAccount.LCUserName.ToLower();
//            cmd.Parameters.Add("@PassFmt", SqlDbType.VarChar).Value = UserAccount.PassFmt ;
//            cmd.Parameters.Add("@PassSalt", SqlDbType.VarChar).Value = UserAccount.PassSalt ;
//            cmd.Parameters.Add("@MobilePIN", SqlDbType.VarChar).Value = UserAccount.MobilePIN ;
//            cmd.Parameters.Add("@LCEmail", SqlDbType.VarChar).Value = UserAccount.LCEmail ;
//            cmd.Parameters.Add("@PWQuest", SqlDbType.VarChar).Value = UserAccount.PWQuest ;
//            cmd.Parameters.Add("@PWAns", SqlDbType.VarChar).Value = UserAccount.PWAns.ToLower();
//            cmd.Parameters.Add("@IsApproved", SqlDbType.Bit).Value = UserAccount.IsApproved ;
//            cmd.Parameters.Add("@IsOnline", SqlDbType.Bit).Value = UserAccount.IsOnline ;
//            cmd.Parameters.Add("@IsLocked", SqlDbType.Bit).Value = UserAccount.IsLocked ;
//            cmd.Parameters.Add("@XPWAtt", SqlDbType.Int).Value = UserAccount.XPWAtt ;
//            cmd.Parameters.Add("@XPWAnsAtt", SqlDbType.Int).Value = UserAccount.XPWAnsAtt ;
//            cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = UserAccount.Comment ;
//            cmd.Parameters.Add("@Created", SqlDbType.DateTime).Value = UserAccount.Created ;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.AgeGroup == 0)
//            {
//                cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = UserAccount.AgeGroup;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.RegTypeID == 0)
//            {
//                cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = UserAccount.RegTypeID;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.RepID == 0)
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = UserAccount.RepID;
//            }

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (UserAccount.Work_Phone == "0")
//            //{
//            //    cmd.Parameters.Add("@Work_Phone", SqlDbType.VarChar).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            cmd.Parameters.Add("@Work_Phone", SqlDbType.VarChar).Value = UserAccount.Work_Phone;
//            //}

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (UserAccount.Badge_ID == "0")
//            //{
//            //    cmd.Parameters.Add("@Badge_ID", SqlDbType.VarChar).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            cmd.Parameters.Add("@Badge_ID", SqlDbType.VarChar).Value = UserAccount.Badge_ID;
//            //}

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.LastAct == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@LastAct", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@LastAct", SqlDbType.DateTime).Value = UserAccount.LastAct;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.LastLogin == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = UserAccount.LastLogin;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.LastPWChg == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@LastPWChg", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@LastPWChg", SqlDbType.DateTime).Value = UserAccount.LastPWChg;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.LastLock == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@LastLock", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@LastLock", SqlDbType.DateTime).Value = UserAccount.LastLock;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.XPWAttSt == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@XPWAttSt", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@XPWAttSt", SqlDbType.DateTime).Value = UserAccount.XPWAttSt;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            if (UserAccount.XPWAnsSt == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@XPWAnsSt", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@XPWAnsSt", SqlDbType.DateTime).Value = UserAccount.XPWAnsSt;
//            }


//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.Hire_Date == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@Hire_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Hire_Date", SqlDbType.DateTime).Value = UserAccount.Hire_Date;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.Termin_Date == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@Termin_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Termin_Date", SqlDbType.DateTime).Value = UserAccount.Termin_Date;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.Dept_ID == 0)
//            {
//                cmd.Parameters.Add("@Dept_ID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Dept_ID", SqlDbType.Int).Value = UserAccount.Dept_ID;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.Position_ID == 0)
//            {
//                cmd.Parameters.Add("@Position_ID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Position_ID", SqlDbType.Int).Value = UserAccount.Position_ID;
//            }

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.Clinical_Ind == 0)
//            {
//                cmd.Parameters.Add("@Clinical_Ind", SqlDbType.Bit).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Clinical_Ind", SqlDbType.Bit).Value = UserAccount.Clinical_Ind;
//            }

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (UserAccount.Title == "0")
//            //{
//            //    cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = UserAccount.Title;
//            //}

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (UserAccount.Giftcard_Ind == false)
//            {
//                cmd.Parameters.Add("@Giftcard_Ind", SqlDbType.Bit).Value = 0;
//            }
//            else
//            {
//                cmd.Parameters.Add("@Giftcard_Ind", SqlDbType.Bit).Value = UserAccount.Giftcard_Ind;
//            }

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (UserAccount.Giftcard_Chour == 0)
//            //{
//            //    cmd.Parameters.Add("@Giftcard_Chour", SqlDbType.Decimal).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            cmd.Parameters.Add("@Giftcard_Chour", SqlDbType.Decimal).Value = UserAccount.Giftcard_Chour;
//            //}

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (UserAccount.Giftcard_Uhour == 0)
//            //{
//            //    cmd.Parameters.Add("@Giftcard_Uhour", SqlDbType.Decimal).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            cmd.Parameters.Add("@Giftcard_Uhour", SqlDbType.Decimal).Value = UserAccount.Giftcard_Uhour;
//            //}

             
//              cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = UserAccount.UniqueID;
//              cmd.Parameters.Add("@FirstLogin_Ind", SqlDbType.Bit).Value = UserAccount.FirstLogin_Ind;
            
//              cmd.Parameters.Add("@iID", SqlDbType.Int).Value = UserAccount.iID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of UserAccounts for the specified promo code
//      /// </summary>
//      public override int GetUserAccountCountByPromoCode(string cPromoCode)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from UserAccount where cPromoCode = @cPromoCode", cn);
////              cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = cPromoCode;
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all UserAccounts for the specified promo code
//      /// </summary>
//      public override List<UserAccountInfo> GetUserAccountsByPromoCode(string cPromoCode, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                  "cExpires, cFirstName, cFloridaNo, " +
//                  "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                  "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                  "SpecID, LCUserName, " +
//                  "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                  "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                  "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                  "RegTypeID, RepID, Work_Phone, Badge_ID, " +
//                  "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                  "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                  "from UserAccount where cPromoCode = @cPromoCode";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {

//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//            cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = cPromoCode;
//            cn.Open();
//            return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
//         }
//      }

//      /// <summary>
//      /// Retrieves all UserAccounts for the specified lastname (can be portion)
//      /// </summary>
//      public override List<UserAccountInfo> GetUserAccountsByLastName(string cLastName, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                  "cExpires, cFirstName, cFloridaNo, " +
//                  "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                  "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                  "SpecID, LCUserName, " +
//                  "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                  "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                  "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                  "RegTypeID, RepID, Work_Phone, Badge_ID, " +
//                  "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                  "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                  "from UserAccount where cLastName Like @cLastName";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {

//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = cLastName + '%';
//              cn.Open();
//              return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all UserAccounts for the specified ShortUserName (can be portion)
//      /// NOTE: The ShortUsername is the cUserName field without the facilityid on the front of it
//      /// </summary>
//      public override List<UserAccountInfo> GetUserAccountsByShortUserName(string ShortUserName, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                  "cExpires, cFirstName, cFloridaNo, " +
//                  "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                  "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                  "SpecID, LCUserName, " +
//                  "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                  "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                  "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                  "RegTypeID, RepID, Work_Phone, Badge_ID, " +
//                  "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                  "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                  "from UserAccount where cUserName Like @ShortUserName";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {

//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@ShortUserName", SqlDbType.VarChar).Value = ShortUserName.ToLower() + '%';
//              cn.Open();
//              return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all UserAccounts for the specified email (can be portion)
//      /// </summary>
//      public override List<UserAccountInfo> GetUserAccountsByEmail(string cEmail, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                  "cExpires, cFirstName, cFloridaNo, " +
//                  "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                  "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                  "SpecID, LCUserName, " +
//                  "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                  "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                  "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                  "RegTypeID, RepID, Work_Phone, Badge_ID, " +
//                  "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                  "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                  "from UserAccount where cEmail Like @cEmail";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {

//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@cEmail", SqlDbType.VarChar).Value = cEmail + '%';
//              cn.Open();
//              return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of UserAccounts for the specified promoID
//      /// </summary>
//      public override int GetUserAccountCountByPromoID(int PromoID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from UserAccount where PromoID = @PromoID", cn);
//              //              cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = PromoID;
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all UserAccounts for the specified promoID
//      /// </summary>
//      public override List<UserAccountInfo> GetUserAccountsByPromoID(int PromoID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                  "cExpires, cFirstName, cFloridaNo, " +
//                  "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                  "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                  "SpecID, LCUserName, " +
//                  "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                  "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                  "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                  "RegTypeID, RepID, Work_Phone, Badge_ID, " +
//                  "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                  "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                  "from UserAccount where PromoID = @PromoID";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {

//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = PromoID;
//              cn.Open();
//              return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of UserAccounts for the specified FacilityID
//      /// </summary>
//      public override int GetUserAccountCountByFacilityID(int FacilityID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from UserAccount where FacilityID = @FacilityID", cn);
//              //              cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all UserAccounts for the specified FacilityID
//      /// </summary>
//      public override List<UserAccountInfo> GetUserAccountsByFacilityID(int FacilityID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                  "cExpires, cFirstName, cFloridaNo, " +
//                  "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                  "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                  "SpecID, LCUserName, " +
//                  "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                  "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                  "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                  "RegTypeID, RepID, Work_Phone, Badge_ID, " +
//                  "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                  "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                  "from UserAccount where FacilityID = @FacilityID";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {

//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
//              cn.Open();
//              return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//       /// <summary>
//      /// Retrieves all admin UserAccounts for the specified FacilityID
//      /// </summary>
//      public override List<UserAccountInfo> GetAdminUserAccountsByFacilityID(int FacilityID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                  "cExpires, cFirstName, cFloridaNo, " +
//                  "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                  "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                  "SpecID, LCUserName, " +
//                  "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                  "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                  "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                  "RegTypeID, RepID, Work_Phone, Badge_ID, " +
//                  "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                  "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                  "from UserAccount where FacilityID = @FacilityID and lAdmin='1'";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {

//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
//              cn.Open();
//              return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Updates a UserAccount Admin Status
//      /// </summary>
//      public override bool UpdateUserAccountAdminStatus(int iID, bool lAdmin)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update UserAccount set " +
//                  "lAdmin = @lAdmin where iid = @iID", cn);

//              cmd.Parameters.Add("@lAdmin", SqlDbType.Bit).Value = lAdmin;
//              cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;

//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /////////////////////////////////////////////////////////
//      // methods that work with RegistrationTypes

//      /// <summary>
//      /// Returns the total number of RegistrationTypes
//      /// </summary>
//      public override int GetRegistrationTypeCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from RegistrationType", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all RegistrationTypes
//      /// </summary>
//      public override List<RegistrationTypeInfo> GetRegistrationTypes(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from RegistrationType";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }
//              else
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by RegType";
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetRegistrationTypeCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the RegistrationType with the specified ID
//      /// </summary>
//      public override RegistrationTypeInfo GetRegistrationTypeByID(int RegTypeID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from RegistrationType where RegTypeID=@RegTypeID", cn);
//            cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = RegTypeID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetRegistrationTypeFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a RegistrationType
//      /// </summary>
//      public override bool DeleteRegistrationType(int RegTypeID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from RegistrationType where RegTypeID=@RegTypeID", cn);
//            cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = RegTypeID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new RegistrationType
//      /// </summary>
//      public override int InsertRegistrationType(RegistrationTypeInfo RegistrationType)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into RegistrationType " +
//            "(RegType) " +
//            "VALUES (" +
//            "@RegType) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@RegType", SqlDbType.VarChar).Value = RegistrationType.RegType ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a RegistrationType
//      /// </summary>
//      public override bool UpdateRegistrationType(RegistrationTypeInfo RegistrationType)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update RegistrationType set " +
//            "RegType = @RegType " +
//            "where RegTypeID = @RegTypeID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@RegType", SqlDbType.VarChar).Value = RegistrationType.RegType ;
//            cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = RegistrationType.RegTypeID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with ResourcePersons

//      /// <summary>
//      /// Returns the total number of ResourcePersons
//      /// </summary>
//      public override int GetResourcePersonCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from ResourcePerson where FacilityID=0", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all ResourcePersons
//      /// </summary>
//      public override List<ResourcePersonInfo> GetResourcePersons(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from ResourcePerson";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetResourcePersonCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//       /// <summary>
//       /// Retrieves all ResourcePersons for a TopicID and Resource Type
//       /// (author="A", editor="E", nurse planner="N")
//       /// </summary>
//       public override List<ResourcePersonInfo> GetResourcePersonsByTopicIDAndType(int TopicID,
//      string ResourceType, string cSortExpression)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               string cSQLCommand = "select * " +
//                   "from ResourcePerson " +
//                   "join TopicResourceLink on ResourcePerson.ResourceID = TopicResourceLink.ResourceID " +
//                   "where TopicResourceLink.TopicID = @TopicID " +
//                   "and TopicResourceLink.ResType = @ResourceType ";

//               // add on ORDER BY if provided
//               if (cSortExpression.Length > 0)
//               {
//                   cSQLCommand = cSQLCommand +
//                       " order by " + cSortExpression;
//               }

//               SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//               cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//               cmd.Parameters.Add("@ResourceType", SqlDbType.VarChar).Value = ResourceType;

//               cn.Open();
//               return GetResourcePersonCollectionFromReader(ExecuteReader(cmd), false);
//           }
//       }

//      /// <summary>
//      /// Retrieves the ResourcePerson with the specified ID
//      /// </summary>
//      public override ResourcePersonInfo GetResourcePersonByID(int ResourceID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from ResourcePerson where ResourceID=@ResourceID", cn);
//            cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = ResourceID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetResourcePersonFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Retrieves the ResourcePerson with the Facility ID
//      /// </summary>
//      public override List<ResourcePersonInfo> GetResourcePersonByFacilityID(int FacilityID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from ResourcePerson where facilityid=@FacilityID", cn);
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
//              cn.Open();
//              return GetResourcePersonCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }



//      /// <summary>
//      /// Deletes a ResourcePerson
//      /// </summary>
//      public override bool DeleteResourcePerson(int ResourceID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from ResourcePerson where ResourceID=@ResourceID", cn);
//            cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = ResourceID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new ResourcePerson
//      /// </summary>
//      public override int InsertResourcePerson(ResourcePersonInfo ResourcePerson)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into ResourcePerson " +
//            "(Fullname, " +
//            "LastFirst, " +
//            "Address, " +
//            "City, " +
//            "State, " +
//            "Zip, " +
//            "Country, " +
//            "Phone, " +
//            "Email, " +
//            "SSN, " +
//            "Specialty, " +
//            "OtherNotes, " +
//            "ContrRecd, " +
//            "DateDiscl, " +
//            "Disclosure, " +
//            "StateLic, " +
//            "Degrees, " +
//            "Position, " +
//            "Experience, " +
//              "FacilityID) " +
//            "VALUES (" +
//            "@Fullname, " +
//            "@LastFirst, " +
//            "@Address, " +
//            "@City, " +
//            "@State, " +
//            "@Zip, " +
//            "@Country, " +
//            "@Phone, " +
//            "@Email, " +
//            "@SSN, " +
//            "@Specialty, " +
//            "@OtherNotes, " +
//            "@ContrRecd, " +
//            "@DateDiscl, " +
//            "@Disclosure, " +
//            "@StateLic, " +
//            "@Degrees, " +
//            "@Position, " +
//            "@Experience, " +
//              "@FacilityID) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@Fullname", SqlDbType.VarChar).Value = ResourcePerson.Fullname ;
//            cmd.Parameters.Add("@LastFirst", SqlDbType.VarChar).Value = ResourcePerson.LastFirst ;
//            cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = ResourcePerson.Address;
//            cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = ResourcePerson.City;
//            cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = ResourcePerson.State;
//            cmd.Parameters.Add("@Zip", SqlDbType.VarChar).Value = ResourcePerson.Zip;
//            cmd.Parameters.Add("@Country", SqlDbType.VarChar).Value = ResourcePerson.Country;
//            cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ResourcePerson.Phone;
//            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ResourcePerson.Email;
//            cmd.Parameters.Add("@SSN", SqlDbType.VarChar).Value = ResourcePerson.SSN;
//            cmd.Parameters.Add("@Specialty", SqlDbType.VarChar).Value = ResourcePerson.Specialty;
//            cmd.Parameters.Add("@OtherNotes", SqlDbType.VarChar).Value = ResourcePerson.OtherNotes;
//            cmd.Parameters.Add("@ContrRecd", SqlDbType.Bit).Value = ResourcePerson.ContrRecd ;

//            if (ResourcePerson.DateDiscl == null
//                || ResourcePerson.DateDiscl.Trim().Length == 0)
//            {
//                cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = DBNull.Value;
//            }
//            else
//            {
//                // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
//                try
//                {
//                    DateTime dDateDiscl = Convert.ToDateTime(ResourcePerson.DateDiscl);
//                    cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = dDateDiscl;
//                }
//                catch
//                {
//                    // something went wrong with convert, so store NULL instead
//                    cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = DBNull.Value;
//                }
//            }

//            cmd.Parameters.Add("@Disclosure", SqlDbType.VarChar).Value = ResourcePerson.Disclosure;
//            cmd.Parameters.Add("@StateLic", SqlDbType.VarChar).Value = ResourcePerson.StateLic;
//            cmd.Parameters.Add("@Degrees", SqlDbType.VarChar).Value = ResourcePerson.Degrees;
//            cmd.Parameters.Add("@Position", SqlDbType.VarChar).Value = ResourcePerson.Position;
//            cmd.Parameters.Add("@Experience", SqlDbType.VarChar).Value = ResourcePerson.Experience;
//            cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = ResourcePerson.FacilityID;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a ResourcePerson
//      /// </summary>
//      public override bool UpdateResourcePerson(ResourcePersonInfo ResourcePerson)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update ResourcePerson set " +
//            "Fullname = @Fullname, " +
//            "LastFirst = @LastFirst, " +
//            "Address = @Address, " +
//            "City = @City, " +
//            "State = @State, " +
//            "Zip = @Zip, " +
//            "Country = @Country, " +
//            "Phone = @Phone, " +
//            "Email = @Email, " +
//            "SSN = @SSN, " +
//            "Specialty = @Specialty, " +
//            "OtherNotes = @OtherNotes, " +
//            "ContrRecd = @ContrRecd, " +
//            "DateDiscl = @DateDiscl, " +
//            "Disclosure = @Disclosure, " +
//            "StateLic = @StateLic, " +
//            "Degrees = @Degrees, " +
//            "Position = @Position, " +
//            "Experience = @Experience, " +
//            "FacilityID = @FacilityID " +
//            "where ResourceID = @ResourceID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@Fullname", SqlDbType.VarChar).Value = ResourcePerson.Fullname ;
//            cmd.Parameters.Add("@LastFirst", SqlDbType.VarChar).Value = ResourcePerson.LastFirst ;
//            cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = ResourcePerson.Address;
//            cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = ResourcePerson.City;
//            cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = ResourcePerson.State;
//            cmd.Parameters.Add("@Zip", SqlDbType.VarChar).Value = ResourcePerson.Zip;
//            cmd.Parameters.Add("@Country", SqlDbType.VarChar).Value = ResourcePerson.Country;
//            cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ResourcePerson.Phone;
//            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ResourcePerson.Email;
//            cmd.Parameters.Add("@SSN", SqlDbType.VarChar).Value = ResourcePerson.SSN;
//            cmd.Parameters.Add("@Specialty", SqlDbType.VarChar).Value = ResourcePerson.Specialty;
//            cmd.Parameters.Add("@OtherNotes", SqlDbType.VarChar).Value = ResourcePerson.OtherNotes;
//            cmd.Parameters.Add("@ContrRecd", SqlDbType.Bit).Value = ResourcePerson.ContrRecd;

//            if (ResourcePerson.DateDiscl == null
//                || ResourcePerson.DateDiscl.Trim().Length == 0)
//            {
//                cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = DBNull.Value;
//            }
//            else
//            {
//                // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
//                try
//                {
//                    DateTime dDateDiscl = Convert.ToDateTime(ResourcePerson.DateDiscl);
//                    cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = dDateDiscl;
//                }
//                catch
//                {
//                    // something went wrong with convert, so store NULL instead
//                    cmd.Parameters.Add("@DateDiscl", SqlDbType.DateTime).Value = DBNull.Value;
//                }
//            }
            
//            cmd.Parameters.Add("@Disclosure", SqlDbType.VarChar).Value = ResourcePerson.Disclosure;
//            cmd.Parameters.Add("@StateLic", SqlDbType.VarChar).Value = ResourcePerson.StateLic;
//            cmd.Parameters.Add("@Degrees", SqlDbType.VarChar).Value = ResourcePerson.Degrees;
//            cmd.Parameters.Add("@Position", SqlDbType.VarChar).Value = ResourcePerson.Position;
//            cmd.Parameters.Add("@Experience", SqlDbType.VarChar).Value = ResourcePerson.Experience;
//            cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = ResourcePerson.FacilityID;
//            cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = ResourcePerson.ResourceID;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with Roles

//      /// <summary>
//      /// Returns the total number of Roles
//      /// </summary>
//      public override int GetRoleCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Roles", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Roles
//      /// </summary>
//      public override List<RoleInfo> GetRoles(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Roles";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }
//              else
//              {
//                  // default to RoleID order if none specified
//                  cSQLCommand = cSQLCommand +
//                      " order by RoleID ";
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetRoleCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the Role with the specified ID
//      /// </summary>
//      public override RoleInfo GetRoleByID(int RoleID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from Roles where RoleID=@RoleID", cn);
//            cmd.Parameters.Add("@RoleID", SqlDbType.Int).Value = RoleID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetRoleFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a Role
//      /// </summary>
//      public override bool DeleteRole(int RoleID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from Roles where RoleID=@RoleID", cn);
//            cmd.Parameters.Add("@RoleID", SqlDbType.Int).Value = RoleID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new Role
//      /// </summary>
//      public override int InsertRole(RoleInfo Role)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into Roles " +
//            "(RoleName) " +
//            "VALUES (" +
//            "@RoleName) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@RoleName", SqlDbType.VarChar).Value = Role.RoleName ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a Role
//      /// </summary>
//      public override bool UpdateRole(RoleInfo Role)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Roles set " +
//            "RoleName = @RoleName " +
//            "where RoleID = @RoleID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@RoleName", SqlDbType.VarChar).Value = Role.RoleName ;
//            cmd.Parameters.Add("@RoleID", SqlDbType.Int).Value = Role.RoleID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with SalesReps

//      /// <summary>
//      /// Returns the total number of SalesReps
//      /// </summary>
//      public override int GetSalesRepCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from SalesRep", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all SalesReps
//      /// </summary>
//      public override List<SalesRepInfo> GetSalesReps(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from SalesRep";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetSalesRepCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the SalesRep with the specified ID
//      /// </summary>
//      public override SalesRepInfo GetSalesRepByID(int RepID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from SalesRep where RepID=@RepID", cn);
//            cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = RepID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetSalesRepFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a SalesRep
//      /// </summary>
//      public override bool DeleteSalesRep(int RepID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from SalesRep where RepID=@RepID", cn);
//            cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = RepID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new SalesRep
//      /// </summary>
//      public override int InsertSalesRep(SalesRepInfo SalesRep)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into SalesRep " +
//            "(RepCode, " +
//            "RepName, " +
//            "RepComment) " +
//            "VALUES (" +
//            "@RepCode, " +
//            "@RepName, " +
//            "@RepComment) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@RepCode", SqlDbType.VarChar).Value = SalesRep.RepCode ;
//            cmd.Parameters.Add("@RepName", SqlDbType.VarChar).Value = SalesRep.RepName ;
//            cmd.Parameters.Add("@RepComment", SqlDbType.VarChar).Value = SalesRep.RepComment ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a SalesRep
//      /// </summary>
//      public override bool UpdateSalesRep(SalesRepInfo SalesRep)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update SalesRep set " +
//            "RepCode = @RepCode, " +
//            "RepName = @RepName, " +
//            "RepComment = @RepComment " +
//            "where RepID = @RepID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@RepCode", SqlDbType.VarChar).Value = SalesRep.RepCode ;
//            cmd.Parameters.Add("@RepName", SqlDbType.VarChar).Value = SalesRep.RepName ;
//            cmd.Parameters.Add("@RepComment", SqlDbType.VarChar).Value = SalesRep.RepComment ;
//            cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = SalesRep.RepID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with Sidebars

//      /// <summary>
//      /// Returns the total number of Sidebars
//      /// </summary>
//      public override int GetSidebarCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Sidebars", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Sidebars
//      /// </summary>
//      public override List<SidebarInfo> GetSidebars(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Sidebars";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetSidebarCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the Sidebar with the specified ID
//      /// </summary>
//      public override SidebarInfo GetSidebarByID(int SB_ID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from Sidebars where SB_ID=@SB_ID", cn);
//              cmd.Parameters.Add("@SB_ID", SqlDbType.Int).Value = SB_ID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetSidebarFromReader(reader, true);
//              else
//                  return null;
//          }
//      }


//      /// <summary>
//      /// Retrieves all Sidebars associated with the specified TopicID
//      /// </summary>
//      public override List<SidebarInfo> GetSidebarsByTopicID(int TopicID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Sidebars where TopicID=@TopicID";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

//              cn.Open();
//              return GetSidebarCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Returns the total number of Sidebars associated with the specified TopicID
//      /// </summary>
//      public override int GetSidebarCountByTopicID(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Sidebars where TopicID=@TopicID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }


//      /// <summary>
//      /// Deletes a Sidebar
//      /// </summary>
//      public override bool DeleteSidebar(int SB_ID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from Sidebars where SB_ID=@SB_ID", cn);
//              cmd.Parameters.Add("@SB_ID", SqlDbType.Int).Value = SB_ID;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new Sidebar
//      /// </summary>
//      public override int InsertSidebar(SidebarInfo Sidebar)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into Sidebars " +
//            "(TopicID, " +
//            "Title, " +
//            "Body) " +
//            "VALUES (" +
//            "@TopicID, " +
//            "@Title, " +
//            "@Body) SET @ID = SCOPE_IDENTITY()", cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Sidebar.TopicID;
//              cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = Sidebar.Title;
//              cmd.Parameters.Add("@Body", SqlDbType.VarChar).Value = Sidebar.Body;

//              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              IDParameter.Direction = ParameterDirection.Output;
//              cmd.Parameters.Add(IDParameter);

//              cn.Open();
//              cmd.ExecuteNonQuery();

//              int NewID = (int)IDParameter.Value;
//              return NewID;

//          }
//      }

//      /// <summary>
//      /// Updates a Sidebar
//      /// </summary>
//      public override bool UpdateSidebar(SidebarInfo Sidebar)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Sidebars set " +
//            "TopicID = @TopicID, " +
//            "Title = @Title, " +
//            "Body = @Body " +
//            "where SB_ID = @SB_ID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Sidebar.TopicID;
//              cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = Sidebar.Title;
//              cmd.Parameters.Add("@Body", SqlDbType.VarChar).Value = Sidebar.Body;
//              cmd.Parameters.Add("@SB_ID", SqlDbType.Int).Value = Sidebar.SB_ID;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

       
//       /////////////////////////////////////////////////////////
//      // methods that work with Snippets

//      /// <summary>
//      /// Returns the total number of Snippets
//      /// </summary>
//      public override int GetSnippetCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Snippets", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Snippets
//      /// </summary>
//      public override List<SnippetInfo> GetSnippets(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Snippets";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length == 0)
//              {
//                  cSortExpression = "snippetkey";
//              }

//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetSnippetCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the Snippet with the specified ID
//      /// </summary>
//      public override SnippetInfo GetSnippetByID(int SnippetID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from Snippets where SnippetID=@SnippetID", cn);
//              cmd.Parameters.Add("@SnippetID", SqlDbType.Int).Value = SnippetID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetSnippetFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Retrieves the Snippet with the specified Message Key
//      /// </summary>
//      public override SnippetInfo GetSnippetBySnippetKey(string SnippetKey)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from Snippets where snippetkey=@SnippetKey", cn);
//              cmd.Parameters.Add("@SnippetKey", SqlDbType.VarChar).Value = SnippetKey;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetSnippetFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a Snippet
//      /// </summary>
//      public override bool DeleteSnippet(int SnippetID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from Snippets where SnippetID=@SnippetID", cn);
//              cmd.Parameters.Add("@SnippetID", SqlDbType.Int).Value = SnippetID;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new Snippet
//      /// </summary>
//      public override int InsertSnippet(SnippetInfo Snippet)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into Snippets " +
//            "(SnippetKey, " +
//            "SnippetText) " +
//            "VALUES (" +
//            "@SnippetKey, " +
//            "@SnippetText) SET @ID = SCOPE_IDENTITY()", cn);

//              cmd.Parameters.Add("@SnippetKey", SqlDbType.VarChar).Value = Snippet.SnippetKey;
//              cmd.Parameters.Add("@SnippetText", SqlDbType.Text).Value = Snippet.SnippetText;

//              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              IDParameter.Direction = ParameterDirection.Output;
//              cmd.Parameters.Add(IDParameter);

//              cn.Open();
//              cmd.ExecuteNonQuery();

//              int NewID = (int)IDParameter.Value;
//              return NewID;

//          }
//      }

//      /// <summary>
//      /// Updates a Snippet
//      /// </summary>
//      public override bool UpdateSnippet(SnippetInfo Snippet)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Snippets set " +
//            "SnippetKey = @SnippetKey, " +
//             "SnippetText = @SnippetText " +
//            "where SnippetID = @SnippetID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@SnippetKey", SqlDbType.VarChar).Value = Snippet.SnippetKey;
//              cmd.Parameters.Add("@SnippetText", SqlDbType.Text).Value = Snippet.SnippetText;
//              cmd.Parameters.Add("@SnippetID", SqlDbType.Int).Value = Snippet.SnippetID;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /////////////////////////////////////////////////////////
//      // methods that work with Specialties

//      /// <summary>
//      /// Returns the total number of Specialties
//      /// </summary>
//      public override int GetSpecialtyCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Specialty", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Specialties
//      /// </summary>
//      public override List<SpecialtyInfo> GetSpecialties(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Specialty";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }
//              else
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by SpecTitle";
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetSpecialtyCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the Specialty with the specified ID
//      /// </summary>
//      public override SpecialtyInfo GetSpecialtyByID(int SpecID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from Specialty where SpecID=@SpecID", cn);
//            cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = SpecID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetSpecialtyFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a Specialty
//      /// </summary>
//      public override bool DeleteSpecialty(int SpecID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from Specialty where SpecID=@SpecID", cn);
//            cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = SpecID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new Specialty
//      /// </summary>
//      public override int InsertSpecialty(SpecialtyInfo Specialty)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into Specialty " +
//            "(SpecTitle) " +
//            "VALUES (" +
//            "@SpecTitle) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@SpecTitle", SqlDbType.VarChar).Value = Specialty.SpecTitle ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a Specialty
//      /// </summary>
//      public override bool UpdateSpecialty(SpecialtyInfo Specialty)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Specialty set " +
//            "SpecTitle = @SpecTitle " +
//            "where SpecID = @SpecID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@SpecTitle", SqlDbType.VarChar).Value = Specialty.SpecTitle ;
//            cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = Specialty.SpecID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with SurveyDefinitions

//      /// <summary>
//      /// Returns the total number of SurveyDefinitions
//      /// </summary>
//      public override int GetSurveyDefinitionCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from SurveyDefinition where FacilityID=0 ", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all SurveyDefinitions
//      /// </summary>
//      public override List<SurveyDefinitionInfo> GetSurveyDefinitions(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from SurveyDefinition where FacilityID=0 ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetSurveyDefinitionCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the SurveyDefinition with the specified ID
//      /// </summary>
//      public override SurveyDefinitionInfo GetSurveyDefinitionByID(int SurveyID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from SurveyDefinition where SurveyID=@SurveyID", cn);
//            cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = SurveyID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetSurveyDefinitionFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Bhaskar
//      /// Retrieves the SurveyDefinition with the specified ID
//      /// </summary>
//      public override SurveyDefinitionInfo GetSurveyDefinitionByTopicID(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from SurveyDefinition where SurveyID=@TopicID", cn);
//              //"from SurveyDefinition where TopicId=@TopicID", cn); //add new column(TopicId)..in SurveyDefinition (Bsk)
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetSurveyDefinitionFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a SurveyDefinition
//      /// </summary>
//      public override bool DeleteSurveyDefinition(int SurveyID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from SurveyDefinition where SurveyID=@SurveyID", cn);
//            cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = SurveyID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new SurveyDefinition
//      /// </summary>
//      public override int InsertSurveyDefinition(SurveyDefinitionInfo SurveyDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into SurveyDefinition " +
//            "(SurveyName, " +
//            "XMLSurvey, " +
//            "Version, " +
//              "FacilityID ) " +
//            "VALUES (" +
//            "@SurveyName, " +
//            "@XMLSurvey, " +
//            "@Version, " +
//              "@FacilityID) SET @ID = SCOPE_IDENTITY()", cn);

//              cmd.Parameters.Add("@SurveyName", SqlDbType.VarChar).Value = SurveyDefinition.SurveyName;
//              cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = SurveyDefinition.XMLSurvey;
//            cmd.Parameters.Add("@Version", SqlDbType.Int).Value = SurveyDefinition.Version ;
//            cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = SurveyDefinition.FacilityID;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a SurveyDefinition
//      /// </summary>
//      public override bool UpdateSurveyDefinition(SurveyDefinitionInfo SurveyDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update SurveyDefinition set " +
//            "SurveyName = @SurveyName, " +
//            "XMLSurvey = @XMLSurvey, " +
//            "Version = @Version, " +
//            "FacilityID = @FacilityID " +
//            "where SurveyID = @SurveyID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@SurveyName", SqlDbType.VarChar).Value = SurveyDefinition.SurveyName;
//              cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = SurveyDefinition.XMLSurvey;
//            cmd.Parameters.Add("@Version", SqlDbType.Int).Value = SurveyDefinition.Version ;
//            cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = SurveyDefinition.FacilityID;
//            cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = SurveyDefinition.SurveyID;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Updates a SurveyDefinition
//      /// </summary>
//      public override bool UpdateSurveyDefinition(int SurveyID, string XMLSurvey, int Version, int FacilityID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update SurveyDefinition set " +
//            "XMLSurvey = @XMLSurvey, " +
//            "Version = @Version, " +
//            "FacilityID = @FacilityID " +
//            "where SurveyID = @SurveyID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;              
//              cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = XMLSurvey;
//              cmd.Parameters.Add("@Version", SqlDbType.Int).Value = Version;
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
//              cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = SurveyID;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with Tests

//      /// <summary>
//      /// Returns the total number of Tests
//      /// </summary>
//      public override int GetTestCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Test", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Tests
//      /// </summary>
//      public override List<TestInfo> GetTests(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from Test";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTestCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the Test with the specified ID
//      /// </summary>
//      public override TestInfo GetTestByID(int TestID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from Test where TestID=@TestID", cn);
//            cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = TestID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetTestFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Retrieves all Tests(Bhaskar N)
//      /// </summary>
//      public override List<TestInfo> GetAllTestsByTopicId(int TopicId, string FromDate, string Todate, string SortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * from Test " +
//                  "where status='c' and TopicId= @TopicId";
                  
//              if (FromDate.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " and lastmod >= @FromDate";
//              }
//              if (Todate.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " and lastmod <= @Todate";
//              }
              
//              if (SortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + SortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
//              cmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = FromDate;
//              cmd.Parameters.Add("@Todate", SqlDbType.VarChar).Value = Todate;
//              //cmd.Parameters.Add("@SortExpression", SqlDbType.VarChar).Value = SortExpression;

//              cn.Open();
//              return GetTestCollectionFromReader(ExecuteReader(cmd), false);


              
//          }
//      }


//      /// <summary>
//      /// Deletes a Test
//      /// </summary>
//      public override bool DeleteTest(int TestID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from Test where TestID=@TestID", cn);
//            cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = TestID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//       ///  <summary>
//       ///  Inserts a new gift card
//       ///  </summary>
//       ///  

//      public override int InsertNewGiftcard(NewGiftcardInfo NewGiftcard)
//      {

//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into NewGiftcard " +
//           "(gcnumber, " +
//           "giftcard_chour, " +
//           "facilityid, " +
//           "facname) " +
//           "VALUES (" +
//           "@GcNumber, " +
//           "@GiftCardCHour, " +
//           "@FacilityID, " +
//           "@FacilityName) SET @ID = SCOPE_IDENTITY()", cn);




//              cmd.Parameters.Add("@GcNumber", SqlDbType.Int).Value = NewGiftcard.GcNumber;
//              cmd.Parameters.Add("@GiftCardCHour", SqlDbType.VarChar).Value = NewGiftcard.GiftCardCHour;
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = NewGiftcard.FacilityID;
//              cmd.Parameters.Add("@FacilityName", SqlDbType.VarChar).Value = NewGiftcard.FacilityName;

//              cn.Open();

//              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              IDParameter.Direction = ParameterDirection.Output;
//              cmd.Parameters.Add(IDParameter);

//              cmd.ExecuteNonQuery();

//              int NewID = (int)IDParameter.Value;
//              return NewID;

//          }
//      }


//      /// <summary>
//      /// Retrieves all  Giftcard Info for the specified gift card id
//      /// </summary>
//      public override List<NewGiftcardInfo> GetNewGiftcardByGcNumber(string GcNumber)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "gcid, " +
//                  "gcnumber, " +
//                  "giftcard_chour, " +
//                  "facilityid, " +
//                  "facname " +
//                  "from NewGiftcard " +
//                  "where gcnumber = @GcNumber " +
//                  "order by gcnumber DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@GcNumber", SqlDbType.VarChar).Value = GcNumber;
//              cn.Open();
//              return GetNewGiftcardCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Retrieves all  facilityarealink Info for the specified facility id
//      /// </summary>
//      public override List<FacilityAreaLinkInfo> GetFacilityAreaLinkByFacilityId(int facilityid)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "facilityid, " +
//                  "areaid " +
//                  "from FacilityAreaLink " +
//                  "where facilityid = @FacilityID " +
//                  "order by areaid DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@FacilityID", SqlDbType.VarChar).Value = facilityid;
//              cn.Open();
//              return GetFacilityAreaLinkCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      public override int UpdateFacilityAreaLink(FacilityAreaLinkInfo FacilityAreaLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into FacilityAreaLink " +
//                  "(facilityid, " +
//                  "areaid )" +
//                  "VALUES (" +
//                  "@FacilityId, " +
//                  "@AreaId)", cn);
//              cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = FacilityAreaLink.facilityid;
//              cmd.Parameters.Add("@AreaId", SqlDbType.Int).Value = FacilityAreaLink.areaid;

//              //SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              //IDParameter.Direction = ParameterDirection.Output;
//              //cmd.Parameters.Add(IDParameter);

//              foreach (IDataParameter param in cmd.Parameters)
//              {
//                  if (param.Value == null)
//                      param.Value = DBNull.Value;
//              }

//              cn.Open();
//             int ret= cmd.ExecuteNonQuery();
//              //int NewID = (int)IDParameter.Value;
//              return ret;


//          }
          
//          throw new NotImplementedException();
//      }


//      public override bool DeleteFacilityAreaLink(int facilityid, int areaid)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from facilityarealink where facilityid=@FacilityId And areaid=@AreaId", cn);
//              cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = facilityid;
//              cmd.Parameters.Add("@AreaId", SqlDbType.Int).Value = areaid;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }




//      /// <summary>
//      /// Inserts a new Cart
//      /// </summary>
//      public override int InsertCart(CartInfo Cart)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              // no matching id, so we are good to go
//              SqlCommand cmd = new SqlCommand("insert into cart " +
//              "(TopicID, " +
//              "UserID, " +
//              "Media_Type, " +
//              "Score, " +
//              "LastMod, " +
//              "XMLTest, " +
//              "XMLSurvey, " +
//              "Quantity, " +
//              "DiscountId, " +
//              "Process_Ind, " +
//              "Optin_Ind) " +
//              "VALUES (" +
//              "@TopicID, " +
//              "@UserID, " +
//              "@Media_Type, " +
//              "@Score, " +
//              "@LastMod, " +
//              "@XMLTest, " +
//              "@XMLSurvey, " +
//              "@Quantity, " +
//              "@DiscountId, " +
//              "@Process_Ind, " +
//              "@OptionId) SET @ID = SCOPE_IDENTITY()", cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Cart.TopicID;
//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Cart.UserID;
//              cmd.Parameters.Add("@Media_Type", SqlDbType.VarChar).Value = Cart.MediaType;
//              cmd.Parameters.Add("@Score", SqlDbType.Int).Value = Cart.Score;
//              cmd.Parameters.Add("@XMLTest", SqlDbType.VarChar).Value = Cart.XmlTest;
//              cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = Cart.XmlSurvey;
//              cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = Cart.Quantity;
//              cmd.Parameters.Add("@DiscountId", SqlDbType.Int).Value = Cart.DiscountId;
//              cmd.Parameters.Add("@OptionId", SqlDbType.Bit).Value = Cart.Option_Id;
//              cmd.Parameters.Add("@Process_Ind", SqlDbType.Bit).Value = Cart.Process_Ind;
                          
//              // pass null to database if value is zero (nothing selected by user)
//              // this allows the database to have a NULL foreign key and still enforce relational integrity
//              if (Cart.Lastmod == System.DateTime.MinValue)
//              {
//                  cmd.Parameters.Add("@LastMod", SqlDbType.DateTime).Value = System.DBNull.Value;
//              }
//              else
//              {
//                  cmd.Parameters.Add("@LastMod", SqlDbType.DateTime).Value = Cart.Lastmod;
//              }

//              cn.Open();

//              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              IDParameter.Direction = ParameterDirection.Output;
//              cmd.Parameters.Add(IDParameter);
//              cmd.ExecuteNonQuery();

//              int NewID = (int)IDParameter.Value;
//              return NewID;
//          }
//      }


//      /// <summary>
//      /// Updates a cart
//      /// </summary>
//      public override bool UpdateCart(CartInfo Cart)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Cart set " +
//            "TopicID = @TopicID, " +
//            "UserID = @UserID, " +
//            "LastMod = @LastMod, " +
//            "XMLTest = @XMLTest, " +
//            "XMLSurvey = @XMLSurvey, " +
//            "Media_Type = @Media_Type, "+
//            "Score=@Score,"+
//            "Quantity=@Quantity,"+
//            "DiscountId=@DiscountId,"+
//            "Process_Ind=@Process_Ind, " +
//            "Optin_Ind=@OptionId" +           
//            " where CartID = @CartID ", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Cart.TopicID;
//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Cart.UserID;
//              cmd.Parameters.Add("@Media_Type", SqlDbType.VarChar).Value = Cart.MediaType;
//              cmd.Parameters.Add("@Score", SqlDbType.Int).Value = Cart.Score;
//              cmd.Parameters.Add("@LastMod", SqlDbType.DateTime).Value = Cart.Lastmod;
//              cmd.Parameters.Add("@XMLTest", SqlDbType.VarChar).Value = Cart.XmlTest;
//              cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = Cart.XmlSurvey;
//              cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = Cart.Quantity;
//              cmd.Parameters.Add("@DiscountId", SqlDbType.Int).Value = Cart.DiscountId;
//              cmd.Parameters.Add("@OptionId", SqlDbType.Bit).Value = Cart.Option_Id;
//              cmd.Parameters.Add("@Process_Ind", SqlDbType.Bit).Value = Cart.Process_Ind;
//              cmd.Parameters.Add("@CartID", SqlDbType.Int).Value = Cart.CartID;
//               cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /// <summary>
//      /// Retrieves all Carts for the specified UserID
//      /// </summary>
//      public override List<CartInfo> GetCartsByUserID(int UserID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "cartid, " +
//                  "topicid, " +
//                  "userid, " +
//                  "media_type, " +
//                  "score, " +
//                  "lastmod, " +
//                  "xmltest, " +
//                  "xmlsurvey, " +
//                  "quantity, " +
//                  "discountid, " +
//                  "process_ind, " +
//                  "optin_ind " +
//                  "from cart " +
//                  "where UserID = @UserID and process_ind = '0'" +
//                  "order by LastMod DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//              cn.Open();
//              return GetCartCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves Coupon Amount for the specified UserID
//      /// </summary>
//      public override decimal GetCouponAmountByUserIdandCode(int UserId, string CouponCode)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select dbo.udf_couponamount( "+ UserId + " , '" + CouponCode + " ')" ;
//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
//              cmd.Parameters.Add("@CouponCode", SqlDbType.VarChar).Value = CouponCode;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd);
//              if (reader.Read())
//                  return (decimal)reader[0];
//              else
//                  return -1;

//          }
//      }

//      /// <summary>
//      /// Retrieves all Incomplete Carts for the specified UserID
//      /// </summary>
//      public override List<CartInfo> GetIncompleteCartsByUserIDAndTopicID(int UserID, int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "cartid, " +
//                  "topicid, " +
//                  "userid, " +
//                  "xmltest, " +
//                  "media_type, " +
//                  "score, " +
//                  "lastmod, " +
//                  "xmlsurvey, " +
//                  "quantity, " +
//                  "discountid, " +
//                  "process_ind, " +
//                  "optin_ind " +
//                  "from cart " +
//                  "where UserID = @UserID " +
//                  "and TopicID = @TopicID " +
//                  "and process_ind='0' and media_type='B' " +
//                  "order by LastMod DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return GetCartCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Retrieves all Incomplete Carts for the specified UserID
//      /// </summary>
//      public override List<CartInfo> GetcompleteCartsByUserIDAndTopicID(int UserID, int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "cartid, " +
//                  "topicid, " +
//                  "userid, " +
//                  "xmltest, " +
//                  "media_type, " +
//                  "score, " +
//                  "lastmod, " +
//                  "xmlsurvey, " +
//                  "quantity, " +
//                  "discountid, " +
//                  "process_ind, " +
//                  "optin_ind " +
//                  "from cart " +
//                  "where UserID = @UserID " +
//                  "and TopicID = @TopicID " +
//                  "and process_ind='1' " +
//                  "order by LastMod DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return GetCartCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the Cart with the specified ID
//      /// </summary>
//      public override CartInfo GetCartByID(int CartID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from Cart where CartID=@CartID", cn);
//              cmd.Parameters.Add("@CartID", SqlDbType.Int).Value = CartID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetCartFromReader(reader, true);
//              else
//                  return null;
//          }
//      }


//       ///<summary>
//       ///
//       ///</summary>
//       ///
//      public override int CheckAllOnline(int UserId)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select case when ((select count(cartid) from cart where userid=@UserId and process_ind='0' )= (select count(cartid) from cart " +
//                  " where userid=@UserId and process_ind='0' and media_type='O')) then 1 else 0 end as ind",cn);
//              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd);
//              if (reader.Read())
//                  return (int)reader["ind"];
//              else
//                  return -1;

//          }
//      }


//      /// <summary>
//      /// Deletes a Cart
//      /// </summary>
//      public override bool DeleteCart(int CartID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from Cart where CartID=@CartID", cn);
//              cmd.Parameters.Add("@CartID", SqlDbType.Int).Value = CartID;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//        /// <summary>
//      /// Retrieves the Cart with price with the specified userID
//      /// </summary>
//      public override System.Data.DataSet GetCartPriceByUserId(int UserId)
//      {
//          SqlDataAdapter adapter;
//          System.Data.DataSet dsCart= new DataSet();
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand();
//              cmd.Connection = cn;
//              cn.Open();
//              cmd.CommandText = "sp_r_shop_cart";
//              cmd.CommandType=CommandType.StoredProcedure;
//              cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = UserId;
//              adapter = new SqlDataAdapter(cmd);
//              adapter.Fill(dsCart);
//          }
//          return dsCart;
//      }


//      public override System.Data.DataSet GetUnlimCartPriceByUserId(int UserId)
//      {
//          SqlDataAdapter adapter;
//          System.Data.DataSet dsCart = new DataSet();
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand();
//              cmd.Connection = cn;
//              cn.Open();
//              cmd.CommandText = "sp_r_ul_shop_cart";
//              cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = UserId;
//              adapter = new SqlDataAdapter(cmd);
//              adapter.Fill(dsCart);
//          }
//          return dsCart;
//      }

//      /// <summary>
//      /// Retrieves the Cart with price with the specified cart Id
//      /// </summary>
//      public override Decimal GetCartPriceByCartId(int CartId)
//      {
//             using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select dbo.udf_cartprice(@CartId) ", cn);
//              cmd.Parameters.Add("@CartID", SqlDbType.Int).Value = CartId;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd);
//              if (reader.Read())
//                  return (decimal)reader[0];
//              else
//                  return (decimal)-1;
//          }
//       }


//      public override int CheckNumberOfPharmacy(int UserId)
//      {
//          SqlDataAdapter adapter;
//          System.Data.DataSet dsCart = new DataSet();
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand();
//              cmd.Connection = cn;
//              cn.Open();
//              cmd.CommandText = "SP_Check_Pharmacist";
//              cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@userid", SqlDbType.Int).Value = UserId;
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return (int)reader[0];
//              else
//                  return 0;
//          }

//      }

//      /// <summary>
//      /// Retrieves the Cart with price with the specified userID
//      /// </summary>
//      public override System.Data.DataSet GetOrderPriceByUserId(int UserId)
//      {
//          SqlDataAdapter adapter;
//          System.Data.DataSet dsCart = new DataSet();
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand();
//              cmd.Connection = cn;
//              cn.Open();
//              cmd.CommandText = "sp_r_order_summary";
//              cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = UserId;
//              adapter = new SqlDataAdapter(cmd);
//              adapter.Fill(dsCart);
//          }
//          return dsCart;
//      }


//      /// <summary>
//      /// Retrieves the Cart with price with the specified userID
//      /// </summary>
//      public override System.Data.DataSet GetUnlimOrderPriceByUserId(int UserId)
//      {
//          SqlDataAdapter adapter;
//          System.Data.DataSet dsCart = new DataSet();
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand();
//              cmd.Connection = cn;
//              cn.Open();
//              cmd.CommandText = "sp_r_ul_order_summary";
//              cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = UserId;
//              adapter = new SqlDataAdapter(cmd);
//              adapter.Fill(dsCart);
//          }
//          return dsCart;
//      }

       
       
//      /// <summary>
//      /// Inserts a new Test
//      /// </summary>
//      public override int InsertTest(TestInfo Test)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
////              SqlCommand cmd = new SqlCommand("select RIGHT(SYS(2015),9) from Test WHERE '0' ",cn);
////              string newID = (string)ExecuteScalar(cmd);
//              //Test.TestID = newID.ToString();

////              SqlCommand cmd2 = new SqlCommand("SELECT Count(*) FROM Test WHERE TestID = '" + newID + "'", cn);
////              int IDCount = (int)ExecuteScalar(cmd2);
////
////			  if (IDCount > 0)
////			  {
////                  // matching id, so return false
////                  return false;                  
////              }    
            
//              // no matching id, so we are good to go
//            SqlCommand cmd = new SqlCommand("insert into Test " +
//            "(OldTestID, " +
//            "TopicID, " +
//            "UserID, " +
//            "BuyDate, " +
//            "Credits, " +
//            "LastMod, " +
//            "Name, " +
//            "NoSurvey, " +
//            "Status, " +
//            "UserName, " +
//            "XMLTest, " +
//            "XMLSurvey, " +
//            "TestVers, " +
//            "LectEvtID, " +
//            "SurveyComplete, " +
//            "VignetteComplete, " +
//            //"XMLVignette, " +
//            //"VignetteVersion, " +
//            //"VignetteScore, " +
//            "Score, " +
//            //"Printed_Date, " +
//            //"Emailed_Date, " +
//            //"Reported_Date, " +
//            "View_Date, " +
//            "UniqueID, " +
//            "AlterID, " +
//            "GCCharged) " +
//            "VALUES (" +
//            "@OldTestID, " +
//            "@TopicID, " +
//            "@UserID, " +
//            "@BuyDate, " +
//            "@Credits, " +
//            "@LastMod, " +
//            "@Name, " +
//            "@NoSurvey, " +
//            "@Status, " +
//            "@UserName, " +
//            "@XMLTest, " +
//            "@XMLSurvey, " +
//            "@TestVers, " +
//            "@LectEvtID, " +
//            "@SurveyComplete, " +
//            "@VignetteComplete, " +
//            //"@XMLVignette, " +
//            //"@VignetteVersion, " +
//            //"@VignetteScore, " +
//            "@Score, " +
//            //"@Printed_Date, " +
//            //"@Emailed_Date, " +
//            //"@Reported_Date, " +
//            "@View_Date, " +
//            "@UniqueID, " +
//            "@AlterID, " +
//            "@GCCharged) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@OldTestID", SqlDbType.Char).Value = Test.OldTestID ;
//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Test.TopicID;
//            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Test.UserID;
//            cmd.Parameters.Add("@BuyDate", SqlDbType.DateTime).Value = Test.BuyDate;
//            cmd.Parameters.Add("@Credits", SqlDbType.Decimal).Value = Test.Credits ;
//            cmd.Parameters.Add("@LastMod", SqlDbType.DateTime).Value = Test.LastMod ;
//            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = Test.Name ;
//            cmd.Parameters.Add("@NoSurvey", SqlDbType.Bit).Value = Test.NoSurvey ;
//            cmd.Parameters.Add("@Status", SqlDbType.Char).Value = Test.Status ;
//            cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Test.UserName ;
//            cmd.Parameters.Add("@XMLTest", SqlDbType.Text).Value = Test.XMLTest ;
//            cmd.Parameters.Add("@XMLSurvey", SqlDbType.Text).Value = Test.XMLSurvey ;
//            cmd.Parameters.Add("@TestVers", SqlDbType.Int).Value = Test.TestVers ;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (Test.LectEvtID == 0)
//            {
//                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = Test.LectEvtID;
//            }

//            cmd.Parameters.Add("@SurveyComplete", SqlDbType.Bit).Value = Test.SurveyComplete;
//            cmd.Parameters.Add("@VignetteComplete", SqlDbType.Bit).Value = Test.VignetteComplete;
//            //cmd.Parameters.Add("@XMLVignette", SqlDbType.Text).Value = Test.XMLVignette;
//            //cmd.Parameters.Add("@VignetteVersion", SqlDbType.Int).Value = Test.VignetteVersion;
//            //cmd.Parameters.Add("@VignetteScore", SqlDbType.Int).Value = Test.VignetteScore;
//            cmd.Parameters.Add("@Score", SqlDbType.Int).Value = Test.Score;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (Test.Printed_Date == System.DateTime.MinValue)
//            //{
//            //    cmd.Parameters.Add("@Printed_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            //    cmd.Parameters.Add("@Printed_Date", SqlDbType.DateTime).Value = Test.Printed_Date;
//            //}

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (Test.Emailed_Date == System.DateTime.MinValue)
//            //{
//            //    cmd.Parameters.Add("@Emailed_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            //    cmd.Parameters.Add("@Emailed_Date", SqlDbType.DateTime).Value = Test.Emailed_Date;
//            //}

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (Test.Reported_Date == System.DateTime.MinValue)
//            //{
//            //    cmd.Parameters.Add("@Reported_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            //    cmd.Parameters.Add("@Reported_Date", SqlDbType.DateTime).Value = Test.Reported_Date;
//            //}

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (Test.View_Date == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@View_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@View_Date", SqlDbType.DateTime).Value = Test.View_Date;
//            }

//            cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = Test.UniqueID;
//            cmd.Parameters.Add("@AlterID", SqlDbType.Int).Value = Test.AlterID;
//            cmd.Parameters.Add("@GCCharged", SqlDbType.Bit).Value = Test.GCCharged;

//              cn.Open();

//           SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//           IDParameter.Direction = ParameterDirection.Output;
//           cmd.Parameters.Add(IDParameter);

//           cmd.ExecuteNonQuery();

//           int NewID = (int)IDParameter.Value;
//           return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a Test
//      /// </summary>
//      public override bool UpdateTest(TestInfo Test)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Test set " +
//            "OldTestID = @OldTestID, " +
//            "TopicID = @TopicID, " +
//            "UserID = @UserID, " +
//            "BuyDate = @BuyDate, " +
//            "Credits = @Credits, " +
//            "LastMod = @LastMod, " +
//            "Name = @Name, " +
//            "NoSurvey = @NoSurvey, " +
//            "Status = @Status, " +
//            "UserName = @UserName, " +
//            "XMLTest = @XMLTest, " +
//            "XMLSurvey = @XMLSurvey, " +
//            "TestVers = @TestVers, " +
//            "LectEvtID = @LectEvtID, " +
//            "SurveyComplete = @SurveyComplete, " +
//            "VignetteComplete = @VignetteComplete, " +
//            //"XMLVignette = @XMLVignette, " +
//            //"VignetteVersion = @VignetteVersion, " +
//            //"VignetteScore = @VignetteScore, " +
//            "Score = @Score, " +
//            //"Printed_Date = @Printed_Date, " +
//            //"Emailed_Date = @Emailed_Date, " +
//            //"Reported_Date = @Reported_Date, " +
//            "View_Date = @View_Date, " +
//            "UniqueID = @UniqueID, " +
//            "AlterID = @AlterID, " +
//            "GCCharged = @GCCharged " +
//            "where TestID = @TestID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@OldTestID", SqlDbType.Char).Value = Test.OldTestID ;
//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Test.TopicID;
//            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Test.UserID;
//            cmd.Parameters.Add("@BuyDate", SqlDbType.DateTime).Value = Test.BuyDate;
//            cmd.Parameters.Add("@Credits", SqlDbType.Decimal).Value = Test.Credits ;
//            cmd.Parameters.Add("@LastMod", SqlDbType.DateTime).Value = Test.LastMod ;
//            cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = Test.Name ;
//            cmd.Parameters.Add("@NoSurvey", SqlDbType.Bit).Value = Test.NoSurvey ;
//            cmd.Parameters.Add("@Status", SqlDbType.Char).Value = Test.Status ;
//            cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Test.UserName ;
//            cmd.Parameters.Add("@XMLTest", SqlDbType.VarChar).Value = Test.XMLTest ;
//            cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = Test.XMLSurvey ;
//            cmd.Parameters.Add("@TestVers", SqlDbType.Int).Value = Test.TestVers ;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (Test.LectEvtID == 0)
//            {
//                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = Test.LectEvtID;
//            }

//            cmd.Parameters.Add("@SurveyComplete", SqlDbType.Bit).Value = Test.SurveyComplete;
//            cmd.Parameters.Add("@VignetteComplete", SqlDbType.Bit).Value = Test.VignetteComplete;
//            //cmd.Parameters.Add("@XMLVignette", SqlDbType.Text).Value = Test.XMLVignette;
//            //cmd.Parameters.Add("@VignetteVersion", SqlDbType.Int).Value = Test.VignetteVersion;
//            //cmd.Parameters.Add("@VignetteScore", SqlDbType.Int).Value = Test.VignetteScore;
//            cmd.Parameters.Add("@Score", SqlDbType.Int).Value = Test.Score;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (Test.Printed_Date == System.DateTime.MinValue)
//            //{
//            //    cmd.Parameters.Add("@Printed_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            //    cmd.Parameters.Add("@Printed_Date", SqlDbType.DateTime).Value = Test.Printed_Date;
//            //}

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (Test.Emailed_Date == System.DateTime.MinValue)
//            //{
//            //    cmd.Parameters.Add("@Emailed_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            //    cmd.Parameters.Add("@Emailed_Date", SqlDbType.DateTime).Value = Test.Emailed_Date;
//            //}

//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (Test.Reported_Date == System.DateTime.MinValue)
//            //{
//            //    cmd.Parameters.Add("@Reported_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            //    cmd.Parameters.Add("@Reported_Date", SqlDbType.DateTime).Value = Test.Reported_Date;
//            //}

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (Test.View_Date == System.DateTime.MinValue)
//            {
//                cmd.Parameters.Add("@View_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@View_Date", SqlDbType.DateTime).Value = Test.View_Date;
//            }

//            cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = Test.UniqueID;
//            cmd.Parameters.Add("@AlterID", SqlDbType.Int).Value = Test.AlterID;
//            cmd.Parameters.Add("@GCCharged", SqlDbType.Bit).Value = Test.GCCharged;
//            cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = Test.TestID;

//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Updates all Test's TopicName field for a certain TopicID
//      /// </summary>
//      public override bool UpdateTestTopicNameByTopicID(int TopicID, string TopicName)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Test set " +
//            "Name = @Name " +
//            "where TopicID = @TopicID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = TopicName;
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of test records for the specified TopicID
//       /// </summary>
//       public override int GetTestCountByTopicID(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Test " +
//                  "where TopicID = @TopicID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }
//       public override int GetFacilityIDByTestID(int TestID)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand(" SELECT   Topic.facilityid FROM Test INNER JOIN   Topic ON Test.topicid = Topic.topicid" +
//                   " where Test.testid  = @TestID", cn);
//               cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = TestID;
//               cn.Open();
//               return (int)ExecuteScalar(cmd);
//           }
//       }

//       /// <summary>
//       /// Returns the total number of test records for the specified UserName
//       /// </summary>
//       public override int GetTestCountByUserName(string UserName)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("select Count(*) from Test " +
//                   "where Test.username = @UserName", cn);
//               cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName;
//               cn.Open();
//               return (int)ExecuteScalar(cmd);
//           }
//       }

//       /// <summary>
//       /// Returns the total number of test records for the specified UserID
//       /// </summary>
//       public override int GetTestCountByUserID(int UserID)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("select Count(*) from Test " +
//                   "where UserID = @UserID", cn);
//               cmd.Parameters.Add("@UserID", SqlDbType.VarChar).Value = UserID;
//               cn.Open();
//               return (int)ExecuteScalar(cmd);
//           }
//       }

//      /// <summary>
//      /// Retrieves all Tests for the specified UserName
//      /// </summary>
//      public override List<TestInfo> GetTestsByUserName(string UserName)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TestID, " +
//                  "OldTestID, " +
//                  "TopicID, " +
//                  "UserID, " +
//                  "UserName, " +
//                  "Name, " +
//                  "Status, " +
//                  "BuyDate, " +
//                  "LastMod, " +
//                  "Credits, " +
//                  "NoSurvey, " +
//                  "XMLTest, " +
//                  "XMLSurvey, " +
//                  "TestVers, " +
//                  "LectEvtID, " +
//                  "SurveyComplete, " +
//                "VignetteComplete, " +
//                //"XMLVignette, " +
//                //"VignetteVersion, " +
//                //"VignetteScore, " +
//                "Score, " +
//                //"Printed_Date, " +
//                //"Emailed_Date, " +
//                //"Reported_Date, " +
//                "View_Date, " +
//                "UniqueID, " +
//                "AlterID, " +
//                "GCCharged " +
//                  "from Test " +
//                  "where UserName = @UserName " +
//                  "order by LastMod DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName;
//              cn.Open();
//              return GetTestCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Incomplete Tests for the specified UserName
//      /// </summary>
//      public override List<TestInfo> GetIncompleteTestsByUserName(string UserName)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TestID, " +
//                  "OldTestID, " +
//                  "TopicID, " +
//                  "UserID, " +
//                  "UserName, " +
//                  "Name, " +
//                  "Status, " +
//                  "BuyDate, " +
//                  "LastMod, " +
//                  "Credits, " +
//                  "NoSurvey, " +
//                  "XMLTest, " +
//                  "XMLSurvey, " +
//                  "TestVers, " +
//                  "LectEvtID, " +
//                  "SurveyComplete, " +
//                "VignetteComplete, " +
//                //"XMLVignette, " +
//                //"VignetteVersion, " +
//                //"VignetteScore, " +
//                "Score, " +
//                //"Printed_Date, " +
//                //"Emailed_Date, " +
//                //"Reported_Date, " +
//                "View_Date, " +
//                "UniqueID, " +
//                "AlterID, " +
//                "GCCharged " +
//                  "from Test " +
//                  "where UserName = @UserName " +
//                  "and Status = 'I' " +
//                  "order by LastMod DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName;
//              cn.Open();
//              return GetTestCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Completed Tests for the specified UserName
//      /// </summary>
//      public override List<TestInfo> GetCompletedTestsByUserName(string UserName)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TestID, " +
//                  "OldTestID, " +
//                  "TopicID, " +
//                  "UserID, " +
//                  "UserName, " +
//                  "Name, " +
//                  "Status, " +
//                  "BuyDate, " +
//                  "LastMod, " +
//                  "Credits, " +
//                  "NoSurvey, " +
//                  "XMLTest, " +
//                  "XMLSurvey, " +
//                  "TestVers, " +
//                  "LectEvtID, " +
//                  "SurveyComplete, " +
//                "VignetteComplete, " +
//                //"XMLVignette, " +
//                //"VignetteVersion, " +
//                //"VignetteScore, " +
//                "Score, " +
//                //"Printed_Date, " +
//                //"Emailed_Date, " +
//                //"Reported_Date, " +
//                "View_Date, " +
//                "UniqueID, " +
//                "AlterID, " +
//                "GCCharged " +
//                  "from Test " +
//                  "where UserName = @UserName " +
//                  "and Status = 'C' " +
//                  "order by LastMod DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName;
//              cn.Open();
//              return GetTestCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Tests for the specified UserID
//      /// </summary>
//      public override List<TestInfo> GetTestsByUserID(int UserID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TestID, " +
//                  "OldTestID, " +
//                  "TopicID, " +
//                  "UserID, " +
//                  "UserName, " +
//                  "Name, " +
//                  "Status, " +
//                  "BuyDate, " +
//                  "LastMod, " +
//                  "Credits, " +
//                  "NoSurvey, " +
//                  "XMLTest, " +
//                  "XMLSurvey, " +
//                  "TestVers, " +
//                  "LectEvtID, " +
//                  "SurveyComplete, " +
//                "VignetteComplete, " +
//                //"XMLVignette, " +
//                //"VignetteVersion, " +
//                //"VignetteScore, " +
//                "Score, " +
//                //"Printed_Date, " +
//                //"Emailed_Date, " +
//                //"Reported_Date, " +
//                "View_Date, " +
//                "UniqueID, " +
//                "AlterID, " +
//                "GCCharged " +
//                  "from Test " +
//                  "where UserID = @UserID " +
//                  "order by LastMod DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//              cn.Open();
//              return GetTestCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Incomplete Tests for the specified UserID
//      /// </summary>
//      public override List<TestInfo> GetIncompleteTestsByUserID(int UserID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TestID, " +
//                  "OldTestID, " +
//                  "TopicID, " +
//                  "UserID, " +
//                  "UserName, " +
//                  "Name, " +
//                  "Status, " +
//                  "BuyDate, " +
//                  "LastMod, " +
//                  "Credits, " +
//                  "NoSurvey, " +
//                  "XMLTest, " +
//                  "XMLSurvey, " +
//                  "TestVers, " +
//                  "LectEvtID, " +
//                  "SurveyComplete, " +
//                "VignetteComplete, " +
//                //"XMLVignette, " +
//                //"VignetteVersion, " +
//                //"VignetteScore, " +
//                "Score, " +
//                //"Printed_Date, " +
//                //"Emailed_Date, " +
//                //"Reported_Date, " +
//                "View_Date, " +
//                "UniqueID, " +
//                "AlterID, " +
//                "GCCharged " +
//                  "from Test " +
//                  "where UserID = @UserID " +
//                  "and Status = 'I' " +
//                  "order by LastMod DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//              cn.Open();
//              return GetTestCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Incomplete Tests for the specified UserID
//      /// </summary>
//      public override List<TestInfo> GetIncompleteTestsByUserIDAndTopicID(int UserID, int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TestID, " +
//                  "OldTestID, " +
//                  "TopicID, " +
//                  "UserID, " +
//                  "UserName, " +
//                  "Name, " +
//                  "Status, " +
//                  "BuyDate, " +
//                  "LastMod, " +
//                  "Credits, " +
//                  "NoSurvey, " +
//                  "XMLTest, " +
//                  "XMLSurvey, " +
//                  "TestVers, " +
//                  "LectEvtID, " +
//                  "SurveyComplete, " +
//                "VignetteComplete, " +
//                //"XMLVignette, " +
//                //"VignetteVersion, " +
//                //"VignetteScore, " +
//                "Score, " +
//                //"Printed_Date, " +
//                //"Emailed_Date, " +
//                //"Reported_Date, " +
//                "View_Date, " +
//                "UniqueID, " +
//                "AlterID, " +
//                "GCCharged " +
//                  "from Test " +
//                  "where UserID = @UserID " +
//                  "and TopicID = @TopicID " +
//                  "and Status = 'I' " +
//                  "order by BuyDate DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return GetTestCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }
//      /// <summary>
//      /// Retrieves all Completed Tests for the specified UserID
//      /// </summary>
//      public override DataSet GetCompletedTestsByUserID(int UserID)
//      {
//          SqlDataAdapter adapter;
//          System.Data.DataSet ds= new DataSet();
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "a.TestID, " +
//                  "a.OldTestID, " +
//                  "a.TopicID, " +                  
//                  "b.Subtitle, " +
//                  "b.Course_Number, " +
//                  "a.UserID, " +
//                  "a.UserName, " +
//                  "a.Name, " +
//                  "a.Status, " +
//                  "a.BuyDate, " +
//                  "a.LastMod, " +
//                  "a.Credits, " +
//                  "a.NoSurvey, " +
//                  "a.XMLTest, " +
//                  "a.XMLSurvey, " +
//                  "a.TestVers, " +
//                  "a.LectEvtID, " +
//                  "a.SurveyComplete, " +
//                "a.VignetteComplete, " +
//                //"a.XMLVignette, " +
//                //"a.VignetteVersion, " +
//                //"a.VignetteScore, " +
//                "a.Score, " +
//                //"a.Printed_Date, " +
//                //"a.Emailed_Date, " +
//                //"a.Reported_Date, " +
//                "a.View_Date, " +
//                "a.UniqueID, " +
//                "a.AlterID, " +
//                "a.GCCharged, " +
//                "b.facilityid " +
//                  "from Test a inner join Topic b on a.topicid = b.topicid " +
//                  "where UserID = @UserID " +
//                  "and Status = 'C' " +
//                  "order by LastMod DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//              cn.Open();
//              cmd.CommandType = CommandType.Text;
//              adapter = new SqlDataAdapter(cmd);
//              adapter.Fill(ds);             
              
//                    return ds;
//          }
//      }

//      /// <summary>
//      /// Retrieves Completed Test for the specified UserID  and TestID
//      /// </summary>
//      public override  TestInfo GetCompletedTestByTestIDUserID(int UserID,int TestID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "a.TestID, " +
//                  "a.OldTestID, " +
//                  "a.TopicID, " +
//                  "a.UserID, " +
//                  "a.UserName, " +
//                  "a.Name, " +
//                  "a.Status, " +
//                  "a.BuyDate, " +
//                  "a.LastMod, " +
//                  "a.Credits, " +
//                  "a.NoSurvey, " +
//                  "a.XMLTest, " +
//                  "a.XMLSurvey, " +
//                  "a.TestVers, " +
//                  "a.LectEvtID, " +
//                  "a.SurveyComplete, " +
//                "a.VignetteComplete, " +
//                  //"a.XMLVignette, " +
//                  //"a.VignetteVersion, " +
//                  //"a.VignetteScore, " +
//                "a.Score, " +
//                  //"a.Printed_Date, " +
//                  //"a.Emailed_Date, " +
//                  //"a.Reported_Date, " +
//                "a.View_Date, " +
//                "a.UniqueID, " +
//                "a.AlterID, " +
//                "a.GCCharged, " +
//                "b.facilityid " +
//                  "from Test a inner join Topic b on a.topicid = b.topicid " +
//                  "where UserID = @UserID " +
//                  "and Status = 'C' " +
//                  "and TestID = @TestID " +
//                  "order by LastMod DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

//              cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = TestID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetTestFromReader(reader, true);
//              else
//                  return null;
            
//          }
//      }
//      /// <summary>
//      /// Retrieves all Completed Tests for the specified UserID
//      /// </summary>
//      public override DataSet GetNeedSurveyTestsByUserID(int UserID)
//      {
//          SqlDataAdapter adapter;
//          System.Data.DataSet ds = new DataSet();
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "a.TestID, " +
//                  "a.OldTestID, " +
//                  "a.TopicID, " +  
//                  "b.subtitle, "+
//                  "b.Course_Number, "+
//                  "a.UserID, " +
//                  "a.UserName, " +
//                  "a.Name, " +
//                  "a.Status, " +
//                  "a.BuyDate, " +
//                  "a.LastMod, " +
//                  "a.Credits, " +
//                  "a.NoSurvey, " +
//                  "a.XMLTest, " +
//                  "a.XMLSurvey, " +
//                  "a.TestVers, " +
//                  "a.LectEvtID, " +
//                  "a.SurveyComplete, " +
//                "a.VignetteComplete, " +
//                //"a.XMLVignette, " +
//                //"a.VignetteVersion, " +
//                //"a.VignetteScore, " +
//                "a.Score, " +
//                //"a.Printed_Date, " +
//                //"a.Emailed_Date, " +
//                //"a.Reported_Date, " +
//                "a.View_Date, " +
//                "a.UniqueID, " +
//                "a.AlterID, " +
//                "a.GCCharged, " +
//                "b.facilityid " +
//                  "from Test a inner join Topic b on a.topicid = b.topicid " +
//                  "where UserID = @UserID " +
//                  "and Status = 'I' and a. Score >=b.pass_score " +
//                  "order by LastMod DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//              cn.Open();
//              cmd.CommandType = CommandType.Text;
//              adapter = new SqlDataAdapter(cmd);
//              adapter.Fill(ds);
//              return ds;
//          }
//      }

//      /// <summary>
//      /// Returns the total number of test completions for the specified FacilityID
//      /// </summary>
//      public override int GetCompletedTestCountByFacilityID(int FacilityID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Test " +
//                  "where status='C' and UserID IN (select iid from useraccount where " +
//                  "FacilityID=@FacilityID) ", cn);
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }


       
//       /////////////////////////////////////////////////////////
//      // methods that work with TestDefinitions

//      /// <summary>
//      /// Returns the total number of TestDefinitions
//      /// </summary>
//      public override int GetTestDefinitionCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from TestDefinition", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all TestDefinitions
//      /// </summary>
//      public override List<TestDefinitionInfo> GetTestDefinitions(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from TestDefinition";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTestDefinitionCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the TestDefinition with the specified ID
//      /// </summary>
//      public override TestDefinitionInfo GetTestDefinitionByID(int TestDefID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from TestDefinition where TestDefID=@TestDefID", cn);
//            cmd.Parameters.Add("@TestDefID", SqlDbType.Int).Value = TestDefID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetTestDefinitionFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Retrieves the TestDefinition associated with the specified TopicID
//      /// </summary>
//      public override TestDefinitionInfo GetTestDefinitionByTopicID(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from TestDefinition where TopicID=@TopicID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetTestDefinitionFromReader(reader, true);
//              else
//                  return null;
//          }
//      }


       
//       /// <summary>
//      /// Deletes a TestDefinition
//      /// </summary>
//      public override bool DeleteTestDefinition(int TestDefID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from TestDefinition where TestDefID=@TestDefID", cn);
//            cmd.Parameters.Add("@TestDefID", SqlDbType.Int).Value = TestDefID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new TestDefinition
//      /// </summary>
//      public override int InsertTestDefinition(TestDefinitionInfo TestDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into TestDefinition " +
//            "(TopicID, " +
//            "XMLTest, " +
//            "Version) " +
//            "VALUES (" +
//            "@TopicID, " +
//            "@XMLTest, " +
//            "@Version) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TestDefinition.TopicID ;
//            cmd.Parameters.Add("@XMLTest", SqlDbType.VarChar).Value = TestDefinition.XMLTest ;
//            cmd.Parameters.Add("@Version", SqlDbType.Int).Value = TestDefinition.Version ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a TestDefinition
//      /// </summary>
//      public override bool UpdateTestDefinition(TestDefinitionInfo TestDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update TestDefinition set " +
//            "TopicID = @TopicID, " +
//            "XMLTest = @XMLTest, " +
//            "Version = @Version " +
//            "where TestDefID = @TestDefID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TestDefinition.TopicID ;
//            cmd.Parameters.Add("@XMLTest", SqlDbType.VarChar).Value = TestDefinition.XMLTest ;
//            cmd.Parameters.Add("@Version", SqlDbType.Int).Value = TestDefinition.Version ;
//            cmd.Parameters.Add("@TestDefID", SqlDbType.Int).Value = TestDefinition.TestDefID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with ViDefinitions

//      /// <summary>
//      /// Returns the total number of ViDefinitions
//      /// </summary>
//      public override int GetViDefinitionCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from ViDefinition", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all ViDefinitions
//      /// </summary>
//      public override List<ViDefinitionInfo> GetViDefinitions(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from ViDefinition";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetViDefinitionCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the ViDefinition with the specified ID
//      /// </summary>
//      public override ViDefinitionInfo GetViDefinitionByID(int ViDefID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from ViDefinition where ViDefID=@ViDefID", cn);
//              cmd.Parameters.Add("@ViDefID", SqlDbType.Int).Value = ViDefID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetViDefinitionFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Retrieves the ViDefinition associated with the specified TopicID
//      /// </summary>
//      public override ViDefinitionInfo GetViDefinitionByTopicID(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from ViDefinition where TopicID=@TopicID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetViDefinitionFromReader(reader, true);
//              else
//                  return null;
//          }
//      }



//      /// <summary>
//      /// Deletes a ViDefinition
//      /// </summary>
//      public override bool DeleteViDefinition(int ViDefID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from ViDefinition where ViDefID=@ViDefID", cn);
//              cmd.Parameters.Add("@ViDefID", SqlDbType.Int).Value = ViDefID;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new ViDefinition
//      /// </summary>
//      public override int InsertViDefinition(ViDefinitionInfo ViDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into ViDefinition " +
//            "(TopicID, " +
//            "XMLTest, " +
//            "Vignette_Desc, " +
//            "Version) " +
//            "VALUES (" +
//            "@TopicID, " +
//            "@XMLTest, " +
//            "@Vignette_Desc, " +
//            "@Version) SET @ID = SCOPE_IDENTITY()", cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = ViDefinition.TopicID;
//              cmd.Parameters.Add("@XMLTest", SqlDbType.Text).Value = ViDefinition.XMLTest;
//              cmd.Parameters.Add("@Vignette_Desc", SqlDbType.Text).Value = ViDefinition.Vignette_Desc;
//              cmd.Parameters.Add("@Version", SqlDbType.Int).Value = ViDefinition.Version;

//              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              IDParameter.Direction = ParameterDirection.Output;
//              cmd.Parameters.Add(IDParameter);

//              cn.Open();
//              cmd.ExecuteNonQuery();

//              int NewID = (int)IDParameter.Value;
//              return NewID;

//          }
//      }

//      /// <summary>
//      /// Updates a ViDefinition
//      /// </summary>
//      public override bool UpdateViDefinition(ViDefinitionInfo ViDefinition)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update ViDefinition set " +
//            "TopicID = @TopicID, " +
//            "XMLTest = @XMLTest, " +
//            "Vignette_Desc = @Vignette_Desc, " +
//            "Version = @Version " +
//            "where ViDefID = @ViDefID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = ViDefinition.TopicID;
//              cmd.Parameters.Add("@XMLTest", SqlDbType.Text).Value = ViDefinition.XMLTest;
//              cmd.Parameters.Add("@Vignette_Desc", SqlDbType.Text).Value = ViDefinition.Vignette_Desc;
//              cmd.Parameters.Add("@Version", SqlDbType.Int).Value = ViDefinition.Version;
//              cmd.Parameters.Add("@ViDefID", SqlDbType.Int).Value = ViDefinition.ViDefID;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /////////////////////////////////////////////////////////
//      // methods that work with Topics

//      /// <summary>
//      /// Returns the total number of Topics (not obsolete ones)
//      /// </summary>
//      public override int GetTopicCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Topic where Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=0 ", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }


//      public override int GetSectionDiscountIdByTopicId(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select discountid from discount where topicid=@TopicID and section_ind='1'",cn);
//              cmd.Parameters.Add("TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              System.Data.IDataReader objreader = ExecuteReader(cmd);
//              if (objreader.Read())
//              {
//                  return (int)objreader[0];
//              }
//              else
//              {
//                  return 0;
//              }

//          }
//      }

//      /// <summary>
//      /// Returns the total number of Textbook Topics (not obsolete ones)
//      /// </summary>
//      public override int GetTextbookTopicCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Topic where textbook='1' and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=0 ", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of Non-Textbook Topics (not obsolete ones)
//      /// </summary>
//      public override int GetNonTextbookTopicCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Topic where textbook='0' and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=0 ", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of Obsolete Topics 
//      /// </summary>
//      public override int GetObsoleteTopicCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Topic where Obsolete = '1' and FacilityID=0 ", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of Obsolete Textbook Topics 
//      /// </summary>
//      public override int GetObsoleteTextbookTopicCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Topic where textbook='1' and Obsolete = '1' and FacilityID=0 ", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of Obsolete Non-Textbook Topics 
//      /// </summary>
//      public override int GetObsoleteNonTextbookTopicCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Topic where textbook='0' and Obsolete = '1' and FacilityID=0 ", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Topics (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetTopics(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName+' , '+ Course_Number  As TopicName," +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                  "Featured, " +
//                  "rev, "+
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, "+
//                  "notest, " +
//                    "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic where Obsolete = '0' and FacilityID in (0,-1) ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }
             
     
//      /// <summary>
//      /// Retrieves all Featured Topics (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetFeaturedTopics(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                 "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                    "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic  where Featured='1' and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=0  ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }




//      /// <summary>
//      /// Retrieves all Audio Topics(only Retail) (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetAudioTopics(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                 "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                    "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic  where audio_ind='1' and Obsolete = '0' and Topic.Avail_Ind = '1' and FacilityID=0  ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Retrieves all Featured Topics (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetFeaturedTopicsByFacilityId(int facilityid,string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                   "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic  where Featured='1' and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=@FacilityId  ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = facilityid;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Chapters (Bhaskar N)
//      /// </summary>
//      public override List<TopicInfo> GetAllChaptersByTopicId(int TopicId)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "Topic.TopicID, " +
//                  "cast(TopicCollection.chapternumber as nvarchar) + ' - ' + Topic.TopicName As TopicName," +                  
//                  "Topic.HTML, " +
//                  "Topic.CorLecture, " +
//                  "Topic.Fla_CEType, " +
//                  "Topic.Fla_IDNo, " +
//                  "Topic.LastUpdate, " +
//                  "Topic.FolderName, " +
//                  "Topic.NoSurvey, " +
//                  "Topic.SurveyID, " +
//                  "Topic.Compliance, " +
//                  "Topic.MetaKW, " +
//                  "Topic.MetaDesc, " +
//                  "Topic.Hours, " +
//                  "Topic.MediaType, " +
//                  "Topic.Objectives, " +
//                  "Topic.Content, " +
//                  "Topic.Textbook, " +
//                  "Topic.CertID, " +
//                  "Topic.Method, " +
//                  "Topic.GrantBy, " +
//                  "Topic.DOCXFile, " +
//                  "Topic.DOCXHoFile, " +
//                  "Topic.Obsolete, " +
//                  "Topic.FacilityID, " +
//                  "Topic.Course_Number, " +
//                  //"Topic.CeRef, " +
//                  //"Topic.Release_Date, " +
//                  "Topic.Minutes, " +
//                  "Topic.Audio_Ind, " +
//                  "Topic.Apn_Ind, " +
//                  "Topic.Icn_Ind, " +
//                  "Topic.Jcaho_Ind, " +
//                  "Topic.Magnet_Ind, " +
//                  "Topic.Active_Ind, " +
//                  "Topic.Video_Ind, " +
//                  "Topic.Online_Ind, " +
//                  "Topic.Ebp_Ind, " +
//                  "Topic.Ccm_Ind, " +
//                  "Topic.Avail_Ind, " +
//                  "Topic.Cert_Cerp, " +
//                  "Topic.Ref_Html, " +
//                  "Topic.AlterID, " +
//                  "Topic.Pass_Score, " +
//                  "Topic.Subtitle, " +
//                  //"Topic.Cost, " +
//                  "Topic.Topic_Type, " +
//                  "Topic.Img_Name, " +
//                  "Topic.Img_Credit, " +
//                  "Topic.Img_Caption, " +
//                  "Topic.Accreditation, " +
//                  "Topic.Views, " +
//                  "Topic.PrimaryViews, " +
//                  "Topic.PageReads, " +
//                 "Topic.Featured, " +
//                  "Topic.rev, " +
//                  "Topic.urlmark, " +
//                  "Topic.offline_cost, " +
//                  "Topic.online_cost, " +
//                  "Topic.notest, " +
//                    "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from TopicCollection " +
//                  "join Topic on TopicCollection.chapterid = Topic.TopicId " +
//                  "where Topic.Active_Ind = '1' and TopicCollection.TopicId= @TopicId  " +
//                  "order by TopicCollection.chapternumber ";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Retrieves all Textbook Topics (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetTextbookTopics(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                 "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                    "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic  where Textbook='1' and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=0  ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all Non-Textbook Topics (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetNonTextbookTopics(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                   "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic where Textbook='0'  and Obsolete = '0' and Topic.Active_Ind = '1' ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Retrieves all Non-Textbook Topics By Facility Id(not obsolete by Facilityid)
//      /// </summary>
//      public override List<TopicInfo> GetNonTextbookTopicsByFacilityId(int facilityid,string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                    "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic where Textbook='0'  and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID= @FacilityID  ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }
              
//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = facilityid;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }




//      /// <summary>
//      /// Retrieves all Anthology Topics By Topic Id(not obsolete by Facilityid)
//      /// </summary>
//      public override List<TopicInfo> GetAnthologyTopicsByTopicId(int TopicID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "Topic.TopicID, " +
//                "TopicName+' , '+ Course_Number  As TopicName," +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                   "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic where topicid in (select top 500 chapterid from topiccollection where topiccollection.TopicID=@TopicID order by chapternumber desc ) and Textbook='0'  and Obsolete = '0' and Topic.Active_Ind = '1'";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }






//      /// <summary>
//      /// Retrieves all Obsolete Topics
//      /// </summary>
//      public override List<TopicInfo> GetObsoleteTopics(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                  "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                   "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic where Obsolete = '1' and FacilityID=0  ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }



//      /// <summary>
//      /// Retrieves all Obsolete Topics
//      /// </summary>
//      public override List<TopicInfo> GetFacilityTopics()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                    "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic where Obsolete = '0' and Active_Ind='1' and FacilityID>0  ";

//              // add on ORDER BY if provided
             
//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }



//      /// <summary>
//      /// Retrieves all Obsolete Textbook Topics
//      /// </summary>
//      public override List<TopicInfo> GetObsoleteTextbookTopics(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                   "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic  where Textbook='1' and Obsolete = '1' and FacilityID=0  ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }



//      /// <summary>
//      /// Retrieves all Obsolete Textbook Topics
//      /// </summary>
//      public override List<TopicInfo> GetTopicsByType(string CourseNumber, string Title, string category, string active, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//               string cSQLCommand = "select " +
//                      "Topic.TopicID, " +
//                      "TopicName, " +
//                      "HTML, " +
//                      "CorLecture, " +
//                      "Fla_CEType, " +
//                      "Fla_IDNo, " +
//                      "LastUpdate, " +
//                      "FolderName, " +
//                      "NoSurvey, " +
//                      "SurveyID, " +
//                      "Compliance, " +
//                      "Topic.MetaKW, " +
//                      "Topic.MetaDesc, " +
//                      "Hours, " +
//                      "MediaType, " +
//                      "Objectives, " +
//                      "Content, " +
//                      "Topic.Textbook, " +
//                      "CertID, " +
//                      "Method, " +
//                      "GrantBy, " +
//                      "DOCXFile, " +
//                      "DOCXHoFile, " +
//                      "Obsolete, " +
//                      "Topic.FacilityID, " +
//                      "Course_Number, " +
//                      //"CeRef, " +
//                      //"Release_Date, " +
//                      "Minutes, " +
//                      "Audio_Ind, " +
//                      "Apn_Ind, " +
//                      "Icn_Ind, " +
//                      "Jcaho_Ind, " +
//                      "Magnet_Ind, " +
//                      "Active_Ind, " +
//                      "Video_Ind, " +
//                      "Online_Ind, " +
//                      "Ebp_Ind, " +
//                      "Ccm_Ind, " +
//                      "Avail_Ind, " +
//                      "Cert_Cerp, " +
//                      "Ref_Html, " +
//                      "Topic.AlterID, " +
//                      "Pass_Score, " +
//                      "Subtitle, " +
//                      //"Cost, " +
//                      "Topic_Type, " +
//                      "Img_Name, " +
//                      "Img_Credit, " +
//                      "Img_Caption, " +
//                      "Accreditation, " +
//                      "Views, " +
//                      "PrimaryViews, " +
//                      "PageReads, " +
//                      "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                   "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                      "from Topic "; 
//              if (category == "1000007")
//              {
//                  cSQLCommand = cSQLCommand + "where  FacilityID=0 and topicname like '%" + Title + "%' and course_number like '%" + CourseNumber + "'";
//              }
//              else if (category == "1000008")
//              {
//                  cSQLCommand = cSQLCommand + "where  FacilityID=-1 and topicname like '%" + Title + "%' and course_number like '%" + CourseNumber + "'";
//              }
//              else if (category == "1000009")
//              {
//                  cSQLCommand = cSQLCommand + "where  FacilityID=-3 and topicname like '%" + Title + "%' and course_number like '%" + CourseNumber + "'";
//              }
//              else if (category == "1000001")
//              {
//                  cSQLCommand = cSQLCommand + "where topicname like '%" + Title + "%' and course_number like '%" + CourseNumber + "'";
//              }
//              else if (category == "1000003")
//              {
//                  cSQLCommand = cSQLCommand + "where topicname like '%" + Title + "%' and course_number like '%" + CourseNumber + "%' and active_ind='0'";
//              }
//              else
//              {
//                  cSQLCommand = cSQLCommand + "inner join categorylink on topic.topicid=categorylink.topicid inner join categories on categories.id=categorylink.categoryid " +
//                  "where topicname like '%" + Title + "%' and course_number like '%" + CourseNumber + "%' and categoryid='" + category + "'";
//              }
//              if (active == "1")
//              {
//                  cSQLCommand = cSQLCommand + "and active_ind='1'";
//              }
//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }



//      /// <summary>
//      /// Retrieves all Obsolete Non-Textbook Topics
//      /// </summary>
//      public override List<TopicInfo> GetObsoleteNonTextbookTopics(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                     "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic where Textbook='0' and Obsolete = '1' and FacilityID=0  ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Returns a collection with all Compliance Topics (compliance = true), except obsolete ones
//      /// NOTE: Because this is used by facility admins to customize compliance topics, we will
//      /// eliminate from the list any that are not assigned to a category, since they are not
//      /// available to the public
//      /// </summary>
//      public override List<TopicInfo> GetComplianceTopics(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                     "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                  "from Topic where Compliance='1' and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=0  " +
//                  "and TopicID in (select TopicID from CategoryLink) " ;

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Retrieves the Topic with the specified ID
//      /// </summary>
//      public override TopicInfo GetTopicByID(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select " +
//                      "TopicID, " +
//                      "TopicName, " +
//                      "HTML, " +
//                      "CorLecture, " +
//                      "Fla_CEType, " +
//                      "Fla_IDNo, " +
//                      "LastUpdate, " +
//                      "FolderName, " +
//                      "NoSurvey, " +
//                      "SurveyID, " +
//                      "Compliance, " +
//                      "MetaKW, " +
//                      "MetaDesc, " +
//                      "Hours, " +
//                      "MediaType, " +
//                      "Objectives, " +
//                      "Content, " +
//                      "Textbook, " +
//                      "CertID, " +
//                      "Method, " +
//                      "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                  "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                    "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                      "from Topic where TopicID=@TopicID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetTopicFromReader(reader, true);
//              else
//                  return null;
//          }
//      }


//      /// <summary>
//      /// Retrieves the Topic with the specified ID
//      /// Bsk
//      /// </summary>
//      public override TopicInfo GetTopicByCourseNumber(string Course_Number)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select " +
//                      "TopicID, " +
//                      "TopicName, " +
//                      "HTML, " +
//                      "CorLecture, " +
//                      "Fla_CEType, " +
//                      "Fla_IDNo, " +
//                      "LastUpdate, " +
//                      "FolderName, " +
//                      "NoSurvey, " +
//                      "SurveyID, " +
//                      "Compliance, " +
//                      "MetaKW, " +
//                      "MetaDesc, " +
//                      "Hours, " +
//                      "MediaType, " +
//                      "Objectives, " +
//                      "Content, " +
//                      "Textbook, " +
//                      "CertID, " +
//                      "Method, " +
//                      "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                  "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                  "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                  "uce_Ind, " +
//                  "shortname, " +
//                  "categoryid, " +
//                  "hold_ind " +
//                      "from Topic where Course_Number=@Course_Number and Avail_Ind='1' and obsolete='0'", cn);
//              cmd.Parameters.Add("@Course_Number", SqlDbType.VarChar).Value = Course_Number;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetTopicFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Retrieves the Topic with the specified ID
//      /// Bsk
//      /// </summary>
//      public override TopicInfo GetTopicByUserName(string UserName)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select " +
//                    "Topic.TopicID, " +
//                    "TopicName, " +
//                    "HTML, " +
//                    "CorLecture, " +
//                    "Fla_CEType, " +
//                    "Fla_IDNo, " +
//                    "LastUpdate, " +
//                    "FolderName, " +
//                    "NoSurvey, " +
//                    "SurveyID, " +
//                    "Compliance, " +
//                    "MetaKW, " +
//                    "MetaDesc, " +
//                    "Hours, " +
//                    "MediaType, " +
//                    "Objectives, " +
//                    "Content, " +
//                    "Textbook, " +
//                    "CertID, " +
//                    "Method, " +
//                    "GrantBy, " +
//                    "DOCXFile, " +
//                    "DOCXHoFile, " +
//                    "Obsolete, " +
//                    "FacilityID, " +
//                    "Course_Number, " +
//                    //"CeRef, " +
//                    //"Release_Date, " +
//                    "Minutes, " +
//                    "Audio_Ind, " +
//                    "Apn_Ind, " +
//                    "Icn_Ind, " +
//                    "Jcaho_Ind, " +
//                    "Magnet_Ind, " +
//                    "Active_Ind, " +
//                    "Video_Ind, " +
//                    "Online_Ind, " +
//                    "Ebp_Ind, " +
//                    "Ccm_Ind, " +
//                    "Avail_Ind, " +
//                    "Cert_Cerp, " +
//                    "Ref_Html, " +
//                    "AlterID, " +
//                    "Pass_Score, " +
//                    "Subtitle, " +
//                    //"Cost, " +
//                    "Topic_Type, " +
//                    "Img_Name, " +
//                    "Img_Credit, " +
//                    "Img_Caption, " +
//                    "Accreditation, " +
//                    "Views, " +
//                    "PrimaryViews, " +
//                    "PageReads, " +
//                    "Featured, " +
//                    "rev, " +
//                    "urlmark, " +
//                    "offline_cost, " +
//                    "online_cost, " +
//                    "notest, " +
//                    "uce_Ind, " +
//                    "shortname, " +
//                    "categoryid, " +
//                    "hold_ind " +
//                "from Topic " +
//                "join Discount on Topic.topicid =Discount.topicid " +
//                " where Discount.username=@UserName and Discount.virtualurl_ind = 1 ", cn);

//              cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName.ToLower();
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetTopicFromReader(reader, true);
//              else
//                  return null;
//          }
//      }
//      /// <summary>
//      /// Deletes a Topic
//      /// </summary>
//      public override bool DeleteTopic(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from Topic where TopicID=@TopicID", cn);
//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new Topic
//      /// </summary>
//      public override int InsertTopic(TopicInfo Topic)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into Topic " +
//            "(CorLecture, " +
//            "Fla_CEType, " +
//            "Fla_IDNo, " +
//            "FolderName, " +
//            "HTML, " +
//            "LastUpdate, " +
//            "NoSurvey, " +
//            "TopicName, " +
//            "SurveyID, " +
//            "Compliance, " +
//            "MetaKW, " +
//            "MetaDesc, " +
//            "Hours, " +
//            "MediaType, " +
//            "Objectives, " +
//            "Content, " +
//            "Textbook, " +
//            "CertID, " +
//            "Method, " +
//            "GrantBy, " +
//            "DOCXFile, " +
//            "DOCXHoFile, " +
//            "Obsolete, " +
//              "FacilityID, " +
//              "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                 "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                 "uce_Ind, " +
//            "shortname, " +
//            "categoryid, " +
//            "hold_ind )" +
//            "VALUES (" +
//            "@CorLecture, " +
//            "@Fla_CEType, " +
//            "@Fla_IDNo, " +
//            "@FolderName, " +
//            "@HTML, " +
//            "@LastUpdate, " +
//            "@NoSurvey, " +
//            "@TopicName, " +
//            "@SurveyID, " +
//            "@Compliance, " +
//            "@MetaKW, " +
//            "@MetaDesc, " +
//            "@Hours, " +
//            "@MediaType, " +
//            "@Objectives, " +
//            "@Content, " +
//            "@Textbook, " +
//            "@CertID, " +
//            "@Method, " +
//            "@GrantBy, " +
//            "@DOCXFile, " +
//            "@DOCXHoFile, " +
//            "@Obsolete, " +
//              "@FacilityID, " +
//                  "@Course_Number, " +
//                  //"@CeRef, " +
//                  //"@Release_Date, " +
//                  "@Minutes, " +
//                  "@Audio_Ind, " +
//                  "@Apn_Ind, " +
//                  "@Icn_Ind, " +
//                  "@Jcaho_Ind, " +
//                  "@Magnet_Ind, " +
//                  "@Active_Ind, " +
//                  "@Video_Ind, " +
//                  "@Online_Ind, " +
//                  "@Ebp_Ind, " +
//                  "@Ccm_Ind, " +
//                  "@Avail_Ind, " +
//                  "@Cert_Cerp, " +
//                  "@Ref_Html, " +
//                  "@AlterID, " +
//                  "@Pass_Score, " +
//                  "@Subtitle, " +
//                  //"@Cost, " +
//                  "@Topic_Type, " +
//                  "@Img_Name, " +
//                  "@Img_Credit, " +
//                  "@Img_Caption, " +
//                  "@Accreditation, " +
//                  "@Views, " +
//                  "@PrimaryViews, " +
//                  "@PageReads, " +
//                  "@Featured, " +
//                  "@rev, " +
//                  "@urlmark, " +
//                  "@offline_cost, " +
//                  "@online_cost, " +
//                  "@notest, " +
//                 "@uce_Ind, " +
//            "@shortname, " +
//            "@categoryid, " +
//            "@hold_ind  ) SET @ID = SCOPE_IDENTITY()", cn);

//              cmd.Parameters.Add("@notest", SqlDbType.Bit).Value = Topic.notest;
//              cmd.Parameters.Add("@uce_Ind", SqlDbType.Bit).Value = Topic.uce_Ind;
//              cmd.Parameters.Add("@rev", SqlDbType.VarChar).Value = Topic.rev;
//              cmd.Parameters.Add("@urlmark", SqlDbType.VarChar).Value = Topic.urlmark;
//              cmd.Parameters.Add("@offline_cost", SqlDbType.Decimal).Value = Topic.offline_Cost;
//              cmd.Parameters.Add("@online_cost", SqlDbType.Decimal).Value = Topic.online_Cost;
//            cmd.Parameters.Add("@CorLecture", SqlDbType.Bit).Value = Topic.CorLecture ;
//            cmd.Parameters.Add("@Fla_CEType", SqlDbType.Char).Value = Topic.Fla_CEType ;
//            cmd.Parameters.Add("@Fla_IDNo", SqlDbType.VarChar).Value = Topic.Fla_IDNo;
//            cmd.Parameters.Add("@FolderName", SqlDbType.VarChar).Value = Topic.FolderName ;
//            cmd.Parameters.Add("@HTML", SqlDbType.VarChar).Value = Topic.HTML ;
//            cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = Topic.LastUpdate ;
//            cmd.Parameters.Add("@NoSurvey", SqlDbType.Bit).Value = Topic.NoSurvey ;
//            cmd.Parameters.Add("@TopicName", SqlDbType.VarChar).Value = Topic.TopicName ;

//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (Topic.SurveyID == 0)
//            {
//                cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = Topic.SurveyID;
//            }

//            cmd.Parameters.Add("@Compliance", SqlDbType.Bit).Value = Topic.Compliance ;
//            cmd.Parameters.Add("@MetaKW", SqlDbType.VarChar).Value = Topic.MetaKW ;
//            cmd.Parameters.Add("@MetaDesc", SqlDbType.VarChar).Value = Topic.MetaDesc ;
//            cmd.Parameters.Add("@Hours", SqlDbType.Decimal).Value = Topic.Hours ;
//            cmd.Parameters.Add("@MediaType", SqlDbType.VarChar).Value = Topic.MediaType ;
//            cmd.Parameters.Add("@Objectives", SqlDbType.VarChar).Value = Topic.Objectives ;
//            cmd.Parameters.Add("@Content", SqlDbType.VarChar).Value = Topic.Content ;
//            cmd.Parameters.Add("@Textbook", SqlDbType.Bit).Value = Topic.Textbook ;
//            cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = Topic.CertID ;
//            cmd.Parameters.Add("@Method", SqlDbType.VarChar).Value = Topic.Method;
//            cmd.Parameters.Add("@GrantBy", SqlDbType.VarChar).Value = Topic.GrantBy;
//            cmd.Parameters.Add("@DOCXFile", SqlDbType.VarChar).Value = Topic.DOCXFile;
//            cmd.Parameters.Add("@DOCXHoFile", SqlDbType.VarChar).Value = Topic.DOCXHoFile;
//            cmd.Parameters.Add("@Obsolete", SqlDbType.Bit).Value = Topic.Obsolete;
//            cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = Topic.FacilityID;

//            cmd.Parameters.Add("@Course_Number", SqlDbType.VarChar).Value = Topic.Course_Number;
//            //cmd.Parameters.Add("@CeRef", SqlDbType.VarChar).Value = Topic.CeRef;
//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (Topic.Release_Date == System.DateTime.MinValue)
//            //{
//            //    cmd.Parameters.Add("@Release_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            //    cmd.Parameters.Add("@Release_Date", SqlDbType.DateTime).Value = Topic.Release_Date;
//            //}
//            cmd.Parameters.Add("@Minutes", SqlDbType.Int).Value = Topic.Minutes;

//            cmd.Parameters.Add("@Audio_Ind", SqlDbType.Bit).Value = Topic.Audio_Ind;
//            cmd.Parameters.Add("@Apn_Ind", SqlDbType.Bit).Value = Topic.Apn_Ind;
//            cmd.Parameters.Add("@Icn_Ind", SqlDbType.Bit).Value = Topic.Icn_Ind;
//            cmd.Parameters.Add("@Jcaho_Ind", SqlDbType.Bit).Value = Topic.Jcaho_Ind;
//            cmd.Parameters.Add("@Magnet_Ind", SqlDbType.Bit).Value = Topic.Magnet_Ind;
//            cmd.Parameters.Add("@Active_Ind", SqlDbType.Bit).Value = Topic.Active_Ind;
//            cmd.Parameters.Add("@Video_Ind", SqlDbType.Bit).Value = Topic.Video_Ind;
//            cmd.Parameters.Add("@Online_Ind", SqlDbType.Bit).Value = Topic.Online_Ind;
//            cmd.Parameters.Add("@Ebp_Ind", SqlDbType.Bit).Value = Topic.Ebp_Ind;
//            cmd.Parameters.Add("@Ccm_Ind", SqlDbType.Bit).Value = Topic.Ccm_Ind;
//            cmd.Parameters.Add("@Avail_Ind", SqlDbType.Bit).Value = Topic.Avail_Ind;

//            cmd.Parameters.Add("@Cert_Cerp", SqlDbType.VarChar).Value = Topic.Cert_Cerp;
//            cmd.Parameters.Add("@Ref_Html", SqlDbType.Text).Value = Topic.Ref_Html;
//            cmd.Parameters.Add("@AlterID", SqlDbType.Int).Value = Topic.AlterID;
//            cmd.Parameters.Add("@Pass_Score", SqlDbType.Int).Value = Topic.Pass_Score;
//            cmd.Parameters.Add("@Subtitle", SqlDbType.VarChar).Value = Topic.Subtitle;
//            //cmd.Parameters.Add("@Cost", SqlDbType.Decimal).Value = Topic.Cost;
//            cmd.Parameters.Add("@Topic_Type", SqlDbType.VarChar).Value = Topic.Topic_Type;
//            cmd.Parameters.Add("@Img_Name", SqlDbType.VarChar).Value = Topic.Img_Name;
//            cmd.Parameters.Add("@Img_Credit", SqlDbType.VarChar).Value = Topic.Img_Credit;
//            cmd.Parameters.Add("@Img_Caption", SqlDbType.VarChar).Value = Topic.Img_Caption;
//            cmd.Parameters.Add("@Accreditation", SqlDbType.VarChar).Value = Topic.Accreditation;
//            cmd.Parameters.Add("@Views", SqlDbType.Int).Value = Topic.Views;
//            cmd.Parameters.Add("@PrimaryViews", SqlDbType.Int).Value = Topic.PrimaryViews;
//            cmd.Parameters.Add("@PageReads", SqlDbType.Int).Value = Topic.PageReads;
//            cmd.Parameters.Add("@Featured", SqlDbType.Bit).Value = Topic.Featured;
//            cmd.Parameters.Add("@shortname", SqlDbType.VarChar).Value = Topic.shortname;
//            cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = Topic.categoryid;
//            cmd.Parameters.Add("@hold_ind", SqlDbType.Bit).Value = Topic.hold_ind;


//              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a Topic
//      /// </summary>
//      public override bool UpdateTopic(TopicInfo Topic)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Topic set " +
//            "CorLecture = @CorLecture, " +
//            "Fla_CEType = @Fla_CEType, " +
//            "Fla_IDNo = @Fla_IDNo, " +
//            "FolderName = @FolderName, " +
//            "HTML = @HTML, " +
//            "LastUpdate = @LastUpdate, " +
//            "NoSurvey = @NoSurvey, " +
//            "TopicName = @TopicName, " +
//            "SurveyID = @SurveyID, " +
//            "Compliance = @Compliance, " +
//            "MetaKW = @MetaKW, " +
//            "MetaDesc = @MetaDesc, " +
//            "Hours = @Hours, " +
//            "MediaType = @MediaType, " +
//            "Objectives = @Objectives, " +
//            "Content = @Content, " +
//            "Textbook = @Textbook, " +
//            "CertID = @CertID, " +
//            "Method = @Method, " +
//            "GrantBy = @GrantBy, " +
//              "DOCXFile = @DOCXFile, " +
//              "DOCXHoFile = @DOCXHoFile, " +
//              "Obsolete = @Obsolete, " +
//              "FacilityID = @FacilityID, " +
//              "Course_Number = @Course_Number, " +
//              //"CeRef = @CeRef, " +
//              //"Release_Date = @Release_Date, " +
//              "Minutes = @Minutes, " +
//              "Audio_Ind = @Audio_Ind, " +
//              "Apn_Ind = @Apn_Ind, " +
//              "Icn_Ind = @Icn_Ind, " +
//              "Jcaho_Ind = @Jcaho_Ind, " +
//              "Magnet_Ind = @Magnet_Ind, " +
//              "Active_Ind = @Active_Ind, " +
//              "Video_Ind = @Video_Ind, " +
//              "Online_Ind = @Online_Ind, " +
//              "Ebp_Ind = @Ebp_Ind, " +
//              "Ccm_Ind = @Ccm_Ind, " +
//              "Avail_Ind = @Avail_Ind, " +
//              "Cert_Cerp = @Cert_Cerp, " +
//              "Ref_Html = @Ref_Html, " +
//              "AlterID = @AlterID, " +
//              "Pass_Score = @Pass_Score, " +
//              "Subtitle = @Subtitle, " +
//              //"Cost = @Cost, " +
//              "Topic_Type = @Topic_Type, " +
//              "Img_Name = @Img_Name, " +
//              "Img_Credit = @Img_Credit, " +
//              "Img_Caption = @Img_Caption, " +
//              "Accreditation = @Accreditation, " +
//              "Views = @Views, " +
//              "PrimaryViews = @PrimaryViews, " +
//              "PageReads = @PageReads, " +
//              "Featured = @Featured ," +
//               "rev = @rev ," +
//                "online_cost = @online_cost ," +
//                 "urlmark = @urlmark ," +
//                  "offline_cost = @offline_cost ," +
//                   "notest = @notest ," +
//                  "uce_Ind = @uce_Ind, " +
//            "shortname = @shortname, " +
//            "categoryid = @categoryid, " +
//            "hold_ind = @hold_ind " +
//            "where TopicID = @TopicID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@notest", SqlDbType.Bit).Value = Topic.notest;
//              cmd.Parameters.Add("@uce_Ind", SqlDbType.Bit).Value = Topic.uce_Ind;
//              cmd.Parameters.Add("@rev", SqlDbType.VarChar).Value = Topic.rev;
//              cmd.Parameters.Add("@urlmark", SqlDbType.VarChar).Value = Topic.urlmark;
//              cmd.Parameters.Add("@offline_cost", SqlDbType.Decimal).Value = Topic.offline_Cost;
//              cmd.Parameters.Add("@online_cost", SqlDbType.Decimal).Value = Topic.online_Cost;
//            cmd.Parameters.Add("@CorLecture", SqlDbType.Bit).Value = Topic.CorLecture ;
//            cmd.Parameters.Add("@Fla_CEType", SqlDbType.Char).Value = Topic.Fla_CEType ;
//            cmd.Parameters.Add("@Fla_IDNo", SqlDbType.VarChar).Value = Topic.Fla_IDNo ;
//            cmd.Parameters.Add("@FolderName", SqlDbType.VarChar).Value = Topic.FolderName ;
//            cmd.Parameters.Add("@HTML", SqlDbType.VarChar).Value = Topic.HTML ;
//            cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = Topic.LastUpdate ;
//            cmd.Parameters.Add("@NoSurvey", SqlDbType.Bit).Value = Topic.NoSurvey ;
//            cmd.Parameters.Add("@TopicName", SqlDbType.VarChar).Value = Topic.TopicName ;
            
//            // pass null to database if value is zero (nothing selected by user)
//            // this allows the database to have a NULL foreign key and still enforce relational integrity
//            if (Topic.SurveyID == 0)
//            {
//                cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = System.DBNull.Value;
//            }
//            else
//            {
//                cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = Topic.SurveyID;
//            }

//            cmd.Parameters.Add("@Compliance", SqlDbType.Bit).Value = Topic.Compliance;
//            cmd.Parameters.Add("@MetaKW", SqlDbType.VarChar).Value = Topic.MetaKW ;
//            cmd.Parameters.Add("@MetaDesc", SqlDbType.VarChar).Value = Topic.MetaDesc ;
//            cmd.Parameters.Add("@Hours", SqlDbType.Decimal).Value = Topic.Hours ;
//            cmd.Parameters.Add("@MediaType", SqlDbType.VarChar).Value = Topic.MediaType ;
//            cmd.Parameters.Add("@Objectives", SqlDbType.VarChar).Value = Topic.Objectives ;
//            cmd.Parameters.Add("@Content", SqlDbType.VarChar).Value = Topic.Content ;
//            cmd.Parameters.Add("@Textbook", SqlDbType.Bit).Value = Topic.Textbook ;
//            cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = Topic.CertID ;
//            cmd.Parameters.Add("@Method", SqlDbType.VarChar).Value = Topic.Method;
//            cmd.Parameters.Add("@GrantBy", SqlDbType.VarChar).Value = Topic.GrantBy;
//            cmd.Parameters.Add("@DOCXFile", SqlDbType.VarChar).Value = Topic.DOCXFile;
//            cmd.Parameters.Add("@DOCXHoFile", SqlDbType.VarChar).Value = Topic.DOCXHoFile;
//            cmd.Parameters.Add("@Obsolete", SqlDbType.Bit).Value = Topic.Obsolete;
//            cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = Topic.FacilityID;

//            cmd.Parameters.Add("@Course_Number", SqlDbType.VarChar).Value = Topic.Course_Number;
//            //cmd.Parameters.Add("@CeRef", SqlDbType.VarChar).Value = Topic.CeRef;
//            //// pass null to database if value is zero (nothing selected by user)
//            //// this allows the database to have a NULL foreign key and still enforce relational integrity
//            //if (Topic.Release_Date == System.DateTime.MinValue)
//            //{
//            //    cmd.Parameters.Add("@Release_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
//            //}
//            //else
//            //{
//            //    cmd.Parameters.Add("@Release_Date", SqlDbType.DateTime).Value = Topic.Release_Date;
//            //}
//            cmd.Parameters.Add("@Minutes", SqlDbType.Int).Value = Topic.Minutes;

//            cmd.Parameters.Add("@Audio_Ind", SqlDbType.Bit).Value = Topic.Audio_Ind;
//            cmd.Parameters.Add("@Apn_Ind", SqlDbType.Bit).Value = Topic.Apn_Ind;
//            cmd.Parameters.Add("@Icn_Ind", SqlDbType.Bit).Value = Topic.Icn_Ind;
//            cmd.Parameters.Add("@Jcaho_Ind", SqlDbType.Bit).Value = Topic.Jcaho_Ind;
//            cmd.Parameters.Add("@Magnet_Ind", SqlDbType.Bit).Value = Topic.Magnet_Ind;
//            cmd.Parameters.Add("@Active_Ind", SqlDbType.Bit).Value = Topic.Active_Ind;
//            cmd.Parameters.Add("@Video_Ind", SqlDbType.Bit).Value = Topic.Video_Ind;
//            cmd.Parameters.Add("@Online_Ind", SqlDbType.Bit).Value = Topic.Online_Ind;
//            cmd.Parameters.Add("@Ebp_Ind", SqlDbType.Bit).Value = Topic.Ebp_Ind;
//            cmd.Parameters.Add("@Ccm_Ind", SqlDbType.Bit).Value = Topic.Ccm_Ind;
//            cmd.Parameters.Add("@Avail_Ind", SqlDbType.Bit).Value = Topic.Avail_Ind;

//            cmd.Parameters.Add("@Cert_Cerp", SqlDbType.VarChar).Value = Topic.Cert_Cerp;
//            cmd.Parameters.Add("@Ref_Html", SqlDbType.Text).Value = Topic.Ref_Html;
//            cmd.Parameters.Add("@AlterID", SqlDbType.Int).Value = Topic.AlterID;
//            cmd.Parameters.Add("@Pass_Score", SqlDbType.Int).Value = Topic.Pass_Score;
//            cmd.Parameters.Add("@Subtitle", SqlDbType.VarChar).Value = Topic.Subtitle;
//            //cmd.Parameters.Add("@Cost", SqlDbType.Decimal).Value = Topic.Cost;
//            cmd.Parameters.Add("@Topic_Type", SqlDbType.VarChar).Value = Topic.Topic_Type;
//            cmd.Parameters.Add("@Img_Name", SqlDbType.VarChar).Value = Topic.Img_Name;
//            cmd.Parameters.Add("@Img_Credit", SqlDbType.VarChar).Value = Topic.Img_Credit;
//            cmd.Parameters.Add("@Img_Caption", SqlDbType.VarChar).Value = Topic.Img_Caption;
//            cmd.Parameters.Add("@Accreditation", SqlDbType.VarChar).Value = Topic.Accreditation;
//            cmd.Parameters.Add("@Views", SqlDbType.Int).Value = Topic.Views;
//            cmd.Parameters.Add("@PrimaryViews", SqlDbType.Int).Value = Topic.PrimaryViews;
//            cmd.Parameters.Add("@PageReads", SqlDbType.Int).Value = Topic.PageReads;
//            cmd.Parameters.Add("@Featured", SqlDbType.Bit).Value = Topic.Featured;
//            cmd.Parameters.Add("@shortname", SqlDbType.VarChar).Value = Topic.shortname;
//            cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = Topic.categoryid;
//            cmd.Parameters.Add("@hold_ind", SqlDbType.Bit).Value = Topic.hold_ind; 
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Topic.TopicID;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Updates a Topic Active Status
//      /// </summary>
//      public override bool UpdateTopicActiveStatus(bool status,int topicid)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Topic set  " +
//              "Active_Ind = @Active_Ind " +
//              "where TopicID = @TopicID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@Active_Ind", SqlDbType.Bit).Value = status;
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = topicid;

//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /// <summary>
//      /// Returns the total number of Topics for the specified CategoryID (not obsolete)
//      /// </summary>
//      public override int GetTopicCountByCategoryID(int CategoryID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from Topic " +
//                  "join categorylink on Topic.topicid = categorylink.topicid " +
//                  "where categorylink.CategoryID = @CategoryID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' ", cn);
//              //              cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }


//      /// <summary>
//      /// Returns the total number of Topics for the specified CategoryID (not obsolete)
//      /// </summary>
//      public override DataSet GetRecommendationTopicsByTopicId(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand();
//              cmd.Connection = cn;
//              cmd.CommandType = CommandType.StoredProcedure;
//              cmd.CommandText = "SP_Course_Recommendation";
//              cmd.Parameters.Add("@topic_id", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              SqlDataAdapter adpt = new SqlDataAdapter();
//              adpt.SelectCommand = cmd;
//              DataSet ds = new DataSet();
//              adpt.Fill(ds);
//              return ds;             
//          }
//      }

//      /// <summary>
//      /// Retrieves all topics for the specified CategoryID (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetTopicsByCategoryID(int CategoryID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "Topic.TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                 "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                  "uce_Ind " +
//                  "from Topic " +
//                  "join categorylink on Topic.topicid = categorylink.topicid " +
//                  "where categorylink.CategoryID = @CategoryID and Topic.Obsolete = '0' and Topic.Active_Ind = '1'";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all topics for the specified CategoryID (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetTextBookTopicsByCategoryId(int CategoryID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "Topic.TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                  "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                 "uce_Ind, " +
//                  "shortname, " +
//                  "Topic.categoryid, " +
//                  "hold_ind " +
//                  "from Topic " +
//                  "join categorylink on Topic.topicid = categorylink.topicid " +
//                  "where categorylink.CategoryID = @CategoryID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' and Topic.Textbook = '1' ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Retrieves all Retail topics for the specified CategoryID (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetTopicsByCategoryName(string CategoryName, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "Topic.TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                  "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                 "uce_Ind, " +
//                  "shortname, " +
//                  "Topic.categoryid, " +
//                  "hold_ind " +
//                  "from Topic " +
//                  "join categorylink on Topic.topicid = categorylink.topicid " +
//                  "where categorylink.CategoryID = (select id from categories where website=@CategoryName and showinlist='1' and facilityid='0' )and Topic.Obsolete = '0' and Topic.Avail_Ind = '1'";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@CategoryName", SqlDbType.VarChar).Value = CategoryName;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

      
//      /// <summary>
//      /// Retrieves all topics for the specified CategoryID (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetTextAndNonTextTopicsByCategoryId(int CategoryID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "Topic.TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                  "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                 "uce_Ind, " +
//                  "shortname, " +
//                  "Topic.categoryid, " +
//                  "hold_ind " +
//                  "from Topic " +
//                  "join categorylink on Topic.topicid = categorylink.topicid " +
//                  "where categorylink.CategoryID = @CategoryID and Topic.Obsolete = '0' and Topic.Active_Ind = '1'";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//           /// <summary>
//      /// Retrieves all topics for the specified search text (not obsolete)and Facility
//      /// NOTE: Looks in topicname, subtitle, objectives, course_number and authorname
//      /// </summary>
//      public override List<TopicInfo> GetTopicsBySearchTextandFacility(string cSearchText,int facilityid, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              // add space on each side of text (this limits the results to just those words and not fragments)
//              if (String.IsNullOrEmpty(cSearchText))
//              {
//                  // no search text, so make up some that will return nothing
//                  cSearchText = "XZXZXZXZXZXZXZXZXZXZ";
//              }
//              else
//              {
//                  cSearchText = cSearchText.Trim() ;
//              }
              
//              string cSQLCommand = "select " +
//                  "Topic.TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                  "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                  "uce_Ind, " +
//                  "shortname, " +
//                  "Topic.categoryid, " +
//                  "hold_ind " +
//                  "from Topic " +
//                  "where (topicid in " + 
//                  "(select topicid from categorylink inner join categories on categorylink.categoryid=categories.id where " +  
//                  "categories.showinlist='1') ) and "+
//                  "Topic.facilityid=@FacilityId and Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
//                  "and ( " +
//                  "    ( topic.topicname like '%" + cSearchText.Replace("'", "''") + "%' " +
//                  "      or topic.subtitle like '%" + cSearchText.Replace("'", "''") + "%' " +
//                  "      or topic.course_number like '%" + cSearchText.Replace("'", "''") + "%' " +
//                  "      or topic.objectives like '%" + cSearchText.Replace("'", "''") + "%' ) " +
//                  "    or " +
//                  "    ( topic.topicid in ( select topicid from topicresourcelink join resourceperson " +
//                  "                         on topicresourcelink.resourceid = resourceperson.resourceid " +
//                  "                         where topicresourcelink.restype = 'A' and " +
//                  "                         resourceperson.fullname like '%" + cSearchText.Replace("'", "''") + "%' ) ) " +
//                  "    ) " ;

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@SearchText", SqlDbType.VarChar).Value = cSearchText;
//              cmd.Parameters.Add("@FacilityId", SqlDbType.VarChar).Value = facilityid;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Retrieves all Retail topics for the specified search text (not obsolete)and Facility
//      /// NOTE: Looks in topicname, subtitle, objectives, course_number and authorname
//      /// </summary>
//      public override List<TopicInfo> GetRetailTopicsBySearchTextandFacility(string cSearchText, int facilityid, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              // add space on each side of text (this limits the results to just those words and not fragments)
//              if (String.IsNullOrEmpty(cSearchText))
//              {
//                  // no search text, so make up some that will return nothing
//                  cSearchText = "XZXZXZXZXZXZXZXZXZXZ";
//              }
//              else
//              {
//                  cSearchText = cSearchText.Trim();
//              }

//              string cSQLCommand = "select " +
//                  "Topic.TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                  "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                 "uce_Ind, " +
//                  "shortname, " +
//                  "Topic.categoryid, " +
//                  "hold_ind " +
//                  "from Topic " +
//                  "where (topicid in " +
//                    "(select topicid from categorylink inner join categories on categorylink.categoryid=categories.id " +
//                   "join categoryarealink on categories.id=categoryarealink.categoryid and categoryarealink.areaid=2 where " +
//                  "categories.showinlist='1') ) and " +
//                  "Topic.facilityid=@FacilityId and Topic.Obsolete = '0' and Topic.avail_Ind = '1' " +
//                  "and ( " +
//                  "    ( topic.topicname like '%" + cSearchText.Replace("'", "''") + "%' " +
//                  "      or topic.subtitle like '%" + cSearchText.Replace("'", "''") + "%' " +
//                  "      or topic.course_number like '%" + cSearchText.Replace("'", "''") + "%' " +
//                  "      or topic.objectives like '%" + cSearchText.Replace("'", "''") + "%' ) " +
//                  "    or " +
//                  "    ( topic.topicid in ( select topicid from topicresourcelink join resourceperson " +
//                  "                         on topicresourcelink.resourceid = resourceperson.resourceid " +
//                  "                         where topicresourcelink.restype = 'A' and " +
//                  "                         resourceperson.fullname like '%" + cSearchText.Replace("'", "''") + "%' ) ) " +
//                  " or" +
//                  "   ( topic.topicid in (select topicid from categorylink inner join categories on categorylink.categoryid=categories.id " +
//                  "join categoryarealink on categories.id=categoryarealink.categoryid and categoryarealink.areaid=2 where " +
//                  "categories.showinlist='1' and categories.title like '%" + cSearchText.Replace("'","''") + "%' ) )" +
//                  "    ) ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@SearchText", SqlDbType.VarChar).Value = cSearchText;
//              cmd.Parameters.Add("@FacilityId", SqlDbType.VarChar).Value = facilityid;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }





//      /// <summary>
//      /// Retrieves all topics for the specified search text (not obsolete)
//      /// NOTE: Looks in topicname, subtitle, objectives, course_number and authorname
//      /// </summary>
//      public override List<TopicInfo> GetTopicsBySearchText(string cSearchText, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              // add space on each side of text (this limits the results to just those words and not fragments)
//              if (String.IsNullOrEmpty(cSearchText))
//              {
//                  // no search text, so make up some that will return nothing
//                  cSearchText = "XZXZXZXZXZXZXZXZXZXZ";
//              }
//              else if(cSearchText.Contains("'"))
//              {
//                  cSearchText=cSearchText.Replace("'","").Trim();
//              }
             
//              else      
//              {
//                  cSearchText = cSearchText.Trim() ;
             
//              }
              
//              string cSQLCommand = "select " +
//                  "Topic.TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                  "uce_Ind, " +
//                  "shortname, " +
//                  "Topic.categoryid, " +
//                  "hold_ind " +
//                  "from Topic " +
//                  "where Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
//                  "and ( " +
//                  "    ( topic.topicname like '%" + cSearchText + "%' " +
//                  "      or topic.subtitle like '%" + cSearchText + "%' " +
//                  "      or topic.course_number like '%" + cSearchText + "' " +
//                  "      or topic.objectives like '%" + cSearchText + "%' ) " +
//                  "    or " +
//                  "    ( topic.topicid in ( select topicid from topicresourcelink join resourceperson " +
//                  "                         on topicresourcelink.resourceid = resourceperson.resourceid " +
//                  "                         where topicresourcelink.restype = 'A' and " +
//                  "                         resourceperson.fullname like '%" + cSearchText + "%' ) ) " +
//                  "    ) " ;

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@SearchText", SqlDbType.VarChar).Value = cSearchText;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

       
//       /// <summary>
//      /// Retrieves all topics in progress for the specified UserID (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetTopicsInProgressByUserID(int UserID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "Topic.TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//                "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                 "uce_Ind, " +
//                  "shortname, " +
//                  "Topic.categoryid, " +
//                  "hold_ind " +
//                  "from Topic " +
//                  "where Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
//                  "and Topic.TopicID IN " +
//                  "(select distinct TopicID from Test where UserID = @UserID " +
//                  "  and ( status = 'I' ) ) ";


//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      public override int IsPTOTTopic(int TopicId)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select topicid from categorylink where categoryid in (60,160,174) and topicid= @TopicId ";
//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd);
//              if (reader.Read())
//              {
//                  return 1;
//              }
//              else
//              {
//                  return 0;
//              }
//          }
          
//          }
       
//       ///// <summary>
//      ///// Retrieves all available topics for the specified CategoryID and username (not obsolete)
//      ///// NOTE: This blocks topics that are already in the user's portfolio as Incomplete, or are in there
//      /////    as complete with a completion date of at least 1 year ago
//      ///// </summary>
//      //public override List<TopicInfo> GetAvailableTopicsByCategoryIDAndUserName(int CategoryID, string UserName, string cSortExpression)
//      //{
//      //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//      //    {
//      //        string cSQLCommand = "select " +
//      //            "Topic.TopicID, " +
//      //            "TopicName, " +
//      //            "HTML, " +
//      //            "CorLecture, " +
//      //            "Fla_CEType, " +
//      //            "Fla_IDNo, " +
//      //            "LastUpdate, " +
//      //            "FolderName, " +
//      //            "NoSurvey, " +
//      //            "SurveyID, " +
//      //            "Compliance, " +
//      //            "MetaKW, " +
//      //            "MetaDesc, " +
//      //            "Hours, " +
//      //            "MediaType, " +
//      //            "Objectives, " +
//      //            "Content, " +
//      //            "Textbook, " +
//      //            "CertID, " +
//      //            "Method, " +
//      //            "GrantBy, " +
//      //            "DOCXFile, " +
//      //            "DOCXHoFile, " +
//      //            "Obsolete, " +
//      //            "FacilityID " +
//      //            "from Topic " +
//      //            "join categorylink on Topic.topicid = categorylink.topicid " +
//      //            "where categorylink.CategoryID = @CategoryID  and Topic.Obsolete = '0' " +
//      //            "and Topic.Active_Ind = '1' and Topic.TopicID NOT IN " +
//      //            "(select TopicID from Test where username = @UserName " +
//      //            "  and ( status = 'I' or (status='C' and lastmod < GetDate()-365) ) ) ";

//      //        // add on ORDER BY if provided
//      //        if (cSortExpression.Length > 0)
//      //        {
//      //            cSQLCommand = cSQLCommand +
//      //                " order by " + cSortExpression;
//      //        }

//      //        SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//      //        cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//      //        cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName.Trim();
//      //        cn.Open();
//      //        return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//      //    }
//      //}

//      /// <summary>
//      /// Retrieves all available topics for the specified CategoryID and username
//      /// NOTE: This blocks topics that are already in the user's portfolio as Incomplete, or are in there
//      ///    as complete with a completion date of at least 1 year ago
//      /// </summary>
//      public override List<TopicInfo> GetAvailableTopicsByCategoryIDAndUserID(int CategoryID, int UserID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "Topic.TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//              "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                   "uce_Ind, " +
//                  "shortname, " +
//                  "Topic.categoryid, " +
//                  "hold_ind " +
//                  "from Topic " +
//                  "join categorylink on Topic.topicid = categorylink.topicid " +
//                  "where categorylink.CategoryID = @CategoryID  and Topic.Obsolete = '0' " +
//                  "and Topic.Active_Ind = '1' and Topic.TopicID NOT IN " +
//                  "(select TopicID from Test where UserID = @UserID " +
//                  "  and ( status = 'I' or (status='C' and lastmod < GetDate()-365) ) ) ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all topics (not obsolete), plus category assignments for a CategoryID
//      /// </summary>
//       public override List<CheckBoxListTopicInfo> GetAllTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              //string cSQLCommand = "select TopicID, Topic.TopicName, " +
//              //    "IIF(ISNULL(cl.CategoryID), '0', '1') AS Selected from Topic co " +
//              //    "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
//              //    "and cl.CategoryID = ?";
//              string cSQLCommand = "select Topic.TopicID, Topic.TopicName, " +
//                  "CAST( (CASE WHEN (cl.CategoryID IS NULL) THEN '0' ELSE '1' END) AS bit ) AS Selected from Topic " +
//                  "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
//                  "and cl.CategoryID = @CategoryID where Topic.Obsolete = '0' and Topic.FacilityID=0 and Topic.Active_Ind = '1'  ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//              cn.Open();
//              return GetCheckBoxListTopicCollectionFromReader(ExecuteReader(cmd));
//          }
//      }

//       /// <summary>
//       /// Retrieves all non-textbook topics (not obsolete), plus category assignments for a CategoryID
//       /// </summary>
//       public override List<CheckBoxListTopicInfo> GetAllNonTextbookTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               //string cSQLCommand = "select TopicID, Topic.TopicName, " +
//               //    "IIF(ISNULL(cl.CategoryID), '0', '1') AS Selected from Topic co " +
//               //    "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
//               //    "and cl.CategoryID = ?";
//               string cSQLCommand = "select Topic.TopicID, Topic.TopicName, " +
//                   "CAST( (CASE WHEN (cl.CategoryID IS NULL) THEN '0' ELSE '1' END) AS bit ) AS Selected from Topic " +
//                   "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
//                   "and cl.CategoryID = @CategoryID where Topic.Textbook='0' and Topic.Obsolete = '0' and Topic.Active_Ind = '1' and topic.facilityid = " +
//                   " (select Categories.facilityid from categories where Categories.id = @CategoryID)";

//               // add on ORDER BY if provided
//               if (cSortExpression.Length > 0)
//               {
//                   cSQLCommand = cSQLCommand +
//                       " order by " + cSortExpression;
//               }

//               SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//               cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//               cn.Open();
//               return GetCheckBoxListTopicCollectionFromReader(ExecuteReader(cmd));
//           }
//       }

//       /// <summary>
//       /// Retrieves all textbook topics (not obsolete), plus category assignments for a CategoryID
//       /// </summary>
//       public override List<CheckBoxListTopicInfo> GetAllTextbookTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               //string cSQLCommand = "select TopicID, Topic.TopicName, " +
//               //    "IIF(ISNULL(cl.CategoryID), '0', '1') AS Selected from Topic co " +
//               //    "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
//               //    "and cl.CategoryID = ?";
//               string cSQLCommand = "select Topic.TopicID, Topic.TopicName, " +
//                   "CAST( (CASE WHEN (cl.CategoryID IS NULL) THEN '0' ELSE '1' END) AS bit ) AS Selected from Topic " +
//                   "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
//                   "and cl.CategoryID = @CategoryID where Topic.Textbook='1' and Topic.Obsolete = '0' and Topic.FacilityID=0 and Topic.Active_Ind = '1'  ";

//               // add on ORDER BY if provided
//               if (cSortExpression.Length > 0)
//               {
//                   cSQLCommand = cSQLCommand +
//                       " order by " + cSortExpression;
//               }

//               SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//               cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
//               cn.Open();
//               return GetCheckBoxListTopicCollectionFromReader(ExecuteReader(cmd));
//           }
//       }

       
//       /// <summary>
//      /// Retrieves all textbook topics that have NO categories assigned (not obsolete)
//      /// </summary>
//      public override List<TopicInfo> GetTextbookTopicsWithNoCategories(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//              "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                  "uce_Ind, " +
//                  "shortname, " +
//                  "Topic.categoryid, " +
//                  "hold_ind " +
//                  "from Topic " +
//                  "where Textbook='1' and Obsolete = '0' and FacilityID=0 and Topic.Active_Ind = '1' and TopicID not in " +
//                  "(select distinct TopicID from categorylink)";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all non-textbook topics (not obsolete) that have NO categories assigned
//      /// </summary>
//      public override List<TopicInfo> GetNonTextbookTopicsWithNoCategories(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "TopicName, " +
//                  "HTML, " +
//                  "CorLecture, " +
//                  "Fla_CEType, " +
//                  "Fla_IDNo, " +
//                  "LastUpdate, " +
//                  "FolderName, " +
//                  "NoSurvey, " +
//                  "SurveyID, " +
//                  "Compliance, " +
//                  "MetaKW, " +
//                  "MetaDesc, " +
//                  "Hours, " +
//                  "MediaType, " +
//                  "Objectives, " +
//                  "Content, " +
//                  "Textbook, " +
//                  "CertID, " +
//                  "Method, " +
//                  "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//              "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                  "uce_Ind, " +
//                  "shortname, " +
//                  "Topic.categoryid, " +
//                  "hold_ind " +
//                  "from Topic " +
//                  "where Textbook='0' and Obsolete = '0' and FacilityID=0 and Topic.Active_Ind = '1' and TopicID not in " +
//                  "(select distinct TopicID from categorylink)";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cn.Open();
//              return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//       /// Retrieves topics for the specified LectureID (LectureDefinition link)
//       /// </summary>
//       public override List<TopicInfo> GetTopicsByLectureID(int LectureID, string cSortExpression)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               string cSQLCommand = "select " +
//                   "Topic.TopicID, " +
//                   "TopicName, " +
//                   "HTML, " +
//                   "CorLecture, " +
//                   "Fla_CEType, " +
//                   "Fla_IDNo, " +
//                   "LastUpdate, " +
//                   "FolderName, " +
//                   "NoSurvey, " +
//                   "SurveyID, " +
//                   "Compliance, " +
//                   "MetaKW, " +
//                   "MetaDesc, " +
//                   "Hours, " +
//                   "MediaType, " +
//                   "Objectives, " +
//                   "Content, " +
//                   "Textbook, " +
//                   "CertID, " +
//                   "Method, " +
//                   "GrantBy, " +
//                  "DOCXFile, " +
//                  "DOCXHoFile, " +
//                  "Obsolete, " +
//                  "FacilityID, " +
//              "Course_Number, " +
//                  //"CeRef, " +
//                  //"Release_Date, " +
//                  "Minutes, " +
//                  "Audio_Ind, " +
//                  "Apn_Ind, " +
//                  "Icn_Ind, " +
//                  "Jcaho_Ind, " +
//                  "Magnet_Ind, " +
//                  "Active_Ind, " +
//                  "Video_Ind, " +
//                  "Online_Ind, " +
//                  "Ebp_Ind, " +
//                  "Ccm_Ind, " +
//                  "Avail_Ind, " +
//                  "Cert_Cerp, " +
//                  "Ref_Html, " +
//                  "AlterID, " +
//                  "Pass_Score, " +
//                  "Subtitle, " +
//                  //"Cost, " +
//                  "Topic_Type, " +
//                  "Img_Name, " +
//                  "Img_Credit, " +
//                  "Img_Caption, " +
//                  "Accreditation, " +
//                  "Views, " +
//                  "PrimaryViews, " +
//                  "PageReads, " +
//                   "Featured, " +
//                  "rev, " +
//                  "urlmark, " +
//                  "offline_cost, " +
//                  "online_cost, " +
//                  "notest, " +
//                 "uce_Ind, " +
//                  "shortname, " +
//                  "Topic.categoryid, " +
//                  "hold_ind " +
//                   "from Topic " +
//                   "join LectureTopicLink on Topic.topicid = LectureTopicLink.topicid " +
//                   "where LectureTopicLink.LectureID = @LectureID";

//               // add on ORDER BY if provided
//               if (cSortExpression.Length > 0)
//               {
//                   cSQLCommand = cSQLCommand +
//                       " order by " + cSortExpression;
//               }

//               SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//               cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID;
//               cn.Open();
//               return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
//           }
//       }

      
//       // TODO: Check this out!!!

//       /// <summary>
//      /// Returns the highest TopicID
//      /// (used by the front-end to find the last ID inserted because
//      /// all other methods via ObjectDataSource are failing)
//      /// </summary>
//      public override int GetHighestTopicID()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select top 1 cast(TopicID as Integer) as TopicID from Topic order by TopicID Desc", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

 
//       /// <summary>
//      /// Updates a Topic's HTML field
//      /// </summary>
//       public override bool UpdateTopicHTML(int TopicID, string HTML, string LastUpdate, string FolderName)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Topic set " +
//                  "HTML = @HTML, " +
//                  "LastUpdate = @LastUpdate, " +
//                  "FolderName = @FolderName " +
//                  "where TopicID = @TopicID", cn);

//              cmd.Parameters.Add("@HTML", SqlDbType.VarChar).Value = HTML;
//              cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = LastUpdate;
//              cmd.Parameters.Add("@FolderName", SqlDbType.VarChar).Value = FolderName;
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//       /// <summary>
//       /// Updates a Topic's HTML field
//       /// </summary>
//       public override bool UpdateTopicImage(int TopicID, string image)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("update Topic set " +
//                   "img_name = @image " +
//                   "where TopicID = @TopicID", cn);

//               cmd.Parameters.Add("@image", SqlDbType.VarChar).Value = image;
//               cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

//               cn.Open();
//               int ret = ExecuteNonQuery(cmd);
//               return (ret == 1);
//           }
//       }

//       /// <summary>
//       /// Updates a Textbook Topic's HTML field
//       /// </summary>
//       public override bool UpdateTextbookTopicHTML(int TopicID, string HTML, string LastUpdate, string FolderName,
//           string DOCXFile, string DOCXHoFile)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {
//               SqlCommand cmd = new SqlCommand("update Topic set " +
//                   "HTML = @HTML, " +
//                   "LastUpdate = @LastUpdate, " +
//                   "FolderName = @FolderName, " +
//                   "DOCXFile = @DOCXFile, " +
//                   "DOCXHoFile = @DOCXHoFile " +
//                   "where TopicID = @TopicID", cn);

//               cmd.Parameters.Add("@HTML", SqlDbType.VarChar).Value = HTML;
//               cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = LastUpdate;
//               cmd.Parameters.Add("@FolderName", SqlDbType.VarChar).Value = FolderName;
//               cmd.Parameters.Add("@DOCXFile", SqlDbType.VarChar).Value = DOCXFile;
//               cmd.Parameters.Add("@DOCXHoFile", SqlDbType.VarChar).Value = DOCXHoFile;
//               cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

//               cn.Open();
//               int ret = ExecuteNonQuery(cmd);
//               return (ret == 1);
//           }
//       }

//       /// <summary>
//      /// Updates a Topic's User-Editable fields (called from Topic Edit)
//      /// </summary>
//       public override bool UpdateTopicEditableFields(int TopicID, bool CorLecture,
//           string Fla_CEType, string Fla_IDNo, string LastUpdate, bool NoSurvey,
//           int SurveyID, bool Compliance, string MetaKW, string MetaDesc,
//           decimal Hours, string MediaType, string Objectives, string Content,
//           bool Textbook, int CertID, string Method, string GrantBy, bool Obsolete, 
//            string TopicName)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update Topic set " +
//                  "CorLecture = @CorLecture, " +
//                  "Fla_CEType = @Fla_CEType, " +
//                  "Fla_IDNo = @Fla_IDNo, " +
//                  "LastUpdate = @LastUpdate, " +
//                  "NoSurvey = @NoSurvey, " +
//                  "SurveyID = @SurveyID, " +
//                  "Compliance = @Compliance, " +
//                  "MetaKW = @MetaKW, " +
//                  "MetaDesc = @MetaDesc, " +
//                  "Hours = @Hours, " +
//                  "MediaType = @MediaType, " +
//                  "Objectives = @Objectives, " +
//                  "Content = @Content, " +
//                  "Textbook = @Textbook, " +
//                  "CertID = @CertID, " +
//                  "Method = @Method, " +
//                  "GrantBy = @GrantBy, " +
//                  "Obsolete = @Obsolete, " +
//                  "TopicName = @TopicName " + 
//                  "where TopicID = @TopicID", cn);

//              cmd.Parameters.Add("@CorLecture", SqlDbType.Bit).Value = CorLecture;
//              cmd.Parameters.Add("@Fla_CEType", SqlDbType.Char).Value = Fla_CEType;
//              cmd.Parameters.Add("@Fla_IDNo", SqlDbType.VarChar).Value = Fla_IDNo;
//              cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = LastUpdate;
//              cmd.Parameters.Add("@NoSurvey", SqlDbType.Bit).Value = NoSurvey;

//              // pass null to database if value is zero (nothing selected by user)
//              // this allows the database to have a NULL foreign key and still enforce relational integrity
//              if (SurveyID == 0)
//              {
//                  cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = System.DBNull.Value;
//              }
//              else
//              {
//                  cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = SurveyID;
//              }

//              cmd.Parameters.Add("@Compliance", SqlDbType.Bit).Value = Compliance;
//              cmd.Parameters.Add("@MetaKW", SqlDbType.VarChar).Value = MetaKW;
//              cmd.Parameters.Add("@MetaDesc", SqlDbType.VarChar).Value = MetaDesc;
//              cmd.Parameters.Add("@Hours", SqlDbType.Decimal).Value = Hours;
//              cmd.Parameters.Add("@MediaType", SqlDbType.VarChar).Value = MediaType;
//              cmd.Parameters.Add("@Objectives", SqlDbType.VarChar).Value = Objectives;
//              cmd.Parameters.Add("@Content", SqlDbType.VarChar).Value = Content;
//              cmd.Parameters.Add("@Textbook", SqlDbType.Bit).Value = Textbook;
//              cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = CertID;
//              cmd.Parameters.Add("@Method", SqlDbType.VarChar).Value = Method;
//              cmd.Parameters.Add("@GrantBy", SqlDbType.VarChar).Value = GrantBy;
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cmd.Parameters.Add("@Obsolete", SqlDbType.Bit).Value = Obsolete;
//              cmd.Parameters.Add("@TopicName", SqlDbType.VarChar).Value = TopicName;

//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with TopicComplianceLinks

//      /// <summary>
//      /// Returns the total number of TopicComplianceLinks
//      /// </summary>
//      public override int GetTopicComplianceLinkCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from TopicComplianceLink", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all TopicComplianceLinks
//      /// </summary>
//      public override List<TopicComplianceLinkInfo> GetTopicComplianceLinks(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from TopicComplianceLink";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicComplianceLinkCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all TopicComplianceLinks for a FacilityID
//      /// </summary>
//      public override List<TopicComplianceLinkInfo> GetTopicComplianceLinksByFacilityID(int FacilityID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from TopicComplianceLink where FacID=@FacilityID ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;

//              cn.Open();
//              return GetTopicComplianceLinkCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Retrieves all TopicComplianceLinks for a FacilityID
//      /// </summary>
//      public override List<TopicCollectionInfo> GetTopicCollectionsByTopicID(int TopicID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from TopicCollection where TopicID=@TopicID ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

//              cn.Open();
//              return GetTopicCollectionCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Retrieves all TopicComplianceLinks for a FacilityID
//      /// </summary>
//      public override DataSet GetTopicCollectionsDataSetByTopicID(int TopicID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlDataAdapter adpt = new SqlDataAdapter();
//              string cSQLCommand = "select tc.chapternumber,t.topicname,tc.chapterid,tc.topicid,tc.tcid from topiccollection tc join (select topicname,topicid from topic where topic.topicid in (select chapterid from topiccollection where topiccollection.topicid=@TopicID)) t on t.topicid=tc.chapterid where tc.topicid=@TopicID ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }
//              cn.Open();
//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cmd.CommandType=CommandType.Text;
//              adpt.SelectCommand = cmd;
//              DataSet ds = new DataSet();
//              adpt.Fill(ds);
//              return ds;
//          }
//      }

       
//       /// <summary>
//      /// Retrieves the TopicComplianceLink with the specified ID
//      /// </summary>
//      public override TopicComplianceLinkInfo GetTopicComplianceLinkByID(int TopCompID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from TopicComplianceLink where TopCompID=@TopCompID", cn);
//            cmd.Parameters.Add("@TopCompID", SqlDbType.Int).Value = TopCompID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetTopicComplianceLinkFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a TopicComplianceLink
//      /// </summary>
//      public override bool DeleteTopicComplianceLink(int TopCompID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from TopicComplianceLink where TopCompID=@TopCompID", cn);
//            cmd.Parameters.Add("@TopCompID", SqlDbType.Int).Value = TopCompID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      public override bool DeleteMediaContent(int mcid)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from mediacontent where mcid=@mcid", cn);
//              cmd.Parameters.Add("@mcid", SqlDbType.Int).Value = mcid;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new TopicComplianceLink
//      /// </summary>
//      public override int InsertTopicComplianceLink(TopicComplianceLinkInfo TopicComplianceLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into TopicComplianceLink " +
//            "(TopicID, " +
//            "FacID, " +
//            "ComplFile) " +
//            "VALUES (" +
//            "@TopicID, " +
//            "FacID, " +
//            "@ComplFile) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicComplianceLink.TopicID ;
//            cmd.Parameters.Add("@FacID", SqlDbType.Int).Value = TopicComplianceLink.FacID ;
//            cmd.Parameters.Add("@ComplFile", SqlDbType.VarChar).Value = TopicComplianceLink.ComplFile ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }


//      /// <summary>
//      /// Inserts a new mediacontent
//      /// </summary>
//      public override int InsertMediaContent(MediaContentInfo MediaContent)
//      {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into mediacontent " +
//            "(TopicID, " +
//            "mediatype, " +
//            "asseturl, " +
//            "description) " +
//            "VALUES (" +
//            "@TopicID, " +
//            "@mediatype, " +
//            "@asseturl, " +
//            "@description) SET @ID = SCOPE_IDENTITY()", cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = MediaContent.topicid;
//              //cmd.Parameters.Add("@mediatype", SqlDbType.Int).Value = MediaContent.mediatype;
//              cmd.Parameters.Add("@mediatype", SqlDbType.VarChar).Value = MediaContent.mediatype;
//              cmd.Parameters.Add("@asseturl", SqlDbType.VarChar).Value = MediaContent.asseturl;
//              cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = MediaContent.description;

//              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              IDParameter.Direction = ParameterDirection.Output;
//              cmd.Parameters.Add(IDParameter);

//              cn.Open();
//              cmd.ExecuteNonQuery();

//              int NewID = (int)IDParameter.Value;
//              return NewID;

//          }
//      }


//      public override bool DeleteTopicCollection(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from topiccollection where tcid=@TopicID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /// <summary>
//      /// Inserts a new mediacontent
//      /// </summary>
//      public override int InsertTopicCollection(TopicCollectionInfo TopicCollection)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into TopicCollection " +
//            "(TopicID, " +
//            "chapterid, " +
//            "chapternumber) " +
//            "VALUES (" +
//            "@TopicID, " +
//            "@chapterid, " +
//            "@chapternumber) SET @ID = SCOPE_IDENTITY()", cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicCollection.topicid;
//              cmd.Parameters.Add("@chapterid", SqlDbType.Int).Value = TopicCollection.chapterid;
//              cmd.Parameters.Add("@chapternumber", SqlDbType.Int).Value = TopicCollection.chapternumber;

//              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              IDParameter.Direction = ParameterDirection.Output;
//              cmd.Parameters.Add(IDParameter);

//              cn.Open();
//              cmd.ExecuteNonQuery();

//              int NewID = (int)IDParameter.Value;
//              return NewID;
            
//          }
//      }

//      public override bool UpdateTopicCollections(int chapterid, int chapternumber,int tcid)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update TopicCollection " +
//                  "set chapternumber = @chapternumber, " +
//                  "chapterid = @chapterid " +
//                  "where tcid = @tcid", cn);
//              cmd.Parameters.Add("@chapterid", SqlDbType.Int).Value = chapterid;
//              cmd.Parameters.Add("@chapternumber", SqlDbType.Int).Value = chapternumber;
//              cmd.Parameters.Add("@tcid", SqlDbType.Int).Value = tcid;
//              cn.Open();
//              int ret=cmd.ExecuteNonQuery();
//              return(ret == 1);

//          }
//      }


//      /// <summary>
//      /// Updates a TopicComplianceLink
//      /// </summary>
//      public override bool UpdateTopicComplianceLink(TopicComplianceLinkInfo TopicComplianceLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update TopicComplianceLink set " +
//            "TopicID = @TopicID, " +
//            "FacID = FacID, " +
//            "ComplFile = ? " +
//            "where TopCompID = @TopCompID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicComplianceLink.TopicID ;
//            cmd.Parameters.Add("@FacID", SqlDbType.Int).Value = TopicComplianceLink.FacID ;
//            cmd.Parameters.Add("@ComplFile", SqlDbType.VarChar).Value = TopicComplianceLink.ComplFile ;
//            cmd.Parameters.Add("@TopCompID", SqlDbType.Int).Value = TopicComplianceLink.TopCompID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      public override bool UpdateMediaContent(MediaContentInfo MediaContent)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update mediacontent set " +
//            "TopicID = @TopicID, " +
//            "mediatype = @mediatype, " +
//            "asseturl = @asseturl, " +
//            "description = @description " +
//            "where mcid = @mcid ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = MediaContent.topicid;
//              cmd.Parameters.Add("@mcid", SqlDbType.Int).Value = MediaContent.mcid;
//              cmd.Parameters.Add("@mediatype", SqlDbType.VarChar).Value = MediaContent.mediatype;
//              cmd.Parameters.Add("@asseturl", SqlDbType.VarChar).Value = MediaContent.asseturl;
//              cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = MediaContent.description;

//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with TopicResourceLinks

//      /// <summary>
//      /// Returns the total number of TopicResourceLinks
//      /// </summary>
//      public override int GetTopicResourceLinkCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from TopicResourceLink", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of TopicResourceLinks for a TopicID
//      /// </summary>
//      public override int GetTopicResourceLinkCountByTopicID(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from TopicResourceLink where TopicID=@TopicID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of TopicResourceLinks for a TopicID
//      /// </summary>
//      public override List<MediaContentInfo> GetMediaContentByTopicID(int TopicID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from mediacontent where TopicID=@TopicID";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

//              cn.Open();
//              return GetMediaContentCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all TopicResourceLinks
//      /// </summary>
//      public override List<TopicResourceLinkInfo> GetTopicResourceLinks(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from TopicResourceLink";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetTopicResourceLinkCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the TopicResourceLink with the specified ID
//      /// </summary>
//      public override TopicResourceLinkInfo GetTopicResourceLinkByID(int TopicResID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from TopicResourceLink where TopicResID=@TopicResID ", cn);
//            cmd.Parameters.Add("@TopicResID", SqlDbType.Int).Value = TopicResID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetTopicResourceLinkFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a TopicResourceLink
//      /// </summary>
//      public override bool DeleteTopicResourceLink(int TopicResID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from TopicResourceLink where TopicResID=@TopicResID", cn);
//            cmd.Parameters.Add("@TopicResID", SqlDbType.Int).Value = TopicResID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new TopicResourceLink
//      /// </summary>
//      public override int InsertTopicResourceLink(TopicResourceLinkInfo TopicResourceLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into TopicResourceLink " +
//            "(TopicID, " +
//            "ResourceID, " +
//            "ResType) " +
//            "VALUES (" +
//            "@TopicID, " +
//            "@ResourceID, " +
//            "@ResType) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicResourceLink.TopicID ;
//            cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = TopicResourceLink.ResourceID ;
//            cmd.Parameters.Add("@ResType", SqlDbType.Char).Value = TopicResourceLink.ResType;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a TopicResourceLink
//      /// </summary>
//      public override bool UpdateTopicResourceLink(TopicResourceLinkInfo TopicResourceLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update TopicResourceLink set " +
//            "TopicID = @TopicID, " +
//            "ResourceID = @ResourceID, " +
//            "ResType = @ResType " +
//            "where TopicResID = @TopicResID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicResourceLink.TopicID ;
//            cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = TopicResourceLink.ResourceID ;
//            cmd.Parameters.Add("@ResType", SqlDbType.Char).Value = TopicResourceLink.ResType;
//            cmd.Parameters.Add("@TopicResID", SqlDbType.Int).Value = TopicResourceLink.TopicResID;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//       public override bool UpdateResourcePersonAssignments(int TopicID, 
//           string ResourceIDAssignments, string ResourceType)
//       {
//           using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//           {

//               // first delete any assignments that are not in the list passed in
//               //              SqlCommand cmd = new SqlCommand(
//               //                  "DELETE FROM categorylink " +
//               //                  "where TopicID = ? and CategoryID NOT IN (?)", cn);

//               // NOTE; For now, just delete them all
//               SqlCommand cmd = new SqlCommand(
//                   "DELETE FROM TopicResourceLink " +
//                   "where TopicID = @TopicID " +
//                   "and ResType = @ResourceType ", cn);

//               cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//               cmd.Parameters.Add("@ResourceType", SqlDbType.VarChar).Value = ResourceType;
//               //              cmd.Parameters.Add("@CategoryIDAssignments", SqlDbType.VarChar).Value = CategoryIDAssignments;
//               cn.Open();
//               int ret = ExecuteNonQuery(cmd);
//               //              return (ret == 1);

//               if (ResourceIDAssignments.Trim().Length < 1)
//                   // no assignments, so nothing more to do
//                   return true;

//               // now step through all ResourceIDs in the list passed in and call
//               // this.InsertTopicResourceLink() for each, which will insert the assignment
//               // if it does not already exist
//               string[] ResourceIDs = ResourceIDAssignments.Split(',');

//               TopicResourceLinkInfo TopicResourceLink;

//               foreach (string cResourceID in ResourceIDs)
//               {
//                   TopicResourceLink = new TopicResourceLinkInfo(0, TopicID, Int32.Parse(cResourceID), ResourceType);
//                   this.InsertTopicResourceLink(TopicResourceLink);
//               }

//               return true;
//           }
//       }


//      /////////////////////////////////////////////////////////
//      // methods that work with UserInterestLinks

//      /// <summary>
//      /// Returns the total number of UserInterestLinks
//      /// </summary>
//      public override int GetUserInterestLinkCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from UserInterestLink", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all UserInterestLinks
//      /// </summary>
//      public override List<UserInterestLinkInfo> GetUserInterestLinks(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from UserInterestLink";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetUserInterestLinkCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the UserInterestLink with the specified ID
//      /// </summary>
//      public override UserInterestLinkInfo GetUserInterestLinkByID(int UserIntID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from UserInterestLink where UserIntID=@UserIntID", cn);
//            cmd.Parameters.Add("@UserIntID", SqlDbType.Int).Value = UserIntID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetUserInterestLinkFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a UserInterestLink
//      /// </summary>
//      public override bool DeleteUserInterestLink(int UserIntID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from UserInterestLink where UserIntID=@UserIntID", cn);
//            cmd.Parameters.Add("@UserIntID", SqlDbType.Int).Value = UserIntID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new UserInterestLink
//      /// </summary>
//      public override int InsertUserInterestLink(UserInterestLinkInfo UserInterestLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into UserInterestLink " +
//            "(UserID, " +
//            "InterestID) " +
//            "VALUES (" +
//            "@UserID, " +
//            "@InterestID) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserInterestLink.UserID ;
//            cmd.Parameters.Add("@InterestID", SqlDbType.Int).Value = UserInterestLink.InterestID ;

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a UserInterestLink
//      /// </summary>
//      public override bool UpdateUserInterestLink(UserInterestLinkInfo UserInterestLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update UserInterestLink set " +
//            "UserID = @UserID, " +
//            "InterestID = @InterestID " +
//            "where UserIntID = @UserIntID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserInterestLink.UserID ;
//            cmd.Parameters.Add("@InterestID", SqlDbType.Int).Value = UserInterestLink.InterestID ;
//            cmd.Parameters.Add("@UserIntID", SqlDbType.Int).Value = UserInterestLink.UserIntID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      /////////////////////////////////////////////////////////
//      // methods that work with UserLicenses

//      /// <summary>
//      /// Returns the total number of UserLicenses
//      /// </summary>
//      public override int GetUserLicenseCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from UserLicense", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all UserLicenses
//      /// </summary>
//      public override List<UserLicenseInfo> GetUserLicenses(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from UserLicense";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetUserLicenseCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves all UserLicenses for a UserID
//      /// </summary>
//      public override List<UserLicenseInfo> GetUserLicensesByUserID(int UserID, string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from UserLicense where UserID=@UserID ";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

//              cn.Open();
//              return GetUserLicenseCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }


//      /// <summary>
//      /// Retrieves  UserLicense for a UserID
//      /// </summary>
//      public override UserLicenseInfo GetUserLicenseByUserID(int UserID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from UserLicense where UserID=@UserID ";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

//              cn.Open();


//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetUserLicenseFromReader(reader, true);
//              else
//                  return null;
//           }
//      }



//      /// <summary>
//      /// Retrieves the UserLicense with the specified ID
//      /// </summary>
//      public override UserLicenseInfo GetUserLicenseByID(int LicenseID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from UserLicense where LicenseID=@LicenseID", cn);
//              cmd.Parameters.Add("@LicenseID", SqlDbType.Int).Value = LicenseID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetUserLicenseFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a UserLicense
//      /// </summary>
//      public override bool DeleteUserLicense(int LicenseID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from UserLicense where LicenseID=@LicenseID", cn);
//              cmd.Parameters.Add("@LicenseID", SqlDbType.Int).Value = LicenseID;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new UserLicense
//      /// </summary>
//      public override int InsertUserLicense(UserLicenseInfo UserLicense)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into UserLicense " +
//            "(UserID, " +
//            "License_Type_ID, " +
//            "State, " +
//            "License_Number, " +
//            "Issue_Date, " +
//            "Expire_Date, " +
//            "Report_To_State " +
//            ") " +
//            "VALUES (" +
//            "@UserID, " +
//            "@License_Type_ID, " +
//            "@State, " +
//            "@License_Number, " +
//            "@Issue_Date, " +
//            "@Expire_Date, " +
//            "@Report_To_State ) SET @ID = SCOPE_IDENTITY()", cn);

//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserLicense.UserID;
//              cmd.Parameters.Add("@License_Type_ID", SqlDbType.Int).Value = UserLicense.License_Type_ID;
//              cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = UserLicense.State;
//              cmd.Parameters.Add("@License_Number", SqlDbType.VarChar).Value = UserLicense.License_Number;

//              if (UserLicense.Issue_Date == null
//                  || UserLicense.Issue_Date.Trim().Length == 0)
//              {
//                  cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = DBNull.Value;
//              }
//              else
//              {
//                  // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
//                  try
//                  {
//                      DateTime dIssue_Date = Convert.ToDateTime(UserLicense.Issue_Date);
//                      cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = dIssue_Date;
//                  }
//                  catch
//                  {
//                      // something went wrong with convert, so store NULL instead
//                      cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = DBNull.Value;
//                  }
//              }

//              if (UserLicense.Expire_Date == null
//                  || UserLicense.Expire_Date.Trim().Length == 0)
//              {
//                  cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = DBNull.Value;
//              }
//              else
//              {
//                  // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
//                  try
//                  {
//                      DateTime dExpire_Date = Convert.ToDateTime(UserLicense.Expire_Date);
//                      cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = dExpire_Date;
//                  }
//                  catch
//                  {
//                      // something went wrong with convert, so store NULL instead
//                      cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = DBNull.Value;
//                  }
//              }

//              cmd.Parameters.Add("@Report_To_State", SqlDbType.Bit).Value = UserLicense.Report_To_State;

//              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              IDParameter.Direction = ParameterDirection.Output;
//              cmd.Parameters.Add(IDParameter);

//              cn.Open();
//              cmd.ExecuteNonQuery();

//              int NewID = (int)IDParameter.Value;
//              return NewID;

//          }
//      }

//      /// <summary>
//      /// Updates a UserLicense
//      /// </summary>
//      public override bool UpdateUserLicense(UserLicenseInfo UserLicense)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update UserLicense set " +
//            "UserID = @UserID, " +
//            "License_Type_ID = @License_Type_ID, " +
//            "State = @State, " +
//            "License_Number = @License_Number, " +
//            "Issue_Date = @Issue_Date, " +
//            "Expire_Date = @Expire_Date, " +
//            "Report_To_State = @Report_To_State " +
//            "where LicenseID = @LicenseID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserLicense.UserID;
//              cmd.Parameters.Add("@License_Type_ID", SqlDbType.Int).Value = UserLicense.License_Type_ID;
//              cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = UserLicense.State;
//              cmd.Parameters.Add("@License_Number", SqlDbType.VarChar).Value = UserLicense.License_Number;

//              if (UserLicense.Issue_Date == null
//                  || UserLicense.Issue_Date.Trim().Length == 0)
//              {
//                  cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = DBNull.Value;
//              }
//              else
//              {
//                  // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
//                  try
//                  {
//                      DateTime dIssue_Date = Convert.ToDateTime(UserLicense.Issue_Date);
//                      cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = dIssue_Date;
//                  }
//                  catch
//                  {
//                      // something went wrong with convert, so store NULL instead
//                      cmd.Parameters.Add("@Issue_Date", SqlDbType.DateTime).Value = DBNull.Value;
//                  }
//              }

//              if (UserLicense.Expire_Date == null
//                  || UserLicense.Expire_Date.Trim().Length == 0)
//              {
//                  cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = DBNull.Value;
//              }
//              else
//              {
//                  // Be sure we can't pass in a bad date or datetime string that will cause the convert to fail
//                  try
//                  {
//                      DateTime dExpire_Date = Convert.ToDateTime(UserLicense.Expire_Date);
//                      cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = dExpire_Date;
//                  }
//                  catch
//                  {
//                      // something went wrong with convert, so store NULL instead
//                      cmd.Parameters.Add("@Expire_Date", SqlDbType.DateTime).Value = DBNull.Value;
//                  }
//              }

//              cmd.Parameters.Add("@Report_To_State", SqlDbType.Bit).Value = UserLicense.Report_To_State;

//              cmd.Parameters.Add("@LicenseID", SqlDbType.Int).Value = UserLicense.LicenseID;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /////////////////////////////////////////////////////////
//      // methods that work with UsersInRoles

//      /// <summary>
//      /// Returns the total number of UsersInRoles
//      /// </summary>
//      public override int GetUsersInRolesCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from UsersInRoles", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all UsersInRoles
//      /// </summary>
//      public override List<UsersInRolesInfo> GetUsersInRoles(string cSortExpression)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select * " +
//                  "from UsersInRoles";

//              // add on ORDER BY if provided
//              if (cSortExpression.Length > 0)
//              {
//                  cSQLCommand = cSQLCommand +
//                      " order by " + cSortExpression;
//              }

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cn.Open();
//              return GetUsersInRolesCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Retrieves the UsersInRoles with the specified ID
//      /// </summary>
//      public override UsersInRolesInfo GetUsersInRolesByID(int UserRoleID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * " +
//                      "from UsersInRoles where UserRoleID=@UserRoleID", cn);
//            cmd.Parameters.Add("@UserRoleID", SqlDbType.Int).Value = UserRoleID ;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetUsersInRolesFromReader(reader, true);
//              else
//                  return null;
//          }
//      }

//      /// <summary>
//      /// Deletes a UsersInRoles
//      /// </summary>
//      public override bool DeleteUsersInRoles(int UserRoleID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from UsersInRoles where UserRoleID=@UserRoleID", cn);
//            cmd.Parameters.Add("@UserRoleID", SqlDbType.Int).Value = UserRoleID ;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Deletes a UsersInRoles by username
//      /// </summary>
//      public override bool DeleteUsersInRolesByUserName(string UserName)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from UsersInRoles where UserName=@UserName", cn);
//              cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName.ToLower();
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new UsersInRoles
//      /// </summary>
//      public override int InsertUsersInRoles(UsersInRolesInfo UsersInRoles)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into UsersInRoles " +
//            "(RoleName, " +
//            "UserName) " +
//            "VALUES (" +
//            "@RoleName, " +
//            "@UserName) SET @ID = SCOPE_IDENTITY()", cn);

//            cmd.Parameters.Add("@RoleName", SqlDbType.VarChar).Value = UsersInRoles.RoleName ;
//            cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UsersInRoles.UserName.ToLower();

//            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//            IDParameter.Direction = ParameterDirection.Output;
//            cmd.Parameters.Add(IDParameter);

//            cn.Open();
//            cmd.ExecuteNonQuery();

//            int NewID = (int)IDParameter.Value;
//            return NewID;  

//          }
//      }

//      /// <summary>
//      /// Updates a UsersInRoles
//      /// </summary>
//      public override bool UpdateUsersInRoles(UsersInRolesInfo UsersInRoles)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("update UsersInRoles set " +
//            "RoleName = @RoleName, " +
//            "UserName = @UserName " +
//            "where UserRoleID = @UserRoleID ", cn);
//              //            cmd.CommandType = CommandType.StoredProcedure;
//            cmd.Parameters.Add("@RoleName", SqlDbType.VarChar).Value = UsersInRoles.RoleName ;
//            cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UsersInRoles.UserName.ToLower();
//            cmd.Parameters.Add("@UserRoleID", SqlDbType.Int).Value = UsersInRoles.UserRoleID ;


//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }


//      ////////////////////////////////////////////////////
//      // ADDED AT THE BOTTOM FROM AddToVFP.txt////////////
//      ////////////////////////////////////////////////////

//      ///////////////////////////////////////////////////////////
//      // methods that work with TopicDetails

//      /// <summary>
//      /// Returns the total number of topics
//      /// </summary>
//      public override int GetTopicDetailCount()
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from coursedetail", cn);
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Returns the total number of TopicDetails for the specified TopicID
//      /// </summary>
//      public override int GetTopicDetailCountByTopicID(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("select Count(*) from coursedetail " +
//                  "where TopicID = @TopicID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return (int)ExecuteScalar(cmd);
//          }
//      }

//      /// <summary>
//      /// Retrieves all TopicDetails for the specified TopicID
//      /// </summary>
//      public override List<TopicDetailInfo> GetTopicDetailsByTopicID(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "TopicID, " +
//                  "Type, " +
//                  "Data, " +
//                  "CorrectAns, " +
//                  "Order, " +
//                  "Discussion " +
//                  "from coursedetail " +
//                  "where TopicID = @TopicID";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              return GetTopicDetailCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      /// <summary>
//      /// Deletes all TopicDetail records for a specific TopicID
//      /// </summary>
//      public override bool DeleteTopicDetailsByTopicID(int TopicID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from coursedetail where TopicID=@TopicID", cn);
//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      /// <summary>
//      /// Inserts a new TopicDetail
//      /// </summary>
//      public override bool InsertTopicDetail(TopicDetailInfo TopicDetail)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into coursedetail " +
//                  "(TopicID, " +
//                  "Type, " +
//                  "Data, " +
//                  "CorrectAns, " +
//                  "Order, " +
//                  "Discussion) " +
//                  "VALUES (" +
//                  "@TopicID, " +
//                  "@Type, " +
//                  "@Data, " +
//                  "@CorrectAns, " +
//                  "@Order, " +
//                  "@Discussion)", cn);

//              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicDetail.TopicID;
//              cmd.Parameters.Add("@Type", SqlDbType.VarChar).Value = TopicDetail.Type;
//              cmd.Parameters.Add("@Data", SqlDbType.VarChar).Value = TopicDetail.Data;
//              cmd.Parameters.Add("@CorrectAns", SqlDbType.VarChar).Value = TopicDetail.CorrectAns;
//              cmd.Parameters.Add("@Order", SqlDbType.VarChar).Value = TopicDetail.Order;
//              cmd.Parameters.Add("@Discussion", SqlDbType.Int).Value = TopicDetail.Discussion;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);

//          }
//      }

//      /// <summary>
//      /// Inserts a set of new TopicDetail records
//      /// </summary>
//      public override bool InsertTopicDetailsFromTopicDetailList(
//                  List<TopicDetailInfo> TopicDetails)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              int ret = 0;
//              cn.Open();

//              foreach (TopicDetailInfo record in TopicDetails)
//              {
//                  SqlCommand cmd = new SqlCommand(
//                    "insert into coursedetail " +
//                    "(TopicID, " +
//                    "Type, " +
//                    "Data, " +
//                    "CorrectAns, " +
//                    "Order, " +
//                    "Discussion) " +
//                    "VALUES (" +
//                    "@TopicID, " +
//                    "@Type, " +
//                    "@Data, " +
//                    "@CorrectAns, " +
//                    "@Order, " +
//                    "@Discussion)", cn);

//                  cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = record.TopicID;
//                  cmd.Parameters.Add("@Type", SqlDbType.VarChar).Value = record.Type;
//                  cmd.Parameters.Add("@Data", SqlDbType.VarChar).Value = record.Data;
//                  cmd.Parameters.Add("@CorrectAns", SqlDbType.VarChar).Value = record.CorrectAns;
//                  cmd.Parameters.Add("@Order", SqlDbType.Int).Value = record.Order;
//                  cmd.Parameters.Add("@Discussion", SqlDbType.VarChar).Value = record.Discussion;
//                  ret = ExecuteNonQuery(cmd);

//                  cmd = null;

//              }
//              return (ret == 1);

//          }

//      }

//      /// <summary>
//      /// Updates a UserAccount
//      /// </summary>
//      public override bool UpdateUserAccountDeptByuserID(int userID,int deptID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              if (deptID == 0) 
//             {
//                  SqlCommand cmd = new SqlCommand("update UserAccount set " +
//                      "Dept_ID = NULL " +
//                      "where iID = @userID ", cn);

//                  cmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;
//                  cn.Open();
//                  int ret = ExecuteNonQuery(cmd);
//                  return (ret == 1);
//              }
             

//              else 
//              {SqlCommand cmd = new SqlCommand("update UserAccount set " +
//                  "Dept_ID = @deptID " +
//                  "where iID = @userID ", cn);
//              cmd.Parameters.Add("@deptID", SqlDbType.Int).Value = deptID;
//                  cmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;
//                  cn.Open();
//                  int ret = ExecuteNonQuery(cmd);
//                  return (ret == 1);
//              }
              
            
//          }
//      }
//       ///////////////////////////////////
//       ///Methods that work with Nurse
//       /////////////////////////////////


//      /// <summary>
//      /// Inserts a new UserAccount
//      /// </summary>
//      public override int InsertNurse(string FirstName, string LastName, string UserName, string Email, string Password)
//      {
//          using (SqlConnection cn = new SqlConnection(this.RConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into UserAccount " +
//            "([UserName], " +
//            "[Password], " +
//            "[FirstName], " +
//            "[LastName], " +
//            "[DateEntered], " +
//            "[UCEend], " +
//            "[Disabled] ) " +
//            "VALUES (" +
//              "@username, " +
//              "@password, " +
//              "@firstname, " +
//              "@lastname, " +
//              "@dateenter, " +
//              "@uceend, " +
//              "@disabled) SET @ID = SCOPE_IDENTITY()", cn);
//              cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = UserName;
//              cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = Password;
//              cmd.Parameters.Add("@firstname", SqlDbType.Bit).Value = FirstName;
//              cmd.Parameters.Add("@lastname", SqlDbType.Bit).Value = LastName;
//              cmd.Parameters.Add("@dateenter", SqlDbType.DateTime).Value = System.DateTime.Now;
//              cmd.Parameters.Add("@uceend", SqlDbType.DateTime).Value = System.DateTime.Now;
//              cmd.Parameters.Add("@disabled", SqlDbType.Bit).Value = 0;

//              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              IDParameter.Direction = ParameterDirection.Output;
//              cmd.Parameters.Add(IDParameter);

//              cn.Open();
//              cmd.ExecuteNonQuery();

//              int NewID = (int)IDParameter.Value;
//              return NewID;
            
//          }
//      }

//      public override bool UpdateNurse(NurseInfo Nurse)
//      {
//          using (SqlConnection cn = new SqlConnection(this.RConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand(" update Nurse set " +
//          //     "rnID = @rnID, " +
//          "UserName = @UserName, " +
//          "Password = @Password, " +
//          "Registered = @Registered, " +
//          "FirstName = @FirstName, " +
//          "LastName = @LastName, " +
//          "Address1 = @Address1, " +
//          "Address2 = @Address2, " +
//          "City = @City, " +
//          "State = @State, " +
//          "Zip = @Zip, " +
//          "Country = @Country, " +
//          "S_Address1 = @SAddress1, " +
//          "S_Address2 = @SAddress2, " +
//          "S_City = @SCity, " +
//          "S_State = @SState, " +
//          "S_Zip = @SZip, " +
//          "S_Country = @SCountry, " +
//          "Email = @Email, " +
//                    "EmailPref = @EmailPref, " +
//                    "OptIn = @OptIn, " +
//                    "Offers = @Offers, " +
//                    "RNstate = @RNstate, " +
//                    "RNnum = @RNnum, " +
//                    "RNCode = @RNCode, " +
//                    "RNnumX = @RNnumX, " +
//                    "RNstate2 = @RNstate2, " +
//                    "RNnum2 = @RNnum2, " +
//                    "RNCode2 = @RNCode2, " +
//                    "RNnumX2 = @RNnumX2, " +
//                    "SpecialtyID = @SpecialtyID, " +
//                    "SpecialtyIDlist = @SpecialtyIDlist, " +
//                    "ImportedFrom = @ImportedFrom, " +
//                    "FieldID = @FieldID, " +
//                    "AgeGroup = @AgeGroup, " +
                   
//            "BrowserAgent = @BrowserAgent, " +
//          //"Phone = @Phone " +
//                     "PhoneType = @PhoneType, " +
//                     "Specialty = @Specialty, " +
//                     "Education = @Education, " +
//                     "Position = @Position, " +
//                     "DateEntered = @DateEntered, " +
//                     "UCEend = @UCEend, " +
//                     "Updated = @Updated, " +
//                     "LastLogin = @LastLogin, " +
//                     "IP = @IP, " +
//                     "Disabled = @Disabled " +
                  
//            "where rnID = @rnID ", cn);
//              cmd.Parameters.Add("@rnID", SqlDbType.Int).Value = Nurse.rnID;
//              cmd.Parameters.Add("UserName", SqlDbType.VarChar).Value = Nurse.UserName;
//              cmd.Parameters.Add("Password", SqlDbType.VarChar).Value = Nurse.Password;
            
//                  cmd.Parameters.Add("@Registered", SqlDbType.Bit).Value = Nurse.Registered;
            
//              cmd.Parameters.Add("FirstName", SqlDbType.VarChar).Value = Nurse.FirstName;
//              cmd.Parameters.Add("LastName", SqlDbType.VarChar).Value = Nurse.LastName;
//              cmd.Parameters.Add("Address1", SqlDbType.VarChar).Value = Nurse.Address1;
//              cmd.Parameters.Add("Address2", SqlDbType.VarChar).Value = Nurse.Address2;
//             cmd.Parameters.Add("State", SqlDbType.VarChar).Value = Nurse.State;
//              cmd.Parameters.Add("Zip", SqlDbType.VarChar).Value = Nurse.Zip;
//              cmd.Parameters.Add("Country", SqlDbType.VarChar).Value = Nurse.Country;
//              cmd.Parameters.Add("City", SqlDbType.VarChar).Value = Nurse.City;
//              cmd.Parameters.Add("SAddress1", SqlDbType.VarChar).Value = Nurse.SAddress1;
//              cmd.Parameters.Add("SAddress2", SqlDbType.VarChar).Value = Nurse.SAddress2;
//              cmd.Parameters.Add("SCity", SqlDbType.VarChar).Value = Nurse.SCity;
//              cmd.Parameters.Add("SState", SqlDbType.VarChar).Value = Nurse.SState;
//              cmd.Parameters.Add("SZip", SqlDbType.VarChar).Value = Nurse.SZip;
//              cmd.Parameters.Add("SCountry", SqlDbType.VarChar).Value = Nurse.SCountry;
//              cmd.Parameters.Add("Email", SqlDbType.VarChar).Value = Nurse.Email;
//              cmd.Parameters.Add("EmailPref", SqlDbType.VarChar).Value = Nurse.EmailPref;
             
//                  cmd.Parameters.Add("@OptIn", SqlDbType.Bit).Value = Nurse.OptIn;
              
             
//                  cmd.Parameters.Add("@Offers", SqlDbType.Bit).Value = Nurse.Offers;
             

//              cmd.Parameters.Add("RNstate", SqlDbType.VarChar).Value = Nurse.RNstate;
//              cmd.Parameters.Add("RNnum", SqlDbType.VarChar).Value = Nurse.RNnum;
//              cmd.Parameters.Add("RNCode", SqlDbType.VarChar).Value = Nurse.RNCode;
//              cmd.Parameters.Add("RNnumX", SqlDbType.VarChar).Value = Nurse.RNnumX;
//              cmd.Parameters.Add("RNstate2", SqlDbType.VarChar).Value = Nurse.RNstate2;
//              cmd.Parameters.Add("RNnum2", SqlDbType.VarChar).Value = Nurse.RNnum2;
//              cmd.Parameters.Add("RNCode2", SqlDbType.VarChar).Value = Nurse.RNCode2;
//              cmd.Parameters.Add("RNnumX2", SqlDbType.VarChar).Value = Nurse.RNnumX2;
//              cmd.Parameters.Add("SpecialtyID", SqlDbType.VarChar).Value = Nurse.SpecialtyID;
//              cmd.Parameters.Add("SpecialtyIDlist", SqlDbType.VarChar).Value = Nurse.SpecialtyIDlist;
//              cmd.Parameters.Add("ImportedFrom", SqlDbType.VarChar).Value = Nurse.ImportedFrom;
//              cmd.Parameters.Add("FieldID", SqlDbType.Int).Value = Nurse.FieldID;
//              cmd.Parameters.Add("AgeGroup", SqlDbType.Int).Value = Nurse.AgeGroup;
//              cmd.Parameters.Add("BrowserAgent", SqlDbType.VarChar).Value = Nurse.BrowserAgent;
//              cmd.Parameters.Add("Phone", SqlDbType.VarChar).Value = Nurse.Phone;
//              cmd.Parameters.Add("PhoneType", SqlDbType.VarChar).Value = Nurse.PhoneType;
//              cmd.Parameters.Add("Specialty", SqlDbType.VarChar).Value = Nurse.Specialty;
//              cmd.Parameters.Add("Education", SqlDbType.VarChar).Value = Nurse.Education;
//              cmd.Parameters.Add("Position", SqlDbType.VarChar).Value = Nurse.Position;
//              if (Nurse.DateEntered == System.DateTime.MinValue)
//              {
//                  cmd.Parameters.Add("@DateEntered", SqlDbType.DateTime).Value = System.DBNull.Value;
//              }
//              else
//              {
//                  cmd.Parameters.Add("@DateEntered", SqlDbType.DateTime).Value = Nurse.DateEntered;
//              }
//              if (Nurse.UCEend == System.DateTime.MinValue)
//              {
//                  cmd.Parameters.Add("@UCEend", SqlDbType.DateTime).Value = System.DBNull.Value;
//              }
//              else
//              {
//                  cmd.Parameters.Add("@UCEend", SqlDbType.DateTime).Value = Nurse.UCEend;
//              }
//              if (Nurse.Updated == System.DateTime.MinValue)
//              {
//                  cmd.Parameters.Add("@Updated", SqlDbType.DateTime).Value = System.DBNull.Value;
//              }
//              else
//              {
//                  cmd.Parameters.Add("@Updated", SqlDbType.DateTime).Value = Nurse.Updated;
//              }
//              if (Nurse.LastLogin == System.DateTime.MinValue)
//              {
//                  cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = System.DBNull.Value;
//              }
//              else
//              {
//                  cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = Nurse.LastLogin;
//              }
//              cmd.Parameters.Add("IP", SqlDbType.VarChar).Value = Nurse.IP;

             
//                  cmd.Parameters.Add("@Disabled", SqlDbType.Bit).Value = Nurse.Disabled;
             
//              try
//              {

//                  cn.Open();
//                int ret =(int) ExecuteNonQuery(cmd);
//                  return (ret >= 2);
//              }
//              catch
//              {
//              return false;
//              }
//              //return (ret == 1);
//          }



//      }

//      public override NurseInfo GetNurseByID(int rnID)
//      {
//          using (SqlConnection cn = new SqlConnection(this.RConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("Select * from Nurse where rnID=@rnID", cn);
//              cmd.Parameters.Add("@rnID", SqlDbType.Int).Value = rnID;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetNurseFromReader(reader, true);
//              else
//                  return null;
//          }
//      }



//       /////////////////////////////////////////////////////////
//       // methods that work with HistTranscript

//      public override List<HistTranscriptInfo> GetHistTranscriptRecords(string cSortExpression, int TranscriptID, int ItemID, int Score, DateTime DateComplete,
//            string FirstName, string LastName, string RNState, string RNNum, string RNCode, string RNState2, string RNNum2, string RNCode2, string CourseName,
//            string CourseNum, decimal Credits)
//      {
//          string qry = "";
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              qry = "select transcriptid,itemid,score,datecomplete,firstname,lastname,rnstate,rnnum,rncode,rnstate2,rnnum2,rncode2,coursename,coursenum,credits  from histtranscript where ";
//              if ((FirstName.Length > 0) & (FirstName != null))
//              {
//                  qry += " (FirstName Like '" + FirstName.Replace("'", "''") + "%') ";
//              }
//              if (ItemID > 0)
//              {
//                  qry += " and (ItemID Like '" + ItemID + "%') ";
//              }
//              if (Score> 0)
//              {
//                  qry += "and (Score Like '" + Score + "%') ";
//              }
//              if ((DateComplete > System.DateTime.MinValue) & (DateComplete != null))
//              {
//                  //qry += "and (DateComplete Like '" + DateComplete + "%') ";
//              }
//              if ((LastName.Length > 0) & (LastName != null))
//              {
//                  qry += "and (LastName Like '" + LastName.Replace("'", "''") + "%') ";
//              }
//              if ((RNState.Length > 0) & (RNState != null))
//              {
//                  qry += "and (RNState Like '" + RNState.ToLower().Replace("'", "''") + "%') ";
//              }
             
//              if ((RNCode.Length > 0) & (RNCode != null))
//              {
//                  qry += "and (RNCode Like '" + RNCode.Replace("'", "''") + "%') ";
//              }
//              if ((RNState2.Length > 0) & (RNState2 != null))
//              {
//                  qry += "and (RNState2 Like '" + RNState2.Replace("'", "''") + "%') ";
//              }
//              if ((RNNum.Length > 0) & (RNNum != null))
//              {
//                  qry += "and ((RNNum Like '" + RNNum.Replace("'", "''") + "%') or (RNNum2 Like '" + RNNum2.Replace("'", "''") + "%')) ";
//              }
//              //if ((RNNum2.Length > 0) & (RNNum2 != null))
//              //{
//              //    qry += " ";
//              //}

//              if ((RNCode2.Length > 0) & (RNCode2 != null))
//              {
//                  qry += "and (RNCode2 Like '" + RNCode2.Replace("'", "''") + "%') ";
//              }
//              if ((CourseName.Length > 0) & (CourseName != null))
//              {
//                  qry += "and (CourseName Like '" + CourseName.Replace("'", "''") + "%') ";
//              }

//              if ((CourseNum.Length > 0) & (CourseNum != null))
//              {
//                  qry += "and (CourseNum Like '" + CourseNum.Replace("'", "''") + "%') ";
//              }
//              if ((Credits > 0) & (Credits != null))
//              {
//                  qry += "and (Credits Like '" + Credits + "%') ";
//              }

//              qry += " order by lastname,firstname";
//              SqlCommand cmd = new SqlCommand(qry, cn);
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd);

//              List<HistTranscriptInfo> HistTranscripts = new List<HistTranscriptInfo>();
//              while (reader.Read())
//              {
//                  DateTime dt = Convert.ToDateTime(reader["DateComplete"].ToString());
//                  string strdate = dt.ToShortDateString();

//                  HistTranscriptInfo HistTranscript = new HistTranscriptInfo(
//             Convert.ToInt32(reader["TranscriptID"].ToString()),
//             Convert.ToInt32(reader["ItemID"].ToString()),
//             Convert.ToInt32(reader["Score"].ToString()),
//             Convert.ToDateTime(reader["DateComplete"].ToString()),
//             reader["FirstName"].ToString(),
//             reader["LastName"].ToString(),
//             reader["RNState"].ToString(),
//             reader["RNNum"].ToString(),
//             reader["RNCode"].ToString(),
//             reader["RNState2"].ToString(),
//             reader["RNNum2"].ToString(),
//             reader["RNCode2"].ToString(),
//             reader["CourseName"].ToString(),
//             reader["CourseNum"].ToString(),
//             Convert.ToDecimal(reader["Credits"].ToString()));
//                  HistTranscripts.Add(HistTranscript);
//              }
//              return HistTranscripts;
//          }
//      }


//      /// <summary>
//      /// Retrieves the Topic with the specified ID
//      /// </summary>
//      public override MetaInfo GetMetaByPath(string Path)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {

//              SqlCommand cmd = new SqlCommand("select * from Meta" +
//                                           " where path=@path", cn);
//              cmd.Parameters.Add("@path", SqlDbType.VarChar).Value = Path;
//              cn.Open();
//              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//              if (reader.Read())
//                  return GetMetaFromReader(reader, true);
//              else
//                  return null;
//          }
//      }



//      #region Methods that work with CategoryAreaLink

//      /// <summary>        
//      /// Retrieves all  Categoryarealink Info for the specified Category id
//      /// </summary>
//      public override List<CategoryAreaLinkInfo> GetCategoryAreaLinkByCategoryId(int Categoryid)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              string cSQLCommand = "select " +
//                  "Categoryid, " +
//                  "areaid " +
//                  "from CategoryAreaLink " +
//                  "where Categoryid = @CategoryID " +
//                  "order by areaid DESC";

//              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//              cmd.Parameters.Add("@CategoryID", SqlDbType.VarChar).Value = Categoryid;
//              cn.Open();
//              return GetCategoryAreaLinkCollectionFromReader(ExecuteReader(cmd), false);
//          }
//      }

//      public override int UpdateCategoryAreaLink(CategoryAreaLinkInfo CategoryAreaLink)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("insert into CategoryAreaLink " +
//                  "(Categoryid, " +
//                  "areaid )" +
//                  "VALUES (" +
//                  "@CategoryId, " +
//                  "@AreaId)", cn);
//              cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = CategoryAreaLink.categoryid;
//              cmd.Parameters.Add("@AreaId", SqlDbType.Int).Value = CategoryAreaLink.areaid;

//              //SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//              //IDParameter.Direction = ParameterDirection.Output;
//              //cmd.Parameters.Add(IDParameter);

//              foreach (IDataParameter param in cmd.Parameters)
//              {
//                  if (param.Value == null)
//                      param.Value = DBNull.Value;
//              }

//              cn.Open();
//              int ret = cmd.ExecuteNonQuery();
//              //int NewID = (int)IDParameter.Value;
//              return ret;
//          }
//          throw new NotImplementedException();
//      }

//      public override bool DeleteCategoryAreaLink(int Categoryid, int areaid)
//      {
//          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//          {
//              SqlCommand cmd = new SqlCommand("delete from Categoryarealink where Categoryid=@CategoryId And areaid=@AreaId", cn);
//              cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = Categoryid;
//              cmd.Parameters.Add("@AreaId", SqlDbType.Int).Value = areaid;
//              cn.Open();
//              int ret = ExecuteNonQuery(cmd);
//              return (ret == 1);
//          }
//      }

//      #endregion    


   }
}