﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region LectureTopicLinkInfo

namespace PearlsReview.DAL
{
    public class LectureTopicLinkInfo
    {
        public LectureTopicLinkInfo() { }

        public LectureTopicLinkInfo(int LectTopID, int LectureID, int TopicID, int Sequence)
        {
            this.LectTopID = LectTopID;
            this.LectureID = LectureID;
            this.TopicID = TopicID;
            this.Sequence = Sequence;
        }

        private int _LectTopID = 0;
        public int LectTopID
        {
            get { return _LectTopID; }
            protected set { _LectTopID = value; }
        }

        private int _LectureID = 0;
        public int LectureID
        {
            get { return _LectureID; }
            private set { _LectureID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }


        private int _Sequence = 0;
        public int Sequence
        {
            get { return _Sequence; }
            private set { _Sequence = value; }
        }


    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with LectureTopicLinks

        /// <summary>
        /// Returns the total number of LectureTopicLinks
        /// </summary>
        public int GetLectureTopicLinkCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from LectureTopicLink", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all LectureTopicLinks
        /// </summary>
        public List<LectureTopicLinkInfo> GetLectureTopicLinks(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from LectureTopicLink";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetLectureTopicLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all LectureTopicLinks for a LectureID
        /// </summary>
        public List<LectureTopicLinkInfo> GetLectureTopicLinksByLectureID(int LectureID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from LectureTopicLink where LectureID = @LectureID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID;

                cn.Open();
                return GetLectureTopicLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves the LectureTopicLink with the specified ID
        /// </summary>
        public LectureTopicLinkInfo GetLectureTopicLinkByID(int LectTopID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from LectureTopicLink where LectTopID=@LectTopID", cn);
                cmd.Parameters.Add("@LectTopID", SqlDbType.Int).Value = LectTopID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetLectureTopicLinkFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a LectureTopicLink
        /// </summary>
        public bool DeleteLectureTopicLink(int LectTopID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from LectureTopicLink where LectTopID=@LectTopID", cn);
                cmd.Parameters.Add("@LectTopID", SqlDbType.Int).Value = LectTopID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a LectureTopicLink
        /// </summary>
        public bool DeleteLectureTopicLinkByTopicAndLecID(int TopicID , int LectureID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from LectureTopicLink where TopicID=@TopicID ANd  LectureID = @LectureID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new LectureTopicLink
        /// </summary>
        public int InsertLectureTopicLink(LectureTopicLinkInfo LectureTopicLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_InsertLectureTopicLink", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureTopicLink.LectureID;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = LectureTopicLink.TopicID;
                //cmd.Parameters.Add("@Sequence", SqlDbType.Int).Value = LectureTopicLink.Sequence;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a LectureTopicLink
        /// </summary>
        public bool UpdateLectureTopicLink(LectureTopicLinkInfo LectureTopicLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update LectureTopicLink set " +
              "LectureID = @LectureID, " +
              "TopicID = @TopicID " +
              "Sequence = @Sequence " +
              "where LectTopID = @LectTopID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureTopicLink.LectureID;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = LectureTopicLink.TopicID;
                cmd.Parameters.Add("@Sequence", SqlDbType.Int).Value = LectureTopicLink.Sequence;
                cmd.Parameters.Add("@LectTopID", SqlDbType.Int).Value = LectureTopicLink.LectTopID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Assign topics to Lecture Definition from comma-delimited list
        /// </summary>
        public bool UpdateLectureTopicLinkAssignments(int LectureID, string TopicIDAssignments, int Sequence)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                // first delete any assignments that are not in the list passed in
                //              SqlCommand cmd = new SqlCommand(
                //                  "DELETE FROM categorylink " +
                //                  "where CategoryID = ? and TopicID NOT IN ("+TopicIDAssignments.Trim()+")", cn);

                // NOTE: For now, just delete them all
                SqlCommand cmd = new SqlCommand(
                    "DELETE FROM LectureTopicLink " +
                    "where LectureID = @LectureID and Sequence = @Sequence", cn);

                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID;
                cmd.Parameters.Add("@Sequence", SqlDbType.Int).Value = Sequence;

                //              cmd.Parameters.Add("@TopicIDAssignments", SqlDbType.VarChar).Value = TopicIDAssignments;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                //              return (ret == 1);

                if (TopicIDAssignments.Trim().Length < 1)
                    // no assignments, so nothing to do
                    return true;

                // now step through all TopicIDs in the list passed in and call
                // this.AssignTopicToCategory() for each, which will insert the assignment
                // if it does not already exist
                string[] TopicIDs = TopicIDAssignments.Split(',');

                LectureTopicLinkInfo LectureTopicLink;

                foreach (string cTopicID in TopicIDs)
                {
                    LectureTopicLink = new LectureTopicLinkInfo(0, LectureID, Int32.Parse(cTopicID), Sequence);
                    this.InsertLectureTopicLink(LectureTopicLink);
                }

                return true;
            }
        }

        #endregion

        #region PRProvider

        /////////////////////////////////////////////////////////
        // methods that work with LectureTopicLinks
 
        /// <summary>
        /// Returns a new LectureTopicLinkInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual LectureTopicLinkInfo GetLectureTopicLinkFromReader(IDataReader reader)
        {
            return GetLectureTopicLinkFromReader(reader, true);
        }
        protected virtual LectureTopicLinkInfo GetLectureTopicLinkFromReader(IDataReader reader, bool readMemos)
        {
            LectureTopicLinkInfo LectureTopicLink = new LectureTopicLinkInfo(
              (int)reader["LectTopID"],
              (int)reader["LectureID"],
              (int)reader["TopicID"],
              (int)reader["Sequence"]);


            return LectureTopicLink;
        }

        /// <summary>
        /// Returns a collection of LectureTopicLinkInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<LectureTopicLinkInfo> GetLectureTopicLinkCollectionFromReader(IDataReader reader)
        {
            return GetLectureTopicLinkCollectionFromReader(reader, true);
        }
        protected virtual List<LectureTopicLinkInfo> GetLectureTopicLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<LectureTopicLinkInfo> LectureTopicLinks = new List<LectureTopicLinkInfo>();
            while (reader.Read())
                LectureTopicLinks.Add(GetLectureTopicLinkFromReader(reader, readMemos));
            return LectureTopicLinks;
        }
        #endregion
    }
}
#endregion