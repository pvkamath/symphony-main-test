﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region SpecialtyInfo

namespace PearlsReview.DAL
{
    public class SpecialtyInfo
    {
        public SpecialtyInfo() { }

        public SpecialtyInfo(int SpecID, string SpecTitle)
        {
            this.SpecID = SpecID;
            this.SpecTitle = SpecTitle;
        }

        private int _SpecID = 0;
        public int SpecID
        {
            get { return _SpecID; }
            protected set { _SpecID = value; }
        }

        private string _SpecTitle = "";
        public string SpecTitle
        {
            get { return _SpecTitle; }
            private set { _SpecTitle = value; }
        }


    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Specialties

        /// <summary>
        /// Returns the total number of Specialties
        /// </summary>
        public  int GetSpecialtyCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Specialty", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Specialties
        /// </summary>
        public  List<SpecialtyInfo> GetSpecialties(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Specialty";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by SpecTitle";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetSpecialtyCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Specialty with the specified ID
        /// </summary>
        public  SpecialtyInfo GetSpecialtyByID(int SpecID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Specialty where SpecID=@SpecID", cn);
                cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = SpecID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSpecialtyFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a Specialty
        /// </summary>
        public  bool DeleteSpecialty(int SpecID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Specialty where SpecID=@SpecID", cn);
                cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = SpecID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Specialty
        /// </summary>
        public  int InsertSpecialty(SpecialtyInfo Specialty)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Specialty " +
              "(SpecTitle) " +
              "VALUES (" +
              "@SpecTitle) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@SpecTitle", SqlDbType.VarChar).Value = Specialty.SpecTitle;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Specialty
        /// </summary>
        public  bool UpdateSpecialty(SpecialtyInfo Specialty)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Specialty set " +
              "SpecTitle = @SpecTitle " +
              "where SpecID = @SpecID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SpecTitle", SqlDbType.VarChar).Value = Specialty.SpecTitle;
                cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = Specialty.SpecID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new SpecialtyInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual SpecialtyInfo GetSpecialtyFromReader(IDataReader reader)
        {
            return GetSpecialtyFromReader(reader, true);
        }
        protected virtual SpecialtyInfo GetSpecialtyFromReader(IDataReader reader, bool readMemos)
        {
            SpecialtyInfo Specialty = new SpecialtyInfo(
              (int)reader["SpecID"],
              reader["SpecTitle"].ToString());


            return Specialty;
        }

        /// <summary>
        /// Returns a collection of SpecialtyInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<SpecialtyInfo> GetSpecialtyCollectionFromReader(IDataReader reader)
        {
            return GetSpecialtyCollectionFromReader(reader, true);
        }
        protected virtual List<SpecialtyInfo> GetSpecialtyCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<SpecialtyInfo> Specialties = new List<SpecialtyInfo>();
            while (reader.Read())
                Specialties.Add(GetSpecialtyFromReader(reader, readMemos));
            return Specialties;
        }
        #endregion
    }
}
#endregion