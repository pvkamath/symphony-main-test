﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region TopicLicenseHourInfo


/// <summary>
/// Summary description for TopicLicenseHourInfo
/// </summary>
public class TopicLicenseHourInfo
{
    public TopicLicenseHourInfo()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public TopicLicenseHourInfo(int tlhid, int topicid, int Lpuid, decimal Th_hours, string Th_provider)
    {       
        this.topicid = topicid;
        this.tlhid = tlhid;
        this.Lpuid = Lpuid;
        this.Th_hours = Th_hours;
        this.Th_provider = Th_provider;        
    }

    private int _tlhid = 0;
    public int tlhid
    {
        get { return _tlhid; }
        protected set { _tlhid = value; }
    }

    private int _topicid = 0;
    public int topicid
    {
        get { return _topicid; }
        protected set { _topicid = value; }
    }

    private int _Lpuid = 0;
    public int Lpuid
    {
        get { return _Lpuid; }
        protected set { _Lpuid = value; }
    }

    private decimal _Th_hours=0.00M;    
    public decimal Th_hours
    {
        get { return _Th_hours; }
        protected set { _Th_hours = value; }
    }
    private string _Th_provider = "";
    public string Th_provider
    {
        get { return _Th_provider; }
        protected set { _Th_provider = value; }
    }   
}

#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>            
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /// <summary>
        /// Retrieves the Topic with the specified ID
        /// </summary>
        public TopicLicenseHourInfo GetTopicLicenseHourByID(int tlhid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from TopicLicenseHour where tlhid=@tlhid", cn);
                cmd.Parameters.Add("@tlhid", SqlDbType.Int).Value = tlhid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicLicenseHourFromReader(reader, true);
                else
                    return null;
            }
        }
        /// <summary>
        /// Get Topic License Hour By TopicID
        /// </summary>
        public List<TopicLicenseHourInfo> GetTopicLicenseHourByTopicID(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from TopicLicenseHour where TopicID=@TopicID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                cn.Open();
                return GetTopicLicenseHourCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public bool UpdateTopicLicenseHour(TopicLicenseHourInfo TopicLicenseHour)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update TopicLicenseHour set " +                  
                  "TopicID = @TopicID, " +
                  "Lpuid = @Lpuid, " +
                  "Th_hours = @Th_hours, " +
                  "Th_provider = @Th_provider " +
                  "where tlhid = @tlhid ", cn);

                cmd.Parameters.Add("@tlhid", SqlDbType.Int).Value = TopicLicenseHour.tlhid;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicLicenseHour.topicid;
                cmd.Parameters.Add("@Lpuid", SqlDbType.Int).Value = TopicLicenseHour.Lpuid;
                cmd.Parameters.Add("@Th_hours", SqlDbType.Decimal).Value = TopicLicenseHour.Th_hours;
                cmd.Parameters.Add("@Th_provider", SqlDbType.VarChar).Value = string.IsNullOrEmpty(TopicLicenseHour.Th_provider) ? "" : TopicLicenseHour.Th_provider;              

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new TopicLicenseHour
        /// </summary>
        public int InsertTopicLicenseHour(TopicLicenseHourInfo TopicLicenseHour)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into TopicLicenseHour " +
                  "(TopicID, " +
                  "Lpuid, " +
                  "Th_hours, " + 
                  "Th_provider) " +
                  "VALUES (" +
                  "@TopicID, " +
                  "@Lpuid, " +
                  "@Th_hours, " +              
                  "@Th_provider) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicLicenseHour.topicid;
                cmd.Parameters.Add("@Lpuid", SqlDbType.Int).Value = TopicLicenseHour.Lpuid; 
                cmd.Parameters.Add("@Th_hours", SqlDbType.Decimal).Value = TopicLicenseHour.Th_hours;
                cmd.Parameters.Add("@Th_provider", SqlDbType.VarChar).Value = string.IsNullOrEmpty(TopicLicenseHour.Th_provider) ? "" : TopicLicenseHour.Th_provider;              

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }
        public bool DeleteTopicLicenseHour(int tlhid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from TopicLicenseHour where tlhid=@tlhid", cn);
                cmd.Parameters.Add("@tlhid", SqlDbType.Int).Value = tlhid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public System.Data.DataSet GetLicenseTypesbyLicenseProfUnit()
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "select  " +
                     " md.index_title + ' - ' + lt.description + ' - ' +  lt.licensecode As Ldescription, lpu.*" +
                    " from LicenseType lt " +
                    " join LicenseProfUnit lpu on lt.license_type_id= lpu.lpu_license_type_id "+
                    " join MicrositeDomain md on lpu.lpu_msid=md.msid "+
                    "order by  Ldescription";

                SqlCommand cmd = new SqlCommand(qry, cn);

                //cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "LicenseTypes");
            }
            return ds;

        }

        public System.Data.DataSet GetTopicLicenseHourAndLicenseByTopicID(int TopicID, string cSortExpression)
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "SELECT * from TopicLicenseHour tlh   " +
                     " join LicenseProfUnit lpu  on tlh.lpuid=lpu.lpuid" +
                    " join LicenseType lt on lpu.lpu_license_type_id =lt.license_type_id " +
                    " where tlh.TopicID=@TopicID";

                if (cSortExpression.Length > 0)
                {
                    qry = qry +
                        " order by " + cSortExpression;
                }
                else
                {
                    qry = qry +
                        " order by Licensecode";
                }

                SqlCommand cmd = new SqlCommand(qry, cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;                

                //cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "LicenseTypes");
            }
            return ds;

        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a collection of TopicInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<TopicLicenseHourInfo> GetTopicLicenseHourCollectionFromReader(IDataReader reader)
        {
            return GetTopicLicenseHourCollectionFromReader(reader, true);
        }
        protected virtual List<TopicLicenseHourInfo> GetTopicLicenseHourCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TopicLicenseHourInfo> TopicLicenseHours = new List<TopicLicenseHourInfo>();
            while (reader.Read())
                TopicLicenseHours.Add(GetTopicLicenseHourFromReader(reader, readMemos));
            return TopicLicenseHours;
        }



        protected virtual TopicLicenseHourInfo GetTopicLicenseHourFromReader(IDataReader reader)
        {
            return GetTopicLicenseHourFromReader(reader, true);
        }
        protected virtual TopicLicenseHourInfo GetTopicLicenseHourFromReader(IDataReader reader, bool readMemos)
        {
            TopicLicenseHourInfo TopicLicenseHour = new TopicLicenseHourInfo(
                (int)reader["tlhid"],
                (int)reader["topicid"],
                (int)reader["Lpuid"],
                (decimal)reader["Th_hours"], 
              reader["Th_provider"].ToString());

            return TopicLicenseHour;
        }
        #endregion

    }
}
#endregion