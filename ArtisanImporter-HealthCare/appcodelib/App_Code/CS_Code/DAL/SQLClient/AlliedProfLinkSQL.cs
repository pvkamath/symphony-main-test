﻿
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

/// <summary>
/// Summary description for AlliedProfLinkSQL
/// </summary>

#region AlliedProfLinkInfo

namespace PearlsReview.DAL
{

    /// <summary> 
    /// Summary description for AlliedProfLinkInfo

    /// </summary>
    public class AlliedProfLinkInfo
    {

        public AlliedProfLinkInfo() { }


        public AlliedProfLinkInfo(int apl_id, int ad_id, int msid, string apl_comment)
        {
             this.Apl_id = apl_id;
            this.Ad_id = ad_id;
            this.Msid = msid;
            this.Apl_comment = apl_comment;
            
        }

       

             private int _apl_id = 0;
        public int Apl_id
        {
            get { return _apl_id; }
            protected set { _apl_id = value; }
        }

     

        private int _ad_id = 0;
        public int Ad_id
        {
            get { return _ad_id; }
            protected set { _ad_id = value; }
        }

        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }



        private string _apl_comment = "";
        public string Apl_comment
        {
            get { return _apl_comment; }
            protected set { _apl_comment = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

 namespace PearlsReview.DAL.SQLClient
    {
        public partial class SQL2PRProvider : DataAccess
        {
            #region SQLPRProvider

            /////////////////////////////////////////////////////////
            // Methods that work with AlliedProfLink


            /// <summary>
            /// Retrieves all AlliedProfLinks
            /// </summary>
            public List<AlliedProfLinkInfo> GetAlliedProfLink(string cSortExpression)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    string cSQLCommand;
                    cSQLCommand = "SELECT * from AlliedProfLink ";

                    // add on ORDER BY if provided
                    if (cSortExpression.Length > 0)
                    {
                        cSQLCommand = cSQLCommand +
                            " order by " + cSortExpression;
                    }

                    SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                    cn.Open();
                    return GetAlliedProfLinkCollectionFromReader(ExecuteReader(cmd), false);
                }
            }


            public  DataTable GetAlliedProfLinkTableByAdID(int Ad_id)
            {
                DataTable dt = new DataTable();
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("select a.*, m.domain_name as domain_name,m.index_title as index_title  " +
                            "from AlliedProfLink a,MicrositeDomain m  where a.msid=m.msid and ad_id=@ad_id", cn);

                    cmd.Parameters.Add("@ad_id", SqlDbType.Int).Value = Ad_id;

                    cn.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);
                }
                return dt;




            }

            public List<AlliedProfLinkInfo> GetAlliedProfLinksByAdID(int Ad_id)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                   
                    SqlCommand cmd = new SqlCommand("select * " +
                            "from AlliedProfLink where ad_id=@ad_id", cn);
                    cmd.Parameters.Add("@ad_id", SqlDbType.Int).Value = Ad_id;
                    cn.Open();
                    return GetAlliedProfLinkCollectionFromReader(ExecuteReader(cmd), false);
                }
            }


            //for admin site
            public List<AlliedProfLinkInfo> GetAlliedProfLinkAdmin(string cSortExpression)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    string cSQLCommand;
                    cSQLCommand = "SELECT * from AlliedProfLink ";

                    // add on ORDER BY if provided
                    if (cSortExpression.Length > 0)
                    {
                        cSQLCommand = cSQLCommand +
                            " order by " + cSortExpression;
                    }

                    SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                    cn.Open();
                    return GetAlliedProfLinkCollectionFromReader(ExecuteReader(cmd), false);
                }
            }


            /// <summary>
            /// Retrieves the AlliedProfLink with the specified ID
            /// </summary>
            public AlliedProfLinkInfo GetAlliedProfLinkByID(int Apl_id)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand("select * " +
                            "from AlliedProfLink where apl_id=@apl_id", cn);

                    cmd.Parameters.Add("@apl_id", SqlDbType.Int).Value = Apl_id;
                    cn.Open();
                    IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                    if (reader.Read())
                        return GetAlliedProfLinkFromReader(reader, true);
                    else
                        return null;
                }
            }


            public DataTable GetAlliedProfLinkTableByID(int Apl_id)
            {
                DataTable dt = new DataTable();
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("select a.*, m.domain_name as domain_name,f.facname as facilityname " +
                            "from AlliedProfLink a,MicrositeDomain m, FacilityGroup f  where a.msid=m.msid and a.ad_id=f.facid and apl_id=@apl_id", cn);

                    cmd.Parameters.Add("@apl_id", SqlDbType.Int).Value = Apl_id;

                    cn.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dt);
                }
                return dt;




            }



          //insert 

            public int InsertAlliedProfLink(AlliedProfLinkInfo AlliedProfLink)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("insert into AlliedProfLink " +
                      "(ad_id , " +
                      "msid,comment) VALUES (@ad_id, @msid,@comment)", cn);

                    cmd.Parameters.Add("@msid", SqlDbType.Int).Value = AlliedProfLink.Msid;
                    cmd.Parameters.Add("@comment", SqlDbType.Char).Value = AlliedProfLink.Ad_id;
                    cmd.Parameters.Add("@msid", SqlDbType.Int).Value = AlliedProfLink.Apl_comment;

                    SqlParameter IDParameter = new SqlParameter("@apl_id", SqlDbType.Int);
                    IDParameter.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(IDParameter);

                    foreach (IDataParameter param in cmd.Parameters)
                    {
                        if (param.Value == null)
                            param.Value = DBNull.Value;
                    }

                    cn.Open();
                    cmd.ExecuteNonQuery();

                    int NewID = (int)IDParameter.Value;
                    return NewID;

                }
            }
            //update
            public bool UpdateAlliedProfLinkLists(List<string> Professions, int Ad_id)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    cn.Open();
                    SqlTransaction transaction;
                    SqlCommand cmd = cn.CreateCommand();
                    transaction = cn.BeginTransaction();
                    cmd.Connection = cn;
                    cmd.Transaction = transaction;

                    try
                    {
                        //cmd.CommandText = "delete from AlliedProfLink where ad_id=@ad_id";
                        //cmd.Parameters.Add("@ad_id", SqlDbType.Int).Value = Ad_id;
                        //cmd.ExecuteNonQuery();

                        foreach (string Profession in Professions)
                        {
                            cmd.CommandText = "insert into AlliedProfLink (ad_id , msid) values (@ad_id,@msid)";
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@ad_id", SqlDbType.Int).Value = Ad_id;
                            cmd.Parameters.Add("@msid", SqlDbType.Int).Value = int.Parse(Profession);
                            cmd.ExecuteNonQuery();
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return false;
                    }
                    return true;
                }
            }






            /// <summary>
            /// Updates 
            /// </summary>
            public bool UpdateAlliedProfLink(AlliedProfLinkInfo AlliedProfLink)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    cn.Open();
                    SqlTransaction transaction;
                    SqlCommand cmd = cn.CreateCommand();
                    transaction = cn.BeginTransaction();
                    cmd.Transaction = transaction;
                    cmd.Connection = cn;
                    try
                    {
                        cmd.CommandText = "update AlliedProfLink set " +
                            
                            "ad_id =@ad_id, " +
                            "msid =@msid, " +
                            "apl_comment =@apl_comment "+
                            "where apl_id=@apl_id ";

                        
                        cmd.Parameters.Add("@apl_id", SqlDbType.Int).Value = AlliedProfLink.Apl_id;
                        cmd.Parameters.Add("@ad_id", SqlDbType.Int).Value = AlliedProfLink.Ad_id;
                        cmd.Parameters.Add("@msid", SqlDbType.Int).Value = AlliedProfLink.Msid;
                        cmd.Parameters.Add("@apl_comment", SqlDbType.VarChar).Value = AlliedProfLink.Apl_comment;
                        

                        foreach (IDataParameter param in cmd.Parameters)
                        {
                            if (param.Value == null)
                                param.Value = DBNull.Value;
                        }
                        ExecuteNonQuery(cmd);


                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        return false;
                    }
                    return true;
                }
            }
            /// <summary>
            /// Deletes
            /// </summary>
            public bool DeleteAlliedProfLink(int apl_id)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("delete from AlliedProfLink where apl_id=@apl_id", cn);
                    cmd.Parameters.Add("@apl_id", SqlDbType.Int).Value = apl_id;
                    cn.Open();
                    int ret = ExecuteNonQuery(cmd);
                    return (ret == 1);
                }
            }



            

            #endregion

            #region PRProvider
            /////////////////////////////////////////////////////////
            // methods that work with Discount  

            /// <summary>
            /// Returns a new DiscountInfo instance filled with the DataReader's current record data
            /// </summary>
            protected virtual AlliedProfLinkInfo GetAlliedProfLinkFromReader(IDataReader reader)
            {
                return GetAlliedProfLinkFromReader(reader, true);
            }
            protected virtual AlliedProfLinkInfo GetAlliedProfLinkFromReader(IDataReader reader, bool readMemos)
            {
                AlliedProfLinkInfo AlliedProfLink = new AlliedProfLinkInfo(
                       
                    
                       (int)reader["apl_id"],
                       (int)reader["ad_id"],
                       (int)reader["msid"],
                       reader["apl_comment"].ToString()
                      
                       );
                

                return AlliedProfLink;
            }


            /// <summary>
            /// Returns a collection of DiscountInfo objects with the data read from the input DataReader
            /// </summary>
            protected virtual List<AlliedProfLinkInfo> GetAlliedProfLinkCollectionFromReader(IDataReader reader)
            {
                return GetAlliedProfLinkCollectionFromReader(reader, true);
            }
            protected virtual List<AlliedProfLinkInfo> GetAlliedProfLinkCollectionFromReader(IDataReader reader, bool readMemos)
            {
                List<AlliedProfLinkInfo> AlliedProfLink = new List<AlliedProfLinkInfo>();
                while (reader.Read())
                    AlliedProfLink.Add(GetAlliedProfLinkFromReader(reader, readMemos));
                return AlliedProfLink;
            }

            #endregion

        }
    }
    #endregion

