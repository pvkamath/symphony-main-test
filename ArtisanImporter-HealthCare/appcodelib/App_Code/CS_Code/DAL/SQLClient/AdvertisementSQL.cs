﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region AdvertisementInfo

namespace PearlsReview.DAL
{
    public class AdvertisementInfo
    {
        public AdvertisementInfo() { }

        public AdvertisementInfo(int AdID, string AdName, string AdType, string AdDest, string AdText, string AdFilename, string AdUnitType)
        {
            this.AdID = AdID;
            this.AdName = AdName;
            this.AdType = AdType;
            this.AdDest = AdDest;
            this.AdText = AdText;
            this.AdFilename = AdFilename;
            this.AdUnitType = AdUnitType;
        }

        private int _AdID = 0;
        public int AdID
        {
            get { return _AdID; }
            protected set { _AdID = value; }
        }

        private string _AdName = "";
        public string AdName
        {
            get { return _AdName; }
            private set { _AdName = value; }
        }

        private string _AdType = "";
        public string AdType
        {
            get { return _AdType; }
            private set { _AdType = value; }
        }

        private string _AdDest = "";
        public string AdDest
        {
            get { return _AdDest; }
            private set { _AdDest = value; }
        }

        private string _AdText = "";
        public string AdText
        {
            get { return _AdText; }
            private set { _AdText = value; }
        }

        private string _AdFilename = "";
        public string AdFilename
        {
            get { return _AdFilename; }
            private set { _AdFilename = value; }
        }

        private string _AdUnitType = "";
        public string AdUnitType
        {
            get { return _AdUnitType; }
            private set { _AdUnitType = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Advertisements

        /// <summary>
        /// Returns the total number of Advertisements
        /// </summary>
        public  int GetAdvertisementCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Advertisement", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Advertisements
        /// </summary>
        public  List<AdvertisementInfo> GetAdvertisements(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Advertisement";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAdvertisementCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Advertisement with the specified ID
        /// </summary>
        public  AdvertisementInfo GetAdvertisementByID(int AdID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Advertisement where AdID=@AdID", cn);
                cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = AdID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetAdvertisementFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a Advertisement
        /// </summary>
        public  bool DeleteAdvertisement(int AdID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Advertisement where AdID=@AdID", cn);
                cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = AdID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Advertisement
        /// </summary>
        public  int InsertAdvertisement(AdvertisementInfo Advertisement)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Advertisement " +
              "(AdName, " +
              "AdType, " +
              "AdDest, " +
              "AdText, " +
              "AdFilename, " +
              "AdUnitType) " +
              "VALUES (" +
              "@AdName, " +
              "@AdType, " +
              "@AdDest, " +
              "@AdText, " +
              "@AdFilename, " +
              "@AdUnitType) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@AdName", SqlDbType.VarChar).Value = Advertisement.AdName;
                cmd.Parameters.Add("@AdType", SqlDbType.VarChar).Value = Advertisement.AdType;
                cmd.Parameters.Add("@AdDest", SqlDbType.VarChar).Value = Advertisement.AdDest;
                cmd.Parameters.Add("@AdText", SqlDbType.VarChar).Value = Advertisement.AdText;
                cmd.Parameters.Add("@AdFilename", SqlDbType.VarChar).Value = Advertisement.AdFilename;
                cmd.Parameters.Add("@AdUnitType", SqlDbType.VarChar).Value = Advertisement.AdUnitType;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Advertisement
        /// </summary>
        public  bool UpdateAdvertisement(AdvertisementInfo Advertisement)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Advertisement set " +
              "AdName = @AdName, " +
              "AdType = @AdType, " +
              "AdDest = @AdDest, " +
              "AdText = @AdText, " +
              "AdFilename = @AdFilename, " +
              "AdUnitType = @AdUnitType " +
              "where AdID = @AdID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@AdName", SqlDbType.VarChar).Value = Advertisement.AdName;
                cmd.Parameters.Add("@AdType", SqlDbType.VarChar).Value = Advertisement.AdType;
                cmd.Parameters.Add("@AdDest", SqlDbType.VarChar).Value = Advertisement.AdDest;
                cmd.Parameters.Add("@AdText", SqlDbType.VarChar).Value = Advertisement.AdText;
                cmd.Parameters.Add("@AdFilename", SqlDbType.VarChar).Value = Advertisement.AdFilename;
                cmd.Parameters.Add("@AdUnitType", SqlDbType.VarChar).Value = Advertisement.AdUnitType;
                cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = Advertisement.AdID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new AdvertisementInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual AdvertisementInfo GetAdvertisementFromReader(IDataReader reader)
        {
            return GetAdvertisementFromReader(reader, true);
        }
        protected virtual AdvertisementInfo GetAdvertisementFromReader(IDataReader reader, bool readMemos)
        {
            AdvertisementInfo Advertisement = new AdvertisementInfo(
              (int)reader["AdID"],
              reader["AdName"].ToString(),
              reader["AdType"].ToString(),
              reader["AdDest"].ToString(),
              reader["AdText"].ToString(),
              reader["AdFilename"].ToString(),
              reader["AdUnitType"].ToString());


            return Advertisement;
        }

        /// <summary>
        /// Returns a collection of AdvertisementInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<AdvertisementInfo> GetAdvertisementCollectionFromReader(IDataReader reader)
        {
            return GetAdvertisementCollectionFromReader(reader, true);
        }
        protected virtual List<AdvertisementInfo> GetAdvertisementCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<AdvertisementInfo> Advertisements = new List<AdvertisementInfo>();
            while (reader.Read())
                Advertisements.Add(GetAdvertisementFromReader(reader, readMemos));
            return Advertisements;
        }
        #endregion
    }
}
#endregion