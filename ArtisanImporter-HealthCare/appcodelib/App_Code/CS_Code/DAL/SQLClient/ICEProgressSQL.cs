﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region ICEProgressInfo

namespace PearlsReview.DAL
{
    public class ICEProgressInfo
    {
        public ICEProgressInfo(int ipid, int testid, int ip_topicid, DateTime? ip_lastmod, int? ip_score, string ip_status, bool ip_vignettecomplete, string ip_XMLTest, int ip_TestVers)
        {
            this.Ipid = ipid;
            this.Testid = testid;
            this.Ip_topicid = ip_topicid;
            this.Ip_lastmod = ip_lastmod;
            this.Ip_score = ip_score;
            this.Ip_status = ip_status;
            this.Ip_vignettecomplete = ip_vignettecomplete;
            this.Ip_XMLTest = ip_XMLTest;
            this.Ip_TestVers = ip_TestVers;
        }

        private int _ipid = 0;
        public int Ipid
        {
            get { return _ipid; }
            set { _ipid = value; }
        }
        private int _testid = 0;
        public int Testid
        {
            get { return _testid; }
            set { _testid = value; }
        }
        private int _ip_topicid = 0;
        public int Ip_topicid
        {
            get { return _ip_topicid; }
            set { _ip_topicid = value; }
        }
        private DateTime? _ip_lastmod;
        public DateTime? Ip_lastmod
        {
            get { return _ip_lastmod; }
            set { _ip_lastmod = value; }
        }
        private int? _ip_score = 0;
        public int? Ip_score
        {
            get { return _ip_score; }
            set { _ip_score = value; }
        }
        private string _ip_status = "";
        public string Ip_status
        {
            get { return _ip_status; }
            set { _ip_status = value; }
        }
        private bool _ip_vignettecomplete = false;
        public bool Ip_vignettecomplete
        {
            get { return _ip_vignettecomplete; }
            set { _ip_vignettecomplete = value; }
        }
        private string _ip_XMLTest = "";
        public string Ip_XMLTest
        {
            get { return _ip_XMLTest; }
            set { _ip_XMLTest = value; }
        }
        private int _ip_TestVers = 0;
        public int Ip_TestVers
        {
            get { return _ip_TestVers; }
            set { _ip_TestVers = value; }
        }
    }

}

#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        public int insertICEProgress(ICEProgressInfo iceprogress)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into ICEProgress " +
                    "(testid, " +
                    "ip_topicid, " +
                    "ip_lastmod, " +
                    "ip_score, " +
                    "ip_XMLTest, " +
                    "ip_TestVers, " +
                    "ip_status) "+
                     "VALUES " +
                    "(@testid, " +
                    "@ip_topicid, " +
                    "@ip_lastmod, " +
                    "@ip_score, " +
                    "ip_XMLTest, " +
                    "ip_TestVers, " +
                    "@ip_status) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@testid", SqlDbType.Int).Value = iceprogress.Testid;
                cmd.Parameters.Add("@ip_topicid", SqlDbType.Int).Value = iceprogress.Ip_topicid;
                cmd.Parameters.Add("@ip_lastmod", SqlDbType.DateTime).Value = iceprogress.Ip_lastmod;
                cmd.Parameters.Add("@ip_score", SqlDbType.Int).Value = iceprogress.Ip_score;
                cmd.Parameters.Add("@ip_status", SqlDbType.VarChar).Value = iceprogress.Ip_status;
                cmd.Parameters.Add("@ip_XMLTest", SqlDbType.VarChar).Value = iceprogress.Ip_XMLTest;
                cmd.Parameters.Add("@ip_TestVers", SqlDbType.VarChar).Value = iceprogress.Ip_TestVers;                

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                return (int)IDParameter.Value;
            }
        }

        public bool updateICEProgress(ICEProgressInfo iceprogress)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update ICEProgress set " +
                    "testid = @testid, " +
                    "ip_topicid = @ip_topicid, " +
                    "ip_lastmod = @ip_lastmod, " +
                    "ip_score = @ip_score, " +
                    "ip_status = @ip_status, " +
                    "ip_vignettecomplete = @ip_vignettecomplete, " +
                    "ip_XMLTest = @ip_XMLTest, " +
                    "ip_TestVers = @ip_TestVers " +    
                    "where ipid = @ipid ", cn);

                cmd.Parameters.Add("@testid", SqlDbType.Int).Value = iceprogress.Testid;
                cmd.Parameters.Add("@ip_topicid", SqlDbType.Int).Value = iceprogress.Ip_topicid;
                cmd.Parameters.Add("@ip_lastmod", SqlDbType.DateTime).Value = iceprogress.Ip_lastmod;
                cmd.Parameters.Add("@ip_score", SqlDbType.Int).Value = iceprogress.Ip_score;
                cmd.Parameters.Add("@ip_status", SqlDbType.VarChar).Value = iceprogress.Ip_status;
                cmd.Parameters.Add("@ip_vignettecomplete", SqlDbType.Bit).Value = iceprogress.Ip_vignettecomplete? 1:0;
                cmd.Parameters.Add("@ip_XMLTest", SqlDbType.VarChar).Value = iceprogress.Ip_XMLTest;
                cmd.Parameters.Add("@ip_TestVers", SqlDbType.Int).Value = iceprogress.Ip_TestVers;                
                cmd.Parameters.Add("@ipid", SqlDbType.Int).Value = iceprogress.Ipid;

                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public bool deleteICEProgress(int ipid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from ICEProgress where ipid = @ipid", cn);
                cmd.Parameters.Add("@ipid", SqlDbType.Int).Value = ipid;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public ICEProgressInfo GetICEProgressByID(int ipid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From ICEProgress Where ipid = @ipid", cn);
                cmd.Parameters.Add("@ipid", SqlDbType.Int).Value = ipid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetICEProgressFromReader(reader);
                else
                    return null;
            }
        }

        public DataSet GetICEProgressListByICESectionID(int isid, int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_get_iceprogress", cn);
                cmd.Parameters.Add("@isid", SqlDbType.Int).Value = isid;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.CommandType = CommandType.StoredProcedure;                
                DataSet ds = new DataSet();
                SqlDataAdapter dataadapter = new SqlDataAdapter(cmd);
                dataadapter.Fill(ds);
                return ds;
            }
        }

        public DataSet GetICEProgressListByICESectionIDP(int isid, int userid,int testid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_get_iceprogress_viewProgress", cn);
                cmd.Parameters.Add("@isid", SqlDbType.Int).Value = isid;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@testid", SqlDbType.Int).Value = testid;
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                SqlDataAdapter dataadapter = new SqlDataAdapter(cmd);
                dataadapter.Fill(ds);
                return ds;
            }
        }

        public bool EnrollICEProgressByTestId(int testid, bool reduceSeat)
        {
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_enroll_userbytestid", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@testid", SqlDbType.Int).Value = testid;
                    cmd.Parameters.Add("@reduceseat", SqlDbType.Char).Value = reduceSeat ? "Y" : "N";
                    cn.Open();
                    int ret = ExecuteNonQuery(cmd);
                    return (ret == 1);
                }
            }
        }

        /// <summary>
        /// Retrieves the completed ICEProgress by userid and topicid
        /// </summary>
        public ICEProgressInfo GetICEProgressByUserIDAndTopicID(int UserID, int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select top 1 * from iceprogress join test on iceprogress.testid=test.testid where ip_status='c'  " +
                        " and UserID= @UserID and ip_TopicId= @TopicID order by ip_lastmod desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@TopicId", SqlDbType.VarChar).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetICEProgressFromReader(reader, true);
                else
                    return null;
            }
        }

        public int GetICEProgressCompletionPercentage(int topicid, int userid)
        {
            int completionPercentage = 0;
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_get_icecompletionpercentage", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                    cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                    
                    SqlParameter outParameter = new SqlParameter("@cpercentage", SqlDbType.Int);
                    outParameter.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(outParameter);

                    cn.Open();
                    ExecuteNonQuery(cmd);

                    completionPercentage = Convert.ToInt32(outParameter.Value.ToString());
                }
            }
            return completionPercentage;
        }

        public int GetICEProgressCompletionPercentageP(int testid)
        {
            int completionPercentage = 0;
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_get_icecompletionpercentageP", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@testid", SqlDbType.Int).Value = testid;
                   
                    SqlParameter outParameter = new SqlParameter("@cpercentage", SqlDbType.Int);
                    outParameter.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(outParameter);

                    cn.Open();
                    ExecuteNonQuery(cmd);

                    completionPercentage = Convert.ToInt32(outParameter.Value.ToString());
                }
            }
            return completionPercentage;
        }
        #endregion


        #region PRProvider

        protected virtual ICEProgressInfo GetICEProgressFromReader(IDataReader reader)
        {
            return GetICEProgressFromReader(reader, true);
        }
        protected virtual ICEProgressInfo GetICEProgressFromReader(IDataReader reader, bool readMemos)
        {
                ICEProgressInfo ICEProgress = new ICEProgressInfo(
                Convert.IsDBNull(reader["ipid"]) ? (int)0 : (int)reader["ipid"],
                Convert.IsDBNull(reader["testid"]) ? (int)0 : (int)reader["testid"],
                Convert.IsDBNull(reader["ip_topicid"])? (int)0 : (int)reader["ip_topicid"],
                Convert.IsDBNull(reader["ip_lastmod"]) ? DateTime.MinValue : Convert.ToDateTime(reader["ip_lastmod"]),
                Convert.IsDBNull(reader["ip_score"]) ? (int)0 : (int)reader["ip_score"],
                Convert.ToString(reader["ip_status"].ToString()),
                (bool)reader["ip_vignettecomplete"],
                reader["ip_XMLTest"].ToString(),              
                (int)reader["ip_TestVers"]
                );
            return ICEProgress;
        }

        protected virtual List<ICEProgressInfo> GetICEProgressCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<ICEProgressInfo> ICEProgressList = new List<ICEProgressInfo>();
            while (reader.Read())
            {
                ICEProgressList.Add(GetICEProgressFromReader(reader, readMemos));
            }
            return ICEProgressList;
        }

        protected virtual List<ICEProgressInfo> GetICEProgressCollectionFromReader(IDataReader reader)
        {
            return GetICEProgressCollectionFromReader(reader, true);
        }

        #endregion

    }
}
#endregion
