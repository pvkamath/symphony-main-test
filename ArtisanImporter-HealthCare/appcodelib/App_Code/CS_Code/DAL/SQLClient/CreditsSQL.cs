﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region CreditsInfo

namespace PearlsReview.DAL
{
    public class CreditsInfo
    {
        public CreditsInfo() { }

        public CreditsInfo(int CreditID, int UserID, int CmsID, int HourLimit, DateTime? Expiredate, int CreditNumber)
        {
            this.CreditID = CreditID;
            this.UserID = UserID;
            this.CmsID = CmsID;
            this.HourLimit = HourLimit;
            this.Expiredate = Expiredate;
            this.CreditNumber = CreditNumber;
        }

        private int _creditID;
        public int CreditID
        {
            get { return _creditID; }
            protected set { _creditID = value; }
        }

        private int _userID;
        public int UserID
        {
            get { return _userID; }
            private set { _userID = value; }
        }

        private int _cmsID;
        public int CmsID
        {
            get { return _cmsID; }
            private set { _cmsID = value; }
        }

        private int _hourLimit ;
        public int HourLimit
        {
            get { return _hourLimit; }
            private set { _hourLimit = value; }
        }

        private DateTime? _expiredate = null;
        public DateTime? Expiredate
        {
            get { return _expiredate; }
            set { _expiredate = value; }
        }

        private int _creditNumber ;
        public int CreditNumber
        {
            get { return _creditNumber; }
            private set { _creditNumber = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Credits

        /// <summary>
        /// Returns the total number of Credits
        /// </summary>
        public int GetCreditsCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Credits", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Credits
        /// </summary>
        public List<CreditsInfo> GetCredits(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Credits";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCreditsCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves all Credits
        /// </summary>
        public List<CreditsInfo> GetCreditsByUserId(int UserId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Credits where creditnumber >0 and expiredate >= getdate() and UserID=@UserID order by cmsid,hourlimit";   
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserId;
                cn.Open();
                return GetCreditsCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Credits by UserID
        /// </summary>
        public List<CreditsInfo> GetAllCreditsByUserId(int UserId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select c.*, md.index_title " +
                    "from Credits c left join MicrositeDomain md  on c.cmsid=md.msid where UserID=@UserID order by ExpireDate, CreditNumber DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserId;
                cn.Open();
                return GetCreditsCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the MyCredit with specified ID
        /// </summary>
        public System.Data.DataSet GetMyCreditsById(int UserId)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet dsCredits = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "sp_get_mycreditbyid";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserId", SqlDbType.VarChar).Value = UserId;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dsCredits);
            }
            return dsCredits;
        }


        /// <summary>
        /// Retrieves the Cart with price with the specified userID
        /// </summary>
        public System.Data.DataSet GetCreditsCartByUserId(int UserId, int DomainID)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet dsCredits = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_credits_by_UserId";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
                cmd.Parameters.Add("@DomainID", SqlDbType.Int).Value = DomainID;    
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dsCredits);
            }
            return dsCredits;
        }

        /// <summary>
        /// Retrieves the Credits 
        /// </summary>
        public System.Data.DataSet GetCreditsByUserIDAndDomainIdAndHrs(int UserID, int DomainId, decimal  Hours,string DomainName,string ContactHrs)
        {           
              SqlDataAdapter adapter;
              System.Data.DataSet dsCredits = new DataSet();
              using (SqlConnection cn = new SqlConnection(this.ConnectionString))
              {
                  SqlCommand cmd = new SqlCommand();
                  cmd.Connection = cn;
                  cn.Open();
                  cmd.CommandText = "sp_get_credits_by_UserId_DomainId_Hrs";
                  cmd.CommandType=CommandType.StoredProcedure;
                  cmd.Parameters.Add("@UserId", SqlDbType.VarChar).Value = UserID;
                  cmd.Parameters.Add("@DomainId", SqlDbType.VarChar).Value = DomainId;
                  cmd.Parameters.Add("@Hours", SqlDbType.Decimal).Value = Hours;
                  cmd.Parameters.Add("@DomainName", SqlDbType.VarChar).Value = DomainName;
                  cmd.Parameters.Add("@ContactHrs", SqlDbType.VarChar).Value = ContactHrs; 
                  adapter = new SqlDataAdapter(cmd);
                  adapter.Fill(dsCredits);
              }
              return dsCredits;
        }
        /// <summary>
        ///
        /// </summary>
        public int GetCredtsCreditNumberByUserIDAndDomainIdAndHrs(int UserID, int DomainId, decimal Hours)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select creditnumber from credits where creditnumber > 0 and expiredate >= getdate() " +
                    " and userid=@UserID and hourlimit = @Hours and cmsid=@DomainId " +
                    " order by hourlimit";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserId", SqlDbType.VarChar).Value = UserID;
                cmd.Parameters.Add("@DomainId", SqlDbType.VarChar).Value = DomainId;
                cmd.Parameters.Add("@Hours", SqlDbType.Decimal).Value = Hours;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }
        /// <summary>
        /// Retrieves the Credits with the specified ID
        /// </summary>
        public CreditsInfo GetCreditsByID(int CreditID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                        "from Credits where CreditID=@CreditID", cn);
                cmd.Parameters.Add("@CreditID", SqlDbType.Int).Value = CreditID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCreditsFromReader(reader, true);
                else
                    return null;
            }
        }        
        /// <summary>
        /// Retrieves the Credits with the specified ID
        /// </summary>
        public CreditsInfo GetCreditsByCriteria(int CreditID, int UserID,int Cmsid, int HourLimit, DateTime? Expiredate)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd;
                if (Expiredate != null)
                    cmd = new SqlCommand("select top 1 * " +
                        "from Credits where CreditID <> @CreditID and UserID=@UserID and Cmsid=@Cmsid and HourLimit=@HourLimit and Expiredate=@Expiredate", cn);
                else
                    cmd = new SqlCommand("select top 1 * " +
                        "from Credits where CreditID <> @CreditID and UserID=@UserID and Cmsid=@Cmsid and HourLimit=@HourLimit and Expiredate is null", cn);
                cmd.Parameters.Add("@CreditID", SqlDbType.Int).Value = CreditID;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@Cmsid", SqlDbType.Int).Value = Cmsid;
                cmd.Parameters.Add("@HourLimit", SqlDbType.Int).Value = HourLimit;
                cmd.Parameters.Add("@Expiredate", SqlDbType.DateTime).Value =
                    (Expiredate == null ? (Object)DBNull.Value : (Object)Expiredate);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCreditsFromReader(reader, true);
                else
                    return null;
            }
        }
        /// <summary>
        /// Deletes a Credits
        /// </summary>
        public bool DeleteCredits(int CreditID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Credits set CreditNumber = 0 where CreditID=@CreditID", cn);
                cmd.Parameters.Add("@CreditID", SqlDbType.Int).Value = CreditID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Credits
        /// </summary>
        public int InsertCredits(CreditsInfo Credits)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Credits " +
              "(UserID, " +
              "CmsID, " +
              "HourLimit, " +
              "Expiredate, " +
              "CreditNumber) " +
              "VALUES (" +
              "@UserID, " +
              "@CmsID, " +
              "@HourLimit, " +
              "@Expiredate, " +
              "@CreditNumber) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Credits.UserID;
                cmd.Parameters.Add("@CmsID", SqlDbType.Int).Value = Credits.CmsID;
                cmd.Parameters.Add("@HourLimit", SqlDbType.Int).Value = Credits.HourLimit;
                cmd.Parameters.Add("@Expiredate", SqlDbType.DateTime).Value =
                    (Credits.Expiredate == null ? (Object)DBNull.Value : (Object)Credits.Expiredate);
                cmd.Parameters.Add("@CreditNumber", SqlDbType.Int).Value = Credits.CreditNumber;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Credits
        /// </summary>
        public bool UpdateCredits(CreditsInfo Credits)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Credits set " +
              "UserID = @UserID, " +
              "CmsID = @CmsID, " +
              "HourLimit = @HourLimit, " +
              "Expiredate = @Expiredate, " +
              "CreditNumber = @CreditNumber " +
              "where CreditID = @CreditID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CreditID", SqlDbType.Int).Value = Credits.CreditID;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Credits.UserID;
                cmd.Parameters.Add("@CmsID", SqlDbType.Int).Value = Credits.CmsID;                
                cmd.Parameters.Add("@HourLimit", SqlDbType.Int).Value = Credits.HourLimit;
                cmd.Parameters.Add("@Expiredate", SqlDbType.DateTime).Value =
                    (Credits.Expiredate == null ? (Object)DBNull.Value : (Object)Credits.Expiredate);
                cmd.Parameters.Add("@CreditNumber", SqlDbType.Int).Value = Credits.CreditNumber;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Transfer Credits
        /// </summary>
        public bool TransferCredits(int CreditID, string R_username, int CreditNumber)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_transfer_credit", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CreditID", SqlDbType.Int).Value = CreditID;
                cmd.Parameters.Add("@r_username", SqlDbType.VarChar).Value = R_username;
                cmd.Parameters.Add("@CreditNumber", SqlDbType.Int).Value = CreditNumber;
                cmd.Parameters.Add("@success", SqlDbType.Bit).Value = 0;
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters["@success"].Direction = ParameterDirection.InputOutput;
                cn.Open();
                ExecuteNonQuery(cmd);
                string res = cmd.Parameters["@success"].Value.ToString();
                return ( res== "True");
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new CreditsInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual CreditsInfo GetCreditsFromReader(IDataReader reader)
        {
            return GetCreditsFromReader(reader, true);
        }
        protected virtual CreditsInfo GetCreditsFromReader(IDataReader reader, bool readMemos)
        {
            CreditsInfo Credits = new CreditsInfo(
              (int)reader["CreditID"],
              (int)reader["UserID"],
              (DBNull.Value.Equals(reader["Cmsid"])) ? -1 : (int)reader["Cmsid"],
              (DBNull.Value.Equals(reader["HourLimit"])) ? -1 : (int)reader["HourLimit"],
              (Convert.IsDBNull(reader["Expiredate"]) ? null : (DateTime?)reader["Expiredate"]),
              (int)reader["CreditNumber"]);
            return Credits;
        }

        /// <summary>
        /// Returns a collection of CreditsInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<CreditsInfo> GetCreditsCollectionFromReader(IDataReader reader)
        {
            return GetCreditsCollectionFromReader(reader, true);
        }
        protected virtual List<CreditsInfo> GetCreditsCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<CreditsInfo> Credits = new List<CreditsInfo>();
            while (reader.Read())
                Credits.Add(GetCreditsFromReader(reader, readMemos));
            return Credits;
        }
        #endregion
    }
}
#endregion