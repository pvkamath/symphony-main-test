﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region TestDefinitionInfo

namespace PearlsReview.DAL
{
    public class TestDefinitionInfo
    {
        public TestDefinitionInfo() { }

        public TestDefinitionInfo(int TestDefID, int TopicID, string XMLTest, int Version)
        {
            this.TestDefID = TestDefID;
            this.TopicID = TopicID;
            this.XMLTest = XMLTest;
            this.Version = Version;
        }

        private int _TestDefID = 0;
        public int TestDefID
        {
            get { return _TestDefID; }
            protected set { _TestDefID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

        private string _XMLTest = "";
        public string XMLTest
        {
            get { return _XMLTest; }
            private set { _XMLTest = value; }
        }

        private int _Version = 0;
        public int Version
        {
            get { return _Version; }
            private set { _Version = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with TestDefinitions

        /// <summary>
        /// Returns the total number of TestDefinitions
        /// </summary>
        public  int GetTestDefinitionCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from TestDefinition", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all TestDefinitions
        /// </summary>
        public  List<TestDefinitionInfo> GetTestDefinitions(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from TestDefinition";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTestDefinitionCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the TestDefinition with the specified ID
        /// </summary>
        public  TestDefinitionInfo GetTestDefinitionByID(int TestDefID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from TestDefinition where TestDefID=@TestDefID", cn);
                cmd.Parameters.Add("@TestDefID", SqlDbType.Int).Value = TestDefID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestDefinitionFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the TestDefinition associated with the specified TopicID
        /// </summary>
        public  TestDefinitionInfo GetTestDefinitionByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from TestDefinition where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestDefinitionFromReader(reader, true);
                else
                    return null;
            }
        }



        /// <summary>
        /// Deletes a TestDefinition
        /// </summary>
        public  bool DeleteTestDefinition(int TestDefID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from TestDefinition where TestDefID=@TestDefID", cn);
                cmd.Parameters.Add("@TestDefID", SqlDbType.Int).Value = TestDefID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new TestDefinition
        /// </summary>
        public  int InsertTestDefinition(TestDefinitionInfo TestDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into TestDefinition " +
              "(TopicID, " +
              "XMLTest, " +
              "Version) " +
              "VALUES (" +
              "@TopicID, " +
              "@XMLTest, " +
              "@Version) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TestDefinition.TopicID;
                cmd.Parameters.Add("@XMLTest", SqlDbType.NVarChar).Value = TestDefinition.XMLTest;
                cmd.Parameters.Add("@Version", SqlDbType.Int).Value = TestDefinition.Version;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a TestDefinition
        /// </summary>
        public  bool UpdateTestDefinition(TestDefinitionInfo TestDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update TestDefinition set " +
              "TopicID = @TopicID, " +
              "XMLTest = @XMLTest, " +
              "Version = @Version " +
              "where TestDefID = @TestDefID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TestDefinition.TopicID;
                cmd.Parameters.Add("@XMLTest", SqlDbType.NVarChar).Value = TestDefinition.XMLTest;
                cmd.Parameters.Add("@Version", SqlDbType.Int).Value = TestDefinition.Version;
                cmd.Parameters.Add("@TestDefID", SqlDbType.Int).Value = TestDefinition.TestDefID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new TestDefinitionInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual TestDefinitionInfo GetTestDefinitionFromReader(IDataReader reader)
        {
            return GetTestDefinitionFromReader(reader, true);
        }
        protected virtual TestDefinitionInfo GetTestDefinitionFromReader(IDataReader reader, bool readMemos)
        {
            TestDefinitionInfo TestDefinition = new TestDefinitionInfo(
              (int)reader["TestDefID"],
              (int)reader["TopicID"],
              reader["XMLTest"].ToString(),
              (int)reader["Version"]);


            return TestDefinition;
        }

        /// <summary>
        /// Returns a collection of TestDefinitionInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<TestDefinitionInfo> GetTestDefinitionCollectionFromReader(IDataReader reader)
        {
            return GetTestDefinitionCollectionFromReader(reader, true);
        }
        protected virtual List<TestDefinitionInfo> GetTestDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TestDefinitionInfo> TestDefinitions = new List<TestDefinitionInfo>();
            while (reader.Read())
                TestDefinitions.Add(GetTestDefinitionFromReader(reader, readMemos));
            return TestDefinitions;
        }
        #endregion
    }
}
#endregion