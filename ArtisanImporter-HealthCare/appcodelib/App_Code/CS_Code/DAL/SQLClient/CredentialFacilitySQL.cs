﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

/// <summary>
/// Summary description for CredentialSQL
/// </summary>
/// 
#region CredentialFacilityInfo
namespace PearlsReview.DAL
{
    public class CredentialFacilityInfo
    {
        public CredentialFacilityInfo()
        {}

        public CredentialFacilityInfo(int CredentialID, string CredentialDescription, string CredentialCode,string CredentialURL)
        {
            this.CredentialID = CredentialID;
            this.CredentialDescription = CredentialDescription;
            this.CredentialCode = CredentialCode;
            this.CredentialURL = CredentialURL;
        }

        public int CredentialID { get; set; }
        public string  CredentialDescription { get; set; }
        public string CredentialCode { get; set; }
        public string CredentialURL { get; set; }
        
    }
}
#endregion

#region SQLPRProvider and PRProvider
namespace PearlsReview.DAL.SQLClient
{

    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        public int InsertCredential(CredentialFacilityInfo cinfo)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO CredentialFacility" +
                "(cre_desc,cre_code,cre_URL)" +
                "VALUES(@cre_desc,@cre_code,@cre_URL) SET @id=SCOPE_IDENTITY()", cn);
                cmd.Parameters.Add("@cre_desc", SqlDbType.VarChar).Value = cinfo.CredentialDescription;
                cmd.Parameters.Add("@cre_code", SqlDbType.VarChar).Value = cinfo.CredentialCode;
                cmd.Parameters.Add("@cre_URL", SqlDbType.VarChar).Value = cinfo.CredentialURL;

                SqlParameter IDParameter = new SqlParameter("@id", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;
            }
        }

        public bool UpdateCredential(CredentialFacilityInfo cinfo)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("UPDATE CredentialFacility SET " +
                    "cre_desc=@cre_desc," +
                    "cre_code=@cre_code," +
                    "cre_URL=@cre_URL " +
                    "WHERE cre_id=@cre_id", cn);

                cmd.Parameters.Add("@cre_desc", SqlDbType.VarChar).Value = cinfo.CredentialDescription;
                cmd.Parameters.Add("@cre_code", SqlDbType.VarChar).Value = cinfo.CredentialCode;
                cmd.Parameters.Add("@cre_URL", SqlDbType.VarChar).Value = cinfo.CredentialURL;
                cmd.Parameters.Add("@cre_id", SqlDbType.Int).Value = cinfo.CredentialID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public CredentialFacilityInfo GetCredentialByID(int cid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from CredentialFacility WHERE cre_id = @cid", cn);
                cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCredentialFromReader(reader, true);
                else
                    return null;
            }
        }


        public List<CredentialFacilityInfo> GetCredentials(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand = "SELECT * FROM CredentialFacility ";

              if (cSortExpression.Length > 0)
              {
                  cSQLCommand = cSQLCommand + "ORDER BY " + cSortExpression;
              }
              else
              {
                  cSQLCommand = cSQLCommand + "ORDER BY cre_desc";
              }

              SqlCommand cmd = new SqlCommand(cSQLCommand,cn);

              cn.Open();
              return GetCredentialCollectionFromReader(ExecuteReader(cmd),false);
          }
        }

        public bool DeleteCredential(int cre_id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("DELETE FROM CredentialFacility WHERE cre_id=@cre_id", cn);
                cmd.Parameters.Add("@cre_id", SqlDbType.Int).Value = cre_id;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret==1);
            }
        }
        #endregion

        #region PRProvider
        protected virtual CredentialFacilityInfo GetCredentialFromReader(IDataReader reader)
        {
            return GetCredentialFromReader(reader, true);
        }
        protected virtual CredentialFacilityInfo GetCredentialFromReader(IDataReader reader, bool readMemo)
        {
            CredentialFacilityInfo creinfo = new CredentialFacilityInfo((int)reader["cre_id"], reader["cre_desc"].ToString(), reader["cre_code"].ToString(), reader["cre_URL"].ToString());
            return creinfo;
        }

        protected virtual List<CredentialFacilityInfo> GetCredentialCollectionFromReader(IDataReader reader)
        {
            return GetCredentialCollectionFromReader(reader, true);
        }

        protected virtual List<CredentialFacilityInfo> GetCredentialCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<CredentialFacilityInfo> creinfo = new List<CredentialFacilityInfo>();
            while (reader.Read())
                creinfo.Add(GetCredentialFromReader(reader,readMemos));
            return creinfo;
        }

        #endregion
    }
}
#endregion
