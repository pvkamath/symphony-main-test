﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region NurseInfo

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for NurseInfo
    /// </summary>
    public class NurseInfo
    {
        public NurseInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public NurseInfo(int rnID, string UserName, string Password, bool Registered, string FirstName, string LastName,
          string Address1, string Address2, string City, string State, string Zip,
          string Country, string SAddress1, string SAddress2, string SCity, string SState,
          string SZip, string SCountry, string Email, string EmailPref, bool OptIn,
          bool Offers, string RNstate, string RNnum, string RNCode, string RNnumX,
          string RNstate2, string RNnum2, string RNCode2, string RNnumX2,
          int SpecialtyID, string SpecialtyIDlist, string ImportedFrom, int FieldID, int AgeGroup,
         string BrowserAgent, string Phone, string PhoneType, string Specialty, string Education,
          string Position, DateTime DateEntered, DateTime UCEend, DateTime Updated, DateTime LastLogin,
          string IP, bool Disabled)
        {
            this.rnID = rnID;
            this.UserName = UserName;
            this.Password = Password;
            this.Registered = Registered;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Address1 = Address1;
            this.Address2 = Address2;
            this.City = City;
            this.State = State;
            this.Zip = Zip;
            this.Country = Country;
            this.SAddress1 = SAddress1;
            this.SAddress2 = SAddress2;
            this.SCity = SCity;
            this.SState = SState;
            this.SZip = SZip;
            this.SCountry = SCountry;
            this.Email = Email;
            this.EmailPref = EmailPref;
            this.OptIn = OptIn;
            this.Offers = Offers;
            this.RNstate = RNstate;
            this.RNnum = RNnum;
            this.RNCode = RNCode;
            this.RNnumX = RNnumX;
            this.RNstate2 = RNstate2;
            this.RNnum2 = RNnum2;
            this.RNCode2 = RNCode2;
            this.RNnumX2 = RNnumX2;
            this.SpecialtyID = SpecialtyID;
            this.SpecialtyIDlist = SpecialtyIDlist;
            this.ImportedFrom = ImportedFrom;
            this.FieldID = FieldID;
            this.AgeGroup = AgeGroup;
            this.BrowserAgent = BrowserAgent;
            this.Phone = Phone;
            this.PhoneType = PhoneType;
            this.Specialty = Specialty;
            this.Education = Education;
            this.Position = Position;
            this.DateEntered = DateEntered;
            this.UCEend = UCEend;
            this.Updated = Updated;
            this.LastLogin = LastLogin;
            this.IP = IP;
            this.Disabled = Disabled;

        }


        private int _rnID = 0;
        public int rnID
        {
            get { return _rnID; }
            set { _rnID = value; }
        }

        private string _UserName = "";
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        private string _Password = "";
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        private bool _Registered = false;
        public bool Registered
        {
            get { return _Registered; }
            set { _Registered = value; }
        }

        private string _FirstName = "";
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        private string _LastName = "";
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        private string _Address1 = "";
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }
        private string _Address2 = "";
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }
        private string _City = "";
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }
        private string _State = "";
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }
        private string _Zip = "";
        public string Zip
        {
            get { return _Zip; }
            set { _Zip = value; }
        }
        private string _Country = "";
        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }
        private string _SAddress1 = "";
        public string SAddress1
        {
            get { return _SAddress1; }
            set { _SAddress1 = value; }
        }
        private string _SAddress2 = "";
        public string SAddress2
        {
            get { return _SAddress2; }
            set { _SAddress2 = value; }
        }
        private string _SCity = "";
        public string SCity
        {
            get { return _SCity; }
            set { _SCity = value; }
        }
        private string _SState = "";
        public string SState
        {
            get { return _SState; }
            set { _SState = value; }
        }
        private string _SZip = "";
        public string SZip
        {
            get { return _SZip; }
            set { _SZip = value; }
        }
        private string _SCountry = "";
        public string SCountry
        {
            get { return _SCountry; }
            set { _SCountry = value; }
        }
        private string _Email = "";
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        private string _EmailPref = "";
        public string EmailPref
        {
            get { return _EmailPref; }
            set { _EmailPref = value; }
        }
        private bool _OptIn = false;
        public bool OptIn
        {
            get { return _OptIn; }
            set { _OptIn = value; }
        }
        private bool _Offers = false;
        public bool Offers
        {
            get { return _Offers; }
            set { _Offers = value; }
        }
        private string _RNstate = "";
        public string RNstate
        {
            get { return _RNstate; }
            set { _RNstate = value; }
        }
        private string _RNnum = "";
        public string RNnum
        {
            get { return _RNnum; }
            set { _RNnum = value; }
        }
        private string _RNCode = "";
        public string RNCode
        {
            get { return _RNCode; }
            set { _RNCode = value; }
        }
        private string _RNnumX = "";
        public string RNnumX
        {
            get { return _RNnumX; }
            set { _RNnumX = value; }
        }
        private string _RNstate2 = "";
        public string RNstate2
        {
            get { return _RNstate2; }
            set { _RNstate2 = value; }
        }
        private string _RNnum2 = "";
        public string RNnum2
        {
            get { return _RNnum2; }
            set { _RNnum2 = value; }
        }
        private string _RNCode2 = "";
        public string RNCode2
        {
            get { return _RNCode2; }
            set { _RNCode2 = value; }
        }
        private string _RNnumX2 = "";
        public string RNnumX2
        {
            get { return _RNnumX2; }
            set { _RNnumX2 = value; }
        }
        private int _SpecialtyID = 0;
        public int SpecialtyID
        {
            get { return _SpecialtyID; }
            set { _SpecialtyID = value; }
        }
        private string _SpecialtyIDlist = "";
        public string SpecialtyIDlist
        {
            get { return _SpecialtyIDlist; }
            set { _SpecialtyIDlist = value; }
        }
        private string _ImportedFrom = "";
        public string ImportedFrom
        {
            get { return _ImportedFrom; }
            set { _ImportedFrom = value; }
        }
        private int _FieldID = 0;
        public int FieldID
        {
            get { return _FieldID; }
            set { _FieldID = value; }
        }
        private int _AgeGroup = 0;
        public int AgeGroup
        {
            get { return _AgeGroup; }
            set { _AgeGroup = value; }
        }
        private string _BrowserAgent = "";
        public string BrowserAgent
        {
            get { return _BrowserAgent; }
            set { _BrowserAgent = value; }
        }
        private string _Phone = "";
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }
        private string _PhoneType = "";
        public string PhoneType
        {
            get { return _PhoneType; }
            set { _PhoneType = value; }
        }
        private string _Specialty = "";
        public string Specialty
        {
            get { return _Specialty; }
            set { _Specialty = value; }
        }
        private string _Education = "";
        public string Education
        {
            get { return _Education; }
            set { _Education = value; }
        }
        private string _Position = "";
        public string Position
        {
            get { return _Position; }
            set { _Position = value; }
        }
        private DateTime _DateEntered = System.DateTime.Now;
        public DateTime DateEntered
        {
            get { return _DateEntered; }
            set { _DateEntered = value; }
        }
        private DateTime _UCEend = System.DateTime.Now;
        public DateTime UCEend
        {
            get { return _UCEend; }
            set { _UCEend = value; }
        }
        private DateTime _Updated = System.DateTime.Now;
        public DateTime Updated
        {
            get { return _Updated; }
            set { _Updated = value; }
        }
        private DateTime _LastLogin = System.DateTime.Now;
        public DateTime LastLogin
        {
            get { return _LastLogin; }
            set { _LastLogin = value; }
        }
        private string _IP = "";
        public string IP
        {
            get { return _IP; }
            set { _IP = value; }
        }
        private bool _Disabled = false;
        public bool Disabled
        {
            get { return _Disabled; }
            set { _Disabled = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        ///////////////////////////////////
        ///Methods that work with Nurse
        /////////////////////////////////


        /// <summary>
        /// Inserts a new UserAccount
        /// </summary>
        public  int InsertNurse(string FirstName, string LastName, string UserName, string Email, string Password)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into UserAccount " +
              "([UserName], " +
              "[Password], " +
              "[FirstName], " +
              "[LastName], " +
              "[DateEntered], " +
              "[UCEend], " +
              "[Disabled] ) " +
              "VALUES (" +
                "@username, " +
                "@password, " +
                "@firstname, " +
                "@lastname, " +
                "@dateenter, " +
                "@uceend, " +
                "@disabled) SET @ID = SCOPE_IDENTITY()", cn);
                cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = UserName;
                cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = Password;
                cmd.Parameters.Add("@firstname", SqlDbType.Bit).Value = FirstName;
                cmd.Parameters.Add("@lastname", SqlDbType.Bit).Value = LastName;
                cmd.Parameters.Add("@dateenter", SqlDbType.DateTime).Value = System.DateTime.Now;
                cmd.Parameters.Add("@uceend", SqlDbType.DateTime).Value = System.DateTime.Now;
                cmd.Parameters.Add("@disabled", SqlDbType.Bit).Value = 0;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        public  bool UpdateNurse(NurseInfo Nurse)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand(" update Nurse set " +
                    //     "rnID = @rnID, " +
            "UserName = @UserName, " +
            "Password = @Password, " +
            "Registered = @Registered, " +
            "FirstName = @FirstName, " +
            "LastName = @LastName, " +
            "Address1 = @Address1, " +
            "Address2 = @Address2, " +
            "City = @City, " +
            "State = @State, " +
            "Zip = @Zip, " +
            "Country = @Country, " +
            "S_Address1 = @SAddress1, " +
            "S_Address2 = @SAddress2, " +
            "S_City = @SCity, " +
            "S_State = @SState, " +
            "S_Zip = @SZip, " +
            "S_Country = @SCountry, " +
            "Email = @Email, " +
                      "EmailPref = @EmailPref, " +
                      "OptIn = @OptIn, " +
                      "Offers = @Offers, " +
                      "RNstate = @RNstate, " +
                      "RNnum = @RNnum, " +
                      "RNCode = @RNCode, " +
                      "RNnumX = @RNnumX, " +
                      "RNstate2 = @RNstate2, " +
                      "RNnum2 = @RNnum2, " +
                      "RNCode2 = @RNCode2, " +
                      "RNnumX2 = @RNnumX2, " +
                      "SpecialtyID = @SpecialtyID, " +
                      "SpecialtyIDlist = @SpecialtyIDlist, " +
                      "ImportedFrom = @ImportedFrom, " +
                      "FieldID = @FieldID, " +
                      "AgeGroup = @AgeGroup, " +

                       "BrowserAgent = @BrowserAgent, " +
                       "Phone = @Phone, " +
                       "PhoneType = @PhoneType, " +
                       "Specialty = @Specialty, " +
                       "Education = @Education, " +
                       "Position = @Position, " +
                       "DateEntered = @DateEntered, " +
                       "UCEend = @UCEend, " +
                       "Updated = @Updated, " +
                       "LastLogin = @LastLogin, " +
                       "IP = @IP, " +
                       "Disabled = @Disabled " +

              "where rnID = @rnID ", cn);
                cmd.Parameters.Add("@rnID", SqlDbType.Int).Value = Nurse.rnID;
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Nurse.UserName;
                cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = Nurse.Password;
                if (Nurse.Registered == false)
                {
                    cmd.Parameters.Add("@Registered", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@Registered", SqlDbType.Bit).Value = Nurse.Registered;
                }
                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = Nurse.FirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = Nurse.LastName;
                cmd.Parameters.Add("@Address1", SqlDbType.VarChar).Value = Nurse.Address1;
                cmd.Parameters.Add("@Address2", SqlDbType.VarChar).Value = Nurse.Address2;
                cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = Nurse.City;
                cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = Nurse.State;
                cmd.Parameters.Add("@Zip", SqlDbType.VarChar).Value = Nurse.Zip;
                cmd.Parameters.Add("@Country", SqlDbType.VarChar).Value = Nurse.Country;
                cmd.Parameters.Add("@SAddress1", SqlDbType.VarChar).Value = Nurse.SAddress1;
                cmd.Parameters.Add("@SAddress2", SqlDbType.VarChar).Value = Nurse.SAddress2;
                cmd.Parameters.Add("@SCity", SqlDbType.VarChar).Value = Nurse.SCity;
                cmd.Parameters.Add("@SState", SqlDbType.VarChar).Value = Nurse.SState;
                cmd.Parameters.Add("@SZip", SqlDbType.VarChar).Value = Nurse.SZip;
                cmd.Parameters.Add("@SCountry", SqlDbType.VarChar).Value = Nurse.SCountry;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = Nurse.Email;
                cmd.Parameters.Add("@EmailPref", SqlDbType.VarChar).Value = Nurse.EmailPref;
                if (Nurse.OptIn == false)
                {
                    cmd.Parameters.Add("@OptIn", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@OptIn", SqlDbType.Bit).Value = Nurse.OptIn;
                }
                if (Nurse.Offers == false)
                {
                    cmd.Parameters.Add("@Offers", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@Offers", SqlDbType.Bit).Value = Nurse.Offers;
                }

                cmd.Parameters.Add("@RNstate", SqlDbType.VarChar).Value = Nurse.RNstate;
                cmd.Parameters.Add("@RNnum", SqlDbType.VarChar).Value = Nurse.RNnum;
                cmd.Parameters.Add("@RNCode", SqlDbType.VarChar).Value = Nurse.RNCode;
                cmd.Parameters.Add("@RNnumX", SqlDbType.VarChar).Value = Nurse.RNnumX;
                cmd.Parameters.Add("@RNstate2", SqlDbType.VarChar).Value = Nurse.RNstate2;
                cmd.Parameters.Add("@RNnum2", SqlDbType.VarChar).Value = Nurse.RNnum2;
                cmd.Parameters.Add("@RNCode2", SqlDbType.VarChar).Value = Nurse.RNCode2;
                cmd.Parameters.Add("@RNnumX2", SqlDbType.VarChar).Value = Nurse.RNnumX2;
                cmd.Parameters.Add("@SpecialtyID", SqlDbType.VarChar).Value = Nurse.SpecialtyID;
                cmd.Parameters.Add("@SpecialtyIDlist", SqlDbType.VarChar).Value = Nurse.SpecialtyIDlist;
                cmd.Parameters.Add("@ImportedFrom", SqlDbType.VarChar).Value = Nurse.ImportedFrom;
                cmd.Parameters.Add("@FieldID", SqlDbType.Int).Value = Nurse.FieldID;
                cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = Nurse.AgeGroup;
                cmd.Parameters.Add("@BrowserAgent", SqlDbType.VarChar).Value = Nurse.BrowserAgent;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = Nurse.Phone;
                cmd.Parameters.Add("@PhoneType", SqlDbType.VarChar).Value = Nurse.PhoneType;
                cmd.Parameters.Add("@Specialty", SqlDbType.VarChar).Value = Nurse.Specialty;
                cmd.Parameters.Add("@Education", SqlDbType.VarChar).Value = Nurse.Education;
                cmd.Parameters.Add("@Position", SqlDbType.VarChar).Value = Nurse.Position;
                if (Nurse.DateEntered == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@DateEntered", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@DateEntered", SqlDbType.DateTime).Value = Nurse.DateEntered;
                }
                if (Nurse.UCEend == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@UCEend", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@UCEend", SqlDbType.DateTime).Value = Nurse.UCEend;
                }
                if (Nurse.Updated == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Updated", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Updated", SqlDbType.DateTime).Value = Nurse.Updated;
                }
                if (Nurse.LastLogin == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = Nurse.LastLogin;
                }
                cmd.Parameters.Add("@IP", SqlDbType.VarChar).Value = Nurse.IP;

                if (Nurse.Disabled == false)
                {
                    cmd.Parameters.Add("@Disabled", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@Disabled", SqlDbType.Bit).Value = Nurse.Disabled;
                }
                try
                {

                    cn.Open();
                    int ret = (int)ExecuteNonQuery(cmd);
                    return (ret >= 1);
                }
                catch 
                {
                    return false;
                }
                //return (ret == 1);
            }



        }

        public  int InsertNurse(NurseInfo nurse)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand(" INSERT INTO Nurse (UserName, Password, Registered, FirstName, LastName, Address1, Address2, City, " +
               "State, Zip, Country, S_Address1, S_Address2, S_City, S_State, S_Zip, S_Country, Email, EmailPref, OptIn, Offers, RNstate, RNnum, " +
               "RNCode, RNnumX, RNstate2, RNnum2, RNCode2, RNnumX2, SpecialtyID, SpecialtyIDlist, ImportedFrom, FieldID, AgeGroup, BrowserAgent, " +
               "Phone, PhoneType, Specialty, Education, Position, DateEntered, UCEend, Updated, LastLogin, IP, Disabled )" +
               " VALUES (@userName, @password, @registered, @firstName, @lastName, @address1, @address2, @city, " +
               "@state, @zip, @country, @sAddress1, @sAddress2, @sCity, @sState, @sZip, @sCountry, @email, @emailPref, @optIn, @offers, @rNstate, @rNnum, " +
               "@rNCode, @rNnumX, @rNstate2, @rNnum2, @rNCode2, @rNnumX2, @specialtyID, @specialtyIDlist, @importedFrom, @fieldID, @ageGroup, @browserAgent, " +
               "@phone, @phoneType, @specialty, @education, @position, @dateEntered, @uCEend, @updated, @lastLogin, @iP, @disabled ) SET @retrnID = SCOPE_IDENTITY()", cn);
                // (
                //   "where rnID = @rnID ", cn);
                //    cmd.Parameters.Add("@rnID", SqlDbType.Int).Value = nurse.rnID;
                cmd.Parameters.Add("@userName", SqlDbType.VarChar).Value = nurse.UserName;
                cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = nurse.Password;
                if (nurse.Registered == false)
                {
                    cmd.Parameters.Add("@registered", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@registered", SqlDbType.Bit).Value = nurse.Registered;
                }
                cmd.Parameters.Add("@firstName", SqlDbType.VarChar).Value = nurse.FirstName;
                cmd.Parameters.Add("@lastName", SqlDbType.VarChar).Value = nurse.LastName;
                cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = nurse.Address1;
                cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = nurse.Address2;
                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = nurse.City;
                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = nurse.State;
                cmd.Parameters.Add("@zip", SqlDbType.VarChar).Value = nurse.Zip;
                cmd.Parameters.Add("@country", SqlDbType.VarChar).Value = nurse.Country;
                cmd.Parameters.Add("@sAddress1", SqlDbType.VarChar).Value = nurse.SAddress1;
                cmd.Parameters.Add("@sAddress2", SqlDbType.VarChar).Value = nurse.SAddress2;
                cmd.Parameters.Add("@sCity", SqlDbType.VarChar).Value = nurse.SCity;
                cmd.Parameters.Add("@sState", SqlDbType.VarChar).Value = nurse.SState;
                cmd.Parameters.Add("@sZip", SqlDbType.VarChar).Value = nurse.SZip;
                cmd.Parameters.Add("@sCountry", SqlDbType.VarChar).Value = nurse.SCountry;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = nurse.Email;
                cmd.Parameters.Add("@emailPref", SqlDbType.VarChar).Value = nurse.EmailPref;
                if (nurse.OptIn == false)
                {
                    cmd.Parameters.Add("@optIn", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@optIn", SqlDbType.Bit).Value = nurse.OptIn;
                }
                if (nurse.Offers == false)
                {
                    cmd.Parameters.Add("@offers", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@offers", SqlDbType.Bit).Value = nurse.Offers;
                }

                cmd.Parameters.Add("@rNstate", SqlDbType.VarChar).Value = nurse.RNstate;
                cmd.Parameters.Add("@rNnum", SqlDbType.VarChar).Value = nurse.RNnum;
                cmd.Parameters.Add("@rNCode", SqlDbType.VarChar).Value = nurse.RNCode;
                cmd.Parameters.Add("@rNnumX", SqlDbType.VarChar).Value = nurse.RNnumX;
                cmd.Parameters.Add("@rNstate2", SqlDbType.VarChar).Value = nurse.RNstate2;
                cmd.Parameters.Add("@rNnum2", SqlDbType.VarChar).Value = nurse.RNnum2;
                cmd.Parameters.Add("@rNCode2", SqlDbType.VarChar).Value = nurse.RNCode2;
                cmd.Parameters.Add("@rNnumX2", SqlDbType.VarChar).Value = nurse.RNnumX2;
                cmd.Parameters.Add("@specialtyID", SqlDbType.VarChar).Value = nurse.SpecialtyID;
                cmd.Parameters.Add("@specialtyIDlist", SqlDbType.VarChar).Value = nurse.SpecialtyIDlist;
                cmd.Parameters.Add("@importedFrom", SqlDbType.VarChar).Value = nurse.ImportedFrom;
                cmd.Parameters.Add("@fieldID", SqlDbType.Int).Value = nurse.FieldID;
                cmd.Parameters.Add("@ageGroup", SqlDbType.Int).Value = nurse.AgeGroup;
                cmd.Parameters.Add("@browserAgent", SqlDbType.VarChar).Value = nurse.BrowserAgent;
                cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = nurse.Phone;
                cmd.Parameters.Add("@phoneType", SqlDbType.VarChar).Value = nurse.PhoneType;
                cmd.Parameters.Add("@specialty", SqlDbType.VarChar).Value = nurse.Specialty;
                cmd.Parameters.Add("@education", SqlDbType.VarChar).Value = nurse.Education;
                cmd.Parameters.Add("@position", SqlDbType.VarChar).Value = nurse.Position;
                if (nurse.DateEntered == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@dateEntered", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@dateEntered", SqlDbType.DateTime).Value = nurse.DateEntered;
                }
                if (nurse.UCEend == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@uCEend", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@uCEend", SqlDbType.DateTime).Value = nurse.UCEend;
                }
                if (nurse.Updated == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@updated", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@updated", SqlDbType.DateTime).Value = nurse.Updated;
                }
                if (nurse.LastLogin == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@lastLogin", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@lastLogin", SqlDbType.DateTime).Value = nurse.LastLogin;
                }
                cmd.Parameters.Add("@iP", SqlDbType.VarChar).Value = nurse.IP;

                if (nurse.Disabled == false)
                {
                    cmd.Parameters.Add("@disabled", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@disabled", SqlDbType.Bit).Value = nurse.Disabled;
                }
                SqlParameter IDParameter = new SqlParameter("@retrnID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);


                try
                {

                    cn.Open();
                    //int ret = (int)ExecuteNonQuery(cmd1);
                    int ret = (int)ExecuteNonQuery(cmd);
                    int NewID = (int)IDParameter.Value;
                    return NewID;


                }
                catch
                {
                    return 0;
                }
                //return (ret == 1);
            }



        }

        public int SetEmailSubscription(int rnid, bool job, bool ezine, bool education)
        {
            string emaillists = string.Empty;
            if (job) emaillists += "1";
            if (ezine) 
                if (!string.IsNullOrEmpty(emaillists)) emaillists += ",4";
                else emaillists = "4";
            if (education)
                if (!string.IsNullOrEmpty(emaillists)) emaillists += ",7";
                else emaillists = "7";

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("EXEC SiteParams.dbo.SP_EMAILSYNC_LISTS_ONLY_BY_RNID @rnid,"+
                ",'RN','CE Direct',@emailLists,'add'", cn);
                cmd.Connection = cn;
                cn.Open();                                
                cmd.Parameters.Add("@rnid", SqlDbType.Int).Value = rnid;
                cmd.Parameters.Add("@emaillists", SqlDbType.Int).Value = emaillists;
                try
                {
                    return (int)cmd.ExecuteNonQuery();
                }
                catch
                {
                    return -2;
                }
            }
        }

        public  int IsUsernameExists(string Username)
        {

            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                string str = "Select count(UserName) from Nurse where UserName= '" + Username.ToString().Trim() + "' ";
                SqlCommand cmd = new SqlCommand(str, cn);
                //cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = Username;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }

        }

        public int IsExistingRetailUser(string email)
        {

            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                string str = "Select count(*) from Nurse where email= '" + email.ToString().Trim() + "'";
                SqlCommand cmd = new SqlCommand(str, cn);
                //cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = Username;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }

        }

        public  NurseInfo GetNurseByID(int rnID)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from Nurse where rnID=@rnID", cn);
                cmd.Parameters.Add("@rnID", SqlDbType.Int).Value = rnID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetNurseFromReader(reader, true);
                else
                    return null;
            }
        }

        public  int CheckUserLogin(string UserName, string PassWord)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select rnid from Nurse where username=@UserName and Password = @Password ", cn);
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName;
                cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = PassWord;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                if (reader.Read())
                    return (int)(reader[0]);
                else
                    return 0;
            }
        }
        #endregion

        #region PRProvider

        protected virtual NurseInfo GetNurseFromReader(IDataReader reader)
        {
            return GetNurseFromReader(reader, true);
        }
        protected virtual NurseInfo GetNurseFromReader(IDataReader reader, bool readMemos)
        {

            NurseInfo Nurse = new NurseInfo(
              (int)(Convert.IsDBNull(reader["rnID"]) ? (int)0 : (int)reader["rnID"]),
              reader["UserName"].ToString(),
              reader["Password"].ToString(),
              (bool)(Convert.IsDBNull(reader["Registered"]) ? false : Convert.ToBoolean(reader["Registered"])),
              reader["FirstName"].ToString(),
              reader["LastName"].ToString(),
              reader["Address1"].ToString(),
              reader["Address2"].ToString(),
              reader["City"].ToString(),
              reader["State"].ToString(),
              reader["Zip"].ToString(),
              reader["Country"].ToString(),
              reader["S_Address1"].ToString(),
              reader["S_Address2"].ToString(),
              reader["S_City"].ToString(),
              reader["S_State"].ToString(),
              reader["S_Zip"].ToString(),
              reader["S_Country"].ToString(),
              reader["Email"].ToString(),
              reader["EmailPref"].ToString(),
              (bool)(Convert.IsDBNull(reader["OptIn"]) ? false : Convert.ToBoolean(reader["OptIn"])),
              (bool)(Convert.IsDBNull(reader["Offers"]) ? false : Convert.ToBoolean(reader["Offers"])),
              reader["RNstate"].ToString(),
              reader["RNnum"].ToString(),
              reader["RNCode"].ToString(),
              reader["RNnumX"].ToString(),
              reader["RNstate2"].ToString(),
              reader["RNnum2"].ToString(),
              reader["RNCode2"].ToString(),
              reader["RNnumX2"].ToString(),    //(int)(Convert.IsDBNull(reader["SpecialityID"],
              (int)(Convert.IsDBNull(reader["SpecialtyID"]) ? (int)0 : (int)reader["SpecialtyID"]),
                //  1,"",
              (reader["SpecialtyIDlist"] == null) ? String.Empty : reader["SpecialtyIDlist"].ToString(),
              reader["ImportedFrom"].ToString(),
             (int)(Convert.IsDBNull(reader["FieldID"]) ? (int)0 : (int)reader["FieldID"]),
             (int)(Convert.IsDBNull(reader["AgeGroup"]) ? (int)0 : (int)reader["AgeGroup"]),
             reader["BrowserAgent"].ToString(),
             reader["Phone"].ToString(),
             reader["PhoneType"].ToString(),
             reader["Specialty"].ToString().ToLower(),
             reader["Education"].ToString(),
             reader["Position"].ToString(),
              (DateTime)(Convert.IsDBNull(reader["DateEntered"])
                  ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["DateEntered"]),
                  (DateTime)(Convert.IsDBNull(reader["UCEend"])
                  ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["UCEend"]),
                  (DateTime)(Convert.IsDBNull(reader["Updated"])
                  ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Updated"]),
                  (DateTime)(Convert.IsDBNull(reader["LastLogin"])
                  ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLogin"]),
              reader["IP"].ToString(),
                       (bool)(Convert.IsDBNull(reader["Disabled"]) ? false : Convert.ToBoolean(reader["Disabled"])));
            return Nurse;
        }
        #endregion
    }
}
#endregion