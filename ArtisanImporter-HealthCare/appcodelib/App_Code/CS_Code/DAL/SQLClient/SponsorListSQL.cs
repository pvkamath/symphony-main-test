﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;


#region SponsorListInfo

namespace PearlsReview.DAL
{
    public class SponsorListInfo
    {
        public SponsorListInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        private int _SponsorID = 0;
        public int SponsorID
        {
            get { return _SponsorID; }
            protected set { _SponsorID = value; }
        }

        private int _InitiativeID = 0;
        public int InitiativeID
        {
            get { return _InitiativeID; }
            protected set { _InitiativeID = value; }
        }

        public SponsorListInfo(int InitiativeID, int SponsorID)
        {
            this.SponsorID = SponsorID;
            this.InitiativeID = InitiativeID;
        }

    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider


        /// <summary>
        /// Retrieves all  SponsorList Info for the specified facility id
        /// </summary>
        public List<SponsorListInfo> GetSponsorListBySponsorID(int SponsorID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "SponsorID, " +
                    "initid " +
                    "from SponsorList " +
                    "where SponsorID = @SponsorID " +
                    "order by initid DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@SponsorID", SqlDbType.VarChar).Value = SponsorID;
                cn.Open();
                return GetSponsorListCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        public int InsertSponsorList(SponsorListInfo SponsorList)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into SponsorList " +
                    "(initid, " +
                    "SponsorID )" +
                    "VALUES (" +
                    "@initid, " +
                    "@SponsorID)", cn);
                cmd.Parameters.Add("@initid", SqlDbType.Int).Value = SponsorList.InitiativeID;
                cmd.Parameters.Add("@SponsorID", SqlDbType.Int).Value = SponsorList.SponsorID;

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }
                cn.Open();
                int ret = cmd.ExecuteNonQuery();                
                return ret;                
            }
            throw new NotImplementedException();
        }


        public bool DeleteSponsorList(int InitID, int SponsorID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from SponsorList where initid=@initid And SponsorID=@SponsorID", cn);
                cmd.Parameters.Add("@initid", SqlDbType.Int).Value = InitID;
                cmd.Parameters.Add("@SponsorID", SqlDbType.Int).Value = SponsorID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);                
            }
        }
       
        #endregion

        #region PRProvider

        //////////////////////////////////////////////////////////
        // methods that work with SponsorList


        protected virtual List<SponsorListInfo> GetSponsorListCollectionFromReader(IDataReader reader)
        {
            return GetSponsorListCollectionFromReader(reader, true);
        }
        protected virtual List<SponsorListInfo> GetSponsorListCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<SponsorListInfo> SponsorLists = new List<SponsorListInfo>();
            while (reader.Read())
                SponsorLists.Add(GetSponsorListFromReader(reader, readMemos));
            return SponsorLists;
        }
        protected virtual SponsorListInfo GetSponsorListFromReader(IDataReader reader)
        {
            return GetSponsorListFromReader(reader, true);
        }

        protected virtual SponsorListInfo GetSponsorListFromReader(IDataReader reader, bool readMemos)
        {
            // NOTE: NULL integer foreign key LectEvtID is replaced with value of
            // zero, which will trigger "Please select..." or "(Not Selected)"
            // message in UI dropdown for that field
            SponsorListInfo SponsorList = new SponsorListInfo(
              (int)reader["initid"],
              (int)reader["SponsorID"]);

            return SponsorList;
        }
        #endregion
    }
}
#endregion