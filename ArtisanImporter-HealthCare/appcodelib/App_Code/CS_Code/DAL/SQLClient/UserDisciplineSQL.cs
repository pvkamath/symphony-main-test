﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for UserDisciplineSQL
/// </summary>
#region UserDisciplineInfo
/// 
namespace PearlsReview.DAL
{
    public class UserDisciplineInfo
    {
        public UserDisciplineInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

       #region Variables and Properties

        private int _userdspid = 0;
        public int userdspid
        {
            get { return _userdspid; }
            protected set { _userdspid = value; }
        }
        public int _userid = 0;
        public int userid
        {
            get { return _userid; }
            set { _userid = value; }
        }
        public int _disciplineid = 0;
        public int disciplineid
        {
            get { return _disciplineid; }
            set { _disciplineid = value; }
        }
        public bool _primary_ind = false;
        public bool primary_ind
        {
            get { return _primary_ind; }
            set { _primary_ind = value; }
        }
        public UserDisciplineInfo(int userdspid, int userid, int disciplineid, bool primary_ind)
        {
            this.userdspid = userdspid;
            this.userid = userid;
            this.disciplineid = disciplineid;
            this.primary_ind = primary_ind;
        }

       #endregion
    }
}

#endregion
#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        /// <summary>
        /// Inserts a new Discipline
        /// </summary>
        public int InsertUserDiscipline(UserDisciplineInfo UserDiscipline)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into UserDiscipline " +
                "( userid, " +
                "disciplineid, " +
                "primary_ind ) " +
                "VALUES ( " +
                "@userid, " +
                "@disciplineid, " +
                "@primary_ind ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = UserDiscipline.userid;
                cmd.Parameters.Add("@disciplineid", SqlDbType.Int).Value = UserDiscipline.disciplineid;
                cmd.Parameters.Add("@primary_ind", SqlDbType.Bit).Value = UserDiscipline.primary_ind;
                cn.Open();
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;
            }
        }


        /// <summary>
        /// Updates a UserDiscipline
        /// </summary>
        public bool UpdateUserDiscipline(UserDisciplineInfo UserDiscipline)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserDiscipline set " +
              "userid = @userid, " +
              "disciplineid = @disciplineid, " +
              "primary_ind = @primary_ind " +
              " where userdspid = @userdspid ", cn);
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = UserDiscipline.userid;
                cmd.Parameters.Add("@disciplineid", SqlDbType.Int).Value = UserDiscipline.disciplineid;
                cmd.Parameters.Add("@primary_ind", SqlDbType.Bit).Value = UserDiscipline.primary_ind;
                cmd.Parameters.Add("@userdspid", SqlDbType.Int).Value = UserDiscipline.userdspid;
               
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        /// <summary>
        /// Retrieves all UserDiscipline for the specified UserID
        /// </summary>
        public List<UserDisciplineInfo> GetUserDisciplinesByUserID(int UserID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "userdspid, " +
                    "userid, " +
                    "disciplineid, " +
                    "primary_ind " +
                    "from UserDiscipline " +
                    "where UserID = @UserID " +
                    "order by " + cSortExpression;

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return GetUserDisciplineCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


     
        public UserDisciplineInfo GetUserDisciplinesByUserIDAndDisciplineID(int UserID, int Disciplineid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select " +
                    "userdspid, " +
                    "userid, " +
                    "disciplineid, " +
                    "primary_ind " +
                    "from UserDiscipline " +
                    "where UserID = @UserID  and Disciplineid = @Disciplineid", cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@Disciplineid", SqlDbType.Int).Value = Disciplineid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUserDisciplineFromReader(reader, true);
                else
                    return null;
            }
        }


        public UserDisciplineInfo GetUserDisciplinesByUserDsspID(int UserDsspID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select " +
                    "userdspid, " +
                    "userid, " +
                    "disciplineid, " +
                    "primary_ind " +
                    "from UserDiscipline " +
                    "where userdspid = @UserDsspID ", cn);

                cmd.Parameters.Add("@UserDsspID", SqlDbType.Int).Value = UserDsspID;

                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUserDisciplineFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a UserDiscipline
        /// </summary>
        public bool DeleteAllUserDisciplinesByUserId(int UserId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from userdiscipline where Userid=@UserId", cn);
                cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a UserDiscipline by userdspid
        /// </summary>
        public bool DeleteUserDisciplineByUserdspid(int userdspid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from userdiscipline where userdspid=@userdspid ", cn);
                cmd.Parameters.Add("@userdspid", SqlDbType.Int).Value = userdspid;                
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

         #region PRProvider

        protected virtual UserDisciplineInfo GetUserDisciplineFromReader(IDataReader reader)
        {
            return GetUserDisciplineFromReader(reader, true);
        }
        protected virtual UserDisciplineInfo GetUserDisciplineFromReader(IDataReader reader, bool readMemos)
        {
            UserDisciplineInfo UserDiscipline = new UserDisciplineInfo(
              (int)reader["userdspid"],
              (int)reader["userid"],
              (int)reader["disciplineid"],
              (bool)reader["primary_ind"]);

            return UserDiscipline;
        }

        protected virtual List<UserDisciplineInfo> GetUserDisciplineCollectionFromReader(IDataReader reader)
        {
            return GetUserDisciplineCollectionFromReader(reader, true);
        }
        protected virtual List<UserDisciplineInfo> GetUserDisciplineCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<UserDisciplineInfo> UserDisciplines = new List<UserDisciplineInfo>();
            while (reader.Read())
                UserDisciplines.Add(GetUserDisciplineFromReader(reader, readMemos));
            return UserDisciplines;
        }

        #endregion
    
    }
}
#endregion