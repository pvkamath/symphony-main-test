﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region MicrositeResourceInfo

namespace PearlsReview.DAL
{

    /// <summary> 
    /// Summary description for MicrositeDomainInfo
    /// Bhaskar N
    /// </summary>
    public class MicrositeResourceInfo
    {

        public MicrositeResourceInfo() { }


        public MicrositeResourceInfo(int id, int msid, string resource_name, string file_name, int resource_order, string resource_title, string resource_type, string resource_html)
        {
            this.ID = id;
            this.Msid = msid;
            this.Resource_name = resource_name;
            this.File_name = file_name;
            this.Resource_order = resource_order;
            this.Resource_title = resource_title;
            this.Resource_type = resource_type;
            this.Resource_html = resource_html;
        }
        private int _id = 0;
        public int ID
        {
            get { return _id; }
            protected set { _id = value; }
        }
        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }
        private string _resource_name = "";
        public string Resource_name
        {
            get { return _resource_name; }
            private set { _resource_name = value; }
        }
        private string _file_name = "";
        public string File_name
        {
            get { return _file_name; }
            private set { _file_name = value; }
        }
        private int _resource_order = 0;
        public int Resource_order
        {
            get { return _resource_order; }
            private set { _resource_order = value; }
        }
        private string _resource_title = "";
        public string Resource_title
        {
            get { return _resource_title; }
            private set { _resource_title = value; }
        }
        private string _resource_type = "";
        public string Resource_type
        {
            get { return _resource_type; }
            private set { _resource_type = value; }
        }
        private string _resource_html = "";
        public string Resource_html
        {
            get { return _resource_html; }
            private set { _resource_html = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // Methods that work with MicrositeDomain
        // Bhaskar N

        /// <summary>
        /// Retrieves all MicrositeDomains
        /// </summary>
        public List<MicrositeResourceInfo> GetMicrositeResource(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from MicrositeResource ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetMicrositeResourceCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public List<MicrositeResourceInfo> GetMicrositeResourceByMsid(int Msid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from MicrositeResource where msid=" + Msid;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetMicrositeResourceCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves the MicrositeDomain with the specified ID
        /// </summary>
        public MicrositeResourceInfo GetMicrositeResourceByID(int id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositeResource where id=@id", cn);

                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositeResourceFromReader(reader, true);
                else
                    return null;
            }
        }
        public MicrositeResourceInfo GetMicrositeResourceByID(int msid, string resource_name)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositeResource where msid=@msid and resource_name = @resource_name", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cmd.Parameters.Add("@resource_name", SqlDbType.VarChar).Value = resource_name;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositeResourceFromReader(reader, true);
                else
                    return null;
            }
        }
        public int GetMicrositeResourceMaxOrder(int msid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select count(*) as max " +
                        "from MicrositeResource where msid=@msid", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return int.Parse(reader["max"].ToString());
                else
                    return 0;
            }
        }
        /// <summary>
        /// Inserts a new Discount
        /// </summary>
        public int InsertMicrositeResource(MicrositeResourceInfo MicrositeResource)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into MicrositeResource " +
                  "(msid , " +
                  "resource_name, " +
                  "file_name, " +
                  "resource_order, " +
                  "resource_title, " +
                  "resource_type, " +
                  "resource_html) VALUES (@msid, @resource_name, @file_name, @resource_order, @resource_title, @resource_type, @resource_html)", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeResource.Msid;
                cmd.Parameters.Add("@resource_name", SqlDbType.VarChar).Value = MicrositeResource.Resource_name;
                cmd.Parameters.Add("@file_name", SqlDbType.VarChar).Value = MicrositeResource.File_name;
                cmd.Parameters.Add("@resource_order", SqlDbType.Int).Value = MicrositeResource.Resource_order;
                cmd.Parameters.Add("@resource_title", SqlDbType.VarChar).Value = MicrositeResource.Resource_title;
                cmd.Parameters.Add("@resource_type", SqlDbType.VarChar).Value = MicrositeResource.Resource_type;
                cmd.Parameters.Add("@resource_html", SqlDbType.VarChar).Value = MicrositeResource.Resource_html;
                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int i = cmd.ExecuteNonQuery();

                return i;
            }
        }
        /// <summary>
        /// Updates a Discount
        /// </summary>
        public bool UpdateMicrositeResource(MicrositeResourceInfo MicrositeResource)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update MicrositeResource set " +
              "msid = @msid, " +
              "resource_name = @resource_name, " +
              "file_name = @file_name, " +
              "resource_order = @resource_order, " +
              "resource_title = @resource_title, " +
              "resource_type = @resource_type, " +
              "resource_html = @resource_html where id=@id", cn);

                cmd.Parameters.Add("@id", SqlDbType.Int).Value = MicrositeResource.ID;
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeResource.Msid;
                cmd.Parameters.Add("@resource_name", SqlDbType.VarChar).Value = MicrositeResource.Resource_name;
                cmd.Parameters.Add("@file_name", SqlDbType.VarChar).Value = MicrositeResource.File_name;
                cmd.Parameters.Add("@resource_order", SqlDbType.Int).Value = MicrositeResource.Resource_order;
                cmd.Parameters.Add("@resource_title", SqlDbType.VarChar).Value = MicrositeResource.Resource_title;
                cmd.Parameters.Add("@resource_type", SqlDbType.VarChar).Value = MicrositeResource.Resource_type;
                cmd.Parameters.Add("@resource_html", SqlDbType.VarChar).Value = MicrositeResource.Resource_html;

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a Discount
        /// </summary>
        public bool DeleteMicrositeResource(int id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from MicrositeResource where id=@id", cn);
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider
        /////////////////////////////////////////////////////////
        // methods that work with Discount  

        /// <summary>
        /// Returns a new DiscountInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual MicrositeResourceInfo GetMicrositeResourceFromReader(IDataReader reader)
        {
            return GetMicrositeResourceFromReader(reader, true);
        }
        protected virtual MicrositeResourceInfo GetMicrositeResourceFromReader(IDataReader reader, bool readMemos)
        {
            MicrositeResourceInfo MicrositeResource = new MicrositeResourceInfo(
                  (int)reader["id"],
                  (int)reader["msid"],
                  reader["resource_name"].ToString(),
                  reader["file_name"].ToString(),
                  String.IsNullOrEmpty(reader["resource_order"].ToString()) ? 0 : int.Parse(reader["resource_order"].ToString()),
                  reader["resource_title"].ToString(),
                  reader["resource_type"].ToString(),
                  reader["resource_html"].ToString());
            return MicrositeResource;
        }


        /// <summary>
        /// Returns a collection of DiscountInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<MicrositeResourceInfo> GetMicrositeResourceCollectionFromReader(IDataReader reader)
        {
            return GetMicrositeResourceCollectionFromReader(reader, true);
        }
        protected virtual List<MicrositeResourceInfo> GetMicrositeResourceCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<MicrositeResourceInfo> MicrositeResource = new List<MicrositeResourceInfo>();
            while (reader.Read())
                MicrositeResource.Add(GetMicrositeResourceFromReader(reader, readMemos));
            return MicrositeResource;
        }

        #endregion

    }
}
#endregion
