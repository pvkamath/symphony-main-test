﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

#region MicrositeStateReqTopicLinkInfo
namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for MicrositeStateReqTopicLinkInfo
    /// Alex Chen
    /// </summary>
    public class MicrositeStateReqTopicLinkInfo
    {
        public MicrositeStateReqTopicLinkInfo() { }

        public MicrositeStateReqTopicLinkInfo(int msrtID, int msrID, int topicID)
        {
            this.MsrtID = msrtID;
            this.MsrID = msrID;
            this.TopicID = topicID;
        }

        private int _msrtID = 0;
        public int MsrtID
        {
            get { return _msrtID; }
            set { _msrtID = value; }
        }

        private int _msrID = 0;
        public int MsrID
        {
            get { return _msrID; }
            set { _msrID = value; }
        }

        private int _topicID = 0;
        public int TopicID
        {
            get { return _topicID; }
            set { _topicID = value; }
        }
    }
}
#endregion MicrositeStateReqTopicLinkInfo

#region SQLPRProvider and PRProvider
namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        public int insertMicrositeStateReqTopicLink(MicrositeStateReqTopicLinkInfo msStateReqTopicLinkInfo)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into MicrositeStateReqTopicLink " +
                    "(msrid, " + 
                    "topicid) " +
                    "VALUES " + 
                    "(@msrid, " + 
                    "@topicid) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@msrid", SqlDbType.Int).Value = msStateReqTopicLinkInfo.MsrID;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = msStateReqTopicLinkInfo.TopicID;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                return (int)IDParameter.Value;
            }
        }

        public bool deleteMicrositeStateReqTopicLink(int msrtID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from MicrositeStateReqTopicLink where msrtid = @msrtid", cn);
                cmd.Parameters.Add("@msrtid", SqlDbType.Int).Value = msrtID;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public MicrositeStateReqTopicLinkInfo getMicrositeStateReqTopicLinkByMsrIDAndTopicID(int msrID, int topicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(
                    "select * from MicrositeStateReqTopicLink where msrid = @msrid and topicid = @topicid", cn);
                cmd.Parameters.Add("@msrid", SqlDbType.Int).Value = msrID;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicID;
                cn.Open();
                IDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                    return getMicrositeStateReqTopicLinkFromReader(reader, false);
                else
                    return null;
            }
        }
        #endregion SQLPRProvider

        #region PRProvider
        protected virtual MicrositeStateReqTopicLinkInfo getMicrositeStateReqTopicLinkFromReader(IDataReader reader)
        {
            return getMicrositeStateReqTopicLinkFromReader(reader, true);
        }

        protected virtual MicrositeStateReqTopicLinkInfo getMicrositeStateReqTopicLinkFromReader(IDataReader reader, bool readMemos)
        {
            MicrositeStateReqTopicLinkInfo micrositeStateReqTopicLink = new MicrositeStateReqTopicLinkInfo(
                  (int)reader["msrtid"],
                  (int)reader["msrid"],
                  (int)reader["topicid"]);
            return micrositeStateReqTopicLink;
        }

        protected virtual List<MicrositeStateReqTopicLinkInfo> getMicrositeStateReqTopicLinkCollectionFromReader(IDataReader reader)
        {
            return getMicrositeStateReqTopicLinkCollectionFromReader(reader, true);
        }
        protected virtual List<MicrositeStateReqTopicLinkInfo> getMicrositeStateReqTopicLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<MicrositeStateReqTopicLinkInfo> micrositeStateReqTopicLink = new List<MicrositeStateReqTopicLinkInfo>();
            while (reader.Read())
                micrositeStateReqTopicLink.Add(getMicrositeStateReqTopicLinkFromReader(reader, readMemos));
            return micrositeStateReqTopicLink;
        }
        #endregion PRProvider
    }
}
#endregion SQLPRProvider and PRProvider
