﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

/// <summary>
/// Summary description for MicrositeDomainSQL
/// </summary>

#region MicrositeDomainInfo

namespace PearlsReview.DAL
{

    /// <summary> 
    /// Summary description for MicrositeDomainInfo
    /// Bhaskar N
    /// </summary>
    public class MicrositeDomainInfo
    {

        public MicrositeDomainInfo() { }


        public MicrositeDomainInfo(int msid, string domain_name, string index_title, string logo_filename, string logo_desc, string banner_filename, 
            string banner_desc, string meta_kw, string meta_desc, decimal membership_cost, bool membership_ind, string membership_image, 
            string membership_desc, string market_text, string folder_name, string contacthour_longname, string contacthour_shortname,
            string course_cat_header, string state_mandated_header, string accreditation_statement, string membership_ads, bool active_ind,
            string facebook_link, string google_plus_link, string twitter_link, string disclaimer)
        {
            this.Msid = msid;
            this.Domain_name = domain_name;
            this.Index_title = index_title;
            this.Logo_filename = logo_filename;
            this.Logo_Desc = logo_desc;
            this.Banner_filename = banner_filename;
            this.Banner_Desc = banner_desc;
            this.Meta_kw = meta_kw;
            this.Meta_desc = meta_desc;
            this.Membership_cost = membership_cost;
            this.Membership_ind = membership_ind;
            this.Membership_image = membership_image;
            this.Membership_desc = membership_desc;
            this.Market_text = market_text;
            this.Folder_name = folder_name;
            this.Contacthour_longname = contacthour_longname;
            this.Contacthour_shortname = contacthour_shortname;
            this.Course_cat_header = course_cat_header;
            this.State_mandated_header = state_mandated_header;
            this.Accreditation_statement = accreditation_statement;
            this.Membership_ads = membership_ads;
            this.Active_ind = active_ind;
            this.Facebook_link = facebook_link;
            this.Google_plus_link = google_plus_link;
            this.Twitter_link = twitter_link;
            this.Disclaimer = disclaimer;
        }

        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }
        private string _domain_name = "";
        public string Domain_name
        {
            get { return _domain_name; }
            private set { _domain_name = value; }
        }
        private string _index_title = "";
        public string Index_title
        {
            get { return _index_title; }
            private set { _index_title = value; }
        }
        private string _logo_filename = "";
        public string Logo_filename
        {
            get { return _logo_filename; }
            private set { _logo_filename = value; }
        }
        private string _logo_desc = "";
        public string Logo_Desc
        {
            get { return _logo_desc; }
            private set { _logo_desc = value; }
        }
        private string _banner_filename = "";
        public string Banner_filename
        {
            get { return _banner_filename; }
            private set { _banner_filename = value; }
        }
        private string _banner_desc = "";
        public string Banner_Desc
        {
            get { return _banner_desc; }
            private set { _banner_desc = value; }
        }
        private string _meta_kw = "";
        public string Meta_kw
        {
            get { return _meta_kw; }
            private set { _meta_kw = value; }
        }
        private string _meta_desc = "";
        public string Meta_desc
        {
            get { return _meta_desc; }
            private set { _meta_desc = value; }
        }
        private decimal _membership_cost;
        public decimal Membership_cost
        {
            get { return _membership_cost; }
            private set { _membership_cost = value; }
        }
        private string _market_text = "";
        public string Market_text
        {
            get { return _market_text; }
            private set { _market_text = value; }
        }
        private string _folder_name = "";
        public string Folder_name
        {
            get { return _folder_name; }
            private set { _folder_name = value; }
        }
        private bool _membership_ind;
        public bool Membership_ind
        {
            get { return _membership_ind; }
            private set { _membership_ind = value; }
        }
        private string _membership_image = "";
        public string Membership_image
        {
            get { return _membership_image; }
            private set { _membership_image = value; }
        }
        private string _membership_desc = "";
        public string Membership_desc
        {
            get { return _membership_desc; }
            private set { _membership_desc = value; }
        }
        private string _contacthour_longname = "";
        public string Contacthour_longname
        {
            get { return _contacthour_longname; }
            private set { _contacthour_longname = value; }
        }
        private string _contacthour_shortname = "";
        public string Contacthour_shortname
        {
            get { return _contacthour_shortname; }
            private set { _contacthour_shortname = value; }
        }
        private string _course_cat_header = "";
        public string Course_cat_header
        {
            get { return _course_cat_header; }
            private set { _course_cat_header = value; }
        }
        private string _state_mandated_header = "";
        public string State_mandated_header
        {
            get { return _state_mandated_header; }
            private set { _state_mandated_header = value; }
        }
        private string _accreditation_statement = "";
        public string Accreditation_statement
        {
            get { return _accreditation_statement; }
            private set { _accreditation_statement = value; }
        }
        private string _membership_ads = "";
        public string Membership_ads
        {
            get { return _membership_ads; }
            private set { _membership_ads = value; }
        }
        private bool _active_ind;
        public bool Active_ind
        {
            get { return _active_ind; }
            private set { _active_ind = value; }
        }
        private string _facebook_link = "";
        public string Facebook_link
        {
            get { return _facebook_link; }
            private set { _facebook_link = value; }
        }
        private string _google_plus_link = "";
        public string Google_plus_link
        {
            get { return _google_plus_link; }
            private set { _google_plus_link = value; }
        }
        private string _twitter_link = "";
        public string Twitter_link
        {
            get { return _twitter_link; }
            private set { _twitter_link = value; }
        }
        private string _disclaimer = "";
        public string Disclaimer
        {
            get { return _disclaimer; }
            private set { _disclaimer = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // Methods that work with MicrositeDomain
        // Bhaskar N

        /// <summary>
        /// Retrieves all MicrositeDomains
        /// </summary>
        public List<MicrositeDomainInfo> GetMicrositeDomain(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from MicrositeDomain where msid > 30";
                
                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetMicrositeDomainCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        //for admin site
        public List<MicrositeDomainInfo> GetMicrositeDomainAdmin(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from MicrositeDomain where msid <> 5";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetMicrositeDomainCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<MicrositeDomainInfo> GetMicrositeDomain(string cSortExpression, bool IsActive, int domainId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from MicrositeDomain where msid > 30 and active_ind = @active_ind and msid in (select A.msid from AlliedProfLink A join AhDomain B on A.ad_id=B.ad_id where B.ad_id=@domainid )";
                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@active_ind", SqlDbType.Bit).Value = IsActive;
                cmd.Parameters.Add("@domainid", SqlDbType.Int).Value = domainId;
                cn.Open();
                return GetMicrositeDomainCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves the MicrositeDomain with the specified ID
        /// </summary>
        public MicrositeDomainInfo GetMicrositeDomainByID(int msid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositeDomain where msid=@msid", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositeDomainFromReader(reader, true);
                else
                    return null;
            }
        }
        public MicrositeDomainInfo GetMicrositeDomainIDByName(string DomainName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositeDomain where domain_name=@domain_name", cn);

                cmd.Parameters.Add("@domain_name", SqlDbType.VarChar).Value = DomainName;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositeDomainFromReader(reader, true);
                else
                    return null;
            }
        }
        
        /// <summary>
        ///  Retrieves all MicrositeDomains by User Id
        /// </summary>
        public DataSet GetMicrositeDomainByUserId(int userID, string cSortExpression)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = " select * from MicrositeDomain md " +
                   " join UserDiscipline ud on md.msid=ud.disciplineid  " +
                   " where ud.userid= @UserID";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }

        /// <summary>
        /// Inserts a new Discount
        /// </summary>
        public int InsertMicrositeDomain(MicrositeDomainInfo MicrositeDomain)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into MicrositeDomain " +
                  "(msid , " +
                  "domain_name, " +
                  "index_title , " +
                  "logo_filename, " +
                  "logo_desc, " +
                  "banner_filename, " +
                  "banner_desc, " +
                  "meta_kw , " +
                  "meta_desc , " +
                  "membership_cost, " +
                  "membership_ind, " +
                  "membership_image, " +
                  "membership_desc, " +
                  "market_text, " +
                  "folder_name, " +
                  "contacthour_longname, " +
                  "contacthour_shortname, " +
                  "course_cat_header, " +
                  "state_mandated_header, " +
                  "accreditation_statement, " +
                  "membership_ads, " +
                  "active_ind, " +
                  "facebook_link, " +
                  "google_plus_link, " +
                  "twitter_link," +
                  "disclaimer) SET @msid = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeDomain.Msid;
                cmd.Parameters.Add("@domain_name", SqlDbType.VarChar).Value = MicrositeDomain.Domain_name;
                cmd.Parameters.Add("@index_title", SqlDbType.VarChar).Value = MicrositeDomain.Index_title;
                cmd.Parameters.Add("@logo_filename", SqlDbType.VarChar).Value = MicrositeDomain.Logo_filename;
                cmd.Parameters.Add("@logo_desc", SqlDbType.VarChar).Value = MicrositeDomain.Logo_Desc;
                cmd.Parameters.Add("@banner_filename", SqlDbType.VarChar).Value = MicrositeDomain.Banner_filename;
                cmd.Parameters.Add("@banner_desc", SqlDbType.VarChar).Value = MicrositeDomain.Banner_Desc;
                cmd.Parameters.Add("@meta_kw", SqlDbType.VarChar).Value = MicrositeDomain.Meta_kw;
                cmd.Parameters.Add("@meta_desc", SqlDbType.VarChar).Value = MicrositeDomain.Meta_desc;
                cmd.Parameters.Add("@membership_cost", SqlDbType.Decimal).Value = MicrositeDomain.Membership_cost;
                cmd.Parameters.Add("@membership_ind", SqlDbType.Bit).Value = MicrositeDomain.Membership_ind;
                cmd.Parameters.Add("@membership_image", SqlDbType.VarChar).Value = MicrositeDomain.Membership_image;
                cmd.Parameters.Add("@membership_desc", SqlDbType.VarChar).Value = MicrositeDomain.Membership_desc;
                cmd.Parameters.Add("@market_text", SqlDbType.VarChar).Value = MicrositeDomain.Market_text;
                cmd.Parameters.Add("@folder_name", SqlDbType.VarChar).Value = MicrositeDomain.Folder_name;
                cmd.Parameters.Add("@contacthour_longname", SqlDbType.VarChar).Value = MicrositeDomain.Contacthour_longname;
                cmd.Parameters.Add("@ntacthour_shortname", SqlDbType.VarChar).Value = MicrositeDomain.Contacthour_shortname;
                cmd.Parameters.Add("@course_cat_header", SqlDbType.VarChar).Value = MicrositeDomain.Course_cat_header;
                cmd.Parameters.Add("@state_mandated_header", SqlDbType.VarChar).Value = MicrositeDomain.State_mandated_header;
                cmd.Parameters.Add("@accreditation_statement", SqlDbType.VarChar).Value = MicrositeDomain.Accreditation_statement;
                cmd.Parameters.Add("@membership_ads", SqlDbType.VarChar).Value = MicrositeDomain.Membership_ads;
                cmd.Parameters.Add("@active_ind", SqlDbType.Bit).Value = MicrositeDomain.Active_ind;
                cmd.Parameters.Add("@facebook_link", SqlDbType.VarChar).Value = MicrositeDomain.Facebook_link;
                cmd.Parameters.Add("@google_plus_link", SqlDbType.VarChar).Value = MicrositeDomain.Google_plus_link;
                cmd.Parameters.Add("@twitter_link", SqlDbType.VarChar).Value = MicrositeDomain.Twitter_link;
                cmd.Parameters.Add("@disclaimer", SqlDbType.VarChar).Value = MicrositeDomain.Disclaimer;

                SqlParameter IDParameter = new SqlParameter("@msid", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }


        /// <summary>
        /// Updates a Discount
        /// </summary>
        public bool UpdateMicrositeDomain(MicrositeDomainInfo MicrositeDomain)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                cn.Open();
                SqlTransaction transaction;
                SqlCommand cmd = cn.CreateCommand();
                transaction = cn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.Connection = cn;
                try
                {
                    cmd.CommandText = "update MicrositeDomain set " +
                      "domain_name = @domain_name, " +
                      "index_title = @index_title, " +
                      "logo_filename = @logo_filename, " +
                      "logo_desc = @logo_desc, " +
                      "banner_filename = @banner_filename, " +
                      "banner_desc = @banner_desc, " +
                      "meta_kw = @meta_kw, " +
                      "meta_desc = @meta_desc, " +
                      "membership_cost = @membership_cost, " +
                      "membership_ind = @membership_ind, " +
                      "membership_image = @membership_image, " +
                      "membership_desc = @membership_desc, " +
                      "market_text = @market_text, " +
                      "folder_name = @folder_name, " +
                      "contacthour_longname = @contacthour_longname, " +
                      "contacthour_shortname = @contacthour_shortname, " +
                      "course_cat_header = @course_cat_header, " +
                      "state_mandated_header = @state_mandated_header, " +
                      "accreditation_statement = @accreditation_statement, " +
                      "membership_ads = @membership_ads, " +
                      "facebook_link = @facebook_link, " +
                      "google_plus_link = @google_plus_link, " +
                      "twitter_link = @twitter_link, " +
                      "disclaimer = @disclaimer, " +
                      "active_ind = @active_ind where msid = @msid";

                    cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeDomain.Msid;
                    cmd.Parameters.Add("@domain_name", SqlDbType.VarChar).Value = MicrositeDomain.Domain_name;
                    cmd.Parameters.Add("@index_title", SqlDbType.VarChar).Value = MicrositeDomain.Index_title;
                    cmd.Parameters.Add("@logo_filename", SqlDbType.VarChar).Value = MicrositeDomain.Logo_filename;
                    cmd.Parameters.Add("@logo_desc", SqlDbType.VarChar).Value = MicrositeDomain.Logo_Desc;
                    cmd.Parameters.Add("@banner_filename", SqlDbType.VarChar).Value = MicrositeDomain.Banner_filename;
                    cmd.Parameters.Add("@banner_desc", SqlDbType.VarChar).Value = MicrositeDomain.Banner_Desc;
                    cmd.Parameters.Add("@meta_kw", SqlDbType.VarChar).Value = MicrositeDomain.Meta_kw;
                    cmd.Parameters.Add("@meta_desc", SqlDbType.VarChar).Value = MicrositeDomain.Meta_desc;
                    cmd.Parameters.Add("@membership_cost", SqlDbType.Decimal).Value = MicrositeDomain.Membership_cost;
                    cmd.Parameters.Add("@membership_ind", SqlDbType.Bit).Value = MicrositeDomain.Membership_ind;
                    cmd.Parameters.Add("@membership_image", SqlDbType.VarChar).Value = MicrositeDomain.Membership_image;
                    cmd.Parameters.Add("@membership_desc", SqlDbType.VarChar).Value = MicrositeDomain.Membership_desc;
                    cmd.Parameters.Add("@market_text", SqlDbType.VarChar).Value = MicrositeDomain.Market_text;
                    cmd.Parameters.Add("@folder_name", SqlDbType.VarChar).Value = MicrositeDomain.Folder_name;
                    cmd.Parameters.Add("@contacthour_longname", SqlDbType.VarChar).Value = MicrositeDomain.Contacthour_longname;
                    cmd.Parameters.Add("@contacthour_shortname", SqlDbType.VarChar).Value = MicrositeDomain.Contacthour_shortname;
                    cmd.Parameters.Add("@course_cat_header", SqlDbType.VarChar).Value = MicrositeDomain.Course_cat_header;
                    cmd.Parameters.Add("@state_mandated_header", SqlDbType.VarChar).Value = MicrositeDomain.State_mandated_header;
                    cmd.Parameters.Add("@accreditation_statement", SqlDbType.VarChar).Value = MicrositeDomain.Accreditation_statement;
                    cmd.Parameters.Add("@membership_ads", SqlDbType.VarChar).Value = MicrositeDomain.Membership_ads;
                    cmd.Parameters.Add("@facebook_link", SqlDbType.VarChar).Value = MicrositeDomain.Facebook_link;
                    cmd.Parameters.Add("@google_plus_link", SqlDbType.VarChar).Value = MicrositeDomain.Google_plus_link;
                    cmd.Parameters.Add("@twitter_link", SqlDbType.VarChar).Value = MicrositeDomain.Twitter_link;
                    cmd.Parameters.Add("@disclaimer", SqlDbType.VarChar).Value = MicrositeDomain.Disclaimer;
                    cmd.Parameters.Add("@active_ind", SqlDbType.Bit).Value = MicrositeDomain.Active_ind;

                    foreach (IDataParameter param in cmd.Parameters)
                    {
                        if (param.Value == null)
                            param.Value = DBNull.Value;
                    }
                    ExecuteNonQuery(cmd);

                    //Update Topic
                    cmd.CommandText = "update topic set objectives=@objectives, online_cost=@cost,offline_cost=@cost where course_number='UCE' and facilityid=@msid";
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@cost", SqlDbType.Decimal).Value = MicrositeDomain.Membership_cost;
                    cmd.Parameters.Add("@objectives", SqlDbType.VarChar).Value = MicrositeDomain.Membership_ads;
                    cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeDomain.Msid;
                    ExecuteNonQuery(cmd);
                    transaction.Commit();
                } 
                catch
                {
                    transaction.Rollback();
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Deletes a Discount
        /// </summary>
        public bool DeleteMicrositeDomain(int Msid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from MicrositeDomain where msid=@msid", cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public List<MicrositeDomainInfo> GetMicrositeDomainByTopicID(int topicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cmdtext = "select distinct md.* from micrositedomain md  " +
                    " join categorymicrositelink cm on md.msid = cm.msid join " +
                    " categories c on cm.categoryid = c.id " +
                    " join categorylink cl on c.id = cl.categoryid where cl.topicid = @topicID";
                if (string.IsNullOrEmpty(cSortExpression))
                    cmdtext = cmdtext + " Order by msid ";
                else
                    cmdtext = cmdtext + " Order by " + cSortExpression;
                SqlCommand cmd = new SqlCommand(cmdtext, cn);
                cmd.Parameters.Add("@topicID", SqlDbType.Int).Value = topicID;
                cn.Open();
                return GetMicrositeDomainCollectionFromReader(ExecuteReader(cmd), false);                   
            }
        }

        #endregion

        #region PRProvider
        /////////////////////////////////////////////////////////
        // methods that work with Discount  

        /// <summary>
        /// Returns a new DiscountInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual MicrositeDomainInfo GetMicrositeDomainFromReader(IDataReader reader)
        {
            return GetMicrositeDomainFromReader(reader, true);
        }
        protected virtual MicrositeDomainInfo GetMicrositeDomainFromReader(IDataReader reader, bool readMemos)
        {
            MicrositeDomainInfo MicrositeDomain = new MicrositeDomainInfo(
                   (int)reader["msid"],
                   reader["domain_name"].ToString(),
                   reader["index_title"].ToString(),
                   reader["logo_filename"].ToString(),
                   reader["logo_desc"].ToString(),
                   reader["banner_filename"].ToString(),
                   reader["banner_desc"].ToString(),
                   reader["meta_kw"].ToString(),
                   reader["meta_desc"].ToString(),
                   String.IsNullOrEmpty(reader["membership_cost"].ToString()) ? 0 : (decimal)reader["membership_cost"],
                   String.IsNullOrEmpty(reader["membership_ind"].ToString()) ? false : bool.Parse(reader["membership_ind"].ToString()),
                   reader["membership_image"].ToString(),
                   reader["membership_desc"].ToString(),
                   reader["market_text"].ToString(),
                   reader["folder_name"].ToString(),
                   reader["contacthour_longname"].ToString(),
                   reader["contacthour_shortname"].ToString(),
                   reader["course_cat_header"].ToString(),
                   reader["state_mandated_header"].ToString(),
                   reader["accreditation_statement"].ToString(),
                   reader["membership_ads"].ToString(),
                   String.IsNullOrEmpty(reader["active_ind"].ToString()) ? false : bool.Parse(reader["active_ind"].ToString()),
                   reader["facebook_link"].ToString(),
                   reader["google_plus_link"].ToString(),
                   reader["twitter_link"].ToString(),
                   reader["disclaimer"].ToString());
            return MicrositeDomain;
        }


        /// <summary>
        /// Returns a collection of DiscountInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<MicrositeDomainInfo> GetMicrositeDomainCollectionFromReader(IDataReader reader)
        {
            return GetMicrositeDomainCollectionFromReader(reader, true);
        }
        protected virtual List<MicrositeDomainInfo> GetMicrositeDomainCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<MicrositeDomainInfo> MicrositeDomain = new List<MicrositeDomainInfo>();
            while (reader.Read())
                MicrositeDomain.Add(GetMicrositeDomainFromReader(reader, readMemos));
            return MicrositeDomain;
        }

        #endregion

    }
}
#endregion
