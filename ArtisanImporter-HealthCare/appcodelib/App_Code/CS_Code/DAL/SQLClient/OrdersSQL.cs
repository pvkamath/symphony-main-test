﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region OrdersInfo

namespace PearlsReview.DAL
{ 

    public class OrdersInfo
    {
    public OrdersInfo(){}

    public OrdersInfo(int OrderID, int UserID, string FirstName, string LastName, string Address1, string Address2,
        string City, string State, string Zip, string Country, string Email, decimal SubTotal, decimal ShipCost, decimal Tax, decimal TotalCost, bool Validate_Ind,
        string  Verification, bool Ship_Ind, DateTime  OrderDate, string Comment, int CouponID, decimal CouponAmount,int facilityId)


	{
        this.OrderID = OrderID;
        this.UserID = UserID;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Address1 = Address1;
        this.Address2 = Address2;
        this.City = City;
        this.State = State;
        this.Zip = Zip;
        this.Country = Country;
        this.Email = Email;
        this.SubTotal = SubTotal;
        this.ShipCost = ShipCost;
        this.Tax = Tax;
        this.TotalCost = TotalCost;
        this.Validate_Ind = Validate_Ind;
        this.Verification = Verification;
        this.Ship_Ind = Ship_Ind;
        this.OrderDate = OrderDate;
        this.Comment = Comment;
        this.CouponID = CouponID;
        this.CouponAmount = CouponAmount;
        this.facilityId = facilityId;
        
    }


    private int _OrderID = 0;
    public int OrderID
    {
        get { return _OrderID; }
        set { _OrderID = value; }
    }

    private int _UserID = 0;
    public int UserID
    {
        get { return _UserID; }
        set { _UserID = value; }
    }

    private string _FirstName = "";
    public string FirstName
    {
        get { return _FirstName; }
        set { _FirstName = value; }
    }
    
    private string _LastName = "";
    public string LastName
    {
        get { return _LastName; }
        set { _LastName = value; }
    }

    private string _Address1 = "";
    public string Address1
    {
        get { return _Address1; }
        set { _Address1 = value; }
    }


    private string _Address2 = "";
    public string  Address2
    {
        get { return _Address2; }
        set { _Address2 = value; }
    }

    private string _City = "";
    public string City
    {
        get { return _City; }
        set { _City = value; }
    }

    private string _State = "";
    public string State
    {
        get { return _State; }
        set { _State = value; }
    }

    private string _Zip = "";
    public string Zip
    {
        get { return _Zip; }
        set { _Zip = value; }
    }


    private string _Country = "";
    public string Country
    {
        get { return _Country; }
        set { _Country = value; }
    }

    
    private string _Email = "";
    public string Email
    {
        get { return _Email; }
        set { _Email = value; }
    }

    private decimal _SubTotal = 0;
    public decimal SubTotal
    {
        get { return _SubTotal; }
        set { _SubTotal = value; }
    }

    private decimal _ShipCost = 0;
    public decimal ShipCost
    {
        get { return _ShipCost; }
        set { _ShipCost = value; }
    }

    private decimal _Tax = 0;
    public decimal Tax
    {
        get { return _Tax; }
        set { _Tax = value; }
    }

    private decimal _TotalCost = 0;
    public decimal TotalCost
    {
        get { return _TotalCost; }
        set { _TotalCost = value; }
    }

    private bool _Validate_Ind = false;
    public bool Validate_Ind
    {
        get { return _Validate_Ind; }
        set { _Validate_Ind = value; }
    }

    private string _Verification = "";
    public string Verification
    {
        get { return _Verification; }
        set { _Verification = value; }
    }

    private bool _Ship_Ind = false;
    public bool Ship_Ind
    {
        get { return _Ship_Ind; }
        set { _Ship_Ind = value; }
    }

    private DateTime _OrderDate = System.DateTime.MinValue;
    public DateTime OrderDate
    {
        get { return _OrderDate; }
        set { _OrderDate = value; }
    }


    private string _Comment = "";
    public string Comment
    {
        get { return _Comment; }
        set { _Comment = value; }
    }

    private int _CouponID = 0;
    public int CouponID
    {
        get { return _CouponID; }
        set { _CouponID = value; }
    }

    private decimal _CouponAmount = 0;
    public decimal CouponAmount
    {
        get { return _CouponAmount; }
        set { _CouponAmount = value; }
    }

    private int _facilityId = 0;
    public int facilityId
    {
        get { return _facilityId; }
        set { _facilityId = value; }

    }

}
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        
        /// <summary>
        /// Retrieves all Orders
        /// </summary>
        public List<OrdersInfo> GetOrders(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Orders";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Orders by date
        /// </summary>
        public List<OrdersInfo> GetOrdersbyDate(string cSortExpression,string selDate, bool IsPending, bool isShipped)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
               // "SELECT     * FROM         Orders WHERE     (orderdate >= CONVERT(DATETIME, '2009-12-12 00:00:00', 102))"
                string strWhereClause = string.Empty;
                if (IsPending && !isShipped)
                {
                    strWhereClause += " and ship_ind = 0 ";
                }
                if (isShipped && !IsPending)
                {
                    strWhereClause += " and ship_ind = 1 ";
                }

                if (!isShipped && ! IsPending)
                {
                    strWhereClause += " and ship_ind = 0 ";          
                }
                if (isShipped && IsPending)
                {
                    strWhereClause += "  ";
                }
                string cSQLCommand = "select * " +
                    "from Orders where  (orderdate >= CONVERT(DATETIME, '" + selDate + "', 102)) and orderid in (select orderid from orderitem where (media_type='book' or media_type='video')) " + strWhereClause + "order by orderdate desc";

                //// add on ORDER BY if provided
                //if (cSortExpression.Length > 0)
                //{
                //    cSQLCommand = cSQLCommand +
                //        " order by " + cSortExpression;
                //}

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all Orders by date
        /// </summary>
        public List<OrdersInfo> GetOrdersbyUserID(int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "SELECT  * FROM Orders WHERE     (userid = " + userid+")";
                string cSQLCommand = "select * from orders left join orderitem on orders.orderid = orderitem.orderid where orders.userid = " + userid + " and Verification<> 'Unlimited CE' ";

                //string cSQLCommand = "select * from orders left join orderitem on orders.orderid = orderitem.orderid where orders.userid ='700979' and Verification<>'Unlimited CE'";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves all Microsite Orders by User ID
        /// </summary>
        public List<OrdersInfo> GetMSOrdersbyUserID(int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from orders"
                    + " where userid = " + userid 
                    + " and (facilityid IN (select msid from MicrositeDomain))"
                    + " and totalcost > 0"
                    + " ORDER BY orderid DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        //added for ahDomain


        /// <summary>
        /// Retrieves all Microsite Orders by User ID
        /// </summary>
        public List<OrdersInfo> GetMSOrdersbyUserIDAndAhDomainId(int userid, int ahDomainId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from orders"
                    + " where userid = " + userid
                    + " and facilityid = " + ahDomainId
                    //+ " and (facilityid IN (select msid from MicrositeDomain))"
                    + " and totalcost > 0"
                    + " ORDER BY orderid DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all Retail Orders by User ID
        /// </summary>
        public List<OrdersInfo> GetRetailOrdersbyUserID(int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from orders"
                    + " where userid = " + userid
                    + " and facilityid = 2"
                    + " and totalcost > 0"
                    + " ORDER BY orderid DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        
        /// <summary>
        /// Retrieves all Pending Orders
        /// </summary>
        public List<OrdersInfo> GetPendingOrders(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Orders where ship_ind = 0 and ship_ind>0";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all Shipped Orders
        /// </summary>
        public List<OrdersInfo> GetShippedOrders(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Orders where ship_ind = 1 and ship_ind>0 ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        
        /// <summary>
        /// Retrieves the Orders with the specified ID
        /// </summary>
        public OrdersInfo GetOrdersByID(int OrderID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from Orders where orderid=@OrderID", cn);
                cmd.Parameters.Add("@OrderID", SqlDbType.Int).Value = OrderID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetOrdersFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Inserts a new Orders
        /// </summary>
        public int InsertOrders(OrdersInfo Orders)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Orders values ( " +
              " @userid, " +
              " @firstname, " +
              " @lastname, " +
              " @address1, " +
              " @address2, " +
              " @city, " +
              " @state, " +
              " @zip, " +
              " @country, " +
              " @email, " +
              " @subtotal, " +
              " @shipcost, " +
              " @tax, " +
              " @totalcost, " +
              " @validate_ind, " +
              " @verification, " +
              " @ship_ind, " +
              " @orderdate, " +
              " @comment, " +
              " @CouponId, " +
              " @Couponamount, " +
               " @facilityId ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@orderid", SqlDbType.Int).Value = Orders.OrderID;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = Orders.UserID;
                cmd.Parameters.Add("@firstname", SqlDbType.VarChar).Value = Orders.FirstName == null ? DBNull.Value as Object : Orders.FirstName as Object;
                cmd.Parameters.Add("@lastname", SqlDbType.VarChar).Value = Orders.LastName == null ? DBNull.Value as Object : Orders.LastName as Object;
                cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = Orders.Address1 == null ? DBNull.Value as Object : Orders.Address1 as Object;
                cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = Orders.Address2 == null ? DBNull.Value as Object : Orders.Address2 as Object;
                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = Orders.City == null ? DBNull.Value as Object : Orders.City as Object;
                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = Orders.State == null ? DBNull.Value as Object : Orders.State as Object;
                cmd.Parameters.Add("@zip", SqlDbType.VarChar).Value = Orders.Zip == null ? DBNull.Value as Object : Orders.Zip as Object;
                cmd.Parameters.Add("@country", SqlDbType.VarChar).Value = Orders.Country == null ? DBNull.Value as Object : Orders.Country as Object;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = Orders.Email == null ? DBNull.Value as Object : Orders.Email as Object;
                cmd.Parameters.Add("@subtotal", SqlDbType.Decimal).Value = Orders.SubTotal;
                cmd.Parameters.Add("@shipcost", SqlDbType.Decimal).Value = Orders.ShipCost;
                cmd.Parameters.Add("@tax", SqlDbType.Decimal).Value = Orders.Tax;
                cmd.Parameters.Add("@totalcost", SqlDbType.Decimal).Value = Orders.TotalCost;
                cmd.Parameters.Add("@Couponamount", SqlDbType.Decimal).Value = Orders.CouponAmount;
                cmd.Parameters.Add("@CouponId", SqlDbType.Int).Value = Orders.CouponID;
                cmd.Parameters.Add("@validate_ind", SqlDbType.Bit).Value = Orders.Validate_Ind;
                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = Orders.Verification == null ? DBNull.Value as Object : Orders.Verification as Object;
                cmd.Parameters.Add("@ship_ind", SqlDbType.Bit).Value = Orders.Ship_Ind;
                cmd.Parameters.Add("@facilityId", SqlDbType.Int).Value = Orders.facilityId;


                if (Orders.OrderDate == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@orderdate", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@orderdate", SqlDbType.DateTime).Value = Orders.OrderDate;
                }

                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = Orders.Comment == null ? DBNull.Value as Object : Orders.Comment as Object;


                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }
        
        /// <summary>
        /// Updates Orders
        /// </summary>
        public bool UpdateOrders(OrdersInfo Orders)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Orders SET " +
              "userid = @userid, " +
              "firstname = @firstname, " +
              "lastname =@lastname, " +
              "address1 =@address1, " +
              "address2 =@address2, " +
              "city =@city, " +
              "state =@state, " +
              "zip =@zip, " +
              "country =@country, " +
              "email =@email, " +
              "subtotal =@subtotal, " +
              "shipcost =@shipcost, " +
              "tax =@tax, " +
              "totalcost =@totalcost, " +
              "validate_ind =@validate_ind, " +
              "verification =@verification, " +
              "ship_ind =@ship_ind, " +
              "orderdate =@orderdate, " +
              "comment =@comment, " +
              "couponid =@couponid, " +
              "facilityid = @facilityId, " +
              "couponamount = @couponamount " +
              "where orderid = @orderid ", cn);
                cmd.Parameters.Add("@orderid", SqlDbType.Int).Value = Orders.OrderID;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = Orders.UserID;
                cmd.Parameters.Add("@firstname", SqlDbType.VarChar).Value = Orders.FirstName == null ? DBNull.Value as Object : Orders.FirstName as Object;
                cmd.Parameters.Add("@lastname", SqlDbType.VarChar).Value = Orders.LastName == null ? DBNull.Value as Object : Orders.LastName as Object;
                cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = Orders.Address1 == null ? DBNull.Value as Object : Orders.Address1 as Object;
                cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = Orders.Address2 == null ? DBNull.Value as Object : Orders.Address2 as Object;
                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = Orders.City == null ? DBNull.Value as Object : Orders.City as Object;
                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = Orders.State == null ? DBNull.Value as Object : Orders.State as Object;
                cmd.Parameters.Add("@zip", SqlDbType.VarChar).Value = Orders.Zip == null ? DBNull.Value as Object : Orders.Zip as Object;
                cmd.Parameters.Add("@country", SqlDbType.VarChar).Value = Orders.Country == null ? DBNull.Value as Object : Orders.Country as Object;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = Orders.Email == null ? DBNull.Value as Object : Orders.Email as Object;
                cmd.Parameters.Add("@subtotal", SqlDbType.Decimal).Value = Orders.SubTotal;
                cmd.Parameters.Add("@shipcost", SqlDbType.Decimal).Value = Orders.ShipCost;
                cmd.Parameters.Add("@tax", SqlDbType.Decimal).Value = Orders.Tax;
                cmd.Parameters.Add("@totalcost", SqlDbType.Decimal).Value = Orders.TotalCost;
                cmd.Parameters.Add("@facilityId", SqlDbType.Int).Value = Orders.facilityId;
                if (Orders.Validate_Ind == false)
                {
                    cmd.Parameters.Add("@validate_ind", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@validate_ind", SqlDbType.Bit).Value = Orders.Validate_Ind;
                }

                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = Orders.Verification == null ? DBNull.Value as Object : Orders.Verification as Object;

                if (Orders.Ship_Ind == false)
                {
                    cmd.Parameters.Add("@ship_ind", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@ship_ind", SqlDbType.Bit).Value = Orders.Ship_Ind;
                }

                if (Orders.OrderDate == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@orderdate", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@orderdate", SqlDbType.DateTime).Value = Orders.OrderDate;
                }

                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = Orders.Comment == null ? DBNull.Value as Object : Orders.Comment as Object;
                cmd.Parameters.Add("@couponid", SqlDbType.Int).Value = Orders.CouponID;
                cmd.Parameters.Add("@couponamount", SqlDbType.Decimal).Value = Orders.CouponAmount;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes Orders
        /// </summary>
        public bool DeleteOrders(int OrderID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Orders where orderid=@OrderID", cn);
                cmd.Parameters.Add("@OrderID", SqlDbType.Int).Value = OrderID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        public void UpdateOrdersForOrderID(OrdersInfo Orders, int OrderID)
        {
            using (SqlConnection conn = new SqlConnection(this.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);

                // Must assign both transaction object and connection
                // to Command object for a pending local transaction
                command.Connection = conn;
                command.Transaction = transaction;

                try
                {
                    command.CommandText = "Update Orders Set firstname=@FirstName, lastname=@LastName,address1=@Address1,address2=@Address2," +
                        " city=@City,state=@State,zip=@Zip,comments=@Comments, userid=@UserID where OrderID=@OrderID";
                   
                    command.Parameters.Add("@OrderID", SqlDbType.Int).Value = OrderID;
                    
                    command.ExecuteNonQuery();
                   transaction.Commit();
                }
                catch { transaction.Rollback(); return; }
                finally { transaction.Dispose(); }
            }
        }


        #endregion

        #region PRProvider

         /// <summary>
        /// Returns a new OrdersInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual OrdersInfo GetOrdersFromReader(IDataReader reader)
        {
            return GetOrdersFromReader(reader, true);
        }
        protected virtual OrdersInfo GetOrdersFromReader(IDataReader reader, bool readMemos)
        {
            OrdersInfo Orders = new OrdersInfo(
                  (int)(Convert.IsDBNull(reader["OrderID"]) ? (int)0 : (int)reader["OrderID"]),
                  (int)(Convert.IsDBNull(reader["UserID"]) ? (int)0 : (int)reader["UserID"]),
                  Convert.ToString(reader["FirstName"].ToString()),
                  Convert.ToString(reader["LastName"].ToString()),
                  Convert.ToString(reader["Address1"].ToString()),
                  Convert.ToString(reader["Address2"].ToString()),
                  Convert.ToString(reader["City"].ToString()),
                  Convert.ToString(reader["State"].ToString()),
                  Convert.ToString(reader["Zip"].ToString()),
                  Convert.ToString(reader["Country"].ToString()),
                  Convert.ToString(reader["Email"].ToString()),
                  (decimal)(Convert.IsDBNull(reader["SubTotal"]) ? (decimal)0 : (decimal)reader["SubTotal"]),
                  (decimal)(Convert.IsDBNull(reader["ShipCost"]) ? (decimal)0 : (decimal)reader["ShipCost"]),
                  (decimal)(Convert.IsDBNull(reader["Tax"]) ? (decimal)0 : (decimal)reader["Tax"]),
                  (decimal)(Convert.IsDBNull(reader["TotalCost"]) ? (decimal)0 : (decimal)reader["TotalCost"]),
                  (Convert.IsDBNull(reader["Validate_Ind"]) ? false : (bool)reader["Validate_Ind"]),
                  Convert.ToString(reader["Verification"].ToString()),
                  (Convert.IsDBNull (reader["Ship_Ind"]) ? false : (bool)reader["Ship_Ind"]),
                  (DateTime)reader["OrderDate"],
                  Convert.ToString(reader["Comment"].ToString()),
                  (int)(Convert.IsDBNull(reader["CouponID"]) ? (int)0 : (int)reader["CouponID"]),
                   (decimal)(Convert.IsDBNull(reader["CouponAmount"]) ? (decimal)0 : (decimal)reader["CouponAmount"]),
                   (int)(Convert.IsDBNull(reader["facilityid"]) ? (int)0: (int)reader["facilityid"]));
                   
            return Orders;
        }
        
        /// <summary>
        /// Returns a collection of OrdersInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<OrdersInfo> GetOrdersCollectionFromReader(IDataReader reader)
        {
            return GetOrdersCollectionFromReader(reader, true);
        }
        protected virtual List<OrdersInfo> GetOrdersCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<OrdersInfo> Orders = new List<OrdersInfo>();
            while (reader.Read())
                Orders.Add(GetOrdersFromReader(reader, readMemos));
            return Orders;
        }



        #endregion

    }
}
#endregion


