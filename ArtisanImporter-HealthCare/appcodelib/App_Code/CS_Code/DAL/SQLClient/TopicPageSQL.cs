﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region TopicPageInfo

namespace PearlsReview.DAL
{
    public class TopicPageInfo
    {
        public TopicPageInfo() { }

        public TopicPageInfo(int PageID, int TopicID, int Page_Num, string Page_Content)
        {
            this.PageID = PageID;
            this.TopicID = TopicID;
            this.Page_Num = Page_Num;
            this.Page_Content = Page_Content;
        }

        private int _PageID = 0;
        public int PageID
        {
            get { return _PageID; }
            protected set { _PageID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

        private int _Page_Num = 0;
        public int Page_Num
        {
            get { return _Page_Num; }
            private set { _Page_Num = value; }
        }

        private string _Page_Content = "";
        public string Page_Content
        {
            get { return _Page_Content.ToString().Replace(" href=", " target=\"_blank\" href="); }
            private set { _Page_Content = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with TopicPages

        /// <summary>
        /// Returns the total number of TopicPages
        /// </summary>
        public  int GetTopicPageCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Pages", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of TopicPages for a TopicID
        /// </summary>
        public  int GetTopicPageCountByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Pages where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all TopicPages
        /// </summary>
        public  List<TopicPageInfo> GetTopicPages(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Pages";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicPageCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all TopicPages
        /// </summary>
        public  List<TopicPageInfo> GetTopicPagesByTopicID(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                string cSQLCommand = "select * from Pages where TopicID=@TopicID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                cn.Open();
                return GetTopicPageCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the TopicPage with the specified TopicID and Page_Num
        /// </summary>
        public  TopicPageInfo GetTopicPageByTopicIDAndPageNum(int TopicID, int Page_Num)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Pages where TopicID=@TopicID and Page_Num=@Page_Num ", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@Page_Num", SqlDbType.Int).Value = Page_Num;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicPageFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the TopicPage with the specified ID
        /// </summary>
        public  TopicPageInfo GetTopicPageByID(int PageID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Pages where PageID=@PageID ", cn);
                cmd.Parameters.Add("@PageID", SqlDbType.Int).Value = PageID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicPageFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a TopicPage
        /// </summary>
        public  bool DeleteTopicPage(int PageID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Pages where PageID=@PageID", cn);
                cmd.Parameters.Add("@PageID", SqlDbType.Int).Value = PageID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);

                return (ret == 1);

            }
        }

        /// <summary>
        /// Inserts a new TopicPage
        /// </summary>
        public  int InsertTopicPage(TopicPageInfo TopicPage)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Pages " +
              "(TopicID, " +
              "Page_Num, " +
              "Page_Content) " +
              "VALUES (" +
              "@TopicID, " +
              "@Page_Num, " +
              "@Page_Content) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicPage.TopicID;
                cmd.Parameters.Add("@Page_Num", SqlDbType.Int).Value = TopicPage.Page_Num;
                cmd.Parameters.Add("@Page_Content", SqlDbType.Text).Value = TopicPage.Page_Content;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a TopicPage
        /// </summary>
        public  bool UpdateTopicPage(TopicPageInfo TopicPage)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Pages set " +
              "TopicID = @TopicID, " +
              "Page_Num = @Page_Num, " +
              "Page_Content = @Page_Content " +
              "where PageID = @PageID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicPage.TopicID;
                cmd.Parameters.Add("@Page_Num", SqlDbType.Int).Value = TopicPage.Page_Num;
                cmd.Parameters.Add("@Page_Content", SqlDbType.Text).Value = TopicPage.Page_Content;
                cmd.Parameters.Add("@PageID", SqlDbType.Int).Value = TopicPage.PageID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new TopicPageInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual TopicPageInfo GetTopicPageFromReader(IDataReader reader)
        {
            return GetTopicPageFromReader(reader, true);
        }
        protected virtual TopicPageInfo GetTopicPageFromReader(IDataReader reader, bool readMemos)
        {
            TopicPageInfo TopicPage = new TopicPageInfo(
              (int)reader["PageID"],
              (int)reader["TopicID"],
              (int)reader["Page_Num"],
              reader["Page_Content"].ToString());


            return TopicPage;
        }

        /// <summary>
        /// Returns a collection of TopicPageInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<TopicPageInfo> GetTopicPageCollectionFromReader(IDataReader reader)
        {
            return GetTopicPageCollectionFromReader(reader, true);
        }
        protected virtual List<TopicPageInfo> GetTopicPageCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TopicPageInfo> TopicPages = new List<TopicPageInfo>();
            while (reader.Read())
                TopicPages.Add(GetTopicPageFromReader(reader, readMemos));
            return TopicPages;
        }
        #endregion
    }
}
#endregion