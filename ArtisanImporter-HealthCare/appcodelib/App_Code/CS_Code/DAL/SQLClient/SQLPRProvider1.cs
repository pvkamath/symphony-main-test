﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider1
    /// </summary>
    public class SQLPRProvider1 : PRProvider1
    {
        public SQLPRProvider1()
        {
        }

//        /// <summary>
//        /// Updates a Department
//        /// </summary>
//        public override bool UpdateDepartment(DepartmentInfo departmentInfo)
//        {

//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update Dept SET " +
//              "dept_name= @dept_name, " +
//              "dept_abbrev = @dept_abbrev, " +
//              "dept_number = @dept_number, " +
//              "dept_bed_total =@dept_bed_total, " +
//              "facilityid =@facilityid, " +
//              "dept_active_ind =@dept_active_ind " +
//              "where dept_id = @dept_id ", cn);
//                cmd.Parameters.Add("@dept_id", SqlDbType.Int).Value = departmentInfo.dept_id;
//                cmd.Parameters.Add("@dept_name", SqlDbType.VarChar).Value = departmentInfo.dept_name;
//                cmd.Parameters.Add("@dept_abbrev", SqlDbType.VarChar).Value = departmentInfo.dept_abbrev == null ? DBNull.Value as Object : departmentInfo.dept_abbrev as Object;
//                cmd.Parameters.Add("@dept_number", SqlDbType.VarChar).Value = departmentInfo.dept_number == null ? DBNull.Value as object : departmentInfo.dept_number as object;
//                cmd.Parameters.Add("@dept_bed_total", SqlDbType.Int).Value = departmentInfo.dept_bed_total == null ? DBNull.Value as object : departmentInfo.dept_bed_total as object;
//                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = departmentInfo.facilityid == null ? DBNull.Value as Object : departmentInfo.facilityid as object;
//                cmd.Parameters.Add("@dept_active_ind", SqlDbType.Bit).Value = departmentInfo.dept_active_ind;

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }
//        /// <summary>a
//        /// Inserts the Department
//        /// </summary>
//        public override int InsertDepartment(DepartmentInfo departmentInfo)
//        {

//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("insert into Dept " +
//              "(dept_name," +
//              " dept_abbrev, " +
//              "dept_number, " +
//              "dept_bed_total, " +
//              "facilityid, " +
//              "dept_active_ind) " +
//              "VALUES (" +
//              "@dept_name, " +
//              "@dept_abbrev, " +
//              "@dept_number, " +
//              "@dept_bed_total, " +
//              "@facilityid, " +
//              "@dept_active_ind) SET @ID = SCOPE_IDENTITY()", cn);
//                cmd.Parameters.Add("@dept_name", SqlDbType.VarChar).Value = departmentInfo.dept_name;
//                cmd.Parameters.Add("@dept_abbrev", SqlDbType.VarChar).Value = departmentInfo.dept_abbrev == null ? DBNull.Value as Object : departmentInfo.dept_abbrev as Object;
//                cmd.Parameters.Add("@dept_number", SqlDbType.VarChar).Value = departmentInfo.dept_number == null ? DBNull.Value as Object : departmentInfo.dept_number as Object;
//                cmd.Parameters.Add("@dept_bed_total", SqlDbType.VarChar).Value = departmentInfo.dept_bed_total == null ? DBNull.Value as Object : departmentInfo.dept_bed_total as Object; ;
//                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = departmentInfo.facilityid == null ? DBNull.Value as Object : departmentInfo.facilityid as Object;
//                cmd.Parameters.Add("@dept_active_ind", SqlDbType.Bit).Value = departmentInfo.dept_active_ind;

//                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//                IDParameter.Direction = ParameterDirection.Output;
//                cmd.Parameters.Add(IDParameter);

//                cn.Open();
//                cmd.ExecuteNonQuery();

//                int NewID = (int)IDParameter.Value;
//                return NewID;

//                //throw new NotImplementedException();
//            }
//        }
//        /// <summary>
//        /// Deletes the Department
//        /// </summary>
//        public override bool DeleteDepartment(int departmentID)
//        {

//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("delete from Dept where dept_id=@dept_id", cn);
//                cmd.Parameters.Add("@dept_id", SqlDbType.Int).Value = departmentID;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }


//            // throw new NotImplementedException();
//        }

//        public override List<DepartmentInfo> GetDepartments(string fac_id, string sortExpression)
//        {
//            List<DepartmentInfo> departments = null;
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "SELECT dept_name,dept_id,dept_abbrev, dept_number, dept_bed_total, " +
//                                        "facilityid, dept_active_ind  FROM Dept";

//                if (fac_id.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand + " where facilityid= " + fac_id;
//                }

//                // add on ORDER BY if provided
//                if (sortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + sortExpression;
//                }
//                else
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by dept_name";
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                SqlDataReader reader = cmd.ExecuteReader();
//                departments = new List<DepartmentInfo>();
//                while (reader.Read())
//                {
//                    DepartmentInfo department = new DepartmentInfo();
//                    department.dept_name = reader["dept_name"].ToString();
//                    department.dept_id = reader["dept_id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["dept_id"]);
//                    department.dept_abbrev = reader["dept_abbrev"].ToString();
//                    department.dept_number = reader["dept_number"].ToString();
//                    department.dept_bed_total = reader["dept_bed_total"] == DBNull.Value ? 0 : Convert.ToInt32(reader["dept_bed_total"]);
//                    department.facilityid = reader["facilityid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["facilityid"]);
//                    department.dept_active_ind = Convert.ToBoolean(reader["dept_active_ind"]);

//                    departments.Add(department);
//                }
//            }
//            return departments;
//        }

//        public override UserAccountInfo1 GetUserAccountByUserName(string UserName)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                //string cSQLCommand = "select " +
//                //    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                //    "cExpires, cFirstName, cFloridaNo, " +
//                //    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                //    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                //    "SpecID, LCUserName, " +
//                //    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                //    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                //    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                //    "RegTypeID, RepID ,Work_Phone, Badge_ID, "+
//                //    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                //    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID " +
//                //    "from UserAccount where lcUserName Like @UserName";
//                string cSQLCommand = "select " +
//                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                    "cExpires, cFirstName, cFloridaNo, " +
//                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                    "SpecID, LCUserName, " +
//                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                    "RegTypeID, RepID ,Work_Phone, Badge_ID, " +
//                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                    "from UserAccount where lcUserName = @UserName";

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName.ToLower();
//                cn.Open();
//                return GetUserAccountFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        // NOTE; The following method is used only for GiftCard login, and uses the "short"
//        // version of the username with no faciltiyid attached to it.
//        // This works because gift card codes are unique regardless of any facility related to it.
//        public override UserAccountInfo1 GetUserAccountByGiftCardRedemptionCode(string RedemptionCode)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select " +
//                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
//                    "cExpires, cFirstName, cFloridaNo, " +
//                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
//                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
//                    "SpecID, LCUserName, " +
//                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
//                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
//                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
//                    "RegTypeID, RepID ,Work_Phone, Badge_ID, " +
//                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
//                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
//                    "from UserAccount where cUserName = @RedemptionCode and giftcard_ind = '1'";

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cmd.Parameters.Add("@RedemptionCode", SqlDbType.VarChar).Value = RedemptionCode.ToLower();
//                cn.Open();
//                return GetUserAccountFromReader(ExecuteReader(cmd), false);
//            }
//        }
//        /// Returns the UserID for a UserName
//        /// </summary>
//        public override string GetUserNameByID(int userID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("select cusername from UserAccount where iID=@userID", cn);
//                cmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;
//                cn.Open();

//                //TODO: Handle NULL result
//                return Convert.ToString(ExecuteScalar(cmd));
//            }
//        }
//        public override bool Switchfacility(int iID, int facid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                cn.Open();
//                SqlCommand cmd = cn.CreateCommand();
//                SqlTransaction tran = null;

//                try
//                {
//                    tran = cn.BeginTransaction(IsolationLevel.ReadCommitted);
//                    cmd.Connection = cn;
//                    cmd.Transaction = tran;
//                    cmd.CommandText = "SELECT lcusername, cusername, dept_id FROM UserAccount WHERE iID=@userID";
//                    cmd.Parameters.Clear();
//                    cmd.Parameters.Add("userID", SqlDbType.Int).Value = iID;
//                    string lcUsername = "", cUsername = "";
//                    string deptid = "";
//                    using (SqlDataReader dr = cmd.ExecuteReader())
//                    {
//                        while (dr.Read())
//                        {
//                            lcUsername = dr["lcusername"].ToString();
//                            cUsername = dr["cusername"].ToString();
//                            deptid = dr["dept_id"].ToString();
//                            break;
//                        }
//                        dr.Close();
//                    }
//                    cmd.CommandText = "UPDATE UserAccount SET facilityid=@facilityid,lcusername=@newUsername,dept_id =  null ,position_id =null where iID=@iID";
//                    cmd.Parameters.Clear();
//                    cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;
//                    cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facid;
//                    cmd.Parameters.Add("@newUsername", SqlDbType.VarChar).Value = facid.ToString().PadLeft(5, '0') + cUsername;
//                    ExecuteNonQuery(cmd);

//                    cmd.CommandText = "DELETE from Manage where userid=@iID ";
//                    cmd.Parameters.Clear();
//                    cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;
//                    // cmd.Parameters.Add("@dept_id", SqlDbType.VarChar).Value = deptid;                    
//                    ExecuteNonQuery(cmd);

//                    cmd.CommandText = "UPDATE UsersInRoles SET username=@username, facilityid=@facilityid where username=@lcusername";
//                    cmd.Parameters.Clear();
//                    cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = facid.ToString().PadLeft(5, '0') + cUsername;
//                    cmd.Parameters.Add("@lcusername", SqlDbType.VarChar).Value = lcUsername;
//                    cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facid;
//                    ExecuteNonQuery(cmd);
//                    tran.Commit();
//                    return true;
//                }

//                catch (Exception ex)
//                {
//                    tran.Rollback();
//                    return false;
//                }
//                finally
//                {
//                    if (cn.State == ConnectionState.Open)
//                        cn.Close();
//                    tran.Dispose();
//                    cn.Dispose();

//                }
//                return true;
//            }

//        }

//        ////       SelectCommand="SELECT clastname + ',' + cfirstname AS FullName, cfirstname, clastname, COUNT(cfirstname) AS OCR FROM UserAccount WHERE (cfirstname + ' ' + clastname &lt;&gt; '') AND (cfirstname + ' ' + clastname &lt;&gt; '. .') GROUP BY cfirstname, clastname HAVING (COUNT(cfirstname) &gt;= 2) ORDER BY cfirstname">
//        //public override System.Data.DataSet GetDupUsersByFnameAndLname()
//        //{
//        //    string cSQLCommand = "SELECT clastname + ',' + cfirstname AS FullName, "+
//        //            "cfirstname, clastname, COUNT(cfirstname) AS OCR FROM UserAccount "+
//        //            " WHERE (cfirstname + ' ' + clastname &lt;&gt; '') AND (cfirstname + ' ' + clastname &lt;&gt; '. .') "+
//        //            " GROUP BY cfirstname, clastname HAVING (COUNT(cfirstname) &gt;= 2) ORDER BY cfirstname";

//        //    SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//        //    //cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName.ToLower();
//        //    cn.Open();

//        //    return  GetUserAccountFromReader(ExecuteReader(cmd), false);

//        //}

//        public override bool MergeUsers(string strMasteriID, string fname, string lname, List<string> recsToMerge)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                cn.Open();
//                SqlCommand cmd = cn.CreateCommand();
//                SqlTransaction tran;

//                tran = cn.BeginTransaction(IsolationLevel.ReadCommitted);
//                cmd.Connection = cn;
//                cmd.Transaction = tran;
//                int ret = 0;
//                try
//                {
//                    string strUpdateQry = "";
//                    foreach (string striID in recsToMerge)
//                    {
//                        strUpdateQry = "UPDATE Test SET [userid] = " + strMasteriID +
//                                  " WHERE [userid] = " + striID;
//                        //in (SELECT UserAccount.iid FROM UserAccount " +
//                        //          " Left JOIN Test ON UserAccount.iid = Test.userid " +
//                        //          " WHERE (UserAccount.cfirstname = '" + fname + "') " +
//                        //          " AND (UserAccount.clastname = '" + lname + "') " +
//                        //          " AND (UserAccount.iid <> " + strMasteriID + ") AND (Test.status ='C'))";

//                        cmd.CommandText = strUpdateQry;
//                        ret = ExecuteNonQuery(cmd);
//                        cmd.Dispose();

//                        cmd.CommandText = "delete from test where userid = " + striID;
//                        ret = ExecuteNonQuery(cmd);
//                        cmd.Dispose();

//                        cmd.CommandText = "DELETE FROM UserAccount WHERE iID ='" + striID + "'";
//                        ret = ExecuteNonQuery(cmd);
//                        cmd.Dispose();
//                    }
//                    tran.Commit();
//                    return true;
//                }

//                catch (Exception ex)
//                {
//                    tran.Rollback();
//                    return false;
//                }
//                finally
//                {
//                    if (cn.State == ConnectionState.Open)
//                        cn.Close();
//                    tran.Dispose();
//                    cn.Dispose();

//                }
//                return true;
//            }
//        }

//        public override System.Data.DataSet GetMergeRecs(string fname, string lname, string facilityid)
//        {
//            System.Data.DataSet ds = new DataSet();
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                //string qry = "SELECT Test.testid,UserAccount.iID,UserAccount.cusername, UserAccount.cfirstname, UserAccount.clastname, UserAccount.facilityid," +
//                //" (SELECT TOP (1) Dept.dept_name FROM Dept INNER JOIN UserAccount U ON Dept.dept_id = U.dept_id WHERE (U.cfirstname = UserAccount.cfirstname) AND (U.clastname = UserAccount.clastname) " +
//                //" AND (U.facilityid = UserAccount.facilityid )) as DeptName,(SELECT Roles.displayname FROM Roles INNER JOIN UsersInRoles ON UsersInRoles.rolename = Roles.rolename " +
//                //" WHERE (UsersInRoles.facilityid = UserAccount.facilityid) AND (UsersInRoles.username = UserAccount.lcUsername)) as AccessType, UserAccount.isapproved,Test.status, " +
//                //" Test.status FROM UserAccount Left JOIN Test ON UserAccount.iid = Test.userid WHERE " +
//                //" (UserAccount.cfirstname = @FirstName) AND (UserAccount.clastname = @LastName) AND (UserAccount.facilityid = @facid ) ";

               

//                string qry = "SELECT distinct Useraccount.iid, UserAccount.cusername,UserAccount.cfirstname, UserAccount.clastname, UserAccount.facilityid , " +
//                              " (SELECT TOP (1) Dept.dept_name FROM Dept INNER JOIN UserAccount U ON Dept.dept_id = U.dept_id WHERE (U.cfirstname = UserAccount.cfirstname) AND (U.clastname = UserAccount.clastname) " +
//                              " AND (U.facilityid = UserAccount.facilityid )) as DeptName, " +
//                              " (SELECT Roles.displayname FROM Roles INNER JOIN UsersInRoles ON UsersInRoles.rolename = Roles.rolename " +
//                              " WHERE (UsersInRoles.facilityid = UserAccount.facilityid) AND (UsersInRoles.username = UserAccount.lcUsername)) as AccessType, UserAccount.isapproved FROM UserAccount WHERE (UserAccount.cfirstname = @FirstName) AND (UserAccount.clastname = @LastName) AND (UserAccount.facilityid = @facid ) ";


//                qry = qry.Replace("@FirstName", "'" + fname + "'");
//                qry = qry.Replace("@LastName", "'" + lname + "'");
//                qry = qry.Replace("@facid", facilityid);
//                //qry = qry.Replace("@isapproved", 1);
//                //qry = qry.Replace("@islocked", 0);

//                SqlCommand cmd = new SqlCommand(qry, cn);

//                cn.Open();
//                SqlDataAdapter adap = new SqlDataAdapter(cmd);
//                adap.Fill(ds, "MergeRecs");
//            }
//            return ds;
//        }

//        public override System.Data.DataSet GetAssetUrlByTopicID(string TopicID)
//        {
//            System.Data.DataSet ds = new DataSet();
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string qry = "SELECT mcid, asseturl, description FROM Mediacontent WHERE (topicid = " + TopicID+") order by description";
//                SqlCommand cmd = new SqlCommand(qry, cn);

//                //cn.Open();
//                SqlDataAdapter adap = new SqlDataAdapter(cmd);
//                adap.Fill(ds, "AssetUrl");
//            }
//            return ds;
//        }
       

//        //Deletes only the User who do not have any Transcripts.   
   
//        //1/27/2009-Swetha- Revised and set facility to 1 when a user deletes the trancript as per Zhongs suggestion

//        public override bool DeleteUser(int iID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                cn.Open();
//                SqlCommand cmd = cn.CreateCommand();
//                SqlTransaction tran;

//                tran = cn.BeginTransaction(IsolationLevel.ReadCommitted);
//                cmd.Connection = cn;
//                cmd.Transaction = tran;

//                //try
//                //{
//                //    cmd.CommandText = "Select top(1) Status from Test where userID = @userID";
//                //    cmd.Parameters.Clear();
//                //    cmd.Parameters.Add("@userID", SqlDbType.Int).Value = iID;
//                //    //IDataReader dr = ExecuteReader(cmd);
//                //    string retStatus = "";
//                //    retStatus = Convert.ToString(cmd.ExecuteScalar());

//                //    //while (dr.Read())
//                //    //{
//                //    //    retStatus=dr["Status"].ToString();
//                //    //}
//                //    //dr.Close();
//                //    //dr.Dispose();
//                //    cmd.Dispose();

//                //    if (retStatus != "C")
//                //    {

//                        cmd.CommandText = "UPDATE UserAccount SET facilityid=1 where iID=@iID";
//                        cmd.Parameters.Clear();
//                        cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;
//                        ExecuteNonQuery(cmd);
//                //    }
//               tran.Commit();
//                //    return true;
//                //}

//                //catch (Exception ex)
//                //{
//                //    tran.Rollback();
//                //    return false;
//                //}
//                //finally
//                //{
//                //    if (cn.State == ConnectionState.Open)
//                //        cn.Close();
//                //    tran.Dispose();
//                //    cn.Dispose();

//                }
//                return true;
//            }

       
//        ////Access Type

//        // 11/20/2008 David Stevenson -- not sure if we need this one now

//        //public override bool InsertUser(string username, string firstname, string middlename, string lastname, string title, string password, string address1, string address2, string city, string state, string zip, string homephone, string workphone, string birthdate, string badgeid, string ssn, string hiredate, string termindate, int deptid, int positionid, string pwdqs, string pwdans, string rolename, int facilityid)
//        //{
//        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//        //    {
//        //        cn.Open();
//        //        SqlCommand cmd = cn.CreateCommand();
//        //        SqlTransaction tran;

//        //        tran = cn.BeginTransaction(IsolationLevel.ReadCommitted);
//        //        cmd.Connection = cn;
//        //        cmd.Transaction = tran;

//        //        try
//        //        {
//        //            cmd.CommandText = "Insert into UsersInRoles(rolename,username,facilityid) Values (@rolename,@username,@facilityid)";
//        //            cmd.Parameters.Clear();
//        //            cmd.Parameters.Add("@rolename", SqlDbType.VarChar).Value = rolename;
//        //            cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = username;
//        //            cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
//        //            ExecuteNonQuery(cmd);

//        //            //cmd.CommandText = "Insert into UserAccount(cusername,cfirstname,cmiddle,clastname,cemail,caddress1,caddress2,ccity,cstate,czipcode,cphone,cpw,cbirthdate,pwquest,pwans,work_phone,badge_id,hire_date,termin_date,dept_id,position_id,title,uniqueid,"+
//        //            //"cexpires,ladmin,lprimary,cfloridano,cinstitute,clectdate,cphone,cpromocode,crefcode,cregtype,csocial,cbirthdate,lcusername,"+
//        //            //"passfmt,passalt,mobilepin,lcemail,pwquest,pwans,isapproved,isonline,islocked,xpwatt) Values (@username, @firstname, @middlename, @lastname,@email, @address1, @address2, @city, @state, @zip, @phone, @password, @birthdate, @pwquest, @pwans, @workphone, @badge_id, @hireDate, @termin_date, @dept_id, @position_id, @title, @uniqueID,"+
//        //            //    "10/28/2008,0,0,1,1,1,1,1,1,1,0000,1,1,"+
//        //            //"1,1,1,1,0,0,1,0,0,1)";

//        //            cmd.CommandText = "INSERT INTO [ECE].[dbo].[UserAccount]([cusername],[cfirstname],[cmiddle],[clastname],[caddress1],[caddress2],[ccity],[cstate],[czipcode],[cemail],[cexpires],[ladmin],[lprimary],[cfloridano],[cinstitute],[clectdate],[cphone],[cpromocode],[promoid],[facilityid],[cpw],[crefcode],[cregtype],[csocial],[cbirthdate],[specid],[lcusername],[lastact],[passfmt],[passsalt],[mobilepin],[lcemail],[pwquest],[pwans],[isapproved],[isonline],[islocked],[lastlogin],[lastpwchg],[lastlock],[xpwatt],[xpwattst],[xpwansatt],[xpwansst],[comment],[created],[agegroup],[regtypeid],[repid],[work_phone],[badge_id],[hire_date],[termin_date],[dept_id],[position_id],[clinical_ind],[title],[giftcard_ind],[giftcard_chour],[giftcard_uhour],[uniqueid])VALUES" +
//        //                  "('username','fn','cmiddle','ln','asd','dff','il','il',60156,'kk@CE.COM','10/28/2008',1,1,'fl','ins','elc','2246782234','prm',1,3197,'manju','er'" +
//        //                           ",'ert','0000','rtty',1,'yser','10/28/2008','psee','pass','224','kk@CE.COM','ertt','ertyy',1,1,1,'10/28/2008','10/28/2008','10/28/2008',1,'10/28/2008',1,'10/28/2008','errty','10/28/2008',1,1,1,'','12344','10/28/2008','10/28/2008',1,1,0,'title',1,0.22,0.33,10)";
//        //            //cmd.CommandText = "Insert into UserAccount(cusername,cfirstname,cmiddle,clastname,caddress1,caddress2,ccity,cstate,czipcode,cphone,cpw,cbirthdate,pwquest,pwans,work_phone,badge_id,hire_date,termin_date,dept_id,position_id,title,uniqueid)"+
//        //            //    "Values (@username, @firstname, @middlename, @lastname, @address1, @address2, @city, @state, @zip, @phone, @password, @birthdate, @pwquest, @pwans, @workphone, @badge_id, @hireDate, @termin_date, @dept_id, @position_id, @title, @uniqueID)";

//        //            cmd.Parameters.Clear();
//        //            cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = username;
//        //            cmd.Parameters.Add("@firstname", SqlDbType.VarChar).Value = firstname;
//        //            cmd.Parameters.Add("@middlename", SqlDbType.VarChar).Value = middlename;
//        //            cmd.Parameters.Add("@lastname", SqlDbType.VarChar).Value = lastname;
//        //            cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = "swethaa@ce.com";
//        //            cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = address1;
//        //            cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = address2;
//        //            cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = city;
//        //            cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = state;
//        //            cmd.Parameters.Add("@zip", SqlDbType.VarChar).Value = zip;
//        //            cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = homephone;
//        //            cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = password;
//        //            cmd.Parameters.Add("@birthdate", SqlDbType.VarChar).Value = birthdate;
//        //            cmd.Parameters.Add("@pwquest", SqlDbType.VarChar).Value = pwdqs;
//        //            cmd.Parameters.Add("@pwans", SqlDbType.VarChar).Value = pwdans;
//        //            cmd.Parameters.Add("@workphone", SqlDbType.VarChar).Value = workphone;
//        //            cmd.Parameters.Add("@badge_id", SqlDbType.VarChar).Value = badgeid;
//        //            cmd.Parameters.Add("@hireDate", SqlDbType.VarChar).Value = hiredate;
//        //            cmd.Parameters.Add("@termin_date", SqlDbType.VarChar).Value = termindate;
//        //            cmd.Parameters.Add("@dept_id", SqlDbType.Int).Value = deptid;
//        //            cmd.Parameters.Add("@position_id", SqlDbType.Int).Value = positionid;
//        //            cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = title;
//        //            cmd.Parameters.Add("@uniqueID", SqlDbType.VarChar).Value = 12211;

//        //            ExecuteNonQuery(cmd);

//        //            tran.Commit();
//        //            return true;
//        //        }
//        //        catch (Exception ex)
//        //        {
//        //            tran.Rollback();
//        //            return false;
//        //        }
//        //        finally
//        //        {
//        //            if (cn.State == ConnectionState.Open)
//        //                cn.Close();
//        //            tran.Dispose();
//        //            cn.Dispose();

//        //        }
//        //        return true;
//        //    }
//        //}

//        public override bool DeActivateUser(int userID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update UserAccount SET " +
//                    "IsApproved='false' ," +
//                    " IsLocked = 'true'" +
//                    " where iID =@userID", cn);
//                cmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;

//                try
//                {
//                    cn.Open();
//                    ExecuteNonQuery(cmd);
//                    return true;
//                }
//                catch
//                {
//                    return false;
//                }
//            }
//        }
//        public override bool ActivateUser(int userID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update UserAccount SET " +
//                    "IsApproved='true' ," +
//                    " IsLocked ='false'" +
//                    " where iID =@userID", cn);
//                cmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;

//                try
//                {
//                    cn.Open();
//                    ExecuteNonQuery(cmd);
//                    return true;
//                }
//                catch
//                {
//                    return false;
//                }
//            }
//        }
//        public override int GetUserAccountsCount(string firstName, string lastName,
//                string cUserName, int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select distinct " +
//                      "iID, cFirstName, cLastName, (select facname from facilitygroup where facid=u.facilityid) as facname, cUserName, " +
//                      "IsApproved,Badge_ID, (select dept_name from dept where dept_id=u.dept_id) as Dept_Name, cSocial, giftcard_ind " +
//                      "from UserAccount u";

//                string whereClause = string.Empty;
//                if (!String.IsNullOrEmpty(firstName))
//                    whereClause += "or cFirstName like '" + firstName.Replace("'", "''") + "%' ";
//                if (!String.IsNullOrEmpty(lastName))
//                    whereClause += "or cLastName like '" + lastName.Replace("'", "''") + "%' ";
//                if (!String.IsNullOrEmpty(cUserName))
//                    whereClause += "or cUserName like '" + cUserName.ToLower().Replace("'", "''") + "%' ";
//                //if (iID != null && iID > 0)
//                // whereClause += "or iID like '" + iID + "%' ";
//                if (facilityid != null && facilityid > 0)
//                    whereClause += "or u.facilityid = " + facilityid + " ";
//                if (!String.IsNullOrEmpty(badge_id))
//                    whereClause += "or Badge_ID like '" + badge_id.Replace("'", "''") + "%' ";
//                if (!String.IsNullOrEmpty(cSocial))
//                    whereClause += "or cSocial like '" + cSocial.Replace("'", "''") + "%' ";
//                if (Dept_ID != null && Dept_ID > 0)
//                    whereClause += "or u.dept_id = " + Dept_ID + " ";
//                if (isApproved)
//                    whereClause += "or IsApproved = 'true'";
//                else if (isNotApproved)
//                    whereClause += "or IsApproved = 'false'";

//                if (whereClause != string.Empty)
//                    whereClause = " where " + whereClause.Substring(2);

//                cSQLCommand += whereClause;

//                string sql = "select count(*) from (" + cSQLCommand + ") as temp";

//                SqlCommand cmd = new SqlCommand(sql, cn);

//                cn.Open();
//                return Convert.ToInt32(ExecuteScalar(cmd));
//            }
//        }
//        public override List<UserAccountInfo1> GetUserAccountsBySearchCriteria(string cSortExpression, string firstName, string lastName, string cUserName,
//            int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID, string rolename, int startRowIndex, int maximumRows)
//        {
//            string qry = "";
//            string subqry = "";
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                if (facilityid != 0)
//                {
//                    subqry = "select distinct iID,cFirstName,cLastName,(select facname from FacilityGroup where facid=u.facilityid) as facname," +
//                       "(select dept_name from Dept where dept_id=u.dept_id and facilityid=u.facilityid) as Dept_Name," +
//                       "(select Top 1 Roles.displayname from UsersInRoles INNER JOIN Roles ON UsersInRoles.rolename = Roles.rolename where username = u.lcusername and " +
//                       "(UsersInRoles.facilityid = u.facilityid ) order by priorder) AS Roles," + "cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from UserAccount u where (u.facilityid = " + facilityid + ")";
//                }
//                else
//                {
//                    subqry = "select distinct iID,cFirstName,cLastName,(select facname from FacilityGroup where facid=u.facilityid) as facname," +
//                             "(select dept_name from Dept where dept_id=u.dept_id and facilityid=u.facilityid) as Dept_Name," +
//                             "(select Top 1 Roles.displayname from UsersInRoles INNER JOIN Roles ON UsersInRoles.rolename = Roles.rolename where username = u.lcusername and " +
//                             "(UsersInRoles.facilityid = u.facilityid ) order by priorder) AS Roles," + "cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from UserAccount u where (u.facilityid <> 0 )";


//                }
//                if (Dept_ID > 0)
//                {
//                    string appendDeptCond = " and (u.dept_id =" + Dept_ID + ")";
//                    subqry += appendDeptCond;
//                }
//                if ((firstName.Length > 0) & (firstName != null))
//                {
//                    subqry += "and (u.cFirstName Like '" + firstName.Replace("'", "''") + "%')";
//                }
//                if ((lastName.Length > 0) & (lastName != null))
//                {
//                    subqry += "and (u.cLastName Like '" + lastName.Replace("'", "''") + "%')";
//                }
//                if ((cUserName.Length > 0) & (cUserName != null))
//                {
//                    subqry += "and (u.cUserName Like '" + cUserName.ToLower().Replace("'", "''") + "%')";
//                }
//                if ((cSocial.Length > 0) & (cSocial != null))
//                {
//                    subqry += "and (u.cSocial Like '" + cSocial.Replace("'", "''") + "%')";
//                }

//                if ((badge_id.Length > 0) & (badge_id != null))
//                {
//                    subqry += "and (u.Badge_ID Like '" + badge_id.Replace("'", "''") + "%')";
//                }
//                if ((isApproved))
//                {
//                    subqry += "and (u.IsApproved ='true')";
//                }
//                if ((isNotApproved))
//                {
//                    subqry += "and (u.IsApproved = 'false' )";
//                }

//                qry = "select iID,cFirstName,cLastName,facname,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from (" + subqry + ") as result where (Roles <> ' ') ";

//                //if ((rolename.Length > 1) & rolename != null)
//                //{
//                //    string appendRoleStr = " and ( Roles Like '" + rolename + "%')";
//                //    qry += appendRoleStr;
//                //}

//                if ((rolename.Length > 1) & rolename != null & rolename != "All AccessTypes")
//                {
//                    string appendRoleStr = " and ( Roles Like '" + rolename + "%') Order by cLastName,cFirstName,iID,facname,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind ";
//                    qry += appendRoleStr;
//                }
//                else
//                {
//                    string appendOrderStr = " Order by  cLastName,cFirstName,iID,facname,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind ";
//                    qry += appendOrderStr;


//                }
//                SqlCommand cmd = new SqlCommand(qry, cn);
//                //SqlParameter[] parms = new SqlParameter[] {
//                //        new SqlParameter("sortExpression", cSortExpression),
//                //        new SqlParameter("startRowIndex", startRowIndex),
//                //        new SqlParameter("maximumRows", maximumRows)
//                //};
//                //cmd.Parameters.AddRange(parms);

//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd);

//                List<UserAccountInfo1> UserAccounts = new List<UserAccountInfo1>();
//                while (reader.Read())
//                {
//                    UserAccountInfo1 UserAccount = new UserAccountInfo1(
//               (int)reader["iID"],
//               reader["cUserName"].ToString().ToLower(),
//               reader["cFirstName"].ToString(),
//               reader["cLastName"].ToString(),
//               (Convert.ToString(reader["facname"])),
//               (bool)reader["IsApproved"],
//               Convert.ToString(reader["Badge_ID"]),
//               Convert.ToString(reader["Dept_name"]),
//               Convert.ToString(reader["Roles"]),
//               Convert.ToString(reader["cSocial"]),
//               (reader["giftcard_ind"] != DBNull.Value ? (bool)reader["giftcard_ind"] : false)
//                        //(int)(Convert.IsDBNull(reader["UniqueID"]) ?(int)0 : (int)reader["UniqueID"])
//                        );

//                    UserAccounts.Add(UserAccount);
//                }
//                return UserAccounts;
//            }



//        }
//        public override List<UserAccountInfo1> GetUserAccountsBySearchCriteria(string cSortExpression, string firstName, string lastName, string cUserName,
//             int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID, string rolename, int startRowIndex, int maximumRows,
//             int rolepriority, int currentUserID, bool isActiveFacilityManager)
//        {
//            string qry = "";
//            string subqry = "";
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                if (facilityid != 0)
//                {
//                    subqry = "select distinct iID,cFirstName,cLastName,(select facname from FacilityGroup where facid=u.facilityid) as facname," +
//                       "(select dept_name from Dept where dept_id=u.dept_id and facilityid=u.facilityid) as Dept_Name," +
//                       "(select Top 1 Roles.displayname from UsersInRoles INNER JOIN Roles ON UsersInRoles.rolename = Roles.rolename where username = u.lcusername and " +
//                       "(UsersInRoles.facilityid = u.facilityid) and Roles.priorder <= '" + rolepriority + "') AS Roles," + "cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from UserAccount u where (u.facilityid = " + facilityid + ")";
//                }
//                else
//                {
//                    subqry = "select distinct iID,cFirstName,cLastName,(select facname from FacilityGroup where facid=u.facilityid) as facname," +
//                             "(select dept_name from Dept where dept_id=u.dept_id and facilityid=u.facilityid) as Dept_Name," +
//                             "(select Top 1 Roles.displayname from UsersInRoles INNER JOIN Roles ON UsersInRoles.rolename = Roles.rolename where username = u.lcusername and " +
//                             "(UsersInRoles.facilityid = u.facilityid ) and Roles.priorder <= '" + rolepriority + "') AS Roles," + "cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from UserAccount u where (u.facilityid <> 0 )";


//                }
//                if (Dept_ID > 0)
//                {
//                    string appendDeptCond = " and (u.dept_id =" + Dept_ID + ") ";
//                    subqry += appendDeptCond;
//                }
//                else
//                {
//                    if (isActiveFacilityManager)
//                        subqry += "and u.Dept_id IN (SELECT Dept.dept_id FROM Manage INNER JOIN Dept ON Manage.dept_id = Dept.dept_id " +
//                                  "INNER JOIN UserAccount ON Manage.userid = UserAccount.iid " +
//                                  "WHERE UserAccount.iID = " + currentUserID + ") ";
//                    //else
//                    //    subqry += "(SELECT Dept.[dept_id] FROM [Dept] Where " + (facilityid!=0 ? "facilityID=" + facilityid : "facilityID <> 0") + ") ";
//                }
//                if ((firstName.Length > 0) & (firstName != null))
//                {
//                    subqry += "and (u.cFirstName Like '" + firstName.Replace("'", "''") + "%') ";
//                }
//                if ((lastName.Length > 0) & (lastName != null))
//                {
//                    subqry += "and (u.cLastName Like '" + lastName.Replace("'", "''") + "%') ";
//                }
//                if ((cUserName.Length > 0) & (cUserName != null))
//                {
//                    subqry += "and (u.cUserName Like '" + cUserName.ToLower().Replace("'", "''") + "%') ";
//                }
//                if ((cSocial.Length > 0) & (cSocial != null))
//                {
//                    subqry += "and (u.cSocial Like '" + cSocial.Replace("'", "''") + "%') ";
//                }

//                if ((badge_id.Length > 0) & (badge_id != null))
//                {
//                    subqry += "and (u.Badge_ID Like '" + badge_id.Replace("'", "''") + "%') ";
//                }
//                if ((isApproved))
//                {
//                    subqry += "and (u.IsApproved ='true') ";
//                }
//                if ((isNotApproved))
//                {
//                    subqry += "and (u.IsApproved = 'false') ";
//                }

//                qry = "select iID,cFirstName,cLastName,facname,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from (" + subqry + ") as result where (Roles <> ' ') ";

//                //if ((rolename.Length > 1) & rolename != null)
//                //{
//                //    string appendRoleStr = " and ( Roles Like '" + rolename + "%')";
//                //    qry += appendRoleStr;
//                //}

//                if ((rolename.Length > 1) & rolename != null & rolename != "All AccessTypes")
//                {
//                    string appendRoleStr = " and ( Roles Like '" + rolename + "%') Order by cLastName,cFirstName,iID,facname,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind ";
//                    qry += appendRoleStr;
//                }
//                else
//                {
//                    string appendOrderStr = " Order by  cLastName,cFirstName,iID,facname,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind ";
//                    qry += appendOrderStr;


//                }
//                SqlCommand cmd = new SqlCommand(qry, cn);
//                //SqlParameter[] parms = new SqlParameter[] {
//                //        new SqlParameter("sortExpression", cSortExpression),
//                //        new SqlParameter("startRowIndex", startRowIndex),
//                //        new SqlParameter("maximumRows", maximumRows)
//                //};
//                //cmd.Parameters.AddRange(parms);

//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd);

//                List<UserAccountInfo1> UserAccounts = new List<UserAccountInfo1>();
//                while (reader.Read())
//                {
//                    UserAccountInfo1 UserAccount = new UserAccountInfo1(
//               (int)reader["iID"],
//               reader["cUserName"].ToString().ToLower(),
//               reader["cFirstName"].ToString(),
//               reader["cLastName"].ToString(),
//               (Convert.ToString(reader["facname"])),
//               (bool)reader["IsApproved"],
//               Convert.ToString(reader["Badge_ID"]),
//               Convert.ToString(reader["Dept_name"]),
//               Convert.ToString(reader["Roles"]),
//               Convert.ToString(reader["cSocial"]),
//               (reader["giftcard_ind"] != DBNull.Value ? (bool)reader["giftcard_ind"] : false)
//                        //(int)(Convert.IsDBNull(reader["UniqueID"]) ?(int)0 : (int)reader["UniqueID"])
//                        );

//                    UserAccounts.Add(UserAccount);
//                }
//                return UserAccounts;
//            }
//        }

//        public override List<UserAccountInfo1> GetCERetailUsersBySearchCriteria(string cSortExpression, string cfirstName, string clastName, string cUserName,
//           string cpw, string cAddress1, string cEmail, string license_number, string cState, string State, string cZipCode)
//        {
//            string qry = "";
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                qry = "select distinct iid,cfirstname,clastname,cusername,cpw,caddress1,cemail,ccity,cstate,czipcode,cexpires,created,state,uniqueid,license_number from useraccount u inner join userlicense ul on u.iid = ul.userid where facilityid=2 ";

//                if ((cfirstName.Length > 0) & (cfirstName != null))
//                {
//                    qry += "and (u.cFirstName Like '" + cfirstName.Replace("'", "''") + "%') ";
//                }
//                if ((clastName.Length > 0) & (clastName != null))
//                {
//                    qry += "and (u.cLastName Like '" + clastName.Replace("'", "''") + "%') ";
//                }
//                if ((cUserName.Length > 0) & (cUserName != null))
//                {
//                    qry += "and (u.cUserName Like '" + cUserName.ToLower().Replace("'", "''") + "%') ";
//                }
//                if ((cpw.Length > 0) & (cpw != null))
//                {
//                   qry += "and (u.cpw Like '" + cpw.ToLower().Replace("'", "''") + "%') ";
//                }
//                if ((cAddress1.Length > 0) & (cAddress1 != null))
//                {
//                    qry += "and (u.cAddress1 Like '" + cAddress1.Replace("'", "''") + "%') ";
//                }
//                if ((cEmail.Length > 0) & (cEmail != null))
//                {
//                    qry += "and (u.cEmail Like '" + cEmail.Replace("'", "''") + "%') ";
//                }
                
//                if ((license_number.Length > 0) & (license_number != null))
//                {
//                    qry += "and (ul.license_number Like '" + license_number.Replace("'", "''") + "%') ";
//                }
               
//                if ((cState.Length > 0) & (cState != null))
//                {
//                    qry += "and (u.cState Like '" + cState.Replace("'", "''") + "%') ";
//                }
//                if ((State.Length > 0) & (State != null))
//                {
//                   qry += "and (ul.State Like '" + State.Replace("'", "''") + "%') ";
//                }

//                if ((cZipCode.Length > 0) & (cZipCode != null))
//                {
//                    qry += "and (u.cZipCode Like '" + cZipCode.Replace("'", "''") + "%') ";
//                }

//                qry += " order by clastname,cfirstname";
//                SqlCommand cmd = new SqlCommand(qry, cn);
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd);

//                List<UserAccountInfo1> UserAccounts = new List<UserAccountInfo1>();
//                while (reader.Read())
//                {
//                    DateTime dt = Convert.ToDateTime(reader["created"].ToString());
//                    string strdate = dt.ToShortDateString();
                    
//                    UserAccountInfo1 UserAccount = new UserAccountInfo1(
//               Convert.ToInt32(reader["iID"].ToString()),
//               reader["cfirstName"].ToString(),
//               reader["clastName"].ToString().ToLower(),
//               reader["cUserName"].ToString(),
//               reader["cpw"].ToString(),
//               reader["cAddress1"].ToString(),
//               reader["cEmail"].ToString(),
//               reader["license_number"].ToString(),
//               reader["ccity"].ToString(),
//               reader["cState"].ToString(),
//               Convert.ToDateTime(reader["cexpires"].ToString()),
//               Convert.ToDateTime(strdate),
//               reader["State"].ToString(),
//               Convert.ToInt32(reader["uniqueid"].ToString()),
//               reader["cZipCode"].ToString());
           

//               UserAccounts.Add(UserAccount);
//                }
//                return UserAccounts;
//            }
//        }
//        public override System.Data.DataSet GetNonUnlimitedOrders(string cSortExpression, string cfirstName, string clastName, string cUserName,
//         string cpw, string cAddress1, string cEmail, string license_number, string cState, string state, string cZipCode)

//        {
//            using (SqlConnection sqlConn = new SqlConnection(this.ConnectionString))
//            {
//                string sql = "select distinct firstname,lastname,orderid,city,o.state,zip,orderdate from orders o " +
//                "inner join useraccount u on o.userid=u.iid inner join userlicense ul on u.iid = ul.userid where u.facilityid=2 ";

//                if (!String.IsNullOrEmpty(cfirstName))
//                {
//                    sql += " and o.firstname like '" + cfirstName + "%' ";
//                }
//                if (!String.IsNullOrEmpty(clastName))
//                {
//                    sql += " and o.lastname like '" + clastName + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cUserName))
//                {
//                    sql += " and u.cUserName like '" + cUserName + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cpw))
//                {
//                    sql += " and u.cpw like '" + cpw + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cAddress1))
//                {
//                    sql += " and u.cAddress1 like '" + cAddress1 + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cEmail))
//                {
//                    sql += " and u.cEmail like '" + cEmail + "%' ";
//                }
//                if (!String.IsNullOrEmpty(license_number))
//                {
//                    sql += " and ul.license_number like '" + license_number + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cState))
//                {
//                    sql += " and o.state like '" + cState + "%' ";
//                }
//                if (!String.IsNullOrEmpty(state))
//                {
//                    sql += " and ul.state like '" + state + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cZipCode))
//                {
//                    sql += " and o.zip like '" + cZipCode + "%' ";
//                }
//                sql += " order by lastname,firstname ";
//                sqlConn.Open();
//                SqlCommand sqlSelect = new SqlCommand(sql, sqlConn);
//                sqlSelect.CommandType = System.Data.CommandType.Text;
//                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlSelect);
//                DataSet myDataset = new DataSet();
//                sqlAdapter.Fill(myDataset);
//                sqlConn.Close();
//                return myDataset;
//            }
//        }
//        //inner join histtranscript ht on ht.transcriptid = t.testid

//        public override System.Data.DataSet GetTranscripts(string cSortExpression, string cfirstName, string clastName, string cUserName,
//         string cpw, string cAddress1, string cEmail, string license_number, string cState, string state, string cZipCode)
//        {
//            using (SqlConnection sqlConn = new SqlConnection(this.ConnectionString))
//            {
//                string sql = "select distinct cfirstname,iid,clastname,testid,course_number,name,lastmod from Test t " +
//                             "inner join topic tp on tp.topicid = t.topicid " +
//                             "inner join useraccount u on u.iid = t.userid inner join userlicense ul  on u.iid = ul.userid " +
//                             "where u.facilityid = 2" ;

//                if (!String.IsNullOrEmpty(cfirstName))
//                {
//                    sql += " and cfirstname like '" + cfirstName + "%' ";
//                }
//                if (!String.IsNullOrEmpty(clastName))
//                {
//                    sql += " and clastname like '" + clastName + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cUserName))
//                {
//                    sql += " and u.cUserName like '" + cUserName + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cpw))
//                {
//                    sql += " and u.cpw like '" + cpw + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cAddress1))
//                {
//                    sql += " and u.cAddress1 like '" + cAddress1 + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cEmail))
//                {
//                    sql += " and u.cEmail like '" + cEmail + "%' ";
//                }
//                if (!String.IsNullOrEmpty(license_number))
//                {
//                    sql += " and ul.license_number like '" + license_number + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cState))
//                {
//                    sql += " and cstate like '" + cState + "%' ";
//                }
//                if (!String.IsNullOrEmpty(state))
//                {
//                    sql += " and ul.state like '" + state + "%' ";
//                }
//                if (!String.IsNullOrEmpty(cZipCode))
//                {
//                    sql += " and czipcode like '" + cZipCode + "%' ";
//                }
//                sql += " order by clastname,cfirstname ";
//                sqlConn.Open();
//                SqlCommand sqlSelect = new SqlCommand(sql, sqlConn);
//                sqlSelect.CommandType = System.Data.CommandType.Text;
//                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlSelect);
//                DataSet myDataset = new DataSet();
//                sqlAdapter.Fill(myDataset);
//                sqlConn.Close();
//                return myDataset;
//            }
//        }

//        public override System.Data.DataSet GetDuplicatesByFirstAndLastname(string facilityid)
//        {
//            System.Data.DataSet ds = new DataSet();
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string qry = "SELECT F.clastname + ',' + F.cfirstname AS FullName, F.cfirstname, F.clastname,F.facilityid, " +
//                 "(SELECT Count(e.cfirstname) FROM UserAccount e Inner JOIN Test ON Test.userid =e.iid " +
//                 " WHERE (e.cfirstname = F.cfirstname) AND (e.cfirstname + ' ' +e. clastname <> '') AND (e.cfirstname + ' ' + e.clastname <> '. .') " +
//                 " AND (e.clastname = F.clastname)  AND (e.iid = iID) AND (e.facilityid = @facid ) " +
//                 " ) as ActualCount, COUNT(F.cfirstname) AS OCR FROM UserAccount F WHERE (F.cfirstname + ' ' +  F.clastname <> '') " +
//                 " AND (F.cfirstname + ' ' + F.clastname <> '. .') and (F.facilityid = @facid ) " +
//                 " GROUP BY F.cfirstname, F.clastname, F.facilityid HAVING (COUNT(F.cfirstname) >= 2) ORDER BY F.clastname,F.cfirstname";

//                qry = qry.Replace("@facid", facilityid);

//                SqlCommand cmd = new SqlCommand(qry, cn);

//                cn.Open();
//                SqlDataAdapter adap = new SqlDataAdapter(cmd);
//                adap.Fill(ds, "Duplicates");
//            }
//            return ds;

//        }
//        public override System.Data.DataSet GetDuplicatesByUniqueid(string facilityid)
//        {
//            System.Data.DataSet ds = new DataSet();
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string qry = "SELECT F.clastname + ',' + F.cfirstname AS FullName, F.cfirstname, F.clastname,F.facilityid, " +
//                       "(SELECT Count(e.cfirstname) FROM UserAccount e Inner JOIN Test ON Test.userid =e.iid " +
//                       " WHERE (e.cfirstname = F.cfirstname) AND (e.cfirstname + ' ' +e. clastname <> '') AND (e.cfirstname + ' ' + e.clastname <> '. .')  " +
//                       " AND (e.clastname = F.clastname)  AND (e.iid = iID) AND (e.facilityid = @facid )  " +
//                       " ) as ActualCount, F.cSocial, COUNT(F.cfirstname) AS OCR FROM UserAccount F WHERE (F.cfirstname + ' ' +  F.clastname <> '') " +
//                       " AND (F.cfirstname + ' ' + F.clastname <> '. .') and (F.facilityid = @facid ) " +
//                       " GROUP BY F.cSocial,F.cfirstname, F.clastname,F.facilityid HAVING (COUNT(F.cSocial) >= 2) ORDER BY F.clastname,F.cfirstname ";

//                qry = qry.Replace("@facid", facilityid);

//                SqlCommand cmd = new SqlCommand(qry, cn);

//                cn.Open();
//                SqlDataAdapter adap = new SqlDataAdapter(cmd);
//                adap.Fill(ds, "Duplicates");
//            }
//            return ds;

//        }

//        protected virtual UserAccountInfo1 GetUserAccountFromReader(IDataReader reader, bool readMemos)
//        {
//            UserAccountInfo1 UserAccount = null;
//            while (reader.Read())
//            {
//                UserAccount = new UserAccountInfo1(
//                 (int)reader["iID"],
//                 reader["cAddress1"].ToString(),
//                 reader["cAddress2"].ToString(),
//                 (bool)reader["lAdmin"],
//                 (bool)reader["lPrimary"],
//                 reader["cBirthDate"].ToString(),
//                 reader["cCity"].ToString(),
//                 reader["cEmail"].ToString(),
//                 reader["cExpires"].ToString(),
//                 reader["cFirstName"].ToString(),
//                 reader["cFloridaNo"].ToString(),
//                 reader["cInstitute"].ToString(),
//                 reader["cLastName"].ToString(),
//                 reader["cLectDate"].ToString(),
//                 reader["cMiddle"].ToString(),
//                 reader["cPhone"].ToString(),
//                 reader["cPromoCode"].ToString(),
//                 (int)(Convert.IsDBNull(reader["PromoID"]) ? (int)0 : (int)reader["PromoID"]),
//                 (int)(Convert.IsDBNull(reader["FacilityID"]) ? (int)0 : (int)reader["FacilityID"]),
//                 reader["cPW"].ToString().ToLower(),
//                 reader["cRefCode"].ToString(),
//                 reader["cRegType"].ToString(),
//                 reader["cSocial"].ToString(),
//                 reader["cState"].ToString(),
//                 reader["cUserName"].ToString().ToLower(),
//                 reader["cZipCode"].ToString(),
//                (int)(Convert.IsDBNull(reader["SpecID"]) ? (int)0 : (int)reader["SpecID"]),
//                 reader["LCUserName"].ToString().ToLower(),
//                 (DateTime)(Convert.IsDBNull(reader["LastAct"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastAct"]),
//                 reader["PassFmt"].ToString(),
//                 reader["PassSalt"].ToString(),
//                 reader["MobilePIN"].ToString(),
//                 reader["LCEmail"].ToString(),
//                 reader["PWQuest"].ToString(),
//                 reader["PWAns"].ToString().ToLower(),
//                 (bool)reader["IsApproved"],
//                 (bool)reader["IsOnline"],
//                 (bool)reader["IsLocked"],
//                 (DateTime)(Convert.IsDBNull(reader["LastLogin"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLogin"]),
//                 (DateTime)(Convert.IsDBNull(reader["LastPWChg"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastPWChg"]),
//                (DateTime)(Convert.IsDBNull(reader["LastLock"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLock"]),
//                 (int)reader["XPWAtt"],
//                 (DateTime)(Convert.IsDBNull(reader["XPWAttSt"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAttSt"]),
//                 (int)reader["XPWAnsAtt"],
//                 (DateTime)(Convert.IsDBNull(reader["XPWAnsSt"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAnsSt"]),
//                 (Convert.IsDBNull(reader["Comment"]) ? string.Empty : reader["Comment"].ToString()),
//                 (DateTime)(Convert.IsDBNull(reader["Created"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Created"]),
//                 (int)(Convert.IsDBNull(reader["AgeGroup"]) ? (int)0 : (int)reader["AgeGroup"]),
//                 (int)(Convert.IsDBNull(reader["RegTypeID"]) ? (int)0 : (int)reader["RegTypeID"]),
//                 (int)(Convert.IsDBNull(reader["RepID"]) ? (int)0 : (int)reader["RepID"]),
//                 (Convert.IsDBNull(reader["Work_Phone"]) ? string.Empty : reader["Work_Phone"].ToString()),
//                 (Convert.IsDBNull(reader["Badge_ID"]) ? string.Empty : reader["Badge_ID"].ToString()),
//                 (DateTime)(Convert.IsDBNull(reader["Hire_Date"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Hire_Date"]),
//                 (DateTime)(Convert.IsDBNull(reader["Termin_Date"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Termin_Date"]),
//                 (int)(Convert.IsDBNull(reader["Dept_ID"]) ? (int)0 : (int)reader["Dept_ID"]),
//                 (int)(Convert.IsDBNull(reader["Position_ID"]) ? 0 : (int)reader["Position_ID"]),
//                 (int)(Convert.IsDBNull(reader["Clinical_Ind"]) ? 0 : Convert.ToInt32(reader["Clinical_Ind"])),
//                 (Convert.IsDBNull(reader["Title"]) ? string.Empty : reader["Title"].ToString()),
//                 (bool)(Convert.IsDBNull(reader["Giftcard_Ind"]) ? false : reader["Giftcard_Ind"]),
//                 (decimal)(Convert.IsDBNull(reader["Giftcard_Chour"]) ? 0 : (decimal)reader["Giftcard_Chour"]),
//                 (decimal)(Convert.IsDBNull(reader["Giftcard_Uhour"]) ? 0 : (decimal)reader["Giftcard_Uhour"]),
//                 (int)(Convert.IsDBNull(reader["UniqueID"]) ? 0 : (int)reader["UniqueID"]),
//                 (bool)reader["FirstLogin_Ind"]);
//                break;
//            }

//            return UserAccount;
//        }
//        public override DepartmentInfo GetDepartmentByID(int dept_id)
//        {
//            DepartmentInfo deptInfo = null;
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("Select dept_id,dept_name,dept_abbrev,dept_number, " +
//                        "dept_bed_total,facilityid,dept_active_ind from Dept where dept_id='" + dept_id + "'", cn);

//                cn.Open();
//                SqlDataReader reader = cmd.ExecuteReader();
//                deptInfo = new DepartmentInfo();
//                while (reader.Read())
//                {
//                    deptInfo.dept_id = Convert.ToInt32(reader["dept_id"]);
//                    deptInfo.dept_name = reader["dept_name"].ToString();
//                    deptInfo.dept_abbrev = reader["dept_abbrev"].ToString();
//                    deptInfo.dept_number = reader["dept_number"].ToString();
//                    deptInfo.dept_bed_total = reader["dept_bed_total"] != DBNull.Value ? Convert.ToInt32(reader["dept_bed_total"]) : 0;
//                    deptInfo.facilityid = reader["facilityid"] != DBNull.Value ? Convert.ToInt32(reader["facilityid"]) : 0;
//                    deptInfo.dept_active_ind = reader["dept_active_ind"] != DBNull.Value ? Convert.ToBoolean(reader["dept_active_ind"]) : false;

//                }
//                reader.Dispose();
//                return deptInfo;
//            }



//            //throw new NotImplementedException()
//        }

//        public override List<UserAccountInfo1> GetUserAccountsByDeptName(string dept_name, string cSortExpression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select a.iid,a.cusername,a.cfirstname,a.clastname,a.facilityid from UserAccount a " +
//                                    "INNER JOIN Manage b ON a.iid = b.userid " +
//                                    "INNER JOIN Dept c ON b.dept_id = c.dept_id WHERE c.dept_name = @dept_name";
//                //string cSQLCommand = "Select iid,cusername,cfirstname,clastname,facilityid from UserAccount" +
//                //    "where iid=(select userid from Manage" +
//                //    "where dept_id = (select dept_id from dept where dept_name ='@dept_name'))";
//                //add on Order By if provided

//                if (cSortExpression.Length > 0)
//                {

//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cmd.Parameters.Add("@dept_name", SqlDbType.NVarChar).Value = dept_name;
//                cn.Open();
//                return GetUserAccountCollectionFromReader1(ExecuteReader(cmd), false);
//            }
//        }
//        public override List<DepartmentInfo> GetDepartmentsByUserID(int userID)
//        {
//            List<DepartmentInfo> departments = new List<DepartmentInfo>();
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "SELECT a.dept_id, a.dept_name FROM Dept a INNER JOIN Manage b ON " +
//                                     "a.dept_id = b.dept_id WHERE b.userid = @userid";

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userID;
//                cn.Open();

//                using (SqlDataReader reader = cmd.ExecuteReader())
//                {
//                    while (reader.Read())
//                    {
//                        DepartmentInfo dept = new DepartmentInfo();
//                        dept.dept_id = Convert.ToInt32(reader["dept_id"]);
//                        dept.dept_name = reader["dept_name"].ToString();
//                        departments.Add(dept);
//                    }
//                }
//            }
//            return departments;
//        }
//        public override void UpdateDepartmentsForUserID(List<DepartmentInfo> departments, int userID)
//        {
//            using (SqlConnection conn = new SqlConnection(this.ConnectionString))
//            {
//                conn.Open();

//                SqlCommand command = conn.CreateCommand();
//                SqlTransaction transaction;

//                // Start a local transaction.
//                transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);

//                // Must assign both transaction object and connection
//                // to Command object for a pending local transaction
//                command.Connection = conn;
//                command.Transaction = transaction;

//                try
//                {
//                    command.CommandText = "DELETE FROM Manage WHERE UserID=@UserID";
//                    command.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
//                    command.ExecuteNonQuery();

//                    command.CommandText = "INSERT INTO Manage(UserID, Dept_ID) VALUES(@UserID, @DeptID)";
//                    command.Parameters.Clear();
//                    command.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int));
//                    command.Parameters.Add(new SqlParameter("@DeptID", SqlDbType.Int));
//                    command.Parameters[0].Value = userID;
//                    foreach (DepartmentInfo deptInfo in departments)
//                    {
//                        command.Parameters[1].Value = deptInfo.dept_id;
//                        command.ExecuteNonQuery();
//                    }
//                    transaction.Commit();
//                }
//                catch { transaction.Rollback(); return; }
//                finally { transaction.Dispose(); }
//            }
//        }

//        public override void UpdatePrimaryDepartmentForUserID(int deptID, int userID)
//        {
//            using (SqlConnection conn = new SqlConnection(this.ConnectionString))
//            {
//                conn.Open();

//            SqlCommand command = conn.CreateCommand();
//            SqlTransaction transaction;

//            // Start a local transaction.
//            transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);

//            // Must assign both transaction object and connection
//            // to Command object for a pending local transaction
//            command.Connection = conn;
//            command.Transaction = transaction;

//            try
//            {

//                command.CommandText = "DELETE FROM Manage WHERE UserID=@UserID and Dept_ID = (Select dept_id from UserAccount where iID =@UserID) ";
//                command.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
//                //command.Parameters.Add("@DeptID", SqlDbType.Int).Value = deptID;
//                command.ExecuteNonQuery();

//                command.CommandText = "Update UserAccount Set dept_id =  @DeptID where iID = @UserID";
//                command.Parameters.Clear();
//                command.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int));
//                command.Parameters.Add(new SqlParameter("@DeptID", SqlDbType.Int));
//                command.Parameters[0].Value = userID;
//                command.Parameters[1].Value = deptID;
//                command.ExecuteNonQuery();


//                command.CommandText = "INSERT INTO Manage(UserID, Dept_ID) VALUES(@UserID, @DeptID)";
//                command.Parameters.Clear();
//                command.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int));
//                command.Parameters.Add(new SqlParameter("@DeptID", SqlDbType.Int));
//                command.Parameters[0].Value = userID;
//                command.Parameters[1].Value = deptID;
//                command.ExecuteNonQuery();
//                transaction.Commit();
//            }
//            catch
//            { 
//                transaction.Rollback(); 
//                return;
//            }
//            finally
//            { 
//                transaction.Dispose(); 
//            }
//        }
//        }

//        /// <summary>
//        /// Retrieves all Orders
//        /// </summary>
//        public override List<OrdersInfo> GetOrders(string cSortExpression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select * from Orders";

//                // add on ORDER BY if provided
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }

//        /// <summary>
//        /// Retrieves all Orders by date
//        /// </summary>
//        public override List<OrdersInfo> GetOrdersbyDate(string cSortExpression,string selDate, bool IsPending, bool isShipped)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//               // "SELECT     * FROM         Orders WHERE     (orderdate >= CONVERT(DATETIME, '2009-12-12 00:00:00', 102))"
//                string strWhereClause = string.Empty;
//                if (IsPending && !isShipped)
//                {
//                    strWhereClause += " and ship_ind = 0 ";
//                }
//                if (isShipped && !IsPending)
//                {
//                    strWhereClause += " and ship_ind = 1 ";
//                }

//                string cSQLCommand = "select * " +
//                    "from Orders where  (orderdate >= CONVERT(DATETIME, '"+selDate+"', 102)) " + strWhereClause;

//                // add on ORDER BY if provided
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }


//        /// <summary>
//        /// Retrieves all Orders by date
//        /// </summary>
//        public override List<OrdersInfo> GetOrdersbyUserID(int userid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                //string cSQLCommand = "SELECT  * FROM Orders WHERE     (userid = " + userid+")";
//                string cSQLCommand = "select * from orders left join orderitem on orders.orderid = orderitem.orderid where orders.userid = " + userid + " and Verification<> 'Unlimited CE' ";

//                //string cSQLCommand = "select * from orders left join orderitem on orders.orderid = orderitem.orderid where orders.userid ='700979' and Verification<>'Unlimited CE'";

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }


        
//        /// <summary>
//        /// Retrieves all Pending Orders
//        /// </summary>
//        public override List<OrdersInfo> GetPendingOrders(string cSortExpression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select * from Orders where ship_ind = 0";

//                // add on ORDER BY if provided
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }


//        /// <summary>
//        /// Retrieves all Shipped Orders
//        /// </summary>
//        public override List<OrdersInfo> GetShippedOrders(string cSortExpression)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "select * from Orders where ship_ind = 1";

//                // add on ORDER BY if provided
//                if (cSortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + cSortExpression;
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                return GetOrdersCollectionFromReader(ExecuteReader(cmd), false);
//            }
//        }
        
//        /// <summary>
//        /// Retrieves the Orders with the specified ID
//        /// </summary>
//        public override OrdersInfo GetOrdersByID(int OrderID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                SqlCommand cmd = new SqlCommand("select * " +
//                        "from Orders where orderid=@OrderID", cn);
//                cmd.Parameters.Add("@OrderID", SqlDbType.Int).Value = OrderID;
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//                if (reader.Read())
//                    return GetOrdersFromReader(reader, true);
//                else
//                    return null;
//            }
//        }

//        /// <summary>
//        /// Inserts a new Orders
//        /// </summary>
//        public override int InsertOrders(OrdersInfo Orders)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("insert into Orders values ( " +
//              " @userid, " +
//              " @firstname, " +
//              " @lastname, " +
//              " @address1, " +
//              " @address2, " +
//              " @city, " +
//              " @state, " +
//              " @zip, " +
//              " @country, " +
//              " @email, " +
//              " @subtotal, " +
//              " @shipcost, " +
//              " @tax, " +
//              " @totalcost, " +
//              " @validate_ind, " +
//              " @verification, " +
//              " @ship_ind, " +
//              " @orderdate, " +
//              " @comment, " +
//              " @CouponId, " +
//              " @Couponamount) SET @ID = SCOPE_IDENTITY()", cn);

//                cmd.Parameters.Add("@orderid", SqlDbType.Int).Value = Orders.OrderID;
//                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = Orders.UserID;
//                cmd.Parameters.Add("@firstname", SqlDbType.VarChar).Value = Orders.FirstName == null ? DBNull.Value as Object : Orders.FirstName as Object;
//                cmd.Parameters.Add("@lastname", SqlDbType.VarChar).Value = Orders.LastName == null ? DBNull.Value as Object : Orders.LastName as Object;
//                cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = Orders.Address1 == null ? DBNull.Value as Object : Orders.Address1 as Object;
//                cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = Orders.Address2 == null ? DBNull.Value as Object : Orders.Address2 as Object;
//                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = Orders.City == null ? DBNull.Value as Object : Orders.City as Object;
//                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = Orders.State == null ? DBNull.Value as Object : Orders.State as Object;
//                cmd.Parameters.Add("@zip", SqlDbType.VarChar).Value = Orders.Zip == null ? DBNull.Value as Object : Orders.Zip as Object;
//                cmd.Parameters.Add("@country", SqlDbType.VarChar).Value = Orders.Country == null ? DBNull.Value as Object : Orders.Country as Object;
//                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = Orders.Email == null ? DBNull.Value as Object : Orders.Email as Object;
//                cmd.Parameters.Add("@subtotal", SqlDbType.Decimal).Value = Orders.SubTotal;
//                cmd.Parameters.Add("@shipcost", SqlDbType.Decimal).Value = Orders.ShipCost;
//                cmd.Parameters.Add("@tax", SqlDbType.Decimal).Value = Orders.Tax;
//                cmd.Parameters.Add("@totalcost", SqlDbType.Decimal).Value = Orders.TotalCost;
//                cmd.Parameters.Add("@Couponamount", SqlDbType.Decimal).Value = Orders.CouponAmount;
//                cmd.Parameters.Add("@CouponId", SqlDbType.Int).Value = Orders.CouponID;
//                cmd.Parameters.Add("@validate_ind", SqlDbType.Bit).Value = Orders.Validate_Ind;
//                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = Orders.Verification == null ? DBNull.Value as Object : Orders.Verification as Object;
//                cmd.Parameters.Add("@ship_ind", SqlDbType.Bit).Value = Orders.Ship_Ind;


//                if (Orders.OrderDate == System.DateTime.MinValue)
//                {
//                    cmd.Parameters.Add("@orderdate", SqlDbType.DateTime).Value = System.DBNull.Value;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@orderdate", SqlDbType.DateTime).Value = Orders.OrderDate;
//                }

//                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = Orders.Comment == null ? DBNull.Value as Object : Orders.Comment as Object;


//                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//                IDParameter.Direction = ParameterDirection.Output;
//                cmd.Parameters.Add(IDParameter);

//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                cmd.ExecuteNonQuery();

//                int NewID = (int)IDParameter.Value;
//                return NewID;

//            }
//        }

//        /// <summary>
//        /// Updates Orders
//        /// </summary>
//        public override bool UpdateOrders(OrdersInfo Orders)
//        {

//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("update Orders SET " +
//              "orderid= @orderid, " +
//              "userid = @userid, " +
//              "firstname = @firstname, " +
//              "lastname =@lastname, " +
//              "address1 =@address1, " +
//              "address2 =@address2, " +
//              "city =@city, " +
//              "state =@state, " +
//              "zip =@zip, " +
//              "country =@country, " +
//              "email =@email, " +
//              "subtotal =@subtotal, " +
//              "shipcost =@shipcost, " +
//              "tax =@tax, " +
//              "totalcost =@totalcost, " +
//              "validate_ind =@validate_ind, " +
//              "verification =@verification, " +
//              "ship_ind =@ship_ind, " +
//              "orderdate =@orderdate, " +
//              "comment =@comment ," +
//              "CouponId =@CouponId ," +
//              "Couponamount =@Couponamount " +
//              "where orderid = @orderid ", cn);
//                cmd.Parameters.Add("@orderid", SqlDbType.Int).Value = Orders.OrderID;
//                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = Orders.UserID;
//                cmd.Parameters.Add("@firstname", SqlDbType.VarChar).Value = Orders.FirstName == null ? DBNull.Value as Object : Orders.FirstName as Object;
//                cmd.Parameters.Add("@lastname", SqlDbType.VarChar).Value = Orders.LastName == null ? DBNull.Value as Object : Orders.LastName as Object;
//                cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = Orders.Address1 == null ? DBNull.Value as Object : Orders.Address1 as Object;
//                cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = Orders.Address2 == null ? DBNull.Value as Object : Orders.Address2 as Object;
//                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = Orders.City == null ? DBNull.Value as Object : Orders.City as Object;
//                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = Orders.State == null ? DBNull.Value as Object : Orders.State as Object;
//                cmd.Parameters.Add("@zip", SqlDbType.VarChar).Value = Orders.Zip == null ? DBNull.Value as Object : Orders.Zip as Object;
//                cmd.Parameters.Add("@country", SqlDbType.VarChar).Value = Orders.Country == null ? DBNull.Value as Object : Orders.Country as Object;
//                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = Orders.Email == null ? DBNull.Value as Object : Orders.Email as Object;
//                cmd.Parameters.Add("@subtotal", SqlDbType.Decimal).Value = Orders.SubTotal;
//                cmd.Parameters.Add("@shipcost", SqlDbType.Decimal).Value = Orders.ShipCost;
//                cmd.Parameters.Add("@tax", SqlDbType.Decimal).Value = Orders.Tax;
//                cmd.Parameters.Add("@totalcost", SqlDbType.Decimal).Value = Orders.TotalCost;
//                cmd.Parameters.Add("@Couponamount", SqlDbType.Decimal).Value = Orders.CouponAmount;
//                cmd.Parameters.Add("@CouponId", SqlDbType.Int).Value = Orders.CouponID;
//                cmd.Parameters.Add("@validate_ind", SqlDbType.Bit).Value = Orders.Validate_Ind;
//                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = Orders.Verification == null ? DBNull.Value as Object : Orders.Verification as Object;
//                cmd.Parameters.Add("@ship_ind", SqlDbType.Bit).Value = Orders.Ship_Ind;
//                if (Orders.OrderDate == System.DateTime.MinValue)
//                {
//                    cmd.Parameters.Add("@orderdate", SqlDbType.DateTime).Value = System.DBNull.Value;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@orderdate", SqlDbType.DateTime).Value = Orders.OrderDate;
//                }
//                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = Orders.Comment == null ? DBNull.Value as Object : Orders.Comment as Object;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }

//        /// <summary>
//        /// Deletes Orders
//        /// </summary>
//        public override bool DeleteOrders(int OrderID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("delete from Orders where orderid=@OrderID", cn);
//                cmd.Parameters.Add("@OrderID", SqlDbType.Int).Value = OrderID;
//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);
//            }
//        }


//        public override void UpdateOrdersForOrderID(OrdersInfo Orders, int OrderID)
//        {
//            using (SqlConnection conn = new SqlConnection(this.ConnectionString))
//            {
//                conn.Open();

//                SqlCommand command = conn.CreateCommand();
//                SqlTransaction transaction;

//                // Start a local transaction.
//                transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);

//                // Must assign both transaction object and connection
//                // to Command object for a pending local transaction
//                command.Connection = conn;
//                command.Transaction = transaction;

//                try
//                {
//                    command.CommandText = "Update Orders Set firstname=@FirstName, lastname=@LastName,address1=@Address1,address2=@Address2," +
//                        " city=@City,state=@State,zip=@Zip,comments=@Comments, userid=@UserID where OrderID=@OrderID";
                   
//                    command.Parameters.Add("@OrderID", SqlDbType.Int).Value = OrderID;
                    
//                    command.ExecuteNonQuery();
//                   transaction.Commit();
//                }
//                catch { transaction.Rollback(); return; }
//                finally { transaction.Dispose(); }
//            }
//        }


//        public override List<OrderItemInfo> GetOrderDetails(int OrderID)
//        {
          
//            List<OrderItemInfo> ordInfo = new List<OrderItemInfo>();

//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
                             
//                string cSQLCommand = "SELECT orderitemid, orderid, topicid, cost, media_type, quantity, discountid, optin_ind " +
//                                                "FROM  OrderItem  WHERE    orderid = @OrderID";

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
//                cmd.Parameters.Add("@OrderID", SqlDbType.Int).Value = OrderID;
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd);
//                while (reader.Read())
//                {
//                    ordInfo.Add(GetOrderItemFromReader(reader, true));
//                }
//            }
//            return ordInfo;
          
//        }

//        /// <summary>
//        /// Inserts a new Orders
//        /// </summary>
//        public override int InsertOrderItem(OrderItemInfo OrderItem)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("insert into Orderitem values ( " +
//              " @orderid, " +
//              " @topicid, " +
//              " dbo.udf_cartprice(@cost), " +
//              " @media_type, " +
//              " @quantity, " +
//              " @discountid, " +
//              " @optin_ind ) SET @ID = SCOPE_IDENTITY()", cn);

//                cmd.Parameters.Add("@orderid", SqlDbType.Int).Value = OrderItem.OrderID;
//                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = OrderItem.TopicID;
//                cmd.Parameters.Add("@cost", SqlDbType.Decimal).Value = OrderItem.Cost;
//                cmd.Parameters.Add("@media_type", SqlDbType.VarChar).Value = OrderItem.Media_Type == null ? DBNull.Value as Object : OrderItem.Media_Type as Object;
//                cmd.Parameters.Add("@quantity", SqlDbType.Int).Value = OrderItem.Quantity;
//                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = OrderItem.DiscountID;
//                cmd.Parameters.Add("@optin_ind", SqlDbType.Bit).Value = OrderItem.Optin_Ind;
//                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
//                IDParameter.Direction = ParameterDirection.Output;
//                cmd.Parameters.Add(IDParameter);

//                foreach (IDataParameter param in cmd.Parameters)
//                {
//                    if (param.Value == null)
//                        param.Value = DBNull.Value;
//                }

//                cn.Open();
//                cmd.ExecuteNonQuery();

//                int NewID = (int)IDParameter.Value;
//                return NewID;

//            }
//        }
        
        
//        /// <summary>
//        /// Updates Membershiplog
//        /// </summary>

//        public override bool UpdateMembershipLog(MembershipLogInfo MembershipLog)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("Update MembershipLog SET " +
//                    "mlid = @mlid, " +
//                    "userid = @userid, " +
//                    "logdate = @logdate, " +
//                    "expdate = @expdate, " +
//                    "verification = @verification, " +
//                    "profileid = @profileid, " +
//                    "renew_ind = @renew_ind, " +
//                    "active_ind = @active_Ind, " +
//                    "amount = @amount, " +
//                    "comment = @comment, " +
//                    "ccnum = @ccnum " +
//                    "where  userid = @userid ", cn);
//            cmd.Parameters.Add("@mlid", SqlDbType.Int).Value = MembershipLog.mlid;
//            cmd.Parameters.Add("userid", SqlDbType.Int).Value = MembershipLog.userid;
//            if (MembershipLog.logdate == System.DateTime.MinValue)
//                {
//                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = System.DBNull.Value;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = MembershipLog.logdate;
//                }
//              if (MembershipLog.expdate == System.DateTime.MinValue)
//                {
//                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = System.DBNull.Value;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = MembershipLog.expdate;
//                }
            
//                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = MembershipLog.verification == null ? DBNull.Value as Object : MembershipLog.verification as Object;
              
//                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = MembershipLog.profileid == null ? DBNull.Value as Object : MembershipLog.profileid as Object;
            

              
//                    cmd.Parameters.Add("@renew_ind", SqlDbType.Bit).Value = MembershipLog.renew_ind;
                
         
//                    cmd.Parameters.Add("@active_ind", SqlDbType.Bit).Value = MembershipLog.active_ind;
               
//             cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = MembershipLog.amount;
//             cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = MembershipLog.comment == null ? DBNull.Value as Object : MembershipLog.comment as Object;
//             cmd.Parameters.Add("@ccnum", SqlDbType.VarChar).Value = MembershipLog.ccnum == null ? DBNull.Value as Object : MembershipLog.ccnum as Object;

//            cn.Open();
//            int ret = ExecuteNonQuery(cmd);
//            return(ret == 1);

//            }
//        }

//        public override MembershipLogInfo GetMembershipLogByUserID(int userid)
//        {
//            MembershipLogInfo memloginfo = null;
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("Select * from membershiplog where userid='" + userid + "' order by active_ind desc, expdate desc", cn);

//                cn.Open();
//                SqlDataReader reader = cmd.ExecuteReader();
//                memloginfo = new MembershipLogInfo();
//                while (reader.Read())
//                {
//                    memloginfo.mlid = Convert.ToInt32(reader["mlid"]);
//                    memloginfo.userid = Convert.ToInt32(reader["userid"]);
//                    memloginfo.logdate = (DateTime)(Convert.IsDBNull(reader["logdate"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
//                    memloginfo.expdate = (DateTime)(Convert.IsDBNull(reader["expdate"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
//                    memloginfo.verification = reader["verification"].ToString();
//                    memloginfo.profileid = reader["profileid"].ToString();
//                    memloginfo.renew_ind = reader["renew_ind"] != DBNull.Value ? Convert.ToBoolean(reader["renew_ind"]) : false;
//                    memloginfo.active_ind = reader["active_ind"] != DBNull.Value ? Convert.ToBoolean(reader["active_ind"]) : false;
//                    memloginfo.amount = (decimal)(Convert.IsDBNull(reader["amount"]) ? 0 : (decimal)reader["amount"]);
//                    memloginfo.comment = reader["comment"].ToString();
//                    memloginfo.ccnum = reader["ccnum"].ToString();
//                }
//                reader.Dispose();
//                return memloginfo;
//            }
//        }

//        public override bool CancelMembershipLog(int userid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("Update membershiplog SET " +
//                    "active_ind='false' ," +
//                    "comment= 'Cancelled by CSR on + getdate()'," +
//                    "expdate= '" + DateTime.Now+"' "+
//                    "where Userid = @userid", cn);
//                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
//                try
//                {
//                    cn.Open();
//                    ExecuteNonQuery(cmd);
//                    return true;
//                }
//                catch
//                {
//                   return false;
//                }

//            }
//        }

//        public override bool InsertMembershipLog(MembershipLogInfo MembershipLog)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("Insert into MembershipLog (userid,logdate,expdate,verification, " +
//                    "profileid,renew_ind,active_ind,amount,comment,ccnum)values " +
//                    "(@userid, " +
//                    "@logdate, " +
//                    "@expdate, " +
//                    "@verification, " +
//                    "@profileid, " +
//                    "@renew_ind, " +
//                    "@active_Ind, " +
//                    "@amount, " +
//                    "@comment, " +
//                    "@ccnum )",cn);
//                //+
//                  //  "where  userid = @userid ", cn);
//                //cmd.Parameters.Add("@mlid", SqlDbType.Int).Value = MembershipLog.mlid;
//                cmd.Parameters.Add("userid", SqlDbType.Int).Value = MembershipLog.userid;
//                if (MembershipLog.logdate == System.DateTime.MinValue)
//                {
//                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = System.DBNull.Value;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = MembershipLog.logdate;
//                }
//                if (MembershipLog.expdate == System.DateTime.MinValue)
//                {
//                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = System.DBNull.Value;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = MembershipLog.expdate;
//                }

//                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = MembershipLog.verification == null ? DBNull.Value as Object : MembershipLog.verification as Object;

//                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = MembershipLog.profileid == null ? DBNull.Value as Object : MembershipLog.profileid as Object;


//                if (MembershipLog.renew_ind == false)
//                {
//                    cmd.Parameters.Add("@renew_ind", SqlDbType.Bit).Value = false;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@renew_ind", SqlDbType.Bit).Value = MembershipLog.renew_ind;
//                }
//                if (MembershipLog.active_ind == false)
//                {
//                    cmd.Parameters.Add("@active_ind", SqlDbType.Bit).Value = false;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@active_ind", SqlDbType.Bit).Value = MembershipLog.active_ind;
//                }
//                cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = MembershipLog.amount;
//                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = MembershipLog.comment == null ? DBNull.Value as Object : MembershipLog.comment as Object;
//                cmd.Parameters.Add("@ccnum", SqlDbType.VarChar).Value = MembershipLog.ccnum == null ? DBNull.Value as Object : MembershipLog.ccnum as Object;

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);

//            }
//        }

//        public override List<MembershipLogInfo> GetMembershipLogListByUserID(int userid)
//        {
//            List<MembershipLogInfo> memloginfoList = new List<MembershipLogInfo>();
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("Select * from membershiplog where userid='" + userid + "' order by active_ind desc, expdate desc", cn);

//                cn.Open();
//                SqlDataReader reader = cmd.ExecuteReader();
//                MembershipLogInfo memloginfo;
//                bool firstFlag = true;
//                while (reader.Read())
//                {
//                    memloginfo = new MembershipLogInfo();
//                    if (firstFlag == true)
//                    {
//                        memloginfo.FirstRec = true;
//                        firstFlag = false;
//                    }
//                    else
//                    {
//                        memloginfo.FirstRec = false;
//                    }                   
                        
//                    memloginfo.mlid = Convert.ToInt32(reader["mlid"]);
//                    memloginfo.userid = Convert.ToInt32(reader["userid"]);
//                    memloginfo.logdate = (DateTime)(Convert.IsDBNull(reader["logdate"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
//                    memloginfo.expdate = (DateTime)(Convert.IsDBNull(reader["expdate"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
//                    memloginfo.verification = reader["verification"].ToString();
//                    memloginfo.profileid = reader["profileid"].ToString();
//                    memloginfo.renew_ind = reader["renew_ind"] != DBNull.Value ? Convert.ToBoolean(reader["renew_ind"]) : false;
//                    memloginfo.active_ind = reader["active_ind"] != DBNull.Value ? Convert.ToBoolean(reader["active_ind"]) : false;
//                    memloginfo.amount = (decimal)(Convert.IsDBNull(reader["amount"]) ? 0 : (decimal)reader["amount"]);
//                    memloginfo.comment = reader["comment"].ToString();
//                    memloginfo.ccnum = reader["ccnum"].ToString();
//                    memloginfoList.Add(memloginfo);
//                }
//                reader.Dispose();
//                return memloginfoList;
//            }
//        }


//        public override List<CouponsInfo> GetCoupons(string facilityid, string sortExpression)
//        {
//            List<CouponsInfo> coups = null;
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                string cSQLCommand = "SELECT cid,couponcode,coupondesc,camount,expiredate,facilityid,coupontype,topicid, "+
//                                        "mediatype FROM Coupons";

//                if (facilityid.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand + " where facilityid= " + facilityid;
//                }

//                // add on ORDER BY if provided
//                if (sortExpression.Length > 0)
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by " + sortExpression;
//                }
//                else
//                {
//                    cSQLCommand = cSQLCommand +
//                        " order by couponcode";
//                }

//                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

//                cn.Open();
//                SqlDataReader reader = cmd.ExecuteReader();
//                coups = new List<CouponsInfo>();
//                while (reader.Read())
//                {
//                    CouponsInfo coup = new CouponsInfo();
//                    coup.CID = reader["cid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["cid"]);
//                    coup.CouponCode = reader["couponcode"].ToString();
//                    coup.CouponDesc = reader["coupondesc"].ToString();
//                    coup.CAmount = reader["camount"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["camount"]);
//                    coup.ExpireDate = reader["expiredate"]== DBNull.Value ?  DateTime.MinValue : Convert.ToDateTime(reader["expiredate"]);
//                    coup.FacilityID = reader["facilityid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["facilityid"]);
//                    coup.CouponType = reader["coupontype"].ToString();
//                    coup.TopicID = reader["topicid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["topicid"]);
//                    coup.MediaType = reader["mediatype"].ToString();
//                    coups.Add(coup);
                    
//                }
//            }
//            return coups;
//        }

//        public override CouponsInfo GetCouponByFacID(int facilityid)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {

//                SqlCommand cmd = new SqlCommand("select * " +
//                        "from Coupons where facilityid=@facilityid", cn);
//                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
//                if (reader.Read())
//                    return GetCouponsFromReader(reader, true);
//                else
//                    return null;
//            }
//        }

//        public override int GetCouponIdByCode(string Code)
//        {
//            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("select cid from coupons where couponcode=@Code and expiredate >= getdate()", cn);
//                cmd.Parameters.Add("@Code", SqlDbType.VarChar).Value = Code.Trim();
//                cn.Open();
//                IDataReader reader = ExecuteReader(cmd);
//                if (reader.Read())
//                {
//                    return (int)reader[0];
//                }
//                else
//                {
//                    return 0;
//                }
//            }
//        }




//        /// <summary>
//        /// Updates UCEmembership
//        /// </summary>
//        /// 
//        public override bool UpdateUCEmembership(UCEmembershipInfo UCEmembership)
//        {
//            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("Update UCEmembership SET " +
//                    "umID = @umID, " +
//                    "rnID = @rnID, " +
//                    "logdate = @logdate, " +
//                    "expdate = @expdate, " +
//                    "verification = @verification, " +
//                    "profileid = @profileid, " +
//                    "renew = @renew, " +
//                    "active = @active, " +
//                    "comment = @comment, " +
//                    "amount = @amount, " +
//                    "ccnum = @ccnum ", cn);
//                // cmd.Parameters.Add("@umID", SqlDbType.Int).Value = UCEmembership.UmID;
//                cmd.Parameters.Add("@rnID", SqlDbType.Int).Value = UCEmembership.RnID;

//                if (UCEmembership.LogDate == System.DateTime.MinValue)
//                {
//                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = System.DBNull.Value;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = UCEmembership.LogDate;
//                }


//                if (UCEmembership.ExpDate == System.DateTime.MinValue)
//                {
//                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = System.DBNull.Value;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = UCEmembership.ExpDate;
//                }
//                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = UCEmembership.Verification == null ? DBNull.Value
//                    as Object : UCEmembership.Verification as Object;
//                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = UCEmembership.ProfileID == null ? DBNull.Value
//                    as Object : UCEmembership.ProfileID as Object;

//                if (UCEmembership.Renew == false)
//                {
//                    cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = false;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = UCEmembership.Renew;
//                }

//                if (UCEmembership.Active == false)
//                {
//                    cmd.Parameters.Add("@active", SqlDbType.Bit).Value = false;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@active", SqlDbType.Bit).Value = UCEmembership.Active;
//                }
//                cmd.Parameters.Add("@amount", SqlDbType.Float).Value = UCEmembership.Amount;
//                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = UCEmembership.Comment == null ? DBNull.Value
//                      as Object : UCEmembership.Comment as Object;

//                cmd.Parameters.Add("@ccnum", SqlDbType.VarChar).Value = UCEmembership.CcNum == null ? DBNull.Value
//                    as Object : UCEmembership.CcNum as Object;

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);


//            }
//        }

//        public override UCEmembershipInfo GetUCEmembershipByRnID(int RnID)
//        {
//            UCEmembershipInfo ucememinfo = null;
//            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("Select * from ucemembership where rnid ='" + RnID + "' order by active desc, expdate desc", cn);
//                cn.Open();
//                SqlDataReader reader = cmd.ExecuteReader();
//                ucememinfo = new UCEmembershipInfo();
//                while (reader.Read())
//                {
//                    ucememinfo.UmID = Convert.ToInt32(reader["umid"]);
//                    ucememinfo.RnID = Convert.ToInt32(reader["rnid"]);
//                    ucememinfo.LogDate = (DateTime)(Convert.IsDBNull(reader["logdate"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
//                    ucememinfo.ExpDate = (DateTime)(Convert.IsDBNull(reader["expdate"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
//                    ucememinfo.Verification = reader["verification"].ToString();
//                    ucememinfo.ProfileID = reader["profileid"].ToString();
//                    ucememinfo.Renew = reader["renew"] != DBNull.Value ? Convert.ToBoolean(reader["renew"]) : false;
//                    ucememinfo.Active = reader["active"] != DBNull.Value ? Convert.ToBoolean(reader["active"]) : false;
//                    ucememinfo.Amount = Convert.IsDBNull(reader["amount"]) ? 0 : float.Parse(reader["amount"].ToString());
//                    ucememinfo.Comment = reader["comment"].ToString();
//                    ucememinfo.CcNum = reader["ccnum"].ToString();
//                }

//                reader.Dispose();
//                return ucememinfo;

//            }
//        }

//        public override bool CancelUCEmembership(int RnID)
//        {
//            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("Update ucemembership SET " +
//                    "active ='false' ," +
//                    "comment= 'Cancelled by CSR on + getdate()'," +
//                    "expdate= '" + DateTime.Now + "' " +
//                    "where RnID = @rnid", cn);
//                cmd.Parameters.Add("@rnid", SqlDbType.Int).Value = RnID;
//                try
//                {
//                    cn.Open();
//                    ExecuteNonQuery(cmd);
//                    return true;
//                }
//                catch
//                {
//                    return false;
//                }

//            }
//        }


//        public override bool InsertUCEmembership(UCEmembershipInfo UCEmembership)
//        {
//            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("Insert into ucemembership(rnid,logdate,expdate,verification, " +
//                    "profileid,renew,active,comment,amount,ccnum)values " +
//                    "(@rnid, " +
//                    "@logdate, " +
//                    "@expdate, " +
//                    "@verification, " +
//                    "@profileid, " +
//                    "@renew, " +
//                    "@active, " +
//                    "@comment, " +
//                    "@amount, " +
//                    "@ccnum )", cn);

//                cmd.Parameters.Add("rnid", SqlDbType.Int).Value = UCEmembership.RnID;
//                if (UCEmembership.LogDate == System.DateTime.MinValue)
//                {
//                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = System.DBNull.Value;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = UCEmembership.LogDate;
//                }
//                if (UCEmembership.ExpDate == System.DateTime.MinValue)
//                {
//                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = System.DBNull.Value;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = UCEmembership.ExpDate;
//                }

//                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = UCEmembership.Verification == null ? DBNull.Value as Object : UCEmembership.Verification as Object;

//                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = UCEmembership.ProfileID == null ? DBNull.Value as Object : UCEmembership.ProfileID as Object;


//                if (UCEmembership.Renew == false)
//                {
//                    cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = false;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = UCEmembership.Renew;
//                }
//                if (UCEmembership.Active == false)
//                {
//                    cmd.Parameters.Add("@active", SqlDbType.Bit).Value = false;
//                }
//                else
//                {
//                    cmd.Parameters.Add("@active", SqlDbType.Bit).Value = UCEmembership.Active;
//                }
//                cmd.Parameters.Add("@amount", SqlDbType.Float).Value = UCEmembership.Amount;
//                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = UCEmembership.Comment == null ? DBNull.Value as Object : UCEmembership.Comment as Object;
//                cmd.Parameters.Add("@ccnum", SqlDbType.VarChar).Value = UCEmembership.CcNum == null ? DBNull.Value as Object : UCEmembership.CcNum as Object;

//                cn.Open();
//                int ret = ExecuteNonQuery(cmd);
//                return (ret == 1);

//            }
//        }

//        public override List<UCEmembershipInfo> GetUCEmembershipListByRnID(int RnID)
//        {
//            List<UCEmembershipInfo> ucememlist = new List<UCEmembershipInfo>();
//            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
//            {
//                SqlCommand cmd = new SqlCommand("Select * from ucemembership where rnid='" + RnID + "' order by active desc, expdate desc", cn);

//                cn.Open();
//                SqlDataReader reader = cmd.ExecuteReader();
//                UCEmembershipInfo ucememinfo;
//                bool firstFlag = true;
//                while (reader.Read())
//                {
//                    ucememinfo = new UCEmembershipInfo();
//                    if (firstFlag == true)
//                    {
//                        ucememinfo.FirstRec = true;
//                        firstFlag = false;
//                    }
//                    else
//                    {
//                        ucememinfo.FirstRec = false;
//                    }

//                    ucememinfo.UmID = Convert.ToInt32(reader["umid"]);
//                    ucememinfo.RnID = Convert.ToInt32(reader["rnid"]);
//                    ucememinfo.LogDate = (DateTime)(Convert.IsDBNull(reader["logdate"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
//                    ucememinfo.ExpDate = (DateTime)(Convert.IsDBNull(reader["expdate"])
//                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
//                    ucememinfo.Verification = reader["verification"].ToString();
//                    ucememinfo.ProfileID = reader["profileid"].ToString();
//                    ucememinfo.Renew = reader["renew"] != DBNull.Value ? Convert.ToBoolean(reader["renew"]) : false;
//                    ucememinfo.Active = reader["active"] != DBNull.Value ? Convert.ToBoolean(reader["active"]) : false;
//                    ucememinfo.Amount = reader["amount"] == DBNull.Value ? 0 : float.Parse(reader["amount"].ToString());
//                    ucememinfo.Comment = reader["comment"].ToString();
//                    ucememinfo.CcNum = reader["ccnum"].ToString();
//                    ucememlist.Add(ucememinfo);
//                }
//                reader.Dispose();
//                return ucememlist;
//            }
//        }






    }

    }
