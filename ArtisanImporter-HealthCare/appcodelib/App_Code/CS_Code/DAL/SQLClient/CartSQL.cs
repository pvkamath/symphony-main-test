﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for CartSQL
/// </summary>
/// 
#region CartInfo
namespace PearlsReview.DAL
{
    public class CartInfo
    {
        public CartInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

         public CartInfo(int CartID, int TopicID, int UserID, string MediaType, int Score, DateTime Lastmod, string XmlTest,
            string XmlSurvey, int Quantity, int DiscountId, bool Process_Ind, bool Option_Id,int facilityId,bool cart_prepaid)
        {
            this.CartID = CartID;
            this.TopicID = TopicID;
            this.UserID = UserID;
            this.MediaType = MediaType;
            this.Score = Score;
            this.Lastmod = Lastmod;
            this.XmlTest = XmlTest;
            this.XmlSurvey = XmlSurvey;
            this.Quantity = Quantity;
            this.DiscountId = DiscountId;
            this.Process_Ind = Process_Ind;
            this.Option_Id = Option_Id;
            this.facilityId = facilityId;
            this.cart_prepaid = cart_prepaid;
        }
        private int _CartID = 0;
        public int CartID
        {
            get { return _CartID; }
            private set { _CartID = value; }
        }
        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }
        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            private set { _UserID = value; }
        }

        private string _MediaType = "";
        public string MediaType
        {
            get { return _MediaType; }
            private set { _MediaType = value; }
        }
        private int _Score = 0;
        public int Score
        {
            get { return _Score; }
            private set { _Score = value; }
        }
        private DateTime _Lastmod = System.DateTime.Now;
        public DateTime Lastmod
        {
            get { return _Lastmod; }
            private set { _Lastmod = value; }
        }
        private string _XmlTest = "";
        public string XmlTest
        {
            get { return _XmlTest; }
            private set { _XmlTest = value; }
        }
        private string _XmlSurvey = "";
        public string XmlSurvey
        {
            get { return _XmlSurvey; }
            private set { _XmlSurvey = value; }
        }
        private int _Quantity = 0;
        public int Quantity
        {
            get { return _Quantity; }
            private set { _Quantity = value; }
        }
        private int _DiscountId = 0;
        public int DiscountId
        {
            get { return _DiscountId; }
            private set { _DiscountId = value; }
        }

        private bool _Process_Ind = false;
        public bool Process_Ind
        {
            get { return _Process_Ind; }
            private set { _Process_Ind = value; }
        }
        private bool _Option_Id = false;
        public bool Option_Id
        {
            get { return _Option_Id; }
            private set { _Option_Id = value; }
        }
        private int _facilityId = 0;
        public int facilityId
         {
             get { return _facilityId; }
             set { _facilityId = value; }
         }
        private bool _cart_prepaid = false;
        public bool cart_prepaid
        {
            get { return _cart_prepaid; }
            set { _cart_prepaid = value; }
        }

    }
}
#endregion
#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

         /// <summary>
      /// Inserts a new Cart
      /// </summary>
      public  int InsertCart(CartInfo Cart)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              // no matching id, so we are good to go
              SqlCommand cmd = new SqlCommand("insert into cart " +
              "(TopicID, " +
              "UserID, " +
              "Media_Type, " +
              "Score, " +
              "LastMod, " +
              "XMLTest, " +
              "XMLSurvey, " +
              "Quantity, " +
              "DiscountId, " +
              "Process_Ind, " +
              "Optin_Ind, " +
              "cart_prepaid, " +
              "facilityId ) " +
              "VALUES (" +
              "@TopicID, " +
              "@UserID, " +
              "@Media_Type, " +
              "@Score, " +
              "@LastMod, " +
              "@XMLTest, " +
              "@XMLSurvey, " +
              "@Quantity, " +
              "@DiscountId, " +
              "@Process_Ind, " +
              "@OptionId, " +
              "@Cart_PrePaid, " +
              "@facilityid ) SET @ID = SCOPE_IDENTITY()", cn);

              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Cart.TopicID;
              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Cart.UserID;
              cmd.Parameters.Add("@Media_Type", SqlDbType.VarChar).Value = Cart.MediaType;
              cmd.Parameters.Add("@Score", SqlDbType.Int).Value = Cart.Score;
              cmd.Parameters.Add("@XMLTest", SqlDbType.VarChar).Value = Cart.XmlTest;
              cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = Cart.XmlSurvey;
              cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = Cart.Quantity;
              cmd.Parameters.Add("@DiscountId", SqlDbType.Int).Value = Cart.DiscountId;
              cmd.Parameters.Add("@OptionId", SqlDbType.Bit).Value = Cart.Option_Id;
              cmd.Parameters.Add("@Process_Ind", SqlDbType.Bit).Value = Cart.Process_Ind;
              cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = Cart.facilityId;
              cmd.Parameters.Add("@Cart_PrePaid", SqlDbType.Bit).Value = Cart.cart_prepaid;
                          
              // pass null to database if value is zero (nothing selected by user)
              // this allows the database to have a NULL foreign key and still enforce relational integrity
              if (Cart.Lastmod == System.DateTime.MinValue)
              {
                  cmd.Parameters.Add("@LastMod", SqlDbType.DateTime).Value = System.DBNull.Value;
              }
              else
              {
                  cmd.Parameters.Add("@LastMod", SqlDbType.DateTime).Value = Cart.Lastmod;
              }

              cn.Open();

              SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
              IDParameter.Direction = ParameterDirection.Output;
              cmd.Parameters.Add(IDParameter);
              cmd.ExecuteNonQuery();

              int NewID = (int)IDParameter.Value;
              return NewID;
          }
      }


      /// <summary>
      /// Updates a cart
      /// </summary>
      public  bool UpdateCart(CartInfo Cart)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("update Cart set " +
            "TopicID = @TopicID, " +
            "UserID = @UserID, " +
            "LastMod = @LastMod, " +
            "XMLTest = @XMLTest, " +
            "XMLSurvey = @XMLSurvey, " +
            "Media_Type = @Media_Type, "+
            "Score=@Score,"+
            "Quantity=@Quantity,"+
            "DiscountId=@DiscountId,"+
            "Process_Ind=@Process_Ind, " +
            "Optin_Ind=@OptionId , " +   
            "facilityid = @facilityid, " +
            "cart_prepaid = @cart_prepaid " +
            "where CartID = @CartID ", cn);
              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Cart.TopicID;
              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Cart.UserID;
              cmd.Parameters.Add("@Media_Type", SqlDbType.VarChar).Value = Cart.MediaType;
              cmd.Parameters.Add("@Score", SqlDbType.Int).Value = Cart.Score;
              cmd.Parameters.Add("@LastMod", SqlDbType.DateTime).Value = Cart.Lastmod;
              cmd.Parameters.Add("@XMLTest", SqlDbType.VarChar).Value = Cart.XmlTest;
              cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = Cart.XmlSurvey;
              cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = Cart.Quantity;
              cmd.Parameters.Add("@DiscountId", SqlDbType.Int).Value = Cart.DiscountId;
              cmd.Parameters.Add("@OptionId", SqlDbType.Bit).Value = Cart.Option_Id;
              cmd.Parameters.Add("@Process_Ind", SqlDbType.Bit).Value = Cart.Process_Ind;
              cmd.Parameters.Add("@CartID", SqlDbType.Int).Value = Cart.CartID;
              cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = Cart.facilityId;
              cmd.Parameters.Add("@cart_prepaid", SqlDbType.Bit).Value = Cart.cart_prepaid;
               cn.Open();
              int ret = ExecuteNonQuery(cmd);
              return (ret == 1);
          }
      }


      /// <summary>
      /// Retrieves all Carts for the specified UserID
      /// </summary>
      public  List<CartInfo> GetCartsByUserID(int UserID)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand = "select " +
                  "cartid, " +
                  "topicid, " +
                  "userid, " +
                  "media_type, " +
                  "score, " +
                  "lastmod, " +
                  "xmltest, " +
                  "xmlsurvey, " +
                  "quantity, " +
                  "discountid, " +
                  "process_ind, " +
                  "optin_ind , " +
                  "facilityid, " +
                  "cart_prepaid " +
                  "from cart " +
                  "where UserID = @UserID and process_ind = '0'" +
                  "order by LastMod DESC";

              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
              cn.Open();
              return GetCartCollectionFromReader(ExecuteReader(cmd), false);
          }
      }

         /// <summary>
      /// Retrieves all Carts for the specified UserID and DoamiNid
      /// </summary>
      public List<CartInfo> GetCartsByUserIDAndDomainId(int UserID, int DomainId, string cSortExpression)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string Query = "select " +
                  "cartid, " +
                  "topicid, " +
                  "userid, " +
                  "media_type, " +
                  "score, " +
                  "lastmod, " +
                  "xmltest, " +
                  "xmlsurvey, " +
                  "quantity, " +
                  "discountid, " +
                  "process_ind, " +
                  "optin_ind , " +
                  "facilityid, " +
                  "cart_prepaid " +
                  "from cart " +
                  "where UserID = @UserID and process_ind = '0' and facilityid=@DomainId " +
                  "order by LastMod DESC";

              SqlCommand cmd = new SqlCommand(Query, cn);
              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
              cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
              cn.Open();
              return GetCartCollectionFromReader(ExecuteReader(cmd), false);
          }

      }

      /// <summary>
      /// Retrieves Coupon Amount for the specified UserID
      /// </summary>
      public decimal GetCouponAmountByUserIdandCode(int UserId, string CouponCode, string Unlimited, int FacilityId, string TopicId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand;

              cSQLCommand = "select dbo.udf_couponamount( " + UserId + " , '" + CouponCode + " ' , '" + Unlimited + "' , '" + FacilityId + "', '" + TopicId + "' )";
             
              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@CouponCode", SqlDbType.VarChar).Value = CouponCode;
              cmd.Parameters.Add("@Unlimited", SqlDbType.VarChar).Value = Unlimited;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
                  return (decimal)reader[0];
              else
                  return -1;

          }
      }
      public decimal GetMSCouponAmountByUserIdandCode(int UserId, string CouponCode, string Unlimited, string FacilityId, string TopicId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand;

              cSQLCommand = "select dbo.udf_MS_couponamount( " + UserId + " , '" + CouponCode + " ' , '" + Unlimited + "' , '" + FacilityId + "', '" + TopicId + "' )";

              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@CouponCode", SqlDbType.VarChar).Value = CouponCode;
              cmd.Parameters.Add("@Unlimited", SqlDbType.VarChar).Value = Unlimited;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
                  return (decimal)reader[0];
              else
                  return -1;

          }
      }
      /// <summary>
      /// Retrieves Coupon Amount for the specified TopicID and CouponCode 
      /// </summary>
      public decimal GetCouponAmountByTopicIdandCode(int TopicId, string CouponCode)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand;

              cSQLCommand = "select dbo.udf_buynow_couponamount( " + TopicId + " , '" + CouponCode + "' )";

              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
              cmd.Parameters.Add("@CouponCode", SqlDbType.VarChar).Value = CouponCode;              
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
                  return (decimal)reader[0];
              else
                  return -1;
          }
      }

      /// <summary>
      ///  Retrieves the coupon amount for the Specifief Coupon code in PR
      /// </summary>
      /// 

      public decimal GetPRCouponAmountByUserIdandCode(int UserId, string CouponCode, string Unlimited, int facilityid)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string CommandText = "select dbo.udf_PR_couponamount ( " + UserId + " , '" + CouponCode + " ' , '" + Unlimited + "' , " + facilityid + " ) ";
              SqlCommand cmd = new SqlCommand(CommandText, cn);
              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@CouponCode", SqlDbType.VarChar).Value = CouponCode;
              cmd.Parameters.Add("@Unlimited", SqlDbType.VarChar).Value = Unlimited;
              cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
                  return (decimal)reader[0];
              else
                  return -1;

          }
      }

      /// <summary>
      ///  Retrieves whether user has Nutrition Dimension course in his/her shopping cart
      /// </summary>
      /// 

      public bool HasNutritionDimensionCourse(int UserId, int DomainId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string CommandText = "select count(*) from cart c join categorylink cl on c.topicid=cl.topicid "+						   
							"join categorymicrositelink cm on cm.categoryid=cl.categoryid "+
							"join micrositedomain ms on ms.msid=cm.msid " +
                            "where userid=@userid and ms.msid in (34,40,41) and process_ind=0 and facilityid=@DomainId";
              SqlCommand cmd = new SqlCommand(CommandText, cn);
              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;             
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
                  return (int)reader[0] > 0 ? true: false;
              else
                  return false;
          }
      }

      /// <summary>
      /// Retrieves all Incomplete Carts for the specified UserID
      /// </summary>
      public  List<CartInfo> GetIncompleteCartsByUserIDAndTopicID(int UserID, int TopicID)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand = "select " +
                  "cartid, " +
                  "topicid, " +
                  "userid, " +
                  "xmltest, " +
                  "media_type, " +
                  "score, " +
                  "lastmod, " +
                  "xmlsurvey, " +
                  "quantity, " +
                  "discountid, " +
                  "process_ind, " +
                  "optin_ind , " +
                  "facilityid, " +
                  "cart_prepaid " +
                  "from cart " +
                  "where UserID = @UserID " +
                  "and TopicID = @TopicID " +
                  "and process_ind='0' and media_type='B' " +
                  "order by LastMod DESC";

              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
              cn.Open();
              return GetCartCollectionFromReader(ExecuteReader(cmd), false);
          }
      }

      public CartInfo GetIncompleteCartByUserIDAndTopicIDAndMediaType(int userID, int topicID, string mediaType, int DomainId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand = "select " +
                  "cartid, " +
                  "topicid, " +
                  "userid, " +
                  "xmltest, " +
                  "media_type, " +
                  "score, " +
                  "lastmod, " +
                  "xmlsurvey, " +
                  "quantity, " +
                  "discountid, " +
                  "process_ind, " +
                  "optin_ind , " +
                  "facilityid, " +
                  "cart_prepaid " +
                  "from cart " +
                  "where UserID = @UserID " +
                  "and TopicID = @TopicID " +
                  "and media_Type = @MediaType " +
                  "and FacilityID = @DomainID " +
                  "and process_ind= 0 " +
                  "order by LastMod DESC";

              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = topicID;
              cmd.Parameters.Add("@DomainID", SqlDbType.Int).Value = DomainId;
              cmd.Parameters.Add("@MediaType", SqlDbType.VarChar).Value = mediaType;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
              if (reader.Read())
                  return GetCartFromReader(reader, true);
              else
                  return null; 
          }
      }


      /// <summary>
      /// Retrieves all Incomplete Carts for the specified UserID
      /// </summary>
      public  List<CartInfo> GetcompleteCartsByUserIDAndTopicID(int UserID, int TopicID)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand = "select " +
                  "cartid, " +
                  "topicid, " +
                  "userid, " +
                  "xmltest, " +
                  "media_type, " +
                  "score, " +
                  "lastmod, " +
                  "xmlsurvey, " +
                  "quantity, " +
                  "discountid, " +
                  "process_ind, " +
                  "optin_ind , " +
                  "facilityid, " +
                  "cart_prepaid " +
                  "from cart " +
                  "where UserID = @UserID " +
                  "and TopicID = @TopicID " +
                  "and process_ind='1' " +
                  "order by LastMod DESC";

              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
              cn.Open();
              return GetCartCollectionFromReader(ExecuteReader(cmd), false);
          }
      }

      /// <summary>
      /// Retrieves all Incomplete Carts for the specified UserID
      /// </summary>
      public CartInfo GetcompleteCartByUserIDAndTopicID(int UserID, int TopicID,int facilityID)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand = "select top 1" +
                  "cartid, " +
                  "topicid, " +
                  "userid, " +
                  "xmltest, " +
                  "media_type, " +
                  "score, " +
                  "lastmod, " +
                  "xmlsurvey, " +
                  "quantity, " +
                  "discountid, " +
                  "process_ind, " +
                  "optin_ind , " +
                  "facilityid, " +
                  "cart_prepaid " +
                  "from cart " +
                  "where UserID = @UserID " +
                  "and TopicID = @TopicID " +
                  "and facilityid = @FacilityID " +
                  "and process_ind='1' " +
                  "order by LastMod DESC";

              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
              cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = facilityID;
               cn.Open();
              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
              if (reader.Read())
                  return GetCartFromReader(reader, true);
              else
                  return null;              
          }
      }

      public List<CartInfo> GetInCompleteCartsByUserIdAndDomainId(int UserId, int DomainId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand = "select " +
                  "cartid, " +
                  "topicid, " +
                  "userid, " +
                  "xmltest, " +
                  "media_type, " +
                  "score, " +
                  "lastmod, " +
                  "xmlsurvey, " +
                  "quantity, " +
                  "discountid, " +
                  "process_ind, " +
                  "optin_ind , " +
                  "facilityid, " +
                  "cart_prepaid " +
                  "from cart " +
                  "where UserID = @UserID " +
                  "and facilityid = @DomainId " +
                  "and process_ind='0' " +
                  "order by LastMod DESC";

              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
              cn.Open();
              return GetCartCollectionFromReader(ExecuteReader(cmd), false);
          }

      }
      public List<CartInfo> GetTop5InCompleteCartsByUserId(int UserId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand = "select Top 5 " +
                  "cartid, " +
                  "topicid, " +
                  "userid, " +
                  "xmltest, " +
                  "media_type, " +
                  "score, " +
                  "lastmod, " +
                  "xmlsurvey, " +
                  "quantity, " +
                  "discountid, " +
                  "process_ind, " +
                  "optin_ind , " +
                  "facilityid, " +
                  "cart_prepaid " +
                  "from cart " +
                  "where UserID = @UserID " +                 
                  "and process_ind='0' " +
                  "order by LastMod DESC";

              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserId;             
              cn.Open();
              return GetCartCollectionFromReader(ExecuteReader(cmd), false);
          }

      }
      /// <summary>
      /// Retrieves all Incomplete Carts for the specified UserID
      /// </summary>
      public List<CartInfo> GetcompleteCartsByUserIDAndTopicID(int UserID, int TopicID,int DomainID)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand = "select " +
                  "cartid, " +
                  "topicid, " +
                  "userid, " +
                  "xmltest, " +
                  "media_type, " +
                  "score, " +
                  "lastmod, " +
                  "xmlsurvey, " +
                  "quantity, " +
                  "discountid, " +
                  "process_ind, " +
                  "optin_ind , " +
                  "facilityid, " +
                  "cart_prepaid " +
                  "from cart " +
                  "where UserID = @UserID " +
                  "and TopicID = @TopicID " +
                  "and facilityid = @DomainID " +
                  "and process_ind='1' " +
                  "order by LastMod DESC";

              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
              cmd.Parameters.Add("@DomainID", SqlDbType.Int).Value = DomainID;
              cn.Open();
              return GetCartCollectionFromReader(ExecuteReader(cmd), false);
          }
      }
      /// <summary>
      /// Retrieves all Incomplete Carts for the specified UserID
      /// </summary>
      public List<CartInfo> GetIncompleteCartsByUserIDAndTopicID(int UserID, int TopicID, int DomainID)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand = "select " +
                  "cartid, " +
                  "topicid, " +
                  "userid, " +
                  "xmltest, " +
                  "media_type, " +
                  "score, " +
                  "lastmod, " +
                  "xmlsurvey, " +
                  "quantity, " +
                  "discountid, " +
                  "process_ind, " +
                  "optin_ind , " +
                  "facilityid, " +
                  "cart_prepaid " +
                  "from cart " +
                  "where UserID = @UserID " +
                  "and TopicID = @TopicID " +
                  "and facilityid = @DomainID " +
                  "and process_ind='0' " +
                  "order by LastMod DESC";
              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

              cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
              cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
              cmd.Parameters.Add("@DomainID", SqlDbType.Int).Value = DomainID;
              cn.Open();
              return GetCartCollectionFromReader(ExecuteReader(cmd), false);
          }
      }


      /// <summary>
      /// Retrieves the Cart with the specified ID
      /// </summary>
      public  CartInfo GetCartByID(int CartID)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              string cSQLCommand = "select " +
                 "cartid, " +
                 "topicid, " +
                 "userid, " +
                 "xmltest, " +
                 "media_type, " +
                 "score, " +
                 "lastmod, " +
                 "xmlsurvey, " +
                 "quantity, " +
                 "discountid, " +
                 "process_ind, " +
                 "optin_ind  , " +
                 "facilityid, " +
                  "cart_prepaid " +
                 "from cart " +
                 "where CartID=@CartID ";
              SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
              cmd.Parameters.Add("@CartID", SqlDbType.Int).Value = CartID;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
              if (reader.Read())
                  return GetCartFromReader(reader, true);
              else
                  return null;
          }
      }


       ///<summary>
       ///
       ///</summary>
       ///
      public  int CheckAllOnline(int UserId,int DomainId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("select case when ((select count(cartid) from cart where userid=@UserId and process_ind='0' and facilityid=@DomainId )= (select count(cartid) from cart " +
                  " where userid=@UserId and process_ind='0' and facilityid =@DomainId and (media_type='O' or media_type='C'))) then 1 else 0 end as ind",cn);
              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
                  return (int)reader["ind"];
              else
                  return -1;

          }
      }


      /// <summary>
      /// Deletes a Cart
      /// </summary>
      public  bool DeleteCart(int CartID)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("delete from Cart where CartID=@CartID", cn);
              cmd.Parameters.Add("@CartID", SqlDbType.Int).Value = CartID;
              cn.Open();
              int ret = ExecuteNonQuery(cmd);
              return (ret == 1);
          }
      }

        /// <summary>
      /// Retrieves the Cart with price with the specified userID
      /// </summary>
      public  System.Data.DataSet GetCartPriceByUserId(int UserId)
      {
          SqlDataAdapter adapter;
          System.Data.DataSet dsCart= new DataSet();
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {

              SqlCommand cmd = new SqlCommand();
              cmd.Connection = cn;
              cn.Open();
              cmd.CommandText = "sp_r_shop_cart";
              cmd.CommandType=CommandType.StoredProcedure;
              cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = UserId;
              adapter = new SqlDataAdapter(cmd);
              adapter.Fill(dsCart);
          }
          return dsCart;
      }

      public DataSet GetCartPriceUserIdAndDomainId(int UserId, int DomainId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand();
              cn.Open();
              cmd.Connection = cn;
              cmd.CommandText = "sp_MS_shop_cart";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@Domain_id", SqlDbType.Int).Value = DomainId;
              SqlDataAdapter adapter = new SqlDataAdapter(cmd);
              System.Data.DataSet dsCart = new DataSet();
              adapter.Fill(dsCart);
              return dsCart;
          }
      }

      public DataSet GetCartPriceUserIdAndDomainIdWithoutCEPRO(int UserId, int DomainId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand();
              cn.Open();
              cmd.Connection = cn;
              cmd.CommandText = "sp_MS_shop_cart_NoCEPRO";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@Domain_id", SqlDbType.Int).Value = DomainId;
              SqlDataAdapter adapter = new SqlDataAdapter(cmd);
              System.Data.DataSet dsCart = new DataSet();
              adapter.Fill(dsCart);
              return dsCart;
          }
      }
        
      public decimal GetShipCostByUserIdAndDomainId(int UserId, int DomainId, bool Expedited)
      {
        
              using (SqlConnection cn = new SqlConnection(this.ConnectionString))
              {
                  SqlCommand cmd = new SqlCommand("select dbo.udf_MS_usershipping(@UserId,@DomainId,@Expedited) ", cn);
                  cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
                  cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
                  cmd.Parameters.Add("@Expedited", SqlDbType.Bit).Value = Expedited;
                
                  cn.Open();
                  IDataReader reader = ExecuteReader(cmd);
                  if (reader.Read())
                  {
                      if (String.IsNullOrEmpty(reader[0].ToString()))
                          return 0;
                      return (decimal)reader[0];
                  }
                  else
                      return (decimal)-1;
              }
         
      }
      public decimal GetTotalCEPROSavingByUseridDomainId(int UserId, int DomainId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("select sum(dbo.udf_MS_CEPRO_savingprice(cartid)) from cart where userid=@UserId and facilityid=@DomainId and process_ind=0 ", cn);
              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
              {
                  if (String.IsNullOrEmpty(reader[0].ToString()))
                      return 0;
                  return (decimal)reader[0];
              }
              else
                  return (decimal)-1;
          }
      }
      public decimal GetTotalCostByUseridDomainId(int UserId, int DomainId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("select sum(quantity * dbo.udf_MS_cartprice(cartid))+dbo.udf_MS_usershipping(@UserId,@DomainId,0) from cart where userid=@UserId and facilityid=@DomainId and process_ind=0 ", cn);
              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
              {
                  if (String.IsNullOrEmpty(reader[0].ToString()))
                      return 0;
                  return (decimal)reader[0];
              }
              else
                  return (decimal)-1;
          }
      }
      public decimal GetTotalCostWithCouponByUseridDomainId(int UserId, int DomainId, decimal CouponAmount)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("select dbo.udf_MS_cart_total_price_with_coupon(@UserId, @CouponAmount, @DomainId)+dbo.udf_MS_usershipping(@UserId,@DomainId,0) from cart where userid=@UserId and facilityid=@DomainId and process_ind=0 ", cn);
              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
              cmd.Parameters.Add("@CouponAmount", SqlDbType.Decimal).Value = CouponAmount;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
              {
                  if (String.IsNullOrEmpty(reader[0].ToString()))
                      return 0;
                  return (decimal)reader[0];
              }
              else
                  return (decimal)-1;
          }
      }
      public decimal GetSubTotalByUseridDomainIdWithoutCEPRO(int UserId, int DomainId, decimal CouponAmount)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("select dbo.udf_MS_newcart_total_price_with_coupon_nocepro(@UserId, @CouponAmount, @DomainID) from cart where userid=@UserId and facilityid=@DomainId and process_ind=0 ", cn);
              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
              cmd.Parameters.Add("@CouponAmount", SqlDbType.Decimal).Value = CouponAmount;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
              {
                  if (String.IsNullOrEmpty(reader[0].ToString()))
                      return 0;
                  return (decimal)reader[0];
              }
              else
                  return (decimal)-1;
          }
      }
     
      public bool ISCarthasUnlim(int Userid, int DomainId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("select Cartid from cart where media_type = 'U' and userid=@UserId and facilityid=@DomainId and process_ind=0", cn);
              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = Userid;
              cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
                  return true;
              else
                  return false;
          }
      }



      public  System.Data.DataSet GetUnlimCartPriceByUserId(int UserId)
      {
          SqlDataAdapter adapter;
          System.Data.DataSet dsCart = new DataSet();
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {

              SqlCommand cmd = new SqlCommand();
              cmd.Connection = cn;
              cn.Open();
              cmd.CommandText = "sp_r_ul_shop_cart";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = UserId;
              adapter = new SqlDataAdapter(cmd);
              adapter.Fill(dsCart);
          }
          return dsCart;
      }


      public System.Data.DataSet GetUnlimCartPrice()
      {
          SqlDataAdapter adapter;
          System.Data.DataSet dsCart = new DataSet();
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {

              SqlCommand cmd = new SqlCommand();
              cmd.Connection = cn;
              cn.Open();
              cmd.CommandText = "sp_r_unlimited";
              cmd.CommandType = CommandType.StoredProcedure;
              adapter = new SqlDataAdapter(cmd);
              adapter.Fill(dsCart);
          }
          return dsCart;
      }



      /// <summary>
      /// Retrieves the Cart with price with the specified cart Id
      /// </summary>
      public  Decimal GetCartPriceByCartId(int CartId)
      {
             using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("select dbo.udf_cartprice(@CartId) ", cn);
              cmd.Parameters.Add("@CartID", SqlDbType.Int).Value = CartId;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
                  return (decimal)reader[0];
              else
                  return (decimal)-1;
          }
       }

      /// <summary>
      /// Retrieves the Cart with price with the specified cart Id for Microsites
      /// </summary>
      public Decimal GetMSCartPriceByCartId(int CartId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("select dbo.udf_MS_cartprice(@CartId) ", cn);
              cmd.Parameters.Add("@CartID", SqlDbType.Int).Value = CartId;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
                  return (decimal)reader[0];
              else
                  return (decimal)-1;
          }
      }



      /// <summary>
      /// Retrieves the tax with the specified User Id
      /// </summary>
      public Decimal GetCartTaxByUserIdAndDomainId(int UserId, int DomainId, Boolean NextDayShip_Ind)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("select dbo.udf_MS_usercarttax(@UserId,@DomainId,@NextDay_Ind) ", cn);
              cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
              cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
              cmd.Parameters.Add("@NextDay_Ind", SqlDbType.Bit).Value = NextDayShip_Ind;
              cn.Open();
              IDataReader reader = ExecuteReader(cmd);
              if (reader.Read())
                  return (decimal)reader[0];
              else
                  return (decimal)-1;
          }
      }


      public  int CheckNumberOfPharmacy(int UserId)
      {
          //SqlDataAdapter adapter;
          System.Data.DataSet dsCart = new DataSet();
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {

              SqlCommand cmd = new SqlCommand();
              cmd.Connection = cn;
              cn.Open();
              cmd.CommandText = "SP_Check_Pharmacist";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add("@userid", SqlDbType.Int).Value = UserId;
              IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
              if (reader.Read())
                  return (int)reader[0];
              else
                  return 0;
          }

      }

      /// <summary>
      /// Retrieves the Cart with price with the specified userID
      /// </summary>
      public  System.Data.DataSet GetOrderPriceByUserId(int UserId)
      {
          SqlDataAdapter adapter;
          System.Data.DataSet dsCart = new DataSet();
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {

              SqlCommand cmd = new SqlCommand();
              cmd.Connection = cn;
              cn.Open();
              cmd.CommandText = "sp_r_order_summary";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = UserId;
              adapter = new SqlDataAdapter(cmd);
              adapter.Fill(dsCart);
          }
          return dsCart;
      }


      /// <summary>
      /// Retrieves the Cart with price with the specified userID
      /// </summary>
      public  System.Data.DataSet GetUnlimOrderPriceByUserId(int UserId)
      {
          SqlDataAdapter adapter;
          System.Data.DataSet dsCart = new DataSet();
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {

              SqlCommand cmd = new SqlCommand();
              cmd.Connection = cn;
              cn.Open();
              cmd.CommandText = "sp_r_ul_order_summary";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = UserId;
              adapter = new SqlDataAdapter(cmd);
              adapter.Fill(dsCart);
          }
          return dsCart;
      }


      public int GetCartCountByUserId(int UserId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand();
              cmd.Connection = cn;
              cn.Open();
              cmd.CommandText = "select count(cartid) from cart where userid= " + UserId.ToString() + " and process_ind=0";
              cmd.CommandType = CommandType.Text;
              return Convert.ToInt32(cmd.ExecuteScalar());
          }

      }

      /// <summary>
      /// Retrieves the Cart with price 
      /// </summary>
      public System.Data.DataSet GetOnlyUnlimOrderPrice()
      {
          SqlDataAdapter adapter;
          System.Data.DataSet dsCart = new DataSet();
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {

              SqlCommand cmd = new SqlCommand();
              cmd.Connection = cn;
              cn.Open();
              cmd.CommandText = "sp_r_unlimited_Order";
              cmd.CommandType = CommandType.StoredProcedure;
              adapter = new SqlDataAdapter(cmd);
              adapter.Fill(dsCart);
          }
          return dsCart;
      }
      /// <summary>
      /// Retrieves the Cart with price with the specified userID
      /// </summary>
      public System.Data.DataSet GetSavedCartPriceByUserId(int UserId)
      {
          SqlDataAdapter adapter;
          System.Data.DataSet dsCart = new DataSet();
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {

              SqlCommand cmd = new SqlCommand();
              cmd.Connection = cn;
              cn.Open();
              cmd.CommandText = "sp_get_saved_cart";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = UserId;
              adapter = new SqlDataAdapter(cmd);
              adapter.Fill(dsCart);
          }
          return dsCart;
      }


      public int Move_To_Cart(int SavedCartId)
      {
          
         using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
             SqlCommand cmd = new SqlCommand();
              cmd.Connection = cn;
              cn.Open();
              cmd.CommandText = "sp_Move_To_Cart";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add("@SavedCart_Id", SqlDbType.Int).Value = SavedCartId;
              SqlParameter retValParam = new SqlParameter("@RETURN_VALUE", SqlDbType.Int) ;
              retValParam.Direction = ParameterDirection.ReturnValue;
              cmd.Parameters.Add(retValParam);
              IDataReader reader = ExecuteReader(cmd);
              return Convert.ToInt32(retValParam.Value);
          }

      }

      public int Move_To_SavedCart(int CartId)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand();
              cmd.Connection = cn;
              cn.Open();
              cmd.CommandText = "sp_Move_To_SavedCart";
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add("@Cart_Id", SqlDbType.Int).Value = CartId;
              SqlParameter retValParam = new SqlParameter("@RETURN_VALUE", SqlDbType.Int);
              retValParam.Direction = ParameterDirection.ReturnValue;
              cmd.Parameters.Add(retValParam);
              IDataReader reader = ExecuteReader(cmd);
              return Convert.ToInt32(retValParam.Value);
          }

      }

      /// <summary>
      /// Deletes a Cart
      /// </summary>
      public bool DeleteSavedCart(int CartID)
      {
          using (SqlConnection cn = new SqlConnection(this.ConnectionString))
          {
              SqlCommand cmd = new SqlCommand("delete from SavedCart where CartID=@CartID", cn);
              cmd.Parameters.Add("@CartID", SqlDbType.Int).Value = CartID;
              cn.Open();
              int ret = ExecuteNonQuery(cmd);
              return (ret == 1);
          }
      }


         #endregion
        #region PRProvider
         protected virtual CartInfo GetCartFromReader(IDataReader reader)
      {
          return GetCartFromReader(reader, true);
      }
  
      protected virtual CartInfo GetCartFromReader(IDataReader reader, bool readMemos)
      {
          // NOTE: NULL integer foreign key LectEvtID is replaced with value of
          // zero, which will trigger "Please select..." or "(Not Selected)"
          // message in UI dropdown for that field
          CartInfo Cart = new CartInfo(
            (int)reader["CartID"], 
            (int)reader["TopicID"],
            (int)reader["UserID"],
            reader["Media_Type"].ToString(),
            (int)reader["Score"],
            (DateTime)reader["Lastmod"],
            reader["XmlTest"].ToString(),
            reader["XmlSurvey"].ToString(),
            (int)reader["Quantity"],
            (int)reader["DiscountId"],
            (bool)reader["Process_Ind"],
            (bool)reader["Optin_Ind"],
            (int)reader["facilityid"],
            (bool)reader["cart_prepaid"]);
                    return Cart;
      } 
     
      /// <summary>
      /// Returns a collection of TestInfo objects with the data read from the input DataReader
      /// </summary>
      protected virtual List<CartInfo> GetCartCollectionFromReader(IDataReader reader)
      {
          return GetCartCollectionFromReader(reader, true);
      }
      protected virtual List<CartInfo> GetCartCollectionFromReader(IDataReader reader, bool readMemos)
      {
          List<CartInfo> Carts = new List<CartInfo>();
          while (reader.Read())
              Carts.Add(GetCartFromReader(reader, readMemos));
          return Carts;
      }
    
#endregion

    }

}
#endregion