﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

/// <summary>
/// Summary description for AhDomainSQL
/// </summary>

#region AhDomainInfo

namespace PearlsReview.DAL
{

    /// <summary> 
    /// Summary description for AhDomainInfo

    /// </summary>
    public class AhDomainInfo
    {

        public AhDomainInfo() { }


        public AhDomainInfo(int ad_id, string ad_name, string ad_domainname, string ad_fullurl, string ad_defaultpage, string ad_logo,
            string ad_largeimage, int ad_default_licensetype, bool single_prof_ind, string primary_prof_url, string contact_phone,
            string contact_email, string contact_helptext, string contact_helppage, string ad_header, string ad_footer)
        {
            this.Ad_id = ad_id;
            this.Ad_name = ad_name;
            this.Ad_domainname = ad_domainname;
            this.Ad_fullurl = ad_fullurl;
            this.Ad_defaultpage = ad_defaultpage;
            this.Ad_logo = ad_logo;
            this.Ad_largeimage = ad_largeimage;
            this.Ad_default_licensetype = ad_default_licensetype;
            this.Single_prof_ind = single_prof_ind;
            this.Primary_prof_url = primary_prof_url;
            this.Contact_phone = contact_phone;
            this.Contact_email = contact_email;
            this.Contact_helptext = contact_helptext;
            this.Contact_helppage = contact_helppage;
            this.Ad_header = ad_header;
            this.Ad_footer = ad_footer;
        }

        private int _ad_id = 0;
        public int Ad_id
        {
            get { return _ad_id; }
            protected set { _ad_id = value; }
        }

        private string _ad_name = "";
        public string Ad_name
        {
            get { return _ad_name; }
            protected set { _ad_name = value; }
        }

        private string _ad_domainname = "";
        public string Ad_domainname
        {
            get { return _ad_domainname; }
            protected set { _ad_domainname = value; }
        }

        private string _ad_fullurl = "";
        public string Ad_fullurl
        {
            get { return _ad_fullurl; }
            protected set { _ad_fullurl = value; }
        }

        private string _ad_defaultpage = "";
        public string Ad_defaultpage
        {
            get { return _ad_defaultpage; }
            protected set { _ad_defaultpage = value; }
        }

        private string _ad_logo = "";
        public string Ad_logo
        {
            get { return _ad_logo; }
            protected set { _ad_logo = value; }
        }

        private string _ad_largeimage = "";
        public string Ad_largeimage
        {
            get { return _ad_largeimage; }
            protected set { _ad_largeimage = value; }
        }

        private int _ad_default_licensetype = 0;
        public int Ad_default_licensetype
        {
            get { return _ad_default_licensetype; }
            protected set { _ad_default_licensetype = value; }
        }

        private bool _single_prof_ind;
        public bool Single_prof_ind
        {
            get { return _single_prof_ind; }
            protected set { _single_prof_ind = value; }
        }

        private string _primary_prof_url = "";
        public string Primary_prof_url
        {
            get { return _primary_prof_url; }
            protected set { _primary_prof_url = value; }
        }

        private string _contact_phone = "";
        public string Contact_phone
        {
            get { return _contact_phone; }
            protected set { _contact_phone = value; }
        }

        private string _contact_email = "";
        public string Contact_email
        {
            get { return _contact_email; }
            protected set { _contact_email = value; }
        }

        private string _contact_helptext = "";
        public string Contact_helptext
        {
            get { return _contact_helptext; }
            protected set { _contact_helptext = value; }
        }

        private string _contact_helppage = "";
        public string Contact_helppage
        {
            get { return _contact_helppage; }
            protected set { _contact_helppage = value; }
        }

        private string _ad_header = "";
        public string Ad_header
        {
            get { return _ad_header; }
            protected set { _ad_header = value; }
        }

        private string _ad_footer = "";
        public string Ad_footer
        {
            get { return _ad_footer; }
            protected set { _ad_footer = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

 namespace PearlsReview.DAL.SQLClient
    {
        public partial class SQL2PRProvider : DataAccess
        {
            #region SQLPRProvider

            /////////////////////////////////////////////////////////
            // Methods that work with AhDomain


            /// <summary>
            /// Retrieves all AhDomains
            /// </summary>
            public List<AhDomainInfo> GetAhDomain(string cSortExpression)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    string cSQLCommand;
                    cSQLCommand = "SELECT * from AhDomain ";

                    // add on ORDER BY if provided
                    if (cSortExpression.Length > 0)
                    {
                        cSQLCommand = cSQLCommand +
                            " order by " + cSortExpression;
                    }

                    SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                    cn.Open();
                    return GetAhDomainCollectionFromReader(ExecuteReader(cmd), false);
                }
            }

            //for admin site
            public List<AhDomainInfo> GetAhDomainAdmin(string cSortExpression)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    string cSQLCommand;
                    cSQLCommand = "SELECT * from AhDomain ";

                    // add on ORDER BY if provided
                    if (cSortExpression.Length > 0)
                    {
                        cSQLCommand = cSQLCommand +
                            " order by " + cSortExpression;
                    }

                    SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                    cn.Open();
                    return GetAhDomainCollectionFromReader(ExecuteReader(cmd), false);
                }
            }


            /// <summary>
            /// Retrieves the AhDomain with the specified ID
            /// </summary>
            public List<AhDomainInfo> GetAhDomainByID(int ad_id)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand("select * " +
                            "from AhDomain where ad_id=@ad_id", cn);

                    cmd.Parameters.Add("@ad_id", SqlDbType.Int).Value = ad_id;
                    cn.Open();
                    return GetAhDomainCollectionFromReader(ExecuteReader(cmd), false);
                    
                   
                }
            }
            public AhDomainInfo GetAhDomainByAdID(string Ad_id)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand("select * " +
                            "from AhDomain where ad_id=@ad_id", cn);

                    cmd.Parameters.Add("@ad_id", SqlDbType.VarChar).Value = Ad_id;
                    cn.Open();
                    IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                    if (reader.Read())
                        return GetAhDomainFromReader(reader, true);
                    else
                        return null;
                }
            }

            public AhDomainInfo GetAhDomainIDByName(string Ad_name)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {

                    SqlCommand cmd = new SqlCommand("select * " +
                            "from AhDomain where ad_name=@ad_name", cn);

                    cmd.Parameters.Add("@ad_name", SqlDbType.VarChar).Value = Ad_name;
                    cn.Open();
                    IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                    if (reader.Read())
                        return GetAhDomainFromReader(reader, true);
                    else
                        return null;
                }
            }





            /// <summary>
            /// Updates 
            /// </summary>
            public bool UpdateAhDomain(AhDomainInfo AhDomain)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    cn.Open();
                    SqlTransaction transaction;
                    SqlCommand cmd = cn.CreateCommand();
                    transaction = cn.BeginTransaction();
                    cmd.Transaction = transaction;
                    cmd.Connection = cn;
                    try
                    {
                        cmd.CommandText = "update AhDomain set " +
                            "ad_id =@ad_id, " +
                            "ad_name =@ad_name, " +
                            "ad_domainname =@ad_domainname, " +
                            "ad_fullurl =@ad_fullurl, " +
                            "ad_defaultpage =@ad_defaultpage, " +
                            "ad_logo =@ad_logo, " +
                            "ad_largeimage =@ad_largeimage, " +
                            "ad_default_licensetype =@ad_default_licensetype, " +
                            "single_prof_ind =@single_prof_ind, " +
                            "primary_prof_url =@primary_prof_url, " +
                            "contact_phone =@contact_phone, " +
                            "contact_email =@contact_email, " +
                            "contact_helptext =@contact_helptext, " +
                            "contact_helppage =@contact_helppage, " +
                            "ad_header =@ad_header, " +
                            "ad_footer =@ad_footer " +
                            "where ad_id =@ad_id";

                        cmd.Parameters.Add("@ad_id", SqlDbType.Int).Value = AhDomain.Ad_id;
                        cmd.Parameters.Add("@ad_name", SqlDbType.VarChar).Value = AhDomain.Ad_name;
                        cmd.Parameters.Add("@ad_domainname", SqlDbType.VarChar).Value = AhDomain.Ad_domainname;
                        cmd.Parameters.Add("@ad_fullurl", SqlDbType.VarChar).Value = AhDomain.Ad_fullurl;
                        cmd.Parameters.Add("@ad_defaultpage", SqlDbType.VarChar).Value = AhDomain.Ad_defaultpage;
                        cmd.Parameters.Add("@ad_logo", SqlDbType.VarChar).Value = AhDomain.Ad_logo;
                        cmd.Parameters.Add("@ad_largeimage", SqlDbType.VarChar).Value = AhDomain.Ad_largeimage;
                        cmd.Parameters.Add("@ad_default_licensetype", SqlDbType.Int).Value = AhDomain.Ad_default_licensetype;
                        cmd.Parameters.Add("@single_prof_ind", SqlDbType.Bit).Value = AhDomain.Single_prof_ind;
                        cmd.Parameters.Add("@primary_prof_url", SqlDbType.VarChar).Value = AhDomain.Primary_prof_url;
                        cmd.Parameters.Add("@contact_phone", SqlDbType.VarChar).Value = AhDomain.Contact_phone;
                        cmd.Parameters.Add("@contact_email", SqlDbType.VarChar).Value = AhDomain.Contact_email;
                        cmd.Parameters.Add("@contact_helptext", SqlDbType.VarChar).Value = AhDomain.Contact_helptext;
                        cmd.Parameters.Add("@contact_helppage", SqlDbType.VarChar).Value = AhDomain.Contact_helppage;
                        cmd.Parameters.Add("@ad_header", SqlDbType.VarChar).Value = AhDomain.Ad_header;
                        cmd.Parameters.Add("@ad_footer", SqlDbType.VarChar).Value = AhDomain.Ad_footer;


                        foreach (IDataParameter param in cmd.Parameters)
                        {
                            if (param.Value == null)
                                param.Value = DBNull.Value;
                        }
                        ExecuteNonQuery(cmd);


                        transaction.Commit();
                    }
                    catch
                    {
                        transaction.Rollback();
                        return false;
                    }
                    return true;
                }
            }
            /// <summary>
            /// Deletes
            /// </summary>
            public bool DeleteAhDomain(int ad_id)
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("delete from AhDomain where ad_id=@ad_id", cn);
                    cmd.Parameters.Add("@ad_id", SqlDbType.Int).Value = ad_id;
                    cn.Open();
                    int ret = ExecuteNonQuery(cmd);
                    return (ret == 1);
                }
            }



            

            #endregion

            #region PRProvider
            /////////////////////////////////////////////////////////
            // methods that work with Discount  

            /// <summary>
            /// Returns a new DiscountInfo instance filled with the DataReader's current record data
            /// </summary>
            protected virtual AhDomainInfo GetAhDomainFromReader(IDataReader reader)
            {
                return GetAhDomainFromReader(reader, true);
            }
            protected virtual AhDomainInfo GetAhDomainFromReader(IDataReader reader, bool readMemos)
            {
                AhDomainInfo AhDomain = new AhDomainInfo(
                       (int)reader["ad_id"],
                       reader["ad_name"].ToString(),
                       reader["ad_domainname"].ToString(),
                       reader["ad_fullurl"].ToString(),
                       reader["ad_defaultpage"].ToString(),
                       reader["ad_logo"].ToString(),
                       reader["ad_largeimage"].ToString(),
                       (int)reader["ad_default_licensetype"],

                       String.IsNullOrEmpty(reader["single_prof_ind"].ToString()) ? false : bool.Parse(reader["single_prof_ind"].ToString()),
                       reader["primary_prof_url"].ToString(),
                       reader["contact_phone"].ToString(),
                       reader["contact_email"].ToString(),
                       reader["contact_helptext"].ToString(),
                       reader["contact_helppage"].ToString(),
                       reader["ad_header"].ToString(),
                       reader["ad_footer"].ToString()
                       );


                return AhDomain;
            }


            /// <summary>
            /// Returns a collection of DiscountInfo objects with the data read from the input DataReader
            /// </summary>
            protected virtual List<AhDomainInfo> GetAhDomainCollectionFromReader(IDataReader reader)
            {
                return GetAhDomainCollectionFromReader(reader, true);
            }
            protected virtual List<AhDomainInfo> GetAhDomainCollectionFromReader(IDataReader reader, bool readMemos)
            {
                List<AhDomainInfo> AhDomain = new List<AhDomainInfo>();
                while (reader.Read())
                    AhDomain.Add(GetAhDomainFromReader(reader, readMemos));
                return AhDomain;
            }

            #endregion

        }
    }
    #endregion

