﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region RetailFacilityInfo

namespace PearlsReview.DAL
{
    public class RetailFacilityInfo
    {
        public RetailFacilityInfo(int rfacid, int facid, DateTime expiredate, bool renew, bool active, int seat, decimal recuramount, string membertype, string transid, string rfcomment, string cclast, string profileid)
        {
            this.Rfacid = rfacid;
            this.Facid = facid;
            this.Expiredate = expiredate;
            this.Renew = renew;
            this.Active = active;
            this.Seat = seat;
            this.RecurAmount = recuramount;
            this.MemberType = membertype;
            this.TransId = transid;
            this.Rfcomment = rfcomment;
            this.CCLast = cclast;
            this.ProfileID = profileid;
        }

        private int _rfacid = 0;
        public int Rfacid
        {
            get { return _rfacid; }
            set { _rfacid = value; }
        }
        private int _facid = 0;
        public int Facid
        {
            get { return _facid; }
            set { _facid = value; }
        }
        private DateTime _expiredate = System.DateTime.MinValue;
        public DateTime Expiredate
        {
            get { return _expiredate; }
            set { _expiredate = value; }
        }
        private bool _renew = false;
        public bool Renew
        {
            get { return _renew; }
            set { _renew = value; }
        }
        private bool _active = false;
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
        private int _seat = 0;
        public int Seat
        {
            get { return _seat; }
            set { _seat = value; }
        }
        private decimal _recurAmount = 0;
        public decimal RecurAmount
        {
            get { return _recurAmount; }
            set { _recurAmount = value; }
        }
        private string _membertype = "";
        public string MemberType
        {
            get { return _membertype; }
            set { _membertype = value; }
        }
        private string _transid = "";
        public string TransId
        {
            get { return _transid; }
            set { _transid = value; }
        }
        private string _rfcomment = "";
        public string Rfcomment
        {
            get { return _rfcomment; }
            set { _rfcomment = value; }
        }
        private string _cclast = "";
        public string CCLast
        {
            get { return _cclast; }
            set { _cclast = value; }
        }
        private string _profileid = "";
        public string ProfileID
        {
            get { return _profileid; }
            set { _profileid = value; }
        }
    }

}

#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>       
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        public int insertRetailFacility(RetailFacilityInfo retailfacility)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into RetailFacility " +
                    "(facid, " +
                     "expiredate, " +
                    "renew, " +
                    "active, " +
                    "seat, " +
                    "recuramount, " +
                    "profileid, " +
                    "membertype, " +
                    "transid, " +
                    "rfcomment, " +
                    "cclast) " +
                     "VALUES " +
                     "(@facid, " +
                    "@expiredate, " +
                    "@renew, " +
                    "@active, " +
                    "@seat, " +
                    "@recuramount, " +
                    "@profileid, " +
                    "@membertype, " +
                    "@transid, " +
                    "@rfcomment, " +
                    "@cclast) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@facid", SqlDbType.Int).Value = retailfacility.Facid;
                cmd.Parameters.Add("@expiredate", SqlDbType.DateTime).Value = retailfacility.Expiredate;
                cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = retailfacility.Renew ? 1 : 0;
                cmd.Parameters.Add("@active", SqlDbType.Bit).Value = retailfacility.Active ? 1 : 0; ;
                cmd.Parameters.Add("@seat", SqlDbType.Int).Value = retailfacility.Seat;
                cmd.Parameters.Add("@recuramount", SqlDbType.Decimal).Value = retailfacility.RecurAmount;
                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = retailfacility.ProfileID;
                cmd.Parameters.Add("@membertype", SqlDbType.VarChar).Value = retailfacility.MemberType;
                cmd.Parameters.Add("@transid", SqlDbType.VarChar).Value = retailfacility.TransId;
                cmd.Parameters.Add("@rfcomment", SqlDbType.VarChar).Value = retailfacility.Rfcomment;
                cmd.Parameters.Add("@cclast", SqlDbType.VarChar).Value = retailfacility.CCLast;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                return (int)IDParameter.Value;
            }
        }

        public bool updateRetailFacility(RetailFacilityInfo retailfacility)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update RetailFacility set " +
                    "facid = @facid, " +
                    "expiredate = @expiredate, " +
                    "renew = @renew, " +
                    "active = @active, " +
                    "seat = @seat, " +
                    "recuramount = @recuramount, " +
                    "profileid = @profileid, " +
                    "membertype = @membertype, " +
                    "transid = @transid, " +
                    "rfcomment = @rfcomment, " +
                    "cclast = @cclast " +
                    "where rfacid = @rfacid ", cn);

                cmd.Parameters.Add("@facid", SqlDbType.Int).Value = retailfacility.Facid;
                cmd.Parameters.Add("@expiredate", SqlDbType.DateTime).Value = retailfacility.Expiredate;
                cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = retailfacility.Renew ? 1 : 0;
                cmd.Parameters.Add("@active", SqlDbType.Bit).Value = retailfacility.Active ? 1 : 0;
                cmd.Parameters.Add("@seat", SqlDbType.Int).Value = retailfacility.Seat;
                cmd.Parameters.Add("@recuramount", SqlDbType.Decimal).Value = retailfacility.RecurAmount;
                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = retailfacility.ProfileID;
                cmd.Parameters.Add("@membertype", SqlDbType.VarChar).Value = retailfacility.MemberType;
                cmd.Parameters.Add("@transid", SqlDbType.VarChar).Value = retailfacility.TransId;
                cmd.Parameters.Add("@rfcomment", SqlDbType.VarChar).Value = retailfacility.Rfcomment;
                cmd.Parameters.Add("@cclast", SqlDbType.VarChar).Value = retailfacility.CCLast;

                cmd.Parameters.Add("@rfacid", SqlDbType.Int).Value = retailfacility.Rfacid;

                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public bool deleteRetailFacility(int rfacid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from RetailFacility where rfacid = @rfacid", cn);
                cmd.Parameters.Add("@rfacid", SqlDbType.Int).Value = rfacid;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public RetailFacilityInfo GetRetailFacilityByID(int rfacid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From RetailFacility Where rfacid = @rfacid", cn);
                cmd.Parameters.Add("@rfacid", SqlDbType.Int).Value = rfacid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetRetailFacilityFromReader(reader);
                else
                    return null;
            }
        }

        public RetailFacilityInfo GetRetailFacilityByFacID(int facid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From RetailFacility Where facid = @facid", cn);
                cmd.Parameters.Add("@facid", SqlDbType.Int).Value = facid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetRetailFacilityFromReader(reader);
                else
                    return null;
            }
        }
        public List<RetailFacilityInfo> DailyCEDPaymentCancelledUpdate()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                "from retailfacility where expiredate>=CONVERT(VARCHAR(10),DATEADD(day,0,GETDATE()),21) and expiredate <CONVERT(VARCHAR(10),DATEADD(day,1,GETDATE()),21) " +
                    " and Renew=0 and Active=1", cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                cn.Open();
                return GetRetailFacilityCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        public List<RetailFacilityInfo> DailyCEDPaymentUpdate()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                    "from retailfacility where expiredate <= CONVERT(VARCHAR(10),DATEADD(day,-2,GETDATE()),21) " +
                    " and Renew=1 and Active=1", cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                cn.Open();
                return GetRetailFacilityCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public bool DailyCEDFacilityAccountUpdate(int facilityid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "sp_Daily_CEDPayment_Update";
                cmd.Connection = cn;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret >= 1);
            }
        }

        #endregion


        #region PRProvider

        protected virtual RetailFacilityInfo GetRetailFacilityFromReader(IDataReader reader)
        {
            return GetRetailFacilityFromReader(reader, true);
        }
        protected virtual RetailFacilityInfo GetRetailFacilityFromReader(IDataReader reader, bool readMemos)
        {
            RetailFacilityInfo RetailFacility = new RetailFacilityInfo(
            Convert.IsDBNull(reader["rfacid"]) ? (int)0 : (int)reader["rfacid"],
            Convert.IsDBNull(reader["facid"]) ? (int)0 : (int)reader["facid"],
            Convert.IsDBNull(reader["expiredate"]) ? DateTime.MinValue : Convert.ToDateTime(reader["expiredate"]),
            (bool)reader["renew"],
            (bool)reader["active"],
            Convert.IsDBNull(reader["seat"]) ? (int)0 : (int)reader["seat"],
            Convert.IsDBNull(reader["recuramount"]) ? (decimal)0 : (decimal)reader["recuramount"],
            Convert.ToString(reader["membertype"]),
            Convert.ToString(reader["transid"]),
            Convert.ToString(reader["rfcomment"]),
            reader["cclast"].ToString(),
            Convert.ToString(reader["profileid"])
            );
            return RetailFacility;
        }

        protected virtual List<RetailFacilityInfo> GetRetailFacilityCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<RetailFacilityInfo> RetailFacilityList = new List<RetailFacilityInfo>();
            while (reader.Read())
            {
                RetailFacilityList.Add(GetRetailFacilityFromReader(reader, readMemos));
            }
            return RetailFacilityList;
        }

        protected virtual List<RetailFacilityInfo> GetRetailFacilityCollectionFromReader(IDataReader reader)
        {
            return GetRetailFacilityCollectionFromReader(reader, true);
        }

        #endregion

    }
}
#endregion