﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

/// <summary>
/// Summary description for ProHistorySQL
/// </summary>
#region ProHistoryInfo
namespace PearlsReview.DAL
{
    public class ProHistoryInfo
    {
        public ProHistoryInfo(int phid, int userid, int memberid, DateTime logdate, string historytype, string comment, DateTime lastupdate)
        {
            this.Phid = phid;
            this.Memberid = memberid;
            this.Userid = userid;
            this.Logdate = logdate;
            this.Historytype = historytype;
            this.Comment = comment;
            this.Lastupdate = lastupdate;
        }
        private int _phid = 0;
        public int Phid
        {
            get { return _phid; }
            set { _phid = value; }
        }
        private int _memberid = 0;
        public int Memberid
        {
            get { return _memberid; }
            set { _memberid = value; }
        }
        private int _userid = 0;
        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }
        private DateTime _logdate = System.DateTime.MinValue;
        public DateTime Logdate
        {
            get { return _logdate; }
            set { _logdate = value; }
        }
        private string _historytype;
        public string Historytype
        {
            get { return _historytype; }
            set { _historytype = value; }
        }
        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }
        private DateTime _lastupdate = System.DateTime.MinValue;
        public DateTime Lastupdate
        {
            get { return _lastupdate; }
            set { _lastupdate = value; }
        }
    }
}
#endregion
#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        public int insertProHistory(ProHistoryInfo ProHistory)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into ProHistory " +
                    "(memberid, " +
                    "userid, " +
                    "logdate, " +
                    "historytype, " +
                    "comment, " +
                    "lastupdate) " +
                    "VALUES " +
                    "(@memberid, " +
                    "@userid, " +
                    "@logdate, " +
                    "@historytype, " +
                    "@comment, " +
                    "@lastupdate) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@memberid", SqlDbType.Int).Value =
                    (ProHistory.Memberid == null ? (Object)DBNull.Value : (Object)ProHistory.Memberid);
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value =
                    (ProHistory.Userid == null ? (Object)DBNull.Value : (Object)ProHistory.Userid);
                cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = ProHistory.Logdate;
                cmd.Parameters.Add("@historytype", SqlDbType.VarChar).Value = ProHistory.Historytype;
                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = ProHistory.Comment;
                cmd.Parameters.Add("@lastupdate", SqlDbType.DateTime).Value = ProHistory.Lastupdate;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                return (int)IDParameter.Value;
            }
        }
        public List<ProHistoryInfo> GetProHistoryByMemberID(int MemberID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From ProHistory Where memberid = @memberid order by lastupdate desc", cn);
                cmd.Parameters.Add("@memberid", SqlDbType.Int).Value = MemberID;
                cn.Open();
                return GetProHistoryCollectionFromReader(cmd.ExecuteReader());
            }
        }
        public List<ProHistoryInfo> GetProHistoryByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From ProHistory Where userid = @UserID order by lastupdate desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return GetProHistoryCollectionFromReader(cmd.ExecuteReader());
            }
        }
        #endregion


        #region PRProvider

        protected virtual ProHistoryInfo GetProHistoryFromReader(IDataReader reader)
        {
            return GetProHistoryFromReader(reader, true);
        }
        protected virtual ProHistoryInfo GetProHistoryFromReader(IDataReader reader, bool readMemos)
        {
            ProHistoryInfo info = new ProHistoryInfo(
                Convert.IsDBNull(reader["phid"]) ? (int)0 : (int)reader["phid"],
                Convert.IsDBNull(reader["userid"]) ? (int)0 : (int)reader["userid"],
                Convert.IsDBNull(reader["memberid"]) ? (int)0 : (int)reader["memberid"],
                Convert.IsDBNull(reader["logdate"]) ? DateTime.MinValue : Convert.ToDateTime(reader["logdate"]),
                Convert.ToString(reader["historytype"].ToString()),
                Convert.ToString(reader["comment"].ToString()),
                Convert.IsDBNull(reader["lastupdate"]) ? DateTime.MinValue : Convert.ToDateTime(reader["lastupdate"]));

            return info;
        }

        protected virtual List<ProHistoryInfo> GetProHistoryCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<ProHistoryInfo> ProHistoryList = new List<ProHistoryInfo>();
            while (reader.Read())
            {
                ProHistoryList.Add(GetProHistoryFromReader(reader, readMemos));
            }
            return ProHistoryList;
        }
        protected virtual List<ProHistoryInfo> GetProHistoryCollectionFromReader(IDataReader reader)
        {
            return GetProHistoryCollectionFromReader(reader, true);
        }

        #endregion

    }
}
#endregion

