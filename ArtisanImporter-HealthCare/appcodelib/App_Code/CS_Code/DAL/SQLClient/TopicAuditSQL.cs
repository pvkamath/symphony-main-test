﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region PositionInfo

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for Test
    /// </summary>
    public class TopicAuditInfo
    {
        public TopicAuditInfo() { }
        public TopicAuditInfo(int TopicID, string plan_comm, string dates_pub,
           bool media_learner_ind, bool media_provider_ind, string media_other, string goal_purpose,
           string lead_nurse_planner, string former_planners, string target_audience,
           bool nau_feedback_ind, bool nau_survey_ind, bool nau_advances_ind,
           bool nau_emerging_ind, bool nau_deficits_ind, string nau_other, string na_findings,
            //string off_label_use, string conflict_of_interest,  //check
           string comm_support, string eval_method, string eval_tools,
           bool eval_cat_sat_ind, bool eval_cat_know_ind, bool eval_cat_skill_ind,
           bool eval_cat_practice_ind, bool eval_cat_rel_ind, string eval_cat_other,
           bool eval_data_refine_ind, bool eval_data_create_ind, bool eval_data_dis_ind,
           string eval_data_other, string how_feedback, string verif_compl,
           bool crit_compl_form_ind, bool crit_compl_score_ind, string crit_compl_other,
           string doc_compl, bool discl_compl_ind, bool discl_conflicts_ind,
           bool discl_comm_ind, bool discl_products_ind, bool discl_offlabel_ind,
           bool discl_bias_ind, bool discl_disability_ind, string discl_other,
           bool contact_calc_pilot_ind, bool contact_calc_author_ind,
           bool contact_calc_exp_ind, bool contact_calc_rev_ind, bool contact_calc_peer_ind,
           string contact_calc_other, bool adv_web_ind, bool adv_flyers_ind,
           bool adv_journals_ind, bool adv_accred_ind, string adv_other,
           string record_system, string topic_notes,
           string off_label_use, string conflict_of_interest, string gapAnalysis, 
            bool coi_not_app_ind, bool coi_removed_ind, bool coi_revised_ind,
            bool coi_not_award_ind, bool coi_undertaking1_ind, bool coi_undertaking2_ind, bool core_comp_value_ind, bool core_comp_role_ind, bool core_comp_com_ind, bool
            core_comp_team_ind, string core_comp_other)
        {
            this.TopicID = TopicID;
            this.plan_comm = plan_comm;
            this.dates_pub = dates_pub;
            this.media_learner_ind = media_learner_ind;
            this.media_provider_ind = media_provider_ind;
            this.media_other = media_other;
            this.goal_purpose = goal_purpose;
            this.lead_nurse_planner = lead_nurse_planner;
            this.former_planners = former_planners;
            this.target_audience = target_audience;
            this.nau_feedback_ind = nau_feedback_ind;
            this.nau_survey_ind = nau_survey_ind;
            this.nau_advances_ind = nau_advances_ind;
            this.nau_emerging_ind = nau_emerging_ind;
            this.nau_deficits_ind = nau_deficits_ind;
            this.nau_other = nau_other;
            this.na_findings = na_findings;
            //this.off_label_use = off_label_use;
            //this.conflict_of_interest = conflict_of_interest;
            this.comm_support = comm_support;
            this.eval_method = eval_method;
            this.eval_tools = eval_tools;
            this.eval_cat_sat_ind = eval_cat_sat_ind;
            this.eval_cat_know_ind = eval_cat_know_ind;
            this.eval_cat_skill_ind = eval_cat_skill_ind;
            this.eval_cat_practice_ind = eval_cat_practice_ind;
            this.eval_cat_rel_ind = eval_cat_rel_ind;
            this.eval_cat_other = eval_cat_other;
            this.eval_data_refine_ind = eval_data_refine_ind;
            this.eval_data_create_ind = eval_data_create_ind;
            this.eval_data_dis_ind = eval_data_dis_ind;
            this.eval_data_other = eval_data_other;
            this.how_feedback = how_feedback;
            this.verif_compl = verif_compl;
            this.crit_compl_form_ind = crit_compl_form_ind;
            this.crit_compl_score_ind = crit_compl_score_ind;
            this.crit_compl_other = crit_compl_other;
            this.doc_compl = doc_compl;
            this.discl_compl_ind = discl_compl_ind;
            this.discl_conflicts_ind = discl_conflicts_ind;
            this.discl_comm_ind = discl_comm_ind;
            this.discl_products_ind = discl_products_ind;
            this.discl_offlabel_ind = discl_offlabel_ind;
            this.discl_bias_ind = discl_bias_ind;
            this.discl_disability_ind = discl_disability_ind;
            this.discl_other = discl_other;
            this.contact_calc_pilot_ind = contact_calc_pilot_ind;
            this.contact_calc_author_ind = contact_calc_author_ind;
            this.contact_calc_exp_ind = contact_calc_exp_ind;
            this.contact_calc_rev_ind = contact_calc_rev_ind;
            this.contact_calc_peer_ind = contact_calc_peer_ind;
            this.contact_calc_other = contact_calc_other;
            this.adv_web_ind = adv_web_ind;
            this.adv_flyers_ind = adv_flyers_ind;
            this.adv_journals_ind = adv_journals_ind;
            this.adv_accred_ind = adv_accred_ind;
            this.adv_other = adv_other;
            this.record_system = record_system;
            this.topic_notes = topic_notes;
            this.off_label_use = off_label_use;
            this.conflict_of_interest = conflict_of_interest;
            this.GapAnalysis = gapAnalysis;
            this.coi_not_app_ind = coi_not_app_ind;
            this.coi_removed_ind = coi_removed_ind;
            this.coi_revised_ind = coi_revised_ind;
            this.coi_not_award_ind = coi_not_award_ind;
            this.coi_undertaking1_ind = coi_undertaking1_ind;
            this.coi_undertaking2_ind = coi_undertaking2_ind;
            this.core_comp_value_ind = core_comp_value_ind;
            this.core_comp_role_ind = core_comp_role_ind;
            this.core_comp_com_ind = core_comp_com_ind;
            this.core_comp_team_ind = core_comp_team_ind;
            this.core_comp_other = core_comp_other;
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            protected set { _TopicID = value; }
        }

        private string _plan_comm = "";
        public string plan_comm
        {
            get { return _plan_comm; }
            private set { _plan_comm = value; }
        }

        private string _dates_pub = "";
        public string dates_pub
        {
            get { return _dates_pub; }
            private set { _dates_pub = value; }
        }

        private bool _media_learner_ind = true;
        public bool media_learner_ind
        {
            get { return _media_learner_ind; }
            private set { _media_learner_ind = value; }
        }

        private bool _media_provider_ind = false;
        public bool media_provider_ind
        {
            get { return _media_provider_ind; }
            private set { _media_provider_ind = value; }
        }


        private string _media_other = "";
        public string media_other
        {
            get { return _media_other; }
            private set { _media_other = value; }
        }

        private string _goal_purpose = "";
        public string goal_purpose
        {
            get { return _goal_purpose; }
            private set { _goal_purpose = value; }
        }

        private string _lead_nurse_planner = "";
        public string lead_nurse_planner
        {
            get { return _lead_nurse_planner; }
            private set { _lead_nurse_planner = value; }
        }

        private string _former_planners = "";
        public string former_planners
        {
            get { return _former_planners; }
            private set { _former_planners = value; }
        }

        private string _target_audience = "";
        public string target_audience
        {
            get { return _target_audience; }
            private set { _target_audience = value; }
        }

        private bool _nau_feedback_ind = true;
        public bool nau_feedback_ind
        {
            get { return _nau_feedback_ind; }
            private set { _nau_feedback_ind = value; }
        }

        private bool _nau_survey_ind = true;
        public bool nau_survey_ind
        {
            get { return _nau_survey_ind; }
            private set { _nau_survey_ind = value; }
        }

        private bool _nau_advances_ind = true;
        public bool nau_advances_ind
        {
            get { return _nau_advances_ind; }
            private set { _nau_advances_ind = value; }
        }

        private bool _nau_emerging_ind = true;
        public bool nau_emerging_ind
        {
            get { return _nau_emerging_ind; }
            private set { _nau_emerging_ind = value; }
        }

        private bool _nau_deficits_ind = true;
        public bool nau_deficits_ind
        {
            get { return _nau_deficits_ind; }
            private set { _nau_deficits_ind = value; }
        }


        private string _nau_other = "";
        public string nau_other
        {
            get { return _nau_other; }
            private set { _nau_other = value; }
        }

        private string _na_findings = "";
        public string na_findings
        {
            get { return _na_findings; }
            private set { _na_findings = value; }
        }

        //private string _off_label_use = "";
        //public string off_label_use
        //{
        //    get { return _off_label_use; }
        //    private set { _off_label_use = value; }
        //}

        //private string _conflict_of_interest = "";
        //public string conflict_of_interest
        //{
        //    get { return _conflict_of_interest; }
        //    private set { _conflict_of_interest = value; }
        //}

        private string _comm_support = "";
        public string comm_support
        {
            get { return _comm_support; }
            private set { _comm_support = value; }
        }

        private string _eval_method = "";
        public string eval_method
        {
            get { return _eval_method; }
            private set { _eval_method = value; }
        }

        private string _eval_tools = "";
        public string eval_tools
        {
            get { return _eval_tools; }
            private set { _eval_tools = value; }
        }

        private bool _eval_cat_sat_ind = true;
        public bool eval_cat_sat_ind
        {
            get { return _eval_cat_sat_ind; }
            private set { _eval_cat_sat_ind = value; }
        }

        private bool _eval_cat_know_ind = true;
        public bool eval_cat_know_ind
        {
            get { return _eval_cat_know_ind; }
            private set { _eval_cat_know_ind = value; }
        }

        private bool _eval_cat_skill_ind = true;
        public bool eval_cat_skill_ind
        {
            get { return _eval_cat_skill_ind; }
            private set { _eval_cat_skill_ind = value; }
        }

        private bool _eval_cat_practice_ind = true;
        public bool eval_cat_practice_ind
        {
            get { return _eval_cat_practice_ind; }
            private set { _eval_cat_practice_ind = value; }
        }

        private bool _eval_cat_rel_ind = true;
        public bool eval_cat_rel_ind
        {
            get { return _eval_cat_rel_ind; }
            private set { _eval_cat_rel_ind = value; }
        }


        private string _eval_cat_other = "";
        public string eval_cat_other
        {
            get { return _eval_cat_other; }
            private set { _eval_cat_other = value; }
        }

        private bool _eval_data_refine_ind = true;
        public bool eval_data_refine_ind
        {
            get { return _eval_data_refine_ind; }
            private set { _eval_data_refine_ind = value; }
        }

        private bool _eval_data_create_ind = true;
        public bool eval_data_create_ind
        {
            get { return _eval_data_create_ind; }
            private set { _eval_data_create_ind = value; }
        }

        private bool _eval_data_dis_ind = true;
        public bool eval_data_dis_ind
        {
            get { return _eval_data_dis_ind; }
            private set { _eval_data_dis_ind = value; }
        }


        private string _eval_data_other = "";
        public string eval_data_other
        {
            get { return _eval_data_other; }
            private set { _eval_data_other = value; }
        }

        private string _how_feedback = "";
        public string how_feedback
        {
            get { return _how_feedback; }
            private set { _how_feedback = value; }
        }

        private string _verif_compl = "";
        public string verif_compl
        {
            get { return _verif_compl; }
            private set { _verif_compl = value; }
        }

        private bool _crit_compl_form_ind = true;
        public bool crit_compl_form_ind
        {
            get { return _crit_compl_form_ind; }
            private set { _crit_compl_form_ind = value; }
        }

        private bool _crit_compl_score_ind = true;
        public bool crit_compl_score_ind
        {
            get { return _crit_compl_score_ind; }
            private set { _crit_compl_score_ind = value; }
        }

        private string _crit_compl_other = "";
        public string crit_compl_other
        {
            get { return _crit_compl_other; }
            private set { _crit_compl_other = value; }
        }

        private string _doc_compl = "";
        public string doc_compl
        {
            get { return _doc_compl; }
            private set { _doc_compl = value; }
        }

        private bool _discl_compl_ind = true;
        public bool discl_compl_ind
        {
            get { return _discl_compl_ind; }
            private set { _discl_compl_ind = value; }
        }

        private bool _discl_conflicts_ind = true;
        public bool discl_conflicts_ind
        {
            get { return _discl_conflicts_ind; }
            private set { _discl_conflicts_ind = value; }
        }

        private bool _discl_comm_ind = true;
        public bool discl_comm_ind
        {
            get { return _discl_comm_ind; }
            private set { _discl_comm_ind = value; }
        }

        private bool _discl_products_ind = true;
        public bool discl_products_ind
        {
            get { return _discl_products_ind; }
            private set { _discl_products_ind = value; }
        }

        private bool _discl_offlabel_ind = true;
        public bool discl_offlabel_ind
        {
            get { return _discl_offlabel_ind; }
            private set { _discl_offlabel_ind = value; }
        }

        private bool _discl_bias_ind = true;
        public bool discl_bias_ind
        {
            get { return _discl_bias_ind; }
            private set { _discl_bias_ind = value; }
        }

        private bool _discl_disability_ind = true;
        public bool discl_disability_ind
        {
            get { return _discl_disability_ind; }
            private set { _discl_disability_ind = value; }
        }


        private string _discl_other = "";
        public string discl_other
        {
            get { return _discl_other; }
            private set { _discl_other = value; }
        }

        private bool _contact_calc_pilot_ind = true;
        public bool contact_calc_pilot_ind
        {
            get { return _contact_calc_pilot_ind; }
            private set { _contact_calc_pilot_ind = value; }
        }

        private bool _contact_calc_author_ind = true;
        public bool contact_calc_author_ind
        {
            get { return _contact_calc_author_ind; }
            private set { _contact_calc_author_ind = value; }
        }

        private bool _contact_calc_exp_ind = true;
        public bool contact_calc_exp_ind
        {
            get { return _contact_calc_exp_ind; }
            private set { _contact_calc_exp_ind = value; }
        }

        private bool _contact_calc_rev_ind = true;
        public bool contact_calc_rev_ind
        {
            get { return _contact_calc_rev_ind; }
            private set { _contact_calc_rev_ind = value; }
        }

        private bool _contact_calc_peer_ind = true;
        public bool contact_calc_peer_ind
        {
            get { return _contact_calc_peer_ind; }
            private set { _contact_calc_peer_ind = value; }
        }


        private string _contact_calc_other = "";
        public string contact_calc_other
        {
            get { return _contact_calc_other; }
            private set { _contact_calc_other = value; }
        }

        private bool _adv_web_ind = true;
        public bool adv_web_ind
        {
            get { return _adv_web_ind; }
            private set { _adv_web_ind = value; }
        }

        private bool _adv_flyers_ind = true;
        public bool adv_flyers_ind
        {
            get { return _adv_flyers_ind; }
            private set { _adv_flyers_ind = value; }
        }

        private bool _adv_journals_ind = true;
        public bool adv_journals_ind
        {
            get { return _adv_journals_ind; }
            private set { _adv_journals_ind = value; }
        }

        private bool _adv_accred_ind = true;
        public bool adv_accred_ind
        {
            get { return _adv_accred_ind; }
            private set { _adv_accred_ind = value; }
        }


        private string _adv_other = "";
        public string adv_other
        {
            get { return _adv_other; }
            private set { _adv_other = value; }
        }

        private string _record_system = "";
        public string record_system
        {
            get { return _record_system; }
            private set { _record_system = value; }
        }

        private string _topic_notes = "";
        public string topic_notes
        {
            get { return _topic_notes; }
            private set { _topic_notes = value; }
        }

        private string _off_label_use = "";
        public string off_label_use
        {
            get { return _off_label_use; }
            private set { _off_label_use = value; }
        }

        private string _conflict_of_interest = "";
        public string conflict_of_interest
        {
            get { return _conflict_of_interest; }
            private set { _conflict_of_interest = value; }
        }

        private String _gapAnalysis = "";
        public String GapAnalysis
        {
            get { return _gapAnalysis; }
            private set { _gapAnalysis = value; }
        }

        private bool _coi_not_app_ind = false;
        public bool coi_not_app_ind
        {
            get { return _coi_not_app_ind; }
            private set { _coi_not_app_ind = value; }
        }

        private bool _coi_removed_ind = false;
        public bool coi_removed_ind
        {
            get { return _coi_removed_ind; }
            private set { _coi_removed_ind = value; }
        }        
        
        private bool _coi_revised_ind = false;
        public bool coi_revised_ind
        {
            get { return _coi_revised_ind; }
            private set { _coi_revised_ind = value; }
        }

        private bool _coi_not_award_ind = false;
        public bool coi_not_award_ind
        {
            get { return _coi_not_award_ind; }
            private set { _coi_not_award_ind = value; }
        }

        private bool _coi_undertaking1_ind = false;
        public bool coi_undertaking1_ind
        {
            get { return _coi_undertaking1_ind; }
            private set { _coi_undertaking1_ind = value; }
        }

        private bool _coi_undertaking2_ind = false;
        public bool coi_undertaking2_ind
        {
            get { return _coi_undertaking2_ind; }
            private set { _coi_undertaking2_ind = value; }
        }

        private bool _core_comp_value_ind = false;
        public bool core_comp_value_ind
        {
            get { return _core_comp_value_ind; }
            private set { _core_comp_value_ind = value; }
        }

        private bool _core_comp_role_ind = false;
        public bool core_comp_role_ind
        {
            get { return _core_comp_role_ind; }
            private set { _core_comp_role_ind = value; }
        }

        private bool _core_comp_com_ind = false;
        public bool core_comp_com_ind
        {
            get { return _core_comp_com_ind; }
            private set { _core_comp_com_ind = value; }
        }

        private bool _core_comp_team_ind = false;
        public bool core_comp_team_ind
        {
            get { return _core_comp_team_ind; }
            private set { _core_comp_team_ind = value; }
        }

        private string _core_comp_other = "";
        public string core_comp_other
        {
            get { return _core_comp_other; }
            private set { _core_comp_other = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /// <summary>
        /// Retrieves all TopicAudits (not obsolete)
        /// </summary>
        public List<TopicAuditInfo> GetTopicAudit(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from TopicAudit ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicAuditCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves the TopicAudit with the specified ID
        /// </summary>
        public TopicAuditInfo GetTopicAuditByID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from TopicAudit where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicAuditFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a TopicAudit
        /// </summary>
        public bool DeleteTopicAudit(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from TopicAudit where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new TopicAudit
        /// </summary>
        public int InsertTopicAudit(TopicAuditInfo TopicAudit)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into TopicAudit " +
                "(topicid, " +
                "plan_comm, " +
                "dates_pub, " +
                "media_learner_ind, " +
                "media_provider_ind, " +
                "media_other, " +
                "goal_purpose, " +
                "lead_nurse_planner, " +
                "former_planners, " +
                "target_audience, " +
                "nau_feedback_ind, " +
                "nau_survey_ind, " +
                "nau_advances_ind, " +
                "nau_emerging_ind, " +
                "nau_deficits_ind, " +
                "nau_other, " +
                "na_findings, " +
                "comm_support, " +
                "eval_method, " +
                "eval_tools, " +
                "eval_cat_sat_ind, " +
                "eval_cat_know_ind, " +
                "eval_cat_skill_ind, " +
                "eval_cat_practice_ind, " +
                "eval_cat_rel_ind, " +
                "eval_cat_other, " +
                "eval_data_refine_ind, " +
                "eval_data_create_ind, " +
                "eval_data_dis_ind, " +
                "eval_data_other, " +
                "how_feedback, " +
                "verif_compl, " +
                "crit_compl_form_ind, " +
                "crit_compl_score_ind, " +
                "crit_compl_other, " +
                "doc_compl, " +
                "discl_compl_ind, " +
                "discl_conflicts_ind, " +
                "discl_comm_ind, " +
                "discl_products_ind, " +
                "discl_offlabel_ind, " +
                "discl_bias_ind, " +
                "discl_disability_ind, " +
                "discl_other, " +
                "contact_calc_pilot_ind, " +
                "contact_calc_author_ind, " +
                "contact_calc_exp_ind, " +
                "contact_calc_rev_ind, " +
                "contact_calc_peer_ind, " +
                "contact_calc_other, " +
                "adv_web_ind, " +
                "adv_flyers_ind, " +
                "adv_journals_ind, " +
                "adv_accred_ind, " +
                "adv_other, " +
                "record_system, " +
                "topic_notes, " +
                "off_label_use, " +
                "conflict_of_interest, " +
                "gap_analysis, " +
                "coi_not_app_ind, " +
                "coi_removed_ind, " +
                "coi_revised_ind, " +
                "coi_not_award_ind, " +
                "coi_undertaking1_ind, " +
                "coi_undertaking2_ind , " +
                "core_comp_value_ind , " +
                "core_comp_role_ind , " +
                "core_comp_com_ind , " +
                "core_comp_team_ind , " +
                "core_comp_other )" +
              "VALUES (@TopicID, " +
                "@plan_comm, " +
                "@dates_pub, " +
                "@media_learner_ind, " +
                "@media_provider_ind, " +
                "@media_other, " +
                "@goal_purpose, " +
                "@lead_nurse_planner, " +
                "@former_planners, " +
                "@target_audience, " +
                "@nau_feedback_ind, " +
                "@nau_survey_ind, " +
                "@nau_advances_ind, " +
                "@nau_emerging_ind, " +
                "@nau_deficits_ind, " +
                "@nau_other, " +
                "@na_findings, " +               
                "@comm_support, " +
                "@eval_method, " +
                "@eval_tools, " +
                "@eval_cat_sat_ind, " +
                "@eval_cat_know_ind, " +
                "@eval_cat_skill_ind, " +
                "@eval_cat_practice_ind, " +
                "@eval_cat_rel_ind, " +
                "@eval_cat_other, " +
                "@eval_data_refine_ind, " +
                "@eval_data_create_ind, " +
                "@eval_data_dis_ind, " +
                "@eval_data_other, " +
                "@how_feedback, " +
                "@verif_compl, " +
                "@crit_compl_form_ind, " +
                "@crit_compl_score_ind, " +
                "@crit_compl_other, " +
                "@doc_compl, " +
                "@discl_compl_ind, " +
                "@discl_conflicts_ind, " +
                "@discl_comm_ind, " +
                "@discl_products_ind, " +
                "@discl_offlabel_ind, " +
                "@discl_bias_ind, " +
                "@discl_disability_ind, " +
                "@discl_other, " +
                "@contact_calc_pilot_ind, " +
                "@contact_calc_author_ind, " +
                "@contact_calc_exp_ind, " +
                "@contact_calc_rev_ind, " +
                "@contact_calc_peer_ind, " +
                "@contact_calc_other, " +
                "@adv_web_ind, " +
                "@adv_flyers_ind, " +
                "@adv_journals_ind, " +
                "@adv_accred_ind, " +
                "@adv_other, " +
                "@record_system, " +
                "@topic_notes, " +
                "@off_label_use, " +
                "@conflict_of_interest, " +
                "@gap_analysis, " +
                "@coi_not_app_ind, " +
                "@coi_removed_ind, " +
                "@coi_revised_ind, " +
                "@coi_not_award_ind, " +
                "@coi_undertaking1_ind, " +
                "@coi_undertaking2_ind ,"+
                "@core_comp_value_ind , " +
                "@core_comp_role_ind , " +
                "@core_comp_com_ind , " +
                "@core_comp_team_ind , " +
                "@core_comp_other )" ,cn);
                //" SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicAudit.TopicID;
                cmd.Parameters.Add("@plan_comm", SqlDbType.VarChar).Value = TopicAudit.plan_comm;
                cmd.Parameters.Add("@dates_pub", SqlDbType.VarChar).Value = TopicAudit.dates_pub;
                cmd.Parameters.Add("@media_learner_ind", SqlDbType.Bit).Value = TopicAudit.media_learner_ind;
                cmd.Parameters.Add("@media_provider_ind", SqlDbType.Bit).Value = TopicAudit.media_provider_ind;
                cmd.Parameters.Add("@media_other", SqlDbType.VarChar).Value = TopicAudit.media_other;
                cmd.Parameters.Add("@goal_purpose", SqlDbType.VarChar).Value = TopicAudit.goal_purpose;
                cmd.Parameters.Add("@lead_nurse_planner", SqlDbType.VarChar).Value = TopicAudit.lead_nurse_planner;
                cmd.Parameters.Add("@former_planners", SqlDbType.VarChar).Value = TopicAudit.former_planners;
                cmd.Parameters.Add("@target_audience", SqlDbType.VarChar).Value = TopicAudit.target_audience;
                cmd.Parameters.Add("@nau_feedback_ind", SqlDbType.Bit).Value = TopicAudit.nau_feedback_ind;
                cmd.Parameters.Add("@nau_survey_ind", SqlDbType.Bit).Value = TopicAudit.nau_survey_ind;
                cmd.Parameters.Add("@nau_advances_ind", SqlDbType.Bit).Value = TopicAudit.nau_advances_ind;
                cmd.Parameters.Add("@nau_emerging_ind", SqlDbType.Bit).Value = TopicAudit.nau_emerging_ind;
                cmd.Parameters.Add("@nau_deficits_ind", SqlDbType.Bit).Value = TopicAudit.nau_deficits_ind;
                cmd.Parameters.Add("@nau_other", SqlDbType.VarChar).Value = TopicAudit.nau_other;
                cmd.Parameters.Add("@na_findings", SqlDbType.VarChar).Value = TopicAudit.na_findings;
                cmd.Parameters.Add("@comm_support", SqlDbType.VarChar).Value = TopicAudit.comm_support;
                cmd.Parameters.Add("@eval_method", SqlDbType.VarChar).Value = TopicAudit.eval_method;
                cmd.Parameters.Add("@eval_tools", SqlDbType.VarChar).Value = TopicAudit.eval_tools;
                cmd.Parameters.Add("@eval_cat_sat_ind", SqlDbType.Bit).Value = TopicAudit.eval_cat_sat_ind;
                cmd.Parameters.Add("@eval_cat_know_ind", SqlDbType.Bit).Value = TopicAudit.eval_cat_know_ind;
                cmd.Parameters.Add("@eval_cat_skill_ind", SqlDbType.Bit).Value = TopicAudit.eval_cat_skill_ind;
                cmd.Parameters.Add("@eval_cat_practice_ind", SqlDbType.Bit).Value = TopicAudit.eval_cat_practice_ind;
                cmd.Parameters.Add("@eval_cat_rel_ind", SqlDbType.Bit).Value = TopicAudit.eval_cat_rel_ind;
                cmd.Parameters.Add("@eval_cat_other", SqlDbType.VarChar).Value = TopicAudit.eval_cat_other;
                cmd.Parameters.Add("@eval_data_refine_ind", SqlDbType.Bit).Value = TopicAudit.eval_data_refine_ind;
                cmd.Parameters.Add("@eval_data_create_ind", SqlDbType.Bit).Value = TopicAudit.eval_data_create_ind;
                cmd.Parameters.Add("@eval_data_dis_ind", SqlDbType.Bit).Value = TopicAudit.eval_data_dis_ind;
                cmd.Parameters.Add("@eval_data_other", SqlDbType.VarChar).Value = TopicAudit.eval_data_other;
                cmd.Parameters.Add("@how_feedback", SqlDbType.VarChar).Value = TopicAudit.how_feedback;
                cmd.Parameters.Add("@verif_compl", SqlDbType.VarChar).Value = TopicAudit.verif_compl;
                cmd.Parameters.Add("@crit_compl_form_ind", SqlDbType.Bit).Value = TopicAudit.crit_compl_form_ind;
                cmd.Parameters.Add("@crit_compl_score_ind", SqlDbType.Bit).Value = TopicAudit.crit_compl_score_ind;
                cmd.Parameters.Add("@crit_compl_other", SqlDbType.VarChar).Value = TopicAudit.crit_compl_other;
                cmd.Parameters.Add("@doc_compl", SqlDbType.VarChar).Value = TopicAudit.doc_compl;
                cmd.Parameters.Add("@discl_compl_ind", SqlDbType.Bit).Value = TopicAudit.discl_compl_ind;
                cmd.Parameters.Add("@discl_conflicts_ind", SqlDbType.Bit).Value = TopicAudit.discl_conflicts_ind;
                cmd.Parameters.Add("@discl_comm_ind", SqlDbType.Bit).Value = TopicAudit.discl_comm_ind;
                cmd.Parameters.Add("@discl_products_ind", SqlDbType.Bit).Value = TopicAudit.discl_products_ind;
                cmd.Parameters.Add("@discl_offlabel_ind", SqlDbType.Bit).Value = TopicAudit.discl_offlabel_ind;
                cmd.Parameters.Add("@discl_bias_ind", SqlDbType.Bit).Value = TopicAudit.discl_bias_ind;
                cmd.Parameters.Add("@discl_disability_ind", SqlDbType.Bit).Value = TopicAudit.discl_disability_ind;
                cmd.Parameters.Add("@discl_other", SqlDbType.VarChar).Value = TopicAudit.discl_other;
                cmd.Parameters.Add("@contact_calc_pilot_ind", SqlDbType.Bit).Value = TopicAudit.contact_calc_pilot_ind;
                cmd.Parameters.Add("@contact_calc_author_ind", SqlDbType.Bit).Value = TopicAudit.contact_calc_author_ind;
                cmd.Parameters.Add("@contact_calc_exp_ind", SqlDbType.Bit).Value = TopicAudit.contact_calc_exp_ind;
                cmd.Parameters.Add("@contact_calc_rev_ind", SqlDbType.Bit).Value = TopicAudit.contact_calc_rev_ind;
                cmd.Parameters.Add("@contact_calc_peer_ind", SqlDbType.Bit).Value = TopicAudit.contact_calc_peer_ind;
                cmd.Parameters.Add("@contact_calc_other", SqlDbType.VarChar).Value = TopicAudit.contact_calc_other;
                cmd.Parameters.Add("@adv_web_ind", SqlDbType.Bit).Value = TopicAudit.adv_web_ind;
                cmd.Parameters.Add("@adv_flyers_ind", SqlDbType.Bit).Value = TopicAudit.adv_flyers_ind;
                cmd.Parameters.Add("@adv_journals_ind", SqlDbType.Bit).Value = TopicAudit.adv_journals_ind;
                cmd.Parameters.Add("@adv_accred_ind", SqlDbType.Bit).Value = TopicAudit.adv_accred_ind;
                cmd.Parameters.Add("@adv_other", SqlDbType.VarChar).Value = TopicAudit.adv_other;
                cmd.Parameters.Add("@record_system", SqlDbType.VarChar).Value = TopicAudit.record_system;
                cmd.Parameters.Add("@topic_notes", SqlDbType.VarChar).Value = TopicAudit.topic_notes;
                cmd.Parameters.Add("@off_label_use", SqlDbType.VarChar).Value = TopicAudit.off_label_use;
                cmd.Parameters.Add("@conflict_of_interest", SqlDbType.VarChar).Value = TopicAudit.conflict_of_interest;
                cmd.Parameters.Add("@gap_analysis", SqlDbType.VarChar).Value = TopicAudit.GapAnalysis;
                cmd.Parameters.Add("@coi_not_app_ind", SqlDbType.Bit).Value = TopicAudit.coi_not_app_ind;
                cmd.Parameters.Add("@coi_removed_ind", SqlDbType.Bit).Value = TopicAudit.coi_removed_ind;
                cmd.Parameters.Add("@coi_revised_ind", SqlDbType.Bit).Value = TopicAudit.coi_revised_ind;
                cmd.Parameters.Add("@coi_not_award_ind", SqlDbType.Bit).Value = TopicAudit.coi_not_award_ind;
                cmd.Parameters.Add("@coi_undertaking1_ind", SqlDbType.Bit).Value = TopicAudit.coi_undertaking1_ind;
                cmd.Parameters.Add("@coi_undertaking2_ind", SqlDbType.Bit).Value = TopicAudit.coi_undertaking2_ind;
                cmd.Parameters.Add("@core_comp_value_ind", SqlDbType.Bit).Value = TopicAudit.core_comp_value_ind;
                cmd.Parameters.Add("@core_comp_role_ind", SqlDbType.Bit).Value = TopicAudit.core_comp_role_ind;
                cmd.Parameters.Add("@core_comp_com_ind", SqlDbType.Bit).Value = TopicAudit.core_comp_com_ind;
                cmd.Parameters.Add("@core_comp_team_ind", SqlDbType.Bit).Value = TopicAudit.core_comp_team_ind;
                cmd.Parameters.Add("@core_comp_other", SqlDbType.VarChar).Value = TopicAudit.core_comp_other;
                //SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                //IDParameter.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                //int NewID = (int)IDParameter.Value;
                int NewID = TopicAudit.TopicID;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a TopicAudit
        /// </summary>
        public bool UpdateTopicAudit(TopicAuditInfo TopicAudit)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update TopicAudit set " +
                "plan_comm = @plan_comm, " +
                "dates_pub = @dates_pub, " +
                "media_learner_ind = @media_learner_ind, " +
                "media_provider_ind = @media_provider_ind, " +
                "media_other = @media_other, " +
                "goal_purpose = @goal_purpose, " +
                "lead_nurse_planner = @lead_nurse_planner, " +
                "former_planners = @former_planners, " +
                "target_audience = @target_audience, " +
                "nau_feedback_ind = @nau_feedback_ind, " +
                "nau_survey_ind = @nau_survey_ind, " +
                "nau_advances_ind = @nau_advances_ind, " +
                "nau_emerging_ind = @nau_emerging_ind, " +
                "nau_deficits_ind = @nau_deficits_ind, " +
                "nau_other = @nau_other, " +
                "na_findings = @na_findings, " +
                "comm_support = @comm_support, " +
                "eval_method = @eval_method, " +
                "eval_tools = @eval_tools, " +
                "eval_cat_sat_ind = @eval_cat_sat_ind, " +
                "eval_cat_know_ind = @eval_cat_know_ind, " +
                "eval_cat_skill_ind = @eval_cat_skill_ind, " +
                "eval_cat_practice_ind = @eval_cat_practice_ind, " +
                "eval_cat_rel_ind = @eval_cat_rel_ind, " +
                "eval_cat_other = @eval_cat_other, " +
                "eval_data_refine_ind = @eval_data_refine_ind, " +
                "eval_data_create_ind = @eval_data_create_ind, " +
                "eval_data_dis_ind = @eval_data_dis_ind, " +
                "eval_data_other = @eval_data_other, " +
                "how_feedback = @how_feedback, " +
                "verif_compl = @verif_compl, " +
                "crit_compl_form_ind = @crit_compl_form_ind, " +
                "crit_compl_score_ind = @crit_compl_score_ind, " +
                "crit_compl_other = @crit_compl_other, " +
                "doc_compl = @doc_compl, " +
                "discl_compl_ind = @discl_compl_ind, " +
                "discl_conflicts_ind = @discl_conflicts_ind, " +
                "discl_comm_ind = @discl_comm_ind, " +
                "discl_products_ind = @discl_products_ind, " +
                "discl_offlabel_ind = @discl_offlabel_ind, " +
                "discl_bias_ind = @discl_bias_ind, " +
                "discl_disability_ind = @discl_disability_ind, " +
                "discl_other = @discl_other, " +
                "contact_calc_pilot_ind = @contact_calc_pilot_ind, " +
                "contact_calc_author_ind = @contact_calc_author_ind, " +
                "contact_calc_exp_ind = @contact_calc_exp_ind, " +
                "contact_calc_rev_ind = @contact_calc_rev_ind, " +
                "contact_calc_peer_ind = @contact_calc_peer_ind, " +
                "contact_calc_other = @contact_calc_other, " +
                "adv_web_ind = @adv_web_ind, " +
                "adv_flyers_ind = @adv_flyers_ind, " +
                "adv_journals_ind = @adv_journals_ind, " +
                "adv_accred_ind = @adv_accred_ind, " +
                "adv_other = @adv_other, " +
                "record_system = @record_system, " +
                "topic_notes = @topic_notes, " +
                "off_label_use = @off_label_use, " +
                "conflict_of_interest = @conflict_of_interest, " +
                "gap_analysis = @gap_analysis, " +
                "coi_not_app_ind = @coi_not_app_ind, " +
                "coi_removed_ind = @coi_removed_ind, " +
                "coi_revised_ind = @coi_revised_ind, " +
                "coi_not_award_ind = @coi_not_award_ind, " +
                "coi_undertaking1_ind = @coi_undertaking1_ind, " +
                "coi_undertaking2_ind = @coi_undertaking2_ind, " +

                "core_comp_value_ind=@core_comp_value_ind , " +
                "core_comp_role_ind=@core_comp_role_ind , " +
                "core_comp_com_ind=@core_comp_com_ind , " +
                "core_comp_team_ind=@core_comp_team_ind , " +
                "core_comp_other=@core_comp_other " +
              "where TopicID = @TopicID ", cn);

                cmd.Parameters.Add("@plan_comm", SqlDbType.VarChar).Value = TopicAudit.plan_comm;
                cmd.Parameters.Add("@dates_pub", SqlDbType.VarChar).Value = TopicAudit.dates_pub;
                cmd.Parameters.Add("@media_learner_ind", SqlDbType.Bit).Value = TopicAudit.media_learner_ind;
                cmd.Parameters.Add("@media_provider_ind", SqlDbType.Bit).Value = TopicAudit.media_provider_ind;
                cmd.Parameters.Add("@media_other", SqlDbType.VarChar).Value = TopicAudit.media_other;
                cmd.Parameters.Add("@goal_purpose", SqlDbType.VarChar).Value = TopicAudit.goal_purpose;
                cmd.Parameters.Add("@lead_nurse_planner", SqlDbType.VarChar).Value = TopicAudit.lead_nurse_planner;
                cmd.Parameters.Add("@former_planners", SqlDbType.VarChar).Value = TopicAudit.former_planners;
                cmd.Parameters.Add("@target_audience", SqlDbType.VarChar).Value = TopicAudit.target_audience;
                cmd.Parameters.Add("@nau_feedback_ind", SqlDbType.Bit).Value = TopicAudit.nau_feedback_ind;
                cmd.Parameters.Add("@nau_survey_ind", SqlDbType.Bit).Value = TopicAudit.nau_survey_ind;
                cmd.Parameters.Add("@nau_advances_ind", SqlDbType.Bit).Value = TopicAudit.nau_advances_ind;
                cmd.Parameters.Add("@nau_emerging_ind", SqlDbType.Bit).Value = TopicAudit.nau_emerging_ind;
                cmd.Parameters.Add("@nau_deficits_ind", SqlDbType.Bit).Value = TopicAudit.nau_deficits_ind;
                cmd.Parameters.Add("@nau_other", SqlDbType.VarChar).Value = TopicAudit.nau_other;
                cmd.Parameters.Add("@na_findings", SqlDbType.VarChar).Value = TopicAudit.na_findings;
                cmd.Parameters.Add("@comm_support", SqlDbType.VarChar).Value = TopicAudit.comm_support;
                cmd.Parameters.Add("@eval_method", SqlDbType.VarChar).Value = TopicAudit.eval_method;
                cmd.Parameters.Add("@eval_tools", SqlDbType.VarChar).Value = TopicAudit.eval_tools;
                cmd.Parameters.Add("@eval_cat_sat_ind", SqlDbType.Bit).Value = TopicAudit.eval_cat_sat_ind;
                cmd.Parameters.Add("@eval_cat_know_ind", SqlDbType.Bit).Value = TopicAudit.eval_cat_know_ind;
                cmd.Parameters.Add("@eval_cat_skill_ind", SqlDbType.Bit).Value = TopicAudit.eval_cat_skill_ind;
                cmd.Parameters.Add("@eval_cat_practice_ind", SqlDbType.Bit).Value = TopicAudit.eval_cat_practice_ind;
                cmd.Parameters.Add("@eval_cat_rel_ind", SqlDbType.Bit).Value = TopicAudit.eval_cat_rel_ind;
                cmd.Parameters.Add("@eval_cat_other", SqlDbType.VarChar).Value = TopicAudit.eval_cat_other;
                cmd.Parameters.Add("@eval_data_refine_ind", SqlDbType.Bit).Value = TopicAudit.eval_data_refine_ind;
                cmd.Parameters.Add("@eval_data_create_ind", SqlDbType.Bit).Value = TopicAudit.eval_data_create_ind;
                cmd.Parameters.Add("@eval_data_dis_ind", SqlDbType.Bit).Value = TopicAudit.eval_data_dis_ind;
                cmd.Parameters.Add("@eval_data_other", SqlDbType.VarChar).Value = TopicAudit.eval_data_other;
                cmd.Parameters.Add("@how_feedback", SqlDbType.VarChar).Value = TopicAudit.how_feedback;
                cmd.Parameters.Add("@verif_compl", SqlDbType.VarChar).Value = TopicAudit.verif_compl;
                cmd.Parameters.Add("@crit_compl_form_ind", SqlDbType.Bit).Value = TopicAudit.crit_compl_form_ind;
                cmd.Parameters.Add("@crit_compl_score_ind", SqlDbType.Bit).Value = TopicAudit.crit_compl_score_ind;
                cmd.Parameters.Add("@crit_compl_other", SqlDbType.VarChar).Value = TopicAudit.crit_compl_other;
                cmd.Parameters.Add("@doc_compl", SqlDbType.VarChar).Value = TopicAudit.doc_compl;
                cmd.Parameters.Add("@discl_compl_ind", SqlDbType.Bit).Value = TopicAudit.discl_compl_ind;
                cmd.Parameters.Add("@discl_conflicts_ind", SqlDbType.Bit).Value = TopicAudit.discl_conflicts_ind;
                cmd.Parameters.Add("@discl_comm_ind", SqlDbType.Bit).Value = TopicAudit.discl_comm_ind;
                cmd.Parameters.Add("@discl_products_ind", SqlDbType.Bit).Value = TopicAudit.discl_products_ind;
                cmd.Parameters.Add("@discl_offlabel_ind", SqlDbType.Bit).Value = TopicAudit.discl_offlabel_ind;
                cmd.Parameters.Add("@discl_bias_ind", SqlDbType.Bit).Value = TopicAudit.discl_bias_ind;
                cmd.Parameters.Add("@discl_disability_ind", SqlDbType.Bit).Value = TopicAudit.discl_disability_ind;
                cmd.Parameters.Add("@discl_other", SqlDbType.VarChar).Value = TopicAudit.discl_other;
                cmd.Parameters.Add("@contact_calc_pilot_ind", SqlDbType.Bit).Value = TopicAudit.contact_calc_pilot_ind;
                cmd.Parameters.Add("@contact_calc_author_ind", SqlDbType.Bit).Value = TopicAudit.contact_calc_author_ind;
                cmd.Parameters.Add("@contact_calc_exp_ind", SqlDbType.Bit).Value = TopicAudit.contact_calc_exp_ind;
                cmd.Parameters.Add("@contact_calc_rev_ind", SqlDbType.Bit).Value = TopicAudit.contact_calc_rev_ind;
                cmd.Parameters.Add("@contact_calc_peer_ind", SqlDbType.Bit).Value = TopicAudit.contact_calc_peer_ind;
                cmd.Parameters.Add("@contact_calc_other", SqlDbType.VarChar).Value = TopicAudit.contact_calc_other;
                cmd.Parameters.Add("@adv_web_ind", SqlDbType.Bit).Value = TopicAudit.adv_web_ind;
                cmd.Parameters.Add("@adv_flyers_ind", SqlDbType.Bit).Value = TopicAudit.adv_flyers_ind;
                cmd.Parameters.Add("@adv_journals_ind", SqlDbType.Bit).Value = TopicAudit.adv_journals_ind;
                cmd.Parameters.Add("@adv_accred_ind", SqlDbType.Bit).Value = TopicAudit.adv_accred_ind;
                cmd.Parameters.Add("@adv_other", SqlDbType.VarChar).Value = TopicAudit.adv_other;
                cmd.Parameters.Add("@record_system", SqlDbType.VarChar).Value = TopicAudit.record_system;
                cmd.Parameters.Add("@topic_notes", SqlDbType.VarChar).Value = TopicAudit.topic_notes;
                cmd.Parameters.Add("@off_label_use", SqlDbType.VarChar).Value = TopicAudit.off_label_use;
                cmd.Parameters.Add("@conflict_of_interest", SqlDbType.VarChar).Value = TopicAudit.conflict_of_interest;
                cmd.Parameters.Add("@gap_analysis", SqlDbType.VarChar).Value = TopicAudit.GapAnalysis;
                cmd.Parameters.Add("@coi_not_app_ind", SqlDbType.Bit).Value = TopicAudit.coi_not_app_ind;
                cmd.Parameters.Add("@coi_removed_ind", SqlDbType.Bit).Value = TopicAudit.coi_removed_ind;
                cmd.Parameters.Add("@coi_revised_ind", SqlDbType.Bit).Value = TopicAudit.coi_revised_ind;
                cmd.Parameters.Add("@coi_not_award_ind", SqlDbType.Bit).Value = TopicAudit.coi_not_award_ind;
                cmd.Parameters.Add("@coi_undertaking1_ind", SqlDbType.Bit).Value = TopicAudit.coi_undertaking1_ind;
                cmd.Parameters.Add("@coi_undertaking2_ind", SqlDbType.Bit).Value = TopicAudit.coi_undertaking2_ind;

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicAudit.TopicID;
                cmd.Parameters.Add("@core_comp_value_ind", SqlDbType.Bit).Value = TopicAudit.core_comp_value_ind;
                cmd.Parameters.Add("@core_comp_role_ind", SqlDbType.Bit).Value = TopicAudit.core_comp_role_ind;
                cmd.Parameters.Add("@core_comp_com_ind", SqlDbType.Bit).Value = TopicAudit.core_comp_com_ind;
                cmd.Parameters.Add("@core_comp_team_ind", SqlDbType.Bit).Value = TopicAudit.core_comp_team_ind;
                cmd.Parameters.Add("@core_comp_other", SqlDbType.VarChar).Value = TopicAudit.core_comp_other;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new TopicAuditInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual TopicAuditInfo GetTopicAuditFromReader(IDataReader reader)
        {
            return GetTopicAuditFromReader(reader, true);
        }

        protected virtual TopicAuditInfo GetTopicAuditFromReader(IDataReader reader, bool readMemos)
        {
            TopicAuditInfo TopicAudit = new TopicAuditInfo(
              (int)reader["TopicID"],
              reader["plan_comm"].ToString(),
              reader["dates_pub"].ToString(),
              (bool)reader["media_learner_ind"],
              (bool)reader["media_provider_ind"],
              reader["media_other"].ToString(),
              reader["goal_purpose"].ToString(),
              reader["lead_nurse_planner"].ToString(),
              reader["former_planners"].ToString(),
              reader["target_audience"].ToString(),
              (bool)reader["nau_feedback_ind"],
              (bool)reader["nau_survey_ind"],
              (bool)reader["nau_advances_ind"],
              (bool)reader["nau_emerging_ind"],
              (bool)reader["nau_deficits_ind"],
              reader["nau_other"].ToString(),
              reader["na_findings"].ToString(),
                //reader["off_label_use"].ToString(),
                //reader["conflict_of_interest"].ToString(),
              reader["comm_support"].ToString(),
              reader["eval_method"].ToString(),
              reader["eval_tools"].ToString(),
              (bool)reader["eval_cat_sat_ind"],
              (bool)reader["eval_cat_know_ind"],
              (bool)reader["eval_cat_skill_ind"],
              (bool)reader["eval_cat_practice_ind"],
              (bool)reader["eval_cat_rel_ind"],
              reader["eval_cat_other"].ToString(),
              (bool)reader["eval_data_refine_ind"],
              (bool)reader["eval_data_create_ind"],
              (bool)reader["eval_data_dis_ind"],
              reader["eval_data_other"].ToString(),
              reader["how_feedback"].ToString(),
              reader["verif_compl"].ToString(),
              (bool)reader["crit_compl_form_ind"],
              (bool)reader["crit_compl_score_ind"],
              reader["crit_compl_other"].ToString(),
              reader["doc_compl"].ToString(),
              (bool)reader["discl_compl_ind"],
              (bool)reader["discl_conflicts_ind"],
              (bool)reader["discl_comm_ind"],
              (bool)reader["discl_products_ind"],
              (bool)reader["discl_offlabel_ind"],
              (bool)reader["discl_bias_ind"],
              (bool)reader["discl_disability_ind"],
              reader["discl_other"].ToString(),
              (bool)reader["contact_calc_pilot_ind"],
              (bool)reader["contact_calc_author_ind"],
              (bool)reader["contact_calc_exp_ind"],
              (bool)reader["contact_calc_rev_ind"],
              (bool)reader["contact_calc_peer_ind"],
              reader["contact_calc_other"].ToString(),
              (bool)reader["adv_web_ind"],
              (bool)reader["adv_flyers_ind"],
              (bool)reader["adv_journals_ind"],
              (bool)reader["adv_accred_ind"],
              reader["adv_other"].ToString(),
              reader["record_system"].ToString(),
              reader["topic_notes"].ToString(),
              reader["off_label_use"].ToString(),
              reader["conflict_of_interest"].ToString(), 
              reader["gap_analysis"].ToString(), 
              (bool)reader["coi_not_app_ind"],
              (bool)reader["coi_removed_ind"],
              (bool)reader["coi_revised_ind"],
              (bool)reader["coi_not_award_ind"],
              (bool)reader["coi_undertaking1_ind"],
              (bool)reader["coi_undertaking2_ind"],

              (bool)reader["core_comp_value_ind"],
              (bool)reader["core_comp_role_ind"],
              (bool)reader["core_comp_com_ind"],
              (bool)reader["core_comp_team_ind"],
              reader["core_comp_other"].ToString());
              

            return TopicAudit;
        }

        /// <summary>
        /// Returns a collection of TopicAuditInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<TopicAuditInfo> GetTopicAuditCollectionFromReader(IDataReader reader)
        {
            return GetTopicAuditCollectionFromReader(reader, true);
        }

        protected virtual List<TopicAuditInfo> GetTopicAuditCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TopicAuditInfo> TopicAudits = new List<TopicAuditInfo>();
            while (reader.Read())
                TopicAudits.Add(GetTopicAuditFromReader(reader, readMemos));
            return TopicAudits;
        }

        #endregion
    }
}
#endregion