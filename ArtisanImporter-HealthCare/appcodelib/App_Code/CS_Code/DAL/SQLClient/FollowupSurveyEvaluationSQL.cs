﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Alex Chen
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region FollowupSurveyEvaluationInfo

namespace PearlsReview.DAL
{
    public class FollowupSurveyEvaluationInfo
    {
        public FollowupSurveyEvaluationInfo() { }

        public FollowupSurveyEvaluationInfo(int fSID, int topicID, int testID, int qNumber,  DateTime surveyDate, int qAnswer)
        {
            this.FSID = fSID;
            this.TopicID = topicID;
            this.TestID = testID;
            this.QNumber = qNumber;            
            this.SurveyDate = surveyDate;
            this.QAnswer = qAnswer;
        }

        private int _FSID = 0;
        public int FSID
        {
            get { return _FSID; }
            set { _FSID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _TestID = 0;
        public int TestID
        {
            get { return _TestID; }
            set { _TestID = value; }
        }

        private int _QNumber = 0;
        public int QNumber
        {
            get { return _QNumber; }
            set { _QNumber = value; }
        }        

        private DateTime _SurveyDate = DateTime.Now;
        public DateTime SurveyDate
        {
            get { return _SurveyDate; }
            set { _SurveyDate = value; }
        }

        private int _QAnswer = 0;
        public int QAnswer
        {
            get { return _QAnswer; }
            set { _QAnswer = value; }
        }

    }
}

#endregion FollowupSurveyEvaluationInfo

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        public int InsertFollowupSurveyEvaluation(int topicID, int testID, DateTime surveyDate,int qNumber, int qAnswer)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into FollowupSurveyEvaluation " +
                    "(TopicID, " +
                    "TestID, " +
                    "SurveyDate, " +                   
                    "QNumber, " +
                    "QAnswer) " +
                    "VALUES (" +
                    "@TopicID, " +
                    "@TestID, " +
                    "@SurveyDate, " +                   
                    "@QNumber, " +
                    "@QAnswer) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = topicID;
                cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = testID;
                cmd.Parameters.Add("@SurveyDate", SqlDbType.DateTime).Value = surveyDate;                
                cmd.Parameters.Add("@QNumber", SqlDbType.Int).Value = qNumber;
                cmd.Parameters.Add("@QAnswer", SqlDbType.Int).Value = qAnswer;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;
            }
        }

        /// <summary>
        /// Delete Followup Survey Evaluation By TestId
        /// </summary>
        public bool DeleteFollowupSurveyEvaluationByTestId(int TestId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from FollowupSurveyEvaluation where TestId=@TestId", cn);
                cmd.Parameters.Add("@TestId", SqlDbType.Int).Value = TestId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
    }
}

#endregion SQLPRProvider and PRProvider
