﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

# region DepartmentInfo

namespace PearlsReview.DAL
{
    public class DepartmentInfo
    {
        public DepartmentInfo()
        {
        }
        public DepartmentInfo(int dept_id, string dept_name, string dept_abbrev, string dept_number,
                                        int dept_bed_total, int facilityid, bool dept_active_ind)
        {
            this.dept_id = dept_id;
            this.dept_name = dept_name;
            this.dept_abbrev = dept_abbrev;
            this.dept_number = dept_number;
            this.dept_bed_total = dept_bed_total;
            this.facilityid = facilityid;
            this.dept_active_ind = dept_active_ind;
        }
        public int dept_id
        {
            get;
            set;
        }

        public string dept_name
        {
            get;
            set;
        }

        public string dept_abbrev
        {
            get;
            set;
        }

        public string dept_number
        {
            get;
            set;
        }

        public int dept_bed_total
        {
            get;
            set;
        }

        public int facilityid
        {
            get;
            set;
        }
        public bool dept_active_ind
        {
            get;
            set;
        }
    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
         /// <summary>
        /// Updates a Department
        /// </summary>
        public bool UpdateDepartment(DepartmentInfo departmentInfo)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Dept SET " +
              "dept_name= @dept_name, " +
              "dept_abbrev = @dept_abbrev, " +
              "dept_number = @dept_number, " +
              "dept_bed_total =@dept_bed_total, " +
              "facilityid =@facilityid, " +
              "dept_active_ind =@dept_active_ind " +
              "where dept_id = @dept_id ", cn);
                cmd.Parameters.Add("@dept_id", SqlDbType.Int).Value = departmentInfo.dept_id;
                cmd.Parameters.Add("@dept_name", SqlDbType.VarChar).Value = departmentInfo.dept_name;
                cmd.Parameters.Add("@dept_abbrev", SqlDbType.VarChar).Value = departmentInfo.dept_abbrev == null ? DBNull.Value as Object : departmentInfo.dept_abbrev as Object;
                cmd.Parameters.Add("@dept_number", SqlDbType.VarChar).Value = departmentInfo.dept_number == null ? DBNull.Value as object : departmentInfo.dept_number as object;
                cmd.Parameters.Add("@dept_bed_total", SqlDbType.Int).Value =  departmentInfo.dept_bed_total; //departmentInfo.dept_bed_total == null ? DBNull.Value as object : departmentInfo.dept_bed_total as object;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = departmentInfo.facilityid;  //departmentInfo.facilityid == null ? DBNull.Value as Object : departmentInfo.facilityid as object;
                cmd.Parameters.Add("@dept_active_ind", SqlDbType.Bit).Value = departmentInfo.dept_active_ind;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        /// <summary>a
        /// Inserts the Department
        /// </summary>
        public int InsertDepartment(DepartmentInfo departmentInfo)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Dept " +
              "(dept_name," +
              " dept_abbrev, " +
              "dept_number, " +
              "dept_bed_total, " +
              "facilityid, " +
              "dept_active_ind) " +
              "VALUES (" +
              "@dept_name, " +
              "@dept_abbrev, " +
              "@dept_number, " +
              "@dept_bed_total, " +
              "@facilityid, " +
              "@dept_active_ind) SET @ID = SCOPE_IDENTITY()", cn);
                cmd.Parameters.Add("@dept_name", SqlDbType.VarChar).Value = departmentInfo.dept_name;
                cmd.Parameters.Add("@dept_abbrev", SqlDbType.VarChar).Value = departmentInfo.dept_abbrev == null ? DBNull.Value as Object : departmentInfo.dept_abbrev as Object;
                cmd.Parameters.Add("@dept_number", SqlDbType.VarChar).Value = departmentInfo.dept_number == null ? DBNull.Value as Object : departmentInfo.dept_number as Object;
                cmd.Parameters.Add("@dept_bed_total", SqlDbType.Int).Value = departmentInfo.dept_bed_total; // departmentInfo.dept_bed_total == null ? DBNull.Value as Object : departmentInfo.dept_bed_total as Object;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = departmentInfo.facilityid; //departmentInfo.facilityid == null ? DBNull.Value as Object : departmentInfo.facilityid as Object;
                cmd.Parameters.Add("@dept_active_ind", SqlDbType.Bit).Value = departmentInfo.dept_active_ind;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

                //throw new NotImplementedException();
            }
        }
        /// <summary>
        /// Deletes the Department
        /// </summary>
        public bool DeleteDepartment(int departmentID)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Dept where dept_id=@dept_id", cn);
                cmd.Parameters.Add("@dept_id", SqlDbType.Int).Value = departmentID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }


            // throw new NotImplementedException();
        }

        public List<DepartmentInfo> GetDepartments(string fac_id, string sortExpression)
        {
            List<DepartmentInfo> departments = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "SELECT dept_name,dept_id,dept_abbrev, dept_number, dept_bed_total, " +
                                        "facilityid, dept_active_ind  FROM Dept";

                if (fac_id.Length > 0)
                {
                    cSQLCommand = cSQLCommand + " where facilityid= " + fac_id;
                }

                // add on ORDER BY if provided
                if (sortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + sortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by dept_name";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                departments = new List<DepartmentInfo>();
                while (reader.Read())
                {
                    DepartmentInfo department = new DepartmentInfo();
                    department.dept_name = reader["dept_name"].ToString();
                    department.dept_id = reader["dept_id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["dept_id"]);
                    department.dept_abbrev = reader["dept_abbrev"].ToString();
                    department.dept_number = reader["dept_number"].ToString();
                    department.dept_bed_total = reader["dept_bed_total"] == DBNull.Value ? 0 : Convert.ToInt32(reader["dept_bed_total"]);
                    department.facilityid = reader["facilityid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["facilityid"]);
                    department.dept_active_ind = Convert.ToBoolean(reader["dept_active_ind"]);

                    departments.Add(department);
                }
            }
            return departments;
        }

        public List<DepartmentInfo> GetDepartmentsByUserID(int userID)
        {
            List<DepartmentInfo> departments = new List<DepartmentInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "SELECT a.dept_id, a.dept_name FROM Dept a INNER JOIN Manage b ON " +
                                     "a.dept_id = b.dept_id WHERE b.userid = @userid";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userID;
                cn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        DepartmentInfo dept = new DepartmentInfo();
                        dept.dept_id = Convert.ToInt32(reader["dept_id"]);
                        dept.dept_name = reader["dept_name"].ToString();
                        departments.Add(dept);
                    }
                }
            }
            return departments;
        }

        public DepartmentInfo GetUserDepartmentByDeptName(string DeptName)
        {
            DepartmentInfo department = new DepartmentInfo();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "SELECT dept_id, dept_name FROM Dept WHERE dept_name = @dept_name";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@dept_name", SqlDbType.VarChar).Value = DeptName;
                cn.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        department.dept_id = Convert.ToInt32(reader["dept_id"]);
                        department.dept_name = reader["dept_name"].ToString();                        
                    }
                }
            }           
            return department;
        }

       

        public void UpdateDepartmentsForUserID(List<DepartmentInfo> departments, int userID)
        {
            using (SqlConnection conn = new SqlConnection(this.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);

                // Must assign both transaction object and connection
                // to Command object for a pending local transaction
                command.Connection = conn;
                command.Transaction = transaction;

                try
                {
                    command.CommandText = "DELETE FROM Manage WHERE UserID=@UserID";
                    command.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
                    command.ExecuteNonQuery();

                    command.CommandText = "INSERT INTO Manage(UserID, Dept_ID) VALUES(@UserID, @DeptID)";
                    command.Parameters.Clear();
                    command.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@DeptID", SqlDbType.Int));
                    command.Parameters[0].Value = userID;
                    foreach (DepartmentInfo deptInfo in departments)
                    {
                        command.Parameters[1].Value = deptInfo.dept_id;
                        command.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                catch { transaction.Rollback(); return; }
                finally { transaction.Dispose(); }
            }
        }


        public DepartmentInfo GetDepartmentByID(int dept_id)
        {
            DepartmentInfo deptInfo = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select dept_id,dept_name,dept_abbrev,dept_number, " +
                        "dept_bed_total,facilityid,dept_active_ind from Dept where dept_id='" + dept_id + "'", cn);

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                deptInfo = new DepartmentInfo();
                while (reader.Read())
                {
                    deptInfo.dept_id = Convert.ToInt32(reader["dept_id"]);
                    deptInfo.dept_name = reader["dept_name"].ToString();
                    deptInfo.dept_abbrev = reader["dept_abbrev"].ToString();
                    deptInfo.dept_number = reader["dept_number"].ToString();
                    deptInfo.dept_bed_total = reader["dept_bed_total"] != DBNull.Value ? Convert.ToInt32(reader["dept_bed_total"]) : 0;
                    deptInfo.facilityid = reader["facilityid"] != DBNull.Value ? Convert.ToInt32(reader["facilityid"]) : 0;
                    deptInfo.dept_active_ind = reader["dept_active_ind"] != DBNull.Value ? Convert.ToBoolean(reader["dept_active_ind"]) : false;

                }
                reader.Dispose();
                return deptInfo;
            }
            //throw new NotImplementedException()
        }



        #endregion

        #region PRProvider

        //No files


        #endregion





    }
}

#endregion



