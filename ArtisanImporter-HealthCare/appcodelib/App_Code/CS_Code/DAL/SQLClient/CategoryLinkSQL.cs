﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region CategoryLinkInfo

namespace PearlsReview.DAL
{
    public class CategoryLinkInfo
    {
        public CategoryLinkInfo() { }

        public CategoryLinkInfo(int CatTopicID, int CategoryID, int TopicID)
        {
            this.CatTopicID = CatTopicID;
            this.CategoryID = CategoryID;
            this.TopicID = TopicID;
        }

        private int _CatTopicID = 0;
        public int CatTopicID
        {
            get { return _CatTopicID; }
            protected set { _CatTopicID = value; }
        }

        private int _CategoryID = 0;
        public int CategoryID
        {
            get { return _CategoryID; }
            private set { _CategoryID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with CategoryLinks

        /// <summary>
        /// Returns the total number of CategoryLinks
        /// </summary>
        public  int GetCategoryLinkCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from CategoryLink", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all CategoryLinks
        /// </summary>
        public  List<CategoryLinkInfo> GetCategoryLinks(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from CategoryLink";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the CategoryLink with the specified ID
        /// </summary>
        public  CategoryLinkInfo GetCategoryLinkByID(int CatTopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from CategoryLink where CatTopicID=@CatTopicID", cn);
                cmd.Parameters.Add("@CatTopicID", SqlDbType.Int).Value = CatTopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCategoryLinkFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a CategoryLink
        /// </summary>
        public  bool DeleteCategoryLink(int CatTopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from CategoryLink where CatTopicID=@CatTopicID", cn);
                cmd.Parameters.Add("@CatTopicID", SqlDbType.Int).Value = CatTopicID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new CategoryLink
        /// </summary>
        public  int InsertCategoryLink(CategoryLinkInfo CategoryLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into CategoryLink " +
              "(CategoryID, " +
              "TopicID) " +
              "VALUES (" +
              "@CategoryID, " +
              "@TopicID) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryLink.CategoryID;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = CategoryLink.TopicID;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a CategoryLink
        /// </summary>
        public  bool UpdateCategoryLink(CategoryLinkInfo CategoryLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update CategoryLink set " +
              "CategoryID = @CategoryID, " +
              "TopicID = @TopicID " +
              "where CatTopicID = @CatTopicID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryLink.CategoryID;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = CategoryLink.TopicID;
                cmd.Parameters.Add("@CatTopicID", SqlDbType.Int).Value = CategoryLink.CatTopicID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new CategoryLinkInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual CategoryLinkInfo GetCategoryLinkFromReader(IDataReader reader)
        {
            return GetCategoryLinkFromReader(reader, true);
        }
        protected virtual CategoryLinkInfo GetCategoryLinkFromReader(IDataReader reader, bool readMemos)
        {
            CategoryLinkInfo CategoryLink = new CategoryLinkInfo(
              (int)reader["CatTopicID"],
              (int)reader["CategoryID"],
              (int)reader["TopicID"]);


            return CategoryLink;
        }

        /// <summary>
        /// Returns a collection of CategoryLinkInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<CategoryLinkInfo> GetCategoryLinkCollectionFromReader(IDataReader reader)
        {
            return GetCategoryLinkCollectionFromReader(reader, true);
        }
        protected virtual List<CategoryLinkInfo> GetCategoryLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<CategoryLinkInfo> CategoryLinks = new List<CategoryLinkInfo>();
            while (reader.Read())
                CategoryLinks.Add(GetCategoryLinkFromReader(reader, readMemos));
            return CategoryLinks;
        }
        #endregion
    }
}
#endregion