﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region AdClickInfo

namespace PearlsReview.DAL
{
    public class AdClickInfo
    {
        public AdClickInfo() { }

        public AdClickInfo(int AdClickID, int AdID, DateTime Date, string PageName)
        {
            this.AdClickID = AdClickID;
            this.AdID = AdID;
            this.Date = Date;
            this.PageName = PageName;
        }

        private int _AdClickID = 0;
        public int AdClickID
        {
            get { return _AdClickID; }
            protected set { _AdClickID = value; }
        }

        private int _AdID = 0;
        public int AdID
        {
            get { return _AdID; }
            private set { _AdID = value; }
        }

        private DateTime _Date = System.DateTime.Now;
        public DateTime Date
        {
            get { return _Date; }
            private set { _Date = value; }
        }

        private string _PageName = "";
        public string PageName
        {
            get { return _PageName; }
            private set { _PageName = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with AdClicks

        /// <summary>
        /// Returns the total number of AdClicks
        /// </summary>
        public  int GetAdClickCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from AdClick", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all AdClicks
        /// </summary>
        public  List<AdClickInfo> GetAdClicks(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from AdClick";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetAdClickCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the AdClick with the specified ID
        /// </summary>
        public  AdClickInfo GetAdClickByID(int AdClickID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from AdClick where AdClickID=@AdClickID", cn);
                cmd.Parameters.Add("@AdClickID", SqlDbType.Int).Value = AdClickID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetAdClickFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a AdClick
        /// </summary>
        public  bool DeleteAdClick(int AdClickID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from AdClick where AdClickID=@AdClickID", cn);
                cmd.Parameters.Add("@AdClickID", SqlDbType.Int).Value = AdClickID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new AdClick
        /// </summary>
        public  int InsertAdClick(AdClickInfo AdClick)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into AdClick " +
              "(AdID, " +
              "Date, " +
              "PageName) " +
              "VALUES (" +
              "@AdID, " +
              "@Date, " +
              "@PageName) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = AdClick.AdID;
                cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = AdClick.Date;
                cmd.Parameters.Add("@PageName", SqlDbType.VarChar).Value = AdClick.PageName;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a AdClick
        /// </summary>
        public  bool UpdateAdClick(AdClickInfo AdClick)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update AdClick set " +
              "AdID = @AdID, " +
              "Date = @Date, " +
              "PageName = @PageName " +
              "where AdClickID = @AdClickID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@AdID", SqlDbType.Int).Value = AdClick.AdID;
                cmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = AdClick.Date;
                cmd.Parameters.Add("@PageName", SqlDbType.VarChar).Value = AdClick.PageName;
                cmd.Parameters.Add("@AdClickID", SqlDbType.Int).Value = AdClick.AdClickID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new AdClickInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual AdClickInfo GetAdClickFromReader(IDataReader reader)
        {
            return GetAdClickFromReader(reader, true);
        }
        protected virtual AdClickInfo GetAdClickFromReader(IDataReader reader, bool readMemos)
        {
            AdClickInfo AdClick = new AdClickInfo(
              (int)reader["AdClickID"],
              (int)reader["AdID"],
              (DateTime)reader["Date"],
              reader["PageName"].ToString());


            return AdClick;
        }

        /// <summary>
        /// Returns a collection of AdClickInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<AdClickInfo> GetAdClickCollectionFromReader(IDataReader reader)
        {
            return GetAdClickCollectionFromReader(reader, true);
        }
        protected virtual List<AdClickInfo> GetAdClickCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<AdClickInfo> AdClicks = new List<AdClickInfo>();
            while (reader.Read())
                AdClicks.Add(GetAdClickFromReader(reader, readMemos));
            return AdClicks;
        }
        #endregion
    }
}
#endregion