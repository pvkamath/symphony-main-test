﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;


#region UCEMembershipInfo

namespace PearlsReview.DAL
{

    public class UCEmembershipInfo
    {
        public UCEmembershipInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public UCEmembershipInfo(int UmID, int RnID, DateTime LogDate, DateTime ExpDate,
            string Verification, string ProfileID, bool Renew, bool Active, string Comment,
            float Amount, string CcNum)
        {

            this.UmID = UmID;
            this.RnID = RnID;
            this.LogDate = LogDate;
            this.ExpDate = ExpDate;
            this.Verification = Verification;
            this.ProfileID = ProfileID;
            this.Renew = Renew;
            this.Active = Active;
            this.Comment = Comment;
            this.Amount = Amount;
            this.CcNum = CcNum;

        }

        public UCEmembershipInfo(int RnID, DateTime LogDate, DateTime ExpDate,
           string Verification, string ProfileID, bool Renew, bool Active, string Comment,
           float Amount, string CcNum)
        {

            this.RnID = RnID;
            this.LogDate = LogDate;
            this.ExpDate = ExpDate;
            this.Verification = Verification;
            this.ProfileID = ProfileID;
            this.Renew = Renew;
            this.Active = Active;
            this.Comment = Comment;
            this.Amount = Amount;
            this.CcNum = CcNum;

        }
        private int _UmID = 0;
        public int UmID
        {
            get { return _UmID; }
            set { _UmID = value; }
        }

        private int _RnID = 0;
        public int RnID
        {
            get { return _RnID; }
            set { _RnID = value; }
        }

        private DateTime _LogDate = System.DateTime.MinValue;
        public DateTime LogDate
        {
            get { return _LogDate; }
            set { _LogDate = value; }
        }

        private DateTime _ExpDate = System.DateTime.MinValue;
        public DateTime ExpDate
        {
            get { return _ExpDate; }
            set { _ExpDate = value; }
        }

        private string _Verification = "";
        public string Verification
        {
            get { return _Verification; }
            set { _Verification = value; }
        }

        private string _ProfileID = "";
        public string ProfileID
        {
            get { return _ProfileID; }
            set { _ProfileID = value; }
        }

        private bool _Renew = false;
        public bool Renew
        {
            get { return _Renew; }
            set { _Renew = value; }
        }

        private bool _Active = false;
        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment.ToLower(); }
            set { _Comment = value.ToLower(); }
        }

        private float _Amount = 0;
        public float Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        private string _CcNum = "";
        public string CcNum
        {
            get { return _CcNum; }
            set { _CcNum = value; }
        }

        private bool _FirstRec;
        public bool FirstRec
        {
            get { return _FirstRec; }
            set { _FirstRec = value; }
        }

    }

}
#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        public bool UpdateUCEmembership(UCEmembershipInfo UCEmembership)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Update UCEmembership SET " +                    
                    "rnID = @rnID, " +
                    "logdate = @logdate, " +
                    "expdate = @expdate, " +
                    "verification = @verification, " +
                    "profileid = @profileid, " +
                    "renew = @renew, " +
                    "active = @active, " +
                    "comment = @comment, " +
                    "amount = @amount, " +
                    "ccnum = @ccnum WHERE umID=@umid", cn);
                // cmd.Parameters.Add("@umID", SqlDbType.Int).Value = UCEmembership.UmID;
                cmd.Parameters.Add("@rnID", SqlDbType.Int).Value = UCEmembership.RnID;

                if (UCEmembership.LogDate == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = UCEmembership.LogDate;
                }


                if (UCEmembership.ExpDate == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = UCEmembership.ExpDate;
                }
                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = UCEmembership.Verification == null ? DBNull.Value
                    as Object : UCEmembership.Verification as Object;
                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = UCEmembership.ProfileID == null ? DBNull.Value
                    as Object : UCEmembership.ProfileID as Object;

                if (UCEmembership.Renew == false)
                {
                    cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = UCEmembership.Renew;
                }

                if (UCEmembership.Active == false)
                {
                    cmd.Parameters.Add("@active", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@active", SqlDbType.Bit).Value = UCEmembership.Active;
                }
                cmd.Parameters.Add("@amount", SqlDbType.Float).Value = UCEmembership.Amount;
                cmd.Parameters.Add("@umID", SqlDbType.Int).Value = UCEmembership.UmID;
                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = UCEmembership.Comment == null ? DBNull.Value
                      as Object : UCEmembership.Comment as Object;

                cmd.Parameters.Add("@ccnum", SqlDbType.VarChar).Value = UCEmembership.CcNum == null ? DBNull.Value
                    as Object : UCEmembership.CcNum as Object;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);


            }
        }

        public UCEmembershipInfo GetUCEmembershipByProfileID(string ProfileID) 
        {
            UCEmembershipInfo ucememinfo = null;
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select top 1 * from ucemembership where profileid ='" + ProfileID + "' and active=1 and renew=1 order by umid desc", cn);
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                ucememinfo = new UCEmembershipInfo();
                if (reader.Read())
                {
                    ucememinfo.UmID = Convert.ToInt32(reader["umid"]);
                    ucememinfo.RnID = Convert.ToInt32(reader["rnid"]);
                    ucememinfo.LogDate = (DateTime)(Convert.IsDBNull(reader["logdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
                    ucememinfo.ExpDate = (DateTime)(Convert.IsDBNull(reader["expdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
                    ucememinfo.Verification = reader["verification"].ToString();
                    ucememinfo.ProfileID = reader["profileid"].ToString();
                    ucememinfo.Renew = reader["renew"] != DBNull.Value ? Convert.ToBoolean(reader["renew"]) : false;
                    ucememinfo.Active = reader["active"] != DBNull.Value ? Convert.ToBoolean(reader["active"]) : false;
                    ucememinfo.Amount = Convert.IsDBNull(reader["amount"]) ? 0 : float.Parse(reader["amount"].ToString());
                    ucememinfo.Comment = reader["comment"].ToString();
                    ucememinfo.CcNum = reader["ccnum"].ToString();
                }
                else ucememinfo = null;
                reader.Dispose();
                return ucememinfo;

            }
        }

        public UCEmembershipInfo GetUCEmembershipByRnID(int RnID)
        {
            UCEmembershipInfo ucememinfo = null;
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from ucemembership where rnid ='" + RnID + "' order by active desc, expdate desc", cn);
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                ucememinfo = new UCEmembershipInfo();
                while (reader.Read())
                {
                    ucememinfo.UmID = Convert.ToInt32(reader["umid"]);
                    ucememinfo.RnID = Convert.ToInt32(reader["rnid"]);
                    ucememinfo.LogDate = (DateTime)(Convert.IsDBNull(reader["logdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
                    ucememinfo.ExpDate = (DateTime)(Convert.IsDBNull(reader["expdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
                    ucememinfo.Verification = reader["verification"].ToString();
                    ucememinfo.ProfileID = reader["profileid"].ToString();
                    ucememinfo.Renew = reader["renew"] != DBNull.Value ? Convert.ToBoolean(reader["renew"]) : false;
                    ucememinfo.Active = reader["active"] != DBNull.Value ? Convert.ToBoolean(reader["active"]) : false;
                    ucememinfo.Amount = Convert.IsDBNull(reader["amount"]) ? 0 : float.Parse(reader["amount"].ToString());
                    ucememinfo.Comment = reader["comment"].ToString();
                    ucememinfo.CcNum = reader["ccnum"].ToString();
                }

                reader.Dispose();
                return ucememinfo;

            }
        }

        public UCEmembershipInfo GetUCEmembershipByRnIDAndVerification(int RnID, string Verification)
        {
            UCEmembershipInfo ucememinfo = null;
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from ucemembership where rnid ='" + RnID + "' and Verification='" + Verification + "' order by active desc, expdate desc", cn);
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                ucememinfo = new UCEmembershipInfo();
                while (reader.Read())
                {
                    ucememinfo.UmID = Convert.ToInt32(reader["umid"]);
                    ucememinfo.RnID = Convert.ToInt32(reader["rnid"]);
                    ucememinfo.LogDate = (DateTime)(Convert.IsDBNull(reader["logdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
                    ucememinfo.ExpDate = (DateTime)(Convert.IsDBNull(reader["expdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
                    ucememinfo.Verification = reader["verification"].ToString();
                    ucememinfo.ProfileID = reader["profileid"].ToString();
                    ucememinfo.Renew = reader["renew"] != DBNull.Value ? Convert.ToBoolean(reader["renew"]) : false;
                    ucememinfo.Active = reader["active"] != DBNull.Value ? Convert.ToBoolean(reader["active"]) : false;
                    ucememinfo.Amount = Convert.IsDBNull(reader["amount"]) ? 0 : float.Parse(reader["amount"].ToString());
                    ucememinfo.Comment = reader["comment"].ToString();
                    ucememinfo.CcNum = reader["ccnum"].ToString();
                }

                reader.Dispose();
                return ucememinfo;

            }
        }


        public UCEmembershipInfo GetUCEmembershipByUmID(int UmID)
        {
            UCEmembershipInfo ucememinfo = null;
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from ucemembership where Umid = " + UmID, cn);
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                ucememinfo = new UCEmembershipInfo();
                while (reader.Read())
                {
                    ucememinfo.UmID = Convert.ToInt32(reader["umid"]);
                    ucememinfo.RnID = Convert.ToInt32(reader["rnid"]);
                    ucememinfo.LogDate = (DateTime)(Convert.IsDBNull(reader["logdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
                    ucememinfo.ExpDate = (DateTime)(Convert.IsDBNull(reader["expdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
                    ucememinfo.Verification = reader["verification"].ToString();
                    ucememinfo.ProfileID = reader["profileid"].ToString();
                    ucememinfo.Renew = reader["renew"] != DBNull.Value ? Convert.ToBoolean(reader["renew"]) : false;
                    ucememinfo.Active = reader["active"] != DBNull.Value ? Convert.ToBoolean(reader["active"]) : false;
                    ucememinfo.Amount = Convert.IsDBNull(reader["amount"]) ? 0 : float.Parse(reader["amount"].ToString());
                    ucememinfo.Comment = reader["comment"].ToString();
                    ucememinfo.CcNum = reader["ccnum"].ToString();
                }

                reader.Dispose();
                return ucememinfo;

            }
        }



        public bool CancelUCEmembership(int RnID)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Update ucemembership SET " +
                "active ='false' ,renew = 'false' ," +
                "comment= 'Cancelled by CSR on  ' + convert(varchar(10), getdate(), 101)," +
                "expdate= '" + DateTime.Now + "' " +
                "where umID = @rnid and active = 1", cn);
                cmd.Parameters.Add("@rnid", SqlDbType.Int).Value = RnID;
                try
                {
                    cn.Open();
                    int ret = (int)ExecuteNonQuery(cmd);
                    return (ret >= 1);
                }
                catch
                {
                    return false;
                }
            }
        }
        public bool CancelUCEmembershipUpdateNurse(int RnID)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {

                SqlCommand cmd = new SqlCommand("Update Nurse SET " +
                     "Uceend = '" + DateTime.Now + "'," +
                     "optin= 'false' " +
                     "where rnid = @rnid ", cn);
                cmd.Parameters.Add("@rnid", SqlDbType.Int).Value = RnID;
                try
                {
                    cn.Open();
                    int ret = (int)ExecuteNonQuery(cmd);
                    return (ret >= 1);
                }
                catch
                {
                    return false;
                }


            }
        }

        public bool AddUCEmembership(int RnID, DateTime ExpDate)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Update Nurse SET " +
                    "Uceend = '" + ExpDate + "' " +
                    "where rnid = @rnid ", cn);
                cmd.Parameters.Add("@rnid", SqlDbType.Int).Value = RnID;
                try
                {
                    cn.Open();
                    int ret = (int)ExecuteNonQuery(cmd);
                    return (ret >= 1);
                }
                catch
                {
                    return false;
                }

            }
        }

        public bool HaltUCEMemRenewals(int RnID)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Update ucemembership SET " +
                "renew ='false' ," +
                "comment = 'UnlimitedCEAdminCancellation'" +
                "where umID = @rnid", cn);
                cmd.Parameters.Add("@rnid", SqlDbType.Int).Value = RnID;
                try
                {
                    cn.Open();
                    int ret = (int)ExecuteNonQuery(cmd);
                    return (ret >= 1);
                }
                catch
                {
                    return false;
                }

            }
        }

        public bool HaltUCEMemRenewalsUpdateNurse(int RnID)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Update Nurse SET " +
                "optin= 'false'" +
                "where rnid = @rnid ", cn);
                cmd.Parameters.Add("@rnid", SqlDbType.Int).Value = RnID;
                try
                {
                    cn.Open();
                    int ret = (int)ExecuteNonQuery(cmd);
                    return (ret >= 1);
                }
                catch
                {
                    return false;
                }
            }
        }
        public bool UpdateUCEmembershipRenewWithRnID(int rnid, bool renew) 
        {
            int renewal = renew ? 1 : 0;
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Update UCEMembership SET " +
                "renew=@renew" +
                " where rnid = @rnid ", cn);
                cmd.Parameters.Add("@rnid", SqlDbType.Int).Value = rnid;
                cmd.Parameters.Add("@renew", SqlDbType.TinyInt).Value = renewal;
                try
                {
                    cn.Open();
                    int ret = (int)ExecuteNonQuery(cmd);
                    return (ret >= 1);
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool UpdateUCEmembershipActiveWithRnID(int rnid, bool active)
        {
            int isactive = active ? 1 : 0;
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Update UCEMembership SET " +
                "active=@active" +
                " where rnid = @rnid ", cn);
                cmd.Parameters.Add("@rnid", SqlDbType.Int).Value = rnid;
                cmd.Parameters.Add("@active", SqlDbType.TinyInt).Value = isactive;
                try
                {
                    cn.Open();
                    int ret = (int)ExecuteNonQuery(cmd);
                    return (ret >= 1);
                }
                catch
                {
                    return false;
                }
            }
        }


        public bool InsertUCEmembership(UCEmembershipInfo UCEmembership)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Insert into ucemembership(rnid,logdate,expdate,verification, " +
                    "profileid,renew,active,comment,amount,ccnum)values " +
                    "(@rnid, " +
                    "@logdate, " +
                    "@expdate, " +
                    "@verification, " +
                    "@profileid, " +
                    "@renew, " +
                    "@active, " +
                    "@comment, " +
                    "@amount, " +
                    "@ccnum )", cn);

                cmd.Parameters.Add("rnid", SqlDbType.Int).Value = UCEmembership.RnID;
                if (UCEmembership.LogDate == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = UCEmembership.LogDate;
                }
                if (UCEmembership.ExpDate == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = UCEmembership.ExpDate;
                }

                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = UCEmembership.Verification == null ? DBNull.Value as Object : UCEmembership.Verification as Object;

                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = UCEmembership.ProfileID == null ? DBNull.Value as Object : UCEmembership.ProfileID as Object;


                if (UCEmembership.Renew == false)
                {
                    cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@renew", SqlDbType.Bit).Value = UCEmembership.Renew;
                }
                if (UCEmembership.Active == false)
                {
                    cmd.Parameters.Add("@active", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@active", SqlDbType.Bit).Value = UCEmembership.Active;
                }
                cmd.Parameters.Add("@amount", SqlDbType.Float).Value = UCEmembership.Amount;
                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = UCEmembership.Comment == null ? DBNull.Value as Object : UCEmembership.Comment as Object;
                cmd.Parameters.Add("@ccnum", SqlDbType.VarChar).Value = UCEmembership.CcNum == null ? DBNull.Value as Object : UCEmembership.CcNum as Object;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);

            }
        }

        public List<UCEmembershipInfo> GetUCEmembershipListByRnID(int RnID)
        {
            List<UCEmembershipInfo> ucememlist = new List<UCEmembershipInfo>();
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from ucemembership where rnid='" + RnID + "' order by active desc, expdate desc", cn);

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                UCEmembershipInfo ucememinfo;
                bool firstFlag = true;
                while (reader.Read())
                {
                    ucememinfo = new UCEmembershipInfo();
                    if (firstFlag == true)
                    {
                        ucememinfo.FirstRec = true;
                        firstFlag = false;
                    }
                    else
                    {
                        ucememinfo.FirstRec = false;
                    }

                    ucememinfo.UmID = Convert.ToInt32(reader["umid"]);
                    ucememinfo.RnID = Convert.ToInt32(reader["rnid"]);
                    ucememinfo.LogDate = (DateTime)(Convert.IsDBNull(reader["logdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
                    ucememinfo.ExpDate = (DateTime)(Convert.IsDBNull(reader["expdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
                    ucememinfo.Verification = reader["verification"].ToString();
                    ucememinfo.ProfileID = reader["profileid"].ToString();
                    ucememinfo.Renew = reader["renew"] != DBNull.Value ? Convert.ToBoolean(reader["renew"]) : false;
                    ucememinfo.Active = reader["active"] != DBNull.Value ? Convert.ToBoolean(reader["active"]) : false;
                    ucememinfo.Amount = reader["amount"] == DBNull.Value ? 0 : float.Parse(reader["amount"].ToString());
                    ucememinfo.Comment = reader["comment"].ToString();
                    ucememinfo.CcNum = reader["ccnum"].ToString();
                    ucememlist.Add(ucememinfo);
                }
                reader.Dispose();
                return ucememlist;
            }
        }

        /// <summary>
        /// Gets the active UCE Memberships which are expiring this month
        /// </summary>
        /// <returns></returns>
        public List<UCEmembershipInfo> GetActiveUCEMembershipsByExpDateInThisMonth()
        {
            List<UCEmembershipInfo> ucememlist = new List<UCEmembershipInfo>();
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from ucemembership " +
                    " where expdate between GETDATE() AND DATEADD(m,12,GETDATE()) and Renew=1 and Active=1 Order by ExpDate", cn);

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                UCEmembershipInfo ucememinfo;
                while (reader.Read())
                {
                    ucememinfo = new UCEmembershipInfo();

                    ucememinfo.UmID = Convert.ToInt32(reader["umid"]);
                    ucememinfo.RnID = Convert.ToInt32(reader["rnid"]);
                    ucememinfo.LogDate = (DateTime)(Convert.IsDBNull(reader["logdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
                    ucememinfo.ExpDate = (DateTime)(Convert.IsDBNull(reader["expdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
                    ucememinfo.Verification = reader["verification"].ToString();
                    ucememinfo.ProfileID = reader["profileid"].ToString();
                    ucememinfo.Renew = reader["renew"] != DBNull.Value ? Convert.ToBoolean(reader["renew"]) : false;
                    ucememinfo.Active = reader["active"] != DBNull.Value ? Convert.ToBoolean(reader["active"]) : false;
                    ucememinfo.Amount = reader["amount"] == DBNull.Value ? 0 : float.Parse(reader["amount"].ToString());
                    ucememinfo.Comment = reader["comment"].ToString();
                    ucememinfo.CcNum = reader["ccnum"].ToString();
                    ucememlist.Add(ucememinfo);
                }
                reader.Dispose();
                return ucememlist;
            }
        }

        /// <summary>
        /// Updates UCEMembership Reminder Date
        /// </summary>
        /// <param name="reminderDate"></param>
        /// <param name="umID"></param>
        /// <returns></returns>
        public bool UpdateUCEMemberShipReminderDate(DateTime reminderDate, int umID)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UCEMembership set ReminderDate=@reminderDate where umID=@umID", cn);
                cmd.Parameters.Add("@umID", SqlDbType.Int).Value = umID;
                cmd.Parameters.Add("@reminderDate", SqlDbType.DateTime).Value = reminderDate;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Get Expired Credit Card users List
        /// </summary>
        /// <returns></returns>
        public DataSet GetUCECCExpiredUsers()
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select N.Email,U.ExpDate,U.ReminderDate,U.CCNum,N.FirstName from UCEMembership U join nurse N on N.rnid=U.rnid where (U.expDate Between GETDATE() AND DATEADD(m,1,GETDATE())) and " +
                "Renew=1 and Active=1  and DATEADD(mm,1,U.ReminderDate) <=ExpDate ", cn);
                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "ExpUsers");
            }
            return ds;

        }
      
        #endregion

        #region PRProvider

      
        #endregion
    }
}

#endregion




