﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region TopicCollectionInfo

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for TopicCollection
    /// </summary>
    public class TopicCollectionInfo
    {
        public TopicCollectionInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public TopicCollectionInfo(int topicid, int chapterid, int chapternumber, int tcid)
        {
            this.topicid = topicid;
            this.chapterid = chapterid;
            this.chapternumber = chapternumber;
            this.tcid = tcid;

        }
        public int topicid
        {
            get;
            set;
        }
        public int chapterid
        {
            get;
            set;
        }
        public int chapternumber
        {
            get;
            set;
        }

        public int tcid
        {
            get;
            set;
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /// <summary>
        /// Retrieves all TopicComplianceLinks for a FacilityID
        /// </summary>
        public List<TopicCollectionInfo> GetTopicCollectionsByTopicID(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from TopicCollection where TopicID=@TopicID ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                cn.Open();
                return GetTopicCollectionCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all TopicComplianceLinks for a FacilityID
        /// </summary>
        public DataSet GetTopicCollectionsDataSetByTopicID(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlDataAdapter adpt = new SqlDataAdapter();
                string cSQLCommand = "select t.course_number, tc.chapternumber,t.topicname,tc.chapterid,tc.topicid,tc.tcid from topiccollection tc join (select topicname,topicid ,course_number from topic where Topic.Obsolete = '0' and Topic.avail_ind = '1' and topic.topicid in (select chapterid from topiccollection where topiccollection.topicid=@TopicID)) t on t.topicid=tc.chapterid where tc.topicid=@TopicID ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                cn.Open();
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.CommandType = CommandType.Text;
                adpt.SelectCommand = cmd;
                DataSet ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
        }
        public bool DeleteTopicCollection(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from topiccollection where tcid=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        /// <summary>
        /// Inserts a new mediacontent
        /// </summary>
        public int InsertTopicCollection(TopicCollectionInfo TopicCollection)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into TopicCollection " +
              "(TopicID, " +
              "chapterid, " +
              "chapternumber) " +
              "VALUES (" +
              "@TopicID, " +
              "@chapterid, " +
              "@chapternumber) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicCollection.topicid;
                cmd.Parameters.Add("@chapterid", SqlDbType.Int).Value = TopicCollection.chapterid;
                cmd.Parameters.Add("@chapternumber", SqlDbType.Int).Value = TopicCollection.chapternumber;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        public bool UpdateTopicCollections(int chapterid, int chapternumber, int tcid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update TopicCollection " +
                    "set chapternumber = @chapternumber, " +
                    "chapterid = @chapterid " +
                    "where tcid = @tcid", cn);
                cmd.Parameters.Add("@chapterid", SqlDbType.Int).Value = chapterid;
                cmd.Parameters.Add("@chapternumber", SqlDbType.Int).Value = chapternumber;
                cmd.Parameters.Add("@tcid", SqlDbType.Int).Value = tcid;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);

            }
        }
        #endregion

        #region PRProvider


        /// <summary>
        /// Returns a new TopicCollectionInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual TopicCollectionInfo GetTopicCollectionTFromReader(IDataReader reader)
        {
            return GetTopicCollectionTFromReader(reader, true);
        }
        protected virtual TopicCollectionInfo GetTopicCollectionTFromReader(IDataReader reader, bool readMemos)
        {
            TopicCollectionInfo TopicCollection = new TopicCollectionInfo(
              (int)reader["TopicID"],
              (int)reader["ChapterID"],
              (int)reader["ChapterNumber"],
              (int)reader["TCID"]);

            return TopicCollection;
        }

        /// <summary>
        /// Returns a collection of TopicComplianceLinkInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<TopicCollectionInfo> GetTopicCollectionCollectionFromReader(IDataReader reader)
        {
            return GetTopicCollectionCollectionFromReader(reader, true);
        }
        protected virtual List<TopicCollectionInfo> GetTopicCollectionCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TopicCollectionInfo> TopicCollections = new List<TopicCollectionInfo>();
            while (reader.Read())
                TopicCollections.Add(GetTopicCollectionTFromReader(reader, readMemos));
            return TopicCollections;
        }
        #endregion
    }
}
#endregion