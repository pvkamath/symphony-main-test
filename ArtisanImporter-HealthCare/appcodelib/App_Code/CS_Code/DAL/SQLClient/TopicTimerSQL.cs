﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Fangbo Yang
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider: DataAccess
    {
        public string GetTopicTimerByID(int topicid)
        {
            using(SqlConnection cn=new SqlConnection(this.ConnectionString))
            {
                string sql = "SELECT tmin FROM topictimer WHERE topicid=@topicid";

                SqlCommand cmd = new SqlCommand(sql,cn);
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;

                cn.Open();

                IDataReader reader = ExecuteReader(cmd);

                if (reader.Read())
                    return reader["tmin"] == null ? "nothing" : reader["tmin"].ToString();
                else
                    return "null";
            }
        }

        public void AddorUpdateTopicTimer(int topicid, int tmin)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                bool isAdd = GetTopicTimerByID(topicid) == "null" ? true : false;
                string sql = "";
                if (isAdd)
                {
                    sql = "INSERT  INTO topictimer(topicid,tmin) VALUES(@topicid,@tmin)";
                }
                else
                {
                    sql = "UPDATE topictimer SET tmin=@tmin WHERE topicid=@topicid";
                }

                SqlCommand cmd = new SqlCommand(sql, cn);
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@tmin", SqlDbType.Int).Value = tmin;

                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }
       
    }
}