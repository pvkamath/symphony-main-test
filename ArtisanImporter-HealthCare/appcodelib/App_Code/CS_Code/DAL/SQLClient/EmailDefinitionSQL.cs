﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region EmailDefinitionInfo

namespace PearlsReview.DAL
{
    public class EmailDefinitionInfo
    {
        public EmailDefinitionInfo() { }

        public EmailDefinitionInfo(int EDefID, string EDefName, string EDefText)
        {
            this.EDefID = EDefID;
            this.EDefName = EDefName;
            this.EDefText = EDefText;
        }

        private int _EDefID = 0;
        public int EDefID
        {
            get { return _EDefID; }
            protected set { _EDefID = value; }
        }

        private string _EDefName = "";
        public string EDefName
        {
            get { return _EDefName; }
            private set { _EDefName = value; }
        }

        private string _EDefText = "";
        public string EDefText
        {
            get { return _EDefText; }
            private set { _EDefText = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with EmailDefinitions

        /// <summary>
        /// Returns the total number of EmailDefinitions
        /// </summary>
        public  int GetEmailDefinitionCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from EmailDefinition", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all EmailDefinitions
        /// </summary>
        public  List<EmailDefinitionInfo> GetEmailDefinitions(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from EmailDefinition";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetEmailDefinitionCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the EmailDefinition with the specified ID
        /// </summary>
        public  EmailDefinitionInfo GetEmailDefinitionByID(int EDefID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from EmailDefinition where EDefID=@EDefID", cn);
                cmd.Parameters.Add("@EDefID", SqlDbType.Int).Value = EDefID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetEmailDefinitionFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves the EmailDefinition with the specified ID
        /// </summary>
        public EmailDefinitionInfo GetEmailDefinitionByType(string EName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from EmailDefinition where EDefname=@EDefName", cn);
                cmd.Parameters.Add("@EDefName", SqlDbType.VarChar).Value = EName;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetEmailDefinitionFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a EmailDefinition
        /// </summary>
        public  bool DeleteEmailDefinition(int EDefID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from EmailDefinition where EDefID=@EDefID", cn);
                cmd.Parameters.Add("@EDefID", SqlDbType.Int).Value = EDefID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new EmailDefinition
        /// </summary>
        public  int InsertEmailDefinition(EmailDefinitionInfo EmailDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into EmailDefinition " +
              "(EDefName, " +
              "EDefText) " +
              "VALUES (" +
              "@EDefName, " +
              "@EDefText) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@EDefName", SqlDbType.VarChar).Value = EmailDefinition.EDefName;
                cmd.Parameters.Add("@EDefText", SqlDbType.VarChar).Value = EmailDefinition.EDefText;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a EmailDefinition
        /// </summary>
        public  bool UpdateEmailDefinition(EmailDefinitionInfo EmailDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update EmailDefinition set " +
              "EDefName = @EDefName, " +
              "EDefText = @EDefText " +
              "where EDefID = @EDefID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@EDefName", SqlDbType.VarChar).Value = EmailDefinition.EDefName;
                cmd.Parameters.Add("@EDefText", SqlDbType.VarChar).Value = EmailDefinition.EDefText;
                cmd.Parameters.Add("@EDefID", SqlDbType.Int).Value = EmailDefinition.EDefID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new EmailDefinitionInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual EmailDefinitionInfo GetEmailDefinitionFromReader(IDataReader reader)
        {
            return GetEmailDefinitionFromReader(reader, true);
        }
        protected virtual EmailDefinitionInfo GetEmailDefinitionFromReader(IDataReader reader, bool readMemos)
        {
            EmailDefinitionInfo EmailDefinition = new EmailDefinitionInfo(
              (int)reader["EDefID"],
              reader["EDefName"].ToString(),
              reader["EDefText"].ToString());


            return EmailDefinition;
        }

        /// <summary>
        /// Returns a collection of EmailDefinitionInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<EmailDefinitionInfo> GetEmailDefinitionCollectionFromReader(IDataReader reader)
        {
            return GetEmailDefinitionCollectionFromReader(reader, true);
        }
        protected virtual List<EmailDefinitionInfo> GetEmailDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<EmailDefinitionInfo> EmailDefinitions = new List<EmailDefinitionInfo>();
            while (reader.Read())
                EmailDefinitions.Add(GetEmailDefinitionFromReader(reader, readMemos));
            return EmailDefinitions;
        }
        #endregion
    }
}
#endregion