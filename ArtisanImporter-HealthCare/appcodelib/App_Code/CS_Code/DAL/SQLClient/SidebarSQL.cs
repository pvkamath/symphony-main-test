﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region SidebarInfo

namespace PearlsReview.DAL
{
    public class SidebarInfo
    {
        public SidebarInfo() { }

        public SidebarInfo(int SB_ID, int TopicID, string Title, string Body)
        {
            this.SB_ID = SB_ID;
            this.TopicID = TopicID;
            this.Title = Title;
            this.Body = Body;
        }

        private int _SB_ID = 0;
        public int SB_ID
        {
            get { return _SB_ID; }
            protected set { _SB_ID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

        private string _Title = "";
        public string Title
        {
            get { return _Title; }
            private set { _Title = value; }
        }

        private string _Body = "";
        public string Body
        {
            get { return _Body; }
            private set { _Body = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {         
        #region SQLPRPROVIDER

        /////////////////////////////////////////////////////////
        // methods that work with Sidebars

        /// <summary>
        /// Returns the total number of Sidebars
        /// </summary>
        public int GetSidebarCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Sidebars", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Sidebars
        /// </summary>
        public List<SidebarInfo> GetSidebars(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Sidebars";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetSidebarCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Sidebar with the specified ID
        /// </summary>
        public SidebarInfo GetSidebarByID(int SB_ID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Sidebars where SB_ID=@SB_ID", cn);
                cmd.Parameters.Add("@SB_ID", SqlDbType.Int).Value = SB_ID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSidebarFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves all Sidebars associated with the specified TopicID
        /// </summary>
        public List<SidebarInfo> GetSidebarsByTopicID(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Sidebars where TopicID=@TopicID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                cn.Open();
                return GetSidebarCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Returns the total number of Sidebars associated with the specified TopicID
        /// </summary>
        public int GetSidebarCountByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Sidebars where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }


        /// <summary>
        /// Deletes a Sidebar
        /// </summary>
        public bool DeleteSidebar(int SB_ID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Sidebars where SB_ID=@SB_ID", cn);
                cmd.Parameters.Add("@SB_ID", SqlDbType.Int).Value = SB_ID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Sidebar
        /// </summary>
        public int InsertSidebar(SidebarInfo Sidebar)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Sidebars " +
              "(TopicID, " +
              "Title, " +
              "Body) " +
              "VALUES (" +
              "@TopicID, " +
              "@Title, " +
              "@Body) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Sidebar.TopicID;
                cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = Sidebar.Title;
                cmd.Parameters.Add("@Body", SqlDbType.VarChar).Value = Sidebar.Body;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Sidebar
        /// </summary>
        public bool UpdateSidebar(SidebarInfo Sidebar)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Sidebars set " +
              "TopicID = @TopicID, " +
              "Title = @Title, " +
              "Body = @Body " +
              "where SB_ID = @SB_ID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Sidebar.TopicID;
                cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = Sidebar.Title;
                cmd.Parameters.Add("@Body", SqlDbType.VarChar).Value = Sidebar.Body;
                cmd.Parameters.Add("@SB_ID", SqlDbType.Int).Value = Sidebar.SB_ID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new SidebarInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual SidebarInfo GetSidebarFromReader(IDataReader reader)
        {
            return GetSidebarFromReader(reader, true);
        }
        protected virtual SidebarInfo GetSidebarFromReader(IDataReader reader, bool readMemos)
        {
            SidebarInfo Sidebar = new SidebarInfo(
              (int)reader["SB_ID"],
              (int)reader["TopicID"],
              reader["Title"].ToString(),
              reader["Body"].ToString());

            return Sidebar;
        }

        /// <summary>
        /// Returns a collection of SidebarInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<SidebarInfo> GetSidebarCollectionFromReader(IDataReader reader)
        {
            return GetSidebarCollectionFromReader(reader, true);
        }
        protected virtual List<SidebarInfo> GetSidebarCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<SidebarInfo> Sidebars = new List<SidebarInfo>();
            while (reader.Read())
                Sidebars.Add(GetSidebarFromReader(reader, readMemos));
            return Sidebars;
        }

        #endregion
    }
}
#endregion