﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;



namespace PearlsReview.DAL
{
    #region TopicComplianceLinkInfo

    public class TopicComplianceLinkInfo
    {
        public TopicComplianceLinkInfo() { }

        public TopicComplianceLinkInfo(int TopCompID, int TopicID, int FacID, string ComplFile)
        {
            this.TopCompID = TopCompID;
            this.TopicID = TopicID;
            this.FacID = FacID;
            this.ComplFile = ComplFile;
        }

        private int _TopCompID = 0;
        public int TopCompID
        {
            get { return _TopCompID; }
            protected set { _TopCompID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

        private int _FacID = 0;
        public int FacID
        {
            get { return _FacID; }
            private set { _FacID = value; }
        }

        private string _ComplFile = "";
        public string ComplFile
        {
            get { return _ComplFile; }
            private set { _ComplFile = value; }
        }

    }

    #endregion    
}


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with TopicComplianceLinks

        /// <summary>
        /// Returns the total number of TopicComplianceLinks
        /// </summary>
        public  int GetTopicComplianceLinkCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from TopicComplianceLink", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all TopicComplianceLinks
        /// </summary>
        public  List<TopicComplianceLinkInfo> GetTopicComplianceLinks(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from TopicComplianceLink";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicComplianceLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all TopicComplianceLinks for a FacilityID
        /// </summary>
        public  List<TopicComplianceLinkInfo> GetTopicComplianceLinksByFacilityID(int FacilityID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from TopicComplianceLink where FacID=@FacilityID ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;

                cn.Open();
                return GetTopicComplianceLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        


        /// <summary>
        /// Retrieves the TopicComplianceLink with the specified ID
        /// </summary>
        public  TopicComplianceLinkInfo GetTopicComplianceLinkByID(int TopCompID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from TopicComplianceLink where TopCompID=@TopCompID", cn);
                cmd.Parameters.Add("@TopCompID", SqlDbType.Int).Value = TopCompID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicComplianceLinkFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a TopicComplianceLink
        /// </summary>
        public  bool DeleteTopicComplianceLink(int TopCompID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from TopicComplianceLink where TopCompID=@TopCompID", cn);
                cmd.Parameters.Add("@TopCompID", SqlDbType.Int).Value = TopCompID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        //public override bool DeleteMediaContent(int mcid)
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {
        //        SqlCommand cmd = new SqlCommand("delete from mediacontent where mcid=@mcid", cn);
        //        cmd.Parameters.Add("@mcid", SqlDbType.Int).Value = mcid;
        //        cn.Open();
        //        int ret = ExecuteNonQuery(cmd);
        //        return (ret == 1);
        //    }
        //}

        /// <summary>
        /// Inserts a new TopicComplianceLink
        /// </summary>
        public  int InsertTopicComplianceLink(TopicComplianceLinkInfo TopicComplianceLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into TopicComplianceLink " +
              "(TopicID, " +
              "FacID, " +
              "ComplFile) " +
              "VALUES (" +
              "@TopicID, " +
              "FacID, " +
              "@ComplFile) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicComplianceLink.TopicID;
                cmd.Parameters.Add("@FacID", SqlDbType.Int).Value = TopicComplianceLink.FacID;
                cmd.Parameters.Add("@ComplFile", SqlDbType.VarChar).Value = TopicComplianceLink.ComplFile;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        


        /// <summary>
        /// Updates a TopicComplianceLink
        /// </summary>
        public  bool UpdateTopicComplianceLink(TopicComplianceLinkInfo TopicComplianceLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update TopicComplianceLink set " +
              "TopicID = @TopicID, " +
              "FacID = FacID, " +
              "ComplFile = ? " +
              "where TopCompID = @TopCompID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicComplianceLink.TopicID;
                cmd.Parameters.Add("@FacID", SqlDbType.Int).Value = TopicComplianceLink.FacID;
                cmd.Parameters.Add("@ComplFile", SqlDbType.VarChar).Value = TopicComplianceLink.ComplFile;
                cmd.Parameters.Add("@TopCompID", SqlDbType.Int).Value = TopicComplianceLink.TopCompID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new TopicComplianceLinkInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual TopicComplianceLinkInfo GetTopicComplianceLinkFromReader(IDataReader reader)
        {
            return GetTopicComplianceLinkFromReader(reader, true);
        }
        protected virtual TopicComplianceLinkInfo GetTopicComplianceLinkFromReader(IDataReader reader, bool readMemos)
        {
            TopicComplianceLinkInfo TopicComplianceLink = new TopicComplianceLinkInfo(
              (int)reader["TopCompID"],
              (int)reader["TopicID"],
              (int)reader["FacID"],
              reader["ComplFile"].ToString());


            return TopicComplianceLink;
        }

        /// <summary>
        /// Returns a collection of TopicComplianceLinkInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<TopicComplianceLinkInfo> GetTopicComplianceLinkCollectionFromReader(IDataReader reader)
        {
            return GetTopicComplianceLinkCollectionFromReader(reader, true);
        }
        protected virtual List<TopicComplianceLinkInfo> GetTopicComplianceLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TopicComplianceLinkInfo> TopicComplianceLinks = new List<TopicComplianceLinkInfo>();
            while (reader.Read())
                TopicComplianceLinks.Add(GetTopicComplianceLinkFromReader(reader, readMemos));
            return TopicComplianceLinks;
        }




        #endregion
    }
}
#endregion