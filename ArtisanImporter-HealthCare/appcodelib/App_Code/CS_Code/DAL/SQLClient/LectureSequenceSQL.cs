﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Collections.Generic;
using System.Data.SqlClient;


#region LectureEventInfo

namespace PearlsReview.DAL
{
    public class LectureSequenceInfo
    {
        public LectureSequenceInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public LectureSequenceInfo(int LectSeqID, int Sequence, int TopicID, int ResourceID1, int ResourceID2, int ResourceID3)
        {
            this.LectSeqID = LectSeqID;
            this.Sequence = Sequence;
            this.TopicID = TopicID;
            this.ResourceID1 = ResourceID1;
            this.ResourceID2 = ResourceID2;
            this.ResourceID3 = ResourceID3;

        }

        private int _LectSeqID = 0;
        public int LectSeqID
        {
            get { return _LectSeqID; }
            set { _LectSeqID = value; }
        }

        private int _Sequence = 0;
        public int Sequence
        {
            get { return _Sequence; }
            set { _Sequence = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _ResourceID1 = 0;
        public int ResourceID1
        {
            get { return _ResourceID1; }
            set { _ResourceID1 = value; }
        }

        private int _ResourceID2 = 0;
        public int ResourceID2
        {
            get { return _ResourceID2; }
            set { _ResourceID2 = value; }
        }

        private int _ResourceID3 = 0;
        public int ResourceID3
        {
            get { return _ResourceID3; }
            set { _ResourceID3 = value; }
        }
    }
}
#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        public List<LectureSequenceInfo> GetLectureSequenceDetailsByTopicID(int topicID)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                String cSQLCommand = "select * From LectureSequence " +
                            "where topicID =  @topicID ";

             
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@topicID", SqlDbType.Int).Value = topicID;

                cn.Open();
                return GetLectureSequenceCollectionFromReader(ExecuteReader(cmd), false);

            }
        }

        public List<LectureSequenceInfo> GetLectureSequenceDetailsBySequence(int sequence)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                String cSQLCommand = "select * From LectureSequence " +
                            "where sequence =  @sequence ";


                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@sequence", SqlDbType.Int).Value = sequence;

                cn.Open();
                return GetLectureSequenceCollectionFromReader(ExecuteReader(cmd), false);

            }
        }
        /// <summary>
        /// Inserts a new LectureSequence
        /// </summary>
        public int InsertLectureSequence(LectureSequenceInfo LectureSequence)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into LectureSequence " +
                    "(sequence, " +
                    "topicid, " +
                    "resourceid1, " +
                    "resourceid2, " +
                    "resourceid3)" +
                    "VALUES (" +
                    "@Sequence, " +
                    "@TopicID, " +
                    "@ResourceID1, " +
                    "@ResourceID2, " +
                    "@ResourceID3 ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@sequence", SqlDbType.Int).Value = LectureSequence.Sequence;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = LectureSequence.TopicID;
                cmd.Parameters.Add("@resourceid1", SqlDbType.Int).Value = LectureSequence.ResourceID1;
                cmd.Parameters.Add("@resourceid2", SqlDbType.Int).Value = LectureSequence.ResourceID2;
                cmd.Parameters.Add("@resourceid3", SqlDbType.Int).Value = LectureSequence.ResourceID3;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

         /// <summary>
        /// Updates a LectureSequence
        /// </summary>
        
          public  bool UpdateLectureSequence(LectureSequenceInfo LectureSequence)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update LectureSequence set " +
                    "sequence = @Sequence, " +
                    "topicid = @TopicID, " +
                    "resourceid1 = @ResourceID1, " +
                    "resourceid2 = @ResourceID2, " +
                    "resourceid3 = @ResourceID3 " +
                    "where lectseqid = @LectSeqID ", cn);

                cmd.Parameters.Add("@sequence", SqlDbType.Int).Value = LectureSequence.Sequence;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = LectureSequence.TopicID;
                cmd.Parameters.Add("@resourceid1", SqlDbType.Int).Value = LectureSequence.ResourceID1;
                cmd.Parameters.Add("@resourceid2", SqlDbType.Int).Value = LectureSequence.ResourceID2;
                cmd.Parameters.Add("@resourceid3", SqlDbType.Int).Value = LectureSequence.ResourceID3;

                cmd.Parameters.Add("@LectSeqID", SqlDbType.Int).Value = LectureSequence.LectSeqID;


                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);

            }

        }

        #endregion

        #region PRProvider

        /////////////////////////////////////////////////////////
        // methods that work with LectureSequence  

        /// <summary>
        /// Returns a new LectureSequenceInfo instance filled with the DataReader's current record data
        /// </summary>

        protected virtual LectureSequenceInfo GetLectureSequenceFromReader(IDataReader reader)
        {
            return GetLectureSequenceFromReader(reader,true);
        }

        protected virtual LectureSequenceInfo GetLectureSequenceFromReader(IDataReader reader, bool readMemos)
        {
            LectureSequenceInfo LectureSequence = new LectureSequenceInfo(
            (int)reader["LectSeqID"],
            (int)reader["Sequence"],
            (int)reader["TopicID"],
            (int)reader["ResourceID1"],
            (int)reader["ResourceID2"],
            (int)reader["ResourceID3"]);
            return LectureSequence;
        }

        /// <summary>
        /// Returns a collection of LectureSequenceInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<LectureSequenceInfo> GetLectureSequenceCollectionFromReader(IDataReader reader)
        {
            return GetLectureSequenceCollectionFromReader(reader, true);
        }
        protected virtual List<LectureSequenceInfo> GetLectureSequenceCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<LectureSequenceInfo> LectureSequences = new List<LectureSequenceInfo>();
            while (reader.Read())
                LectureSequences.Add(GetLectureSequenceFromReader(reader, readMemos));
            return LectureSequences;
        }

        #endregion



    }
}
#endregion