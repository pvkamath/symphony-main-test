﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region UserInterestLinkInfo

namespace PearlsReview.DAL
{
    public class UserInterestLinkInfo
    {
        public UserInterestLinkInfo() { }

        public UserInterestLinkInfo(int UserIntID, int UserID, int InterestID)
        {
            this.UserIntID = UserIntID;
            this.UserID = UserID;
            this.InterestID = InterestID;
        }

        private int _UserIntID = 0;
        public int UserIntID
        {
            get { return _UserIntID; }
            protected set { _UserIntID = value; }
        }

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            private set { _UserID = value; }
        }

        private int _InterestID = 0;
        public int InterestID
        {
            get { return _InterestID; }
            private set { _InterestID = value; }
        }

    }
}

#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider


        /////////////////////////////////////////////////////////
        // methods that work with UserInterestLinks

        /// <summary>
        /// Returns the total number of UserInterestLinks
        /// </summary>
        public int GetUserInterestLinkCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from UserInterestLink", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all UserInterestLinks
        /// </summary>
        public List<UserInterestLinkInfo> GetUserInterestLinks(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from UserInterestLink";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetUserInterestLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves a list of FieldsOfInterest with the specified UserID
        /// </summary>
        public List<UserInterestLinkInfo> GetUserInterestLinksByUserID(int UserID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
               string cSQLCommand = "select * from UserInterestLink where UserInterestLink.UserID =" + UserID.ToString();


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
               
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
              //  cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

                cn.Open();
                return GetUserInterestLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the UserInterestLink with the specified ID
        /// </summary>
        public UserInterestLinkInfo GetUserInterestLinkByID(int UserIntID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from UserInterestLink where UserIntID=@UserIntID", cn);
                cmd.Parameters.Add("@UserIntID", SqlDbType.Int).Value = UserIntID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUserInterestLinkFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a UserInterestLink
        /// </summary>
        public bool DeleteUserInterestLink(int UserIntID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from UserInterestLink where UserIntID=@UserIntID", cn);
                cmd.Parameters.Add("@UserIntID", SqlDbType.Int).Value = UserIntID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a UserInterestLink
        /// </summary>
        public bool DeleteUserInterestLinksByiID(int iID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from UserInterestLink where Userid=@iID", cn);
                cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new UserInterestLink
        /// </summary>
        public int InsertUserInterestLink(UserInterestLinkInfo UserInterestLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into UserInterestLink " +
              "(UserID, " +
              "InterestID) " +
              "VALUES (" +
              "@UserID, " +
              "@InterestID) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserInterestLink.UserID;
                cmd.Parameters.Add("@InterestID", SqlDbType.Int).Value = UserInterestLink.InterestID;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a UserInterestLink
        /// </summary>
        public bool UpdateUserInterestLink(UserInterestLinkInfo UserInterestLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserInterestLink set " +
              "UserID = @UserID, " +
              "InterestID = @InterestID " +
              "where UserIntID = @UserIntID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserInterestLink.UserID;
                cmd.Parameters.Add("@InterestID", SqlDbType.Int).Value = UserInterestLink.InterestID;
                cmd.Parameters.Add("@UserIntID", SqlDbType.Int).Value = UserInterestLink.UserIntID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Assign fields of interest to user from comma-delimited list
        /// </summary>
        public  bool UpdateUserInterestAssignments(int UserID, string InterestIDAssignments)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                // first delete any assignments that are not in the list passed in
                //              SqlCommand cmd = new SqlCommand(
                //                  "DELETE FROM categorylink " +
                //                  "where TopicID = ? and CategoryID NOT IN (?)", cn);

                // NOTE; For now, just delete them all
                SqlCommand cmd = new SqlCommand(
                    "DELETE FROM UserInterestLink " +
                    "where UserID = @UserID ", cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                //              return (ret == 1);

                if (InterestIDAssignments.Trim().Length < 1)
                    // no assignments, so nothing else to do
                    return true;

                // now step through all InterestIDs in the list passed in and call
                // this.AssignInterestToUser() for each, which will insert the assignment
                // if it does not already exist
                string[] InterestIDs = InterestIDAssignments.Split(',');

                foreach (string cInterestID in InterestIDs)
                {
                    this.AssignInterestToUser(UserID, Int32.Parse(cInterestID));
                }

                return true;
            }
        }


        #endregion

        #region PRProvider

        /////////////////////////////////////////////////////////
        // methods that work with UserInterestLinks
        /// <summary>
        /// Returns a new UserInterestLinkInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual UserInterestLinkInfo GetUserInterestLinkFromReader(IDataReader reader)
        {
            return GetUserInterestLinkFromReader(reader, true);
        }
        protected virtual UserInterestLinkInfo GetUserInterestLinkFromReader(IDataReader reader, bool readMemos)
        {
            UserInterestLinkInfo UserInterestLink = new UserInterestLinkInfo(
              (int)reader["UserIntID"],
              (int)reader["UserID"],
              (int)reader["InterestID"]);


            return UserInterestLink;
        }

        /// <summary>
        /// Returns a collection of UserInterestLinkInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<UserInterestLinkInfo> GetUserInterestLinkCollectionFromReader(IDataReader reader)
        {
            return GetUserInterestLinkCollectionFromReader(reader, true);
        }
        protected virtual List<UserInterestLinkInfo> GetUserInterestLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<UserInterestLinkInfo> UserInterestLinks = new List<UserInterestLinkInfo>();
            while (reader.Read())
                UserInterestLinks.Add(GetUserInterestLinkFromReader(reader, readMemos));
            return UserInterestLinks;
        }

        #endregion
    }
}
#endregion


