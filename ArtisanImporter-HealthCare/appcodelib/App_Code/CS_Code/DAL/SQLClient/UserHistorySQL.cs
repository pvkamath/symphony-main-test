﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Collections.Generic;
using System.Data.SqlClient;

#region UserHistoryInfo

namespace PearlsReview.DAL
{

    /// <summary>
    /// Summary description for UserHistorySQL
    /// </summary>
    public class UserHistoryInfo
    {
        public UserHistoryInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

         private int _Uh_ID = 0;
        public int Uh_ID
        {
            get { return _Uh_ID; }
            protected set { _Uh_ID = value; }
        }

        private int _User_ID = 0;
        public int User_ID
        {
            get { return _User_ID; }
            protected set { _User_ID = value; }
        }

        private DateTime _Action_Date =  System.DateTime.Now;
        public DateTime Action_Date
        {
            get { return _Action_Date; }
            protected set { _Action_Date = value; }
        }

        private string _Action_Type = "";
        public string Action_Type
        {
            get { return _Action_Type; }
            set { _Action_Type = value; }
        }

        private string _Action_Comment = "";
        public string Action_Comment
        {
            get { return _Action_Comment; }
            set { _Action_Comment = value; }
        }

          public UserHistoryInfo(int Uh_ID, int User_ID, DateTime Action_Date, string Action_Type, string Action_Comment)
        {
            this.Uh_ID = Uh_ID;
            this.User_ID = User_ID;
            this.Action_Date = Action_Date;
            this.Action_Type = Action_Type;
            this.Action_Comment = Action_Comment;
        }
    }
}
#endregion
#region SQLPRProvider and PRProvider
namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
         #region SQLPRProvider

        /// <summary>
        /// Retrieves all AdClicks
        /// </summary>
        public  List<UserHistoryInfo> GetUserHistories(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from UserHistory";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetUserHistoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the AdClick with the specified ID
        /// </summary>
        public  UserHistoryInfo GetUserHistoryByID(int UserHistoryID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from UserHistory where uh_id=@UserHistoryID", cn);
                cmd.Parameters.Add("@UserHistoryID", SqlDbType.Int).Value = UserHistoryID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUserHistoryFromReader(reader, true);
                else
                    return null;
            }
        }


        public DataSet GetUserHistoryByParam(DateTime StartDate, DateTime EndDate, string ActionType, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                DataSet dsresult= new DataSet();
                String sqlquery = "select action_date,action_type,action_comment,useraccount.cusername from userhistory inner join useraccount on userhistory.user_id=useraccount.iid " +
                    "where action_date >= '" + StartDate + "' and action_date <= '" + EndDate + "'"  ;
                if(ActionType == string.Empty)
                {
                    sqlquery = sqlquery + " order by " + cSortExpression;
                }
                else
                {
                    sqlquery = sqlquery + " and action_type like '%" + ActionType + "%' order by " + cSortExpression;
                }
                SqlDataAdapter adapter = new SqlDataAdapter(sqlquery, cn);
                cn.Open();
                adapter.Fill(dsresult);
                return dsresult;
                            }
        }


         /// <summary>
        /// Inserts a new AdClick
        /// </summary>
        public  int InsertUserHistory(UserHistoryInfo UserHistory)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into UserHistory " +
              "(User_ID, " +
              "Action_Date, " +
              "Action_Type, " +
              "Action_Comment ) " +
              "VALUES (" +
              "@User_ID, " +
              "@Action_Date, " +
              " @Action_Type, " +
              "@Action_Comment ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@User_ID", SqlDbType.Int).Value = UserHistory.User_ID;
                cmd.Parameters.Add("@Action_Date", SqlDbType.DateTime).Value = UserHistory.Action_Date;
                cmd.Parameters.Add("@Action_Type", SqlDbType.VarChar).Value = UserHistory.Action_Type;
                 cmd.Parameters.Add("@Action_Comment", SqlDbType.VarChar).Value = UserHistory.Action_Comment;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        #endregion
        #region PRProvider
          /// <summary>
        /// Returns a new AdClickInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual UserHistoryInfo GetUserHistoryFromReader(IDataReader reader)
        {
            return GetUserHistoryFromReader(reader, true);
        }
        protected virtual UserHistoryInfo GetUserHistoryFromReader(IDataReader reader, bool readMemos)
        {
            UserHistoryInfo UserHistory = new UserHistoryInfo(
              (int)reader["Uh_ID"],
              (int)reader["User_ID"],
              (DateTime)reader["Action_Date"],
              reader["Action_Type"].ToString(),
              reader["Action_Comment"].ToString());
                        return UserHistory;
        }

        /// <summary>
        /// Returns a collection of AdClickInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<UserHistoryInfo> GetUserHistoryCollectionFromReader(IDataReader reader)
        {
            return GetUserHistoryCollectionFromReader(reader, true);
        }
        protected virtual List<UserHistoryInfo> GetUserHistoryCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<UserHistoryInfo> UserHistories = new List<UserHistoryInfo>();
            while (reader.Read())
                UserHistories.Add(GetUserHistoryFromReader(reader, readMemos));
            return UserHistories;
        }


       #endregion


    }
}
#endregion
