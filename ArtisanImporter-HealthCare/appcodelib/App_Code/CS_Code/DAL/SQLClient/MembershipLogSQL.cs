﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for MembershipLogSQL
/// </summary>
/// 

#region MembershipLogInfo

namespace PearlsReview.DAL
{

    public class MembershipLogInfo
    {
        public MembershipLogInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }



        public MembershipLogInfo(int mlid, int userid, DateTime logdate, DateTime expdate, string verification, string profileid,
        bool renew_ind, bool active_ind, decimal amount, string comment, string ccnum)
        {
            this.mlid = mlid;
            this.userid = userid;
            this.logdate = logdate;
            this.expdate = expdate;
            this.verification = verification;
            this.profileid = profileid;
            this.renew_ind = renew_ind;
            this.active_ind = active_ind;
            this.amount = amount;
            this.comment = comment;
            this.ccnum = ccnum;

        }

        public MembershipLogInfo(int userid, DateTime logdate, DateTime expdate, string verification, string profileid,
           bool renew_ind, bool active_ind, decimal amount, string comment, string ccnum)
        {
            // this.mlid = mlid;
            this.userid = userid;
            this.logdate = logdate;
            this.expdate = expdate;
            this.verification = verification;
            this.profileid = profileid;
            this.renew_ind = renew_ind;
            this.active_ind = active_ind;
            this.amount = amount;
            this.comment = comment;
            this.ccnum = ccnum;

        }

        private int _mlid = 0;
        public int mlid
        {
            get { return _mlid; }
            set { _mlid = value; }
        }

        private int _userid = 0;
        public int userid
        {
            get { return _userid; }
            set { _userid = value; }
        }

        private DateTime _logdate;
        public DateTime logdate
        {
            get { return _logdate; }
            set { _logdate = value; }
        }

        private DateTime _expdate;
        public DateTime expdate
        {
            get { return _expdate; }
            set { _expdate = value; }
        }

        private string _verification;
        public string verification
        {
            get { return _verification; }
            set { _verification = value; }
        }


        private string _profileid;
        public string profileid
        {
            get { return _profileid; }
            set { _profileid = value; }
        }

        private bool _renew_ind = false;
        public bool renew_ind
        {
            get { return _renew_ind; }
            set { _renew_ind = value; }
        }

        private bool _active_ind = false;
        public bool active_ind
        {
            get { return _active_ind; }
            set { _active_ind = value; }
        }

        private decimal _amount = 0;
        public decimal amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private string _comment;
        public string comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private string _ccnum;
        public string ccnum
        {
            get { return _ccnum; }
            set { _ccnum = value; }
        }

        private bool _firstrec;
        public bool FirstRec
        {
            get { return _firstrec; }
            set { _firstrec = value; }
        }


    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        /// <summary>
        /// Updates Membershiplog
        /// </summary>

        public  bool UpdateMembershipLog(MembershipLogInfo MembershipLog)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Update MembershipLog SET " +
                    "mlid = @mlid, " +
                    "userid = @userid, " +
                    "logdate = @logdate, " +
                    "expdate = @expdate, " +
                    "verification = @verification, " +
                    "profileid = @profileid, " +
                    "renew_ind = @renew_ind, " +
                    "active_ind = @active_Ind, " +
                    "amount = @amount, " +
                    "comment = @comment, " +
                    "ccnum = @ccnum " +
                    "where  userid = @userid ", cn);
                cmd.Parameters.Add("@mlid", SqlDbType.Int).Value = MembershipLog.mlid;
                cmd.Parameters.Add("userid", SqlDbType.Int).Value = MembershipLog.userid;
                if (MembershipLog.logdate == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = MembershipLog.logdate;
                }
                if (MembershipLog.expdate == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = MembershipLog.expdate;
                }

                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = MembershipLog.verification == null ? DBNull.Value as Object : MembershipLog.verification as Object;

                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = MembershipLog.profileid == null ? DBNull.Value as Object : MembershipLog.profileid as Object;



                cmd.Parameters.Add("@renew_ind", SqlDbType.Bit).Value = MembershipLog.renew_ind;


                cmd.Parameters.Add("@active_ind", SqlDbType.Bit).Value = MembershipLog.active_ind;

                cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = MembershipLog.amount;
                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = MembershipLog.comment == null ? DBNull.Value as Object : MembershipLog.comment as Object;
                cmd.Parameters.Add("@ccnum", SqlDbType.VarChar).Value = MembershipLog.ccnum == null ? DBNull.Value as Object : MembershipLog.ccnum as Object;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);

            }
        }

        public  MembershipLogInfo GetMembershipLogByUserID(int userid)
        {
            MembershipLogInfo memloginfo = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from membershiplog where userid='" + userid + "' order by active_ind desc, expdate desc", cn);

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                memloginfo = new MembershipLogInfo();
                while (reader.Read())
                {
                    memloginfo.mlid = Convert.ToInt32(reader["mlid"]);
                    memloginfo.userid = Convert.ToInt32(reader["userid"]);
                    memloginfo.logdate = (DateTime)(Convert.IsDBNull(reader["logdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
                    memloginfo.expdate = (DateTime)(Convert.IsDBNull(reader["expdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
                    memloginfo.verification = reader["verification"].ToString();
                    memloginfo.profileid = reader["profileid"].ToString();
                    memloginfo.renew_ind = reader["renew_ind"] != DBNull.Value ? Convert.ToBoolean(reader["renew_ind"]) : false;
                    memloginfo.active_ind = reader["active_ind"] != DBNull.Value ? Convert.ToBoolean(reader["active_ind"]) : false;
                    memloginfo.amount = (decimal)(Convert.IsDBNull(reader["amount"]) ? 0 : (decimal)reader["amount"]);
                    memloginfo.comment = reader["comment"].ToString();
                    memloginfo.ccnum = reader["ccnum"].ToString();
                }
                reader.Dispose();
                return memloginfo;
            }
        }

        public  bool CancelMembershipLog(int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Update membershiplog SET " +
                    "active_ind='false' ," +
                    "comment= 'Cancelled by CSR on + getdate()'," +
                    "expdate= '" + DateTime.Now + "' " +
                    "where Userid = @userid", cn);
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                try
                {
                    cn.Open();
                    ExecuteNonQuery(cmd);
                    return true;
                }
                catch
                {
                    return false;
                }

            }
        }

        public  bool InsertMembershipLog(MembershipLogInfo MembershipLog)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Insert into MembershipLog (userid,logdate,expdate,verification, " +
                    "profileid,renew_ind,active_ind,amount,comment,ccnum)values " +
                    "(@userid, " +
                    "@logdate, " +
                    "@expdate, " +
                    "@verification, " +
                    "@profileid, " +
                    "@renew_ind, " +
                    "@active_Ind, " +
                    "@amount, " +
                    "@comment, " +
                    "@ccnum )", cn);
                //+
                //  "where  userid = @userid ", cn);
                //cmd.Parameters.Add("@mlid", SqlDbType.Int).Value = MembershipLog.mlid;
                cmd.Parameters.Add("userid", SqlDbType.Int).Value = MembershipLog.userid;
                if (MembershipLog.logdate == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@logdate", SqlDbType.DateTime).Value = MembershipLog.logdate;
                }
                if (MembershipLog.expdate == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@expdate", SqlDbType.DateTime).Value = MembershipLog.expdate;
                }

                cmd.Parameters.Add("@verification", SqlDbType.VarChar).Value = MembershipLog.verification == null ? DBNull.Value as Object : MembershipLog.verification as Object;

                cmd.Parameters.Add("@profileid", SqlDbType.VarChar).Value = MembershipLog.profileid == null ? DBNull.Value as Object : MembershipLog.profileid as Object;


                if (MembershipLog.renew_ind == false)
                {
                    cmd.Parameters.Add("@renew_ind", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@renew_ind", SqlDbType.Bit).Value = MembershipLog.renew_ind;
                }
                if (MembershipLog.active_ind == false)
                {
                    cmd.Parameters.Add("@active_ind", SqlDbType.Bit).Value = false;
                }
                else
                {
                    cmd.Parameters.Add("@active_ind", SqlDbType.Bit).Value = MembershipLog.active_ind;
                }
                cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = MembershipLog.amount;
                cmd.Parameters.Add("@comment", SqlDbType.VarChar).Value = MembershipLog.comment == null ? DBNull.Value as Object : MembershipLog.comment as Object;
                cmd.Parameters.Add("@ccnum", SqlDbType.VarChar).Value = MembershipLog.ccnum == null ? DBNull.Value as Object : MembershipLog.ccnum as Object;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);

            }
        }

        public  List<MembershipLogInfo> GetMembershipLogListByUserID(int userid)
        {
            List<MembershipLogInfo> memloginfoList = new List<MembershipLogInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from membershiplog where userid='" + userid + "' order by active_ind desc, expdate desc", cn);

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                MembershipLogInfo memloginfo;
                bool firstFlag = true;
                while (reader.Read())
                {
                    memloginfo = new MembershipLogInfo();
                    if (firstFlag == true)
                    {
                        memloginfo.FirstRec = true;
                        firstFlag = false;
                    }
                    else
                    {
                        memloginfo.FirstRec = false;
                    }

                    memloginfo.mlid = Convert.ToInt32(reader["mlid"]);
                    memloginfo.userid = Convert.ToInt32(reader["userid"]);
                    memloginfo.logdate = (DateTime)(Convert.IsDBNull(reader["logdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["logdate"]);
                    memloginfo.expdate = (DateTime)(Convert.IsDBNull(reader["expdate"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["expdate"]);
                    memloginfo.verification = reader["verification"].ToString();
                    memloginfo.profileid = reader["profileid"].ToString();
                    memloginfo.renew_ind = reader["renew_ind"] != DBNull.Value ? Convert.ToBoolean(reader["renew_ind"]) : false;
                    memloginfo.active_ind = reader["active_ind"] != DBNull.Value ? Convert.ToBoolean(reader["active_ind"]) : false;
                    memloginfo.amount = (decimal)(Convert.IsDBNull(reader["amount"]) ? 0 : (decimal)reader["amount"]);
                    memloginfo.comment = reader["comment"].ToString();
                    memloginfo.ccnum = reader["ccnum"].ToString();
                    memloginfoList.Add(memloginfo);
                }
                reader.Dispose();
                return memloginfoList;
            }
        }

        #endregion

        #region PRProvider


        #endregion

    }
}
#endregion