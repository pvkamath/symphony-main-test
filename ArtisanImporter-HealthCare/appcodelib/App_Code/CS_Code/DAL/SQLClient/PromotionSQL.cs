﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region PromotionInfo

namespace PearlsReview.DAL
{
    public class PromotionInfo
    {
        public PromotionInfo() { }

        public PromotionInfo(int ID, string cCode, string PromoType, string PromoName, bool lActive,
            bool lOneTime, decimal nDollars, string Notes, decimal nPercent, decimal nUnits, DateTime tExpires,
            DateTime tStart, int RepID)
        {
            this.ID = ID;
            this.cCode = cCode;
            this.PromoType = PromoType;
            this.PromoName = PromoName;
            this.lActive = lActive;
            this.lOneTime = lOneTime;
            this.nDollars = nDollars;
            this.Notes = Notes;
            this.nPercent = nPercent;
            this.nUnits = nUnits;
            this.tExpires = tExpires;
            this.tStart = tStart;
            this.RepID = RepID;
        }

        private int _ID = 0;
        public int ID
        {
            get { return _ID; }
            protected set { _ID = value; }
        }

        private string _cCode = "";
        public string cCode
        {
            get { return _cCode; }
            private set { _cCode = value; }
        }

        private string _PromoType = "";
        public string PromoType
        {
            get { return _PromoType; }
            private set { _PromoType = value; }
        }

        private string _PromoName = "";
        public string PromoName
        {
            get { return _PromoName; }
            private set { _PromoName = value; }
        }

        private bool _lActive = false;
        public bool lActive
        {
            get { return _lActive; }
            private set { _lActive = value; }
        }

        private bool _lOneTime = false;
        public bool lOneTime
        {
            get { return _lOneTime; }
            private set { _lOneTime = value; }
        }

        private decimal _nDollars = 0.00M;
        public decimal nDollars
        {
            get { return _nDollars; }
            private set { _nDollars = value; }
        }

        private string _Notes = "";
        public string Notes
        {
            get { return _Notes; }
            private set { _Notes = value; }
        }

        private decimal _nPercent = 0.00M;
        public decimal nPercent
        {
            get { return _nPercent; }
            private set { _nPercent = value; }
        }

        private decimal _nUnits = 0.00M;
        public decimal nUnits
        {
            get { return _nUnits; }
            private set { _nUnits = value; }
        }

        private DateTime _tExpires = DateTime.Now.AddYears(1);
        public DateTime tExpires
        {
            get { return _tExpires; }
            private set { _tExpires = value; }
        }

        private DateTime _tStart = DateTime.Now;
        public DateTime tStart
        {
            get { return _tStart; }
            private set { _tStart = value; }
        }

        private int _RepID = 0;
        public int RepID
        {
            get { return _RepID; }
            private set { _RepID = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Promotions

        /// <summary>
        /// Returns the total number of Promotions
        /// </summary>
        public  int GetPromotionCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Promotions", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Promotions
        /// </summary>
        public  List<PromotionInfo> GetPromotions(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Promotions";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetPromotionCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Promotion with the specified ID
        /// </summary>
        public  PromotionInfo GetPromotionByID(int ID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Promotions where ID=@ID", cn);
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetPromotionFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a Promotion
        /// </summary>
        public  bool DeletePromotion(int ID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Promotions where ID=@ID", cn);
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Promotion
        /// </summary>
        public  int InsertPromotion(PromotionInfo Promotion)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Promotions " +
              "(cCode, " +
              "PromoType, " +
              "PromoName, " +
              "lOneTime, " +
              "lActive, " +
              "nDollars, " +
              "Notes, " +
              "nPercent, " +
              "nUnits, " +
              "tExpires, " +
              "tStart, " +
              "RepID) " +
              "VALUES (" +
              "@cCode, " +
              "@PromoType, " +
              "@PromoName, " +
              "@lActive, " +
              "@lOneTime, " +
              "@nDollars, " +
              "@Notes, " +
              "@nPercent, " +
              "@nUnits, " +
              "@tExpires, " +
              "@tStart, " +
              "@RepID) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@cCode", SqlDbType.VarChar).Value = Promotion.cCode;
                cmd.Parameters.Add("@PromoType", SqlDbType.Char).Value = Promotion.PromoType;
                cmd.Parameters.Add("@PromoName", SqlDbType.VarChar).Value = Promotion.PromoName;
                cmd.Parameters.Add("@lActive", SqlDbType.Bit).Value = Promotion.lActive;
                cmd.Parameters.Add("@lOneTime", SqlDbType.Bit).Value = Promotion.lOneTime;
                cmd.Parameters.Add("@nDollars", SqlDbType.Decimal).Value = Promotion.nDollars;
                cmd.Parameters.Add("@Notes", SqlDbType.VarChar).Value = Promotion.Notes;
                cmd.Parameters.Add("@nPercent", SqlDbType.Decimal).Value = Promotion.nPercent;
                cmd.Parameters.Add("@nUnits", SqlDbType.Decimal).Value = Promotion.nUnits;
                cmd.Parameters.Add("@tExpires", SqlDbType.DateTime).Value = Promotion.tExpires;
                cmd.Parameters.Add("@tStart", SqlDbType.DateTime).Value = Promotion.tStart;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Promotion.RepID == 0)
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = Promotion.RepID;
                }


                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;

                return NewID;

            }
        }

        /// <summary>
        /// Updates a Promotion
        /// </summary>
        public  bool UpdatePromotion(PromotionInfo Promotion)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Promotions set " +
              "cCode = @cCode, " +
              "PromoType = @PromoType, " +
              "PromoName = @PromoName, " +
              "lActive = @lActive, " +
              "lOneTime = @lOneTime, " +
              "nDollars = @nDollars, " +
              "Notes = @Notes, " +
              "nPercent = @nPercent, " +
              "nUnits = @nUnits, " +
              "tExpires = @tExpires, " +
              "tStart = @tStart, " +
              "RepID = @RepID " +
              "where ID = @ID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@cCode", SqlDbType.VarChar).Value = Promotion.cCode;
                cmd.Parameters.Add("@PromoType", SqlDbType.Char).Value = Promotion.PromoType;
                cmd.Parameters.Add("@PromoName", SqlDbType.VarChar).Value = Promotion.PromoName;
                cmd.Parameters.Add("@lActive", SqlDbType.Bit).Value = Promotion.lActive;
                cmd.Parameters.Add("@lOneTime", SqlDbType.Bit).Value = Promotion.lOneTime;
                cmd.Parameters.Add("@nDollars", SqlDbType.Decimal).Value = Promotion.nDollars;
                cmd.Parameters.Add("@Notes", SqlDbType.VarChar).Value = Promotion.Notes;
                cmd.Parameters.Add("@nPercent", SqlDbType.Decimal).Value = Promotion.nPercent;
                cmd.Parameters.Add("@nUnits", SqlDbType.Decimal).Value = Promotion.nUnits;
                cmd.Parameters.Add("@tExpires", SqlDbType.DateTime).Value = Promotion.tExpires;
                cmd.Parameters.Add("@tStart", SqlDbType.DateTime).Value = Promotion.tStart;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Promotion.RepID == 0)
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = Promotion.RepID;
                }

                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = Promotion.ID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new PromotionInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual PromotionInfo GetPromotionFromReader(IDataReader reader)
        {
            return GetPromotionFromReader(reader, true);
        }
        protected virtual PromotionInfo GetPromotionFromReader(IDataReader reader, bool readMemos)
        {
            PromotionInfo Promotion = new PromotionInfo(
              (int)reader["ID"],
              reader["cCode"].ToString(),
              reader["PromoType"].ToString(),
              reader["PromoName"].ToString(),
              (bool)reader["lActive"],
              (bool)reader["lOneTime"],
              (decimal)reader["nDollars"],
              reader["Notes"].ToString(),
              (decimal)reader["nPercent"],
              (decimal)reader["nUnits"],
              (DateTime)reader["tExpires"],
              (DateTime)reader["tStart"],
              (int)(Convert.IsDBNull(reader["RepID"]) ? (int)0 : (int)reader["RepID"]));

            return Promotion;
        }

        /// <summary>
        /// Returns a collection of PromotionInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<PromotionInfo> GetPromotionCollectionFromReader(IDataReader reader)
        {
            return GetPromotionCollectionFromReader(reader, true);
        }
        protected virtual List<PromotionInfo> GetPromotionCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<PromotionInfo> Promotions = new List<PromotionInfo>();
            while (reader.Read())
                Promotions.Add(GetPromotionFromReader(reader, readMemos));
            return Promotions;
        }
        #endregion
    }
}
#endregion