﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region TestInfo

namespace PearlsReview.DAL
{
    public class TestInfo
    {
        public TestInfo() { }

        public TestInfo(int TestID, string OldTestID, int TopicID, int UserID, DateTime BuyDate,
            decimal Credits, DateTime LastMod, string Name, bool NoSurvey, string Status, string UserName,
            string XMLTest, string XMLSurvey, int TestVers, int LectEvtID, bool SurveyComplete,
            bool VignetteComplete,
            //string XMLVignette, int VignetteVersion, int VignetteScore,
            int Score,
            DateTime Printed_Date,
            DateTime Emailed_Date, DateTime Reported_Date, 
            DateTime View_Date, int UniqueID, int AlterID, bool GCCharged, int facilityid)
        {
            this.TestID = TestID;
            this.OldTestID = OldTestID;
            this.TopicID = TopicID;
            this.UserID = UserID;
            this.BuyDate = BuyDate;
            this.Credits = Credits;
            this.LastMod = LastMod;
            this.Name = Name;
            this.NoSurvey = NoSurvey;
            this.Status = Status;
            this.UserName = UserName;
            this.XMLTest = XMLTest;
            this.XMLSurvey = XMLSurvey;
            this.TestVers = TestVers;
            this.LectEvtID = LectEvtID;
            this.SurveyComplete = SurveyComplete;
            this.VignetteComplete = VignetteComplete;
            //this.XMLVignette = XMLVignette;
            //this.VignetteVersion = VignetteVersion;
            //this.VignetteScore = VignetteScore;
            this.Score = Score;
            this.Printed_Date = Printed_Date;
            this.Emailed_Date = Emailed_Date;
            this.Reported_Date = Reported_Date;
            this.View_Date = View_Date;
            this.UniqueID = UniqueID;
            this.AlterID = AlterID;
            this.GCCharged = GCCharged;
            this.facilityid = facilityid;

        }

        public TestInfo(int topicid, int userid, DateTime buydate, string topicname, string username, string status, DateTime
             printeddate, DateTime emaileddate, DateTime reporteddate, int uniqueid, int facilityid)
        {
            //this.TopicID = topicid;
            //this.UserID = userid;
            //this.BuyDate = buydate;
            //this.Name = topicname;
            //this.UserName = username;
            //this.Status = status;
            //this.Printed_Date = printeddate;
            //this.Emailed_Date = emaileddate;
            //this.Reported_Date = reporteddate;
            //this.UniqueID = uniqueid;
            //this.facilityid = facilityid;
        }


        private int _TestID = 0;
        public int TestID
        {
            get { return _TestID; }
            private set { _TestID = value; }
        }

        private string _OldTestID = "";
        public string OldTestID
        {
            get { return _OldTestID; }
            private set { _OldTestID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

        //private string _Subtitle;
        //public string Subtitle
        //{
        //    get { return _Subtitle; }
        //    private set { _Subtitle = value; }
        //}

        private string _Course_Number;
        public string Course_Number
        {
            get { return _Course_Number; }
            private set { _Course_Number = value; }
        }
        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            private set { _UserID = value; }
        }

        private DateTime _BuyDate = System.DateTime.Now;
        public DateTime BuyDate
        {
            get { return _BuyDate; }
            private set { _BuyDate = value; }
        }

        private decimal _Credits = 0.00M;
        public decimal Credits
        {
            get { return _Credits; }
            private set { _Credits = value; }
        }

        private DateTime _LastMod = System.DateTime.Now;
        public DateTime LastMod
        {
            get { return _LastMod; }
            private set { _LastMod = value; }
        }

        private string _Name = "";
        public string Name
        {
            get { return _Name; }
            private set { _Name = value; }
        }

        private bool _NoSurvey = false;
        public bool NoSurvey
        {
            get { return _NoSurvey; }
            private set { _NoSurvey = value; }
        }

        private string _Status = "";
        public string Status
        {
            get { return _Status; }
            private set { _Status = value; }
        }

        private string _UserName = "";
        public string UserName
        {
            get { return _UserName; }
            private set { _UserName = value; }
        }

        private string _XMLTest = "";
        public string XMLTest
        {
            get { return _XMLTest; }
            private set { _XMLTest = value; }
        }

        private string _XMLSurvey = "";
        public string XMLSurvey
        {
            get { return _XMLSurvey; }
            private set { _XMLSurvey = value; }
        }

        private int _TestVers = 0;
        public int TestVers
        {
            get { return _TestVers; }
            private set { _TestVers = value; }
        }

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            private set { _LectEvtID = value; }
        }

        private bool _SurveyComplete = false;
        public bool SurveyComplete
        {
            get { return _SurveyComplete; }
            private set { _SurveyComplete = value; }
        }

        private bool _VignetteComplete = false;
        public bool VignetteComplete
        {
            get { return _VignetteComplete; }
            private set { _VignetteComplete = value; }
        }

        //private string _XMLVignette = "";
        //public string XMLVignette
        //{
        //    get { return _XMLVignette; }
        //    private set { _XMLVignette = value; }
        //}

        //private int _VignetteVersion = 0;
        //public int VignetteVersion
        //{
        //    get { return _VignetteVersion; }
        //    private set { _VignetteVersion = value; }
        //}

        //private int _VignetteScore = 0;
        //public int VignetteScore
        //{
        //    get { return _VignetteScore; }
        //    private set { _VignetteScore = value; }
        //}

        private int _Score = 0;
        public int Score
        {
            get { return _Score; }
            private set { _Score = value; }
        }

        private DateTime _Printed_Date = System.DateTime.Now;
        public DateTime Printed_Date
        {
            get { return _Printed_Date; }
            private set { _Printed_Date = value; }
        }

        private DateTime _Emailed_Date = System.DateTime.Now;
        public DateTime Emailed_Date
        {
            get { return _Emailed_Date; }
            private set { _Emailed_Date = value; }
        }

        private DateTime _Reported_Date = System.DateTime.Now;
        public DateTime Reported_Date
        {
            get { return _Reported_Date; }
            private set { _Reported_Date = value; }
        }

        private DateTime _View_Date = System.DateTime.Now;
        public DateTime View_Date
        {
            get { return _View_Date; }
            private set { _View_Date = value; }
        }

        private int _UniqueID = 0;
        public int UniqueID
        {
            get { return _UniqueID; }
            private set { _UniqueID = value; }
        }

        private int _AlterID = 0;
        public int AlterID
        {
            get { return _AlterID; }
            private set { _AlterID = value; }
        }

        private bool _GCCharged = false;
        public bool GCCharged
        {
            get { return _GCCharged; }
            private set { _GCCharged = value; }
        }
        private int _facilityid = 0;
        public int facilityid
        {
            get { return _facilityid; }
            set { _facilityid = value; }
        }



    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Tests

        public DataSet GetStaffEnrollmentsBySearechCriteria(bool isAdmin,int uniqueid,string firstname, string lastname, int facilityid, int deptid, int topicid,
           int startrow, int maximumrows, string sortexpression, ref int numResults
           )
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "sp_SearchStaffByCriteria";
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[]{
              new SqlParameter("@isAdmin",isAdmin),     
              new SqlParameter("@FirstName",firstname.Trim()),
              new SqlParameter("@LastName",lastname.Trim()),
              new SqlParameter("@FacilityID",facilityid),
              new SqlParameter("@Deptid",deptid),
              new SqlParameter("@Topicid",topicid),
              new SqlParameter("@StartRow",startrow),
              new SqlParameter("@MaximumRows",maximumrows),
              new SqlParameter("@SortExpression",sortexpression),
               new SqlParameter("@Uniqueid",uniqueid),
              new SqlParameter("@NumResults", numResults)
              };

                parameters[10].Direction = ParameterDirection.InputOutput;

                cmd.Parameters.AddRange(parameters);
                cn.Open();

                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                ad.Fill(ds, "staff");

                numResults = Convert.ToInt32(cmd.Parameters["@NumResults"].Value.ToString());
                cn.Close();
                int counts = ds.Tables[0].Rows.Count;
                return ds;
            }
        }

        public bool DeleteEnrollment(int testid, int topicid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_DeleteEnrollment", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@testid", SqlDbType.Int).Value = testid;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cn.Open();

                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);

            }
        }

        public int GetTestid(int topicid, int facilityid, int iid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                int result;
                string sql = "SELECT testid FROM test  WHERE test.topicid=@topicid AND test.facilityid=@facilityid AND test.userid=@iid";
                SqlCommand cmd = new SqlCommand(sql, cn);
                cn.Open();
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
                cmd.Parameters.Add("@iid", SqlDbType.Int).Value = iid;

                IDataReader reader = ExecuteReader(cmd);

                if (reader.Read())

                    result = (int)reader["testid"];
                else
                    return 0;

                return result;
            }
        }

        public string GetEnrollmentStatus(int topicid, int facilityid, int iid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string result;
                string sql = "SELECT CASE WHEN EXISTS (SELECT 1 FROM test  WHERE test.topicid=@topicid AND test.facilityid=@facilityid AND test.userid=@iid)THEN 'Yes' ELSE 'No' END AS enrolled";
                SqlCommand cmd = new SqlCommand(sql, cn);
                cn.Open();
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
                cmd.Parameters.Add("@iid", SqlDbType.Int).Value = iid;

                IDataReader reader = ExecuteReader(cmd);

                if (reader.Read())

                    result = Convert.ToString(reader["enrolled"]);

                else result = "no";

                return result;
            }
        }
        public string GetCompletePercentage(int testid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string sql = "SELECT  CONVERT(INTEGER,CONVERT(DECIMAL(3,0),SUM (CASE WHEN ip_status='C' THEN 1 ELSE 0 END)) /CONVERT(DECIMAL(3,0),COUNT(*))*100) AS '% Complete' FROM dbo.IceProgress WHERE testid=@testid";
                SqlCommand cmd2 = new SqlCommand(sql, cn);
                cmd2.Parameters.Add("@testid", SqlDbType.Int).Value = testid;
                cn.Open();

                IDataReader reader = ExecuteReader(cmd2);
                if (reader.Read())
                {
                    string percent = Convert.ToString(reader["% Complete"]);
                    if (percent == "")
                        percent = "0";
                    return percent;
                }
                else
                    return "0";
            }
        }


        public DataSet GetOTCompletions()
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_OTcompletions";              
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }


        public DataSet GetEnrolleeByUserid(int userid, int topicid, int facilityid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("sp_get_enrollee", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
                cn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();


                da.Fill(ds, "Enrollee");


                cn.Close();
                return ds;

            }

        }

        public int UpdateEnrollment(int testid, int uniqueid, DateTime printed_date, DateTime email_date, string status)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string sql = "UPDATE test set uniqueid=@uniqueid, printed_date=@printed_date, status=@status,emailed_date=@email_date WHERE testid=@testid";
                SqlCommand cmd = new SqlCommand(sql,cn);

                cmd.Parameters.Add("@uniqueid", SqlDbType.Int).Value = uniqueid;
                cmd.Parameters.Add("@printed_date", SqlDbType.DateTime).Value = printed_date;
                cmd.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
                cmd.Parameters.Add("@testid", SqlDbType.Int).Value = testid;
                cmd.Parameters.Add("@email_date", SqlDbType.DateTime).Value = email_date;

                cn.Open();
                return cmd.ExecuteNonQuery();
            }   
        }

        public DataSet GetUserEnrollmentsBySearechCriteria(int uniqueid, string firstname, string lastname, int facilityid, int
 deptid, int topicid,
           int startrow, int maximumrows,string status,string sortexpression, ref int numResults
           )
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "sp_SearchEnrollmentsByCriteria";
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
               
                
                SqlParameter[] parameters = new SqlParameter[]{
              new SqlParameter("@FirstName",firstname.Trim()),
              new SqlParameter("@LastName",lastname.Trim()),
              new SqlParameter("@FacilityID",facilityid),
              new SqlParameter("@Deptid",deptid),
              new SqlParameter("@Topicid",topicid),
              new SqlParameter("@StartRow",startrow),
              new SqlParameter("@MaximumRows",maximumrows),
              new SqlParameter("@Status",status),
              new SqlParameter("@SortExpression",sortexpression),   
              new SqlParameter("@Uniqueid",uniqueid),
              new SqlParameter("@NumResults", numResults)
              };

                parameters[10].Direction = ParameterDirection.InputOutput;

                cmd.Parameters.AddRange(parameters);
                cn.Open();

                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                ad.Fill(ds, "enrollment");

               
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                   
                    string deadline = Convert.ToString(row["deadline"]);
                    DateTime dt = new DateTime();
                    if (deadline != "")
                    {
                        dt = Convert.ToDateTime(deadline);
                        row["deadline"] = dt.ToString("d");
                    }

                    
                   

                    string courseName = Convert.ToString(row["TopicName"]);
                    if (courseName.Length > 41)
                        courseName.Insert(4, "\n");
                    row["TopicName"] = courseName;


                }
                numResults = Convert.ToInt32(cmd.Parameters["@NumResults"].Value.ToString());

                cn.Close();
                int counts = ds.Tables[0].Rows.Count;

              
                return ds;

            }
        }
        /// <summary>
        /// Returns the total number of Tests
        /// </summary>
        public  int GetTestCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Test", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

       
        /// <summary>
        /// Retrieves all Tests
        /// </summary>
        public  List<TestInfo> GetTests(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Test";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTestCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Test with the specified ID
        /// </summary>
        public  TestInfo GetTestByID(int TestID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Test where TestID=@TestID", cn);
                cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = TestID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves all Tests(Bhaskar N)
        /// </summary>
        public  List<TestInfo> GetAllTestsByTopicId(int TopicId, string FromDate, string Todate, string SortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Test " +
                    "where status='c' and TopicId= @TopicId";

                if (FromDate.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " and lastmod >= @FromDate";
                }
                if (Todate.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " and lastmod <= @Todate";
                }

                if (SortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + SortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = FromDate;
                cmd.Parameters.Add("@Todate", SqlDbType.VarChar).Value = Todate;
                //cmd.Parameters.Add("@SortExpression", SqlDbType.VarChar).Value = SortExpression;

                cn.Open();
                return GetTestCollectionFromReader(ExecuteReader(cmd), false);



            }
        }


        /// <summary>
        /// Deletes a Test
        /// </summary>
        public  bool DeleteTest(int TestID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Test where TestID=@TestID", cn);
                cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = TestID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        ///  <summary>
        ///  Inserts a new gift card
        ///  </summary>
        ///  
         /// <summary>
        /// Inserts a new Test and update icesetion and iceprogress
        /// </summary>
        public int EnrollStaff(TestInfo Test)
        {
            //using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            //{
            //    SqlCommand cmd = new SqlCommand("sp_enrollStaff", cn);
            //    cmd.CommandType=CommandType.StoredProcedure;
            //    cmd.Parameters.Add("@topicid",SqlDbType.Int).Value=Test.TopicID;
            //    cmd.Parameters.Add("@userid", SqlDbType.Int).Value = Test.UserID;
            //    cmd.Parameters.Add("@buydate", SqlDbType.DateTime).Value = Test.BuyDate;
            //    cmd.Parameters.Add("@topicname", SqlDbType.VarChar).Value = Test.Name;
            //    cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = Test.UserName;
            //    cmd.Parameters.Add("@status", SqlDbType.Char).Value = Test.Status;
            //    cmd.Parameters.Add("@printeddate", SqlDbType.DateTime).Value = Test.Printed_Date;
            //    cmd.Parameters.Add("@emaileddate", SqlDbType.DateTime).Value = Test.Emailed_Date;
            //    cmd.Parameters.Add("@reporteddate", SqlDbType.DateTime).Value = Test.Reported_Date;
            //    cmd.Parameters.Add("@uniqueid", SqlDbType.Int).Value = Test.UniqueID;
            //    cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = Test.facilityid;
            //    cn.Open();

            //   return  cmd.ExecuteNonQuery();
            //}
            return 0;
        }
        /// <summary>
        /// Inserts a new Test
        /// </summary>
        public  int InsertTest(TestInfo Test)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //              SqlCommand cmd = new SqlCommand("select RIGHT(SYS(2015),9) from Test WHERE '0' ",cn);
                //              string newID = (string)ExecuteScalar(cmd);
                //Test.TestID = newID.ToString();
               
                //              SqlCommand cmd2 = new SqlCommand("SELECT Count(*) FROM Test WHERE TestID = '" + newID + "'", cn);
                //              int IDCount = (int)ExecuteScalar(cmd2);
                //
                //			  if (IDCount > 0)
                //			  {
                //                  // matching id, so return false
                //                  return false;                  
                //              }    

                // no matching id, so we are good to go
                SqlCommand cmd = new SqlCommand("insert into Test " +
                "(OldTestID, " +
                "TopicID, " +
                "UserID, " +
                "BuyDate, " +
                "Credits, " +
                "LastMod, " +
                "Name, " +
                "NoSurvey, " +
                "Status, " +
                "UserName, " +
                "XMLTest, " +
                "XMLSurvey, " +
                "TestVers, " +
                "LectEvtID, " +
                "SurveyComplete, " +
                "VignetteComplete, " +
                    //"XMLVignette, " +
                    //"VignetteVersion, " +
                    //"VignetteScore, " +
                "Score, " +
                    "Printed_Date, " +
                    "Emailed_Date, " +
                    "Reported_Date, " +
                "View_Date, " +
                "UniqueID, " +
                "facilityid, " +
                "AlterID, " +
                "GCCharged) " +
                "VALUES (" +
                "@OldTestID, " +
                "@TopicID, " +
                "@UserID, " +
                "@BuyDate, " +
                "@Credits, " +
                "@LastMod, " +
                "@Name, " +
                "@NoSurvey, " +
                "@Status, " +
                "@UserName, " +
                "@XMLTest, " +
                "@XMLSurvey, " +
                "@TestVers, " +
                "@LectEvtID, " +
                "@SurveyComplete, " +
                "@VignetteComplete, " +
                    //"@XMLVignette, " +
                    //"@VignetteVersion, " +
                    //"@VignetteScore, " +
                "@Score, " +
                "@Printed_Date, " +
                "@Emailed_Date, " +
                "@Reported_Date, " +
                "@View_Date, " +
                "@UniqueID, " +
                "@facilityid, " +
                "@AlterID, " +
                "@GCCharged) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@OldTestID", SqlDbType.Char).Value = Test.OldTestID;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Test.TopicID;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Test.UserID;
                cmd.Parameters.Add("@BuyDate", SqlDbType.DateTime).Value = Test.BuyDate;
                cmd.Parameters.Add("@Credits", SqlDbType.Decimal).Value = Test.Credits;
                cmd.Parameters.Add("@LastMod", SqlDbType.DateTime).Value = Test.LastMod;
                cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = Test.Name;
                cmd.Parameters.Add("@NoSurvey", SqlDbType.Bit).Value = Test.NoSurvey;
                cmd.Parameters.Add("@Status", SqlDbType.Char).Value = Test.Status;
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Test.UserName;
                cmd.Parameters.Add("@XMLTest", SqlDbType.Text).Value = Test.XMLTest;
                cmd.Parameters.Add("@XMLSurvey", SqlDbType.Text).Value = Test.XMLSurvey;
                cmd.Parameters.Add("@TestVers", SqlDbType.Int).Value = Test.TestVers;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Test.LectEvtID == 0)
                {
                    cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = Test.LectEvtID;
                }

                cmd.Parameters.Add("@SurveyComplete", SqlDbType.Bit).Value = Test.SurveyComplete;
                cmd.Parameters.Add("@VignetteComplete", SqlDbType.Bit).Value = Test.VignetteComplete;
                //cmd.Parameters.Add("@XMLVignette", SqlDbType.Text).Value = Test.XMLVignette;
                //cmd.Parameters.Add("@VignetteVersion", SqlDbType.Int).Value = Test.VignetteVersion;
                //cmd.Parameters.Add("@VignetteScore", SqlDbType.Int).Value = Test.VignetteScore;
                cmd.Parameters.Add("@Score", SqlDbType.Int).Value = Test.Score;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Test.Printed_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Printed_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Printed_Date", SqlDbType.DateTime).Value = Test.Printed_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Test.Emailed_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Emailed_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Emailed_Date", SqlDbType.DateTime).Value = Test.Emailed_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Test.Reported_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Reported_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Reported_Date", SqlDbType.DateTime).Value = Test.Reported_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Test.View_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@View_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@View_Date", SqlDbType.DateTime).Value = Test.View_Date;
                }

                cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = Test.UniqueID;
                cmd.Parameters.Add("@AlterID", SqlDbType.Int).Value = Test.AlterID;
                cmd.Parameters.Add("@GCCharged", SqlDbType.Bit).Value = Test.GCCharged;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = Test.facilityid;

                cn.Open();

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        public bool UpdateMergedIDs(string Ids, int AccountMergeTo)
        {
            int result = 0;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                String Str = String.Format("Update test set userid = {0} where userid in ({1})", AccountMergeTo, Ids);
                SqlCommand cmd = new SqlCommand(Str, cn);
                cn.Open();
                result = ExecuteNonQuery(cmd);
            }
            return (result > 0);
        }
        /// <summary>
        /// Updates a Test
        /// </summary>
        public  bool UpdateTest(TestInfo Test)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Test set " +
              "OldTestID = @OldTestID, " +
              "TopicID = @TopicID, " +
              "UserID = @UserID, " +
              "BuyDate = @BuyDate, " +
              "Credits = @Credits, " +
              "LastMod = @LastMod, " +
              "Name = @Name, " +
              "NoSurvey = @NoSurvey, " +
              "Status = @Status, " +
              "UserName = @UserName, " +
              "XMLTest = @XMLTest, " +
              "XMLSurvey = @XMLSurvey, " +
              "TestVers = @TestVers, " +
              "LectEvtID = @LectEvtID, " +
              "SurveyComplete = @SurveyComplete, " +
              "VignetteComplete = @VignetteComplete, " +
                    //"XMLVignette = @XMLVignette, " +
                    //"VignetteVersion = @VignetteVersion, " +
                    //"VignetteScore = @VignetteScore, " +
              "Score = @Score, " +
              "Printed_Date = @Printed_Date, " +
              "Emailed_Date = @Emailed_Date, " +
              "Reported_Date = @Reported_Date, " +
              "View_Date = @View_Date, " +
              "UniqueID = @UniqueID, " +
              "AlterID = @AlterID, " +
              "facilityid = @facilityid, " +
              "GCCharged = @GCCharged " +
              "where TestID = @TestID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OldTestID", SqlDbType.Char).Value = Test.OldTestID;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Test.TopicID;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Test.UserID;
                cmd.Parameters.Add("@BuyDate", SqlDbType.DateTime).Value = Test.BuyDate;
                cmd.Parameters.Add("@Credits", SqlDbType.Decimal).Value = Test.Credits;
                cmd.Parameters.Add("@LastMod", SqlDbType.DateTime).Value = Test.LastMod;
                cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = Test.Name;
                cmd.Parameters.Add("@NoSurvey", SqlDbType.Bit).Value = Test.NoSurvey;
                cmd.Parameters.Add("@Status", SqlDbType.Char).Value = Test.Status;
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Test.UserName;
                cmd.Parameters.Add("@XMLTest", SqlDbType.VarChar).Value = Test.XMLTest;
                cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = Test.XMLSurvey;
                cmd.Parameters.Add("@TestVers", SqlDbType.Int).Value = Test.TestVers;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Test.LectEvtID == 0)
                {
                    cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = Test.LectEvtID;
                }

                cmd.Parameters.Add("@SurveyComplete", SqlDbType.Bit).Value = Test.SurveyComplete;
                cmd.Parameters.Add("@VignetteComplete", SqlDbType.Bit).Value = Test.VignetteComplete;
                //cmd.Parameters.Add("@XMLVignette", SqlDbType.Text).Value = Test.XMLVignette;
                //cmd.Parameters.Add("@VignetteVersion", SqlDbType.Int).Value = Test.VignetteVersion;
                //cmd.Parameters.Add("@VignetteScore", SqlDbType.Int).Value = Test.VignetteScore;
                cmd.Parameters.Add("@Score", SqlDbType.Int).Value = Test.Score;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Test.Printed_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Printed_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Printed_Date", SqlDbType.DateTime).Value = Test.Printed_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Test.Emailed_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Emailed_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Emailed_Date", SqlDbType.DateTime).Value = Test.Emailed_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Test.Reported_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Reported_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Reported_Date", SqlDbType.DateTime).Value = Test.Reported_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Test.View_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@View_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@View_Date", SqlDbType.DateTime).Value = Test.View_Date;
                }

                cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = Test.UniqueID;
                cmd.Parameters.Add("@AlterID", SqlDbType.Int).Value = Test.AlterID;
                cmd.Parameters.Add("@GCCharged", SqlDbType.Bit).Value = Test.GCCharged;
                cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = Test.TestID;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = Test.facilityid;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Updates all Test's TopicName field for a certain TopicID
        /// </summary>
        public  bool UpdateTestTopicNameByTopicID(int TopicID, string TopicName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Test set " +
              "Name = @Name " +
              "where TopicID = @TopicID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = TopicName;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public  bool UpdateTestUseridByNewId(int newId, int OldId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Test set " +
              "UserId = @newId " +
              "where UserId = @OldId ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@newId", SqlDbType.Int).Value = newId;
                cmd.Parameters.Add("@OldId", SqlDbType.Int).Value = OldId;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public bool UpdateTestFollowUpSurvey(int testid, bool value) 
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString)) 
            {
                SqlCommand cmd = new SqlCommand("update test set followupsurvey=@value where testid=@testid", cn);
                cmd.Parameters.Add("@testid", SqlDbType.Int).Value = testid;
                cmd.Parameters.Add("@value", SqlDbType.Bit).Value = value ? 1 : 0;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public bool UnEnrollByUserIdAndTopicId(int userid, int topicid) 
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from ICEProgress where testid in (select testid from test where userid=@userid and topicid=@topicid and status in ('A','I') and printed_date is not null) "+
                    " delete from test where userid=@userid and topicid=@topicid and status in ('A','I') and printed_date is not null update icesection set available_seat=available_seat+1 where topicid=@topicid ", cn);
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public bool IsEnrolledByTopicIdAndUserId(int userid, int topicid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From Test Where userid=@userid and topicid=@topicid and status in ('A','I') and printed_date is not null", cn);
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return true;
                else
                    return false;
            }
        }


        public bool HasEnrolledStuffByTopicIdAndEnrolledById(int enrolledbyid, int topicid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From Test Where uniqueid=@enrolledbyid and topicid=@topicid and status in ('A','I') and printed_date is not null", cn);
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@enrolledbyid", SqlDbType.Int).Value = enrolledbyid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return true;
                else
                    return false;
            }
        }
        

        /// <summary>
        /// Returns the total number of test records for the specified TopicID
        /// </summary>
        public  int GetTestCountByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Test " +
                    "where TopicID = @TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }
        public  int GetFacilityIDByTestID(int TestID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(" SELECT   Topic.facilityid FROM Test INNER JOIN   Topic ON Test.topicid = Topic.topicid" +
                    " where Test.testid  = @TestID", cn);
                cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = TestID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of test records for the specified UserName
        /// </summary>
        public  int GetTestCountByUserName(string UserName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Test " +
                    "where Test.username = @UserName", cn);
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of test records for the specified UserID
        /// </summary>
        public  int GetTestCountByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Test " +
                    "where UserID = @UserID", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.VarChar).Value = UserID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Tests for the specified UserName
        /// </summary>
        public  List<TestInfo> GetTestsByUserName(string UserName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "TestID, " +
                    "OldTestID, " +
                    "TopicID, " +
                    "UserID, " +
                    "UserName, " +
                    "Name, " +
                    "Status, " +
                    "BuyDate, " +
                    "LastMod, " +
                    "Credits, " +
                    "NoSurvey, " +
                    "XMLTest, " +
                    "XMLSurvey, " +
                    "TestVers, " +
                    "LectEvtID, " +
                    "SurveyComplete, " +
                  "VignetteComplete, " +
                    //"XMLVignette, " +
                    //"VignetteVersion, " +
                    //"VignetteScore, " +
                  "Score, " +
                  "Printed_Date, " +
                  "Emailed_Date, " +
                  "Reported_Date, " +
                  "View_Date, " +
                  "UniqueID, " +
                  "AlterID, " +
                  "facilityid, " +
                  "GCCharged " +
                    "from Test " +
                    "where UserName = @UserName " +
                    "order by LastMod DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName;
                cn.Open();
                return GetTestCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all Incomplete Tests for the specified UserName
        /// </summary>
        public  List<TestInfo> GetIncompleteTestsByUserName(string UserName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "TestID, " +
                    "OldTestID, " +
                    "TopicID, " +
                    "UserID, " +
                    "UserName, " +
                    "Name, " +
                    "Status, " +
                    "BuyDate, " +
                    "LastMod, " +
                    "Credits, " +
                    "NoSurvey, " +
                    "XMLTest, " +
                    "XMLSurvey, " +
                    "TestVers, " +
                    "LectEvtID, " +
                    "SurveyComplete, " +
                  "VignetteComplete, " +
                    //"XMLVignette, " +
                    //"VignetteVersion, " +
                    //"VignetteScore, " +
                  "Score, " +
                    "Printed_Date, " +
                    "Emailed_Date, " +
                    "Reported_Date, " +
                  "View_Date, " +
                  "UniqueID, " +
                  "AlterID, " +
                    "facilityid, " +
                  "GCCharged " +
                    "from Test " +
                    "where UserName = @UserName " +
                    "and Status = 'I' " +
                    "order by LastMod DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName;
                cn.Open();
                return GetTestCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Completed Tests for the specified UserName
        /// </summary>
        public  List<TestInfo> GetCompletedTestsByUserName(string UserName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "TestID, " +
                    "OldTestID, " +
                    "TopicID, " +
                    "UserID, " +
                    "UserName, " +
                    "Name, " +
                    "Status, " +
                    "BuyDate, " +
                    "LastMod, " +
                    "Credits, " +
                    "NoSurvey, " +
                    "XMLTest, " +
                    "XMLSurvey, " +
                    "TestVers, " +
                    "LectEvtID, " +
                    "SurveyComplete, " +
                  "VignetteComplete, " +
                    //"XMLVignette, " +
                    //"VignetteVersion, " +
                    //"VignetteScore, " +
                  "Score, " +
                    "Printed_Date, " +
                    "Emailed_Date, " +
                    "Reported_Date, " +
                  "View_Date, " +
                  "UniqueID, " +
                  "AlterID, " +
                    "facilityid, " +
                  "GCCharged " +
                    "from Test " +
                    "where UserName = @UserName " +
                    "and Status = 'C' " +
                    "order by LastMod DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName;
                cn.Open();
                return GetTestCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Tests for the specified UserID
        /// </summary>
        public  List<TestInfo> GetTestsByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "TestID, " +
                    "OldTestID, " +
                    "TopicID, " +
                    "UserID, " +
                    "UserName, " +
                    "Name, " +
                    "Status, " +
                    "BuyDate, " +
                    "LastMod, " +
                    "Credits, " +
                    "NoSurvey, " +
                    "XMLTest, " +
                    "XMLSurvey, " +
                    "TestVers, " +
                    "LectEvtID, " +
                    "SurveyComplete, " +
                  "VignetteComplete, " +
                    //"XMLVignette, " +
                    //"VignetteVersion, " +
                    //"VignetteScore, " +
                  "Score, " +
                    "Printed_Date, " +
                    "Emailed_Date, " +
                    "Reported_Date, " +
                  "View_Date, " +
                  "UniqueID, " +
                  "AlterID, " +
                    "facilityid, " +
                  "GCCharged " +
                    "from Test " +
                    "where UserID = @UserID " +
                    "order by LastMod DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return GetTestCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Incomplete Tests for the specified UserID
        /// </summary>
        public  List<TestInfo> GetIncompleteTestsByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "TestID, " +
                    "OldTestID, " +
                    "TopicID, " +
                    "UserID, " +
                    "UserName, " +
                    "Name, " +
                    "Status, " +
                    "BuyDate, " +
                    "LastMod, " +
                    "Credits, " +
                    "NoSurvey, " +
                    "XMLTest, " +
                    "XMLSurvey, " +
                    "TestVers, " +
                    "LectEvtID, " +
                    "SurveyComplete, " +
                  "VignetteComplete, " +
                    //"XMLVignette, " +
                    //"VignetteVersion, " +
                    //"VignetteScore, " +
                  "Score, " +
                    "Printed_Date, " +
                    "Emailed_Date, " +
                    "Reported_Date, " +
                  "View_Date, " +
                  "UniqueID, " +
                  "AlterID, " +
                    "facilityid, " +
                  "GCCharged " +
                    "from Test " +
                    "where UserID = @UserID " +
                    "and Status = 'I' " +
                    "order by LastMod DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return GetTestCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Incomplete Tests for the specified UserID
        /// </summary>
        public  List<TestInfo> GetIncompleteTestsByUserIDAndTopicID(int UserID, int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "TestID, " +
                    "OldTestID, " +
                    "TopicID, " +
                    "UserID, " +
                    "UserName, " +
                    "Name, " +
                    "Status, " +
                    "BuyDate, " +
                    "LastMod, " +
                    "Credits, " +
                    "NoSurvey, " +
                    "XMLTest, " +
                    "XMLSurvey, " +
                    "TestVers, " +
                    "LectEvtID, " +
                    "SurveyComplete, " +
                  "VignetteComplete, " +
                    //"XMLVignette, " +
                    //"VignetteVersion, " +
                    //"VignetteScore, " +
                  "Score, " +
                    "Printed_Date, " +
                    "Emailed_Date, " +
                    "Reported_Date, " +
                  "View_Date, " +
                  "UniqueID, " +
                  "AlterID, " +
                    "facilityid, " +
                  "GCCharged " +
                    "from Test " +
                    "where UserID = @UserID " +
                    "and TopicID = @TopicID " +
                    "and Status In ('A','I') " +
                    "order by BuyDate DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return GetTestCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Test with the specified ID
        /// </summary>
        public TestInfo GetIncompleteICETestByUserIDAndTopicID(int UserID, int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select top 1 * from test a join topic b on a.topicid=b.topicid where status in ('I','A')  " +
                        " and UserID= @UserID and b.TopicId= @TopicID and b.topic_type='E' and printed_date is not null order by lastmod desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@TopicId", SqlDbType.VarChar).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestFromReader(reader, true);
                else
                    return null;
            }
        }
        /// <summary>
        /// Retrieves the Test with the specified ID
        /// </summary>
        public TestInfo GetTestByUserIDAndTopicID(int UserID, int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select top 1 * from test where status='c'  " +
                        " and UserID= @UserID and TopicId= @TopicID order by lastmod desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@TopicId", SqlDbType.VarChar).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the Test with the specified ID without assigning status
        /// </summary>
        public TestInfo GetTestByUserIDAndTopicIDG(int UserID, int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select top 1 * from test where  " +
                        "  UserID= @UserID and TopicId= @TopicID order by lastmod desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@TopicId", SqlDbType.VarChar).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestFromReader(reader, true);
                else
                    return null;
            }
        }
        /// <summary>
        /// Retrieves the Test with the specified ID, viewprogress use only
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="TopicID"></param>
        /// <returns></returns>
        public TestInfo GetTestByUserIDAndTopicIDP(int UserID, int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select top 1 * from test where " +
                        " UserID= @UserID and TopicId= @TopicID and printed_date is not null order by lastmod desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@TopicId", SqlDbType.VarChar).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the Test with the specified ID
        /// </summary>
        public TestInfo GetEnrolledTestByUserIDAndTopicID(int UserID, int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select top 1 * from test where status in ('I','A')  " +
                        " and UserID= @UserID and TopicId= @TopicID and printed_date is not null order by lastmod desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@TopicId", SqlDbType.VarChar).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves the failed Test with the specified ID
        /// </summary>
        public TestInfo GetFailedTestByUserIDAndTopicID(int UserID, int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select top 1 * from test where status='F'  " +
                        " and UserID= @UserID and TopicId= @TopicID order by lastmod desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@TopicId", SqlDbType.VarChar).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestFromReader(reader, true);
                else
                    return null;
            }
        }

        public TestInfo GetPaidPrepaidMSTestByUserIDAndTopicID(int UserID, int TopicID, int ProfessionID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select top 1 * from test where (status='p')  " +
                        " and UserID= @UserID and TopicId= @TopicID and facilityid = @facilityid order by lastmod desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@TopicId", SqlDbType.VarChar).Value = TopicID;
                cmd.Parameters.Add("@facilityid", SqlDbType.VarChar).Value = ProfessionID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestFromReader(reader, true);
                else
                    return null;
            }
        }

        public TestInfo GetInProgressTestByUserIDTopicIDAndMsid(int userID, int topicID, int professionID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select top 1 * from test " + 
                    " where status='p' " +
                    " and UserID= @UserID " + 
                    " and TopicId= @TopicID " + 
                    " and facilityid = @facilityid " + 
                    " order by lastmod desc", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
                cmd.Parameters.Add("@TopicId", SqlDbType.VarChar).Value = topicID;
                cmd.Parameters.Add("@facilityid", SqlDbType.VarChar).Value = professionID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves all Completed Tests for the specified UserID
        /// </summary>
        public  DataSet GetCompletedTestsByUserID(int UserID)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                  "a.VignetteComplete, " +
                    //"a.XMLVignette, " +
                    //"a.VignetteVersion, " +
                    //"a.VignetteScore, " +
                  "a.Score, " +
                    //"a.Printed_Date, " +
                    //"a.Emailed_Date, " +
                    //"a.Reported_Date, " +
                  "a.View_Date, " +
                  "a.UniqueID, " +
                  "a.AlterID, " +
                  "a.GCCharged, " +
                  "b.facilityid, " +
                  "b.Img_Name, " +
                  "b.topic_hourlong, " +
                  "b.topic_hourshort " +  
                    "from Test a inner join Topic b on a.topicid = b.topicid " +
                    "where UserID = @UserID " +
                    "and Status = 'C'" +
                    "order by LastMod DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }
        public DataSet GetMicrositeCompletedTestsByUserID(int UserID, int DomainID)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                  "a.VignetteComplete, " +
                    //"a.XMLVignette, " +
                    //"a.VignetteVersion, " +
                    //"a.VignetteScore, " +
                  "a.Score, " +
                    //"a.Printed_Date, " +
                    //"a.Emailed_Date, " +
                    //"a.Reported_Date, " +
                  "a.View_Date, " +
                  "a.UniqueID, " +
                  "a.AlterID, " +
                  "a.GCCharged, " +
                  "b.facilityid, " +
                  "b.Img_Name " +
                    "from Test a inner join Topic b on a.topicid = b.topicid " +
                    "where UserID = @UserID " +
                    "and Status = 'C' and a.facilityid = @facilityid " +
                    "order by LastMod DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = DomainID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }
        public DataSet GetCompletedTestsByUserID(int UserID, string cSortExpression)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                  "a.VignetteComplete, " +
                    //"a.XMLVignette, " +
                    //"a.VignetteVersion, " +
                    //"a.VignetteScore, " +
                  "a.Score, " +
                    //"a.Printed_Date, " +
                    //"a.Emailed_Date, " +
                    //"a.Reported_Date, " +
                  "a.View_Date, " +
                  "a.UniqueID, " +
                  "a.AlterID, " +
                  "a.GCCharged, " +
                  "b.categoryid ," + 
                  "b.facilityid, " +
                  "b.Img_Name, " +
                  "b.topic_hourlong, " +
                  "b.topic_hourshort " + 
                    "from Test a inner join Topic b on a.topicid = b.topicid " +
                    "where UserID = @UserID " +
                    "and Status = 'C'";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand + "  order by  " + cSortExpression;

                }
                else
                {
                    cSQLCommand = cSQLCommand + " order by LastMod DESC";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }
        public DataSet GetTop3CompletedTestsByUserID(int UserID, string cSortExpression)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top 3 " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "b.TopicName +' - '+ b.Course_Number  As TopicName_CourseNumber," +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                  "a.VignetteComplete, " +                  
                  "a.Score, " +                  
                  "a.View_Date, " +
                  "a.UniqueID, " +
                  "a.AlterID, " +
                  "a.GCCharged, " +
                  "b.categoryid ," +
                  "b.facilityid, " +
                  "b.Img_Name " +
                    "from Test a inner join Topic b on a.topicid = b.topicid " +
                    "where UserID = @UserID " +
                    "and Status = 'C' ";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand + "  order by  " + cSortExpression;

                }
                else
                {
                    cSQLCommand = cSQLCommand + " order by LastMod DESC";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }
        public DataSet GetMicrositeCompletedTestsByUserID(int UserID, int DomainID, string cSortExpression)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                  "a.VignetteComplete, " +
                    //"a.XMLVignette, " +
                    //"a.VignetteVersion, " +
                    //"a.VignetteScore, " +
                  "a.Score, " +
                    //"a.Printed_Date, " +
                    //"a.Emailed_Date, " +
                    //"a.Reported_Date, " +
                  "a.View_Date, " +
                  "a.UniqueID, " +
                  "a.AlterID, " +
                  "a.GCCharged, " +
                  "b.facilityid, " +
                  "b.Img_Name " +
                    "from Test a inner join Topic b on a.topicid = b.topicid " +
                    "where UserID = @UserID " +
                    "and Status = 'C' ";

                if (DomainID != 5)
                {
                    cSQLCommand += " and a.facilityid = " + DomainID.ToString() + " ";
                }
                else
                {
                    cSQLCommand += " and a.facilityid in (Select msid From MicrositeDomain) ";
                }

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand + "  order by  " + cSortExpression;

                }
                else
                {
                    cSQLCommand = cSQLCommand + " order by LastMod DESC";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }
        public DataSet GetMSPaidCoursesInProgressByUserId(int userID, string cSortExpression)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                    "a.VignetteComplete, " +
                    "a.Score, " +
                    "a.View_Date, " +
                    "a.UniqueID, " +
                    "a.AlterID, " +
                    "a.GCCharged, " +
                    "b.facilityid, " +
                    "b.urlmark, " +
                    "b.Img_Name " +
                "from Test a inner join Topic b on a.topicid = b.topicid " +
                "where UserID = @UserID " +
                "and Status = 'p' ";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand + "  order by  " + cSortExpression;

                }
                else
                {
                    cSQLCommand = cSQLCommand + " order by LastMod DESC";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }
        public DataSet GetPRRetailCoursesInProgressByUserId(int userID, string cSortExpression)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                    "a.VignetteComplete, " +                    
                    "a.Score, " +                   
                    "a.View_Date, " +
                    "a.UniqueID, " +
                    "a.AlterID, " +
                    "a.GCCharged, " +
                    "b.facilityid, " +
                    "b.urlmark, " +
                    "b.Img_Name " +
                "from Test a inner join Topic b on a.topicid = b.topicid " +
                "where UserID = @UserID " +
                "and a.facilityid=3 " +
                "and Status = 'I' ";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand + "  order by  " + cSortExpression;

                }
                else
                {
                    cSQLCommand = cSQLCommand + " order by LastMod DESC";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;               
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }


        // used for AhDomain
        public DataSet GetMSPaidCoursesInProgressByUserIdAndAhDomainId(int userID, int ahDomainID, string cSortExpression)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                    "a.VignetteComplete, " +
                    "a.Score, " +
                    "a.View_Date, " +
                    "a.UniqueID, " +
                    "a.AlterID, " +
                    "a.GCCharged, " +
                    "b.facilityid, " +
                    "b.urlmark, " +
                    "b.Img_Name " +
                "from Test a inner join Topic b on a.topicid = b.topicid " +
                "where UserID = @UserID " +
                " and a.facilityid=@ahDomainID "+
                "and Status = 'p' ";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand + "  order by  " + cSortExpression;

                }
                else
                {
                    cSQLCommand = cSQLCommand + " order by LastMod DESC";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
                cmd.Parameters.Add("@ahDomainID", SqlDbType.Int).Value = ahDomainID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }



        public DataSet GetTop3MSPaidCoursesInProgressByUserId(int userID, string cSortExpression)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top 3 " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "b.TopicName +' - '+ b.Course_Number  As TopicName_CourseNumber," +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                    "a.VignetteComplete, " +
                    "a.Score, " +
                    "a.View_Date, " +
                    "a.UniqueID, " +
                    "a.AlterID, " +
                    "a.GCCharged, " +
                    "b.facilityid, " +
                    "b.urlmark, " +
                    "b.Img_Name " +
                "from Test a inner join Topic b on a.topicid = b.topicid " +
                "where UserID = @UserID " +
                "and Status = 'p' ";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand + "  order by  " + cSortExpression;

                }
                else
                {
                    cSQLCommand = cSQLCommand + " order by LastMod DESC";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;                
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }


        public DataSet GetTop3PRRetailCoursesInProgressByUserId(int userID, string cSortExpression)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top 3 " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "b.TopicName +' - '+ b.Course_Number  As TopicName_CourseNumber," +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                    "a.VignetteComplete, " +
                    "a.Score, " +
                    "a.View_Date, " +
                    "a.UniqueID, " +
                    "a.AlterID, " +
                    "a.GCCharged, " +
                    "b.facilityid, " +
                    "b.urlmark, " +
                    "b.Img_Name " +
                "from Test a inner join Topic b on a.topicid = b.topicid " +
                "where UserID = @UserID " +
                "and a.facilityid=3 and Status = 'I' ";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand + "  order by  " + cSortExpression;

                }
                else
                {
                    cSQLCommand = cSQLCommand + " order by LastMod DESC";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }

        //for ahDomain

        public DataSet GetTop3MSPaidCoursesInProgressByUserIdandAhDomain(int userID, int ahDomainID, string cSortExpression)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top 3 " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "b.TopicName +' - '+ b.Course_Number  As TopicName_CourseNumber," +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                    "a.VignetteComplete, " +
                    "a.Score, " +
                    "a.View_Date, " +
                    "a.UniqueID, " +
                    "a.AlterID, " +
                    "a.GCCharged, " +
                    "b.facilityid, " +
                    "b.urlmark, " +
                    "b.Img_Name " +
                "from Test a inner join Topic b on a.topicid = b.topicid " +
                "where UserID = @UserID " +
                " and a.facilityid=@ahDomainID " +
                "and Status = 'p' ";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand + "  order by  " + cSortExpression;

                }
                else
                {
                    cSQLCommand = cSQLCommand + " order by LastMod DESC";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
                cmd.Parameters.Add("@ahDomainID", SqlDbType.Int).Value = ahDomainID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }

        /// <summary>
        /// Retrieves top 5 Completed Tests for the specified UserID
        /// </summary>
        public DataSet GetTopFiveCompletedTestsByUserID(int UserID)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top 11 " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                  "a.VignetteComplete, " +
                    //"a.XMLVignette, " +
                    //"a.VignetteVersion, " +
                    //"a.VignetteScore, " +
                  "a.Score, " +
                    //"a.Printed_Date, " +
                    //"a.Emailed_Date, " +
                    //"a.Reported_Date, " +
                  "a.View_Date, " +
                  "a.UniqueID, " +
                  "a.AlterID, " +
                  "a.GCCharged, " +
                  "b.facilityid, " +
                  "b.Img_Name, " +
                  "b.topic_hourlong, " +
                  "b.topic_hourshort " +                 
                    "from Test a inner join Topic b on a.topicid = b.topicid " +
                    "where UserID = @UserID " +
                    "and Status = 'C' " +
                    "order by LastMod DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }


        /// <summary>
        /// Retrieves all Completed Tests for the specified UserID
        /// </summary>
        public  DataSet GetCompletedTestsByUserIDandFacID(int UserID, int facilityID, string cSortExpression)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.Subtitle, " +
                    "b.Course_Number, " +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                  "a.VignetteComplete, " +
                    //"a.XMLVignette, " +
                    //"a.VignetteVersion, " +
                    //"a.VignetteScore, " +
                  "a.Score, " +
                    //"a.Printed_Date, " +
                    //"a.Emailed_Date, " +
                    //"a.Reported_Date, " +
                  "a.View_Date, " +
                  "a.UniqueID, " +
                  "a.AlterID, " +
                  "a.GCCharged, " +
                  "b.facilityid " +
                    "from Test a inner join Topic b on a.topicid = b.topicid " +
                    "where UserID = @UserID " +
                    "and Status = 'C' " +
                    "and b.facilityid  =" + facilityID +
                    "order by " + cSortExpression + " DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }

        /// <summary>
        /// Retrieves Completed Test for the specified UserID  and TestID
        /// </summary>
        public  TestInfo GetCompletedTestByTestIDUserID(int UserID, int TestID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                  "a.VignetteComplete, " +
                    //"a.XMLVignette, " +
                    //"a.VignetteVersion, " +
                    //"a.VignetteScore, " +
                  "a.Score, " +
                    "a.Printed_Date, " +
                    "a.Emailed_Date, " +
                    "a.Reported_Date, " +
                  "a.View_Date, " +
                  "a.UniqueID, " +
                  "a.AlterID, " +
                  "a.GCCharged, " +
                  "b.facilityid " +
                    "from Test a inner join Topic b on a.topicid = b.topicid " +
                    "where UserID = @UserID " +
                    "and Status = 'C' " +
                    "and TestID = @TestID " +
                    "order by LastMod DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

                cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = TestID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestFromReader(reader, true);
                else
                    return null;

            }
        }

        public int GetCompletedTestYearCountByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(Status) from Test " +
                    "WHERE (userid = @UserID) AND (status = 'C') AND (lastmod <= GETDATE()) " +
                    " AND (lastmod >= DATEADD(yy, - 1, GETDATE())) ", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public decimal GetTotalContactHoursYearCountByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select  SUM(credits) from Test " +
                    "WHERE (userid = @UserID) AND (status = 'C') AND (lastmod <= GETDATE()) " +
                    " AND (lastmod >= DATEADD(yy, - 1, GETDATE())) ", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                String ret = ExecuteScalar(cmd).ToString();
                if (string.IsNullOrEmpty(ret))
                    return 0;
                else
                    return Convert.ToDecimal(ret);
            }
        }


        /// <summary>
        /// Retrieves all Completed Tests for the specified UserID
        /// </summary>
        public  DataSet GetNeedSurveyTestsByUserID(int UserID)
        {
            SqlDataAdapter adapter;
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "a.TestID, " +
                    "a.OldTestID, " +
                    "a.TopicID, " +
                    "b.subtitle, " +
                    "b.Course_Number, " +
                    "a.UserID, " +
                    "a.UserName, " +
                    "a.Name, " +
                    "a.Status, " +
                    "a.BuyDate, " +
                    "a.LastMod, " +
                    "a.Credits, " +
                    "a.NoSurvey, " +
                    "a.XMLTest, " +
                    "a.XMLSurvey, " +
                    "a.TestVers, " +
                    "a.LectEvtID, " +
                    "a.SurveyComplete, " +
                  "a.VignetteComplete, " +
                    //"a.XMLVignette, " +
                    //"a.VignetteVersion, " +
                    //"a.VignetteScore, " +
                  "a.Score, " +
                    //"a.Printed_Date, " +
                    //"a.Emailed_Date, " +
                    //"a.Reported_Date, " +
                  "a.View_Date, " +
                  "a.UniqueID, " +
                  "a.AlterID, " +
                  "a.GCCharged, " +
                  "b.facilityid, " +
                  "b.topic_hourlong, " +
                  "b.topic_hourshort " +  
                    "from Test a inner join Topic b on a.topicid = b.topicid " +
                    "where UserID = @UserID " +
                    "and Status = 'I' and a. Score >=b.pass_score " +
                    "order by LastMod DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
                return ds;
            }
        }

        /// <summary>
        /// Retrieves all Completed Tests for the specified UserID
        /// </summary>
        public int GetNeedSurveyTestsCountByUserID(int UserID)
        {
           
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select count(*) " +
                    "from Test a inner join Topic b on a.topicid = b.topicid " +
                    "where UserID = @UserID " +
                    "and Status = 'I' and a. Score >=b.pass_score";
                   
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of test completions for the specified FacilityID
        /// </summary>
        public  int GetCompletedTestCountByFacilityID(int FacilityID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Test " +
                    "where status='C' and UserID IN (select iid from useraccount where " +
                    "FacilityID=@FacilityID) ", cn);
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the users need to send email to remind enrollment confirm or course complete
        /// </summary>
        public DataSet GetUnconfirmedorUncompletedUsers(char status)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_get_unconfirmed_or_uncompleted_users", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@status", SqlDbType.Char).Value = status;
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                return ds;
            }

        }
        /// <summary>
        /// Returns the total number of test completions for the specified User
        /// </summary>
        public int GetCompletedTestCountByUserID(int UserID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select Count(*) " +
                    " from Test a inner join Topic b on a.topicid = b.topicid where  " +
                    " UserID = @UserID and Status = 'C' ", cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Incomplete Test for the specified UserID and topicId
        /// </summary>
        public TestInfo GetPrePaidTestByUserIDAndTopicID(int UserID, int TopicID,int FacilityID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "TestID, " +
                    "OldTestID, " +
                    "TopicID, " +
                    "UserID, " +
                    "UserName, " +
                    "Name, " +
                    "Status, " +
                    "BuyDate, " +
                    "LastMod, " +
                    "Credits, " +
                    "NoSurvey, " +
                    "XMLTest, " +
                    "XMLSurvey, " +
                    "TestVers, " +
                    "LectEvtID, " +
                    "SurveyComplete, " +
                  "VignetteComplete, " +
                    //"XMLVignette, " +
                    //"VignetteVersion, " +
                    //"VignetteScore, " +
                  "Score, " +
                    "Printed_Date, " +
                    "Emailed_Date, " +
                    "Reported_Date, " +
                  "View_Date, " +
                  "UniqueID, " +
                  "AlterID, " +
                    "facilityid, " +
                  "GCCharged " +
                    "from Test " +
                    "where UserID = @UserID " +
                    "and TopicID = @TopicID " +
                    " and FacilityID = @FacilityID " +
                    "and Status = 'P' " +
                    "order by BuyDate DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTestFromReader(reader, true);
                else
                    return null;
            }
        }

         //public bool  IsUserTakePrePaidTopic(int UserID, int TopicID, int FacilityID)
         //{
         //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
         //    {
         //        string cSQLCommand = "select " +
         //            "TestID, " +
         //            "OldTestID, " +
         //            "TopicID, " +
         //            "UserID, " +
         //            "UserName, " +
         //            "Name, " +
         //            "Status, " +
         //            "BuyDate, " +
         //            "LastMod, " +
         //            "Credits, " +
         //            "NoSurvey, " +
         //            "XMLTest, " +
         //            "XMLSurvey, " +
         //            "TestVers, " +
         //            "LectEvtID, " +
         //            "SurveyComplete, " +
         //          "VignetteComplete, " +
         //            //"XMLVignette, " +
         //            //"VignetteVersion, " +
         //            //"VignetteScore, " +
         //          "Score, " +
         //            //"Printed_Date, " +
         //            //"Emailed_Date, " +
         //            //"Reported_Date, " +
         //          "View_Date, " +
         //          "UniqueID, " +
         //          "AlterID, " +
         //            "facilityid, " +
         //          "GCCharged " +
         //            "from Test " +
         //            "where UserID = @UserID " +
         //            "and TopicID = @TopicID " +
         //            " and FacilityID = @FacilityID " +
         //            "and Status = 'P' " +
         //            " order by BuyDate DESC";

         //        SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

         //        cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
         //        cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
         //        cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
         //        cn.Open();
         //        IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
         //        if (reader.Read())
         //            return true;
         //        else
         //            return false;
         //    }
         //}

        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new TestInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual TestInfo GetTestFromReader(IDataReader reader)
        {
            return GetTestFromReader(reader, true);
        }
        protected virtual TestInfo GetTestFromReader(IDataReader reader, bool readMemos)
        {
            // NOTE: NULL integer foreign key LectEvtID is replaced with value of
            // zero, which will trigger "Please select..." or "(Not Selected)"
            // message in UI dropdown for that field
            TestInfo Test = new TestInfo(
              (int)reader["TestID"],
              reader["OldTestID"].ToString(),
              (int)reader["TopicID"],
              (int)reader["UserID"],
              (DateTime)reader["BuyDate"],
              (decimal)reader["Credits"],
              (DateTime)reader["LastMod"],
              reader["Name"].ToString(),
              (bool)reader["NoSurvey"],
              reader["Status"].ToString(),
              reader["UserName"].ToString(),
              reader["XMLTest"].ToString(),
              reader["XMLSurvey"].ToString(),
              (int)reader["TestVers"],
              (int)(Convert.IsDBNull(reader["LectEvtID"]) ? (int)0 : (int)reader["LectEvtID"]),
              (bool)reader["SurveyComplete"],
              (bool)reader["VignetteComplete"],
                //reader["XMLVignette"].ToString(),
                //(int)reader["VignetteVersion"],
                //(int)(Convert.IsDBNull(reader["VignetteScore"]) ? (int)0 : (int)reader["VignetteScore"]),
              (int)(Convert.IsDBNull(reader["Score"]) ? (int)0 : (int)reader["Score"]),
                (DateTime)(Convert.IsDBNull(reader["Printed_Date"])
                    ? System.DateTime.MinValue : (DateTime)reader["Printed_Date"]),
                (DateTime)(Convert.IsDBNull(reader["Emailed_Date"])
                    ? System.DateTime.MinValue : (DateTime)reader["Emailed_Date"]),
                (DateTime)(Convert.IsDBNull(reader["Reported_Date"])
                ? System.DateTime.MinValue : (DateTime)reader["Reported_Date"]),
              (DateTime)(Convert.IsDBNull(reader["View_Date"])
                  ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["View_Date"]),
              (int)(Convert.IsDBNull(reader["UniqueID"]) ? (int)0 : (int)reader["UniqueID"]),
              (int)(Convert.IsDBNull(reader["AlterID"]) ? (int)0 : (int)reader["AlterID"]),
              (bool)reader["GCCharged"],(int)reader["facilityid"]);

            return Test;
        }

        /// <summary>
        /// Returns a collection of TestInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<TestInfo> GetTestCollectionFromReader(IDataReader reader)
        {
            return GetTestCollectionFromReader(reader, true);
        }
        protected virtual List<TestInfo> GetTestCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TestInfo> Tests = new List<TestInfo>();
            while (reader.Read())
                Tests.Add(GetTestFromReader(reader, readMemos));
            return Tests;
        }
        #endregion
    }
}
#endregion