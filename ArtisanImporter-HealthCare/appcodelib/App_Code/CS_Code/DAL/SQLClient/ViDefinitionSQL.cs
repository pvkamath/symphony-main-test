﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region ViDefinitionInfo

namespace PearlsReview.DAL
{
    public class ViDefinitionInfo
    {
        public ViDefinitionInfo() { }

        public ViDefinitionInfo(int ViDefID, int TopicID, string XMLTest, string Vignette_Desc, int Version)
        {
            this.ViDefID = ViDefID;
            this.TopicID = TopicID;
            this.XMLTest = XMLTest;
            this.Vignette_Desc = Vignette_Desc;
            this.Version = Version;
        }

        private int _ViDefID = 0;
        public int ViDefID
        {
            get { return _ViDefID; }
            protected set { _ViDefID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

        private string _XMLTest = "";
        public string XMLTest
        {
            get { return _XMLTest; }
            private set { _XMLTest = value; }
        }

        private string _Vignette_Desc = "";
        public string Vignette_Desc
        {
            get { return _Vignette_Desc; }
            private set { _Vignette_Desc = value; }
        }

        private int _Version = 0;
        public int Version
        {
            get { return _Version; }
            private set { _Version = value; }
        }

    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        /////////////////////////////////////////////////////////
        // methods that work with ViDefinitions

        /// <summary>
        /// Returns the total number of ViDefinitions
        /// </summary>
        public int GetViDefinitionCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from ViDefinition", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all ViDefinitions
        /// </summary>
        public List<ViDefinitionInfo> GetViDefinitions(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from ViDefinition";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetViDefinitionCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the ViDefinition with the specified ID
        /// </summary>
        public ViDefinitionInfo GetViDefinitionByID(int ViDefID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from ViDefinition where ViDefID=@ViDefID", cn);
                cmd.Parameters.Add("@ViDefID", SqlDbType.Int).Value = ViDefID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetViDefinitionFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the ViDefinition associated with the specified TopicID
        /// </summary>
        public ViDefinitionInfo GetViDefinitionByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from ViDefinition where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetViDefinitionFromReader(reader, true);
                else
                    return null;
            }
        }



        /// <summary>
        /// Deletes a ViDefinition
        /// </summary>
        public bool DeleteViDefinition(int ViDefID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from ViDefinition where ViDefID=@ViDefID", cn);
                cmd.Parameters.Add("@ViDefID", SqlDbType.Int).Value = ViDefID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new ViDefinition
        /// </summary>
        public int InsertViDefinition(ViDefinitionInfo ViDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into ViDefinition " +
              "(TopicID, " +
              "XMLTest, " +
              "Vignette_Desc, " +
              "Version) " +
              "VALUES (" +
              "@TopicID, " +
              "@XMLTest, " +
              "@Vignette_Desc, " +
              "@Version) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = ViDefinition.TopicID;
                cmd.Parameters.Add("@XMLTest", SqlDbType.NText).Value = ViDefinition.XMLTest;
                cmd.Parameters.Add("@Vignette_Desc", SqlDbType.Text).Value = ViDefinition.Vignette_Desc;
                cmd.Parameters.Add("@Version", SqlDbType.Int).Value = ViDefinition.Version;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a ViDefinition
        /// </summary>
        public bool UpdateViDefinition(ViDefinitionInfo ViDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update ViDefinition set " +
              "TopicID = @TopicID, " +
              "XMLTest = @XMLTest, " +
              "Vignette_Desc = @Vignette_Desc, " +
              "Version = @Version " +
              "where ViDefID = @ViDefID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = ViDefinition.TopicID;
                cmd.Parameters.Add("@XMLTest", SqlDbType.NText).Value = ViDefinition.XMLTest;
                cmd.Parameters.Add("@Vignette_Desc", SqlDbType.Text).Value = ViDefinition.Vignette_Desc;
                cmd.Parameters.Add("@Version", SqlDbType.Int).Value = ViDefinition.Version;
                cmd.Parameters.Add("@ViDefID", SqlDbType.Int).Value = ViDefinition.ViDefID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        #endregion

        #region PRProvider

        /////////////////////////////////////////////////////////
        // methods that work with ViDefinitions
      
        /// <summary>
        /// Returns a new ViDefinitionInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual ViDefinitionInfo GetViDefinitionFromReader(IDataReader reader)
        {
            return GetViDefinitionFromReader(reader, true);
        }
        protected virtual ViDefinitionInfo GetViDefinitionFromReader(IDataReader reader, bool readMemos)
        {
            ViDefinitionInfo ViDefinition = new ViDefinitionInfo(
              (int)reader["ViDefID"],
              (int)reader["TopicID"],
              reader["XMLTest"].ToString(),
              reader["Vignette_Desc"].ToString(),
              (int)reader["Version"]);


            return ViDefinition;
        }

        /// <summary>
        /// Returns a collection of ViDefinitionInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<ViDefinitionInfo> GetViDefinitionCollectionFromReader(IDataReader reader)
        {
            return GetViDefinitionCollectionFromReader(reader, true);
        }
        protected virtual List<ViDefinitionInfo> GetViDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<ViDefinitionInfo> ViDefinitions = new List<ViDefinitionInfo>();
            while (reader.Read())
                ViDefinitions.Add(GetViDefinitionFromReader(reader, readMemos));
            return ViDefinitions;
        }
        #endregion
    }
}
        #endregion