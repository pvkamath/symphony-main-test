﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region UsersInRolesInfo


namespace PearlsReview.DAL
{
    public class UsersInRolesInfo
    {
        public UsersInRolesInfo() { }

        public UsersInRolesInfo(int UserRoleID, string RoleName, string UserName, int FacilityId)
        {
            this.UserRoleID = UserRoleID;
            this.RoleName = RoleName;
            this.UserName = UserName.ToLower();
            this.FacilityId = FacilityId;
        }

        private int _UserRoleID = 0;
        public int UserRoleID
        {
            get { return _UserRoleID; }
            protected set { _UserRoleID = value; }
        }

        private string _RoleName = "";
        public string RoleName
        {
            get { return _RoleName; }
            private set { _RoleName = value; }
        }

        private string _UserName = "";
        public string UserName
        {
            get { return _UserName.ToLower(); }
            private set { _UserName = value.ToLower(); }
        }
        private int _FacilityId = 0;
        public int FacilityId
        {
            get { return _FacilityId; }
            private set { _FacilityId = value; }
        }

    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLProvider

        /////////////////////////////////////////////////////////
        // methods that work with UsersInRoles

        /// <summary>
        /// Returns the total number of UsersInRoles
        /// </summary>
        public int GetUsersInRolesCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from UsersInRoles", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all UsersInRoles
        /// </summary>
        public List<UsersInRolesInfo> GetUsersInRoles(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from UsersInRoles";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetUsersInRolesCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the UsersInRoles with the specified ID
        /// </summary>
        public UsersInRolesInfo GetUsersInRolesByID(int UserRoleID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from UsersInRoles where UserRoleID=@UserRoleID", cn);
                cmd.Parameters.Add("@UserRoleID", SqlDbType.Int).Value = UserRoleID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUsersInRolesFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Returns a UsersInRoles object with the specified UserName
        /// </summary>
        public UsersInRolesInfo GetUsersInRolesByUserName(string UserName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from UsersInRoles where username=@UserName", cn);
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUsersInRolesFromReader(reader, true);
                else
                    return null;
            }
            
        }

        /// <summary>
        /// Deletes a UsersInRoles
        /// </summary>
        public bool DeleteUsersInRoles(int UserRoleID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from UsersInRoles where UserRoleID=@UserRoleID", cn);
                cmd.Parameters.Add("@UserRoleID", SqlDbType.Int).Value = UserRoleID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a UsersInRoles by username
        /// </summary>
        public bool DeleteUsersInRolesByUserName(string UserName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from UsersInRoles where UserName=@UserName", cn);
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName.ToLower();
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new UsersInRoles
        /// </summary>
        public int InsertUsersInRoles(UsersInRolesInfo UsersInRoles)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into UsersInRoles " +
              "(RoleName, " +
              "UserName," +
                "FacilityId ) " +
              "VALUES (" +
              "@RoleName, " +
              "@UserName, " +
                "@FacilityId ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@RoleName", SqlDbType.VarChar).Value = UsersInRoles.RoleName;
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UsersInRoles.UserName.ToLower();
                cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = UsersInRoles.FacilityId;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a UsersInRoles
        /// </summary>
        public bool UpdateUsersInRoles(UsersInRolesInfo UsersInRoles)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UsersInRoles set " +
              "RoleName = @RoleName, " +
              "UserName = @UserName, " +
              "FacilityID = @facilityID " +
              "where UserRoleID = @UserRoleID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@RoleName", SqlDbType.VarChar).Value = UsersInRoles.RoleName;
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UsersInRoles.UserName.ToLower();
                cmd.Parameters.Add("@UserRoleID", SqlDbType.Int).Value = UsersInRoles.UserRoleID;
                cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = UsersInRoles.FacilityId;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider

        /////////////////////////////////////////////////////////
        // methods that work with UsersInRoles
       
        /// <summary>
        /// Returns a new UsersInRolesInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual UsersInRolesInfo GetUsersInRolesFromReader(IDataReader reader)
        {
            return GetUsersInRolesFromReader(reader, true);
        }
        protected virtual UsersInRolesInfo GetUsersInRolesFromReader(IDataReader reader, bool readMemos)
        {
            UsersInRolesInfo UsersInRoles = new UsersInRolesInfo(
              (int)reader["UserRoleID"],
              reader["RoleName"].ToString(),
              reader["UserName"].ToString().ToLower(),
              (int)reader["FacilityId"]);


            return UsersInRoles;
        }

        /// <summary>
        /// Returns a collection of UsersInRolesInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<UsersInRolesInfo> GetUsersInRolesCollectionFromReader(IDataReader reader)
        {
            return GetUsersInRolesCollectionFromReader(reader, true);
        }
        protected virtual List<UsersInRolesInfo> GetUsersInRolesCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<UsersInRolesInfo> UsersInRoles = new List<UsersInRolesInfo>();
            while (reader.Read())
                UsersInRoles.Add(GetUsersInRolesFromReader(reader, readMemos));
            return UsersInRoles;
        }
        #endregion
    }
}
#endregion