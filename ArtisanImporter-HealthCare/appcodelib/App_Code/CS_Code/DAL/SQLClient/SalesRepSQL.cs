﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region SalesRepInfo

namespace PearlsReview.DAL
{
    public class SalesRepInfo
    {
        public SalesRepInfo() { }

        public SalesRepInfo(int RepID, string RepCode, string RepName, string RepComment)
        {
            this.RepID = RepID;
            this.RepCode = RepCode;
            this.RepName = RepName;
            this.RepComment = RepComment;
        }

        private int _RepID = 0;
        public int RepID
        {
            get { return _RepID; }
            protected set { _RepID = value; }
        }

        private string _RepCode = "";
        public string RepCode
        {
            get { return _RepCode; }
            private set { _RepCode = value; }
        }

        private string _RepName = "";
        public string RepName
        {
            get { return _RepName; }
            private set { _RepName = value; }
        }

        private string _RepComment = "";
        public string RepComment
        {
            get { return _RepComment; }
            private set { _RepComment = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with SalesReps

        /// <summary>
        /// Returns the total number of SalesReps
        /// </summary>
        public  int GetSalesRepCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from SalesRep", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all SalesReps
        /// </summary>
        public  List<SalesRepInfo> GetSalesReps(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from SalesRep";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetSalesRepCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the SalesRep with the specified ID
        /// </summary>
        public  SalesRepInfo GetSalesRepByID(int RepID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from SalesRep where RepID=@RepID", cn);
                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = RepID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSalesRepFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a SalesRep
        /// </summary>
        public  bool DeleteSalesRep(int RepID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from SalesRep where RepID=@RepID", cn);
                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = RepID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new SalesRep
        /// </summary>
        public  int InsertSalesRep(SalesRepInfo SalesRep)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into SalesRep " +
              "(RepCode, " +
              "RepName, " +
              "RepComment) " +
              "VALUES (" +
              "@RepCode, " +
              "@RepName, " +
              "@RepComment) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@RepCode", SqlDbType.VarChar).Value = SalesRep.RepCode;
                cmd.Parameters.Add("@RepName", SqlDbType.VarChar).Value = SalesRep.RepName;
                cmd.Parameters.Add("@RepComment", SqlDbType.VarChar).Value = SalesRep.RepComment;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a SalesRep
        /// </summary>
        public  bool UpdateSalesRep(SalesRepInfo SalesRep)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update SalesRep set " +
              "RepCode = @RepCode, " +
              "RepName = @RepName, " +
              "RepComment = @RepComment " +
              "where RepID = @RepID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@RepCode", SqlDbType.VarChar).Value = SalesRep.RepCode;
                cmd.Parameters.Add("@RepName", SqlDbType.VarChar).Value = SalesRep.RepName;
                cmd.Parameters.Add("@RepComment", SqlDbType.VarChar).Value = SalesRep.RepComment;
                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = SalesRep.RepID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new SalesRepInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual SalesRepInfo GetSalesRepFromReader(IDataReader reader)
        {
            return GetSalesRepFromReader(reader, true);
        }
        protected virtual SalesRepInfo GetSalesRepFromReader(IDataReader reader, bool readMemos)
        {
            SalesRepInfo SalesRep = new SalesRepInfo(
              (int)reader["RepID"],
              reader["RepCode"].ToString(),
              reader["RepName"].ToString(),
              reader["RepComment"].ToString());


            return SalesRep;
        }

        /// <summary>
        /// Returns a collection of SalesRepInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<SalesRepInfo> GetSalesRepCollectionFromReader(IDataReader reader)
        {
            return GetSalesRepCollectionFromReader(reader, true);
        }
        protected virtual List<SalesRepInfo> GetSalesRepCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<SalesRepInfo> SalesReps = new List<SalesRepInfo>();
            while (reader.Read())
                SalesReps.Add(GetSalesRepFromReader(reader, readMemos));
            return SalesReps;
        }
        #endregion
    }
}
#endregion