﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region NewGiftcardInfo

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for NewGiftcardInfo
    /// </summary>
    public class NewGiftcardInfo
    {
        public NewGiftcardInfo()
        {
            //
            // TODO: Add constructor logic here
            //

        }

        public NewGiftcardInfo(int GcID, string GcNumber, int GiftCardCHour,
               int FacilityID, string FacilityName)
        {
            this.GcID = GcID;
            this.GcNumber = GcNumber;
            this.GiftCardCHour = GiftCardCHour;
            this.FacilityID = FacilityID;
            this.FacilityName = FacilityName;

        }

        private int _GcID = 0;
        public int GcID
        {
            get { return _GcID; }
            protected set { _GcID = value; }
        }

        private string _GcNumber = "";

        public string GcNumber
        {
            get { return _GcNumber; }
            set { _GcNumber = value; }
        }

        private int _GiftCardCHour = 0;

        public int GiftCardCHour
        {
            get { return _GiftCardCHour; }
            set { _GiftCardCHour = value; }
        }

        private int _FacilityID = 0;

        public int FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }

        private string _FacilityName = "";

        public string FacilityName
        {
            get { return _FacilityName; }
            set { _FacilityName = value; }
        }

    }

}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        public  int InsertNewGiftcard(NewGiftcardInfo NewGiftcard)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into NewGiftcard " +
             "(gcnumber, " +
             "giftcard_chour, " +
             "facilityid, " +
             "facname) " +
             "VALUES (" +
             "@GcNumber, " +
             "@GiftCardCHour, " +
             "@FacilityID, " +
             "@FacilityName) SET @ID = SCOPE_IDENTITY()", cn);




                cmd.Parameters.Add("@GcNumber", SqlDbType.Int).Value = NewGiftcard.GcNumber;
                cmd.Parameters.Add("@GiftCardCHour", SqlDbType.VarChar).Value = NewGiftcard.GiftCardCHour;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = NewGiftcard.FacilityID;
                cmd.Parameters.Add("@FacilityName", SqlDbType.VarChar).Value = NewGiftcard.FacilityName;

                cn.Open();

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }


        /// <summary>
        /// Retrieves all  Giftcard Info for the specified gift card id
        /// </summary>
        public  List<NewGiftcardInfo> GetNewGiftcardByGcNumber(string GcNumber)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "gcid, " +
                    "gcnumber, " +
                    "giftcard_chour, " +
                    "facilityid, " +
                    "facname " +
                    "from NewGiftcard " +
                    "where gcnumber = @GcNumber " +
                    "order by gcnumber DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@GcNumber", SqlDbType.VarChar).Value = GcNumber;
                cn.Open();
                return GetNewGiftcardCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        #endregion

        #region PRProvider

        protected virtual List<NewGiftcardInfo> GetNewGiftcardCollectionFromReader(IDataReader reader)
        {
            return GetNewGiftcardCollectionFromReader(reader, true);
        }
        protected virtual List<NewGiftcardInfo> GetNewGiftcardCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<NewGiftcardInfo> NewGiftcards = new List<NewGiftcardInfo>();
            while (reader.Read())
                NewGiftcards.Add(GetNewGiftcardFromReader(reader, readMemos));
            return NewGiftcards;
        }
        protected virtual NewGiftcardInfo GetNewGiftcardFromReader(IDataReader reader)
        {
            return GetNewGiftcardFromReader(reader, true);
        }

        protected virtual NewGiftcardInfo GetNewGiftcardFromReader(IDataReader reader, bool readMemos)
        {
            // NOTE: NULL integer foreign key LectEvtID is replaced with value of
            // zero, which will trigger "Please select..." or "(Not Selected)"
            // message in UI dropdown for that field
            NewGiftcardInfo NewGiftcard = new NewGiftcardInfo(
              (int)reader["GcID"],
              reader["GcNumber"].ToString(),
              (int)reader["GiftCard_CHour"],
              (int)reader["FacilityID"],
              reader["FacName"].ToString());

            return NewGiftcard;
        }
        #endregion
    }
}
#endregion