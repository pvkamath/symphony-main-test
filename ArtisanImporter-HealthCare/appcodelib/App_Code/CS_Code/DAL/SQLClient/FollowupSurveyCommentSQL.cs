﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region FollowupSurveyCommentInfo

namespace PearlsReview.DAL
{
public class FollowupSurveyCommentInfo
{
	public FollowupSurveyCommentInfo()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public FollowupSurveyCommentInfo(int fsID, int topicID, int testID, int qNumber, DateTime surveyDate, string comment) 
        {
            this.fsID = fsID;
            this.TopicID = topicID;
            this.TestID = testID;
            this.QNumber = qNumber;           
            this.SurveyDate = surveyDate;
            this.Comment = comment;
        }

        private int _fsID = 0;
        public int fsID
        {
            get { return _fsID; }
            set { _fsID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _TestID = 0;
        public int TestID
        {
            get { return _TestID; }
            set { _TestID = value; }
        }

        private int _QNumber = 0;
        public int QNumber
        {
            get { return _QNumber; }
            set { _QNumber = value; }
        }

        

        private DateTime _SurveyDate = DateTime.Now;
        public DateTime SurveyDate
        {
            get { return _SurveyDate; }
            set { _SurveyDate = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }
    }
}

#endregion FollowupSurveyCommentInfo

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        public int InsertFollowupSurveyComment(int topicID, int testID, DateTime surveyDate, int qNumber, string comment)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into FollowupSurveyComment " +
                    "(TopicID, " +
                    "TestID, " +
                    "SurveyDate, " +                    
                    "QNumber, " + 
                    "Comment) " +
                    "VALUES (" +
                    "@TopicID, " +
                    "@TestID, " +
                    "@SurveyDate, " +                    
                    "@QNumber, " +
                    "@Comment) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = topicID;
                cmd.Parameters.Add("@TestID", SqlDbType.Int).Value = testID;
                cmd.Parameters.Add("@SurveyDate", SqlDbType.DateTime).Value = surveyDate;                
                cmd.Parameters.Add("@QNumber", SqlDbType.Int).Value = qNumber;
                cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = comment;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;
            }
        }
        /// <summary>
        /// Delete FollowupSurvey Comment By TestId
        /// </summary>
        public bool DeleteFollowupSurveyCommentByTestId(int TestId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from FollowupSurveyComment where TestId=@TestId", cn);
                cmd.Parameters.Add("@TestId", SqlDbType.Int).Value = TestId;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
    }
}

#endregion SQLPRProvider and PRProvider
