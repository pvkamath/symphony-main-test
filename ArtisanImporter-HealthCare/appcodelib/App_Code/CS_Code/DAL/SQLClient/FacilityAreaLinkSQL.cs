﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;


#region FacilityAreaLinkInfo

namespace PearlsReview.DAL
{
    public class FacilityAreaLinkInfo
    {
        public FacilityAreaLinkInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        private int _areaid = 0;
        public int areaid
        {
            get { return _areaid; }
            protected set { _areaid = value; }
        }

        private int _facilityid = 0;
        public int facilityid
        {
            get { return _facilityid; }
            protected set { _facilityid = value; }
        }

        public FacilityAreaLinkInfo(int facilityid, int areaid)
        {
            this.areaid = areaid;
            this.facilityid = facilityid;
        }

    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider


        /// <summary>
        /// Retrieves all  facilityarealink Info for the specified facility id
        /// </summary>
        public List<FacilityAreaLinkInfo> GetFacilityAreaLinkByFacilityId(int facilityid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "facilityid, " +
                    "areaid " +
                    "from FacilityAreaLink " +
                    "where facilityid = @FacilityID " +
                    "order by areaid DESC";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@FacilityID", SqlDbType.VarChar).Value = facilityid;
                cn.Open();
                return GetFacilityAreaLinkCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        public int UpdateFacilityAreaLink(FacilityAreaLinkInfo FacilityAreaLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into FacilityAreaLink " +
                    "(facilityid, " +
                    "areaid )" +
                    "VALUES (" +
                    "@FacilityId, " +
                    "@AreaId)", cn);
                cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = FacilityAreaLink.facilityid;
                cmd.Parameters.Add("@AreaId", SqlDbType.Int).Value = FacilityAreaLink.areaid;

                //SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                //IDParameter.Direction = ParameterDirection.Output;
                //cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                //int NewID = (int)IDParameter.Value;
                return ret;


            }

            throw new NotImplementedException();
        }


        public bool DeleteFacilityAreaLink(int facilityid, int areaid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from facilityarealink where facilityid=@FacilityId And areaid=@AreaId", cn);
                cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = facilityid;
                cmd.Parameters.Add("@AreaId", SqlDbType.Int).Value = areaid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider

        //////////////////////////////////////////////////////////
        // methods that work with FacilityAreaLink

     
        protected virtual List<FacilityAreaLinkInfo> GetFacilityAreaLinkCollectionFromReader(IDataReader reader)
        {
            return GetFacilityAreaLinkCollectionFromReader(reader, true);
        }
        protected virtual List<FacilityAreaLinkInfo> GetFacilityAreaLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<FacilityAreaLinkInfo> FacilityAreaLinks = new List<FacilityAreaLinkInfo>();
            while (reader.Read())
                FacilityAreaLinks.Add(GetFacilityAreaLinkFromReader(reader, readMemos));
            return FacilityAreaLinks;
        }
        protected virtual FacilityAreaLinkInfo GetFacilityAreaLinkFromReader(IDataReader reader)
        {
            return GetFacilityAreaLinkFromReader(reader, true);
        }
       
        protected virtual FacilityAreaLinkInfo GetFacilityAreaLinkFromReader(IDataReader reader, bool readMemos)
        {
            // NOTE: NULL integer foreign key LectEvtID is replaced with value of
            // zero, which will trigger "Please select..." or "(Not Selected)"
            // message in UI dropdown for that field
            FacilityAreaLinkInfo FacilityAreaLink = new FacilityAreaLinkInfo(
              (int)reader["facilityid"],
              (int)reader["areaid"]);

            return FacilityAreaLink;
        }
        #endregion
    }
}
#endregion
