﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region TopicDetailInfo

namespace PearlsReview.DAL
{
    public class TopicDetailInfo
    {
        public TopicDetailInfo() { }

        public TopicDetailInfo(int TopicID, string Type, string Data,
            string CorrectAns, int Order, string Discussion)
        {
            this.TopicID = TopicID;
            this.Type = Type;
            this.Data = Data;
            this.CorrectAns = CorrectAns;
            this.Order = Order;
            this.Discussion = Discussion;
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            protected set { _TopicID = value; }
        }

        private string _Type = "";
        public string Type
        {
            get { return _Type; }
            private set { _Type = value; }
        }

        private string _Data = "";
        public string Data
        {
            get { return _Data; }
            private set { _Data = value; }
        }

        private string _CorrectAns = "";
        public string CorrectAns
        {
            get { return _CorrectAns; }
            private set { _CorrectAns = value; }
        }

        private int _Order = 0;
        public int Order
        {
            get { return _Order; }
            private set { _Order = value; }
        }

        private string _Discussion = "";
        public string Discussion
        {
            get { return _Discussion; }
            private set { _Discussion = value; }
        }
    }
}
#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        ////////////////////////////////////////////////////
        // ADDED AT THE BOTTOM FROM AddToVFP.txt////////////
        ////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////
        // methods that work with TopicDetails

        /// <summary>
        /// Returns the total number of topics
        /// </summary>
        public int GetTopicDetailCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from coursedetail", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of TopicDetails for the specified TopicID
        /// </summary>
        public int GetTopicDetailCountByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from coursedetail " +
                    "where TopicID = @TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all TopicDetails for the specified TopicID
        /// </summary>
        public List<TopicDetailInfo> GetTopicDetailsByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "TopicID, " +
                    "Type, " +
                    "Data, " +
                    "CorrectAns, " +
                    "Order, " +
                    "Discussion " +
                    "from coursedetail " +
                    "where TopicID = @TopicID";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return GetTopicDetailCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Deletes all TopicDetail records for a specific TopicID
        /// </summary>
        public bool DeleteTopicDetailsByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from coursedetail where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new TopicDetail
        /// </summary>
        public bool InsertTopicDetail(TopicDetailInfo TopicDetail)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into coursedetail " +
                    "(TopicID, " +
                    "Type, " +
                    "Data, " +
                    "CorrectAns, " +
                    "Order, " +
                    "Discussion) " +
                    "VALUES (" +
                    "@TopicID, " +
                    "@Type, " +
                    "@Data, " +
                    "@CorrectAns, " +
                    "@Order, " +
                    "@Discussion)", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicDetail.TopicID;
                cmd.Parameters.Add("@Type", SqlDbType.VarChar).Value = TopicDetail.Type;
                cmd.Parameters.Add("@Data", SqlDbType.VarChar).Value = TopicDetail.Data;
                cmd.Parameters.Add("@CorrectAns", SqlDbType.VarChar).Value = TopicDetail.CorrectAns;
                cmd.Parameters.Add("@Order", SqlDbType.VarChar).Value = TopicDetail.Order;
                cmd.Parameters.Add("@Discussion", SqlDbType.Int).Value = TopicDetail.Discussion;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);

            }
        }

        /// <summary>
        /// Inserts a set of new TopicDetail records
        /// </summary>
        public bool InsertTopicDetailsFromTopicDetailList(
                    List<TopicDetailInfo> TopicDetails)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                int ret = 0;
                cn.Open();

                foreach (TopicDetailInfo record in TopicDetails)
                {
                    SqlCommand cmd = new SqlCommand(
                      "insert into coursedetail " +
                      "(TopicID, " +
                      "Type, " +
                      "Data, " +
                      "CorrectAns, " +
                      "Order, " +
                      "Discussion) " +
                      "VALUES (" +
                      "@TopicID, " +
                      "@Type, " +
                      "@Data, " +
                      "@CorrectAns, " +
                      "@Order, " +
                      "@Discussion)", cn);

                    cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = record.TopicID;
                    cmd.Parameters.Add("@Type", SqlDbType.VarChar).Value = record.Type;
                    cmd.Parameters.Add("@Data", SqlDbType.VarChar).Value = record.Data;
                    cmd.Parameters.Add("@CorrectAns", SqlDbType.VarChar).Value = record.CorrectAns;
                    cmd.Parameters.Add("@Order", SqlDbType.Int).Value = record.Order;
                    cmd.Parameters.Add("@Discussion", SqlDbType.VarChar).Value = record.Discussion;
                    ret = ExecuteNonQuery(cmd);

                    cmd = null;

                }
                return (ret == 1);

            }

        }
        #endregion

        #region PRProvider

        ////////////////////////////////////////////////////
        // ADDED AT THE BOTTOM FROM AddToProv.txt///////////
        ////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////
        // methods that work with TopicDetails
       
        /// <summary>
        /// Returns a new TopicDetailInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual TopicDetailInfo GetTopicDetailFromReader(IDataReader reader)
        {
            return GetTopicDetailFromReader(reader, true);
        }
        protected virtual TopicDetailInfo GetTopicDetailFromReader(IDataReader reader, bool readMemos)
        {
            TopicDetailInfo TopicDetail = new TopicDetailInfo(
             (int)reader["TopicID"],
             reader["Type"].ToString(),
             reader["Data"].ToString(),
             reader["CorrectAns"].ToString(),
             (int)reader["Order"],
             reader["Discussion"].ToString());

            //         if (readMemos)
            //            article.Body = reader["Body"].ToString();

            return TopicDetail;
        }

        /// <summary>
        /// Returns a collection of TopicDetailInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<TopicDetailInfo> GetTopicDetailCollectionFromReader(IDataReader reader)
        {
            return GetTopicDetailCollectionFromReader(reader, true);
        }
        protected virtual List<TopicDetailInfo> GetTopicDetailCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TopicDetailInfo> TopicDetails = new List<TopicDetailInfo>();
            while (reader.Read())
                TopicDetails.Add(GetTopicDetailFromReader(reader, readMemos));
            return TopicDetails;
        }
        #endregion
    }
}
#endregion