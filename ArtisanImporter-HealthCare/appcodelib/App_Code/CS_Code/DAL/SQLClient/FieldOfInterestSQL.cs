﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region FieldOfInterestInfo

namespace PearlsReview.DAL
{
    public class FieldOfInterestInfo
    {
        public FieldOfInterestInfo() { }

        public FieldOfInterestInfo(int InterestID, string IntDescr)
        {
            this.InterestID = InterestID;
            this.IntDescr = IntDescr;
        }

        private int _InterestID = 0;
        public int InterestID
        {
            get { return _InterestID; }
            protected set { _InterestID = value; }
        }

        private string _IntDescr = "";
        public string IntDescr
        {
            get { return _IntDescr; }
            private set { _IntDescr = value; }
        }


    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with FieldsOfInterest

        /// <summary>
        /// Returns the total number of FieldsOfInterest
        /// </summary>
        public  int GetFieldOfInterestCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from FieldOfInterest", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all FieldsOfInterest
        /// </summary>
        public  List<FieldOfInterestInfo> GetFieldsOfInterest(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from FieldOfInterest where alterid is not null ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by intDescr";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetFieldOfInterestCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the FieldOfInterest with the specified ID
        /// </summary>
        public  FieldOfInterestInfo GetFieldOfInterestByID(int InterestID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from FieldOfInterest where InterestID=@InterestID", cn);
                cmd.Parameters.Add("@InterestID", SqlDbType.Int).Value = InterestID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetFieldOfInterestFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves a list of FieldsOfInterest with the specified UserID
        /// </summary>
        public  List<FieldOfInterestInfo> GetFieldsOfInterestByUserID(int UserID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from FieldOfInterest " +
                    "join UserInterestLink on FieldOfinterest.InterestID = UserInterestLink.InterestID " +
                    "where UserInterestLink.UserID = @UserID";


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by intDescr";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

                cn.Open();
                return GetFieldOfInterestCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Deletes a FieldOfInterest
        /// </summary>
        public  bool DeleteFieldOfInterest(int InterestID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from FieldOfInterest where InterestID=@InterestID", cn);
                cmd.Parameters.Add("@InterestID", SqlDbType.Int).Value = InterestID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new FieldOfInterest
        /// </summary>
        public  int InsertFieldOfInterest(FieldOfInterestInfo FieldOfInterest)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into FieldOfInterest " +
              "(IntDescr) " +
              "VALUES (" +
              "@IntDescr) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@IntDescr", SqlDbType.VarChar).Value = FieldOfInterest.IntDescr;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a FieldOfInterest
        /// </summary>
        public  bool UpdateFieldOfInterest(FieldOfInterestInfo FieldOfInterest)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update FieldOfInterest set " +
              "IntDescr = @IntDescr " +
              "where InterestID = @InterestID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@IntDescr", SqlDbType.VarChar).Value = FieldOfInterest.IntDescr;
                cmd.Parameters.Add("@InterestID", SqlDbType.Int).Value = FieldOfInterest.InterestID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        ///// <summary>
        ///// Assign fields of interest to user from comma-delimited list
        ///// </summary>
        //public  bool UpdateUserInterestAssignments(int UserID, string InterestIDAssignments)
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {

        //        // first delete any assignments that are not in the list passed in
        //        //              SqlCommand cmd = new SqlCommand(
        //        //                  "DELETE FROM categorylink " +
        //        //                  "where TopicID = ? and CategoryID NOT IN (?)", cn);

        //        // NOTE; For now, just delete them all
        //        SqlCommand cmd = new SqlCommand(
        //            "DELETE FROM UserInterestLink " +
        //            "where UserID = @UserID ", cn);

        //        cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;

        //        cn.Open();
        //        int ret = ExecuteNonQuery(cmd);
        //        //              return (ret == 1);

        //        if (InterestIDAssignments.Trim().Length < 1)
        //            // no assignments, so nothing else to do
        //            return true;

        //        // now step through all InterestIDs in the list passed in and call
        //        // this.AssignInterestToUser() for each, which will insert the assignment
        //        // if it does not already exist
        //        string[] InterestIDs = InterestIDAssignments.Split(',');

        //        foreach (string cInterestID in InterestIDs)
        //        {
        //            this.AssignInterestToUser(UserID, Int32.Parse(cInterestID));
        //        }

        //        return true;
        //    }
        //}

        /// <summary>
        /// Assign a field of interest to a user
        /// </summary>
        public  bool AssignInterestToUser(int UserID, int InterestID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //SqlCommand cmd = new SqlCommand(
                //  "select Count(*) from categorylink " +
                //"where TopicID = ? and CategoryID = ?", cn);
                //cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                //cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cn.Open();
                //int ResultCount = (int)ExecuteScalar(cmd);

                //if (ResultCount > 1)
                // link already exists, so nothing to do
                //  return true;

                // link does not exist, so insert it
                SqlCommand cmd2 = new SqlCommand("insert into UserInterestLink " +
                    "(UserID, " +
                    "InterestID) " +
                    "VALUES (" +
                    "@UserID, " +
                    "@InterestID)", cn);

                cmd2.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd2.Parameters.Add("@InterestID", SqlDbType.Int).Value = InterestID;

                int ret = ExecuteNonQuery(cmd2);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new FieldOfInterestInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual FieldOfInterestInfo GetFieldOfInterestFromReader(IDataReader reader)
        {
            return GetFieldOfInterestFromReader(reader, true);
        }
        protected virtual FieldOfInterestInfo GetFieldOfInterestFromReader(IDataReader reader, bool readMemos)
        {
            FieldOfInterestInfo FieldOfInterest = new FieldOfInterestInfo(
              (int)reader["InterestID"],
              reader["IntDescr"].ToString());


            return FieldOfInterest;
        }

        /// <summary>
        /// Returns a collection of FieldOfInterestInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<FieldOfInterestInfo> GetFieldOfInterestCollectionFromReader(IDataReader reader)
        {
            return GetFieldOfInterestCollectionFromReader(reader, true);
        }
        protected virtual List<FieldOfInterestInfo> GetFieldOfInterestCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<FieldOfInterestInfo> FieldsOfInterest = new List<FieldOfInterestInfo>();
            while (reader.Read())
                FieldsOfInterest.Add(GetFieldOfInterestFromReader(reader, readMemos));
            return FieldsOfInterest;
        }
        #endregion
    }
}
#endregion