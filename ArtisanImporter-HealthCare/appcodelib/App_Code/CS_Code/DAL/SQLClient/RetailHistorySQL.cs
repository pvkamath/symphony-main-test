﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region RetailHistoryInfo

namespace PearlsReview.DAL
{
    public class RetailHistoryInfo
    {
        public RetailHistoryInfo(int rhid, int facid, DateTime paydate, int seat, decimal payamount, string paystatus, string membertype, string transid, string paycomment, decimal couponamount, int couponid, string cclast)
        {
            this.Rhid = rhid;
            this.FacId = facid;
            this.Paydate = paydate;
            this.Seat = seat;
            this.PayAmount = payamount;
            this.Paystatus = paystatus;
            this.MemberType = membertype;
            this.TransId = transid;
            this.PayComment = paycomment;
            this.CouponAmount = couponamount;
            this.CouponId = couponid;
            this.CCLast = cclast;
        }
        
        private int _rhid = 0;
        public int Rhid
        {
            get { return _rhid; }
            set { _rhid = value; }
        }
        private int _facid = 0;
        public int FacId
        {
            get { return _facid; }
            set { _facid = value; }
        }
        private DateTime _paydate=System.DateTime.MinValue;
        public DateTime Paydate
        {
            get { return _paydate; }
            set { _paydate = value; }
        }
        private int _seat = 0;
        public int Seat
        {
            get { return _seat; }
            set { _seat = value; }
        }
        private decimal _payamount = 0;
        public decimal PayAmount
        {
            get { return _payamount; }
            set { _payamount = value; }
        }
        private string _paystatus = "";
        public string Paystatus
        {
            get { return _paystatus; }
            set { _paystatus = value; }
        }       
        private string _membertype = "";
        public string MemberType
        {
            get { return _membertype; }
            set { _membertype = value; }
        }
        private string _transid = "";
        public string TransId
        {
            get { return _transid; }
            set { _transid = value; }
        }
        private string _paycomment = "";
        public string PayComment
        {
            get { return _paycomment; }
            set { _paycomment = value; }
        }
        private decimal _couponamount = 0;
        public decimal CouponAmount
        {
            get { return _couponamount; }
            set { _couponamount = value; }
        }
        private int _couponid = 0;
        public int CouponId
        {
            get { return _couponid; }
            set { _couponid = value; }
        }
        private string _cclast = "";
        public string CCLast
        {
            get { return _cclast; }
            set { _cclast = value; }
        }
    }

}

#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /// <summary>
        /// Retrieves all RetailHistory by FaciD
        /// </summary>
        public List<RetailHistoryInfo> GetRetailHistoryListByFacID(int facid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from RetailHistory where facid=@facid";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@facid", SqlDbType.Int).Value = facid;
                cn.Open();
                return GetRetailHistoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves all RetailHistory by FaciD
        /// </summary>
        public RetailHistoryInfo GetLastRetailHistoryByFacID(int facid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top 1 * from RetailHistory where facid=@facid order by paydate desc ";             

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@facid", SqlDbType.Int).Value = facid;
                cn.Open();              
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetRetailHistoryFromReader(reader);
                else
                    return null;
            }
        }

        public int insertRetailHistory(RetailHistoryInfo retailhistory)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {              
                SqlCommand cmd = new SqlCommand("insert into RetailHistory " +
                    "(facid, " +
                    "paydate, " +
                    "seat, " +
                    "payamount, " +
                    "paystatus, " +
                    "membertype, " +
                    "transid, " +
                    "paycomment, " +
                    "couponamount, " +
                    "couponid, " +
                    "cclast) " +
                     "VALUES " +
                    "(@facid, " +
                    "@paydate, " +
                    "@seat, " +
                    "@payamount, " +
                    "@paystatus, " +
                    "@membertype, " +
                    "@transid, " +
                    "@paycomment, " +
                    "@couponamount, " +
                    "@couponid, " +
                    "@cclast) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@facid", SqlDbType.Int).Value = retailhistory.FacId;
                cmd.Parameters.Add("@paydate", SqlDbType.DateTime).Value = retailhistory.Paydate;
                cmd.Parameters.Add("@seat", SqlDbType.Int).Value = retailhistory.Seat;
                cmd.Parameters.Add("@payamount", SqlDbType.Decimal).Value = retailhistory.PayAmount;
                cmd.Parameters.Add("@paystatus", SqlDbType.VarChar).Value = retailhistory.Paystatus;
                cmd.Parameters.Add("@membertype", SqlDbType.VarChar).Value = retailhistory.MemberType;
                cmd.Parameters.Add("@transid", SqlDbType.VarChar).Value = retailhistory.TransId;
                cmd.Parameters.Add("@paycomment", SqlDbType.VarChar).Value = retailhistory.PayComment;
                cmd.Parameters.Add("@couponamount", SqlDbType.Decimal).Value = retailhistory.CouponAmount;
                cmd.Parameters.Add("@couponid", SqlDbType.Int).Value = retailhistory.CouponId;
                cmd.Parameters.Add("@cclast", SqlDbType.VarChar).Value = retailhistory.CCLast;                

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                return (int)IDParameter.Value;
            }
        }

        public bool updateRetailHistory(RetailHistoryInfo retailhistory)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update RetailHistory set " +
                    "facid = @facid, " +
                    "paydate = @paydate, " +
                    "seat = @seat, " +
                    "payamount = @payamount, " +
                    "paystatus = @paystatus, " +
                    "membertype = @membertype, " +
                    "transid = @transid, " +
                    "paycomment = @paycomment, " +
                    "paycomment = @paycomment, " +
                    "couponamount = @couponamount, " +
                    "couponid = @couponid, " +
                    "cclast = @cclast " +    
                    "where rhid = @rhid ", cn);

                cmd.Parameters.Add("@facid", SqlDbType.Int).Value = retailhistory.FacId;
                cmd.Parameters.Add("@paydate", SqlDbType.DateTime).Value = retailhistory.Paydate;
                cmd.Parameters.Add("@seat", SqlDbType.Int).Value = retailhistory.Seat;
                cmd.Parameters.Add("@payamount", SqlDbType.Decimal).Value = retailhistory.PayAmount;
                cmd.Parameters.Add("@paystatus", SqlDbType.VarChar).Value = retailhistory.Paystatus;
                cmd.Parameters.Add("@membertype", SqlDbType.VarChar).Value = retailhistory.MemberType;
                cmd.Parameters.Add("@transid", SqlDbType.VarChar).Value = retailhistory.TransId;
                cmd.Parameters.Add("@paycomment", SqlDbType.VarChar).Value = retailhistory.PayComment;
                cmd.Parameters.Add("@couponamount", SqlDbType.Decimal).Value = retailhistory.CouponAmount;
                cmd.Parameters.Add("@couponid", SqlDbType.Int).Value = retailhistory.CouponId;
                cmd.Parameters.Add("@cclast", SqlDbType.VarChar).Value = retailhistory.CCLast;

                cmd.Parameters.Add("@rhid", SqlDbType.Int).Value = retailhistory.Rhid;

                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public bool deleteRetailHistory(int rhid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from RetailHistory where rhid = @rhid", cn);
                cmd.Parameters.Add("@rhid", SqlDbType.Int).Value = rhid;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public RetailHistoryInfo GetRetailHistoryByID(int rhid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From RetailHistory Where rhid = @rhid", cn);
                cmd.Parameters.Add("@rhid", SqlDbType.Int).Value = rhid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetRetailHistoryFromReader(reader);
                else
                    return null;
            }
        }       

        #endregion


        #region PRProvider

        protected virtual RetailHistoryInfo GetRetailHistoryFromReader(IDataReader reader)
        {
            return GetRetailHistoryFromReader(reader, true);
        }
        protected virtual RetailHistoryInfo GetRetailHistoryFromReader(IDataReader reader, bool readMemos)
        {
                RetailHistoryInfo RetailHistory = new RetailHistoryInfo(
                Convert.IsDBNull(reader["rhid"]) ? (int)0 : (int)reader["rhid"],
                Convert.IsDBNull(reader["facid"]) ? (int)0 : (int)reader["facid"],
                Convert.IsDBNull(reader["paydate"]) ? DateTime.MinValue : Convert.ToDateTime(reader["paydate"]),
                Convert.IsDBNull(reader["seat"]) ? (int)0 : (int)reader["seat"],
                Convert.IsDBNull(reader["payamount"]) ? (decimal)0 : (decimal)reader["payamount"],
                Convert.ToString(reader["paystatus"]),
                Convert.ToString(reader["membertype"]),
                Convert.ToString(reader["transid"]),
                Convert.ToString(reader["paycomment"]),
                Convert.IsDBNull(reader["couponamount"]) ? (decimal)0 : (decimal)reader["couponamount"],                
                (int)reader["couponid"],
                reader["cclast"].ToString()
                );
            return RetailHistory;
        }

        protected virtual List<RetailHistoryInfo> GetRetailHistoryCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<RetailHistoryInfo> RetailHistoryList = new List<RetailHistoryInfo>();
            while (reader.Read())
            {
                RetailHistoryList.Add(GetRetailHistoryFromReader(reader, readMemos));
            }
            return RetailHistoryList;
        }

        protected virtual List<RetailHistoryInfo> GetRetailHistoryCollectionFromReader(IDataReader reader)
        {
            return GetRetailHistoryCollectionFromReader(reader, true);
        }

        #endregion

    }
}
#endregion
