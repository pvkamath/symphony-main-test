﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region UserAccountInfo,UserAccountInfo1 and UserEnrollmentInfo

namespace PearlsReview.DAL
{
    public class UserAccountInfo
    {
        public UserAccountInfo() { }

        public UserAccountInfo(int iID, string cAddress1, string cAddress2, bool lAdmin, bool lPrimary, string cBirthDate,
            string cCity, string cEmail, string cExpires, string cFirstName, string cFloridaNo,
            string cInstitute, string cLastName, string cLectDate, string cMiddle, string cPhone,
            string cPromoCode, int PromoID, int FacilityID, string cPW, string cRefCode,
            string cRegType, string cSocial, string cState, string cUserName, string cZipCode,
            int SpecID, string LCUserName, DateTime LastAct, string PassFmt,
            string PassSalt, string MobilePIN, string LCEmail, string PWQuest, string PWAns,
            bool IsApproved, bool IsOnline, bool IsLocked, DateTime LastLogin, DateTime LastPWChg,
            DateTime LastLock, int XPWAtt, DateTime XPWAttSt, int XPWAnsAtt, DateTime XPWAnsSt,
            string Comment, DateTime Created, int AgeGroup, int RegTypeID, int RepID, string Work_Phone,
            string Badge_ID, DateTime Hire_Date, DateTime Termin_Date, int Dept_ID, int Position_ID,
            int Clinical_Ind, string Title, bool Giftcard_Ind, decimal Giftcard_Chour,
            decimal Giftcard_Uhour, int UniqueID, bool FirstLogin_Ind)
        {
            try
            {
                this.iID = iID;
                this.cAddress1 = cAddress1;
                this.cAddress2 = cAddress2;
                this.lAdmin = lAdmin;
                this.lPrimary = lPrimary;
                this.cBirthDate = cBirthDate;
                this.cCity = cCity;
                this.cEmail = cEmail;
                this.cExpires = cExpires;
                this.cFirstName = cFirstName;
                this.cFloridaNo = cFloridaNo;
                this.cInstitute = cInstitute;
                this.cLastName = cLastName;
                this.cLectDate = cLectDate;
                this.cMiddle = cMiddle;
                this.cPhone = cPhone;
                this.cPromoCode = cPromoCode;
                this.PromoID = PromoID;
                this.FacilityID = FacilityID;
                this.cPW = cPW;
                this.cRefCode = cRefCode;
                this.cRegType = cRegType;
                this.cSocial = cSocial;
                this.cState = cState;
                this.cUserName = cUserName;
                this.cZipCode = cZipCode;
                this.SpecID = SpecID;
                this.LCUserName = LCUserName;
                this.LastAct = LastAct;
                this.PassFmt = PassFmt;
                this.PassSalt = PassSalt;
                this.MobilePIN = MobilePIN;
                this.LCEmail = LCEmail;
                this.PWQuest = PWQuest;
                this.PWAns = PWAns;
                this.IsApproved = IsApproved;
                this.IsOnline = IsOnline;
                this.IsLocked = IsLocked;
                this.LastLogin = LastLogin;
                this.LastPWChg = LastPWChg;
                this.LastLock = LastLock;
                this.XPWAtt = XPWAtt;
                this.XPWAttSt = XPWAttSt;
                this.XPWAnsAtt = XPWAnsAtt;
                this.XPWAnsSt = XPWAnsSt;
                this.Comment = Comment;
                this.Created = Created;
                this.AgeGroup = AgeGroup;
                this.RegTypeID = RegTypeID;
                this.RepID = RepID;
                this.Work_Phone = Work_Phone;
                this.Badge_ID = Badge_ID;
                this.Hire_Date = Hire_Date;
                this.Termin_Date = Termin_Date;
                this.Dept_ID = Dept_ID;
                this.Position_ID = Position_ID;
                this.Clinical_Ind = Clinical_Ind;
                this.Title = Title;
                this.Giftcard_Ind = Giftcard_Ind;
                this.Giftcard_Chour = Giftcard_Chour;
                this.Giftcard_Uhour = Giftcard_Uhour;
                this.UniqueID = UniqueID;
                this.FirstLogin_Ind = FirstLogin_Ind;
            }
            catch(Exception ex)
            {

            }
        }

        private int _iID = 0;
        public int iID
        {
            get { return _iID; }
            protected set { _iID = value; }
        }

        private string _cAddress1 = "";
        public string cAddress1
        {
            get { return _cAddress1; }
            private set { _cAddress1 = value; }
        }

        private string _cAddress2 = "";
        public string cAddress2
        {
            get { return _cAddress2; }
            private set { _cAddress2 = value; }
        }

        private bool _lAdmin = false;
        public bool lAdmin
        {
            get { return _lAdmin; }
            private set { _lAdmin = value; }
        }

        private bool _lPrimary = false;
        public bool lPrimary
        {
            get { return _lPrimary; }
            private set { _lPrimary = value; }
        }

        private string _cBirthDate = "";
        public string cBirthDate
        {
            get { return _cBirthDate; }
            private set { _cBirthDate = value; }
        }

        private string _cCity = "";
        public string cCity
        {
            get { return _cCity; }
            private set { _cCity = value; }
        }

        private string _cEmail = "";
        public string cEmail
        {
            get { return _cEmail; }
            private set { _cEmail = value; }
        }

        private string _cExpires = "";
        public string cExpires
        {
            get { return _cExpires; }
            private set { _cExpires = value; }
        }

        private string _cFirstName = "";
        public string cFirstName
        {
            get { return _cFirstName; }
            private set { _cFirstName = value; }
        }

        private string _cFloridaNo = "";
        public string cFloridaNo
        {
            get { return _cFloridaNo; }
            private set { _cFloridaNo = value; }
        }

        private string _cInstitute = "";
        public string cInstitute
        {
            get { return _cInstitute; }
            private set { _cInstitute = value; }
        }

        private string _cLastName = "";
        public string cLastName
        {
            get { return _cLastName; }
            private set { _cLastName = value; }
        }

        private string _cLectDate = "";
        public string cLectDate
        {
            get { return _cLectDate; }
            private set { _cLectDate = value; }
        }

        private string _cMiddle = "";
        public string cMiddle
        {
            get { return _cMiddle; }
            private set { _cMiddle = value; }
        }

        private string _cPhone = "";
        public string cPhone
        {
            get { return _cPhone; }
            private set { _cPhone = value; }
        }

        private string _cPromoCode = "";
        public string cPromoCode
        {
            get { return _cPromoCode; }
            private set { _cPromoCode = value; }
        }

        private int _PromoID = 0;
        public int PromoID
        {
            get { return _PromoID; }
            protected set { _PromoID = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            protected set { _FacilityID = value; }
        }

        private string _cPW = "";
        public string cPW
        {
            get { return _cPW; }
            private set { _cPW = value; }
        }

        private string _cRefCode = "";
        public string cRefCode
        {
            get { return _cRefCode; }
            private set { _cRefCode = value; }
        }

        private string _cRegType = "";
        public string cRegType
        {
            get { return _cRegType; }
            private set { _cRegType = value; }
        }

        private string _cSocial = "";
        public string cSocial
        {
            get { return _cSocial; }
            private set { _cSocial = value; }
        }

        private string _cState = "";
        public string cState
        {
            get { return _cState; }
            private set { _cState = value; }
        }

        private string _cUserName = "";
        public string cUserName
        {
            get { return _cUserName.ToLower(); }
            private set { _cUserName = value.ToLower(); }
        }

        private string _cZipCode = "";
        public string cZipCode
        {
            get { return _cZipCode; }
            private set { _cZipCode = value; }
        }

        private int _SpecID = 0;
        public int SpecID
        {
            get { return _SpecID; }
            private set { _SpecID = value; }
        }

        private string _LCUserName = "";
        public string LCUserName
        {
            get { return _LCUserName.ToLower(); }
            private set { _LCUserName = value.ToLower(); }
        }

        private DateTime _LastAct = System.DateTime.Now;
        public DateTime LastAct
        {
            get { return _LastAct; }
            private set { _LastAct = value; }
        }

        private string _PassFmt = "";
        public string PassFmt
        {
            get { return _PassFmt; }
            private set { _PassFmt = value; }
        }

        private string _PassSalt = "";
        public string PassSalt
        {
            get { return _PassSalt; }
            private set { _PassSalt = value; }
        }

        private string _MobilePIN = "";
        public string MobilePIN
        {
            get { return _MobilePIN; }
            private set { _MobilePIN = value; }
        }

        private string _LCEmail = "";
        public string LCEmail
        {
            get { return _LCEmail.ToLower(); }
            private set { _LCEmail = value.ToLower(); }
        }

        private string _PWQuest = "";
        public string PWQuest
        {
            get { return _PWQuest; }
            private set { _PWQuest = value; }
        }

        private string _PWAns = "";
        public string PWAns
        {
            get { return _PWAns.ToLower(); }
            private set { _PWAns = value.ToLower(); }
        }

        private bool _IsApproved = false;
        public bool IsApproved
        {
            get { return _IsApproved; }
            private set { _IsApproved = value; }
        }

        private bool _IsOnline = false;
        public bool IsOnline
        {
            get { return _IsOnline; }
            private set { _IsOnline = value; }
        }

        private bool _IsLocked = false;
        public bool IsLocked
        {
            get { return _IsLocked; }
            private set { _IsLocked = value; }
        }

        private DateTime _LastLogin = System.DateTime.MinValue;
        public DateTime LastLogin
        {
            get { return _LastLogin; }
            private set { _LastLogin = value; }
        }

        private DateTime _LastPWChg = System.DateTime.MinValue;
        public DateTime LastPWChg
        {
            get { return _LastPWChg; }
            private set { _LastPWChg = value; }
        }

        private DateTime _LastLock = System.DateTime.MinValue;
        public DateTime LastLock
        {
            get { return _LastLock; }
            private set { _LastLock = value; }
        }

        private int _XPWAtt = 0;
        public int XPWAtt
        {
            get { return _XPWAtt; }
            private set { _XPWAtt = value; }
        }

        private DateTime _XPWAttSt = System.DateTime.MinValue;
        public DateTime XPWAttSt
        {
            get { return _XPWAttSt; }
            private set { _XPWAttSt = value; }
        }

        private int _XPWAnsAtt = 0;
        public int XPWAnsAtt
        {
            get { return _XPWAnsAtt; }
            private set { _XPWAnsAtt = value; }
        }

        private DateTime _XPWAnsSt = System.DateTime.MinValue;
        public DateTime XPWAnsSt
        {
            get { return _XPWAnsSt; }
            private set { _XPWAnsSt = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            private set { _Comment = value; }
        }

        private DateTime _Created = System.DateTime.Now;
        public DateTime Created
        {
            get { return _Created; }
            private set { _Created = value; }
        }

        private int _AgeGroup = 0;
        public int AgeGroup
        {
            get { return _AgeGroup; }
            private set { _AgeGroup = value; }
        }

        private int _RegTypeID = 0;
        public int RegTypeID
        {
            get { return _RegTypeID; }
            private set { _RegTypeID = value; }
        }

        private int _RepID = 0;
        public int RepID
        {
            get { return _RepID; }
            private set { _RepID = value; }
        }
        private string _Work_Phone = "";
        public string Work_Phone
        {
            get { return _Work_Phone; }
            set { _Work_Phone = value; }
        }
        private string _Badge_ID = "";
        public string Badge_ID
        {
            get { return _Badge_ID; }
            set { _Badge_ID = value; }
        }

        private DateTime _Hire_Date = System.DateTime.MinValue;
        public DateTime Hire_Date
        {
            get { return _Hire_Date; }
            set { _Hire_Date = value; }
        }

        private DateTime _Termin_Date = System.DateTime.MinValue;
        public DateTime Termin_Date
        {
            get { return _Termin_Date; }
            set { _Termin_Date = value; }
        }

        private int _Dept_ID = 0;
        public int Dept_ID
        {
            get { return _Dept_ID; }
            set { _Dept_ID = value; }
        }

        private int _Position_ID = 0;
        public int Position_ID
        {
            get { return _Position_ID; }
            set { _Position_ID = value; }
        }

        private int _Clinical_Ind = 0;
        public int Clinical_Ind
        {
            get { return _Clinical_Ind; }
            set { _Clinical_Ind = value; }
        }

        private string _Title = "";
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private bool _Giftcard_Ind = false;
        public bool Giftcard_Ind
        {
            get { return _Giftcard_Ind; }
            set { _Giftcard_Ind = value; }
        }

        private decimal _Giftcard_Chour = 0;
        public decimal Giftcard_Chour
        {
            get { return _Giftcard_Chour; }
            set { _Giftcard_Chour = value; }
        }

        private decimal _Giftcard_Uhour = 0;
        public decimal Giftcard_Uhour
        {
            get { return _Giftcard_Uhour; }
            set { _Giftcard_Uhour = value; }
        }

        private int _UniqueID = 0;
        public int UniqueID
        {
            get { return _UniqueID; }
            set { _UniqueID = value; }
        }

        private bool _FirstLogin_Ind = false;
        public bool FirstLogin_Ind
        {
            get { return _FirstLogin_Ind; }
            set { _FirstLogin_Ind = value; }
        }

    }

    ////////////////////////////////////////////////////////////
    /// <summary>
    /// UserAccount1 business object class
    /// </summary>
    
    public class UserAccountInfo1
    {
        public UserAccountInfo1() { }

        public UserAccountInfo1(int iID, string cFirstName, string cLastName)
        {
            this.iID = iID;
            this.cFirstName = cFirstName;
            this.cLastName = cLastName;
        }
        public UserAccountInfo1(int iID, string cUserName, string cFirstName, string cLastName, string facilityName, bool facilityActive, bool IsApproved,
                                string Badge_ID, string DeptName, string roleNames, string cSocial, bool giftcard_ind)
        {
            this.iID = iID;
            this.cUserName = cUserName.ToLower();
            this.cFirstName = cFirstName;
            this.cLastName = cLastName;
            this.FacilityName = facilityName;
            this.FacilityActive = facilityActive;
            this.IsApproved = IsApproved;
            this.Badge_ID = Badge_ID;
            this.DeptName = DeptName;
            this.RoleNames = roleNames;
            this.cSocial = cSocial;
            this.Giftcard_Ind = giftcard_ind;
        }


        public UserAccountInfo1(int iID, string cfirstName, string clastName, string cUserName,
             string cpw, string cAddress1, string cEmail, string license_number, string cCity, string cState, DateTime cexpries, DateTime created, string state, string cZipCode)
        {
            this.iID = iID;
            this.cFirstName = cfirstName;
            this.cLastName = clastName;
            this.cUserName = cUserName;
            this.cPW = cPW;
            this.cAddress1 = cAddress1;
            this.cEmail = cEmail;
            this.license_number = license_number;
            this.cCity = cCity;
            this.cState = cState;
            this.cExpires = cexpries.ToShortDateString();
            this.Created = created;
            this.State = State;
            this.cZipCode = cZipCode;


        }

        public UserAccountInfo1(int iID, string cfirstName, string clastName, string cUserName,
            string cpw, string cAddress1, string cEmail, string license_number, string cCity, string cState, DateTime cexpries, DateTime created, string state,int uniqueid, string cZipCode)
        {
            this.iID = iID;
            this.cFirstName = cfirstName;
            this.cLastName = clastName;
            this.cUserName = cUserName;
            this.cPW = cPW;
            this.cAddress1 = cAddress1;
            this.cEmail = cEmail;
            this.license_number = license_number;
            this.cCity = cCity;
            this.cState = cState;
            this.cExpires = cexpries.ToShortDateString();
            this.Created = created;
            this.State = State;
            this.UniqueID = uniqueid;
            this.cZipCode = cZipCode;


        }

        public UserAccountInfo1(int iID, string cfirstName, string clastName, string cUserName,
           string cpw, string cAddress1, string cEmail, string cCity, string cState, DateTime cexpries, DateTime created, int uniqueid, string cZipCode)
        {
            this.iID = iID;
            this.cFirstName = cfirstName;
            this.cLastName = clastName;
            this.cUserName = cUserName;
            this.cPW = cPW;
            this.cAddress1 = cAddress1;
            this.cEmail = cEmail;
            this.cCity = cCity;
            this.cState = cState;
            this.cExpires = cexpries.ToShortDateString();
            this.Created = created;
            this.cZipCode = cZipCode;
            this.UniqueID = uniqueid;

        }


        public UserAccountInfo1(string cfirstName, string clastName, string cUserName,
                 string cpw, string cAddress1, string cEmail, string license_number, string cState, string state, string cZipCode)
        {
            this.cFirstName = cFirstName;
            this.cLastName = cLastName;
            this.cUserName = cUserName;
            this.cPW = cPW;
            this.cAddress1 = cAddress1;
            this.cEmail = cEmail;
            this.license_number = license_number;
            this.cState = cState;
            this.State = State;
            this.cZipCode = cZipCode;
        }

        public UserAccountInfo1( int _iid, string _cfirstName, string _clastName,
                 string _cuserName, string _cpw, string _caddress1, string _cemail, string _ccity,
                string _cstate, string _czipCode, DateTime  _created, DateTime _cexpires, bool _isApproved)
        {
            this.iID = _iid;
            this.cFirstName = _cfirstName;
            this.cLastName = _clastName;
            this.cUserName = _cuserName;
            this.cPW = _cpw;
            this.cAddress1 = _caddress1;
            this.cEmail = _cemail;
            this.cState = _cstate;
            this.cCity = _ccity;
            this.cZipCode = _czipCode;
            this.Created = _created;
            this.cExpires = _cexpires.ToShortDateString();
            this.IsApproved = _isApproved;
        }

        public UserAccountInfo1(int _iid, string _cfirstName, string _clastName,
         string _cuserName, string _cpw, string _caddress1, string _cemail, string _ccity,
        string _cstate, string _czipCode, DateTime _created, DateTime _cexpires)
        {
            this.iID = _iid;
            this.cFirstName = _cfirstName;
            this.cLastName = _clastName;
            this.cUserName = _cuserName;
            this.cPW = _cpw;
            this.cAddress1 = _caddress1;
            this.cEmail = _cemail;
            this.cState = _cstate;
            this.cCity = _ccity;
            this.cZipCode = _czipCode;
            this.Created = _created;
            this.cExpires = _cexpires.ToShortDateString();
        }

        public UserAccountInfo1(int iID, string cAddress1, string cAddress2, bool lAdmin, bool lPrimary, string cBirthDate,
          string cCity, string cEmail, string cExpires, string cFirstName, string cFloridaNo,
          string cInstitute, string cLastName, string cLectDate, string cMiddle, string cPhone,
          string cPromoCode, int PromoID, int FacilityID, string cPW, string cRefCode,
          string cRegType, string cSocial, string cState, string cUserName, string cZipCode,
          int SpecID, string LCUserName, DateTime LastAct, string PassFmt,
          string PassSalt, string MobilePIN, string LCEmail, string PWQuest, string PWAns,
          bool IsApproved, bool IsOnline, bool IsLocked, DateTime LastLogin, DateTime LastPWChg,
          DateTime LastLock, int XPWAtt, DateTime XPWAttSt, int XPWAnsAtt, DateTime XPWAnsSt,
          string Comment, DateTime Created, int AgeGroup, int RegTypeID, int RepID, string Work_Phone,
          string Badge_ID, DateTime Hire_Date, DateTime Termin_Date, int Dept_ID, int Position_ID,
          int Clinical_Ind, string Title, bool Giftcard_Ind, decimal Giftcard_Chour,
          decimal Giftcard_Uhour, int UniqueID, bool FirstLogin_Ind)
        {
            this.iID = iID;
            this.cAddress1 = cAddress1;
            this.cAddress2 = cAddress2;
            this.lAdmin = lAdmin;
            this.lPrimary = lPrimary;
            this.cBirthDate = cBirthDate;
            this.cCity = cCity;
            this.cEmail = cEmail;
            this.cExpires = cExpires;
            this.cFirstName = cFirstName;
            this.cFloridaNo = cFloridaNo;
            this.cInstitute = cInstitute;
            this.cLastName = cLastName;
            this.cLectDate = cLectDate;
            this.cMiddle = cMiddle;
            this.cPhone = cPhone;
            this.cPromoCode = cPromoCode;
            this.PromoID = PromoID;
            this.FacilityID = FacilityID;
            this.cPW = cPW;
            this.cRefCode = cRefCode;
            this.cRegType = cRegType;
            this.cSocial = cSocial;
            this.cState = cState;
            this.cUserName = cUserName.ToLower();
            this.cZipCode = cZipCode;
            this.SpecID = SpecID;
            this.LCUserName = LCUserName.ToLower();
            this.LastAct = LastAct;
            this.PassFmt = PassFmt;
            this.PassSalt = PassSalt;
            this.MobilePIN = MobilePIN;
            this.LCEmail = LCEmail;
            this.PWQuest = PWQuest;
            this.PWAns = PWAns.ToLower();
            this.IsApproved = IsApproved;
            this.IsOnline = IsOnline;
            this.IsLocked = IsLocked;
            this.LastLogin = LastLogin;
            this.LastPWChg = LastPWChg;
            this.LastLock = LastLock;
            this.XPWAtt = XPWAtt;
            this.XPWAttSt = XPWAttSt;
            this.XPWAnsAtt = XPWAnsAtt;
            this.XPWAnsSt = XPWAnsSt;
            this.Comment = Comment;
            this.Created = Created;
            this.AgeGroup = AgeGroup;
            this.RegTypeID = RegTypeID;
            this.RepID = RepID;
            this.Work_Phone = Work_Phone;
            this.Badge_ID = Badge_ID;
            this.Hire_Date = Hire_Date;
            this.Termin_Date = Termin_Date;
            this.Dept_ID = Dept_ID;
            this.Position_ID = Position_ID;
            this.Clinical_Ind = Clinical_Ind;
            this.Title = Title;
            this.Giftcard_Ind = Giftcard_Ind;
            this.Giftcard_Chour = Giftcard_Chour;
            this.Giftcard_Uhour = Giftcard_Uhour;
            this.UniqueID = UniqueID;
            this.FirstLogin_Ind = FirstLogin_Ind;

        }

        private int _iID = 0;
        public int iID
        {
            get { return _iID; }
            protected set { _iID = value; }
        }

        private string _cAddress1 = "";
        public string cAddress1
        {
            get { return _cAddress1; }
            private set { _cAddress1 = value; }
        }

        private string _cAddress2 = "";
        public string cAddress2
        {
            get { return _cAddress2; }
            private set { _cAddress2 = value; }
        }

        private bool _lAdmin = false;
        public bool lAdmin
        {
            get { return _lAdmin; }
            private set { _lAdmin = value; }
        }

        private bool _lPrimary = false;
        public bool lPrimary
        {
            get { return _lPrimary; }
            private set { _lPrimary = value; }
        }

        private string _cBirthDate = "";
        public string cBirthDate
        {
            get { return _cBirthDate; }
            private set { _cBirthDate = value; }
        }

        private string _cCity = "";
        public string cCity
        {
            get { return _cCity; }
            private set { _cCity = value; }
        }

        private string _cEmail = "";
        public string cEmail
        {
            get { return _cEmail; }
            private set { _cEmail = value; }
        }

        private string _cExpires = "";
        public string cExpires
        {
            get { return _cExpires; }
            private set { _cExpires = value; }
        }

        private string _cFirstName = "";
        public string cFirstName
        {
            get { return _cFirstName; }
            private set { _cFirstName = value; }
        }

        private string _cFloridaNo = "";
        public string cFloridaNo
        {
            get { return _cFloridaNo; }
            private set { _cFloridaNo = value; }
        }

        private string _cInstitute = "";
        public string cInstitute
        {
            get { return _cInstitute; }
            private set { _cInstitute = value; }
        }

        private string _cLastName = "";
        public string cLastName
        {
            get { return _cLastName; }
            private set { _cLastName = value; }
        }

        private string _cLectDate = "";
        public string cLectDate
        {
            get { return _cLectDate; }
            private set { _cLectDate = value; }
        }

        private string _cMiddle = "";
        public string cMiddle
        {
            get { return _cMiddle; }
            private set { _cMiddle = value; }
        }

        private string _cPhone = "";
        public string cPhone
        {
            get { return _cPhone; }
            private set { _cPhone = value; }
        }

        private string _cPromoCode = "";
        public string cPromoCode
        {
            get { return _cPromoCode; }
            private set { _cPromoCode = value; }
        }

        private int _PromoID = 0;
        public int PromoID
        {
            get { return _PromoID; }
            protected set { _PromoID = value; }
        }

        private string _facilityName = "";
        public string FacilityName
        {
            get { return _facilityName; }
            set { _facilityName = value; }
        }
        private bool _facilityActive = false;
        public bool FacilityActive
        {
            get { return _facilityActive; }
            set { _facilityActive = value; }
        }
        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            protected set { _FacilityID = value; }
        }

        private string _cPW = "";
        public string cPW
        {
            get { return _cPW; }
            private set { _cPW = value; }
        }

        private string _cRefCode = "";
        public string cRefCode
        {
            get { return _cRefCode; }
            private set { _cRefCode = value; }
        }

        private string _cRegType = "";
        public string cRegType
        {
            get { return _cRegType; }
            private set { _cRegType = value; }
        }

        private string _cSocial = "";
        public string cSocial
        {
            get { return _cSocial; }
            private set { _cSocial = value; }
        }

        private string _cState = "";
        public string cState
        {
            get { return _cState; }
            private set { _cState = value; }
        }

        private string _cUserName = "";
        public string cUserName
        {
            get { return _cUserName.ToLower(); }
            private set { _cUserName = value.ToLower(); }
        }

        private string _cZipCode = "";
        public string cZipCode
        {
            get { return _cZipCode; }
            private set { _cZipCode = value; }
        }

        private int _SpecID = 0;
        public int SpecID
        {
            get { return _SpecID; }
            private set { _SpecID = value; }
        }

        private string _LCUserName = "";
        public string LCUserName
        {
            get { return _LCUserName.ToLower(); }
            private set { _LCUserName = value.ToLower(); }
        }

        private DateTime _LastAct = System.DateTime.Now;
        public DateTime LastAct
        {
            get { return _LastAct; }
            private set { _LastAct = value; }
        }

        private string _PassFmt = "";
        public string PassFmt
        {
            get { return _PassFmt; }
            private set { _PassFmt = value; }
        }

        private string _PassSalt = "";
        public string PassSalt
        {
            get { return _PassSalt; }
            private set { _PassSalt = value; }
        }

        private string _MobilePIN = "";
        public string MobilePIN
        {
            get { return _MobilePIN; }
            private set { _MobilePIN = value; }
        }

        private string _LCEmail = "";
        public string LCEmail
        {
            get { return _LCEmail.ToLower(); }
            private set { _LCEmail = value.ToLower(); }
        }

        private string _PWQuest = "";
        public string PWQuest
        {
            get { return _PWQuest; }
            private set { _PWQuest = value; }
        }

        private string _PWAns = "";
        public string PWAns
        {
            get { return _PWAns.ToLower(); }
            private set { _PWAns = value.ToLower(); }
        }

        private bool _IsApproved = false;
        public bool IsApproved
        {
            get { return _IsApproved; }
            private set { _IsApproved = value; }
        }

        private bool _IsOnline = false;
        public bool IsOnline
        {
            get { return _IsOnline; }
            private set { _IsOnline = value; }
        }

        private bool _IsLocked = false;
        public bool IsLocked
        {
            get { return _IsLocked; }
            private set { _IsLocked = value; }
        }

        private DateTime _LastLogin = System.DateTime.MinValue;
        public DateTime LastLogin
        {
            get { return _LastLogin; }
            private set { _LastLogin = value; }
        }

        private DateTime _LastPWChg = System.DateTime.MinValue;
        public DateTime LastPWChg
        {
            get { return _LastPWChg; }
            private set { _LastPWChg = value; }
        }

        private DateTime _LastLock = System.DateTime.MinValue;
        public DateTime LastLock
        {
            get { return _LastLock; }
            private set { _LastLock = value; }
        }

        private int _XPWAtt = 0;
        public int XPWAtt
        {
            get { return _XPWAtt; }
            private set { _XPWAtt = value; }
        }

        private DateTime _XPWAttSt = System.DateTime.MinValue;
        public DateTime XPWAttSt
        {
            get { return _XPWAttSt; }
            private set { _XPWAttSt = value; }
        }

        private int _XPWAnsAtt = 0;
        public int XPWAnsAtt
        {
            get { return _XPWAnsAtt; }
            private set { _XPWAnsAtt = value; }
        }

        private DateTime _XPWAnsSt = System.DateTime.MinValue;
        public DateTime XPWAnsSt
        {
            get { return _XPWAnsSt; }
            private set { _XPWAnsSt = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            private set { _Comment = value; }
        }

        private DateTime _Created = System.DateTime.Now;
        public DateTime Created
        {
            get { return _Created; }
            private set { _Created = value; }
        }

        private int _AgeGroup = 0;
        public int AgeGroup
        {
            get { return _AgeGroup; }
            private set { _AgeGroup = value; }
        }

        private int _RegTypeID = 0;
        public int RegTypeID
        {
            get { return _RegTypeID; }
            private set { _RegTypeID = value; }
        }

        private int _RepID = 0;
        public int RepID
        {
            get { return _RepID; }
            private set { _RepID = value; }
        }


        private string _Work_Phone = "";
        public string Work_Phone
        {
            get { return _Work_Phone; }
            set { _Work_Phone = value; }
        }
        private string _Badge_ID = "";
        public string Badge_ID
        {
            get { return _Badge_ID; }
            set { _Badge_ID = value; }
        }

        private DateTime _Hire_Date = System.DateTime.MinValue;
        public DateTime Hire_Date
        {
            get { return _Hire_Date; }
            set { _Hire_Date = value; }
        }

        private DateTime _Termin_Date = System.DateTime.MinValue;
        public DateTime Termin_Date
        {
            get { return _Termin_Date; }
            set { _Termin_Date = value; }
        }

        private int _Dept_ID = 0;
        public int Dept_ID
        {
            get { return _Dept_ID; }
            set { _Dept_ID = value; }
        }

        private string _deptName = "";
        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }

        private string _roleNames = "";
        public string RoleNames
        {
            get { return _roleNames; }
            set { _roleNames = value; }
        }

        private int _Position_ID = 0;
        public int Position_ID
        {
            get { return _Position_ID; }
            set { _Position_ID = value; }
        }

        private int _Clinical_Ind = 0;
        public int Clinical_Ind
        {
            get { return _Clinical_Ind; }
            set { _Clinical_Ind = value; }
        }

        private string _Title = "";
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private bool _Giftcard_Ind = false;
        public bool Giftcard_Ind
        {
            get { return _Giftcard_Ind; }
            set { _Giftcard_Ind = value; }
        }

        private decimal _Giftcard_Chour = 0;
        public decimal Giftcard_Chour
        {
            get { return _Giftcard_Chour; }
            set { _Giftcard_Chour = value; }
        }

        private decimal _Giftcard_Uhour = 0;
        public decimal Giftcard_Uhour
        {
            get { return _Giftcard_Uhour; }
            set { _Giftcard_Uhour = value; }
        }

        private int _UniqueID = 0;
        public int UniqueID
        {
            get { return _UniqueID; }
            set { _UniqueID = value; }
        }

        private bool _FirstLogin_Ind = false;
        public bool FirstLogin_Ind
        {
            get { return _FirstLogin_Ind; }
            set { _FirstLogin_Ind = value; }
        }

        private string _license_number = "";
        public string license_number
        {
            get { return _license_number; }
            private set { _license_number = value; }
        }

        private string _State = "";
        public string State
        {
            get { return _State; }
            private set { _State = value; }
        }

    }

    public class UserEnrollmentInfo
    {
        public int iid { get; set; }
        public int testid { get; set; }
        public string cfirstname { get; set; }
        public string clastname { get; set; }
        public int facilityid { get; set; }
        public string facilityname { get; set; }
        public int deptid { get; set; }
        public string deptname { get; set; }
        public int topicid { get; set; }
        public string topicname { get; set; }
        public string coursenumber { get; set; }
        public string deadline { get; set; }
        public string complete { get; set; }
        public string cusername { get; set; }
        public string  email { get; set; }
        public UserEnrollmentInfo()
        {}

        public UserEnrollmentInfo(string cfirstname, string clastname, string deptname, string email)
        {
            this.cfirstname = cfirstname;
            this.clastname = clastname;
            this.deptname = deptname;
            this.email = email;
        }

          public UserEnrollmentInfo(int iid, string cusername ,string firstname, string lastname, string detpname,string email, int facilityid)
        {
            this.iid = iid;
            this.cfirstname = firstname;
            this.clastname = lastname;
            this.deptname = detpname;
            this.cusername = cusername;
            this.facilityid = facilityid;
            this.email = email;
        }

        public UserEnrollmentInfo(int iid,int testid, string firstname, string lastname, int fid, string fname, int deptid, string deptname,
            int topicid, string topicname, string coursenumber, string deadline,string complete )
        {
            this.iid = iid;
             this.testid=testid;
            this.cfirstname = firstname;
            this.clastname = lastname;
            this.facilityid = fid;
            this.facilityname = fname;
            this.deptid = deptid;
            this.deptname = deptname;
            this.topicid = topicid;
            this.topicname = topicname;
            this.coursenumber = coursenumber;
            this.deadline = deadline;
            this.complete = complete;

        }
    }

}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        /////////////////////////////////////////////////////////
        // methods that work with UserAccounts

        /// <summary>
        /// Returns the total number of UserAccounts
        /// </summary>
        public int GetUserAccountCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from UserAccount", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public int GetUserAccountByUniqueid(string uniqueid, string password)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select cpw,iid from UserAccount where uniqueid=@uniqueid and facilityid=2", cn);
                cmd.Parameters.Add("@uniqueid", SqlDbType.Int).Value = uniqueid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    if (reader["cpw"] != null)
                    {
                        if (password.ToString() == encrypt(encrypt(encrypt(reader["cpw"].ToString()) + "abc") + "VBF2D8EDB0"))
                        {
                            return (int)reader["iid"];
                        }
                        else
                        {
                            return 0;
                        }

                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }

            }
        }

        public int GetUserAccountByUserName(string username, string password)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select cpw,iid from UserAccount where cusername=@username and facilityid=2", cn);
                cmd.Parameters.Add("@username", SqlDbType.Int).Value = username;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    if (reader["cpw"] != null)
                    {
                        if (password.ToString() == encrypt(encrypt(encrypt(reader["cpw"].ToString()) + "abc") + "VBF2D8EDB0"))
                        {
                            return (int)reader["iid"];
                        }
                        else
                        {
                            return 0;
                        }

                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }

            }
        }

        public string encrypt(string text)
        {

            System.Security.Cryptography.MD5CryptoServiceProvider md5Obj = new System.Security.Cryptography.MD5CryptoServiceProvider();

            byte[] bytesToHash = md5Obj.ComputeHash(System.Text.Encoding.ASCII.GetBytes(text));

            string strResult = "";

            foreach (byte b in bytesToHash)
            {


                strResult += b.ToString("x2");
            }
            return strResult;
        }


        /// Returns the UserID for a UserName
        /// </summary>
        public int GetUserIDByUserName(string UserName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select iID from UserAccount where lcUserName=@cUserName", cn);
                cmd.Parameters.Add("@cUserName", SqlDbType.VarChar).Value = UserName.ToLower();
                cn.Open();

                //TODO: Handle NULL result
                return (int)ExecuteScalar(cmd);
            }
        }

        /// Returns the UserID for a ShortUserName and FacilityID
        /// </summary>
        public int GetUserIDByShortUserNameAndFacilityID(string ShortUserName, int FacilityID)
        {
            int ret = 0;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select iID from UserAccount where cUserName=@ShortUserName and FacilityID=@FacilityID", cn);
                cmd.Parameters.Add("@ShortUserName", SqlDbType.VarChar).Value = ShortUserName.ToLower();
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cn.Open();
                try
                {
                    //TODO: Handle NULL result
                    ret = (int)ExecuteScalar(cmd);
                }
                catch 
                {
                }
            }
            return ret;
        }

        /// Returns the UserID for a ShortUserName
        /// </summary>
        public int GetUserIDByShortUserName(string ShortUserName)
        {
            int ret = 0;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select iID from UserAccount where cUserName=@ShortUserName", cn);
                cmd.Parameters.Add("@ShortUserName", SqlDbType.VarChar).Value = ShortUserName.ToLower();                
                cn.Open();
                try
                {
                    //TODO: Handle NULL result
                    ret = (int)ExecuteScalar(cmd);
                }
                catch
                {
                }
            }
            return ret;
        }


        public bool GetGiftCardUsersFromUserAccountInfoList(string cGiftUsername)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * from useraccount where " +
                    "giftcard_ind='1' and cusername= @UserName", cn);
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = cGiftUsername;

                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        /// <summary>
        /// Retrieves all UserAccounts
        /// </summary>
        public List<UserAccountInfo> GetUserAccounts(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID, Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves most recent 300 UserAccounts
        /// </summary>
        public List<UserAccountInfo> GetRecentUserAccounts()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top 300 " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID,Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount order by created desc ";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the UserAccount with the specified ID
        /// </summary>
        public UserAccountInfo GetUserAccountByID(int iID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID,Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where iID=@iID", cn);
                cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUserAccountFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the UserAccount with the specified username and facilityid
        /// </summary>
        public UserAccountInfo GetUserAccountByShortUserNameAndFacilityID(string ShortUserName, int FacilityID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID,Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where cUserName=@ShortUserName and FacilityID = @FacilityID ", cn);
                cmd.Parameters.Add("@ShortUserName", SqlDbType.VarChar).Value = ShortUserName.ToLower();
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;

                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUserAccountFromReader(reader, true);
                else
                    return null;
            }
        }
        /// <summary>
        /// Retrieves the UserAccount with the specified Email And FacilityId
        /// </summary>
        public UserAccountInfo GetUserAccountByEmailAndFacid(string EmailAddress, int Facid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID,Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where facilityid=@Facid and cemail=@Email ", cn);

                cmd.Parameters.Add("@Facid", SqlDbType.Int).Value = Facid;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = EmailAddress;

                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUserAccountFromReader(reader, true);
                else
                    return null;
            }
        }
        /// <summary>
        /// Deletes a UserAccount
        /// </summary>
        public bool DeleteUserAccount(int iID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from UserAccount where iID=@iID", cn);
                cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new UserAccount
        /// </summary>
        public int InsertUserAccount(UserAccountInfo UserAccount)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into UserAccount " +
              "(cAddress1, " +
              "cAddress2, " +
              "lAdmin, " +
              "lPrimary, " +
              "cBirthDate, " +
              "cCity, " +
              "cEmail, " +
              "cExpires, " +
              "cFirstName, " +
              "cFloridaNo, " +
              "cInstitute, " +
              "cLastName, " +
              "cLectDate, " +
              "cMiddle, " +
              "cPhone, " +
              "cPromoCode, " +
              "PromoID, " +
              "FacilityID, " +
              "cPW, " +
              "cRefCode, " +
              "cRegType, " +
              "cSocial, " +
              "cState, " +
              "cUserName, " +
              "cZipCode, " +
              "SpecID, " +
              "LCUserName, " +
              "LastAct, " +
              "PassFmt, " +
              "PassSalt, " +
              "MobilePIN, " +
              "LCEmail, " +
              "PWQuest, " +
              "PWAns, " +
              "IsApproved, " +
              "IsOnline, " +
              "IsLocked, " +
              "LastLogin, " +
              "LastPWChg, " +
              "LastLock, " +
              "XPWAtt, " +
              "XPWAttSt, " +
              "XPWAnsAtt, " +
              "XPWAnsSt, " +
              "Comment, " +
              "Created, " +
              "AgeGroup, " +
              "RegTypeID, " +
              "RepID, " +
              "Work_Phone," +
              "Badge_ID," +
              "Hire_Date," +
              "Termin_Date," +
              "Dept_ID," +
              "Position_ID," +
              "Clinical_Ind," +
              "Title," +
              "Giftcard_Ind," +
              "Giftcard_Chour," +
              "Giftcard_Uhour," +
              "UniqueID," +
              "FirstLogin_Ind ) " +
              "VALUES (" +
                "@cAddress1, " +
                "@cAddress2, " +
                "@lAdmin, " +
                "@lPrimary, " +
                "@cBirthDate, " +
                "@cCity, " +
                "@cEmail, " +
                "@cExpires, " +
                "@cFirstName, " +
                "@cFloridaNo, " +
                "@cInstitute, " +
                "@cLastName, " +
                "@cLectDate, " +
                "@cMiddle, " +
                "@cPhone, " +
              "@cPromoCode, " +
              "@PromoID, " +
              "@FacilityID, " +
              "@cPW, " +
              "@cRefCode, " +
              "@cRegType, " +
              "@cSocial, " +
              "@cState, " +
              "@cUserName, " +
              "@cZipCode, " +
              "@SpecID, " +
              "@LCUserName, " +
              "@LastAct, " +
              "@PassFmt, " +
              "@PassSalt, " +
              "@MobilePIN, " +
              "@LCEmail, " +
              "@PWQuest, " +
              "@PWAns, " +
              "@IsApproved, " +
              "@IsOnline, " +
              "@IsLocked, " +
              "@LastLogin, " +
              "@LastPWChg, " +
              "@LastLock, " +
              "@XPWAtt, " +
              "@XPWAttSt, " +
              "@XPWAnsAtt, " +
              "@XPWAnsSt, " +
              "@Comment, " +
              "@Created, " +
              "@AgeGroup, " +
              "@RegTypeID, " +
              "@RepID, " +
                "@Work_Phone, " +
                "@Badge_ID, " +
                "@Hire_Date, " +
                "@Termin_Date, " +
                "@Dept_ID, " +
                "@Position_ID, " +
                "@Clinical_Ind, " +
                "@Title, " +
                "@Giftcard_Ind, " +
                "@Giftcard_Chour, " +
                "@Giftcard_Uhour, " +
                "@UniqueID, " +
                "@FirstLogin_Ind) SET @ID = SCOPE_IDENTITY()", cn);
                cmd.Parameters.Add("@cAddress1", SqlDbType.VarChar).Value = UserAccount.cAddress1;
                cmd.Parameters.Add("@cAddress2", SqlDbType.VarChar).Value = UserAccount.cAddress2;
                cmd.Parameters.Add("@lAdmin", SqlDbType.Bit).Value = UserAccount.lAdmin;
                cmd.Parameters.Add("@lPrimary", SqlDbType.Bit).Value = UserAccount.lPrimary;
                cmd.Parameters.Add("@cBirthDate", SqlDbType.VarChar).Value = UserAccount.cBirthDate;
                cmd.Parameters.Add("@cCity", SqlDbType.VarChar).Value = UserAccount.cCity;
                cmd.Parameters.Add("@cEmail", SqlDbType.VarChar).Value = UserAccount.cEmail;
                cmd.Parameters.Add("@cExpires", SqlDbType.VarChar).Value = UserAccount.cExpires;
                cmd.Parameters.Add("@cFirstName", SqlDbType.VarChar).Value = UserAccount.cFirstName;
                cmd.Parameters.Add("@cFloridaNo", SqlDbType.VarChar).Value = UserAccount.cFloridaNo;
                cmd.Parameters.Add("@cInstitute", SqlDbType.VarChar).Value = UserAccount.cInstitute;
                cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = UserAccount.cLastName;
                cmd.Parameters.Add("@cLectDate", SqlDbType.VarChar).Value = UserAccount.cLectDate;
                cmd.Parameters.Add("@cMiddle", SqlDbType.VarChar).Value = UserAccount.cMiddle;
                cmd.Parameters.Add("@cPhone", SqlDbType.VarChar).Value = UserAccount.cPhone;
                cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = UserAccount.cPromoCode;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.PromoID == 0)
                {
                    cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = UserAccount.PromoID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.FacilityID == 0)
                {
                    cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = UserAccount.FacilityID;
                }

                cmd.Parameters.Add("@cPW", SqlDbType.VarChar).Value = UserAccount.cPW;
                cmd.Parameters.Add("@cRefCode", SqlDbType.VarChar).Value = UserAccount.cRefCode;
                cmd.Parameters.Add("@cRegType", SqlDbType.VarChar).Value = UserAccount.cRegType;
                cmd.Parameters.Add("@cSocial", SqlDbType.VarChar).Value = UserAccount.cSocial;
                cmd.Parameters.Add("@cState", SqlDbType.Char).Value = UserAccount.cState;
                cmd.Parameters.Add("@cUserName", SqlDbType.VarChar).Value = UserAccount.cUserName.ToLower();
                cmd.Parameters.Add("@cZipCode", SqlDbType.VarChar).Value = UserAccount.cZipCode;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.SpecID == 0)
                {
                    cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = UserAccount.SpecID;
                }

                cmd.Parameters.Add("@LCUserName", SqlDbType.VarChar).Value = UserAccount.LCUserName.ToLower();
                cmd.Parameters.Add("@PassFmt", SqlDbType.VarChar).Value = UserAccount.PassFmt;
                cmd.Parameters.Add("@PassSalt", SqlDbType.VarChar).Value = UserAccount.PassSalt;
                cmd.Parameters.Add("@MobilePIN", SqlDbType.VarChar).Value = UserAccount.MobilePIN;
                cmd.Parameters.Add("@LCEmail", SqlDbType.VarChar).Value = UserAccount.LCEmail;
                cmd.Parameters.Add("@PWQuest", SqlDbType.VarChar).Value = UserAccount.PWQuest;
                cmd.Parameters.Add("@PWAns", SqlDbType.VarChar).Value = UserAccount.PWAns.ToLower();
                cmd.Parameters.Add("@IsApproved", SqlDbType.Bit).Value = UserAccount.IsApproved;
                cmd.Parameters.Add("@IsOnline", SqlDbType.Bit).Value = UserAccount.IsOnline;
                cmd.Parameters.Add("@IsLocked", SqlDbType.Bit).Value = UserAccount.IsLocked;
                cmd.Parameters.Add("@XPWAtt", SqlDbType.Int).Value = UserAccount.XPWAtt;
                cmd.Parameters.Add("@XPWAnsAtt", SqlDbType.Int).Value = UserAccount.XPWAnsAtt;
                cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = UserAccount.Comment;
                cmd.Parameters.Add("@Created", SqlDbType.DateTime).Value = UserAccount.Created;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.AgeGroup == 0)
                {
                    cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = UserAccount.AgeGroup;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.RegTypeID == 0)
                {
                    cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = UserAccount.RegTypeID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.RepID == 0)
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = UserAccount.RepID;
                }


                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Work_Phone == "0")
                //{
                //    cmd.Parameters.Add("@Work_Phone", SqlDbType.VarChar).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Work_Phone", SqlDbType.VarChar).Value = UserAccount.Work_Phone;
                //}

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Badge_ID == "0")
                //{
                //    cmd.Parameters.Add("@Badge_ID", SqlDbType.VarChar).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Badge_ID", SqlDbType.VarChar).Value = UserAccount.Badge_ID;
                //}

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastAct == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastAct", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastAct", SqlDbType.DateTime).Value = UserAccount.LastAct;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastLogin == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = UserAccount.LastLogin;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastPWChg == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastPWChg", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastPWChg", SqlDbType.DateTime).Value = UserAccount.LastPWChg;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastLock == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastLock", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastLock", SqlDbType.DateTime).Value = UserAccount.LastLock;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.XPWAttSt == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@XPWAttSt", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@XPWAttSt", SqlDbType.DateTime).Value = UserAccount.XPWAttSt;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.XPWAnsSt == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@XPWAnsSt", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@XPWAnsSt", SqlDbType.DateTime).Value = UserAccount.XPWAnsSt;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.Hire_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Hire_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Hire_Date", SqlDbType.DateTime).Value = UserAccount.Hire_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.Termin_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Termin_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Termin_Date", SqlDbType.DateTime).Value = UserAccount.Termin_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Dept_ID == 0)
                {
                    cmd.Parameters.Add("@Dept_ID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Dept_ID", SqlDbType.Int).Value = UserAccount.Dept_ID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Position_ID == 0)
                {
                    cmd.Parameters.Add("@Position_ID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Position_ID", SqlDbType.Int).Value = UserAccount.Position_ID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Clinical_Ind == 0)
                {
                    cmd.Parameters.Add("@Clinical_Ind", SqlDbType.Bit).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Clinical_Ind", SqlDbType.Bit).Value = UserAccount.Clinical_Ind;
                }

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Title == "0")
                //{
                //    cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = UserAccount.Title;
                //}

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Giftcard_Ind == false)
                {
                    cmd.Parameters.Add("@Giftcard_Ind", SqlDbType.Bit).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Giftcard_Ind", SqlDbType.Bit).Value = UserAccount.Giftcard_Ind;
                }

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Giftcard_Chour == 0)
                //{
                //    cmd.Parameters.Add("@Giftcard_Chour", SqlDbType.Decimal).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Giftcard_Chour", SqlDbType.Decimal).Value = UserAccount.Giftcard_Chour;
                //}

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Giftcard_Uhour == 0)
                //{
                //    cmd.Parameters.Add("@Giftcard_Uhour", SqlDbType.Decimal).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Giftcard_Uhour", SqlDbType.Decimal).Value = UserAccount.Giftcard_Uhour;
                //}

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.UniqueID == 0)
                //{
                //    cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = UserAccount.UniqueID;
                //}
                cmd.Parameters.Add("@FirstLogin_Ind", SqlDbType.Bit).Value = UserAccount.FirstLogin_Ind;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Inserts a new UserAccount
        /// </summary>
        public int InsertUserAccountMS(UserAccountInfo UserAccount)
        {
            int NewID = 0;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into UserAccount " +
              "(cAddress1, " +
              "cAddress2, " +
              "lAdmin, " +
              "lPrimary, " +
              "cBirthDate, " +
              "cCity, " +
              "cEmail, " +
              "cExpires, " +
              "cFirstName, " +
              "cFloridaNo, " +
              "cInstitute, " +
              "cLastName, " +
              "cLectDate, " +
              "cMiddle, " +
              "cPhone, " +
              "cPromoCode, " +
              "PromoID, " +
              "FacilityID, " +
              "cPW, " +
              "cRefCode, " +
              "cRegType, " +
              "cSocial, " +
              "cState, " +
              "cUserName, " +
              "cZipCode, " +
              "SpecID, " +
              "LCUserName, " +
              "LastAct, " +
              "PassFmt, " +
              "PassSalt, " +
              "MobilePIN, " +
              "LCEmail, " +
              "PWQuest, " +
              "PWAns, " +
              "IsApproved, " +
              "IsOnline, " +
              "IsLocked, " +
              "LastLogin, " +
              "LastPWChg, " +
              "LastLock, " +
              "XPWAtt, " +
              "XPWAttSt, " +
              "XPWAnsAtt, " +
              "XPWAnsSt, " +
              "Comment, " +
              "Created, " +
              "AgeGroup, " +
              "RegTypeID, " +
              "RepID, " +
              "Work_Phone," +
              "Badge_ID," +
              "Hire_Date," +
              "Termin_Date," +
              "Dept_ID," +
              "Position_ID," +
              "Clinical_Ind," +
              "Title," +
              "Giftcard_Ind," +
              "Giftcard_Chour," +
              "Giftcard_Uhour," +
              "UniqueID," +
              "FirstLogin_Ind ) " +
              "VALUES (" +
                "@cAddress1, " +
                "@cAddress2, " +
                "@lAdmin, " +
                "@lPrimary, " +
                "@cBirthDate, " +
                "@cCity, " +
                "@cEmail, " +
                "@cExpires, " +
                "@cFirstName, " +
                "@cFloridaNo, " +
                "@cInstitute, " +
                "@cLastName, " +
                "@cLectDate, " +
                "@cMiddle, " +
                "@cPhone, " +
              "@cPromoCode, " +
              "@PromoID, " +
              "@FacilityID, " +
              "'', " +
              "@cRefCode, " +
              "@cRegType, " +
              "@cSocial, " +
              "@cState, " +
              "@cUserName, " +
              "@cZipCode, " +
              "@SpecID, " +
              "@LCUserName, " +
              "@LastAct, " +
              "@PassFmt, " +
              "@PassSalt, " +
              "@MobilePIN, " +
              "@LCEmail, " +
              "@PWQuest, " +
              "@PWAns, " +
              "@IsApproved, " +
              "@IsOnline, " +
              "@IsLocked, " +
              "@LastLogin, " +
              "@LastPWChg, " +
              "@LastLock, " +
              "@XPWAtt, " +
              "@XPWAttSt, " +
              "@XPWAnsAtt, " +
              "@XPWAnsSt, " +
              "@Comment, " +
              "@Created, " +
              "@AgeGroup, " +
              "@RegTypeID, " +
              "@RepID, " +
                "@Work_Phone, " +
                "@Badge_ID, " +
                "@Hire_Date, " +
                "@Termin_Date, " +
                "@Dept_ID, " +
                "@Position_ID, " +
                "@Clinical_Ind, " +
                "@Title, " +
                "@Giftcard_Ind, " +
                "@Giftcard_Chour, " +
                "@Giftcard_Uhour, " +
                "@UniqueID, " +
                "@FirstLogin_Ind) SET @ID = SCOPE_IDENTITY()", cn);
                cmd.Parameters.Add("@cAddress1", SqlDbType.VarChar).Value = UserAccount.cAddress1;
                cmd.Parameters.Add("@cAddress2", SqlDbType.VarChar).Value = UserAccount.cAddress2;
                cmd.Parameters.Add("@lAdmin", SqlDbType.Bit).Value = UserAccount.lAdmin;
                cmd.Parameters.Add("@lPrimary", SqlDbType.Bit).Value = UserAccount.lPrimary;
                cmd.Parameters.Add("@cBirthDate", SqlDbType.VarChar).Value = UserAccount.cBirthDate;
                cmd.Parameters.Add("@cCity", SqlDbType.VarChar).Value = UserAccount.cCity;
                cmd.Parameters.Add("@cEmail", SqlDbType.VarChar).Value = UserAccount.cEmail;
                cmd.Parameters.Add("@cExpires", SqlDbType.VarChar).Value = UserAccount.cExpires;
                cmd.Parameters.Add("@cFirstName", SqlDbType.VarChar).Value = UserAccount.cFirstName;
                cmd.Parameters.Add("@cFloridaNo", SqlDbType.VarChar).Value = UserAccount.cFloridaNo;
                cmd.Parameters.Add("@cInstitute", SqlDbType.VarChar).Value = UserAccount.cInstitute;
                cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = UserAccount.cLastName;
                cmd.Parameters.Add("@cLectDate", SqlDbType.VarChar).Value = UserAccount.cLectDate;
                cmd.Parameters.Add("@cMiddle", SqlDbType.VarChar).Value = UserAccount.cMiddle;
                cmd.Parameters.Add("@cPhone", SqlDbType.VarChar).Value = UserAccount.cPhone;
                cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = UserAccount.cPromoCode;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.PromoID == 0)
                {
                    cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = UserAccount.PromoID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.FacilityID == 0)
                {
                    cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = UserAccount.FacilityID;
                }

               // cmd.Parameters.Add("@cPW", SqlDbType.VarChar).Value = UserAccount.cPW;
                cmd.Parameters.Add("@cRefCode", SqlDbType.VarChar).Value = UserAccount.cRefCode;
                cmd.Parameters.Add("@cRegType", SqlDbType.VarChar).Value = UserAccount.cRegType;
                cmd.Parameters.Add("@cSocial", SqlDbType.VarChar).Value = UserAccount.cSocial;
                cmd.Parameters.Add("@cState", SqlDbType.Char).Value = UserAccount.cState;
                cmd.Parameters.Add("@cUserName", SqlDbType.VarChar).Value = UserAccount.cUserName.ToLower();
                cmd.Parameters.Add("@cZipCode", SqlDbType.VarChar).Value = UserAccount.cZipCode;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.SpecID == 0)
                {
                    cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = UserAccount.SpecID;
                }

                cmd.Parameters.Add("@LCUserName", SqlDbType.VarChar).Value = UserAccount.LCUserName.ToLower();
                cmd.Parameters.Add("@PassFmt", SqlDbType.VarChar).Value = UserAccount.PassFmt;
                cmd.Parameters.Add("@PassSalt", SqlDbType.VarChar).Value = UserAccount.PassSalt;
                cmd.Parameters.Add("@MobilePIN", SqlDbType.VarChar).Value = UserAccount.MobilePIN;
                cmd.Parameters.Add("@LCEmail", SqlDbType.VarChar).Value = UserAccount.LCEmail;
                cmd.Parameters.Add("@PWQuest", SqlDbType.VarChar).Value = UserAccount.PWQuest;
                cmd.Parameters.Add("@PWAns", SqlDbType.VarChar).Value = UserAccount.PWAns.ToLower();
                cmd.Parameters.Add("@IsApproved", SqlDbType.Bit).Value = UserAccount.IsApproved;
                cmd.Parameters.Add("@IsOnline", SqlDbType.Bit).Value = UserAccount.IsOnline;
                cmd.Parameters.Add("@IsLocked", SqlDbType.Bit).Value = UserAccount.IsLocked;
                cmd.Parameters.Add("@XPWAtt", SqlDbType.Int).Value = UserAccount.XPWAtt;
                cmd.Parameters.Add("@XPWAnsAtt", SqlDbType.Int).Value = UserAccount.XPWAnsAtt;
                cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = UserAccount.Comment;
                cmd.Parameters.Add("@Created", SqlDbType.DateTime).Value = UserAccount.Created;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.AgeGroup == 0)
                {
                    cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = UserAccount.AgeGroup;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.RegTypeID == 0)
                {
                    cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = UserAccount.RegTypeID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.RepID == 0)
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = UserAccount.RepID;
                }


                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Work_Phone == "0")
                //{
                //    cmd.Parameters.Add("@Work_Phone", SqlDbType.VarChar).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Work_Phone", SqlDbType.VarChar).Value = UserAccount.Work_Phone;
                //}

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Badge_ID == "0")
                //{
                //    cmd.Parameters.Add("@Badge_ID", SqlDbType.VarChar).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Badge_ID", SqlDbType.VarChar).Value = UserAccount.Badge_ID;
                //}

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastAct == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastAct", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastAct", SqlDbType.DateTime).Value = UserAccount.LastAct;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastLogin == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = UserAccount.LastLogin;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastPWChg == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastPWChg", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastPWChg", SqlDbType.DateTime).Value = UserAccount.LastPWChg;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastLock == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastLock", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastLock", SqlDbType.DateTime).Value = UserAccount.LastLock;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.XPWAttSt == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@XPWAttSt", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@XPWAttSt", SqlDbType.DateTime).Value = UserAccount.XPWAttSt;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.XPWAnsSt == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@XPWAnsSt", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@XPWAnsSt", SqlDbType.DateTime).Value = UserAccount.XPWAnsSt;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.Hire_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Hire_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Hire_Date", SqlDbType.DateTime).Value = UserAccount.Hire_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.Termin_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Termin_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Termin_Date", SqlDbType.DateTime).Value = UserAccount.Termin_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Dept_ID == 0)
                {
                    cmd.Parameters.Add("@Dept_ID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Dept_ID", SqlDbType.Int).Value = UserAccount.Dept_ID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Position_ID == 0)
                {
                    cmd.Parameters.Add("@Position_ID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Position_ID", SqlDbType.Int).Value = UserAccount.Position_ID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Clinical_Ind == 0)
                {
                    cmd.Parameters.Add("@Clinical_Ind", SqlDbType.Bit).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Clinical_Ind", SqlDbType.Bit).Value = UserAccount.Clinical_Ind;
                }

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Title == "0")
                //{
                //    cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = UserAccount.Title;
                //}

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Giftcard_Ind == false)
                {
                    cmd.Parameters.Add("@Giftcard_Ind", SqlDbType.Bit).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Giftcard_Ind", SqlDbType.Bit).Value = UserAccount.Giftcard_Ind;
                }

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Giftcard_Chour == 0)
                //{
                //    cmd.Parameters.Add("@Giftcard_Chour", SqlDbType.Decimal).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Giftcard_Chour", SqlDbType.Decimal).Value = UserAccount.Giftcard_Chour;
                //}

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Giftcard_Uhour == 0)
                //{
                //    cmd.Parameters.Add("@Giftcard_Uhour", SqlDbType.Decimal).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Giftcard_Uhour", SqlDbType.Decimal).Value = UserAccount.Giftcard_Uhour;
                //}

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.UniqueID == 0)
                //{
                //    cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = UserAccount.UniqueID;
                //}
                cmd.Parameters.Add("@FirstLogin_Ind", SqlDbType.Bit).Value = UserAccount.FirstLogin_Ind;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                NewID = (int)IDParameter.Value;
            }
            if(NewID > 0)
                ResetUserPassword(NewID, UserAccount.cPW);
            return NewID;
        }
        public DataSet GetSEIUUsers()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_r_seiu_user",cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds,"users");

      

                return ds;
            }
        }
        
        /// <summary>
        /// Inserts a new Nurse Account
        /// </summary>
        public int InsertNurseAccount(int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_insert_to_nurse", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userid;
                SqlParameter IDParameter = new SqlParameter("@NurseID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);
                cn.Open();
                cmd.ExecuteNonQuery();
                int NewID = (int)IDParameter.Value;
                return NewID;
              }
        }

        /// <summary>
        /// Updates user SSN and Birthdate
        /// </summary>
        public bool UpdateSSNAndBirthDate(int UserID, int SSN, string BirthDate)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_update_userssnbirth", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@SSN", SqlDbType.NVarChar).Value = SSN.ToString();
                cmd.Parameters.Add("@BirthDate", SqlDbType.VarChar).Value = BirthDate;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        

        //update ssn
        public bool UpdateSSN(int UserID, int SSN)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_update_userssn", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@SSN", SqlDbType.VarChar).Value = SSN.ToString();
              
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        // /// TODO: Check on this!!!!!

        // /// <summary>
        ///// Returns the highest UserAccountID
        ///// (used by the front-end to find the last ID inserted because
        ///// all other methods via ObjectDataSource are failing)
        ///// </summary>
        //public override int GetNextUserAccountID()
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {
        //        SqlCommand cmd = new SqlCommand("select top 1 cast(iID as Integer) as iID from UserAccount order by iID Desc", cn);
        //        cn.Open();
        //        int iID;
        //        iID = (int)ExecuteScalar(cmd);
        //        return ++iID;
        //    }
        //}

        /// <summary>
        /// Updates a UserAccount
        /// </summary>
        public bool UpdateUserAccount(UserAccountInfo UserAccount)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserAccount set " +
              "cAddress1 = @cAddress1, " +
              "cAddress2 = @cAddress2, " +
              "lAdmin = @lAdmin, " +
              "lPrimary = @lPrimary, " +
              "cBirthDate = @cBirthDate, " +
              "cCity = @cCity, " +
              "cEmail = @cEmail, " +
              "cExpires = @cExpires, " +
              "cFirstName = @cFirstName, " +
              "cFloridaNo = @cFloridaNo, " +
              "cInstitute = @cInstitute, " +
              "cLastName = @cLastName, " +
              "cLectDate = @cLectDate, " +
              "cMiddle = @cMiddle, " +
              "cPhone = @cPhone, " +
              "cPromoCode = @cPromoCode, " +
              "PromoID = @PromoID, " +
              "FacilityID = @FacilityID, " +
              "cPW = @cPW, " +
              "cRefCode = @cRefCode, " +
              "cRegType = @cRegType, " +
              "cSocial = @cSocial, " +
              "cState = @cState, " +
              "cUserName = @cUserName, " +
              "cZipCode = @cZipCode, " +
              "SpecID = @SpecID, " +
              "LCUserName = @LCUserName, " +
              "LastAct = @LastAct, " +
              "PassFmt = @PassFmt, " +
              "PassSalt = @PassSalt, " +
              "MobilePIN = @MobilePIN, " +
              "LCEmail = @LCEmail, " +
              "PWQuest = @PWQuest, " +
              "PWAns = @PWAns, " +
              "IsApproved = @IsApproved, " +
              "IsOnline = @IsOnline, " +
              "IsLocked = @IsLocked, " +
              "LastLogin = @LastLogin, " +
              "LastPWChg = @LastPWChg, " +
              "LastLock = @LastLock, " +
              "XPWAtt = @XPWAtt, " +
              "XPWAttSt = @XPWAttSt, " +
              "XPWAnsAtt = @XPWAnsAtt, " +
              "XPWAnsSt = @XPWAnsSt, " +
              "Comment = @Comment, " +
              "Created = @Created, " +
              "AgeGroup = @AgeGroup, " +
              "RegTypeID = @RegTypeID, " +
              "RepID = @RepID, " +
              "Work_Phone = @Work_Phone, " +
              "Badge_ID = @Badge_ID, " +
              "Hire_Date = @Hire_Date, " +
              "Termin_Date = @Termin_Date, " +
              "Dept_ID = @Dept_ID, " +
              "Position_ID = @Position_ID, " +
              "Clinical_Ind = @Clinical_Ind, " +
              "Title = @Title, " +
              "Giftcard_Ind = @Giftcard_Ind, " +
              "Giftcard_Chour = @Giftcard_Chour, " +
              "Giftcard_Uhour = @Giftcard_Uhour, " +
              "UniqueID = @UniqueID, " +
              "FirstLogin_Ind = @FirstLogin_Ind " +
              "where iID = @iID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;


                cmd.Parameters.Add("@cAddress1", SqlDbType.VarChar).Value = UserAccount.cAddress1;
                cmd.Parameters.Add("@cAddress2", SqlDbType.VarChar).Value = UserAccount.cAddress2;
                cmd.Parameters.Add("@lAdmin", SqlDbType.Bit).Value = UserAccount.lAdmin;
                cmd.Parameters.Add("@lPrimary", SqlDbType.Bit).Value = UserAccount.lPrimary;
                cmd.Parameters.Add("@cBirthDate", SqlDbType.VarChar).Value = UserAccount.cBirthDate;
                cmd.Parameters.Add("@cCity", SqlDbType.VarChar).Value = UserAccount.cCity;
                cmd.Parameters.Add("@cEmail", SqlDbType.VarChar).Value = UserAccount.cEmail;
                cmd.Parameters.Add("@cExpires", SqlDbType.VarChar).Value = UserAccount.cExpires;
                cmd.Parameters.Add("@cFirstName", SqlDbType.VarChar).Value = UserAccount.cFirstName;
                cmd.Parameters.Add("@cFloridaNo", SqlDbType.VarChar).Value = UserAccount.cFloridaNo;
                cmd.Parameters.Add("@cInstitute", SqlDbType.VarChar).Value = UserAccount.cInstitute;
                cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = UserAccount.cLastName;
                cmd.Parameters.Add("@cLectDate", SqlDbType.VarChar).Value = UserAccount.cLectDate;
                cmd.Parameters.Add("@cMiddle", SqlDbType.VarChar).Value = UserAccount.cMiddle;
                cmd.Parameters.Add("@cPhone", SqlDbType.VarChar).Value = UserAccount.cPhone;
                cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = UserAccount.cPromoCode;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.PromoID == 0)
                {
                    cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = UserAccount.PromoID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.FacilityID == 0)
                {
                    cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = UserAccount.FacilityID;
                }

                cmd.Parameters.Add("@cPW", SqlDbType.VarChar).Value = UserAccount.cPW;
                cmd.Parameters.Add("@cRefCode", SqlDbType.VarChar).Value = UserAccount.cRefCode;
                cmd.Parameters.Add("@cRegType", SqlDbType.VarChar).Value = UserAccount.cRegType;
                cmd.Parameters.Add("@cSocial", SqlDbType.VarChar).Value = UserAccount.cSocial;
                cmd.Parameters.Add("@cState", SqlDbType.Char).Value = UserAccount.cState;
                cmd.Parameters.Add("@cUserName", SqlDbType.VarChar).Value = UserAccount.cUserName.ToLower();
                cmd.Parameters.Add("@cZipCode", SqlDbType.VarChar).Value = UserAccount.cZipCode;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.SpecID == 0)
                {
                    cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = UserAccount.SpecID;
                }

                cmd.Parameters.Add("@LCUserName", SqlDbType.VarChar).Value = UserAccount.LCUserName.ToLower();
                cmd.Parameters.Add("@PassFmt", SqlDbType.VarChar).Value = UserAccount.PassFmt;
                cmd.Parameters.Add("@PassSalt", SqlDbType.VarChar).Value = UserAccount.PassSalt;
                cmd.Parameters.Add("@MobilePIN", SqlDbType.VarChar).Value = UserAccount.MobilePIN;
                cmd.Parameters.Add("@LCEmail", SqlDbType.VarChar).Value = UserAccount.LCEmail;
                cmd.Parameters.Add("@PWQuest", SqlDbType.VarChar).Value = UserAccount.PWQuest;
                cmd.Parameters.Add("@PWAns", SqlDbType.VarChar).Value = UserAccount.PWAns.ToLower();
                cmd.Parameters.Add("@IsApproved", SqlDbType.Bit).Value = UserAccount.IsApproved;
                cmd.Parameters.Add("@IsOnline", SqlDbType.Bit).Value = UserAccount.IsOnline;
                cmd.Parameters.Add("@IsLocked", SqlDbType.Bit).Value = UserAccount.IsLocked;
                cmd.Parameters.Add("@XPWAtt", SqlDbType.Int).Value = UserAccount.XPWAtt;
                cmd.Parameters.Add("@XPWAnsAtt", SqlDbType.Int).Value = UserAccount.XPWAnsAtt;
                cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = UserAccount.Comment;
                cmd.Parameters.Add("@Created", SqlDbType.DateTime).Value = UserAccount.Created;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.AgeGroup == 0)
                {
                    cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = UserAccount.AgeGroup;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.RegTypeID == 0)
                {
                    cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = UserAccount.RegTypeID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.RepID == 0)
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = UserAccount.RepID;
                }

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Work_Phone == "0")
                //{
                //    cmd.Parameters.Add("@Work_Phone", SqlDbType.VarChar).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Work_Phone", SqlDbType.VarChar).Value = UserAccount.Work_Phone;
                //}

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Badge_ID == "0")
                //{
                //    cmd.Parameters.Add("@Badge_ID", SqlDbType.VarChar).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Badge_ID", SqlDbType.VarChar).Value = UserAccount.Badge_ID;
                //}

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastAct == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastAct", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastAct", SqlDbType.DateTime).Value = UserAccount.LastAct;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastLogin == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = UserAccount.LastLogin;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastPWChg == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastPWChg", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastPWChg", SqlDbType.DateTime).Value = UserAccount.LastPWChg;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.LastLock == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@LastLock", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastLock", SqlDbType.DateTime).Value = UserAccount.LastLock;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.XPWAttSt == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@XPWAttSt", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@XPWAttSt", SqlDbType.DateTime).Value = UserAccount.XPWAttSt;
                }

                // pass null to database if value is zero (nothing selected by user)
                if (UserAccount.XPWAnsSt == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@XPWAnsSt", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@XPWAnsSt", SqlDbType.DateTime).Value = UserAccount.XPWAnsSt;
                }


                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Hire_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Hire_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Hire_Date", SqlDbType.DateTime).Value = UserAccount.Hire_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Termin_Date == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@Termin_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Termin_Date", SqlDbType.DateTime).Value = UserAccount.Termin_Date;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Dept_ID == 0)
                {
                    cmd.Parameters.Add("@Dept_ID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Dept_ID", SqlDbType.Int).Value = UserAccount.Dept_ID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Position_ID == 0)
                {
                    cmd.Parameters.Add("@Position_ID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Position_ID", SqlDbType.Int).Value = UserAccount.Position_ID;
                }

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Clinical_Ind == 0)
                {
                    cmd.Parameters.Add("@Clinical_Ind", SqlDbType.Bit).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Clinical_Ind", SqlDbType.Bit).Value = UserAccount.Clinical_Ind;
                }

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Title == "0")
                //{
                //    cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = UserAccount.Title;
                //}

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (UserAccount.Giftcard_Ind == false)
                {
                    cmd.Parameters.Add("@Giftcard_Ind", SqlDbType.Bit).Value = 0;
                }
                else
                {
                    cmd.Parameters.Add("@Giftcard_Ind", SqlDbType.Bit).Value = UserAccount.Giftcard_Ind;
                }

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Giftcard_Chour == 0)
                //{
                //    cmd.Parameters.Add("@Giftcard_Chour", SqlDbType.Decimal).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Giftcard_Chour", SqlDbType.Decimal).Value = UserAccount.Giftcard_Chour;
                //}

                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (UserAccount.Giftcard_Uhour == 0)
                //{
                //    cmd.Parameters.Add("@Giftcard_Uhour", SqlDbType.Decimal).Value = System.DBNull.Value;
                //}
                //else
                //{
                cmd.Parameters.Add("@Giftcard_Uhour", SqlDbType.Decimal).Value = UserAccount.Giftcard_Uhour;
                //}


                cmd.Parameters.Add("@UniqueID", SqlDbType.Int).Value = UserAccount.UniqueID;
                cmd.Parameters.Add("@FirstLogin_Ind", SqlDbType.Bit).Value = UserAccount.FirstLogin_Ind;

                cmd.Parameters.Add("@iID", SqlDbType.Int).Value = UserAccount.iID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public bool ResetUserPassword(int UserID, string Password)
        {
            using (SqlConnection cn = new SqlConnection(this.RConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SP_UPDATE_NURSE_PASSWORD", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = Password;
                cn.Open();
                ExecuteNonQuery(cmd);
            }
            return true;
        }
        /// <summary>
        /// Updates a nurse after user account is updated
        /// </summary>
        public void UpdateNurseAccount(int userid) 
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update A set A.username=B.cusername,A.Password=B.cpw,A.firstname=B.cfirstname,A.lastname=B.clastname,A.address1=B.caddress1,A.address2=B.caddress2, " +
                "A.city=B.ccity,A.state=B.cstate,A.zip=B.czipcode,A.email=B.cemail,A.agegroup=B.agegroup,A.phone=B.cphone,A.education=B.cinstitute FROM Nurse.dbo.nurse A JOIN UserAccount B on A.rnid=B.uniqueID WHERE B.iid=@iid and A.CESync=1", cn);
                cmd.Parameters.Add("@iid", SqlDbType.Int).Value = userid;
                cn.Open();
                ExecuteNonQuery(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of UserAccounts for the specified promo code
        /// </summary>
        public int GetUserAccountCountByPromoCode(string cPromoCode)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from UserAccount where cPromoCode = @cPromoCode", cn);
                //              cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = cPromoCode;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all UserAccounts for the specified promo code
        /// </summary>
        public List<UserAccountInfo> GetUserAccountsByPromoCode(string cPromoCode, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID, Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where cPromoCode = @cPromoCode";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {

                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = cPromoCode;
                cn.Open();
                return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all UserAccounts for the specified lastname (can be portion)
        /// </summary>
        public List<UserAccountInfo> GetUserAccountsByLastName(string cLastName, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID, Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where cLastName Like @cLastName";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {

                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = cLastName + '%';
                cn.Open();
                return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all UserAccounts for the specified ShortUserName (can be portion)
        /// NOTE: The ShortUsername is the cUserName field without the facilityid on the front of it
        /// </summary>
        public List<UserAccountInfo> GetUserAccountsByShortUserName(string ShortUserName, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID, Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where cUserName Like @ShortUserName";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {

                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@ShortUserName", SqlDbType.VarChar).Value = ShortUserName.ToLower() + '%';
                cn.Open();
                return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all UserAccounts for the specified email (can be portion)
        /// </summary>
        public List<UserAccountInfo> GetUserAccountsByEmail(string cEmail, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID, Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where cEmail Like @cEmail";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {

                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@cEmail", SqlDbType.VarChar).Value = cEmail + '%';
                cn.Open();
                return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Returns the total number of UserAccounts for the specified promoID
        /// </summary>
        public int GetUserAccountCountByPromoID(int PromoID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from UserAccount where PromoID = @PromoID", cn);
                //              cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = PromoID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all UserAccounts for the specified promoID
        /// </summary>
        public List<UserAccountInfo> GetUserAccountsByPromoID(int PromoID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID, Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where PromoID = @PromoID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {

                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = PromoID;
                cn.Open();
                return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Returns the total number of UserAccounts for the specified FacilityID
        /// </summary>
        public int GetUserAccountCountByFacilityID(int FacilityID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from UserAccount where FacilityID = @FacilityID and isapproved=1", cn);
                //              cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all UserAccounts for the specified FacilityID
        /// </summary>
        public List<UserAccountInfo> GetUserAccountsByFacilityID(int FacilityID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID, Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where FacilityID = @FacilityID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {

                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cn.Open();
                return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<UserAccountInfo> GetUserAccountsByFacilityIDAndSuperCourseID(
            int facilityID, int superCourseID, string sortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_get_userlist_supercourse", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityID;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = superCourseID;
                cmd.Parameters.Add("@orderby", SqlDbType.VarChar).Value = sortExpression;
                cn.Open();
                return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all admin UserAccounts for the specified FacilityID
        /// </summary>
        public List<UserAccountInfo> GetAdminUserAccountsByFacilityID(int FacilityID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID, Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where FacilityID = @FacilityID and lAdmin='1'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {

                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cn.Open();
                return GetUserAccountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Updates a UserAccount Admin Status
        /// </summary>
        public bool UpdateUserAccountAdminStatus(int iID, bool lAdmin)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserAccount set " +
                    "lAdmin = @lAdmin where iid = @iID", cn);

                cmd.Parameters.Add("@lAdmin", SqlDbType.Bit).Value = lAdmin;
                cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Updates a UserAccount lPrimary Status
        /// </summary>
        public bool UpdateUserAccountlPrimaryStatus(int iID, bool lPrimary)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserAccount set " +
                    "lPrimary = @lPrimary where iid = @iID", cn);

                cmd.Parameters.Add("@lPrimary", SqlDbType.Bit).Value = lPrimary;
                cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Updates a UserAccount
        /// </summary>
        public bool UpdateUserAccountDeptByuserID(int userID, int deptID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                if (deptID == 0)
                {
                    SqlCommand cmd = new SqlCommand("update UserAccount set " +
                        "Dept_ID = NULL " +
                        "where iID = @userID ", cn);

                    cmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;
                    cn.Open();
                    int ret = ExecuteNonQuery(cmd);
                    return (ret == 1);
                }


                else
                {
                    SqlCommand cmd = new SqlCommand("update UserAccount set " +
                       "Dept_ID = @deptID " +
                       "where iID = @userID ", cn);
                    cmd.Parameters.Add("@deptID", SqlDbType.Int).Value = deptID;
                    cmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;
                    cn.Open();
                    int ret = ExecuteNonQuery(cmd);
                    return (ret == 1);
                }


            }
        }

        public int CreateRetailUser(string UserName, string Password, int iid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_u_create_nurse_user";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = iid;
                cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = UserName;
                cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = Password;
                SqlParameter IDParameter = new SqlParameter("@rnid", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);
                cmd.ExecuteNonQuery();
                int rnid = (int)IDParameter.Value;
                return rnid;

            }
        }

        public int TranferTrans(int iid, int rnid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_u_copy_transcript_license";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@rnid", SqlDbType.Int).Value = rnid;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = iid;
                try
                {
                    return (int)cmd.ExecuteNonQuery();
                }
                catch
                {
                    return -2;
                }
            }
        }

        /////////////////////////////////////////////////////////
        // methods that work with UserAccount1


        public UserAccountInfo1 GetUserAccountByUserName(string UserName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select " +
                //    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                //    "cExpires, cFirstName, cFloridaNo, " +
                //    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                //    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                //    "SpecID, LCUserName, " +
                //    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                //    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                //    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                //    "RegTypeID, RepID ,Work_Phone, Badge_ID, "+
                //    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                //    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID " +
                //    "from UserAccount where lcUserName Like @UserName";
                string cSQLCommand = "select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID ,Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where lcUserName = @UserName";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName.ToLower();
                cn.Open();
                return GetUserAccount1FromReader(ExecuteReader(cmd), false);
            }
        }

        // NOTE; The following method is used only for GiftCard login, and uses the "short"
        // version of the username with no faciltiyid attached to it.
        // This works because gift card codes are unique regardless of any facility related to it.
        public UserAccountInfo1 GetUserAccountByGiftCardRedemptionCode(string RedemptionCode)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID ,Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where cUserName = @RedemptionCode and giftcard_ind = '1'";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@RedemptionCode", SqlDbType.VarChar).Value = RedemptionCode.ToLower();
                cn.Open();
                return GetUserAccount1FromReader(ExecuteReader(cmd), false);
            }
        }
        /// Returns the UserID for a UserName
        /// </summary>
        public string GetUserNameByID(int userID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select cusername from UserAccount where iID=@userID", cn);
                cmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;
                cn.Open();

                //TODO: Handle NULL result
                return Convert.ToString(ExecuteScalar(cmd));
            }
        }
        public bool Switchfacility(int iID, int facid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_SwitchFacility";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@iID", SqlDbType.VarChar).Value = iID;
                cmd.Parameters.Add("@facid", SqlDbType.VarChar).Value = facid;

                try
                {
                    cn.Open();
                    int ret = 0;
                    ret = ExecuteNonQuery(cmd);
                    return true;
                }
                catch
                {
                    return false;
                }
                finally
                {
                    if (cn.State == ConnectionState.Open)
                        cn.Close();
                    cn.Dispose();
                }
                //return true;
            }
        }


        public bool MergeUsers(string strMasteriID, string fname, string lname, List<string> recsToMerge)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_MergeUser";
                cmd.CommandType = CommandType.StoredProcedure;
                string idlst = "";

                foreach (string s in recsToMerge)
                {
                    idlst += s + ",";
                }

                idlst = idlst.TrimEnd(',');
                cmd.Parameters.Add("@strMasteriID", SqlDbType.VarChar).Value = strMasteriID;
                cmd.Parameters.Add("@UserIds", SqlDbType.VarChar).Value = idlst;

                try
                {
                    cn.Open();
                    int ret = 0;
                    ret = ExecuteNonQuery(cmd);

                    return true;
                }

                catch 
                {
                    //tran.Rollback();
                    return false;
                }
                finally
                {
                    if (cn.State == ConnectionState.Open)
                        cn.Close();
                    // tran.Dispose();
                    cn.Dispose();

                }
                //return true;
            }
        }

        public System.Data.DataSet GetMergeRecs(string fname, string lname, string facilityid)
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string qry = "SELECT Test.testid,UserAccount.iID,UserAccount.cusername, UserAccount.cfirstname, UserAccount.clastname, UserAccount.facilityid," +
                //" (SELECT TOP (1) Dept.dept_name FROM Dept INNER JOIN UserAccount U ON Dept.dept_id = U.dept_id WHERE (U.cfirstname = UserAccount.cfirstname) AND (U.clastname = UserAccount.clastname) " +
                //" AND (U.facilityid = UserAccount.facilityid )) as DeptName,(SELECT Roles.displayname FROM Roles INNER JOIN UsersInRoles ON UsersInRoles.rolename = Roles.rolename " +
                //" WHERE (UsersInRoles.facilityid = UserAccount.facilityid) AND (UsersInRoles.username = UserAccount.lcUsername)) as AccessType, UserAccount.isapproved,Test.status, " +
                //" Test.status FROM UserAccount Left JOIN Test ON UserAccount.iid = Test.userid WHERE " +
                //" (UserAccount.cfirstname = @FirstName) AND (UserAccount.clastname = @LastName) AND (UserAccount.facilityid = @facid ) ";



                string qry = "SELECT distinct Useraccount.iid, UserAccount.cusername,UserAccount.cfirstname, UserAccount.clastname, UserAccount.facilityid , " +
                              " (SELECT TOP (1) Dept.dept_name FROM Dept INNER JOIN UserAccount U ON Dept.dept_id = U.dept_id WHERE (U.cfirstname = UserAccount.cfirstname) AND (U.clastname = UserAccount.clastname) " +
                              " AND (U.facilityid = UserAccount.facilityid )) as DeptName, " +
                              " (SELECT Roles.displayname FROM Roles INNER JOIN UsersInRoles ON UsersInRoles.rolename = Roles.rolename " +
                              " WHERE (UsersInRoles.facilityid = UserAccount.facilityid) AND (UsersInRoles.username = UserAccount.lcUsername)) as AccessType, UserAccount.isapproved FROM UserAccount WHERE (UserAccount.cfirstname = @FirstName) AND (UserAccount.clastname = @LastName) AND (UserAccount.facilityid = @facid ) ";



                //qry = qry.Replace("@FirstName", "'" + fname.Replace("'", "\'") + "'");
                //qry = qry.Replace("@LastName", "'" + lname.Replace("'", "\'") + "'");
                qry = qry.Replace("@FirstName", "'" + fname.Replace("'", "''") + "'");
                qry = qry.Replace("@LastName", "'" + lname.Replace("'", "''") + "'");
                
                qry = qry.Replace("@facid", facilityid);
                //qry = qry.Replace("@isapproved", 1);
                //qry = qry.Replace("@islocked", 0);

                SqlCommand cmd = new SqlCommand(qry, cn);

                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "MergeRecs");
            }
            return ds;
        }

        public System.Data.DataSet GetAssetUrlByTopicID(string TopicID)
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "SELECT mcid, asseturl, description FROM Mediacontent WHERE (topicid = " + TopicID + ") order by description";
                SqlCommand cmd = new SqlCommand(qry, cn);

                //cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "AssetUrl");
            }
            return ds;
        }

        public System.Data.DataSet GetLicenseTypes()
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "SELECT  * FROM LicenseType";
                SqlCommand cmd = new SqlCommand(qry, cn);

                //cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "LicenseTypes");
            }
            return ds;

        }

        //Deletes only the User who do not have any Transcripts.   

        //1/27/2009-Swetha- Revised and set facility to 1 when a user deletes the trancript as per Zhongs suggestion

        public bool DeleteUser(int iID, int facid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_SwitchFacility";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@iID", SqlDbType.VarChar).Value = iID;
                cmd.Parameters.Add("@facid", SqlDbType.VarChar).Value = 1;


                try
                {
                    cn.Open();
                    int ret = 0;
                    ret = ExecuteNonQuery(cmd);
                    return true;
                }
                catch 
                {
                    return false;
                }
                finally
                {
                    if (cn.State == ConnectionState.Open)
                        cn.Close();
                    cn.Dispose();
                }
                //return true;
            }


        }


        ////Access Type

        // 11/20/2008 David Stevenson -- not sure if we need this one now

        //public override bool InsertUser(string username, string firstname, string middlename, string lastname, string title, string password, string address1, string address2, string city, string state, string zip, string homephone, string workphone, string birthdate, string badgeid, string ssn, string hiredate, string termindate, int deptid, int positionid, string pwdqs, string pwdans, string rolename, int facilityid)
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {
        //        cn.Open();
        //        SqlCommand cmd = cn.CreateCommand();
        //        SqlTransaction tran;

        //        tran = cn.BeginTransaction(IsolationLevel.ReadCommitted);
        //        cmd.Connection = cn;
        //        cmd.Transaction = tran;

        //        try
        //        {
        //            cmd.CommandText = "Insert into UsersInRoles(rolename,username,facilityid) Values (@rolename,@username,@facilityid)";
        //            cmd.Parameters.Clear();
        //            cmd.Parameters.Add("@rolename", SqlDbType.VarChar).Value = rolename;
        //            cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = username;
        //            cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
        //            ExecuteNonQuery(cmd);

        //            //cmd.CommandText = "Insert into UserAccount(cusername,cfirstname,cmiddle,clastname,cemail,caddress1,caddress2,ccity,cstate,czipcode,cphone,cpw,cbirthdate,pwquest,pwans,work_phone,badge_id,hire_date,termin_date,dept_id,position_id,title,uniqueid,"+
        //            //"cexpires,ladmin,lprimary,cfloridano,cinstitute,clectdate,cphone,cpromocode,crefcode,cregtype,csocial,cbirthdate,lcusername,"+
        //            //"passfmt,passalt,mobilepin,lcemail,pwquest,pwans,isapproved,isonline,islocked,xpwatt) Values (@username, @firstname, @middlename, @lastname,@email, @address1, @address2, @city, @state, @zip, @phone, @password, @birthdate, @pwquest, @pwans, @workphone, @badge_id, @hireDate, @termin_date, @dept_id, @position_id, @title, @uniqueID,"+
        //            //    "10/28/2008,0,0,1,1,1,1,1,1,1,0000,1,1,"+
        //            //"1,1,1,1,0,0,1,0,0,1)";

        //            cmd.CommandText = "INSERT INTO [ECE].[dbo].[UserAccount]([cusername],[cfirstname],[cmiddle],[clastname],[caddress1],[caddress2],[ccity],[cstate],[czipcode],[cemail],[cexpires],[ladmin],[lprimary],[cfloridano],[cinstitute],[clectdate],[cphone],[cpromocode],[promoid],[facilityid],[cpw],[crefcode],[cregtype],[csocial],[cbirthdate],[specid],[lcusername],[lastact],[passfmt],[passsalt],[mobilepin],[lcemail],[pwquest],[pwans],[isapproved],[isonline],[islocked],[lastlogin],[lastpwchg],[lastlock],[xpwatt],[xpwattst],[xpwansatt],[xpwansst],[comment],[created],[agegroup],[regtypeid],[repid],[work_phone],[badge_id],[hire_date],[termin_date],[dept_id],[position_id],[clinical_ind],[title],[giftcard_ind],[giftcard_chour],[giftcard_uhour],[uniqueid])VALUES" +
        //                  "('username','fn','cmiddle','ln','asd','dff','il','il',60156,'kk@CE.COM','10/28/2008',1,1,'fl','ins','elc','2246782234','prm',1,3197,'manju','er'" +
        //                           ",'ert','0000','rtty',1,'yser','10/28/2008','psee','pass','224','kk@CE.COM','ertt','ertyy',1,1,1,'10/28/2008','10/28/2008','10/28/2008',1,'10/28/2008',1,'10/28/2008','errty','10/28/2008',1,1,1,'','12344','10/28/2008','10/28/2008',1,1,0,'title',1,0.22,0.33,10)";
        //            //cmd.CommandText = "Insert into UserAccount(cusername,cfirstname,cmiddle,clastname,caddress1,caddress2,ccity,cstate,czipcode,cphone,cpw,cbirthdate,pwquest,pwans,work_phone,badge_id,hire_date,termin_date,dept_id,position_id,title,uniqueid)"+
        //            //    "Values (@username, @firstname, @middlename, @lastname, @address1, @address2, @city, @state, @zip, @phone, @password, @birthdate, @pwquest, @pwans, @workphone, @badge_id, @hireDate, @termin_date, @dept_id, @position_id, @title, @uniqueID)";

        //            cmd.Parameters.Clear();
        //            cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = username;
        //            cmd.Parameters.Add("@firstname", SqlDbType.VarChar).Value = firstname;
        //            cmd.Parameters.Add("@middlename", SqlDbType.VarChar).Value = middlename;
        //            cmd.Parameters.Add("@lastname", SqlDbType.VarChar).Value = lastname;
        //            cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = "swethaa@ce.com";
        //            cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = address1;
        //            cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = address2;
        //            cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = city;
        //            cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = state;
        //            cmd.Parameters.Add("@zip", SqlDbType.VarChar).Value = zip;
        //            cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = homephone;
        //            cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = password;
        //            cmd.Parameters.Add("@birthdate", SqlDbType.VarChar).Value = birthdate;
        //            cmd.Parameters.Add("@pwquest", SqlDbType.VarChar).Value = pwdqs;
        //            cmd.Parameters.Add("@pwans", SqlDbType.VarChar).Value = pwdans;
        //            cmd.Parameters.Add("@workphone", SqlDbType.VarChar).Value = workphone;
        //            cmd.Parameters.Add("@badge_id", SqlDbType.VarChar).Value = badgeid;
        //            cmd.Parameters.Add("@hireDate", SqlDbType.VarChar).Value = hiredate;
        //            cmd.Parameters.Add("@termin_date", SqlDbType.VarChar).Value = termindate;
        //            cmd.Parameters.Add("@dept_id", SqlDbType.Int).Value = deptid;
        //            cmd.Parameters.Add("@position_id", SqlDbType.Int).Value = positionid;
        //            cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = title;
        //            cmd.Parameters.Add("@uniqueID", SqlDbType.VarChar).Value = 12211;

        //            ExecuteNonQuery(cmd);

        //            tran.Commit();
        //            return true;
        //        }
        //        catch (Exception ex)
        //        {
        //            tran.Rollback();
        //            return false;
        //        }
        //        finally
        //        {
        //            if (cn.State == ConnectionState.Open)
        //                cn.Close();
        //            tran.Dispose();
        //            cn.Dispose();

        //        }
        //        return true;
        //    }
        //}
        
        public bool DeActivateUser(int userID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserAccount SET " +
                    "IsApproved='false' ," +
                    " IsLocked = 'true'" +
                    " where iID =@userID", cn);
                cmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;

                try
                {
                    cn.Open();
                    ExecuteNonQuery(cmd);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
        public bool DeActivateMutiUser(string userIDs)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(String.Format("update UserAccount SET " +
                    "IsApproved='false' ," +
                    " IsLocked = 'true'" +
                    " where iID in ({0})", userIDs), cn);
                try
                {
                    cn.Open();
                    ExecuteNonQuery(cmd);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
        public bool ActivateUser(int userID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserAccount SET " +
                    "IsApproved='true' ," +
                    " IsLocked ='false'" +
                    " where iID =@userID", cn);
                cmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;

                try
                {
                    cn.Open();
                    ExecuteNonQuery(cmd);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
        public int GetUserAccountsCount(string firstName, string lastName,
                string cUserName, int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct " +
                      "iID, cFirstName, cLastName, (select facname from facilitygroup where facid=u.facilityid) as facname, cUserName, " +
                      "IsApproved,Badge_ID, (select dept_name from dept where dept_id=u.dept_id) as Dept_Name, cSocial, giftcard_ind " +
                      "from UserAccount u";

                string whereClause = string.Empty;
                if (!String.IsNullOrEmpty(firstName))
                    whereClause += "or cFirstName like '" + firstName.Replace("'", "''") + "%' ";
                if (!String.IsNullOrEmpty(lastName))
                    whereClause += "or cLastName like '" + lastName.Replace("'", "''") + "%' ";
                if (!String.IsNullOrEmpty(cUserName))
                    whereClause += "or cUserName like '" + cUserName.ToLower().Replace("'", "''") + "%' ";
                //if (iID != null && iID > 0)
                // whereClause += "or iID like '" + iID + "%' ";
                //if (facilityid != null && facilityid > 0) //bsk
                if (facilityid > 0)
                    whereClause += "or u.facilityid = " + facilityid + " ";            
                if (!String.IsNullOrEmpty(badge_id))
                    whereClause += "or Badge_ID like '" + badge_id.Replace("'", "''") + "%' ";
                if (!String.IsNullOrEmpty(cSocial))
                    whereClause += "or cSocial like '" + cSocial.Replace("'", "''") + "%' ";
                //if (Dept_ID != null && Dept_ID > 0)//bsk
                if (Dept_ID > 0)
                    whereClause += "or u.dept_id = " + Dept_ID + " ";
                if (isApproved)
                    whereClause += "or IsApproved = 'true'";
                else if (isNotApproved)
                    whereClause += "or IsApproved = 'false'";

                if (whereClause != string.Empty)
                    whereClause = " where " + whereClause.Substring(2);

                cSQLCommand += whereClause;

                string sql = "select count(*) from (" + cSQLCommand + ") as temp";

                SqlCommand cmd = new SqlCommand(sql, cn);

                cn.Open();
                return Convert.ToInt32(ExecuteScalar(cmd));
            }
        }
       
        public List<UserAccountInfo1> GetUserAccountsBySearchCriteria(string cSortExpression, string firstName, string lastName, string cUserName,
            int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID, string rolename, int startRowIndex, int maximumRows)
        {
            string qry = "";
            string subqry = "";
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                if (facilityid != 0)
                {
                    subqry = "select distinct iID,cFirstName,cLastName,(select facname from FacilityGroup where facid=u.facilityid) as facname," +
                        "(select active from FacilityGroup where facid=u.facilityid) as factive," +
                       "(select dept_name from Dept where dept_id=u.dept_id and facilityid=u.facilityid) as Dept_Name," +
                       "(select Top 1 Roles.displayname from UsersInRoles INNER JOIN Roles ON UsersInRoles.rolename = Roles.rolename where username = u.lcusername and " +
                       "(UsersInRoles.facilityid = u.facilityid ) order by priorder) AS Roles," + "cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from UserAccount u where (u.facilityid = " + facilityid + ")";
                }
                else
                {
                    subqry = "select distinct iID,cFirstName,cLastName,(select facname from FacilityGroup where facid=u.facilityid) as facname," +
                        "(select active from FacilityGroup where facid=u.facilityid) as factive," +
                             "(select dept_name from Dept where dept_id=u.dept_id and facilityid=u.facilityid) as Dept_Name," +
                             "(select Top 1 Roles.displayname from UsersInRoles INNER JOIN Roles ON UsersInRoles.rolename = Roles.rolename where username = u.lcusername and " +
                             "(UsersInRoles.facilityid = u.facilityid ) order by priorder) AS Roles," + "cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from UserAccount u where (u.facilityid <> 0 )";


                }
                if (Dept_ID > 0)
                {
                    string appendDeptCond = " and (u.dept_id =" + Dept_ID + ")";
                    subqry += appendDeptCond;
                }
                if ((firstName.Length > 0) & (firstName != null))
                {
                    subqry += "and (u.cFirstName Like '" + firstName.Replace("'", "''") + "%')";
                }
                if ((lastName.Length > 0) & (lastName != null))
                {
                    subqry += "and (u.cLastName Like '" + lastName.Replace("'", "''") + "%')";
                }
                if ((cUserName.Length > 0) & (cUserName != null))
                {
                    subqry += "and (u.cUserName Like '" + cUserName.ToLower().Replace("'", "''") + "%')";
                }
                if ((cSocial.Length > 0) & (cSocial != null))
                {
                    subqry += "and (u.cSocial Like '" + cSocial.Replace("'", "''") + "%')";
                }

                if ((badge_id.Length > 0) & (badge_id != null))
                {
                    subqry += "and (u.Badge_ID Like '" + badge_id.Replace("'", "''") + "%')";
                }
                if ((isApproved))
                {
                    subqry += "and (u.IsApproved ='true')";
                }
                if ((isNotApproved))
                {
                    subqry += "and (u.IsApproved = 'false' )";
                }

                qry = "select iID,cFirstName,cLastName,facname,factive,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from (" + subqry + ") as result where (Roles <> ' ') ";

                //if ((rolename.Length > 1) & rolename != null)
                //{
                //    string appendRoleStr = " and ( Roles Like '" + rolename + "%')";
                //    qry += appendRoleStr;
                //}

                if ((rolename.Length > 1) & rolename != null & rolename != "All AccessTypes")
                {
                    string appendRoleStr = " and ( Roles Like '" + rolename + "%') Order by cLastName,cFirstName,iID,facname,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind ";
                    qry += appendRoleStr;
                }
                else
                {
                    string appendOrderStr = " Order by  cLastName,cFirstName,iID,facname,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind ";
                    qry += appendOrderStr;


                }
                SqlCommand cmd = new SqlCommand(qry, cn);
                //SqlParameter[] parms = new SqlParameter[] {
                //        new SqlParameter("sortExpression", cSortExpression),
                //        new SqlParameter("startRowIndex", startRowIndex),
                //        new SqlParameter("maximumRows", maximumRows)
                //};
                //cmd.Parameters.AddRange(parms);

                cn.Open();
                IDataReader reader = ExecuteReader(cmd);

                List<UserAccountInfo1> UserAccounts = new List<UserAccountInfo1>();
                while (reader.Read())
                {
                    UserAccountInfo1 UserAccount = new UserAccountInfo1(
               (int)reader["iID"],
               reader["cUserName"].ToString().ToLower(),
               reader["cFirstName"].ToString(),
               reader["cLastName"].ToString(),
               (Convert.ToString(reader["facname"])),
               (bool)reader["factive"],
               (bool)reader["IsApproved"],
               Convert.ToString(reader["Badge_ID"]),
               Convert.ToString(reader["Dept_name"]),
               Convert.ToString(reader["Roles"]),
               Convert.ToString(reader["cSocial"]),
               (reader["giftcard_ind"] != DBNull.Value ? (bool)reader["giftcard_ind"] : false)
                        //(int)(Convert.IsDBNull(reader["UniqueID"]) ?(int)0 : (int)reader["UniqueID"])
                        );

                    UserAccounts.Add(UserAccount);
                }
                return UserAccounts;
            }



        }
        public List<UserAccountInfo1> GetUserAccountsBySearchCriteria(string cSortExpression, string firstName, string lastName, string cUserName,
             int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID, string rolename, int startRowIndex, int maximumRows,
             int rolepriority, int currentUserID, bool isActiveFacilityManager)
        {
            string qry = "";
            string subqry = "";
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                if (facilityid != 0)
                {
                    subqry = "select distinct iID,cFirstName,cLastName,(select facname from FacilityGroup where facid=u.facilityid) as facname," +
                       "(select active from FacilityGroup where facid=u.facilityid) as factive," +
                       "(select dept_name from Dept where dept_id=u.dept_id and facilityid=u.facilityid) as Dept_Name," +
                       "(select Top 1 Roles.displayname from UsersInRoles INNER JOIN Roles ON UsersInRoles.rolename = Roles.rolename where username = u.lcusername and " +
                       "(UsersInRoles.facilityid = u.facilityid) and Roles.priorder <= '" + rolepriority + "') AS Roles," + "cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from UserAccount u where (u.facilityid = " + facilityid + ")";
                }
                else
                {
                    if (rolepriority == 4) // parent organization admin
                    {
                        subqry = "select distinct iID,cFirstName,cLastName,FG.Facname, FG.active as factive," +                                   
                                   "(select dept_name from Dept where dept_id=u.dept_id and facilityid=u.facilityid) as Dept_Name," +
                                   "(select Top 1 Roles.displayname from UsersInRoles INNER JOIN Roles ON UsersInRoles.rolename = Roles.rolename where username = u.lcusername and " +
                                   "(UsersInRoles.facilityid = u.facilityid ) and Roles.priorder <= '" + rolepriority + "') AS Roles," + "cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind " +
                                   " from UserAccount u INNER JOIN FacilityGroup FG ON u.FacilityID = FG.facid" +
                                     " where (u.facilityid <> 0 ) and ( FG.Facid in (select facilityid from useraccount where useraccount.iid = "+currentUserID+") or "+
                                     " FG.Parent_id in (select parent_id from FacilityGroup inner join useraccount on useraccount.facilityid=facilitygroup.facid"+
                                     " where useraccount.iid="+currentUserID+" and parent_id <>0))";

                    }
                    else
                    {
                        subqry = "select distinct iID,cFirstName,cLastName,(select facname from FacilityGroup where facid=u.facilityid) as facname," +
                                 "(select active from FacilityGroup where facid=u.facilityid) as factive," +
                                 "(select dept_name from Dept where dept_id=u.dept_id and facilityid=u.facilityid) as Dept_Name," +
                                 "(select Top 1 Roles.displayname from UsersInRoles INNER JOIN Roles ON UsersInRoles.rolename = Roles.rolename where username = u.lcusername and " +
                                 "(UsersInRoles.facilityid = u.facilityid ) and Roles.priorder <= '" + rolepriority + "') AS Roles," + "cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from UserAccount u where (u.facilityid <> 0 )";

                    }
                }
                if (Dept_ID > 0)
                {
                    string appendDeptCond = " and (u.dept_id =" + Dept_ID + ") ";
                    subqry += appendDeptCond;
                }
                else
                {
                    if (isActiveFacilityManager)
                        subqry += "and u.Dept_id IN (SELECT Dept.dept_id FROM Manage INNER JOIN Dept ON Manage.dept_id = Dept.dept_id " +
                                  "INNER JOIN UserAccount ON Manage.userid = UserAccount.iid " +
                                  "WHERE UserAccount.iID = " + currentUserID + ") ";
                    //else
                    //    subqry += "(SELECT Dept.[dept_id] FROM [Dept] Where " + (facilityid!=0 ? "facilityID=" + facilityid : "facilityID <> 0") + ") ";
                }
                if ((firstName.Length > 0) & (firstName != null))
                {
                    subqry += "and (u.cFirstName Like '" + firstName.Replace("'", "''") + "%') ";
                }
                if ((lastName.Length > 0) & (lastName != null))
                {
                    subqry += "and (u.cLastName Like '" + lastName.Replace("'", "''") + "%') ";
                }
                if ((cUserName.Length > 0) & (cUserName != null))
                {
                    subqry += "and (u.cUserName Like '" + cUserName.ToLower().Replace("'", "''") + "%') ";
                }
                if ((cSocial.Length > 0) & (cSocial != null))
                {
                    subqry += "and (u.cSocial Like '" + cSocial.Replace("'", "''") + "%') ";
                }

                if ((badge_id.Length > 0) & (badge_id != null))
                {
                    subqry += "and (u.Badge_ID Like '" + badge_id.Replace("'", "''") + "%') ";
                }
                if ((isApproved))
                {
                    subqry += "and (u.IsApproved ='true') ";
                }
                if ((isNotApproved))
                {
                    subqry += "and (u.IsApproved = 'false') ";
                }

                qry = "select iID,cFirstName,cLastName,facname,factive,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind from (" + subqry + ") as result where (Roles <> ' ') ";

                //if ((rolename.Length > 1) & rolename != null)
                //{
                //    string appendRoleStr = " and ( Roles Like '" + rolename + "%')";
                //    qry += appendRoleStr;
                //}

                if ((rolename.Length > 1) & rolename != null & rolename != "All AccessTypes")
                {
                    string appendRoleStr = " and ( Roles Like '" + rolename + "%') Order by cLastName,cFirstName,iID,facname,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind ";
                    qry += appendRoleStr;
                }
                else
                {
                    string appendOrderStr = " Order by  cLastName,cFirstName,iID,facname,Dept_Name,Roles,cUserName,IsApproved,Badge_ID,cSocial,giftcard_ind ";
                    qry += appendOrderStr;


                }
                SqlCommand cmd = new SqlCommand(qry, cn);
                //SqlParameter[] parms = new SqlParameter[] {
                //        new SqlParameter("sortExpression", cSortExpression),
                //        new SqlParameter("startRowIndex", startRowIndex),
                //        new SqlParameter("maximumRows", maximumRows)
                //};
                //cmd.Parameters.AddRange(parms);

                cn.Open();
                IDataReader reader = ExecuteReader(cmd);

                List<UserAccountInfo1> UserAccounts = new List<UserAccountInfo1>();
                while (reader.Read())
                {
                    UserAccountInfo1 UserAccount = new UserAccountInfo1(
               (int)reader["iID"],
               reader["cUserName"].ToString().ToLower(),
               reader["cFirstName"].ToString(),
               reader["cLastName"].ToString(),
               (Convert.ToString(reader["facname"])),
               (bool)reader["factive"],
               (bool)reader["IsApproved"],
               Convert.ToString(reader["Badge_ID"]),
               Convert.ToString(reader["Dept_name"]),
               Convert.ToString(reader["Roles"]),
               Convert.ToString(reader["cSocial"]),
               (reader["giftcard_ind"] != DBNull.Value ? (bool)reader["giftcard_ind"] : false)
                        //(int)(Convert.IsDBNull(reader["UniqueID"]) ?(int)0 : (int)reader["UniqueID"])
                        );

                    UserAccounts.Add(UserAccount);
                }
                return UserAccounts;
            }
        }

        public List<UserAccountInfo1> GetUsersBySearchCriteria(string cSortExpression, string firstName, string lastName, string cUserName,
             int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID, string rolename, int startRowIndex, int maximumRows,
             int rolepriority, int currentUserID, bool isActiveFacilityManager, ref int numResults)
        {
            string status;
            if (isApproved) status = "1";
            else if (isNotApproved) status = "0";
            else status = "";
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "sp_SearchUserByCriteria";
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = new SqlParameter[] {
                        new SqlParameter("@Status", status),
                        new SqlParameter("@LastName", lastName.Trim()),
                        new SqlParameter("@FirstName", firstName.Trim()),
                        new SqlParameter("@LoginID", cUserName.Trim()),
                        new SqlParameter("@FacilityID", facilityid),
                        new SqlParameter("@BadgeID", badge_id.Trim()),
                        new SqlParameter ("@DeptID", Dept_ID),
                        new SqlParameter("@Priorder", rolepriority),
                        new SqlParameter("@Roles", rolename),
                        new SqlParameter("@UniqueID", cSocial.Trim()),
                        new SqlParameter("@SortExpression", cSortExpression),
                        new SqlParameter("@StartRow", startRowIndex),
                        new SqlParameter("@MaximumRows", maximumRows),                        
                        new SqlParameter("@NumResults", numResults),
                        new SqlParameter("@CurrentUserID", currentUserID)
                };
                parameters[13].Direction = ParameterDirection.InputOutput;
                parameters[13].DbType = DbType.Int32;
                parameters[13].Size = 8;
                cmd.Parameters.AddRange(parameters);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                
                List<UserAccountInfo1> UserAccounts = new List<UserAccountInfo1>();
                while (reader.Read())
                {
                   UserAccountInfo1 UserAccount = new UserAccountInfo1(
                   (int)reader["iID"],
                   reader["cUserName"].ToString().ToLower(),
                   reader["cFirstName"].ToString(),
                   reader["cLastName"].ToString(),
                   (Convert.ToString(reader["facname"])),
                   (bool)reader["factive"],
                   (bool)reader["IsApproved"],
                   Convert.ToString(reader["Badge_ID"]),
                   Convert.ToString(reader["DeptName"]),
                   Convert.ToString(reader["RoleNames"]),
                   Convert.ToString(reader["cSocial"]),
                   (reader["giftcard_ind"] != DBNull.Value ? (bool)reader["giftcard_ind"] : false));

                   UserAccounts.Add(UserAccount);
                }
                reader.Close();
                numResults = Convert.ToInt32(cmd.Parameters["@NumResults"].Value.ToString());
                return UserAccounts;
            }
        }

       


        //public List<UserEnrollmentInfo> GetUserEnrollmentsBySearechCriteria(string firstname, string lastname, int facilityid, int deptid, int topicid,
        //    int startrow, int maximumrows, string sortexpression,ref int numResults
        //    )
        //{
        //  using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //  {
        //      SqlCommand cmd=new SqlCommand();
        //      cmd.CommandText = "sp_SearchEnrollmentsByCriteria";
        //      cmd.Connection = cn;
        //      cmd.CommandType = CommandType.StoredProcedure;

        //      SqlParameter [] parameters=new SqlParameter[]{
        //      new SqlParameter("@FirstName",firstname.Trim()),
        //      new SqlParameter("@LastName",lastname.Trim()),
        //      new SqlParameter("@FacilityID",facilityid),
        //      new SqlParameter("@Deptid",deptid),
        //      new SqlParameter("@Topicid",topicid),
        //      new SqlParameter("@StartRow",startrow),
        //      new SqlParameter("@MaximumRows",maximumrows),
        //      new SqlParameter("@SortExpression",sortexpression),
              
        //      new SqlParameter("@NumResults", numResults)
        //      };

        //      parameters[8].Direction= ParameterDirection.InputOutput;
            
        //      cmd.Parameters.AddRange(parameters);
        //      cn.Open();

        //      IDataReader reader=ExecuteReader(cmd);
        //      List<UserEnrollmentInfo> userEnrollments=new List<UserEnrollmentInfo>();

             

        //      while(reader.Read())
        //      {
        //          int complete = GetCompletePercentage((int)reader["testid"]);

        //          string Pcomplete = complete + "%";
        //          UserEnrollmentInfo userEnrollment=new UserEnrollmentInfo(
        //              (int)reader["iid"],
        //              (int)reader["testid"],
        //              reader["cFirstName"].ToString(),
        //              reader["cLastName"].ToString(),
        //              (int)reader["FacilityID"],
        //              reader["FacilityName"].ToString(),
        //              (int)reader["Dept_ID"],
        //              reader["DeptName"].ToString(),
        //              (int)reader["TopicID"],
        //              reader["TopicName"].ToString(),
        //              reader["CourseNumber"].ToString(),
        //              reader["DeadLine"].ToString(),
        //              Pcomplete
        //              );
                  
        //          userEnrollments.Add(userEnrollment);
        //      }

        //      numResults = Convert.ToInt32(cmd.Parameters["@NumResults"].Value.ToString());

        //      reader.Close();
          
        //      return userEnrollments;
        //  }
        //}

        //public string  GetCompletePercentage(int testid)
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {
        //        string sql = "SELECT  CONVERT(INTEGER,CONVERT(DECIMAL(3,0),SUM (CASE WHEN ip_status='C' THEN 1 ELSE 0 END)) /CONVERT(DECIMAL(3,0),COUNT(*))*100) AS '% Complete' FROM dbo.IceProgress WHERE testid=@testid";
        //        SqlCommand cmd2 = new SqlCommand(sql, cn);
        //        cmd2.Parameters.Add("@testid", SqlDbType.Int).Value = testid;
        //        cn.Open();

        //        IDataReader reader = ExecuteReader(cmd2);
        //        if (reader.Read())
        //        {
        //            string percent = Convert.ToString(reader["% Complete"]);
        //            if (percent == "")
        //                percent = "0";
        //            return percent + "%";
        //        }
        //        else
        //            return "0%";
        //    }
        //}

        //public DataSet GetStaffEnrollmentsBySearechCriteria(string firstname, string lastname, int facilityid, int deptid, int topicid,
        //    int startrow, int maximumrows, string sortexpression, ref int numResults
        //    ) 
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {
        //        SqlCommand cmd = new SqlCommand();
        //        cmd.CommandText = "sp_SearchStaffByCriteria";
        //        cmd.Connection = cn;
        //        cmd.CommandType = CommandType.StoredProcedure;

        //        SqlParameter[] parameters = new SqlParameter[]{
        //      new SqlParameter("@FirstName",firstname.Trim()),
        //      new SqlParameter("@LastName",lastname.Trim()),
        //      new SqlParameter("@FacilityID",facilityid),
        //      new SqlParameter("@Deptid",deptid),
          
        //      new SqlParameter("@StartRow",startrow),
        //      new SqlParameter("@MaximumRows",maximumrows),
        //      new SqlParameter("@SortExpression",sortexpression),
              
        //      new SqlParameter("@NumResults", numResults)
        //      };

        //        parameters[7].Direction = ParameterDirection.InputOutput;

        //        cmd.Parameters.AddRange(parameters);
        //        cn.Open();

        //        SqlDataAdapter ad = new SqlDataAdapter(cmd);
        //        DataSet ds = new DataSet();
        //        ad.Fill(ds,"staff");

        //        numResults = Convert.ToInt32(cmd.Parameters["@NumResults"].Value.ToString());
        //        cn.Close();

        //        return ds;
        //    }
        //}


        //    public List<UserEnrollmentInfo> GetStaffEnrollmentsBySearechCriteria(string firstname, string lastname, int facilityid, int deptid, int topicid,
        //    int startrow, int maximumrows, string sortexpression, ref int numResults
        //    )
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {
        //        SqlCommand cmd = new SqlCommand();
        //        cmd.CommandText = "sp_SearchStaffByCriteria";
        //        cmd.Connection = cn;
        //        cmd.CommandType = CommandType.StoredProcedure;

        //        SqlParameter[] parameters = new SqlParameter[]{
        //      new SqlParameter("@FirstName",firstname.Trim()),
        //      new SqlParameter("@LastName",lastname.Trim()),
        //      new SqlParameter("@FacilityID",facilityid),
        //      new SqlParameter("@Deptid",deptid),
          
        //      new SqlParameter("@StartRow",startrow),
        //      new SqlParameter("@MaximumRows",maximumrows),
        //      new SqlParameter("@SortExpression",sortexpression),
              
        //      new SqlParameter("@NumResults", numResults)
        //      };

        //        parameters[7].Direction = ParameterDirection.InputOutput;

        //        cmd.Parameters.AddRange(parameters);
        //        cn.Open();

        //        IDataReader reader = ExecuteReader(cmd);
        //        List<UserEnrollmentInfo> userEnrollments = new List<UserEnrollmentInfo>();
        //        while (reader.Read())
        //        {
        //            UserEnrollmentInfo userEnrollment = new UserEnrollmentInfo(
        //                (int)reader["iid"],
        //                 reader["cusername"].ToString(),
        //                reader["cFirstName"].ToString(),
        //                reader["cLastName"].ToString(),                       
        //                reader["DeptName"].ToString(),
        //                reader["email"].ToString(),
        //                (int)reader["facilityid"]
        //                );

        //            userEnrollments.Add(userEnrollment);
        //        }
        //        reader.Close();
        //        numResults = Convert.ToInt32(cmd.Parameters["@NumResults"].Value.ToString());
        //        return userEnrollments;
        //    }
        //}



        public List<UserAccountInfo1> GetCERetailUsersBySearchCriteria(string cSortExpression, string cfirstName, string clastName, string cUserName,
           string cpw, string cAddress1, string cEmail, string uniqueid, string cState, string State, string cZipCode, string verifNumber)
        {
            //string qry = "";
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_SearchRegUsers";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@cSortExpression", SqlDbType.VarChar).Value = cSortExpression;
                cmd.Parameters.Add("@cfirstName", SqlDbType.VarChar).Value = cfirstName.Replace("'", "\"");
                cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = clastName.Replace("'", "\"");
                cmd.Parameters.Add("@cUserName", SqlDbType.VarChar).Value = cUserName.Replace("'", "\"");
                cmd.Parameters.Add("@cpw", SqlDbType.VarChar).Value = cpw.Replace("'", "\"");
                cmd.Parameters.Add("@cAddress1", SqlDbType.VarChar).Value = cAddress1.Replace("'", "\"");
                cmd.Parameters.Add("@cEmail", SqlDbType.VarChar).Value = cEmail.Replace("'", "\"");
                cmd.Parameters.Add("@uniqueid", SqlDbType.VarChar).Value = uniqueid.Replace("'", "\"");
                cmd.Parameters.Add("@cState", SqlDbType.VarChar).Value = cState;
                cmd.Parameters.Add("@State", SqlDbType.VarChar).Value = State;
                cmd.Parameters.Add("@cZipCode", SqlDbType.VarChar).Value = cZipCode.Replace("'", "\"");
                cmd.Parameters.Add("@verifNumber", SqlDbType.VarChar).Value = verifNumber.Replace("'", "\"");
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);

                List<UserAccountInfo1> UserAccounts = new List<UserAccountInfo1>();
                while (reader.Read())
                {
                    string strdate = string.Empty;
                    if (reader["created"].ToString() != null)
                    {
                        strdate = reader["created"].ToString();
                        DateTime dt = Convert.ToDateTime(strdate);
                        strdate = dt.ToShortDateString();
                    }

                    UserAccountInfo1 UserAccount = new UserAccountInfo1(
               Convert.ToInt32(reader["iID"].ToString()),
               reader["cfirstName"].ToString(),
               reader["clastName"].ToString().ToLower(),
               reader["cUserName"].ToString(),
               reader["cpw"].ToString(),
               reader["cAddress1"].ToString(),
               reader["cEmail"].ToString(),
               reader["ccity"].ToString(),
               reader["cState"].ToString(),
               Convert.ToDateTime(reader["cexpires"].ToString()),
               Convert.ToDateTime(strdate),
               Convert.ToInt32(reader["uniqueid"].ToString()),
               reader["cZipCode"].ToString());


                    UserAccounts.Add(UserAccount);
                }

                return UserAccounts;
            }
        }

        public List<UserAccountInfo1> GetPearlsUsersBySearchCriteria(string cSortExpression, string cfirstName, string clastName,
                 string cUserName, string cpw, string cAddress1, string cEmail, string cState, string cZipCode, bool isApproved, bool isNotApproved)
        {
            string qry = "";
          
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                qry = "select top 200 iid,cfirstname,clastname,cusername,cpw,caddress1,cemail,ccity,cstate,czipcode,isapproved,cexpires,created from useraccount u where facilityid=3";


                if ((cfirstName.Length > 0) & (cfirstName != null))
                {
                    qry += "and (u.cFirstName Like '" + cfirstName.Replace("'", "''") + "%') ";
                }
                if ((clastName.Length > 0) & (clastName != null))
                {
                    qry += "and (u.cLastName Like '" + clastName.Replace("'", "''") + "%') ";
                }
                if ((cUserName.Length > 0) & (cUserName != null))
                {
                    qry += "and (u.cUserName Like '" + cUserName.ToLower().Replace("'", "''") + "%') ";
                }
                if ((cpw.Length > 0) & (cpw != null))
                {
                  qry += "and (u.cpw Like '" + cpw.Replace("'", "''") + "%') ";
                }

                if ((cAddress1.Length > 0) & (cAddress1 != null))
                {
                    qry += "and (u.cAddress1 Like '" + cAddress1.Replace("'", "''") + "%') ";
                }
                if ((cEmail.Length > 0) & (cEmail != null))
                {
                    qry += "and (u.cEmail Like '" + cEmail.Replace("'", "''") + "%') ";
                }
                if ((cState.Length > 0) & (cState != null))
                {
                    qry += "and (u.cState Like '" + cState.Replace("'", "''") + "%') ";
                }
                if ((cZipCode.Length > 0) & (cZipCode != null))
                {
                    qry += "and (u.cZipCode Like '" + cZipCode.Replace("'", "''") + "%') ";
                }
                if ((isApproved))
                {
                    qry += "and (u.IsApproved ='true') ";
                }
                if ((isNotApproved))
                {
                    qry += "and (u.IsApproved = 'false') ";
                }

                SqlCommand cmd = new SqlCommand(qry, cn);
                 
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);

                List<UserAccountInfo1> UserAccounts = new List<UserAccountInfo1>();
                while (reader.Read())
                {
                    UserAccountInfo1 UserAccount = new UserAccountInfo1(

                        (int)reader["iid"],
               reader["cfirstName"].ToString(),
               reader["clastName"].ToString(),
               reader["cUserName"].ToString(),
               reader["cpw"].ToString(),
               reader["cAddress1"].ToString(),
               reader["cEmail"].ToString(),
               reader["cCity"].ToString(),
               reader["cState"].ToString(),
               reader["cZipCode"].ToString(),
               String.IsNullOrEmpty(reader["created"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(reader["created"].ToString()),
               String.IsNullOrEmpty(reader["cexpires"].ToString())?DateTime.MinValue: Convert.ToDateTime(reader["cexpires"].ToString()),
               (bool)reader["IsApproved"]);

                    UserAccounts.Add(UserAccount);
                }
                return UserAccounts;
            }
        }

        public List<UserAccountInfo1> GetMicrositeUsersBySearchCriteria(string cSortExpression, string cfirstName, string clastName, string cUserName,
   string cpw, string cAddress1, string cEmail, string cState, string cZipCode)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_SearchMicrositeUsers";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@cSortExpression", SqlDbType.VarChar).Value = cSortExpression;
                cmd.Parameters.Add("@cfirstName", SqlDbType.VarChar).Value = cfirstName.Replace("'", "\"");
                cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = clastName.Replace("'", "\"");
                cmd.Parameters.Add("@cUserName", SqlDbType.VarChar).Value = cUserName.Replace("'", "\"");
                cmd.Parameters.Add("@cpw", SqlDbType.VarChar).Value = cpw.Replace("'", "\"");
                cmd.Parameters.Add("@cAddress1", SqlDbType.VarChar).Value = cAddress1.Replace("'", "\"");
                cmd.Parameters.Add("@cEmail", SqlDbType.VarChar).Value = cEmail.Replace("'", "\"");
                cmd.Parameters.Add("@cState", SqlDbType.VarChar).Value = cState;
                cmd.Parameters.Add("@cZipCode", SqlDbType.VarChar).Value = cZipCode.Replace("'", "\"");

                cn.Open();
                IDataReader reader = ExecuteReader(cmd);

                List<UserAccountInfo1> UserAccounts = new List<UserAccountInfo1>();
                while (reader.Read())
                {
                    UserAccountInfo1 UserAccount = new UserAccountInfo1(
                        (int)reader["iid"],
                        reader["cfirstName"].ToString(),
                        reader["clastName"].ToString(),
                        reader["cUserName"].ToString(),
                        reader["cpw"].ToString(),
                        reader["cAddress1"].ToString(),
                        reader["cEmail"].ToString(),
                        reader["cCity"].ToString(),
                        reader["cState"].ToString(),
                        reader["cZipCode"].ToString(),
                        String.IsNullOrEmpty(reader["created"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(reader["created"].ToString()),
                        String.IsNullOrEmpty(reader["cexpires"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(reader["cexpires"].ToString()));

                    UserAccount.UniqueID = Convert.ToInt32(reader["uniqueid"]);
                    //UserAccount.FacilityName = reader["domain_name"].ToString();
                    UserAccounts.Add(UserAccount);
                }
                return UserAccounts;
            }
        }
        

        public System.Data.DataSet GetNonUnlimitedOrders(string cSortExpression, string cfirstName, string clastName, string cUserName,
         string cpw, string cAddress1, string cEmail, string license_number, string cState, string state, string cZipCode)
        {
            using (SqlConnection sqlConn = new SqlConnection(this.ConnectionString))
            {
                string sql = "select distinct firstname,lastname,orderid,city,o.state,zip,orderdate from orders o " +
                "inner join useraccount u on o.userid=u.iid inner join userlicense ul on u.iid = ul.userid where u.facilityid=2 ";

                if (!String.IsNullOrEmpty(cfirstName))
                {
                    sql += " and o.firstname like '" + cfirstName.Replace("'", "''") + "%' ";
                }
                if (!String.IsNullOrEmpty(clastName))
                {
                    sql += " and o.lastname like '" + clastName.Replace("'", "''") + "%' ";
                }
                if (!String.IsNullOrEmpty(cUserName))
                {
                    sql += " and u.cUserName like '" + cUserName.Replace("'", "''") + "%' ";
                }
                if (!String.IsNullOrEmpty(cpw))
                {
                    sql += " and u.cpw like '" + cpw.Replace("'", "''") + "%' ";
                }
                if (!String.IsNullOrEmpty(cAddress1))
                {
                    sql += " and u.cAddress1 like '" + cAddress1.Replace("'", "''") + "%' ";
                }
                if (!String.IsNullOrEmpty(cEmail))
                {
                    sql += " and u.cEmail like '" + cEmail.Replace("'", "''") + "%' ";
                }
                if (!String.IsNullOrEmpty(license_number))
                {
                    sql += " and ul.license_number like '" + license_number.Replace("'", "''") + "%' ";
                }
                if (!String.IsNullOrEmpty(cState))
                {
                    sql += " and o.state like '" + cState + "%' ";
                }
                if (!String.IsNullOrEmpty(state))
                {
                    sql += " and ul.state like '" + state + "%' ";
                }
                if (!String.IsNullOrEmpty(cZipCode))
                {
                    sql += " and o.zip like '" + cZipCode.Replace("'", "''") + "%' ";
                }
                sql += " order by lastname,firstname ";
                sqlConn.Open();
                SqlCommand sqlSelect = new SqlCommand(sql, sqlConn);
                sqlSelect.CommandType = System.Data.CommandType.Text;
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlSelect);
                DataSet myDataset = new DataSet();
                sqlAdapter.Fill(myDataset);
                sqlConn.Close();
                return myDataset;
            }
        }
        //inner join histtranscript ht on ht.transcriptid = t.testid

        public System.Data.DataSet GetTranscripts(string cSortExpression, string cfirstName, string clastName, string cUserName,
         string cpw, string cAddress1, string cEmail, string uniqueid, string cState, string state, string cZipCode)
        {
            using (SqlConnection sqlConn = new SqlConnection(this.ConnectionString))
            {
                //string sql = "select distinct cfirstname,iid,clastname,testid,course_number,name,lastmod from Test t " +
                //             "inner join topic tp on tp.topicid = t.topicid " +
                //             "inner join useraccount u on u.iid = t.userid inner join userlicense ul  on u.iid = ul.userid " +
                //             "where u.facilityid = 2" ;

                string sql = "select top 200 transcriptid,itemid,firstname,lastname,datecomplete,rnstate,rnnum,rnstate2,rnnum2,coursename,coursenum, credits from histtranscript where itemid=0 ";

                if (!String.IsNullOrEmpty(cfirstName))
                {
                    sql += " and firstname like '" + cfirstName.Replace("'", "\"") + "%' ";
                }
                if (!String.IsNullOrEmpty(clastName))
                {
                    sql += " and lastname like '" + clastName.Replace("'", "\"") + "%' ";
                }
                if (!String.IsNullOrEmpty(cUserName))
                {
                    //sql += " and u.cUserName like '" + cUserName + "%' ";
                }
                if (!String.IsNullOrEmpty(cpw))
                {
                    //sql += " and u.cpw like '" + cpw + "%' ";
                }
                if (!String.IsNullOrEmpty(cAddress1))
                {
                    //    sql += " and u.cAddress1 like '" + cAddress1 + "%' ";
                }
                if (!String.IsNullOrEmpty(cEmail))
                {
                    //    sql += " and u.cEmail like '" + cEmail + "%' ";
                }

                if (!String.IsNullOrEmpty(state))
                {
                    sql += " and ((rnstate Like '" + state.Replace("'", "\"") + "%') or (rnstate2 Like '" + state.Replace("'", "\"") + "%')) ";
                }
                if (!String.IsNullOrEmpty(uniqueid))
                {
                    //sql += " and ((rnnum Like '" + license_number.Replace("'", "\"") + "%') or (rnnum2 Like '" + license_number.Replace("'", "\"") + "%')) ";
                }
                //if (!String.IsNullOrEmpty(license_number))
                //{
                //    sql += " and ul.license_number like '" + license_number + "%' ";
                //}
                if (!String.IsNullOrEmpty(cState))
                {
                    //   sql += " and cstate like '" + cState + "%' ";
                }
                //if (!String.IsNullOrEmpty(state))
                //{
                //    sql += " and ul.state like '" + state + "%' ";
                //}
                if (!String.IsNullOrEmpty(cZipCode))
                {
                    //  sql += " and czipcode like '" + cZipCode + "%' ";
                }
                sql += " order by lastname,firstname ";
                sqlConn.Open();
                SqlCommand sqlSelect = new SqlCommand(sql, sqlConn);
                sqlSelect.CommandType = System.Data.CommandType.Text;
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlSelect);
                DataSet myDataset = new DataSet();
                sqlAdapter.Fill(myDataset);
                sqlConn.Close();
                return myDataset;
            }
        }

        public System.Data.DataSet GetDuplicatesByFirstAndLastname(string facilityid)
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "SELECT F.clastname + ',' + F.cfirstname AS FullName, F.cfirstname, F.clastname,F.facilityid, " +
                 "(SELECT Count(e.cfirstname) FROM UserAccount e Inner JOIN Test ON Test.userid =e.iid " +
                 " WHERE (e.cfirstname = F.cfirstname) AND (e.cfirstname + ' ' +e. clastname <> '') AND (e.cfirstname + ' ' + e.clastname <> '. .') " +
                 " AND (e.clastname = F.clastname)  AND (e.iid = iID) AND (e.facilityid = @facid ) " +
                 " ) as ActualCount, COUNT(F.cfirstname) AS OCR FROM UserAccount F WHERE (F.cfirstname + ' ' +  F.clastname <> '') " +
                 " AND (F.cfirstname + ' ' + F.clastname <> '. .') and (F.facilityid = @facid ) " +
                 " GROUP BY F.cfirstname, F.clastname, F.facilityid HAVING (COUNT(F.cfirstname) >= 2) ORDER BY F.clastname,F.cfirstname";

                qry = qry.Replace("@facid", facilityid);

                SqlCommand cmd = new SqlCommand(qry, cn);

                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "Duplicates");
            }
            return ds;

        }
        public System.Data.DataSet GetDuplicatesByUniqueid(string facilityid)
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "SELECT F.clastname + ',' + F.cfirstname AS FullName, F.cfirstname, F.clastname,F.facilityid, " +
                       "(SELECT Count(e.cfirstname) FROM UserAccount e Inner JOIN Test ON Test.userid =e.iid " +
                       " WHERE (e.cfirstname = F.cfirstname) AND (e.cfirstname + ' ' +e. clastname <> '') AND (e.cfirstname + ' ' + e.clastname <> '. .')  " +
                       " AND (e.clastname = F.clastname)  AND (e.iid = iID) AND (e.facilityid = @facid )  " +
                       " ) as ActualCount, F.cSocial, COUNT(F.cfirstname) AS OCR FROM UserAccount F WHERE (F.cfirstname + ' ' +  F.clastname <> '') " +
                       " AND (F.cfirstname + ' ' + F.clastname <> '. .') and (F.facilityid = @facid ) " +
                       " GROUP BY F.cSocial,F.cfirstname, F.clastname,F.facilityid HAVING (COUNT(F.cSocial) >= 2) ORDER BY F.clastname,F.cfirstname ";

                qry = qry.Replace("@facid", facilityid);

                SqlCommand cmd = new SqlCommand(qry, cn);

                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "Duplicates");
            }
            return ds;

        }

        protected virtual UserAccountInfo1 GetUserAccount1FromReader(IDataReader reader)
        {
            return GetUserAccount1FromReader(reader, true);
        }
        protected virtual UserAccountInfo1 GetUserAccount1FromReader(IDataReader reader, bool readMemos)
        {
            UserAccountInfo1 UserAccount = null;
            while (reader.Read())
            {
                UserAccount = new UserAccountInfo1(
                 (int)reader["iID"],
                 reader["cAddress1"].ToString(),
                 reader["cAddress2"].ToString(),
                 (bool)reader["lAdmin"],
                 (bool)reader["lPrimary"],
                 reader["cBirthDate"].ToString(),
                 reader["cCity"].ToString(),
                 reader["cEmail"].ToString(),
                 reader["cExpires"].ToString(),
                 reader["cFirstName"].ToString(),
                 reader["cFloridaNo"].ToString(),
                 reader["cInstitute"].ToString(),
                 reader["cLastName"].ToString(),
                 reader["cLectDate"].ToString(),
                 reader["cMiddle"].ToString(),
                 reader["cPhone"].ToString(),
                 reader["cPromoCode"].ToString(),
                 (int)(Convert.IsDBNull(reader["PromoID"]) ? (int)0 : (int)reader["PromoID"]),
                 (int)(Convert.IsDBNull(reader["FacilityID"]) ? (int)0 : (int)reader["FacilityID"]),
                 reader["cPW"].ToString(),
                 reader["cRefCode"].ToString(),
                 reader["cRegType"].ToString(),
                 reader["cSocial"].ToString(),
                 reader["cState"].ToString(),
                 reader["cUserName"].ToString().ToLower(),
                 reader["cZipCode"].ToString(),
                (int)(Convert.IsDBNull(reader["SpecID"]) ? (int)0 : (int)reader["SpecID"]),
                 reader["LCUserName"].ToString().ToLower(),
                 (DateTime)(Convert.IsDBNull(reader["LastAct"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastAct"]),
                 reader["PassFmt"].ToString(),
                 reader["PassSalt"].ToString(),
                 reader["MobilePIN"].ToString(),
                 reader["LCEmail"].ToString(),
                 reader["PWQuest"].ToString(),
                 reader["PWAns"].ToString().ToLower(),
                 (bool)reader["IsApproved"],
                 (bool)reader["IsOnline"],
                 (bool)reader["IsLocked"],
                 (DateTime)(Convert.IsDBNull(reader["LastLogin"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLogin"]),
                 (DateTime)(Convert.IsDBNull(reader["LastPWChg"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastPWChg"]),
                (DateTime)(Convert.IsDBNull(reader["LastLock"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLock"]),
                 (int)reader["XPWAtt"],
                 (DateTime)(Convert.IsDBNull(reader["XPWAttSt"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAttSt"]),
                 (int)reader["XPWAnsAtt"],
                 (DateTime)(Convert.IsDBNull(reader["XPWAnsSt"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAnsSt"]),
                 (Convert.IsDBNull(reader["Comment"]) ? string.Empty : reader["Comment"].ToString()),
                 (DateTime)(Convert.IsDBNull(reader["Created"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Created"]),
                 (int)(Convert.IsDBNull(reader["AgeGroup"]) ? (int)0 : (int)reader["AgeGroup"]),
                 (int)(Convert.IsDBNull(reader["RegTypeID"]) ? (int)0 : (int)reader["RegTypeID"]),
                 (int)(Convert.IsDBNull(reader["RepID"]) ? (int)0 : (int)reader["RepID"]),
                 (Convert.IsDBNull(reader["Work_Phone"]) ? string.Empty : reader["Work_Phone"].ToString()),
                 (Convert.IsDBNull(reader["Badge_ID"]) ? string.Empty : reader["Badge_ID"].ToString()),
                 (DateTime)(Convert.IsDBNull(reader["Hire_Date"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Hire_Date"]),
                 (DateTime)(Convert.IsDBNull(reader["Termin_Date"])
                     ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Termin_Date"]),
                 (int)(Convert.IsDBNull(reader["Dept_ID"]) ? (int)0 : (int)reader["Dept_ID"]),
                 (int)(Convert.IsDBNull(reader["Position_ID"]) ? 0 : (int)reader["Position_ID"]),
                 (int)(Convert.IsDBNull(reader["Clinical_Ind"]) ? 0 : Convert.ToInt32(reader["Clinical_Ind"])),
                 (Convert.IsDBNull(reader["Title"]) ? string.Empty : reader["Title"].ToString()),
                 (bool)(Convert.IsDBNull(reader["Giftcard_Ind"]) ? false : reader["Giftcard_Ind"]),
                 (decimal)(Convert.IsDBNull(reader["Giftcard_Chour"]) ? 0 : (decimal)reader["Giftcard_Chour"]),
                 (decimal)(Convert.IsDBNull(reader["Giftcard_Uhour"]) ? 0 : (decimal)reader["Giftcard_Uhour"]),
                 (int)(Convert.IsDBNull(reader["UniqueID"]) ? 0 : (int)reader["UniqueID"]),
                 (bool)reader["FirstLogin_Ind"]);
                break;
            }

            return UserAccount;
        }

     

        public List<UserAccountInfo1> GetUserAccountsByDeptName(string dept_name, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select a.iid,a.cusername,a.cfirstname,a.clastname,a.facilityid from UserAccount a " +
                                    "INNER JOIN Manage b ON a.iid = b.userid " +
                                    "INNER JOIN Dept c ON b.dept_id = c.dept_id WHERE c.dept_name = @dept_name";
                //string cSQLCommand = "Select iid,cusername,cfirstname,clastname,facilityid from UserAccount" +
                //    "where iid=(select userid from Manage" +
                //    "where dept_id = (select dept_id from dept where dept_name ='@dept_name'))";
                //add on Order By if provided

                if (cSortExpression.Length > 0)
                {

                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@dept_name", SqlDbType.NVarChar).Value = dept_name;
                cn.Open();
                return GetUserAccountCollectionFromReader1(ExecuteReader(cmd), false);
            }
        }


        public void UpdatePrimaryDepartmentForUserID(int deptID, int userID)
        {
            using (SqlConnection conn = new SqlConnection(this.ConnectionString))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);

                // Must assign both transaction object and connection
                // to Command object for a pending local transaction
                command.Connection = conn;
                command.Transaction = transaction;

                try
                {

                    command.CommandText = "DELETE FROM Manage WHERE UserID=@UserID and Dept_ID = (Select dept_id from UserAccount where iID =@UserID) ";
                    command.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
                    //command.Parameters.Add("@DeptID", SqlDbType.Int).Value = deptID;
                    command.ExecuteNonQuery();

                    command.CommandText = "Update UserAccount Set dept_id =  @DeptID where iID = @UserID";
                    command.Parameters.Clear();
                    command.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@DeptID", SqlDbType.Int));
                    command.Parameters[0].Value = userID;
                    command.Parameters[1].Value = deptID;
                    command.ExecuteNonQuery();


                    command.CommandText = "INSERT INTO Manage(UserID, Dept_ID) VALUES(@UserID, @DeptID)";
                    command.Parameters.Clear();
                    command.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int));
                    command.Parameters.Add(new SqlParameter("@DeptID", SqlDbType.Int));
                    command.Parameters[0].Value = userID;
                    command.Parameters[1].Value = deptID;
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    return;
                }
                finally
                {
                    transaction.Dispose();
                }
            }
        }
        ///Returns mgr name by topidID
        ///
        ///
        public string GetMgrNameByTestID(int TestID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("SELECT A.cusername FROM dbo.UserAccount A, test T WHERE  A.iid=T.uniqueid AND T.testid=@testid",cn);
                cmd.Parameters.Add("@testid",SqlDbType.Int).Value=TestID;

                cn.Open();

                IDataReader dr = ExecuteReader(cmd);
                if (dr.Read())
                {
                    return Convert.ToString(dr["cusername"]);
                }
                else
                    return "";
            }
        }
        /// Returns the taxrate for UserId
        /// 
        public  decimal GetTaxRateByUserId(int iID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select cstate from useraccount where uniqueid=@iID", cn);
                cmd.Parameters.Add("@iID", SqlDbType.Int).Value = iID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    using (SqlConnection Retailcn = new SqlConnection(this.ConnectionString))
                    {
                        SqlCommand cmd1 = new SqlCommand("select taxrate from StateReq where stateabr=@state", Retailcn);
                        cmd1.Parameters.Add("@state", SqlDbType.VarChar).Value = reader[0].ToString();
                        Retailcn.Open();
                        IDataReader reader1 = ExecuteReader(cmd1);
                        if (reader1.Read())
                        {
                            return (decimal)reader1[0];
                        }
                        else
                        {
                            return (decimal)-1.0;
                        }
                    }
                }
                else
                {
                    return (decimal)-1.0;
                }

            }
        }

        public string GetEmailByUserid(int iid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select cemail from useraccount where iid=@iid", cn);
                cmd.Parameters.Add("@iid", SqlDbType.Int).Value = iid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    string email = Convert.ToString(reader["cemail"]);
                    return email;
                }
                else
                    return null;

            }
        }

        

        /// Return useraccount for uniqueid
        /// 

        public UserAccountInfo GetUserAccountByUniqueid(int uniqueid)
        {       
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select " +
                    "iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cEmail, " +
                    "cExpires, cFirstName, cFloridaNo, " +
                    "cInstitute, cLastName, cLectDate, cMiddle, cPhone, cPromoCode, PromoID, FacilityID, " +
                    "cPW, cRefCode, cRegType, cSocial, cState, cUserName, cZipCode, " +
                    "SpecID, LCUserName, " +
                    "LastAct, PassFmt, PassSalt, MobilePIN, LCEmail, PWQuest, PWAns, " +
                    "IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg, LastLock, " +
                    "XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, Comment, Created, AgeGroup, " +
                    "RegTypeID, RepID,Work_Phone, Badge_ID, " +
                    "Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, " +
                    "Giftcard_Ind, Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind " +
                    "from UserAccount where uniqueid=@uniqueid", cn);
                cmd.Parameters.Add("@uniqueid", SqlDbType.Int).Value = uniqueid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetUserAccountFromReader(reader, true);
                else
                    return null;
            }
        }
        /// <summary>
        /// Parameter format for roles: role1,role2,role3
        /// </summary>
        /// <param name="Facilityid"></param>
        /// <param name="roles"></param>
        /// <param name="csortExpression"></param>
        /// <returns></returns>
        public System.Data.DataSet GetUserNameWithRoleByFacilityId(string Facilityid, string roles, string csortExpression)
        {
            string[] RoleNames = roles.Replace("'", "").Split(new Char[] { ',' });
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "select iid,useraccount.cfirstname + ' ' + useraccount.clastname as UserName, cusername,rolename,cfirstname,clastname, cemail, lprimary from useraccount join usersinroles on " +
                    "useraccount.lcusername=usersinroles.username where usersinroles.rolename in ('" + String.Join("','", RoleNames) + "') and useraccount.facilityid=" + Facilityid;
                if (string.IsNullOrEmpty(csortExpression))
                {
                    qry = qry + " order by cusername ";
                }
                else
                {
                    qry = qry + " order by " + csortExpression;
                }

                SqlCommand cmd = new SqlCommand(qry, cn);
                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "GetUser");
            }
            return ds;
        }


        

        public System.Data.DataSet GetUserNameWithRoleByFacilityId(string Facilityid, string[] roles, string csortExpression)
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "select iid,useraccount.cfirstname + ', ' + useraccount.clastname as UserName, cusername,rolename,cfirstname,clastname, cemail, lprimary from useraccount join usersinroles on " +
                    "useraccount.lcusername=usersinroles.username where usersinroles.rolename in ('" + String.Join("','", roles) + "') and useraccount.facilityid=" + Facilityid;
                if (string.IsNullOrEmpty(csortExpression))
                {
                    qry = qry + " order by cusername ";
                }
                else
                {
                    qry = qry + " order by " + csortExpression;
                }

                SqlCommand cmd = new SqlCommand(qry, cn);
                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "GetUser");
            }
            return ds;
        }
        public System.Data.DataSet GetUserNameWithRoleByFacilityId(string Facilityid, string csortExpression)
        {
            System.Data.DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "select iid,cusername,rolename,cfirstname,clastname, cemail, lprimary from useraccount join usersinroles on " +
                    "useraccount.lcusername=usersinroles.username where useraccount.facilityid=" + Facilityid  ;
                if (csortExpression == string.Empty)
                {
                    qry = qry + " order by cusername ";
                }
                else
                {
                    qry = qry + " order by " + csortExpression;
                }

                SqlCommand cmd = new SqlCommand(qry, cn);
                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds,"GetUser");
            }
            return ds;
        }

        public void UpdateEmailByUserID(int userid, string email)
        {
            using (SqlConnection conn = new SqlConnection(this.ConnectionString))
            {
                try
                {
                    string cSQLCommand = "Update Useraccount Set cemail=@email where iid=@userid";
                    SqlCommand cmd = new SqlCommand(cSQLCommand, conn);
                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = email;
                    cmd.Parameters.Add("@userid", SqlDbType.VarChar).Value = userid;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch
                {

                }
            }
        }

        public Boolean IsUnlimtedByUserId(int UserId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                String qry = "select * from UserAccount where iid=@UserId and ladmin= 1 and cexpires >= getdate()";
                SqlCommand cmd = new SqlCommand(qry, cn);
                cn.Open();
                cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
                IDataReader reader = ExecuteReader(cmd,CommandBehavior.SingleRow);
                if (reader.Read())
                    return true;
                else
                    return false;
            }
        }

        public int DailyPRUserAccountExpireDate()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "Update useraccount set cexpires=CONVERT(VARCHAR(10),DATEADD(day,4,GETDATE()),101) where iid in (select userid from payment where renew=1 and active=1 and " +
                    "  ExpDate between GETDATE() and DATEADD(day,1,GETDATE())) and facilityid=3";

                SqlCommand cmd = new SqlCommand(qry, cn);
                cn.Open();
                return (int)cmd.ExecuteNonQuery();
                    
            }
           
        }

        public DataSet GetPRUserAccountExpireDate()
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "select cemail, iid,EXPDate,CCLast from useraccount U inner join payment P on U.iid=P.userid and active=1 and renew=1 " +
                    " where ExpDate between DATEADD(day,2,GETDATE()) and DATEADD(day,3,GETDATE()) and U.facilityid=3";
                SqlCommand cmd = new SqlCommand(qry, cn);
                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds);
                
            }
            return ds;
        }

        public DataSet GetPRUserAccountExpireDateWithoutRenew()
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string qry = "select cemail, iid,EXPDate,CCLast,ProfileID from useraccount U inner join payment P on U.iid=P.userid and renew=0 " +
                    " where ExpDate between DATEADD(day,2,GETDATE()) and DATEADD(day,3,GETDATE()) and U.facilityid=3";
                SqlCommand cmd = new SqlCommand(qry, cn);
                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds);

            }
            return ds;
        }
        
        public DataSet GetFollowupUserAccount()
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_get_followup_user_account", cn);
                cmd.CommandType = CommandType.StoredProcedure;        
                cn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }

        public DataSet GetFollowupReminderUserAccount()
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_get_followup_reminder_user_account", cn);
                cmd.CommandType = CommandType.StoredProcedure;  
                cn.Open();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds);

            }
            return ds;
        }

        //public bool DeleteEnrollment(int testid, int topicid)
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {
        //        SqlCommand cmd = new SqlCommand("sp_DeleteEnrollment",cn);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.Add("@testid", SqlDbType.Int).Value = testid;
        //        cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
        //        cn.Open();

        //        int ret = ExecuteNonQuery(cmd);
        //        return (ret==1);

        //    }
        //}

        //public string GetEnrollmentStatus(int topicid, int facilityid, int iid)
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {
        //      string result;
        //      string sql = "SELECT CASE WHEN EXISTS (SELECT 1 FROM test  WHERE test.topicid=@topicid AND test.facilityid=@facilityid AND test.userid=@iid)THEN 'yes' ELSE 'no' END AS enrolled";
        //      SqlCommand cmd = new SqlCommand(sql,cn);
        //      cn.Open();
        //      cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
        //      cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
        //      cmd.Parameters.Add("@iid", SqlDbType.Int).Value = iid;

        //      IDataReader reader = ExecuteReader(cmd);

        //      if (reader.Read())

        //          result = Convert.ToString(reader["enrolled"]);

        //      else result = "no";

        //      return result;
        //    }
        //}

        //public int GetTestid(int topicid, int facilityid, int iid)
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {
        //        int result;
        //        string sql = "SELECT testid FROM test  WHERE test.topicid=@topicid AND test.facilityid=@facilityid AND test.userid=@iid";
        //        SqlCommand cmd = new SqlCommand(sql, cn);
        //        cn.Open();
        //        cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
        //        cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
        //        cmd.Parameters.Add("@iid", SqlDbType.Int).Value = iid;

        //        IDataReader reader = ExecuteReader(cmd);

        //        if (reader.Read())

        //            result = (int)reader["testid"];
        //        else
        //            return 0;

        //        return result;
        //    }
        //}

     
        #endregion

        #region PRProvider


        /////////////////////////////////////////////////////////
        // methods that work with UserAccount
       
        /// <summary>
        /// Returns a new UserAccountInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual UserAccountInfo GetUserAccountFromReader(IDataReader reader)
        {
            return GetUserAccountFromReader(reader, true);
        }
        protected virtual UserAccountInfo GetUserAccountFromReader(IDataReader reader, bool readMemos)
        {
            try
            {
                UserAccountInfo UserAccount = new UserAccountInfo(
                  (int)reader["iID"],
                  reader["cAddress1"].ToString(),
                  reader["cAddress2"].ToString(),
                  (bool)reader["lAdmin"],
                  (bool)reader["lPrimary"],
                  reader["cBirthDate"].ToString(),
                  reader["cCity"].ToString(),
                  reader["cEmail"].ToString(),
                 String.IsNullOrEmpty(reader["cExpires"].ToString()) ? DateTime.MinValue.ToShortDateString() : reader["cExpires"].ToString(),
                  reader["cFirstName"].ToString(),
                  reader["cFloridaNo"].ToString(),
                  reader["cInstitute"].ToString(),
                  reader["cLastName"].ToString(),
                  reader["cLectDate"].ToString(),
                  reader["cMiddle"].ToString(),
                  reader["cPhone"].ToString(),
                  reader["cPromoCode"].ToString(),
                  (int)(Convert.IsDBNull(reader["PromoID"]) ? (int)0 : (int)reader["PromoID"]),
                  (int)(Convert.IsDBNull(reader["FacilityID"]) ? (int)0 : (int)reader["FacilityID"]),
                  reader["cPW"].ToString(),
                  reader["cRefCode"].ToString(),
                  reader["cRegType"].ToString(),
                  reader["cSocial"].ToString(),
                  reader["cState"].ToString(),
                  reader["cUserName"].ToString().ToLower(),
                  reader["cZipCode"].ToString(),
                  (int)(Convert.IsDBNull(reader["SpecID"]) ? (int)0 : (int)reader["SpecID"]),
                  reader["LCUserName"].ToString().ToLower(),
                  (DateTime)(Convert.IsDBNull(reader["LastAct"])
                      ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastAct"]),
                  reader["PassFmt"].ToString(),
                  reader["PassSalt"].ToString(),
                  reader["MobilePIN"].ToString(),
                  reader["LCEmail"].ToString(),
                  reader["PWQuest"].ToString(),
                  reader["PWAns"].ToString(),
                  (bool)reader["IsApproved"],
                  (bool)reader["IsOnline"],
                  (bool)reader["IsLocked"],
                  (DateTime)(Convert.IsDBNull(reader["LastLogin"])
                      ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLogin"]),
                  (DateTime)(Convert.IsDBNull(reader["LastPWChg"])
                      ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastPWChg"]),
                 (DateTime)(Convert.IsDBNull(reader["LastLock"])
                      ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLock"]),
                  (int)reader["XPWAtt"],
                  (DateTime)(Convert.IsDBNull(reader["XPWAttSt"])
                      ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAttSt"]),
                  (int)reader["XPWAnsAtt"],
                  (DateTime)(Convert.IsDBNull(reader["XPWAnsSt"])
                      ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAnsSt"]),
                  reader["Comment"].ToString(),
                  (DateTime)(Convert.IsDBNull(reader["Created"])
                      ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Created"]),
                  (int)(Convert.IsDBNull(reader["AgeGroup"]) ? (int)0 : (int)reader["AgeGroup"]),
                  (int)(Convert.IsDBNull(reader["RegTypeID"]) ? (int)0 : (int)reader["RegTypeID"]),
                  (int)(Convert.IsDBNull(reader["RepID"]) ? (int)0 : (int)reader["RepID"]),
                  reader["Work_phone"].ToString(),
                  String.IsNullOrEmpty(reader["Badge_ID"].ToString()) ? "" : reader["Badge_ID"].ToString(),
                  (DateTime)(Convert.IsDBNull(reader["Hire_Date"])
                      ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Hire_Date"]),
                  (DateTime)(Convert.IsDBNull(reader["Termin_Date"])
                      ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Termin_Date"]),
                  (int)(Convert.IsDBNull(reader["dept_ID"]) ? (int)0 : (int)reader["dept_ID"]),
                  (int)(Convert.IsDBNull(reader["Position_ID"]) ? (int)0 : (int)reader["Position_ID"]),
                  (int)(Convert.IsDBNull(reader["Clinical_Ind"]) ? 0 : Convert.ToInt32(reader["Clinical_Ind"])),
                  String.IsNullOrEmpty(reader["Title"].ToString())? "": reader["Title"].ToString(),
                  (bool)(Convert.IsDBNull(reader["Giftcard_ind"]) ? false : Convert.ToBoolean(reader["Giftcard_Ind"])),
                  (decimal)(Convert.IsDBNull(reader["Giftcard_Chour"]) ? (decimal)0 : (decimal)reader["Giftcard_Chour"]),
                  (decimal)(Convert.IsDBNull(reader["Giftcard_Uhour"]) ? (decimal)0 : (decimal)reader["Giftcard_Uhour"]),
                  (int)(Convert.IsDBNull(reader["UniqueID"]) ? (int)0 : (int)reader["UniqueID"]),
                  (bool)(Convert.IsDBNull(reader["FirstLogin_Ind"]))? false: (bool)reader["FirstLogin_Ind"]);
                return UserAccount;
            }
            catch(Exception ex)
            {
                return null;
            }
            
        }

        /// <summary>
        /// Returns a collection of UserAccountInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<UserAccountInfo> GetUserAccountCollectionFromReader(IDataReader reader)
        {
            return GetUserAccountCollectionFromReader(reader, true);
        }
        protected virtual List<UserAccountInfo> GetUserAccountCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<UserAccountInfo> UserAccounts = new List<UserAccountInfo>();
            while (reader.Read())
                UserAccounts.Add(GetUserAccountFromReader(reader, readMemos));
            return UserAccounts;
        }

        /////////////////////////////////////////////////////////
        // methods that work with UserAccount1

        protected virtual UserAccountInfo1 GetUserAccountFromReader1(IDataReader reader)
        {
            return GetUserAccountFromReader1(reader, true);
        }
        protected virtual UserAccountInfo1 GetUserAccountFromReader1(IDataReader reader, bool readMemos)
        {
            UserAccountInfo1 UserAccount = new UserAccountInfo1(
            (int)reader["iID"],
            reader["cAddress1"].ToString(),
            reader["cAddress2"].ToString(),
            (bool)reader["lAdmin"],
            (bool)reader["lPrimary"],
            reader["cBirthDate"].ToString(),
            reader["cCity"].ToString(),
            reader["cEmail"].ToString(),
            reader["cExpires"].ToString(),
            reader["cFirstName"].ToString(),
            reader["cFloridaNo"].ToString(),
            reader["cInstitute"].ToString(),
            reader["cLastName"].ToString(),
            reader["cLectDate"].ToString(),
            reader["cMiddle"].ToString(),
            reader["cPhone"].ToString(),
            reader["cPromoCode"].ToString(),
            (int)(Convert.IsDBNull(reader["PromoID"]) ? (int)0 : (int)reader["PromoID"]),
            (int)(Convert.IsDBNull(reader["FacilityID"]) ? (int)0 : (int)reader["FacilityID"]),
            reader["cPW"].ToString(),
            reader["cRefCode"].ToString(),
            reader["cRegType"].ToString(),
            reader["cSocial"].ToString(),
            reader["cState"].ToString(),
            reader["cUserName"].ToString().ToLower(),
            reader["cZipCode"].ToString(),
            (int)(Convert.IsDBNull(reader["SpecID"]) ? (int)0 : (int)reader["SpecID"]),
            reader["LCUserName"].ToString().ToLower(),
            (DateTime)(Convert.IsDBNull(reader["LastAct"])
                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastAct"]),
            reader["PassFmt"].ToString(),
            reader["PassSalt"].ToString(),
            reader["MobilePIN"].ToString(),
            reader["LCEmail"].ToString(),
            reader["PWQuest"].ToString(),
            reader["PWAns"].ToString(),
            (bool)reader["IsApproved"],
            (bool)reader["IsOnline"],
            (bool)reader["IsLocked"],
            (DateTime)(Convert.IsDBNull(reader["LastLogin"])
                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLogin"]),
            (DateTime)(Convert.IsDBNull(reader["LastPWChg"])
                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastPWChg"]),
           (DateTime)(Convert.IsDBNull(reader["LastLock"])
                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLock"]),
            (int)reader["XPWAtt"],
            (DateTime)(Convert.IsDBNull(reader["XPWAttSt"])
                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAttSt"]),
            (int)reader["XPWAnsAtt"],
            (DateTime)(Convert.IsDBNull(reader["XPWAnsSt"])
                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAnsSt"]),
            reader["Comment"].ToString(),
            (DateTime)(Convert.IsDBNull(reader["Created"])
                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Created"]),
            (int)(Convert.IsDBNull(reader["AgeGroup"]) ? (int)0 : (int)reader["AgeGroup"]),
            (int)(Convert.IsDBNull(reader["RegTypeID"]) ? (int)0 : (int)reader["RegTypeID"]),
            (int)(Convert.IsDBNull(reader["RepID"]) ? (int)0 : (int)reader["RepID"]),
            Convert.ToString(reader["Work_phone"].ToString()),
            Convert.ToString(reader["Badge_ID"].ToString()),
            (DateTime)(Convert.IsDBNull(reader["Hire_Date"])
                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Hire_Date"]),
            (DateTime)(Convert.IsDBNull(reader["Termin_Date"])
                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Termin_Date"]),
            (int)(Convert.IsDBNull(reader["dept_ID"]) ? (int)0 : (int)reader["dept_ID"]),
            (int)(Convert.IsDBNull(reader["Position_ID"]) ? (int)0 : (int)reader["Position_ID"]),
            (Convert.IsDBNull(reader["Clinical_Ind"]) ? 0 : Convert.ToInt32(reader["Clinical_Ind"])),
            Convert.ToString(reader["Title"].ToString()),
            (Convert.IsDBNull(reader["Giftcard_Ind"]) ? false : Convert.ToBoolean(reader["Giftcard_Ind"])),
            (decimal)(Convert.IsDBNull(reader["Giftcard_Chour"]) ? (decimal)0 : (decimal)reader["Giftcard_Chour"]),
            (decimal)(Convert.IsDBNull(reader["Giftcard_Uhour"]) ? (decimal)0 : (decimal)reader["Giftcard_Uhour"]),
            (int)(Convert.IsDBNull(reader["UniqueID"]) ? (int)0 : (int)reader["UniqueID"]),
            (bool)reader["FirstLogin_Ind"]);

            return UserAccount;

            //UserAccountInfo1 UserAccount = new UserAccountInfo1(

            ////UserAccountInfo1 UserAccount = new UserAccountInfo1(
            //  (int)reader["iID"],
            //    //reader["cAddress1"].ToString(),
            //    //reader["cAddress2"].ToString(),
            //    //(bool)reader["lAdmin"],
            //    //(bool)reader["lPrimary"],
            //    //reader["cBirthDate"].ToString(),
            //    //reader["cCity"].ToString(),
            //    //reader["cEmail"].ToString(),
            //    //reader["cExpires"].ToString(),
            //  reader["cFirstName"].ToString(),
            //reader["cLastName"].ToString());

            //return UserAccount;
        }

       
        
        protected virtual List<UserAccountInfo1> GetUserAccountCollectionFromReader1(IDataReader reader)
        {
            return GetUserAccountCollectionFromReader1(reader, true);
        }
        protected virtual List<UserAccountInfo1> GetUserAccountCollectionFromReader1(IDataReader reader, bool readMemos)
        {
            List<UserAccountInfo1> UserAccounts = new List<UserAccountInfo1>();
            while (reader.Read())
                UserAccounts.Add(GetUserAccountFromReader1(reader, readMemos));

            return UserAccounts;
        }
 

        #endregion
    }
}
#endregion