﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region SurveyDefinitionInfo

namespace PearlsReview.DAL
{
    public class SurveyDefinitionInfo
    {
        public SurveyDefinitionInfo() { }

        public SurveyDefinitionInfo(int SurveyID, string SurveyName, string XMLSurvey, int Version, int FacilityID)
        {
            this.SurveyID = SurveyID;
            this.SurveyName = SurveyName;
            this.XMLSurvey = XMLSurvey;
            this.Version = Version;
            this.FacilityID = FacilityID;
        }

        private int _SurveyID = 0;
        public int SurveyID
        {
            get { return _SurveyID; }
            protected set { _SurveyID = value; }
        }

        private string _SurveyName = "";
        public string SurveyName
        {
            get { return _SurveyName; }
            private set { _SurveyName = value; }
        }

        private string _XMLSurvey = "";
        public string XMLSurvey
        {
            get { return _XMLSurvey; }
            private set { _XMLSurvey = value; }
        }

        private int _Version = 0;
        public int Version
        {
            get { return _Version; }
            private set { _Version = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            private set { _FacilityID = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with SurveyDefinitions

        /// <summary>
        /// Returns the total number of SurveyDefinitions
        /// </summary>
        public  int GetSurveyDefinitionCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from SurveyDefinition where FacilityID=0 ", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all SurveyDefinitions
        /// </summary>
        public  List<SurveyDefinitionInfo> GetSurveyDefinitions(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from SurveyDefinition where FacilityID=0 ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetSurveyDefinitionCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the SurveyDefinition with the specified ID
        /// </summary>
        public  SurveyDefinitionInfo GetSurveyDefinitionByID(int SurveyID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from SurveyDefinition where SurveyID=@SurveyID", cn);
                cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = SurveyID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSurveyDefinitionFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves the SurveyDefinition with the specified ID
        /// </summary>
        public SurveyDefinitionInfo GetSurveyDefinitionBySurveyName(string SurveyName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from SurveyDefinition where surveyname=@SurveyName", cn);                
                cmd.Parameters.Add("@SurveyName", SqlDbType.VarChar).Value = SurveyName;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSurveyDefinitionFromReader(reader, true);
                else
                    return null;
            }
        }
        ///// <summary>
        ///// Bhaskar
        ///// Retrieves the SurveyDefinition with the specified ID
        ///// </summary>
        //public override SurveyDefinitionInfo GetSurveyDefinitionByTopicID(int TopicID)
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {

        //        SqlCommand cmd = new SqlCommand("select * " +
        //                "from SurveyDefinition where SurveyID=@TopicID", cn);
        //        //"from SurveyDefinition where TopicId=@TopicID", cn); //add new column(TopicId)..in SurveyDefinition (Bsk)
        //        cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
        //        cn.Open();
        //        IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
        //        if (reader.Read())
        //            return GetSurveyDefinitionFromReader(reader, true);
        //        else
        //            return null;
        //    }
        //}

        /// <summary>
        /// Deletes a SurveyDefinition
        /// </summary>
        public  bool DeleteSurveyDefinition(int SurveyID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from SurveyDefinition where SurveyID=@SurveyID", cn);
                cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = SurveyID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new SurveyDefinition
        /// </summary>
        public  int InsertSurveyDefinition(SurveyDefinitionInfo SurveyDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into SurveyDefinition " +
              "(SurveyName, " +
              "XMLSurvey, " +
              "Version, " +
                "FacilityID ) " +
              "VALUES (" +
              "@SurveyName, " +
              "@XMLSurvey, " +
              "@Version, " +
                "@FacilityID) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@SurveyName", SqlDbType.VarChar).Value = SurveyDefinition.SurveyName;
                cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = SurveyDefinition.XMLSurvey;
                cmd.Parameters.Add("@Version", SqlDbType.Int).Value = SurveyDefinition.Version;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = SurveyDefinition.FacilityID;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a SurveyDefinition
        /// </summary>
        public  bool UpdateSurveyDefinition(SurveyDefinitionInfo SurveyDefinition)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update SurveyDefinition set " +
              "SurveyName = @SurveyName, " +
              "XMLSurvey = @XMLSurvey, " +
              "Version = @Version, " +
              "FacilityID = @FacilityID " +
              "where SurveyID = @SurveyID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SurveyName", SqlDbType.VarChar).Value = SurveyDefinition.SurveyName;
                cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = SurveyDefinition.XMLSurvey;
                cmd.Parameters.Add("@Version", SqlDbType.Int).Value = SurveyDefinition.Version;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = SurveyDefinition.FacilityID;
                cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = SurveyDefinition.SurveyID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        /// <summary>
        /// Updates a SurveyDefinition
        /// </summary>
        public  bool UpdateSurveyDefinition(int SurveyID, string XMLSurvey, int Version, int FacilityID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update SurveyDefinition set " +
              "XMLSurvey = @XMLSurvey, " +
              "Version = @Version, " +
              "FacilityID = @FacilityID " +
              "where SurveyID = @SurveyID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;              
                cmd.Parameters.Add("@XMLSurvey", SqlDbType.VarChar).Value = XMLSurvey;
                cmd.Parameters.Add("@Version", SqlDbType.Int).Value = Version;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = SurveyID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Updates a SurveyDefinition
        /// </summary>
        public  bool UpdateSurveyDefBySurveyName(int SurveyID, string SurveyName, int Version, int FacilityID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update SurveyDefinition set " +
              "SurveyName = @SurveyName, " +
              "Version = @Version, " +
              "FacilityID = @FacilityID " +
              "where SurveyID = @SurveyID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;              
                cmd.Parameters.Add("@SurveyName", SqlDbType.VarChar).Value = SurveyName;
                cmd.Parameters.Add("@Version", SqlDbType.Int).Value = Version;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = SurveyID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        /// Retrieves the SurveyDefinition with the specified ID
        /// </summary>
        public  SurveyDefinitionInfo GetSurveyDefinitionByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from SurveyDefinition where SurveyID=@TopicID", cn);
                //"from SurveyDefinition where TopicId=@TopicID", cn); //add new column(TopicId)..in SurveyDefinition (Bsk)
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSurveyDefinitionFromReader(reader, true);
                else
                    return null;
            }
        }

        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new SurveyDefinitionInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual SurveyDefinitionInfo GetSurveyDefinitionFromReader(IDataReader reader)
        {
            return GetSurveyDefinitionFromReader(reader, true);
        }
        protected virtual SurveyDefinitionInfo GetSurveyDefinitionFromReader(IDataReader reader, bool readMemos)
        {
            SurveyDefinitionInfo SurveyDefinition = new SurveyDefinitionInfo(
              (int)reader["SurveyID"],
              reader["SurveyName"].ToString(),
              reader["XMLSurvey"].ToString(),
              (int)reader["Version"],
              (int)reader["FacilityID"]);


            return SurveyDefinition;
        }

        /// <summary>
        /// Returns a collection of SurveyDefinitionInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<SurveyDefinitionInfo> GetSurveyDefinitionCollectionFromReader(IDataReader reader)
        {
            return GetSurveyDefinitionCollectionFromReader(reader, true);
        }
        protected virtual List<SurveyDefinitionInfo> GetSurveyDefinitionCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<SurveyDefinitionInfo> SurveyDefinitions = new List<SurveyDefinitionInfo>();
            while (reader.Read())
                SurveyDefinitions.Add(GetSurveyDefinitionFromReader(reader, readMemos));
            return SurveyDefinitions;
        }
        #endregion
    }
}
#endregion