﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region ParentOrganizationInfo

namespace PearlsReview.DAL
{
    public class ParentOrganizationInfo
    {
        public ParentOrganizationInfo() { }

        public ParentOrganizationInfo(int parent_id,
            string parent_name, string address1, string address2, string city, string state,
            string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
            string contact_email, bool active, string Comment, int MaxUsers, DateTime Started,
            DateTime Expires)
        {
            this.Parent_id = parent_id;
            this.Parent_name = parent_name;
            this.address1 = address1;
            this.address2 = address2;
            this.city = city;
            this.state = state;
            this.zipcode = zipcode;
            this.phone = phone;
            this.contact_name = contact_name;
            this.contact_phone = contact_phone;
            this.contact_ext = contact_ext;
            this.contact_email = contact_email;
            this.active = active;
            this.Comment = Comment;
            this.Started = Started;
            this.Expires = Expires;
            this.MaxUsers = MaxUsers;
        }

        private int _Parent_id = 0;
        public int Parent_id
        {
            get { return _Parent_id; }
            protected set { _Parent_id = value; }
        }

        private string _Parent_name = "";
        public string Parent_name
        {
            get { return _Parent_name; }
            private set { _Parent_name = value; }
        }

        private string _address1 = "";
        public string address1
        {
            get { return _address1; }
            private set { _address1 = value; }
        }

        private string _address2 = "";
        public string address2
        {
            get { return _address2; }
            private set { _address2 = value; }
        }

        private string _city = "";
        public string city
        {
            get { return _city; }
            private set { _city = value; }
        }

        private string _state = "";
        public string state
        {
            get { return _state; }
            private set { _state = value; }
        }

        private string _zipcode = "";
        public string zipcode
        {
            get { return _zipcode; }
            private set { _zipcode = value; }
        }

        private string _phone = "";
        public string phone
        {
            get { return _phone; }
            private set { _phone = value; }
        }

        private string _contact_name = "";
        public string contact_name
        {
            get { return _contact_name; }
            private set { _contact_name = value; }
        }

        private string _contact_phone = "";
        public string contact_phone
        {
            get { return _contact_phone; }
            private set { _contact_phone = value; }
        }

        private string _contact_ext = "";
        public string contact_ext
        {
            get { return _contact_ext; }
            private set { _contact_ext = value; }
        }

        private string _contact_email = "";
        public string contact_email
        {
            get { return _contact_email; }
            private set { _contact_email = value; }
        }

        private bool _active = true;
        public bool active
        {
            get { return _active; }
            private set { _active = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            private set { _Comment = value; }
        }

        private int _MaxUsers = 1000;
        public int MaxUsers
        {
            get { return _MaxUsers; }
            protected set { _MaxUsers = value; }
        }

        private DateTime _Started = System.DateTime.Now;
        public DateTime Started
        {
            get { return _Started; }
            private set { _Started = value; }
        }

        private DateTime _Expires = System.DateTime.Now.AddYears(1);
        public DateTime Expires
        {
            get { return _Expires; }
            private set { _Expires = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with ParentOrganizations
        /// ***** CEDIRECT VERSION ************

        /// <summary>
        /// Returns the total number of ParentOrganizations
        /// </summary>
        public  int GetParentOrganizationCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from ParentOrganization", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all ParentOrganizations
        /// </summary>
        public  List<ParentOrganizationInfo> GetParentOrganizations(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from ParentOrganization";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetParentOrganizationCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the ParentOrganization with the specified ID
        /// </summary>
        public  ParentOrganizationInfo GetParentOrganizationByID(int Parent_id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from ParentOrganization where Parent_id=@Parent_id", cn);
                cmd.Parameters.Add("@Parent_id", SqlDbType.Int).Value = Parent_id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetParentOrganizationFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Returns the total number of UserAccounts for the specified FacilityID
        /// </summary>
        public  int GetUserAccountCountByParentFacilityID(int FacilityID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from UserAccount u join FacilityGroup f on u.facilityid=f.facid where u.isapproved=1 and f.parent_id = @FacilityID", cn);
                //              cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }


        /// <summary>
        /// Deletes a ParentOrganization
        /// </summary>
        public  bool DeleteParentOrganization(int Parent_id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from ParentOrganization where Parent_id=@Parent_id", cn);
                cmd.Parameters.Add("@Parent_id", SqlDbType.Int).Value = Parent_id;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new ParentOrganization
        /// </summary>
        public  int InsertParentOrganization(ParentOrganizationInfo ParentOrganization)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into ParentOrganization " +
              "(Parent_name, " +
              "address1, " +
              "address2, " +
              "city, " +
              "state, " +
              "zipcode, " +
              "phone, " +
              "contact_name, " +
              "contact_phone, " +
              "contact_ext, " +
              "contact_email, " +
              "active, " +
              "MaxUsers, " +
              "Started, " +
              "Expires, " +
              "comment ) " +
              "VALUES (" +
              "@Parent_name, " +
              "@address1, " +
              "@address2, " +
              "@city, " +
              "@state, " +
              "@zipcode, " +
              "@phone, " +
              "@contact_name, " +
              "@contact_phone, " +
              "@contact_ext, " +
              "@contact_email, " +
              "@active, " +
              "@MaxUsers, " +
              "@Started, " +
              "@Expires, " +
              "@comment ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@Parent_name", SqlDbType.VarChar).Value = ParentOrganization.Parent_name;
                cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = ParentOrganization.address1;
                cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = ParentOrganization.address2;
                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = ParentOrganization.city;
                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = ParentOrganization.state;
                cmd.Parameters.Add("@zipcode", SqlDbType.VarChar).Value = ParentOrganization.zipcode;
                cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = ParentOrganization.phone;
                cmd.Parameters.Add("@contact_name", SqlDbType.VarChar).Value = ParentOrganization.contact_name;
                cmd.Parameters.Add("@contact_phone", SqlDbType.VarChar).Value = ParentOrganization.contact_phone;
                cmd.Parameters.Add("@contact_ext", SqlDbType.VarChar).Value = ParentOrganization.contact_ext;
                cmd.Parameters.Add("@contact_email", SqlDbType.VarChar).Value = ParentOrganization.contact_email;
                cmd.Parameters.Add("@active", SqlDbType.Bit).Value = ParentOrganization.active;
                cmd.Parameters.Add("@MaxUsers", SqlDbType.Int).Value = ParentOrganization.MaxUsers;
                cmd.Parameters.Add("@Started", SqlDbType.DateTime).Value = ParentOrganization.Started;
                cmd.Parameters.Add("@Expires", SqlDbType.DateTime).Value = ParentOrganization.Expires;
                cmd.Parameters.Add("@Comment", SqlDbType.Text).Value = ParentOrganization.Comment;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a ParentOrganization
        /// </summary>
        public  bool UpdateParentOrganization(ParentOrganizationInfo ParentOrganization)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update ParentOrganization set " +
              "Parent_name = @Parent_name, " +
              "address1 = @address1, " +
              "address2 = @address2, " +
              "city = @city, " +
              "state = @state, " +
              "zipcode = @zipcode, " +
              "phone = @phone, " +
              "contact_name = @contact_name, " +
              "contact_phone = @contact_phone, " +
              "contact_ext = @contact_ext, " +
              "contact_email = @contact_email, " +
              "active = @active, " +
            "MaxUsers = @MaxUsers, " +
            "Started = @Started, " +
            "Expires = @Expires, " +
              "comment = @comment " +
              "where Parent_id = @Parent_id ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@Parent_name", SqlDbType.VarChar).Value = ParentOrganization.Parent_name;
                cmd.Parameters.Add("@address1", SqlDbType.VarChar).Value = ParentOrganization.address1;
                cmd.Parameters.Add("@address2", SqlDbType.VarChar).Value = ParentOrganization.address2;
                cmd.Parameters.Add("@city", SqlDbType.VarChar).Value = ParentOrganization.city;
                cmd.Parameters.Add("@state", SqlDbType.VarChar).Value = ParentOrganization.state;
                cmd.Parameters.Add("@zipcode", SqlDbType.VarChar).Value = ParentOrganization.zipcode;
                cmd.Parameters.Add("@phone", SqlDbType.VarChar).Value = ParentOrganization.phone;
                cmd.Parameters.Add("@contact_name", SqlDbType.VarChar).Value = ParentOrganization.contact_name;
                cmd.Parameters.Add("@contact_phone", SqlDbType.VarChar).Value = ParentOrganization.contact_phone;
                cmd.Parameters.Add("@contact_ext", SqlDbType.VarChar).Value = ParentOrganization.contact_ext;
                cmd.Parameters.Add("@contact_email", SqlDbType.VarChar).Value = ParentOrganization.contact_email;
                cmd.Parameters.Add("@active", SqlDbType.Bit).Value = ParentOrganization.active;
                cmd.Parameters.Add("@MaxUsers", SqlDbType.Int).Value = ParentOrganization.MaxUsers;
                cmd.Parameters.Add("@Started", SqlDbType.DateTime).Value = ParentOrganization.Started;
                cmd.Parameters.Add("@Expires", SqlDbType.DateTime).Value = ParentOrganization.Expires;
                cmd.Parameters.Add("@comment", SqlDbType.Text).Value = ParentOrganization.Comment;

                cmd.Parameters.Add("@Parent_id", SqlDbType.Int).Value = ParentOrganization.Parent_id;


                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new ParentOrganizationInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual ParentOrganizationInfo GetParentOrganizationFromReader(IDataReader reader)
        {
            return GetParentOrganizationFromReader(reader, true);
        }
        protected virtual ParentOrganizationInfo GetParentOrganizationFromReader(IDataReader reader, bool readMemos)
        {
            ParentOrganizationInfo ParentOrganization = new ParentOrganizationInfo(
                (int)reader["Parent_id"],
                reader["Parent_name"].ToString(),
                reader["address1"].ToString(),
                reader["address2"].ToString(),
                reader["city"].ToString(),
                reader["state"].ToString(),
                reader["zipcode"].ToString(),
                reader["phone"].ToString(),
                reader["contact_name"].ToString(),
                reader["contact_phone"].ToString(),
                reader["contact_ext"].ToString(),
                reader["contact_email"].ToString(),
                (bool)reader["active"],
                reader["Comment"].ToString(),
                (int)reader["MaxUsers"],
                (DateTime)reader["Started"],
                (DateTime)reader["Expires"]);

            return ParentOrganization;
        }

        /// <summary>
        /// Returns a collection of ParentOrganizationInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<ParentOrganizationInfo> GetParentOrganizationCollectionFromReader(IDataReader reader)
        {
            return GetParentOrganizationCollectionFromReader(reader, true);
        }
        protected virtual List<ParentOrganizationInfo> GetParentOrganizationCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<ParentOrganizationInfo> ParentOrganizations = new List<ParentOrganizationInfo>();
            while (reader.Read())
                ParentOrganizations.Add(GetParentOrganizationFromReader(reader, readMemos));
            return ParentOrganizations;
        }

        #endregion
    }
}
#endregion