﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;


#region RegistrationTypeInfo

namespace PearlsReview.DAL
{
    public class RegistrationTypeInfo
    {
        public RegistrationTypeInfo() { }

        public RegistrationTypeInfo(int RegTypeID, string RegType)
        {
            this.RegTypeID = RegTypeID;
            this.RegType = RegType;
        }

        private int _RegTypeID = 0;
        public int RegTypeID
        {
            get { return _RegTypeID; }
            protected set { _RegTypeID = value; }
        }

        private string _RegType = "";
        public string RegType
        {
            get { return _RegType; }
            private set { _RegType = value; }
        }

    }
}
#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with RegistrationTypes

        /// <summary>
        /// Returns the total number of RegistrationTypes
        /// </summary>
        public int GetRegistrationTypeCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from RegistrationType", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all RegistrationTypes
        /// </summary>
        public List<RegistrationTypeInfo> GetRegistrationTypes(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from RegistrationType";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by RegType";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetRegistrationTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the RegistrationType with the specified ID
        /// </summary>
        public RegistrationTypeInfo GetRegistrationTypeByID(int RegTypeID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from RegistrationType where RegTypeID=@RegTypeID", cn);
                cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = RegTypeID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetRegistrationTypeFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a RegistrationType
        /// </summary>
        public bool DeleteRegistrationType(int RegTypeID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from RegistrationType where RegTypeID=@RegTypeID", cn);
                cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = RegTypeID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new RegistrationType
        /// </summary>
        public int InsertRegistrationType(RegistrationTypeInfo RegistrationType)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into RegistrationType " +
              "(RegType) " +
              "VALUES (" +
              "@RegType) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@RegType", SqlDbType.VarChar).Value = RegistrationType.RegType;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a RegistrationType
        /// </summary>
        public bool UpdateRegistrationType(RegistrationTypeInfo RegistrationType)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update RegistrationType set " +
              "RegType = @RegType " +
              "where RegTypeID = @RegTypeID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@RegType", SqlDbType.VarChar).Value = RegistrationType.RegType;
                cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = RegistrationType.RegTypeID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider

        /////////////////////////////////////////////////////////
        // methods that work with RegistrationTypes
       
        /// <summary>
        /// Returns a new RegistrationTypeInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual RegistrationTypeInfo GetRegistrationTypeFromReader(IDataReader reader)
        {
            return GetRegistrationTypeFromReader(reader, true);
        }
        protected virtual RegistrationTypeInfo GetRegistrationTypeFromReader(IDataReader reader, bool readMemos)
        {
            RegistrationTypeInfo RegistrationType = new RegistrationTypeInfo(
              (int)reader["RegTypeID"],
              reader["RegType"].ToString());


            return RegistrationType;
        }

        /// <summary>
        /// Returns a collection of RegistrationTypeInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<RegistrationTypeInfo> GetRegistrationTypeCollectionFromReader(IDataReader reader)
        {
            return GetRegistrationTypeCollectionFromReader(reader, true);
        }
        protected virtual List<RegistrationTypeInfo> GetRegistrationTypeCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<RegistrationTypeInfo> RegistrationTypes = new List<RegistrationTypeInfo>();
            while (reader.Read())
                RegistrationTypes.Add(GetRegistrationTypeFromReader(reader, readMemos));
            return RegistrationTypes;
        }

        #endregion
    }
}
#endregion


