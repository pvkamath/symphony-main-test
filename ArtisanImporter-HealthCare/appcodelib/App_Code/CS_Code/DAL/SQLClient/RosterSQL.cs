﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.BLL;

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        public bool UpdateRoster(RosterInfo rosterInfo)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Rosters SET " +
                    "testid= @testid, " +
                    "license_number = @license_number, " +
                    "course_id = @course_id, " +
                    "roster_id = @roster_id, " +
                    "licensecode = @licensecode, " +
                    "cb_message = @cb_message, " +
                    "cb_time = @cb_time " +
                    "where rid=@rid ", cn);
             
                cmd.Parameters.Add("@testid", SqlDbType.Int).Value = rosterInfo.testid;
                cmd.Parameters.Add("@license_number", SqlDbType.VarChar).Value = (rosterInfo.license_number == null ? DBNull.Value as Object : rosterInfo.license_number as Object);
                cmd.Parameters.Add("@course_id", SqlDbType.VarChar).Value = (rosterInfo.course_id == null ? DBNull.Value as object : rosterInfo.course_id as object);
                cmd.Parameters.Add("@roster_id", SqlDbType.VarChar).Value = (rosterInfo.roster_id == null ? DBNull.Value as object : rosterInfo.roster_id as object);
                cmd.Parameters.Add("@licensecode", SqlDbType.VarChar).Value = (rosterInfo.licensecode == null ? DBNull.Value as object : rosterInfo.licensecode as object);
                cmd.Parameters.Add("@cb_message", SqlDbType.VarChar).Value = (rosterInfo.cb_message == null ? DBNull.Value as object : rosterInfo.cb_message as object);
                cmd.Parameters.Add("@cb_time", SqlDbType.DateTime).Value = rosterInfo.cb_time;
                cmd.Parameters.Add("@rid", SqlDbType.Int).Value = rosterInfo.rid;
                cn.Open();
                return (ExecuteNonQuery(cmd) == 1);
            }
        }

        public int InsertRoster(RosterInfo rosterInfo)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Rosters " +
              "(testid, " +
              "license_number, " +
              "course_id, " +
              "roster_id, " +
              "licensecode, " +
              "cb_message, " +
              "cb_time) " +
              "VALUES (" +
              "@testid, " +
              "@license_number, " +
              "@course_id, " +
              "@roster_id, " +
              "@licensecode, " +
              "@cb_message, " +
              "@cb_time) ", cn);

              cmd.Parameters.Add("@testid", SqlDbType.Int).Value = rosterInfo.testid;
              cmd.Parameters.Add("@license_number", SqlDbType.VarChar).Value = 
                    (rosterInfo.license_number == null ? DBNull.Value as Object : rosterInfo.license_number as Object);
              cmd.Parameters.Add("@course_id", SqlDbType.VarChar).Value =
                    (rosterInfo.course_id == null ? DBNull.Value as Object : rosterInfo.course_id as Object);
              cmd.Parameters.Add("@roster_id", SqlDbType.VarChar).Value =
                    (rosterInfo.roster_id == null ? DBNull.Value as Object : rosterInfo.roster_id as Object);
              cmd.Parameters.Add("@licensecode", SqlDbType.VarChar).Value =
                    (rosterInfo.licensecode == null ? DBNull.Value as Object : rosterInfo.licensecode as Object);
              cmd.Parameters.Add("@cb_message", SqlDbType.VarChar).Value =
                    (rosterInfo.cb_message == null ? DBNull.Value as Object : rosterInfo.cb_message as Object);
              cmd.Parameters.Add("@cb_time", SqlDbType.DateTime).Value =
                    (rosterInfo.cb_time == null ? DBNull.Value as Object : rosterInfo.cb_time as Object);

                cn.Open();
                cmd.ExecuteNonQuery();
                return 1;
            }
        }

        public bool DeleteRoster(int rid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Rosters where rid=@rid", cn);
                cmd.Parameters.Add("@rid", SqlDbType.Int).Value = rid;
                cn.Open();
                return (ExecuteNonQuery(cmd) == 1);
            }
        }

        public List<RosterInfo> GetRosters(string sortExpression)
        {
            List<RosterInfo> rosters = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "SELECT rid, testid, license_number, course_id, roster_id, licensecode, cb_message, cb_time FROM Rosters";
 
                // add on ORDER BY if provided
                if (sortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + sortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by rid";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                rosters = new List<RosterInfo>();
                while (reader.Read())
                {
                    RosterInfo roster = new RosterInfo(
                        Convert.ToInt32(reader["rid"]),
                        reader["testid"] == DBNull.Value ? 0: Convert.ToInt32(reader["testid"]),
                        reader["license_number"].ToString(),
                        reader["course_id"].ToString(),
                        reader["roster_id"].ToString(),
                        reader["licensecode"].ToString(),
                        reader["cb_message"].ToString(),
                        Convert.ToDateTime(reader["cb_time"].ToString()) );

                    rosters.Add(roster);
                }
            }
            return rosters;
        }

        public RosterInfo GetRosterByID(int rid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * from Rosters where rid='" + rid + "'", cn);

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                RosterInfo rosterInfo = new RosterInfo();
                while (reader.Read())
                {
                    rosterInfo.rid = Convert.ToInt32(reader["rid"]);
                    rosterInfo.testid = (reader["testid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["testid"]));
                    rosterInfo.license_number = reader["license_number"].ToString();
                    rosterInfo.course_id = reader["course_id"].ToString();
                    rosterInfo.roster_id = reader["roster_id"].ToString();
                    rosterInfo.licensecode = reader["licensecode"].ToString();
                    rosterInfo.cb_message = reader["cb_message"].ToString();
                    rosterInfo.cb_time = Convert.ToDateTime(reader["cb_time"].ToString());
                }
                reader.Dispose();
                return rosterInfo;
            }
        }
    }
}
#endregion