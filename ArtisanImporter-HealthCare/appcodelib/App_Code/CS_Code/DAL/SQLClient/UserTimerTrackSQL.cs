﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Budgerel Tumurbaatar
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider: DataAccess
    {
        public int GetUserTimerByTopicAndUser(int userid, int topicid)
        {
            using(SqlConnection cn=new SqlConnection(this.ConnectionString))
            {
                string sql = "SELECT timebysec FROM usertimertrack WHERE userid=@userid and topicid=@topicid";

                SqlCommand cmd = new SqlCommand(sql,cn);
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;

                cn.Open();

                IDataReader reader = ExecuteReader(cmd);

                if (reader.Read())
                    return reader["timebysec"] == null ? -2 : Convert.ToInt32(reader["timebysec"].ToString());
                else
                    return -2;
            }
        }

        public void AddorUpdateUserTimerTrack(int userid, int topicid, int timebysec)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                bool isAdd = GetUserTimerByTopicAndUser(userid, topicid) == -2 ? true : false;
                string sql = "";
                if (isAdd)
                {
                    sql = "INSERT  INTO usertimertrack(userid,topicid,timebysec,lastup) VALUES(@userid,@topicid,@timebysec,getdate())";
                }
                else
                {
                    sql = "UPDATE usertimertrack SET timebysec=@timebysec, lastup=getdate() WHERE userid=@userid and topicid=@topicid";
                }

                SqlCommand cmd = new SqlCommand(sql, cn);
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@timebysec", SqlDbType.Int).Value = timebysec;

                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }
       
    }
}