﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region DQueryInfo

/// <summary>
/// Summary description for DQueryInfo
/// </summary>
public class DQueryInfo
{
    public DQueryInfo()
    {
    }

    public DQueryInfo(int QueryID, string QueryName, string QueryType, string QueryDescription, string QueryContent,
        string FirstPrompt, string SecondPrompt, string ThirdPrompt)
    {
        this._QueryID = QueryID;
        this._QueryName = QueryName;
        this._QueryType = QueryType;
        this._QueryDescription = QueryDescription;
        this._QueryContent = QueryContent;
        this._FirstPrompt = FirstPrompt;
        this._SecondPrompt = SecondPrompt;
        this._ThirdPrompt = ThirdPrompt;
    }

    private int _QueryID = 0;
    public int QueryID
    {
        get { return _QueryID; }
        set { _QueryID = value; }
    }

    private string _QueryName = "";
    public string QueryName
    {
        get { return _QueryName; }
        set { _QueryName = value; }
    }

    private string _QueryType = "";
    public string QueryType
    {
        get { return _QueryType; }
        set { _QueryType = value; }
    }

    private string _QueryDescription = "";
    public string QueryDescription
    {
        get { return _QueryDescription; }
        set { _QueryDescription = value; }
    }

    private string _QueryContent = "";
    public string QueryContent
    {
        get { return _QueryContent; }
        set { _QueryContent = value; }
    }

    private string _FirstPrompt = "";
    public string FirstPrompt
    {
        get { return _FirstPrompt; }
        set { _FirstPrompt = value; }
    }

    private string _SecondPrompt = "";
    public string SecondPrompt
    {
        get { return _SecondPrompt; }
        set { _SecondPrompt = value; }
    }

    private string _ThirdPrompt = "";
    public string ThirdPrompt
    {
        get { return _ThirdPrompt; }
        set { _ThirdPrompt = value; }
    }

}

#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

         public List<DQueryInfo> GetAllDQueryByQueryId(int QueryID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "Select * from DQuery  where QueryID=@QueryID";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@QueryID", SqlDbType.Int).Value = QueryID;
                cn.Open();
                return GetDQueryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        public List<DQueryInfo> GetAllDQuerys()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "Select * from DQuery";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                //cmd.Parameters.Add("@QueryID", SqlDbType.Int).Value = QueryID;
                cn.Open();
                return GetDQueryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public DataSet FetchDQueryResults(string qname, string qtype, string fp,string sp,string tp, string input1, string input2, string input3)
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = qname;
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                if (qtype  != "q")
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                }
                
                if (input1 != string.Empty & input1.Length > 0)
                {
                    cmd.Parameters.Add(fp, SqlDbType.VarChar).Value = input1;
                }
                if (input2 != string.Empty & input2.Length > 0)
                {
                    cmd.Parameters.Add(sp, SqlDbType.VarChar).Value = input2;
                }
                if (input3 != string.Empty & input3.Length > 0)
                {
                    cmd.Parameters.Add(tp, SqlDbType.VarChar).Value = input3;
                }

                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "Results");
                          
            }
            return ds;
        }
 #endregion     

        #region PRProvider

        protected virtual DQueryInfo GetDQueryFromReader(IDataReader reader)
        {
            return GetDQueryFromReader(reader, true);
        }

        protected virtual DQueryInfo GetDQueryFromReader(IDataReader reader, bool readMemos)
        {
            DQueryInfo dqry = new DQueryInfo(
              (int)(Convert.IsDBNull(reader["QueryID"]) ? (int)0 : (int)reader["QueryID"]),
               Convert.ToString(reader["QueryName"].ToString()),
               reader["QueryType"].ToString(),
               reader["QueryDescription"].ToString(),
               reader["QueryContent"].ToString(),
               reader["FirstPrompt"].ToString(),
               reader["SecondPrompt"].ToString(),
               reader["ThirdPrompt"].ToString());
            return dqry;
        }

        /// <summary>
        /// Returns a collection of DQueryInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<DQueryInfo> GetDQueryCollectionFromReader(IDataReader reader)
        {
            return GetDQueryCollectionFromReader(reader, true);
        }
        protected virtual List<DQueryInfo> GetDQueryCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<DQueryInfo> dqry = new List<DQueryInfo>();
            while (reader.Read())
                dqry.Add(GetDQueryFromReader(reader, readMemos));
            return dqry;
        }


        #endregion 
    
    
    }
     
    }

#endregion

