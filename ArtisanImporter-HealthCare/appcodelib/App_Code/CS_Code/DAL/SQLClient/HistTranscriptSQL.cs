﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;


#region HistTranscriptInfo
    
namespace PearlsReview.DAL
{
    public class HistTranscriptInfo
    {
        public HistTranscriptInfo()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public HistTranscriptInfo(int TranscriptID, int ItemID, int Score, DateTime DateComplete, string FirstName,
            string LastName, string RnState, string RnNum, string RnCode, string RnState2, string RnNum2, string RnCode2,
            string CourseName, string CourseNum, Decimal Credits)
        {
            this.TranscriptID = TranscriptID;
            this.ItemID = ItemID;
            this.Score = Score;
            this.DateComplete = DateComplete;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.RnState = RnState;
            this.RnNum = RnNum;
            this.RnCode = RnCode;
            this.RnState2 = RnState2;
            this.RnNum2 = RnNum2;
            this.RnCode2 = RnCode2;
            this.CourseName = CourseName;
            this.CourseNum = CourseNum;
            this.Credits = Credits;

        }
        private int _TranscriptID = 0;
        public int TranscriptID
        {
            get { return _TranscriptID; }
            set { _TranscriptID = value; }
        }

        private int _ItemID = 0;
        public int ItemID
        {
            get { return _ItemID; }
            set { _ItemID = value; }
        }

        private int _Score = 0;
        public int Score
        {
            get { return _Score; }
            set { _Score = value; }
        }

        private DateTime _DateComplete = System.DateTime.MinValue;
        public DateTime DateComplete
        {
            get { return _DateComplete; }
            set { _DateComplete = value; }
        }

        private string _FirstName = "";
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        private string _LastName = "";
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        private string _RnState = "";
        public string RnState
        {
            get { return _RnState; }
            set { _RnState = value; }
        }

        private string _RnNum = "";
        public string RnNum
        {
            get { return _RnNum; }
            set { _RnNum = value; }
        }

        private string _RnCode = "";
        public string RnCode
        {
            get { return _RnCode; }
            set { _RnCode = value; }
        }

        private string _RnState2 = "";
        public string RnState2
        {
            get { return _RnState2; }
            set { _RnState2 = value; }
        }

        private string _RnNum2 = "";
        public string RnNum2
        {
            get { return _RnNum2; }
            set { _RnNum2 = value; }
        }

        private string _RnCode2 = "";
        public string RnCode2
        {
            get { return _RnCode2; }
            set { _RnCode2 = value; }
        }

        private string _CourseName = "";
        public string CourseName
        {
            get { return _CourseName; }
            set { _CourseName = value; }
        }

        private string _CourseNum = "";
        public string CourseNum
        {
            get { return _CourseNum; }
            set { _CourseNum = value; }

        }

        private decimal _Credits = 0;
        public decimal Credits
        {
            get { return _Credits; }
            set { _Credits = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        /// <summary>
        /// Retrieves the HistTranscript with the specified ID
        /// </summary>
        public HistTranscriptInfo GetHistTranscriptByID(int Histtransid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select " +
                    "transcriptid,itemid,score,datecomplete,firstname,lastname,rnstate,rnnum, " +
                    "rncode,rnstate2,rnnum2,rncode2,coursename,coursenum,credits " +
                    "from HistTranscript where transcriptid=@TID and itemid=0", cn);
                cmd.Parameters.Add("@TID", SqlDbType.Int).Value = Histtransid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetHistTranscriptFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves the HistTranscript with the specified ID
        /// </summary>
        public List<HistTranscriptInfo> GetHistTranscriptByFirstLastName(string fname, string lname)
        {
            List<HistTranscriptInfo> histInfo = new List<HistTranscriptInfo>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select " +
                    "transcriptid,itemid,score,datecomplete,firstname,lastname,rnstate,rnnum, " +
                    "rncode,rnstate2,rnnum2,rncode2,coursename,coursenum,credits " +
                    "from HistTranscript where firstname =@fname and  lastname =@lname ", cn);
                cmd.Parameters.Add("@fname", SqlDbType.VarChar).Value = fname;
                cmd.Parameters.Add("@lname", SqlDbType.VarChar).Value = lname;

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                //  IDataReader reader = ExecuteReader(cmd)
                while (reader.Read())
                {
                    HistTranscriptInfo hinfo = new HistTranscriptInfo();
                    hinfo = GetHistTranscriptFromReader(reader, true);
                    histInfo.Add(hinfo);
                }

                return histInfo;
            }
        }


        /// <summary>
        /// Updates Histtranscript
        /// </summary>
        public bool UpdateHistTranscript(HistTranscriptInfo hisin)
        {

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update histtranscript SET " +
              "itemid = @itemid, " +
              "firstname =@firstname, " +
              "lastname =@lastname, " +
              "datecomplete =@datecomplete, " +
              "rnnum =@rnnum, " +
              "rnstate =@rnstate, " +
              "rncode =@rncode, " +
              "rnnum2 =@rnnum2, " +
              "rnstate2 =@rnstate2, " +
              "rncode2 =@rncode2, " +
              "coursenum =@coursenum, " +
              "coursename =@coursename, " +
              "score =@score, " +
              "credits =@credits " +
              "where transcriptid = @transcriptid ", cn);
                cmd.Parameters.Add("@transcriptid", SqlDbType.Int).Value = hisin.TranscriptID;
                cmd.Parameters.Add("@itemid", SqlDbType.Int).Value = hisin.ItemID;
                cmd.Parameters.Add("@firstname", SqlDbType.VarChar).Value = hisin.FirstName == null ? DBNull.Value as Object : hisin.FirstName as Object;
                cmd.Parameters.Add("@lastname", SqlDbType.VarChar).Value = hisin.LastName == null ? DBNull.Value as Object : hisin.LastName as Object;
                if (hisin.DateComplete == System.DateTime.MinValue)
                {
                    cmd.Parameters.Add("@datecomplete", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@datecomplete", SqlDbType.DateTime).Value = hisin.DateComplete;
                }

                //cmd.Parameters.Add("@score", SqlDbType.VarChar).Value = hisin.Score == null ? DBNull.Value as Object : hisin.Score as Object;
                cmd.Parameters.Add("@score", SqlDbType.VarChar).Value = hisin.Score;
                cmd.Parameters.Add("@credits", SqlDbType.Decimal).Value = hisin.Credits;
                cmd.Parameters.Add("@rnnum", SqlDbType.VarChar).Value = hisin.RnNum == null ? DBNull.Value as Object : hisin.RnNum as Object;
                cmd.Parameters.Add("@rnstate", SqlDbType.VarChar).Value = hisin.RnState == null ? DBNull.Value as Object : hisin.RnState as Object;
                cmd.Parameters.Add("@rncode", SqlDbType.VarChar).Value = hisin.RnCode == null ? DBNull.Value as Object : hisin.RnCode as Object;
                cmd.Parameters.Add("@rnnum2", SqlDbType.VarChar).Value = hisin.RnNum2 == null ? DBNull.Value as Object : hisin.RnNum2 as Object;
                cmd.Parameters.Add("@rnstate2", SqlDbType.VarChar).Value = hisin.RnState2 == null ? DBNull.Value as Object : hisin.RnState2 as Object;
                cmd.Parameters.Add("@rncode2", SqlDbType.VarChar).Value = hisin.RnCode2 == null ? DBNull.Value as Object : hisin.RnCode2 as Object;
                cmd.Parameters.Add("@coursenum", SqlDbType.VarChar).Value = hisin.CourseNum == null ? DBNull.Value as Object : hisin.CourseNum as Object;
                cmd.Parameters.Add("@coursename", SqlDbType.VarChar).Value = hisin.CourseName == null ? DBNull.Value as Object : hisin.CourseName as Object;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /////////////////////////////////////////////////////////
        // methods that work with HistTranscript

        public  List<HistTranscriptInfo> GetHistTranscriptRecords(string cSortExpression, int TranscriptID, int ItemID, int Score, DateTime DateComplete,
              string FirstName, string LastName, string RNState, string RNNum, string RNCode, string RNState2, string RNNum2, string RNCode2, string CourseName,
              string CourseNum, decimal Credits)
        {
            string qry = "";
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                qry = "select transcriptid,itemid,score,datecomplete,firstname,lastname,rnstate,rnnum,rncode,rnstate2,rnnum2,rncode2,coursename,coursenum,credits  from histtranscript where ";
                if ((FirstName.Length > 0) & (FirstName != null))
                {
                    qry += " (FirstName Like '" + FirstName.Replace("'", "''") + "%') ";
                }
                if (ItemID > 0)
                {
                    qry += " and (ItemID Like '" + ItemID + "%') ";
                }
                if (Score > 0)
                {
                    qry += "and (Score Like '" + Score + "%') ";
                }
                if ((DateComplete > System.DateTime.MinValue) & (DateComplete != null))
                {
                    //qry += "and (DateComplete Like '" + DateComplete + "%') ";
                }
                if ((LastName.Length > 0) & (LastName != null))
                {
                    qry += "and (LastName Like '" + LastName.Replace("'", "''") + "%') ";
                }
                if ((RNState.Length > 0) & (RNState != null))
                {
                    qry += "and (RNState Like '" + RNState.ToLower().Replace("'", "''") + "%') ";
                }

                if ((RNCode.Length > 0) & (RNCode != null))
                {
                    qry += "and (RNCode Like '" + RNCode.Replace("'", "''") + "%') ";
                }
                if ((RNState2.Length > 0) & (RNState2 != null))
                {
                    qry += "and (RNState2 Like '" + RNState2.Replace("'", "''") + "%') ";
                }
                if ((RNNum.Length > 0) & (RNNum != null))
                {
                    qry += "and ((RNNum Like '" + RNNum.Replace("'", "''") + "%') or (RNNum2 Like '" + RNNum2.Replace("'", "''") + "%')) ";
                }
                //if ((RNNum2.Length > 0) & (RNNum2 != null))
                //{
                //    qry += " ";
                //}

                if ((RNCode2.Length > 0) & (RNCode2 != null))
                {
                    qry += "and (RNCode2 Like '" + RNCode2.Replace("'", "''") + "%') ";
                }
                if ((CourseName.Length > 0) & (CourseName != null))
                {
                    qry += "and (CourseName Like '" + CourseName.Replace("'", "''") + "%') ";
                }

                if ((CourseNum.Length > 0) & (CourseNum != null))
                {
                    qry += "and (CourseNum Like '" + CourseNum.Replace("'", "''") + "%') ";
                }
                //if ((Credits > 0) & (Credits != null))
                if (Credits > 0) //bsk
                {
                    qry += "and (Credits Like '" + Credits + "%') ";
                }

                qry += " order by lastname,firstname";
                SqlCommand cmd = new SqlCommand(qry, cn);
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);

                List<HistTranscriptInfo> HistTranscripts = new List<HistTranscriptInfo>();
                while (reader.Read())
                {
                    DateTime dt = Convert.ToDateTime(reader["DateComplete"].ToString());
                    string strdate = dt.ToShortDateString();

                    HistTranscriptInfo HistTranscript = new HistTranscriptInfo(
               Convert.ToInt32(reader["TranscriptID"].ToString()),
               Convert.ToInt32(reader["ItemID"].ToString()),
               Convert.ToInt32(reader["Score"].ToString()),
               Convert.ToDateTime(reader["DateComplete"].ToString()),
               reader["FirstName"].ToString(),
               reader["LastName"].ToString(),
               reader["RNState"].ToString(),
               reader["RNNum"].ToString(),
               reader["RNCode"].ToString(),
               reader["RNState2"].ToString(),
               reader["RNNum2"].ToString(),
               reader["RNCode2"].ToString(),
               reader["CourseName"].ToString(),
               reader["CourseNum"].ToString(),
               Convert.ToDecimal(reader["Credits"].ToString()));
                    HistTranscripts.Add(HistTranscript);
                }
                return HistTranscripts;
            }
        }

        #endregion
        
        #region  PRProvider

        /// <summary>
        /// Returns a new HistTranscriptInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual HistTranscriptInfo GetHistTranscriptFromReader(IDataReader reader)
        {
            return GetHistTranscriptFromReader(reader, true);
        }
        protected virtual HistTranscriptInfo GetHistTranscriptFromReader(IDataReader reader, bool readMemos)
        {
            HistTranscriptInfo HistTranscript = new HistTranscriptInfo(
              (int)reader["transcriptid"],
              (int)reader["itemid"],
              (int)reader["score"],
              (DateTime)(Convert.IsDBNull(reader["datecomplete"])
                  ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["datecomplete"]),
              reader["firstname"].ToString(),
              reader["lastname"].ToString(),
              reader["rnstate"].ToString(),
              reader["rnnum"].ToString(),
              reader["rncode"].ToString(),
              reader["rnstate"].ToString(),
              reader["rnnum2"].ToString(),
              reader["rncode2"].ToString(),
              reader["coursename"].ToString(),
              reader["coursenum"].ToString(),
               (decimal)(Convert.IsDBNull(reader["credits"]) ? (decimal)0 : (decimal)reader["credits"]));
            return HistTranscript;
        }

        /// <summary>
        /// Returns a collection of UserAccountInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<HistTranscriptInfo> GetHistTranscriptCollectionFromReader(IDataReader reader)
        {
            return GetHistTranscriptCollectionFromReader(reader, true);
        }
        protected virtual List<HistTranscriptInfo> GetHistTranscriptCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<HistTranscriptInfo> HistTranscripts = new List<HistTranscriptInfo>();
            while (reader.Read())
                HistTranscripts.Add(GetHistTranscriptFromReader(reader, readMemos));
            return HistTranscripts;
        }
        #endregion
    }
}
#endregion
