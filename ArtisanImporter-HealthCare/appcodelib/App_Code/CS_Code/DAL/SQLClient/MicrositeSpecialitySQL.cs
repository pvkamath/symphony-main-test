﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region MicrositeSpecialityInfo

namespace PearlsReview.DAL
{

    /// <summary> 
    /// Summary description for MicrositeDomainInfo
    /// Bhaskar N
    /// </summary>
    public class MicrositeSpecialityInfo
    {

        public MicrositeSpecialityInfo() { }


        public MicrositeSpecialityInfo(int dsid, int msid, int disciplineid, bool primary_ind)
        {
            this.Dsid = dsid;
            this.Msid = msid;
            this.Disciplineid = disciplineid;
            this.Primary_ind = primary_ind;
        }
        private int _dsid = 0;
        public int Dsid
        {
            get { return _dsid; }
            protected set { _dsid = value; }
        }
        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }
        private int _disciplineid = 0;
        public int Disciplineid
        {
            get { return _disciplineid; }
            protected set { _disciplineid = value; }
        }
        private Boolean _primary_ind = false;
        public Boolean Primary_ind
        {
            get { return _primary_ind; }
            protected set { _primary_ind = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // Methods that work with MicrositeDomain
        // Bhaskar N

        /// <summary>
        /// Retrieves all MicrositeDomains
        /// </summary>
        public List<MicrositeSpecialityInfo> GetMicrositeSpeciality(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from MicrositeSpeciality ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetMicrositeSpecialityCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public List<MicrositeSpecialityInfo> GetMicrositeSpecialityByMsid(int Msid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from MicrositeSpeciality where msid=" + Msid;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetMicrositeSpecialityCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves the MicrositeDomain with the specified ID
        /// </summary>
        public MicrositeSpecialityInfo GetMicrositeSpecialityByID(int dsid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositeSpeciality where dsid=@dsid", cn);

                cmd.Parameters.Add("@dsid", SqlDbType.Int).Value = dsid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositeSpecialityFromReader(reader, true);
                else
                    return null;
            }
        }
        public int InsertMicrositeSpeciality(MicrositeSpecialityInfo MicrositeSpeciality)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into MicrositeSpeciality " +
                  "(msid , " +
                  "disciplineid, " +
                  "primary_ind) VALUES (@msid, @disciplineid, @primary_ind)", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeSpeciality.Msid;
                cmd.Parameters.Add("@disciplineid", SqlDbType.Int).Value = MicrositeSpeciality.Disciplineid;
                cmd.Parameters.Add("@primary_ind", SqlDbType.Bit).Value = MicrositeSpeciality.Primary_ind;
                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int i = cmd.ExecuteNonQuery();

                return i;
            }
        }
        public bool UpdateMicrositeSpecialityLists(List<MicrositeSpecialityInfo> Specialities, int Msid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                cn.Open();
                SqlTransaction transaction;
                SqlCommand cmd = cn.CreateCommand();
                transaction = cn.BeginTransaction();
                cmd.Connection = cn;
                cmd.Transaction = transaction;

                try
                {
                    cmd.CommandText = "delete from MicrositeSpeciality where msid=@msid";
                    cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                    cmd.ExecuteNonQuery();

                    foreach (MicrositeSpecialityInfo specialities in Specialities)
                    {
                        cmd.CommandText = "insert into MicrositeSpeciality (msid , disciplineid, primary_ind) values (@msid, @disciplineid, @primary_ind)";
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@msid", SqlDbType.Int).Value = Msid;
                        cmd.Parameters.Add("@disciplineid", SqlDbType.Int).Value = specialities.Disciplineid;
                        cmd.Parameters.Add("@primary_ind", SqlDbType.Bit).Value = specialities.Primary_ind;
                        cmd.ExecuteNonQuery();
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return false;
                }
                return true;
            }
        }
        public bool UpdateMicrositeSpeciality(MicrositeSpecialityInfo MicrositeSpeciality)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update MicrositeSpeciality set " +
              "msid = @msid, " +
              "disciplineid = @disciplineid, " +
              "primary_ind = @primary_ind where dsid=@dsid", cn);

                cmd.Parameters.Add("@dsid", SqlDbType.Int).Value = MicrositeSpeciality.Dsid;
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeSpeciality.Msid;
                cmd.Parameters.Add("@disciplineid", SqlDbType.Int).Value = MicrositeSpeciality.Disciplineid;
                cmd.Parameters.Add("@primary_ind", SqlDbType.Bit).Value = MicrositeSpeciality.Primary_ind;
                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a Discount
        /// </summary>
        public bool DeleteMicrositeSpeciality(int dsid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from MicrositeSpeciality where dsid=@dsid", cn);
                cmd.Parameters.Add("@dsid", SqlDbType.Int).Value = dsid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider
        /////////////////////////////////////////////////////////
        // methods that work with Discount  

        /// <summary>
        /// Returns a new DiscountInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual MicrositeSpecialityInfo GetMicrositeSpecialityFromReader(IDataReader reader)
        {
            return GetMicrositeSpecialityFromReader(reader, true);
        }
        protected virtual MicrositeSpecialityInfo GetMicrositeSpecialityFromReader(IDataReader reader, bool readMemos)
        {
            MicrositeSpecialityInfo MicrositeSpeciality = new MicrositeSpecialityInfo(
                  (int)reader["dsid"],
                  (int)reader["msid"],
                  (int)reader["disciplineid"],
                  String.IsNullOrEmpty(reader["primary_ind"].ToString()) ? false : bool.Parse(reader["primary_ind"].ToString()));
            return MicrositeSpeciality;
        }


        /// <summary>
        /// Returns a collection of DiscountInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<MicrositeSpecialityInfo> GetMicrositeSpecialityCollectionFromReader(IDataReader reader)
        {
            return GetMicrositeSpecialityCollectionFromReader(reader, true);
        }
        protected virtual List<MicrositeSpecialityInfo> GetMicrositeSpecialityCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<MicrositeSpecialityInfo> MicrositeSpeciality = new List<MicrositeSpecialityInfo>();
            while (reader.Read())
                MicrositeSpeciality.Add(GetMicrositeSpecialityFromReader(reader, readMemos));
            return MicrositeSpeciality;
        }

        #endregion

    }
}
#endregion

