﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region LicenseTypeInfo

namespace PearlsReview.DAL
{
    public class LicenseTypeInfo
    {
        public LicenseTypeInfo() { }

        public LicenseTypeInfo(int License_Type_ID, string Description, string LicenseCode, string instruction)
        {
            this.License_Type_ID = License_Type_ID;
            this.Description = Description;
            this.LicenseCode = LicenseCode;
            this.Instruction = instruction;
        }

        private int _License_Type_ID = 0;
        public int License_Type_ID
        {
            get { return _License_Type_ID; }
            protected set { _License_Type_ID = value; }
        }

        private string _Description = "";
        public string Description
        {
            get { return _Description; }
            private set { _Description = value; }
        }

        //added for edit

        private string _LicenseCode = "";
        public string LicenseCode
        {
            get { return _LicenseCode; }
            private set { _LicenseCode = value; }
        }


        private string _Instruction = "";
        public string Instruction
        {
            get { return _Instruction; }
            set { _Instruction = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with LicenseTypes

        /// <summary>
        /// Returns the total number of LicenseTypes
        /// </summary>
        public int GetLicenseTypeCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from LicenseType", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all LicenseTypes
        /// </summary>
        public List<LicenseTypeInfo> GetLicenseTypes(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select License_Type_ID, Description + ' - ' + LicenseCode as Description, LicenseCode,Instruction " +
                    "from LicenseType";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by Description";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetLicenseTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all LicenseTypes for edit   added 11/1/2012
        /// </summary>
        public List<LicenseTypeInfo> GetLicenseTypesForEdit(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select License_Type_ID, Description,  LicenseCode,  Instruction " +
                    "from LicenseType";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by Description";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetLicenseTypeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves the LicenseType with the specified ID
        /// </summary>
        public LicenseTypeInfo GetLicenseTypeByID(int License_Type_ID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from LicenseType where License_Type_ID=@License_Type_ID", cn);
                cmd.Parameters.Add("@License_Type_ID", SqlDbType.Int).Value = License_Type_ID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetLicenseTypeFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a LicenseType
        /// </summary>
        public bool DeleteLicenseType(int License_Type_ID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from LicenseType where License_Type_ID=@License_Type_ID", cn);
                cmd.Parameters.Add("@License_Type_ID", SqlDbType.Int).Value = License_Type_ID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new LicenseType
        /// </summary>
        public int InsertLicenseType(LicenseTypeInfo LicenseType)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into LicenseType " +
              "(Description,LicenseCode, Instruction) " +
              "VALUES (" +
              "@Description, @LicenseCode, @Instruction) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = LicenseType.Description;
                cmd.Parameters.Add("@LicenseCode", SqlDbType.VarChar).Value = LicenseType.LicenseCode;
                cmd.Parameters.Add("@Instruction", SqlDbType.VarChar).Value = LicenseType.Instruction;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a LicenseType
        /// </summary>
        public bool UpdateLicenseType(LicenseTypeInfo LicenseType)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update LicenseType set " +
              "Description = @Description, " +
              "LicenseCode = @LicenseCode, " +
              "Instruction = @Instruction " +
              "where License_Type_ID = @License_Type_ID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = LicenseType.Description;
                cmd.Parameters.Add("@LicenseCode", SqlDbType.VarChar).Value = LicenseType.LicenseCode;
                cmd.Parameters.Add("@License_Type_ID", SqlDbType.Int).Value = LicenseType.License_Type_ID;
                cmd.Parameters.Add("@Instruction", SqlDbType.VarChar).Value = LicenseType.Instruction;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new LicenseTypeInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual LicenseTypeInfo GetLicenseTypeFromReader(IDataReader reader)
        {
            return GetLicenseTypeFromReader(reader, true);
        }
        protected virtual LicenseTypeInfo GetLicenseTypeFromReader(IDataReader reader, bool readMemos)
        {
            LicenseTypeInfo LicenseType = new LicenseTypeInfo(
              (int)reader["License_Type_ID"],
              reader["Description"].ToString(),
              reader["LicenseCode"].ToString(),
              reader["Instruction"].ToString());


            return LicenseType;
        }

        /// <summary>
        /// Returns a collection of LicenseTypeInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<LicenseTypeInfo> GetLicenseTypeCollectionFromReader(IDataReader reader)
        {
            return GetLicenseTypeCollectionFromReader(reader, true);
        }
        protected virtual List<LicenseTypeInfo> GetLicenseTypeCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<LicenseTypeInfo> LicenseTypes = new List<LicenseTypeInfo>();
            while (reader.Read())
                LicenseTypes.Add(GetLicenseTypeFromReader(reader, readMemos));
            return LicenseTypes;
        }
        #endregion
    }
}
#endregion