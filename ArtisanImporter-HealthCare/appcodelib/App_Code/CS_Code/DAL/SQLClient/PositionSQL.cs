﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region PositionInfo

namespace PearlsReview.DAL
{
    public class PositionInfo
    {
        public PositionInfo() { }

        public PositionInfo(int PositionID, int FacID, string Position_name, bool Position_active_ind)
        {
            this.PositionID = PositionID;
            this.Position_name = Position_name;
            this.FacID = FacID;
            this.Position_active_ind = Position_active_ind;
        }

        private int _PositionID = 0;
        public int PositionID
        {
            get { return _PositionID; }
            set { _PositionID = value; }
        }

        private int _FacID = 0;
        public int FacID
        {
            get { return _FacID; }
            set { _FacID = value; }
        }

        private string _Position_name = "";
        public string Position_name
        {
            get { return _Position_name; }
            set { _Position_name = value; }
        }

        private bool _Position_active_ind = true;
        public bool Position_active_ind
        {
            get { return _Position_active_ind; }
            set { _Position_active_ind = value; }
        }

    }
}

#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Positions

        /// <summary>
        /// Returns the total number of Positions
        /// </summary>
        public int GetPositionCount(int FacID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Position where facid=@facid", cn);
                cmd.Parameters.Add("@facid", SqlDbType.Int).Value = FacID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Positions
        /// </summary>
        public List<PositionInfo> GetPositions(string fac_id, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Position";

                if (fac_id.Length > 0)
                {
                    cSQLCommand = cSQLCommand + " where facilityid= " + fac_id;
                }

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by position_name";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetPositionCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Position with the specified ID
        /// </summary>
        public PositionInfo GetPositionByID(int PositionID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from Position where position_id=@PositionID", cn);
                cmd.Parameters.Add("@PositionID", SqlDbType.Int).Value = PositionID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetPositionFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a Position
        /// </summary>
        public bool DeletePosition(int PositionID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Position where position_id=@PositionID", cn);
                cmd.Parameters.Add("@PositionID", SqlDbType.Int).Value = PositionID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Position
        /// </summary>
        public int InsertPosition(PositionInfo Position)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Position " +
              "(facilityid,position_name,position_active_ind) " +
              "VALUES (" +
              "@facid,@position_name,@position_active_ind) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@facid", SqlDbType.Int).Value = Position.FacID;
                cmd.Parameters.Add("@position_name", SqlDbType.VarChar).Value = Position.Position_name;
                cmd.Parameters.Add("@position_active_ind", SqlDbType.Bit).Value = Position.Position_active_ind;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Position
        /// </summary>
        public bool UpdatePosition(PositionInfo Position)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Position set " +
              "position_name = @position_name, position_active_ind=@position_active_ind " +
              "where position_id = @positionID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@position_name", SqlDbType.VarChar).Value = Position.Position_name;
                cmd.Parameters.Add("@position_active_ind", SqlDbType.Bit).Value = Position.Position_active_ind;
                cmd.Parameters.Add("@positionID", SqlDbType.Int).Value = Position.PositionID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider
        /// <summary>
        /// Returns a new PositionInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual PositionInfo GetPositionFromReader(IDataReader reader)
        {
            return GetPositionFromReader(reader, true);
        }
        protected virtual PositionInfo GetPositionFromReader(IDataReader reader, bool readMemos)
        {
            PositionInfo position = new PositionInfo(
              (int)reader["position_id"],
              (int)reader["facilityid"],
              reader["position_name"].ToString(),
              (bool)reader["position_active_ind"]);

            return position;
        }

        /// <summary>
        /// Returns a collection of PositionInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<PositionInfo> GetPositionCollectionFromReader(IDataReader reader)
        {
            return GetPositionCollectionFromReader(reader, true);
        }
        protected virtual List<PositionInfo> GetPositionCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<PositionInfo> Positions = new List<PositionInfo>();
            while (reader.Read())
                Positions.Add(GetPositionFromReader(reader, readMemos));
            return Positions;
        }

        #endregion
    }
}
#endregion
