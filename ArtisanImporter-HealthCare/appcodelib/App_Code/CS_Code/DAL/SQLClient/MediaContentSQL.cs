﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region MediaContentInfo


/// <summary>
/// Summary description for MediaContentInfo
/// </summary>
public class MediaContentInfo
{
    public MediaContentInfo()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public MediaContentInfo(int mcid, int topicid, string mediatype, string asseturl, string description)
    {
        this.mcid = mcid;
        this.topicid = topicid;
        this.mediatype = mediatype;
        this.asseturl = asseturl;
        this.description = description;
    }

    private int _mcid = 0;
    public int mcid
    {
        get { return _mcid; }
        protected set { _mcid = value; }
    }
    private int _topicid = 0;
    public int topicid
    {
        get { return _topicid; }
        protected set { _topicid = value; }
    }
    private string _mediatype;
    public string mediatype
    {
        get { return _mediatype; }
        protected set { _mediatype = value; }
    }
    private string _asseturl = "";
    public string asseturl
    {
        get { return _asseturl; }
        protected set { _asseturl = value; }
    }
    private string _description = "";
    public string description
    {
        get { return _description; }
        protected set { _description = value; }
    }
}

#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider


        /// <summary>
        /// Returns the total number of TopicResourceLinks for a TopicID
        /// </summary>
        public List<MediaContentInfo> GetMediaContentByTopicID(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from mediacontent where TopicID=@TopicID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                cn.Open();
                return GetMediaContentCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public bool UpdateMediaContent(MediaContentInfo MediaContent)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update mediacontent set " +
              "TopicID = @TopicID, " +
              "mediatype = @mediatype, " +
              "asseturl = @asseturl, " +
              "description = @description " +
              "where mcid = @mcid ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = MediaContent.topicid;
                cmd.Parameters.Add("@mcid", SqlDbType.Int).Value = MediaContent.mcid;
                cmd.Parameters.Add("@mediatype", SqlDbType.VarChar).Value = MediaContent.mediatype;
                cmd.Parameters.Add("@asseturl", SqlDbType.VarChar).Value = MediaContent.asseturl;
                cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = MediaContent.description;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new mediacontent
        /// </summary>
        public int InsertMediaContent(MediaContentInfo MediaContent)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into mediacontent " +
              "(TopicID, " +
              "mediatype, " +
              "asseturl, " +
              "description) " +
              "VALUES (" +
              "@TopicID, " +
              "@mediatype, " +
              "@asseturl, " +
              "@description) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = MediaContent.topicid;
                //cmd.Parameters.Add("@mediatype", SqlDbType.Int).Value = MediaContent.mediatype;
                cmd.Parameters.Add("@mediatype", SqlDbType.VarChar).Value = MediaContent.mediatype;
                cmd.Parameters.Add("@asseturl", SqlDbType.VarChar).Value = MediaContent.asseturl;
                cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = MediaContent.description;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }
        public bool DeleteMediaContent(int mcid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from mediacontent where mcid=@mcid", cn);
                cmd.Parameters.Add("@mcid", SqlDbType.Int).Value = mcid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a collection of TopicInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<MediaContentInfo> GetMediaContentCollectionFromReader(IDataReader reader)
        {
            return GetMediaContentCollectionFromReader(reader, true);
        }
        protected virtual List<MediaContentInfo> GetMediaContentCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<MediaContentInfo> MediaContents = new List<MediaContentInfo>();
            while (reader.Read())
                MediaContents.Add(GetMediaContentFromReader(reader, readMemos));
            return MediaContents;
        }



        protected virtual MediaContentInfo GetMediaContentFromReader(IDataReader reader)
        {
            return GetMediaContentFromReader(reader, true);
        }
        protected virtual MediaContentInfo GetMediaContentFromReader(IDataReader reader, bool readMemos)
        {
            MediaContentInfo MediaContent = new MediaContentInfo(
                (int)reader["mcid"],
                (int)reader["topicid"],
                 reader["mediatype"].ToString(),
              reader["asseturl"].ToString(),
              reader["description"].ToString());

            return MediaContent;
        }
        #endregion

    }
}
#endregion