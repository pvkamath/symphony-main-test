﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Alex Chen
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region SurveyEvaluationInfo

namespace PearlsReview.DAL
{
    public class SurveyEvaluationInfo
    {
        public SurveyEvaluationInfo() { }

        public SurveyEvaluationInfo(int sID, int topicID, long testID, int qNumber, string qText, DateTime surveyDate, int qAnswer) 
        {
            this.SID = sID;
            this.TopicID = topicID;
            this.TestID = testID;
            this.QNumber = qNumber;
            this.QText = qText;
            this.SurveyDate = surveyDate;
            this.QAnswer = qAnswer;
        }

        private int _SID = 0;
        public int SID
        {
            get { return _SID; }
            set { _SID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private long _TestID = 0;
        public long TestID
        {
            get { return _TestID; }
            set { _TestID = value; }
        }

        private int _QNumber = 0;
        public int QNumber
        {
            get { return _QNumber; }
            set { _QNumber = value; }
        }

        private string _QText = "";
        public string QText
        {
            get { return _QText; }
            set { _QText = value; }
        }

        private DateTime _SurveyDate = DateTime.Now;
        public DateTime SurveyDate
        {
            get { return _SurveyDate; }
            set { _SurveyDate = value; }
        }

        private int _QAnswer = 0;
        public int QAnswer
        {
            get { return _QAnswer; }
            set { _QAnswer = value; }
        }

    }
}

#endregion SurveyEvaluationInfo

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        public int InsertSurveyEvaluation(int topicID, long testID, DateTime surveyDate, string qText, int qNumber, int qAnswer)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into SurveyEvaluation " +
                    "(TopicID, " +
                    "TestID, " +
                    "SurveyDate, " +
                    "QText, " +
                    "QNumber, " +
                    "QAnswer) " +
                    "VALUES (" +
                    "@TopicID, " +
                    "@TestID, " +
                    "@SurveyDate, " +
                    "@QText, " +
                    "@QNumber, " +
                    "@QAnswer) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = topicID;
                cmd.Parameters.Add("@TestID", SqlDbType.BigInt).Value = testID;
                cmd.Parameters.Add("@SurveyDate", SqlDbType.DateTime).Value = surveyDate;
                cmd.Parameters.Add("@QText", SqlDbType.VarChar).Value = qText;
                cmd.Parameters.Add("@QNumber", SqlDbType.Int).Value = qNumber;
                cmd.Parameters.Add("@QAnswer", SqlDbType.Int).Value = qAnswer;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;
            }
        }
    }
}

#endregion SQLPRProvider and PRProvider
