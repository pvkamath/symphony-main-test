﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;



namespace PearlsReview.DAL
{
    #region CategoryInfo

    public class CategoryInfo
    {
        public CategoryInfo() { }

        public CategoryInfo(int ID, string Title, bool BuildPage, string MetaKW, string MetaDesc,
            string MetaTitle, string PageText, bool Textbook, bool ShowInList, string CertName,
            string CredAward, string ExamType, string ExamCost, string AdminOrg, string Website,
            string Reqments, string EligCrit, string Image, int FacilityID)
        {
            this.ID = ID;
            this.Title = Title;
            this.BuildPage = BuildPage;
            this.MetaKW = MetaKW;
            this.MetaDesc = MetaDesc;
            this.MetaTitle = MetaTitle;
            this.PageText = PageText;
            this.Textbook = Textbook;
            this.ShowInList = ShowInList;
            this.CertName = CertName;
            this.CredAward = CredAward;
            this.ExamType = ExamType;
            this.ExamCost = ExamCost;
            this.AdminOrg = AdminOrg;
            this.Website = Website;
            this.Reqments = Reqments;
            this.EligCrit = EligCrit;
            this.Image = Image;
            this.FacilityID = FacilityID;
        }

        private int _ID = 0;
        public int ID
        {
            get { return _ID; }
            protected set { _ID = value; }
        }

        private string _Title = "";
        public string Title
        {
            get { return _Title; }
            private set { _Title = value; }
        }

        private bool _BuildPage = false;
        public bool BuildPage
        {
            get { return _BuildPage; }
            private set { _BuildPage = value; }
        }

        private string _MetaKW = "";
        public string MetaKW
        {
            get { return _MetaKW; }
            private set { _MetaKW = value; }
        }

        private string _MetaDesc = "";
        public string MetaDesc
        {
            get { return _MetaDesc; }
            private set { _MetaDesc = value; }
        }

        private string _MetaTitle = "";
        public string MetaTitle
        {
            get { return _MetaTitle; }
            private set { _MetaTitle = value; }
        }

        private string _PageText = "";
        public string PageText
        {
            get { return _PageText; }
            private set { _PageText = value; }
        }

        private bool _Textbook = false;
        public bool Textbook
        {
            get { return _Textbook; }
            private set { _Textbook = value; }
        }

        private bool _ShowInList = false;
        public bool ShowInList
        {
            get { return _ShowInList; }
            private set { _ShowInList = value; }
        }

        private string _CertName = "";
        public string CertName
        {
            get { return _CertName; }
            private set { _CertName = value; }
        }

        private string _CredAward = "";
        public string CredAward
        {
            get { return _CredAward; }
            private set { _CredAward = value; }
        }

        private string _ExamType = "";
        public string ExamType
        {
            get { return _ExamType; }
            private set { _ExamType = value; }
        }

        private string _ExamCost = "";
        public string ExamCost
        {
            get { return _ExamCost; }
            private set { _ExamCost = value; }
        }

        private string _AdminOrg = "";
        public string AdminOrg
        {
            get { return _AdminOrg; }
            private set { _AdminOrg = value; }
        }

        private string _Website = "";
        public string Website
        {
            get { return _Website; }
            private set { _Website = value; }
        }

        private string _Reqments = "";
        public string Reqments
        {
            get { return _Reqments; }
            private set { _Reqments = value; }
        }

        private string _EligCrit = "";
        public string EligCrit
        {
            get { return _EligCrit; }
            private set { _EligCrit = value; }
        }

        private string _Image = "";
        public string Image
        {
            get { return _Image; }
            private set { _Image = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            private set { _FacilityID = value; }
        }

    }
#endregion

    #region CategoryTopicCountInfo

    public class CategoryTopicCountInfo
    {
        public CategoryTopicCountInfo() { }

        public CategoryTopicCountInfo(int ID, string Title, int TopicCount, string DTitle)
        {
            this.ID = ID;
            this.Title = Title;
            this.TopicCount = TopicCount;
            this.DTitle = DTitle;
        }

        private int _ID = 0;
        public int ID
        {
            get { return _ID; }
            protected set { _ID = value; }
        }

        private string _Title = "";
        public string Title
        {
            get { return _Title; }
            private set { _Title = value; }
        }

        private string _DTitle = "";
        public string DTitle
        {
            get { return _DTitle; }
            private set { _DTitle = value; }
        }

        private int _TopicCount = 0;
        public int TopicCount
        {
            get { return _TopicCount; }
            private set { _TopicCount = value; }
        }

    }
    #endregion

    #region CheckBoxListCategoryInfo

    public class CheckBoxListCategoryInfo
    {
        public CheckBoxListCategoryInfo() { }

        public CheckBoxListCategoryInfo(int ID, string Title, bool Selected)
        {
            this.ID = ID;
            this.Title = Title;
            this.Selected = Selected;
        }

        private int _ID = 0;
        public int ID
        {
            get { return _ID; }
            protected set { _ID = value; }
        }

        private string _Title = "";
        public string Title
        {
            get { return _Title; }
            private set { _Title = value; }
        }

        private bool _Selected = false;
        public bool Selected
        {
            get { return _Selected; }
            private set { _Selected = value; }
        }


    }
    #endregion

}


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Categories

        /// <summary>
        /// Returns the total number of Categories
        /// </summary>
        public  int GetCategoryCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Categories where FacilityID=0 ", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of Textbook Categories
        /// </summary>
        public  int GetTextbookCategoryCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Categories where Textbook='1' and FacilityID=0 ", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of Non-Textbook Categories
        /// </summary>
        public  int GetNonTextbookCategoryCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Categories where Textbook='0' and FacilityID=0 ", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all Categories
        /// </summary>
        public  List<CategoryInfo> GetCategories(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Categories where FacilityID=0 ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all ComplianceCategories
        /// </summary>
        public  List<CategoryInfo> GetComplianceCategories(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Categories where title like 'Compliance%' and FacilityID=0 ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Categories(Bsk New)
        /// </summary>
        public  List<CategoryInfo> GetAllCategories(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Categories ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves all Categories(Bsk New)
        /// </summary>
        public  List<CategoryInfo> GetAllPrimaryCategories(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select * " +
                //    "from Categories where showinlist=1 and facilityid = 0 ";

                string cSQLCommand = "select   " +
                        "id,  " +
                        " case facilityid  " +
                                "when 0 then 'CE Course -'+title  " +
                                "when -1 then 'Review -' +title  " +
                                "when -2 then 'PR Textbook-'+title  " +
                                "else 'Facility -'+title  " +
                            " end as title,  " +
                        " BuildPage, MetaKW, MetaDesc, MetaTitle, PageText, " +
                        " Textbook, ShowInList, CertName, CredAward, ExamType, ExamCost,  " +
                        " AdminOrg, Website, Reqments, EligCrit, Image," +
                        "Facilityid   " +
                        "from categories  ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves all Textbook Categories
        /// </summary>
        public  List<CategoryInfo> GetTextbookCategories(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Categories where textbook='1' and FacilityID=0 ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Non-Textbook Categories
        /// </summary>
        public  List<CategoryInfo> GetNonTextbookCategories(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Categories where textbook='0' ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Textbook Categories that should be displayed to the website users
        /// (those with ShowInList true)
        /// </summary>
        public  List<CategoryInfo> GetTextbookCategoriesForDisplay(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Categories where textbook='1' and showinlist='1' and FacilityID=0 ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Non-Textbook Categories that should be displayed to the website users
        /// (those with ShowInList true)
        /// </summary>
        public  List<CategoryInfo> GetNonTextbookCategoriesForDisplay(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Categories where textbook='0' and showinlist='1' and FacilityID=0 ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all Non-Textbook Categories that should be displayed to the website users
        /// (those with ShowInList true)
        /// </summary>
        public List<CategoryInfo> GetPearlsCategoriesByFacilityId(int FacilityID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Categories where  showinlist='1' and FacilityID = @FacilityID ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;

                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Non-Textbook Categories with topic counts that should be displayed to the website users
        /// (those with ShowInList true)
        /// </summary>
        public  List<CategoryTopicCountInfo> GetNonTextbookCategoriesWithTopicCount(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "categories.id, " +
                    "categories.title, " +
                    "categories.title As DTitle, " +
                    "(select Cast(count(*) as int)" +
                    "   from topic " +
                    "   join categorylink on Topic.topicid = categorylink.topicid " +
                    "   where categorylink.CategoryID = Categories.ID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
                    "   group by categorylink.categoryid )  As topiccount " +
                    //Bsk New Join (CategoryAreaLink)
                     "from Categories where textbook='0' and showinlist='1' " +
                    " and id in (select cal.categoryid from CategoryAreaLink cal " +
                    "    join categorylink cl on cal.CategoryID=cl.CategoryID " +
                    "    join topic tp on cl.topicid = tp.topicid  " +
                    "    where tp.Obsolete = '0' and tp.Active_Ind = '1' and cal.areaid = 0  ) ";

                //"from Categories where textbook='0' and showinlist='1' and FacilityID=0 " +
                //" and id in (select categoryid from categorylink " +
                //"    join topic on categorylink.topicid = topic.topicid " +
                //"    where Topic.Obsolete = '0' and Topic.Active_Ind = '1' ) " ;



                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves all Non-Textbook Categories with topic counts that should be displayed to the website users
        /// (those with ShowInList true)
        /// </summary>
        public  List<CategoryTopicCountInfo> GetRNonTextbookCategoriesWithTopicCount(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                     "c.website As DTitle ," +
                     "c.id,c.title+ ' ('+convert(varchar(3),count(*)) +')' as title,count(*) As topiccount" +
                     "   from categories c  join categoryarealink ca on c.id=ca.categoryid join categorylink cl on c.id=cl.categoryid  " +
                    "   join topic t on cl.topicid=t.topicid " +
                     "   where ca.areaid='2' and c.showinlist='1' " +
                     "and t.Obsolete = '0' and t.Avail_Ind = '1' " +
                     "  group by c.id ,c.website,c.title";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all Non-Textbook Unlimited Categories with topic counts that should be displayed to the website users
        /// (those with ShowInList true)
        /// </summary>
        public List<CategoryTopicCountInfo> GetRNonTextbookUnlimitedCategoriesWithTopicCount(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                       "c.website As DTitle ," +
                       "c.id, " +
                       "c.title+ '('+convert(varchar(3),count(*)) +')' as title,count(*) As topiccount " +
                       "   from categories c   " +
                       "   inner join categorylink cl on c.id=cl.categoryid  " +
                       "   inner join topic t on cl.topicid=t.topicid   " +
                       "   join categoryarealink cal on c.id=cal.categoryid and cal.areaid=2   " +
                       "   where t.avail_ind=1 and t.obsolete='0'  and t.online_ind=1 " +
                       "   	and c.showinlist='1' and (t.uce_ind='1' or t.hours<=1.5)   " +
                       "   group by c.id ,c.website,c.title ";
                   


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Non-Textbook Categories with topic counts that should be displayed to the website users by facility id
        /// (those with ShowInList true)
        /// </summary>
        public  List<CategoryTopicCountInfo> GetNonTextbookCategoriesWithTopicCountByFacilityId(int facilityid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "categories.id, " +
                    "categories.title, " +
                    "categories.website as DTitle, " +
                    "(select Cast(count(*) as int)" +
                    "   from topic " +
                    "   join categorylink on Topic.topicid = categorylink.topicid " +
                    "   where categorylink.CategoryID = Categories.ID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
                    "   group by categorylink.categoryid )  As topiccount " +
                    //Bsk New Join (CategoryAreaLink)
                    "from Categories where textbook='0' and showinlist='1' " +
                      " and id in (select cal.categoryid from CategoryAreaLink cal " +
                      "    join categorylink cl on cal.CategoryID=cl.CategoryID " +
                      "    join topic tp on cl.topicid = tp.topicid  " +
                      "    where tp.Obsolete = '0' and tp.Active_Ind = '1' and cal.areaid = @facilityid  ) ";

                //"from Categories where textbook='0' and showinlist='1' and FacilityID=@facilityid " +
                //" and id in (select categoryid from categorylink " +
                //"    join topic on categorylink.topicid = topic.topicid " +
                //"    where Topic.Obsolete = '0' and Topic.Active_Ind = '1' ) " ;



                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;

                cn.Open();
                return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves all Non-Textbook Categories with topic counts that should be displayed to the website users by facility id
        /// (those with ShowInList true)
        /// </summary>
        public  List<CategoryTopicCountInfo> GetTextbookCategoriesWithTopicCountByFacilityId(int facilityid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "categories.id, " +
                    "categories.title, " +
                    "categories.title as DTitle, " +
                    "(select Cast(count(*) as int)" +
                    "   from topic " +
                    "   join categorylink on Topic.topicid = categorylink.topicid " +
                    "   where categorylink.CategoryID = Categories.ID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
                    "   group by categorylink.categoryid )  As topiccount " +
                    //Bsk New Join (CategoryAreaLink)
                     "from Categories where textbook='1' and showinlist='1' " +
                    " and id in (select cal.categoryid from CategoryAreaLink cal " +
                    "    join categorylink cl on cal.CategoryID=cl.CategoryID " +
                    "    join topic tp on cl.topicid = tp.topicid  " +
                    "    where tp.Obsolete = '0' and tp.Active_Ind = '1' and cal.areaid = @facilityid ) ";

                //"from Categories where textbook='1' and showinlist='1' and FacilityID=@facilityid " +
                //" and id in (select categoryid from categorylink " +
                //"    join topic on categorylink.topicid = topic.topicid " +
                //"    where Topic.Obsolete = '0' and Topic.Active_Ind = '1' ) ";



                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;

                cn.Open();
                return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }




        /// <summary>
        /// Retrieves the Category with the specified ID
        /// </summary>
        public  CategoryInfo GetCategoryByID(int ID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Categories where ID=@ID", cn);
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCategoryFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a Category
        /// </summary>
        public  bool DeleteCategory(int ID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Categories where ID=@ID", cn);
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Category
        /// </summary>
        public  int InsertCategory(CategoryInfo Category)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Categories " +
              "(Title, " +
              "BuildPage, " +
              "MetaKW, " +
              "MetaDesc, " +
              "MetaTitle, " +
              "PageText, " +
              "Textbook, " +
              "ShowInList, " +
              "CertName, " +
              "CredAward, " +
              "ExamType, " +
              "ExamCost, " +
              "AdminOrg, " +
              "Website, " +
              "Reqments, " +
              "EligCrit, " +
              "Image, " +
                "FacilityID) " +
              "VALUES (" +
              "@Title, " +
              "@BuildPage, " +
              "@MetaKW, " +
              "@MetaDesc, " +
              "@MetaTitle, " +
              "@PageText, " +
              "@Textbook, " +
              "@ShowInList, " +
              "@CertName, " +
              "@CredAward, " +
              "@ExamType, " +
              "@ExamCost, " +
              "@AdminOrg, " +
              "@Website, " +
              "@Reqments, " +
              "@EligCrit, " +
              "@Image, " +
                "@FacilityID ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = Category.Title;
                cmd.Parameters.Add("@BuildPage", SqlDbType.Bit).Value = Category.BuildPage;
                cmd.Parameters.Add("@MetaKW", SqlDbType.VarChar).Value = Category.MetaKW;
                cmd.Parameters.Add("@MetaDesc", SqlDbType.VarChar).Value = Category.MetaDesc;
                cmd.Parameters.Add("@MetaTitle", SqlDbType.VarChar).Value = Category.MetaTitle;
                cmd.Parameters.Add("@PageText", SqlDbType.VarChar).Value = Category.PageText;
                cmd.Parameters.Add("@Textbook", SqlDbType.Bit).Value = Category.Textbook;
                cmd.Parameters.Add("@ShowInList", SqlDbType.Bit).Value = Category.ShowInList;
                cmd.Parameters.Add("@CertName", SqlDbType.VarChar).Value = Category.CertName;
                cmd.Parameters.Add("@CredAward", SqlDbType.VarChar).Value = Category.CredAward;
                cmd.Parameters.Add("@ExamType", SqlDbType.VarChar).Value = Category.ExamType;
                cmd.Parameters.Add("@ExamCost", SqlDbType.VarChar).Value = Category.ExamCost;
                cmd.Parameters.Add("@AdminOrg", SqlDbType.VarChar).Value = Category.AdminOrg;
                cmd.Parameters.Add("@Website", SqlDbType.VarChar).Value = Category.Website;
                cmd.Parameters.Add("@Reqments", SqlDbType.VarChar).Value = Category.Reqments;
                cmd.Parameters.Add("@EligCrit", SqlDbType.VarChar).Value = Category.EligCrit;
                cmd.Parameters.Add("@Image", SqlDbType.VarChar).Value = Category.Image;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = Category.FacilityID;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Category
        /// </summary>
        public  bool UpdateCategory(CategoryInfo Category)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Categories set " +
              "Title = @Title, " +
              "BuildPage = @BuildPage, " +
              "MetaKW = @MetaKW, " +
              "MetaDesc = @MetaDesc, " +
              "MetaTitle = @MetaTitle, " +
              "PageText = @PageText, " +
              "Textbook = @Textbook, " +
              "ShowInList = @ShowInList, " +
              "CertName = @CertName, " +
              "CredAward = @CredAward, " +
              "ExamType = @ExamType, " +
              "ExamCost = @ExamCost, " +
              "AdminOrg = @AdminOrg, " +
              "Website = @Website, " +
              "Reqments = @Reqments, " +
              "EligCrit = @EligCrit, " +
              "Image = @Image, " +
              "FacilityID = @FacilityID " +
              "where ID = @ID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = Category.Title;
                cmd.Parameters.Add("@BuildPage", SqlDbType.Bit).Value = Category.BuildPage;
                cmd.Parameters.Add("@MetaKW", SqlDbType.VarChar).Value = Category.MetaKW;
                cmd.Parameters.Add("@MetaDesc", SqlDbType.VarChar).Value = Category.MetaDesc;
                cmd.Parameters.Add("@MetaTitle", SqlDbType.VarChar).Value = Category.MetaTitle;
                cmd.Parameters.Add("@PageText", SqlDbType.VarChar).Value = Category.PageText;
                cmd.Parameters.Add("@Textbook", SqlDbType.Bit).Value = Category.Textbook;
                cmd.Parameters.Add("@ShowInList", SqlDbType.Bit).Value = Category.ShowInList;
                cmd.Parameters.Add("@CertName", SqlDbType.VarChar).Value = Category.CertName;
                cmd.Parameters.Add("@CredAward", SqlDbType.VarChar).Value = Category.CredAward;
                cmd.Parameters.Add("@ExamType", SqlDbType.VarChar).Value = Category.ExamType;
                cmd.Parameters.Add("@ExamCost", SqlDbType.VarChar).Value = Category.ExamCost;
                cmd.Parameters.Add("@AdminOrg", SqlDbType.VarChar).Value = Category.AdminOrg;
                cmd.Parameters.Add("@Website", SqlDbType.VarChar).Value = Category.Website;
                cmd.Parameters.Add("@Reqments", SqlDbType.VarChar).Value = Category.Reqments;
                cmd.Parameters.Add("@EligCrit", SqlDbType.VarChar).Value = Category.EligCrit;
                cmd.Parameters.Add("@Image", SqlDbType.VarChar).Value = Category.Image;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = Category.FacilityID;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = Category.ID;

                //    string cBuildPage = "'0'";
                //  if (Category.BuildPage == true) 
                //  {cBuildPage = "'1'";}

                //    SqlCommand cmd = new SqlCommand("update Categories set " +
                //"Title = '" + Category.Title + "', " +
                //"Order = " + Category.Order.ToString() + ", " +
                //"BuildPage = " + cBuildPage + ", " +
                //"MetaKW = '" + Category.MetaKW + "', " +
                //"MetaDesc = '" + Category.MetaDesc + "', " +
                //"MetaTitle = '" + Category.MetaTitle + "', " +
                //"PageText = '" + Category.PageText + "' " + 
                //"where ID = " + Category.ID.ToString(), cn);

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Returns the total number of categories for a TopicID
        /// </summary>
        public  int GetCategoryCountByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from categories " +
                    "join categorylink on categories.id = categorylink.categoryid " +
                    "where categorylink.TopicID = @TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        ///// <summary>
        ///// Retrieves all categories, then selects just 4th to 6th items
        ///// </summary>
        //public override List<CategoryInfo> GetCategoriesX(string cSortExpression)
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {
        //        string cSQLCommand = "select * from (select " +
        //            "ID, " +
        //            "Title, " +
        //            "Order " +
        //            "from categories";

        //        // add on ORDER BY if provided
        //        if (cSortExpression.Length > 0)
        //        {
        //            cSQLCommand = cSQLCommand +
        //                " order by " + cSortExpression;
        //        }

        //        cSQLCommand = cSQLCommand +
        //            ") catlist where recno() > 3 and recno() < 7 " ;

        //        SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

        //        cn.Open();
        //        return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
        //    }
        //}

        /// <summary>
        /// Retrieves all categories for a TopicID
        /// </summary>
        public  List<CategoryInfo> GetCategoriesByTopicID(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from categories " +
                    "join categorylink on categories.id = categorylink.categoryid " +
                    "where categorylink.TopicID = @TopicID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all categories, plus topics assignments for a TopicID
        /// </summary>
        public  List<CheckBoxListCategoryInfo> GetAllCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select ca.ID, ca.Title, " +
                    "CAST( (CASE WHEN (cl.TopicID IS NULL) THEN '0' ELSE '1' END) As bit ) AS Selected from categories ca " +
                    "left outer join categorylink cl on ca.id = cl.categoryid " +
                    "and cl.TopicID = @TopicID where ca.FacilityID=0 ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return GetCheckBoxListCategoryCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Retrieves all non-textbook categories, plus topics assignments for a TopicID
        /// </summary>
        public List<CheckBoxListCategoryInfo> GetAllNonTextbookCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select ca.ID, a.tabname + ' - ' + ca.Title as Title, " +
                //    "CAST( (CASE WHEN (cl.TopicID IS NULL) THEN '0' ELSE '1' END) As bit ) AS Selected from topic t join categories ca on (t.facilityid=ca.facilityid or (t.facilityid=0 and ca.facilityid>2))" +
                //    " join areatype a on ca.facilityid=a.areaid " +
                //    "left outer join categorylink cl on ca.id = cl.categoryid " +
                //    "and cl.TopicID = @TopicID where t.topicid=@TopicID ";

                //Changes made on 10/20/2010

                string cSQLCommand = "select cl.TopicID, a.areaid, ca.ID, a.tabname + ' - ' + ca.Title as Title, CAST( (CASE WHEN (cl.TopicID IS NULL) THEN '0' ELSE '1' END) As bit ) AS Selected " +
                                    "from areatype as a " +
                                    "join categories as ca on a.areaid = ca.facilityid " +
                                    "left outer join categorylink cl on ca.id = cl.categoryid and cl.topicid = @TopicID ";
                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return GetCheckBoxListCategoryCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Retrieves all textbook categories, plus topics assignments for a TopicID
        /// </summary>
        public  List<CheckBoxListCategoryInfo> GetAllTextbookCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select ca.ID, ca.Title, " +
                //    "IIF(ISNULL(cl.TopicID), '0', '1') AS Selected from categories ca " +
                //    "left outer join categorylink cl on ca.id = cl.categoryid " +
                //    "and cl.TopicID = ? where ca.textbook='1'";
                string cSQLCommand = "select ca.ID, ca.Title, " +
                    "CAST( (CASE WHEN (cl.TopicID IS NULL) THEN '0' ELSE '1' END) As bit ) AS Selected from categories ca " +
                    "left outer join categorylink cl on ca.id = cl.categoryid " +
                    "and cl.TopicID = @TopicID where ca.textbook='1' and ca.FacilityID=0 ";


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return GetCheckBoxListCategoryCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Retrieves all textbook categories NOT assigned to a specific TopicID
        /// </summary>
        public  List<CategoryInfo> GetTextbookCategoriesNotAssignedToTopicID(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from categories " +
                    "where textbook='1' and ca.FacilityID=0 and id not in " +
                    "(select categoryid from categorylink " +
                    "where categorylink.TopicID = @TopicID)";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all non-textbook categories NOT assigned to a specific TopicID
        /// </summary>
        public  List<CategoryInfo> GetNonTextbookCategoriesNotAssignedToTopicID(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from categories " +
                    "where textbook='0' and ca.FacilityID=0 and id not in " +
                    "(select categoryid from categorylink " +
                    "where categorylink.TopicID = @TopicID)";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        public  CategoryInfo GetCatNameByTopicId(int TopicId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from categories inner join categorylink on categories.id=categorylink.categoryid where categorylink.topicid=@TopicId", cn);
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCategoryFromReader(reader, true);
                else
                    return null;
            }

        }
        public CategoryInfo GetLastTopicCatByUserID(int UserId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select top 1 a.* from categories a join topic b on a.id=b.categoryid" +
                                                                                    " join test c on b.topicid=c.topicid"+
                                                                                    " where c.userid=@UserId order by lastmod desc", cn);
                cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = UserId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCategoryFromReader(reader, true);
                else
                    return null;
            }

        }
        
        public  CategoryInfo GetPrimaryCatNameByTopicId(int TopicId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from categories where id in ( select categoryid from topic where topicid=@TopicId )", cn);
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCategoryFromReader(reader, true);
                else
                    return null;
            }

        }

        public  string CheckCatNameByTopicId(int TopicId, string CatName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select website from categories inner join categorylink on categories.id=categorylink.categoryid where categorylink.topicid=@TopicId and categories.website=@Title", cn);
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = CatName;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return reader[0].ToString();
                else
                    return null;
            }
        }

        public DataSet GetCategoryByWebSitePRRetail(string website, string areaid)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Categories inner join categoryarealink on categories.id=categoryarealink.categoryid where website=@website and areaid=@areaid", cn);
                cmd.Parameters.Add("@website", SqlDbType.VarChar).Value = website;
                cmd.Parameters.Add("@areaid", SqlDbType.VarChar).Value = areaid;
                cn.Open();

                cmd.CommandType = CommandType.Text;
                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds == null)
                {
                    return null;
                }
                else
                {
                    return ds;
                }
            }
        }
        public  DataSet GetCategoryByWebSite(string website)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Categories inner join categoryarealink on categories.id=categoryarealink.categoryid where website=@website and areaid=2", cn);
                cmd.Parameters.Add("@website", SqlDbType.VarChar).Value = website;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds == null)
                {
                    return null;
                }
                else
                {
                    return ds;
                }
            }
        }

        public CategoryInfo GetPearlsCategoryByWebSite(string website)
        {
            DataSet ds = new DataSet();
            //SqlDataAdapter da;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select top 1 * " +
                        "from Categories where showinlist='1' and facilityid in (-1,-2) and website=@website", cn);
                cmd.Parameters.Add("@website", SqlDbType.VarChar).Value = website;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCategoryFromReader(reader, true);
                else
                    return null;
            }
        }

        public CategoryInfo GetMSCategoryByWebSite(string website)
        {
            DataSet ds = new DataSet();
            //SqlDataAdapter da;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select top 1 * " +
                        "from Categories where showinlist='1' and website=@website", cn);
                cmd.Parameters.Add("@website", SqlDbType.VarChar).Value = website;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetCategoryFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves all Non-Textbook State Specific Categories with topic counts that should be displayed to the website users
        /// (those with ShowInList true)
        /// </summary>
        public  List<CategoryTopicCountInfo> GetRNonTextbookStateCategoriesWithTopicCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_State_CE";
                cmd.CommandType = CommandType.StoredProcedure;
                return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        #endregion


        /// <summary>
        /// Retrieves all categories by facilityid
        /// </summary>
        public List<CategoryInfo> GetCategoriesByFacilityID(int FacilityID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select categories.* from categories join categoryarealink on categories.id=categoryarealink.categoryid" +
                        "where showinlist='1' and categoryarealink.areaid=@FacilityID";
                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all ice category by userid
        /// </summary>
        public int GetICECategoryIDByUserID(int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                int categoryid = 0;
                string cSQLCommand = "select top 1 c.id from Categories c join UserDiscipline u on c.alterid=u.disciplineid where userid=@userid and u.primary_ind=1 and c.facilityid=30";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                SqlDataReader reader = null;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cn.Open();
                reader = cmd.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.HasRows)
                {
                    reader.Read();
                    categoryid = reader.GetInt32(0);
                    reader.Close();
                }
                cn.Close();
                return categoryid;
            }
        }


        /// <summary>
        /// Retrieves all categories who has topics by facilityid
        /// </summary>
        public List<CategoryInfo> GetCategorieswithTopicByFacilityID(int FacilityID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct categories.* from categories join categoryarealink on categories.id=categoryarealink.categoryid " +
                    "inner join categorylink on categories.id=categorylink.categoryid " +
                    "where showinlist='1' and categoryarealink.areaid=@FacilityID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all categories who has topics by facilityid
        /// </summary>
        public List<CategoryInfo> GetCategorieswithTopicByAreaname(string Areaname, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct categories.* from categories join categoryarealink on categories.id=categoryarealink.categoryid " +
                    "inner join categorylink on categories.id=categorylink.categoryid " +
                    " inner join areatype on categoryarealink.areaid=areatype.areaid " +
                    "where showinlist='1' and areatype.areaname=@Areaname";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@Areaname", SqlDbType.VarChar).Value = Areaname;
                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all categories who has topics by facilityid
        /// </summary>
        public List<CategoryInfo> GetCategorieswithTopicByTabname(string Tabname, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct categories.* from categories join categoryarealink on categories.id=categoryarealink.categoryid " +
                    "inner join categorylink on categories.id=categorylink.categoryid " +
                    " inner join topic on topic.topicid=categorylink.topicid " +
                    " inner join areatype on categoryarealink.areaid=areatype.areaid " +
                    "where showinlist='1' and areatype.tabname=@Tabname and topic.active_ind=1 ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@Tabname", SqlDbType.VarChar).Value = Tabname;
                cn.Open();
                return GetCategoryCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public DataSet GetCategoriesandAreaTypeByAreaname(string Areaname, string cSortExpression)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select a.*, c.* " +
                   "from Categories c inner join categoryarealink ca on c.id=ca.categoryid  join areatype a on ca.areaid=a.areaid where areaname=@Areaname and showinlist='1' ";
               
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@Areaname", SqlDbType.VarChar).Value = Areaname;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds == null)
                {
                    return null;
                }
                else
                {
                    return ds;
                }
            }
        }

        public DataSet GetCategoriesandAreaTypeByTabname(string Tabname, string cSortExpression)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select a.*, c.* " +
                   "from Categories c inner join categoryarealink ca on c.id=ca.categoryid  join areatype a on ca.areaid=a.areaid where tabname=@Tabname and showinlist='1' ";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@Tabname", SqlDbType.VarChar).Value = Tabname;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds == null)
                {
                    return null;
                }
                else
                {
                    return ds;
                }
            }
        }

        /// <summary>
        /// Retrieves all  Categories with topic counts that should be displayed to the website users by tabname
        /// (those with ShowInList true)
        /// </summary>
        public List<CategoryTopicCountInfo> GetCategorieswithTopicCountByTabName(string Tabname, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct " +
                    "categories.id, " +
                    "categories.title, " +
                    "categories.title as DTitle, " +
                    "(select Cast(count(*) as int)" +
                    "   from topic " +
                    "   join categorylink on Topic.topicid = categorylink.topicid " +
                    "   where categorylink.CategoryID = Categories.ID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
                    "   group by categorylink.categoryid )  As topiccount " +
                    "from Categories join categoryarealink on categories.id=categoryarealink.categoryid " +
                    "inner join categorylink on categories.id=categorylink.categoryid " +
                    " inner join topic on topic.topicid=categorylink.topicid " +
                    " inner join areatype on categoryarealink.areaid=areatype.areaid " +
                    "where showinlist='1' and areatype.tabname=@Tabname and topic.active_ind=1 ";
              
                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@Tabname", SqlDbType.VarChar).Value = Tabname;

                cn.Open();
                return GetCategoryTopicCountCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public DataSet GetCategoriesByMicroSiteDomainIdWithBundle(int domainID, string sortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "microsite_Categories_getCategoriesByDomainIDWithBundle";
                cmd.Parameters.Add("@domainID", SqlDbType.Int).Value = domainID;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = sortExpression;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }


        #region PRProvider
        /// <summary>
        /// Returns a new CategoryInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual CategoryInfo GetCategoryFromReader(IDataReader reader)
        {
            return GetCategoryFromReader(reader, true);
        }
        protected virtual CategoryInfo GetCategoryFromReader(IDataReader reader, bool readMemos)
        {
            CategoryInfo Category = new CategoryInfo(
              (int)reader["ID"],
              reader["Title"].ToString(),
              (bool)reader["BuildPage"],
              reader["MetaKW"].ToString(),
              reader["MetaDesc"].ToString(),
              reader["MetaTitle"].ToString(),
              reader["PageText"].ToString(),
              (bool)reader["Textbook"],
              (bool)reader["ShowInList"],
              reader["CertName"].ToString(),
              reader["CredAward"].ToString(),
              reader["ExamType"].ToString(),
              reader["ExamCost"].ToString(),
              reader["AdminOrg"].ToString(),
              reader["Website"].ToString(),
              reader["Reqments"].ToString(),
              reader["EligCrit"].ToString(),
              reader["Image"].ToString(),
              (int)reader["FacilityID"]);

            return Category;
        }

        /// <summary>
        /// Returns a collection of CategoryInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<CategoryInfo> GetCategoryCollectionFromReader(IDataReader reader)
        {
            return GetCategoryCollectionFromReader(reader, true);
        }
        protected virtual List<CategoryInfo> GetCategoryCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<CategoryInfo> Categories = new List<CategoryInfo>();
            while (reader.Read())
                Categories.Add(GetCategoryFromReader(reader, readMemos));
            return Categories;
        }

        /// NOTE: These variations are for CategoryTopicCountInfo lists
        /// <summary>
        /// Returns a new CategoryTopicCountInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual CategoryTopicCountInfo GetCategoryTopicCountFromReader(IDataReader reader)
        {
            return GetCategoryTopicCountFromReader(reader, true);
        }
        protected virtual CategoryTopicCountInfo GetCategoryTopicCountFromReader(IDataReader reader, bool readMemos)
        {
            CategoryTopicCountInfo Category = new CategoryTopicCountInfo(
              (int)reader["ID"],
              reader["Title"].ToString(),
              (int)reader["TopicCount"],
              reader["DTitle"].ToString());

            return Category;
        }

        /// <summary>
        /// Returns a collection of CategoryTopicCountInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<CategoryTopicCountInfo> GetCategoryTopicCountCollectionFromReader(IDataReader reader)
        {
            return GetCategoryTopicCountCollectionFromReader(reader, true);
        }
        protected virtual List<CategoryTopicCountInfo> GetCategoryTopicCountCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<CategoryTopicCountInfo> Categories = new List<CategoryTopicCountInfo>();
            while (reader.Read())
                Categories.Add(GetCategoryTopicCountFromReader(reader, readMemos));
            return Categories;
        }

        /// <summary>
        /// Returns a new CheckBoxListCategoryInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual CheckBoxListCategoryInfo GetCheckBoxListCategoryFromReader(IDataReader reader)
        {
            CheckBoxListCategoryInfo CheckBoxListCategory = new CheckBoxListCategoryInfo(
              (int)reader["ID"],
              reader["Title"].ToString(),
              (bool)reader["Selected"]);

            return CheckBoxListCategory;
        }

        /// <summary>
        /// Returns a collection of CheckBoxListCategoryInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<CheckBoxListCategoryInfo> GetCheckBoxListCategoryCollectionFromReader(IDataReader reader)
        {
            List<CheckBoxListCategoryInfo> CheckBoxListCategories = new List<CheckBoxListCategoryInfo>();
            while (reader.Read())
                CheckBoxListCategories.Add(GetCheckBoxListCategoryFromReader(reader));
            return CheckBoxListCategories;
        }
        #endregion
    }
}
#endregion