﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Text.RegularExpressions;


namespace PearlsReview.DAL
{
    #region TopicInfo

    public class TopicInfo
    {
        public TopicInfo() { }

        public TopicInfo(int id, string name)
        {
            this.TopicID = id;
            this.TopicName = name;
        }

        public TopicInfo(bool CorLecture, string Fla_CEType, string Fla_IDNo, string FolderName, string HTML,
            DateTime? releaseDate, DateTime? expireDate, string LastUpdate, bool NoSurvey, int TopicID, string TopicName,
            int SurveyID, bool Compliance, string MetaKW, string MetaDesc, decimal Hours, string MediaType, string Objectives,
            string Content, bool Textbook, int CertID, string Method, string GrantBy, string DOCXFile,
            string DOCXHoFile, bool Obsolete, int FacilityID, string Course_Number,
             int Minutes, bool Audio_Ind,
            bool Apn_Ind, bool Icn_Ind, bool Jcaho_Ind, bool Magnet_Ind, bool Active_Ind, bool Video_Ind,
            bool Online_Ind, bool Ebp_Ind, bool Ccm_Ind, bool Avail_Ind, string Cert_Cerp, string Ref_Html,
            int AlterID, int Pass_Score,
            string Subtitle, string Topic_Type, string Img_Name, string Img_Credit,
            string Img_Caption, string Accreditation, int Views, int PrimaryViews, int PageReads, bool Featured,
            string rev, string urlmark, decimal offline_Cost, decimal online_Cost, bool notest, bool uce_Ind,
            string shortname, int categoryid, bool hold_ind, decimal rating_avg, int rating_num_comment,
            int rating_total, bool prepaid_ind, string topic_summary, bool cmeInd, bool cmeSponsorInd,
            string cmeSponsorName, string topic_hourlong, string topic_hourshort, DateTime? webinar_start, DateTime? webinar_end)
        {
            this.CorLecture = CorLecture;
            this.Fla_CEType = Fla_CEType;
            this.Fla_IDNo = Fla_IDNo;
            this.FolderName = FolderName;
            this.HTML = HTML;
            this.ReleaseDate = releaseDate;
            this.ExpireDate = expireDate;
            this.LastUpdate = LastUpdate;
            this.NoSurvey = NoSurvey;
            this.TopicID = TopicID;
            this.TopicName = TopicName;
            this.SurveyID = SurveyID;
            this.Compliance = Compliance;
            this.MetaKW = MetaKW;
            this.MetaDesc = MetaDesc;
            this.Hours = Hours;
            this.MediaType = MediaType;
            this.Objectives = Objectives;
            this.Content = Content;
            this.Textbook = Textbook;
            this.CertID = CertID;
            this.Method = Method;
            this.GrantBy = GrantBy;
            this.DOCXFile = DOCXFile;
            this.DOCXHoFile = DOCXHoFile;
            this.Obsolete = Obsolete;
            this.FacilityID = FacilityID;
            this.Course_Number = Course_Number;
            //this.CeRef = CeRef;
            //this.Release_Date = Release_Date;
            this.Minutes = Minutes;
            this.Audio_Ind = Audio_Ind;
            this.Apn_Ind = Apn_Ind;
            this.Icn_Ind = Icn_Ind;
            this.Jcaho_Ind = Jcaho_Ind;
            this.Magnet_Ind = Magnet_Ind;
            this.Active_Ind = Active_Ind;
            this.Video_Ind = Video_Ind;
            this.Online_Ind = Online_Ind;
            this.Ebp_Ind = Ebp_Ind;
            this.Ccm_Ind = Ccm_Ind;
            this.Avail_Ind = Avail_Ind;
            this.Cert_Cerp = Cert_Cerp;
            this.Ref_Html = Ref_Html;
            this.AlterID = AlterID;
            this.Pass_Score = Pass_Score;
            this.Subtitle = Subtitle;
            this.Topic_Type = Topic_Type;
            this.Img_Name = Img_Name;
            this.Img_Credit = Img_Credit;
            this.Img_Caption = Img_Caption;
            this.Accreditation = Accreditation;
            this.Views = Views;
            this.PrimaryViews = PrimaryViews;
            this.PageReads = PageReads;
            this.Featured = Featured;
            this.rev = rev;
            this.urlmark = urlmark;
            this.offline_Cost = offline_Cost;
            this.online_Cost = online_Cost;
            this.notest = notest;
            this.uce_Ind = uce_Ind;
            this.shortname = shortname;
            this.categoryid = categoryid;
            this.hold_ind = hold_ind;
            this.rating_avg = rating_avg;
            this.rating_num_comment = rating_num_comment;
            this.rating_total = rating_total;
            this.prepaid_ind = prepaid_ind;
            this.topic_summary = topic_summary;
            this.CMEInd = cmeInd;
            this.CMESponsorInd = cmeSponsorInd;
            this.CMESponsorName = cmeSponsorName;
            this.Topic_hourlong = topic_hourlong;
            this.Topic_hourshort = topic_hourshort;
            this.Webinar_start = webinar_start;
            this.Webinar_end = webinar_end;
        }

        private DateTime? _webinar_start = null;
        public DateTime? Webinar_start
        {
            get { return _webinar_start; }
            set { _webinar_start = value; }
        }

        private DateTime? _webinar_end = null;
        public DateTime? Webinar_end
        {
            get { return _webinar_end; }
            set { _webinar_end = value; }
        }

        private bool _CorLecture = false;
        public bool CorLecture
        {
            get { return _CorLecture; }
            private set { _CorLecture = value; }
        }

        private string _Fla_CEType = "";
        public string Fla_CEType
        {
            get { return _Fla_CEType; }
            private set { _Fla_CEType = value; }
        }

        private string _Fla_IDNo = "";
        public string Fla_IDNo
        {
            get { return _Fla_IDNo; }
            private set { _Fla_IDNo = value; }
        }

        private string _FolderName = "";
        public string FolderName
        {
            get { return _FolderName; }
            private set { _FolderName = value; }
        }

        private string _HTML = "";
        public string HTML
        {
            get { return _HTML; }
            private set { _HTML = value; }
        }

        private DateTime? _releaseDate = null;
        public DateTime? ReleaseDate
        {
            get { return _releaseDate; }
            set { _releaseDate = value; }
        }

        private DateTime? _expireDate = null;
        public DateTime? ExpireDate
        {
            get { return _expireDate; }
            set { _expireDate = value; }
        }

        private string _LastUpdate = DateTime.Now.ToShortDateString();
        public string LastUpdate
        {
            get { return _LastUpdate; }
            private set { _LastUpdate = value; }
        }

        private bool _NoSurvey = false;
        public bool NoSurvey
        {
            get { return _NoSurvey; }
            private set { _NoSurvey = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            protected set { _TopicID = value; }
        }

        //private string _TopicName = "";
        //public string TopicName
        //{
        //    get { return _TopicName.Replace("<i>","").Replace("</i>",""); }
        //    private set { _TopicName = value; }
        //}
        private string _TopicName = "";
        public string TopicName
        {
            get { return _TopicName; }
            private set { _TopicName = value; }
        }

        private int _SurveyID = 0;
        public int SurveyID
        {
            get { return _SurveyID; }
            private set { _SurveyID = value; }
        }

        private bool _Compliance = false;
        public bool Compliance
        {
            get { return _Compliance; }
            private set { _Compliance = value; }
        }

        private string _MetaKW = "";
        public string MetaKW
        {
            get { return _MetaKW; }
            private set { _MetaKW = value; }
        }

        private string _MetaDesc = "";
        public string MetaDesc
        {
            get { return _MetaDesc; }
            private set { _MetaDesc = value; }
        }

        private decimal _Hours = 0.00M;
        public decimal Hours
        {
            get { return _Hours; }
            private set { _Hours = value; }
        }

        private string _MediaType = "";
        public string MediaType
        {
            get { return _MediaType; }
            private set { _MediaType = value; }
        }

        private string _Objectives = "";
        public string Objectives
        {
            get { return _Objectives; }
            private set { _Objectives = value; }
        }

        private string _Content = "";
        public string Content
        {
            get { return _Content; }
            private set { _Content = value; }
        }

        private bool _Textbook = false;
        public bool Textbook
        {
            get { return _Textbook; }
            private set { _Textbook = value; }
        }

        private int _CertID = 0;
        public int CertID
        {
            get { return _CertID; }
            private set { _CertID = value; }
        }

        private string _Method = "";
        public string Method
        {
            get { return _Method; }
            private set { _Method = value; }
        }

        private string _GrantBy = "";
        public string GrantBy
        {
            get { return _GrantBy; }
            private set { _GrantBy = value; }
        }

        private string _DOCXFile = "";
        public string DOCXFile
        {
            get { return _DOCXFile; }
            private set { _DOCXFile = value; }
        }
        private string _DOCXHoFile = "";
        public string DOCXHoFile
        {
            get { return _DOCXHoFile; }
            private set { _DOCXHoFile = value; }
        }

        private bool _Obsolete = false;
        public bool Obsolete
        {
            get { return _Obsolete; }
            private set { _Obsolete = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            private set { _FacilityID = value; }
        }

        private string _Course_Number = "";
        public string Course_Number
        {
            get { return _Course_Number; }
            private set { _Course_Number = value; }
        }

        //private string _CeRef = "";
        //public string CeRef
        //{
        //    get { return _CeRef; }
        //    private set { _CeRef = value; }
        //}

        //private DateTime _Release_Date = System.DateTime.MinValue;
        //public DateTime Release_Date
        //{
        //    get { return _Release_Date; }
        //    private set { _Release_Date = value; }
        //}

        private int _Minutes = 0;
        public int Minutes
        {
            get { return _Minutes; }
            protected set { _Minutes = value; }
        }

        private bool _Audio_Ind = false;
        public bool Audio_Ind
        {
            get { return _Audio_Ind; }
            private set { _Audio_Ind = value; }
        }

        private bool _Apn_Ind = false;
        public bool Apn_Ind
        {
            get { return _Apn_Ind; }
            private set { _Apn_Ind = value; }
        }

        private bool _Icn_Ind = false;
        public bool Icn_Ind
        {
            get { return _Icn_Ind; }
            private set { _Icn_Ind = value; }
        }

        private bool _Jcaho_Ind = false;
        public bool Jcaho_Ind
        {
            get { return _Jcaho_Ind; }
            private set { _Jcaho_Ind = value; }
        }

        private bool _Magnet_Ind = false;
        public bool Magnet_Ind
        {
            get { return _Magnet_Ind; }
            private set { _Magnet_Ind = value; }
        }

        private bool _Active_Ind = false;
        public bool Active_Ind
        {
            get { return _Active_Ind; }
            private set { _Active_Ind = value; }
        }

        private bool _Video_Ind = false;
        public bool Video_Ind
        {
            get { return _Video_Ind; }
            private set { _Video_Ind = value; }
        }

        private bool _Online_Ind = false;
        public bool Online_Ind
        {
            get { return _Online_Ind; }
            private set { _Online_Ind = value; }
        }

        private bool _Ebp_Ind = false;
        public bool Ebp_Ind
        {
            get { return _Ebp_Ind; }
            private set { _Ebp_Ind = value; }
        }

        private bool _Ccm_Ind = false;
        public bool Ccm_Ind
        {
            get { return _Ccm_Ind; }
            private set { _Ccm_Ind = value; }
        }

        private bool _Avail_Ind = false;
        public bool Avail_Ind
        {
            get { return _Avail_Ind; }
            private set { _Avail_Ind = value; }
        }

        private string _Cert_Cerp = "";
        public string Cert_Cerp
        {
            get { return _Cert_Cerp; }
            private set { _Cert_Cerp = value; }
        }

        private string _Ref_Html = "";
        public string Ref_Html
        {
            get { return _Ref_Html.ToString().Replace(" href=", " target=\"_blank\" href="); }
            private set { _Ref_Html = value; }
        }

        private int _AlterID = 0;
        public int AlterID
        {
            get { return _AlterID; }
            protected set { _AlterID = value; }
        }

        private int _Pass_Score = 0;
        public int Pass_Score
        {
            get { return _Pass_Score; }
            protected set { _Pass_Score = value; }
        }

        private string _Subtitle = "";
        public string Subtitle
        {
            get { return _Subtitle; }
            private set { _Subtitle = value; }
        }

        //private decimal _Cost = 0.00M;
        //public decimal Cost
        //{
        //    get { return _Cost; }
        //    private set { _Cost = value; }
        //}

        private string _Topic_Type = "";
        public string Topic_Type
        {
            get { return _Topic_Type; }
            private set { _Topic_Type = value; }
        }

        private string _Img_Name = "";
        public string Img_Name
        {
            get { return _Img_Name; }
            private set { _Img_Name = value; }
        }

        private string _Img_Credit = "";
        public string Img_Credit
        {
            get { return _Img_Credit; }
            private set { _Img_Credit = value; }
        }

        private string _Img_Caption = "";
        public string Img_Caption
        {
            get { return _Img_Caption; }
            private set { _Img_Caption = value; }
        }

        private string _Accreditation = "";
        public string Accreditation
        {
            get { return _Accreditation; }
            private set { _Accreditation = value; }
        }

        private int _Views = 0;
        public int Views
        {
            get { return _Views; }
            protected set { _Views = value; }
        }

        private int _PrimaryViews = 0;
        public int PrimaryViews
        {
            get { return _PrimaryViews; }
            protected set { _PrimaryViews = value; }
        }

        private int _PageReads = 0;
        public int PageReads
        {
            get { return _PageReads; }
            protected set { _PageReads = value; }
        }

        private bool _Featured = false;
        public bool Featured
        {
            get { return _Featured; }
            private set { _Featured = value; }
        }

        private bool _notest = false;
        public bool notest
        {
            get { return _notest; }
            set { _notest = value; }
        }

        private bool _uce_Ind = false;
        public bool uce_Ind
        {
            get { return _uce_Ind; }
            set { _uce_Ind = value; }
        }

        private string _rev = "";
        public string rev
        {
            get { return _rev; }
            set { _rev = value; }
        }
        private string _urlmark = "";
        public string urlmark
        {
            get { return _urlmark; }
            set { _urlmark = value; }
        }

        private decimal _online_Cost = 0.00M;
        public decimal online_Cost
        {
            get { return _online_Cost; }
            set { _online_Cost = value; }
        }
        private decimal _offline_Cost = 0.00M;
        public decimal offline_Cost
        {
            get { return _offline_Cost; }
            set { _offline_Cost = value; }
        }

        private string _shortname = "";
        public string shortname
        {
            get { return _shortname; }
            set { _shortname = value; }
        }

        private int _categoryid = 0;
        public int categoryid
        {
            get { return _categoryid; }
            set { _categoryid = value; }
        }

        private bool _hold_ind = false;
        public bool hold_ind
        {
            get { return _hold_ind; }
            set { _hold_ind = value; }
        }
        private decimal _rating_avg = (decimal)0.0;
        public decimal rating_avg
        {
            get { return _rating_avg; }
            set { _rating_avg = value; }
        }

        private int _rating_num_comment = 0;
        public int rating_num_comment
        {
            get { return _rating_num_comment; }
            set { _rating_num_comment = value; }
        }
        private int _rating_total = 0;
        public int rating_total
        {
            get { return _rating_total; }
            set { _rating_total = value; }
        }
        private bool _prepaid_ind = false;
        public bool prepaid_ind
        {
            get { return _prepaid_ind; }
            set { _prepaid_ind = value; }
        }
        private string _topic_summary = "";
        public string topic_summary
        {
            get { return _topic_summary; }
            set { _topic_summary = value; }
        }

        private bool _CMEInd = false;
        public bool CMEInd
        {
            get { return _CMEInd; }
            set { _CMEInd = value; }
        }

        private bool _CMESponsorInd = false;
        public bool CMESponsorInd
        {
            get { return _CMESponsorInd; }
            set { _CMESponsorInd = value; }
        }

        private string _CMESponsorName = "";
        public string CMESponsorName
        {
            get { return _CMESponsorName; }
            set { _CMESponsorName = value; }
        }
        private string _topic_hourlong = "";
        public string Topic_hourlong
        {
            get { return _topic_hourlong; }
            set { _topic_hourlong = value; }
        }
        private string _topic_hourshort = "";
        public string Topic_hourshort
        {
            get { return _topic_hourshort; }
            set { _topic_hourshort = value; }
        }
    }
    #endregion

    #region CheckBoxListTopicInfo
    public class CheckBoxListTopicInfo
    {
        public CheckBoxListTopicInfo() { }

        public CheckBoxListTopicInfo(int TopicID, string TopicName, bool Selected)
        {
            this.TopicID = TopicID;
            this.TopicName = TopicName;
            this.Selected = Selected;
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            protected set { _TopicID = value; }
        }

        private string _TopicName = "";
        public string TopicName
        {
            get { return _TopicName; }
            private set { _TopicName = value; }
        }

        private bool _Selected = false;
        public bool Selected
        {
            get { return _Selected; }
            private set { _Selected = value; }
        }
    }
    #endregion
}


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Topics

        /// <summary>
        /// Returns the total number of Topics (not obsolete ones)
        /// </summary>
        public int GetTopicCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Topic where Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=0 ", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of Textbook Topics (not obsolete ones)
        /// </summary>
        public int GetTextbookTopicCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Topic where textbook='1' and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=0 ", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of Non-Textbook Topics (not obsolete ones)
        /// </summary>
        public int GetNonTextbookTopicCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Topic where textbook='0' and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=0 ", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of Obsolete Topics 
        /// </summary>
        public int GetObsoleteTopicCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Topic where Obsolete = '1' and FacilityID=0 ", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of Obsolete Textbook Topics 
        /// </summary>
        public int GetObsoleteTextbookTopicCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Topic where textbook='1' and Obsolete = '1' and FacilityID=0 ", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the total number of Obsolete Non-Textbook Topics 
        /// </summary>
        public int GetObsoleteNonTextbookTopicCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Topic where textbook='0' and Obsolete = '1' and FacilityID=0 ", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Returns the Areatype by TopicId 
        /// </summary>
        public int GetTopicAreatypeByTopicId(int TopicId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top 1 cl.areaid from topic t " +
                    " join categoryarealink cl on (t.categoryid=cl.categoryid or (t.categoryid=0 and cl.categoryid=2)) and cl.areaid <> 2 " +
                    " where topicid=@TopicId ";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        public DataSet GetNewPublishedTopics()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select t.topicid,t.releasedate,t.topicname,t.course_number from topic t join categoryarealink cl " +
                  "on t.categoryid=cl.categoryid where cl.areaid in (2,-1,-2,-3) and CONVERT(date,t.releasedate,101)=CONVERT(date, dateadd(day,-1,getdate()),101)";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);

                return ds;
            }
        }
        /// <summary>
        /// Retrieves all Topics (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTopics(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "TopicID, " +
                    "TopicName+' , '+ Course_Number  As TopicName," +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                    "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                    "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind ," +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic where Obsolete = '0' and FacilityID in (0,-1,-2,-3) ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        public List<TopicInfo> GetUnAssignedTopicsByResID(int ResourceID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic where " +
                                    " topicid not in (select topicid " +
                                    " from TopicResourceLink where resourceid =@ResourceID)";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@ResourceID", SqlDbType.Int).Value = ResourceID;

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public List<TopicInfo> GetTopicsByMicrositeDomain(int DomainID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct t.* " +
                    "from topic t right outer join CategoryLink cl on t.topicid = cl.topicid " +
                    "join Categories c on c.id = cl.categoryid " +
                    "right outer join CategoryMicrositeLink ml on ml.categoryid = c.id " +
                    "where ml.msid = @msid and t.active_ind='1' and t.obsolete='0'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = DomainID;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Topics (not obsolete)
        /// </summary>
        public List<TopicInfo> GetRetailUnlimCourseByCategoryID(int CategoryId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select  t.TopicID, " +
                    "t.topicname as TopicName, " +
                    "t.HTML,  " +
                    "t.CorLecture,  " +
                    "t.Fla_CEType,  " +
                    "t.Fla_IDNo,  " +
                    "t.ReleaseDate, " +
                    "t.ExpireDate, " +
                    "t.LastUpdate,  " +
                    "t.FolderName,  " +
                    "t.NoSurvey,  " +
                    "t.SurveyID,  " +
                    "t.Compliance, " +
                    "t.MetaKW,  " +
                    "t.MetaDesc,  " +
                    //"t.hours as ContactHours , " +
                    "t.hours , " +
                    "t.MediaType,  " +
                    "t.Objectives,  " +
                    "t.Content,  " +
                    "isnull(t.textbook,0) as textbook, " +
                    "t.CertID,  " +
                    "t.Method,  " +
                    "t.GrantBy,  " +
                    "t.DOCXFile,  " +
                    "t.DOCXHoFile,  " +
                    "t.Obsolete,  " +
                    "t.FacilityID,  " +
                    //"t.course_number as CourseNumber, " +
                    "t.course_number, " +
                    "t.Minutes,  " +
                    "isnull(t.audio_ind,0)as audio_ind,  " +
                    "isnull(t.apn_ind,0) as apn_ind, " +
                    "isnull(t.icn_ind,0) as icn_ind, " +
                    "isnull(t.jcaho_ind,0) as jcaho_ind, " +
                    "isnull(t.magnet_ind,0) as magnet_ind,  " +
                    "t.Active_Ind,  " +
                    "isnull(t.video_ind,0) as video_ind,  " +
                    "isnull(t.online_ind,0) As online_ind, " +
                    "isnull(t.ebp_ind,0) as ebp_ind, " +
                    "t.Ccm_Ind,  " +
                    "t.Avail_Ind,  " +
                    "t.Cert_Cerp,  " +
                    "t.Ref_Html,  " +
                    "t.AlterID,  " +
                    "t.Pass_Score,  " +
                    "t.subtitle as SubTitle , " +
                    "t.Topic_Type,  " +
                    "t.Img_Name,  " +
                    "t.Img_Credit, " +
                    "t.Img_Caption,  " +
                    "t.Accreditation,  " +
                    "t.Views,  " +
                    "t.PrimaryViews,  " +
                    "t.PageReads,  " +
                    "t.Featured,  " +
                    "t.rev,  " +
                    "t.urlmark,  " +
                    "t.offline_cost,  " +
                    "t.online_cost,  " +
                    "t.notest,  " +
                    "t.uce_Ind, " +
                    "t.shortname,  " +
                    "t.categoryid, " +
                    "t.hold_ind,  " +
                    "t.rating_total, " +
                    "t.rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "t.rating_num_comment, " +
                    "t.topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                "from topic t " +
                    "inner join Categorylink cl on t.topicid=cl.topicid  " +
                    "inner join categories c on c.id=cl.categoryid  " +
                    "join categoryarealink cal on c.id=cal.categoryid and cal.areaid=2  " +
                "where t.avail_ind=1 and t.obsolete='0' and t.facilityid=0  " +
                    "and c.showinlist='1' and (t.uce_ind='1' or t.hours<=1.5)  " +
                    "and c.id = @CategoryId and t.online_ind=1 " +
                    "  order by c.title,t.topicname ";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = CategoryId;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Topics (not obsolete)
        /// </summary>
        public List<TopicInfo> GetRetailCourseByCategoryID(int CategoryId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select  t.TopicID, " +
                    "t.topicname as TopicName, " +
                    "t.HTML,  " +
                    "t.CorLecture,  " +
                    "t.Fla_CEType,  " +
                    "t.Fla_IDNo,  " +
                    "t.ReleaseDate, " +
                    "t.ExpireDate, " +
                    "t.LastUpdate,  " +
                    "t.FolderName,  " +
                    "t.NoSurvey,  " +
                    //"t.SurveyID,  " +
                    "case when isnull(d.topicid,0)>0 then 1 else 0 end as SurveyID, " + //check this one(free_ind)
                    "t.Compliance, " +
                    "t.MetaKW,  " +
                    "t.MetaDesc,  " +
                    //"t.hours as ContactHours , " +
                    "t.hours , " +
                    "t.MediaType,  " +
                    "t.Objectives,  " +
                    "t.Content,  " +
                    "isnull(t.textbook,0) as textbook, " +
                    "t.CertID,  " +
                    "t.Method,  " +
                    "t.GrantBy,  " +
                    "t.DOCXFile,  " +
                    "t.DOCXHoFile,  " +
                    "t.Obsolete,  " +
                    "t.FacilityID,  " +
                    //"t.course_number as CourseNumber, " +
                    "t.course_number, " +
                    "t.Minutes,  " +
                    "isnull(t.audio_ind,0)as audio_ind,  " +
                    "isnull(t.apn_ind,0) as apn_ind, " +
                    "isnull(t.icn_ind,0) as icn_ind, " +
                    "isnull(t.jcaho_ind,0) as jcaho_ind, " +
                    "isnull(t.magnet_ind,0) as magnet_ind,  " +
                    "t.Active_Ind,  " +
                    "isnull(t.video_ind,0) as video_ind,  " +
                    "isnull(t.online_ind,0) As online_ind, " +
                    "isnull(t.ebp_ind,0) as ebp_ind, " +
                    "t.Ccm_Ind,  " +
                    "t.Avail_Ind,  " +
                    "t.Cert_Cerp,  " +
                    "t.Ref_Html,  " +
                    "t.AlterID,  " +
                    "t.Pass_Score,  " +
                    "t.subtitle as SubTitle , " +
                    "t.Topic_Type,  " +
                    "t.Img_Name,  " +
                    "t.Img_Credit, " +
                    "t.Img_Caption,  " +
                    "t.Accreditation,  " +
                    "t.Views,  " +
                    "t.PrimaryViews,  " +
                    "t.PageReads,  " +
                    "t.Featured,  " +
                    "t.rev,  " +
                    "t.urlmark,  " +
                    "t.offline_cost,  " +
                    "t.online_cost,  " +
                    "t.notest,  " +
                    "t.uce_Ind, " +
                    "t.shortname,  " +
                    "t.categoryid, " +
                    "t.hold_ind,  " +
                    "t.rating_total, " +
                    "t.rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "t.rating_num_comment, " +
                    "t.topic_hourlong, " +
                    "t.topic_hourshort,  " +
                    "t.webinar_start,  " +
                    "t.webinar_end  " +
                "from topic t " +
                    "inner join Categorylink cl on t.topicid=cl.topicid  " +
                    "inner join categories c on c.id=cl.categoryid  " +
                    "join categoryarealink cal on c.id=cal.categoryid and cal.areaid=2  " +
                    "left join (select topicid from discount where startdate <getdate() and enddate >getdate() and discount_type='S' and section_ind=1) d on t.topicid=d.topicid   " +
                "where t.avail_ind=1 and t.obsolete='0' and t.facilityid=0  " +
                    "and c.showinlist='1' and c.id = @CategoryId  " +
                    "  order by c.title,t.topicname ";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = CategoryId;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Featured Topics (not obsolete)
        /// </summary>
        public List<TopicInfo> GetFeaturedTopics(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select topic.* from Topic join categorylink on topic.topicid=categorylink.topicid" +
                    " join categories on categories.id=categorylink.categoryid " +
                "where categories.title='*Featured Courses' and Obsolete = '0' and Topic.Active_Ind = '1' ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }




        /// <summary>
        /// Retrieves all Audio Topics (not obsolete)
        /// </summary>
        public List<TopicInfo> GetAudioTopics(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic  where audio_ind=1 and active_ind=1 and facilityid= 0 and topicid in (select topicid from CategoryAreaLink ca join CategoryLink cl on ca.categoryid=cl.categoryid where areaid=2) ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves all Audio Topics by Categoryid (not obsolete)
        /// </summary>
        public List<TopicInfo> GetAudioTopicsByCategoryId(int CategoryId, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic join categorylink on topic.topicid=categorylink.topicid " +
                    " where categorylink.categoryid= " + CategoryId + " and mediatype='audio' and Obsolete = '0' and Topic.Active_Ind = '1'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all Audio Topics by Categoryid (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTopicsByCategoryIdAndMediaType(int CategoryId, string MediaType, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic join categorylink on topic.topicid=categorylink.topicid " +
                    " where categorylink.categoryid= " + CategoryId + " and mediatype= '" + MediaType + " ' and Obsolete = '0' and Topic.Active_Ind = '1'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }




        /// <summary>
        /// Retrieves all Featured Topics by FacilityId(not obsolete)
        /// </summary>
        public List<TopicInfo> GetFeaturedTopicsByFacilityId(int facilityid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct topic.* from topic inner join categorylink on topic.topicid = categorylink.topicid " +
                    " join categories on categories.id=categorylink.categoryid join categoryarealink on categoryarealink.categoryid=categories.id " +
                    " where categories.title='*Featured Courses' and Obsolete = '0' and Topic.Active_Ind = '1' and categoryarealink.areaid=@FacilityId  ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = facilityid;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all Featured Topics by tabname(not obsolete)
        /// </summary>
        public List<TopicInfo> GetFeaturedTopicsByTabname(string Tabname, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct topic.* from topic inner join categorylink on topic.topicid = categorylink.topicid " +
                    " join categories on categories.id=categorylink.categoryid join categoryarealink on categoryarealink.categoryid=categories.id " +
                    " join areatype on categoryarealink.areaid=areatype.areaid " +
                    " where categories.title='*Featured Courses' and Obsolete = '0' and Topic.Active_Ind = '1' and areatype.tabname=@Tabname  ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@Tabname", SqlDbType.VarChar).Value = Tabname;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves all Featured Topics by areaname (not obsolete)
        /// </summary>
        public List<TopicInfo> GetFeaturedTopicsByAreaname(string Areaname, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct topic.* from topic inner join categorylink on topic.topicid = categorylink.topicid " +
                    " join categories on categories.id=categorylink.categoryid join categoryarealink on categoryarealink.categoryid=categories.id " +
                    " join areatype on categoryarealink.areaid=areatype.areaid " +
                    " where categories.title='*Featured Courses' and Obsolete = '0' and Topic.Active_Ind = '1' and areatype.areaname=@Areaname  ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@Areaname", SqlDbType.VarChar).Value = Areaname;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Chapters (Bhaskar N)
        /// </summary>
        public List<TopicInfo> GetAllChaptersByTopicId(int TopicId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "cast(TopicCollection.chapternumber as nvarchar) + ' - ' + Topic.TopicName As TopicName," +
                    "Topic.HTML, " +
                    "Topic.CorLecture, " +
                    "Topic.Fla_CEType, " +
                    "Topic.Fla_IDNo, " +
                    "Topic.ReleaseDate, " +
                    "Topic.ExpireDate, " +
                    "Topic.LastUpdate, " +
                    "Topic.FolderName, " +
                    "Topic.NoSurvey, " +
                    "Topic.SurveyID, " +
                    "Topic.Compliance, " +
                    "Topic.MetaKW, " +
                    "Topic.MetaDesc, " +
                    "Topic.Hours, " +
                    "Topic.MediaType, " +
                    "Topic.Objectives, " +
                    "Topic.Content, " +
                    "Topic.Textbook, " +
                    "Topic.CertID, " +
                    "Topic.Method, " +
                    "Topic.GrantBy, " +
                    "Topic.DOCXFile, " +
                    "Topic.DOCXHoFile, " +
                    "Topic.Obsolete, " +
                    "Topic.FacilityID, " +
                    "Topic.Course_Number, " +
                    //"Topic.CeRef, " +
                    //"Topic.Release_Date, " +
                    "Topic.Minutes, " +
                    "Topic.Audio_Ind, " +
                    "Topic.Apn_Ind, " +
                    "Topic.Icn_Ind, " +
                    "Topic.Jcaho_Ind, " +
                    "Topic.Magnet_Ind, " +
                    "Topic.Active_Ind, " +
                    "Topic.Video_Ind, " +
                    "Topic.Online_Ind, " +
                    "Topic.Ebp_Ind, " +
                    "Topic.Ccm_Ind, " +
                    "Topic.Avail_Ind, " +
                    "Topic.Cert_Cerp, " +
                    "Topic.Ref_Html, " +
                    "Topic.AlterID, " +
                    "Topic.Pass_Score, " +
                    "Topic.Subtitle, " +
                    //"Topic.Cost, " +
                    "Topic.Topic_Type, " +
                    "Topic.Img_Name, " +
                    "Topic.Img_Credit, " +
                    "Topic.Img_Caption, " +
                    "Topic.Accreditation, " +
                    "Topic.Views, " +
                    "Topic.PrimaryViews, " +
                    "Topic.PageReads, " +
                   "Topic.Featured, " +
                    "Topic.rev, " +
                    "Topic.urlmark, " +
                    "Topic.offline_cost, " +
                    "Topic.online_cost, " +
                    "Topic.notest, " +
                    "Topic.uce_Ind, " +
                    "Topic.shortname, " +
                    "Topic.categoryid, " +
                    "Topic.hold_ind, " +
                    "Topic.rating_total, " +
                    "Topic.rating_avg,  " +
                    "Topic.prepaid_ind, " +
                    "Topic.topic_summary, " +
                    "Topic.cme_ind, " +
                    "Topic.cme_sponsor_ind, " +
                    "Topic.cme_sponsor_name, " +
                    "Topic.rating_num_comment, " +
                    "Topic.topic_hourlong, " +
                    "Topic.topic_hourshort,   " +
                    "Topic.webinar_start,    " +
                    "Topic.webinar_end    " +
                    "from TopicCollection " +
                    "join Topic on TopicCollection.chapterid = Topic.TopicId " +
                    "where Topic.Active_Ind = '1' and TopicCollection.TopicId= @TopicId  " +
                    "order by TopicCollection.chapternumber ";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves all Textbook Topics (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTextbookTopics(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic  where Textbook='1' and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=0  ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Non-Textbook Topics (not obsolete)
        /// </summary>
        public List<TopicInfo> GetNonTextbookTopics(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic where Textbook='0'  and Obsolete = '0' and Topic.Active_Ind = '1' ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all Non-Textbook Topics By Facility Id(not obsolete by Facilityid)
        /// </summary>
        public List<TopicInfo> GetNonTextbookTopicsByFacilityId(int facilityid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic where Textbook='0'  and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID= @FacilityID  ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = facilityid;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }




        /// <summary>
        /// Retrieves all Anthology Topics By Topic Id(not obsolete by Facilityid)
        /// </summary>
        public List<TopicInfo> GetAnthologyTopicsByTopicId(int TopicID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                  "TopicName+' , '+ Course_Number  As TopicName," +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                    "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                     "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "Topic.topic_hourlong, " +
                    "Topic.topic_hourshort,   " +
                    "Topic.webinar_start,   " +
                    "Topic.webinar_end   " +
                    "from Topic where topicid in (select top 500 chapterid from topiccollection where topiccollection.TopicID=@TopicID order by chapternumber desc ) and Textbook='0'  and Obsolete = '0' and Topic.Active_Ind = '1'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves all Obsolete Topics
        /// </summary>
        public List<TopicInfo> GetObsoleteTopics(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic where Obsolete = '1' and FacilityID=0  ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves all Obsolete Topics
        /// </summary>
        public List<TopicInfo> GetFacilityTopics()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic where Obsolete = '0' and Active_Ind='1' and FacilityID>0  ";

                // add on ORDER BY if provided

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves all Obsolete Textbook Topics
        /// </summary>
        public List<TopicInfo> GetObsoleteTextbookTopics(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic  where Textbook='1' and Obsolete = '1' and FacilityID=0  ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves all Topics by type
        /// </summary>
        public List<TopicInfo> GetTopicsByType(string CourseNumber, string Title, string category, int Areaid, string active, string cSortExpression)
        {
            if (!String.IsNullOrEmpty(CourseNumber))
            {
                if (CourseNumber.Contains("'"))
                {
                    CourseNumber = CourseNumber.Replace("'", "''").Trim();
                }
                else
                {
                    CourseNumber = CourseNumber.Trim();
                }
            }

            if (!String.IsNullOrEmpty(Title))
            {
                if (Title.Contains("'"))
                {
                    Title = Title.Replace("'", "''").Trim();
                }

                else
                {
                    Title = Title.Trim();
                }
            }

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select distinct " +
                         "Topic.TopicID, " +
                         "TopicName, " +
                         "HTML, " +
                         "CorLecture, " +
                         "Fla_CEType, " +
                         "Fla_IDNo, " +
                         "ReleaseDate, " +
                         "ExpireDate, " +
                         "convert(DATETIME,LastUpdate)," +
                         "LastUpdate, " +
                         "FolderName, " +
                         "NoSurvey, " +
                         "SurveyID, " +
                         "Compliance, " +
                         "Topic.MetaKW, " +
                         "Topic.MetaDesc, " +
                         "Hours, " +
                         "MediaType, " +
                         "Objectives, " +
                         "Content, " +
                         "Topic.Textbook, " +
                         "CertID, " +
                         "Method, " +
                         "GrantBy, " +
                         "DOCXFile, " +
                         "DOCXHoFile, " +
                         "Obsolete, " +
                         "Topic.FacilityID, " +
                         "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                         "Minutes, " +
                         "Audio_Ind, " +
                         "Apn_Ind, " +
                         "Icn_Ind, " +
                         "Jcaho_Ind, " +
                         "Magnet_Ind, " +
                         "Active_Ind, " +
                         "Video_Ind, " +
                         "Online_Ind, " +
                         "Ebp_Ind, " +
                         "Ccm_Ind, " +
                         "Avail_Ind, " +
                         "Cert_Cerp, " +
                         "Ref_Html, " +
                         "Topic.AlterID, " +
                         "Pass_Score, " +
                         "Subtitle, " +
                    //"Cost, " +
                         "Topic_Type, " +
                         "Img_Name, " +
                         "Img_Credit, " +
                         "Img_Caption, " +
                         "Accreditation, " +
                         "Views, " +
                         "PrimaryViews, " +
                         "PageReads, " +
                         "Featured, " +
                         "rev, " +
                         "urlmark, " +
                         "offline_cost, " +
                         "online_cost, " +
                         "notest, " +
                         "uce_Ind, " +
                         "shortname, " +
                         "Topic.categoryid, " +
                         "hold_ind, " +
                         "rating_total, " +
                         "rating_avg,  " +
                         "prepaid_ind, " +
                         "topic_summary, " +
                         "cme_ind, " +
                         "cme_sponsor_ind, " +
                         "cme_sponsor_name, " +
                         "rating_num_comment, " +
                         "topic_hourlong, " +
                         "topic_hourshort,    " +
                         "webinar_start, " +
                         "webinar_end " +
                         "from Topic ";

                if (Areaid == -300)
                {
                    if (category != "1000001")
                    {
                        cSQLCommand = cSQLCommand + "join categorylink on topic.topicid=categorylink.topicid where categorylink.categoryid = ' " + category + "'";
                        cSQLCommand = cSQLCommand + " and topicname like '%" + Title + "%' and course_number like '%" + CourseNumber + "%'";
                    }

                    else
                    {
                        cSQLCommand = cSQLCommand + " where topicname like '%" + Title + "%' and course_number like '%" + CourseNumber + "%'";
                    }

                }
                else if (Areaid == -100)
                {

                    cSQLCommand = cSQLCommand + " where topicname like '%" + Title + "%' and course_number like '%" + CourseNumber + "%' and facilityid>0 ";


                }
                else
                {
                    cSQLCommand = cSQLCommand + "inner join categorylink on topic.topicid=categorylink.topicid inner join categories on categories.id=categorylink.categoryid " +
                     " join categoryarealink on categoryarealink.categoryid=categories.id " +
                    "where topicname like '%" + Title + "%' and course_number like '%" + CourseNumber + "%'";

                    if (Areaid != -200)
                    {
                        cSQLCommand = cSQLCommand + " and categoryarealink.areaid = " + Areaid;
                    }

                    if (category == "1000001")
                    {
                        cSQLCommand = cSQLCommand + "";
                    }

                    else
                    {
                        cSQLCommand = cSQLCommand + " and categorylink.categoryid= '" + category + "'";
                    }
                }
                if (active == "1")
                {
                    //cSQLCommand = cSQLCommand + "and active_ind='1'";
                    cSQLCommand = cSQLCommand + "and avail_Ind='1'";
                }
                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    if (cSortExpression == "LastUpdate")
                    {
                        cSQLCommand = cSQLCommand +
                        " order by convert(DATETIME,LastUpdate)";
                    }
                    else if (cSortExpression == "LastUpdate DESC")
                    {
                        cSQLCommand = cSQLCommand +
                        " order by convert(DATETIME,LastUpdate) DESC";
                    }
                    else
                    {
                        cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                    }
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves all Obsolete Non-Textbook Topics
        /// </summary>
        public List<TopicInfo> GetObsoleteNonTextbookTopics(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic where Textbook='0' and Obsolete = '1' and FacilityID=0  ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Returns a collection with all Compliance Topics (compliance = true), except obsolete ones
        /// NOTE: Because this is used by facility admins to customize compliance topics, we will
        /// eliminate from the list any that are not assigned to a category, since they are not
        /// available to the public
        /// </summary>
        public List<TopicInfo> GetComplianceTopics(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * from Topic where Compliance='1' and Obsolete = '0' and Topic.Active_Ind = '1' and FacilityID=0  " +
                    "and TopicID in (select TopicID from CategoryLink) ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves the Topic with the specified ID
        /// </summary>
        public TopicInfo GetTopicByID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from Topic where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves the Topic with the specified ID
        /// Bsk
        /// </summary>
        public TopicInfo GetTopicByCourseNumber(string Course_Number)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from Topic where Course_Number=@Course_Number", cn);
                cmd.Parameters.Add("@Course_Number", SqlDbType.VarChar).Value = Course_Number;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves the Topic with the specified ID
        /// Bsk
        /// </summary>
        public TopicInfo GetTopicByCourseNumberAndDomainId(string Course_Number, int DomainId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from Topic where Course_Number=@Course_Number and facilityid=@DomainId and active_ind='1' and obsolete='0'", cn);
                cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
                cmd.Parameters.Add("@Course_Number", SqlDbType.VarChar).Value = Course_Number;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Retrieves the Topic with the specified ID
        /// Bsk
        /// </summary>
        public TopicInfo GetTopicByUserName(string UserName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select " +
                        "Topic.TopicID, " +
                        "TopicName, " +
                        "HTML, " +
                        "CorLecture, " +
                        "Fla_CEType, " +
                        "Fla_IDNo, " +
                        "ReleaseDate, " +
                        "ExpireDate, " +
                        "LastUpdate, " +
                        "FolderName, " +
                        "NoSurvey, " +
                        "SurveyID, " +
                        "Compliance, " +
                        "MetaKW, " +
                        "MetaDesc, " +
                        "Hours, " +
                        "MediaType, " +
                        "Objectives, " +
                        "Content, " +
                        "Textbook, " +
                        "CertID, " +
                        "Method, " +
                        "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "Topic.FacilityID, " +
                    "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                    "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "topic_hourlong, " +
                    "topic_hourshort,    " +
                    "webinar_start,       " +
                    "webinar_end    " +
                    "from Topic " +
                    "join discount on Topic.topicid =discount.topicid " +
                    " where discount.username=@UserName and virtualurl_ind = 1", cn);

                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = UserName.ToLower();
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a Topic
        /// </summary>
        public bool DeleteTopic(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Topic where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new Topic
        /// </summary>
        public int InsertTopic(TopicInfo Topic)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Topic " +
                "(CorLecture, " +
                "Fla_CEType, " +
                "Fla_IDNo, " +
                "FolderName, " +
                "HTML, " +
                "ReleaseDate, " +
                "ExpireDate, " +
                "LastUpdate, " +
                "NoSurvey, " +
                "TopicName, " +
                "SurveyID, " +
                "Compliance, " +
                "MetaKW, " +
                "MetaDesc, " +
                "Hours, " +
                "MediaType, " +
                "Objectives, " +
                "Content, " +
                "Textbook, " +
                "CertID, " +
                "Method, " +
                "GrantBy, " +
                "DOCXFile, " +
                "DOCXHoFile, " +
                "Obsolete, " +
                "FacilityID, " +
                "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                "Minutes, " +
                "Audio_Ind, " +
                "Apn_Ind, " +
                "Icn_Ind, " +
                "Jcaho_Ind, " +
                "Magnet_Ind, " +
                "Active_Ind, " +
                "Video_Ind, " +
                "Online_Ind, " +
                "Ebp_Ind, " +
                "Ccm_Ind, " +
                "Avail_Ind, " +
                "Cert_Cerp, " +
                "Ref_Html, " +
                "AlterID, " +
                "Pass_Score, " +
                "Subtitle, " +
                    //"Cost, " +
                "Topic_Type, " +
                "Img_Name, " +
                "Img_Credit, " +
                "Img_Caption, " +
                "Accreditation, " +
                "Views, " +
                "PrimaryViews, " +
                "PageReads, " +
                "Featured, " +
                "rev, " +
                "urlmark, " +
                "offline_cost, " +
                "online_cost, " +
                "notest, " +
                "uce_Ind, " +
                "shortname, " +
                "categoryid, " +
                "topic_summary, "+
                "webinar_start, " +
                "webinar_end, " +
                "hold_ind )" +
                "VALUES (" +
                "@CorLecture, " +
                "@Fla_CEType, " +
                "@Fla_IDNo, " +
                "@FolderName, " +
                "@HTML, " +
                "@ReleaseDate, " +
                "@ExpireDate, " +
                "@LastUpdate, " +
                "@NoSurvey, " +
                "@TopicName, " +
                "@SurveyID, " +
                "@Compliance, " +
                "@MetaKW, " +
                "@MetaDesc, " +
                "@Hours, " +
                "@MediaType, " +
                "@Objectives, " +
                "@Content, " +
                "@Textbook, " +
                "@CertID, " +
                "@Method, " +
                "@GrantBy, " +
                "@DOCXFile, " +
                "@DOCXHoFile, " +
                "@Obsolete, " +
                "@FacilityID, " +
                "@Course_Number, " +
                    //"@CeRef, " +
                    //"@Release_Date, " +
                "@Minutes, " +
                "@Audio_Ind, " +
                "@Apn_Ind, " +
                "@Icn_Ind, " +
                "@Jcaho_Ind, " +
                "@Magnet_Ind, " +
                "@Active_Ind, " +
                "@Video_Ind, " +
                "@Online_Ind, " +
                "@Ebp_Ind, " +
                "@Ccm_Ind, " +
                "@Avail_Ind, " +
                "@Cert_Cerp, " +
                "@Ref_Html, " +
                "@AlterID, " +
                "@Pass_Score, " +
                "@Subtitle, " +
                    //"@Cost, " +
                "@Topic_Type, " +
                "@Img_Name, " +
                "@Img_Credit, " +
                "@Img_Caption, " +
                "@Accreditation, " +
                "@Views, " +
                "@PrimaryViews, " +
                "@PageReads, " +
                "@Featured, " +
                "@rev, " +
                "@urlmark, " +
                "@offline_cost, " +
                "@online_cost, " +
                "@notest, " +
                "@uce_Ind, " +
                "@shortname, " +
                "@categoryid, " +
                "@topic_summary, " +
                "@webinar_start, " +
                "@webinar_end, " +
                "@hold_ind  ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@notest", SqlDbType.Bit).Value = Topic.notest;
                cmd.Parameters.Add("@uce_Ind", SqlDbType.Bit).Value = Topic.uce_Ind;
                cmd.Parameters.Add("@rev", SqlDbType.VarChar).Value = Topic.rev;
                cmd.Parameters.Add("@urlmark", SqlDbType.VarChar).Value = Topic.urlmark;
                cmd.Parameters.Add("@offline_cost", SqlDbType.Decimal).Value = Topic.offline_Cost;
                cmd.Parameters.Add("@online_cost", SqlDbType.Decimal).Value = Topic.online_Cost;
                cmd.Parameters.Add("@CorLecture", SqlDbType.Bit).Value = Topic.CorLecture;
                cmd.Parameters.Add("@Fla_CEType", SqlDbType.Char).Value = Topic.Fla_CEType;
                cmd.Parameters.Add("@Fla_IDNo", SqlDbType.VarChar).Value = Topic.Fla_IDNo;
                cmd.Parameters.Add("@FolderName", SqlDbType.VarChar).Value = Topic.FolderName;
                cmd.Parameters.Add("@HTML", SqlDbType.VarChar).Value = Topic.HTML;
                cmd.Parameters.Add("@ReleaseDate", SqlDbType.DateTime).Value =
                    (Topic.ReleaseDate == null ? (Object)DBNull.Value : (Object)Topic.ReleaseDate);
                cmd.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value =
                    (Topic.ExpireDate == null ? (Object)DBNull.Value : (Object)Topic.ExpireDate);
                cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = Topic.LastUpdate;
                //cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = System.DateTime.Now.ToShortDateString();            
                cmd.Parameters.Add("@NoSurvey", SqlDbType.Bit).Value = Topic.NoSurvey;
                cmd.Parameters.Add("@TopicName", SqlDbType.VarChar).Value = Topic.TopicName;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Topic.SurveyID == 0)
                {
                    cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = Topic.SurveyID;
                }

                cmd.Parameters.Add("@Compliance", SqlDbType.Bit).Value = Topic.Compliance;
                cmd.Parameters.Add("@MetaKW", SqlDbType.VarChar).Value = Topic.MetaKW;
                cmd.Parameters.Add("@MetaDesc", SqlDbType.VarChar).Value = Topic.MetaDesc;
                cmd.Parameters.Add("@Hours", SqlDbType.Decimal).Value = Topic.Hours;
                cmd.Parameters.Add("@MediaType", SqlDbType.VarChar).Value = Topic.MediaType;
                cmd.Parameters.Add("@Objectives", SqlDbType.VarChar).Value = Topic.Objectives;
                cmd.Parameters.Add("@Content", SqlDbType.VarChar).Value = Topic.Content;
                cmd.Parameters.Add("@Textbook", SqlDbType.Bit).Value = Topic.Textbook;
                cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = Topic.CertID;
                cmd.Parameters.Add("@Method", SqlDbType.VarChar).Value = Topic.Method;
                cmd.Parameters.Add("@GrantBy", SqlDbType.VarChar).Value = Topic.GrantBy;
                cmd.Parameters.Add("@DOCXFile", SqlDbType.VarChar).Value = Topic.DOCXFile;
                cmd.Parameters.Add("@DOCXHoFile", SqlDbType.VarChar).Value = Topic.DOCXHoFile;
                cmd.Parameters.Add("@Obsolete", SqlDbType.Bit).Value = Topic.Obsolete;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = Topic.FacilityID;
                cmd.Parameters.Add("@topic_summary", SqlDbType.VarChar).Value = Topic.topic_summary;

                cmd.Parameters.Add("@Course_Number", SqlDbType.VarChar).Value = Topic.Course_Number;
                //cmd.Parameters.Add("@CeRef", SqlDbType.VarChar).Value = Topic.CeRef;
                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (Topic.Release_Date == System.DateTime.MinValue)
                //{
                //    cmd.Parameters.Add("@Release_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                //}
                //else
                //{
                //    cmd.Parameters.Add("@Release_Date", SqlDbType.DateTime).Value = Topic.Release_Date;
                //}
                cmd.Parameters.Add("@Minutes", SqlDbType.Int).Value = Topic.Minutes;

                cmd.Parameters.Add("@Audio_Ind", SqlDbType.Bit).Value = Topic.Audio_Ind;
                cmd.Parameters.Add("@Apn_Ind", SqlDbType.Bit).Value = Topic.Apn_Ind;
                cmd.Parameters.Add("@Icn_Ind", SqlDbType.Bit).Value = Topic.Icn_Ind;
                cmd.Parameters.Add("@Jcaho_Ind", SqlDbType.Bit).Value = Topic.Jcaho_Ind;
                cmd.Parameters.Add("@Magnet_Ind", SqlDbType.Bit).Value = Topic.Magnet_Ind;
                cmd.Parameters.Add("@Active_Ind", SqlDbType.Bit).Value = Topic.Active_Ind;
                cmd.Parameters.Add("@Video_Ind", SqlDbType.Bit).Value = Topic.Video_Ind;
                cmd.Parameters.Add("@Online_Ind", SqlDbType.Bit).Value = Topic.Online_Ind;
                cmd.Parameters.Add("@Ebp_Ind", SqlDbType.Bit).Value = Topic.Ebp_Ind;
                cmd.Parameters.Add("@Ccm_Ind", SqlDbType.Bit).Value = Topic.Ccm_Ind;
                cmd.Parameters.Add("@Avail_Ind", SqlDbType.Bit).Value = Topic.Avail_Ind;

                cmd.Parameters.Add("@Cert_Cerp", SqlDbType.VarChar).Value = Topic.Cert_Cerp;
                cmd.Parameters.Add("@Ref_Html", SqlDbType.Text).Value = Topic.Ref_Html;
                cmd.Parameters.Add("@AlterID", SqlDbType.Int).Value = Topic.AlterID;
                cmd.Parameters.Add("@Pass_Score", SqlDbType.Int).Value = Topic.Pass_Score;
                cmd.Parameters.Add("@Subtitle", SqlDbType.VarChar).Value = Topic.Subtitle;
                //cmd.Parameters.Add("@Cost", SqlDbType.Decimal).Value = Topic.Cost;
                //cmd.Parameters.Add("@Topic_Type", SqlDbType.VarChar).Value = Topic.Topic_Type;

                if (Topic.Topic_Type == null)
                {
                    cmd.Parameters.Add("@Topic_Type", SqlDbType.VarChar).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Topic_Type", SqlDbType.VarChar).Value = Topic.Topic_Type;
                }

                if (Topic.Img_Name == null | Topic.Img_Name == string.Empty)
                {
                    if (Topic.FacilityID == 0)
                    {
                        cmd.Parameters.Add("@Img_Name", SqlDbType.VarChar).Value = "CeIcon.jpg";
                    }
                    else if (Topic.FacilityID == -1)
                    {
                        cmd.Parameters.Add("@Img_Name", SqlDbType.VarChar).Value = "PRIcon.jpg";
                    }
                    else if (Topic.FacilityID == -2)
                    {
                        cmd.Parameters.Add("@Img_Name", SqlDbType.VarChar).Value = "PRTextIcon.jpg";
                    }
                    else if (Topic.FacilityID > 0)
                    {
                        cmd.Parameters.Add("@Img_Name", SqlDbType.VarChar).Value = "Facility.jpg";
                    }
                }
                else
                {
                    cmd.Parameters.Add("@Img_Name", SqlDbType.VarChar).Value = Topic.Img_Name;
                }
                cmd.Parameters.Add("@Img_Credit", SqlDbType.VarChar).Value = Topic.Img_Credit;
                cmd.Parameters.Add("@Img_Caption", SqlDbType.VarChar).Value = Topic.Img_Caption;
                cmd.Parameters.Add("@Accreditation", SqlDbType.VarChar).Value = Topic.Accreditation;
                cmd.Parameters.Add("@Views", SqlDbType.Int).Value = Topic.Views;
                cmd.Parameters.Add("@PrimaryViews", SqlDbType.Int).Value = Topic.PrimaryViews;
                cmd.Parameters.Add("@PageReads", SqlDbType.Int).Value = Topic.PageReads;
                cmd.Parameters.Add("@Featured", SqlDbType.Bit).Value = Topic.Featured;
                cmd.Parameters.Add("@shortname", SqlDbType.VarChar).Value = Topic.shortname;
                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = Topic.categoryid;
                cmd.Parameters.Add("@hold_ind", SqlDbType.Bit).Value = Topic.hold_ind;

                if (Topic.Webinar_start == null)
                {
                    cmd.Parameters.Add("@webinar_start", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@webinar_start", SqlDbType.DateTime).Value = Topic.Webinar_start;
                }


                if (Topic.Webinar_end == null)
                {
                    cmd.Parameters.Add("@webinar_end", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@webinar_end", SqlDbType.DateTime).Value = Topic.Webinar_end;
                }




                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                if (NewID > 0)
                {
                    using (SqlConnection cn1 = new SqlConnection(this.ConnectionString))
                    {
                        SqlCommand cmd1 = new SqlCommand("insert into TopicAudit(topicid)VALUES (@NewID)", cn1);
                        cmd1.Parameters.Add("@NewID", SqlDbType.Int).Value = NewID;
                        cn1.Open();
                        cmd1.ExecuteNonQuery();
                    }
                }

                return NewID;

            }
        }
        /// <summary>
        /// Bhaskar N
        /// Inserts a new Topic Using Existing Topic data
        /// </summary>
        public int InsertTopicFromExistingTopicData(int TopicID, string New_TopicName, string New_Course_Number)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert Topic " +
               "(TopicName, " +
               "CorLecture, " +
               "Fla_CEType, " +
               "Fla_IDNo, " +
               "FolderName, " +
               "HTML, " +
               "ReleaseDate, " +
               "ExpireDate, " +
               "LastUpdate, " +
               "NoSurvey, " +
               "SurveyID, " +
               "Compliance, " +
               "MetaKW, " +
               "MetaDesc, " +
               "Hours, " +
               "MediaType, " +
               "Objectives, " +
               "Content, " +
               "Textbook, " +
               "CertID, " +
               "Method, " +
               "GrantBy, " +
               "DOCXFile, " +
               "DOCXHoFile, " +
               "Obsolete, " +
               "Course_Number, " +
               "FacilityID, " +
               "Minutes, " +
               "Audio_Ind, " +
               "Apn_Ind, " +
               "Icn_Ind, " +
               "Jcaho_Ind, " +
               "Magnet_Ind, " +
               "Active_Ind, " +
               "Video_Ind, " +
               "Online_Ind, " +
               "Ebp_Ind, " +
               "Ccm_Ind, " +
               "Avail_Ind, " +
               "Cert_Cerp, " +
               "Ref_Html, " +
               "AlterID, " +
               "Pass_Score, " +
               "Subtitle, " +
               "Topic_Type, " +
               "Img_Name, " +
               "Img_Credit, " +
               "Img_Caption, " +
               "Accreditation, " +
               "Views, " +
               "PrimaryViews, " +
               "PageReads, " +
               "Featured, " +
               "rev, " +
               "urlmark, " +
               "offline_cost, " +
               "online_cost, " +
               "notest, " +
               "uce_Ind, " +
               "shortname, " +
               "categoryid, " +
               "webinar_start, " +
               "webinar_end, " +
               "topic_summary, " +
               "hold_ind )" +
           " Select " +
               "@New_TopicName, " +
               "CorLecture, " +
               "Fla_CEType, " +
               "Fla_IDNo, " +
               "FolderName, " +
               "HTML, " +
               "ReleaseDate, " +
                "ExpireDate, " +
               "@LastUpdate, " +
               "NoSurvey, " +
               "SurveyID, " +
               "Compliance, " +
               "MetaKW, " +
               "MetaDesc, " +
               "Hours, " +
               "MediaType, " +
               "Objectives, " +
               "Content, " +
               "Textbook, " +
               "CertID, " +
               "Method, " +
               "GrantBy, " +
               "DOCXFile, " +
               "DOCXHoFile, " +
               "Obsolete, " +
               "@New_Course_Number, " +
               "FacilityID, " +
               "Minutes, " +
               "Audio_Ind, " +
               "Apn_Ind, " +
               "Icn_Ind, " +
               "Jcaho_Ind, " +
               "Magnet_Ind, " +
               "Active_Ind, " +
               "Video_Ind, " +
               "Online_Ind, " +
               "Ebp_Ind, " +
               "Ccm_Ind, " +
               "Avail_Ind, " +
               "Cert_Cerp, " +
               "Ref_Html, " +
               "AlterID, " +
               "Pass_Score, " +
               "Subtitle, " +
               "Topic_Type, " +
               "Img_Name, " +
               "Img_Credit, " +
               "Img_Caption, " +
               "Accreditation, " +
               "Views, " +
               "PrimaryViews, " +
               "PageReads, " +
               "Featured, " +
               "rev, " +
               "urlmark, " +
               "offline_cost, " +
               "online_cost, " +
               "notest, " +
               "uce_Ind, " +
               "shortname, " +
               "categoryid, " +
               "webinar_start, " +
               "webinar_end, " +
               "topic_summary, " +
               " hold_ind from Topic where TopicID=@TopicID" +
               "  SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@New_TopicName", SqlDbType.VarChar).Value = New_TopicName;
                cmd.Parameters.Add("@New_Course_Number", SqlDbType.VarChar).Value = New_Course_Number;
                cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = DateTime.Now.ToShortDateString();


                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                cn.Close();
                if (NewID > 0)
                {
                    SqlCommand cmd1 = new SqlCommand();
                    cmd1.Connection = cn;
                    cmd1.CommandText = "sp_Topic_Copy";
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.Add("@Topic_Id_Existing", SqlDbType.Int).Value = TopicID;
                    cmd1.Parameters.Add("@Topic_Id_New", SqlDbType.Int).Value = NewID;
                    cn.Open();
                    cmd1.ExecuteNonQuery();

                }

                return NewID;
            }
        }


        /// <summary>
        /// Updates a Topic
        /// </summary>
        public bool UpdateTopic(TopicInfo Topic)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Topic set " +
                "CorLecture = @CorLecture, " +
                "Fla_CEType = @Fla_CEType, " +
                "Fla_IDNo = @Fla_IDNo, " +
                "FolderName = @FolderName, " +
                "HTML = @HTML, " +
                "ReleaseDate = @ReleaseDate, " +
                "ExpireDate = @ExpireDate, " +
                "LastUpdate = @LastUpdate, " +
                "NoSurvey = @NoSurvey, " +
                "TopicName = @TopicName, " +
                "SurveyID = @SurveyID, " +
                "Compliance = @Compliance, " +
                "MetaKW = @MetaKW, " +
                "MetaDesc = @MetaDesc, " +
                "Hours = @Hours, " +
                "MediaType = @MediaType, " +
                "Objectives = @Objectives, " +
                "Content = @Content, " +
                "Textbook = @Textbook, " +
                "CertID = @CertID, " +
                "Method = @Method, " +
                "GrantBy = @GrantBy, " +
                "DOCXFile = @DOCXFile, " +
                "DOCXHoFile = @DOCXHoFile, " +
                "Obsolete = @Obsolete, " +
                "FacilityID = @FacilityID, " +
                "Course_Number = @Course_Number, " +
                    //"CeRef = @CeRef, " +
                    //"Release_Date = @Release_Date, " +
                "Minutes = @Minutes, " +
                "Audio_Ind = @Audio_Ind, " +
                "Apn_Ind = @Apn_Ind, " +
                "Icn_Ind = @Icn_Ind, " +
                "Jcaho_Ind = @Jcaho_Ind, " +
                "Magnet_Ind = @Magnet_Ind, " +
                "Active_Ind = @Active_Ind, " +
                "Video_Ind = @Video_Ind, " +
                "Online_Ind = @Online_Ind, " +
                "Ebp_Ind = @Ebp_Ind, " +
                "Ccm_Ind = @Ccm_Ind, " +
                "Avail_Ind = @Avail_Ind, " +
                "Cert_Cerp = @Cert_Cerp, " +
                "Ref_Html = @Ref_Html, " +
                "AlterID = @AlterID, " +
                "Pass_Score = @Pass_Score, " +
                "Subtitle = @Subtitle, " +
                    //"Cost = @Cost, " +
                "Topic_Type = @Topic_Type, " +
                "Img_Name = @Img_Name, " +
                "Img_Credit = @Img_Credit, " +
                "Img_Caption = @Img_Caption, " +
                "Accreditation = @Accreditation, " +
                "Views = @Views, " +
                "PrimaryViews = @PrimaryViews, " +
                "PageReads = @PageReads, " +
                "Featured = @Featured ," +
                "rev = @rev ," +
                "online_cost = @online_cost ," +
                "urlmark = @urlmark ," +
                "offline_cost = @offline_cost ," +
                "notest = @notest ," +
                "uce_Ind = @uce_Ind, " +
                "shortname = @shortname, " +
                "categoryid = @categoryid, " +
                "prepaid_ind = @prepaid_ind, " +
                "topic_summary = @topic_summary, " +
                "cme_ind = @cme_ind, " +
                "cme_sponsor_ind = @cme_sponsor_ind, " +
                "cme_sponsor_name = @cme_sponsor_name, " +
                "hold_ind = @hold_ind, " +
                "webinar_start = @webinar_start, " +
                "webinar_end = @webinar_end " +
                "where TopicID = @TopicID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@notest", SqlDbType.Bit).Value = Topic.notest;
                cmd.Parameters.Add("@uce_Ind", SqlDbType.Bit).Value = Topic.uce_Ind;
                cmd.Parameters.Add("@rev", SqlDbType.VarChar).Value = Topic.rev;
                cmd.Parameters.Add("@urlmark", SqlDbType.VarChar).Value = Topic.urlmark;
                cmd.Parameters.Add("@offline_cost", SqlDbType.Decimal).Value = Topic.offline_Cost;
                cmd.Parameters.Add("@online_cost", SqlDbType.Decimal).Value = Topic.online_Cost;
                cmd.Parameters.Add("@CorLecture", SqlDbType.Bit).Value = Topic.CorLecture;
                cmd.Parameters.Add("@Fla_CEType", SqlDbType.Char).Value = Topic.Fla_CEType;
                cmd.Parameters.Add("@Fla_IDNo", SqlDbType.VarChar).Value = Topic.Fla_IDNo;
                cmd.Parameters.Add("@FolderName", SqlDbType.VarChar).Value = Topic.FolderName;
                cmd.Parameters.Add("@HTML", SqlDbType.VarChar).Value = Topic.HTML;
                cmd.Parameters.Add("@ReleaseDate", SqlDbType.DateTime).Value =
                    (Topic.ReleaseDate == null ? (Object)DBNull.Value : (Object)Topic.ReleaseDate);
                cmd.Parameters.Add("@ExpireDate", SqlDbType.DateTime).Value =
                    (Topic.ExpireDate == null ? (Object)DBNull.Value : (Object)Topic.ExpireDate);
                cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = Topic.LastUpdate;
                //cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = System.DateTime.Now.ToShortDateString();
                cmd.Parameters.Add("@NoSurvey", SqlDbType.Bit).Value = Topic.NoSurvey;
                cmd.Parameters.Add("@TopicName", SqlDbType.VarChar).Value = Topic.TopicName;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (Topic.SurveyID == 0)
                {
                    cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = Topic.SurveyID;
                }

                cmd.Parameters.Add("@Compliance", SqlDbType.Bit).Value = Topic.Compliance;
                cmd.Parameters.Add("@MetaKW", SqlDbType.VarChar).Value = Topic.MetaKW;
                cmd.Parameters.Add("@MetaDesc", SqlDbType.VarChar).Value = Topic.MetaDesc;
                cmd.Parameters.Add("@Hours", SqlDbType.Decimal).Value = Topic.Hours;
                cmd.Parameters.Add("@MediaType", SqlDbType.VarChar).Value = Topic.MediaType;
                cmd.Parameters.Add("@Objectives", SqlDbType.VarChar).Value = Topic.Objectives;
                cmd.Parameters.Add("@Content", SqlDbType.VarChar).Value = Topic.Content;
                cmd.Parameters.Add("@Textbook", SqlDbType.Bit).Value = Topic.Textbook;
                cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = Topic.CertID;
                cmd.Parameters.Add("@Method", SqlDbType.VarChar).Value = Topic.Method;
                cmd.Parameters.Add("@GrantBy", SqlDbType.VarChar).Value = Topic.GrantBy;
                cmd.Parameters.Add("@DOCXFile", SqlDbType.VarChar).Value = Topic.DOCXFile;
                cmd.Parameters.Add("@DOCXHoFile", SqlDbType.VarChar).Value = Topic.DOCXHoFile;
                cmd.Parameters.Add("@Obsolete", SqlDbType.Bit).Value = Topic.Obsolete;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = Topic.FacilityID;

                cmd.Parameters.Add("@Course_Number", SqlDbType.VarChar).Value = Topic.Course_Number;
                //cmd.Parameters.Add("@CeRef", SqlDbType.VarChar).Value = Topic.CeRef;
                //// pass null to database if value is zero (nothing selected by user)
                //// this allows the database to have a NULL foreign key and still enforce relational integrity
                //if (Topic.Release_Date == System.DateTime.MinValue)
                //{
                //    cmd.Parameters.Add("@Release_Date", SqlDbType.DateTime).Value = System.DBNull.Value;
                //}
                //else
                //{
                //    cmd.Parameters.Add("@Release_Date", SqlDbType.DateTime).Value = Topic.Release_Date;
                //}
                cmd.Parameters.Add("@Minutes", SqlDbType.Int).Value = Topic.Minutes;

                cmd.Parameters.Add("@Audio_Ind", SqlDbType.Bit).Value = Topic.Audio_Ind;
                cmd.Parameters.Add("@Apn_Ind", SqlDbType.Bit).Value = Topic.Apn_Ind;
                cmd.Parameters.Add("@Icn_Ind", SqlDbType.Bit).Value = Topic.Icn_Ind;
                cmd.Parameters.Add("@Jcaho_Ind", SqlDbType.Bit).Value = Topic.Jcaho_Ind;
                cmd.Parameters.Add("@Magnet_Ind", SqlDbType.Bit).Value = Topic.Magnet_Ind;
                cmd.Parameters.Add("@Active_Ind", SqlDbType.Bit).Value = Topic.Active_Ind;
                cmd.Parameters.Add("@Video_Ind", SqlDbType.Bit).Value = Topic.Video_Ind;
                cmd.Parameters.Add("@Online_Ind", SqlDbType.Bit).Value = Topic.Online_Ind;
                cmd.Parameters.Add("@Ebp_Ind", SqlDbType.Bit).Value = Topic.Ebp_Ind;
                cmd.Parameters.Add("@Ccm_Ind", SqlDbType.Bit).Value = Topic.Ccm_Ind;
                cmd.Parameters.Add("@Avail_Ind", SqlDbType.Bit).Value = Topic.Avail_Ind;

                cmd.Parameters.Add("@Cert_Cerp", SqlDbType.VarChar).Value = Topic.Cert_Cerp;
                cmd.Parameters.Add("@Ref_Html", SqlDbType.Text).Value = Topic.Ref_Html;
                cmd.Parameters.Add("@AlterID", SqlDbType.Int).Value = Topic.AlterID;
                cmd.Parameters.Add("@Pass_Score", SqlDbType.Int).Value = Topic.Pass_Score;
                cmd.Parameters.Add("@Subtitle", SqlDbType.VarChar).Value = Topic.Subtitle;
                //cmd.Parameters.Add("@Cost", SqlDbType.Decimal).Value = Topic.Cost;
                //cmd.Parameters.Add("@Topic_Type", SqlDbType.VarChar).Value = Topic.Topic_Type;
                if (Topic.Topic_Type == null)
                {
                    cmd.Parameters.Add("@Topic_Type", SqlDbType.VarChar).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Topic_Type", SqlDbType.VarChar).Value = Topic.Topic_Type;
                }
                cmd.Parameters.Add("@Img_Name", SqlDbType.VarChar).Value = Topic.Img_Name;
                cmd.Parameters.Add("@Img_Credit", SqlDbType.VarChar).Value = Topic.Img_Credit;
                cmd.Parameters.Add("@Img_Caption", SqlDbType.VarChar).Value = Topic.Img_Caption;
                cmd.Parameters.Add("@Accreditation", SqlDbType.VarChar).Value = Topic.Accreditation;
                cmd.Parameters.Add("@Views", SqlDbType.Int).Value = Topic.Views;
                cmd.Parameters.Add("@PrimaryViews", SqlDbType.Int).Value = Topic.PrimaryViews;
                cmd.Parameters.Add("@PageReads", SqlDbType.Int).Value = Topic.PageReads;
                cmd.Parameters.Add("@Featured", SqlDbType.Bit).Value = Topic.Featured;
                cmd.Parameters.Add("@shortname", SqlDbType.VarChar).Value = Topic.shortname;
                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = Topic.categoryid;
                cmd.Parameters.Add("@hold_ind", SqlDbType.Bit).Value = Topic.hold_ind;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = Topic.TopicID;
                cmd.Parameters.Add("@prepaid_ind", SqlDbType.Bit).Value = Topic.prepaid_ind;
                cmd.Parameters.Add("@topic_summary", SqlDbType.VarChar).Value = Topic.topic_summary;
                cmd.Parameters.Add("@cme_ind", SqlDbType.Bit).Value = Topic.CMEInd;
                cmd.Parameters.Add("@cme_sponsor_ind", SqlDbType.Bit).Value = Topic.CMESponsorInd;
                cmd.Parameters.Add("@cme_sponsor_name", SqlDbType.VarChar).Value = Topic.CMESponsorName;
                if (Topic.Webinar_start == null)
                {
                    cmd.Parameters.Add("@webinar_start", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@webinar_start", SqlDbType.DateTime).Value = Topic.Webinar_start;
                }


                if (Topic.Webinar_end == null)
                {
                    cmd.Parameters.Add("@webinar_end", SqlDbType.DateTime).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@webinar_end", SqlDbType.DateTime).Value = Topic.Webinar_end;
                }


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Updates a Topic Active Status
        /// </summary>
        public bool UpdateTopicActiveStatus(bool status, int topicid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Topic set  " +
                "Active_Ind = @Active_Ind " +
                "where TopicID = @TopicID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Active_Ind", SqlDbType.Bit).Value = status;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = topicid;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        /// <summary>
        /// Returns the total number of Topics for the specified CategoryID (not obsolete)
        /// </summary>
        public int GetTopicCountByCategoryID(int CategoryID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from Topic " +
                    "join categorylink on Topic.topicid = categorylink.topicid " +
                    "where categorylink.CategoryID = @CategoryID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' ", cn);
                //              cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all topics for the specified CategoryID (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTopicsByCategoryID(int CategoryID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                   "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "join categorylink on Topic.topicid = categorylink.topicid " +
                    "where categorylink.CategoryID = @CategoryID and Topic.Obsolete = '0' and Topic.Active_Ind = '1'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all topics for the specified CategoryID (not obsolete)
        /// </summary>
        public DataSet GetICETopicsByCategoryID(int CategoryID, string cSortExpression, bool Upcoming)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                   "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                     "webinar_start, " +
                      "webinar_end, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "topic_hourlong, " +
                    "topic_hourshort, " +
                    "is_start, " +
                    "convert(varchar(10),is_start,101) + '-' + convert(varchar(10),is_end,101) as seriesDate, " +
                    "case when is_start <= convert(varchar(10),getdate(),121) + ' 00:00:00' then '1' else '0' end started " +
                    "from Topic " +
                    "join categorylink on Topic.topicid = categorylink.topicid " +
                    "join icesection on Topic.topicid=icesection.topicid " +
                    "where categorylink.CategoryID = @CategoryID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' and is_end >= convert(datetime,convert(varchar(10),getdate(),121) + ' 00:00:00') ";
                if (Upcoming)
                    cSQLCommand += " and is_start > convert(varchar,getdate(),121)";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cn.Open();
                SqlDataAdapter dadp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("TopicList");
                dadp.Fill(ds);
                return ds;
            }
        }



        public List<TopicInfo> GettwoPearlsNewTopics()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select Top 2 " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "Topic.MetaKW, " +
                    "Topic.MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Topic.Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "Topic.FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "Topic.AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                   "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "where Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
                     " and Topic.facilityid in (-1,-2) order by convert(datetime,topic.lastupdate) desc ";


                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all topics for the specified CategoryID (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTopicsByCategoryWebsite(string Website, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "Topic.MetaKW, " +
                    "Topic.MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Topic.Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "Topic.FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "Topic.AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                   "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                     "prepaid_ind, " +
                     "topic_summary, " +
                     "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                     "rating_num_comment, " +
                     "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "join categorylink on Topic.topicid = categorylink.topicid " +
                    "join Categories on categories.id=categorylink.categoryid " +
                    "where categories.website = @website and Topic.Obsolete = '0' and Topic.Active_Ind = '1' and categories.showinlist = '1' " +
                     " and Topic.facilityid in (-1,-2)"; //Bsk

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@website", SqlDbType.VarChar).Value = Website;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all topics for the specified CategoryID (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTextBookTopicsByCategoryId(int CategoryID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                    "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "join categorylink on Topic.topicid = categorylink.topicid " +
                    "where categorylink.CategoryID = @CategoryID and Topic.Obsolete = '0' and Topic.Active_Ind = '1' and Topic.Textbook = '1' ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all topics for the specified CategoryID (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTopicsByFacilityId(int FacilityId, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                   "TopicID, " +
                   "TopicName+' , '+ Course_Number  As TopicName," +
                   "HTML, " +
                   "CorLecture, " +
                   "Fla_CEType, " +
                   "Fla_IDNo, " +
                   "ReleaseDate, " +
                   "ExpireDate, " +
                   "LastUpdate, " +
                   "FolderName, " +
                   "NoSurvey, " +
                   "SurveyID, " +
                   "Compliance, " +
                   "MetaKW, " +
                   "MetaDesc, " +
                   "Hours, " +
                   "MediaType, " +
                   "Objectives, " +
                   "Content, " +
                   "Textbook, " +
                   "CertID, " +
                   "Method, " +
                   "GrantBy, " +
                   "DOCXFile, " +
                   "DOCXHoFile, " +
                   "Obsolete, " +
                   "FacilityID, " +
                   "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                   "Minutes, " +
                   "Audio_Ind, " +
                   "Apn_Ind, " +
                   "Icn_Ind, " +
                   "Jcaho_Ind, " +
                   "Magnet_Ind, " +
                   "Active_Ind, " +
                   "Video_Ind, " +
                   "Online_Ind, " +
                   "Ebp_Ind, " +
                   "Ccm_Ind, " +
                   "Avail_Ind, " +
                   "Cert_Cerp, " +
                   "Ref_Html, " +
                   "AlterID, " +
                   "Pass_Score, " +
                   "Subtitle, " +
                    //"Cost, " +
                   "Topic_Type, " +
                   "Img_Name, " +
                   "Img_Credit, " +
                   "Img_Caption, " +
                   "Accreditation, " +
                   "Views, " +
                   "PrimaryViews, " +
                   "PageReads, " +
                   "Featured, " +
                   "rev, " +
                   "urlmark, " +
                   "offline_cost, " +
                   "online_cost, " +
                   "notest, " +
                   "uce_Ind, " +
                   "shortname, " +
                   "Topic.categoryid, " +
                   "hold_ind ," +
                   "rating_total, " +
                   "rating_avg,  " +
                   "prepaid_ind, " +
                   "topic_summary, " +
                   "cme_ind, " +
                   "cme_sponsor_ind, " +
                   "cme_sponsor_name, " +
                   "rating_num_comment, " +
                   "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                   "from Topic where Topic.Obsolete = '0' and Topic.avail_ind = '1' and Topic.facilityid=@FacilityId ";
                //string cSQLCommand = "select Topic.* from Topic " +
                //    "where Topic.Obsolete = '0' and Topic.avail_ind = '1' and Topic.facilityid=@FacilityId ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = FacilityId;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all topics for the specified CategoryID (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTopicsByFacIDLessthanEqualsZero(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select Topic.* from Topic " +
                    "where Topic.facilityid<=0 ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all topics for the specified CategoryID (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTopicsByCategoryNameForRetail(string CategoryName, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                    "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "join categorylink on Topic.topicid = categorylink.topicid " +
                    "where categorylink.CategoryID = (select top 1 id from categories where website=@CategoryName and showinlist='1' ) and Topic.Obsolete = '0' and Topic.Avail_Ind = '1'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by ebp_ind DESC, " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@CategoryName", SqlDbType.VarChar).Value = CategoryName;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all topics for the specified CategoryID (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTopicsByCategoryName(string CategoryName, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                    "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "join categorylink on Topic.topicid = categorylink.topicid " +
                    "where categorylink.CategoryID = (select top 1 id from categories where website=@CategoryName and showinlist='1' ) and Topic.Obsolete = '0' and Topic.Avail_Ind = '1'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@CategoryName", SqlDbType.VarChar).Value = CategoryName;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves all topics for the specified CategoryID (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTextAndNonTextTopicsByCategoryId(int CategoryID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                    "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                     "rating_num_comment, " +
                     "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "join categorylink on Topic.topicid = categorylink.topicid " +
                    "where categorylink.CategoryID = @CategoryID and Topic.Obsolete = '0' and Topic.Active_Ind = '1'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }




        /// <summary>
        /// Retrieves all topics for the specified search text (not obsolete)and Facility
        /// NOTE: Looks in topicname, subtitle, objectives, course_number and authorname
        /// </summary>
        public DataSet GetTopicsBySearchTextandFacility(string cSearchText, int facilityid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                // add space on each side of text (this limits the results to just those words and not fragments)
                if (String.IsNullOrEmpty(cSearchText))
                {
                    // no search text, so make up some that will return nothing
                    cSearchText = "XZXZXZXZXZXZXZXZXZXZ";
                }
                else
                {
                    cSearchText = cSearchText.Trim();
                }

                string cSQLCommand = "SELECT  Topic.*,areatype.areaname,[professiontopic].[profession] AS profession" +
                    " FROM Topic inner join areatype on Topic.facilityid=areatype.areaid" +
                    " INNER JOIN (select t.topicid,min( case when t.facilityid=30 then REPLACE(cat.title, 'Focused CE - ', '')  else "+
                    " case when a.areaid <0 then 'Nurses' else a.tabname end end) as profession" +
                    " from topic t join categoryarealink c" +
                    " on t.categoryid=c.categoryid" +
                    " join areatype a on a.areaid=c.areaid " +
                    " join Categories cat on t.categoryid=cat.id " +
                    " where c.areaid <>2" +
                    " group BY t.topicid)professiontopic ON [professiontopic].[topicid] = [Topic].[topicid] " +
                    " where (Topic.topicid in " +
                        "(select categorylink.topicid from facilityarealink join categoryarealink on facilityarealink.areaid=categoryarealink.areaid and facilityarealink.facilityid=@FacilityId " +
                        "join categorylink on categoryarealink.categoryid=categorylink.categoryid " +
                        "join categories on categoryarealink.categoryid=categories.id where " +
                      "categories.showinlist='1') ) and " +
                      " Topic.Obsolete = '0' and Topic.active_Ind = '1' " +
                      "and  " +
                      "(    ( topic.topicname like '%" + cSearchText.Replace("'", "''") + "%' " +
                      "      or topic.subtitle like '%" + cSearchText.Replace("'", "''") + "%' " +
                      "      or topic.course_number like '%" + cSearchText.Replace("'", "''") + "%' " +
                      "      or topic.objectives like '%" + cSearchText.Replace("'", "''") + "%' ) " +
                      "    or " +
                      "    ( topic.topicid in ( select topicid from topicresourcelink join resourceperson " +
                      "                         on topicresourcelink.resourceid = resourceperson.resourceid " +
                      "                         where topicresourcelink.restype = 'A' and " +
                      "                         resourceperson.fullname like '%" + cSearchText.Replace("'", "''") + "%' ) ) " +
                      "    or" +
                      "   ( topic.topicid in (select topicid from categorylink inner join categories on categorylink.categoryid=categories.id " +
                      "                       join facilityarealink on categories.facilityid=facilityarealink.areaid and facilityarealink.facilityid=@FacilityId " +
                      "                       where " +
                      "                       categories.showinlist='1' and categories.title like '%" + cSearchText.Replace("'", "''") + "%' ) )" +
                      ")   " +
                      " union " +
                      " (select topic.*,'Facility' as areaname,'Profession' as profession  from topic where facilityid=@FacilityId and Topic.Obsolete ='0' and Topic.active_ind='1' " +
                      " and ( " +
                    "    ( topic.topicname like '%" + cSearchText.Replace("'", "''") + "%' " +
                      "      or topic.subtitle like '%" + cSearchText.Replace("'", "''") + "%' " +
                      "      or topic.course_number like '%" + cSearchText.Replace("'", "''") + "%' " +
                      "      or topic.objectives like '%" + cSearchText.Replace("'", "''") + "%' ) ) )";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@SearchText", SqlDbType.VarChar).Value = cSearchText;
                cmd.Parameters.Add("@FacilityId", SqlDbType.VarChar).Value = facilityid;
                SqlDataAdapter dap = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("TopicList");
                dap.Fill(ds);
                return ds;
            }
        }
        public DataSet GetTopicsBySearchText(string cSearchText, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                // add space on each side of text (this limits the results to just those words and not fragments)
                if (String.IsNullOrEmpty(cSearchText))
                {
                    // no search text, so make up some that will return nothing
                    cSearchText = "XZXZXZXZXZXZXZXZXZXZ";
                }
                else if (cSearchText.Contains("'"))
                {
                    cSearchText = cSearchText.Replace("'", "").Trim();
                }

                else
                {
                    cSearchText = cSearchText.Trim();

                }

                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                     "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "at.areaname " +
                    "from Topic " +
                    "Inner join AreaType at on at.AreaId=Topic.FacilityID " +
                    "where Topic.Obsolete = '0' and Topic.Active_Ind = '1' " +
                    "and ( " +
                    "    ( topic.topicname like '%" + cSearchText + "%' " +
                    "      or topic.subtitle like '%" + cSearchText + "%' " +
                    "      or topic.course_number like '%" + cSearchText + "%' " +
                    "      or topic.objectives like '%" + cSearchText + "%' ) " +
                    "    or " +
                    "    ( topic.topicid in ( select topicid from topicresourcelink join resourceperson " +
                    "                         on topicresourcelink.resourceid = resourceperson.resourceid " +
                    "                         where topicresourcelink.restype = 'A' and " +
                    "                         resourceperson.fullname like '%" + cSearchText + "%' ) ) " +
                    "    ) ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@SearchText", SqlDbType.VarChar).Value = cSearchText;
                SqlDataAdapter dap = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("TopicList");
                dap.Fill(ds);
                return ds;
            }
        }


        //public DataSet GetTopicsBySearchAndSubsearchTextAndDomainID(
        //    string search, string subSearch, int domainID, string sortExpression)
        //{
        //    using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        //    {
        //        if (String.IsNullOrEmpty(search))
        //            search = "";
        //        else
        //            search = search.Replace("'", "''").Trim();

        //        if (String.IsNullOrEmpty(subSearch))
        //            subSearch = "";
        //        else
        //            subSearch = subSearch.Replace("'", "''").Trim();

        //        DataSet ds = new DataSet();

        //        SqlCommand cmd = new SqlCommand();
        //        SqlDataAdapter adapter;
        //        cmd.Connection = cn;
        //        cn.Open();
        //        cmd.CommandText = "sp_Get_Topics_By_Search_And_SubsearchText_And_DomainID";
        //        cmd.Parameters.Add("@search", SqlDbType.VarChar).Value = search;
        //        cmd.Parameters.Add("@subsearch", SqlDbType.VarChar).Value = subSearch;
        //        cmd.Parameters.Add("@domainID", SqlDbType.Int).Value = domainID;
        //        cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = sortExpression;
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        adapter = new SqlDataAdapter(cmd);
        //        adapter.Fill(ds);

        //        return ds;
        //    }
        //}

        /// <summary>
        /// Retrieves all topics in progress for the specified UserID (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTopicsInProgressByUserID(int UserID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                     "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "where Topic.Obsolete = '0' and Topic.Active_Ind = '1' And (Topic.hold_ind is null or Topic.hold_ind = 0)" +
                    "and Topic.TopicID IN " +
                    "(select distinct TopicID from Test where UserID = @UserID " +
                    "  and ( status = 'I' ) ) ";


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }




        /// <summary>
        /// Retrieves all topics in progress for the specified UserID (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTopicsInProgressByUserIDAndCount(int UserID, int Count, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                     "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_avg,  " +
                     "rating_num_comment, " +
                     "topic_hourlong, " +
                     "topic_hourshort, " +
                     "webinar_start, " +
                     "webinar_end " +
                    "from Topic " +
                    "where Topic.Obsolete = '0' and Topic.Active_Ind = '1' And (Topic.hold_ind is null or Topic.hold_ind = 0)" +
                    "and Topic.TopicID IN " +
                    "(select top " + Count + " TopicID from Test where UserID = @UserID " +
                    "  and ( status = 'I' ) order by " + cSortExpression + " desc  ) ";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }




        /// <summary>
        /// Retrieves all available topics for the specified CategoryID and username
        /// NOTE: This blocks topics that are already in the user's portfolio as Incomplete, or are in there
        ///    as complete with a completion date of at least 1 year ago
        /// </summary>
        public List<TopicInfo> GetAvailableTopicsByCategoryIDAndUserID(int CategoryID, int UserID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                     "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "join categorylink on Topic.topicid = categorylink.topicid " +
                    "where categorylink.CategoryID = @CategoryID  and Topic.Obsolete = '0' " +
                    "and Topic.Active_Ind = '1' and Topic.TopicID NOT IN " +
                    "(select TopicID from Test where UserID = @UserID " +
                    "  and ( status = 'I' or (status='C' and lastmod < GetDate()-365) ) ) ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all topics (not obsolete), plus category assignments for a CategoryID
        /// </summary>
        public List<CheckBoxListTopicInfo> GetAllTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select TopicID, Topic.TopicName, " +
                //    "IIF(ISNULL(cl.CategoryID), '0', '1') AS Selected from Topic co " +
                //    "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
                //    "and cl.CategoryID = ?";
                string cSQLCommand = "select Topic.TopicID, Topic.TopicName, " +
                    "CAST( (CASE WHEN (cl.CategoryID IS NULL) THEN '0' ELSE '1' END) AS bit ) AS Selected from Topic " +
                    "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
                    "and cl.CategoryID = @CategoryID where Topic.Obsolete = '0' and Topic.FacilityID=0 and Topic.Active_Ind = '1'  ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cn.Open();
                return GetCheckBoxListTopicCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Retrieves all non-textbook topics (not obsolete), plus category assignments for a CategoryID
        /// </summary>
        public List<CheckBoxListTopicInfo> GetAllNonTextbookTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select TopicID, Topic.TopicName, " +
                //    "IIF(ISNULL(cl.CategoryID), '0', '1') AS Selected from Topic co " +
                //    "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
                //    "and cl.CategoryID = ?";
                string cSQLCommand = "select Topic.TopicID, Topic.Course_Number + '  -  ' + Topic.TopicName as TopicName , " +
                    "CAST( (CASE WHEN (cl.CategoryID IS NULL) THEN '0' ELSE '1' END) AS bit ) AS Selected from Topic " +
                    "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
                    "and cl.CategoryID = @CategoryID where  Topic.Obsolete = '0' and Topic.Active_Ind = '1' and topic.facilityid = " +
                    " (select (CASE WHEN (Categories.facilityid >=0) THEN '0' ELSE Categories.facilityid END) from categories where Categories.id = @CategoryID)" +
                    " union " +
                    " select Topic.TopicID, Topic.Course_Number + '  -  ' + Topic.TopicName as TopicName, CAST( (CASE WHEN (cl.CategoryID IS NULL) THEN '0' ELSE '1' END) AS bit ) AS Selected " +
                    " from Topic join categorylink cl " +
                    " on Topic.TopicID = cl.topicid and cl.CategoryID = @CategoryID " +
                    " where  Topic.Obsolete = '0' and Topic.Active_Ind = '1'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cn.Open();
                return GetCheckBoxListTopicCollectionFromReader(ExecuteReader(cmd));
            }
        }

        /// <summary>
        /// Retrieves all textbook topics (not obsolete), plus category assignments for a CategoryID
        /// </summary>
        public List<CheckBoxListTopicInfo> GetAllTextbookTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select TopicID, Topic.TopicName, " +
                //    "IIF(ISNULL(cl.CategoryID), '0', '1') AS Selected from Topic co " +
                //    "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
                //    "and cl.CategoryID = ?";
                string cSQLCommand = "select Topic.TopicID, Topic.TopicName, " +
                    "CAST( (CASE WHEN (cl.CategoryID IS NULL) THEN '0' ELSE '1' END) AS bit ) AS Selected from Topic " +
                    "left outer join categorylink cl on Topic.TopicID = cl.topicid " +
                    "and cl.CategoryID = @CategoryID where Topic.Textbook='1' and Topic.Obsolete = '0' and Topic.FacilityID=0 and Topic.Active_Ind = '1'  ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cn.Open();
                return GetCheckBoxListTopicCollectionFromReader(ExecuteReader(cmd));
            }
        }


        /// <summary>
        /// Retrieves all textbook topics that have NO categories assigned (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTextbookTopicsWithNoCategories(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select Topic.* from Topic " +
                    "where Textbook='1' and Obsolete = '0' and FacilityID=0 and Topic.Active_Ind = '1' and TopicID not in " +
                    "(select distinct TopicID from categorylink)";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all non-textbook topics (not obsolete) that have NO categories assigned
        /// </summary>
        public List<TopicInfo> GetNonTextbookTopicsWithNoCategories(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select Topic.* from Topic " +
                    "where Textbook='0' and Obsolete = '0' and FacilityID=0 and Topic.Active_Ind = '1' and TopicID not in " +
                    "(select distinct TopicID from categorylink)";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves topics for the specified LectureID (LectureDefinition link)
        /// </summary>
        public List<TopicInfo> GetTopicsByLectureID(int LectureID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                   "DOCXFile, " +
                   "DOCXHoFile, " +
                   "Obsolete, " +
                   "FacilityID, " +
               "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                   "Minutes, " +
                   "Audio_Ind, " +
                   "Apn_Ind, " +
                   "Icn_Ind, " +
                   "Jcaho_Ind, " +
                   "Magnet_Ind, " +
                   "Active_Ind, " +
                   "Video_Ind, " +
                   "Online_Ind, " +
                   "Ebp_Ind, " +
                   "Ccm_Ind, " +
                   "Avail_Ind, " +
                   "Cert_Cerp, " +
                   "Ref_Html, " +
                   "AlterID, " +
                   "Pass_Score, " +
                   "Subtitle, " +
                    //"Cost, " +
                   "Topic_Type, " +
                   "Img_Name, " +
                   "Img_Credit, " +
                   "Img_Caption, " +
                   "Accreditation, " +
                   "Views, " +
                   "PrimaryViews, " +
                   "PageReads, " +
                    "Featured, " +
                   "rev, " +
                   "urlmark, " +
                   "offline_cost, " +
                   "online_cost, " +
                   "notest, " +
                   "uce_Ind, " +
                   "shortname, " +
                   "Topic.categoryid, " +
                   "hold_ind, " +
                   "rating_total, " +
                   "rating_avg,  " +
                   "prepaid_ind, " +
                   "topic_summary, " +
                   "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                   "rating_num_comment, " +
                   "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "join LectureTopicLink on Topic.topicid = LectureTopicLink.topicid " +
                    "where LectureTopicLink.LectureID = @LectureID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public void UpdateExpiringCourses()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_update_expiring_courses", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdateDaystoexpire()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "update expiringtopic set daystoexpire=datediff(day,getdate(),expiredate)";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }


        public void DeactiveExpiredCourses()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "UPDATE topic SET avail_Ind=0 FROM topic t JOIN dbo.ExpiringTopic e " +
        "ON t.topicid=e.topicid WHERE e.deactive=1 AND CONVERT(VARCHAR(max),e.expiredate,101)=CONVERT(VARCHAR(max),GETDATE(),101)";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void RemoveExpiredCourses()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "DELETE FROM expiringtopic WHERE daystoexpire=-30";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// Retrieves topics about to expire
        /// </summary>
        public DataSet GetExpiringCourses(String cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                string cSQLCommand = "Select [topicid],[topicname] ,[lastupdate] ,[course_number] ,CONVERT(VARCHAR(max),expiredate,101) as 'expiredate',[deactive] ,[daystoexpire] from expiringtopic";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    if (cSortExpression == "LastUpdate")
                    {
                        cSQLCommand = cSQLCommand +
                              " order by convert(DATETIME,LastUpdate)";
                    }
                    else if (cSortExpression == "LastUpdate DESC")
                    {
                        cSQLCommand = cSQLCommand +
                                               " order by convert(DATETIME,LastUpdate) DESC";
                    }
                    else if (cSortExpression == "expiredate")
                    {
                        cSQLCommand = cSQLCommand +
                              " order by convert(DATETIME,expiredate)";
                    }
                    else if (cSortExpression == "expiredate DESC")
                    {
                        cSQLCommand = cSQLCommand +
                                               " order by convert(DATETIME,expiredate) DESC";
                    }
                    else
                    {
                        cSQLCommand = cSQLCommand +
                            " order by " + cSortExpression;
                    }
                }



                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                DataSet ds = new DataSet("ExpiringTopics");
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                return ds;
            }
        }

        public void LoadExpiringCourses()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd1 = new SqlCommand("sp_load_expiring_courses", cn);
                cmd1.CommandType = CommandType.StoredProcedure;
                cn.Open();
                cmd1.ExecuteNonQuery();

            }
        }
        /// <summary>
        /// Update the deactive information
        /// </summary>
        public void UpdateExpiringCourses(int topicid, bool deactive)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                string cSQLCommand = "UPDATE expiringtopic SET Deactive=@Deactive WHERE Topicid=@Topicid";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@Deactive", SqlDbType.Bit).Value = deactive;
                cmd.Parameters.Add("@Topicid", SqlDbType.Int).Value = topicid;
                cn.Open();
                cmd.ExecuteNonQuery();

            }
        }
        /// <summary>
        /// Retrieves topics for the specified LectureID (LectureDefinition link)
        /// </summary>
        public DataSet GetTopicsByLectID(int LectureID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                   "DOCXFile, " +
                   "DOCXHoFile, " +
                   "Obsolete, " +
                   "FacilityID, " +
               "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                   "Minutes, " +
                   "Audio_Ind, " +
                   "Apn_Ind, " +
                   "Icn_Ind, " +
                   "Jcaho_Ind, " +
                   "Magnet_Ind, " +
                   "Active_Ind, " +
                   "Video_Ind, " +
                   "Online_Ind, " +
                   "Ebp_Ind, " +
                   "Ccm_Ind, " +
                   "Avail_Ind, " +
                   "Cert_Cerp, " +
                   "Ref_Html, " +
                   "AlterID, " +
                   "Pass_Score, " +
                   "Subtitle, " +
                    //"Cost, " +
                   "Topic_Type, " +
                   "Img_Name, " +
                   "Img_Credit, " +
                   "Img_Caption, " +
                   "Accreditation, " +
                   "Views, " +
                   "PrimaryViews, " +
                   "PageReads, " +
                    "Featured, " +
                   "rev, " +
                   "urlmark, " +
                   "offline_cost, " +
                   "online_cost, " +
                   "notest, " +
                   "uce_Ind, " +
                   "shortname, " +
                   "sequence, " +
                   "Topic.categoryid, " +
                   "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment " +
                    "from Topic " +
                    "join LectureTopicLink on Topic.topicid = LectureTopicLink.topicid " +
                    "where LectureTopicLink.LectureID = @LectureID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by sequence, " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@LectureID", SqlDbType.Int).Value = LectureID;

                SqlDataAdapter dap = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("TopicList");
                dap.Fill(ds);
                return ds;
            }
        }


        // TODO: Check this out!!!

        /// <summary>
        /// Returns the highest TopicID
        /// (used by the front-end to find the last ID inserted because
        /// all other methods via ObjectDataSource are failing)
        /// </summary>
        public int GetHighestTopicID()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select top 1 cast(TopicID as Integer) as TopicID from Topic order by TopicID Desc", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }


        /// <summary>
        /// Updates a Topic's HTML field
        /// </summary>
        public bool UpdateTopicHTML(int TopicID, string HTML, string LastUpdate, string FolderName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Topic set " +
                    "HTML = @HTML, " +
                    "LastUpdate = @LastUpdate, " +
                    "FolderName = @FolderName " +
                    "where TopicID = @TopicID", cn);

                cmd.Parameters.Add("@HTML", SqlDbType.VarChar).Value = HTML;
                cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = LastUpdate;
                cmd.Parameters.Add("@FolderName", SqlDbType.VarChar).Value = FolderName;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        /// <summary>
        /// Updates a Topic's HTML field
        /// </summary>
        public bool UpdateTopicImage(int TopicID, string image)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Topic set " +
                    "img_name = @image " +
                    "where TopicID = @TopicID", cn);

                cmd.Parameters.Add("@image", SqlDbType.VarChar).Value = image;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Updates a Textbook Topic's HTML field
        /// </summary>
        public bool UpdateTextbookTopicHTML(int TopicID, string HTML, string LastUpdate, string FolderName,
            string DOCXFile, string DOCXHoFile)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Topic set " +
                    "HTML = @HTML, " +
                    "LastUpdate = @LastUpdate, " +
                    "FolderName = @FolderName, " +
                    "DOCXFile = @DOCXFile, " +
                    "DOCXHoFile = @DOCXHoFile " +
                    "where TopicID = @TopicID", cn);

                cmd.Parameters.Add("@HTML", SqlDbType.VarChar).Value = HTML;
                cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = LastUpdate;
                cmd.Parameters.Add("@FolderName", SqlDbType.VarChar).Value = FolderName;
                cmd.Parameters.Add("@DOCXFile", SqlDbType.VarChar).Value = DOCXFile;
                cmd.Parameters.Add("@DOCXHoFile", SqlDbType.VarChar).Value = DOCXHoFile;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Updates a Topic's User-Editable fields (called from Topic Edit)
        /// </summary>
        public bool UpdateTopicEditableFields(int TopicID, bool CorLecture,
            string Fla_CEType, string Fla_IDNo, string LastUpdate, bool NoSurvey,
            int SurveyID, bool Compliance, string MetaKW, string MetaDesc,
            decimal Hours, string MediaType, string Objectives, string Content,
            bool Textbook, int CertID, string Method, string GrantBy, bool Obsolete,
             string TopicName)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Topic set " +
                    "CorLecture = @CorLecture, " +
                    "Fla_CEType = @Fla_CEType, " +
                    "Fla_IDNo = @Fla_IDNo, " +
                    "LastUpdate = @LastUpdate, " +
                    "NoSurvey = @NoSurvey, " +
                    "SurveyID = @SurveyID, " +
                    "Compliance = @Compliance, " +
                    "MetaKW = @MetaKW, " +
                    "MetaDesc = @MetaDesc, " +
                    "Hours = @Hours, " +
                    "MediaType = @MediaType, " +
                    "Objectives = @Objectives, " +
                    "Content = @Content, " +
                    "Textbook = @Textbook, " +
                    "CertID = @CertID, " +
                    "Method = @Method, " +
                    "GrantBy = @GrantBy, " +
                    "Obsolete = @Obsolete, " +
                    "TopicName = @TopicName " +
                    "where TopicID = @TopicID", cn);

                cmd.Parameters.Add("@CorLecture", SqlDbType.Bit).Value = CorLecture;
                cmd.Parameters.Add("@Fla_CEType", SqlDbType.Char).Value = Fla_CEType;
                cmd.Parameters.Add("@Fla_IDNo", SqlDbType.VarChar).Value = Fla_IDNo;
                cmd.Parameters.Add("@LastUpdate", SqlDbType.VarChar).Value = LastUpdate;
                cmd.Parameters.Add("@NoSurvey", SqlDbType.Bit).Value = NoSurvey;

                // pass null to database if value is zero (nothing selected by user)
                // this allows the database to have a NULL foreign key and still enforce relational integrity
                if (SurveyID == 0)
                {
                    cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = SurveyID;
                }

                cmd.Parameters.Add("@Compliance", SqlDbType.Bit).Value = Compliance;
                cmd.Parameters.Add("@MetaKW", SqlDbType.VarChar).Value = MetaKW;
                cmd.Parameters.Add("@MetaDesc", SqlDbType.VarChar).Value = MetaDesc;
                cmd.Parameters.Add("@Hours", SqlDbType.Decimal).Value = Hours;
                cmd.Parameters.Add("@MediaType", SqlDbType.VarChar).Value = MediaType;
                cmd.Parameters.Add("@Objectives", SqlDbType.VarChar).Value = Objectives;
                cmd.Parameters.Add("@Content", SqlDbType.VarChar).Value = Content;
                cmd.Parameters.Add("@Textbook", SqlDbType.Bit).Value = Textbook;
                cmd.Parameters.Add("@CertID", SqlDbType.Int).Value = CertID;
                cmd.Parameters.Add("@Method", SqlDbType.VarChar).Value = Method;
                cmd.Parameters.Add("@GrantBy", SqlDbType.VarChar).Value = GrantBy;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@Obsolete", SqlDbType.Bit).Value = Obsolete;
                cmd.Parameters.Add("@TopicName", SqlDbType.VarChar).Value = TopicName;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public List<TopicInfo> GetTopicsByLectEvtID(int LectEvtID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                    "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                     "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                    "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                     "rating_total, " +
                    "rating_avg,  " +
                    "prepaid_ind, " +
                    "topic_summary, " +
                    "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                    "rating_num_comment, " +
                    "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "join TopicAudit on Topic.topicid = TopicAudit.topicid " +
                    "join LectureEventTopicLink on Topic.topicid = LectureEventTopicLink.topicid " +
                    "where LectureEventTopicLink.LectEvtID = @LectEvtID";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@LectEvtID", SqlDbType.Int).Value = LectEvtID;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public DataSet GetRecommendationTopicsByTopicId(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_Course_Recommendation";
                cmd.Parameters.Add("@topic_id", SqlDbType.Int).Value = TopicID;
                cn.Open();
                SqlDataAdapter adpt = new SqlDataAdapter();
                adpt.SelectCommand = cmd;
                DataSet ds = new DataSet();
                adpt.Fill(ds);
                return ds;
            }
        }

        public int IsPTOTTopic(int TopicId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select topicid from categorylink where categoryid in (60,160,174) and topicid= @TopicId ";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                if (reader.Read())
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }

        }

        /// <summary>
        /// Retrieves all Retail topics for the specified search text (not obsolete)and Facility
        /// NOTE: Looks in topicname, subtitle, objectives, course_number and authorname
        /// </summary>
        public List<TopicInfo> GetRetailTopicsBySearchTextandFacility(string cSearchText, int facilityid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                // add space on each side of text (this limits the results to just those words and not fragments)
                if (String.IsNullOrEmpty(cSearchText))
                {
                    // no search text, so make up some that will return nothing
                    cSearchText = "XZXZXZXZXZXZXZXZXZXZ";
                }
                else
                {
                    cSearchText = cSearchText.Trim();
                }

                string cSQLCommand = "select " +
                    "Topic.TopicID, " +
                    "TopicName, " +
                    "HTML, " +
                    "CorLecture, " +
                    "Fla_CEType, " +
                    "Fla_IDNo, " +
                    "ReleaseDate, " +
                    "ExpireDate, " +
                    "LastUpdate, " +
                    "FolderName, " +
                    "NoSurvey, " +
                    "SurveyID, " +
                    "Compliance, " +
                    "MetaKW, " +
                    "MetaDesc, " +
                    "Hours, " +
                    "MediaType, " +
                    "Objectives, " +
                    "Content, " +
                    "Textbook, " +
                    "CertID, " +
                    "Method, " +
                    "GrantBy, " +
                    "DOCXFile, " +
                    "DOCXHoFile, " +
                    "Obsolete, " +
                    "FacilityID, " +
                  "Course_Number, " +
                    //"CeRef, " +
                    //"Release_Date, " +
                    "Minutes, " +
                    "Audio_Ind, " +
                    "Apn_Ind, " +
                    "Icn_Ind, " +
                    "Jcaho_Ind, " +
                    "Magnet_Ind, " +
                    "Active_Ind, " +
                    "Video_Ind, " +
                    "Online_Ind, " +
                    "Ebp_Ind, " +
                    "Ccm_Ind, " +
                    "Avail_Ind, " +
                    "Cert_Cerp, " +
                    "Ref_Html, " +
                    "AlterID, " +
                    "Pass_Score, " +
                    "Subtitle, " +
                    //"Cost, " +
                    "Topic_Type, " +
                    "Img_Name, " +
                    "Img_Credit, " +
                    "Img_Caption, " +
                    "Accreditation, " +
                    "Views, " +
                    "PrimaryViews, " +
                    "PageReads, " +
                    "Featured, " +
                    "rev, " +
                    "urlmark, " +
                    "offline_cost, " +
                    "online_cost, " +
                    "notest, " +
                   "uce_Ind, " +
                    "shortname, " +
                    "Topic.categoryid, " +
                    "hold_ind, " +
                    "rating_total, " +
                    "rating_avg,  " +
                     "prepaid_ind, " +
                     "topic_summary, " +
                     "cme_ind, " +
                    "cme_sponsor_ind, " +
                    "cme_sponsor_name, " +
                     "rating_num_comment, " +
                     "topic_hourlong, " +
                    "topic_hourshort,  " +
                    "webinar_start,    " +
                    "webinar_end  " +
                    "from Topic " +
                    "where (topicid in " +
                      "(select topicid from categorylink inner join categories on categorylink.categoryid=categories.id " +
                     "join categoryarealink on categories.id=categoryarealink.categoryid and categoryarealink.areaid=2 where " +
                    "categories.showinlist='1') ) and " +
                    "Topic.facilityid=@FacilityId and Topic.Obsolete = '0' and Topic.avail_Ind = '1' " +
                    "and ( " +
                    "    ( topic.topicname like '%" + cSearchText.Replace("'", "''") + "%' " +
                    "      or topic.subtitle like '%" + cSearchText.Replace("'", "''") + "%' " +
                    "      or topic.course_number like '%" + cSearchText.Replace("'", "''") + "%' " +
                    "       or topic.course_number + topic.rev like '%" + cSearchText.Replace("'", "''") + "%' " +
                    "      or topic.objectives like '%" + cSearchText.Replace("'", "''") + "%' ) " +
                    "    or " +
                    "    ( topic.topicid in ( select topicid from topicresourcelink join resourceperson " +
                    "                         on topicresourcelink.resourceid = resourceperson.resourceid " +
                    "                         where topicresourcelink.restype = 'A' and " +
                    "                         resourceperson.fullname like '%" + cSearchText.Replace("'", "''") + "%' ) ) " +
                    " or" +
                    "   ( topic.topicid in (select topicid from categorylink inner join categories on categorylink.categoryid=categories.id " +
                    "join categoryarealink on categories.id=categoryarealink.categoryid and categoryarealink.areaid=2 where " +
                    "categories.showinlist='1' and categories.title like '%" + cSearchText.Replace("'", "''") + "%' ) )" +
                    "    ) ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@SearchText", SqlDbType.VarChar).Value = cSearchText;
                cmd.Parameters.Add("@FacilityId", SqlDbType.VarChar).Value = facilityid;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves Pearls Review topics for the specified search text (not obsolete)
        /// NOTE: Looks in topicname, subtitle, objectives, course_number and authorname
        /// </summary>
        public List<TopicInfo> GetPearlsReviewTopicsBySearchText(string cSearchText, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                // add space on each side of text (this limits the results to just those words and not fragments)
                if (String.IsNullOrEmpty(cSearchText))
                {
                    // no search text, so make up some that will return nothing
                    cSearchText = "XZXZXZXZXZXZXZXZXZXZ";
                }
                else
                {
                    cSearchText = cSearchText.Trim();
                }

                string cSQLCommand = "select *" +
                    "from Topic " +
                    "where categoryid IN (SELECT categoryid FROM dbo.Categories WHERE showinlist='1' AND facilityid=-1 )" +
                    "AND  Obsolete = '0' and avail_Ind = '1' " +
                    "AND " +
                     "    ( topic.topicname like '%" + cSearchText.Replace("'", "''") + "%' " +
                    "      or topic.subtitle like '%" + cSearchText.Replace("'", "''") + "%' " +
                    "      or topic.course_number like '%" + cSearchText.Replace("'", "''") + "%' " +
                    "       or topic.course_number + topic.rev like '%" + cSearchText.Replace("'", "''") + "%' " +
                    "      or topic.objectives like '%" + cSearchText.Replace("'", "''") + "%' ) " +
                               "    or " +
                      "    ( topic.topicid in ( select topicid from topicresourcelink join resourceperson " +
                      "                         on topicresourcelink.resourceid = resourceperson.resourceid " +
                      "                         where topicresourcelink.restype = 'A' and " +
                      "                         resourceperson.fullname like '%" + cSearchText.Replace("'", "''") + "%' ) ) " +
                       "    or" +
                      "   ( topic.topicid in (select topicid from categorylink inner join categories on categorylink.categoryid=categories.id " +
                      "                       join facilityarealink on categories.facilityid=facilityarealink.areaid and facilityarealink.facilityid=0 " +
                      "                       where " +
                      "                       categories.showinlist='1' and categories.title like '%" + cSearchText.Replace("'", "''") + "%' ) )";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@SearchText", SqlDbType.VarChar).Value = cSearchText;

                cn.Open();

                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        public List<TopicInfo> GetFacilityTopicsByFacilityId(int FacilityId, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                                       "from Topic " +
                    "where active_ind='1' and obsolete='0' and facilityid = @FacilityId";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = FacilityId;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        public List<TopicInfo> GetWebinarTopics(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                                       "from Topic " +
                    "where active_ind='1' and obsolete='0' and mediatype = 'webinar'";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<TopicInfo> GetNewTopicsByCount(int Count, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top " + Count + " * " +
                                       "from Topic " +
                    "where active_ind='1' and obsolete='0' and facilityid<=0 order by lastupdate desc";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " , " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        public List<TopicInfo> GetFacilityTopicsByFacilityIdAndMediaType(int FacilityId, string MediaType, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                                       "from Topic " +
                    "where active_ind='1' and obsolete='0' and facilityid = @FacilityId and MediaType=@MediaType";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = FacilityId;
                cmd.Parameters.Add("@MediaType", SqlDbType.VarChar).Value = MediaType;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all topics in progress for the specified UserID (not obsolete)
        /// </summary>
        public List<TopicInfo> GetTopicsInProgressByUserIDAndAreaName(int UserID, string AreaName, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = string.Empty;
                if (cSQLCommand == "Facility")
                {
                    cSQLCommand = "select * " +
                          "from Topic " +
                          "where Topic.Obsolete = '0' and Topic.Active_Ind = '1' And (Topic.hold_ind is null or Topic.hold_ind = 0)" +
                          " and topic.facility > 0 and Topic.TopicID IN " +
                          "(select distinct TopicID from Test where UserID = @UserID " +
                          "  and ( status = 'I' ) )  ";

                }
                else
                {
                    cSQLCommand = "select * " +
                       " from Topic Join AreaType on Topic.facilityid=AreaType.areaid " +
                       " where Topic.Obsolete = '0' and Topic.Active_Ind = '1' And (Topic.hold_ind is null or Topic.hold_ind = 0)" +
                       " and areatype.areaname = @AreaName and Topic.TopicID IN " +
                       " (select distinct TopicID from Test where UserID = @UserID " +
                       "  and ( status = 'I' ) )  ";
                }


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@AreaName", SqlDbType.VarChar).Value = AreaName;
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves all topics in progress for the specified UserID (not obsolete)
        /// </summary>

        public DataSet GetTopicsInProgressByUserIDAndFacilityID(int UserID, int FacilityID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = string.Empty;

                cSQLCommand = " SELECT *, (select top(1) lastmod from test where topicid = topic.topicid and userid = @UserID) lastmod FROM Topic INNER JOIN AreaType ON Topic.facilityid = AreaType.areaid AND Topic.obsolete = '0' AND Topic.active_ind = '1' AND (Topic.hold_ind IS NULL OR " +
                        " Topic.hold_ind = 0) AND Topic.topicid IN (SELECT DISTINCT topicid FROM Test  WHERE (userid = @UserID) AND (status = 'I') AND Printed_date is null union all SELECT DISTINCT topicid FROM Test a join iceprogress b on a.testid=b.testid WHERE (userid = @UserID) AND (status = 'I') and b.ip_status='C' and Printed_date is not null)" +
                " AND AreaType.areaname IN (SELECT     AreaType.areaname " +
                        " FROM AreaType INNER JOIN FacilityAreaLink ON AreaType.areaid = FacilityAreaLink.areaid WHERE (FacilityAreaLink.facilityid = @FacilityID))";


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + " lastmod desc, AreaType.areaname, Topic.topicname  asc";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                SqlDataAdapter dap = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("TopicList");
                dap.Fill(ds);
                return ds;

                //SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                //cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                //cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
                //cn.Open();
                //return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<TopicInfo> GetActiveFeaturedTopicsByFacID(int FacID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = string.Empty;
                cSQLCommand = "select topic.* from topic join categorylink on topic.topicid=categorylink.topicid join  " +
                              "categories on categories.id=categorylink.categoryid " +
                               "join facilityarealink  on categories.facilityid=facilityarealink.areaid where title='*Featured Courses' " +
               "and facilityarealink.facilityid= " + FacID + " and topic.active_ind='1' and categories.showinlist='1'";




                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                     " order by " + " convert(datetime, Topic.lastupdate, 101) desc , " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        public List<TopicInfo> GetNewTopicsByFacID(int FacID, int Count, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = string.Empty;
                cSQLCommand = "select top " + Count + " topic.* " +
                                " from topic where topicid in (select topic.topicid from topic  join categorylink " +
                                " on topic.topicid=categorylink.topicid join  " +
                              "categories on categories.id=categorylink.categoryid " +
                               "join facilityarealink  on categories.facilityid=facilityarealink.areaid where " +
               " facilityarealink.facilityid= " + FacID + " and topic.active_ind='1' and categories.showinlist='1')";


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                       " order by convert(datetime, topic.lastupdate) desc, " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        /// <summary>
        /// Retrieves new topics for welcome page
        /// </summary>
        /// 
        public List<TopicInfo> GetNewTopicsFromPickListByFacID(int FacID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = string.Empty;
                cSQLCommand = "select A.* " +
                                " from topic A join MicrositePickList B ON A.TopicID=B.TopicID WHERE msid= " + FacID + " and pick_Type='new' order by pick_order";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                       ", " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves enrolled topics for welcome page
        /// </summary>
        /// 

        /// <summary>
        /// Retrieves enrolled topics for mycourses page
        /// </summary>
        /// 
        public DataSet GetMyEnrolledTopicsByUserIdAndFacilityId(int userid, int facilityid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = string.Empty;
                cSQLCommand = "select * from (select Topic.*,AreaType.*, case when A.reported_date is not null then A.reported_date else convert(datetime,'01/01/2200') end complete_date," +
                    " case when C.isid is null then 'N' else 'Y' end isICE, " +
                     " case when D.iid is null then 'Self' else D.cfirstname + ' ' + D.clastname end enrolledby " +
                                " from Test A join Topic on A.topicid=Topic.topicid" +
                                             " left join ICESection C on Topic.topicid=C.topicid" +
                                             " join AreaType ON Topic.facilityid = AreaType.areaid" +
                                             " join FacilityAreaLink E on AreaType.Areaid=E.AreaId" +
                                             " left join UserAccount D on A.uniqueid = D.iid" +
                                             " where A.status in ('I') and A.userid=@userid and E.facilityid=@facilityid and A.Printed_date is not null) a ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                       " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = facilityid;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("TopicList");
                sqlDataAdapter.Fill(ds);
                return ds;
            }
        }

        public DataSet GetEnrolledTopicsByUserId(int userid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = string.Empty;
                cSQLCommand = "select * from (select B.*, case when A.reported_date is not null then a.reported_date else convert(datetime,'01/01/2200') end complete_date," +
                    " case when C.isid is null then 'N' else 'Y' end isICE " +
                                " from Test A join Topic B on A.topicid=B.topicid" +
                                             " left join ICESection C on B.topicid=C.topicid" +
                                             " where  A.userid=@userid and printed_date is not null and A.status in ('I'))  a order by  complete_date, course_number";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                       ", " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("TopicList");
                sqlDataAdapter.Fill(ds);
                return ds;
            }
        }

        /// <summary>
        /// Retrieves assigned topics for welcome page
        /// </summary>
        /// 
        public DataSet GetAssignedTopicsByUserId(int userid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = string.Empty;
                cSQLCommand = "select * from (select A.TestID, B.*,case when A.reported_date is not null then A.reported_date when B.expiredate is null then dateadd(year,3,lastupdate) else B.expiredate end complete_date, " +
                    " case when C.isid is null then 'N' else 'Y' end isICE, case when A.uniqueid is null then 0 else A.uniqueid end enrolledbyid, " +
                    " case when A.uniqueid is null then '' else D.cfirstname + ' ' + D.clastname end enrolledby " +
                                " from Test A join Topic B on A.topicid=B.topicid" +
                                             " left join ICESection C on B.topicid=C.topicid" +
                                             " left join useraccount D on A.uniqueid=D.iid" +
                                             " where A.status in ('A') and A.userid=@userid) a order by complete_date";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                       ", " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("TopicList");
                sqlDataAdapter.Fill(ds);
                return ds;
            }
        }


        /// <summary>
        /// Retrieves popular topics for welcome page
        /// </summary>
        /// 
        public List<TopicInfo> GetPopularTopicsFromPickListByFacID(int FacID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = string.Empty;
                cSQLCommand = "select A.* " +
                                " from topic A join MicrositePickList B ON A.TopicID=B.TopicID WHERE msid= " + FacID + " and pick_Type='most-popular' order by pick_order";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                       ", " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        /// <summary>
        /// Retrieves topics for the specified CatID 
        /// </summary>
        public DataSet GetTopicsByCategoryID(int CategoryID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "categories.title as CategoryName,topic.topicname as TopicName,topic.subtitle as SubTitle ,topic.hours as ContactHours ,topic.course_number as CourseNumber,topic.topic_hourshort " +
                     "from topic inner join Categorylink on topic.topicid=categorylink.topicid " +
                     " inner join categories on categories.id=categorylink.categoryid " +
                     " where topic.active_ind=1 and categories.showinlist='1' and Categories.id = " + CategoryID + " order by categories.title,topic.topicname ";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                SqlDataAdapter dadp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("TopicList");
                dadp.Fill(ds);
                return ds;
            }
        }

        /// <summary>
        /// Retrieves facility topics for the facility id
        /// </summary>
        public DataSet GetFacilityTopicsByFacID(int FacID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select " +
                    "'Facility Topics' as CategoryName,topic.topicid as TopicID, topic.topicname as TopicName,topic.subtitle as SubTitle ,topic.hours as ContactHours ,topic.course_number as CourseNumber,topic.topic_hourshort " +
                     "from topic where topic.active_ind='1' and topic.facilityid = " + FacID + " order by CategoryName,topic.topicname ";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                SqlDataAdapter dadp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("TopicList");
                dadp.Fill(ds);
                return ds;
            }
        }

        public DataSet GetCEModulesByICETopicID(int topicid,bool isWebinar)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_get_cemodules_by_iceTopicID", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cmd.Parameters.Add("@isWebinar", SqlDbType.Bit).Value = isWebinar;                

                cn.Open();


                SqlDataAdapter dadp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("CEModules");
                dadp.Fill(ds);
                return ds;
            }
        }

        public DataSet GetEnrolledTopicsByFacIDAndDeptIDs(int FacID,int uniqueid,int deptid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                
                    SqlCommand cmd = new SqlCommand("sp_GetAllCoursesWithEnrolleesByFacIDWithoutPrefix", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@facid", SqlDbType.Int).Value = FacID;
                    cmd.Parameters.Add("@uniqueid", SqlDbType.Int).Value = uniqueid;
                    cmd.Parameters.Add("@deptid", SqlDbType.Int).Value = deptid;

                    cn.Open();

                    SqlDataAdapter dadp = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet("TopicList");
                    dadp.Fill(ds);
                    return ds;
               
            }
        }

        /// <summary>
        /// Retrieves webinar topics for particulat facilityid
        /// </summary>

        public List<TopicInfo> GetWebinarTopicsByFacID(int FacID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = string.Empty;
                cSQLCommand = "select distinct topic.* from topic join categorylink on topic.topicid=categorylink.topicid join  " +
                              "categories on categories.id=categorylink.categoryid " +
                               "join facilityarealink  on categories.facilityid=facilityarealink.areaid where " +
               " facilityarealink.facilityid= " + FacID + " and topic.active_ind='1' and categories.showinlist='1' and topic.mediatype='Webinar' ";


                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by  " + cSortExpression;

                }
                else
                {
                    cSQLCommand = cSQLCommand +
                        " order by TopicName ";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        public bool ISTopicInDomain(int TopicId, int DomainId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQlCommand = string.Empty;
                cSQlCommand = "DECLARE @IsSingle bit select @IsSingle = single_prof_ind from ahdomain where ad_id=@domainid " +
                                  "if @IsSingle = 0 " +
                                  " select Topic.Topicid from topic join categorylink on topic.topicid = categorylink.topicid " +
                                  "join categorymicrositelink on categorylink.categoryid=categorymicrositelink.categoryid " +
                                  " where topic.topicid=@TopicId and topic.active_ind='1' and topic.obsolete='0' and categorymicrositelink.msid in ( " +
                                  " select msid from micrositedomain )" +
                                  "else select Topic.Topicid from topic join categorylink on topic.topicid = categorylink.topicid " +
                                  "join categorymicrositelink on categorylink.categoryid=categorymicrositelink.categoryid " +
                                  " where topic.topicid=@TopicId and topic.active_ind='1' and topic.obsolete='0' and categorymicrositelink.msid=@DomainId";

                SqlCommand cmd = new SqlCommand(cSQlCommand, cn);
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                if (reader.Read())
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public bool ISTopicInAhDomain(int TopicId, int ahDomainId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQlCommand = string.Empty;
                cSQlCommand = " select Topic.Topicid from topic join categorylink on topic.topicid = categorylink.topicid " +
                                  "join categorymicrositelink on categorylink.categoryid=categorymicrositelink.categoryid " +
                                  " where topic.topicid=@TopicId and topic.active_ind='1' and topic.obsolete='0' and categorymicrositelink.msid in ( " +
                                  " select msid from alliedproflink where ad_id=@ahDomainId )";

                SqlCommand cmd = new SqlCommand(cSQlCommand, cn);
                cmd.Parameters.Add("@TopicId", SqlDbType.Int).Value = TopicId;
                cmd.Parameters.Add("@ahDomainId", SqlDbType.Int).Value = ahDomainId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                if (reader.Read())
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }


        public bool withFollowUpSurvey(int topicid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select top 1 * from topicsurveylink where topicid=@topicid and surveyid > 0 and surveytype='3-Month Follow-up Survey'";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool CheckPharmacistTopicByTopicId(int TopicID)
        {
            //Hard coded categoryID for pharmacist
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select topicid from topic where topicid=@TopicID and categoryid=174";
                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                if (reader.Read())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        /// <summary>
        /// returns topic objective
        /// </summary>
        public string GetCourseObjectiveByTopicID(int topicid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select objectives from topic where topicid=@topicid", cn);
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cn.Open();

                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return Convert.ToString(reader["objectives"]);
                else
                    return null;
            }
        }

        #region Moved from category
        /// <summary>
        /// Assign a a topic to a category
        /// </summary>
        public bool AssignTopicToCategory(int TopicID, int CategoryID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(
                    "select Count(*) from categorylink " +
                    "where TopicID = @TopicID and CategoryID = @CategoryID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cn.Open();
                int ResultCount = (int)ExecuteScalar(cmd);

                if (ResultCount > 1)
                    // link already exists, so nothing to do
                    return true;

                // link does not exist, so insert it
                SqlCommand cmd2 = new SqlCommand("insert into categorylink " +
                    "(CategoryID, " +
                    "TopicID) " +
                    "VALUES (" +
                    "@CategoryID, " +
                    "@TopicID)", cn);

                cmd2.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cmd2.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;

                int ret = ExecuteNonQuery(cmd2);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Detach a topic assignment from a category
        /// </summary>
        public bool DetachTopicFromCategory(int TopicID, int CategoryID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(
                    "delete from categorylink " +
                    "where TopicID = @TopicID and CategoryID = @CategoryID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Assign topics to category from comma-delimited list
        /// </summary>
        public bool UpdateCategoryTopicAssignments(int CategoryID, string TopicIDAssignments)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                // first delete any assignments that are not in the list passed in
                //              SqlCommand cmd = new SqlCommand(
                //                  "DELETE FROM categorylink " +
                //                  "where CategoryID = ? and TopicID NOT IN ("+TopicIDAssignments.Trim()+")", cn);

                // NOTE: For now, just delete them all
                SqlCommand cmd = new SqlCommand(
                    "DELETE FROM categorylink " +
                    "where CategoryID = @CategoryID ", cn);

                cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;

                //              cmd.Parameters.Add("@TopicIDAssignments", SqlDbType.VarChar).Value = TopicIDAssignments;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                //              return (ret == 1);

                if (TopicIDAssignments.Trim().Length < 1)
                    // no assignments, so nothing to do
                    return true;

                // now step through all TopicIDs in the list passed in and call
                // this.AssignTopicToCategory() for each, which will insert the assignment
                // if it does not already exist
                string[] TopicIDs = TopicIDAssignments.Split(',');

                foreach (string cTopicID in TopicIDs)
                {
                    this.AssignTopicToCategory(Int32.Parse(cTopicID), CategoryID);
                }

                return true;
            }
        }

        /// <summary>
        /// Assign topics to category from comma-delimited list
        /// </summary>
        public bool UpdateTopicCategoryAssignments(int TopicID, string CategoryIDAssignments)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                // first delete any assignments that are not in the list passed in
                //              SqlCommand cmd = new SqlCommand(
                //                  "DELETE FROM categorylink " +
                //                  "where TopicID = ? and CategoryID NOT IN (?)", cn);

                // NOTE; For now, just delete them all
                SqlCommand cmd = new SqlCommand(
                    "DELETE FROM categorylink " +
                    "where TopicID = @TopicID ", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                //              cmd.Parameters.Add("@CategoryIDAssignments", SqlDbType.VarChar).Value = CategoryIDAssignments;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                //              return (ret == 1);

                if (CategoryIDAssignments.Trim().Length < 1)
                    // no assignments, so nothing to do
                    return true;

                // now step through all CategoryIDs in the list passed in and call
                // this.AssignTopicToCategory() for each, which will insert the assignment
                // if it does not already exist
                string[] CategoryIDs = CategoryIDAssignments.Split(',');

                foreach (string cCategoryID in CategoryIDs)
                {
                    this.AssignTopicToCategory(TopicID, Int32.Parse(cCategoryID));
                }

                return true;
            }
        }


        /// <summary>
        /// Detach a topic from ALL categories
        /// </summary>
        public bool DetachTopicFromAllCategories(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(
                    "delete from categorylink " +
                    "where TopicID = @TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public int UpdateTopicInProgressByUserID(int UserID, int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "Delete from Test where UserID = @UserID and TopicID = @TopicID and Status<>'C' and Printed_date is null";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                cn.Open();
                return ExecuteNonQuery(cmd);
                //return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }


        public List<TopicInfo> GetTopicsByTopicIds(string TopicIds, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSqlQuery = "select * from topic where topicid in ( " + TopicIds + " ) and Obsolete = '0' and Active_Ind = '1' ";
                if (!string.IsNullOrEmpty(cSortExpression))
                {
                    cSqlQuery = cSqlQuery + " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSqlQuery, cn);
                cn.Open();
                return GetTopicCollectionFromReader(ExecuteReader(cmd), false);
            }
        }



        #endregion

        #region MicroSite

        public DataSet GetTopicsByMicroSiteDomainId(int domainID, string sortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "microsite_topic_getCoursesByDomainID";
                cmd.Parameters.Add("@domainID", SqlDbType.Int).Value = domainID;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = sortExpression;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }

        public DataSet GetTopicsByMicroSiteDomainIdWithBundle(int domainID, string sortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "microsite_topic_getCoursesByDomainIDWithBundle";
                cmd.Parameters.Add("@domainID", SqlDbType.Int).Value = domainID;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = sortExpression;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }

        public DataSet GetMostPopularTopicsByMicroSiteDomainID(int domainID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "microsite_topic_getMostPopularCoursesByDomainID";
                cmd.Parameters.Add("@domainID", SqlDbType.Int).Value = domainID;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }


        public DataSet GetSponsoredTopicsByDomainId(int DomainId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "microsite_topic_getFreeCoursesByDomainID";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                cmd.Parameters.Add("@domainId", SqlDbType.Int).Value = DomainId;
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet("SponsoredList");
                adp.Fill(ds);
                return ds;
            }
        }
        public DataSet GetMicrositeStateReqTopicsByDomainIDAndStateAbr(int domainID, string stateabr)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_Get_State_Requirement_Topics_By_State_And_Profession";
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = domainID;
                cmd.Parameters.Add("@stateabr", SqlDbType.Char).Value = stateabr;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }
        public DataSet GetMicrositeStateReqTopicsByDomainIDAndStateAbrWithBundleFirst(int domainID, string stateabr)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_Get_State_Requirement_Topics_By_State_And_Profession_BundleFirst";
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = domainID;
                cmd.Parameters.Add("@stateabr", SqlDbType.Char).Value = stateabr;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }
        public DataSet GetTopicsBySearchFilters(string msid, int categoryID, string mediaType, string releaseOrUpdateDate,
                    string searchText, string sortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "microsite_topic_getCoursesBySearchFilters";
                cmd.Parameters.Add("@searchText", SqlDbType.VarChar).Value = searchText;
                cmd.Parameters.Add("@msid", SqlDbType.VarChar).Value = msid;
                cmd.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;
                cmd.Parameters.Add("@mediaType", SqlDbType.VarChar).Value = mediaType;
                cmd.Parameters.Add("@releaseOrUpdateDate", SqlDbType.VarChar).Value = releaseOrUpdateDate;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = sortExpression;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }

        public DataSet GetTopicCountsBySearchFilters(string msid, int categoryID, string mediaType, string releaseOrUpdateDate,
            string searchText)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "microsite_topic_getCourseCountBySearchFilters";
                cmd.Parameters.Add("@searchText", SqlDbType.VarChar).Value = searchText;
                cmd.Parameters.Add("@msid", SqlDbType.VarChar).Value = msid;
                cmd.Parameters.Add("@categoryID", SqlDbType.Int).Value = categoryID;
                cmd.Parameters.Add("@mediaType", SqlDbType.VarChar).Value = mediaType;
                cmd.Parameters.Add("@releaseOrUpdateDate", SqlDbType.VarChar).Value = releaseOrUpdateDate;

                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);

                return ds;
            }
        }

        public DataTable GetStateMandatedTopicsByDomainId(int DomainId, string cSortExpression)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "";
                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@DomainId", SqlDbType.Int).Value = DomainId;
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }

        #endregion

        #endregion

        #region PRProvider


        /// <summary>
        /// Returns a new TopicInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual TopicInfo GetTopicFromReader(IDataReader reader)
        {
            return GetTopicFromReader(reader, true);
        }
        protected virtual TopicInfo GetTopicFromReader(IDataReader reader, bool readMemos)
        {
            TopicInfo Topic = new TopicInfo(
              (bool)reader["CorLecture"],
              reader["Fla_CEType"].ToString(),
              reader["Fla_IDNo"].ToString(),
              reader["FolderName"].ToString(),
              reader["HTML"].ToString(),
              (Convert.IsDBNull(reader["ReleaseDate"]) ? null : (DateTime?)reader["ReleaseDate"]),
              (Convert.IsDBNull(reader["ExpireDate"]) ? null : (DateTime?)reader["ExpireDate"]),
              reader["LastUpdate"].ToString(),
              (bool)reader["NoSurvey"],
              (int)reader["TopicID"],
              reader["TopicName"].ToString(),
              (int)(Convert.IsDBNull(reader["SurveyID"]) ? (int)0 : (int)reader["SurveyID"]),
              (bool)reader["Compliance"],
              reader["MetaKW"].ToString(),
              reader["MetaDesc"].ToString(),
              (decimal)reader["Hours"],
              reader["MediaType"].ToString(),
              reader["Objectives"].ToString(),
              reader["Content"].ToString(),
              (bool)reader["Textbook"],
              (int)reader["CertID"],
              reader["Method"].ToString(),
              reader["GrantBy"].ToString(),
              reader["DOCXFile"].ToString(),
              reader["DOCXHoFile"].ToString(),
              (bool)reader["Obsolete"],
              (int)reader["FacilityID"],
              (Convert.IsDBNull(reader["Course_Number"]) ? "" : reader["Course_Number"].ToString()),
                //(Convert.IsDBNull(reader["CeRef"]) ? "" : reader["CeRef"].ToString()),
                //(DateTime)(Convert.IsDBNull(reader["Release_Date"])
                //    ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Release_Date"]),
              (int)(Convert.IsDBNull(reader["Minutes"]) ? (int)0 : (int)reader["Minutes"]),
              (Convert.IsDBNull(reader["Audio_Ind"]) ? false : Convert.ToBoolean(reader["Audio_Ind"])),
              (bool)(Convert.IsDBNull(reader["Apn_Ind"]) ? false : (bool)reader["Apn_Ind"]),
              (Convert.IsDBNull(reader["Icn_Ind"]) ? false : Convert.ToBoolean(reader["Icn_Ind"])),
              (Convert.IsDBNull(reader["Jcaho_Ind"]) ? false : Convert.ToBoolean(reader["Jcaho_Ind"])),
              (Convert.IsDBNull(reader["Magnet_Ind"]) ? false : Convert.ToBoolean(reader["Magnet_Ind"])),
              (bool)reader["Active_Ind"],
              (Convert.IsDBNull(reader["Video_Ind"]) ? false : Convert.ToBoolean(reader["Video_Ind"])),
              (Convert.IsDBNull(reader["Online_Ind"]) ? false : Convert.ToBoolean(reader["Online_Ind"])),
              (Convert.IsDBNull(reader["Ebp_Ind"]) ? false : Convert.ToBoolean(reader["Ebp_Ind"])),
              (Convert.IsDBNull(reader["Ccm_Ind"]) ? false : Convert.ToBoolean(reader["Ccm_Ind"])),
              (Convert.IsDBNull(reader["Avail_Ind"]) ? false : Convert.ToBoolean(reader["Avail_Ind"])),
              reader["Cert_Cerp"].ToString(),
              reader["Ref_Html"].ToString(),
              (int)reader["AlterID"],
              (int)(Convert.IsDBNull(reader["Pass_Score"]) ? (int)0 : (int)reader["Pass_Score"]),
              reader["Subtitle"].ToString(),
                //(decimal)(Convert.IsDBNull(reader["Cost"]) ? (decimal)0 : (decimal)reader["Cost"]),
              reader["Topic_Type"].ToString(),
              reader["Img_Name"].ToString(),
              reader["Img_Credit"].ToString(),
              reader["Img_Caption"].ToString(),
              reader["Accreditation"].ToString(),
              (int)(Convert.IsDBNull(reader["Views"]) ? (int)0 : (int)reader["Views"]),
              (int)(Convert.IsDBNull(reader["PrimaryViews"]) ? (int)0 : (int)reader["PrimaryViews"]),
              (int)(Convert.IsDBNull(reader["PageReads"]) ? (int)0 : (int)reader["PageReads"]),
              (Convert.IsDBNull(reader["Featured"]) ? false : Convert.ToBoolean(reader["Featured"])),
              reader["rev"].ToString(),
              reader["urlmark"].ToString(),
              (decimal)(Convert.IsDBNull(reader["offline_Cost"]) ? (decimal)0 : (decimal)reader["offline_Cost"]),
              (decimal)(Convert.IsDBNull(reader["online_Cost"]) ? (decimal)0 : (decimal)reader["online_Cost"]),
              (bool)(Convert.IsDBNull(reader["notest"]) ? false : (bool)reader["notest"]),
              (bool)(Convert.IsDBNull(reader["uce_Ind"]) ? false : (bool)reader["uce_Ind"]),
              (Convert.IsDBNull(reader["shortname"]) ? "" : reader["shortname"].ToString()),
              (int)(Convert.IsDBNull(reader["categoryid"]) ? (int)0 : (int)reader["categoryid"]),
              (bool)(Convert.IsDBNull(reader["hold_ind"]) ? false : (bool)reader["hold_ind"]),
              (decimal)(Convert.IsDBNull(reader["rating_avg"]) ? (decimal)0 : (decimal)reader["rating_avg"]),
              (int)(Convert.IsDBNull(reader["rating_num_comment"]) ? (int)0 : (int)reader["rating_num_comment"]),
              (int)(Convert.IsDBNull(reader["rating_total"]) ? (int)0 : (int)reader["rating_total"]),
              (bool)(Convert.IsDBNull(reader["prepaid_ind"]) ? false : (bool)reader["prepaid_ind"]),
              reader["topic_summary"].ToString(),
              Convert.IsDBNull(reader["cme_ind"]) ? false : (bool)reader["cme_ind"],
              Convert.IsDBNull(reader["cme_sponsor_ind"]) ? false : (bool)reader["cme_sponsor_ind"],
              reader["cme_sponsor_name"].ToString(),
              reader["topic_hourlong"].ToString(),
              reader["topic_hourshort"].ToString(),
              (Convert.IsDBNull(reader["Webinar_start"]) ? null : (DateTime?)reader["Webinar_start"]),
              (Convert.IsDBNull(reader["Webinar_end"]) ? null : (DateTime?)reader["Webinar_end"]));

            return Topic;
        }

        /// <summary>
        /// Returns a collection of TopicInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<TopicInfo> GetTopicCollectionFromReader(IDataReader reader)
        {
            return GetTopicCollectionFromReader(reader, true);
        }
        protected virtual List<TopicInfo> GetTopicCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TopicInfo> Topics = new List<TopicInfo>();
            while (reader.Read())
                Topics.Add(GetTopicFromReader(reader, readMemos));
            return Topics;
        }





        /// <summary>
        /// Returns a new CheckBoxListTopicInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual CheckBoxListTopicInfo GetCheckBoxListTopicFromReader(IDataReader reader)
        {
            CheckBoxListTopicInfo CheckBoxListTopic = new CheckBoxListTopicInfo(
              (int)reader["TopicID"],
              reader["TopicName"].ToString(),
              (bool)reader["Selected"]);

            return CheckBoxListTopic;
        }

        /// <summary>
        /// Returns a collection of CheckBoxListTopicInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<CheckBoxListTopicInfo> GetCheckBoxListTopicCollectionFromReader(IDataReader reader)
        {
            List<CheckBoxListTopicInfo> CheckBoxListTopics = new List<CheckBoxListTopicInfo>();
            while (reader.Read())
                CheckBoxListTopics.Add(GetCheckBoxListTopicFromReader(reader));
            return CheckBoxListTopics;
        }
        #endregion

        public List<TopicInfo> GetTopicInfosFromLookupDtos(List<LookupDTO> dtos)
        {
            List<TopicInfo> topicinfos = new List<TopicInfo>();
            foreach (LookupDTO dto in dtos)
            {
                TopicInfo topicinfo = new TopicInfo(Convert.ToInt32(dto.ItemValue), dto.ItemText);
                topicinfos.Add(topicinfo);
            }
            return topicinfos;
        }


        public string CleanTopicName(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        
        }
    }
}
#endregion