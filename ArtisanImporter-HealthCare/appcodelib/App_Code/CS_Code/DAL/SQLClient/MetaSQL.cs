﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Data.SqlClient;
/// <summary>
/// Summary description for MetaSQL
/// </summary>
/// 
#region MeatInfo
namespace PearlsReview.DAL
{
    public class MetaInfo
    {
        public MetaInfo(){}
        public MetaInfo(int metaID, string path, string title, string description, string keyword, string h1)
        {
            this.metaID = metaID;
            this.path = path;
            this.title = title;
            this.description = description;
            this.keyword = keyword;
            this.h1 = h1;
        }

        public int metaID
        {
            get;
            set;
        }
        public string path
        {
            get;
            set;
        }
        public string title
        {
            get;
            set;
        }
        public string description
        {
            get;
            set;
        }
        public string keyword
        {
            get;
            set;
        }
        public string h1
        {
            get;
            set;
        }
}
}
#endregion
#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {    
		//
		// TODO: Add constructor logic here
        //
        #region SQLPRPROVIDER
        /// <summary>
        /// Retrieves the Topic with the specified ID
        /// </summary>
        public  MetaInfo GetMetaByPath(string Path)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from Meta" +
                                             " where path=@path", cn);
                cmd.Parameters.Add("@path", SqlDbType.VarChar).Value = Path;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMetaFromReader(reader, true);
                else
                    return null;
            }
        }
    
#endregion

        #region PRprovider
        /// <summary>
        /// Returns a new CategoryLinkInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual MetaInfo GetMetaFromReader(IDataReader reader)
        {
            return GetMetaFromReader(reader, true);
        }
        protected virtual MetaInfo GetMetaFromReader(IDataReader reader, bool readMemos)
        {
            MetaInfo Meta = new MetaInfo(
              (int)reader["metaID"],
              reader["path"].ToString(),
                reader["title"].ToString(),
                  reader["description"].ToString(),
                    reader["keyword"].ToString(),
                      reader["h1"].ToString());


            return Meta;
        }

        #endregion
    }
}
#endregion

