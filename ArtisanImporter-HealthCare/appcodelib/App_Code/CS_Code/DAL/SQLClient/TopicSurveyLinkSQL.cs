﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Alex Chen
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using System.Collections.Generic;
using System.Data.SqlClient;

#region TopicSurveyLinkInfo

namespace PearlsReview.DAL
{
    public class TopicSurveyLinkInfo
    {
        private int _tslID = 0;
        public int TSLID
        {
            get { return _tslID; }
            set { _tslID = value; }
        }

        private int _topicID = 0;
        public int TopicID
        {
            get { return _topicID; }
            set { _topicID = value; }
        }

        private int _surveyID = 0;
        public int SurveyID
        {
            get { return _surveyID; }
            set { _surveyID = value; }
        }

        private string _surveyType = "";
        public string SurveyType
        {
            get { return _surveyType; }
            set { _surveyType = value; }
        }

        public TopicSurveyLinkInfo(int tslID, int topicID, int surveyID, string surveyType)
        {
            this.TSLID = tslID;
            this.TopicID = topicID;
            this.SurveyID = surveyID;
            this.SurveyType = surveyType;
        }
    }


}

#endregion TopicSurveyLinkInfo

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        public int InsertTopicSurveyLink(TopicSurveyLinkInfo topicSurveyLink)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into TopicSurveyLink " +
                    "(TopicID, " +
                    "SurveyID, " +
                    "SurveyType) " +
                    "VALUES (" +
                    "@TopicID, " +
                    "@SurveyID, " +
                    "@SurveyType) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = topicSurveyLink.TopicID;
                cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = topicSurveyLink.SurveyID;
                cmd.Parameters.Add("@SurveyType", SqlDbType.VarChar).Value = topicSurveyLink.SurveyType;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;
            }
        }

        public bool UpdateTopicSurveyLinkByTopicIDAndSurveyType(int surveyID, int topicID, string surveyType)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update TopicSurveyLink set " + 
                    "SurveyID = @SurveyID " +
                    "where TopicID = @TopicID " +
                    "and SurveyType = @SurveyType ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = topicID;
                cmd.Parameters.Add("@SurveyID", SqlDbType.Int).Value = surveyID;
                cmd.Parameters.Add("@SurveyType", SqlDbType.VarChar).Value = surveyType;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        public TopicSurveyLinkInfo GetTopicSurveyLinkByTopicIDAndSurveyType(int topicID, string surveyType)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * " +
                        "from TopicSurveyLink where TopicID=@TopicID and SurveyType = @SurveyType ", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = topicID;
                cmd.Parameters.Add("@SurveyType", SqlDbType.VarChar).Value = surveyType;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetTopicSurveyLinkFromReader(reader, true);
                else
                    return null;
            }
        }

        protected virtual TopicSurveyLinkInfo GetTopicSurveyLinkFromReader(IDataReader reader, bool readMemos)
        {
            TopicSurveyLinkInfo topicSurveyLink = new TopicSurveyLinkInfo(
              (int)reader["TSLID"],
              (int)reader["TopicID"],
              (int)reader["SurveyID"],
              reader["SurveyType"].ToString());

            return topicSurveyLink;
        }

        protected virtual List<TopicSurveyLinkInfo> GetTopicSurveyLinkCollectionFromReader(IDataReader reader)
        {
            return GetTopicSurveyLinkCollectionFromReader(reader, true);
        }
        protected virtual List<TopicSurveyLinkInfo> GetTopicSurveyLinkCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<TopicSurveyLinkInfo> topicSurveyLinks = new List<TopicSurveyLinkInfo>();
            while (reader.Read())
                topicSurveyLinks.Add(GetTopicSurveyLinkFromReader(reader, readMemos));
            return topicSurveyLinks;
        }
    }
}

#endregion SQLPRProvider and PRProvider