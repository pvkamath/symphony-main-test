﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region MicrositeAdsInfo

namespace PearlsReview.DAL
{

    /// <summary> 
    /// Summary description for MicrositeDomainInfo
    /// Bhaskar N
    /// </summary>
    public class MicrositeAdsInfo
    {

        public MicrositeAdsInfo() { }


        public MicrositeAdsInfo(int id, int msid, string file_name, string ads_url, string ads_desc, int ads_order, string ads_type, string ads_html)
        {
            this.ID = id;
            this.Msid = msid;
            this.File_name = file_name;
            this.Ads_url = ads_url;
            this.Ads_desc = ads_desc;
            this.Ads_order = ads_order;
            this.Ads_type = ads_type;
            this.Ads_html = ads_html;
        }

        private int _id = 0;
        public int ID
        {
            get { return _id; }
            protected set { _id = value; }
        }
        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }
        private string _file_name = "";
        public string File_name
        {
            get { return _file_name; }
            private set { _file_name = value; }
        }
        private string _ads_url = "";
        public string Ads_url
        {
            get { return _ads_url; }
            private set { _ads_url = value; }
        }
        private string _ads_desc = "";
        public string Ads_desc
        {
            get { return _ads_desc; }
            private set { _ads_desc = value; }
        }
        private int _ads_order = 0;
        public int Ads_order
        {
            get { return _ads_order; }
            protected set { _ads_order = value; }
        }
        private string _ads_type = "";
        public string Ads_type
        {
            get { return _ads_type; }
            private set { _ads_type = value; }
        }
        private string _ads_html = "";
        public string Ads_html
        {
            get { return _ads_html; }
            private set { _ads_html = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // Methods that work with MicrositeDomain
        // Bhaskar N

        /// <summary>
        /// Retrieves all MicrositeDomains
        /// </summary>
        public List<MicrositeAdsInfo> GetMicrositeAds(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from MicrositeAds ";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetMicrositeAdsCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public List<MicrositeAdsInfo> GetMicrositeAdsByMsid(int Msid, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand;
                cSQLCommand = "SELECT * from MicrositeAds where msid=" + Msid;

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetMicrositeAdsCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public MicrositeAdsInfo GetMicrositeAdsByID(int id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositeAds where id = @id", cn);

                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositeAdsFromReader(reader, true);
                else
                    return null;
            }
        }
        /// <summary>
        /// Retrieves the MicrositeDomain with the specified ID
        /// </summary>
        public MicrositeAdsInfo GetMicrositeAdsByID(int msid, string file_name)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from MicrositeAds where msid=@msid and (file_name = @file_name or ads_desc=@file_name)", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cmd.Parameters.Add("@file_name", SqlDbType.VarChar).Value = file_name;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetMicrositeAdsFromReader(reader, true);
                else
                    return null;
            }
        }
        public int GetMicrositeAdsMaxOrder(int msid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select count(*) as max " +
                        "from MicrositeAds where msid=@msid", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return int.Parse(reader["max"].ToString());
                else
                    return 0;
            }
        }
        /// <summary>
        /// Inserts a new Discount
        /// </summary>
        public int InsertMicrositeAds(MicrositeAdsInfo MicrositeAds)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into MicrositeAds " +
                  "(msid, " +
                  "file_name, " +
                  "ads_url, " +
                  "ads_desc, " +
                  "ads_type, " +
                  "ads_html, " +
                  "ads_order) VALUES (@msid, @file_name, @ads_url, @ads_desc, @ads_type, @ads_html, @ads_order)", cn);

                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeAds.Msid;
                cmd.Parameters.Add("@file_name", SqlDbType.VarChar).Value = MicrositeAds.File_name;
                cmd.Parameters.Add("@ads_url", SqlDbType.VarChar).Value = MicrositeAds.Ads_url;
                cmd.Parameters.Add("@ads_desc", SqlDbType.VarChar).Value = MicrositeAds.Ads_desc;
                cmd.Parameters.Add("@ads_type", SqlDbType.VarChar).Value = MicrositeAds.Ads_type;
                cmd.Parameters.Add("@ads_html", SqlDbType.VarChar).Value = MicrositeAds.Ads_html;
                cmd.Parameters.Add("@ads_order", SqlDbType.Int).Value = MicrositeAds.Ads_order;

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int i = cmd.ExecuteNonQuery();

                return i;
            }
        }
        /// <summary>
        /// Updates a Discount
        /// </summary>
        public bool UpdateMicrositeAds(MicrositeAdsInfo MicrositeAds)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update MicrositeAds set " +
              "msid = @msid, " +
              "file_name = @file_name, " +
              "ads_url = @ads_url, " +
              "ads_order = @ads_order, " +
              "ads_type = @ads_type, " +
              "ads_html = @ads_html, " +
              "ads_desc = @ads_desc where id = @id", cn);

                cmd.Parameters.Add("@id", SqlDbType.Int).Value = MicrositeAds.ID;
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = MicrositeAds.Msid;
                cmd.Parameters.Add("@file_name", SqlDbType.VarChar).Value = MicrositeAds.File_name;
                cmd.Parameters.Add("@ads_url", SqlDbType.VarChar).Value = MicrositeAds.Ads_url;
                cmd.Parameters.Add("@ads_desc", SqlDbType.VarChar).Value = MicrositeAds.Ads_desc;
                cmd.Parameters.Add("@ads_type", SqlDbType.VarChar).Value = MicrositeAds.Ads_type;
                cmd.Parameters.Add("@ads_html", SqlDbType.VarChar).Value = MicrositeAds.Ads_html;
                cmd.Parameters.Add("@ads_order", SqlDbType.Int).Value = MicrositeAds.Ads_order;

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a Discount
        /// </summary>
        public bool DeleteMicrositeAds(int id)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from MicrositeAds where id = @id", cn);
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider
        /////////////////////////////////////////////////////////
        // methods that work with Discount  

        /// <summary>
        /// Returns a new DiscountInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual MicrositeAdsInfo GetMicrositeAdsFromReader(IDataReader reader)
        {
            return GetMicrositeAdsFromReader(reader, true);
        }
        protected virtual MicrositeAdsInfo GetMicrositeAdsFromReader(IDataReader reader, bool readMemos)
        {
            MicrositeAdsInfo MicrositeAds = new MicrositeAdsInfo(
                  (int)reader["id"],
                  (int)reader["msid"],
                  reader["file_name"].ToString(),
                  reader["ads_url"].ToString(),
                  reader["ads_desc"].ToString(),
                  String.IsNullOrEmpty(reader["ads_order"].ToString()) ? 0 : int.Parse(reader["ads_order"].ToString()),
                  reader["ads_type"].ToString(),
                  reader["ads_html"].ToString());
            return MicrositeAds;
        }


        /// <summary>
        /// Returns a collection of DiscountInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<MicrositeAdsInfo> GetMicrositeAdsCollectionFromReader(IDataReader reader)
        {
            return GetMicrositeAdsCollectionFromReader(reader, true);
        }
        protected virtual List<MicrositeAdsInfo> GetMicrositeAdsCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<MicrositeAdsInfo> MicrositeAds = new List<MicrositeAdsInfo>();
            while (reader.Read())
                MicrositeAds.Add(GetMicrositeAdsFromReader(reader, readMemos));
            return MicrositeAds;
        }

        #endregion

    }
}
#endregion

