﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;
using PearlsReview.BLL;


#region OrderItemInfo

namespace PearlsReview.DAL
{

    public class OrderItemInfo
    {
        public OrderItemInfo()
        {

        }

        public OrderItemInfo(int OrderItemID, int OrderID, int TopicID, Decimal Cost, string Media_Type,
            int Quantity, int DiscountID, bool Optin_Ind)
        {
            this.OrderItemID = OrderItemID;
            this.OrderID = OrderID;
            this.TopicID = TopicID;
            this.Cost = Cost;
            this.Media_Type = Media_Type;
            this.Quantity = Quantity;
            this.DiscountID = DiscountID;
            this.Optin_Ind = Optin_Ind;
            this.TopicName = Topic.GetTopicByID(TopicID).TopicName;

        }

        private string _topicName = "";
        public string TopicName
        {
            get { return _topicName; }
            set { _topicName = value; }
        }

        private int _OrderItemID = 0;
        public int OrderItemID
        {
            get { return _OrderItemID; }
            private set { _OrderItemID = value; }
        }

        private int _OrderID = 0;
        public int OrderID
        {
            get { return _OrderID; }
            private set { _OrderID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

        private decimal _Cost = 0;
        public decimal Cost
        {
            get { return _Cost; }
            set { _Cost = value; }
        }

        private string _Media_Type = "";
        public string Media_Type
        {
            get { return _Media_Type.ToLower(); }
            private set { _Media_Type = value.ToLower(); }
        }

        private int _Quantity = 0;
        public int Quantity
        {
            get { return _Quantity; }
            private set { _Quantity = value; }
        }

        private int _DiscountID = 0;
        public int DiscountID
        {
            get { return _DiscountID; }
            private set { _DiscountID = value; }
        }

        private bool _Optin_Ind = false;
        public bool Optin_Ind
        {
            get { return _Optin_Ind; }
            set { _Optin_Ind = value; }
        }

       

 }
    
}
 #endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        public List<OrderItemInfo> GetOrderDetails(int OrderID)
        {

            List<OrderItemInfo> ordInfo = new List<OrderItemInfo>();

            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                string cSQLCommand = "SELECT * FROM  OrderItem  WHERE    orderid = @OrderID";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@OrderID", SqlDbType.Int).Value = OrderID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    ordInfo.Add(GetOrderItemFromReader(reader, true));
                }
            }
            return ordInfo;

        }


        /// <summary>
        /// Inserts a new Orders
        /// </summary>
        public  int InsertOrderItem(OrderItemInfo OrderItem)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Orderitem values ( " +
              " @orderid, " +
              " @topicid, " +
              " dbo.udf_cartprice(@cost), " +
              " @media_type, " +
              " @quantity, " +
              " @discountid, " +
              " @optin_ind ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@orderid", SqlDbType.Int).Value = OrderItem.OrderID;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = OrderItem.TopicID;
                cmd.Parameters.Add("@cost", SqlDbType.Decimal).Value = OrderItem.Cost;
                cmd.Parameters.Add("@media_type", SqlDbType.VarChar).Value = OrderItem.Media_Type == null ? DBNull.Value as Object : OrderItem.Media_Type as Object;
                cmd.Parameters.Add("@quantity", SqlDbType.Int).Value = OrderItem.Quantity;
                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = OrderItem.DiscountID;
                cmd.Parameters.Add("@optin_ind", SqlDbType.Bit).Value = OrderItem.Optin_Ind;
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Inserts a new Orders
        /// </summary>
        public int InsertMicrositeOrderItem(OrderItemInfo OrderItem)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Orderitem values ( " +
              " @orderid, " +
              " @topicid, " +
              " dbo.udf_MS_cartprice(@cost), " +
              " @media_type, " +
              " @quantity, " +
              " @discountid, " +
              " @optin_ind ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@orderid", SqlDbType.Int).Value = OrderItem.OrderID;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = OrderItem.TopicID;
                cmd.Parameters.Add("@cost", SqlDbType.Decimal).Value = OrderItem.Cost;
                cmd.Parameters.Add("@media_type", SqlDbType.VarChar).Value = OrderItem.Media_Type == null ? DBNull.Value as Object : OrderItem.Media_Type as Object;
                cmd.Parameters.Add("@quantity", SqlDbType.Int).Value = OrderItem.Quantity;
                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = OrderItem.DiscountID;
                cmd.Parameters.Add("@optin_ind", SqlDbType.Bit).Value = OrderItem.Optin_Ind;
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Inserts a new Orders
        /// </summary>
        public int InsertMicrositeProOrderItem(OrderItemInfo OrderItem)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Orderitem values ( " +
              " @orderid, " +
              " @topicid, " +
              " @cost, " +
              " @media_type, " +
              " @quantity, " +
              " @discountid, " +
              " @optin_ind ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@orderid", SqlDbType.Int).Value = OrderItem.OrderID;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = OrderItem.TopicID;
                cmd.Parameters.Add("@cost", SqlDbType.Decimal).Value = OrderItem.Cost;
                cmd.Parameters.Add("@media_type", SqlDbType.VarChar).Value = OrderItem.Media_Type == null ? DBNull.Value as Object : OrderItem.Media_Type as Object;
                cmd.Parameters.Add("@quantity", SqlDbType.Int).Value = OrderItem.Quantity;
                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = OrderItem.DiscountID;
                cmd.Parameters.Add("@optin_ind", SqlDbType.Bit).Value = OrderItem.Optin_Ind;
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Inserts a pay now new Orders
        /// </summary>
        public int InsertMicrositePayNowOrderItem(OrderItemInfo OrderItem)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Orderitem values ( " +
              " @orderid, " +
              " @topicid, " +
              " @cost, " +
              " @media_type, " +
              " @quantity, " +
              " @discountid, " +
              " @optin_ind ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@orderid", SqlDbType.Int).Value = OrderItem.OrderID;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = OrderItem.TopicID;
                cmd.Parameters.Add("@cost", SqlDbType.Decimal).Value = OrderItem.Cost;
                cmd.Parameters.Add("@media_type", SqlDbType.VarChar).Value = OrderItem.Media_Type == null ? DBNull.Value as Object : OrderItem.Media_Type as Object;
                cmd.Parameters.Add("@quantity", SqlDbType.Int).Value = OrderItem.Quantity;
                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = OrderItem.DiscountID;
                cmd.Parameters.Add("@optin_ind", SqlDbType.Bit).Value = OrderItem.Optin_Ind;
                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        public decimal GetPriceByCartId(int CartId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select dbo.udf_cartprice(@cost) ", cn);
                cmd.Parameters.Add("@cost", SqlDbType.Int).Value = CartId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                if (reader.Read())
                {
                    return (decimal)reader[0];
                }
                else
                {
                    return -1;
                }


            }
        }
        public decimal GetMSPriceByCartId(int CartId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select dbo.udf_MS_cartprice(@cost) ", cn);
                cmd.Parameters.Add("@cost", SqlDbType.Int).Value = CartId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd);
                if (reader.Read())
                {
                    return (decimal)reader[0];
                }
                else
                {
                    return -1;
                }


            }
        }
        #endregion 
        #region PRPRovider

        protected virtual OrderItemInfo GetOrderItemFromReader(IDataReader reader)
        {
            return GetOrderItemFromReader(reader, true);
        }
        protected virtual OrderItemInfo GetOrderItemFromReader(IDataReader reader, bool readMemos)
        {
            OrderItemInfo Orders = new OrderItemInfo(
                  (int)(Convert.IsDBNull(reader["OrderItemID"]) ? (int)0 : (int)reader["OrderItemID"]),
                  (int)(Convert.IsDBNull(reader["OrderID"]) ? (int)0 : (int)reader["OrderID"]),
                  (int)(Convert.IsDBNull(reader["TopicID"]) ? (int)0 : (int)reader["TopicID"]),
                  (decimal)(Convert.IsDBNull(reader["Cost"]) ? (decimal)0 : (decimal)reader["Cost"]),
                  Convert.ToString(reader["Media_Type"].ToString()),
                  (int)(Convert.IsDBNull(reader["Quantity"]) ? (int)0 : (int)reader["Quantity"]),
                  (int)(Convert.IsDBNull(reader["DiscountID"]) ? (int)0 : (int)reader["DiscountID"]),
                  (bool)reader["Optin_Ind"]);

            return Orders;
        }
        #endregion
    }
}
#endregion
   