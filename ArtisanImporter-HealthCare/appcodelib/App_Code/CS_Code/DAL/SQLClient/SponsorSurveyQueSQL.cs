﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region SponsorSurveyQueInfo

namespace PearlsReview.DAL
{

    /// <summary>
    /// Summary description for SponsorSurveyQueSQL
    /// </summary>
    public class SponsorSurveyQueInfo
    {
        public SponsorSurveyQueInfo() { }

        public SponsorSurveyQueInfo(int questionid, string survey_text, bool optional_ind)
        {
            this.questionid = questionid;
            this.survey_text = survey_text;
            this.optional_ind = optional_ind;
        }

        private int _questionid = 0;
        public int questionid
        {
            get { return _questionid; }
            protected set { _questionid = value; }
        }

        private string _survey_text = "";
        public string survey_text
        {
            get { return _survey_text; }
            private set { _survey_text = value; }
        }

        private bool _optional_ind = true;
        public bool optional_ind
        {
            get { return _optional_ind; }
            private set { _optional_ind = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider 

        /////////////////////////////////////////////////////////
        // Methods that work with SponsorSurveyQue
        // Bhaskar N

        /// <summary>
        /// Retrieves all SponsorSurveyQue
        /// </summary>
        public  List<SponsorSurveyQueInfo> GetSponsorSurveyQues(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * FROM SponsorSurveyQue";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetSponsorSurveyQueCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all SponsorSurveyQue
        /// </summary>
        public  List<SponsorSurveyQueInfo> GetSponsorSurveyQuesByDiscountId(string cSortExpression, int discountid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = "select * " +
                //        "FROM GetSponsorSurveyQue";

                string cSQLCommand = "SELECT SponsorSurveyQue.* " +
                        //"SponsorSurveyQue.questionid, " +
                        //"SponsorSurveyQue.survey_text, " +
                        //"SponsorSurveyQue.optional_ind " +
                    "FROM SponsorSurveyQue " +
                    "JOIN SponsorSurveys ON SponsorSurveys.QuestionID=SponsorSurveyQue.QuestionID " +
                    "WHERE SponsorSurveys.discountid=@discountid ";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;

                cn.Open();
                return GetSponsorSurveyQueCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the SponsorSurveyQue with the specified ID
        /// </summary>
        public  SponsorSurveyQueInfo GetSponsorSurveyQueByID(int questionid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from SponsorSurveyQue where questionid=@questionid", cn);

                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSponsorSurveyQueFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Inserts a new SponsorSurveyQue
        /// </summary>
        public  int InsertSponsorSurveyQue(SponsorSurveyQueInfo SponsorSurveyQue)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into SponsorSurveyQue " +
                  "(survey_text , " +
                  "optional_ind ) " +
                  "VALUES (" +
                  "@survey_text, " +
                  "@optional_ind ) SET @ID = SCOPE_IDENTITY()", cn);

                //cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = SponsorSurveyQue.questionid;
                cmd.Parameters.Add("@survey_text", SqlDbType.VarChar).Value = SponsorSurveyQue.survey_text;
                cmd.Parameters.Add("@optional_ind", SqlDbType.Bit).Value = SponsorSurveyQue.optional_ind;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }


        /// <summary>
        /// Updates a SponsorSurveyQue
        /// </summary>
        public  bool UpdateSponsorSurveyQue(SponsorSurveyQueInfo SponsorSurveyQue)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update SponsorSurveyQue set " +
                  "survey_text = @survey_text, " +
                  "optional_ind = @optional_ind " +
                  "where questionid = @questionid ", cn);

                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = SponsorSurveyQue.questionid;
                cmd.Parameters.Add("@survey_text", SqlDbType.VarChar).Value = SponsorSurveyQue.survey_text;
                cmd.Parameters.Add("@optional_ind", SqlDbType.Bit).Value = SponsorSurveyQue.optional_ind;


                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a SponsorSurveyQue
        /// </summary>
        public  bool DeleteSponsorSurveyQue(int questionid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from SponsorSurveyQue where questionid=@questionid", cn);
                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider 


        /// <summary>
        /// Returns a new SponsorSurveyQueInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual SponsorSurveyQueInfo GetSponsorSurveyQueFromReader(IDataReader reader)
        {
            return GetSponsorSurveyQueFromReader(reader, true);
        }
        protected virtual SponsorSurveyQueInfo GetSponsorSurveyQueFromReader(IDataReader reader, bool readMemos)
        {
            SponsorSurveyQueInfo SponsorSurveyQue = new SponsorSurveyQueInfo(
                  (int)reader["questionid"],
                  reader["survey_text"].ToString(),
                  (bool)reader["optional_ind"]);
            return SponsorSurveyQue;
        }


        /// <summary>
        /// Returns a collection of SponsorSurveyQueInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<SponsorSurveyQueInfo> GetSponsorSurveyQueCollectionFromReader(IDataReader reader)
        {
            return GetSponsorSurveyQueCollectionFromReader(reader, true);
        }
        protected virtual List<SponsorSurveyQueInfo> GetSponsorSurveyQueCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<SponsorSurveyQueInfo> SponsorSurveyQues = new List<SponsorSurveyQueInfo>();
            while (reader.Read())
                SponsorSurveyQues.Add(GetSponsorSurveyQueFromReader(reader, readMemos));
            return SponsorSurveyQues;
        }

        #endregion
    }
}
#endregion