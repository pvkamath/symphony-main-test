﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;


#region InitiativeInfo

namespace PearlsReview.DAL
{
    public class InitiativeInfo
    {
        public InitiativeInfo() { }

        public InitiativeInfo(int InitID, string InitName, DateTime StartDate, DateTime EndDate)
        {
            this.InitID = InitID;
            this.InitName = InitName;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
        }

        private int _InitID = 0;
        public int InitID
        {
            get { return _InitID; }
            protected set { _InitID = value; }
        }

        private string _InitName = "";
        public string InitName
        {
            get { return _InitName; }
            private set { _InitName = value; }
        }

        private DateTime _StartDate;
        public DateTime StartDate
        {
            get { return _StartDate; }
            private set { _StartDate = value; }
        }

        private DateTime _EndDate;
        public DateTime EndDate
        {
            get { return _EndDate; }
            private set { _EndDate = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with Initiative



        /// <summary>
        /// Retrieves all Initiative
        /// </summary>
        public List<InitiativeInfo> GetInitiative(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from Initiative";

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetInitiativeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the Initiative with the specified ID
        /// </summary>
        public InitiativeInfo GetInitiativeByID(int InitiativeID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from Initiative where initid=@initid", cn);
                cmd.Parameters.Add("@initid", SqlDbType.Int).Value = InitiativeID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetInitiativeFromReader(reader, true);
                else
                    return null;
            }
        }
        public List<InitiativeInfo> GetUnSelectedInitiativeBySponsorID(int SponsorID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                //string cSQLCommand = String.Format("select * from Initiative where initid not in (select distinct initid from SponsorList where sponsorid = {0})", SponsorID);
                string cSQLCommand = "select * from Initiative";
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetInitiativeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        public List<InitiativeInfo> GetInitiativeBySponsorID(int SponsorID, string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = String.Format("select * from Initiative where initid in (select distinct initid from SponsorList where sponsorid = {0})", SponsorID);

                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetInitiativeCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Deletes a Initiative
        /// </summary>
        public bool DeleteInitiative(int InitiativeID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from Initiative where initid=@initid", cn);
                cmd.Parameters.Add("@initid", SqlDbType.Int).Value = InitiativeID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);

                SqlCommand cmd1 = new SqlCommand("update Discount set initid = null where initid=@initid", cn);
                cmd1.Parameters.Add("@initid", SqlDbType.Int).Value = InitiativeID;
                ExecuteNonQuery(cmd1);

                SqlCommand cmd2 = new SqlCommand("delete from SponsorList where initid=@initid", cn);
                cmd2.Parameters.Add("@initid", SqlDbType.Int).Value = InitiativeID;
                ExecuteNonQuery(cmd1);

                return (ret == 1);


            }
        }

        /// <summary>
        /// Inserts a new Initiative
        /// </summary>
        public int InsertInitiative(InitiativeInfo Initiative)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into Initiative " +
                "(initname, " +
                "startdate, " +
                "enddate ) " +
                "VALUES (" +
                 "@initname, " +
                "@startdate, " +
                "@enddate ) SET @initid = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@initname", SqlDbType.VarChar).Value = Initiative.InitName;
                cmd.Parameters.Add("@startdate", SqlDbType.DateTime).Value = Initiative.StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.DateTime).Value = Initiative.EndDate;

                SqlParameter IDParameter = new SqlParameter("@initid", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a Initiative
        /// </summary>
        public bool UpdateInitiative(InitiativeInfo Initiative)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update Initiative set " +
              "initname = @initname, " +
              "startdate = @startdate, " +
              "enddate = @enddate " +
              "where initid = @initid ", cn);

                cmd.Parameters.Add("@initid", SqlDbType.Int).Value = Initiative.InitID;
                cmd.Parameters.Add("@initname", SqlDbType.Text).Value = Initiative.InitName;
                cmd.Parameters.Add("@startdate", SqlDbType.DateTime).Value = Initiative.StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.DateTime).Value = Initiative.EndDate;

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new InitiativeInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual InitiativeInfo GetInitiativeFromReader(IDataReader reader)
        {
            return GetInitiativeFromReader(reader, true);
        }
        protected virtual InitiativeInfo GetInitiativeFromReader(IDataReader reader, bool readMemos)
        {
            InitiativeInfo Initiative = new InitiativeInfo(
              (int)reader["initid"],
              reader["initname"].ToString(),
              DateTime.Parse(reader["startdate"].ToString()),
              DateTime.Parse(reader["enddate"].ToString()));

            return Initiative;
        }

        /// <summary>
        /// Returns a collection of InitiativeInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<InitiativeInfo> GetInitiativeCollectionFromReader(IDataReader reader)
        {
            return GetInitiativeCollectionFromReader(reader, true);
        }
        protected virtual List<InitiativeInfo> GetInitiativeCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<InitiativeInfo> Initiative = new List<InitiativeInfo>();
            while (reader.Read())
                Initiative.Add(GetInitiativeFromReader(reader, readMemos));
            return Initiative;
        }
        #endregion
    }
}
#endregion
