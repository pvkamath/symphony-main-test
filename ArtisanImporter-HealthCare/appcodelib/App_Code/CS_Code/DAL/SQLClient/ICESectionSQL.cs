﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region ICESectionInfo

namespace PearlsReview.DAL
{
    public class ICESectionInfo
    {
        public ICESectionInfo(int isid, int topicid, DateTime is_start, DateTime is_end, int maxenroll, int available_seat, DateTime? enroll_close, string lore_url,string ori_url, string is_comment, string facilitator, string facilitator_phone, string facilitator_email)
        {
            this.Isid = isid;            
            this.Topicid = topicid;
            this.Is_start = is_start;
            this.Is_end = is_end;
            this.Maxenroll = maxenroll;            
            this.Available_seat = available_seat;
            this.Enroll_close = enroll_close;
            this.Lore_url = lore_url;
            this.Ori_url = ori_url;
            this.Is_comment = is_comment;
            this.Facilitator = facilitator;
            this.Facilitator_phone = facilitator_phone;
            this.Facilitator_email = facilitator_email;
        }

        private int _isid = 0;
        public int Isid
        {
            get { return _isid; }
            set { _isid = value; }
        }
        private int _topicid = 0;
        public int Topicid
        {
            get { return _topicid; }
            set { _topicid = value; }
        }
        private DateTime _is_start;
        public DateTime Is_start
        {
            get { return _is_start; }
            set { _is_start = value; }
        }
        private DateTime _is_end;
        public DateTime Is_end
        {
            get { return _is_end; }
            set { _is_end = value; }
        }
        private int _maxenroll = 0;
        public int Maxenroll
        {
            get { return _maxenroll; }
            set { _maxenroll = value; }
        }   
        private int _available_seat = 0;
        public int Available_seat
        {
            get { return _available_seat; }
            set { _available_seat = value; }
        }
        private DateTime? _enroll_close;
        public DateTime? Enroll_close
        {
            get { return _enroll_close; }
            set { _enroll_close = value; }
        }
        private string _lore_url = "";
        public string Lore_url
        {
            get { return _lore_url; }
            set { _lore_url = value; }
        }
        private string _oir_url = "";
        public string Ori_url
        {
            get { return _oir_url; }
            set { _oir_url = value; }
        }
        private string _is_comment = "";
        public string Is_comment
        {
            get { return _is_comment; }
            set { _is_comment = value; }
        }
        private string _facilitator = "";
        public string Facilitator
        {
            get { return _facilitator; }
            set { _facilitator = value; }
        }

        private string _facilitator_phone = "";
        public string Facilitator_phone
        {
            get { return _facilitator_phone; }
            set { _facilitator_phone = value; }
        }

        private string _facilitator_email = "";
        public string Facilitator_email
        {
            get { return _facilitator_email; }
            set { _facilitator_email = value; }
        }  
   
    }

}

#endregion


#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    /// <summary>
    /// Summary description for SQLPRProvider
    /// </summary>        
    //public partial class SQLPRProvider : PRProvider
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        public int insertICESection(ICESectionInfo ICESection)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into ICESection " +
                    "(topicid, " +
                    "is_start, " +
                    "is_end, " +
                    "maxenroll, " +
                    "available_seat, " +
                    "enroll_close, " +
                    "lore_url, " +
                    "ori_url, " +
                    "is_comment," +
                    "facilitator, " +
                    "facilitator_phone, " +
                    "facilitator_email  ) "+
                     "VALUES " +                    
                    "(@topicid, " +
                    "@is_start, " +
                    "@is_end, " +
                    "@maxenroll, " +
                    "@available_seat, " +
                    "@enroll_close, " +
                    "@lore_url, " +
                    "@ori_url, " +
                    "@is_comment, " +
                    "@facilitator, " +
                    "@facilitator_phone, " +
                    "@facilitator_email ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = ICESection.Topicid;
                cmd.Parameters.Add("@is_start", SqlDbType.DateTime).Value = ICESection.Is_start;
                cmd.Parameters.Add("@is_end", SqlDbType.DateTime).Value = ICESection.Is_end;
                cmd.Parameters.Add("@maxenroll", SqlDbType.Int).Value = ICESection.Maxenroll;                                
                cmd.Parameters.Add("@available_seat", SqlDbType.Int).Value = ICESection.Available_seat;
                cmd.Parameters.Add("@enroll_close", SqlDbType.DateTime).Value = ICESection.Enroll_close;
                cmd.Parameters.Add("@lore_url", SqlDbType.VarChar).Value = ICESection.Lore_url;
                cmd.Parameters.Add("@ori_url", SqlDbType.VarChar).Value = ICESection.Ori_url;
                cmd.Parameters.Add("@is_comment", SqlDbType.VarChar).Value = ICESection.Is_comment;
                cmd.Parameters.Add("@facilitator", SqlDbType.VarChar).Value = ICESection.Facilitator;
                cmd.Parameters.Add("@facilitator_phone", SqlDbType.VarChar).Value = ICESection.Facilitator_phone;
                cmd.Parameters.Add("@facilitator_email", SqlDbType.VarChar).Value = ICESection.Facilitator_email;   

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                return (int)IDParameter.Value;
            }
        }

        public DataSet GetEnrolleesByTopicID(int topicid, ref string reported_date, ref string topicname)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_get_enrollees_by_topicid", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parameter1 =new SqlParameter("@topicid",topicid);
                parameter1.Direction=ParameterDirection.Input;
                cmd.Parameters.Add(parameter1);

                

                 cn.Open();

                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                ad.Fill(ds, "enrollees");

                foreach (DataRow dr in ds.Tables["enrollees"].Rows)
                {
                    reported_date = Convert.ToString(dr["reported_date"]);
                    topicname = Convert.ToString(dr["name"]);
                    string test= Convert.ToString(dr["is_start"]);
                    break;
                }

               
                cn.Close();
                return ds;
            }
        }

        public DataSet GetSeriesAboutToStart()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("sp_get_course_about_to_start",cn);
                cmd.CommandType = CommandType.StoredProcedure;

                cn.Open();

                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                ad.Fill(ds, "series");

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string deadline = Convert.ToString(dr["is_start"]);
                    DateTime dt = new DateTime();
                    if (deadline != "")
                    {
                        dt = Convert.ToDateTime(deadline);
                        dr["is_start"] = dt.ToString("d");
                    }
                }

                return ds;
            }

        }


        public bool updateICESection(ICESectionInfo ICESection)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update ICESection set " +
                    "topicid = @topicid, " +
                    "is_start = @is_start, " +
                    "is_end = @is_end, " +
                    "maxenroll = @maxenroll, " +                                        
                    "available_seat = @available_seat, " +
                    "enroll_close = @enroll_close, " +
                    "lore_url = @lore_url, " +
                    "ori_url = @ori_url, " +
                    "is_comment = @is_comment, " +
                    "facilitator = @facilitator, " +
                    "facilitator_phone = @facilitator_phone, " +
                    "facilitator_email = @facilitator_email " +
                    "where isid = @isid ", cn);
                                
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = ICESection.Topicid;
                cmd.Parameters.Add("@is_start", SqlDbType.DateTime).Value = ICESection.Is_start;
                cmd.Parameters.Add("@is_end", SqlDbType.DateTime).Value = ICESection.Is_end;
                cmd.Parameters.Add("@maxenroll", SqlDbType.Int).Value = ICESection.Maxenroll;
                cmd.Parameters.Add("@available_seat", SqlDbType.Int).Value = ICESection.Available_seat;
                cmd.Parameters.Add("@enroll_close", SqlDbType.DateTime).Value = ICESection.Enroll_close;
                cmd.Parameters.Add("@lore_url", SqlDbType.VarChar).Value = ICESection.Lore_url;
                cmd.Parameters.Add("@ori_url", SqlDbType.VarChar).Value = ICESection.Ori_url;
                cmd.Parameters.Add("@is_comment", SqlDbType.VarChar).Value = ICESection.Is_comment;                
                cmd.Parameters.Add("@isid", SqlDbType.Int).Value = ICESection.Isid;
                cmd.Parameters.Add("@facilitator", SqlDbType.VarChar).Value = ICESection.Facilitator;
                cmd.Parameters.Add("@facilitator_phone", SqlDbType.VarChar).Value = ICESection.Facilitator_phone;
                cmd.Parameters.Add("@facilitator_email", SqlDbType.VarChar).Value = ICESection.Facilitator_email;   
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public bool deleteICESection(int isid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from ICESection where isid = @isid", cn);
                cmd.Parameters.Add("@isid", SqlDbType.Int).Value = isid;
                cn.Open();
                int ret = cmd.ExecuteNonQuery();
                return (ret == 1);
            }
        }

        public ICESectionInfo GetICESectionByID(int isid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From ICESection Where isid = @isid", cn);
                cmd.Parameters.Add("@isid", SqlDbType.Int).Value = isid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetICESectionFromReader(reader);
                else
                    return null;
            }
        }

        public bool IsEnrolledICE(int isid, int userid) 
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From Test Where userid=@userid and topicid in (select topicid from ICESection where isid=@isid) and printed_date is not null and status in ('A','I')", cn);
                cmd.Parameters.Add("@isid", SqlDbType.Int).Value = isid;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return true;
                else
                    return false;
            }
        }

        public bool IsEnrolledOrCompletedICE(int isid, int userid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select * From Test Where userid=@userid and topicid in (select topicid from ICESection where isid=@isid) and printed_date is not null and status in ('A','I','C')", cn);
                cmd.Parameters.Add("@isid", SqlDbType.Int).Value = isid;
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return true;
                else
                    return false;
            }
        }

        public ICESectionInfo GetICESectionByTopicID(int topicid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("Select TOP 1 * From ICESection Where topicid = @topicid", cn);
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = topicid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetICESectionFromReader(reader);
                else
                    return null;
            }
        }       

        #endregion


        #region PRProvider

        protected virtual ICESectionInfo GetICESectionFromReader(IDataReader reader)
        {
            return GetICESectionFromReader(reader, true);
        }
        protected virtual ICESectionInfo GetICESectionFromReader(IDataReader reader, bool readMemos)
        {
                ICESectionInfo ICESection = new ICESectionInfo(
                Convert.IsDBNull(reader["isid"]) ? (int)0 : (int)reader["isid"],
                Convert.IsDBNull(reader["topicid"]) ? (int)0 : (int)reader["topicid"],
                Convert.IsDBNull(reader["is_start"]) ? DateTime.MinValue : Convert.ToDateTime(reader["is_start"]),
                Convert.IsDBNull(reader["is_end"]) ? DateTime.MinValue : Convert.ToDateTime(reader["is_end"]),
                Convert.IsDBNull(reader["maxenroll"]) ? (int)0 : (int)reader["maxenroll"],
                Convert.IsDBNull(reader["available_seat"]) ? (int)0 : (int)reader["available_seat"],
                Convert.IsDBNull(reader["enroll_close"]) ? DateTime.MinValue : Convert.ToDateTime(reader["enroll_close"]),
                Convert.ToString(reader["lore_url"].ToString()),
                Convert.ToString(reader["ori_url"].ToString()),
                Convert.ToString(reader["is_comment"].ToString()),
                Convert.ToString(reader["facilitator"].ToString()),
                Convert.ToString(reader["facilitator_phone"].ToString()),
                 Convert.ToString(reader["facilitator_email"].ToString())
                );
            return ICESection;
        }

        protected virtual List<ICESectionInfo> GetICESectionCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<ICESectionInfo> ICESectionList = new List<ICESectionInfo>();
            while (reader.Read())
            {
                ICESectionList.Add(GetICESectionFromReader(reader, readMemos));
            }
            return ICESectionList;
        }

        protected virtual List<ICESectionInfo> GetICESectionCollectionFromReader(IDataReader reader)
        {
            return GetICESectionCollectionFromReader(reader, true);
        }

        #endregion

    }
}
#endregion
