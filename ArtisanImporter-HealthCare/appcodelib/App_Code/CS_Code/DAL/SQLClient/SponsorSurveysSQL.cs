﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region SponsorSurveysInfo

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for SponsorSurveysInfo
    /// </summary>
    public class SponsorSurveysInfo
    {
        public SponsorSurveysInfo()
        {
        }

        public SponsorSurveysInfo(int discountid, int questionid)
        {
            this.discountid = discountid;
            this.questionid = questionid;
        }

        private int _discountid = 0;
        public int discountid
        {
            get { return _discountid; }
            protected set { _discountid = value; }
        }

        private int _questionid = 0;
        public int questionid
        {
            get { return _questionid; }
            protected set { _questionid = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // Methods that work with SponsorSurveys
        // Bhaskar N

        /// <summary>
        /// Retrieves all Discounts
        /// </summary>
        public  List<SponsorSurveysInfo> GetSponsorSurveys(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * FROM SponsorSurveys";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetSponsorSurveysCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all Discounts
        /// </summary>
        public  List<SponsorSurveysInfo> GetSponsorSurveysByDiscountIdAndQuestionId(int discountid, int questionid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * FROM SponsorSurveys where discountid=@discountid and questionid=@questionid " +
                        " order by questionid";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;
                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;

                cn.Open();
                return GetSponsorSurveysCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Inserts a new SponsorSurveys
        /// </summary>
        public  bool InsertSponsorSurveys(SponsorSurveysInfo SponsorSurveys)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into SponsorSurveys " +
                  "(discountid , " +
                  "questionid ) " +
                  "VALUES (" +
                  "@discountid, " +
                  "@questionid )", cn);

                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = SponsorSurveys.discountid;
                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = SponsorSurveys.questionid;

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);

            }
        }

        /// <summary>
        /// Deletes a SponsorSurveys
        /// </summary>
        public  bool DeleteSponsorSurveysByDiscountidAndQuestionid(int discountid, int questionid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from SponsorSurveys where discountid=@discountid and questionid=@questionid", cn);
                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;
                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Retrieves all Discounts
        /// </summary>
        public List<SponsorSurveysInfo> GetSponsorSurveysByDiscountId(int discountid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                        "FROM SponsorSurveys where discountid=@discountid  " +
                        " order by questionid";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;

                cn.Open();
                return GetSponsorSurveysCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new SponsorSurveysInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual SponsorSurveysInfo GetSponsorSurveysFromReader(IDataReader reader)
        {
            return GetSponsorSurveysFromReader(reader, true);
        }
        protected virtual SponsorSurveysInfo GetSponsorSurveysFromReader(IDataReader reader, bool readMemos)
        {
            SponsorSurveysInfo SponsorSurveys = new SponsorSurveysInfo(
                  (int)reader["discountid"],
                  (int)reader["questionid"]);
            return SponsorSurveys;
        }


        /// <summary>
        /// Returns a collection of SponsorSurveysInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<SponsorSurveysInfo> GetSponsorSurveysCollectionFromReader(IDataReader reader)
        {
            return GetSponsorSurveysCollectionFromReader(reader, true);
        }
        protected virtual List<SponsorSurveysInfo> GetSponsorSurveysCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<SponsorSurveysInfo> SponsorSurveyss = new List<SponsorSurveysInfo>();
            while (reader.Read())
                SponsorSurveyss.Add(GetSponsorSurveysFromReader(reader, readMemos));
            return SponsorSurveyss;
        }
        #endregion
    }
}
#endregion