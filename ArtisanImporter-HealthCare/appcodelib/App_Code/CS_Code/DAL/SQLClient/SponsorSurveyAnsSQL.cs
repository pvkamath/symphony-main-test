﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region SponsorSurveyAnsInfo

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for SponsorSurveyAnsInfo
    /// </summary>
    public class SponsorSurveyAnsInfo
    {
        public SponsorSurveyAnsInfo() { }

        public SponsorSurveyAnsInfo(int answerid, int questionid, string answer_text, bool share_ind)
        {
            this.answerid = answerid;
            this.questionid = questionid;
            this.answer_text = answer_text;
            this.share_ind = share_ind;
        }


        private int _answerid = 0;
        public int answerid
        {
            get { return _answerid; }
            protected set { _answerid = value; }
        }

        private int _questionid = 0;
        public int questionid
        {
            get { return _questionid; }
            protected set { _questionid = value; }
        }

        private string _answer_text = "";
        public string answer_text
        {
            get { return _answer_text; }
            private set { _answer_text = value; }
        }

        private bool _share_ind = true;
        public bool share_ind
        {
            get { return _share_ind; }
            private set { _share_ind = value; }
        }
    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider
        
        /// <summary>
        /// Retrieves all SponsorSurveyAns
        /// </summary>
        public  List<SponsorSurveyAnsInfo> GetSponsorSurveyAns(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * FROM SponsorSurveyAns";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetSponsorSurveyAnsCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves all SponsorSurveyAns by questionid
        /// </summary>
        public  List<SponsorSurveyAnsInfo> GetSponsorSurveyAnsByQuestionId(int questionid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * FROM SponsorSurveyAns where questionid=@questionid " +
                        " order by answerid";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;

                cn.Open();
                return GetSponsorSurveyAnsCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves all SponsorSurveyAns by questionid and share_ind
        /// </summary>
        public List<SponsorSurveyAnsInfo> GetSponsorSurveyAnsByQuestionIdAndShareInd(int questionid, bool share_ind)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * FROM SponsorSurveyAns where questionid=@questionid and share_ind = @share_ind " +
                        " order by answerid";

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);
                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
                cmd.Parameters.Add("@share_ind", SqlDbType.Bit).Value = share_ind;

                cn.Open();
                return GetSponsorSurveyAnsCollectionFromReader(ExecuteReader(cmd), false);
            }
        }
        /// <summary>
        /// Retrieves the SponsorSurveyAns with the specified ID
        /// </summary>
        public SponsorSurveyAnsInfo GetSponsorSurveyAnsByID(int AnswerId)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * from SponsorSurveyAns where AnswerId=@AnswerId", cn);

                cmd.Parameters.Add("@AnswerId", SqlDbType.Int).Value = AnswerId;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSponsorSurveyAnsFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Retrieves the SponsorSurveyAns with the specified ID
        /// </summary>
        public SponsorSurveyAnsInfo GetSponsorSurveyAnsBySponserDetails(int userid, int discountid,int questionid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {                
                SqlCommand cmd = new SqlCommand("select * from SponsorSurveyAns " +
                    " INNER JOIN SponsorSurveyRsp " + 
                    " ON SponsorSurveyAns.answerid = SponsorSurveyRsp.answerid " +
                    " where share_ind =1 and " +
                    "userid = @userid  and " +
                    "discountid = @discountid and  " +
                    "SponsorSurveyAns.questionid=@questionid", cn);
                cmd.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@discountid", SqlDbType.Int).Value = discountid;
                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetSponsorSurveyAnsFromReader(reader, true);
                else
                    return null;
            }
        }


        /// <summary>
        /// Inserts a new SponsorSurveyAns
        /// </summary>
        public  int InsertSponsorSurveyAns(SponsorSurveyAnsInfo SponsorSurveyAns)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into SponsorSurveyAns " +
                  "(questionid , " +
                  "answer_text , " +
                  "share_ind ) " +
                  "VALUES (" +
                  "@questionid, " +
                  "@answer_text, " +
                  "@share_ind ) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = SponsorSurveyAns.questionid;
                cmd.Parameters.Add("@answer_text", SqlDbType.VarChar).Value = SponsorSurveyAns.answer_text;
                cmd.Parameters.Add("@share_ind", SqlDbType.Bit).Value = SponsorSurveyAns.share_ind;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;
            }
        }


        /// <summary>
        /// Updates a SponsorSurveyAns
        /// </summary>
        public  bool UpdateSponsorSurveyAns(SponsorSurveyAnsInfo SponsorSurveyAns)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update SponsorSurveyAns set " +
                  "questionid = @questionid, " +
                  "answer_text = @answer_text, " +
                  "share_ind = @share_ind " +
                  "where answerid = @answerid ", cn);

                cmd.Parameters.Add("@answerid", SqlDbType.Int).Value = SponsorSurveyAns.answerid;
                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = SponsorSurveyAns.questionid;
                cmd.Parameters.Add("@answer_text", SqlDbType.VarChar).Value = SponsorSurveyAns.answer_text;
                cmd.Parameters.Add("@share_ind", SqlDbType.Bit).Value = SponsorSurveyAns.share_ind;                

                foreach (IDataParameter param in cmd.Parameters)
                {
                    if (param.Value == null)
                        param.Value = DBNull.Value;
                }

                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Deletes a SponsorSurveyAns
        /// </summary>
        public bool UpdateSponsorSurveysByDiscountidAndAnsweridAndShareInd(int questionid, int Answerid, bool share_ind)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update SponsorSurveyAns set share_ind=@share_ind where answerid=@Answerid and questionid=@questionid", cn);
                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
                cmd.Parameters.Add("@Answerid", SqlDbType.Int).Value = Answerid;
                cmd.Parameters.Add("@share_ind", SqlDbType.Bit).Value = share_ind;  
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }


        /// <summary>
        /// Deletes a SponsorSurveyAns
        /// </summary>
        public  bool DeleteSponsorSurveyAns(int questionid)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from SponsorSurveyAns where questionid=@questionid", cn);
                cmd.Parameters.Add("@questionid", SqlDbType.Int).Value = questionid;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new SponsorSurveyAnsInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual SponsorSurveyAnsInfo GetSponsorSurveyAnsFromReader(IDataReader reader)
        {
            return GetSponsorSurveyAnsFromReader(reader, true);
        }
        protected virtual SponsorSurveyAnsInfo GetSponsorSurveyAnsFromReader(IDataReader reader, bool readMemos)
        {
            SponsorSurveyAnsInfo SponsorSurveyAns = new SponsorSurveyAnsInfo(
                  (int)reader["answerid"],
                  (int)reader["questionid"],
                  reader["answer_text"].ToString(),
                  (bool)reader["share_ind"]);
            return SponsorSurveyAns;
        }


        /// <summary>
        /// Returns a collection of SponsorSurveyAnsInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<SponsorSurveyAnsInfo> GetSponsorSurveyAnsCollectionFromReader(IDataReader reader)
        {
            return GetSponsorSurveyAnsCollectionFromReader(reader, true);
        }
        protected virtual List<SponsorSurveyAnsInfo> GetSponsorSurveyAnsCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<SponsorSurveyAnsInfo> SponsorSurveyAns = new List<SponsorSurveyAnsInfo>();
            while (reader.Read())
                SponsorSurveyAns.Add(GetSponsorSurveyAnsFromReader(reader, readMemos));
            return SponsorSurveyAns;
        }
        #endregion
    }
}
#endregion
