﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using PearlsReview.DAL;

#region EmailQueueInfo

namespace PearlsReview.DAL
{
    public class EmailQueueInfo
    {
        public EmailQueueInfo() { }

        public EmailQueueInfo(int EQueueID, int UserID, int EDefID, string SentDate)
        {
            this.EQueueID = EQueueID;
            this.UserID = UserID;
            this.EDefID = EDefID;
            this.SentDate = SentDate;
        }

        private int _EQueueID = 0;
        public int EQueueID
        {
            get { return _EQueueID; }
            protected set { _EQueueID = value; }
        }

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            private set { _UserID = value; }
        }

        private int _EDefID = 0;
        public int EDefID
        {
            get { return _EDefID; }
            private set { _EDefID = value; }
        }

        private string _SentDate = "";
        public string SentDate
        {
            get { return _SentDate; }
            private set { _SentDate = value; }
        }

    }
}
#endregion

#region SQLPRProvider and PRProvider

namespace PearlsReview.DAL.SQLClient
{
    public partial class SQL2PRProvider : DataAccess
    {
        #region SQLPRProvider

        /////////////////////////////////////////////////////////
        // methods that work with EmailQueue

        /// <summary>
        /// Returns the total number of EmailQueue
        /// </summary>
        public  int GetEmailQueueCount()
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select Count(*) from EmailQueue", cn);
                cn.Open();
                return (int)ExecuteScalar(cmd);
            }
        }

        /// <summary>
        /// Retrieves all EmailQueue
        /// </summary>
        public  List<EmailQueueInfo> GetEmailQueue(string cSortExpression)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "select * " +
                    "from EmailQueue";

                // add on ORDER BY if provided
                if (cSortExpression.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                        " order by " + cSortExpression;
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                return GetEmailQueueCollectionFromReader(ExecuteReader(cmd), false);
            }
        }

        /// <summary>
        /// Retrieves the EmailQueue with the specified ID
        /// </summary>
        public  EmailQueueInfo GetEmailQueueByID(int EQueueID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from EmailQueue where EQueueID=@EQueueID", cn);
                cmd.Parameters.Add("@EQueueID", SqlDbType.Int).Value = EQueueID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                    return GetEmailQueueFromReader(reader, true);
                else
                    return null;
            }
        }

        /// <summary>
        /// Deletes a EmailQueue
        /// </summary>
        public  bool DeleteEmailQueue(int EQueueID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete from EmailQueue where EQueueID=@EQueueID", cn);
                cmd.Parameters.Add("@EQueueID", SqlDbType.Int).Value = EQueueID;
                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }

        /// <summary>
        /// Inserts a new EmailQueue
        /// </summary>
        public  int InsertEmailQueue(EmailQueueInfo EmailQueue)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into EmailQueue " +
                  "(UserID, " +
                  "EDefID, " +
                  "SentDate) " +
                  "VALUES (" +
                  "@UserID, " +
                  "@EDefID, " +
                  "@SentDate) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = EmailQueue.UserID;
                cmd.Parameters.Add("@EDefID", SqlDbType.Int).Value = EmailQueue.EDefID;
                cmd.Parameters.Add("@SentDate", SqlDbType.DateTime).Value = DBNull.Value;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;

            }
        }

        /// <summary>
        /// Updates a EmailQueue
        /// </summary>
        public  bool UpdateEmailQueue(EmailQueueInfo EmailQueue)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update EmailQueue set " +
                  "UserID = @UserID, " +
                  "EDefID = @EDefID, " +
                  "SentDate = @SentDate " +
                  "where EQueueID = @EQueueID ", cn);
                //            cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = EmailQueue.UserID;
                cmd.Parameters.Add("@EDefID", SqlDbType.Int).Value = EmailQueue.EDefID;
                cmd.Parameters.Add("@SentDate", SqlDbType.DateTime).Value = EmailQueue.SentDate;
                cmd.Parameters.Add("@EQueueID", SqlDbType.Int).Value = EmailQueue.EQueueID;


                cn.Open();
                int ret = ExecuteNonQuery(cmd);
                return (ret == 1);
            }
        }
        #endregion

        #region PRProvider

        /// <summary>
        /// Returns a new EmailQueueInfo instance filled with the DataReader's current record data
        /// </summary>
        protected virtual EmailQueueInfo GetEmailQueueFromReader(IDataReader reader)
        {
            return GetEmailQueueFromReader(reader, true);
        }
        protected virtual EmailQueueInfo GetEmailQueueFromReader(IDataReader reader, bool readMemos)
        {
            EmailQueueInfo EmailQueue = new EmailQueueInfo(
              (int)reader["EQueueID"],
              (int)reader["UserID"],
              (int)reader["EDefID"],
              (Convert.IsDBNull(reader["SentDate"])
                  ? "" : ((DateTime)reader["SentDate"]).ToShortDateString()));

            return EmailQueue;
        }

        /// <summary>
        /// Returns a collection of EmailQueueInfo objects with the data read from the input DataReader
        /// </summary>
        protected virtual List<EmailQueueInfo> GetEmailQueueCollectionFromReader(IDataReader reader)
        {
            return GetEmailQueueCollectionFromReader(reader, true);
        }
        protected virtual List<EmailQueueInfo> GetEmailQueueCollectionFromReader(IDataReader reader, bool readMemos)
        {
            List<EmailQueueInfo> EmailQueue = new List<EmailQueueInfo>();
            while (reader.Read())
                EmailQueue.Add(GetEmailQueueFromReader(reader, readMemos));
            return EmailQueue;
        }
        #endregion
    }
}
#endregion