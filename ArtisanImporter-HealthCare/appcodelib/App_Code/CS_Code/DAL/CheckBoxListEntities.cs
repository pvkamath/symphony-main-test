using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
//   public class CheckBoxListTopicInfo
//   {
//      public CheckBoxListTopicInfo() { }

//       public CheckBoxListTopicInfo(int TopicID, string TopicName, bool Selected)
//      {
//            this.TopicID = TopicID;
//            this.TopicName = TopicName;
//            this.Selected = Selected;
//}
      
//        private int _TopicID = 0;
//        public int TopicID
//        {
//            get { return _TopicID; }
//            protected set { _TopicID = value; }
//        }

//        private string _TopicName = "";
//        public string TopicName
//        {
//            get { return _TopicName; }
//            private set { _TopicName = value; }
//        }

//        private bool _Selected = false;
//       public bool Selected
//        {
//            get { return _Selected; }
//            private set { _Selected = value; }
//        }
//  }

    //public class CheckBoxListCategoryInfo
    //{
    //    public CheckBoxListCategoryInfo() { }

    //    public CheckBoxListCategoryInfo(int ID, string Title, bool Selected)
    //    {
    //        this.ID = ID;
    //        this.Title = Title;
    //        this.Selected = Selected;
    //    }

    //    private int _ID = 0;
    //    public int ID
    //    {
    //        get { return _ID; }
    //        protected set { _ID = value; }
    //    }

    //    private string _Title = "";
    //    public string Title
    //    {
    //        get { return _Title; }
    //        private set { _Title = value; }
    //    }

    //    private bool _Selected = false;
    //    public bool Selected
    //    {
    //        get { return _Selected; }
    //        private set { _Selected = value; }
    //    }


    //}

    public class CheckBoxListResourcePersonInfo
    {
        public CheckBoxListResourcePersonInfo() { }

        public CheckBoxListResourcePersonInfo(int ResourceID, string Fullname, bool Selected)
        {
            this.ResourceID = ResourceID;
            this.Fullname = Fullname;
            this.Selected = Selected;
        }

        private int _ResourceID = 0;
        public int ResourceID
        {
            get { return _ResourceID; }
            protected set { _ResourceID = value; }
        }

        private string _Fullname = "";
        public string Fullname
        {
            get { return _Fullname; }
            private set { _Fullname = value; }
        }

        private bool _Selected = false;
        public bool Selected
        {
            get { return _Selected; }
            private set { _Selected = value; }
        }


    }


}