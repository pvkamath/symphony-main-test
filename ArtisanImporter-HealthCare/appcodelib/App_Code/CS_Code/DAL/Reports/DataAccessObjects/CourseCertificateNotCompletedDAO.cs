﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class CourseCertificateNotCompletedDAO : PRProvider3
    {
	    public CourseCertificateNotCompletedDAO()
	    {		
	    }
        public List<CourseCertificateNotCompletedDTO> GetCourseCertificateNotCompleted(CourseCertificateNotCompletedDTO dto)
            {
                List<CourseCertificateNotCompletedDTO> Result = null;
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "sp_r_course_notcompleted";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                    cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                    cmd.Parameters.Add("@category_id", SqlDbType.VarChar).Value = dto.CategoryId;
                    cmd.Parameters.Add("@course_id", SqlDbType.VarChar).Value = dto.CourseId;
                    cmd.Parameters.Add("@user_id", SqlDbType.VarChar).Value = dto.UserId;
                    cmd.Parameters.Add("@first_name", SqlDbType.VarChar).Value = dto.UserFirstName;
                    cmd.Parameters.Add("@last_name", SqlDbType.VarChar).Value = dto.UserLastName;
                    cmd.Parameters.Add("@date_start", SqlDbType.VarChar).Value = dto.ViewFromDate;
                    cmd.Parameters.Add("@date_end", SqlDbType.VarChar).Value = dto.ViewToDate;
                    cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;                    
                    
    
                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    Result = new List<CourseCertificateNotCompletedDTO>();
                    while (reader.Read())
                    {
                        CourseCertificateNotCompletedDTO returnDto = new CourseCertificateNotCompletedDTO();

                        returnDto.CategoryName = reader["categoryname"].ToString();
                        returnDto.CourseName = reader["coursename"].ToString();
                        returnDto.CourseNumber = reader["coursenumber"].ToString();
                        returnDto.CuserName = reader["cusername"].ToString();
                        returnDto.CfirstName = reader["cfirstname"].ToString();
                        returnDto.ClastName = reader["clastname"].ToString();                        

                        Result.Add(returnDto);
                    }
                }
                return Result;
            }
        }
}