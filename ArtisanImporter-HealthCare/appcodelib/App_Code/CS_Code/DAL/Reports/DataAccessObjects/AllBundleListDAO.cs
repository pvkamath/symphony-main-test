﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

/// <summary>
/// Summary description for AllCoursesByCategoryDAO
/// </summary>
namespace PearlsReview.DAL
{
    public class AllBundleListDAO : PRProvider3
    {
        public AllBundleListDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet GetAllBundlesByProfession(int msid)
        {            
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();                
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_microsite_bundles";
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = msid;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);                
            }
            return ds;
        }
        public DataSet GetCoursesByBundle(int bundleid)
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_microsite_coursesByBundle";
                cmd.Parameters.Add("@bundleid", SqlDbType.Int).Value = bundleid;                    
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);                
            }
            return ds;
        }
    }
}
