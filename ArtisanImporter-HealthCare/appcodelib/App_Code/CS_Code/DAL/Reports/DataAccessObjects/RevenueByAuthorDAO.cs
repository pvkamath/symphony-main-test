﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class RevenueByAuthorDAO : PRProvider3
    {
        public RevenueByAuthorDAO()
        {
        }

        public List<RevenueByAuthorDTO> GetRevenueByAuthor(RevenueByAuthorDTO dto)
        {
            List<RevenueByAuthorDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_msrevenuebyauthor";
                cmd.CommandType = CommandType.StoredProcedure;                
                cmd.Parameters.Add("@fromDate", SqlDbType.VarChar).Value = dto.FromDate;
                cmd.Parameters.Add("@toDate", SqlDbType.VarChar).Value = dto.ToDate;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;     
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<RevenueByAuthorDTO>();

                while (reader.Read())
                {
                    RevenueByAuthorDTO returnDto = new RevenueByAuthorDTO();
                    returnDto.RowID = Convert.ToInt32(reader["rowid"].ToString());
                    returnDto.ResourceID = Convert.ToInt32(reader["resourceid"].ToString());
                    returnDto.FullName = reader["fullname"].ToString();
                    returnDto.LastFirst = reader["lastfirst"].ToString();
                    returnDto.Address = reader["address"].ToString();
                    returnDto.City = reader["city"].ToString();
                    returnDto.State = reader["state"].ToString();
                    returnDto.Phone = reader["phone"].ToString();
                    returnDto.Email = reader["email"].ToString();
                    returnDto.Specialty = reader["specialty"].ToString();
                    returnDto.Degrees = reader["degrees"].ToString();
                    returnDto.Position = reader["position"].ToString();
                    returnDto.Fax = reader["fax"].ToString();
                    returnDto.TotalRevenue = (reader["TotalRevenue"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["TotalRevenue"]));
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}