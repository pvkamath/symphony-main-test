﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class AddMyReportListDAO : PRProvider3
    {
        public AddMyReportListDAO(){ }

        /// <summary>
        /// Add Report selection criteria to MyReport list
        /// </summary>
        public AddMyReportListDTO AddMyReportList(AddMyReportListDTO dto)
        {
            AddMyReportListDTO Result = new AddMyReportListDTO();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("insert into UserReports " +
                "(ReportName," +
                " UniqueID, " +
                "UserReportName, " +
                "Userid, "+
                "ReportParameters) " +
                "VALUES (" +
                "@in_strReportName, " +
                "@in_numUniqueID, " +
                "@in_strUserReportName, " +
                "@in_numUserId, " +
                "@in_strReportParameters)", cn);

                cmd.Parameters.Add("@in_strReportName", SqlDbType.VarChar).Value = dto.ReportName;
                cmd.Parameters.Add("@in_numUniqueID", SqlDbType.Int).Value = dto.UniqueID;
                cmd.Parameters.Add("@in_strUserReportName", SqlDbType.VarChar).Value = dto.UserReportName;
                cmd.Parameters.Add("@in_strReportParameters", SqlDbType.VarChar).Value = dto.ReportParameters;
                cmd.Parameters.Add("@in_numUserId", SqlDbType.Int).Value = dto.UserId;
                
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            return Result;
        }


        /// <summary>
        /// Get All MyReport list
        /// </summary>
        public List<AddMyReportListDTO> GetMyReportList(AddMyReportListDTO dto)
        {
            List<AddMyReportListDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * From UserReports" +
                    " where  UniqueID = @in_numUniqueID and Active = 1 and Userid = @in_numUserId", cn);
                
                cmd.Parameters.Add("@in_numUniqueID", SqlDbType.Int).Value = dto.UniqueID;
                cmd.Parameters.Add("@in_numUserId", SqlDbType.Int).Value = dto.UserId;

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<AddMyReportListDTO>();
                while (reader.Read())
                {
                    AddMyReportListDTO returnDto = new AddMyReportListDTO();

                    returnDto.MyReportID  = reader["MyReportID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["MyReportID"]);
                    returnDto.ReportName = reader["ReportName"].ToString();
                    returnDto.UniqueID = reader["UniqueID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UniqueID"]);
                    returnDto.UserReportName = reader["UserReportName"].ToString();
                    returnDto.ReportParameters = reader["ReportParameters"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }


        /// <summary>
        /// Get Single Report Criteria
        /// </summary>
        public List<AddMyReportListDTO> GetMyReport(AddMyReportListDTO dto)
        {
            List<AddMyReportListDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select From UserReports where MyReportID = @in_numMyReportID", cn);

                cmd.Parameters.Add("@in_numMyReportID", SqlDbType.Int).Value = dto.UniqueID;

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<AddMyReportListDTO>();
                while (reader.Read())
                {
                    AddMyReportListDTO returnDto = new AddMyReportListDTO();

                    returnDto.MyReportID = reader["MyReportID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["MyReportID"]);
                    returnDto.ReportName = reader["ReportName"].ToString();
                    returnDto.UniqueID = reader["UniqueID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UniqueID"]);
                    returnDto.UserReportName = reader["UserReportName"].ToString();
                    returnDto.ReportParameters = reader["ReportParameters"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }

        /// <summary>
        /// Update a MyReport
        /// </summary>
        public AddMyReportListDTO UpdateMyReport(AddMyReportListDTO dto)
        {
            AddMyReportListDTO Result = new AddMyReportListDTO();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserReports set " +
                   "UserReportName=@in_strUserReportName " +
                   "where MyReportID = @in_numMyReportID ", cn);

                cmd.Parameters.Add("@in_numMyReportID", SqlDbType.Int).Value = dto.MyReportID ;
                cmd.Parameters.Add("@in_strUserReportName", SqlDbType.VarChar).Value = dto.UserReportName;               

                cn.Open();
                cmd.ExecuteNonQuery();
            }
            return Result;
        }


        /// <summary>
        /// Deletes a MyReport
        /// </summary>
        public AddMyReportListDTO DeleteMyReport(AddMyReportListDTO dto)
        {
            AddMyReportListDTO Result = new AddMyReportListDTO();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("update UserReports set Active=0 where MyReportID = @in_numMyReportID ", cn);
                cmd.Parameters.Add("@in_numMyReportID", SqlDbType.Int).Value = dto.MyReportID;
                
                cn.Open();
                cmd.ExecuteNonQuery();
            }
            return Result;
        }       
    }
}
