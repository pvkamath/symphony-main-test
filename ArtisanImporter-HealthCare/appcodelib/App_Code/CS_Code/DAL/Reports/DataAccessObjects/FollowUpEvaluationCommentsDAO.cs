﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

/// <summary>
/// Summary description for EvaluationCommentsDAO
/// </summary>
namespace PearlsReview.DAL
{

    public class FollowUpEvaluationCommentsDAO : PRProvider3
    {
        public FollowUpEvaluationCommentsDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public List<FollowUpEvaluationCommentsDTO> GetFollowUpEvaluationComments(FollowUpEvaluationCommentsDTO dto)
        {

            List<FollowUpEvaluationCommentsDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_FollowUp_Survey_Evaluation_Comments";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@topic_ids", SqlDbType.VarChar).Value = dto.CourseIds;
                cmd.Parameters.Add("@surveydate_start", SqlDbType.VarChar).Value = dto.SurveyStartDate;
                cmd.Parameters.Add("@surveydate_end", SqlDbType.VarChar).Value = dto.SurveyEndDate;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<FollowUpEvaluationCommentsDTO>();

                while (reader.Read())
                {
                    FollowUpEvaluationCommentsDTO returnDto = new FollowUpEvaluationCommentsDTO();
                    returnDto.TopicId = reader["topicid"].ToString();
                    returnDto.TopicName = reader["topicname"].ToString();
                    returnDto.Course_Number = reader["course_number"].ToString();
                    returnDto.SurveyDate = reader["surveydate"].ToString() != string.Empty ? string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(reader["surveydate"].ToString())) : "";
                    returnDto.QuestionComment2 = reader["QuestionComment2"].ToString();
                    returnDto.QuestionComment4 = reader["QuestionComment4"].ToString();
                    returnDto.QuestionComment6 = reader["QuestionComment6"].ToString();
                    returnDto.QuestionComment7 = reader["QuestionComment7"].ToString();
                    returnDto.QuestionComment8 = reader["QuestionComment8"].ToString();
                    returnDto.QuestionComment9 = reader["QuestionComment9"].ToString();
                    
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}
