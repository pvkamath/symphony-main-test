﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for CourseCompletionsByCategory
/// </summary>
namespace PearlsReview.DAL
{
    public class CourseCompletionsByCategoryDAO : PRProvider3
{

    public CourseCompletionsByCategoryDAO()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public List<CourseCompletionsByCategoryDTO> GetCompletionsByCategory(CourseCompletionsByCategoryDTO dto)
    {
        List<CourseCompletionsByCategoryDTO> Result = null;
        CourseCompletionsByCategoryDTO returnDto;
        SqlCommand cmd;
        SqlDataReader reader;
        try
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_courseCompletionsbyCategory";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDate_Start;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDate_End;
                cmd.Parameters.Add("@categoryid", SqlDbType.Int).Value = dto.CategoryId;
                cmd.Parameters.Add("@seltype", SqlDbType.VarChar).Value = dto.SelType;
                cmd.Parameters.Add("@facilityid", SqlDbType.VarChar).Value = dto.FacilityID;                                
                cn.Open();
                reader = cmd.ExecuteReader();
                Result = new List<CourseCompletionsByCategoryDTO>();                
                while (reader.Read())
                {
                    returnDto = new CourseCompletionsByCategoryDTO();
                    returnDto.CategoryName = reader["categoryname"].ToString();
                    if (dto.SelType == "month")
                    {
                        returnDto.Year = Convert.ToInt32(reader["year"].ToString());
                        returnDto.Month = Convert.ToInt32(reader["month"].ToString());
                    }
                    else
                        returnDto.StateName = reader["statename"].ToString();
                    returnDto.topicname = reader["topicname"].ToString();
                    returnDto.coursenumber = reader["course_number"].ToString();
                    returnDto.score = reader["score"] == DBNull.Value ? 0 : decimal.Parse(reader["score"].ToString());                    
                    returnDto.total = reader["total"] == DBNull.Value ? 0 : int.Parse(reader["total"].ToString());
                    returnDto.totalUnique = reader["totalunique"] == DBNull.Value ? 0 : int.Parse(reader["totalunique"].ToString());                    
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        catch (Exception ex) { throw ex; }
        finally { returnDto = null; cmd = null; reader = null; }
    }
}
}
