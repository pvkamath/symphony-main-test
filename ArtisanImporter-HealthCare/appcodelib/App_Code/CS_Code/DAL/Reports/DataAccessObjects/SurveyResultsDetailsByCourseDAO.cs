﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class SurveyResultsDetailsByCourseDAO : PRProvider3
    {
	    public SurveyResultsDetailsByCourseDAO()
	    {
	    }
        public List<SurveyResultsDetailsByCourseDTO> GetSrvRstDetailsByCourse(SurveyResultsDetailsByCourseDTO dto)
        {
            List<SurveyResultsDetailsByCourseDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_survey_result";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_select", SqlDbType.VarChar).Value = dto.DeptSelect;
                cmd.Parameters.Add("@course_id", SqlDbType.VarChar).Value = dto.CourseId;
                cmd.Parameters.Add("@surveydate_start", SqlDbType.VarChar).Value = dto.SurveydateStart;
                cmd.Parameters.Add("@surveydate_end", SqlDbType.VarChar).Value = dto.SurveydateEnd;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SurveyResultsDetailsByCourseDTO>();
                while (reader.Read())
                {
                    SurveyResultsDetailsByCourseDTO returnDto = new SurveyResultsDetailsByCourseDTO();
                    returnDto.DeptName = reader["deptname"].ToString();
                    returnDto.TopicName = reader["topicname"].ToString();
                    returnDto.QNumber = reader["qnumber"].ToString();
                    returnDto.QText = reader["qtext"].ToString();
                    returnDto.TComment = reader["tcomment"].ToString();
                    returnDto.SurveyDate = reader["surveydate"].ToString();               

                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}
