﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
/// <summary>
/// Summary description for UserActiveInActive
/// </summary>
/// 
namespace PearlsReview.DAL
{
    public class UserActiveInActiveDAO : PRProvider3
    {
        public UserActiveInActiveDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        public List<UserActiveInActiveDTO> GetActiveInActiveUsers(UserActiveInActiveDTO dto)
        {
            List<UserActiveInActiveDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_user_active";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<UserActiveInActiveDTO>();
                while (reader.Read())
                {
                    UserActiveInActiveDTO returnDto = new UserActiveInActiveDTO();
                    returnDto.FacName = reader["facname"].ToString();
                    returnDto.ActiveTotal = reader["activetotal"].ToString();
                    returnDto.InActiveTotal = reader["inactivetotal"].ToString();
                                       Result.Add(returnDto);
                }
            }
            return Result;
        }

    }
}
