﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

/// <summary>
/// Summary description for GnrlUserSecurityReportDAO
/// </summary>
namespace PearlsReview.DAL
{

    public class GnrlUserSecurityReportDAO : PRProvider3
    {
        public GnrlUserSecurityReportDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public List<GnrlUserSecurityReportDTO> GetGnrlUserSecurity(GnrlUserSecurityReportDTO dto)
        {
            List<GnrlUserSecurityReportDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_general_user_security";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@org_id", SqlDbType.VarChar).Value = dto.OrgId;
                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<GnrlUserSecurityReportDTO>();
                while (reader.Read())
                {
                    GnrlUserSecurityReportDTO returnDto = new GnrlUserSecurityReportDTO();
                    returnDto.FacName = reader["facname"].ToString();
                    returnDto.LastName = reader["clastname"].ToString();
                    returnDto.FirstName = reader["cfirstname"].ToString();
                    returnDto.DeptName = reader["dept_name"].ToString();
                    returnDto.UserName = reader["cusername"].ToString();
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.AccessType = reader["displayname"].ToString();
                    returnDto.PrimaryDiscipline = reader["primarydiscipline"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}
