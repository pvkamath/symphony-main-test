﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class NoOfCompletionsDAO : PRProvider3
    {
        public NoOfCompletionsDAO()
        {

        }

        public List<NoOfCompletionsDTO> GetNoOfCompletions(NoOfCompletionsDTO dto)
    {
        List<NoOfCompletionsDTO> Result = null;
        using(SqlConnection cn = new SqlConnection(this.ConnectionString))
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = "sp_r_NumOfCompletions";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@org_id", SqlDbType.VarChar).Value = dto.OrgIds;
            cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
            cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
            cmd.Parameters.Add("@dept_select", SqlDbType.VarChar).Value = dto.DeptSelect;
            cmd.Parameters.Add("@viewdate_start", SqlDbType.VarChar).Value = dto.ViewDateStart;
            cmd.Parameters.Add("@viewdate_end", SqlDbType.VarChar).Value = dto.ViewDateEnd;
            cmd.Parameters.Add("@type_id", SqlDbType.VarChar).Value = dto.CourseType;
            cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
            cmd.Parameters.Add("@primaryDiscipline", SqlDbType.VarChar).Value = dto.PrimaryDiscipline;
            cn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            Result = new List<NoOfCompletionsDTO>();

            while (reader.Read())
            {
                NoOfCompletionsDTO returnDto = new NoOfCompletionsDTO();
                returnDto.FacName = reader["facilityname"].ToString();
                returnDto.DeptName = reader["deptname"].ToString();       
                returnDto.CourseNumber = reader["course_number"].ToString();
                returnDto.CourseType = reader["coursetype"].ToString();
                returnDto.TopicName = reader["topicname"].ToString();
                returnDto.Completions = Convert.ToInt32(reader["completions"]);
                Result.Add(returnDto);
            }
           
        }
        return Result;
    }

    }
}