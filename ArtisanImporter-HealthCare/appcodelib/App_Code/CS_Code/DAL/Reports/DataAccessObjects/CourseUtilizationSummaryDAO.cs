﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class CourseUtilizationSummaryDAO : PRProvider3
    {
        public CourseUtilizationSummaryDAO()
        {
        }

        public List<CourseUtilizationSummaryDTO> GetCourseUtilizationSummary(CourseUtilizationSummaryDTO dto)
        {
            List<CourseUtilizationSummaryDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_course_utilsummary";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@dept_select", SqlDbType.VarChar).Value = dto.DepartmentID;
                cmd.Parameters.Add("@category_id", SqlDbType.VarChar).Value = dto.CategoryId;
                cmd.Parameters.Add("@course_id", SqlDbType.VarChar).Value = dto.CourseId;
                cmd.Parameters.Add("@credit_hour", SqlDbType.VarChar).Value = dto.CreitHrs;
                cmd.Parameters.Add("@viewdate_start", SqlDbType.VarChar).Value = dto.ViewDateStart;
                cmd.Parameters.Add("@viewdate_end", SqlDbType.VarChar).Value = dto.ViewDateEnd;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
              

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<CourseUtilizationSummaryDTO>();
                while (reader.Read())
                {
                    CourseUtilizationSummaryDTO returnDto = new CourseUtilizationSummaryDTO();

                    returnDto.DeptName = reader["dept_name"].ToString();
                    returnDto.CategoryName = reader["title"].ToString();
                    returnDto.CourseName = reader["topicname"].ToString();
                    returnDto.CreitHrs = reader["hours"].ToString();
                    returnDto.Views = reader["vnum"].ToString();
                    returnDto.Tests = reader["tnum"].ToString();
                    returnDto.Completions = reader["cnum"].ToString();
                    returnDto.TotalcHours = reader["totalchours"].ToString();

                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}