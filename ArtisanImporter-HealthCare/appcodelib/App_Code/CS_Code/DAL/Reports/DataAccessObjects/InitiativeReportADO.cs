﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
namespace PearlsReview.DAL
{

    /// <summary>
    /// Summary description for InitiativeReportADO
    /// </summary>
    public class InitiativeReportADO : PRProvider3
    {
        public InitiativeReportADO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public List<SponsorSurveyDTO> GetInitiativeFullEmailList(string DiscountId, string StartDate, string EndDate, string StateList)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_Full_Email_List";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.FirstName = reader["firstname"].ToString();
                    returnDto.LastName = reader["lastname"].ToString();
                    returnDto.Email = reader["email"].ToString();
                    returnDto.Address1 = reader["address1"].ToString();
                    returnDto.Address2 = reader["address2"].ToString();
                    returnDto.City = reader["city"].ToString();
                    returnDto.State = reader["state"].ToString();
                    returnDto.Zip = reader["zip"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.AgeGroup = reader["agegroup"] == DBNull.Value ? 0 : Convert.ToInt32(reader["agegroup"]);
                    //returnDto.PositionId = reader["position_id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["position_id"]);
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.CourseNumber = reader["course_number"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    returnDto.CourseName = reader["topicname"].ToString();
                    returnDto.Education = reader["education"].ToString();
                    returnDto.OrderDate = reader["orderdate"].ToString() != string.Empty ? string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(reader["orderdate"].ToString())) : "";
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public List<SponsorSurveyDTO> GetInitiativeFullCEEmailList(string DiscountId, string StartDate, string EndDate, string StateList)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_Sponsor_Survey_Completion_CE";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.FirstName = reader["firstname"].ToString();
                    returnDto.LastName = reader["lastname"].ToString();
                    returnDto.Email = reader["email"].ToString();
                    returnDto.Address1 = reader["address1"].ToString();
                    returnDto.Address2 = reader["address2"].ToString();
                    returnDto.City = reader["city"].ToString();
                    returnDto.State = reader["state"].ToString();
                    returnDto.Zip = reader["zip"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.AgeGroup = reader["agegroup"] == DBNull.Value ? 0 : Convert.ToInt32(reader["agegroup"]);
                    //returnDto.PositionId = reader["position_id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["position_id"]);
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.CourseNumber = reader["course_number"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    returnDto.CourseName = reader["topicname"].ToString();
                    returnDto.Education = reader["education"].ToString();
                    returnDto.OrderDate = reader["orderdate"].ToString() != string.Empty ? string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(reader["orderdate"].ToString())) : "";
                    returnDto.Profname = reader["profession"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public List<SponsorSurveyDTO> GetInitiativeFullRetailEmailList(string DiscountId, string StartDate, string EndDate, string StateList)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_Sponsor_Survey_Completion_Retail";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.FirstName = reader["firstname"].ToString();
                    returnDto.LastName = reader["lastname"].ToString();
                    returnDto.Email = reader["email"].ToString();
                    returnDto.Address1 = reader["address1"].ToString();
                    returnDto.Address2 = reader["address2"].ToString();
                    returnDto.City = reader["city"].ToString();
                    returnDto.State = reader["state"].ToString();
                    returnDto.Zip = reader["zip"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.AgeGroup = reader["agegroup"] == DBNull.Value ? 0 : Convert.ToInt32(reader["agegroup"]);
                    //returnDto.PositionId = reader["position_id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["position_id"]);
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.CourseNumber = reader["course_number"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    returnDto.CourseName = reader["topicname"].ToString();
                    returnDto.Education = reader["education"].ToString();
                    returnDto.OrderDate = reader["orderdate"].ToString() != string.Empty ? string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(reader["orderdate"].ToString())) : "";
                    returnDto.Profname = reader["profession"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public List<SponsorSurveyDTO> GetInitiativeUniqueEmailList(string DiscountId, string StartDate, string EndDate, string StateList)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_Unique_Email_List";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.FirstName = reader["cfirstname"].ToString();
                    returnDto.LastName = reader["clastname"].ToString();
                    returnDto.Email = reader["cemail"].ToString();
                    returnDto.Address1 = reader["caddress1"].ToString();
                    returnDto.Address2 = reader["caddress2"].ToString();
                    returnDto.City = reader["ccity"].ToString();
                    returnDto.State = reader["cstate"].ToString();
                    returnDto.Zip = reader["czipcode"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.AgeGroup = reader["agegroup"] == DBNull.Value ? 0 : Convert.ToInt32(reader["agegroup"]);
                    //returnDto.PositionId = reader["position_id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["position_id"]);
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    returnDto.Education = reader["education"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public List<SponsorSurveyDTO> GetInitiativeNewLeadUniqueEmailList(string DiscountId, string StartDate, string EndDate, string StateList)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_New_Unique_Email_List";
                cmd.CommandTimeout = 0;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.FirstName = reader["cfirstname"].ToString();
                    returnDto.LastName = reader["clastname"].ToString();
                    returnDto.Email = reader["cemail"].ToString();
                    returnDto.Address1 = reader["caddress1"].ToString();
                    returnDto.Address2 = reader["caddress2"].ToString();
                    returnDto.City = reader["ccity"].ToString();
                    returnDto.State = reader["cstate"].ToString();
                    returnDto.Zip = reader["czipcode"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.AgeGroup = reader["agegroup"] == DBNull.Value ? 0 : Convert.ToInt32(reader["agegroup"]);
                    //returnDto.PositionId = reader["position_id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["position_id"]);
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    returnDto.Education = reader["education"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public List<SponsorSurveyDTO> GetInitiativeSummary(string DiscountId, string StartDate, string EndDate, string StateList)
        {
            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_Summary";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.CourseNumber = reader["course_number"].ToString();
                    returnDto.CourseName = reader["topicname"].ToString();
                    returnDto.View = reader["impression"].ToString();
                    returnDto.clickThrogh = reader["clickthrough"].ToString();
                    returnDto.totalCompletion = reader["tcomplete"].ToString();
                    returnDto.UniqueCompletion = reader["tunique"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }

        public List<SponsorSurveyDTO> GetInitiativeSummaryAll(string StartDate, string EndDate, string StateList)
        {
            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_Summary_All";
                cmd.CommandType = CommandType.StoredProcedure;
                // cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.CourseNumber = reader["course_number"].ToString();
                    returnDto.CourseName = reader["topicname"].ToString();
                    returnDto.View = reader["impression"].ToString();
                    returnDto.clickThrogh = reader["clickthrough"].ToString();
                    returnDto.totalCompletion = reader["tcomplete"].ToString();
                    returnDto.UniqueCompletion = reader["tunique"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public List<SponsorSurveyDTO> GetSponsorSurvey(SponsorSurveyDTO dto)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_Sponsor_Survey";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = dto.DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = dto.StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = dto.EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = dto.StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.QuestionID = Convert.ToInt32(reader["QuestionID"]);
                    returnDto.UserID = Convert.ToInt32(reader["UserID"]);
                    returnDto.OrderItemID = Convert.ToInt32(reader["OrderItemID"]);
                    returnDto.UserName = reader["FirstName"].ToString();
                    returnDto.FirstName = reader["FirstName"].ToString();
                    returnDto.LastName = reader["LastName"].ToString();
                    returnDto.Address1 = reader["Address1"].ToString();
                    returnDto.Address2 = reader["Address2"].ToString();
                    returnDto.City = reader["City"].ToString();
                    returnDto.State = reader["State"].ToString();
                    returnDto.Zip = reader["Zip"].ToString();
                    returnDto.Email = reader["Email"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.QuestionText = reader["qText"].ToString();
                    returnDto.AnswerText = reader["aText"].ToString();
                    returnDto.CourseName = reader["topicname"].ToString();
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.AgeGroup = String.IsNullOrEmpty(reader["agegroup"].ToString()) ? 0 : int.Parse(reader["agegroup"].ToString());
                    returnDto.OrderDate = reader["OrderDate"].ToString();
                    returnDto.Education = reader["education"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public List<SponsorSurveyDTO> GetSponsorSurveyCompletion(SponsorSurveyDTO dto)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_Sponsor_Survey_Completion";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = dto.DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = dto.StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = dto.EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = dto.StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.QuestionID = Convert.ToInt32(String.IsNullOrEmpty(reader["QuestionID"].ToString()) ? "0" : reader["QuestionID"].ToString());
                    returnDto.UserID = Convert.ToInt32(String.IsNullOrEmpty(reader["UserID"].ToString()) ? "0" : reader["UserID"].ToString());
                    returnDto.OrderItemID = Convert.ToInt32(reader["OrderItemID"]);
                    returnDto.UserName = reader["FirstName"].ToString();
                    returnDto.FirstName = reader["FirstName"].ToString();
                    returnDto.LastName = reader["LastName"].ToString();
                    returnDto.Address1 = reader["Address1"].ToString();
                    returnDto.Address2 = reader["Address2"].ToString();
                    returnDto.City = reader["City"].ToString();
                    returnDto.State = reader["State"].ToString();
                    returnDto.Zip = reader["Zip"].ToString();
                    returnDto.Email = reader["Email"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.QuestionText = reader["qText"].ToString();
                    returnDto.AnswerText = reader["aText"].ToString();
                    returnDto.CourseName = reader["topicname"].ToString();
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.AgeGroup = String.IsNullOrEmpty(reader["agegroup"].ToString()) ? 0 : int.Parse(reader["agegroup"].ToString());
                    returnDto.OrderDate = reader["OrderDate"].ToString();
                    returnDto.Education = reader["education"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public List<SponsorSurveyDTO> GetSponsorSurveyCompletionC(SponsorSurveyDTO dto)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_Sponsor_Survey_Completion_CE";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = dto.DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = dto.StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = dto.EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = dto.StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.QuestionID = Convert.ToInt32(String.IsNullOrEmpty(reader["QuestionID"].ToString()) ? "0" : reader["QuestionID"].ToString());
                    returnDto.UserID = Convert.ToInt32(String.IsNullOrEmpty(reader["UserID"].ToString()) ? "0" : reader["UserID"].ToString());
                    returnDto.OrderItemID = Convert.ToInt32(reader["OrderItemID"]);
                    returnDto.UserName = reader["FirstName"].ToString();
                    returnDto.FirstName = reader["FirstName"].ToString();
                    returnDto.LastName = reader["LastName"].ToString();
                    returnDto.Address1 = reader["Address1"].ToString();
                    returnDto.Address2 = reader["Address2"].ToString();
                    returnDto.City = reader["City"].ToString();
                    returnDto.State = reader["State"].ToString();
                    returnDto.Zip = reader["Zip"].ToString();
                    returnDto.Email = reader["Email"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.QuestionText = reader["qText"].ToString();
                    returnDto.AnswerText = reader["aText"].ToString();
                    returnDto.CourseName = reader["topicname"].ToString();
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.LicenseStates = reader["profession"].ToString();
                    returnDto.OrderDate = reader["OrderDate"].ToString();
                    returnDto.Education = reader["education"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public List<SponsorSurveyDTO> GetSponsorSurveyCompletionR(SponsorSurveyDTO dto)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_Sponsor_Survey_Completion_Retail";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = dto.DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = dto.StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = dto.EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = dto.StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.QuestionID = Convert.ToInt32(String.IsNullOrEmpty(reader["QuestionID"].ToString()) ? "0" : reader["QuestionID"].ToString());
                    returnDto.UserID = Convert.ToInt32(String.IsNullOrEmpty(reader["UserID"].ToString()) ? "0" : reader["UserID"].ToString());
                    returnDto.OrderItemID = Convert.ToInt32(reader["OrderItemID"]);
                    returnDto.UserName = reader["FirstName"].ToString();
                    returnDto.FirstName = reader["FirstName"].ToString();
                    returnDto.LastName = reader["LastName"].ToString();
                    returnDto.Address1 = reader["Address1"].ToString();
                    returnDto.Address2 = reader["Address2"].ToString();
                    returnDto.City = reader["City"].ToString();
                    returnDto.State = reader["State"].ToString();
                    returnDto.Zip = reader["Zip"].ToString();
                    returnDto.Email = reader["Email"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.QuestionText = reader["qText"].ToString();
                    returnDto.AnswerText = reader["aText"].ToString();
                    returnDto.CourseName = reader["topicname"].ToString();
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.LicenseStates = reader["profession"].ToString();
                    returnDto.OrderDate = reader["OrderDate"].ToString();
                    returnDto.Education = reader["education"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        //NOT IN USE
        public DataTable GetInitiativeSurvey(string DiscountId, string StartDate, string EndDate, string StateList)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_initiative_survey";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = DiscountId;
                cmd.Parameters.Add("@startdate", SqlDbType.VarChar).Value = StartDate;
                cmd.Parameters.Add("@enddate", SqlDbType.VarChar).Value = EndDate;
                cmd.Parameters.Add("@statelist", SqlDbType.VarChar).Value = StateList;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                dt.Load(reader);

                return dt;
            }
        }
        public bool CompareDiscount(string DiscountId)
        {
            bool flag = false;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Initiative_CompareDiscount";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = DiscountId;
                SqlParameter output = new SqlParameter("@CompareE", SqlDbType.Int);
                output.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(output);
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                flag = int.Parse(output.Value.ToString()) == 0 ? false : true;
            }
            return flag;
        }
    }
}
