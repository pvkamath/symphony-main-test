﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for RevenueByDtRangeDAO
/// </summary>
namespace PearlsReview.DAL
{
public class RevenueByDtRangeDAO :PRProvider3
{
         
	public RevenueByDtRangeDAO()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public List<RevenueByDtRangeDTO> GetAllCEsTakenByDtRange(RevenueByDtRangeDTO dto)
    {
        List<RevenueByDtRangeDTO> Result = null;
        RevenueByDtRangeDTO returnDto;
        SqlCommand cmd;
        SqlDataReader reader;
        try
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_RevenueByDate_CEsTaken";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDate_Start;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDate_End;
                cmd.Parameters.Add("@course_number", SqlDbType.VarChar).Value = dto.Course_Number;
                //cmd.Parameters.Add("@seltype", SqlDbType.VarChar).Value = dto.SelType;
                cmd.Parameters.Add("@freeOrderListing", SqlDbType.VarChar).Value = dto.comment;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cn.Open();
                reader = cmd.ExecuteReader();
                Result = new List<RevenueByDtRangeDTO>();
                while (reader.Read())
                {
                    returnDto = new RevenueByDtRangeDTO();
                    returnDto.topicid = reader["topicid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["topicid"]);
                    returnDto.topicname = reader["topicname"].ToString();
                    returnDto.Course_Number = reader["course_number"].ToString();
                    returnDto.credits = reader["credits"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["credits"]);
                    returnDto.lastmod = reader["lastmod"].ToString();
                    returnDto.status = reader["status"] == DBNull.Value ? (bool)reader["status"] : true;
                    returnDto.score = reader["score"].ToString();
                    returnDto.cfirstname = reader["cfirstname"].ToString();
                    returnDto.clastname = reader["clastname"].ToString();
                    returnDto.uniqueid = reader["rnid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["rnid"]);
                    returnDto.views = reader["views"] == DBNull.Value ? 0 : Convert.ToInt32(reader["views"]);
                    returnDto.testid = reader["testid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["testid"]);
                    //   returnDto.firstname = reader["cfirstname"].ToString();
                    //returnDto.lastname = reader["clastname"].ToString();                   

                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        catch (Exception ex) { return null; }
        finally { returnDto = null; cmd = null; reader = null; }
    }

    public List<RevenueByDtRangeDTO> GetAllCEsOrderedByDtRange(RevenueByDtRangeDTO dto)
    {
        List<RevenueByDtRangeDTO> Result = null;
       using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_RevenueByDate_CEsOrdered";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDate_Start;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDate_End;
                cmd.Parameters.Add("@course_number", SqlDbType.VarChar).Value = dto.Course_Number;
                //cmd.Parameters.Add("@seltype", SqlDbType.VarChar).Value = dto.SelType;
                cmd.Parameters.Add("@freeOrderListing", SqlDbType.VarChar).Value = dto.comment;
                cmd.Parameters.Add("@rType", SqlDbType.VarChar).Value = dto.SelType;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
           Result = new List<RevenueByDtRangeDTO>();
          
           while (reader.Read())
                {
                    RevenueByDtRangeDTO returnDto = new RevenueByDtRangeDTO();
                    returnDto.topicid = reader["topicid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["topicid"]);
                    returnDto.topicname = reader["topicname"].ToString();
                    returnDto.Course_Number = reader["course_number"].ToString();
                    returnDto.mediatype =reader["mediatype"].ToString();
                    returnDto.userid = reader["rnID"] == DBNull.Value ? 0 : Convert.ToInt32(reader["rnID"]);
                    returnDto.orderid = reader["orderid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["orderid"]);
                    returnDto.firstname = reader["firstname"].ToString();
                    returnDto.lastname = reader["lastname"].ToString();
                    returnDto.orderitemid = reader["orderitemid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["orderitemid"]);
                    returnDto.address1 = reader["address1"].ToString();
                    returnDto.city = reader["city"].ToString();
                    returnDto.state = reader["state"].ToString();
                    returnDto.zip = reader["zip"].ToString();
                    returnDto.email = reader["email"].ToString();
                    returnDto.media_type = reader["media_type"].ToString();
                    returnDto.comment = reader["comment"].ToString();
                    returnDto.cost = reader["cost"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["cost"]);
                    returnDto.discountid = reader["discountid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["discountid"]);
                    returnDto.orderdate = reader["orderdate"].ToString();
                    returnDto.optin_ind = reader["optin_ind"] == DBNull.Value ? (bool)reader["optin_ind"] : false;
                    returnDto.quantity = reader["quantity"] == DBNull.Value ? 0 : Convert.ToInt32(reader["quantity"]);
                    returnDto.shipcost = reader["shipcost"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["shipcost"]);
                    returnDto.verification = reader["verification"].ToString();
                    returnDto.totalcost = reader["totalcost"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["totalcost"]);
                    returnDto.tax = reader["tax"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["tax"]);
                    returnDto.subtotal = reader["subtotal"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["subtotal"]);

               Result.Add(returnDto);
                }
     } 
        return Result;
    }

    public RevenueByDtRangeDTO GetRevenueByDtRangeGTotal_CEsOrdered(RevenueByDtRangeDTO dto)
    {
        //SalesByStateDTO returnDto = null;
        RevenueByDtRangeDTO rCEOrderedDTO = new RevenueByDtRangeDTO();
                 
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_RevenueByDate_CEsOrdered";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDate_Start;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDate_End;
                cmd.Parameters.Add("@course_number", SqlDbType.VarChar).Value = dto.Course_Number;
                //cmd.Parameters.Add("@seltype", SqlDbType.VarChar).Value = dto.SelType;
                cmd.Parameters.Add("@freeOrderListing", SqlDbType.VarChar).Value = dto.comment;
                cmd.Parameters.Add("@rType", SqlDbType.VarChar).Value = dto.SelType;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                RevenueByDtRangeDTO returnDto = new RevenueByDtRangeDTO();
                   
                while (reader.Read())
                {
                    returnDto.shipcost = reader["shipcost"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["shipcost"]);
                    returnDto.subtotal = reader["subtotal"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["subtotal"]);
                    returnDto.tax = reader["tax"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["tax"]);
                   
                    rCEOrderedDTO.TaxSum += returnDto.tax;
                    rCEOrderedDTO.SumTotal += returnDto.subtotal;
                    rCEOrderedDTO.ShipSum += returnDto.shipcost;
                    rCEOrderedDTO.Gtotal = rCEOrderedDTO.TaxSum + rCEOrderedDTO.SumTotal + rCEOrderedDTO.ShipSum;
                 
                }
            }

            return rCEOrderedDTO;
        }
    
         
    public List<RevenueByDtRangeDTO> GetAllUnlimitedCEsByDtRange(RevenueByDtRangeDTO dto)
    {
        List<RevenueByDtRangeDTO> Result = null;
       using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_RevenueByDate_UnlimitedCEs";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDate_Start;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDate_End;
                cmd.Parameters.Add("@course_number", SqlDbType.VarChar).Value = dto.Course_Number;
                //cmd.Parameters.Add("@seltype", SqlDbType.VarChar).Value = dto.SelType;
                cmd.Parameters.Add("@freeOrderListing", SqlDbType.VarChar).Value = dto.comment;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
           Result = new List<RevenueByDtRangeDTO>();
           while (reader.Read())
                {
                    RevenueByDtRangeDTO returnDto = new RevenueByDtRangeDTO();
                    returnDto.userid = reader["userid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["userid"]);
                    returnDto.orderid = reader["orderid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["orderid"]);
                    returnDto.firstname = reader["firstname"].ToString();
                    returnDto.lastname = reader["lastname"].ToString();
                    returnDto.address1 = reader["address1"].ToString();
                    returnDto.city = reader["city"].ToString();
                    returnDto.state = reader["state"].ToString();
                    returnDto.zip = reader["zip"].ToString();
                    returnDto.email = reader["email"].ToString();
                    returnDto.comment = reader["comment"].ToString();
                    returnDto.orderdate = reader["orderdate"].ToString();
                    returnDto.shipcost = reader["shipcost"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["shipcost"]);
                    returnDto.ship_ind = reader["ship_ind"] == DBNull.Value ? false : Convert.ToBoolean(reader["ship_ind"]);
                    returnDto.verification = reader["verification"].ToString();
                    returnDto.totalcost = reader["totalcost"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["totalcost"]);
                    returnDto.tax = reader["tax"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["tax"]);
                    returnDto.subtotal = reader["subtotal"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["subtotal"]);
                
            Result.Add(returnDto);
                }
     } 
        return Result;
    }

    public RevenueByDtRangeDTO GetRevenueByDtRangeGTotal_CEUnlimited(RevenueByDtRangeDTO dto)
    {
        //SalesByStateDTO returnDto = null;
        RevenueByDtRangeDTO rCEOrderedDTO = new RevenueByDtRangeDTO();
       
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_RevenueByDate_UnlimitedCEs";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDate_Start;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDate_End;
                cmd.Parameters.Add("@course_number", SqlDbType.VarChar).Value = dto.Course_Number;
                //cmd.Parameters.Add("@seltype", SqlDbType.VarChar).Value = dto.SelType;
                cmd.Parameters.Add("@freeOrderListing", SqlDbType.VarChar).Value = dto.comment;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                RevenueByDtRangeDTO returnDto = new RevenueByDtRangeDTO();

                while (reader.Read())
                {
                    returnDto.tax = reader["tax"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["tax"]);
                    returnDto.subtotal = reader["subtotal"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["subtotal"]);
                    //returnDto.shipcost = reader["shipcost"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["shipcost"]);

                    rCEOrderedDTO.TaxSum += returnDto.tax;
                    rCEOrderedDTO.SumTotal += returnDto.subtotal;
                   // rCEOrderedDTO.ShipSum += returnDto.shipcost;
                    rCEOrderedDTO.Gtotal = rCEOrderedDTO.TaxSum + rCEOrderedDTO.SumTotal;

                }
            }

            return rCEOrderedDTO;
        }
    
   

   }
}
