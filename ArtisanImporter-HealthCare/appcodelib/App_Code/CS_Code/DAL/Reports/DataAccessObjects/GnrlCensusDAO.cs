﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;


/// <summary>
/// Summary description for GnrlCensusDAO
/// </summary>
/// 
namespace PearlsReview.DAL
{
    public class GnrlCensusDAO : PRProvider3
    {
        public GnrlCensusDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public List<GnrlCensusDTO> GetGnrlCensus(GnrlCensusDTO dto)
        {
            List<GnrlCensusDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_general_census";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@org_id", SqlDbType.VarChar).Value = dto.OrgId;
                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<GnrlCensusDTO>();
                while (reader.Read())
                {
                    GnrlCensusDTO returnDto = new GnrlCensusDTO();
                    returnDto.FacName = reader["fname"].ToString();
                    returnDto.DeptName = reader["deptname"].ToString();
                    returnDto.Tuser = reader["tuser"] == DBNull.Value ? 0 : Convert.ToDouble(reader["tuser"]);
                    returnDto.Tmanager = reader["tmanager"] == DBNull.Value ? 0 : Convert.ToDouble(reader["tmanager"]);
                    returnDto.Tfacadmin = reader["tfacadmin"] == DBNull.Value ? 0 : Convert.ToDouble(reader["tfacadmin"]);
                    returnDto.Tparentorgadmin = reader["tparentorgadmin"] == DBNull.Value ? 0 : Convert.ToDouble(reader["tparentorgadmin"]);
                    returnDto.Ttotaluser = reader["totaluser"] == DBNull.Value ? 0 : Convert.ToDouble(reader["totaluser"]);
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}
