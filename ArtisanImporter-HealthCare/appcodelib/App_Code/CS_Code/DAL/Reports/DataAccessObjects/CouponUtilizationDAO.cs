﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class CouponUtilizationDAO : PRProvider3
    {
        public CouponUtilizationDAO()
        {
        }



        public List<CouponUtilizationDTO> GetCouponUtilization(CouponUtilizationDTO dto)
        {
            List<CouponUtilizationDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_MSCouponUtiliztion";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDateStart;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDateEnd;
                cmd.Parameters.Add("@Coupon", SqlDbType.VarChar).Value = dto.Coupon;
                cmd.Parameters.Add("@OrderBy", SqlDbType.VarChar).Value = dto.OrderBy;          
                cn.Open();
                Result = new List<CouponUtilizationDTO>();

                DataSet ds = new DataSet();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "CouponUtilization");

                foreach (DataRow l_Row in ds.Tables[0].Rows)
                {
                    CouponUtilizationDTO returnDto = new CouponUtilizationDTO();
                    returnDto.Address1 = Convert.ToString(l_Row["address1"]);
                    returnDto.Address2 = Convert.ToString(l_Row["address2"]);
                    returnDto.City = Convert.ToString(l_Row["city"]);
                    returnDto.Couponcode = Convert.ToString(l_Row["couponcode"]);
                    returnDto.FirstName = Convert.ToString(l_Row["firstname"]);
                    returnDto.LastName = Convert.ToString(l_Row["lastname"]);
                    returnDto.OrderDate = Convert.ToDateTime(l_Row["orderdate"]);
                    returnDto.OrderID = Convert.ToInt32(l_Row["orderid"]);
                    returnDto.Verification = Convert.ToString(l_Row["Verification"]);
                    returnDto.Comment = Convert.ToString(l_Row["Comment"]);
                    returnDto.ShipCost = Convert.ToDecimal(l_Row["shipcost"]); //ShipH                     
                    returnDto.State = Convert.ToString(l_Row["state"]);
                    returnDto.SubTotal = Convert.ToDecimal(l_Row["subtotal"]);                    
                    returnDto.Tax = Convert.ToDecimal(l_Row["tax"]);
                    returnDto.TotalCost = Convert.ToDecimal(l_Row["total"]);
                    returnDto.Discount = Convert.ToDecimal(l_Row["coupon"]);
                    returnDto.StateName = Convert.ToString(l_Row["statename"]);
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}