﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for RevenueByDtRangeDAO
/// </summary>
namespace PearlsReview.DAL
{
    public class PimaGiftCardDAO :PRProvider3
    {
         
	    public PimaGiftCardDAO()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }

        public List<PimaGiftCardUserDTO> GetAllPimaGiftCardUsers(PimaGiftCardUserDTO dto)
        {
            List<PimaGiftCardUserDTO> Result = null;
            PimaGiftCardUserDTO returnDto;
            SqlCommand cmd;
            SqlDataReader reader;
            try
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "sp_r_pima_giftcard_users";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@startDate", SqlDbType.VarChar).Value = dto.StartDate;
                    cmd.Parameters.Add("@endDate", SqlDbType.VarChar).Value = dto.EndDate;              
                    cn.Open();
                    reader = cmd.ExecuteReader();
                    Result = new List<PimaGiftCardUserDTO>();
                    while (reader.Read())
                    {
                        returnDto = new PimaGiftCardUserDTO();
                        returnDto.NumberOfCourses = reader["numberofcourses"] == DBNull.Value ? 0 : Convert.ToInt32(reader["numberofcourses"]);
                        returnDto.ContactHours = reader["credits"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["credits"]);
                        returnDto.CAddress1 = reader["caddress1"].ToString();
                        returnDto.CCity = reader["ccity"].ToString();
                        returnDto.CEmail = reader["cemail"].ToString();
                        returnDto.CreatedDate = reader["created"].ToString();
                        returnDto.CZipCode = reader["czipCode"].ToString();
                        returnDto.Employer = reader["employer"].ToString();
                        returnDto.LastLogin = reader["lastlogin"].ToString();
                        returnDto.PrimayProfession = reader["primaryprofession"].ToString();
                        returnDto.StateCode = reader["cstate"].ToString();
                        returnDto.UserFirstName = reader["cfirstname"].ToString();
                        returnDto.UserLastName = reader["clastname"].ToString();
                        returnDto.UserID = reader["cusername"].ToString();
                       
                        //returnDto.lastname = reader["clastname"].ToString();                   

                        Result.Add(returnDto);
                    }
                }
                return Result;
            }
            catch (Exception ex) { return null; }
            finally { returnDto = null; cmd = null; reader = null; }
        }   
   

    }
}
