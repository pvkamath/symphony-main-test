﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for GnrlSummaryCourseActivityDAO
/// </summary>

namespace PearlsReview.DAL
{
    public class GnrlSummaryCourseActivityDAO : PRProvider3
    {
        public GnrlSummaryCourseActivityDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public List<GnrlSummaryCourseActivityDTO> GetGnrlUserSecurity(GnrlSummaryCourseActivityDTO dto)
        {
            List<GnrlSummaryCourseActivityDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_general_course_summary";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@order_by", SqlDbType.VarChar).Value = dto.OrderBy;
                cmd.Parameters.Add("@org_id", SqlDbType.VarChar).Value = dto.OrgIds;
                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@viewdate_start", SqlDbType.VarChar).Value = dto.TestPassFromDate;
                cmd.Parameters.Add("@viewdate_end", SqlDbType.VarChar).Value = dto.TestPassToDate;
                cmd.Parameters.Add("@primarydiscipline", SqlDbType.VarChar).Value = dto.PrimaryDiscipline;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<GnrlSummaryCourseActivityDTO>();
                while (reader.Read())
                {
                    GnrlSummaryCourseActivityDTO returnDto = new GnrlSummaryCourseActivityDTO();
                    returnDto.FacName = reader["facilityname"].ToString();
                    returnDto.UserFullName = reader["UserFullname"].ToString();
                    returnDto.DeptName = reader["DeptName"].ToString();
                    returnDto.AvgScore = reader["AverageScore"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AverageScore"]);
                    returnDto.NoOfCourses = reader["TotalCourse"] == DBNull.Value ? 0 : Convert.ToInt32(reader["TotalCourse"]);
                    returnDto.CreitHrs = reader["TotalCredit"] == DBNull.Value ? 0 : Convert.ToDouble(reader["TotalCredit"]);
                    returnDto.TestPassDate = reader["lastvisit"].ToString();
                    returnDto.PrimaryDiscipline = reader["PrimaryDiscipline"].ToString();
                    Result.Add(returnDto);                    
                }
            }
            return Result;
        }

        public List<GnrlSummaryCourseActivityDTO> GetGnrlSummaryCompletions(GnrlSummaryCourseActivityDTO dto)
        {
            List<GnrlSummaryCourseActivityDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_general_coursecompletions_summary";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@order_by", SqlDbType.VarChar).Value = dto.OrderBy;
                cmd.Parameters.Add("@org_id", SqlDbType.VarChar).Value = dto.OrgIds;
                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@viewdate_start", SqlDbType.VarChar).Value = dto.TestPassFromDate;
                cmd.Parameters.Add("@viewdate_end", SqlDbType.VarChar).Value = dto.TestPassToDate;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<GnrlSummaryCourseActivityDTO>();
                while (reader.Read())
                {
                    GnrlSummaryCourseActivityDTO returnDto = new GnrlSummaryCourseActivityDTO();
                    returnDto.FacName = reader["facilityname"].ToString();
                    returnDto.UserFullName = reader["UserFullname"].ToString();
                    returnDto.DeptName = reader["DeptName"].ToString();
                    returnDto.NoOfCourses = reader["NoOfCourses"] == DBNull.Value ? 0 : Convert.ToInt32(reader["NoOfCourses"]);
                    returnDto.CreitHrs = reader["CreitHrs"] == DBNull.Value ? 0 : Convert.ToDouble(reader["CreitHrs"]);
                    returnDto.Active = (bool)reader["isapproved"];
                    returnDto.PrimaryDiscipline = reader["primarydiscipline"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }


    }
}
