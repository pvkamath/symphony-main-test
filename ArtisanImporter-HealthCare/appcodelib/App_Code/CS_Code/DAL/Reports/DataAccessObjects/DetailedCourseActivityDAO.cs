﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data;


/// <summary>
/// Summary description for DetailedCourseActivityDAO
/// </summary>
namespace PearlsReview.DAL
{
    public class DetailedCourseActivityDAO : PRProvider3
    {
        public DetailedCourseActivityDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public List<DetailedCourseActivityDTO> GetCourseActivity(DetailedCourseActivityDTO dto)
        {
            List<DetailedCourseActivityDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();               
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_r_general_course_detail";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@org_id", SqlDbType.VarChar).Value = dto.OrgId;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@test_start", SqlDbType.VarChar).Value = dto.TestPassFromDate;
                cmd.Parameters.Add("@test_end", SqlDbType.VarChar).Value = dto.TestPassToDate;
                cmd.Parameters.Add("@type_id ", SqlDbType.VarChar).Value = dto.CourseType;
                 cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<DetailedCourseActivityDTO>();
                while (reader.Read())
                {
                    DetailedCourseActivityDTO returnDto = new DetailedCourseActivityDTO();
                         
                    returnDto.TopicName = reader["topicname"].ToString();
                    returnDto.CourseId = reader["course_number"].ToString();
                    returnDto.DeptName = reader["dept_name"].ToString();
                    returnDto.CreitHrs = reader["hours"] == DBNull.Value ? 0 : Convert.ToDouble(reader["hours"]);
                    returnDto.Score = reader["score"].ToString();
                    returnDto.UserFullName = reader["userfullname"].ToString();
                    returnDto.CourseType = reader["coursetype"].ToString();
                    returnDto.FacilityName = reader["facname"].ToString();
                    returnDto.TestPassDate = reader.IsDBNull(5) ? "" : Convert.ToDateTime(reader["lastmodWithTimeStamp"]).ToString();
                    returnDto.PrimaryDiscipline = reader["primarydiscipline"].ToString();
                    returnDto.Topic_hourshort = reader["topic_hourshort"].ToString();
                    Result.Add(returnDto);                    
                }
            }
            return Result;
        }
    }
}
