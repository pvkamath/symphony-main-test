﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for GnrlSummaryUtilizationByPeriod
/// </summary>
/// 
namespace PearlsReview.DAL
{
public class GnrlSummaryUtilizationByPeriodDAO: PRProvider3
{
	public GnrlSummaryUtilizationByPeriodDAO()
	{
        
	}

    public List<GnrlSummaryUtilizationByPeriodDTO> GetPeriodUtilization(GnrlSummaryUtilizationByPeriodDTO dto)

    {

            List<GnrlSummaryUtilizationByPeriodDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_general_dept_utilperiod";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@org_ids", SqlDbType.VarChar).Value = dto.OrgIds;
                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@dept_select", SqlDbType.VarChar).Value = dto.DeptSelect;
                cmd.Parameters.Add("@period_id", SqlDbType.VarChar).Value = dto.Period;
                //cmd.Parameters.Add("@hperiod", SqlDbType.VarChar).Value = dto.Hperiod;
                cmd.Parameters.Add("@viewdate_start", SqlDbType.VarChar).Value = dto.ViewDateStart;
                cmd.Parameters.Add("@viewdate_end", SqlDbType.VarChar).Value = dto.ViewDateEnd;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
                cmd.Parameters.Add("@primaryDiscipline", SqlDbType.VarChar).Value = dto.PrimaryDiscipline;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<GnrlSummaryUtilizationByPeriodDTO>();

                while (reader.Read())
                {
                    GnrlSummaryUtilizationByPeriodDTO returnDto = new GnrlSummaryUtilizationByPeriodDTO();
                   
                    //returnDto.FacName = reader["facname"].ToString();
                    //returnDto.DeptName = reader["dept_name"].ToString();
                    returnDto.Period = reader["tperiod"].ToString();
                    returnDto.tuser = reader["tuser"] == DBNull.Value ? 0 : Convert.ToDouble(reader["tuser"]);
                    returnDto.tcourse = reader["tcourse"] == DBNull.Value ? 0 : Convert.ToDouble(reader["tcourse"]);
                    returnDto.thour = reader["thour"] == DBNull.Value ? 0 : Convert.ToDouble(reader["thour"]);
                    returnDto.AvgUser = reader["avguser"] == DBNull.Value ? 0 : Convert.ToDouble(reader["avguser"]);
                    returnDto.AvgHour = reader["avghour"] == DBNull.Value ? 0 : Convert.ToDouble(reader["avghour"]);
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}

