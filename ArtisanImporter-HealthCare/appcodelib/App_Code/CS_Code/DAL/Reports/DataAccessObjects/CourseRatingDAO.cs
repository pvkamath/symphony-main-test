﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

/// <summary>
/// Summary description for CourseRatingDAO
/// </summary>
namespace PearlsReview.DAL
{
    public class CourseRatingDAO : PRProvider3
    {
        public CourseRatingDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public DataTable getCourseRating(string CourseNumber, string CourseName, string FlagStatus, string ActiveStatus, string StartDate, string EndDate)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_r_courseRating";
                cmd.Parameters.Add("@courseNumber", SqlDbType.VarChar).Value = CourseNumber.Replace("'","''");
                cmd.Parameters.Add("@topicName", SqlDbType.VarChar).Value = CourseName.Replace("'", "''");
                cmd.Parameters.Add("@flagStatus", SqlDbType.VarChar).Value = FlagStatus;
                cmd.Parameters.Add("@activeStatus", SqlDbType.VarChar).Value = ActiveStatus;
                cmd.Parameters.Add("@startDate", SqlDbType.VarChar).Value = StartDate;
                cmd.Parameters.Add("@endDate", SqlDbType.VarChar).Value = EndDate;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
        public DataTable getCourseRatingSummary(string CourseNumber, string CourseName, string FlagStatus, string ActiveStatus, string StartDate, string EndDate)
        {
            DataTable dt = new DataTable();
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_r_courseRating_summary";
                cmd.Parameters.Add("@courseNumber", SqlDbType.VarChar).Value = CourseNumber.Replace("'", "''");
                cmd.Parameters.Add("@topicName", SqlDbType.VarChar).Value = CourseName.Replace("'", "''");
                cmd.Parameters.Add("@flagStatus", SqlDbType.VarChar).Value = FlagStatus;
                cmd.Parameters.Add("@activeStatus", SqlDbType.VarChar).Value = ActiveStatus;
                cmd.Parameters.Add("@startDate", SqlDbType.VarChar).Value = StartDate;
                cmd.Parameters.Add("@endDate", SqlDbType.VarChar).Value = EndDate;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
            }
            return dt;
        }
    }
}
