﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for CECourseOfferDAO
/// </summary>

namespace PearlsReview.DAL
{
    public class CECourseOfferDAO : PRProvider3
    {
        public CECourseOfferDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public List<CECourseOfferDTO> GetSLPCourseOffer(CECourseOfferDTO dto)
        {
            List<CECourseOfferDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_slpUsers";
                cmd.CommandType = CommandType.StoredProcedure;
                
                cmd.Parameters.Add("@completed_start", SqlDbType.VarChar).Value = dto.CompletedStart;
                cmd.Parameters.Add("@completed_end", SqlDbType.VarChar).Value = dto.CompletedEnd;
                cmd.Parameters.Add("@course_number", SqlDbType.VarChar).Value = dto.Course_Number;
                cmd.Parameters.Add("@user_ID", SqlDbType.VarChar).Value = dto.UserID;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<CECourseOfferDTO>();
                while (reader.Read())
                {
                    CECourseOfferDTO returnDto = new CECourseOfferDTO();
                    returnDto.RowID = Convert.ToInt32(reader["RowID"].ToString());
                    returnDto.Iid = Convert.ToInt32(reader["iid"].ToString());
                    returnDto.cUserName = reader["cUserName"].ToString().Trim();
                    returnDto.cFirstName = reader["cFirstName"].ToString().Trim();
                    returnDto.cLastName = reader["cLastName"].ToString().Trim();
                    returnDto.cMiddle = reader["cMiddle"].ToString().Trim();
                    returnDto.caddress1 = reader["cAddress1"].ToString().Trim();
                    returnDto.caddress2 = reader["cAddress2"].ToString().Trim();
                    returnDto.ccity = reader["cCity"].ToString().Trim();
                    returnDto.cState = reader["cState"].ToString().Trim();
                    returnDto.cZipcode = reader["cZipCode"].ToString().Trim();
                    returnDto.cEmail = reader["cEmail"].ToString().Trim();
                    returnDto.PrimaryPhone = reader["PrimaryPhone"].ToString().Trim();
                    returnDto.cSocial = reader["cSocial"].ToString().Trim();
                    returnDto.Badge_id = reader["badge_id"].ToString().Trim();
                    returnDto.TopicID = reader["TopicID"].ToString().Trim();
                    returnDto.CourseNumber = reader["CourseNumber"].ToString().Trim();
                    returnDto.ASHA_ID = reader["ASHA_id"].ToString().Trim();
                    returnDto.CourseName = reader["CourseName"].ToString().Trim();
                    returnDto.Score = reader["Score"].ToString().Trim();
                    returnDto.CompletedDate = reader["CompletedDate"].ToString().Trim();
                    returnDto.Credits = reader["Credits"].ToString().Trim();
                    returnDto.IsSelected = reader["IsSelected"].ToString();
                    returnDto.NumberAttending = reader["NumberAttending"].ToString();
                    returnDto.PartForms = reader["PartForms"].ToString();
                    returnDto.QuarterNumber = reader["QuarterNumber"] == null ? "000" : reader["QuarterNumber"].ToString();

                    Result.Add(returnDto);                    
                }
            }
            return Result;
        }
    }
}
