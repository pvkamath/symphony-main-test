﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
	public class CourseUtilizationByDateRangeDAO : PRProvider3
	{
        public CourseUtilizationByDateRangeDAO()
        {           
        }
        public List<CourseUtilizationByDateRangeDTO> GetCourseUtilizationByDateRange(CourseUtilizationByDateRangeDTO dto)
        {
            List<CourseUtilizationByDateRangeDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_course_utilbydate";                
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@category_id", SqlDbType.VarChar).Value = dto.CategoryId;
                cmd.Parameters.Add("@course_id", SqlDbType.VarChar).Value = dto.CourseId;
                cmd.Parameters.Add("@viewdate_start", SqlDbType.VarChar).Value = dto.ViewDateStart;
                cmd.Parameters.Add("@viewdate_end", SqlDbType.VarChar).Value = dto.ViewDateEnd;                
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<CourseUtilizationByDateRangeDTO>();
                while (reader.Read())
                {
                    CourseUtilizationByDateRangeDTO returnDto = new CourseUtilizationByDateRangeDTO();
                    returnDto.Title = reader["Title"].ToString();
                    returnDto.TopicName = reader["TopicName"].ToString();
                    returnDto.Vnum = reader["Vnum"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Vnum"]);
                    returnDto.Tnum = reader["Tnum"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Tnum"]);
                    returnDto.Cnum = reader["Cnum"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Cnum"]);
                    Result.Add(returnDto);
                }
            }
            return Result;
        }       
	}
}
