﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class SalesByStateDAO : PRProvider3
    {
        public SalesByStateDAO()
        {
        }

        public List<SalesByStateDTO> GetSalesByState(SalesByStateDTO dto)
        {

            List<SalesByStateDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_SalesByStates";
                cmd.CommandType = CommandType.StoredProcedure;
               
                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDateStart;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDateEnd;
                cmd.Parameters.Add("@rType", SqlDbType.VarChar).Value = dto.RType;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.Parameters.Add("@callType", SqlDbType.Int).Value = 0;
                cn.Open();                
                Result = new List<SalesByStateDTO>();

                DataSet ds = new DataSet();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "SalesByState");

                foreach (DataRow l_Row in ds.Tables[0].Rows)
                {
                    SalesByStateDTO returnDto = new SalesByStateDTO();
                    returnDto.Address1 = Convert.ToString(l_Row["address1"]);
                    returnDto.Address2 = Convert.ToString(l_Row["address2"]);
                    returnDto.City = Convert.ToString(l_Row["city"]);
                   
                    returnDto.Comment = Convert.ToString(l_Row["comment"]);
                    returnDto.Country = Convert.ToString(l_Row["country"]);                    
                    returnDto.Email = Convert.ToString(l_Row["Email"]);
                    returnDto.FirstName = Convert.ToString(l_Row["firstname"]);
                    returnDto.LastName = Convert.ToString(l_Row["lastname"]);
                    returnDto.OrderDate = Convert.ToDateTime(l_Row["orderdate"]);
                    returnDto.OrderID = Convert.ToInt32 (l_Row["orderid"]);
                    returnDto.ShipCost = Convert.ToDecimal(l_Row["shipcost"]); //ShipH 
                    if (Convert.IsDBNull(l_Row["ship_ind"]) ? false : Convert.ToBoolean(l_Row["ship_ind"])== true )
                    {
                        returnDto.Ship_Ind = 1;
                    }
                    returnDto.State = Convert.ToString(l_Row["state"]);
                    returnDto.SubTotal = Convert.ToDecimal(l_Row["subtotal"]);
                    returnDto.Gtotal = Convert.ToDecimal(l_Row["Gtotal"]);
                    returnDto.Tax = Convert.ToDecimal(l_Row["tax"]);
                    returnDto.TotalCost = Convert.ToDecimal(l_Row["totalcost"]);
                    returnDto.Discount = Convert.ToDecimal(l_Row["couponamount"]);
                    returnDto.Couponcode = Convert.ToString(l_Row["couponcode"]);
                    returnDto.UserID = Convert.ToInt32(l_Row["userid"]);                    
                    if (Convert.ToBoolean(l_Row["validate_ind"]) == true)
                    {
                        returnDto.Validate_Ind = 1;
                    }
                    returnDto.Verification = Convert.ToString(l_Row["verification"]);
                    returnDto.Zip = Convert.ToString(l_Row["zip"]);

                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        /// <summary>
        /// Retrieves the Discount with the specified ID
        /// </summary>
        public SalesByStateDTO GetSalesByStateGTotal(SalesByStateDTO dto)
        {            
            SalesByStateDTO returnDto = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_SalesByStates";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDateStart;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDateEnd;
                cmd.Parameters.Add("@rType", SqlDbType.VarChar).Value = dto.RType;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.Parameters.Add("@callType", SqlDbType.Int).Value = 1;
                cn.Open();
              

                DataSet ds = new DataSet();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "SalesByState");

                foreach (DataRow l_Row in ds.Tables[0].Rows)
                {
                    returnDto = new SalesByStateDTO(); 
                    returnDto.ShipSum = l_Row["ShipSum"] == DBNull.Value ? 0 : Convert.ToDecimal(l_Row["ShipSum"]);
                    returnDto.TaxSum = l_Row["taxSum"] == DBNull.Value ? 0 : Convert.ToDecimal(l_Row["taxSum"]);
                    returnDto.SumTotal = l_Row["sumSub"] == DBNull.Value ? 0 : Convert.ToDecimal(l_Row["sumSub"]);
                    returnDto.SumGtotal = l_Row["sumGtotal"] == DBNull.Value ? 0 : Convert.ToDecimal(l_Row["sumGtotal"]);
                    returnDto.SumTotal = l_Row["sumTotal"] == DBNull.Value ? 0 : Convert.ToDecimal(l_Row["sumTotal"]);
                    returnDto.SumDiscount = l_Row["sumDiscount"] == DBNull.Value ? 0 : Convert.ToDecimal(l_Row["sumDiscount"]);                    
                }

                return returnDto;
            }
        }        
       
        public List<SalesByStateDTO> GetSalesByStateTotal(SalesByStateDTO dto)
        {            
            List<SalesByStateDTO> Result = null;          
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_SalesByStates";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDateStart;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDateEnd;
                cmd.Parameters.Add("@rType", SqlDbType.VarChar).Value = dto.RType;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.Parameters.Add("@callType", SqlDbType.Int).Value = 2;
                cn.Open();                
                Result = new List<SalesByStateDTO>();

                DataSet ds = new DataSet();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "SalesByState");

                foreach (DataRow l_Row in ds.Tables[0].Rows)
                {
                    SalesByStateDTO returnDto = new SalesByStateDTO();
                    returnDto.StateName = Convert.ToString(l_Row["statename"]);
                    returnDto.SubTotal = Convert.ToDecimal(l_Row["sumSub"]);
                    returnDto.Tax = Convert.ToDecimal(l_Row["taxrate"]);
                    returnDto.TaxSum = Convert.ToDecimal(l_Row["taxSum"]);
                    returnDto.ShipSum = Convert.ToDecimal(l_Row["ShipSum"]);
                    returnDto.SumDiscount= Convert.ToDecimal(l_Row["sumDiscount"]);
                    returnDto.SumGtotal = Convert.ToDecimal(l_Row["sumGTotal"]);
                    returnDto.SumTotal = Convert.ToDecimal(l_Row["sumTotal"]);                       
                    Result.Add(returnDto);
                }
            }
            return Result;
        }

        public List<SalesByStateDTO> GetMSSalesByState(SalesByStateDTO dto)
        {
            List<SalesByStateDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_MSSalesByStates";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDateStart;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDateEnd;
                cmd.Parameters.Add("@rType", SqlDbType.VarChar).Value = dto.RType;                                
                cn.Open();
                Result = new List<SalesByStateDTO>();

                DataSet ds = new DataSet();
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                adap.Fill(ds, "SalesByState");

                foreach (DataRow l_Row in ds.Tables[0].Rows)
                {
                    SalesByStateDTO returnDto = new SalesByStateDTO();
                    returnDto.Address1 = Convert.ToString(l_Row["address1"]);
                    returnDto.Address2 = Convert.ToString(l_Row["address2"]);
                    returnDto.City = Convert.ToString(l_Row["city"]);
                    returnDto.Couponcode = Convert.ToString(l_Row["couponcode"]);
                    returnDto.FirstName = Convert.ToString(l_Row["firstname"]);
                    returnDto.LastName = Convert.ToString(l_Row["lastname"]);
                    returnDto.OrderDate = Convert.ToDateTime(l_Row["orderdate"]);
                    returnDto.OrderID = Convert.ToInt32(l_Row["orderid"]);
                    returnDto.Verification = Convert.ToString(l_Row["Verification"]);
                    returnDto.Comment = Convert.ToString(l_Row["Comment"]);
                    returnDto.ShipCost = Convert.ToDecimal(l_Row["shipcost"]); //ShipH                     
                    returnDto.State = Convert.ToString(l_Row["state"]);
                    returnDto.SubTotal = Convert.ToDecimal(l_Row["subtotal"]);                    
                    returnDto.Tax = Convert.ToDecimal(l_Row["tax"]);
                    returnDto.TotalCost = Convert.ToDecimal(l_Row["total"]);
                    returnDto.Discount = Convert.ToDecimal(l_Row["coupon"]);
                    returnDto.StateName = Convert.ToString(l_Row["statename"]);
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}