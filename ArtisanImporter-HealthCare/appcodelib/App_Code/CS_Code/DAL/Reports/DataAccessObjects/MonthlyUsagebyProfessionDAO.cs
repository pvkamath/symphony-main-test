﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for MonthlyUsagebyProfessionDAO
    /// </summary>

    public class MonthlyUsagebyProfessionDAO : PRProvider3
    {
        public MonthlyUsagebyProfessionDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public List<MonthlyUsagebyProfessionDTO> GetGnrlMonthlyUsageByProfession(MonthlyUsagebyProfessionDTO dto)
        {
            List<MonthlyUsagebyProfessionDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_general_YearlyUsageByProfession";
                cmd.CommandType = CommandType.StoredProcedure;                
                cmd.Parameters.Add("@org_id", SqlDbType.VarChar).Value = dto.OrgIds;
                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@viewdate_start", SqlDbType.VarChar).Value = dto.TestPassFromDate;
                cmd.Parameters.Add("@viewdate_end", SqlDbType.VarChar).Value = dto.TestPassToDate;                 
                cmd.Parameters.Add("@primarydiscipline", SqlDbType.VarChar).Value = dto.PrimaryDiscipline;
                cmd.Parameters.Add("@order_by", SqlDbType.VarChar).Value = dto.OrderBy;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<MonthlyUsagebyProfessionDTO>();
                while (reader.Read())
                {
                    MonthlyUsagebyProfessionDTO returnDto = new MonthlyUsagebyProfessionDTO();                    
                    returnDto.FacName = reader["facilityname"].ToString();
                    returnDto.January = reader["Jan"].ToString();
                    returnDto.February = reader["Feb"].ToString();
                    returnDto.March = reader["Mar"].ToString();
                    returnDto.April = reader["Apr"].ToString();
                    returnDto.May = reader["May"].ToString();
                    returnDto.June = reader["Jun"].ToString();
                    returnDto.July = reader["Jul"].ToString();
                    returnDto.August = reader["Aug"].ToString();
                    returnDto.September = reader["Sep"].ToString();
                    returnDto.October = reader["Oct"].ToString();
                    returnDto.November = reader["Nov"].ToString();
                    returnDto.December = reader["Dec"].ToString();
                    returnDto.GrandTotal = reader["GrandTotal"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}
