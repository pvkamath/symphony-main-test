﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class RevenueByCourseDAO : PRProvider3
    {
        public RevenueByCourseDAO()
        {
        }

        public List<RevenueByCourseDTO> GetRevenueByCourse(RevenueByCourseDTO dto)
        {

            List<RevenueByCourseDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_RevenueByCourse";
                cmd.CommandType = CommandType.StoredProcedure;                
                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDate_start;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDate_end;
                cmd.Parameters.Add("@course_number", SqlDbType.VarChar).Value = dto.CourseNumber;
                cmd.Parameters.Add("@Show_Sponsored", SqlDbType.VarChar).Value = dto.ShowSponsored;
                cmd.Parameters.Add("@Show_Unlimited_Users", SqlDbType.VarChar).Value = dto.ShowUnlimitedUsers;
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                //cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<RevenueByCourseDTO>();

                while (reader.Read())
                {
                    RevenueByCourseDTO returnDto = new RevenueByCourseDTO();
                   // returnDto.CategoryName = reader["CategoryName"].ToString();
                    returnDto.CourseNumber = reader["course_number"].ToString();
                    returnDto.SumOfCost = Convert.ToDecimal(reader["totalcost"]);
                    returnDto.SumOfQuantity = Convert.ToInt32(reader["SumOfQuantity"]);
                    returnDto.Title = reader["Title"].ToString();
                    returnDto.sumOfOffLineQunatity = (reader["offlineQ"] == DBNull.Value ? 0 : Convert.ToInt32(reader["offlineQ"]));
                    returnDto.sumOfOnLineQunatity = (reader["onlineQ"] == DBNull.Value ? 0 : Convert.ToInt32(reader["onlineQ"]));                    
                    returnDto.sumOfOffLineSubTotal = (reader["offlinesubtotal"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["offlinesubtotal"]));
                    returnDto.sumOfOnLineSubTotal = (reader["onlinesubtotal"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["onlinesubtotal"])); 
                    returnDto.sumOfOffLineTotalCost=(reader["offlinetotalcost"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["offlinetotalcost"]));
                    returnDto.sumOfOnLineTotalCost = (reader["onlinetotalcost"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["onlinetotalcost"]));
                    returnDto.sumOfShip = (reader["offlineship"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["offlineship"]));
                    returnDto.sumOfTax = (reader["offlinetax"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["offlinetax"]));
                    returnDto.sumOfSubTotal=(reader["subtotal"] == DBNull.Value ? 0 :Convert.ToDecimal(reader["subtotal"]));
                    Result.Add(returnDto);
                }
            }
            return Result;
        }

        public List<RevenueByCourseDTO> GetMSRevenueByCourse(RevenueByCourseDTO dto)
        {

            List<RevenueByCourseDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_msRevenueByCourse";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDate_start;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDate_end;
                cmd.Parameters.Add("@course_number", SqlDbType.VarChar).Value = dto.CourseNumber;
                cmd.Parameters.Add("@sponsorCourses", SqlDbType.VarChar).Value = dto.ShowSponsored;
                cmd.Parameters.Add("@sortBy", SqlDbType.VarChar).Value = "";
                //cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<RevenueByCourseDTO>();

                while (reader.Read())
                {
                    RevenueByCourseDTO returnDto = new RevenueByCourseDTO();
                    
                    returnDto.CourseNumber = reader["course_number"].ToString();                    
                    returnDto.TopicID = Convert.ToInt32(reader["TopicID"].ToString());
                    returnDto.Title = reader["TopicName"].ToString();

                    returnDto.sumOfOffLineQunatity = (reader["offlineQuantity"] == DBNull.Value ? 0 : Convert.ToInt32(reader["offlineQuantity"]));
                    returnDto.sumOfOffLineSubTotal = (reader["offlinesubtotal"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["offlinesubtotal"]));
                    returnDto.sumOfOffLineTotalCost = (reader["offlinecost"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["offlinecost"]));

                    returnDto.sumOfOnLineQunatity = (reader["onlineQuantity"] == DBNull.Value ? 0 : Convert.ToInt32(reader["onlineQuantity"]));
                    returnDto.sumOfOnLineSubTotal = (reader["onlinesubtotal"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["onlinesubtotal"]));
                    returnDto.sumOfOnLineTotalCost = (reader["onlinecost"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["onlinecost"]));
                    returnDto.sumOfSubTotal = (reader["subtotal"] == DBNull.Value ? 0 : Convert.ToDecimal(reader["subtotal"]));

                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}