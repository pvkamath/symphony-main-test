﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class DepartmentalUserTranscriptsDAO : PRProvider3
{
	public DepartmentalUserTranscriptsDAO()
	{	
	}
    public List<DepartmentalUserTranscriptsDTO> GetDeptUserTranscripts(DepartmentalUserTranscriptsDTO dto)
    {
        List<DepartmentalUserTranscriptsDTO> Result = null;
        using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = "sp_r_dept_usertrans";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
            cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
            cmd.Parameters.Add("@dept_select", SqlDbType.VarChar).Value = dto.DeptSelect;
            cmd.Parameters.Add("@active_ind", SqlDbType.VarChar).Value = dto.ActiveInd;
            cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;

            cn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            Result = new List<DepartmentalUserTranscriptsDTO>();
            while (reader.Read())
            {
                DepartmentalUserTranscriptsDTO returnDto = new DepartmentalUserTranscriptsDTO();
   
                returnDto.FullName = reader["fullname"].ToString();
                returnDto.DeptName  = reader["deptname"].ToString();
                returnDto.CourseTitle = reader["coursetitle"].ToString();
                returnDto.PassDate  = reader["passdate"].ToString();
                //returnDto.PassDate = reader["passdate"].ToString();
                returnDto.PassDate = Convert.ToDateTime(reader["passdate"]).ToShortDateString();
                returnDto.Score   = reader["score"].ToString();
                returnDto.Credits = reader["credits"].ToString();

                Result.Add(returnDto);
            }
        }
        return Result;
    }
}
}
