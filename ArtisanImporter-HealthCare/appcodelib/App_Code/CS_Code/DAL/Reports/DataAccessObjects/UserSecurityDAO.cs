﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class UserSecurityDAO : PRProvider3
    {
        public UserSecurityDAO()
        {  
        }
        public List<UserSecurityDTO> GetUserSecurity(UserSecurityDTO dto)
        {
            List<UserSecurityDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_user_security";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@user_name", SqlDbType.VarChar).Value = dto.UserName;
                cmd.Parameters.Add("@department_id", SqlDbType.VarChar).Value = dto.DepartmentID;
                cmd.Parameters.Add("@ssn_id", SqlDbType.VarChar).Value = dto.SsnID;
                cmd.Parameters.Add("@badge_id", SqlDbType.VarChar).Value = dto.BadgeID;
                cmd.Parameters.Add("@lastlogin_start", SqlDbType.VarChar).Value = dto.LastLoginStart;
                cmd.Parameters.Add("@lastlogin_end", SqlDbType.VarChar).Value = dto.LastLoginEnd;
                cmd.Parameters.Add("@user_active", SqlDbType.VarChar).Value = dto.UserActive;
                cmd.Parameters.Add("@position_id", SqlDbType.VarChar).Value = dto.PositionID;
                //cmd.Parameters.Add("@user_login", SqlDbType.VarChar).Value = dto.UserLogin;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
                cmd.Parameters.Add("@PrimaryDiscipline", SqlDbType.VarChar).Value = dto.PrimaryDiscipline;

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<UserSecurityDTO>();
                while (reader.Read())
                {
                    UserSecurityDTO returnDto = new UserSecurityDTO();
                    returnDto.UserLastName = reader["clastname"].ToString();
                    returnDto.UserFirstName = reader["cfirstname"].ToString();
                    returnDto.UserMiddleName = reader["cmiddle"].ToString();
                    returnDto.UserFullName = reader["userfullname"].ToString();
                    returnDto.DeptName = reader["dept_name"].ToString();
                    returnDto.UserName = reader["cusername"].ToString();                    
                    returnDto.SsnID = (reader["csocial"].ToString()).Trim();  
                    returnDto.BadgeID = reader["badge_id"].ToString();
                    returnDto.IsActive = reader["isactive"].ToString();
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.Created = reader["created"].ToString();
                    returnDto.FacilityName = reader["facname"].ToString();
                    returnDto.Title = reader["title"].ToString();
                    returnDto.PrimaryDiscipline = reader["primarydiscipline"].ToString();

                    string date = "01/01/2000";
                    DateTime dt = Convert.ToDateTime(date);                    
                    if (reader["lastlogin"].ToString() != "")
                    {
                        DateTime dt1 = Convert.ToDateTime(reader["lastlogin"].ToString());
                        if (dt1 >= dt)
                        {
                            returnDto.LastLogin = reader["lastlogin"].ToString();
                        }
                        else
                        {
                            returnDto.LastLogin = "";
                        }
                    }
                    else
                    {
                        returnDto.LastLogin = "";
                    }                        
                    //returnDto.IsLogin = reader["islogin"].ToString();                     
                    
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}
