﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;


/// <summary>
/// Summary description for CourseActivityDAO
/// </summary>
namespace PearlsReview.DAL
{
    public class CourseActivityDAO : PRProvider3
    {
	    public CourseActivityDAO()
	    {
	    }
        public DataSet GetCourseActivity(CourseActivityDTO dto)
        {
            //List<CourseActivityDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_r_course_activity";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@category_id", SqlDbType.VarChar).Value = dto.CategoryId;
                cmd.Parameters.Add("@course_id", SqlDbType.VarChar).Value = dto.CourseId;
                cmd.Parameters.Add("@user_first", SqlDbType.VarChar).Value = dto.UserFirstName.Replace("'", "\"");
                cmd.Parameters.Add("@user_name", SqlDbType.VarChar).Value = dto.UserId;
                cmd.Parameters.Add("@user_last", SqlDbType.VarChar).Value = dto.UserLastName.Replace("'", "\"");
                //cmd.Parameters.Add("@user_title", SqlDbType.VarChar).Value = dto.Title;
                //cmd.Parameters.Add("@user_badge", SqlDbType.VarChar).Value = dto.BadgeID.Replace("'", "\"");
                //cmd.Parameters.Add("@user_ssn", SqlDbType.VarChar).Value = dto.sSN.Replace("'", "\"");
                cmd.Parameters.Add("@user_active", SqlDbType.VarChar).Value = dto.IsActive;                
                cmd.Parameters.Add("@dept_select", SqlDbType.VarChar).Value = dto.DeptSelect;
                cmd.Parameters.Add("@credit_hour", SqlDbType.VarChar).Value = dto.CreitHrs;
                //cmd.Parameters.Add("@viewdate_start", SqlDbType.VarChar).Value = dto.ViewFromDate;
                //cmd.Parameters.Add("@viewdate_end", SqlDbType.VarChar).Value = dto.ViewToDate;
                cmd.Parameters.Add("@test_start", SqlDbType.VarChar).Value = dto.TestPassFromDate;
                cmd.Parameters.Add("@test_end", SqlDbType.VarChar).Value = dto.TestPassToDate;
                //cmd.Parameters.Add("@print_start", SqlDbType.VarChar).Value = dto.PrintedFromDate;
                //cmd.Parameters.Add("@print_end", SqlDbType.VarChar).Value = dto.PrintedToDate;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
                cmd.Parameters.Add("@License_Include", SqlDbType.VarChar).Value = dto.License_Include;
                cmd.Parameters.Add("@primaryDiscipline", SqlDbType.VarChar).Value = dto.PrimaryDiscipline;
                
                adapter = new SqlDataAdapter(cmd);
                                adapter.Fill(ds);  
                             
                
            }
            return ds;
        }
    }

}