﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for EvaluationSummaryDAO
/// </summary>

namespace PearlsReview.DAL
{

    public class EvaluationSummaryDAO : PRProvider3
    {
        public EvaluationSummaryDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public List<EvaluationSummaryDTO> GetEvaluationSummary(EvaluationSummaryDTO dto)
        {
            List<EvaluationSummaryDTO> result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))

            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Survey_Evaluation";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@topic_ids", SqlDbType.VarChar).Value = dto.TopicID;
                cmd.Parameters.Add("@surveydate_start", SqlDbType.VarChar).Value = dto.SurveyStartDate;
                cmd.Parameters.Add("@surveydate_end", SqlDbType.VarChar).Value = dto.SurveyEndDate;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.orderBy;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                result = new List<EvaluationSummaryDTO>();
                while (reader.Read())
                {
                    EvaluationSummaryDTO returnDto = new EvaluationSummaryDTO();
                    returnDto.QNumber = reader["qnumber"].ToString();
                    returnDto.QText = reader["qtext"].ToString();
                    returnDto.QAnswer = reader["qanswer"].ToString();
                    returnDto.CountQAnswer = reader["cqanswer"].ToString();
                    returnDto.Percentage = reader["percentage"].ToString();
                    //returnDto.Course = reader["course"].ToString();
                    result.Add(returnDto);

                }

           }
            return result;
        }



        public List<EvaluationSummaryDTO> GetCourseSummary(EvaluationSummaryDTO dto)
        {
            List<EvaluationSummaryDTO> result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Course_Summary";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@topic_ids", SqlDbType.VarChar).Value = dto.TopicID;
                cmd.Parameters.Add("@surveydate_start", SqlDbType.VarChar).Value = dto.SurveyStartDate;
                cmd.Parameters.Add("@surveydate_end", SqlDbType.VarChar).Value = dto.SurveyEndDate;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.orderBy;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                result = new List<EvaluationSummaryDTO>();
                while (reader.Read())
                {
                    EvaluationSummaryDTO returnDto = new EvaluationSummaryDTO();
                    returnDto.topic_name = reader["topicname"].ToString();
                    returnDto.course_number = reader["course_number"].ToString();
                    returnDto.total_completion = (int)reader["total"];
                    result.Add(returnDto);

                }

            }
            return result;
        }
    }
}