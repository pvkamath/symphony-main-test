﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class DepartmentUtilizationDAO : PRProvider3
    {
	    public DepartmentUtilizationDAO()
	    {
        }
        public List<DepartmentUtilizationDTO> GetDeptUtilization(DepartmentUtilizationDTO dto)
        {
            List<DepartmentUtilizationDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_general_dept_utilsummary";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@org_ids", SqlDbType.VarChar).Value = dto.OrgIds;
                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@dept_select", SqlDbType.VarChar).Value = dto.DeptSelect;
                //cmd.Parameters.Add("@credit_hour", SqlDbType.VarChar).Value = dto.CreitHrs;
                cmd.Parameters.Add("@viewdate_start", SqlDbType.VarChar).Value = dto.ViewDateStart;
                cmd.Parameters.Add("@viewdate_end", SqlDbType.VarChar).Value = dto.ViewDateEnd;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
                cmd.Parameters.Add("@primarydiscipline", SqlDbType.VarChar).Value = dto.PrimaryDiscipline;                
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<DepartmentUtilizationDTO>();

                while (reader.Read())
                {
                    DepartmentUtilizationDTO returnDto = new DepartmentUtilizationDTO();
                    returnDto.FacName = reader["facname"].ToString();
                    returnDto.DeptName = reader["DeptName"].ToString();
                    returnDto.ContactHours = reader["tnum"] == DBNull.Value ? 0 : Convert.ToDouble(reader["tnum"]);
                    returnDto.CompleteHours = reader["cnum"] == DBNull.Value ? 0 : Convert.ToDouble(reader["cnum"]);
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}