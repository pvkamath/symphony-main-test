﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

/// <summary>
/// Summary description for AllCoursesByCategoryDAO
/// </summary>
namespace PearlsReview.DAL
{
    public class AllCoursesByCategoryDAO : PRProvider3
    {
        public AllCoursesByCategoryDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet GetCourseByCategory(AllCoursesByCategoryDTO dto)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_courses_bycatalog";
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.Parameters.Add("@areaid", SqlDbType.Int).Value = dto.AreaId;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }


        public DataSet GetRetailCourseByCategory(AllCoursesByCategoryDTO dto)
        {
           // List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_Retail_courses_bycatalog";
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }

        public DataSet GetRetailStateCourseByCategory(AllCoursesByCategoryDTO dto)
        {
            // List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_Retail_Statecourses_bycatalog";
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }       
        

        public DataSet GetUnlImitedCourseByCategory(AllCoursesByCategoryDTO dto)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_Retail_Unlimcourses_bycatalog";
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }

        public DataSet GetSponsorCourseByCategory(AllCoursesByCategoryDTO dto)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_free_courses_bycatalog";
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }
        public DataSet GetMicrositeSponsorCourseByCategory(int DomainNameID, AllCoursesByCategoryDTO dto)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_microsite_free_courses_bycatalog";
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = DomainNameID;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }
        public DataSet GetMicrositeCourseByPickType(int DomainNameID, string PickType)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                if (PickType == "")
                {
                    cmd.CommandText = "sp_get_microsite_all_courses";                    
                }
                else
                {
                    cmd.CommandText = "sp_get_microsite_courses_ByPickType";
                    cmd.Parameters.Add("@PickType", SqlDbType.VarChar).Value = PickType;
                }
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = DomainNameID;                
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }
        public DataSet GetMicrositeAllCourseByPickType(int DomainNameID, string PickType)
        {            
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();                
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_microsite_all_courses_ByPickType";
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = DomainNameID;
                cmd.Parameters.Add("@PickType", SqlDbType.VarChar).Value = PickType;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }
        public DataSet GetMicrositeNewCourseByDomainId(int DomainNameID)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_microsite_New_courses";                
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = DomainNameID;               
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }
        
        public DataSet GetSponsorCourse(AllCoursesByCategoryDTO dto)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_free_courses";
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }
        public DataSet GetMicrositeSponsorCourse(int DomainNameID)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_microsite_free_courses";
                //cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.Parameters.Add("@msid", SqlDbType.Int).Value = DomainNameID;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }
        public DataSet GetWebinarCourseByCategory(AllCoursesByCategoryDTO dto)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_webinar_bycatalog";
                cmd.Parameters.Add("@facilityid", SqlDbType.Int).Value = dto.FacilityId;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }

        public DataSet GetTopicsBySearchText(string cSearchText, string cSortExpression)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();               
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_Get_Topics_By_SearchText";
                cmd.Parameters.Add("@SearchText", SqlDbType.VarChar).Value = cSearchText;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = cSortExpression;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }

        public DataSet GetCategoriesBySearchText(string cSearchText, string cSortExpression)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_Get_Categories_By_SearchText";
                cmd.Parameters.Add("@SearchText", SqlDbType.VarChar).Value = cSearchText;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = cSortExpression;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }
        public DataSet GetBundleCourseByDomainIdAndTopicId(int DomainNameID, int TopicId)
        {
            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_get_bundlecourses_bytopicid";
                cmd.Parameters.Add("@domainid", SqlDbType.Int).Value = DomainNameID;
                cmd.Parameters.Add("@topicid", SqlDbType.Int).Value = TopicId;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }
    }
}
