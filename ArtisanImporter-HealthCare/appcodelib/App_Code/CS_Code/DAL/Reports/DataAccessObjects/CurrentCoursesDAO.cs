﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;



namespace PearlsReview.DAL
{
    public class CurrentCoursesDAO : PRProvider3
    {
        public CurrentCoursesDAO()
        {

        }

        public List<CurrentCoursesDTO> GetCurrentCourses(CurrentCoursesDTO dto)
        {
            List<CurrentCoursesDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_CurrentCourses";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@org_id", SqlDbType.VarChar).Value = dto.OrgIds;
                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                //if (!String.IsNullOrEmpty(dto.DeptIds))
                //    cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@type_id", SqlDbType.VarChar).Value = dto.CourseType;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<CurrentCoursesDTO>();
                while (reader.Read())
                {
                    CurrentCoursesDTO returnDto = new CurrentCoursesDTO();
                    try { returnDto.FacName = reader["facilityname"].ToString(); }
                    catch { }
                    returnDto.TopicID = reader["topicid"].ToString();
                    returnDto.CourseNumber = reader["course_number"].ToString();
                    returnDto.CourseType = reader["coursetype"].ToString();
                    returnDto.TopicName = reader["topicname"].ToString();
                    returnDto.CourseType = reader["coursetype"].ToString();
                    returnDto.TotalHrs = Convert.ToInt32(reader["totalhrs"]);
                    returnDto.Hours = reader["hours"] == DBNull.Value ? 0 : Convert.ToDouble(reader["hours"]);
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}