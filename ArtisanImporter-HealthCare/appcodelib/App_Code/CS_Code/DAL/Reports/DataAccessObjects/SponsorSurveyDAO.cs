﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for SponsorSurveyDAO
    /// </summary>
    public class SponsorSurveyDAO : PRProvider3
    {
        public SponsorSurveyDAO()
        { }

        public List<SponsorSurveyDTO> GetSponsorSurvey(SponsorSurveyDTO dto)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Sponsor_Survey";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.Int).Value = dto.DiscountId;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.QuestionID = Convert.ToInt32(reader["QuestionID"]);
                    returnDto.UserID = Convert.ToInt32(reader["UserID"]);
                    returnDto.OrderItemID = Convert.ToInt32(reader["OrderItemID"]);
                    returnDto.UserName = reader["FirstName"].ToString();
                    returnDto.FirstName = reader["FirstName"].ToString();
                    returnDto.LastName = reader["LastName"].ToString();
                    returnDto.Address1 = reader["Address1"].ToString();
                    returnDto.Address2 = reader["Address2"].ToString();
                    returnDto.City = reader["City"].ToString();
                    returnDto.State = reader["State"].ToString();
                    returnDto.Zip = reader["Zip"].ToString();
                    returnDto.Email = reader["Email"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.QuestionText = reader["qText"].ToString();
                    returnDto.AnswerText = reader["aText"].ToString();
                    returnDto.AgeGroup = reader["agegroup"] == DBNull.Value ? 0 : Convert.ToInt32(reader["agegroup"]);
                    //returnDto.PositionId = reader["position_id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["position_id"]);
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.OrderDate = reader["orderdate"].ToString() != string.Empty ? string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(reader["orderdate"].ToString())) : "";
                    Result.Add(returnDto);
                }
            }
            return Result;
        }

        public List<SponsorSurveyDTO> GetSponsorSurveyQuestions(SponsorSurveyDTO dto)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Sponsor_Survey_Questions";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.Int).Value = dto.DiscountId;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.QuestionID = Convert.ToInt32(reader["QuestionID"]);
                    returnDto.QuestionText = reader["survey_text"].ToString();
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public int GetTotSurveys(int DiscountID)
        {
            SqlCommand cmd;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                cmd = new SqlCommand("select Count(*) AS Tot from OrderItem JOIN Orders ON Orders.OrderID = OrderItem.OrderID WHERE OrderItem.DiscountId = " + DiscountID, cn);
                cn.Open();
                return (int)cmd.ExecuteScalar();
            }

        }


        public List<SponsorSurveyDTO> GetSponsorEmailList(SponsorSurveyDTO dto)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Sponsor_Email_List";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = dto.DiscountId;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.FirstName = reader["firstname"].ToString();
                    returnDto.LastName = reader["lastname"].ToString();
                    returnDto.Email = reader["email"].ToString();
                    returnDto.Address1 = reader["address1"].ToString();
                    returnDto.Address2 = reader["address2"].ToString();
                    returnDto.City = reader["city"].ToString();
                    returnDto.State = reader["state"].ToString();
                    returnDto.Zip = reader["zip"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.AgeGroup = reader["agegroup"] == DBNull.Value ? 0 : Convert.ToInt32(reader["agegroup"]);
                    //returnDto.PositionId = reader["position_id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["position_id"]);
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    returnDto.OrderDate = reader["orderdate"].ToString() != string.Empty ? string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(reader["orderdate"].ToString())) : "";
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public List<SponsorSurveyDTO> GetSponsorEmailListP(SponsorSurveyDTO dto)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Sponsor_Email_List_With_Profession";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Discountid", SqlDbType.VarChar).Value = dto.DiscountId;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.FirstName = reader["firstname"].ToString();
                    returnDto.LastName = reader["lastname"].ToString();
                    returnDto.Email = reader["email"].ToString();
                    returnDto.Address1 = reader["address1"].ToString();
                    returnDto.Address2 = reader["address2"].ToString();
                    returnDto.City = reader["city"].ToString();
                    returnDto.State = reader["state"].ToString();
                    returnDto.Zip = reader["zip"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                   // returnDto.AgeGroup = reader["agegroup"] == DBNull.Value ? 0 : Convert.ToInt32(reader["agegroup"]);
                    //returnDto.PositionId = reader["position_id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["position_id"]);
                    returnDto.Education = reader["profession"].ToString();
                    returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    returnDto.OrderDate = reader["orderdate"].ToString() != string.Empty ? string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(reader["orderdate"].ToString())) : "";
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        public List<SponsorSurveyDTO> GetSponsorEmailListByCourse(SponsorSurveyDTO dto)
        {

            List<SponsorSurveyDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_Sponsor_Email_List_by_Course";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = dto.StartDate;
                cmd.Parameters.Add("@EndDate", SqlDbType.VarChar).Value = dto.EndDate;
                cmd.Parameters.Add("@course_number", SqlDbType.VarChar).Value = dto.CourseNumber;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<SponsorSurveyDTO>();

                while (reader.Read())
                {
                    SponsorSurveyDTO returnDto = new SponsorSurveyDTO();
                    returnDto.FirstName = reader["firstname"].ToString();
                    returnDto.LastName = reader["lastname"].ToString();
                    returnDto.Email = reader["email"].ToString();
                    returnDto.Address1 = reader["address1"].ToString();
                    returnDto.Address2 = reader["address2"].ToString();
                    returnDto.City = reader["city"].ToString();
                    returnDto.State = reader["state"].ToString();
                    returnDto.Zip = reader["zip"].ToString();
                    returnDto.Phone = reader["cphone"].ToString();
                    returnDto.AgeGroup = reader["agegroup"] == DBNull.Value ? 0 : Convert.ToInt32(reader["agegroup"]);
                    returnDto.PositionId = reader["position_id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["position_id"]);
                    //returnDto.PositionName = reader["position_name"].ToString();
                    returnDto.Specid = reader["specid"].ToString();
                    returnDto.OrderDate = reader["orderdate"].ToString() != string.Empty ? string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(reader["orderdate"].ToString())) : "";
                    Result.Add(returnDto);
                }
            }
            return Result;
        }

        public DataSet GetSponsorSurveyDetails(SponsorSurveyDTO dto)
        {

            //List<AllCoursesByCategoryDTO> Result = null;
            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                //SqlDataReader reader = cmd.ExecuteReader();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "sp_r_Sponsor_Survey_Summary";
                cmd.Parameters.Add("@Discountid", SqlDbType.Int).Value = dto.DiscountId;
                cmd.CommandType = CommandType.StoredProcedure;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;



        }

    }
}

