﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
	public class LookupDAO : PRProvider3
	{
		public LookupDAO()
		{
		}

		/// <summary>
		/// Retrieves all Course Categories for the given filter
		/// </summary>
		/// <param name="FacIds">0 = all ,  otherwise user FacIds</param>
		/// <returns>list of Course Categories</returns>

		public List<LookupDTO> GetAllCourseCategories(string FacIds)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{


				//SqlCommand cmd = new SqlCommand("select id, case facilityid when 0 then 'CE Course -'+title when -1 then 'Review -' +title when -2 then 'PR Textbook-'+title else 'Facility -'+title end as title,Facilityid from categories  where Facilityid in (0,-1," + FacIds + ") order by title", cn);                
				String StrWhere = "";

				if (FacIds != String.Empty)
				{
					StrWhere = " where  Facilityid in (0,-1," + FacIds + ")";
				}
				else
				{
					StrWhere = " where  Facilityid in (0,-1 )";
				}

				String Qry = "select id, case facilityid when 0 then 'CE Course -'+title when -1 then 'Review -' +title when -2 then 'PR Textbook-'+title else 'Facility -'+title end as title,Facilityid from categories " + StrWhere + "order by title";
				SqlCommand cmd = new SqlCommand(Qry, cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO returnDto = new LookupDTO();
					returnDto.ItemValue = reader.GetInt32(0).ToString();
					returnDto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);

					result.Add(returnDto);
				}
			}
			return result;
		}


		/// <summary>
		/// Retrieves all  Categories for the given filter
		/// </summary>
		/// <param name="FacIds">0 = all ,  otherwise user FacIds</param>
		/// <returns>list of  Categories</returns>

		public List<LookupDTO> GetAllCategories()
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				String Qry = "select id, tabname + ' - ' + Categories.title as title , facilityid from categories join areatype on categories.facilityid=areatype.areaid where areaid<>2 order by Title";
				//String Qry = "select id, case facilityid when 0 then 'CE Course -'+title when -1 then 'Review -' +title when -2 then 'PR Textbook-'+title else 'Facility -'+title end as title,Facilityid from categories " + StrWhere + "order by title";
				SqlCommand cmd = new SqlCommand(Qry, cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO returnDto = new LookupDTO();
					returnDto.ItemValue = reader.GetInt32(0).ToString();
					returnDto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);

					result.Add(returnDto);
				}
			}
			return result;
		}

		/// <summary>
		/// Retrieves all Courses  for the given filter
		/// </summary>
		/// <param name="FacIds">0 = all ,  otherwise user FacIds</param>
		/// <returns>list of Courses</returns>

		public List<LookupDTO> GetAllCourses(string FacIds)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{

				String Qry = "select distinct topic.topicid, areatype.areaname + ' - ' + topic.topicname as topicname,facilityid from topic join areatype on topic.facilityid=areatype.areaid where active_ind='1' and obsolete='0'" +
					" union select topic.topicid,'Facility' + ' - '+ topic.topicname ,facilityid from topic where facilityid in ( " + FacIds + ") and active_ind='1' and obsolete='0' order by topicname";
				SqlCommand cmd = new SqlCommand(Qry, cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader.GetInt32(0).ToString();
					dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);

					result.Add(dto);
				}
			}
			return result;
		}

		/// <summary>
		/// Retrieves all Courses with ID's for the given filter
		/// </summary>
		/// <param name="FacIds">0 = all ,  otherwise user FacIds</param>
		/// <returns>list of Courses</returns>

		public List<LookupDTO> GetAllCoursesWithIDs(string FacIds)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				//SqlCommand cmd = new SqlCommand("select distinct topicname,case facilityid when 0 then 'CED -'+topicname when -1 then 'PR -' +topicname when -2 then 'PR Textbook-'+topicname else 'Facility -'+topicname end as TpicName,facilityid from topic where facilityid in ( (select areaid from facilityarealink where facilityarealink.facilityid=" + FacIds + ") union ( select facilityid from topic where facilityid=" + FacIds + "))  order by facilityid desc,topicname", cn);   
				string sql = "select distinct topicid, topicname, case facilityid when 0 THEN 'CED -' " +
					" when -1 then 'PR -' when -2 then 'PR Textbook-' when -3 then 'Quality -' WHEN 30 then 'Focused CE -' else 'Facility -' end + topicname + ' - ' + course_number as TpicName, " +
					" facilityid from topic where facilityid in (select areaid from facilityarealink where facilityarealink.facilityid in(" + FacIds + ")) " + " and facilityid<>5 " +
					" or facilityid in (0,-1,-2, -3, 30, " + FacIds + ") order by facilityid desc,TpicName";
				SqlCommand cmd = new SqlCommand(sql, cn);// qry modified to get courses for newly added facility--Swetha//

				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{

					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader[0].ToString();
					//dto.CourseName = reader.IsDBNull(1) ? "" : reader.GetString(1);
					dto.ItemText = reader.IsDBNull(2) ? "" : reader.GetString(2);

					result.Add(dto);

					//LookupDTO dto = new LookupDTO();
					//dto.ItemValue = reader.GetInt32(1).ToString();
					//dto.ItemText = reader.GetString(2);
					//result.Add(dto);
				}
			}
			return result;
		}
	   
		/// <summary>
		/// Retrieves all Courses with ID's for the given filter
		/// </summary>
		/// <param name="FacIds">0 = all ,  otherwise user FacIds</param> ///RPT SURVEY EVALUATION///
		/// <returns>list of Courses</returns>

		public List<LookupDTO> GetCourses(string FacIds)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("select distinct topicname, topicid,case facilityid when 0 then 'CED -'+topicname " +
								" when -1 then 'PR -' +topicname when -2 then 'PR Textbook-'+topicname else 'Facility -'+topicname end as TpicName, " +
								" facilityid from topic where active_ind =1 and  facilityid in ( (select areaid from facilityarealink where facilityarealink.facilityid in (" + FacIds + ")) " +
								" union ( select facilityid from topic where facilityid in (" + FacIds + ")))  order by facilityid desc,topicname", cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					//dto.ItemValue = reader.GetString(0);
					dto.ItemValue = reader.GetInt32(1).ToString();
					dto.ItemText = reader.IsDBNull(2) ? "" : reader.GetString(2);
					result.Add(dto);
				}
			}
			return result;
		}

		/// <summary>
		/// Retrieves all Courses with ID's for the given filter
		/// </summary>
		/// <param name="FacIds">0 = all ,  otherwise user FacIds</param> ///RPT SURVEY EVALUATION///
		/// <returns>list of Courses</returns>
		/// Bhaskar N

		public List<LookupDTO> GetCoursesWithCourseNumbers(string FacIds)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("select distinct topicname, " +
								"topicid,course_number," +
								"course_number + '-'+topicname as TpicName, " +
								" facilityid  " +
								"from topic where facilityid in  " +
								"( (select areaid from facilityarealink where facilityarealink.facilityid in (" + FacIds + ")) " +
								" union ( select facilityid from topic where facilityid in (" + FacIds + ")))  order by facilityid desc,topicname", cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					//dto.ItemValue = reader.GetString(0);
					dto.ItemValue = reader.GetInt32(1).ToString();
					dto.ItemText = reader.IsDBNull(3) ? "" : reader.GetString(3);
					dto.CourseID = reader["course_number"].ToString();
					dto.CourseName = reader["topicname"].ToString();
					result.Add(dto);
				}
			}
			return result;
		}

		public List<LookupDTO> GetCourseList(string FacIds, string sortBy, string sortDirection)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("select distinct topicid,topicname, " +
								"course_number, facilityid  " +
								"from topic where facilityid in  " +
								"( (select areaid from facilityarealink where facilityarealink.facilityid in (" + FacIds + ")) " +
								" union ( select facilityid from topic where facilityid in (" + FacIds + ")))  order by " + sortBy + " " + sortDirection, cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					//dto.ItemValue = reader.GetString(0);
					dto.ItemValue = reader["topicid"].ToString();
					dto.ItemText = reader["course_number"].ToString() + " - " + reader["topicname"].ToString(); ;
					dto.CourseID = reader["course_number"].ToString();
					dto.CourseName = reader["topicname"].ToString();
					result.Add(dto);
				}
			}
			return result;
		}
		/// <summary>
		/// Retrieves all Courses  for the given filter
		/// </summary>
		/// <param name="FacIds">0 = all ,  otherwise user FacIds</param>
		/// <returns>list of Courses</returns>

		public List<LookupDTO> GetAllCourseIDs(string FacIds)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{

				String Qry = "select topic.course_number as topicid, areatype.areaname + ' - ' + topic.topicname as topicname,facilityid from topic join areatype on topic.facilityid=areatype.areaid where active_ind='1' and obsolete='0'" +
					" union select topic.course_number as topicid,'Facility' + ' - '+ topic.topicname ,facilityid from topic where facilityid in ( " + FacIds + ") and active_ind='1' and obsolete='0' order by topicname";
				SqlCommand cmd = new SqlCommand(Qry, cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader[0].ToString();
					dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);

					result.Add(dto);
				}
			}
			return result;
		}
		public List<LookupDTO> GetAllCourseNumber(string FacIds)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{

				String Qry = "select topic.topicid, topic.course_number, areatype.areaname + ' - ' + topic.topicname as topicname,facilityid from topic join areatype on topic.facilityid=areatype.areaid where active_ind='1' and obsolete='0'" +
					" union select topic.topicid, topic.course_number ,'Facility' + ' - '+ topic.topicname ,facilityid from topic where facilityid in ( " + FacIds + ") and active_ind='1' and obsolete='0' order by topicname";
				SqlCommand cmd = new SqlCommand(Qry, cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader[0].ToString();
					dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);

					result.Add(dto);
				}
			}
			return result;
		}
		/// <summary>
		/// Retrieves all Departments
		/// </summary> 
		/// <returns>list of Departments</returns>       
		public List<LookupDTO> GetAllDepartments(string FacIds)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("Select dept_id,dept_name from Dept where dept_active_ind = 1 and Facilityid in (" + FacIds + ") order by dept_name", cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader.GetInt32(0).ToString();
					dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);

					result.Add(dto);
				}
			}
			return result;
		}

		/// <summary>
		/// Retrieves all Positions
		/// </summary> 
		/// <returns>list of Positions</returns>       
		public List<LookupDTO> GetAllPositions(string FacIds)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("select position_id,position_name from Position where position_active_ind = 1 and Facilityid in (" + FacIds + ") order by position_name", cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader.GetInt32(0).ToString();
					dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);

					result.Add(dto);
				}
			}
			return result;
		}

		/// <summary>
		/// Retrieves all roles
		/// </summary> 
		/// <returns>list of Positions</returns>       
		public List<LookupDTO> GetAllroles()
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("select roleid,displayname from roles where displayname <> '' and displayname is not null order by priorder", cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader.GetInt32(0).ToString();
					dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);

					result.Add(dto);
				}
			}
			return result;
		}

		public List<LookupDTO> GetAllUsers(string FacIds)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("select iid,cusername,Facilityid from useraccount where Facilityid in (0," + FacIds + ")", cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader.GetInt32(0).ToString();
					dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);
					result.Add(dto);
				}
			}
			return result;
		}

		public List<LookupDTO> GetAllParentOrgs()
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand("select parent_id,parent_name from parentorganization where active='1' order by parent_name", cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader.GetInt32(0).ToString();
					dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);
					result.Add(dto);
				}
			}
			return result;
		}

		public List<LookupDTO> GetFacilitiesByParentId(int ParentId, int facilityid)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd;
				if (ParentId < 0)
				{
					cmd = new SqlCommand("select facid,facname from facilitygroup where active='1' order by facname", cn);
				}
				else if (ParentId == 0)
				{
					cmd = new SqlCommand("select facid,facname from facilitygroup where active='1' and facid= " + facilityid + " order by facname", cn);
				}
				else if (ParentId == 999)
				{
					cmd = new SqlCommand("select facid,facname from facilitygroup where active='1' and parent_id = 0 order by facname", cn);
				}
				else
				{
					cmd = new SqlCommand("select facid,facname from facilitygroup where active='1' and parent_id = " + ParentId + " order by facname", cn);
				}
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader.GetInt32(0).ToString();
					dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);
					result.Add(dto);
				}
			}
			return result;
		}




		public List<LookupDTO> GetDepartmentsByfacilityid(int facilityid, int dept_id)
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd;
				if (facilityid < 0)
				{
					cmd = new SqlCommand("select dept_id,dept_name from dept where dept_active_ind='1' order by dept_name", cn);
				}
				else if (facilityid == 0)
				{
					cmd = new SqlCommand("select dept_id,dept_name from dept where dept_active_ind='1' and dept_id= " + dept_id + " order by dept_name", cn);
				}
				else
				{
					cmd = new SqlCommand("select dept_id,dept_name from dept where dept_active_ind='1' and facilityid = " + facilityid + " order by dept_name", cn);
				}
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader.GetInt32(0).ToString();
					dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);
					result.Add(dto);
				}
			}
			return result;
		}


		public int GetParentIdByFacId(int facilityid)
		{
			SqlCommand cmd;
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				cmd = new SqlCommand("select  ISNULL(parent_id,0) from facilitygroup where facid = " + facilityid.ToString(), cn);
				cn.Open();
				int temp = (int)cmd.ExecuteScalar();
				return temp;
			}

		}

		public DataSet GenerateSponsorXML()
		{
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				DataSet ds = new DataSet();
				//SqlDataReader reader = cmd.ExecuteReader();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "SP_Sponsor_Xml";
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
				return ds;

			}

		}


		public DataSet GeneratePopularXML()
		{
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				DataSet ds = new DataSet();
				//SqlDataReader reader = cmd.ExecuteReader();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "SP_Popular_Xml";
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
				return ds;

			}

		}

		public DataSet GenerateRecentXML()
		{
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				DataSet ds = new DataSet();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "SP_Recent_Xml";
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
				return ds;

			}

		}
		public DataSet GeneratePRSiteMapXML()
		{
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				DataSet ds = new DataSet();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_r_PearlsReview_sitemap";
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
				return ds;

			}

		}

		public DataSet GenerateSiteMapXML()
		{
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				DataSet ds = new DataSet();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_r_sitemap";
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
				return ds;
			}
		}

		public DataSet GenerateMicrositeSiteMapXML()
		{
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				DataSet ds = new DataSet();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_r_microsite_sitemap";
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
				return ds;
			}
		}

		public DataSet GenerateSiteMapMonthlySpotlight()
		{
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				DataSet ds = new DataSet();
				//SqlDataReader reader = cmd.ExecuteReader();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_search_month_xml";
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
				return ds;

			}

		}

		public DataSet GenerateSiteMapPopularCourses()
		{
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				DataSet ds = new DataSet();
				//SqlDataReader reader = cmd.ExecuteReader();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_search_popular_xml";
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
				return ds;
			}
		}

		public DataSet GenerateSiteMapPopularCategories()
		{
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				DataSet ds = new DataSet();
				//SqlDataReader reader = cmd.ExecuteReader();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "SP_search_category_Xml";
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
				return ds;
			}
		}

		public DataSet GenerateOverageRpt()
		{
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				DataSet ds = new DataSet();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_r_overage";
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
				return ds;
			}
		}

		public DataSet GenerateOverageRptForNoParent()
		{
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				DataSet ds = new DataSet();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_r_overage_no_parent";
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
				return ds;
			}
		}

		public DataSet GetKaplanleadDetails(string startDate, string endDate)
		{
			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_r_KaplanLead";
				cmd.Parameters.Add("@Date_start", SqlDbType.VarChar).Value = startDate;
				cmd.Parameters.Add("@Date_end", SqlDbType.VarChar).Value = endDate;
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}

		public DataSet GetGeorgetownDetails()
		{
			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_get_georgetownfeed";
				//cmd.Parameters.Add("@Date_start", SqlDbType.VarChar).Value = startDate;
				//cmd.Parameters.Add("@Date_end", SqlDbType.VarChar).Value = endDate;
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}


		//sp_get_GeorgeWashingtonUniversityMPHfeed
		public DataSet GetGeorgeWashingtonUniversityMPHDetails()
		{
			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_get_GeorgeWashingtonUniversityMPHfeed";
				//cmd.Parameters.Add("@Date_start", SqlDbType.VarChar).Value = startDate;
				//cmd.Parameters.Add("@Date_end", SqlDbType.VarChar).Value = endDate;
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}


		public DataSet GetDrexelUniversityDetails()
		{
			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_get_DrexelDetails";
				//cmd.Parameters.Add("@Date_start", SqlDbType.VarChar).Value = startDate;
				//cmd.Parameters.Add("@Date_end", SqlDbType.VarChar).Value = endDate;
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}
		public DataSet GetCPEMonitorDetails(string startDate, string endDate)
		{
			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_get_cpeMonitor";
				cmd.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = startDate;
				cmd.Parameters.Add("@EndDate", SqlDbType.VarChar).Value = endDate;
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}

		public DataSet GetUserTranscripts(int parent_id, int fac_id, string course_number, string user_id, string completed_start, string completed_end)
		{
			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_SSRS_export_transcript";
				cmd.Parameters.Add("@parent_id", SqlDbType.Int).Value = parent_id;
				cmd.Parameters.Add("@fac_id", SqlDbType.Int).Value = fac_id;
				cmd.Parameters.Add("@course_number", SqlDbType.VarChar).Value = course_number;
				cmd.Parameters.Add("@user_id", SqlDbType.VarChar).Value = user_id;
				cmd.Parameters.Add("@completed_start", SqlDbType.VarChar).Value = completed_start;
				cmd.Parameters.Add("@completed_end", SqlDbType.VarChar).Value = completed_end;
				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}
		public DataSet GetUserTrancript(string userID)
		{
			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandText = "sp_r_export_transcript_singleuser";
				cmd.Parameters.Add("@UserID", SqlDbType.VarChar).Value = userID;

				cmd.CommandType = CommandType.StoredProcedure;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}




		public DataSet GetEmail()
		{
			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandType = CommandType.Text;
				cmd.CommandText = " select email, emailcontent from paypal where email not like '%done%' and rnid not in (716574, 693673,718474,627154,575184,721876,696610,697595,722811) and status=0";
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}


	   

		public List<LookupDTO> GetICESeriesList()
		{
			List<LookupDTO> result = new List<LookupDTO>();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
                SqlCommand cmd = new SqlCommand(" SELECT topicid,  topicname+' '+ course_number FROM topic WHERE topic_type='E' and facilityid=30", cn);
				cn.Open();
				SqlDataReader reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					LookupDTO dto = new LookupDTO();
					dto.ItemValue = reader.GetInt32(0).ToString();
					dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);
					result.Add(dto);
				}
			}
			return result;
		}

        public List<LookupDTO> GetICESeriesListWithId(string categoryid)
        {
            List<LookupDTO> result = new List<LookupDTO>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(" SELECT topicid,  topicname+' '+ course_number FROM topic t join Categories on t.categoryid=Categories.id WHERE topic_type='E' and t.facilityid=30 and id=" + categoryid, cn);
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    LookupDTO dto = new LookupDTO();
                    dto.ItemValue = reader.GetInt32(0).ToString();
                    dto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);
                    result.Add(dto);
                }
            }
            return result;
        }

	   

		public DataSet GetICESeriesDetails(string topicid)
		{
			

		   
				
			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{

				string alltopicid;
				if (topicid == "0")
				{
					alltopicid = "SELECT topicid FROM topic WHERE categoryid IN (SELECT id FROM dbo.Categories WHERE facilityid=30)";

				}
				else
				{
					alltopicid = topicid;
				
				}
                string sqlstring = "SELECT  t1.course_number as 'icourseNumber' ,t1.topicname as 'itopicName', t.course_number,t.topicname,i.ip_lastmod as webinar_start ,i.ip_score, t1.course_number+' '+t1.topicname + '<br/> User: ' +u.cfirstname+' '+u.clastname+ ' User email: '+u.cemail  + '<br/> Enrolled by : '+ ISNULL(u1.cfirstname,'Self') + ' '+ISNULL(u1.clastname,'') + ' '+ ISNULL(u1.cemail,'') +'<br/> '+ (SELECT  '  Percentage of Series Completed : ' + CONVERT(varchar(3) ,SUM( CASE   WHEN ip_status='C' THEN 100 else 0 end )/COUNT(*)  )+'%' AS 'ttt'  FROM dbo.IceProgress i1 WHERE i1.testid=i.testid) +'<input type=\"hidden\"  value=\"  '+ CONVERT(varchar(3),te.score) +'\">' AS 'einfo',i.ip_status  " 
							+"FROM dbo.IceProgress i " 
							+"LEFT JOIN topic t ON i.ip_topicid=t.topicid "
							+"LEFT JOIN test te ON i.testid=te.testid "
							+ "LEFT JOIN topic t1 ON te.topicid=t1.topicid "
							+"LEFT JOIN UserAccount u ON te.userid=u.iid "
							+"LEFT JOIN Useraccount u1 ON te.uniqueid=u1.iid "
							+ "WHERE i.testid IN (SELECT testid FROM test WHERE topicid in (" + alltopicid + " )) "
							+ "GROUP BY te.score,t1.course_number,t1.topicname,i.testid, u.cfirstname,u.clastname,u.cemail,t.course_number,t.webinar_start,i.ip_score,u1.cfirstname,u1.clastname,u1.cemail ,i.ip_status  order by t1.course_number";
				
				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandType = CommandType.Text;
				cmd.CommandText = sqlstring;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}

		public DataSet GetFCESeriesDetails(string sOrgId, string sFacId, string sDeptId, string sDeptSelect, string topicid)
		{




			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{

				string alltopicid;
				if (topicid == "-1")
				{
					alltopicid = "SELECT topicid FROM topic WHERE categoryid IN (SELECT id FROM dbo.Categories WHERE facilityid=30)";

				}
				else
				{
					alltopicid = topicid;

				}
                string sqlstring = "SELECT  t1.course_number as 'icourseNumber' ,t1.topicname as 'itopicName', t.course_number,t.topicname,"
                +"case  when ip_status='C' then i.ip_lastmod else null end as 'webinar_start' ,i.ip_score, t1.course_number+' '+t1.topicname + '<br/>User: ' +u.cfirstname+' '"
                +"+u.clastname+ ' User email: '+u.cemail  + '<br/> Enrolled by : '+ ISNULL(u1.cfirstname,'Self') + ' '+ISNULL(u1.clastname,'') + ' '+ ISNULL(u1.cemail,'') +'<br /> '"
                + "+ (SELECT  'Percentage of Series Completed : ' + CONVERT(varchar(3) ,CASE WHEN te.status='I' and SUM( CASE WHEN ip_status='C' THEN 100 else 0 end )/COUNT(*)=100 THEN 98 else SUM( CASE WHEN ip_status='C' THEN 100 else 0 end )/COUNT(*) end )+'%' AS 'ttt'"
                +"FROM dbo.IceProgress i1 WHERE i1.testid=i.testid) +'<input type=\"hidden\"  value=\"  '+ CONVERT(varchar(3),te.score)"
                +"+'\">' AS 'einfo',i.ip_status  "
							+ "FROM dbo.IceProgress i "
							+ "LEFT JOIN topic t ON i.ip_topicid=t.topicid "
							+ "LEFT JOIN test te ON i.testid=te.testid "
							+ "LEFT JOIN topic t1 ON te.topicid=t1.topicid "
							+ "LEFT JOIN UserAccount u ON te.userid=u.iid "
							+ " JOIN facilitygroup f ON u.facilityid=f.facid "
							+ "LEFT JOIN Useraccount u1 ON te.uniqueid=u1.iid "
							+ "WHERE i.testid IN (SELECT testid FROM test WHERE topicid in (" + alltopicid + " )) ";
               
                if (sOrgId == "-1" && sFacId == "-1")
                { }
                else
                {
                    if (sDeptId == "")
                    {

                        if (sDeptSelect != "")
                        {
                            sqlstring = sqlstring + " and u.dept_id = " + sDeptSelect;
                        }
                        else
                        {

                            if (sFacId != "" || sFacId != "0")
                            {
                                sqlstring = sqlstring + " and u.facilityid IN (" + sFacId + ") ";
                            }
                            else
                            {
                                sqlstring = sqlstring + " and f.parent_id= " + sOrgId;
                            }
                        }


                    }
                    else
                    {
                        if (sDeptSelect == "")
                        {
                            sqlstring = sqlstring + " and u.dept_id IN (" + sDeptId + ") ";
                        }
                        else
                        {
                            if (sDeptId.Contains(sDeptSelect))
                            {
                                sqlstring = sqlstring + " and u.dept_id =" + sDeptSelect;
                            }
                            else
                            {
                                sqlstring = sqlstring + " and 1=2 ";
                            }
                        }
                    }

                }

                //if (sDeptId == "")
                //{

                //    if (sDeptSelect != "")
                //    {
                //        sqlstring = sqlstring + " and u.dept_id = " + sDeptSelect;
                //    }else
                //    {

                //        if (sFacId != "" || sFacId != "0")
                //        {
                //            sqlstring = sqlstring + " and u.facilityid IN (" + sFacId + ") ";
                //        }else
                //        {
                //            sqlstring = sqlstring + " and f.parent_id= " + sOrgId;
                //        }
                //    }


                //}else
                //{
                //    if (sDeptSelect == "")
                //    {
                //        sqlstring = sqlstring + " and u.dept_id IN (" + sDeptId + ") ";
                //    }
                //    else {
                //        if (sDeptId.Contains(sDeptSelect))
                //        {
                //            sqlstring = sqlstring + " and u.dept_id =" + sDeptSelect;
                //        }
                //        else
                //        {
                //            sqlstring = sqlstring + " and 1=2 ";
                //        }
                //    }
                //}


			  
						  
				   sqlstring=sqlstring + " GROUP BY te.score,te.status,t1.course_number,t1.topicname,i.testid, u.cfirstname,u.clastname,u.cemail,t.course_number,t.topicname,t.webinar_start,i.ip_lastmod, i.ip_score,u1.cfirstname,u1.clastname,u1.cemail ,i.ip_status  ";

				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandType = CommandType.Text;
				cmd.CommandText = sqlstring;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}

		public DataSet GetFCESummary(string sOrgId, string sFacId, string sDeptId, string sDeptSelect, string topicid)
		{




			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{

               string alltopicid;
				if (topicid == "-1")
				{
                    alltopicid = "	 SELECT topicid FROM topic WHERE facilityid=30";

				}
				else
				{
					alltopicid = topicid;

				}
				//string sqlstring = "SELECT  t1.course_number as 'icourseNumber' ,t1.topicname as 'itopicName', t.course_number,t.topicname,t.webinar_start ,i.ip_score, t1.course_number+' '+t1.topicname + '<br/>' +u.cfirstname+' '+u.clastname+ ' '+u.cemail +' '+ (SELECT  ' &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Course % Complete : ' + CONVERT(varchar(3) ,SUM( CASE   WHEN ip_status='C' THEN 100 else 0 end )/COUNT(*)  )+'%' AS 'ttt'  FROM dbo.IceProgress i1 WHERE i1.testid=i.testid)  + '<br/> Enrolled by : '+ ISNULL(u1.cfirstname,'Self') + ' '+ISNULL(u1.clastname,'') + ' '+ ISNULL(u1.cemail,'') +'<input type=\"hidden\"  value=\"  '+ CONVERT(varchar(3),te.score) +'\">' AS 'einfo',i.ip_status  "
				//            + "FROM dbo.IceProgress i "
				//            + "LEFT JOIN topic t ON i.ip_topicid=t.topicid "
				//            + "LEFT JOIN test te ON i.testid=te.testid "
				//            + "LEFT JOIN topic t1 ON te.topicid=t1.topicid "
				//            + "LEFT JOIN UserAccount u ON te.userid=u.iid "
				//            + " JOIN facilitygroup f ON u.facilityid=f.facid "
				//            + "LEFT JOIN Useraccount u1 ON te.uniqueid=u1.iid "
				//            + "WHERE i.testid IN (SELECT testid FROM test WHERE topicid in (" + alltopicid + " )) ";
                string sqlstring = " SELECT u.cfirstname+' '+u.clastname as name, dept.dept_name, t1.topicname+' ' + t1.course_number as topicname,  "
	+ " CASE ISNULL(Sub.Tabname, '') WHEN '' THEN '' ELSE Sub.Tabname END PrimaryDiscipline,  "
    + " (SELECT   CONVERT(varchar(3) ,CASE WHEN te.status = 'I' and SUM( CASE WHEN ip_status='C' THEN 100 else 0 end )/COUNT(*)=100 then 98 else SUM( CASE WHEN ip_status='C' THEN 100 else 0 end )/COUNT(*) end )+'%' AS 'ttt'   "
	+ " FROM dbo.IceProgress i1 WHERE i1.testid=i.testid) as complete,  t1.hours, ISNULL(u1.cfirstname,'Self') + ' '+ISNULL(u1.clastname,'')  as enrolledby "
	//+ " t1.course_number as 'icourseNumber' ,t1.topicname as 'itopicName'  "
	+ " FROM dbo.IceProgress i  LEFT JOIN test te ON i.testid=te.testid  "
	+ " LEFT JOIN topic t1 ON te.topicid=t1.topicid LEFT JOIN UserAccount u ON te.userid=u.iid  JOIN facilitygroup f ON u.facilityid=f.facid  "
	+ " LEFT JOIN Useraccount u1 ON te.uniqueid=u1.iid  "  
	+ " LEFT JOIN (SELECT A.UserID, B.Tabname FROM UserDiscipline A INNER JOIN AreaType B ON A.DisciplineID = B.AreaID WHERE A.Primary_Ind=1) Sub ON u1.iid=Sub.UserID  "
	+ " left join Dept on u.dept_id=dept.dept_id "
	 + "WHERE i.testid IN (SELECT testid FROM test WHERE topicid in (" + alltopicid + " )) ";

                if (sOrgId == "-1" && sFacId == "-1")
                { }
                else { 
                if (sDeptId == "")
				{

					if (sDeptSelect != "")
					{
						sqlstring = sqlstring + " and u.dept_id = " + sDeptSelect;
					}
					else
					{

						if (sFacId != "" || sFacId != "0")
						{
							sqlstring = sqlstring + " and u.facilityid IN (" + sFacId + ") ";
						}
						else
						{
							sqlstring = sqlstring + " and f.parent_id= " + sOrgId;
						}
					}


				}
				else
				{
					if (sDeptSelect == "")
					{
						sqlstring = sqlstring + " and u.dept_id IN (" + sDeptId + ") ";
					}
					else
					{
						if (sDeptId.Contains(sDeptSelect))
						{
							sqlstring = sqlstring + " and u.dept_id =" + sDeptSelect;
						}
						else
						{
							sqlstring = sqlstring + " and 1=2 ";
						}
					}
				}

                }

                sqlstring = sqlstring + "group by u.cfirstname,u.clastname, u1.cfirstname,u1.clastname,dept.dept_name,Sub.Tabname,i.testid,t1.hours ,u1.cemail, t1.course_number,t1.topicname, te.score, te.status order by t1.topicname,dept.dept_name";
			  //  sqlstring = sqlstring + " GROUP BY te.score,t1.course_number,t1.topicname,i.testid, u.cfirstname,u.clastname,u.cemail order by t1.course_number";

				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandType = CommandType.Text;
				cmd.CommandText = sqlstring;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}

		public DataSet GetICESeriesListByTopicID(string topicid)
		{




			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{


				string sqlstring = " SELECT  t.course_number,t.topicname  FROM TopicCollection  tc  "
							+ "    JOIN topic t ON  tc.topicid=t.topicid "
							+ " WHERE  t.topic_type='E' and chapterid= " + topicid
							 
							+ " order by course_number";

				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandType = CommandType.Text;
				cmd.CommandText = sqlstring;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}

		public int QuestionFourNotTimeRelated(String sqlstring)
		{
		   
		

			SqlCommand cmd;
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{
				
				cmd = new SqlCommand(sqlstring, cn);
				cn.Open();
				return (int)cmd.ExecuteScalar();
			}
		
		}

		public DataSet GetICESeriesAttendeeRoster(string topicid)
		{




			DataSet ds = new DataSet();
			using (SqlConnection cn = new SqlConnection(this.ConnectionString))
			{


			   // string sqlstring =   "select distinct u.cfirstname, u.clastname,u.cemail,f.facname ,tabname as specid, "
			   //  + "u.cphone,u.caddress1+ ISNULL(' '+caddress2,'') as caddress1 ,u.ccity,u.cstate,u.czipcode  "
			   // + " from test  t , useraccount u  , FacilityGroup f , UserDiscipline ud , AreaType a "
			   //  + " where topicid = " + topicid
			   //+ " and t.userid=u.iid and u.facilityid=f.facid and u.iid=ud.userid and  ud.disciplineid=a.areaid and ud.primary_ind=1 ";

                string sqlstring = "select distinct u.cfirstname, u.clastname,u.cemail,f.facname ,isnull (tabname,'') as specid,  "
				+ " u.cphone,u.caddress1+ ISNULL(' '+u.caddress2,'') as caddress1 ,u.ccity,u.cstate,u.czipcode, ISNULL(u1.cfirstname,'Self') + ' '+ISNULL(u1.clastname,'') + ' '+ ISNULL(u1.cemail,'') as enrolledby  "
				+ "from test  t join useraccount u  on t.userid=u.iid  "
				+ "join FacilityGroup f on u.facilityid=f.facid  "
				+ "left join UserDiscipline ud on u.iid=ud.userid  "
				+ "left join AreaType a on ud.disciplineid=a.areaid  "
				+ "left join useraccount u1 on u1.iid=t.uniqueid  "
				 + " where t.topicid=" + topicid
                 + " and (ud.primary_ind=1 or ud.primary_ind is null )  ";
				SqlCommand cmd = new SqlCommand();
				SqlDataAdapter adapter;
				cmd.Connection = cn;
				cn.Open();
				cmd.CommandType = CommandType.Text;
				cmd.CommandText = sqlstring;
				adapter = new SqlDataAdapter(cmd);
				adapter.Fill(ds);
			}
			return ds;
		}

        public List<LookupDTO> GetAllFCECategories()
        {
            List<LookupDTO> result = new List<LookupDTO>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                String Qry = "select id, (SUBSTRING(title,13,len(title)-12)) as title  from categories where facilityid=30";
                //String Qry = "select id, case facilityid when 0 then 'CE Course -'+title when -1 then 'Review -' +title when -2 then 'PR Textbook-'+title else 'Facility -'+title end as title,Facilityid from categories " + StrWhere + "order by title";
                SqlCommand cmd = new SqlCommand(Qry, cn);
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    LookupDTO returnDto = new LookupDTO();
                    returnDto.ItemValue = reader.GetInt32(0).ToString();
                    returnDto.ItemText = reader.IsDBNull(1) ? "" : reader.GetString(1);

                    result.Add(returnDto);
                }
            }
            return result;
        }

        public DataSet GetPAchildAbuse(string startDate, string endDate)
        {




            DataSet ds = new DataSet();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {



                string sqlstring = "select top 10 u.clastname, u.cfirstname,u.cmiddle, isnull (dbo.L4Decrypt (csocial),'NA') as csocial ,u.cbirthdate,convert(varchar(10),lastmod,101 ) as lastmod , " +
                                    "ul.license_number, tp.course_number,tp.topicname,tp.hours as 'Credits', 'ProviderId' as 'ProviderId', " +
                                   "  'ContinuingEducation.com' as 'ProviderName' ,'' as 'CertificationNo','CourseApprovalAgency' as 'CourseApprovalAgency' " +
                                  "   from test t join topic tp on  t.topicid=tp.topicid " +
                                   " join UserAccount u on t.userid=u.iid " +
                                  "  join UserLicense ul on ul.userid=u.iid " +
                                 " where   CONVERT(datetime, t.lastmod, 101) >=  CONVERT(datetime,'" + startDate + "', 101)  and CONVERT(datetime, t.lastmod, 101) < CONVERT(datetime, '" + endDate + "', 101) ";
                                  
                                  //" and tp.course_number in ('60213', 'CMEZ60213', 'SW60213', 'PSY60213', 'AAQL6213', 'OT60213', 'PT60213', 'MT60213', '0513-0000-15-177-H01-P' " + 
                                //  "  ,'AT60213','RT60213') ";
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adapter;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlstring;
                adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
            }
            return ds;
        }
	}
}
	
