﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class EmployeeParticipationReportDAO : PRProvider3
    {
        public EmployeeParticipationReportDAO()
        {           
        }


        /// <summary>
        ///  Retrieves Employee Participation Report for the given filter
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public List<EmployeeParticipationReportDTO> GetEmpParticipation(EmployeeParticipationReportDTO dto, string prmFacilityName)
        {
            List<EmployeeParticipationReportDTO> Result = null;
            try 
            {   
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "sp_r_sysrpts_EmpPart";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@org_id", SqlDbType.VarChar).Value = dto.OrgID;
                    cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                    cmd.Parameters.Add("@active_emps", SqlDbType.Int, 4).Value = dto.ActiveEmps;
                    cmd.Parameters["@active_emps"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@inactive_emps", SqlDbType.Int, 4).Value = dto.InactiveEmps;
                    cmd.Parameters["@inactive_emps"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@login_emps", SqlDbType.Int, 4).Value = dto.LoginEmps;
                    cmd.Parameters["@login_emps"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@view_emps", SqlDbType.Int, 4).Value = dto.ViewEmps;
                    cmd.Parameters["@view_emps"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@complete_emps", SqlDbType.Int, 4).Value = dto.CompleteEmps;
                    cmd.Parameters["@complete_emps"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@total_credits", SqlDbType.Int, 4).Value = dto.TotalCredits;
                    cmd.Parameters["@total_credits"].Direction = ParameterDirection.Output;  
                    
                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    Result = new List<EmployeeParticipationReportDTO>();

                    EmployeeParticipationReportDTO returnDto = new EmployeeParticipationReportDTO();
                    returnDto.ActiveEmps = Convert.ToInt32(cmd.Parameters["@active_emps"].Value);
                    returnDto.InactiveEmps = Convert.ToInt32(cmd.Parameters["@inactive_emps"].Value);
                    returnDto.TotalEmps = (Convert.ToInt32(cmd.Parameters["@active_emps"].Value) + Convert.ToInt32(cmd.Parameters["@inactive_emps"].Value));
                    returnDto.LoginEmps = Convert.ToInt32(cmd.Parameters["@login_emps"].Value);
                    returnDto.ViewEmps = Convert.ToInt32(cmd.Parameters["@view_emps"].Value);                    
                    returnDto.PersentViewEmps = (Convert.ToInt32(cmd.Parameters["@view_emps"].Value) * 100 / Convert.ToInt32(cmd.Parameters["@active_emps"].Value));
                    returnDto.CompleteEmps = Convert.ToInt32(cmd.Parameters["@complete_emps"].Value);
                    returnDto.PersentParticipation = (Convert.ToInt32(cmd.Parameters["@complete_emps"].Value) * 100 / Convert.ToInt32(cmd.Parameters["@active_emps"].Value));
                    returnDto.TotalCredits = Convert.ToInt32(cmd.Parameters["@total_credits"].Value);

                    if (Convert.ToInt32(cmd.Parameters["@view_emps"].Value) != 0)
                    {
                        returnDto.AvgParticipant = (Convert.ToDecimal(cmd.Parameters["@total_credits"].Value) / Convert.ToInt32(cmd.Parameters["@view_emps"].Value));
                    }
                    else
                    {
                        returnDto.AvgParticipant = 0;
                    }

                    if (Convert.ToInt32(cmd.Parameters["@complete_emps"].Value) != 0)
                    {
                        returnDto.AvgCompletions = (Convert.ToDecimal(cmd.Parameters["@total_credits"].Value) / Convert.ToInt32(cmd.Parameters["@complete_emps"].Value));
                    }
                    else
                    {
                        returnDto.AvgCompletions = 0;
                    }


                    returnDto.OrganizationName = prmFacilityName;
                    Result.Add(returnDto);                 
                }
                return Result;
            }
            catch (Exception ex)
            {
                 throw ex;
            }
        }
        /// <summary>
        ///  Retrieves Employee Participation Report for the given filter
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public List<EmployeeParticipationReportDTO> GetActiveEmpParticipation(EmployeeParticipationReportDTO dto)
        {
            List<EmployeeParticipationReportDTO> Result = new List<EmployeeParticipationReportDTO>();
            EmployeeParticipationReportDTO returnDto;

            try
            {
                using (SqlConnection cn = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "sp_r_emp_participation_all";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 1000;
                    cn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        returnDto = new EmployeeParticipationReportDTO();
                        returnDto.FacilityName = reader["facname"].ToString();
                        returnDto.OrganizationName = reader["ParentName"].ToString();
                        returnDto.ActiveEmps = Convert.ToInt32(String.IsNullOrEmpty(reader["activeEm"].ToString()) ? "0" : reader["activeEm"].ToString());
                        returnDto.InactiveEmps = Convert.ToInt32(String.IsNullOrEmpty(reader["inActiveEm"].ToString()) ? "0" : reader["inActiveEm"].ToString());
                        returnDto.TotalEmps = Convert.ToInt32(String.IsNullOrEmpty(reader["totalEm"].ToString()) ? "0" : reader["totalEm"].ToString());
                        returnDto.LoginEmps = Convert.ToInt32(String.IsNullOrEmpty(reader["LoggedinEm"].ToString()) ? "0" : reader["LoggedinEm"].ToString());
                        returnDto.ViewEmps = Convert.ToInt32(String.IsNullOrEmpty(reader["viewedCourse"].ToString()) ? "0" : reader["viewedCourse"].ToString());
                        returnDto.PersentViewEmps = Convert.ToInt32(String.IsNullOrEmpty(reader["viewedCourse"].ToString()) ? "0" : reader["viewedCourse"].ToString()) * 100 / Convert.ToInt32(String.IsNullOrEmpty(reader["activeEm"].ToString()) ? "1" : reader["activeEm"].ToString());
                        returnDto.CompleteEmps = Convert.ToInt32(String.IsNullOrEmpty(reader["completedCourse"].ToString()) ? "0" : reader["completedCourse"].ToString());
                        returnDto.PersentParticipation = Convert.ToInt32(String.IsNullOrEmpty(reader["completedCourse"].ToString()) ? "0" : reader["completedCourse"].ToString()) * 100 / Convert.ToInt32(String.IsNullOrEmpty(reader["activeEm"].ToString()) ? "1" : reader["activeEm"].ToString());
                        returnDto.TotalCredits = Convert.ToInt32(decimal.Parse(String.IsNullOrEmpty(reader["totalC"].ToString()) ? "0" : reader["totalC"].ToString()));
                        if (returnDto.ViewEmps != 0)
                        {
                            returnDto.AvgParticipant = Convert.ToDecimal(decimal.Parse(String.IsNullOrEmpty(reader["totalC"].ToString()) ? "0" : reader["totalC"].ToString())) / Convert.ToInt32(String.IsNullOrEmpty(reader["viewedCourse"].ToString()) ? "1" : reader["viewedCourse"].ToString());
                        }
                        else
                            returnDto.AvgParticipant = 0;
                        if (returnDto.CompleteEmps != 0)
                        {
                            returnDto.AvgCompletions = Convert.ToDecimal(decimal.Parse(String.IsNullOrEmpty(reader["totalC"].ToString()) ? "0" : reader["totalC"].ToString())) / Convert.ToInt32(String.IsNullOrEmpty(reader["completedCourse"].ToString()) ? "1" : reader["completedCourse"].ToString());
                        }
                        else
                            returnDto.AvgCompletions = 0;
                        Result.Add(returnDto);
                    }
                }
                return Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

