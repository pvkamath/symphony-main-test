﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for CEDDefaultPasswordDAO
/// </summary>
/// 
namespace PearlsReview.DAL
{

    public class CEDDefaultPasswordDAO :PRProvider3
    {
        public CEDDefaultPasswordDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public List<CEDDefaultPasswordDTO> GetFacilityDefaultPassword(CEDDefaultPasswordDTO dto)
        {
            List<CEDDefaultPasswordDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_facility_default_password";
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<CEDDefaultPasswordDTO>();

                while (reader.Read())
                {
                    CEDDefaultPasswordDTO returnDto = new CEDDefaultPasswordDTO();
                    returnDto.FacilityName = reader["facname"].ToString();
                    returnDto.Password = reader["default_password"].ToString();                 
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}