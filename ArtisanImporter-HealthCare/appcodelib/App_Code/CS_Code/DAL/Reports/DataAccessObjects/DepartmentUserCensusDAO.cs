﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class DepartmentUserCensusDAO : PRProvider3
    {
	    public DepartmentUserCensusDAO()
	    {
	    }
        public List<DepartmentUserCensusDTO> GetDepartmentUserCensus(DepartmentUserCensusDTO dto)
        {
            List<DepartmentUserCensusDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_user_censusdept";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<DepartmentUserCensusDTO>();
                while (reader.Read())
                {
                    DepartmentUserCensusDTO returnDto = new DepartmentUserCensusDTO();
                    returnDto.FacName = reader["facname"].ToString();
                    returnDto.RoleName = reader["deptname"].ToString();
                    returnDto.TotalUsers = reader["totalUsers"] == DBNull.Value ? 0 : Convert.ToInt32(reader["totalUsers"]);

                    Result.Add(returnDto);
                }
            }
            return Result;
        }
    }
}