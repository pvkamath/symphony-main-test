﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for RevenueByDtRangeDAO
/// </summary>
namespace PearlsReview.DAL
{
public class RevenueByUserDAO :PRProvider3
{

    public RevenueByUserDAO()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public List<RevenueByUserDTO> GetRevenueByUser(RevenueByUserDTO dto)
    {
        List<RevenueByUserDTO> Result = null;
        RevenueByUserDTO returnDto;
        SqlCommand cmd;
        SqlDataReader reader;
        try
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_micrositeuserLog";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@OrderDate_start", SqlDbType.VarChar).Value = dto.OrderDate_Start;
                cmd.Parameters.Add("@OrderDate_end", SqlDbType.VarChar).Value = dto.OrderDate_End;
                cmd.Parameters.Add("@course_number", SqlDbType.VarChar).Value = dto.Course_Number;
                cmd.Parameters.Add("@seltype", SqlDbType.VarChar).Value = dto.SelType;
                cmd.Parameters.Add("@freeCourses", SqlDbType.VarChar).Value = dto.FreeCourses;
                cmd.Parameters.Add("@sortby", SqlDbType.VarChar).Value = dto.OrderBy;                                
                cn.Open();
                reader = cmd.ExecuteReader();
                Result = new List<RevenueByUserDTO>();
                while (reader.Read())
                {
                    returnDto = new RevenueByUserDTO();
                    returnDto.topicid = reader["topicid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["topicid"]);
                    returnDto.topicname = reader["topicname"].ToString();
                    returnDto.coursenumber = reader["course_number"].ToString();   
                    returnDto.cfirstname = reader["cfirstname"].ToString();
                    returnDto.clastname = reader["clastname"].ToString();
                    returnDto.Iid = Convert.ToInt32(reader["iid"].ToString());
                    returnDto.orderid = Convert.ToInt32(reader["orderid"].ToString());
                    returnDto.orderdate = reader["orderdate"].ToString();
                    returnDto.Type = reader["type"].ToString();
                    returnDto.cost = reader["cost"] == DBNull.Value ? 0 : decimal.Parse(reader["cost"].ToString());
                    returnDto.shipcost = reader["shipcost"] == DBNull.Value ? 0 : decimal.Parse(reader["shipcost"].ToString());
                    returnDto.subtotal = reader["subtotal"] == DBNull.Value ? 0 : decimal.Parse(reader["subtotal"].ToString());
                    returnDto.total = reader["total"] == DBNull.Value ? 0 : decimal.Parse(reader["total"].ToString());
                    returnDto.quantity = Convert.ToInt32(reader["quantity"].ToString());
                    Result.Add(returnDto);
                }
            }
            return Result;
        }
        catch (Exception ex) { throw ex; }
        finally { returnDto = null; cmd = null; reader = null; }
    }
}
}
