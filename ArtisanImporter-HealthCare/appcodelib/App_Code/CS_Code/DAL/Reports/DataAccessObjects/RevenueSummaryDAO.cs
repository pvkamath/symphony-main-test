﻿using System.Linq;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;
using System;

namespace PearlsReview.DAL
{
    public class RevenueSummaryDAO : PRProvider3
    {
        public RevenueSummaryDAO() { }

        /// <summary>
        /// Retrieves all Revenue Summary for the given filter
        /// </summary>


        public List<RevenueSummaryDTO> GetRevenueSummary(string OrderStartDate, string OrderEndDate, string DailyOrMonthly, int GetMethod, int facilityid)
        {
            List<RevenueSummaryDTO> result = new List<RevenueSummaryDTO>();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                string cSQLCommand = "";
                cSQLCommand = "SELECT MONTH(OrderDate) as Month, YEAR(OrderDate) as YEAR, ";

                if (DailyOrMonthly == "Month")
                {
                    cSQLCommand = cSQLCommand +
                        " CAST(MONTH(OrderDate)AS VarcHAR(12)) + '/' + CAST(YEAR(OrderDate) as VarChar(4)) AS yMo, "; ;
                }
                else  //Daily
                {
                    if (OrderEndDate == OrderStartDate)
                    {
                        cSQLCommand = cSQLCommand +
                             " Day(OrderDate) AS Day, " +
                             " CAST(MONTH(OrderDate)AS VarcHAR(12)) + '/' + CAST(Day(OrderDate)AS VarcHAR(12)) + '/' + CAST(YEAR(OrderDate) as VarChar(4))  AS yMo,";
                        //+ ' ' + (convert(varchar(10), OrderDate, 108))
                        //convert(varchar(10), OrderDate, 108) AS yMo1,  ";
                    }
                    else
                    {

                        cSQLCommand = cSQLCommand +
                        " Day(OrderDate) AS Day, " +
                        " CAST(MONTH(OrderDate)AS VarcHAR(12)) + '/' + CAST(Day(OrderDate)AS VarcHAR(12)) + '/' + CAST(YEAR(OrderDate) as VarChar(4)) AS yMo,";
                    }

                }

                cSQLCommand += "SUM(Totalcost)  as TotalByMonth  FROM Orders  WHERE Totalcost > 0 ";
                if(facilityid != 0) cSQLCommand += " AND facilityid = " + facilityid.ToString();

                //Where Conditons                
                if (GetMethod == 1)
                {
                    //getUCESalesByMonth
                    cSQLCommand = cSQLCommand +
                        " AND comment Like '%Unlimited%' ";
                }
                else if (GetMethod == 2)
                {
                    //--getRegSalesByMonth
                    cSQLCommand = cSQLCommand +
                            " AND comment NOT Like '%Unlimited%' ";
                }
                else if (GetMethod == 4)
                {
                    //--getUCESalesByMonth(Recurring UCE Sales)..AutoRenew
                    cSQLCommand = cSQLCommand +
                            " AND comment  Like '%Unlimited AutoRenew%' ";
                }
                else if (GetMethod == 5)
                {
                    //--getUCESalesByMonth(New UCE Sales)
                    cSQLCommand = cSQLCommand +
                            " AND comment Like '%Unlimited%'  and comment not like 'Unlimited AutoRenew%' ";
                }
                else if (GetMethod == 6)
                {
                    //CEPro renew+CEPro new
                    cSQLCommand = cSQLCommand +
                        " AND comment Like '%CEPRO%' ";
                }
                else if (GetMethod == 7)
                {
                    //--CE Online Sales
                    cSQLCommand = cSQLCommand +
                            " AND comment NOT Like '%CEPRO%' ";
                }
                else if (GetMethod == 8)
                {
                    //CEPro renew
                    cSQLCommand = cSQLCommand +
                            " AND comment = 'CEPRO AutoRenew' ";
                }
                else if (GetMethod == 9)
                {
                    //CEPro renew
                    cSQLCommand = cSQLCommand +
                            " AND comment = 'CEPRO' ";
                }
                
                //else
                //{
                //    //Else ...getTotalSalesByMonth 
                //    cSQLCommand = cSQLCommand;                    
                //}


                if (OrderStartDate.Length > 0)
                {
                    cSQLCommand = cSQLCommand +
                            " AND OrderDate >= '" + OrderStartDate + " 00:00:00' "; 
                }

                if (OrderEndDate.Length > 0)
                {
                    if (OrderEndDate == OrderStartDate)
                    {
                        DateTime dtEndDate = Convert.ToDateTime(OrderEndDate);
                        dtEndDate = dtEndDate.AddDays(1);


                        cSQLCommand = cSQLCommand +
                           " AND OrderDate < '" + dtEndDate.ToShortDateString() + " 23:59:00" + " ' ";
                    }
                    else
                    {
                        //OrderEndDate = OrderEndDate ;
                        cSQLCommand = cSQLCommand +
                                " AND OrderDate <= '" + OrderEndDate + " 23:59:00" + " ' ";
                    }
                }

                //Group By
                cSQLCommand = cSQLCommand +
                        " Group by MONTH(OrderDate), YEAR(OrderDate) ";

                if (DailyOrMonthly != "Month")
                {
                    cSQLCommand = cSQLCommand +
                        " , DAY(OrderDate) ";
                }

                //Order By
                cSQLCommand = cSQLCommand +
                        " Order by Year, month ";

                if (DailyOrMonthly != "Month")
                {
                    cSQLCommand = cSQLCommand +
                        " , day ";
                }

                SqlCommand cmd = new SqlCommand(cSQLCommand, cn);

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    RevenueSummaryDTO dto = new RevenueSummaryDTO();
                    //dto.TotalCatByMonth = reader["yMo"].ToString();

                    dto.OrderMonth = reader.GetInt32(0);
                    dto.OrderYear = reader.GetInt32(1);

                    if (DailyOrMonthly == "Month")
                    {
                        dto.OrderYearMonth = reader.GetString(2);
                        dto.TotalCatByMonth = reader.GetDecimal(3);
                    }
                    else
                    {
                        dto.OrderDay = reader.GetInt32(2);
                        dto.OrderYearMonth = reader.GetString(3);
                        dto.TotalCatByMonth = reader.GetDecimal(4);
                    }

                    result.Add(dto);
                }
            }
            return result;
        }

        //for retail site, facilityid is 2
        public List<RevenueSummaryDTO> GetRevenueSummary(string OrderStartDate, string OrderEndDate, string DailyOrMonthly, int GetMethod)
        {
            return GetRevenueSummary(OrderStartDate, OrderEndDate, DailyOrMonthly, GetMethod, 2);
        }
        
        public List<RevenueSummaryDTO> GetPearlsReview(string OrderStartDate, string OrderEndDate, string DailyOrMonthly, string Type)
        {
            List<RevenueSummaryDTO> result = new List<RevenueSummaryDTO>();
            StringBuilder sb = new StringBuilder();
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                sb.Append("SELECT MONTH(paydate) as Month, YEAR(paydate) as YEAR,");
                if (DailyOrMonthly == "Month") sb.Append(" CAST(MONTH(paydate)AS VarcHAR(12)) + '/' + CAST(YEAR(paydate) as VarChar(4)) AS yMo, ");
                if (DailyOrMonthly == "Day")
                {
                    if (OrderEndDate == OrderStartDate)
                        sb.Append("Day(paydate) AS Day, CAST(MONTH(paydate)AS VarcHAR(12)) + '/' + CAST(Day(paydate)AS VarcHAR(12)) + '/' + CAST(YEAR(paydate) as VarChar(4)) AS yMo, ");
                    else
                        sb.Append("Day(paydate) AS Day, CAST(MONTH(paydate)AS VarcHAR(12)) + '/' + CAST(Day(paydate)AS VarcHAR(12)) + '/' + CAST(YEAR(paydate) as VarChar(4)) AS yMo, ");
                }
                sb.Append("SUM(payamount) as TotalByMonth FROM payment WHERE payamount > 0 and paystatus = 'S' and Facilityid=3 and userid is not null and not (profileID like 'RT%') ");
                if (OrderStartDate.Length > 0) sb.Append(String.Format("AND paydate >= '{0} 00:00:00'", OrderStartDate));
                if (OrderEndDate.Length > 0)
                {
                    if (OrderEndDate == OrderStartDate)
                    {
                        DateTime dtEndDate = Convert.ToDateTime(OrderEndDate).AddDays(1);
                        sb.Append(String.Format("AND paydate < '{0}' ", dtEndDate.ToShortDateString() + " 23:59:00 "));
                    }
                    else
                        sb.Append(String.Format("AND paydate <= '{0}' ", OrderEndDate + " 23:59:00 "));
                }
                if (Type == "New")
                {
                    sb.Append(" AND (paydesc like 'New Reg%' or paydesc like 'Upgrade%') ");
                }
                else if (Type == "Recurring")
                {
                    sb.Append(" AND paydesc ='Auto Renewal' ");
                }

                //Group By
                sb.Append(" Group by MONTH(paydate), YEAR(paydate) ");

                if (DailyOrMonthly != "Month") sb.Append(", DAY(paydate) ");
                //Order By
                sb.Append("Order by Year, month ");
                if (DailyOrMonthly != "Month") sb.Append(", day ");

                SqlCommand cmd = new SqlCommand(sb.ToString(), cn);

                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    RevenueSummaryDTO dto = new RevenueSummaryDTO();
                    //dto.TotalCatByMonth = reader["yMo"].ToString();

                    dto.OrderMonth = reader.GetInt32(0);
                    dto.OrderYear = reader.GetInt32(1);

                    if (DailyOrMonthly == "Month")
                    {
                        dto.OrderYearMonth = reader.GetString(2);
                        dto.TotalCatByMonth = reader.GetDecimal(3);
                    }
                    else
                    {
                        dto.OrderDay = reader.GetInt32(2);
                        dto.OrderYearMonth = reader.GetString(3);
                        dto.TotalCatByMonth = reader.GetDecimal(4);
                    }

                    result.Add(dto);
                }
                return result;
            }
        }
    }
}