﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for GnrlMostPopularDAO
/// </summary>
/// 


namespace PearlsReview.DAL
{
    public class GnrlMostPopularCoursesDAO : PRProvider3
    {
        public GnrlMostPopularCoursesDAO()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        public List<GnrlMostPopularCoursesDTO> GetMostPopularCourses(GnrlMostPopularCoursesDTO dto)
        {
            List<GnrlMostPopularCoursesDTO> Result = null;
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = "sp_r_general_course_top";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@org_id", SqlDbType.VarChar).Value = dto.OrgIds;
                cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
                cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
                cmd.Parameters.Add("@type_id", SqlDbType.VarChar).Value = dto.TypeID;
                cmd.Parameters.Add("@test_start", SqlDbType.VarChar).Value = dto.TestStart;
                cmd.Parameters.Add("@test_end", SqlDbType.VarChar).Value = dto.TestEnd;
                cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
                cmd.Parameters.Add("@primaryDiscipline", SqlDbType.VarChar).Value = dto.PrimaryDiscipline;
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                Result = new List<GnrlMostPopularCoursesDTO>();

                while (reader.Read())
                {
                    GnrlMostPopularCoursesDTO returnDto = new GnrlMostPopularCoursesDTO();
                    returnDto.CourseNumber = reader["course_number"].ToString();
                    returnDto.CourseType = reader["coursetype"].ToString();
                    returnDto.TopicName = reader["topicname"].ToString();
                    returnDto.Views = reader["tv"] == DBNull.Value ? 0 : Convert.ToDouble(reader["tv"]);
                    returnDto.Completions = reader["tc"] == DBNull.Value ? 0 : Convert.ToDouble(reader["tc"]);
                    returnDto.AvgScore = reader["score"] == DBNull.Value ? 0 : Convert.ToDouble(reader["score"]);
                    Result.Add(returnDto);
                }
            }
            return Result;
        }








    }
}