﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for GnrlCourseComplianceDAO
/// </summary>
/// 
namespace PearlsReview.DAL
{
  
public class GnrlCourseComplianceDAO : PRProvider3
{
	public GnrlCourseComplianceDAO()
	{
	}

    public List<GnrlCourseComplianceDTO>GetCourseCompliance(GnrlCourseComplianceDTO dto)
    {
        List<GnrlCourseComplianceDTO> Result = null;
        using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = "sp_r_general_course_Compliance";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@org_ids", SqlDbType.VarChar).Value = dto.OrgId;
            cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacId;
            cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
            cmd.Parameters.Add("@dept_select", SqlDbType.VarChar).Value = dto.DeptSelect;
            cmd.Parameters.Add("@viewdate_start", SqlDbType.VarChar).Value = dto.ViewDateStart;
            cmd.Parameters.Add("@viewdate_end", SqlDbType.VarChar).Value = dto.ViewDateEnd;
            cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
            cmd.Parameters.Add("@topic_id", SqlDbType.VarChar).Value = dto.TopicID;
            cmd.Parameters.Add("@primarydiscipline", SqlDbType.VarChar).Value = dto.PrimaryDiscipline;
            cn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            Result = new List<GnrlCourseComplianceDTO>();

            while (reader.Read())
            {
                GnrlCourseComplianceDTO returnDto = new GnrlCourseComplianceDTO();
                returnDto.clastname = reader["clastname"].ToString();
                returnDto.cfirstname = reader["cfirstname"].ToString();
                returnDto.cusername = reader["cusername"].ToString();
                //returnDto.FacName = reader["facname"].ToString();
                returnDto.DeptName = reader["DeptName"].ToString();
                returnDto.pstatus = reader["pstatus"].ToString();
                returnDto.viewdate = reader["viewdate"].ToString() != string.Empty ? string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(reader["viewdate"].ToString())) : "";
                returnDto.compdate = reader["compdate"].ToString() != string.Empty ? string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(reader["compdate"].ToString())) : "";
                returnDto.score = reader["score"].ToString();
                returnDto.PrimaryDiscipline = reader["primaryDiscipline"].ToString();
                Result.Add(returnDto);
            }
        }
        return Result;
    }
}
}