﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    public class UserDemographicsDAO : PRProvider3
{
	public UserDemographicsDAO()
	{	
	}
    public List<UserDemographicsDTO> GetUserDemographics(UserDemographicsDTO dto)
    {
        List<UserDemographicsDTO> Result = null;
        using (SqlConnection cn = new SqlConnection(this.ConnectionString))
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = "sp_r_user_demo";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@fac_ids", SqlDbType.VarChar).Value = dto.FacIds;
            cmd.Parameters.Add("@dept_ids", SqlDbType.VarChar).Value = dto.DeptIds;
            cmd.Parameters.Add("@user_first", SqlDbType.VarChar).Value = dto.UserFirstName;
            cmd.Parameters.Add("@user_last", SqlDbType.VarChar).Value = dto.UserLastName;
            cmd.Parameters.Add("@user_title", SqlDbType.VarChar).Value = dto.UserTitle;
            cmd.Parameters.Add("@user_position", SqlDbType.VarChar).Value = dto.UserPosition;
            cmd.Parameters.Add("@dept_select", SqlDbType.VarChar).Value = dto.DeptSelect;
            cmd.Parameters.Add("@user_state", SqlDbType.VarChar).Value = dto.UserState;
            cmd.Parameters.Add("@user_ssn", SqlDbType.VarChar).Value = dto.UserSsn;
            cmd.Parameters.Add("@user_badge", SqlDbType.VarChar).Value = dto.UserBadge;
            cmd.Parameters.Add("@hiredate_start", SqlDbType.VarChar).Value = dto.HiredateStart;
            cmd.Parameters.Add("@hiredate_end", SqlDbType.VarChar).Value = dto.HiredateEnd;
            cmd.Parameters.Add("@termindate_start", SqlDbType.VarChar).Value = dto.TermindateStart;
            cmd.Parameters.Add("@termindate_end", SqlDbType.VarChar).Value = dto.TermindateEnd;
            //cmd.Parameters.Add("@user_canlogin", SqlDbType.VarChar).Value = dto.UserCanlogin;
            cmd.Parameters.Add("@user_type", SqlDbType.VarChar).Value = dto.UserType;
            cmd.Parameters.Add("@user_active", SqlDbType.VarChar).Value = dto.UserActive;
            cmd.Parameters.Add("@orderBy", SqlDbType.VarChar).Value = dto.OrderBy;
            cmd.Parameters.Add("@PrimaryDiscipline", SqlDbType.VarChar).Value = dto.PrimaryDiscipline;

            cn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            Result = new List<UserDemographicsDTO>();
            while (reader.Read())
            {
                UserDemographicsDTO returnDto = new UserDemographicsDTO();
                returnDto.UserLastName = reader["clastname"].ToString();
                returnDto.UserFirstName = reader["cfirstname"].ToString();
                returnDto.UserMiddleName = reader["cmiddle"].ToString();
                returnDto.UserFullName = reader["userfullname"].ToString();
                returnDto.UserTitle = reader["title"].ToString();
                returnDto.PositionName = reader["position_name"].ToString();
                returnDto.DeptName = reader["dept_name"].ToString();
                returnDto.CAddress1 = reader["caddress1"].ToString();
                returnDto.CAddress2 = reader["caddress2"].ToString();
                returnDto.CCity = reader["ccity"].ToString();
                returnDto.UserState = reader["cstate"].ToString();
                returnDto.CZipCode = reader["czipcode"].ToString();
                returnDto.WorkPhone = reader["work_phone"].ToString();
                returnDto.CEmail = reader["cemail"].ToString();
                returnDto.CSocial = reader["csocial"].ToString();
                returnDto.UserBadge = reader["badge_id"].ToString();
                returnDto.HireDate = reader["hire_date"].ToString();
                returnDto.TerminDate = reader["termin_date"].ToString();
                //returnDto.CanLogin = reader["canlogin"].ToString();
                returnDto.UserPosition = reader["displayname"].ToString();
                returnDto.UserActive = reader["isactive"].ToString();
                returnDto.FacName = reader["facname"].ToString();
                returnDto.CPhone = reader["cphone"].ToString();                
                Result.Add(returnDto);
            }
        }
        return Result;
    }
}
}
