﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for MostPopularCoursesDTO
/// </summary>
/// 

namespace PearlsReview.DAL
{
    [Serializable()]

    public class GnrlMostPopularCoursesDTO
    {
        #region Private Variables

        private string _orgId;
        private string _facIds;
        private string _deptIds;
        private string _orderBy;
        private string _testStart;
        private string _testEnd;
        private string _typeId;
        private string _course_number;
        private string _topicname;
        private string _coursetype;
        private double _tv;
        private double _tc;
        private double _score;
        private string _primaryDiscipline;   
        
        #endregion

        #region Public Variables
        /// <summary>
        /// Org Id
        /// </summary>
        public string OrgIds
        {
            get { return _orgId; }
            set { _orgId = value; }
        }

        /// <summary>
        /// Facility Id
        /// </summary>
        public string FacIds
        {
            get { return _facIds; }
            set { _facIds = value; }
        }

        /// <summary>
        /// Department IDs
        /// </summary>
        public string DeptIds
        {
            get { return _deptIds; }
            set { _deptIds = value; }
        }

        /// <summary>
        /// ViewDate Start
        /// </summary>

        public string TestEnd
        {
            get { return _testEnd; }
            set { _testEnd = value; }
        }

        /// <summary>
        /// View Date End
        /// </summary>
        public string TestStart
        {
            get { return _testStart; }
            set { _testStart = value; }
        }

        /// <summary>
        /// Order By
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }

        /// <summary>
        /// Type ID
        /// </summary>
        public string TypeID
        {
            get { return _typeId; }
            set { _typeId = value; }
        }

        /// <summary>
        /// Course Number
        /// </summary>
        public string CourseNumber
        {
            get { return _course_number; }
            set { _course_number = value; }
        }

        /// <summary>
        /// Type ID
        /// </summary>
        public string CourseType
        {
            get { return _coursetype; }
            set { _coursetype = value; }
        }

        /// <summary>
        /// Topic Name
        /// </summary>
        public string TopicName
        {
            get { return _topicname; }
            set { _topicname = value; }
        }

        /// <summary>
        /// Views
        /// </summary>
        public double Views
        {
            get { return _tv; }
            set { _tv = value; }
        }


        /// <summary>
        /// Completions
        /// </summary>
        public double Completions
        {
            get { return _tc; }
            set { _tc = value; }
        }

        /// <summary>
        /// Average Score
        /// </summary>
        public double AvgScore
        {
            get { return _score ; }
            set { _score = value; }
        }
        
        /// <summary>
        /// Primary discipline
        /// </summary>
        public string PrimaryDiscipline
        {
            get { return _primaryDiscipline ; }
            set { _primaryDiscipline = value; }
        }        
        #endregion
    }
}