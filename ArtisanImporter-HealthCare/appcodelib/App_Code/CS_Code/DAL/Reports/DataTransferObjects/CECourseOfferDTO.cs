﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PearlsReview.DAL
{

/// <summary>
/// Summary description for CECourseOfferDTO
/// </summary>
      [Serializable()]
    public class CECourseOfferDTO
    {
        #region Private Variables
        
        private string _completed_start;
        private string _completed_end;
        private string _course_number;
        private string _user_id;

        private int _rowID;
        private int _iid;
        private string _cuserName;
        private string _cfirstName;
        private string _clastName;
        private string _cmiddle;
        private string _caddress1;
        private string _caddress2;
        private string _ccity;
        private string _cstate;
        private string _czipcode;
        private string _cemail;
        private string _primaryPhone;
        private string _cSocial;
        private string _badge_id;
        private string _topicid;
        private string _courseName;
        private string _courseNumber;
        private string _ASHA_ID;
        private string _score;
        private string _completedDate;
        private string _credits;
        private string _isSelected;
        private string _numberAttending;
        private string _partForms;
        private string _quarternumber;

        #endregion

        #region Public Properties
     
        /// <summary>
        /// CompletedStart
        /// </summary>
        public string CompletedStart
        {
            get { return _completed_start; }
            set { _completed_start = value; }
        }

        /// <summary>
        /// CompletedEnd
        /// </summary>
        public string CompletedEnd
        {
            get { return _completed_end; }
            set { _completed_end = value; }
        }

        /// <summary>
        /// Course Number
        /// </summary>
        public string Course_Number
        {
            get { return _course_number; }
            set { _course_number = value; }
        }

        /// <summary>
        /// User ID
        /// </summary>
        public string UserID
        {
            get { return _user_id; }
            set { _user_id = value; }
        }

        /// <summary>
        /// Row Ids
        /// </summary>
        public int RowID
        {
            get { return _rowID; }
            set { _rowID = value; }
        }

        /// <summary>
        /// Iids
        /// </summary>
        public int Iid
        {
            get { return _iid; }
            set { _iid = value; }
        }

        /// <summary>
        /// UserName
        /// </summary>
        public string cUserName
        {
            get { return _cuserName; }
            set { _cuserName = value; }
        }

        /// <summary>
        /// FirstName
        /// </summary>
        public string cFirstName
        {
            get { return _cfirstName; }
            set { _cfirstName = value; }
        }

        /// <summary>
        /// LastName
        /// </summary>
        public string cLastName
        {
            get { return _clastName; }
            set { _clastName = value; }
        }

        /// <summary>
        /// middle
        /// </summary>
        public string cMiddle
        {
            get { return _cmiddle; }
            set { _cmiddle = value; }
        }   
                   

        /// <summary>
        /// Address1
        /// </summary>
        public string caddress1
        {
            get { return _caddress1; }
            set { _caddress1 = value; }
        }

        /// <summary>
        /// Address2
        /// </summary>
        public string caddress2
        {
            get { return _caddress2; }
            set { _caddress2 = value; }
        }

        /// <summary>
        /// City
        /// </summary>
        public string ccity
        {
            get { return _ccity; }
            set { _ccity = value; }
        }   

        /// <summary>
        /// State
        /// </summary>
        public string cState
        {
            get { return _cstate; }
            set { _cstate = value; }
        }

        /// <summary>
        /// Zip code
        /// </summary>
        public string cZipcode
        {
            get { return _czipcode; }
            set { _czipcode = value; }
        }

        /// <summary>
        /// Email
        /// </summary>
        public string cEmail
        {
            get { return _cemail; }
            set { _cemail = value; }
        }

        /// <summary>
        /// Primary Phone
        /// </summary>
        public string PrimaryPhone
        {
            get { return _primaryPhone; }
            set { _primaryPhone = value; }
        }

        /// <summary>
        /// Social
        /// </summary>
        public string cSocial
        {
            get { return _cSocial; }
            set { _cSocial = value; }
        }

        /// <summary>
        /// Badge Id
        /// </summary>
        public string Badge_id
        {
            get { return _badge_id; }
            set { _badge_id = value; }
        }
        
        /// <summary>
        /// Topic Id
        /// </summary>
        public string TopicID
        {
            get { return _topicid; }
            set { _topicid = value; }
        }
        
        /// <summary>
        /// Course name
        /// </summary>
        public string CourseName
        {
            get { return _courseName; }
            set { _courseName = value; }
        }
        
        /// <summary>
        /// Course number
        /// </summary>
        public string CourseNumber
        {
            get { return _courseNumber; }
            set { _courseNumber = value; }
        }
        
        /// <summary>
        /// Score
        /// </summary>
        public string Score
        {
            get { return _score; }
            set { _score = value; }
        }

        /// <summary>
        /// Completed Date
        /// </summary>
        public string CompletedDate
        {
            get { return _completedDate; }
            set { _completedDate = value; }
        }

        /// <summary>
        /// Credits
        /// </summary>
        public string Credits
        {
            get { return _credits; }
            set { _credits = value; }
        }
        
        /// <summary>
        /// is Selected
        /// </summary>
        public string IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; }
        }

        /// <summary>
        /// ASHA_ID
        /// </summary>
        public string ASHA_ID
        {
            get { return _ASHA_ID; }
            set { _ASHA_ID = value; }
        }
        
        /// <summary>
        /// Number Attending
        /// </summary>
        public string NumberAttending
        {
            get { return _numberAttending; }
            set { _numberAttending = value; }
        }        
        
        /// <summary>
        /// Part Forms
        /// </summary>
        public string PartForms
        {
            get { return _partForms; }
            set { _partForms = value; }
        }

        /// <summary>
        /// Quarter Number
        /// </summary>
        public string QuarterNumber
        {
            get { return _quarternumber; }
            set { _quarternumber = value; }
        }
        #endregion

        public CECourseOfferDTO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}
