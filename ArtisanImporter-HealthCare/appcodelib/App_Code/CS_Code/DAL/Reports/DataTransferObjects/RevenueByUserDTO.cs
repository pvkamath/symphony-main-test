﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for RevenueByUserDTO
/// </summary>
/// 
namespace PearlsReview.DAL
{
    [Serializable()]

    public class RevenueByUserDTO
    {
        #region Private Variables

        private string _course_number;
        private string _seltype;
        private string _freeCourses;
        private string _OrderDate_start;
        private string _OrderDate_End;
        private string _orderBy;
        private int _topicid;
        private string _topicname;
        private string _coursenumber;
        private string _cfirstname;
        private string _clastname;
        private int _iid;
        private int _orderid;
        private string _orderdate;
        private string _type;
        private decimal _cost;
        private decimal _shipcost;
        private int _quantity;
        private decimal _subtotal;
        private decimal _total;    

        #endregion


        #region Public Variables
        /// <summary>
        /// course_number
        /// </summary>
        
        public string Course_Number
        {
            get { return _course_number; }
            set { _course_number = value; }
        }

        /// <summary>
        /// seltype
        /// </summary>
        public string SelType
        {
            get { return _seltype; }
            set { _seltype = value; }
        }

        /// <summary>
        /// freeCourses
        /// </summary>
        public string FreeCourses
        {
            get { return _freeCourses; }
            set { _freeCourses = value; }
        }

        /// <summary>
        /// OrderDate_Start
        /// </summary>
        public string OrderDate_Start
        {
            get { return _OrderDate_start; }
            set { _OrderDate_start = value; }
        }


        /// <summary>
        /// OrderDate_End
        /// </summary>
        public string OrderDate_End
        {
            get { return _OrderDate_End; }
            set { _OrderDate_End = value; }
        }

        /// <summary>
        /// Order By
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }

        /// <summary>
        /// topicid
        /// </summary>
        public int topicid
        {
            get { return _topicid; }
            set { _topicid = value; }
        }

        /// <summary>
        /// topicname
        /// </summary>
        public string topicname
        {
            get { return _topicname ; }
            set { _topicname = value; }
        }

        /// <summary>
        /// course number
        /// </summary>
        public string coursenumber
        {
            get { return _course_number; }
            set { _course_number = value; }
        }

        /// <summary>
        /// Firstname
        /// </summary>
        public string cfirstname
        {
            get { return _cfirstname; }
            set { _cfirstname = value; }
        }

        /// <summary>
        /// Lastname
        /// </summary>
        public string clastname
        {
            get { return _clastname; }
            set { _clastname = value; }
        }

        /// <summary>
        /// iid
        /// </summary>
        public int Iid
        {
            get { return _iid; }
            set { _iid = value; }
        }
        /// <summary>
        /// orderid
        /// </summary>
        public int orderid
        {
            get { return _orderid; }
            set { _orderid = value; }
        }
        /// <summary>
        /// cost
        /// </summary>
        public decimal cost
        {
            get { return _cost; }
            set { _cost = value; }
        }
        /// <summary>
        /// shipcost
        /// </summary>
        public decimal shipcost
        {
            get { return _shipcost; }
            set { _shipcost = value; }
        }
        /// <summary>
        /// quantity
        /// </summary>
        public int quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
        /// <summary>
        /// subtotal
        /// </summary>
        public decimal subtotal
        {
            get { return _subtotal; }
            set { _subtotal = value; }
        }
        /// <summary>
        /// total
        /// </summary>
        public decimal total
        {
            get { return _total; }
            set { _total = value; }
        }     
        /// <summary>
        /// orderdate
        /// </summary>
        public string orderdate
        {
            get { return _orderdate; }
            set { _orderdate = value; }
        }
        /// <summary>
        /// type
        /// </summary>
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        #endregion
    }

}