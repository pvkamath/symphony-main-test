﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class AddMyReportListDTO
    {
        #region Private Variables

            private int _myReportID;
            private string _reportName;
            private int _uniqueID;
            private string _userReportName;
            private string _reportParameters;
            private int _UserID;

        #endregion


        #region Public Properties

            /// <summary>
            /// My Report ID
            /// </summary>
            public int MyReportID
            {
                get { return _myReportID; }
                set { _myReportID = value; }
            }


            /// <summary>
            /// Report Name
            /// </summary>
            public string ReportName
            {
                get { return _reportName; }
                set { _reportName = value; }
            }

            /// <summary>
            /// Unique ID
            /// </summary>
            public int UniqueID
            {
                get { return _uniqueID; }
                set { _uniqueID = value; }
            }

            /// <summary>
            /// UserReport Name
            /// </summary>
            public string UserReportName
            {
                get { return _userReportName; }
                set { _userReportName = value; }
            }

            /// <summary>
            /// Report Parameters
            /// </summary>
            public string ReportParameters
            {
                get { return _reportParameters; }
                set { _reportParameters = value; }
            }
          ///<summary>
          ///User Id
          ///</summary>
            public int UserId
            {
                get { return _UserID; }
                set { _UserID = value; }
            }

        #endregion
    }
}
