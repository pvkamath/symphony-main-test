﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class UserSecurityDTO
    {
        #region Private Variables       

            private string _facIds;
            private string _deptIds;
            private string _userName;
            private string _userFirstName;
            private string _userLastName;
            private string _userMiddleName;
            private string _departmentID;
            private string _ssnID;
            private string _badgeID;
            private string _lastLoginStart;
            private string _lastLoginEnd;
            private string _userActive;
            private string _positionID;
            private string _userLogin;
            private string _userFullName;
            private string _deptName;
            private string _lastLogin;
            private string _isActive;
            private string _PositionName;
            private string _isLogin;
            private string _orderBy;
            private string _Created;
            private string _FacilityName;
            private string _title;
            private string _primaryDiscipline;

        #endregion

        #region Public Properties

            /// <summary>
            /// Facility Id
            /// </summary>
            public string FacIds
            {
                get { return _facIds; }
                set { _facIds = value; }
            }

            /// <summary>
            /// Department IDs
            /// </summary>
            public string DeptIds
            {
                get { return _deptIds; }
                set { _deptIds = value; }
            }

            /// <summary>
            /// User Name
            /// </summary>
            public string UserName
            {
                get { return _userName; }
                set { _userName = value; }
            }
            /// <summary>
            /// User First Name
            /// </summary>
            public string UserFirstName
            {
                get { return _userFirstName; }
                set { _userFirstName = value; }
            }

            /// <summary>
            /// User Lat Name
            /// </summary>
            public string UserLastName
            {
                get { return _userLastName; }
                set { _userLastName = value; }
            }
            /// <summary>
            /// User Middle Name
            /// </summary>
            public string UserMiddleName
            {
                get { return _userMiddleName; }
                set { _userMiddleName = value; }
            }   
            /// <summary>
            /// Department ID
            /// </summary>
            public string DepartmentID
            {
                get { return _departmentID; }
                set { _departmentID = value; }
            }
            /// <summary>
            /// SSNID
            /// </summary>
            public string SsnID
            {
                get { return _ssnID; }
                set { _ssnID = value; }
            }

            /// <summary>
            /// Badge ID
            /// </summary>
            public string BadgeID
            {
                get { return _badgeID; }
                set { _badgeID = value; }
            }

            /// <summary>
            /// Last Login Start
            /// </summary>
            public string LastLoginStart
            {
                get { return _lastLoginStart; }
                set { _lastLoginStart = value; }
            }

            /// <summary>
            /// Last Login End
            /// </summary>
            public string LastLoginEnd
            {
                get { return _lastLoginEnd; }
                set { _lastLoginEnd = value; }
            }

            /// <summary>
            /// User Active
            /// </summary>
            public string UserActive
            {
                get { return _userActive; }
                set { _userActive = value; }
            }


            /// <summary>
            /// Position ID
            /// </summary>
            public string PositionID
            {
                get { return _positionID; }
                set { _positionID = value; }
            }

            /// <summary>
            /// User Login
            /// </summary>
            public string UserLogin
            {
                get { return _userLogin; }
                set { _userLogin = value; }
            }

            /// <summary>
            /// User Full Name
            /// </summary>
            public string UserFullName
            {
                get { return _userFullName; }
                set { _userFullName = value; }
            }

            /// <summary>
            /// Department Name
            /// </summary>
            public string DeptName
            {
                get { return _deptName; }
                set { _deptName = value; }
            }

            /// <summary>
            /// Last Login
            /// </summary>
            public string LastLogin
            {
                get { return _lastLogin; }
                set { _lastLogin = value; }
            }


            /// <summary>
            /// Is Active
            /// </summary>
            public string IsActive
            {
                get { return _isActive; }
                set { _isActive = value; }
            }

            /// <summary>
            /// Position Name
            /// </summary>
            public string PositionName
            {
                get { return _PositionName; }
                set { _PositionName = value; }
            }

            /// <summary>
            /// Is Login
            /// </summary>
            public string IsLogin
            {
                get { return _isLogin; }
                set { _isLogin = value; }
            }

            /// <summary>
            /// Order By
            /// </summary>
            public string OrderBy
            {
                get { return _orderBy; }
                set { _orderBy = value; }
            }
        ///<summary>
        ///Created Date
        ///</summary>
        ///

            public string Created
            {
                get { return _Created; }
                set { _Created = value; }
            }

        ///<summary>
        ///facility name
        ///</summary>
        ///

            public string FacilityName
            {
                get { return _FacilityName; }
                set { _FacilityName = value; }
            }
        ///<summary>
        /// Title
        ///</summary>
        ///
            public string Title
            {
                get { return _title; }
                set { _title = value; }
            }              
        ///<summary>
        /// Primary Discipline
        ///</summary>
        ///

            public string PrimaryDiscipline
            {
                get { return _primaryDiscipline; }
                set { _primaryDiscipline = value; }
            }
        #endregion
    }
}

