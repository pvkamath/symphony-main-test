﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for AllCoursesByCategory
/// </summary>
/// 
namespace PearlsReview.DAL
{
    [Serializable()]
    public class AllCoursesByCategoryDTO
    {
        public AllCoursesByCategoryDTO()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Private Variables
        private string _categoryName;
        private string _topicName;
        private string _subTitle;
        private string _contactHours;
        private string _courseNumber;
        private int _facilityid;
        private bool _online_ind;
        private bool _video_ind;
        private bool _audio_ind;
        private bool _ebp_ind;
        private bool _apn_ind;
        private bool _magnet_ind;
        private bool _jcaho_ind;
        private int _categoryid;
        private int _areaid;
        
        #endregion

        #region Public Properties

        /// <summary>
        /// Category Name
        /// </summary>
        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

        /// <summary>
        /// Topic Name
        /// </summary>
        public string TopicName
        {
            get { return _topicName; }
            set { _topicName = value; }
        }
        /// <summary>
        /// Sub title
        /// </summary>
        public string SubTitle
        {
            get { return _subTitle; }
            set { _subTitle = value; }
        }
        /// <summary>
        /// Contact Hours
        /// </summary>
        public string ContactHours
        {
            get { return _contactHours; }
            set { _contactHours = value; }
        }
        /// <summary>
        /// Course Number
        /// </summary>
        public string CourseNumber
        {
            get { return _courseNumber; }
            set { _courseNumber = value; }
        }

        /// <summary>
        /// Facility Id
        /// </Summary>
        /// 
        public int FacilityId
        {
            get { return _facilityid; }
            set { _facilityid = value; }
        }

        public bool jcaho_ind
        {
            get { return _jcaho_ind; }
            set { _jcaho_ind = value; }
        }
        public bool magnet_ind
        {
            get { return _magnet_ind; }
            set { _magnet_ind = value; }
        }
        public bool apn_ind
        {
            get { return _apn_ind; }
            set { _apn_ind = value; }
        }
       
        public bool ebp_ind
        {
            get { return _ebp_ind; }
            set { _ebp_ind = value; }
        }
        public bool audio_ind
        {
            get { return _audio_ind; }
            set { _audio_ind = value; }
        }
        public bool video_ind
        {
            get { return _video_ind; }
            set { _video_ind = value; }
        }
        public bool online_ind
        {
            get { return _online_ind; }
            set { _online_ind = value; }
        }

        /// <summary>
        /// Category Id
        /// </Summary>
        /// 
        public int CategoryId
        {
            get { return _categoryid; }
            set { _categoryid = value; }
        }

        public int AreaId
        {
            get { return _areaid; }
            set { _areaid = value; }
        }
        #endregion
    }
}
