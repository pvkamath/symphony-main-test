﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for UserActiveInActive
/// </summary>
public class UserActiveInActiveDTO
{
	public UserActiveInActiveDTO()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    #region Private Variables
    
    private string _facIds;
    private string _facname;
    private string _activetotal;
    private string _inactivetotal;
    
    #endregion

    #region Public Properties

    /// <summary>
    /// Facility Id
    /// </summary>
    public string FacIds
    {
        get { return _facIds; }
        set { _facIds = value; }
    }

    /// <summary>
    /// Fac Name
    /// </summary>
    public string FacName
    {
        get { return _facname; }
        set { _facname = value; }
    }

    /// <summary>
    /// Active Total
    /// </summary>
    public string ActiveTotal
    {
        get { return _activetotal; }
        set { _activetotal = value; }
    }


    /// <summary>
    /// InActive Total
    /// </summary>
    public string InActiveTotal
    {
        get { return _inactivetotal; }
        set { _inactivetotal = value; }
    }

    #endregion

}
