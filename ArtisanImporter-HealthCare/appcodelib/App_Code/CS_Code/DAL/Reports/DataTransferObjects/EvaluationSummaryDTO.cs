﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for EvaluationSummaryDTO
/// </summary>

namespace PearlsReview.DAL
{
    [Serializable()]

    public class EvaluationSummaryDTO
    {
        #region Private Variables
        private string _surveydate_start;
        private string _surveydate_end;
        private string _qnumber;
        private string _qtext;
        private string _qanswer;
        private string _cqanswer;
        private string _topic_ids;
        private string _orderBy;
        //private string _course;
        private string _percentage;
        private string _topic_name;
        private string _course_number;
        private int _total_completion;

        #endregion

        #region Public Variables

        /// <summary>
        /// SurveyStartDate
        /// </summary>
        public string SurveyStartDate
        {
            get { return _surveydate_start; }
            set { _surveydate_start = value; }
        }

        /// <summary>
        /// SurveyEndDate
        /// </summary>
        public string SurveyEndDate
        {
            get { return _surveydate_end; }
            set { _surveydate_end = value; }
        }

        /// <summary>
        /// QNumber
        /// </summary>
        public string QNumber
        {
            get { return _qnumber; }
            set { _qnumber = value; }
        }

        /// <summary>
        /// QText
        /// </summary>
        public string QText
        {
            get { return _qtext; }
            set { _qtext = value; }
        }


        /// <summary>
        /// QAnswer
        /// </summary>
        public string QAnswer
        {
            get { return _qanswer ; }
            set { _qanswer  = value; }
        }


        /// <summary>
        /// CountQAnswer
        /// </summary>
        public string CountQAnswer
        {
            get { return _cqanswer ; }
            set { _cqanswer  = value; }
        }

        /// <summary>
        /// Course
        /// </summary>

        public string TopicID
        {
            get { return _topic_ids; }
            set { _topic_ids = value; }
        }

        /// <summary>
        /// OrderBy
        /// </summary>

        public string orderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }


        /// <summary>
        /// %
        /// </summary>

        public string Percentage
        {
            get { return _percentage; }
            set { _percentage = value; }
        }

        ///// <summary>
        ///// Course
        ///// </summary>

        public string topic_name
        {
            get { return _topic_name; }
            set { _topic_name = value; }
        }
        public string course_number
        {
            get { return _course_number; }
            set { _course_number = value; }
        }
        public int total_completion
        {
            get { return _total_completion; }
            set { _total_completion = value; }
        }

        #endregion

    }
}
