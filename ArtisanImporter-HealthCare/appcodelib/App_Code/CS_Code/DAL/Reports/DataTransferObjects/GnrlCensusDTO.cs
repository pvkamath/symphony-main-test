﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for GnrlCensusDTO
/// </summary>
/// 
namespace PearlsReview.DAL
{
    public class GnrlCensusDTO
    {


        #region Private Variables

        private string _facIds;
        private string _OrgId;
        private string _FacName;
        private string _Deptname;
        private double _Tuser;
        private double _Tmanager;
        private double _Tfacadmin;
        private double _Tparentorgadmin;
        private double _Ttotaluser;
        private string _deptIds;
        #endregion

        #region Public Properties

        /// <summary>
        /// Facility Ids
        /// </summary>
        public string FacIds
        {
            get { return _facIds; }
            set { _facIds = value; }
        }


        public string DeptIds
        {
            get { return _deptIds; }
            set { _deptIds = value; }
        }

        /// <summary>
        /// Department IDs
        /// </summary>
        public string OrgId
        {
            get { return _OrgId; }
            set { _OrgId = value; }
        }

        /// <summary>
        /// Category Id
        /// </summary>
        public string FacName
        {
            get { return _FacName; }
            set { _FacName = value; }
        }

        /// <summary>
        /// Category Id
        /// </summary>
        public string DeptName
        {
            get { return _Deptname; }
            set { _Deptname = value; }
        }

        /// <summary>
        /// Category Id
        /// </summary>
        public double Tuser
        {
            get { return _Tuser; }
            set { _Tuser = value; }
        }

        /// <summary>
        /// Category Id
        /// </summary>
        public double Tmanager
        {
            get { return _Tmanager; }
            set { _Tmanager = value; }
        }

        /// <summary>
        /// Category Id
        /// </summary>
        public double Tfacadmin
        {
            get { return _Tfacadmin; }
            set { _Tfacadmin = value; }
        }

        /// <summary>
        /// Category Id
        /// </summary>
        public double Tparentorgadmin
        {
            get { return _Tparentorgadmin; }
            set { _Tparentorgadmin = value; }
        }

        /// <summary>
        /// Category Id
        /// </summary>
        public double Ttotaluser
        {
            get { return _Ttotaluser; }
            set { _Ttotaluser = value; }
        }
        #endregion

        public GnrlCensusDTO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}
