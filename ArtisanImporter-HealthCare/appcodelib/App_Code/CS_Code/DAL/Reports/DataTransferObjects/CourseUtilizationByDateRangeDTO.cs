﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
	public  class CourseUtilizationByDateRangeDTO
	{        

        #region Private Variables   
     
            private string _title;
            private string _topicName;
            private int _vnum;
            private int _tnum;
            private int _cnum;
            private string _facIds;
            private string _deptIds;
            private string _categoryId;
            private string _courseId ;
            private string _viewDateStart;
            private string _viewDateEnd;
            private string _orderBy;

        #endregion

        #region Public Properties

            
            /// <summary>
            /// Title
            /// </summary>
            public string Title
            {
                get { return _title; }
                set { _title = value; }
            }

            /// <summary>
            /// FTopic Name
            /// </summary>
            public string TopicName
            {
                get { return _topicName; }
                set { _topicName = value; }
            }


            /// <summary>
            /// Vnum
            /// </summary>
            public int Vnum
            {
                get { return _vnum; }
                set { _vnum = value; }
            }

            /// <summary>
            /// Tnum
            /// </summary>
            public int Tnum
            {
                get { return _tnum; }
                set { _tnum = value; }
            }

            /// <summary>
            /// Cnum
            /// </summary>
            public int Cnum
            {
                get { return _cnum; }
                set { _cnum = value; }
            }

            /// <summary>
            /// Fac Ids
            /// </summary>
            public string FacIds
            {
                get { return _facIds; }
                set { _facIds = value; }
            }

            /// <summary>
            /// Dept Ids
            /// </summary>
            public string DeptIds
            {
                get { return _deptIds; }
                set { _deptIds = value; }
            }

            /// <summary>
            /// Category Id
            /// </summary>
            public string CategoryId
            {
                get { return _categoryId; }
                set { _categoryId = value; }
            }

            /// <summary>
            /// Course Id
            /// </summary>
            public string CourseId
            {
                get { return _courseId; }
                set { _courseId = value; }
            }

            /// <summary>
            /// ViewDateStart
            /// </summary>
            public string ViewDateStart
            {
                get { return _viewDateStart; }
                set { _viewDateStart = value; }
            }

            /// <summary>
            /// ViewDateEnd
            /// </summary>
            public string ViewDateEnd
            {
                get { return _viewDateEnd; }
                set { _viewDateEnd = value; }
            }

            /// <summary>
            /// Order By
            /// </summary>
            public string OrderBy
            {
                get { return _orderBy; }
                set { _orderBy = value; }
            }
        
        #endregion
    }
}
