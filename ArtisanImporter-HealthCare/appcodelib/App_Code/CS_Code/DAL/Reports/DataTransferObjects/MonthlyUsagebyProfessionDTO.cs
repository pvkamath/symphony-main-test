﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PearlsReview.DAL
{  
    /// <summary>
    /// Summary description for MonthlyUsagebyProfessionDTO
    /// </summary>
    [Serializable()]
    public class MonthlyUsagebyProfessionDTO
    {
        #region Private Variables
    
        private string _orgId;
        private string _facIds;
        private string _deptIds;
        private string _primaryDiscipline;
        private string _testPassFromDate;
        private string _testPassToDate;
        private string _orderBy;
        private string _FacName;

        private string _january;
        private string _february;
        private string _march;
        private string _april;
        private string _may;
        private string _june;
        private string _july;
        private string _august;
        private string _september;
        private string _october;
        private string _november;
        private string _december;
        private string _grandTotal;
        

        #endregion

        #region Public Properties

        public MonthlyUsagebyProfessionDTO()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// Org Ids
        /// </summary>
        public string OrgIds
        {
            get { return _orgId; }
            set { _orgId = value; }
        }
        /// <summary>
        /// Facility Ids
        /// </summary>
        public string FacIds
        {
            get { return _facIds; }
            set { _facIds = value; }
        }
        /// <summary>
        /// Department IDs
        /// </summary>
        public string DeptIds
        {
            get { return _deptIds; }
            set { _deptIds = value; }
        }

        public string PrimaryDiscipline
        {
            get { return _primaryDiscipline; }
            set { _primaryDiscipline = value; }
        }

        public string TestPassFromDate
        {
            get { return _testPassFromDate; }
            set { _testPassFromDate = value; }
        }
        public string TestPassToDate
        {
            get { return _testPassToDate; }
            set { _testPassToDate = value; }
        }        
        
        /// <summary>
        ///Order By
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }

        /// <summary>
        /// FacNAme
        /// </summary>
        public string FacName
        {
            get { return _FacName; }
            set { _FacName = value; }
        }

        /// <summary>
        /// January
        /// </summary>
        public string January
        {
            get { return _january; }
            set { _january = value; }
        }
        /// <summary>
        /// February
        /// </summary>
        public string February
        {
            get { return _february; }
            set { _february = value; }
        }
        /// <summary>
        /// March
        /// </summary>
        public string March
        {
            get { return _march; }
            set { _march = value; }
        }
        /// <summary>
        /// April
        /// </summary>
        public string April
        {
            get { return _april; }
            set { _april = value; }
        }

        /// <summary>
        /// May
        /// </summary>
        public string May
        {
            get { return _may; }
            set { _may = value; }
        }

        /// <summary>
        /// June
        /// </summary>
        public string June
        {
            get { return _june; }
            set { _june = value; }
        }
        /// <summary>
        /// July
        /// </summary>
        public string July
        {
            get { return _july; }
            set { _july = value; }
        }

        /// <summary>
        /// August
        /// </summary>
        public string August
        {
            get { return _august; }
            set { _august = value; }
        }

        /// <summary>
        /// September
        /// </summary>
        public string September
        {
            get { return _september; }
            set { _september = value; }
        }
        /// <summary>
        /// October
        /// </summary>
        public string October
        {
            get { return _october; }
            set { _october = value; }
        }
        /// <summary>
        /// November
        /// </summary>
        public string November
        {
            get { return _november; }
            set { _november = value; }
        }
         /// <summary>
        /// December
        /// </summary>
        public string December
        {
            get { return _december; }
            set { _december = value; }
        }
        /// <summary>
        /// GrandTotal
        /// </summary>
        public string GrandTotal
        {
            get { return _grandTotal; }
            set { _grandTotal = value; }
        }
        
       
        #endregion
    }
}
