﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for GnrlCourseComplianceDTO
/// </summary>
/// 
namespace PearlsReview.DAL
{
    [Serializable()]
    public class GnrlCourseComplianceDTO
    {

    #region Private Variables
        private string _orgId;
        private string _facId;
        private string _deptIds;
        private string _facName;
        private string _deptSelect;  
        private string _deptName;
        private string _topicID;
        private string _coursename;      
        private string _orderBy;
        private string _viewDateStart;
        private string _viewDateEnd;
        private string _pstatus;
        private string _viewdate;
        private string _tuserid;
        private string _compdate;
        private string _score;
        private string _clastname;
        private string _cfirstname;
        private string _cusername;
        private string _primaryDiscipline;

        //private double _Score;
    #endregion

        #region Public Properties
        /// <summary>
        /// Org Id
        /// </summary>
        public string OrgId
        {
            get { return _orgId; }
            set { _orgId = value; }
        }
        
        /// <summary>
        /// Facility Id
        /// </summary>
        public string FacId
        {
            get { return _facId; }
            set { _facId = value; }
        }

        /// <summary>
        /// Facility Name
        /// </summary>
        public string FacName
        {
            get { return _facName; }
            set { _facName = value; }
        }

        /// <summary>
        /// Department IDs
        /// </summary>
        public string DeptIds
        {
            get { return _deptIds; }
            set { _deptIds = value; }
        } 
        
        /// <summary>
        /// Department Select
        /// </summary>
        public string DeptSelect
        {
            get { return _deptSelect; }
            set { _deptSelect = value; }
        }
               
        /// <summary>
        /// Department Name
        /// </summary>
        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }

        public string TopicID
        {
            get { return _topicID; }
            set { _topicID = value; }
        }
        
        /// <summary>
        /// Course Name
        /// </summary>
        public string CourseName
        {
            get { return _coursename; }
            set { _coursename = value; }
        }

        /// <summary>
        /// ViewDate Start
        /// </summary>

        public string ViewDateStart
        {
            get { return _viewDateStart; }
            set { _viewDateStart = value; }
        }

        /// <summary>
        /// View Date End
        /// </summary>
        public string ViewDateEnd
        {
            get { return _viewDateEnd; }
            set { _viewDateEnd = value; }
        } 

        /// <summary>
        /// Order By
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }

        /// <summary>
        /// User ID
        /// </summary>
        public string tuserid
        {
            get { return _tuserid ; }
            set { _tuserid = value; }
        }
        
        /// <summary>
        /// Completed date
        /// </summary>
        public string compdate
        {
            get { return _compdate; }
            set { _compdate = value; }
        }

         /// <summary>
        /// View date
        /// </summary>
        public string viewdate
        {
            get { return _viewdate; }
            set { _viewdate = value; }
        }

             
         /// <summary>
        /// Score
        /// </summary>
        public string score
        {
            get { return _score; }
            set { _score = value; }
        }

        /// <summary>
        /// First name
        /// </summary>
        public string cfirstname
        {
            get { return _cfirstname; }
            set { _cfirstname = value; }
        }

        /// <summary>
        /// Last name
        /// </summary>
        public string clastname
        {
            get { return _clastname; }
            set { _clastname = value; }
        }

        /// <summary>
        /// Last name
        /// </summary>
        public string cusername
        {
            get { return _cusername; }
            set { _cusername = value; }
        }
        /// <summary>
        /// Passstatus
        /// </summary>
        public string pstatus
        {
            get { return _pstatus; }
            set { _pstatus = value; }
        }

        /// <summary>
        /// Primary discipline
        /// </summary>
        public string PrimaryDiscipline
        {
            get { return _primaryDiscipline; }
            set { _primaryDiscipline = value; }
        }

        #endregion

    }
}
