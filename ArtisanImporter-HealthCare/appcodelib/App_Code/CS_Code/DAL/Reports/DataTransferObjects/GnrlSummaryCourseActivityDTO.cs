﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PearlsReview.DAL
{

/// <summary>
/// Summary description for GnrlSummaryCourseActivityDTO
/// </summary>
      [Serializable()]
    public class GnrlSummaryCourseActivityDTO
    {
        #region Private Variables

        private string _facIds;
        private string _deptIds;
        private string _course;
        private double _creitHrs;
        private string _testPassFromDate;
        private string _testPassToDate;
        private string _topicName;
        private string _userFullName;
        private string _deptName;
        private int _Avgscore;
        private string _testPassDate;
        private int _noOfCourses;
        private string _orderBy;
        private string _orgId;
        private string _FacName;
        private bool _Active;
        private string _primaryDiscipline;
    
        #endregion

        #region Public Properties

        /// <summary>
        /// Facility Ids
        /// </summary>
        public string FacIds
        {
            get { return _facIds; }
            set { _facIds = value; }
        }

        /// <summary>
        /// Org Ids
        /// </summary>
        public string OrgIds
        {
            get { return _orgId; }
            set { _orgId = value; }
        }

        /// <summary>
        /// FacNAme
        /// </summary>
        public string FacName
        {
            get { return _FacName; }
            set { _FacName = value; }
        }

        /// <summary>
        /// Department IDs
        /// </summary>
        public string DeptIds
        {
            get { return _deptIds; }
            set { _deptIds = value; }
        }

       
        /// <summary>
        /// Course
        /// </summary>
        public string Course
        {
            get { return _course; }
            set { _course = value; }
        }   
                   

        /// <summary>
        /// Creit Hrs
        /// </summary>
        public double CreitHrs
        {
            get { return _creitHrs; }
            set { _creitHrs = value; }
        }

        /// <summary>
        /// TestPass From Date
        /// </summary>
        public string TestPassFromDate
        {
            get { return _testPassFromDate; }
            set { _testPassFromDate = value; }
        }

        /// <summary>
        /// TestPass To Date
        /// </summary>
        public string TestPassToDate
        {
            get { return _testPassToDate; }
            set { _testPassToDate = value; }
        }

        /// <summary>
        ///Topic Name
        /// </summary>
        public string TopicName
        {
            get { return _topicName; }
            set { _topicName = value; }
        }
        /// <summary>
        ///User Full Name
        /// </summary>
        public string UserFullName
        {
            get { return _userFullName; }
            set { _userFullName = value; }
        }  
           /// <summary>
        ///Dept name
        /// </summary>
        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }
        /// <summary>
        ///Avg score
        /// </summary>
        public int AvgScore
        {
            get { return _Avgscore; }
            set { _Avgscore = value; }
        }
        /// <summary>
        ///Test Pass Date
        /// </summary>
        public string TestPassDate
        {
            get { return _testPassDate; }
            set { _testPassDate = value; }
        }
        /// <summary>
        ///No of Courses
        /// </summary>
        public int NoOfCourses
        {
            get { return _noOfCourses; }
            set { _noOfCourses = value; }
        }
        /// <summary>
        ///Order By
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }

        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

        public string PrimaryDiscipline
        {
            get { return _primaryDiscipline; }
            set { _primaryDiscipline = value; }
        }

        public GnrlSummaryCourseActivityDTO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #endregion
    }
}
