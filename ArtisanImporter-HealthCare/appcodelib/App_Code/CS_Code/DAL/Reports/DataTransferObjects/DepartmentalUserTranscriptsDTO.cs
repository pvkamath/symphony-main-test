﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
public class DepartmentalUserTranscriptsDTO
{  
        #region Private Variables

            private string _facIds;
            private string _deptIds;
            private string _deptSelect;            
            private string _activeInd;

            private string _fullName;
            private string _deptName;
            private string _courseTitle;
            private string _passDate;
            private string _score;
            private string _credits;
            private string _orderBy;

        #endregion

        #region Public Properties

            /// <summary>
            /// Facility Id
            /// </summary>
            public string FacIds
            {
                get { return _facIds; }
                set { _facIds = value; }
            }

            /// <summary>
            /// Department IDs
            /// </summary>
            public string DeptIds
            {
                get { return _deptIds; }
                set { _deptIds = value; }
            }

            /// <summary>
            /// Department Select
            /// </summary>
            public string DeptSelect
            {
                get { return _deptSelect; }
                set { _deptSelect = value; }
            }
           
            /// <summary>
            /// Active Ind
            /// </summary>
            public string ActiveInd
            {
                get { return _activeInd; }
                set { _activeInd = value; }
            }

            /// <summary>
            /// Full Name
            /// </summary>
            public string FullName
            {
                get { return _fullName; }
                set { _fullName = value; }
            }

            /// <summary>
            /// Department Name
            /// </summary>
            public string DeptName
            {
                get { return _deptName; }
                set { _deptName = value; }
            }           

            /// <summary>
            /// Course Title
            /// </summary>
            public string CourseTitle
            {
                get { return _courseTitle; }
                set { _courseTitle = value; }
            }

            /// <summary>
            /// Pass Date
            /// </summary>
            public string PassDate
            {
                get { return _passDate; }
                set { _passDate = value; }
            }

            /// <summary>
            /// Score
            /// </summary>
            public string Score
            {
                get { return _score; }
                set { _score = value; }
            }

            /// <summary>
            /// Credits
            /// </summary>
            public string Credits
            {
                get { return _credits; }
                set { _credits = value; }
            }

            /// <summary>
            /// Order By
            /// </summary>
            public string OrderBy
            {
                get { return _orderBy; }
                set { _orderBy = value; }
            }
        #endregion
    }
}
