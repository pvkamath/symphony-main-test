﻿using System;
using System.Text;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class RevenueByCourseDTO
    {
        #region Private Variables

      //  private string _categoryName;
        private string _courseNumber;
        private decimal _sumOfCost;
        private int _sumOfQuantity;
        private string _title;
        private int _topicid;
        private string _orderDate_start;
        private string _orderDate_end;
        private string _showSponsored;
        private string _showUnlimitedUsers;
        private decimal _sumOfOnlineQunatity;
        private decimal _sumOfOfflineQunatity;
        private decimal _sumOfOnlineSubTotal;
        private decimal _sumOfOfflineSubTotal;
        private decimal _sumOfSubTotal;
        private decimal _sumOfTax;
        private decimal _sumOfShip;
        private decimal _sumOfOnlineTotalCost;
        private decimal _sumOfOfflineTotalCost;
        private int _facilityid;
        private string _type;

        #endregion

        #region Public Properties

        //public string CategoryName
        //{
        //    get { return _categoryName; }
        //    set { _categoryName = value; }
        //}

        public string CourseNumber
        {
            get { return _courseNumber; }
            set { _courseNumber = value; }
        }       

        public decimal SumOfCost
        {
            get { return _sumOfCost; }
            set { _sumOfCost = value; }
        }

        public int SumOfQuantity
        {
            get { return _sumOfQuantity; }
            set { _sumOfQuantity = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public int TopicID
        {
            get { return _topicid; }
            set { _topicid = value; }
        }

        public string Type 
        {
            get { return _type; }
            set { _type = value; }
        }

        public string OrderDate_start
        {
            get { return _orderDate_start; }
            set { _orderDate_start = value; }
        }

        public string OrderDate_end
        {
            get { return _orderDate_end; }
            set { _orderDate_end = value; }
        }

        public string ShowSponsored
        {
            get { return _showSponsored; }
            set { _showSponsored = value; }
        }

        public string ShowUnlimitedUsers
        {
            get { return _showUnlimitedUsers; }
            set { _showUnlimitedUsers = value; }
        }
        public decimal sumOfOnLineSubTotal
        {
            get { return _sumOfOnlineSubTotal; }
            set { _sumOfOnlineSubTotal = value; }
        }
        public decimal sumOfOffLineSubTotal
        {
            get { return _sumOfOfflineSubTotal; }
            set { _sumOfOfflineSubTotal = value; }
        }
        public decimal sumOfOnLineTotalCost
        {
            get { return _sumOfOnlineTotalCost; }
            set { _sumOfOnlineTotalCost = value; }
        }
        public decimal sumOfOffLineTotalCost
        {
            get { return _sumOfOfflineTotalCost; }
            set { _sumOfOfflineTotalCost = value; }
        }
        public decimal sumOfOnLineQunatity
        {
            get { return _sumOfOnlineQunatity; }
            set { _sumOfOnlineQunatity = value; }
        }
        public decimal sumOfOffLineQunatity
        {
            get { return _sumOfOfflineQunatity; }
            set { _sumOfOfflineQunatity = value; }
        }
            public decimal sumOfSubTotal
        {
            get { return _sumOfSubTotal; }
            set { _sumOfSubTotal = value; }
        }
        public decimal sumOfTax
        {
            get { return _sumOfTax; }
            set { _sumOfTax = value; }

        }
        public decimal sumOfShip
        {
            get { return _sumOfShip; }
            set { _sumOfShip = value; }

        }

        public int FacilityId
        {
            get { return _facilityid; }
            set { _facilityid = value; }  
        }

        #endregion
    }

}

