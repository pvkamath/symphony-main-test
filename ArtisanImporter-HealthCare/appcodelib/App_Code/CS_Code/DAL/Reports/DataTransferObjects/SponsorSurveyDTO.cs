﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
    /// <summary>
    /// Summary description for SponsorSurveyDTO
    /// </summary>
    public class SponsorSurveyDTO
    {
        public SponsorSurveyDTO(){ }


        #region Private Variables       

        private int _discountid;
        private int _userID;
        private int _orderItemID;
        private string _userName;
        private string _address1;
        private string _address2;
        private string _city;
        private string _state;
        private string _zip;
        private string _email;
        private string _licenseStates;
        private int _questionID;
        private string _questionText;
        private string _answerText;


        private string _firstname;
        private string _lastname;
        private string _phone;
        private int _agegroup;
        private int _position_id;
        private string _specid;
        private string _orderdate;
        private string _startDate;
        private string _endDate;
        private string _courseNumber;
        private string _courseName;
        private string _education;
        private string _position_Name;
        private string _stateList;
        private string _view;
        private string _clickThrogh;
        private string _totalCompletion;
        private string _UniqueCompletion;
        private string _profname;


        #endregion

        #region Public Properties

        /// <summary>
        /// Discount Id
        /// </summary>
        public int DiscountId
        {
            get { return _discountid; }
            set { _discountid = value; }
        }


        /// <summary>
        /// User ID
        /// </summary>
        public int UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        /// <summary>
        /// Order Item ID
        /// </summary>
        public int OrderItemID
        {
            get { return _orderItemID; }
            set { _orderItemID = value; }
        }
        

        /// <summary>
        /// User Name
        /// </summary>
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        /// <summary>
        /// Address1
        /// </summary>
        public string Address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        /// <summary>
        /// Address2
        /// </summary>
        public string Address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        /// <summary>
        /// City
        /// </summary>
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        // <summary>
        /// State
        /// </summary>
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        // <summary>
        /// Zip
        /// </summary>
        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }

        // <summary>
        /// Email
        /// </summary>
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        // <summary>
        /// LicenseStates
        /// </summary>
        public string LicenseStates
        {
            get { return _licenseStates; }
            set { _licenseStates = value; }
        }


        /// <summary>
        /// Question ID
        /// </summary>
        public int QuestionID
        {
            get { return _questionID; }
            set { _questionID = value; }
        }       

        /// <summary>
        /// Question Text
        /// </summary>
        public string QuestionText
        {
            get { return _questionText; }
            set { _questionText = value; }
        }

        /// <summary>
        /// Answer Text
        /// </summary>
        public string AnswerText
        {
            get { return _answerText; }
            set { _answerText = value; }
        }


        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName
        {
            get { return _firstname; }
            set { _firstname = value; }
        }

        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName
        {
            get { return _lastname; }
            set { _lastname = value; }
        }

        /// <summary>
        /// Phone
        /// </summary>
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }
        /// <summary>
        /// Age Group
        /// </summary>
        public int AgeGroup
        {
            get { return _agegroup; }
            set { _agegroup = value; }
        }
        /// <summary>
        /// Position Id
        /// </summary>
        public int PositionId
        {
            get { return _position_id; }
            set { _position_id = value; }
        }

        /// <summary>
        /// Position Id
        /// </summary>
        public string PositionName
        {
            get { return _position_Name; }
            set { _position_Name = value; }
        }
        /// <summary>
        /// Specid
        /// </summary>
        public string Specid
        {
            get { return _specid; }
            set { _specid = value; }
        }
        /// <summary>
        /// Order Date
        /// </summary>
        public string OrderDate
        {
            get { return _orderdate; }
            set { _orderdate = value; }
        }
        /// <summary>
        /// Startdate
        /// </summary>
        public string StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }
        /// <summary>
        /// Order Date
        /// </summary>
        public string EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }
        /// <summary>
        /// CourseNumber
        /// </summary>
        public string CourseNumber
        {
            get { return _courseNumber; }
            set { _courseNumber = value; }
        }
        public string CourseName
        {
            get { return _courseName; }
            set { _courseName = value; }
        }
        public string Education
        {
            get { return _education; }
            set { _education = value; }
        }
        public string View
        {
            get { return _view; }
            set { _view = value; }
        }
        public string StateList
        {
            get { return _stateList; }
            set { _stateList = value; }
        }
        public string clickThrogh
        {
            get { return _clickThrogh; }
            set { _clickThrogh = value; }
        }
        public string totalCompletion
        {
            get { return _totalCompletion; }
            set { _totalCompletion = value; }
        }
        public string UniqueCompletion
        {
            get { return _UniqueCompletion; }
            set { _UniqueCompletion = value; }
        }
        public string Profname
        {
            get { return _profname; }
            set { _profname = value; }
        }

        #endregion
    }
}
