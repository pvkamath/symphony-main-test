﻿using System;
using System.Text;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class RevenueByAuthorDTO
    {
        #region Private Variables

        private string _fromDate;
        private string _toDate;
        private string _orderBy;
        private int _rowid;
        private int _resourceid;
        private string _fullname;
        private decimal _totalRevenue;
        private string _lastfirst;
        private string _degrees;
        private string _position;
        private string _address;
        private string _city;
        private string _state;
        private string _phone;
        private string _fax;
        private string _email;
        private string _specialty;        

        #endregion

        #region Public Properties

        public string FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }

        public string ToDate
        {
            get { return _toDate; }
            set { _toDate = value; }
        }

        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }

        public int RowID
        {
            get { return _rowid; }
            set { _rowid = value; }
        }

        public int ResourceID 
        {
            get { return _resourceid; }
            set { _resourceid = value; }
        }

        public string FullName
        {
            get { return _fullname; }
            set { _fullname = value; }
        }
      
        public decimal TotalRevenue
        {
            get { return _totalRevenue; }
            set { _totalRevenue = value; }
        }

        public string LastFirst
        {
            get { return _lastfirst; }
            set { _lastfirst = value; }
        }
        
        public string Degrees
        {
            get { return _degrees; }
            set { _degrees = value; }
        }

        public string Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        public string Fax
        {
            get { return _fax; }
            set { _fax = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string Specialty
        {
            get { return _specialty; }
            set { _specialty = value; }
        }

        #endregion
    }
}

