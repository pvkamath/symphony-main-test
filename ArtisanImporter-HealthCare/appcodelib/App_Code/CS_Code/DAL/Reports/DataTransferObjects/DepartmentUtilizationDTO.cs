﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class DepartmentUtilizationDTO
    {
        #region Private Variables
            private string _orgIds;
            private string _facIds;
            private string _deptIds;
            private string _facName;
            private string _deptSelect;            
            //private string _creitHrs;            
            private string _deptName;
            private double _contactHours;  
            private double _completeHours;
            private string _orderBy;
            private string _viewDateStart;
            private string _viewDateEnd; 
            private string _primaryDiscipline;

        #endregion

        #region Public Properties
            /// <summary>
            /// Org Id
            /// </summary>
            public string OrgIds
            {
                get { return _orgIds; }
                set { _orgIds = value; }
            }



            /// <summary>
            /// Facility Id
            /// </summary>
            public string FacIds
            {
                get { return _facIds; }
                set { _facIds = value; }
            }

            /// <summary>
            /// Department IDs
            /// </summary>
            public string DeptIds
            {
                get { return _deptIds; }
                set { _deptIds = value; }
            }
            /// <summary>
            /// ViewDate Start
            /// </summary>

            public string ViewDateStart
            {
                get { return _viewDateStart; }
                set { _viewDateStart = value; }
            }

            /// <summary>
            /// View Date End
            /// </summary>
            public string ViewDateEnd
            {
                get { return _viewDateEnd; }
                set { _viewDateEnd = value; }
            }

            /// <summary>
            /// Department Select
            /// </summary>
            public string DeptSelect
            {
                get { return _deptSelect; }
                set { _deptSelect = value; }
            }
           
            ///// <summary>
            ///// Creit Hrs
            ///// </summary>
            //public string CreitHrs
            //{
            //    get { return _creitHrs; }
            //    set { _creitHrs = value; }
            //}

            /// <summary>
            /// Facility Name
            /// </summary>
            public string FacName
            {
                get { return _facName; }
                set { _facName = value; }
            }  


            /// <summary>
            /// Department Name
            /// </summary>
            public string DeptName
            {
                get { return _deptName; }
                set { _deptName = value; }
            }

            /// <summary>
            /// Contact Hours
            /// </summary>
            public double ContactHours
            {
                get { return _contactHours; }
                set { _contactHours = value; }
            }


            /// <summary>
            /// Complete Hours
            /// </summary>
            public double CompleteHours
            {
                get { return _completeHours; }
                set { _completeHours = value; }
            }

            /// <summary>
            /// Order By
            /// </summary>
            public string OrderBy
            {
                get { return _orderBy; }
                set { _orderBy = value; }
            }

            /// <summary>
            /// Primary discipline
            /// </summary>
            public string PrimaryDiscipline
            {
                get { return _primaryDiscipline; }
                set { _primaryDiscipline = value; }
            }
        #endregion
    }
}
