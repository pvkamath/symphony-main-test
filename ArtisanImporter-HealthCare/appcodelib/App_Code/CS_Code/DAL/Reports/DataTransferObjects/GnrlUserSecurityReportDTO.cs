﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for GnrlUserSecurityReportDTO
/// </summary>
///

namespace PearlsReview.DAL
{
    [Serializable()]
    public class GnrlUserSecurityReportDTO
    {
        #region Private Variables

        private string _facIds;
        private string _userName;
        private string _FacName;
        private string _deptName;
        private string _lastLogin;
        private string _PositionName;
        private string _orderBy;
        private string _deptIds;
        private string _LastName;
        private string _FirstName;
        private string _AccessType;
        private string _OrgId;
        private string _PrimaryDiscipline;


        #endregion

        #region Public Properties

        /// <summary>
        /// Facility Id
        /// </summary>
        public string FacIds
        {
            get { return _facIds; }
            set { _facIds = value; }
        }
        /// <summary>
        /// Org Id
        /// </summary>
        public string OrgId
        {
            get { return _OrgId; }
            set { _OrgId = value; }
        }
        /// <summary>
        /// Department IDs
        /// </summary>
        public string DeptIds
        {
            get { return _deptIds; }
            set { _deptIds = value; }
        }
        /// <summary>
        /// User Name
        /// </summary>
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
      
        /// <summary>
        /// User Full Name
        /// </summary>
        public string FacName
        {
            get { return _FacName; }
            set { _FacName = value; }
        }


        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        /// <summary>
        /// First Name
        /// </summary>
        public string AccessType
        {
            get { return _AccessType; }
            set { _AccessType = value; }
        }

        /// <summary>
        /// Department Name
        /// </summary>
        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }

        /// <summary>
        /// Last Login
        /// </summary>
        public string LastLogin
        {
            get { return _lastLogin; }
            set { _lastLogin = value; }
        }          

        /// <summary>
        /// Position Name
        /// </summary>
        public string PositionName
        {
            get { return _PositionName; }
            set { _PositionName = value; }
        }        
        /// <summary>
        /// Order By
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }
        /// <summary>
        /// Primary Discipline
        /// </summary>
        public string PrimaryDiscipline
        {
            get { return _PrimaryDiscipline; }
            set { _PrimaryDiscipline = value; }
        }
        #endregion
    }
    //
    // TODO: Add constructor logic here
    //

}
