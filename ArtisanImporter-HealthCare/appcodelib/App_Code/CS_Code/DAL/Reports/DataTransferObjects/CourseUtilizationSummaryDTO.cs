﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class CourseUtilizationSummaryDTO
    {
        #region Private Variables     
           
            private string _facIds;
            private string _deptIds;
            private string _departmentID;
            private string _categoryId;
            private string _courseId;
            private string _creitHrs;
            private string _viewDateStart;
            private string _viewDateEnd;

            private string _deptName;
            private string _categoryName;
            private string _courseName;            
            private string _views;
            private string _tests;
            private string _completions;
            private string _orderBy;
            private string _totalcHours;

        #endregion

        #region Public Properties

            /// <summary>
            /// Facility Id
            /// </summary>
            public string FacIds
            {
                get { return _facIds; }
                set { _facIds = value; }
            }

            /// <summary>
            /// Department IDs
            /// </summary>
            public string DeptIds
            {
                get { return _deptIds; }
                set { _deptIds = value; }
            }

            /// <summary>
            /// Drop down selection Department ID
            /// </summary>
            public string DepartmentID
            {
                get { return _departmentID; }
                set { _departmentID = value; }
            }

            /// <summary>
            /// Course Category
            /// </summary>
            public string CategoryId
            {
                get { return _categoryId; }
                set { _categoryId = value; }
            }

            /// <summary>
            /// Course Category
            /// </summary>
            public string CourseId
            {
                get { return _courseId; }
                set { _courseId = value; }
            }

            /// <summary>
            /// Creit Hrs
            /// </summary>
            public string CreitHrs
            {
                get { return _creitHrs; }
                set { _creitHrs = value; }
            }

            /// <summary>
            /// View Date Start
            /// </summary>
            public string ViewDateStart
            {
                get { return _viewDateStart; }
                set { _viewDateStart = value; }
            }

            /// <summary>
            /// View Date End
            /// </summary>
            public string ViewDateEnd
            {
                get { return _viewDateEnd; }
                set { _viewDateEnd = value; }
            }

            /// <summary>
            /// Department Name
            /// </summary>
            public string DeptName
            {
                get { return _deptName; }
                set { _deptName = value; }
            }

            /// <summary>
            /// Category Name
            /// </summary>
            public string CategoryName
            {
                get { return _categoryName; }
                set { _categoryName = value; }
            }
            
            /// <summary>
            /// Course Name
            /// </summary>
            public string CourseName
            {
                get { return _courseName; }
                set { _courseName = value; }
            }
        

            /// <summary>
            /// views
            /// </summary>
            public string Views
            {
                get { return _views; }
                set { _views = value; }
            }

            /// <summary>
            /// Tests
            /// </summary>
            public string Tests
            {
                get { return _tests; }
                set { _tests = value; }
            }

            /// <summary>
            /// Completions
            /// </summary>
            public string Completions
            {
                get { return _completions; }
                set { _completions = value; }
            }

            /// <summary>
            /// Total Contact Hours which is the multiplication of credit hours and completions
            /// </summary>
            public string TotalcHours
            {
                get { return _totalcHours; }
                set { _totalcHours = value; }
            }
        
            /// <summary>
            /// Order By
            /// </summary>
            public string OrderBy
            {
                get { return _orderBy; }
                set { _orderBy = value; }
            }
        #endregion
    }
}

