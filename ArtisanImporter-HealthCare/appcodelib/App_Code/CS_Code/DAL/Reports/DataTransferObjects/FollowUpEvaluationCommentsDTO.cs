﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for EvaluationCommentsDTO
/// </summary>
namespace PearlsReview.DAL
{
    [Serializable()]
    public class FollowUpEvaluationCommentsDTO
    {
        public FollowUpEvaluationCommentsDTO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region Private Variables
        private string _surveydate_start;
        private string _surveydate_end;
        private string _courseIds;
        private string _orderBy;
        private string _topicId;
        private string _topicName;
        private string _surveyDate;
        private string _course_number;        
        private string _QuestionComment2;
        private string _QuestionComment4;
        private string _QuestionComment6;
        private string _QuestionComment7;
        private string _QuestionComment8;
        private string _QuestionComment9;
        #endregion

        #region Public Properties

        /// <summary>
        /// Course Ids
        /// </summary>
        public string CourseIds
        {
            get { return _courseIds; }
            set { _courseIds = value; }
        }

        /// <summary>
        /// SurveyStartDate
        /// </summary>
        public string SurveyStartDate
        {
            get { return _surveydate_start; }
            set { _surveydate_start = value; }
        }

        /// <summary>
        /// SurveyEndDate
        /// </summary>
        public string SurveyEndDate
        {
            get { return _surveydate_end; }
            set { _surveydate_end = value; }
        }

        /// <summary>
        /// Order By 
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }


        /// <summary>
        /// Topic Id
        /// </summary>
        public string TopicId
        {
            get { return _topicId; }
            set { _topicId = value; }
        }

        /// <summary>
        /// Topic Name
        /// </summary>
        public string TopicName
        {
            get { return _topicName; }
            set { _topicName = value; }
        }

        /// <summary>
        /// Survey Date
        /// </summary>
        public string SurveyDate
        {
            get { return _surveyDate; }
            set { _surveyDate = value; }
        }

        /// <summary>
        /// Course Number
        /// </summary>
        public string Course_Number
        {
            get { return _course_number; }
            set { _course_number = value; }
        }
                
        
        public string QuestionComment2
        {
            get { return _QuestionComment2; }
            set { _QuestionComment2 = value; }
        }

        public string QuestionComment4
        {
            get { return _QuestionComment4; }
            set { _QuestionComment4 = value; }
        }

        public string QuestionComment6
        {
            get { return _QuestionComment6; }
            set { _QuestionComment6 = value; }
        }

        public string QuestionComment7
        {
            get { return _QuestionComment7; }
            set { _QuestionComment7 = value; }
        }

        public string QuestionComment8
        {
            get { return _QuestionComment8; }
            set { _QuestionComment8 = value; }
        }

        public string QuestionComment9
        {
            get { return _QuestionComment9; }
            set { _QuestionComment9 = value; }
        }


        #endregion


    }
}