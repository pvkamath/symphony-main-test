﻿using System;
using System.Text;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class SalesByStateDTO
    {
        #region Private Variables

        private int _OrderID = 0;
        private int _UserID = 0;
        private string _FirstName = "";
        private string _LastName = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private string _Couponcode = "";
        private string _City = "";
        private string _State = "";
        private string _Zip = "";
        private string _Country = "";
        private string _Email = "";
        private decimal _SubTotal = 0;
        private decimal _ShipCost = 0;
        private decimal _Tax = 0;
        private decimal _TotalCost = 0;
        private int _Validate_Ind = 0;
        private string _Verification = "";        
        private int _Ship_Ind = 0;
        private DateTime _OrderDate = System.DateTime.MinValue;        
        private string _Comment = "";
        private string _orderDateStart = "";
        private string _orderDateEnd = "";
        private string _rType = "";
        private string _StateName;
        private decimal _ShipSum ;
        private decimal _taxSum;
        private decimal _sumTotal;        
        private decimal _Gtotal;
        private decimal _sumGtotal;
        private decimal _Discount;
        private decimal _sumDiscount;
        private int _facilityid;


        #endregion

        #region Public Properties

        public int OrderID
        {
            get { return _OrderID; }
            set { _OrderID = value; }
        }

        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }


        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }


        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }


        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        public string Couponcode
        {
            get { return _Couponcode; }
            set { _Couponcode = value; }
        }


        public string City
        {
            get { return _City; }
            set { _City = value; }
        }
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        public string Zip
        {
            get { return _Zip; }
            set { _Zip = value; }
        }

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public decimal SubTotal
        {
            get { return _SubTotal; }
            set { _SubTotal = value; }
        }

        public decimal ShipCost
        {
            get { return _ShipCost; }
            set { _ShipCost = value; }
        }


        public decimal Tax
        {
            get { return _Tax; }
            set { _Tax = value; }
        }

        public decimal TotalCost
        {
            get { return _TotalCost; }
            set { _TotalCost = value; }
        }

        public int Validate_Ind
        {
            get { return _Validate_Ind; }
            set { _Validate_Ind = value; }
        }

        public string Verification
        {
            get { return _Verification; }
            set { _Verification = value; }
        }

        public int Ship_Ind
        {
            get { return _Ship_Ind; }
            set { _Ship_Ind = value; }
        }

        public DateTime OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; }
        }

        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        public string OrderDateStart
        {
            get { return _orderDateStart; }
            set { _orderDateStart = value; }
        }
        public string OrderDateEnd
        {
            get { return _orderDateEnd; }
            set { _orderDateEnd = value; }
        }
        public string RType
        {
            get { return _rType; }
            set { _rType = value; }
        }


        public string StateName
        {
            get { return _StateName; }
            set { _StateName = value; }
        }

        public decimal ShipSum
        {
            get { return _ShipSum; }
            set { _ShipSum = value; }
        }
        public decimal TaxSum
        {
            get { return _taxSum; }
            set { _taxSum = value; }
        }

        public decimal SumTotal
        {
            get { return _sumTotal; }
            set { _sumTotal = value; }
        }

        public decimal SumDiscount
        {
            get { return _sumDiscount; }
            set { _sumDiscount = value; }
        }

        public decimal Gtotal
        {
            get { return _Gtotal; }
            set { _Gtotal = value; }
        }

        public decimal SumGtotal
        {
            get { return _sumGtotal; }
            set { _sumGtotal = value; }
        }

        public decimal Discount
        {
            get { return _Discount; }
            set { _Discount = value; }
        }

        public int FacilityId
        {
            get { return _facilityid; }
            set { _facilityid = value; }
        }
        #endregion
    }
}
