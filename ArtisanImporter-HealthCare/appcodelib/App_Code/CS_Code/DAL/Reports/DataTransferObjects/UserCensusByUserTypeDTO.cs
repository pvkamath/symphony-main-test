﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
public class UserCensusByUserTypeDTO
{	
    #region Private Variables

        private string _facIds;
        private string _facName;
        private string _displayname;
        private int _totalUsers;

    #endregion

    #region Public Properties


        /// <summary>
        /// Facility Id
        /// </summary>
        public string FacIds
        {
            get { return _facIds; }
            set { _facIds = value; }
        }

        /// <summary>
        /// Facility Name
        /// </summary>
        public string FacName
        {
            get { return _facName; }
            set { _facName = value; }
        }

        /// <summary>
        /// Role Name
        /// </summary>
        public string RoleName
        {
            get { return _displayname; }
            set { _displayname = value; }
        }

        /// <summary>
        /// Total Users
        /// </summary>
        public int TotalUsers
        {
            get { return _totalUsers; }
            set { _totalUsers = value; }
        }  

    #endregion
}
}
