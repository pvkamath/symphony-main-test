﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class CourseActivityDTO
    {
        #region Private Variables       

            private string _facIds;
            private string _deptIds;            
            private string _categoryId;            
            private string _course;
            private string _courseId;
            private string _userFirstName;
            private string _userLastName;
            private string _title;                   
            private string _badgeID;           
            private string _sSN;
            private string _isActive;
            private string _deptSelect;
            private string _creitHrs;
            private string _viewFromDate;
            private string _viewToDate;
            private string _testPassFromDate;
            private string _testPassToDate;
            private string _printedFromDate;
            private string _printedToDate;           

            private string _categoryName; 
            private string _topicName;
            private string _userFullName;
            private string _deptName;
            private string _score;
            private string _viewDate;
            private string _testPassDate;
            private string _printedDate;
            private string _orderBy;
            private string _UserId;
            private string _License_Include;
            private string _primaryDiscipline;


        #endregion

        #region Public Properties

            /// <summary>
            /// Facility Ids
            /// </summary>
            public string FacIds
            {
                get { return _facIds; }
                set { _facIds = value; }
            }

            /// <summary>
            /// Department IDs
            /// </summary>
            public string DeptIds
            {
                get { return _deptIds; }
                set { _deptIds = value; }
            }

            /// <summary>
            /// Category Id
            /// </summary>
            public string CategoryId
            {
                get { return _categoryId; }
                set { _categoryId = value; }
            }


            /// <summary>
            /// Course
            /// </summary>
            public string Course
            {
                get { return _course; }
                set { _course = value; }
            }

            /// <summary>
            /// Course Id
            /// </summary>
            public string CourseId
            {
                get { return _courseId; }
                set { _courseId = value; }
            }

            /// <summary>
            /// User First Name
            /// </summary>
            public string UserFirstName
            {
                get { return _userFirstName; }
                set { _userFirstName = value; }
            }
            public string UserId
            {
                get { return _UserId; }
                set { _UserId = value; }
            }
           
         /// <summary>
            /// User Lat Name
            /// </summary>
            public string UserLastName
            {
                get { return _userLastName; }
                set { _userLastName = value; }
            }

            /// <summary>
            /// User Name
            /// </summary>
            public string Title
            {
                get { return _title; }
                set { _title = value; }
            }

            /// <summary>
            /// Badge ID
            /// </summary>
            public string BadgeID
            {
                get { return _badgeID; }
                set { _badgeID = value; }
            }

            /// <summary>
            /// SSN
            /// </summary>
            public string sSN
            {
                get { return _sSN; }
                set { _sSN = value; }
            }

            /// <summary>
            /// Is Active
            /// </summary>
            public string IsActive
            {
                get { return _isActive; }
                set { _isActive = value; }
            }

            /// <summary>
            /// Dept Select
            /// </summary>
            public string DeptSelect
            {
                get { return _deptSelect; }
                set { _deptSelect = value; }
            }

            /// <summary>
            /// Creit Hrs
            /// </summary>
            public string CreitHrs
            {
                get { return _creitHrs; }
                set { _creitHrs = value; }
            }

            /// <summary>
            /// View From Date
            /// </summary>
            public string ViewFromDate
            {
                get { return _viewFromDate; }
                set { _viewFromDate = value; }
            }

            /// <summary>
            /// View To Date
            /// </summary>
            public string ViewToDate
            {
                get { return _viewToDate; }
                set { _viewToDate = value; }
            }

            /// <summary>
            /// Test Pass From Date
            /// </summary>
            public string TestPassFromDate
            {
                get { return _testPassFromDate; }
                set { _testPassFromDate = value; }
            }

            /// <summary>
            /// Test Pass ToDate
            /// </summary>
            public string TestPassToDate
            {
                get { return _testPassToDate; }
                set { _testPassToDate = value; }
            }

            /// <summary>
            /// Printed From Date
            /// </summary>
            public string PrintedFromDate
            {
                get { return _printedFromDate; }
                set { _printedFromDate = value; }
            }

            /// <summary>
            /// Printed To Date
            /// </summary>
            public string PrintedToDate
            {
                get { return _printedToDate; }
                set { _printedToDate = value; }
            }  

            /// <summary>
            /// Category Name
            /// </summary>
            public string CategoryName
            {
                get { return _categoryName; }
                set { _categoryName = value; }
            }

            /// <summary>
            /// Topic Name
            /// </summary>
            public string TopicName
            {
                get { return _topicName; }
                set { _topicName = value; }
            }

            /// <summary>
            /// User Full Name
            /// </summary>
            public string UserFullName
            {
                get { return _userFullName; }
                set { _userFullName = value; }
            }

            /// <summary>
            /// Department Name
            /// </summary>
            public string DeptName
            {
                get { return _deptName; }
                set { _deptName = value; }
            }

            /// <summary>
            /// Score
            /// </summary>
            public string Score
            {
                get { return _score; }
                set { _score = value; }
            }

            /// <summary>
            /// View Date
            /// </summary>
            public string ViewDate
            {
                get { return _viewDate; }
                set { _viewDate = value; }
            }

            /// <summary>
            /// Test Pass Date
            /// </summary>
            public string TestPassDate
            {
                get { return _testPassDate; }
                set { _testPassDate = value; }
            }

            /// <summary>
            /// Certificate Printed Date
            /// </summary>
            public string PrintedDate
            {
                get { return _printedDate; }
                set { _printedDate = value; }
            }

            /// <summary>
            /// Order By
            /// </summary>
            public string OrderBy
            {
                get { return _orderBy; }
                set { _orderBy = value; }
            }

            public string License_Include
            {
                get { return _License_Include; }
                set { _License_Include = value; }
            }
            /// <summary>
            /// Primary Discipline
            /// </summary>
            public string PrimaryDiscipline
            {
                get { return _primaryDiscipline; }
                set { _primaryDiscipline = value; }
            }
       

        #endregion
    }
}
