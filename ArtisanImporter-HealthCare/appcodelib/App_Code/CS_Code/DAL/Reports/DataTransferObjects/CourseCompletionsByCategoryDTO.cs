﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for RevenueByUserDTO
/// </summary>
/// 
namespace PearlsReview.DAL
{
    [Serializable()]

    public class CourseCompletionsByCategoryDTO
    {
        #region Private Variables

        private int _facilityid;
        private string _OrderDate_start;
        private string _OrderDate_End;
        private string _seltype;
        private int _categoryid;
        private string _categoryname;
        private int _year;
        private int _month;
        private string _statename;
        private string _topicname;
        private string _coursenumber;
        private decimal _score;
        private int _total;
        private int _totalunique;    

        #endregion


        #region Public Variables      

        /// <summary>
        /// Facility id
        /// </summary>
        public int FacilityID
        {
            get { return _facilityid; }
            set { _facilityid = value; }
        }

        /// <summary>
        /// OrderDate_Start
        /// </summary>
        public string OrderDate_Start
        {
            get { return _OrderDate_start; }
            set { _OrderDate_start = value; }
        }


        /// <summary>
        /// OrderDate_End
        /// </summary>
        public string OrderDate_End
        {
            get { return _OrderDate_End; }
            set { _OrderDate_End = value; }
        }

        /// <summary>
        /// seltype
        /// </summary>
        public string SelType
        {
            get { return _seltype; }
            set { _seltype = value; }
        }

        /// <summary>
        /// Category id
        /// </summary>
        public int CategoryId
        {
            get { return _categoryid; }
            set { _categoryid = value; }
        }

        /// <summary>
        /// Category name
        /// </summary>
        public string CategoryName
        {
            get { return _categoryname; }
            set { _categoryname = value; }
        }

        /// <summary>
        /// Year
        /// </summary>
        public int Year
        {
            get { return _year; }
            set { _year = value; }
        }

        /// <summary>
        /// Month
        /// </summary>
        public int Month
        {
            get { return _month; }
            set { _month = value; }
        }

        /// <summary>
        /// statename
        /// </summary>
        public string StateName
        {
            get { return _statename; }
            set { _statename = value; }
        }

        /// <summary>
        /// topicname
        /// </summary>
        public string topicname
        {
            get { return _topicname ; }
            set { _topicname = value; }
        }

        /// <summary>
        /// course number
        /// </summary>
        public string coursenumber
        {
            get { return _coursenumber; }
            set { _coursenumber = value; }
        }

        /// <summary>
        /// score
        /// </summary>
        public decimal score
        {
            get { return _score; }
            set { _score = value; }
        }     
       
        /// <summary>
        /// total
        /// </summary>
        public int total
        {
            get { return _total; }
            set { _total = value; }
        }

        /// <summary>
        /// total unique
        /// </summary>
        public int totalUnique
        {
            get { return _totalunique; }
            set { _totalunique = value; }
        }     
     
        #endregion
    }
}