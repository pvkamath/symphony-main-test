﻿using System;
using System.Text;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class LookupDTO
    {
        #region Private Variables

        private string _itemValue;
        private string _itemText;
        private string _CourseID;
        private string _CourseName;

        #endregion

        #region Public Properties

        /// <summary>
        /// ItemValue
        /// </summary>
        public string ItemValue
        {
            get { return _itemValue; }
            set { _itemValue = value; }
        }

        /// <summary>
        /// ItemText
        /// </summary>
        public string ItemText
        {
            get { return _itemText; }
            set { _itemText = value; }
        }

        public string CourseID
        {
            get { return _CourseID; }
            set { _CourseID = value; }
        }
        public string CourseName
        {
            get { return _CourseName; }
            set { _CourseName = value; }
        }
        #endregion

    }
}