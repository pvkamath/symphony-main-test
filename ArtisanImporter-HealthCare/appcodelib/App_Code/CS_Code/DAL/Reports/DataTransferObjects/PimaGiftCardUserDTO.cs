﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PimaGiftCardUser
/// </summary>
namespace PearlsReview.DAL
{
    [Serializable()]
	public class PimaGiftCardUserDTO
	{
		//
		// TODO: Add constructor logic here
		//
         #region Private Variables


            private string _userFirstName;
            private string _userLastName;
            private string _userID;
            private string _employer;
            private string _primaryProfession;          
            private string _cAddress1;
            private string _cAddress2;
            private string _cCity;
            private string _cZipCode;
            private string _cStateCode;
            private string _cEmail;
            private string _createdDate;
            private string _lastLogin;
            private int _numberOfCourses;
            private decimal _contactHours;
            private  string _startDate;
            private string _endDate;
        #endregion

        #region Public Properties  


            /// <summary>
            /// Employer
            /// </summary>
            public string Employer
            {
                get { return _employer; }
                set { _employer = value; }
            }


            /// <summary>
            /// User First Name
            /// </summary>
            public string UserFirstName
            {
                get { return _userFirstName; }
                set { _userFirstName = value; }
            }

            /// <summary>
            /// User Lat Name
            /// </summary>
            public string UserLastName
            {
                get { return _userLastName; }
                set { _userLastName = value; }
            }
          
            /// <summary>
            /// User address1
            /// </summary>
            public string CAddress1
            {         
                get { return _cAddress1; }
                set { _cAddress1 = value; }
            }
           
            /// CCity
            /// </summary>
            public string CCity
            {
                get { return _cCity; }
                set { _cCity = value; }
            }
        
            /// <summary>
            /// C Zip Code
            /// </summary>
            public string CZipCode
            {
                get { return _cZipCode; }
                set { _cZipCode = value; }
            }          

            /// <summary>
            /// CEmail
            /// </summary>
            public string CEmail
            {
                get { return _cEmail; }
                set { _cEmail = value; }
            }     
            /// <summary>artDate
            /// Hire Date
            /// </summary>
            public string StartDate
            {
                get { return _startDate; }
                set { _startDate = value; }
            }

            /// <summary>
            /// Termin Date
            /// </summary>
            public string EndDate
            {
                get { return _endDate; }
                set { _endDate = value; }
            }

            /// <summary>
            /// User Id
            /// </summary>
            public string UserID
            {
                get { return _userID; }
                set { _userID = value; }
            }
            ///<summary>
            ///Primary Profession
            ///</summary>
            ///
            public string PrimayProfession
            {
                get { return _primaryProfession; }
                set { _primaryProfession = value; }
            }
            /// <summary>
            /// State
            /// </summary>
            public string StateCode
            {
                get { return _cStateCode;}
                set { _cStateCode = value;}
            }
            /// <summary>
            /// Created Date
            /// </summary>
            public  string CreatedDate
            {
                get{ return _createdDate;}
                set { _createdDate = value;}
            }
            /// <summary>
            /// Last Login
            /// </summary>
            public string LastLogin
            {
                get { return _lastLogin;}
                set { _lastLogin = value;}
            }
            /// <summary>
            /// Number of Courses
            /// </summary>
            public int NumberOfCourses
            {
                get { return _numberOfCourses;}
                set { _numberOfCourses = value;}
            }
            /// <summary>
            /// Contact Hours
            /// </summary>
            public decimal ContactHours
            {
                get { return  _contactHours;}
                set { _contactHours = value;}
	        }
    #endregion
    }
}