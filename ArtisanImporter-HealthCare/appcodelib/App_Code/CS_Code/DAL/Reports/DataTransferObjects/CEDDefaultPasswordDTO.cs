﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CEDDefaultPassword
/// </summary>
namespace PearlsReview.DAL
{
    [Serializable()]
    public class CEDDefaultPasswordDTO
    {	
        #region Private Variables   
    
        private string _facilityName;
        private string _Password; 
 
        #endregion

        #region Public Properties

        /// <summary>
        /// Facility Name
        /// </summary>
        public string FacilityName
        {
            get { return _facilityName; }
            set { _facilityName = value; }
        }

        /// <summary>
        /// Password
        /// </summary>
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        #endregion
    }
}