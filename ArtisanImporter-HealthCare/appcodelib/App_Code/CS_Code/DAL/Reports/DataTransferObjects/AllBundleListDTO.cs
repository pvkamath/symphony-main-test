﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for AllCoursesByCategory
/// </summary>
/// 
namespace PearlsReview.DAL
{
    [Serializable()]
    public class AllBundleListDTO
    {
        public AllBundleListDTO()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Private Variables
        private string _bundleName;
        
        #endregion

        #region Public Properties

        public string BundleName
        { 
            get { return _bundleName; }
            set { _bundleName = value; }
        }
        #endregion
    }
}
