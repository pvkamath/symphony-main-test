﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for RevenueByDtRangeDTO
/// </summary>
/// 
namespace PearlsReview.DAL
{
    [Serializable()]

    public class RevenueByDtRangeDTO
    {
        #region Private Variables

        private string _course_number;
        private string _seltype;
        private string _OrderDate_start;
        private string _OrderDate_End;
        private int _topicid;
        private string _topicname;
        private decimal _credits;
        private string _lastmod;
        private bool _status;
        private string _score;
        private int _testid;
        private string _cfirstname;
        private string _clastname;
        private int _uniqueid;
        private int _views;
        private string _mediatype;
        private int _userid;
        private int _orderid;
        private string _firstname;
        private string _lastname;
        private int _orderitemid;
        private string _address1;
        private string _city;
        private string _state;
        private string _zip;
        private string _email;
        private string _media_type;
        private string _comment;
        private decimal _cost;
        private int _discountid;
        private bool _optin_ind;
        private string _orderdate;
        private int _quantity;
        private decimal _shipcost;
        private bool _ship_ind;
        private string _verification;
        private decimal _totalcost;
        private decimal _tax;
        private decimal _subtotal; 
        private decimal _ShipSum;
        private decimal _taxSum;
        private decimal _sumTotal;
        private decimal _Gtotal;
        private int _facilityid;


        #endregion


        #region Public Variables
        /// <summary>
        /// course_number
        /// </summary>
        
        public string Course_Number
        {
            get { return _course_number; }
            set { _course_number = value; }
        }

        /// <summary>
        /// seltype
        /// </summary>
        public string SelType
        {
            get { return _seltype; }
            set { _seltype = value; }
        }

        /// <summary>
        /// OrderDate_Start
        /// </summary>
        public string OrderDate_Start
        {
            get { return _OrderDate_start; }
            set { _OrderDate_start = value; }
        }


        /// <summary>
        /// OrderDate_End
        /// </summary>
        public string OrderDate_End
        {
            get { return _OrderDate_End; }
            set { _OrderDate_End = value; }
        }

        /// <summary>
        /// topicid
        /// </summary>
        public int topicid
        {
            get { return _topicid; }
            set { _topicid = value; }
        }

        /// <summary>
        /// topicname
        /// </summary>
        public string topicname
        {
            get { return _topicname ; }
            set { _topicname = value; }
        }

        /// <summary>
        /// credits
        /// </summary>
        public decimal credits
        {
            get { return _credits; }
            set { _credits = value; }
        }

        /// <summary>
        /// lastmod
        /// </summary>
        public string lastmod
        {
            get { return _lastmod; }
            set { _lastmod = value; }
        }


        /// <summary>
        /// status
        /// </summary>
        public bool status
        {
            get { return _status; }
            set { _status = value; }
        }

        /// <summary>
        /// score
        /// </summary>
        public string score
        {
            get { return _score; }
            set { _score = value; }
        }

        /// <summary>
        /// testid
        /// </summary>
        public int testid
        {
            get { return _testid; }
            set { _testid = value; }
        }

        /// <summary>
        /// Firstname
        /// </summary>
        public string cfirstname
        {
            get { return _cfirstname; }
            set { _cfirstname = value; }
        }

        /// <summary>
        /// Lastname
        /// </summary>
        public string clastname
        {
            get { return _clastname; }
            set { _clastname = value; }
        }

        /// <summary>
        /// Uniqueid
        /// </summary>
        public int uniqueid
        {
            get { return _uniqueid; }
            set { _uniqueid = value; }
        }

        /// <summary>
        /// views
        /// </summary>
        public int views
        {
            get { return _views; }
            set { _views = value; }
        }

        /// <summary>
        /// mediatype
        /// </summary>
        public string mediatype
        {
            get { return _mediatype; }
            set { _mediatype = value; }
        }


        /// <summary>
        /// userid
        /// </summary>
        public int userid
        {
            get { return _userid; }
            set { _userid = value; }
        }


        /// <summary>
        /// orderid
        /// </summary>
        public int orderid
        {
            get { return _orderid; }
            set { _orderid = value; }
        }


        /// <summary>
        /// firstname
        /// </summary>
        public string firstname
        {
            get { return _firstname; }
            set { _firstname = value; }
        }


        /// <summary>
        /// lastname
        /// </summary>
        public string lastname
        {
            get { return _lastname; }
            set { _lastname = value; }
        }

        /// <summary>
        /// orderitemid
        /// </summary>
        public int orderitemid
        {
            get { return _orderitemid; }
            set { _orderitemid = value; }
        }

        /// <summary>
        /// address1
        /// </summary>
        public string address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        /// <summary>
        /// city
        /// </summary>
        public string city
        {
            get { return _city; }
            set { _city = value; }
        }

        /// <summary>
        /// state
        /// </summary>
        public string state
        {
            get { return _state; }
            set { _state = value; }
        }

        /// <summary>
        /// zip
        /// </summary>
        public string zip
        {
            get { return _zip; }
            set { _zip = value; }
        }

        /// <summary>
        /// email
        /// </summary>
        public string email
        {
            get { return _email; }
            set { _email = value; }
        }

        /// <summary>
        /// media_type
        /// </summary>
        public string media_type
        {
            get { return _media_type; }
            set { _media_type = value; }
        }

        /// <summary>
        /// comment
        /// </summary>
        public string comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        /// <summary>
        /// cost
        /// </summary>
        public decimal cost
        {
            get { return _cost; }
            set { _cost = value; }
        }

        /// <summary>
        /// discountid
        /// </summary>
        public int discountid
        {
            get { return _discountid; }
            set { _discountid = value; }
        }

        /// <summary>
        /// optin-ind
        /// </summary>
        public bool optin_ind
        {
            get { return _optin_ind; }
            set { _optin_ind = value; }
        }

        /// <summary>
        /// orderdate
        /// </summary>
        public string orderdate
        {
            get { return _orderdate; }
            set { _orderdate = value; }
        }


        /// <summary>
        /// quantity
        /// </summary>
        public int quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        /// <summary>
        /// shipcost
        /// </summary>
        public decimal shipcost
        {
            get { return _shipcost; }
            set { _shipcost = value; }
        }

        /// <summary>
        /// ship_ind
        /// </summary>
        public bool ship_ind
        {
            get { return _ship_ind; }
            set { _ship_ind = value; }
        }

        /// <summary>
        /// verification
        /// </summary>
        public string verification
        {
            get { return _verification; }
            set { _verification = value; }
        }

        /// <summary>
        /// totalcost
        /// </summary>
        public decimal totalcost
        {
            get { return _totalcost; }
            set { _totalcost = value; }
        }

        /// <summary>
        /// Tax
        /// </summary>
        public decimal tax
        {
            get { return _tax; }
            set { _tax = value; }
        }

        /// <summary>
        /// subtotal
        /// </summary>
        public decimal subtotal
        {
            get { return _subtotal; }
            set { _subtotal = value; }
        }

        public decimal ShipSum
        {
            get { return _ShipSum; }
            set { _ShipSum = value; }
        }
        public decimal TaxSum
        {
            get { return _taxSum; }
            set { _taxSum = value; }
        }

        public decimal SumTotal
        {
            get { return _sumTotal; }
            set { _sumTotal = value; }
        }
        public decimal Gtotal
        {
            get { return _Gtotal; }
            set { _Gtotal = value; }
        }

        public int FacilityId
        {
            get { return _facilityid; }
            set { _facilityid = value; }
        }

        #endregion
    }

}