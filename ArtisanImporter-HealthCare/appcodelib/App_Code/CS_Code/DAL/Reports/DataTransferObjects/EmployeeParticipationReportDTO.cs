﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class EmployeeParticipationReportDTO
    {
        #region Private Variables   
            private string _orgid;
            private string _facIds;
            private int _activeEmps;
            private int _inactiveEmps;
            private int _totalEmps;
            private int _loginEmps;
            private int _viewEmps;
            private int _PersentViewEmps;         
            private int _completeEmps;
            private int _PersentParticipation;
            private int _totalCredits;
            private decimal _avgParticipant;
            private decimal _avgCompletions;
            private string _organizationName;
            private string _facilityName;

        #endregion



        #region Public Properties


            /// <summary>
            /// Org Id
            /// </summary>
            public string OrgID
            {
                get { return _orgid; }
                set { _orgid = value; }
            }
            /// <summary>
            /// Facility Id
            /// </summary>
            public  string  FacIds
            {
                get { return _facIds; }
                set { _facIds = value; }
            }

            /// <summary>
            /// Active Employees Count
            /// </summary>
            public int ActiveEmps
            {
                get { return _activeEmps; }
                set { _activeEmps = value; }
            }

            /// <summary>
            /// Inactive Employee Count
            /// </summary>
            public int InactiveEmps
            {
                get { return _inactiveEmps; }
                set { _inactiveEmps = value; }
            }

            /// <summary>
            /// Total Employees (Active Employees Count + Inactive Employee Count)
            /// </summary>
            public int TotalEmps
            {
                get { return _totalEmps; }
                set { _totalEmps = value; }
            }

            /// <summary>
            /// Employee Count who log in
            /// </summary>
            public int LoginEmps
            {
                get { return _loginEmps; }
                set { _loginEmps = value; }
            }

            /// <summary>
            /// Employee Count Who view course
            /// </summary>
            public int ViewEmps
            {
                get { return _viewEmps; }
                set { _viewEmps = value; }
            }
       
        /// <summary>
            /// Percent of Participation Viewed a Course(Employees who have Viewed a Course * 100 / Active Employees )
            /// </summary>
            public int PersentViewEmps
            {
                get { return _PersentViewEmps; }
                set { _PersentViewEmps = value; }
            }
        
            /// <summary>
            /// Employee Count Who complete course
            /// </summary>
            public int CompleteEmps
            {
                get { return _completeEmps; }
                set { _completeEmps = value; }
            }
             // <summary>
            /// Percent of Participation Completed a Course (Employees who have Completed a Course * 100 / Active Employees)
            /// </summary>
            public int PersentParticipation
            {
                get { return _PersentParticipation; }
                set { _PersentParticipation = value; }
            }
        
            /// <summary>
            /// Total Credit Earned
            /// </summary>
            public int TotalCredits
            {
                get { return _totalCredits; }
                set { _totalCredits = value; }
            }

            /// <summary>
            /// Average Credits per Participant(Total Credits Earned / Employees who have Viewed a Course)
            /// </summary>
            public decimal AvgParticipant
            {
                get { return _avgParticipant; }
                set { _avgParticipant = value; }
            }


            /// <summary>
            /// Average Credits per Participant with Completions (Total Credits Earned / Employees who have Completed a Course)
            /// </summary>
            public decimal AvgCompletions
            {
                get { return _avgCompletions; }
                set { _avgCompletions = value; }
            }      
        
             /// <summary>
            /// Average Credits per Participant with Completions (Total Credits Earned / Employees who have Completed a Course)
            /// </summary>
            public string  OrganizationName
            {
                get { return _organizationName; }
                set { _organizationName = value; }
            }

            public string FacilityName
            {
                get { return _facilityName; }
                set { _facilityName = value; }
            } 
        #endregion
    }
}
