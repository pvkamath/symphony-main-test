﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for GnrlSummaryUtilizationByPeriodDTO
/// </summary>
namespace PearlsReview.DAL
{
    [Serializable()]
    public class GnrlSummaryUtilizationByPeriodDTO
    {

        #region Private Variables
        private string _orgIds;
        private string _facIds;
        private string _facname;
        private string _deptIds;
        private string _deptname;
        private string _deptSelect;
        private string _period_id;
        private string _hperiod;
        private string _viewDateStart;
        private string _viewDateEnd;
        private string _orderBy;
        private string _tperiod;
        private double _tuser;
        private double _tcourse;
        private double _thour;
        private double _avguser;
        private double _avghour;
        private string _primaryDiscipline;
        #endregion


        #region Public Properties
        /// <summary>
        /// Org Id
        /// </summary>

        public string OrgIds
        {
            get { return _orgIds; }
            set { _orgIds = value; }
        }

        /// <summary>
        /// Facility Id
        /// </summary>
        public string FacIds
        {
            get { return _facIds; }
            set { _facIds = value; }
        }

        /// <summary>
        /// Facility Name
        /// </summary>
        public string FacName
        {
            get { return _facname; }
            set { _facname = value; }
        }

        /// <summary>
        /// Department IDs
        /// </summary>
        public string DeptIds
        {
            get { return _deptIds; }
            set { _deptIds = value; }
        }

        /// <summary>
        /// Department Name
        /// </summary>
        public string DeptName
        {
            get { return _deptname; }
            set { _deptname = value; }
        }
        /// <summary>
        /// Department Select
        /// </summary>
        public string DeptSelect
        {
            get { return _deptSelect; }
            set { _deptSelect = value; }
        }

        /// <summary>
        /// Sorting
        /// </summary>
        public string PeriodIds
        {
            get { return _period_id; }
            set { _period_id = value; }
        }
        /// <summary>
        /// Sorting
        /// </summary>
        public string Hperiod
        {
            get { return _hperiod ; }
            set { _hperiod = value; }
        }
        /// <summary>
        /// ViewDate Start
        /// </summary>
        public string ViewDateStart
        {
            get { return _viewDateStart; }
            set { _viewDateStart = value; }
        }

        /// <summary>
        /// View Date End
        /// </summary>
        public string ViewDateEnd
        {
            get { return _viewDateEnd; }
            set { _viewDateEnd = value; }
        }
  
        /// <summary>
        /// Order By
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }

        /// <summary>
        /// Period Type
        /// </summary>
        public string Period
        {
            get { return _tperiod; }
            set { _tperiod = value; }
        }
        /// <summary>
        /// Users Completed
        /// </summary>
        public double tuser
        {
            get { return _tuser; }
            set { _tuser = value; }
        }

        /// <summary>
        /// Course Completed
        /// </summary>
        public double tcourse
        {
            get { return _tcourse; }
            set { _tcourse = value; }
        }


        /// <summary>
        /// Contact Hours Completed
        /// </summary>
        public double thour
        {
            get { return _thour; }
            set { _thour = value; }
        }

        /// <summary>
        /// Avg Users Completed
        /// </summary>
        public double AvgUser
        {
            get { return _avguser; }
            set { _avguser = value; }
        }

        /// <summary>
        /// Avg Contact Hours Completed
        /// </summary>
        public double AvgHour
        {
            get { return _avghour; }
            set { _avghour = value; }
        }
        /// <summary>
        /// Primary Discipline
        /// </summary>
        public string PrimaryDiscipline
        {
            get { return _primaryDiscipline; }
            set { _primaryDiscipline = value; }
        }
        #endregion
    }
}
