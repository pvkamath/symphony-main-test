﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for EvaluationCommentsDTO
/// </summary>
namespace PearlsReview.DAL
{
    [Serializable()]
    public class EvaluationCommentsDTO
    {
        public EvaluationCommentsDTO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region Private Variables
        private string _surveydate_start;
        private string _surveydate_end;
        private string _courseIds;
        private int _courseId;
        private string _orderBy;
        private string _topicId;
        private string _topicName;
        private string _surveyDate;
        private string _course_number;
        private string _QuestionComment4;
        private string _QuestionComment5;
        private string _QuestionComment6;
        private string _QuestionComment7;
        private string _QuestionComment8;
        private string _QuestionComment14;
        #endregion

        #region Public Properties


        /// <summary>
        /// TopicID
        /// </summary>
        public int CourseID
        {
            get { return _courseId; }
            set { _courseId = value; }
        }

        /// <summary>
        /// Course Ids
        /// </summary>
        public string CourseIds
        {
            get { return _courseIds; }
            set { _courseIds = value; }
        }

        /// <summary>
        /// SurveyStartDate
        /// </summary>
        public string SurveyStartDate
        {
            get { return _surveydate_start; }
            set { _surveydate_start = value; }
        }

        /// <summary>
        /// SurveyEndDate
        /// </summary>
        public string SurveyEndDate
        {
            get { return _surveydate_end; }
            set { _surveydate_end = value; }
        }

        /// <summary>
        /// Order By 
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }


        /// <summary>
        /// Topic Id
        /// </summary>
        public string TopicId
        {
            get { return _topicId; }
            set { _topicId = value; }
        }

        /// <summary>
        /// Topic Name
        /// </summary>
        public string TopicName
        {
            get { return _topicName; }
            set { _topicName = value; }
        }

        /// <summary>
        /// Survey Date
        /// </summary>
        public string SurveyDate
        {
            get { return _surveyDate; }
            set { _surveyDate = value; }
        }

        /// <summary>
        /// Course Number
        /// </summary>
        public string Course_Number
        {
            get { return _course_number; }
            set { _course_number = value; }
        }


        /// <summary>
        /// How many minutes did it take to read and complete this program (including time spent on clinical vignette, if applicable)? (responses given in hours:minutes)
        /// </summary>
        public string QuestionComment4
        {
            get { return _QuestionComment4; }
            set { _QuestionComment4 = value; }
        }

        /// <summary>
        /// How will this information affect your professional practice & its quality?
        /// </summary>
        public string QuestionComment5
        {
            get { return _QuestionComment5; }
            set { _QuestionComment5 = value; }
        }

        /// <summary>
        /// Suggestions for future improvement? Future Topics?
        /// </summary>
        public string QuestionComment6
        {
            get { return _QuestionComment6; }
            set { _QuestionComment6 = value; }
        }

        /// <summary>
        /// Suggestions for speaker improvement (if applicable)?
        /// </summary>
        public string QuestionComment7
        {
            get { return _QuestionComment7; }
            set { _QuestionComment7 = value; }
        }

        /// <summary>
        /// Other comments?
        /// </summary>
        public string QuestionComment8
        {
            get { return _QuestionComment8; }
            set { _QuestionComment8 = value; }
        }

        /// <summary>
        /// The program was presented in a fair and unbiased manner.
        /// </summary>
        public string QuestionComment14
        {
            get { return _QuestionComment14; }
            set { _QuestionComment14 = value; }
        }

        #endregion
    }
}