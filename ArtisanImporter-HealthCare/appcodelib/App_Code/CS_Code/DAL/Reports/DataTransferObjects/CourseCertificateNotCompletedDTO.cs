﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

[Serializable()]
public class CourseCertificateNotCompletedDTO
{    
    #region Private Variables

    private string _facIds;
    private string _deptIds;
    private string _categoryId;
    private string _course;
    private string _courseId;
    private string _userId;
    private string _userFirstName;
    private string _userLastName;    
    private string _deptSelect;    
    private string _viewFromDate;
    private string _viewToDate;
    
    private string _categoryName;
    private string _courseName;
    private string _courseNumber;
    private string _cuserName;
    private string _cfirstName;
    private string _clastName;
    private string _orderBy;    

    #endregion

    #region Public Properties

    /// <summary>
    /// Facility Ids
    /// </summary>
    public string FacIds
    {
        get { return _facIds; }
        set { _facIds = value; }
    }

    /// <summary>
    /// Department IDs
    /// </summary>
    public string DeptIds
    {
        get { return _deptIds; }
        set { _deptIds = value; }
    }

    /// <summary>
    /// Category Id
    /// </summary>
    public string CategoryId
    {
        get { return _categoryId; }
        set { _categoryId = value; }
    }


    /// <summary>
    /// Course
    /// </summary>
    public string Course
    {
        get { return _course; }
        set { _course = value; }
    }

    /// <summary>
    /// Course Id
    /// </summary>
    public string CourseId
    {
        get { return _courseId; }
        set { _courseId = value; }
    }
  

    /// <summary>
    /// User ID
    /// </summary>
    public string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }

    /// <summary>
    /// User First Name
    /// </summary>
    public string UserFirstName
    {
        get { return _userFirstName; }
        set { _userFirstName = value; }
    }

    /// <summary>
    /// User Lat Name
    /// </summary>
    public string UserLastName
    {
        get { return _userLastName; }
        set { _userLastName = value; }
    }

   
    /// <summary>
    /// Dept Select
    /// </summary>
    public string DeptSelect
    {
        get { return _deptSelect; }
        set { _deptSelect = value; }
    }

    

    /// <summary>
    /// View From Date
    /// </summary>
    public string ViewFromDate
    {
        get { return _viewFromDate; }
        set { _viewFromDate = value; }
    }

    /// <summary>
    /// View To Date
    /// </summary>
    public string ViewToDate
    {
        get { return _viewToDate; }
        set { _viewToDate = value; }
    }


    /// <summary>
    /// Category Name
    /// </summary>
    public string CategoryName
    {
        get { return _categoryName; }
        set { _categoryName = value; }
    }

    /// <summary>
    /// Course Name
    /// </summary>
    public string CourseName
    {
        get { return _courseName; }
        set { _courseName = value; }
    }

    /// <summary>
    /// Course Number
    /// </summary>
    public string CourseNumber
    {
        get { return _courseNumber; }
        set { _courseNumber = value; }
    }

    /// <summary>
    /// C User Name
    /// </summary>
    public string CuserName
    {
        get { return _cuserName; }
        set { _cuserName = value; }
    }

    /// <summary>
    ///C First Name
    /// </summary>
    public string CfirstName
    {
        get { return _cfirstName; }
        set { _cfirstName = value; }
    }

    /// <summary>
    /// C last Name
    /// </summary>
    public string ClastName
    {
        get { return _clastName; }
        set { _clastName = value; }
    }

    /// <summary>
    /// Order By
    /// </summary>
    public string OrderBy
    {
        get { return _orderBy; }
        set { _orderBy = value; }
    }

    #endregion
}
