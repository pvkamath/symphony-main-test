﻿using System;
using System.Text;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class RevenueSummaryDTO
    {
        #region Private Variables

        private int _orderDay;
        private int _orderMonth ;
        private int _orderYear;
        private string _orderYearMonth;
        private decimal _totalCatByMonth ;
        private string _orderDateStart;
        private string _orderDateEnd;
        private string _rType;

        #endregion

        #region Public Properties

        public int OrderDay
        {
            get { return _orderDay; }
            set { _orderDay = value; }
        }

        public int OrderMonth
        {
            get { return _orderMonth; }
            set { _orderMonth = value; }
        }

        public int OrderYear
        {
            get { return _orderYear; }
            set { _orderYear = value; }
        }

        public string OrderYearMonth
        {
            get { return _orderYearMonth; }
            set { _orderYearMonth = value; }
        }

        public decimal TotalCatByMonth
        {
            get { return _totalCatByMonth; }
            set { _totalCatByMonth = value; }
        }

        public string OrderDateStart
        {
            get { return _orderDateStart; }
            set { _orderDateStart = value; }
        }
        public string OrderDateEnd
        {
            get { return _orderDateEnd; }
            set { _orderDateEnd = value; }
        }
        public string RType
        {
            get { return _rType; }
            set { _rType = value; }
        }       

        #endregion
    }

}
