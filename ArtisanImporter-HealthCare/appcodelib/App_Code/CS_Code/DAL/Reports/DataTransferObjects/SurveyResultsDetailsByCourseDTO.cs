﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
public class SurveyResultsDetailsByCourseDTO
{
    #region Private Variables

        private string _facIds;
        private string _deptSelect;
        private string _courseId;
        private string _surveydateStart;
        private string _surveydateEnd;
       
        private string _deptName;
        private string _topicName;
        private string _qNumber;
        private string _qText;
        private string _tComment;
        private string _surveyDate;
        private string _orderBy;

    #endregion

    #region Public Properties

        /// <summary>
        /// Facility Id
        /// </summary>
        public string FacIds
        {
            get { return _facIds; }
            set { _facIds = value; }
        }

        /// <summary>
        /// Dept Select
        /// </summary>
        public string DeptSelect
        {
            get { return _deptSelect; }
            set { _deptSelect = value; }
        }

        /// <summary>
        /// Course Id
        /// </summary>
        public string CourseId
        {
            get { return _courseId; }
            set { _courseId = value; }
        }

        /// <summary>
        /// Survey date Start
        /// </summary>
        public string SurveydateStart
        {
            get { return _surveydateStart; }
            set { _surveydateStart = value; }
        }

        /// <summary>
        /// Survey date End
        /// </summary>
        public string SurveydateEnd
        {
            get { return _surveydateEnd; }
            set { _surveydateEnd = value; }
        }
        
        /// <summary>
        /// DeptName
        /// </summary>
        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }

        /// <summary>
        /// Topic Name
        /// </summary>
        public string TopicName
        {
            get { return _topicName; }
            set { _topicName = value; }
        }

        /// <summary>
        /// QNumber
        /// </summary>
        public string QNumber
        {
            get { return _qNumber; }
            set { _qNumber = value; }
        }

        /// <summary>
        /// QNumber
        /// </summary>
        public string QText
        {
            get { return _qText; }
            set { _qText = value; }
        }

        /// <summary>
        /// TComment
        /// </summary>
        public string TComment
        {
            get { return _tComment; }
            set { _tComment = value; }
        }

        /// <summary>
        /// Survey Date
        /// </summary>
        public string SurveyDate
        {
            get { return _surveyDate; }
            set { _surveyDate = value; }
        }

        /// <summary>
        /// Order By
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }
    #endregion
}
}

