﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.DAL
{
    [Serializable()]
    public class UserDemographicsDTO
    {
        #region Private Variables

            private string _facIds;
            private string _deptIds;
            private string _userFirstName;
            private string _userLastName;
            private string _userMiddleName;
            private string _userTitle;
            private string _userPosition;
            private string _deptSelect;
            private string _userState;
            private string _userSsn;
            private string _userBadge;
            private string _hiredateStart;
            private string _hiredateEnd;
            private string _termindateStart;
            private string _termindateEnd;
            private string _userCanlogin;  
            private string _userType;
            private string _userActive;

            private string _userFullName;            
            private string _positionName;
            private string _deptName;
            private string _cAddress1;
            private string _cAddress2;
            private string _cCity;
            private string _cZipCode;
            private string _cPhone;
            private string _cEmail;
            private string _cSocial;
            private string _workPhone;            
            private string _hireDate;
            private string _terminDate;
            private string _canLogin;
            private string _orderBy;
            private string _facName;
            private string _primaryDiscipline;

            private string _region;
            private string _employer;
            private string _facility;
            private string _regDate;
            private string _mobielPIN;
        #endregion

        #region Public Properties

            /// <summary>
            /// Region
            /// </summary>
            public string Region
            {
                get { return _region; }
                set { _region = value; }
            }

            /// <summary>
            /// MobilePIN
            /// </summary>
            public string MobilePIN
            {
                get { return _mobielPIN; }
                set { _mobielPIN = value; }
            }


            /// <summary>
            /// Employer
            /// </summary>
            public string Employer
            {
                get { return _employer; }
                set { _employer = value; }
            }

            /// <summary>
            /// Facility
            /// </summary>
            public string Facility
            {
                get { return _facility; }
                set { _facility = value; }
            }

            /// <summary>
            /// RegDate
            /// </summary>
            public string RegDate
            {
                get { return _regDate; }
                set { _regDate = value; }
            } 


            /// <summary>
            /// Facility Id
            /// </summary>
            public string FacIds
            {
                get { return _facIds; }
                set { _facIds = value; }
            }

            /// <summary>
            /// Department IDs
            /// </summary>
            public string DeptIds
            {
                get { return _deptIds; }
                set { _deptIds = value; }
            }

            /// <summary>
            /// User First Name
            /// </summary>
            public string UserFirstName
            {
                get { return _userFirstName; }
                set { _userFirstName = value; }
            }

            /// <summary>
            /// User Lat Name
            /// </summary>
            public string UserLastName
            {
                get { return _userLastName; }
                set { _userLastName = value; }
            }
            /// <summary>
            /// User Middle Name
            /// </summary>
            public string UserMiddleName
            {
                get { return _userMiddleName; }
                set { _userMiddleName = value; }
            }   
            /// <summary>
            /// User Title
            /// </summary>
            public string UserTitle
            {
                get { return _userTitle; }
                set { _userTitle = value; }
            }

            /// <summary>
            /// User Position
            /// </summary>
            public string UserPosition
            {
                get { return _userPosition; }
                set { _userPosition = value; }
            }

            /// <summary>
            /// Dept Select
            /// </summary>
            public string DeptSelect
            {
                get { return _deptSelect; }
                set { _deptSelect = value; }
            }

            /// <summary>
            /// User State
            /// </summary>
            public string UserState
            {
                get { return _userState; }
                set { _userState = value; }
            }

            /// <summary>
            /// User Ssn
            /// </summary>
            public string UserSsn
            {
                get { return _userSsn; }
                set { _userSsn = value; }
            }

            /// <summary>
            /// User Badge
            /// </summary>
            public string UserBadge
            {
                get { return _userBadge; }
                set { _userBadge = value; }
            }

            /// <summary>
            /// Hire date Start
            /// </summary>
            public string HiredateStart
            {
                get { return _hiredateStart; }
                set { _hiredateStart = value; }
            }

            /// <summary>
            /// Hire date End
            /// </summary>
            public string HiredateEnd
            {
                get { return _hiredateEnd; }
                set { _hiredateEnd = value; }
            }
          
            /// <summary>
            /// Termin date Start
            /// </summary>
            public string TermindateStart
            {
                get { return _termindateStart; }
                set { _termindateStart = value; }
            }

            /// <summary>
            /// Termin date End
            /// </summary>
            public string TermindateEnd
            {
                get { return _termindateEnd; }
                set { _termindateEnd = value; }
            }

            /// <summary>
            /// User Can login
            /// </summary>
            public string UserCanlogin
            {
                get { return _userCanlogin; }
                set { _userCanlogin = value; }
            }

            /// <summary>
            /// User Type
            /// </summary>
            public string UserType
            {
                get { return _userType; }
                set { _userType = value; }
            }

            /// <summary>
            /// User Active
            /// </summary>
            public string UserActive
            {
                get { return _userActive; }
                set { _userActive = value; }
            }  

            /// <summary>
            /// User Full Name
            /// </summary>
            public string UserFullName
            {
                get { return _userFullName; }
                set { _userFullName = value; }
            }

            /// <summary>
            /// Position Name
            /// </summary>
            public string PositionName
            {
                get { return _positionName; }
                set { _positionName = value; }
            }

            /// <summary>
            /// Dept Name
            /// </summary>
            public string DeptName
            {
                get { return _deptName; }
                set { _deptName = value; }
            }
            
            /// <summary>
            /// CAddress1
            /// </summary>
            public string CAddress1
            {
                get { return _cAddress1; }
                set { _cAddress1 = value; }
            }

            /// <summary>
            /// CAddress2
            /// </summary>
            public string CAddress2
            {
                get { return _cAddress2; }
                set { _cAddress2 = value; }
            }

            /// <summary>
            /// CCity
            /// </summary>
            public string CCity
            {
                get { return _cCity; }
                set { _cCity = value; }
            }
        
            /// <summary>
            /// C Zip Code
            /// </summary>
            public string CZipCode
            {
                get { return _cZipCode; }
                set { _cZipCode = value; }
            }

            /// <summary>
            /// CPhone
            /// </summary>
            public string CPhone
            {
                get { return _cPhone; }
                set { _cPhone = value; }
            }

            /// <summary>
            /// CEmail
            /// </summary>
            public string CEmail
            {
                get { return _cEmail; }
                set { _cEmail = value; }
            }

            /// <summary>
            /// C Social
            /// </summary>
            public string CSocial
            {
                get { return _cSocial; }
                set { _cSocial = value; }
            }            

            /// <summary>
            /// Hire Date
            /// </summary>
            public string HireDate
            {
                get { return _hireDate; }
                set { _hireDate = value; }
            }

            /// <summary>
            /// Termin Date
            /// </summary>
            public string TerminDate
            {
                get { return _terminDate; }
                set { _terminDate = value; }
            }

            /// <summary>
            /// Can Login
            /// </summary>
            public string CanLogin
            {
                get { return _canLogin; }
                set { _canLogin = value; }
            }

            /// <summary>
            /// Order By
            /// </summary>
            public string OrderBy
            {
                get { return _orderBy; }
                set { _orderBy = value; }
            }

        ///<summary>
        ///Fac Name
        ///</summary>
        ///
            public string FacName
            {
                get { return _facName; }
                set { _facName = value; }
            }
            ///<summary>
            ///Home Phone
            ///</summary>
            ///
            public string WorkPhone
            {
                get { return _workPhone; }
                set { _workPhone = value; }
            }
            ///<summary>
            ///Primary discipline
            ///</summary>
            ///
            public string PrimaryDiscipline
            {
                get { return _primaryDiscipline; }
                set { _primaryDiscipline = value; }
            }
        #endregion
    }
}


