﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace PearlsReview.DAL
{
    [Serializable()]

    public class CurrentCoursesDTO
    {
        #region Private Variables
        private string _orgIds;
        private string _facIds;
        private string _facName;
        private string _deptIds;
        private string _typeId;
        private string _orderBy;
        private string _topicid;
        private string _course_number;
        private string _topicname;
        private string _coursetype;
        private double _totalhrs;
        private double _hours;

        #endregion

        #region Public Properties
        /// <summary>
        /// Org Id
        /// </summary>
        public string OrgIds
        {
            get { return _orgIds; }
            set { _orgIds = value; }
        }

        /// <summary>
        /// Facility Id
        /// </summary>
        public string FacIds
        {
            get { return _facIds; }
            set { _facIds = value; }
        }
       
        /// <summary>
        /// Facility Name
        /// </summary>
        public string FacName
        {
            get { return _facName; }
            set { _facName = value; }
        }

        /// <summary>
        /// Type ID
        /// </summary>
        public string TypeID
        {
            get { return _typeId; }
            set { _typeId = value; }
        }

        /// <summary>
        /// Order By
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }

        /// <summary>
        /// Department IDs
        /// </summary>
        public string DeptIds
        {
            get { return _deptIds; }
            set { _deptIds = value; }
        }

        /// <summary>
        /// Course Number
        /// </summary>
        public string CourseNumber
        {
            get { return _course_number; }
            set { _course_number = value; }
        }

        /// <summary>
        /// Course Type
        /// </summary>
        public string CourseType
        {
            get { return _coursetype; }
            set { _coursetype = value; }
        }

        /// <summary>
        /// Topic Name
        /// </summary>
        public string TopicName
        {
            get { return _topicname; }
            set { _topicname = value; }
        }

        /// <summary>
        /// Hours
        /// </summary>
        public double Hours
        {
            get { return _hours; }
            set { _hours = value; }
        }

        /// <summary>
        /// TotalHours
        /// </summary>
        public double TotalHrs
        {
            get { return _totalhrs; }
            set { _totalhrs= value; }
        }

        /// <summary>
        /// Topic ID
        /// </summary>
        public string TopicID
        {
            get { return _topicid; }
            set { _topicid = value; }
        }

        #endregion
    }
}
