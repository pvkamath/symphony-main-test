﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for DetailedCourseActivityDTO
/// </summary>
namespace PearlsReview.DAL
{
    [Serializable()]
    public class DetailedCourseActivityDTO
    {
        #region Private Variables

        private string _facIds;
        private string _deptIds;
        //private string _course;
        private string _courseId;
        private double _creitHrs;
        private string _testPassFromDate;
        private string _testPassToDate;       
        private string _topicName;
        private string _deptName;
        private string _score;
        private string _testPassDate;
        private string _orderBy;
        private string _userFullName;
        private string _CourseType;
        private string _facilityName;
        private string _orgId;
        private string _primaryDiscipline;
        private string _topic_hourshort;


        #endregion
        public DetailedCourseActivityDTO()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        #region Public Properties

        public string Topic_hourshort
        {
            get { return _topic_hourshort; }
            set { _topic_hourshort = value; }
        }


        /// <summary>
        /// Facility Ids
        /// </summary>
        public string FacIds
        {
            get { return _facIds; }
            set { _facIds = value; }
        }

        /// <summary>
        /// Course Type
        /// </summary>
        public string CourseType
        {
            get { return _CourseType; }
            set { _CourseType = value; }
        }

        /// <summary>
        /// Facility Name
        /// </summary>
        public string FacilityName
        {
            get { return _facilityName; }
            set { _facilityName = value; }
        }

        /// <summary>
        /// Org IDs
        /// </summary>
        public string OrgId
        {
            get { return _orgId; }
            set { _orgId = value; }
        }
              
        
        /// <summary>
        /// Department IDs
        /// </summary>
        public string DeptIds
        {
            get { return _deptIds; }
            set { _deptIds = value; }
        }
                
       
        /// <summary>
        /// Course Id
        /// </summary>
        public string CourseId
        {
            get { return _courseId; }
            set { _courseId = value; }
        }            
        
        /// <summary>
        /// Creit Hrs
        /// </summary>
        public double CreitHrs
        {
            get { return _creitHrs; }
            set { _creitHrs = value; }
        }

       
        /// <summary>
        /// Test Pass From Date
        /// </summary>
        public string TestPassFromDate
        {
            get { return _testPassFromDate; }
            set { _testPassFromDate = value; }
        }

        /// <summary>
        /// Test Pass ToDate
        /// </summary>
        public string TestPassToDate
        {
            get { return _testPassToDate; }
            set { _testPassToDate = value; }
        }
               
       
        /// <summary>
        /// Topic Name
        /// </summary>
        public string TopicName
        {
            get { return _topicName; }
            set { _topicName = value; }
        }

             /// <summary>
        /// Department Name
        /// </summary>
        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }

        /// <summary>
        /// Score
        /// </summary>
        public string Score
        {
            get { return _score; }
            set { _score = value; }
        }

        
        /// <summary>
        /// Test Pass Date
        /// </summary>
        public string TestPassDate
        {
            get { return _testPassDate; }
            set { _testPassDate = value; }
        }

        
        /// <summary>
        /// Order By
        /// </summary>
        public string OrderBy
        {
            get { return _orderBy; }
            set { _orderBy = value; }
        }

        /// <summary>
        /// User Full Name
        /// </summary>
        public string UserFullName
        {
            get { return _userFullName; }
            set { _userFullName = value; }
        }

        /// <summary>
        /// Primary Discipline
        /// </summary>
        public string PrimaryDiscipline
        {
            get { return _primaryDiscipline; }
            set { _primaryDiscipline = value; }
        }

        #endregion
    }
}
