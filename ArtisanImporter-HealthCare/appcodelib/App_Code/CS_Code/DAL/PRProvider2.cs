﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for PRProvider
    /// </summary>
    public abstract partial class PRProvider : DataAccess
    {

//        #region Methods that work with Discount       

//        /////////////////////////////////////////////////////////
//        // methods that work with Discount
//        //Bhaskar N
//        public abstract List<DiscountInfo> GetDiscounts(string cSortExpression);
//        public abstract DiscountInfo GetDiscountByID(int DiscountId);
//        public abstract System.Data.DataSet GetDiscountByTopicID(string TopicID, int UrlIdentity, string UserName);
//        public abstract int InsertDiscount(DiscountInfo Discount);
//        public abstract bool UpdateDiscount(DiscountInfo Discount);

//        public abstract bool DeleteDiscount(int DiscountId);
//        public abstract bool UpdateDiscountByImpression(int Discountid, int Impression);
//        public abstract bool UpdateDiscountByClickThrough(int Discountid, int ClickThrough);
//        public abstract int GetDiscountIdByTopicId(int TopicId);


//        /// <summary>
//        /// Returns a new DiscountInfo instance filled with the DataReader's current record data
//        /// </summary>
//        protected virtual DiscountInfo GetDiscountFromReader(IDataReader reader)
//        {
//            return GetDiscountFromReader(reader, true);
//        }
//        protected virtual DiscountInfo GetDiscountFromReader(IDataReader reader, bool readMemos)
//        {
//            DiscountInfo Discount = new DiscountInfo(
//                  (int)reader["discountid"],
//                  (int)reader["topicid"],
//                  reader["course_number"].ToString(),
//                  reader["tagline"].ToString(),
//                  (DateTime)reader["startdate"],
//                  (DateTime)reader["enddate"],
//                  (decimal)reader["discount_price"],
//                  reader["discount_type"].ToString(),
//                  reader["sponsor_text"].ToString(),
//                  reader["img_file"].ToString(),
//                  reader["discount_url"].ToString(),
//                  (bool)reader["survey_ind"],
//                  (int)reader["impression"],
//                  (int)reader["clickthrough"],
//                  reader["username"].ToString(),
//                  reader["userpassword"].ToString(),
//                  (bool)reader["section_ind"],
//                  (decimal)reader["offline_price"],
//                  (bool)reader["virtualurl_ind"]);
//            return Discount;
//        }



//        /// <summary>
//        /// Returns a collection of DiscountInfo objects with the data read from the input DataReader
//        /// </summary>
//        protected virtual List<DiscountInfo> GetDiscountCollectionFromReader(IDataReader reader)
//        {
//            return GetDiscountCollectionFromReader(reader, true);
//        }
//        protected virtual List<DiscountInfo> GetDiscountCollectionFromReader(IDataReader reader, bool readMemos)
//        {
//            List<DiscountInfo> Discounts = new List<DiscountInfo>();
//            while (reader.Read())
//                Discounts.Add(GetDiscountFromReader(reader, readMemos));
//            return Discounts;
//        }

//        #endregion

//        #region Methods that work with Position    
  
//        /////////////////////////////////////////////////////////
//        // methods that work with Position
//        public abstract int GetPositionCount(int facid);
//        public abstract List<PositionInfo> GetPositions(string fac_id, string cSortExpression);
//        public abstract PositionInfo GetPositionByID(int PositionID);
//        public abstract bool DeletePosition(int PositionID);
//        public abstract bool UpdatePosition(PositionInfo Position);
//        public abstract int InsertPosition(PositionInfo Position);

//        /// <summary>
//        /// Returns a new PositionInfo instance filled with the DataReader's current record data
//        /// </summary>
//        protected virtual PositionInfo GetPositionFromReader(IDataReader reader)
//        {
//            return GetPositionFromReader(reader, true);
//        }
//        protected virtual PositionInfo GetPositionFromReader(IDataReader reader, bool readMemos)
//        {
//            PositionInfo position = new PositionInfo(
//              (int)reader["position_id"],
//              (int)reader["facilityid"],
//              reader["position_name"].ToString(),
//              (bool)reader["position_active_ind"]);

//            return position;
//        }

//        /// <summary>
//        /// Returns a collection of PositionInfo objects with the data read from the input DataReader
//        /// </summary>
//        protected virtual List<PositionInfo> GetPositionCollectionFromReader(IDataReader reader)
//        {
//            return GetPositionCollectionFromReader(reader, true);
//        }
//        protected virtual List<PositionInfo> GetPositionCollectionFromReader(IDataReader reader, bool readMemos)
//        {
//            List<PositionInfo> Positions = new List<PositionInfo>();
//            while (reader.Read())
//                Positions.Add(GetPositionFromReader(reader, readMemos));
//            return Positions;
//        }
//        #endregion

//        #region Methods that work with ParentOrganization

//        /////////////////////////////////////////////////////////
//        // methods that work with ParentOrganization
//        // ******** CEDIRECT VERSION ************
//        public abstract int GetParentOrganizationCount();
//        public abstract List<ParentOrganizationInfo> GetParentOrganizations(string cSortExpression);
//        public abstract ParentOrganizationInfo GetParentOrganizationByID(int Parent_id);
//        public abstract int GetUserAccountCountByParentFacilityID(int FacilityID);
//        public abstract bool DeleteParentOrganization(int Parent_id);
//        public abstract bool UpdateParentOrganization(ParentOrganizationInfo ParentOrganization);
//        public abstract int InsertParentOrganization(ParentOrganizationInfo ParentOrganization);

//        /// <summary>
//        /// Returns a new ParentOrganizationInfo instance filled with the DataReader's current record data
//        /// </summary>
//        protected virtual ParentOrganizationInfo GetParentOrganizationFromReader(IDataReader reader)
//        {
//            return GetParentOrganizationFromReader(reader, true);
//        }
//        protected virtual ParentOrganizationInfo GetParentOrganizationFromReader(IDataReader reader, bool readMemos)
//        {
//            ParentOrganizationInfo ParentOrganization = new ParentOrganizationInfo(
//              (int)reader["Parent_id"],
//              reader["Parent_name"].ToString(),
//              reader["address1"].ToString(),
//              reader["address2"].ToString(),
//              reader["city"].ToString(),
//              reader["state"].ToString(),
//              reader["zipcode"].ToString(),
//              reader["phone"].ToString(),
//              reader["contact_name"].ToString(),
//              reader["contact_phone"].ToString(),
//              reader["contact_ext"].ToString(),
//              reader["contact_email"].ToString(),
//              (bool)reader["active"],
//              reader["Comment"].ToString(),
//             (int)reader["MaxUsers"],
//            (DateTime)reader["Started"],
//            (DateTime)reader["Expires"]);

//            return ParentOrganization;
//        }

//        /// <summary>
//        /// Returns a collection of ParentOrganizationInfo objects with the data read from the input DataReader
//        /// </summary>
//        protected virtual List<ParentOrganizationInfo> GetParentOrganizationCollectionFromReader(IDataReader reader)
//        {
//            return GetParentOrganizationCollectionFromReader(reader, true);
//        }
//        protected virtual List<ParentOrganizationInfo> GetParentOrganizationCollectionFromReader(IDataReader reader, bool readMemos)
//        {
//            List<ParentOrganizationInfo> ParentOrganizations = new List<ParentOrganizationInfo>();
//            while (reader.Read())
//                ParentOrganizations.Add(GetParentOrganizationFromReader(reader, readMemos));
//            return ParentOrganizations;
//        }
//        #endregion

//        #region Methods that work with SponsorSurveyQue

//        /////////////////////////////////////////////////////////
//        // methods that work with SponsorSurveyQue
//        //Bhaskar N
//        public abstract List<SponsorSurveyQueInfo> GetSponsorSurveyQues(string cSortExpression);
//        public abstract List<SponsorSurveyQueInfo> GetSponsorSurveyQuesByDiscountId(string cSortExpression, int discountid);
//        public abstract SponsorSurveyQueInfo GetSponsorSurveyQueByID(int questionid);        
//        public abstract int InsertSponsorSurveyQue(SponsorSurveyQueInfo SponsorSurveyQue);
//        public abstract bool UpdateSponsorSurveyQue(SponsorSurveyQueInfo SponsorSurveyQue);
//        public abstract bool DeleteSponsorSurveyQue(int questionid);
        

//        /// <summary>
//        /// Returns a new SponsorSurveyQueInfo instance filled with the DataReader's current record data
//        /// </summary>
//        protected virtual SponsorSurveyQueInfo GetSponsorSurveyQueFromReader(IDataReader reader)
//        {
//            return GetSponsorSurveyQueFromReader(reader, true);
//        }
//        protected virtual SponsorSurveyQueInfo GetSponsorSurveyQueFromReader(IDataReader reader, bool readMemos)
//        {
//            SponsorSurveyQueInfo SponsorSurveyQue = new SponsorSurveyQueInfo(
//                  (int)reader["questionid"],
//                  reader["survey_text"].ToString(),
//                  (bool)reader["optional_ind"]);
//            return SponsorSurveyQue;
//        }


//        /// <summary>
//        /// Returns a collection of SponsorSurveyQueInfo objects with the data read from the input DataReader
//        /// </summary>
//        protected virtual List<SponsorSurveyQueInfo> GetSponsorSurveyQueCollectionFromReader(IDataReader reader)
//        {
//            return GetSponsorSurveyQueCollectionFromReader(reader, true);
//        }
//        protected virtual List<SponsorSurveyQueInfo> GetSponsorSurveyQueCollectionFromReader(IDataReader reader, bool readMemos)
//        {
//            List<SponsorSurveyQueInfo> SponsorSurveyQues = new List<SponsorSurveyQueInfo>();
//            while (reader.Read())
//                SponsorSurveyQues.Add(GetSponsorSurveyQueFromReader(reader, readMemos));
//            return SponsorSurveyQues;
//        }

//        #endregion

//        #region Methods that work with GetSponsorSurveyAns

//        /////////////////////////////////////////////////////////
//        // methods that work with SponsorSurveyAns
//        //Bhaskar N
//        public abstract List<SponsorSurveyAnsInfo> GetSponsorSurveyAns(string cSortExpression);
//        public abstract List<SponsorSurveyAnsInfo> GetSponsorSurveyAnsByQuestionId(int questionid);
//        public abstract SponsorSurveyAnsInfo GetSponsorSurveyAnsByID(int questionid);
//        public abstract int InsertSponsorSurveyAns(SponsorSurveyAnsInfo SponsorSurveyAns);
//        public abstract bool UpdateSponsorSurveyAns(SponsorSurveyAnsInfo SponsorSurveyAns);

//        public abstract bool DeleteSponsorSurveyAns(int questionid);


//        /// <summary>
//        /// Returns a new SponsorSurveyAnsInfo instance filled with the DataReader's current record data
//        /// </summary>
//        protected virtual SponsorSurveyAnsInfo GetSponsorSurveyAnsFromReader(IDataReader reader)
//        {
//            return GetSponsorSurveyAnsFromReader(reader, true);
//        }
//        protected virtual SponsorSurveyAnsInfo GetSponsorSurveyAnsFromReader(IDataReader reader, bool readMemos)
//        {
//            SponsorSurveyAnsInfo SponsorSurveyAns = new SponsorSurveyAnsInfo(
//                  (int)reader["answerid"],
//                  (int)reader["questionid"],
//                  reader["answer_text"].ToString(),
//                  (bool)reader["share_ind"]);
//            return SponsorSurveyAns;
//        }


//        /// <summary>
//        /// Returns a collection of SponsorSurveyAnsInfo objects with the data read from the input DataReader
//        /// </summary>
//        protected virtual List<SponsorSurveyAnsInfo> GetSponsorSurveyAnsCollectionFromReader(IDataReader reader)
//        {
//            return GetSponsorSurveyAnsCollectionFromReader(reader, true);
//        }
//        protected virtual List<SponsorSurveyAnsInfo> GetSponsorSurveyAnsCollectionFromReader(IDataReader reader, bool readMemos)
//        {
//            List<SponsorSurveyAnsInfo> SponsorSurveyAns = new List<SponsorSurveyAnsInfo>();
//            while (reader.Read())
//                SponsorSurveyAns.Add(GetSponsorSurveyAnsFromReader(reader, readMemos));
//            return SponsorSurveyAns;
//        }

//        #endregion

//        #region Methods that work with SponsorSurveys

//        /////////////////////////////////////////////////////////
//        // methods that work with SponsorSurveys
//        //Bhaskar N
//        public abstract List<SponsorSurveysInfo> GetSponsorSurveys(string cSortExpression);
//        public abstract List<SponsorSurveysInfo> GetSponsorSurveysByDiscountIdAndQuestionId(int discountid, int questionid);
//        public abstract List<SponsorSurveysInfo> GetSponsorSurveysByDiscountId(int discountid);
//        public abstract bool InsertSponsorSurveys(SponsorSurveysInfo SponsorSurveys);
//        public abstract bool DeleteSponsorSurveysByDiscountidAndQuestionid(int discountid, int questionid);


//        /// <summary>
//        /// Returns a new SponsorSurveysInfo instance filled with the DataReader's current record data
//        /// </summary>
//        protected virtual SponsorSurveysInfo GetSponsorSurveysFromReader(IDataReader reader)
//        {
//            return GetSponsorSurveysFromReader(reader, true);
//        }
//        protected virtual SponsorSurveysInfo GetSponsorSurveysFromReader(IDataReader reader, bool readMemos)
//        {
//            SponsorSurveysInfo SponsorSurveys = new SponsorSurveysInfo(
//                  (int)reader["discountid"],
//                  (int)reader["questionid"]);
//            return SponsorSurveys;
//        }


//        /// <summary>
//        /// Returns a collection of SponsorSurveysInfo objects with the data read from the input DataReader
//        /// </summary>
//        protected virtual List<SponsorSurveysInfo> GetSponsorSurveysCollectionFromReader(IDataReader reader)
//        {
//            return GetSponsorSurveysCollectionFromReader(reader, true);
//        }
//        protected virtual List<SponsorSurveysInfo> GetSponsorSurveysCollectionFromReader(IDataReader reader, bool readMemos)
//        {
//            List<SponsorSurveysInfo> SponsorSurveyss = new List<SponsorSurveysInfo>();
//            while (reader.Read())
//                SponsorSurveyss.Add(GetSponsorSurveysFromReader(reader, readMemos));
//            return SponsorSurveyss;
//        }

//        #endregion

//        #region Methods that work with SponsorSurveyRsp

//        /////////////////////////////////////////////////////////
//        // methods that work with SponsorSurveys
//        //Bhaskar N 
//        public abstract bool InsertSponsorSurveyRsp(int userid, int orderitemid, int questionid, int answerid, int discountid);
//        public abstract bool UpdateSponsorSurveyRspByOrderitemId(int userid, int orderitemid, int discountid);
//        #endregion

//        #region Methods that work with StateReq

//        /////////////////////////////////////////////////////////
//        // methods that work with StateReq
//        //Bhaskar N
//        public abstract List<StateReqInfo> GetStateReqs(string cSortExpression);
//        public abstract StateReqInfo GetStateReqByStateAbr(string StateAbr);
//        public abstract StateReqInfo GetStateReqByCategoryId(int CategoryId);
//        public abstract List<StateReqInfo> GetStateReq();
       
//        public abstract bool InsertStateReq(StateReqInfo StateReq);
//        public abstract bool UpdateStateReq(StateReqInfo StateReq);
//        public abstract bool DeleteStateReq(string StateAbr);
//        public abstract bool UpdateStateReqByStateAbr(string Cereq, string Email, string Url, string LicAgency, string Lpn, string Stateabr, int CategoryId, string req_header, string course_recommend);

//        /// <summary>
//        /// Returns a new StateReqInfo instance filled with the DataReader's current record data
//        /// </summary>
//        protected virtual StateReqInfo GetStateReqFromReader(IDataReader reader)
//        {
//            return GetStateReqFromReader(reader, true);
//        }
//        protected virtual StateReqInfo GetStateReqFromReader(IDataReader reader, bool readMemos)
//        {
//            StateReqInfo StateReq = new StateReqInfo(
//                (Convert.IsDBNull(reader["StateAbr"]) ? "" : reader["StateAbr"].ToString()),
//                (Convert.IsDBNull(reader["StateName"]) ? "" : reader["StateName"].ToString()),
//                (Convert.IsDBNull(reader["CeReq"]) ? "" : reader["CeReq"].ToString()),
//                (Convert.IsDBNull(reader["LicAgency"]) ? "" : reader["LicAgency"].ToString()),
//                (Convert.IsDBNull(reader["Permlic"]) ? "" : reader["Permlic"].ToString()),
//                (Convert.IsDBNull(reader["TempLic"]) ? "" : reader["TempLic"].ToString()),
//                (Convert.IsDBNull(reader["ExtraLic"]) ? "" : reader["ExtraLic"].ToString()),
//                (Convert.IsDBNull(reader["Email"]) ? "" : reader["Email"].ToString()),
//                (Convert.IsDBNull(reader["Url"]) ? "" : reader["Url"].ToString()),
//                (Convert.IsDBNull(reader["Lpn"]) ? "" : reader["Lpn"].ToString()),
//                (DateTime)(Convert.IsDBNull(reader["LastUpdate"]) ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastUpdate"]),
//                (int)(Convert.IsDBNull(reader["CategoryId"]) ? (int)0 : (int)reader["CategoryId"]),
//                (decimal)(Convert.IsDBNull(reader["TaxRate"]) ? (decimal)0 : (decimal)reader["TaxRate"]),
//                (bool)(Convert.IsDBNull(reader["Inter_Ind"]) ? false : (bool)reader["Inter_Ind"]),
//            (Convert.IsDBNull(reader["req_header"]) ? "" : reader["req_header"].ToString()),
//                (Convert.IsDBNull(reader["course_recommend"]) ? "" : reader["course_recommend"].ToString()));
//            return StateReq;

//        }



//        /// <summary>
//        /// Returns a collection of StateReqInfo objects with the data read from the input DataReader
//        /// </summary>
//        protected virtual List<StateReqInfo> GetStateReqCollectionFromReader(IDataReader reader)
//        {
//            return GetStateReqCollectionFromReader(reader, true);
//        }
//        protected virtual List<StateReqInfo> GetStateReqCollectionFromReader(IDataReader reader, bool readMemos)
//        {
//            List<StateReqInfo> StateReqs = new List<StateReqInfo>();
//            while (reader.Read())
//                StateReqs.Add(GetStateReqFromReader(reader, readMemos));
//            return StateReqs;
//        }

//        #endregion
    }

}
