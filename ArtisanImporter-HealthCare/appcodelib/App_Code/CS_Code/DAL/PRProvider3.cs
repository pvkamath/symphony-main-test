﻿using System.Web.Security;
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PearlsReview.DAL
{

    /// <summary>
    /// Summary description for PRProvider3
    /// </summary>
    public class PRProvider3 : DataAccess
    {
       
           static private PRProvider3 _instance = null;
        /// <summary>
        /// Returns an instance of the provider type specified in the config file
        /// </summary>
        static public PRProvider3 Instance
        {
            get
            {
                if (_instance == null)
                    _instance = (PRProvider3)Activator.CreateInstance(
                       Type.GetType(Globals.Settings.PR.ProviderType));
                return _instance;
            }
        }

        public PRProvider3()
        {
            this.ConnectionString = Globals.Settings.PR.ConnectionString;
            this.EnableCaching = Globals.Settings.PR.EnableCaching;
            this.CacheDuration = Globals.Settings.PR.CacheDuration;
        }
       
    }
}
