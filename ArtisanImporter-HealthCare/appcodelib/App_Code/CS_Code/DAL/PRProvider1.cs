﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace PearlsReview.DAL
{
    /// <summary>
    /// Summary description for PRProvider1
    /// </summary>
    public abstract class PRProvider1 : DataAccess
    {
        static private PRProvider1 _instance = null;
        /// <summary>
        /// Returns an instance of the provider type specified in the config file
        /// </summary>
        static public PRProvider1 Instance
        {
            get
            {
                if (_instance == null)
                    _instance = (PRProvider1)Activator.CreateInstance(Type.GetType(Globals.Settings.PR.ProviderType));
                return _instance;
            }
        }

        public PRProvider1()
        {
            this.ConnectionString = Globals.Settings.PR.ConnectionString;
            this.EnableCaching = Globals.Settings.PR.EnableCaching;
            this.CacheDuration = Globals.Settings.PR.CacheDuration;
            this.RConnectionString = Globals.Settings.PR.RConnectionString;
        }

//        public abstract bool UpdateDepartment(DepartmentInfo departmentInfo);
//        public abstract int InsertDepartment(DepartmentInfo departmentInfo);
//        public abstract bool DeleteDepartment(int departmentID);
//        public abstract List<DepartmentInfo> GetDepartments(string fac_id, string sortExpression);
//        public abstract DepartmentInfo GetDepartmentByID(int dept_id);
//        public abstract System.Data.DataSet GetDuplicatesByFirstAndLastname(string facilityid);
//        public abstract System.Data.DataSet GetDuplicatesByUniqueid(string facilityid);
//        public abstract System.Data.DataSet GetMergeRecs(string fname, string lname, string facilityid);
//        public abstract System.Data.DataSet GetAssetUrlByTopicID(string TopicID);
//        public abstract bool MergeUsers(string strMasteriID, string fname, string lname, List<string> recsToMerge);
      
//        public abstract UserAccountInfo1 GetUserAccountByUserName(string userName);

//       // NOTE; The following method is used only for GiftCard login, and uses the "short"
//        // version of the username with no faciltiyid attached to it.
//        // This works because gift card codes are unique regardless of any facility related to it.
//        public abstract UserAccountInfo1 GetUserAccountByGiftCardRedemptionCode(string RedemptionCode);
        
//        public abstract List<UserAccountInfo1> GetUserAccountsBySearchCriteria(string sortExpression, string firstName, string lastName,
//                                                                   string cUserName, int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID, string roleName, int startRowIndex, int maximumRows);

//        public abstract List<UserAccountInfo1> GetUserAccountsBySearchCriteria(string sortExpression, string firstName, string lastName,
//                                                                  string cUserName, int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID, string roleName, int startRowIndex, int maximumRows, int rolepriority, int currentUserID, bool isActiveFacilityManager);

//        public abstract List<UserAccountInfo1> GetCERetailUsersBySearchCriteria(string cSortExpression, string cfirstName, string clastName, string cUserName,
//        string cpw, string cAddress1, string cEmail, string license_number, string cState, string state, string cZipCode);

//        public abstract  System.Data.DataSet GetNonUnlimitedOrders(string cSortExpression, string cfirstName, string clastName, string cUserName,
//        string cpw, string cAddress1, string cEmail, string license_number, string cState, string state, string cZipCode);

//        public abstract System.Data.DataSet GetTranscripts(string cSortExpression, string cfirstName, string clastName, string cUserName,
//        string cpw, string cAddress1, string cEmail, string license_number, string cState, string state, string cZipCode);

//        public abstract int GetUserAccountsCount(string firstName, string lastName,
//                                                                   string cUserName, int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID);
//        public abstract bool DeleteUser(int iID);
//        //public abstract bool InsertUser(string username, string firstname, string middlename, string lastname, string title, string password, string address1,
//        //    string address2, string city, string state, string zip, string homephone, string workphone,
//        //    string birthdate, string badgeid, string ssn, string hiredate, string termindate, int deptid,
//        //    int positionid, string pwdqs, string pwdans, string rolename, int facilityid);

//        public abstract bool Switchfacility(int iID, int facid);               
//        public abstract string GetUserNameByID(int userID            );
//        protected virtual UserAccountInfo1 GetUserAccountFromReader1(IDataReader reader)
//        {
//            return GetUserAccountFromReader1(reader, true);
//        }
//        protected virtual UserAccountInfo1 GetUserAccountFromReader1(IDataReader reader, bool readMemos)
//        {
//            UserAccountInfo1 UserAccount = new UserAccountInfo1(
//            (int)reader["iID"],
//            reader["cAddress1"].ToString(),
//            reader["cAddress2"].ToString(),
//            (bool)reader["lAdmin"],
//            (bool)reader["lPrimary"],
//            reader["cBirthDate"].ToString(),
//            reader["cCity"].ToString(),
//            reader["cEmail"].ToString(),
//            reader["cExpires"].ToString(),
//            reader["cFirstName"].ToString(),
//            reader["cFloridaNo"].ToString(),
//            reader["cInstitute"].ToString(),
//            reader["cLastName"].ToString(),
//            reader["cLectDate"].ToString(),
//            reader["cMiddle"].ToString(),
//            reader["cPhone"].ToString(),
//            reader["cPromoCode"].ToString(),
//            (int)(Convert.IsDBNull(reader["PromoID"]) ? (int)0 : (int)reader["PromoID"]),
//            (int)(Convert.IsDBNull(reader["FacilityID"]) ? (int)0 : (int)reader["FacilityID"]),
//            reader["cPW"].ToString(),
//            reader["cRefCode"].ToString(),
//            reader["cRegType"].ToString(),
//            reader["cSocial"].ToString(),
//            reader["cState"].ToString(),
//            reader["cUserName"].ToString().ToLower(),
//            reader["cZipCode"].ToString(),
//            (int)(Convert.IsDBNull(reader["SpecID"]) ? (int)0 : (int)reader["SpecID"]),
//            reader["LCUserName"].ToString().ToLower(),
//            (DateTime)(Convert.IsDBNull(reader["LastAct"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastAct"]),
//            reader["PassFmt"].ToString(),
//            reader["PassSalt"].ToString(),
//            reader["MobilePIN"].ToString(),
//            reader["LCEmail"].ToString(),
//            reader["PWQuest"].ToString(),
//            reader["PWAns"].ToString(),
//            (bool)reader["IsApproved"],
//            (bool)reader["IsOnline"],
//            (bool)reader["IsLocked"],
//            (DateTime)(Convert.IsDBNull(reader["LastLogin"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLogin"]),
//            (DateTime)(Convert.IsDBNull(reader["LastPWChg"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastPWChg"]),
//           (DateTime)(Convert.IsDBNull(reader["LastLock"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["LastLock"]),
//            (int)reader["XPWAtt"],
//            (DateTime)(Convert.IsDBNull(reader["XPWAttSt"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAttSt"]),
//            (int)reader["XPWAnsAtt"],
//            (DateTime)(Convert.IsDBNull(reader["XPWAnsSt"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["XPWAnsSt"]),
//            reader["Comment"].ToString(),
//            (DateTime)(Convert.IsDBNull(reader["Created"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Created"]),
//            (int)(Convert.IsDBNull(reader["AgeGroup"]) ? (int)0 : (int)reader["AgeGroup"]),
//            (int)(Convert.IsDBNull(reader["RegTypeID"]) ? (int)0 : (int)reader["RegTypeID"]),
//            (int)(Convert.IsDBNull(reader["RepID"]) ? (int)0 : (int)reader["RepID"]),
//            Convert.ToString(reader["Work_phone"].ToString()),
//            Convert.ToString(reader["Badge_ID"].ToString()),
//            (DateTime)(Convert.IsDBNull(reader["Hire_Date"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Hire_Date"]),
//            (DateTime)(Convert.IsDBNull(reader["Termin_Date"])
//                ? (DateTime)System.Data.SqlTypes.SqlDateTime.Null : (DateTime)reader["Termin_Date"]),
//            (int)(Convert.IsDBNull(reader["dept_ID"]) ? (int)0 : (int)reader["dept_ID"]),
//            (int)(Convert.IsDBNull(reader["Position_ID"]) ? (int)0 : (int)reader["Position_ID"]),
//            (Convert.IsDBNull(reader["Clinical_Ind"]) ? 0 : Convert.ToInt32(reader["Clinical_Ind"])),
//            Convert.ToString(reader["Title"].ToString()),
//            (Convert.IsDBNull(reader["Giftcard_Ind"]) ? false : Convert.ToBoolean(reader["Giftcard_Ind"])),
//            (decimal)(Convert.IsDBNull(reader["Giftcard_Chour"]) ? (decimal)0 : (decimal)reader["Giftcard_Chour"]),
//            (decimal)(Convert.IsDBNull(reader["Giftcard_Uhour"]) ? (decimal)0 : (decimal)reader["Giftcard_Uhour"]),
//            (int)(Convert.IsDBNull(reader["UniqueID"]) ? (int)0 : (int)reader["UniqueID"]),
//            (bool)reader["FirstLogin_Ind"]);

//            return UserAccount;
            
//            //UserAccountInfo1 UserAccount = new UserAccountInfo1(

//            ////UserAccountInfo1 UserAccount = new UserAccountInfo1(
//            //  (int)reader["iID"],
//            //    //reader["cAddress1"].ToString(),
//            //    //reader["cAddress2"].ToString(),
//            //    //(bool)reader["lAdmin"],
//            //    //(bool)reader["lPrimary"],
//            //    //reader["cBirthDate"].ToString(),
//            //    //reader["cCity"].ToString(),
//            //    //reader["cEmail"].ToString(),
//            //    //reader["cExpires"].ToString(),
//            //  reader["cFirstName"].ToString(),
//            //reader["cLastName"].ToString());

//            //return UserAccount;
//        }              
//        protected virtual List<UserAccountInfo1> GetUserAccountCollectionFromReader1(IDataReader reader)
//        {
//            return GetUserAccountCollectionFromReader1(reader, true);
//        }
//        protected virtual List<UserAccountInfo1> GetUserAccountCollectionFromReader1(IDataReader reader, bool readMemos)
//        {
//            List<UserAccountInfo1> UserAccounts = new List<UserAccountInfo1>();
//            while (reader.Read())
//                UserAccounts.Add(GetUserAccountFromReader1(reader, readMemos));

//            return UserAccounts;
//        }
//        public abstract List<UserAccountInfo1> GetUserAccountsByDeptName(string dept_name, string cSortExpression);
//        public abstract List<DepartmentInfo> GetDepartmentsByUserID(int userID);
//        public abstract void UpdateDepartmentsForUserID(List<DepartmentInfo> departments, int userID);
//        public abstract void UpdatePrimaryDepartmentForUserID(int deptID, int userID);
//        public abstract bool DeActivateUser(int userID);
//        public abstract bool ActivateUser(int userID);

//        public abstract List<OrdersInfo> GetOrdersbyDate(string cSortExpression, string selDate, bool IsPending, bool isShipped);
//        public abstract List<OrdersInfo> GetOrdersbyUserID(int userid);
       
//        public abstract List<OrdersInfo> GetOrders(string cSortExpression);
//        public abstract List<OrdersInfo> GetPendingOrders(string cSortExpression);
//        public abstract List<OrdersInfo> GetShippedOrders(string cSortExpression);
     
//        public abstract OrdersInfo GetOrdersByID(int OrdersID);
//        public abstract int InsertOrders(OrdersInfo Orders);
//        public abstract bool UpdateOrders(OrdersInfo Orders);

//        public abstract bool DeleteOrders(int OrdersID);
      
//        /// <summary>
//        /// Returns a new OrdersInfo instance filled with the DataReader's current record data
//        /// </summary>
//        protected virtual OrdersInfo GetOrdersFromReader(IDataReader reader)
//        {
//            return GetOrdersFromReader(reader, true);
//        }
//        protected virtual OrdersInfo GetOrdersFromReader(IDataReader reader, bool readMemos)
//        {
//            OrdersInfo Orders = new OrdersInfo(
//                  (int)(Convert.IsDBNull(reader["OrderID"]) ? (int)0 : (int)reader["OrderID"]),
//                  (int)(Convert.IsDBNull(reader["UserID"]) ? (int)0 : (int)reader["UserID"]),
//                  Convert.ToString(reader["FirstName"].ToString()),
//                  Convert.ToString(reader["LastName"].ToString()),
//                  Convert.ToString(reader["Address1"].ToString()),
//                  Convert.ToString(reader["Address2"].ToString()),
//                  Convert.ToString(reader["City"].ToString()),
//                  Convert.ToString(reader["State"].ToString()),
//                  Convert.ToString(reader["Zip"].ToString()),
//                  Convert.ToString(reader["Country"].ToString()),
//                  Convert.ToString(reader["Email"].ToString()),
//                  (decimal)(Convert.IsDBNull(reader["SubTotal"]) ? (decimal)0 : (decimal)reader["SubTotal"]),
//                  (decimal)(Convert.IsDBNull(reader["ShipCost"]) ? (decimal)0 : (decimal)reader["ShipCost"]),
//                  (decimal)(Convert.IsDBNull(reader["Tax"]) ? (decimal)0 : (decimal)reader["Tax"]),
//                  (decimal)(Convert.IsDBNull(reader["TotalCost"]) ? (decimal)0 : (decimal)reader["TotalCost"]),
//                  (Convert.IsDBNull(reader["Validate_Ind"]) ? false : (bool)reader["Validate_Ind"]),
//                  Convert.ToString(reader["Verification"].ToString()),
//                  (Convert.IsDBNull (reader["Ship_Ind"]) ? false : (bool)reader["Ship_Ind"]),
//                  (DateTime)reader["OrderDate"],
//                  Convert.ToString(reader["Comment"].ToString()),
//                  (int)(Convert.IsDBNull(reader["CouponId"]) ? (int)0 : (int)reader[""]),
//                  (decimal)(Convert.IsDBNull(reader["Couponamount"]) ? (decimal)0 : (decimal)reader["Couponamount"]));
                   
//            return Orders;
//        }

//        public abstract List<OrderItemInfo> GetOrderDetails(int OrderID);

//        /// <summary>
//        /// Returns a collection of OrdersInfo objects with the data read from the input DataReader
//        /// </summary>
//        protected virtual List<OrdersInfo> GetOrdersCollectionFromReader(IDataReader reader)
//        {
//            return GetOrdersCollectionFromReader(reader, true);
//        }
//        protected virtual List<OrdersInfo> GetOrdersCollectionFromReader(IDataReader reader, bool readMemos)
//        {
//            List<OrdersInfo> Orders = new List<OrdersInfo>();
//            while (reader.Read())
//                Orders.Add(GetOrdersFromReader(reader, readMemos));
//            return Orders;
//        }

//        public abstract void UpdateOrdersForOrderID(OrdersInfo Orders,int OrderID);
//        protected virtual OrderItemInfo GetOrderItemFromReader(IDataReader reader)
//        {
//            return GetOrderItemFromReader(reader, true);
//        }
//        protected virtual OrderItemInfo GetOrderItemFromReader(IDataReader reader, bool readMemos)
//        {
//            OrderItemInfo Orders = new OrderItemInfo(
//                  (int)(Convert.IsDBNull(reader["OrderItemID"]) ? (int)0 : (int)reader["OrderItemID"]),
//                  (int)(Convert.IsDBNull(reader["OrderID"]) ? (int)0 : (int)reader["OrderID"]),
//                  (int)(Convert.IsDBNull(reader["TopicID"]) ? (int)0 : (int)reader["TopicID"]),
//                  (decimal)(Convert.IsDBNull(reader["Cost"]) ? (decimal)0 : (decimal)reader["Cost"]),
//                  Convert.ToString(reader["Media_Type"].ToString()),
//                  (int)(Convert.IsDBNull(reader["Quantity"]) ? (int)0 : (int)reader["Quantity"]),
//                  (int)(Convert.IsDBNull(reader["DiscountID"]) ? (int)0 : (int)reader["DiscountID"]),
//                  (bool)reader["Optin_Ind"]); 

//            return Orders;
//        }
//        public abstract int InsertOrderItem(OrderItemInfo OrderItem);

//        public abstract MembershipLogInfo GetMembershipLogByUserID(int userid);
//        public abstract bool UpdateMembershipLog(MembershipLogInfo MembershipLog);
//        public abstract bool InsertMembershipLog(MembershipLogInfo MembershipLog);
//        public abstract List<MembershipLogInfo> GetMembershipLogListByUserID(int userid);

//        public abstract UCEmembershipInfo GetUCEmembershipByRnID(int RnID);
//        public abstract bool UpdateUCEmembership(UCEmembershipInfo UCEmembership);
//        public abstract bool InsertUCEmembership(UCEmembershipInfo UCEmembership);
//        public abstract List<UCEmembershipInfo> GetUCEmembershipListByRnID(int RnID);
//        public abstract bool CancelUCEmembership(int RnID);
      
//        public abstract bool CancelMembershipLog(int userid);

//        protected virtual CouponsInfo GetCouponsFromReader(IDataReader reader)
//        {
//            return GetCouponsFromReader(reader, true);
//        }
//        protected virtual CouponsInfo GetCouponsFromReader(IDataReader reader, bool readMemos)
//        {
//            CouponsInfo Coupons = new CouponsInfo(


//                   (int)(Convert.IsDBNull(reader["CID"]) ? (int)0 : (int)reader["CID"]),

//                  Convert.ToString(reader["CouponCode"].ToString()),
//                  Convert.ToString(reader["CouponDesc"].ToString()),
//                  (decimal)(Convert.IsDBNull(reader["CAmount"]) ? (decimal)0 : (decimal)reader["CAmount"]),
//                  (DateTime)reader["ExpireDate"],
//                  (int)(Convert.IsDBNull(reader["FacilityID"]) ? (int)0 : (int)reader["FacilityID"]),
//                  Convert.ToString(reader["CouponType"].ToString()),
//                   (int)(Convert.IsDBNull(reader["TopicID"]) ? (int)0 : (int)reader["TopicID"]),

//                  Convert.ToString(reader["MediaType"].ToString())
//                  );

//            return Coupons;
//        }
//        public abstract List<CouponsInfo> GetCoupons(string facilityid, string sortExpression);
//        public abstract CouponsInfo GetCouponByFacID(int facilityid);
//        public abstract int GetCouponIdByCode(string Code);
    }
}