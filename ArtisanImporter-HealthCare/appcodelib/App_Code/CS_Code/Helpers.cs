using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Caching;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using PearlsReview.Constants;
using System.Text;

namespace PearlsReview
{
   public static class Helpers
   {
      private static string[] _countries = new string[] { 
         "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", 
         "Angola", "Anguilla", "Antarctica", "Antigua And Barbuda", "Argentina", 
         "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan",
		   "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus",
		   "Belgium", "Belize", "Benin", "Bermuda", "Bhutan",
		   "Bolivia", "Bosnia Hercegovina", "Botswana", "Bouvet Island", "Brazil",
		   "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Byelorussian SSR",
		   "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands",
		   "Central African Republic", "Chad", "Chile", "China", "Christmas Island",
		   "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Cook Islands",
		   "Costa Rica", "Cote D'Ivoire", "Croatia", "Cuba", "Cyprus",
		   "Czech Republic", "Czechoslovakia", "Denmark", "Djibouti", "Dominica",
		   "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador",
		   "England", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia",
		   "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France",
		   "Gabon", "Gambia", "Georgia", "Germany", "Ghana",
		   "Gibraltar", "Great Britain", "Greece", "Greenland", "Grenada",
		   "Guadeloupe", "Guam", "Guatemela", "Guernsey", "Guiana",
		   "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard Islands",
		   "Honduras", "Hong Kong", "Hungary", "Iceland", "India",
		   "Indonesia", "Iran", "Iraq", "Ireland", "Isle Of Man",
		   "Israel", "Italy", "Jamaica", "Japan", "Jersey",
		   "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, South",
		   "Korea, North", "Kuwait", "Kyrgyzstan", "Lao People's Dem. Rep.", "Latvia",
		   "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein",
		   "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar",
		   "Malawi", "Malaysia", "Maldives", "Mali", "Malta",
		   "Mariana Islands", "Marshall Islands", "Martinique", "Mauritania", "Mauritius",
		   "Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco",
		   "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar",
		   "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles",
		   "Neutral Zone", "New Caledonia", "New Zealand", "Nicaragua", "Niger",
		   "Nigeria", "Niue", "Norfolk Island", "Northern Ireland", "Norway",
		   "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea",
		   "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland",
		   "Polynesia", "Portugal", "Puerto Rico", "Qatar", "Reunion",
		   "Romania", "Russian Federation", "Rwanda", "Saint Helena", "Saint Kitts",
		   "Saint Lucia", "Saint Pierre", "Saint Vincent", "Samoa", "San Marino",
		   "Sao Tome and Principe", "Saudi Arabia", "Scotland", "Senegal", "Seychelles",
		   "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands",
		   "Somalia", "South Africa", "South Georgia", "Spain", "Sri Lanka",
		   "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden",
		   "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikista", "Tanzania",
		   "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago",
		   "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu",
		   "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States",
		   "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City State", "Venezuela",
		   "Vietnam", "Virgin Islands", "Wales", "Western Sahara", "Yemen",
		   "Yugoslavia", "Zaire", "Zambia", "Zimbabwe"};

      /// <summary>
      /// Returns an array with all countries
      /// </summary>     
      public static StringCollection GetCountries()
      {
         StringCollection countries = new StringCollection();
         countries.AddRange(_countries);
         return countries;
      }
      public static SortedList GetCountries(bool insertEmpty)
      {
         SortedList countries = new SortedList();
         if (insertEmpty)
            countries.Add("", "Please select one...");
         foreach (String country in _countries)
            countries.Add(country, country);
         return countries;
      }

      /// <summary>
      /// Returns an array with the names of all local Themes
      /// </summary>
      public static string[] GetThemes()
      {
         if (HttpContext.Current.Cache["SiteThemes"] != null)
         {
            return (string[])HttpContext.Current.Cache["SiteThemes"];
         }
         else
         {
            string themesDirPath = HttpContext.Current.Server.MapPath("~/App_Themes");
            // get the array of themes folders under /app_themes
            string[] themes = Directory.GetDirectories(themesDirPath);
            for (int i = 0; i <= themes.Length - 1; i++)
	            themes[i] = Path.GetFileName(themes[i]);
            // cache the array with a dependency to the folder
            CacheDependency dep = new CacheDependency(themesDirPath);
            HttpContext.Current.Cache.Insert("SiteThemes", themes, dep);
            return themes;
         }
      }

      /// <summary>
      /// Adds the onfocus and onblur attributes to all input controls found in the specified parent,
      /// to change their apperance with the control has the focus
      /// </summary>
      public static void SetInputControlsHighlight(Control container, string className, bool onlyTextBoxes)
      {
         foreach (Control ctl in container.Controls)
         {
            if ((onlyTextBoxes && ctl is TextBox) || ctl is TextBox || ctl is DropDownList ||
	            ctl is ListBox || ctl is CheckBox || ctl is RadioButton || 
	            ctl is RadioButtonList || ctl is CheckBoxList)
            {
               WebControl wctl = ctl as WebControl;
               wctl.Attributes.Add("onfocus", string.Format("this.className = '{0}';", className));
               wctl.Attributes.Add("onblur", "this.className = '';");
            }
            else
            {
               if (ctl.Controls.Count > 0)
                  SetInputControlsHighlight(ctl, className, onlyTextBoxes);
            }
         }
      }


      /// <summary>
      /// Converts the input plain-text to HTML version, replacing carriage returns
      /// and spaces with <br /> and &nbsp;
      /// </summary>
      public static string ConvertToHtml(string content)
      {
         content = HttpUtility.HtmlEncode(content);
         content = content.Replace("  ", "&nbsp;&nbsp;").Replace(
            "\t", "&nbsp;&nbsp;&nbsp;").Replace("\n", "<br>");
         return content;
      }

 
       // smart redirect that can handle target and windowFeatures
       // from public code posted at:
      // http://weblogs.asp.net/infinitiesloop/archive/2007/09/25/response-redirect-into-a-new-window-with-extension-methods.aspx

       public static void Redirect(string url, string target, string windowFeatures) 
      { 
          HttpContext context = HttpContext.Current; 
          if ((String.IsNullOrEmpty(target) || target.Equals("_self", StringComparison.OrdinalIgnoreCase)) 
              && String.IsNullOrEmpty(windowFeatures)) 
          { 
              context.Response.Redirect(url); 
          } 
          else 
          { 
              Page page = (Page)context.Handler; 
              if (page == null) 
              { 
                  throw new InvalidOperationException("Cannot redirect to new window outside Page context."); 
              } 
              url = page.ResolveClientUrl(url); 
              string script; 
              if (!String.IsNullOrEmpty(windowFeatures)) 
              { 
                  script = @"window.open(""{0}"", ""{1}"", ""{2}"");"; 
              } 
              else 
              { 
                  script = @"window.open(""{0}"", ""{1}"");"; 
              } 
              script = String.Format(script, url, target, windowFeatures); 
              ScriptManager.RegisterStartupScript(page, typeof(Page), "Redirect", script, true); 
          } 
      }


    /// <summary>
      /// Use this method to convert CSV to string Array
      /// </summary>
       public static string[] CSVToArray(this string source)
       {
           return source.Split(',');
       }
       /// <summary>
       /// Use this method to convert stringarray to CSV
       /// </summary>
       public static string ToCSV(this string[] source)
       {
           string ret = string.Empty;
           foreach (var item in source)
           {
               ret = ret + item + ",";
           }
           return ret.Substring(0, ret.Length - 1);
       }

       /// <summary>
       /// Removes hexadecimal characters and other control characters
       /// </summary>
       public static string CleanupString(this string inString)
       {
           if (inString == null) return null;

           StringBuilder newString = new StringBuilder();
           char ch;

           for (int i = 0; i < inString.Length; i++)
           {

               ch = inString[i];
               // remove any characters outside the valid UTF-8 range as well as all control characters
               // except tabs and new lines
               if ((ch < 0x00FD && ch > 0x001F) || ch == '\t' || ch == '\n' || ch == '\r')
               {
                   newString.Append(ch);
               }
           }
           return newString.ToString();

       }
       

       public static int ValidateandSaveImage(FileUpload cont, string SavePath, bool OverwriteexistingImage)
       {
           Regex ImageRegex = new Regex(@"(.*?)\.(jpg|jpeg|png|gif)$",RegexOptions.IgnoreCase);
           int ValidationCode = 0;
           try
           {
               if (cont.HasFile)
               {
                   SavePath = HttpContext.Current.Request.MapPath(SavePath) + cont.PostedFile.FileName;
                   string fileextension = Path.GetExtension(SavePath);

                   if (ImageRegex.IsMatch(fileextension))
                   {
                       if (File.Exists(SavePath) && OverwriteexistingImage==false)
                       {                           
                           ValidationCode = ValidationConstants.V_FileExists;
                       }
                       else
                       {
                           ValidationCode = ValidationConstants.V_Pass;
                           cont.PostedFile.SaveAs(SavePath);
                       }
                   }
                   else
                   {
                       ValidationCode = ValidationConstants.V_NotImage;
                   }
               }
           }
           catch (Exception)
           {
               ValidationCode= ValidationConstants.V_Fail;
               //throw;
           }
           
           return ValidationCode;
       }
       /// <summary>
       /// Coverts a querystring kind of string to dictionary key value pairs
       /// </summary>
       public static Dictionary<string,string> QSFormattoKVP(this string inString)
       {
           if (inString == null) return null;
           var x = inString.Split('&');
           Dictionary<string, string> returnval = new Dictionary<string, string>();
           foreach (var item in x)
           {
               var kvarray = item.Split('=');
               returnval.Add(kvarray[0], kvarray[1]);
           }
           return returnval;
       }

       public static string[] GetUserHostAndIPAddress(HttpRequest request)
       {
           string userAgent=string.Empty;                      
           string ip = string.Empty;
           if(request != null)
           {
               if (request.UserAgent != null)
                userAgent = request.UserAgent.Length > 200 ? request.UserAgent.Substring(0, 200) : request.UserAgent;
               string userHostAddress = request.UserHostAddress != null ? request.UserHostAddress.ToString() : "";
               ip = userHostAddress == "10.186.254.17" ? (request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null ? request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString() : "") : userHostAddress;
           }
           return new string[] { userAgent, ip };
       }
   }
}
