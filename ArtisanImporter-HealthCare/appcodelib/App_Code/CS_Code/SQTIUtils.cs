﻿using System;
using System.Data;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using PearlsReview.QTI;

/// <summary>
/// Summary description for SQTIUtils
/// </summary>

namespace PearlsReview.QTI
{
    public class SQTIQuestionObject
    {
        private string _cQuestionID = "";
        public string cQuestionID {
	        get { return _cQuestionID; }
	        set { _cQuestionID = value; }
        }

        private string _cQuestionTitle = "";
        public string cQuestionTitle {
	        get { return _cQuestionTitle; }
	        set { _cQuestionTitle = value; }
        }

        private string _cQuestionText = "";
        public string cQuestionText {
	        get { return _cQuestionText; }
	        set { _cQuestionText = value; }
        }

        private string _cAnswer_A_Text = "";
        public string cAnswer_A_Text {
	        get { return _cAnswer_A_Text; }
	        set { _cAnswer_A_Text = value; }
        }

        private string _cAnswer_B_Text = "";
        public string cAnswer_B_Text {
	        get { return _cAnswer_B_Text; }
	        set { _cAnswer_B_Text = value; }
        }

        private string _cAnswer_C_Text = "";
        public string cAnswer_C_Text {
	        get { return _cAnswer_C_Text; }
	        set { _cAnswer_C_Text = value; }
        }

        private string _cAnswer_D_Text = "";
        public string cAnswer_D_Text {
	        get { return _cAnswer_D_Text; }
	        set { _cAnswer_D_Text = value; }
        }

        private string _cAnswer_E_Text = "";
        public string cAnswer_E_Text {
	        get { return _cAnswer_E_Text; }
	        set { _cAnswer_E_Text = value; }
        }

        private string _cAnswer_F_Text = "";
        public string cAnswer_F_Text {
	        get { return _cAnswer_F_Text; }
	        set { _cAnswer_F_Text = value; }
        }

        private string _cRecordOrder = "";
        public string cRecordOrder {
	        get { return _cRecordOrder; }
	        set { _cRecordOrder = value; }
        }

        private string _cUserAnswer = "";
        public string cUserAnswer {
	        get { return _cUserAnswer; }
	        set { _cUserAnswer = value; }
        }

        private string _cControl_Type = "";
        public string cControl_Type {
	        get { return _cControl_Type; }
	        set { _cControl_Type = value; }
        }
    }

    public class SQTIUtils
    {
        public SQTIUtils()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public String ConvertSQTIQuestionObjectListToQTITestXMLString(List<SQTIQuestionObject> QTIQuestionList)
        {
            // use XML literals to create QTI-formatted XML Question from Question object passed in
            XElement cQTITestXML = new XElement("questestinterop", 
                from SQTIQuestionObject obj in QTIQuestionList
                select ConvertSQTIQuestionObjectToQTIQuestionXML(obj)
                );

            String cQTITestXMLString = cQTITestXML.ToString();
            // fix up some problems with starting and ending < and > around CDATA sections
            // and some other items inside CDATA sections that are encoded when they should not be
            cQTITestXMLString = System.Web.HttpUtility.HtmlDecode(cQTITestXMLString);
            // once more to take care of some items that are "double-encoded"
            // (&amp;quot;) - the first htmldecode takes care of the &amp; but
            // then we need a second one to turn the resulting &quot; into a quote mark 
            cQTITestXMLString = System.Web.HttpUtility.HtmlDecode(cQTITestXMLString);
            return cQTITestXMLString;
        }

        public XElement ConvertSQTIQuestionObjectToQTIQuestionXML(SQTIQuestionObject oQuestionObject)
        {
            // use XML literals to create QTI-formatted XML Question from Question object passed in
            XElement cQTIQuestionXML = new XElement("item", 
                new XAttribute("title", oQuestionObject.cQuestionTitle),
                new XAttribute("ident", oQuestionObject.cQuestionID),

                new XElement("presentation", 
                    new XAttribute("label", oQuestionObject.cQuestionID),
                    new XElement("material", new XElement("mattext", QTIUtils.CreateCDataSection(oQuestionObject.cQuestionText))),
                    new XElement("control_type", new XAttribute("ident", oQuestionObject.cControl_Type)),
                    new XElement("response_lid", 
                        new XAttribute("ident", "0"),
                        new XAttribute("rcardinality", "Single"),
                        new XAttribute("rtiming", "No"),
                        new XElement("render_choice", 
                            new XAttribute("shuffle", "No"),
                            new XElement("response_label", 
                                new XAttribute("ident", "A"),
                                new XElement("material", new XElement("mattext", QTIUtils.CreateCDataSection(oQuestionObject.cAnswer_A_Text)))
                                ),
                            new XElement("response_label", 
                                new XAttribute("ident", "B"),
                                new XElement("material", new XElement("mattext", QTIUtils.CreateCDataSection(oQuestionObject.cAnswer_B_Text)))
                                ),
                            new XElement("response_label", 
                                new XAttribute("ident", "C"),
                                new XElement("material", new XElement("mattext", QTIUtils.CreateCDataSection(oQuestionObject.cAnswer_C_Text)))
                                ),
                            new XElement("response_label", 
                                new XAttribute("ident", "D"),
                                new XElement("material", new XElement("mattext", QTIUtils.CreateCDataSection(oQuestionObject.cAnswer_D_Text)))
                                ),
                            new XElement("response_label", 
                                new XAttribute("ident", "E"),
                                new XElement("material", new XElement("mattext", QTIUtils.CreateCDataSection(oQuestionObject.cAnswer_E_Text)))
                                ),
                            new XElement("response_label", 
                                new XAttribute("ident", "F"),
                                new XElement("material", new XElement("mattext", QTIUtils.CreateCDataSection(oQuestionObject.cAnswer_F_Text)))
                                )
                            )
                        )
                    ),

                new XElement("itemfeedback", 
                    new XAttribute("ident", "Correct"),
                    new XElement("material", new XElement("mattext", QTIUtils.CreateCDataSection(oQuestionObject.cRecordOrder)))
                    )
                );

            return cQTIQuestionXML;

            #region Origional VB Code
            //Dim cQTIQuestionXML As XElement = _
            //<item title=<%= "" + oQuestionObject.cQuestionTitle + "" %> ident=<%= "" + oQuestionObject.cQuestionID + "" %>>
            //    <presentation label=<%= "" + oQuestionObject.cQuestionID + "" %>>
            //        <material>
            //            <mattext><%= CreateCDataSection(oQuestionObject.cQuestionText) %></mattext>
            //        </material>_
            //        <control_type ident=<%= "" + oQuestionObject.cControl_Type + "" %>></control_type>
            //        <response_lid ident="0" rcardinality="Single" rtiming="No">
            //            <render_choice shuffle="No">
            //                <response_label ident="A">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_A_Text) %></mattext></material>
            //                </response_label>
            //                <response_label ident="B">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_B_Text) %></mattext></material>
            //                </response_label>
            //                <response_label ident="C">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_C_Text) %></mattext></material>
            //                </response_label>
            //                <response_label ident="D">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_D_Text) %></mattext></material>
            //                </response_label>
            //                <response_label ident="E">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_E_Text) %></mattext></material>
            //                </response_label>
            //                <response_label ident="F">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_F_Text) %></mattext></material>
            //                </response_label>
            //            </render_choice>
            //        </response_lid>
            //    </presentation>

            //    <itemfeedback ident="Correct">
            //        <material><mattext><%= CreateCDataSection(oQuestionObject.cRecordOrder) %></mattext></material>
            //    </itemfeedback>
            //</item>
            #endregion
        }

        public List<SQTIQuestionObject> ConvertQTITestXMLToSQTIQuestionObjectList(String cQTITestXML)
        {
            List<SQTIQuestionObject> QTIQuestionList = new List<SQTIQuestionObject>();
            // parse out parts of the XML to populate properties of the multiple SQTIQuestionObjects
            XDocument XTest = XDocument.Parse(cQTITestXML);

            var QuestionList = (from xQuestion in XTest.Descendants("item")
                                select new
                                {
                                    QuestionText = xQuestion.Element("presentation").Element("material").Element("mattext").Value,
                                    QuestionID = xQuestion.Element("presentation").Element("response_lid").Attribute("ident").Value,
                                    cControl_Type = xQuestion.Element("presentation").Element("control_type").Attribute("ident").Value,
                                    RecordOrder = xQuestion.Element("itemfeedback").Element("material").Element("mattext").Value,
                                    AnswerTexts = (from xResponse_Label in xQuestion.Element("presentation").Element("response_lid").Element("render_choice").Elements("response_label")
                                                    select new
                                                    {
                                                        AnswerTextOrdinal = xResponse_Label.Attribute("ident").Value, 
                                                        AnswerText = xResponse_Label.Element("material").Element("mattext").Value
                                                    }).ToList()
                                });

            foreach (var Question in QuestionList)
            {
                SQTIQuestionObject oQTIQuestion = new SQTIQuestionObject();
                oQTIQuestion.cQuestionID = Question.QuestionID.ToString();

                if (Question.cControl_Type == null || Question.cControl_Type == "")
                    oQTIQuestion.cControl_Type = "Single";
                else
                    oQTIQuestion.cControl_Type = Question.cControl_Type.ToString();

                oQTIQuestion.cQuestionText = Question.QuestionText.ToString();
                oQTIQuestion.cRecordOrder = Question.RecordOrder.ToString();

                foreach (var AnswerText in Question.AnswerTexts)
                {
                    if (AnswerText.AnswerTextOrdinal.ToString() == "A")
                        oQTIQuestion.cAnswer_A_Text = AnswerText.AnswerText.ToString();

                    if (AnswerText.AnswerTextOrdinal.ToString() == "B")
                        oQTIQuestion.cAnswer_B_Text = AnswerText.AnswerText.ToString();

                    if (AnswerText.AnswerTextOrdinal.ToString() == "C")
                        oQTIQuestion.cAnswer_C_Text = AnswerText.AnswerText.ToString();

                    if (AnswerText.AnswerTextOrdinal.ToString() == "D")
                        oQTIQuestion.cAnswer_D_Text = AnswerText.AnswerText.ToString();

                    if (AnswerText.AnswerTextOrdinal.ToString() == "E")
                        oQTIQuestion.cAnswer_E_Text = AnswerText.AnswerText.ToString();

                    if (AnswerText.AnswerTextOrdinal.ToString() == "F")
                        oQTIQuestion.cAnswer_F_Text = AnswerText.AnswerText.ToString();
                }

                QTIQuestionList.Add(oQTIQuestion);
            }

            return QTIQuestionList;
        }
    }
}
