using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Caching;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.DocumentFormat.OpenXml.Packaging;
using System.Xml;
using System.Xml.Linq;


namespace PearlsReview
{
    public static class Utils
    {

        #region Constants

        /// <summary>
        /// WordProcessingML Namespace
        /// </summary>
        public const string WordProcessingMLNamespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";

        /// <summary>
        /// DrawingML Namespace
        /// </summary>
        public const string DrawingMLNamespace = "http://schemas.openxmlformats.org/drawingml/2006/main";

        /// <summary>
        /// WordprocessingDrawing Namespace
        /// </summary>
        public const string WordprocessingDrawingNamespace = "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing";

        /// <summary>
        /// Relationships Namespace
        /// </summary>
        public const string RelationshipsNamespace = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";

        /// <summary>
        /// Relationships Namespace
        /// </summary>
        public const string VMLNamespace = "urn:schemas-microsoft-com:vml";

        #endregion

        /// <summary>
        /// Converts the DOCX file to HTML version
        /// </summary>
        public static string ConvertTextbookDOCXToHtml(string cFileName, string cFolderName)
        {

            //cHTML = "";
            StringBuilder sb = new StringBuilder();
            StringBuilder mastersb = new StringBuilder();

            bool bAddBulletDebugging = true;

            try
            {
                // Open document
                WordprocessingDocument document = WordprocessingDocument.Open(cFileName, false);

                XNamespace w = WordProcessingMLNamespace;
                XNamespace r = RelationshipsNamespace;
                XNamespace a = DrawingMLNamespace;
                XNamespace wp = WordprocessingDrawingNamespace;
                XNamespace v = VMLNamespace;

                XName w_r = w + "r";
                XName w_ins = w + "ins";
                XName w_hyperlink = w + "hyperlink";

                XDocument xDoc = XDocument.Load(
                    XmlReader.Create(
                        new StreamReader(document.MainDocumentPart.GetStream())
                     )
                );

                //              string defaultStyle = GetDefaultStyleName(document);

                // Fetch paragraphs
                var paragraphs = from l_paragraph in xDoc
                                            .Root
                                            .Element(w + "body")
                                            .Descendants(w + "p")
                                 let l_paragraph_styleNode = l_paragraph
                                             .Elements(w + "pPr")
                                             .Elements(w + "pStyle")
                                             .FirstOrDefault()
                                 let l_paragraph_inlineStyleNode = l_paragraph
                                             .Elements(w + "pPr")
                                             .FirstOrDefault()
                                 let l_paragraph_sz = l_paragraph
                                              .Elements(w + "pPr")
                                              .Elements(w + "rPr")
                                              .Elements(w + "sz")
                                              .FirstOrDefault()
                                 let l_paragraph_firstrunsz = l_paragraph
                                              .Elements(w + "r")
                                              .Elements(w + "rPr")
                                              .Elements(w + "sz")
                                              .FirstOrDefault()
                                 let l_paragraph_vanish = l_paragraph //Bsk
                                              .Elements(w + "pPr")
                                              .Elements(w + "rPr")
                                              .Elements(w + "vanish")
                                              .FirstOrDefault()
                                 let l_paragraph_bullet = l_paragraph
                                              .Elements(w + "pPr")
                                              .Elements(w + "numPr")
                                              .Elements(w + "ilvl")
                                              .FirstOrDefault()
                                 let l_paragraph_bullet_id = l_paragraph //Bsk
                                              .Elements(w + "pPr")
                                              .Elements(w + "numPr")
                                              .Elements(w + "numId")
                                              .FirstOrDefault()                                
                                 let l_paragraph_firstrunshaded = l_paragraph
                                              .Elements(w + "r")
                                              .Elements(w + "rPr")
                                              .Elements(w + "shd")
                                              .FirstOrDefault()
                                 let l_paragraph_shaded = l_paragraph
                                               .Elements(w + "pPr")
                                               .Elements(w + "shd")
                                               .FirstOrDefault()
                                 select new
                                 {
                                     ParagraphElement = l_paragraph,
                                     //CssClass = l_paragraph_styleNode != null ? ((string)l_paragraph_styleNode.Attribute(w + "val")).Replace(" ", "") : defaultStyle,
                                     //CssStyles = l_paragraph_inlineStyleNode != null ? CreateCssProperties(l_paragraph_inlineStyleNode.CreateReader()) : "",
                                     CssStyles = "",
                                     CssClass = "",
                                     ParagraphSize = l_paragraph_sz != null ? l_paragraph_sz.Attribute(w + "val").Value.ToString() : "",
                                     FirstRunSize = l_paragraph_firstrunsz != null ? l_paragraph_firstrunsz.Attribute(w + "val").Value.ToString() : "",
                                     IsVanish = l_paragraph_vanish != null ? true : false, //Bsk
                                     IsBullet = l_paragraph_bullet != null ? true : false,
                                     BulletLevel = l_paragraph_bullet != null ? l_paragraph_bullet.Attribute(w + "val").Value.ToString() : "0",
                                     BulletId = l_paragraph_bullet_id != null ? l_paragraph_bullet_id.Attribute(w + "val").Value.ToString() : "0", //Bsk
                                     FirstRunShaded = l_paragraph_firstrunshaded != null ? true : false,
                                     IsShaded = l_paragraph_shaded != null ? true : false,
                                     Runs = l_paragraph.Elements().Where(z => z.Name == w_r || z.Name == w_ins || z.Name == w_hyperlink)
                                 };


                int iSectionCount = 0;
                bool lInsideSection = false;
                int iCurrentBulletLevel = 0;
                int iCurrentBulletId = 0; //Bsk
                bool lInsideBulletGroup = false;
                bool lParagraphIsSectionHeading = false;
                bool lOverviewHeadingFound = false;
                //bool Orderlist = false; //Bsk New(Display order list(find order list in DOCX 1,2,3...and display in HTML)

                //Bsk
                //    foreach (var paragraph in paragraphs)
                //{
                //    // if we are inside a bullet group and this paragraph is not a bullet, close the <ul> tags
                //    // all the way back, in case we're in a nested bullet situation
                //    // NOTE: iCurrentBulletLevel is zero-based, thus the +1 in the for loop setup
                //    if (lInsideBulletGroup == true & paragraph.IsBullet == false)
                //    {
                //        for (int i = iCurrentBulletLevel + 1; i > 0; i--)
                //        {
                //            sb.AppendLine("</ul>");
                //        }
                //        // we have now cleared any outstanding bullet groups, so reset flags
                //        lInsideBulletGroup = false;
                //        iCurrentBulletLevel = 0;
                //    }

                //    // If this is a bullet and the level is lower than the previous bullet, close the <ul> tag
                //    if (paragraph.IsBullet == true & Int32.Parse(paragraph.BulletLevel.ToString()) < iCurrentBulletLevel)
                //    {
                //        sb.AppendLine("</ul>");
                //        iCurrentBulletLevel = Int32.Parse(paragraph.BulletLevel.ToString());
                //    }



                //    // if this is a bullet and we are not inside a bullet group or if this is a bullet
                //    // and its level is larger than the previous one, start one with new <ul> tag
                //    if ((paragraph.IsBullet == true & lInsideBulletGroup == false) ||
                //        (paragraph.IsBullet == true & Int32.Parse(paragraph.BulletLevel.ToString()) > iCurrentBulletLevel))
                //    {
                //        sb.AppendLine("<ul>");
                //        lInsideBulletGroup = true;
                //        iCurrentBulletLevel = Int32.Parse(paragraph.BulletLevel.ToString());
                //    }



                //************************Start****//Bsk *************************
                int iParagraphCounter = 0;
                Stack sBulletLevelStack = new Stack(32);
                Stack sBulletIdStack = new Stack(32);

                // Write paragraphs
                foreach (var paragraph in paragraphs)
                {
                    // for debugging, increment a paragraph counter
                    iParagraphCounter++;

                    // if we are inside a bullet group and this paragraph has NO runs, jump over it
                    // in case it is a "phantom" non-bullet paragraph embedded into a bullet section
                    // This means that there can be no blank lines between second-or higher-level indented
                    // bullet elements, but that should not be a problem at all.  Otherwise,
                    // our logic for closing out all open bullet groups will be fooled.
                    if (lInsideBulletGroup == true & (int)paragraph.Runs.Count() == 0)
                        continue;

                    // if we are inside a bullet group and this paragraph has a vanish tag, jump over it
                    // so our logic for closing out all open bullet groups will be fooled.
                    if (lInsideBulletGroup == true & paragraph.IsVanish == true)
                        continue;

                    // if we are inside a bullet group and this paragraph is not a bullet, close the <ul> tags
                    // all the way back, in case we're in a nested bullet situation
                    if (lInsideBulletGroup == true & paragraph.IsBullet == false)
                    {
                        //for (int i = iCurrentBulletLevel + 1; i > 0; i--)
                        for (int i = sBulletLevelStack.Count; i > 0; i--)
                        {
                            //Bsk
                            //if (Orderlist == true)
                            //{
                            //    sb.AppendLine("</OL>");
                            //    Orderlist = false;
                            //}
                            sb.AppendLine("</ul>");
                        }
                        // we have now cleared any outstanding bullet groups, so reset flags
                        // and clear the bullet level stack
                        lInsideBulletGroup = false;
                        iCurrentBulletLevel = 0;
                        iCurrentBulletId = 0;
                        sBulletLevelStack.Clear();
                        sBulletIdStack.Clear();
                    }

                    // If this is a bullet and the level is lower than the previous bullet, close the <ul> tag(s)
                    if (
                        paragraph.IsBullet == true &
                        ((Int32.Parse(paragraph.BulletLevel.ToString()) < iCurrentBulletLevel) ||
                          (Int32.Parse(paragraph.BulletLevel.ToString()) == iCurrentBulletLevel &
                           Int32.Parse(paragraph.BulletId.ToString()) < iCurrentBulletId)
                        )
                        )
                    {
                        int iNewBulletLevel = Int32.Parse(paragraph.BulletLevel.ToString());
                        int iNewBulletId = Int32.Parse(paragraph.BulletId.ToString());
                        // we might have several levels that we are skipping backwards, 
                        // but they are not always sequentially numbered, so we have to refer
                        // to the sBulletLevelStack/sBulleteIdStack combination to be sure
                        // we close each one to end up formatted at the correct level
                        int iActualBulletLevel = (int)sBulletLevelStack.Pop();
                        int iActualBulletId = (int)sBulletIdStack.Pop();
                        while
                        (
                        ((iActualBulletLevel > iNewBulletLevel) ||
                          (iActualBulletLevel == iNewBulletLevel & iActualBulletId > iNewBulletId))
                        )
                        {

                            // make sure we can't have hit an empty stack below
                            // This is a workaround for some inconsistently nested
                            // bullet groups
                            //if (sBulletLevelStack.Count == 0 || sBulletIdStack.Count == 0)
                            //    break;   


                            //if (Orderlist == true)  //Bsk New
                            //{
                            //    sb.AppendLine("</OL>");
                            //    Orderlist = false;
                            //}
                            sb.AppendLine("</ul>");

                            iActualBulletLevel = (int)sBulletLevelStack.Peek();
                            iActualBulletId = (int)sBulletIdStack.Peek();
                            if ((iActualBulletLevel > iNewBulletLevel) ||
                                 (iActualBulletLevel == iNewBulletLevel & iActualBulletId > iNewBulletId))
                            {
                                iActualBulletLevel = (int)sBulletLevelStack.Pop();
                                iActualBulletId = (int)sBulletIdStack.Pop();
                            }
                        }

                        // we have now cleared any bullet groups back to the new one for this paragraph,
                        // so set the current bullet level to the level for this paragraph
                        iCurrentBulletLevel = iNewBulletLevel;
                        iCurrentBulletId = iNewBulletId;
                    }


                    // if this is a bullet and we are not inside a bullet group or if this is a bullet
                    // and its level/id combination is larger than the previous one, start one with new <ul> tag
                    if (
                        (paragraph.IsBullet == true & lInsideBulletGroup == false) ||
                        (paragraph.IsBullet == true & Int32.Parse(paragraph.BulletLevel.ToString()) > iCurrentBulletLevel) ||
                        (paragraph.IsBullet == true & Int32.Parse(paragraph.BulletLevel.ToString()) == iCurrentBulletLevel &
                        Int32.Parse(paragraph.BulletId.ToString()) > iCurrentBulletId)
                       )
                    {

                        if (bAddBulletDebugging == true)
                        {
                            sb.AppendLine("<ul dssid='" + paragraph.BulletLevel.ToString() + "_" +
                                paragraph.BulletId.ToString() + "'>");
                            ////if (paragraph.BulletId.ToString() == "35")
                            //if (paragraph.BulletId.ToString() != "1") //Bsk New
                            //{
                            //    sb.AppendLine("<OL>");
                            //    Orderlist = true;

                            //}
                        }
                        else
                        {                            
                            sb.AppendLine("<ul>");
                        }

                        lInsideBulletGroup = true;
                        iCurrentBulletLevel = Int32.Parse(paragraph.BulletLevel.ToString());
                        iCurrentBulletId = Int32.Parse(paragraph.BulletId.ToString());
                        sBulletLevelStack.Push(iCurrentBulletLevel);
                        sBulletIdStack.Push(iCurrentBulletId);
                    }

                    //************************End****//Bsk *************************

                    // sz=28 (14 pt) means a section heading (some are also 24 (12 pt)
                    // NOTE: workaround for some bullets that are 12 pt
                    if ((paragraph.ParagraphSize == "28" || paragraph.ParagraphSize == "24" ||
                          paragraph.FirstRunSize == "28" || paragraph.FirstRunSize == "24") &
                        (paragraph.IsBullet == false &&
                        (paragraph.FirstRunShaded == true || paragraph.IsShaded == true)))
                    {
                        // flag that this paragraph is a section heading (needed for
                        // soft CR workaround in the runs section
                        lParagraphIsSectionHeading = true;

                        // if we're already inside a section, close the previous section div
                        // NOTE: This assumes that we'll only have 28 size as the section headings
                        if (lInsideSection == true)
                        {
                            // now check to see if we want to keep this "section" that is now
                            // contained in the stringbuilder
                            // NOTE: If this is the first "section" containing title and misc info,
                            // we will throw it away, otherwise store this section to the master sb
                            if (iSectionCount > 0 & lOverviewHeadingFound == true)
                            {
                                mastersb.AppendLine("<div id='Section" + iSectionCount.ToString() + "'>");
                                iSectionCount++;
                                mastersb.AppendLine(sb.ToString());
                                mastersb.AppendLine("</div>");
                            }

                            // in either case, we remove the contents of the sb to start over with the 
                            // next section
                            sb.Remove(0, sb.Length);

                        }
                        else
                        {
                            // this would be the first section, so no div tag to close yet
                            // lInsideSection = true;
                            sb.Remove(0, sb.Length);

                        }
                        // increment the section count and start the section and parag with sectionhead class
                        // NOTE: We keep it at zero for the first "section" which is really the overall 
                        // title heading that we are throwing away
                        sb.AppendLine("<p class='textbook_sectionhead'>");

                    }
                    else
                    {
                        lParagraphIsSectionHeading = false;

                        // if this is a bullet, add the <li> tag
                        if (paragraph.IsBullet == true)
                        {

                            if (bAddBulletDebugging == true)//Bsk 
                            {                                                                
                                    sb.AppendLine("<li dssid='" + paragraph.BulletLevel.ToString() + "_" +
                                        paragraph.BulletId.ToString() + "'>");                               
                            }
                            else
                            {
                                sb.AppendLine("<li>");
                            }
                        }
                        else
                        {
                            sb.AppendLine("<p>");
                        }
                    }
                    //                  sb.Append(String.Format("<p class=\"{0}\" style=\"{1}\">", paragraph.CssClass, paragraph.CssStyles));

                    // Fetch runs

                    // NOTE: Because we might have a paragraph with NO runs, we must check for valid Runs property
                    if (paragraph.Runs != null) //Bsk 
                    {
                        // NOTE: In this version, we are not rendering hyperlinks as hyperlinks, but
                        // are using the text of the hyperlink as plain text
                        // This was to work around a reference problem and also because the textbook
                        // topics should not have any hyperlinks in them (there were some words in the
                        // bibliography that had "hidden" hyperlinks embedded into them (did not show visually))
                        var runs = from l_run in paragraph.Runs
                                   let l_run_graphics = l_run
                                               .Elements(w + "drawing")
                                   let l_run_pictures = l_run
                                               .Elements(w + "pict")

                                   //Bsk
                                   //let l_run_styleNode = l_run
                                   //    .Elements(w + "rPr")
                                   //    .FirstOrDefault()
                                   //let l_run_inheritStyle = l_run
                                   //            .Elements(w + "rPr")
                                   //            .Elements(w + "rStyle")
                                   //            .FirstOrDefault()                                   
                                   //let l_run_bold = l_run
                                   //    .Elements(w + "rPr")
                                   //    .Elements(w + "b")
                                   //    .FirstOrDefault()

                                   select new
                                   {
                                       Run = l_run,
                                       RunType = (l_run.Name.LocalName == "hyperlink" ? "run" : l_run.Name.LocalName), //Bsk
                                       CssStyles = "",
                                       CssClass = "",
                                       IsVanish = l_run.Descendants(w + "vanish").FirstOrDefault() != null ? true : false, //Bsk
                                       //IsBold = l_run.Element(w + "rPr").Element(w + "b") != null ? true : false,
                                       // //IsBold = l_run_bold != null ? true : false,
                                       IsBold = l_run.Descendants(w + "b").FirstOrDefault() != null ? true : false,//Bsk
                                       //Text = l_run.Descendants(w + "t").StringConcatenate(element => (string)element),
                                       Text = l_run.Descendants(w + "t") != null ? l_run.Descendants(w + "t").StringConcatenate(element => (string)element) : "", //Bsk
                                       BreakBefore = l_run.Descendants(w + "br").FirstOrDefault() != null, //Bsk
                                       //ExternalRelationId = (l_run.Name == w_hyperlink ? (string)l_run.Attribute(r + "id") : ""), //Bsk
                                       Graphics = l_run_graphics,
                                       Pictures = l_run_pictures
                                   };
                        // Write runs
                        foreach (var run in runs)
                        {

                            // skip if marked "vanish"
                            if (run.IsVanish == true) //Bsk
                                continue;

                            // Break before?
                            if (run.BreakBefore)
                            {

                                if (lParagraphIsSectionHeading == true)
                                {
                                    // workaround for some section headings that don't have a hard
                                    // carriage return (paragraph end) but instead have soft CR
                                    // In those cases, we must forcibly end the paragraph here
                                    // and start another one.
                                    sb.Append("</p>");
                                    sb.AppendLine("<p>");
                                    lParagraphIsSectionHeading = false;
                                }
                                else
                                {
                                    sb.AppendLine("<br />");
                                }
                            }
                            // Write run
                            //sb.Append(
                            //    String.Format("<span class=\"{0}\" style=\"{1}\">",
                            //         run.CssClass,
                            //         run.CssStyles ?? ""
                            //     )
                            // );

                            // see if we need to turn bold on or off
                            if (run.IsBold == true)
                            {
                                sb.Append("<b>");
                            }


                            //// Is it an hyperlink? //Bsk
                            //if (run.RunType == "hyperlink")
                            //{
                            //    ExternalRelationship relation = (from rel in document.MainDocumentPart.ExternalRelationships
                            //                                     where rel.Id == run.ExternalRelationId
                            //                                     select rel).FirstOrDefault() as ExternalRelationship;
                            //    if (relation != null)
                            //    {
                            //        sb.Append("<a href=\"" + relation.Uri.ToString() + "\">");
                            //    }
                            //}

                            // Fetch graphics
                            var graphics = from l_graphic in run.Graphics //Bsk
                                           let l_graphic_blip = l_graphic
                                                     .Descendants(a + "blip")
                                                     .Where(x => x.Attribute(r + "embed") != null)
                                                     .FirstOrDefault()
                                           let l_graphic_extent = l_graphic
                                                     .Descendants(wp + "extent")
                                                     .FirstOrDefault()
                                           select new
                                           {
                                               //Width = l_graphic_extent != null ? EmuUnit.EmuToPixels((int)l_graphic_extent.Attribute("cx")) : 0,
                                               //Height = l_graphic_extent != null ? EmuUnit.EmuToPixels((int)l_graphic_extent.Attribute("cy")) : 0,
                                               ExternalRelationId = l_graphic_blip != null ? (string)l_graphic_blip.Attribute(r + "embed") : ""
                                           };

                            // Write graphics
                            foreach (var graphic in graphics)//Bsk
                            {

                                // Fetch document part
                                ImagePart imagePart = (ImagePart)document.MainDocumentPart.GetPartById(graphic.ExternalRelationId);

                                // Part found?
                                if (imagePart != null)
                                {
                                    // filename
                                    string imagefilename = Path.GetFileName(imagePart.Uri.ToString());

                                    // Write graphic
                                    sb.Append(String.Format(
                                        "<img src='{0}' /><br />",
                                        "/docx/" + cFolderName + "/" + imagefilename)
                                    );
                                }

                            }

                            // Fetch pictures (this format is used when DOC is converted to DOCX)
                            var pictures = from l_picture in run.Pictures
                                           let l_picture_shape = l_picture
                                                      .Descendants(v + "shape")
                                                      .FirstOrDefault()
                                           let l_picture_imagedata = l_picture
                                                      .Descendants(v + "shape")
                                                      .Descendants(v + "imagedata")
                                                      .FirstOrDefault()
                                           select new
                                           {
                                               ImgStyle = l_picture_shape != null ? (string)l_picture_shape.Attribute("style") : "",
                                               //Width = l_graphic_extent != null ? EmuUnit.EmuToPixels((int)l_graphic_extent.Attribute("cx")) : 0,
                                               //Height = l_graphic_extent != null ? EmuUnit.EmuToPixels((int)l_graphic_extent.Attribute("cy")) : 0,
                                               RelationshipId = l_picture_imagedata != null ? (string)l_picture_imagedata.Attribute(r + "id") : ""
                                           };

                            // Write pictures
                            foreach (var picture in pictures)
                            {
                                // get the image name


                                // Fetch document part
                                ImagePart imagePart = (ImagePart)document.MainDocumentPart.GetPartById(picture.RelationshipId);

                                // Part found?
                                if (imagePart != null)
                                {
                                    // filename
                                    string imagefilename = Path.GetFileName(imagePart.Uri.ToString());

                                    // Write graphic
                                    sb.Append(String.Format(
                                        "<img style='{0}' src='{1}' /><br />",
                                        picture.ImgStyle.ToString(),
                                        "/docx/" + cFolderName + "/" + imagefilename)
                                    );
                                }

                            }

                            // debugging code
                            if (run.Text.Contains("NPO until stable"))//Bsk
                                sb.Append("");

                            // Write text
                            sb.Append(RenderPlainText(run.Text));

                            // flag to indicate whether we've found the first section we want to keep
                            if (lOverviewHeadingFound == false & lParagraphIsSectionHeading == true &
                                run.Text.Trim() == "Overview")
                            {
                                // this is the first section we want to keep, so set the section flag and number
                                lOverviewHeadingFound = true;
                                iSectionCount = 1;
                                lInsideSection = true;
                            }

                            //// End hyperlink //Bsk
                            //if (run.RunType == "hyperlink")
                            //{
                            //    sb.Append("</a>");
                            //}

                            // End run
                            if (run.IsBold == true)
                            {
                                sb.Append("</b>");
                            }
                            //sb.Append("</span>");
                        }

                    }

                    if (paragraph.IsBullet == true)
                    {
                        if (bAddBulletDebugging == true)//Bsk
                        {     
                            sb.AppendLine("</li dssid='" + paragraph.BulletLevel.ToString() + "_" +
                                paragraph.BulletId.ToString() + "'>");
                            
                        }
                        else
                        {
                            sb.Append("</li>");
                        }

                    }
                    else
                    {
                        sb.Append("</p>");
                    }

                    sb.AppendLine();
                }

                document.Close();

                //return mastersb.ToString();

                //**********Start ******//Bsk*************************

                // at this point, the only thing remaining in the sb is the last section
                // so close it out by adding it to the mastersb

                mastersb.AppendLine("<div id='Section" + iSectionCount.ToString() + "'>");
                iSectionCount++;
                mastersb.AppendLine(sb.ToString());
                mastersb.AppendLine("</div>");

            }
            catch
            {
                mastersb.AppendLine("<div style='background-color:yellow;'>");
                mastersb.AppendLine("<h1>Formatting Error...</h1>");
                mastersb.AppendLine("</div>");
                mastersb.AppendLine(sb.ToString());
            }

            return mastersb.ToString();

            //**********End ******//Bsk*************************

        }


        /// <summary>
        /// Converts the DOCX file to HTML version
        /// </summary>
        public static string ConvertTextbookHandoutDOCXToHtml(string cFileName, string cFolderName)
        {

            //cHTML = "";
            StringBuilder sb = new StringBuilder();

            // Open document
            WordprocessingDocument document = WordprocessingDocument.Open(cFileName, false);

            XNamespace w = WordProcessingMLNamespace;
            XNamespace rels = RelationshipsNamespace;
            XNamespace a = DrawingMLNamespace;
            XNamespace wp = WordprocessingDrawingNamespace;

            XName w_r = w + "r";
            XName w_ins = w + "ins";
            XName w_hyperlink = w + "hyperlink";

            XDocument xDoc = XDocument.Load(
                XmlReader.Create(
                    new StreamReader(document.MainDocumentPart.GetStream())
                 )
            );

            //              string defaultStyle = GetDefaultStyleName(document);

            // Fetch paragraphs
            var paragraphs = from l_paragraph in xDoc
                                        .Root
                                        .Element(w + "body")
                                        .Descendants(w + "p")
                             let l_paragraph_styleNode = l_paragraph
                                         .Elements(w + "pPr")
                                         .Elements(w + "pStyle")
                                         .FirstOrDefault()
                             let l_paragraph_inlineStyleNode = l_paragraph
                                         .Elements(w + "pPr")
                                         .FirstOrDefault()
                             let l_paragraph_sz = l_paragraph
                                          .Elements(w + "pPr")
                                          .Elements(w + "rPr")
                                          .Elements(w + "sz")
                                          .FirstOrDefault()
                             let l_paragraph_firstrunsz = l_paragraph
                                          .Elements(w + "r")
                                          .Elements(w + "rPr")
                                          .Elements(w + "sz")
                                          .FirstOrDefault()
                             let l_paragraph_bullet = l_paragraph
                                          .Elements(w + "pPr")
                                          .Elements(w + "numPr")
                                          .Elements(w + "ilvl")
                                          .FirstOrDefault()
                             let l_paragraph_bullet_id = l_paragraph // Bsk
                                          .Elements(w + "pPr")
                                          .Elements(w + "numPr")
                                          .Elements(w + "numId")
                                          .FirstOrDefault()
                             let l_paragraph_firstrunshaded = l_paragraph
                                          .Elements(w + "r")
                                          .Elements(w + "rPr")
                                          .Elements(w + "shd")
                                          .FirstOrDefault()
                             let l_paragraph_shaded = l_paragraph
                                           .Elements(w + "pPr")
                                           .Elements(w + "shd")
                                           .FirstOrDefault()
                             select new
                             {
                                 ParagraphElement = l_paragraph,
                                 //CssClass = l_paragraph_styleNode != null ? ((string)l_paragraph_styleNode.Attribute(w + "val")).Replace(" ", "") : defaultStyle,
                                 //CssStyles = l_paragraph_inlineStyleNode != null ? CreateCssProperties(l_paragraph_inlineStyleNode.CreateReader()) : "",
                                 CssStyles = "",
                                 CssClass = "",
                                 ParagraphSize = l_paragraph_sz != null ? l_paragraph_sz.Attribute(w + "val").Value.ToString() : "",
                                 FirstRunSize = l_paragraph_firstrunsz != null ? l_paragraph_firstrunsz.Attribute(w + "val").Value.ToString() : "",
                                 IsBullet = l_paragraph_bullet != null ? true : false,
                                 BulletLevel = l_paragraph_bullet != null ? l_paragraph_bullet.Attribute(w + "val").Value.ToString() : "0",
                                 BulletId = l_paragraph_bullet_id != null ? l_paragraph_bullet_id.Attribute(w + "val").Value.ToString() : "0",// Bsk
                                 FirstRunShaded = l_paragraph_firstrunshaded != null ? true : false,
                                 IsShaded = l_paragraph_shaded != null ? true : false,
                                 Runs = l_paragraph.Elements().Where(z => z.Name == w_r || z.Name == w_ins || z.Name == w_hyperlink)
                             };


            //int iSectionCount = 0;
            //bool lInsideSection = false;
            int iCurrentBulletLevel = 0;
            int iCurrentBulletId = 0;
            bool lInsideBulletGroup = false;
            bool lParagraphIsSectionHeading = false;
            //bool lOverviewHeadingFound = false;


            //foreach (var paragraph in paragraphs)
            //{
            //    // if we are inside a bullet group and this paragraph is not a bullet, close the <ul> tags
            //    // all the way back, in case we're in a nested bullet situation
            //    // NOTE: iCurrentBulletLevel is zero-based, thus the +1 in the for loop setup
            //    if (lInsideBulletGroup == true & paragraph.IsBullet == false)
            //    {
            //        for (int i = iCurrentBulletLevel + 1; i > 0; i--)
            //        {
            //            sb.AppendLine("</ul>");
            //        }
            //        // we have now cleared any outstanding bullet groups, so reset flags
            //        lInsideBulletGroup = false;
            //        iCurrentBulletLevel = 0;
            //    }

            //    // If this is a bullet and the level is lower than the previous bullet, close the <ul> tag
            //    if (paragraph.IsBullet == true & Int32.Parse(paragraph.BulletLevel.ToString()) < iCurrentBulletLevel)
            //    {
            //        sb.AppendLine("</ul>");
            //        iCurrentBulletLevel = Int32.Parse(paragraph.BulletLevel.ToString());
            //    }



            //    // if this is a bullet and we are not inside a bullet group or if this is a bullet
            //    // and its level is larger than the previous one, start one with new <ul> tag
            //    if ((paragraph.IsBullet == true & lInsideBulletGroup == false) ||
            //        (paragraph.IsBullet == true & Int32.Parse(paragraph.BulletLevel.ToString()) > iCurrentBulletLevel))
            //    {
            //        sb.AppendLine("<ul>");
            //        lInsideBulletGroup = true;
            //        iCurrentBulletLevel = Int32.Parse(paragraph.BulletLevel.ToString());
            //    }

            //**********Start ****** //Bsk***************************

            int iParagraphCounter = 0;
            Stack sBulletLevelStack = new Stack(32);
            Stack sBulletIdStack = new Stack(32);

            // Write paragraphs
            foreach (var paragraph in paragraphs)
            {
                // for debugging, increment a paragraph counter
                iParagraphCounter++;

                // if we are inside a bullet group and this paragraph has NO runs, jump over it
                // in case it is a "phantom" non-bullet paragraph embedded into a bullet section
                // This means that there can be no blank lines between second-or higher-level indented
                // bullet elements, but that should not be a problem at all.  Otherwise,
                // our logic for closing out all open bullet groups will be fooled.
                if (lInsideBulletGroup == true & (int)paragraph.Runs.Count() == 0)
                    continue;

                // if we are inside a bullet group and this paragraph is not a bullet, close the <ul> tags
                // all the way back, in case we're in a nested bullet situation
                if (lInsideBulletGroup == true & paragraph.IsBullet == false)
                {
                    //for (int i = iCurrentBulletLevel + 1; i > 0; i--)
                    for (int i = sBulletLevelStack.Count; i > 0; i--)
                    {
                        sb.AppendLine("</ul>");
                    }
                    // we have now cleared any outstanding bullet groups, so reset flags
                    // and clear the bullet level stack
                    lInsideBulletGroup = false;
                    iCurrentBulletLevel = 0;
                    iCurrentBulletId = 0;
                    sBulletLevelStack.Clear();
                    sBulletIdStack.Clear();
                }

                // If this is a bullet and the level is lower than the previous bullet, close the <ul> tag(s)
                if (
                    paragraph.IsBullet == true &
                    ((Int32.Parse(paragraph.BulletLevel.ToString()) < iCurrentBulletLevel) ||
                      (Int32.Parse(paragraph.BulletLevel.ToString()) == iCurrentBulletLevel &
                       Int32.Parse(paragraph.BulletId.ToString()) < iCurrentBulletId)
                    )
                    )
                {
                    int iNewBulletLevel = Int32.Parse(paragraph.BulletLevel.ToString());
                    int iNewBulletId = Int32.Parse(paragraph.BulletId.ToString());
                    // we might have several levels that we are skipping backwards, 
                    // but they are not always sequentially numbered, so we have to refer
                    // to the sBulletLevelStack/sBulleteIdStack combination to be sure
                    // we close each one to end up formatted at the correct level
                    int iActualBulletLevel = (int)sBulletLevelStack.Pop();
                    int iActualBulletId = (int)sBulletIdStack.Pop();
                    while
                    (
                    ((iActualBulletLevel > iNewBulletLevel) ||
                      (iActualBulletLevel == iNewBulletLevel & iActualBulletId > iNewBulletId))
                    )
                    {
                        sb.AppendLine("</ul>");
                        iActualBulletLevel = (int)sBulletLevelStack.Peek();
                        iActualBulletId = (int)sBulletIdStack.Peek();
                        if ((iActualBulletLevel > iNewBulletLevel) ||
                             (iActualBulletLevel == iNewBulletLevel & iActualBulletId > iNewBulletId))
                        {
                            iActualBulletLevel = (int)sBulletLevelStack.Pop();
                            iActualBulletId = (int)sBulletIdStack.Pop();
                        }
                    }

                    // we have now cleared any bullet groups back to the new one for this paragraph,
                    // so set the current bullet level to the level for this paragraph
                    iCurrentBulletLevel = iNewBulletLevel;
                    iCurrentBulletId = iNewBulletId;
                }


                // if this is a bullet and we are not inside a bullet group or if this is a bullet
                // and its level is larger than the previous one, start one with new <ul> tag
                if (
                    (paragraph.IsBullet == true & lInsideBulletGroup == false) ||
                    (paragraph.IsBullet == true & Int32.Parse(paragraph.BulletLevel.ToString()) > iCurrentBulletLevel) ||
                    (paragraph.IsBullet == true & Int32.Parse(paragraph.BulletLevel.ToString()) == iCurrentBulletLevel &
                    Int32.Parse(paragraph.BulletId.ToString()) > iCurrentBulletId)
                   )
                {
                    sb.AppendLine("<ul>");
                    lInsideBulletGroup = true;
                    iCurrentBulletLevel = Int32.Parse(paragraph.BulletLevel.ToString());
                    iCurrentBulletId = Int32.Parse(paragraph.BulletId.ToString());
                    sBulletLevelStack.Push(iCurrentBulletLevel);
                    sBulletIdStack.Push(iCurrentBulletId);
                }

                //**********End ******//Bsk Check***************************

                // sz=28 (14 pt) means a section heading (some are also 24 (12 pt)
                // NOTE: workaround for some bullets that are 12 pt
                if ((paragraph.ParagraphSize == "28" || paragraph.ParagraphSize == "24" ||
                      paragraph.FirstRunSize == "28" || paragraph.FirstRunSize == "24") &
                    (paragraph.IsBullet == false &&
                    (paragraph.FirstRunShaded == true || paragraph.IsShaded == true)))
                {
                    // flag that this paragraph is a section heading (needed for
                    // soft CR workaround in the runs section
                    lParagraphIsSectionHeading = true;

                    // increment the section count and start the section and parag with sectionhead class
                    // NOTE: We keep it at zero for the first "section" which is really the overall 
                    // title heading that we are throwing away
                    sb.AppendLine("<p class='textbook_sectionhead'>");

                }
                else
                {
                    lParagraphIsSectionHeading = false;

                    // if this is a bullet, add the <li> tag
                    if (paragraph.IsBullet == true)
                    {
                        sb.AppendLine("<li>");
                    }
                    else
                    {
                        sb.AppendLine("<p>");
                    }
                }
                //                  sb.Append(String.Format("<p class=\"{0}\" style=\"{1}\">", paragraph.CssClass, paragraph.CssStyles));


                // NOTE: Because we might have a paragraph with NO runs, we must check for valid Runs property
                if (paragraph.Runs != null)
                {
                    // NOTE: In this version, we are not rendering hyperlinks as hyperlinks, but
                    // are using the text of the hyperlink as plain text
                    // This was to work around a reference problem and also because the textbook
                    // topics should not have any hyperlinks in them (there were some words in the
                    // bibliography that had "hidden" hyperlinks embedded into them (did not show visually))
                    var runs = from l_run in paragraph.Runs
                               let l_run_graphics = l_run
                                           .Elements(w + "drawing")
                               let l_run_pictures = l_run
                                           .Elements(w + "pict")


                               //Bsk
                               //let l_run_styleNode = l_run
                               //.Elements(w + "rPr")
                               //.FirstOrDefault()
                               //let l_run_inheritStyle = l_run
                               //            .Elements(w + "rPr")
                               //            .Elements(w + "rStyle")
                               //            .FirstOrDefault()
                               //let l_run_graphics = l_run
                               //            .Elements(w + "drawing")
                               //let l_run_bold = l_run
                               //            .Elements(w + "rPr")
                               //            .Elements(w + "b")
                               //            .FirstOrDefault()
                               select new
                               {
                                   Run = l_run,
                                   RunType = (l_run.Name.LocalName == "hyperlink" ? "run" : l_run.Name.LocalName),//Bsk
                                   CssStyles = "",
                                   CssClass = "",
                                   //IsBold = l_run.Element(w + "rPr").Element(w + "b") != null ? true : false,
                                   ////                                 IsBold = l_run_bold != null ? true : false,
                                   IsBold = l_run.Descendants(w + "b").FirstOrDefault() != null ? true : false,
                                   //Text = l_run.Descendants(w + "t").StringConcatenate(element => (string)element),
                                   Text = l_run.Descendants(w + "t") != null ? l_run.Descendants(w + "t").StringConcatenate(element => (string)element) : "", //Bsk
                                   //BreakBefore = l_run.Element(w + "br") != null,
                                   BreakBefore = l_run.Descendants(w + "br").FirstOrDefault() != null,
                                   //ExternalRelationId = (l_run.Name == w_hyperlink ? (string)l_run.Attribute(rels + "id") : ""),//Bsk
                                   Graphics = l_run_graphics,
                                   Pictures = l_run_pictures //Bsk
                               };


                    //bool lBoldStarted = false;

                    // Write runs
                    foreach (var run in runs)
                    {
                        // Break before?
                        if (run.BreakBefore)
                        {

                            if (lParagraphIsSectionHeading == true)
                            {
                                // workaround for some section headings that don't have a hard
                                // carriage return (paragraph end) but instead have soft CR
                                // In those cases, we must forcibly end the paragraph here
                                // and start another one.
                                sb.Append("</p>");
                                sb.AppendLine("<p>");
                                lParagraphIsSectionHeading = false;
                            }
                            else
                            {
                                sb.AppendLine("<br />");
                            }
                        }
                        // Write run
                        //sb.Append(
                        //    String.Format("<span class=\"{0}\" style=\"{1}\">",
                        //         run.CssClass,
                        //         run.CssStyles ?? ""
                        //     )
                        // );

                        // see if we need to turn bold on or off
                        if (run.IsBold == true)
                        {
                            sb.Append("<b>");
                        }


                        //// Is it an hyperlink? //Bsk
                        //if (run.RunType == "hyperlink")
                        //{
                        //    ExternalRelationship relation = (from rel in document.MainDocumentPart.ExternalRelationships
                        //                                     where rel.Id == run.ExternalRelationId
                        //                                     select rel).FirstOrDefault() as ExternalRelationship;
                        //    if (relation != null)
                        //    {
                        //        sb.Append("<a href=\"" + relation.Uri.ToString() + "\">");
                        //    }
                        //}

                        //// Fetch graphics
                        //var graphics = from l_graphic in run.Graphics
                        //               let l_graphic_blip = l_graphic
                        //                         .Descendants(a + "blip")
                        //                         .Where(x => x.Attribute(rels + "embed") != null)
                        //                         .FirstOrDefault()
                        //               let l_graphic_extent = l_graphic
                        //                         .Descendants(wp + "extent")
                        //                         .FirstOrDefault()
                        //               select new
                        //               {
                        //                   Width = l_graphic_extent != null ? EmuUnit.EmuToPixels((int)l_graphic_extent.Attribute("cx")) : 0,
                        //                   Height = l_graphic_extent != null ? EmuUnit.EmuToPixels((int)l_graphic_extent.Attribute("cy")) : 0,
                        //                   ExternalRelationId = l_graphic_blip != null ? (string)l_graphic_blip.Attribute(rels + "embed") : ""
                        //               };

                        //// Write graphics
                        //foreach (var graphic in graphics)
                        //{
                        //    // Write graphic
                        //    sb.Append(
                        //        String.Format("<div style=\"float:left; clear:none; width:{1}px; height:{2}px; margin:5px;\"><img src=\"{0}\" width=\"{1}\" height=\"{2}\"/></div>",
                        //            RenderInternalRelationshipLink(context, graphic.ExternalRelationId),
                        //            graphic.Width,
                        //            graphic.Height
                        //        )
                        //    );
                        //}

                        // Write text
                        sb.Append(RenderPlainText(run.Text));


                        //// End hyperlink //Bsk
                        //if (run.RunType == "hyperlink")
                        //{
                        //    sb.Append("</a>");
                        //}

                        // End run
                        if (run.IsBold == true)
                        {
                            sb.Append("</b>");
                        }
                        //sb.Append("</span>");
                    }


                }


                if (paragraph.IsBullet == true)
                {
                    sb.Append("</li>");
                }
                else
                {
                    sb.Append("</p>");
                }

                sb.AppendLine();
            }

            document.Close();

            // at this point, the only thing remaining in the sb is the last section that we don't want
            // so we don't need to save the sb to the mastersb at this point
            return sb.ToString();
        }

        /// <summary>
        /// Extracts images from DOCX file and saves to folder
        /// </summary>
        public static int ExtractImagesFromCommonDOCX(string cFileName, string cFolderName, string cPhysicalAppPath)
        {
            // Open document
            WordprocessingDocument document =
                WordprocessingDocument.Open(cFileName, false);

            XNamespace w = WordProcessingMLNamespace;
            XNamespace r = RelationshipsNamespace;
            XNamespace a = DrawingMLNamespace;
            XNamespace wp = WordprocessingDrawingNamespace;
            XNamespace v = VMLNamespace;

            int iImageCount = 0;

            //List<ImagePart> imageParts = new List<ImagePart>();
            //document.MainDocumentPart.GetPartsOfType<ImagePart>(imageParts);

            foreach (ImagePart imagePart in document.MainDocumentPart.ImageParts)
            {
                //string targetPath = cPhysicalAppPath + @"\docx\" + cFolderName + @"\" +
                string targetPath = cPhysicalAppPath + @"\" + cFolderName + @"\" + //Bsk
                    Path.GetFileName(imagePart.Uri.ToString());
                // first be sure the image is not already there
                // if so, get rid of it
                if (File.Exists(targetPath))
                    File.Delete(targetPath);

                Stream source = imagePart.GetStream();
                Stream destination = File.OpenWrite(targetPath);
                byte[] buffer = new byte[0x1000];
                int read;
                while ((read = source.Read(buffer, 0, buffer.Length)) > 0)
                {
                    destination.Write(buffer, 0, read);
                }
                destination.Close();
                iImageCount++;
            }

            document.Close();
            return iImageCount;
        }   
        /// <summary>
        /// Extracts images from DOCX file and saves to folder
        /// </summary>
        public static int ExtractImagesFromDOCX(string cFileName, string cFolderName, string cPhysicalAppPath)
        {
            // Open document
            WordprocessingDocument document =
                WordprocessingDocument.Open(cFileName, false);

            XNamespace w = WordProcessingMLNamespace;
            XNamespace r = RelationshipsNamespace;
            XNamespace a = DrawingMLNamespace;
            XNamespace wp = WordprocessingDrawingNamespace;
            XNamespace v = VMLNamespace;

            int iImageCount = 0;

            //List<ImagePart> imageParts = new List<ImagePart>();
            //document.MainDocumentPart.GetPartsOfType<ImagePart>(imageParts);

            foreach (ImagePart imagePart in document.MainDocumentPart.ImageParts)
            {
                //string targetPath = cPhysicalAppPath + @"\" + cFolderName + @"\" +
                string targetPath = cPhysicalAppPath + @"\docx\" + cFolderName + @"\" + //Bsk
                    Path.GetFileName(imagePart.Uri.ToString());
                // first be sure the image is not already there
                // if so, get rid of it
                if (File.Exists(targetPath))
                    File.Delete(targetPath);

                Stream source = imagePart.GetStream();
                Stream destination = File.OpenWrite(targetPath);
                byte[] buffer = new byte[0x1000];
                int read;
                while ((read = source.Read(buffer, 0, buffer.Length)) > 0)
                {
                    destination.Write(buffer, 0, read);
                }
                destination.Close();
                iImageCount++;
            }

            document.Close();
            return iImageCount;
        }


        /// <summary>
        /// Render plain text
        /// </summary>
        /// <param name="context">Current http context</param>
        /// <param name="text">Text to render</param>
        /// <returns>Rendered text</returns>
        private static string RenderPlainText(string text)
        {
            return System.Web.HttpUtility.HtmlEncode(text);
        }

    }

    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Concatenate strings
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="source">Source</param>
        /// <param name="func">Function delegate</param>
        /// <returns>Concatenated string</returns>
        public static string StringConcatenate<T>(this IEnumerable<T> source, Func<T, string> func)
        {
            StringBuilder sb = new StringBuilder();
            foreach (T item in source)
                sb.Append(func(item));
            return sb.ToString();
        }
    }


}
