﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.IO;
using PearlsReview.Constants;



namespace GHG.Controls
{

    /// <summary>
    /// Summary description for ghgreference to a 
    /// </summary>
    public class ImageUploader : CompositeControl
    {
        private class NewnameTextBox : TextBox
        {
            protected override void Render(HtmlTextWriter writer)
            {
                writer.Write("Rename as:<br>");
                base.Render(writer);
            }
        }
        public string CommandName
        {
            get
            {
                EnsureChildControls();
                return uploadbtn.CommandName;
            }
            set
            {
                EnsureChildControls();
                uploadbtn.CommandName = value;
                Proceed.CommandName = value;

            }
        }
        public string CommandArgument
        {
            get
            {
                EnsureChildControls();
                return uploadbtn.CommandArgument;
            }
            set
            {
                EnsureChildControls();
                uploadbtn.CommandArgument = value;
                Proceed.CommandArgument = value;
            }
        }
        public event EventHandler UploadClick;
        public event CommandEventHandler UploadCommand;

        public string UploadPath { get; set; }

        public  string ServerPath
        {
            get
            {
                return ValCode == ValidationConstants.V_Pass ? serverpath + filename : "";
            }
            set
            {
                ValCode = ValidationConstants.V_Fail;
            }
        }

        public string UploadText
        {
            get 
            {
                EnsureChildControls();
                return uploadbtn.Text;
            }
            set 
            {
                EnsureChildControls();
                uploadbtn.Text = value;
            }
        }
        
        private string serverpath
        {
            get { return (string)ViewState["serverpath"]; }
            set { ViewState["serverpath"] = value; }
        }
        private string fulluploadpath
        {
            get { return (string)ViewState["fulluploadpath"]; }
            set { ViewState["fulluploadpath"] = value; }
        }
        private string fileextension
        {
            get { return (string)ViewState["fileextension"]; }
            set { ViewState["fileextension"] = value; }
        }
        private string filename
        {
            get { return (string)ViewState["filename"]; }
            set { ViewState["filename"] = value; }
        }

        private String fileID
        {
            get
            {
                if (HttpContext.Current.Session[this.ClientID + "_PostedfileGUID"] != null)
                {
                    return (HttpContext.Current.Session[this.ClientID + "_PostedfileGUID"] as String);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Session[this.ClientID + "_PostedfileGUID"] = value;
            }
        }
    

        private Button uploadbtn;
        private FileUpload uploader;
        private Label valMessage;
        private Label Instructions;
        private Panel optionsPanel;
        private RadioButtonList options;
        private Panel uploadpanel;
        private Panel ViewPanel;
        private Image existingImg;
        private NewnameTextBox renametext;
        private Button Proceed;
        private Button Cancel;
        private Int32 ValCode = ValidationConstants.V_Fail;

        private const int Replace = 1;
        private const int Use = 2;
        private const int Rename = 3;
        private static readonly Dictionary<int, string> RadioOptions =
            new Dictionary<int, string>()
            {
            {Replace,"Replace Existing"},
            {Use,"Use Existing"},
            {Rename, "Rename File"}
            };


        public virtual void OnUploadCommand(CommandEventArgs e)
        {
            if (UploadCommand != null)
            {
                UploadCommand(this,e);
            }
            base.RaiseBubbleEvent(this, e);
        }
        public virtual void OnUploadClick(EventArgs e)
        {
            if (UploadClick != null)
            {
                UploadClick(this, e);
            }
        }
        protected override void Render(HtmlTextWriter writer)
        {
            AddAttributesToRender(writer);
            this.RenderBeginTag(writer);
            writer.Write("<table id='table_{0}' ><tr align='left'><td  colspan='2'>", this.ClientID);
            uploadpanel.RenderControl(writer);
            writer.Write("</td></tr><tr align='left' ><td width='30%'>");
            optionsPanel.RenderControl(writer);
            writer.Write("</td><td>");
            ViewPanel.RenderControl(writer);
            writer.Write("</td></tr></table>");
        }


        protected override void CreateChildControls()
        {
            uploadpanel = new Panel();
            uploadbtn = new Button();
            uploader = new FileUpload();
            valMessage = new Label();
            optionsPanel = new Panel();
            options = new RadioButtonList();
            existingImg = new Image();
            renametext = new NewnameTextBox();
            ViewPanel = new Panel();
            Instructions = new Label();
            Proceed = new Button();
            Cancel = new Button();

            base.CreateChildControls();
            Controls.Add(uploadpanel);
            Controls.Add(optionsPanel);
            Controls.Add(ViewPanel);
            uploadpanel.Controls.Add(uploader);
            uploadpanel.Controls.Add(uploadbtn);
            uploadpanel.Controls.Add(valMessage);
            optionsPanel.Controls.Add(Instructions);
            optionsPanel.Controls.Add(options);
            optionsPanel.Controls.Add(Cancel);
            optionsPanel.Controls.Add(Proceed);
            ViewPanel.Controls.Add(existingImg);
            ViewPanel.Controls.Add(renametext);

            options.CssClass = "FormViewradio";
            existingImg.Visible = false;
            renametext.Visible = false;
            optionsPanel.Visible = false;
            ViewPanel.Visible = false;
            valMessage.Text = "";
            existingImg.Height = Unit.Pixel(100);
            valMessage.ForeColor = System.Drawing.Color.Red;

            Instructions.Attributes.Add("style", "white-space:nowrap");
            Instructions.Text = "Choose an option and Click Proceed<br/>";
            uploadbtn.Text = "Attach Image";
            options.Items.Add(new ListItem(RadioOptions[Replace], Replace.ToString()));
            options.Items.Add(new ListItem(RadioOptions[Use], Use.ToString()));
            options.Items.Add(new ListItem(RadioOptions[Rename], Rename.ToString()));
            options.AutoPostBack = true;
            Proceed.Text = "Proceed";
            Cancel.Text = "Cancel";            
            options.TextChanged +=new EventHandler(options_TextChanged);
            uploadbtn.Command +=new CommandEventHandler(uploadbtn_Command);
            uploadbtn.Click += new EventHandler(uploadbtn_Click);
            Proceed.Click += new EventHandler(uploadbtn_Click);
            Proceed.Command += new CommandEventHandler(uploadbtn_Command);
            //SetPaths();
            Cancel.Click +=new EventHandler(Cancel_Click);
            
        }

        private void SetPaths()
        {
            if (uploader.HasFile)
            {
                filename = uploader.PostedFile.FileName;
                fulluploadpath = HttpContext.Current.Request.MapPath(UploadPath) + filename;
                fileextension = Path.GetExtension(fulluploadpath);
                serverpath = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + UploadPath;
                uploader.Enabled = false;
            }
            else if(String.IsNullOrEmpty(fileID))
            {
                filename = null;
                fulluploadpath = null;
                fileextension = null;
                serverpath = null;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            //Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }


        //protected override object SaveControlState()
        //{            
        //    if (uploader != null)
        //    {
        //        if (uploader.HasFile)
        //            HttpContext.Current.Session["ImageUploader_Postedfile"] = uploader.PostedFile. as object;
        //        else if (hiddenfile != null)
        //            HttpContext.Current.Session["ImageUploader_Postedfile"] = hiddenfile;
        //    }
        //    return base.SaveControlState();
        //}

        //protected override void LoadControlState(object savedState)
        //{            
        //    base.LoadControlState(savedState);
        //    hiddenfile = (HttpContext.Current.Session["ImageUploader_Postedfile"] as HttpPostedFile);
        //    HttpContext.Current.Session.Remove("ImageUploader_Postedfile");
        //}
        protected override object SaveControlState()
        {
            Object[] o = new Object[2];
            o[0] = base.SaveControlState();
            if(!string.IsNullOrEmpty(UploadPath))
            o[1] = UploadPath;
            return o;
        }
        protected override void LoadControlState(object savedState)
        {
            object[] o = (savedState as object[]);
            if (o[1]!=null)
            {
                UploadPath = (o[1] as String).ToString();
            }
            base.LoadControlState(o[0]);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);           
        }

        private void options_TextChanged(object sender, EventArgs e)
        {
            
            ViewPanel.Visible = true;            
            switch (Int32.Parse(options.SelectedValue))
            {
                case Replace:
                    existingImg.ImageUrl = serverpath+ filename;                    
                    existingImg.Visible = true;
                    renametext.Visible = false;
                    break;
                case Use:
                    existingImg.ImageUrl = serverpath + filename;
                    existingImg.Visible = true;
                    renametext.Visible = false;
                    break;
                case Rename:
                    existingImg.Visible = false;
                    renametext.Visible = true;
                    break;
                default:
                    break;
            }
        }
        

        private void UploadFile()
        {                  
            if (optionsPanel.Visible)
            {
                try
                {                    
                    switch (Int32.Parse(options.SelectedValue))
                    {
                        case Replace:
                            ValCode = ValidateandSaveImage(fulluploadpath, true);
                            existingImg.Visible = false;
                            ProcessValCode();
                            break;
                        case Use:
                            ValCode = ValidationConstants.V_Pass;                            
                            existingImg.Visible = false;
                            ProcessValCode();
                            break;
                        case Rename:
                            
                            if (!string.IsNullOrEmpty(renametext.Text))
                            {
                                filename = renametext.Text + Path.GetExtension(fulluploadpath);
                                ValCode = ValidateandSaveImage
                                    (HttpContext.Current.Request.MapPath(UploadPath)
                                    + filename , false);
                            }
                            else
                                ValCode = ValidationConstants.V_Fail;
                            if (ValCode == ValidationConstants.V_FileExists)
                            {
                                renametext.Visible = true;
                                options.SelectedIndex = Rename;
                            }
                            ProcessValCode();
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception)
                {

                }
            }
            else
            {
               ValCode = ValidateandSaveImage(fulluploadpath, false);
                ProcessValCode();
            }
        }
        void SettoDefaults()
        {
            if (options.SelectedItem != null)
                options.SelectedItem.Selected = false;
            optionsPanel.Visible = false;
            existingImg.Visible = false;
            renametext.Visible = false;
            ViewPanel.Visible = false;
            uploader.Enabled = true;
            
             if (File.Exists(HttpContext.Current.Server.MapPath(CommonConstants.TempImagesPath) + fileID + "." + fileextension)) ;
            {                
                DeleteTempFile();
            }
        }

        private void ProcessValCode()
        {
            valMessage.Text = ValidationConstants.ValidationMessages[ValCode];

            switch (ValCode)
            {
                case ValidationConstants.V_Pass:
                    SettoDefaults();
                    UploadPath = null;
                    fileID = null;
                    break;
                case ValidationConstants.V_CancelUpload:
                    SettoDefaults();
                    fileID = null;
                    break;
                case ValidationConstants.V_Fail:
                case ValidationConstants.V_NotImage: 
                    SettoDefaults();
                    fileID = null;
                    break;
                case ValidationConstants.V_FileExists:
                    optionsPanel.Visible = true;
                    ViewPanel.Visible = true;
                    break;
                default:
                    break;
            }            
        }
        private void Cancel_Click(object sender, EventArgs e)
        {
            ValCode = ValidationConstants.V_CancelUpload;
            ProcessValCode();
        }

        private void uploadbtn_Click(object sender, EventArgs e)
        {
            SetPaths();
            UploadFile();
            OnUploadClick(EventArgs.Empty);
        }

        private void uploadbtn_Command(object sender, CommandEventArgs e)
        {            
            OnUploadCommand(e);
        }
   

        //public int ValidateandSaveImage(HttpPostedFile cont, string SavePath, bool OverwriteexistingImage)
        //{
        //    Regex ImageRegex = new Regex(@"(.*?)\.(jpg|jpeg|png|gif)$", RegexOptions.IgnoreCase);
        //    int ValidationCode = 0;
        //    try
        //    {

        //        string fileextension = Path.GetExtension(SavePath);

        //        if (ImageRegex.IsMatch(fileextension))
        //        {
        //            if (File.Exists(SavePath) && OverwriteexistingImage == false)
        //            {
        //                ValidationCode = ValidationConstants.V_FileExists;
                        
        //            }
        //            else
        //            {
        //                ValidationCode = ValidationConstants.V_Pass;
        //                cont.SaveAs(SavePath);
        //            }
        //        }
        //        else
        //        {
        //            ValidationCode = ValidationConstants.V_NotImage;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        ValidationCode = ValidationConstants.V_Fail;
        //        //throw;
        //    }

        //    return ValidationCode;
        //}
        protected override void OnUnload(EventArgs e)
        {
            
                if (fileID != null && File.Exists(HttpContext.Current.Server.MapPath(CommonConstants.TempImagesPath) + fileID + "." + fileextension))
                {
                    DeleteTempFile();
                }
            base.OnUnload(e);
        }

        public int ValidateandSaveImage(string SavePath, bool OverwriteexistingImage)
        {
            
            Regex ImageRegex = new Regex(@"(.*?)\.(jpg|jpeg|png|gif)$", RegexOptions.IgnoreCase);
            int ValidationCode = 0;
            try
            {
                if (ImageRegex.IsMatch(fileextension))
                {
                    if (uploader.HasFile)
                    {
                        Guid ID = Guid.NewGuid();
                        fileID = ID.ToString();
                        uploader.PostedFile.SaveAs(HttpContext.Current.Server.MapPath(CommonConstants.TempImagesPath) + fileID + "." + fileextension);                
                    }
                    if (File.Exists(SavePath) && OverwriteexistingImage == false)
                    {
                        ValidationCode = ValidationConstants.V_FileExists;

                    }
                    else
                    {
                        SaveTempFile(SavePath);
                        ValidationCode = ValidationConstants.V_Pass;                        
                    }
                }
                else
                {
                    ValidationCode = ValidationConstants.V_NotImage;
                    ProcessValCode();
                }
            }
            catch (Exception)
            {
                ValidationCode = ValidationConstants.V_Fail;
                ProcessValCode();
                //throw;
            }

            return ValidationCode;
        }

        private void SaveTempFile(string SavePath)
        {
            if (File.Exists(HttpContext.Current.Server.MapPath(CommonConstants.TempImagesPath) + fileID + "." + fileextension)) ;
            {
                File.Copy(HttpContext.Current.Server.MapPath(CommonConstants.TempImagesPath) + fileID + "." + fileextension, SavePath, true);
                SettoDefaults();
            }
        }
        private void DeleteTempFile()
        {
            
            //File.Delete(HttpContext.Current.Server.MapPath(CommonConstants.TempImagesPath) + fileID + "." + fileextension);
        }

     
    }

}
