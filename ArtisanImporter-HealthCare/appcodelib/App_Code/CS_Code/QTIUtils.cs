﻿using System;
using System.Data;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using PearlsReview.QTI;

/// <summary>
/// Summary description for QTIUtils
/// </summary>

namespace PearlsReview.QTI
{
    public class QTIQuestionObject
    {
        private String _cQuestionID = "";
        public String cQuestionID
        {
            get {return _cQuestionID;}
            set {_cQuestionID = value;}
        }

        private String _cHeaderID = "";
        public String cHeaderID
        {
            get {return _cHeaderID;}
            set {_cHeaderID = value;}
        }

        private String _cHeaderName = "";
        public String cHeaderName
        {
            get {return _cHeaderName;}
            set {_cHeaderName = value;}
        }

        private String _cQuestionTitle = "";
        public String cQuestionTitle
        {
            get {return _cQuestionTitle;}
            set {_cQuestionTitle = value;}
        }

        private String _cResponseID = "";
        public String cResponseID
        {
            get {return _cResponseID;}
            set {_cResponseID = value;}
        }

        private String _cQuestionText = "";
        public String cQuestionText
        {
            get {return _cQuestionText;}
            set {_cQuestionText = value;}
        }

        private String _cAnswer_A_Text = "";
        public String cAnswer_A_Text
        {
            get {return _cAnswer_A_Text;}
            set {_cAnswer_A_Text = value;}
        }

        private String _cAnswer_B_Text = "";
        public String cAnswer_B_Text
        {
            get {return _cAnswer_B_Text;}
            set {_cAnswer_B_Text = value;}
        }

        private String _cAnswer_C_Text = "";
        public String cAnswer_C_Text
        {
            get {return _cAnswer_C_Text;}
            set {_cAnswer_C_Text = value;}
        }

        private String _cAnswer_D_Text = "";
        public String cAnswer_D_Text
        {
            get {return _cAnswer_D_Text;}
            set {_cAnswer_D_Text = value;}
        }

        private String _cAnswer_E_Text = "";
        public String cAnswer_E_Text
        {
            get {return _cAnswer_E_Text;}
            set {_cAnswer_E_Text = value;}
        }

        private String _cAnswer_F_Text = "";
        public String cAnswer_F_Text
        {
            get {return _cAnswer_F_Text;}
            set {_cAnswer_F_Text = value;}
        }

        private String _cCorrectAnswer = "";
        public String cCorrectAnswer
        {
            get {return _cCorrectAnswer;}
            set {_cCorrectAnswer = value;}
        }

        private String _cCorrectAnswerFeedback = "";
        public String cCorrectAnswerFeedback
        {
            get {return _cCorrectAnswerFeedback;}
            set {_cCorrectAnswerFeedback = value;}
        }

        private String _cUserAnswer = "";
        public String cUserAnswer
        {
            get {return _cUserAnswer;}
            set {_cUserAnswer = value;}
        }

        private int _cAnswer_A_Count = 0;
        public int cAnswerACount
        {
            get {return _cAnswer_A_Count;}
            set {_cAnswer_A_Count = value;}
        }

        private int _cAnswer_B_Count = 0;
        public int cAnswerBCount
        {
            get {return _cAnswer_B_Count;}
            set {_cAnswer_B_Count = value;}
        }

        private int _cAnswer_C_Count = 0;
        public int cAnswerCCount
        {
            get {return _cAnswer_C_Count;}
            set {_cAnswer_C_Count = value;}
        }

        private int _cAnswer_D_Count = 0;
        public int cAnswerDCount
        {
            get {return _cAnswer_D_Count;}
            set {_cAnswer_D_Count = value;}
        }

        private int _cAnswer_E_Count = 0;
        public int cAnswerECount
        {
            get {return _cAnswer_E_Count;}
            set {_cAnswer_E_Count = value;}
        }

        private int _cAnswer_F_Count = 0;
        public int cAnswerFCount
        {
            get {return _cAnswer_F_Count;}
            set {_cAnswer_F_Count = value;}
        }

        private int _cAnswer_Total_Count = 0;
        public int cAnswerTotalCount
        {
            get {return _cAnswer_Total_Count;}
            set {_cAnswer_Total_Count = value;}
        }
    }

    public class QTIUtils
    {
        public QTIUtils()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }

        //public QTIQuestionObject ConvertQTIQuestionXMLToQTIQuestionObject(String cQTIQuestionXML)
        //{
        //    QTIQuestionObject oQTIQuestion = new QTIQuestionObject();
        //    // parse out parts of the XML to populate properties of the QTIQuestionObject

        //    return oQTIQuestion;

        //}

        public XElement ConvertQTIQuestionObjectToQTIQuestionXML(QTIQuestionObject oQuestionObject)
        {
            // use XML literals to create QTI-formatted XML Question from Question object passed in
            // <item title=<%= "" + oQuestionObject.cQuestionTitle + "" %> ident=<%= "" + oQuestionObject.cQuestionID + "" %>>

            XElement cQTIQuestionXML = new XElement("item", 
                new XAttribute("title", oQuestionObject.cQuestionTitle),
                new XAttribute("ident", oQuestionObject.cQuestionID),
                new XAttribute("identHeader", oQuestionObject.cHeaderID),
                
                new XElement("presentation",
                    new XAttribute("label", oQuestionObject.cQuestionID),
                    new XElement("material", new XElement("mattext", CreateCDataSection(oQuestionObject.cQuestionText))),
                    new XElement("response_lid",
                        new XAttribute("ident", oQuestionObject.cResponseID),
                        new XAttribute("rcardinality", "Single"),
                        new XAttribute("rtiming", "No"),
                        new XElement("render_choice",
                            new XAttribute("shuffle", "No"),
                            new XElement("response_label", 
                                new XAttribute("ident", "A"),
                                new XElement("material", new XElement("mattext", CreateCDataSection(oQuestionObject.cAnswer_A_Text)))
                                ),
                            new XElement("response_label", 
                                new XAttribute("ident", "B"),
                                new XElement("material", new XElement("mattext", CreateCDataSection(oQuestionObject.cAnswer_B_Text)))
                                ),
                            new XElement("response_label", 
                                new XAttribute("ident", "C"),
                                new XElement("material", new XElement("mattext", CreateCDataSection(oQuestionObject.cAnswer_C_Text)))
                                ),
                            new XElement("response_label", 
                                new XAttribute("ident", "D"),
                                new XElement("material", new XElement("mattext", CreateCDataSection(oQuestionObject.cAnswer_D_Text)))
                                ),
                            new XElement("response_label", 
                                new XAttribute("ident", "E"),
                                new XElement("material", new XElement("mattext", CreateCDataSection(oQuestionObject.cAnswer_E_Text)))
                                ),
                            new XElement("response_label", 
                                new XAttribute("ident", "F"),
                                new XElement("material", new XElement("mattext", CreateCDataSection(oQuestionObject.cAnswer_F_Text)))
                                )
                            )
                        )
                    ),

                new XElement("resprocessing", 
                    new XElement("outcomes", 
                        new XElement("decvar",
                            new XAttribute("vartype", "Integer"),
                            new XAttribute("defaultval", "0")
                            )
                        ),
                    new XElement("respcondition",
                        new XAttribute("title", "Correct"),
                        new XElement("conditionvar", 
                            new XElement("varequal", 
                                new XAttribute("respident", oQuestionObject.cResponseID),
                                oQuestionObject.cCorrectAnswer
                                )
                            ),
                        new XElement("setvar",
                            new XAttribute("action", "Set"), 
                            1
                            ),
                        new XElement("displayfeedback",
                            new XAttribute("feedbacktype", "Response"),
                            new XAttribute("linkrefid", "Correct")
                        )
                    ),
                    new XElement("respcondition",
                        new XAttribute("title", "Incorrect"),
                        new XElement("conditionvar", 
                            new XElement("not",
                                new XElement("varequal", 
                                    new XAttribute("respident", oQuestionObject.cResponseID),
                                    oQuestionObject.cCorrectAnswer
                                    )
                                )
                            ),               
                        new XElement("setvar",
                            new XAttribute("action", "Set"), 
                            -1
                            )
                        )
                    ),

                new XElement("itemfeedback", 
                    new XAttribute("ident", "Correct"),
                    new XElement("material", 
                        new XElement("mattext", CreateCDataSection(oQuestionObject.cCorrectAnswerFeedback))
                        )
                    )                       
            );

            return cQTIQuestionXML;

            #region Original VB Code
            //Dim cQTIQuestionXML As XElement = _
            //<item title=<%= "" + oQuestionObject.cQuestionTitle + "" %> ident=<%= "" + oQuestionObject.cQuestionID + "" %> identHeader=<%= "" + oQuestionObject.cHeaderID + "" %>>
            //    <presentation label=<%= "" + oQuestionObject.cQuestionID + "" %>>
            //        <material>
            //            <mattext><%= CreateCDataSection(oQuestionObject.cQuestionText) %></mattext>
            //        </material>_
            //        <response_lid ident=<%= "" + oQuestionObject.cResponseID + "" %> rcardinality="Single" rtiming="No">
            //            <render_choice shuffle="No">
            //                <response_label ident="A">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_A_Text) %></mattext></material>
            //                </response_label>
            //                <response_label ident="B">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_B_Text) %></mattext></material>
            //                </response_label>
            //                <response_label ident="C">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_C_Text) %></mattext></material>
            //                </response_label>
            //                <response_label ident="D">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_D_Text) %></mattext></material>
            //                </response_label>
            //                <response_label ident="E">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_E_Text) %></mattext></material>
            //                </response_label>
            //                <response_label ident="F">
            //                    <material><mattext><%= CreateCDataSection(oQuestionObject.cAnswer_F_Text) %></mattext></material>
            //                </response_label>
            //            </render_choice>
            //        </response_lid>
            //    </presentation>
            //    <resprocessing>
            //        <outcomes>
            //            <decvar vartype="Integer" defaultval="0"/>
            //        </outcomes>
            //        <respcondition title="Correct">
            //            <conditionvar>
            //                <varequal respident=<%= "" + oQuestionObject.cResponseID + "" %>><%= oQuestionObject.cCorrectAnswer %></varequal>
            //            </conditionvar>
            //            <setvar action="Set">1</setvar>
            //            <displayfeedback feedbacktype="Response" linkrefid="Correct"/>
            //        </respcondition>
            //        <respcondition title="Incorrect">
            //            <conditionvar>
            //                <not><varequal respident=<%= "" + oQuestionObject.cResponseID + "" %>><%= oQuestionObject.cCorrectAnswer %></varequal></not>
            //            </conditionvar>
            //            <setvar action="Set">-1</setvar>
            //        </respcondition>
            //    </resprocessing>
            //    <itemfeedback ident="Correct">
            //        <material><mattext><%= CreateCDataSection(oQuestionObject.cCorrectAnswerFeedback) %></mattext></material>
            //    </itemfeedback>
            //</item>
            #endregion
        }

        public static String CreateCDataSection(String ContentText)
        {
            if (ContentText == null)
                return "";
            else
                return "<![CDATA[" + ContentText + "]]>";
        }

        public List<String> GetUserAnswersFromXML(String cXML)
        {
            List<String> AnswerList = new List<string>();
            if (cXML.Trim().Length > 0)
            {
                XDocument XAnswers = XDocument.Parse(cXML);
                var XAnswerList = from xAnswer in XAnswers.Descendants("answer")
                                  select xAnswer.Value.ToString();

                foreach (String XAnswer in XAnswerList)
                {
                    AnswerList.Add(XAnswer);
                }
            }

            return AnswerList;
        }

        public List<QTIQuestionObject> XConvertQTITestXMLToQTIQuestionObjectList(String cQTITestXML)
        {
            List<QTIQuestionObject> QTIQuestionList = new List<QTIQuestionObject>();
            // parse out parts of the XML to populate properties of the multiple QTIQuestionObjects
            XDocument XTest = XDocument.Parse(cQTITestXML);

            var QuestionList =
                from xQuestion in XTest.Descendants("item")
                select new QTIQuestionObject
                {
                    cQuestionID = xQuestion.Attribute("ident").Value,
                    cQuestionTitle = xQuestion.Attribute("title").Value,
                    cResponseID = xQuestion.Element("presentation").Element("response_lid").Attribute("ident").Value,
                    cQuestionText = xQuestion.Element("presentation").Element("material").Element("mattext").Value,
                    cAnswer_A_Text = (from xResponseLabel in xQuestion.Element("presentation").Element("response_lid").Element("render_choice").Elements("response_label")
                                      where xResponseLabel.Attribute("ident").Value == "A"
                                      select xResponseLabel.Element("material").Element("mattext").Value).ToList()[0],
                    cAnswer_B_Text = (from xResponseLabel in xQuestion.Element("presentation").Element("response_lid").Element("render_choice").Elements("response_label")
                                      where xResponseLabel.Attribute("ident").Value == "B"
                                      select xResponseLabel.Element("material").Element("mattext").Value).ToList()[0],
                    cAnswer_C_Text = (from xResponseLabel in xQuestion.Element("presentation").Element("response_lid").Element("render_choice").Elements("response_label")
                                      where xResponseLabel.Attribute("ident").Value == "C"
                                      select xResponseLabel.Element("material").Element("mattext").Value).ToList()[0],
                    cAnswer_D_Text = (from xResponseLabel in xQuestion.Element("presentation").Element("response_lid").Element("render_choice").Elements("response_label")
                                      where xResponseLabel.Attribute("ident").Value == "D"
                                      select xResponseLabel.Element("material").Element("mattext").Value).ToList()[0],
                    cAnswer_E_Text = (from xResponseLabel in xQuestion.Element("presentation").Element("response_lid").Element("render_choice").Elements("response_label")
                                      where xResponseLabel.Attribute("ident").Value == "E"
                                      select xResponseLabel.Element("material").Element("mattext").Value).ToList()[0],
                    cAnswer_F_Text = (from xResponseLabel in xQuestion.Element("presentation").Element("response_lid").Element("render_choice").Elements("response_label")
                                      where xResponseLabel.Attribute("ident").Value == "F"
                                      select xResponseLabel.Element("material").Element("mattext").Value).ToList()[0],
                    cCorrectAnswer = (from xRespCondition in xQuestion.Element("respprocessing").Elements("resp_condition")
                                      where xRespCondition.Attribute("title").Value == "Correct"
                                      select xRespCondition.Element("conditionvar").Element("varequal").Value).ToList()[0],
                    cCorrectAnswerFeedback = (from xItemFeedback in xQuestion.Elements("itemfeedback")
                                              where xItemFeedback.Attribute("ident").Value == "Correct"
                                              select xItemFeedback.Element("material").Element("mattext").Value).ToList()[0]
                };

            foreach (QTIQuestionObject Question in QuestionList)
                QTIQuestionList.Add(Question);

            return QTIQuestionList;
        }

        public List<QTIQuestionObject> ConvertQTITestXMLToQTIQuestionObjectList(String cQTITestXML)
        {
            List<QTIQuestionObject> QTIQuestionList = new List<QTIQuestionObject>();
            // parse out parts of the XML to populate properties of the multiple QTIQuestionObjects
            XDocument XTest = XDocument.Parse(cQTITestXML);

            var QuestionList = (from xQuestion in XTest.Descendants("item") 
                                select new
                                {
                                    QuestionText = xQuestion.Element("presentation").Element("material").Element("mattext").Value, 
                                    QuestionID = xQuestion.Element("presentation").Element("response_lid").Attribute("ident").Value, 
                                    HeaderID = xQuestion.Attribute("identHeader").Value, 
                                    AnswerOptions = (from xRespCondition in xQuestion.Element("resprocessing").Elements("respcondition")
                                                     where xRespCondition.Attribute("title").Value == "Correct"
                                                     select new
                                                     {
                                                        Answer_Option_Title = xRespCondition.Attribute("title").Value,
                                                        Correct_Answer = xRespCondition.Element("conditionvar").Element("varequal").Value
                                                     }), 
                                    Correct_Answer_Feedback = xQuestion.Element("itemfeedback").Element("material").Element("mattext").Value, 
                                    AnswerTexts = (from xResponse_Label in xQuestion.Element("presentation").Element("response_lid").Element("render_choice").Elements("response_label") 
                                        select new
                                        {
                                            AnswerTextOrdinal = xResponse_Label.Attribute("ident").Value, 
                                            AnswerText = xResponse_Label.Element("material").Element("mattext").Value
                                        })
                                });

            foreach (var Question in QuestionList)
            {
                QTIQuestionObject oQTIQuestion = new QTIQuestionObject();
                oQTIQuestion.cQuestionID = Question.QuestionID.ToString();
                if (Question.HeaderID == null || Question.HeaderID == "")
                    oQTIQuestion.cHeaderID = "0";
                else
                    oQTIQuestion.cHeaderID = Question.HeaderID.ToString();

                oQTIQuestion.cQuestionText = Question.QuestionText.ToString();


                foreach (var AnswerOption in Question.AnswerOptions)
                {
                    if (AnswerOption.Answer_Option_Title == "Correct")
                    {
                        oQTIQuestion.cCorrectAnswer = AnswerOption.Correct_Answer;
                    }
                }

                oQTIQuestion.cCorrectAnswerFeedback = Question.Correct_Answer_Feedback.ToString();

                foreach (var AnswerText in Question.AnswerTexts)
                {
                    if (AnswerText.AnswerTextOrdinal.ToString() == "A")
                        oQTIQuestion.cAnswer_A_Text = AnswerText.AnswerText.ToString();

                    if (AnswerText.AnswerTextOrdinal.ToString() == "B")
                        oQTIQuestion.cAnswer_B_Text = AnswerText.AnswerText.ToString();

                    if (AnswerText.AnswerTextOrdinal.ToString() == "C")
                        oQTIQuestion.cAnswer_C_Text = AnswerText.AnswerText.ToString();

                    if (AnswerText.AnswerTextOrdinal.ToString() == "D")
                        oQTIQuestion.cAnswer_D_Text = AnswerText.AnswerText.ToString();

                    if (AnswerText.AnswerTextOrdinal.ToString() == "E")
                        oQTIQuestion.cAnswer_E_Text = AnswerText.AnswerText.ToString();

                    if (AnswerText.AnswerTextOrdinal.ToString() == "F")
                        oQTIQuestion.cAnswer_F_Text = AnswerText.AnswerText.ToString();
                }

                QTIQuestionList.Add(oQTIQuestion);
            }


            return QTIQuestionList;
        }

        public String ConvertQTIQuestionObjectListToQTITestXMLString(List<QTIQuestionObject> QTIQuestionList)
        {
            // use XML literals to create QTI-formatted XML Question from Question object passed in
            XElement cQTITestXML = new XElement("questestinterop", 
                from QTIQuestionObject obj in QTIQuestionList 
                select ConvertQTIQuestionObjectToQTIQuestionXML(obj)
                );

            #region Origional VB Code
            //Dim cQTITestXML As XElement = _
            //<questestinterop>
            //    <%= From QTIQuestionObject As QTIQuestionObject In QTIQuestionList _
            //        Select _
            //        ConvertQTIQuestionObjectToQTIQuestionXML(QTIQuestionObject) %>
            //</questestinterop>
            #endregion

            String cQTITestXMLString = cQTITestXML.ToString();
            // fix up some problems with starting and ending < and > around CDATA sections
            // and some other items inside CDATA sections that are encoded when they should not be
            cQTITestXMLString = System.Web.HttpUtility.HtmlDecode(cQTITestXMLString);
            // once more to take care of some items that are "double-encoded"
            // (&amp;quot;) - the first htmldecode takes care of the &amp; but
            // then we need a second one to turn the resulting &quot; into a quote mark 
            cQTITestXMLString = System.Web.HttpUtility.HtmlDecode(cQTITestXMLString);
            return cQTITestXMLString;
        }
    }
}