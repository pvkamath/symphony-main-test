﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Fangbo Yang
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;


namespace PearlsReview.BLL
{
    public class UserTimerTrack : BasePR
    {
        public int userid { get; set; }
        public int topicid { get; set; }
        public int timebysec { get; set; }

        public static int GetUserTimerByTopicAndUser(int userid, int topicid)
        {
            return SiteProvider.PR2.GetUserTimerByTopicAndUser(userid, topicid);
        }

        public static void AddorUpdateUserTimerTrack(int userid, int topicid, int timebysec)
        {
            SiteProvider.PR2.AddorUpdateUserTimerTrack(userid, topicid, timebysec);
        }
    }
}