﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{   
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// UserAccount business object class
    /// </summary>
    public class UserAccount : BasePR
    {
        private int _iID = 0;
        public int iID
        {
            get { return _iID; }
            protected set { _iID = value; }
        }

        private string _cAddress1 = "";
        public string cAddress1
        {
            get { return _cAddress1; }
            set { _cAddress1 = value; }
        }

        private string _cAddress2 = "";
        public string cAddress2
        {
            get { return _cAddress2; }
            set { _cAddress2 = value; }
        }

        private bool _lAdmin = false;
        public bool lAdmin
        {
            get { return _lAdmin; }
            set { _lAdmin = value; }
        }

        private bool _lPrimary = false;
        public bool lPrimary
        {
            get { return _lPrimary; }
            set { _lPrimary = value; }
        }

        private string _cBirthDate = "";
        public string cBirthDate
        {
            get { return _cBirthDate; }
            set { _cBirthDate = value; }
        }

        private string _cCity = "";
        public string cCity
        {
            get { return _cCity; }
            set { _cCity = value; }
        }

        private string _cEmail = "";
        public string cEmail
        {
            get { return _cEmail; }
            set { _cEmail = value; }
        }

        private string _cExpires = "";
        public string cExpires
        {
            get { return _cExpires; }
            set { _cExpires = value; }
        }

        private string _cFirstName = "";
        public string cFirstName
        {
            get { return _cFirstName; }
            set { _cFirstName = value; }
        }

        private string _cFloridaNo = "";
        public string cFloridaNo
        {
            get { return _cFloridaNo; }
            set { _cFloridaNo = value; }
        }

        private string _cInstitute = "";
        public string cInstitute
        {
            get { return _cInstitute; }
            set { _cInstitute = value; }
        }

        private string _cLastName = "";
        public string cLastName
        {
            get { return _cLastName; }
            set { _cLastName = value; }
        }

        private string _cLectDate = "";
        public string cLectDate
        {
            get { return _cLectDate; }
            set { _cLectDate = value; }
        }

        private string _cMiddle = "";
        public string cMiddle
        {
            get { return _cMiddle; }
            set { _cMiddle = value; }
        }

        private string _cPhone = "";
        public string cPhone
        {
            get { return _cPhone; }
            set { _cPhone = value; }
        }

        private string _cPromoCode = "";
        public string cPromoCode
        {
            get { return _cPromoCode; }
            set { _cPromoCode = value; }
        }

        private int _PromoID = 0;
        public int PromoID
        {
            get { return _PromoID; }
            set { _PromoID = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }

        private string _cPW = "";
        public string cPW
        {
            get { return _cPW; }
            set { _cPW = value; }
        }

        private string _cRefCode = "";
        public string cRefCode
        {
            get { return _cRefCode; }
            set { _cRefCode = value; }
        }

        private string _cRegType = "";
        public string cRegType
        {
            get { return _cRegType; }
            set { _cRegType = value; }
        }

        private string _cSocial = "";
        public string cSocial
        {
            get { return _cSocial; }
            set { _cSocial = value; }
        }

        private string _cState = "";
        public string cState
        {
            get { return _cState; }
            set { _cState = value; }
        }

        private string _cUserName = "";
        public string cUserName
        {
            get { return _cUserName.ToLower(); }
            set { _cUserName = value.ToLower(); }
        }

        private string _cZipCode = "";
        public string cZipCode
        {
            get { return _cZipCode; }
            set { _cZipCode = value; }
        }

        private int _SpecID = 0;
        public int SpecID
        {
            get { return _SpecID; }
            set { _SpecID = value; }
        }

        private string _LCUserName = "";
        public string LCUserName
        {
            get { return _LCUserName.ToLower(); }
            set { _LCUserName = value.ToLower(); }
        }

        private DateTime _LastAct = System.DateTime.Now;
        public DateTime LastAct
        {
            get { return _LastAct; }
            set { _LastAct = value; }
        }

        private string _PassFmt = "";
        public string PassFmt
        {
            get { return _PassFmt; }
            set { _PassFmt = value; }
        }

        private string _PassSalt = "";
        public string PassSalt
        {
            get { return _PassSalt; }
            set { _PassSalt = value; }
        }

        private string _MobilePIN = "";
        public string MobilePIN
        {
            get { return _MobilePIN; }
            set { _MobilePIN = value; }
        }

        private string _LCEmail = "";
        public string LCEmail
        {
            get { return _LCEmail.ToLower(); }
            set { _LCEmail = value.ToLower(); }
        }

        private string _PWQuest = "";
        public string PWQuest
        {
            get { return _PWQuest; }
            set { _PWQuest = value; }
        }

        private string _PWAns = "";
        public string PWAns
        {
            get { return _PWAns.ToLower(); }
            set { _PWAns = value.ToLower(); }
        }

        private bool _IsApproved = false;
        public bool IsApproved
        {
            get { return _IsApproved; }
            set { _IsApproved = value; }
        }

        private bool _IsOnline = false;
        public bool IsOnline
        {
            get { return _IsOnline; }
            set { _IsOnline = value; }
        }

        private bool _IsLocked = false;
        public bool IsLocked
        {
            get { return _IsLocked; }
            set { _IsLocked = value; }
        }

        private DateTime _LastLogin = System.DateTime.MinValue;
        public DateTime LastLogin
        {
            get { return _LastLogin; }
            set { _LastLogin = value; }
        }

        private DateTime _LastPWChg = System.DateTime.MinValue;
        public DateTime LastPWChg
        {
            get { return _LastPWChg; }
            set { _LastPWChg = value; }
        }

        private DateTime _LastLock = System.DateTime.MinValue;
        public DateTime LastLock
        {
            get { return _LastLock; }
            set { _LastLock = value; }
        }

        private int _XPWAtt = 0;
        public int XPWAtt
        {
            get { return _XPWAtt; }
            set { _XPWAtt = value; }
        }

        private DateTime _XPWAttSt = System.DateTime.MinValue;
        public DateTime XPWAttSt
        {
            get { return _XPWAttSt; }
            set { _XPWAttSt = value; }
        }

        private int _XPWAnsAtt = 0;
        public int XPWAnsAtt
        {
            get { return _XPWAnsAtt; }
            set { _XPWAnsAtt = value; }
        }

        private DateTime _XPWAnsSt = System.DateTime.MinValue;
        public DateTime XPWAnsSt
        {
            get { return _XPWAnsSt; }
            set { _XPWAnsSt = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        private DateTime _Created = System.DateTime.Now;
        public DateTime Created
        {
            get { return _Created; }
            set { _Created = value; }
        }

        private int _AgeGroup = 0;
        public int AgeGroup
        {
            get { return _AgeGroup; }
            set { _AgeGroup = value; }
        }

        private int _RegTypeID = 0;
        public int RegTypeID
        {
            get { return _RegTypeID; }
            set { _RegTypeID = value; }
        }

        private int _RepID = 0;
        public int RepID
        {
            get { return _RepID; }
            set { _RepID = value; }
        }

        private string _Work_Phone = "";
        public string Work_Phone
        {
            get { return _Work_Phone; }
            set { _Work_Phone = value; }
        }
        private string _Badge_ID = "";
        public string Badge_ID
        {
            get { return _Badge_ID; }
            set { _Badge_ID = value; }
        }

        private DateTime _Hire_Date = System.DateTime.MinValue;
        public DateTime Hire_Date
        {
            get { return _Hire_Date; }
            set { _Hire_Date = value; }
        }

        private DateTime _Termin_Date = System.DateTime.MinValue;
        public DateTime Termin_Date
        {
            get { return _Termin_Date; }
            set { _Termin_Date = value; }
        }

        private int _Dept_ID = 0;
        public int Dept_ID
        {
            get { return _Dept_ID; }
            set { _Dept_ID = value; }
        }

        private int _Position_ID = 0;
        public int Position_ID
        {
            get { return _Position_ID; }
            set { _Position_ID = value; }
        }

        private int _Clinical_Ind = 0;
        public int Clinical_Ind
        {
            get { return _Clinical_Ind; }
            set { _Clinical_Ind = value; }
        }

        private string _Title = "";
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private bool _Giftcard_Ind = false;
        public bool Giftcard_Ind
        {
            get { return _Giftcard_Ind; }
            set { _Giftcard_Ind = value; }
        }

        private decimal _Giftcard_Chour = 0;
        public decimal Giftcard_Chour
        {
            get { return _Giftcard_Chour; }
            set { _Giftcard_Chour = value; }
        }

        private decimal _Giftcard_Uhour = 0;
        public decimal Giftcard_Uhour
        {
            get { return _Giftcard_Uhour; }
            set { _Giftcard_Uhour = value; }
        }

        private int _UniqueID = 0;
        public int UniqueID
        {
            get { return _UniqueID; }
            set { _UniqueID = value; }
        }

        private bool _FirstLogin_Ind = false;
        public bool FirstLogin_Ind
        {
            get { return _FirstLogin_Ind; }
            set { _FirstLogin_Ind = value; }
        }

        public UserAccount(int iID, string cAddress1, string cAddress2, bool lAdmin, bool lPrimary, string cBirthDate,
           string cCity, string cEmail, string cExpires, string cFirstName, string cFloridaNo,
           string cInstitute, string cLastName, string cLectDate, string cMiddle, string cPhone,
           string cPromoCode, int PromoID, int FacilityID, string cPW, string cRefCode,
           string cRegType, string cSocial, string cState, string cUserName, string cZipCode,
           int SpecID, string LCUserName, DateTime LastAct, string PassFmt,
           string PassSalt, string MobilePIN, string LCEmail, string PWQuest, string PWAns,
           bool IsApproved, bool IsOnline, bool IsLocked, DateTime LastLogin, DateTime LastPWChg,
           DateTime LastLock, int XPWAtt, DateTime XPWAttSt, int XPWAnsAtt, DateTime XPWAnsSt,
           string Comment, DateTime Created, int AgeGroup, int RegTypeID, int RepID, string Work_Phone, string Badge_ID,
           DateTime Hire_Date, DateTime Termin_Date, int Dept_ID, int Position_ID, int Clinical_Ind, string Title, bool Giftcard_Ind,
           decimal Giftcard_Chour, decimal Giftcard_Uhour, int UniqueID, bool FirstLogin_Ind)
        {
            this.iID = iID;
            this.cAddress1 = cAddress1;
            this.cAddress2 = cAddress2;
            this.lAdmin = lAdmin;
            this.lPrimary = lPrimary;
            this.cBirthDate = cBirthDate;
            this.cCity = cCity;
            this.cEmail = cEmail;
            this.cExpires = cExpires;
            this.cFirstName = cFirstName;
            this.cFloridaNo = cFloridaNo;
            this.cInstitute = cInstitute;
            this.cLastName = cLastName;
            this.cLectDate = cLectDate;
            this.cMiddle = cMiddle;
            this.cPhone = cPhone;
            this.cPromoCode = cPromoCode;
            this.PromoID = PromoID;
            this.FacilityID = FacilityID;
            this.cPW = cPW;
            this.cRefCode = cRefCode;
            this.cRegType = cRegType;
            this.cSocial = cSocial;
            this.cState = cState;
            this.cUserName = cUserName.ToLower();
            this.cZipCode = cZipCode;
            this.SpecID = SpecID;
            this.LCUserName = LCUserName.ToLower();
            this.LastAct = LastAct;
            this.PassFmt = PassFmt;
            this.PassSalt = PassSalt;
            this.MobilePIN = MobilePIN;
            this.LCEmail = LCEmail;
            this.PWQuest = PWQuest;
            this.PWAns = PWAns.ToLower();
            this.IsApproved = IsApproved;
            this.IsOnline = IsOnline;
            this.IsLocked = IsLocked;
            this.LastLogin = LastLogin;
            this.LastPWChg = LastPWChg;
            this.LastLock = LastLock;
            this.XPWAtt = XPWAtt;
            this.XPWAttSt = XPWAttSt;
            this.XPWAnsAtt = XPWAnsAtt;
            this.XPWAnsSt = XPWAnsSt;
            this.Comment = Comment;
            this.Created = Created;
            this.AgeGroup = AgeGroup;
            this.RegTypeID = RegTypeID;
            this.RepID = RepID;
            this.Work_Phone = Work_Phone;
            this.Badge_ID = Badge_ID;
            this.Hire_Date = Hire_Date;
            this.Termin_Date = Termin_Date;
            this.Dept_ID = Dept_ID;
            this.Position_ID = Position_ID;
            this.Clinical_Ind = Clinical_Ind;
            this.Title = Title;
            this.Giftcard_Ind = Giftcard_Ind;
            this.Giftcard_Chour = Giftcard_Chour;
            this.Giftcard_Uhour = Giftcard_Uhour;
            this.UniqueID = UniqueID;
            this.FirstLogin_Ind = FirstLogin_Ind;
        }

        public bool Delete()
        {
            bool success = UserAccount.DeleteUserAccount(this.iID);
            if (success)
                this.iID = 0;
            return success;
        }

        public bool Update()
        {
            return UserAccount.UpdateUserAccount(this.iID, this.cAddress1, this.cAddress2, this.lAdmin,
                    this.lPrimary, this.cBirthDate,
          this.cCity, this.cEmail, this.cExpires, this.cFirstName, this.cFloridaNo,
          this.cInstitute, this.cLastName, this.cLectDate, this.cMiddle, this.cPhone,
          this.cPromoCode, this.PromoID, this.FacilityID, this.cPW, this.cRefCode,
          this.cRegType, this.cSocial, this.cState, this.cUserName, this.cZipCode,
          this.SpecID, this.LCUserName, this.LastAct, this.PassFmt,
          this.PassSalt, this.MobilePIN, this.LCEmail, this.PWQuest, this.PWAns,
          this.IsApproved, this.IsOnline, this.IsLocked, this.LastLogin, this.LastPWChg,
          this.LastLock, this.XPWAtt, this.XPWAttSt, this.XPWAnsAtt, this.XPWAnsSt,
          this.Comment, this.Created, this.AgeGroup, this.RegTypeID, this.RepID,
          this.Work_Phone, this.Badge_ID, this.Hire_Date,
          this.Termin_Date, this.Dept_ID, this.Position_ID,
          this.Clinical_Ind, this.Title, this.Giftcard_Ind,
          this.Giftcard_Chour, this.Giftcard_Uhour,
          this.UniqueID, this.FirstLogin_Ind);
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all UserAccounts
        /// </summary>
        public static List<UserAccount> GetUserAccounts(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cUserName";

            List<UserAccount> UserAccounts = null;
            string key = "UserAccounts_UserAccounts_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo> recordset = SiteProvider.PR2.GetUserAccounts(cSortExpression);
                UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        public static void UpdateEmailByUserID(int userid, string email)
        {
            SiteProvider.PR2.UpdateEmailByUserID(userid, email);
        }

        public static bool UpdateSSNAndBirthDate(int UserID, int SSN, string BirthDate) 
        {
            return SiteProvider.PR2.UpdateSSNAndBirthDate(UserID, SSN, BirthDate);
        }
        public static bool UpdateSSN(int UserID, int SSN)
        {
            return SiteProvider.PR2.UpdateSSN(UserID, SSN);
        }
        public static string GetMgrNameByTestID(int testID)
        {
            return SiteProvider.PR2.GetMgrNameByTestID(testID);
        }


        /// <summary>
        /// Returns a collection with most recent UserAccounts
        /// </summary>
        public static List<UserAccount> GetRecentUserAccounts()
        {
            List<UserAccount> UserAccounts = null;
            string key = "UserAccounts_RecentUserAccounts";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo> recordset = SiteProvider.PR2.GetRecentUserAccounts();
                UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        /// <summary>
        /// Returns a collection with most recent UserAccounts
        /// </summary>
        public static int GetUserAccountByUniqueid(string uniqueid, string Password)
        {
            int userid = 0;
            //string key = "UserAccounts_RecentUserAccountsByUniqueid";

            userid = SiteProvider.PR2.GetUserAccountByUniqueid(uniqueid, Password);


            return userid;
        }

        /// <summary>
       
        /// </summary>
        public static UserAccount GetUserAccountByUniqueid(int uniqueid)
        {
            //string key = "UserAccounts_RecentUserAccountsByUniqueid";

            UserAccount useraccount = GetUserAccountFromUserAccountInfo(SiteProvider.PR2.GetUserAccountByUniqueid(uniqueid));
            
            return useraccount;
        }

        /// </summary>
        public static string encryptPassword(string password)
        {           
            string result  = SiteProvider.PR2.encrypt(password);
            return result;
        }

        /// <summary>
        /// Returns the number of total UserAccounts
        /// </summary>
        public static int GetUserAccountCount()
        {
            int UserAccountCount = 0;
            string key = "UserAccounts_UserAccountCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccountCount = (int)BizObject.Cache[key];
            }
            else
            {
                UserAccountCount = SiteProvider.PR2.GetUserAccountCount();
                BasePR.CacheData(key, UserAccountCount);
            }
            return UserAccountCount;
        }

        /// <summary>
        /// Returns the Integer ID (primary key) of a user by username lookup
        /// </summary>
        public static int GetUserIDByUserName(string UserName)
        {
            int UserID = 0;
            string key = "UserID_UserName_" + UserName.Trim();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserID = (int)BizObject.Cache[key];
            }
            else
            {
                UserID = SiteProvider.PR2.GetUserIDByUserName(UserName);
                BasePR.CacheData(key, UserID);
            }
            return UserID;
        }

        /// <summary>
        /// Returns the Integer ID (primary key) of a user by username lookup and FacilityID
        /// </summary>
        public static int GetUserIDByShortUserNameAndFacilityID(string ShortUserName, int FacilityID)
        {
            int UserID = 0;
            string key = "UserID_ShortUserName_" + ShortUserName.Trim() + "_FacilityID_" + FacilityID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserID = (int)BizObject.Cache[key];
            }
            else
            {
                UserID = SiteProvider.PR2.GetUserIDByShortUserNameAndFacilityID(ShortUserName, FacilityID);
                BasePR.CacheData(key, UserID);
            }
            return UserID;
        }


        /// <summary>
        /// Returns the Integer ID (primary key) of a user by username lookup and FacilityID
        /// </summary>
        public static int GetUserIDByShortUserName(string ShortUserName)
        {
            int UserID = 0;
            string key = "UserID_ShortUserName_" + ShortUserName.Trim();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserID = (int)BizObject.Cache[key];
            }
            else
            {
                UserID = SiteProvider.PR2.GetUserIDByShortUserName(ShortUserName);
                BasePR.CacheData(key, UserID);
            }
            return UserID;
        }

        /// <summary>
        /// Returns a UserAccount object with the specified ID
        /// </summary>
        public static UserAccount GetUserAccountByID(int iID)
        {
            UserAccount UserAccount = null;
            string key = "UserAccounts_UserAccount_" + iID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccount = (UserAccount)BizObject.Cache[key];
            }
            else
            {
                UserAccount = GetUserAccountFromUserAccountInfo(SiteProvider.PR2.GetUserAccountByID(iID));
                BasePR.CacheData(key, UserAccount);
            }
            return UserAccount;
        }

        /// Returns a UserAccount object with the specified username and facilityid
        /// </summary>
        public static UserAccount GetUserAccountByShortUserNameAndFacilityID(string ShortUserName, int FacilityID)
        {
            UserAccount UserAccount = null;
            string key = "UserAccounts_UserAccount_ShortUserName_" + ShortUserName.Trim() + "_FacilityID_" + FacilityID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccount = (UserAccount)BizObject.Cache[key];
            }
            else
            {
                UserAccount = GetUserAccountFromUserAccountInfo(SiteProvider.PR2.GetUserAccountByShortUserNameAndFacilityID(ShortUserName, FacilityID));
                BasePR.CacheData(key, UserAccount);
            }
            return UserAccount;
        }

        /// Returns a UserAccount object with the specified Email and facilityid
        /// </summary>
        public static UserAccount GetUserAccountByEmailAndFacid(string EmailAddress, int Facid)
        {
            UserAccount UserAccount = null;
            string key = "UserAccounts_UserAccount_ShortUserName_" + EmailAddress.Trim() + "_FacilityID_" + Facid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccount = (UserAccount)BizObject.Cache[key];
            }
            else
            {
                UserAccount = GetUserAccountFromUserAccountInfo(SiteProvider.PR2.GetUserAccountByEmailAndFacid(EmailAddress, Facid));
                BasePR.CacheData(key, UserAccount);
            }
            return UserAccount;
        }

        public static bool UpdateUserAccountDeptByUserID(int userID, int deptId)
        {
            return SiteProvider.PR2.UpdateUserAccountDeptByuserID(userID, deptId);

        }
        public static bool ResetUserPassword(int UserID, string Password) 
        {
            return SiteProvider.PR2.ResetUserPassword(UserID, Password);
        }

        /// <summary>
        /// Updates an existing UserAccount
        /// </summary>
        public static bool UpdateUserAccount(int iID, string cAddress1, string cAddress2, bool lAdmin, bool lPrimary, string cBirthDate,
          string cCity, string cEmail, string cExpires, string cFirstName, string cFloridaNo,
          string cInstitute, string cLastName, string cLectDate, string cMiddle, string cPhone,
          string cPromoCode, int PromoID, int FacilityID, string cPW, string cRefCode,
          string cRegType, string cSocial, string cState, string cUserName, string cZipCode,
          int SpecID, string LCUserName, DateTime LastAct, string PassFmt,
          string PassSalt, string MobilePIN, string LCEmail, string PWQuest, string PWAns,
          bool IsApproved, bool IsOnline, bool IsLocked, DateTime LastLogin, DateTime LastPWChg,
          DateTime LastLock, int XPWAtt, DateTime XPWAttSt, int XPWAnsAtt, DateTime XPWAnsSt,
          string Comment, DateTime Created, int AgeGroup, int RegTypeID, int RepID, string Work_Phone, string Badge_ID,
           DateTime Hire_Date, DateTime Termin_Date, int Dept_ID, int Position_ID, int Clinical_Ind, string Title, bool Giftcard_Ind,
           decimal Giftcard_Chour, decimal Giftcard_Uhour, int UniqueID, bool FirstLogin_Ind)
        {
            cAddress1 = BizObject.ConvertNullToEmptyString(cAddress1);
            cAddress2 = BizObject.ConvertNullToEmptyString(cAddress2);
            cBirthDate = BizObject.ConvertNullToEmptyString(cBirthDate);
            cCity = BizObject.ConvertNullToEmptyString(cCity);
            cEmail = BizObject.ConvertNullToEmptyString(cEmail);
            cExpires = BizObject.ConvertNullToEmptyString(cExpires);
            cFirstName = BizObject.ConvertNullToEmptyString(cFirstName);
            cFloridaNo = BizObject.ConvertNullToEmptyString(cFloridaNo);
            cInstitute = BizObject.ConvertNullToEmptyString(cInstitute);
            cLastName = BizObject.ConvertNullToEmptyString(cLastName);
            cLectDate = BizObject.ConvertNullToEmptyString(cLectDate);
            cMiddle = BizObject.ConvertNullToEmptyString(cMiddle);
            cPhone = BizObject.ConvertNullToEmptyString(cPhone);
            cPromoCode = BizObject.ConvertNullToEmptyString(cPromoCode);
            cPW = BizObject.ConvertNullToEmptyString(cPW);
            cRefCode = BizObject.ConvertNullToEmptyString(cRefCode);
            cRegType = BizObject.ConvertNullToEmptyString(cRegType);
            cSocial = BizObject.ConvertNullToEmptyString(cSocial);
            cState = BizObject.ConvertNullToEmptyString(cState);
            cUserName = BizObject.ConvertNullToEmptyString(cUserName);
            cZipCode = BizObject.ConvertNullToEmptyString(cZipCode);
            LCUserName = BizObject.ConvertNullToEmptyString(LCUserName);
            PassFmt = BizObject.ConvertNullToEmptyString(PassFmt);
            PassSalt = BizObject.ConvertNullToEmptyString(PassSalt);
            MobilePIN = BizObject.ConvertNullToEmptyString(MobilePIN);
            LCEmail = BizObject.ConvertNullToEmptyString(LCEmail);
            PWQuest = BizObject.ConvertNullToEmptyString(PWQuest);
            PWAns = BizObject.ConvertNullToEmptyString(PWAns);
            Comment = BizObject.ConvertNullToEmptyString(Comment);
            //UniqueID = BizObject.ConvertNullToEmptyString(UniqueID);

            UserAccountInfo record = new UserAccountInfo(iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate,
          cCity, cEmail, cExpires, cFirstName, cFloridaNo,
          cInstitute, cLastName, cLectDate, cMiddle, cPhone,
          cPromoCode, PromoID, FacilityID, cPW, cRefCode,
          cRegType, cSocial, cState, cUserName, cZipCode,
          SpecID, LCUserName, LastAct, PassFmt,
          PassSalt, MobilePIN, LCEmail, PWQuest, PWAns,
          IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg,
          LastLock, XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt,
          Comment, Created, AgeGroup, RegTypeID, RepID, Work_Phone, Badge_ID,
           Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, Giftcard_Ind,
           Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind);
            bool ret = SiteProvider.PR2.UpdateUserAccount(record);
            if (ret && record.FacilityID == 2) SiteProvider.PR2.UpdateNurseAccount(record.iID);
            BizObject.PurgeCacheItems("UserAccounts_UserAccount_" + iID.ToString());
            BizObject.PurgeCacheItems("UserAccounts_UserAccounts");
            return ret;
        }

        /// <summary>
        /// Creates a new UserAccount
        /// </summary>
        public static int InsertUserAccount(string cAddress1, string cAddress2, bool lAdmin, bool lPrimary, string cBirthDate,
          string cCity, string cEmail, string cExpires, string cFirstName, string cFloridaNo,
          string cInstitute, string cLastName, string cLectDate, string cMiddle, string cPhone,
          string cPromoCode, int PromoID, int FacilityID, string cPW, string cRefCode,
          string cRegType, string cSocial, string cState, string cUserName, string cZipCode,
          int SpecID, string LCUserName, DateTime LastAct, string PassFmt,
          string PassSalt, string MobilePIN, string LCEmail, string PWQuest, string PWAns,
          bool IsApproved, bool IsOnline, bool IsLocked, DateTime LastLogin, DateTime LastPWChg,
          DateTime LastLock, int XPWAtt, DateTime XPWAttSt, int XPWAnsAtt, DateTime XPWAnsSt,
          string Comment, DateTime Created, int AgeGroup, int RegTypeID, int RepID, string Work_Phone, string Badge_ID,
           DateTime Hire_Date, DateTime Termin_Date, int Dept_ID, int Position_ID, int Clinical_Ind, string Title, bool Giftcard_Ind,
           decimal Giftcard_Chour, decimal Giftcard_Uhour, int UniqueID, bool FirstLogin_Ind)
        {
            cAddress1 = BizObject.ConvertNullToEmptyString(cAddress1);
            cAddress2 = BizObject.ConvertNullToEmptyString(cAddress2);
            cBirthDate = BizObject.ConvertNullToEmptyString(cBirthDate);
            cCity = BizObject.ConvertNullToEmptyString(cCity);
            cEmail = BizObject.ConvertNullToEmptyString(cEmail);
            cExpires = BizObject.ConvertNullToEmptyString(cExpires);
            cFirstName = BizObject.ConvertNullToEmptyString(cFirstName);
            cFloridaNo = BizObject.ConvertNullToEmptyString(cFloridaNo);
            cInstitute = BizObject.ConvertNullToEmptyString(cInstitute);
            cLastName = BizObject.ConvertNullToEmptyString(cLastName);
            cLectDate = BizObject.ConvertNullToEmptyString(cLectDate);
            cMiddle = BizObject.ConvertNullToEmptyString(cMiddle);
            cPhone = BizObject.ConvertNullToEmptyString(cPhone);
            cPromoCode = BizObject.ConvertNullToEmptyString(cPromoCode);
            cPW = BizObject.ConvertNullToEmptyString(cPW);
            cRefCode = BizObject.ConvertNullToEmptyString(cRefCode);
            cRegType = BizObject.ConvertNullToEmptyString(cRegType);
            cSocial = BizObject.ConvertNullToEmptyString(cSocial);
            cState = BizObject.ConvertNullToEmptyString(cState);
            cUserName = BizObject.ConvertNullToEmptyString(cUserName);
            cZipCode = BizObject.ConvertNullToEmptyString(cZipCode);
            LCUserName = BizObject.ConvertNullToEmptyString(LCUserName);
            PassFmt = BizObject.ConvertNullToEmptyString(PassFmt);
            PassSalt = BizObject.ConvertNullToEmptyString(PassSalt);
            MobilePIN = BizObject.ConvertNullToEmptyString(MobilePIN);
            LCEmail = BizObject.ConvertNullToEmptyString(LCEmail);
            PWQuest = BizObject.ConvertNullToEmptyString(PWQuest);
            PWAns = BizObject.ConvertNullToEmptyString(PWAns);
            Comment = BizObject.ConvertNullToEmptyString(Comment);
            //UniqueID = BizObject.ConvertNullToEmptyString(UniqueID);

            // FirstLogin_Ind will be false for single sign on user when inserting
            UserAccountInfo record = new UserAccountInfo(0, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate,
          cCity, cEmail, cExpires, cFirstName, cFloridaNo,
          cInstitute, cLastName, cLectDate, cMiddle, cPhone,
          cPromoCode, PromoID, FacilityID, cPW, cRefCode,
          cRegType, cSocial, cState, cUserName, cZipCode,
          SpecID, LCUserName, LastAct, PassFmt,
          PassSalt, MobilePIN, LCEmail, PWQuest, PWAns,
          IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg,
          LastLock, XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt,
          Comment, Created, AgeGroup, RegTypeID, RepID, Work_Phone, Badge_ID,
           Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, Giftcard_Ind,
           Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind);
            int ret = SiteProvider.PR2.InsertUserAccount(record);

            BizObject.PurgeCacheItems("UserAccounts_UserAccount");
            return ret;
        }

        /// <summary>
        /// Creates a new UserAccount
        /// </summary>
        public static int InsertUserAccountMS(string cAddress1, string cAddress2, bool lAdmin, bool lPrimary, string cBirthDate,
          string cCity, string cEmail, string cExpires, string cFirstName, string cFloridaNo,
          string cInstitute, string cLastName, string cLectDate, string cMiddle, string cPhone,
          string cPromoCode, int PromoID, int FacilityID, string cPW, string cRefCode,
          string cRegType, string cSocial, string cState, string cUserName, string cZipCode,
          int SpecID, string LCUserName, DateTime LastAct, string PassFmt,
          string PassSalt, string MobilePIN, string LCEmail, string PWQuest, string PWAns,
          bool IsApproved, bool IsOnline, bool IsLocked, DateTime LastLogin, DateTime LastPWChg,
          DateTime LastLock, int XPWAtt, DateTime XPWAttSt, int XPWAnsAtt, DateTime XPWAnsSt,
          string Comment, DateTime Created, int AgeGroup, int RegTypeID, int RepID, string Work_Phone, string Badge_ID,
           DateTime Hire_Date, DateTime Termin_Date, int Dept_ID, int Position_ID, int Clinical_Ind, string Title, bool Giftcard_Ind,
           decimal Giftcard_Chour, decimal Giftcard_Uhour, int UniqueID, bool FirstLogin_Ind)
        {
            cAddress1 = BizObject.ConvertNullToEmptyString(cAddress1);
            cAddress2 = BizObject.ConvertNullToEmptyString(cAddress2);
            cBirthDate = BizObject.ConvertNullToEmptyString(cBirthDate);
            cCity = BizObject.ConvertNullToEmptyString(cCity);
            cEmail = BizObject.ConvertNullToEmptyString(cEmail);
            cExpires = BizObject.ConvertNullToEmptyString(cExpires);
            cFirstName = BizObject.ConvertNullToEmptyString(cFirstName);
            cFloridaNo = BizObject.ConvertNullToEmptyString(cFloridaNo);
            cInstitute = BizObject.ConvertNullToEmptyString(cInstitute);
            cLastName = BizObject.ConvertNullToEmptyString(cLastName);
            cLectDate = BizObject.ConvertNullToEmptyString(cLectDate);
            cMiddle = BizObject.ConvertNullToEmptyString(cMiddle);
            cPhone = BizObject.ConvertNullToEmptyString(cPhone);
            cPromoCode = BizObject.ConvertNullToEmptyString(cPromoCode);
            cPW = BizObject.ConvertNullToEmptyString(cPW);
            cRefCode = BizObject.ConvertNullToEmptyString(cRefCode);
            cRegType = BizObject.ConvertNullToEmptyString(cRegType);
            cSocial = BizObject.ConvertNullToEmptyString(cSocial);
            cState = BizObject.ConvertNullToEmptyString(cState);
            cUserName = BizObject.ConvertNullToEmptyString(cUserName);
            cZipCode = BizObject.ConvertNullToEmptyString(cZipCode);
            LCUserName = BizObject.ConvertNullToEmptyString(LCUserName);
            PassFmt = BizObject.ConvertNullToEmptyString(PassFmt);
            PassSalt = BizObject.ConvertNullToEmptyString(PassSalt);
            MobilePIN = BizObject.ConvertNullToEmptyString(MobilePIN);
            LCEmail = BizObject.ConvertNullToEmptyString(LCEmail);
            PWQuest = BizObject.ConvertNullToEmptyString(PWQuest);
            PWAns = BizObject.ConvertNullToEmptyString(PWAns);
            Comment = BizObject.ConvertNullToEmptyString(Comment);
            //UniqueID = BizObject.ConvertNullToEmptyString(UniqueID);

            // FirstLogin_Ind will be false for single sign on user when inserting
            UserAccountInfo record = new UserAccountInfo(0, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate,
          cCity, cEmail, cExpires, cFirstName, cFloridaNo,
          cInstitute, cLastName, cLectDate, cMiddle, cPhone,
          cPromoCode, PromoID, FacilityID, cPW, cRefCode,
          cRegType, cSocial, cState, cUserName, cZipCode,
          SpecID, LCUserName, LastAct, PassFmt,
          PassSalt, MobilePIN, LCEmail, PWQuest, PWAns,
          IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg,
          LastLock, XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt,
          Comment, Created, AgeGroup, RegTypeID, RepID, Work_Phone, Badge_ID,
           Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, Giftcard_Ind,
           Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind);
            int ret = SiteProvider.PR2.InsertUserAccountMS(record);

            BizObject.PurgeCacheItems("UserAccounts_UserAccount");
            return ret;
        }

            /// <summary>
        /// Creates a new Nurse In nurse database
        /// </summary>
        public static int InsertNurseAccount(int userID) 
        {
            int nurseId = 0;
            nurseId = SiteProvider.PR2.InsertNurseAccount(userID);
            return nurseId;
        }



        ///// <summary>
        ///// Returns the highest iID
        ///// (used to find the last ID inserted because all other methods
        ///// are failing)
        ///// </summary>
        //public static int GetNextUserAccountID()
        //{
        //    int iID = 0;
        //    iID = SiteProvider.PR.GetNextUserAccountID();
        //    return iID;
        //}

        /// <summary>
        /// Deletes an existing UserAccount, but first checks if OK to delete
        /// </summary>
        public static bool DeleteUserAccount(int iID)
        {
            bool IsOKToDelete = OKToDelete(iID);
            if (IsOKToDelete)
            {
                return (bool)DeleteUserAccount(iID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing UserAccount - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteUserAccount(int iID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteUserAccount(iID);
            //         new RecordDeletedEvent("UserAccount", iID, null).Raise();
            BizObject.PurgeCacheItems("UserAccounts_UserAccount");
            return ret;
        }



        /// <summary>
        /// Checks to see if a UserAccount can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int iID)
        {
            return true;
        }



        /// <summary>
        /// Returns a UserAccount object filled with the data taken from the input UserAccountInfo
        /// </summary>
        private static UserAccount GetUserAccountFromUserAccountInfo(UserAccountInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new UserAccount(record.iID, record.cAddress1, record.cAddress2, record.lAdmin,
                    record.lPrimary, record.cBirthDate,
          record.cCity, record.cEmail, record.cExpires, record.cFirstName, record.cFloridaNo,
          record.cInstitute, record.cLastName, record.cLectDate, record.cMiddle, record.cPhone,
          record.cPromoCode, record.PromoID, record.FacilityID, record.cPW, record.cRefCode,
          record.cRegType, record.cSocial, record.cState, record.cUserName.ToLower(), record.cZipCode,
          record.SpecID, record.LCUserName.ToLower(), record.LastAct, record.PassFmt,
          record.PassSalt, record.MobilePIN, record.LCEmail, record.PWQuest, record.PWAns.ToLower(),
          record.IsApproved, record.IsOnline, record.IsLocked, record.LastLogin, record.LastPWChg,
          record.LastLock, record.XPWAtt, record.XPWAttSt, record.XPWAnsAtt, record.XPWAnsSt,
          record.Comment, record.Created, record.AgeGroup, record.RegTypeID, record.RepID,
          record.Work_Phone, record.Badge_ID, record.Hire_Date, record.Termin_Date,
          record.Dept_ID, record.Position_ID, record.Clinical_Ind, record.Title, record.Giftcard_Ind,
           record.Giftcard_Chour, record.Giftcard_Uhour, record.UniqueID, record.FirstLogin_Ind);
            }
        }
        public static DataSet GetSEIUUsers()
        {
            return SiteProvider.PR2.GetSEIUUsers();
        }

        ///<summary>
        ///Returns a list of Gift Card Users
        ///</summary>
        ///

        public static bool GetGiftCardUsersFromUserAccountInfoList(string cGiftUsername)
        {
            bool ret = SiteProvider.PR2.GetGiftCardUsersFromUserAccountInfoList(cGiftUsername);
            return ret;
        }

        /// <summary>
        /// Returns a list of UserAccount objects filled with the data taken from the input list of UserAccountInfo
        /// </summary>
        private static List<UserAccount> GetUserAccountListFromUserAccountInfoList(List<UserAccountInfo> recordset)
        {
            List<UserAccount> UserAccounts = new List<UserAccount>();
            foreach (UserAccountInfo record in recordset)
                UserAccounts.Add(GetUserAccountFromUserAccountInfo(record));
            return UserAccounts;
        }

        /// <summary>
        /// Returns a collection with all UserAccounts for the specified promo code
        /// </summary>
        public static List<UserAccount> GetUserAccountsByPromoCode(string cPromoCode, string cSortExpression)
        {
            if (cPromoCode == null)
                cPromoCode = "";
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cLastName";

            List<UserAccount> UserAccounts = null;
            string key = "UserAccounts_UserAccounts_" + cPromoCode.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo> recordset = SiteProvider.PR2.GetUserAccountsByPromoCode(cPromoCode, cSortExpression);
                UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        /// <summary>
        /// Returns a collection with all UserAccounts for the specified lastname (can be portion)
        /// </summary>
        public static List<UserAccount> GetUserAccountsByLastName(string cLastName, string cSortExpression)
        {
            if (cLastName == null)
                cLastName = "A";
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cLastName";

            List<UserAccount> UserAccounts = null;
            string key = "UserAccounts_UserAccountsByLastName_" + cLastName.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo> recordset = SiteProvider.PR2.GetUserAccountsByLastName(cLastName, cSortExpression);
                UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        /// <summary>
        /// Returns a collection with all UserAccounts for the specified lastname (can be portion)
        /// </summary>
        public static List<UserAccount> GetUserAccountsByShortUserName(string ShortUserName, string cSortExpression)
        {
            if (ShortUserName == null)
                ShortUserName = "a";
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cUserName";

            List<UserAccount> UserAccounts = null;
            string key = "UserAccounts_UserAccountsByShortUserName_" + ShortUserName.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo> recordset = SiteProvider.PR2.GetUserAccountsByShortUserName(ShortUserName, cSortExpression);
                UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        /// <summary>
        /// Returns a collection with all UserAccounts for the specified lastname (can be portion)
        /// </summary>
        public static List<UserAccount> GetUserAccountsByEmail(string cEmail, string cSortExpression)
        {
            if (cEmail == null)
                cEmail = "a";
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cEmail";

            List<UserAccount> UserAccounts = null;
            string key = "UserAccounts_UserAccountsByEmail_" + cEmail.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo> recordset = SiteProvider.PR2.GetUserAccountsByEmail(cEmail, cSortExpression);
                UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        /// <summary>
        /// Returns the number of total UserAccounts for a Promo Code
        /// </summary>
        public static int GetUserAccountCountByPromoCode(string cPromoCode)
        {
            int UserAccountCount = 0;
            string key = "UserAccounts_UserAccountCount_PromoCode_" + cPromoCode.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccountCount = (int)BizObject.Cache[key];
            }
            else
            {
                UserAccountCount = SiteProvider.PR2.GetUserAccountCountByPromoCode(cPromoCode);
                BasePR.CacheData(key, UserAccountCount);
            }
            return UserAccountCount;
        }

        /// <summary>
        /// Returns a collection with all UserAccounts for the specified PromoID
        /// </summary>
        public static List<UserAccount> GetUserAccountsByPromoID(int PromoID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cLastName";

            List<UserAccount> UserAccounts = null;
            string key = "UserAccounts_UserAccounts_PromoID_" + PromoID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo> recordset = SiteProvider.PR2.GetUserAccountsByPromoID(PromoID, cSortExpression);
                UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }
        ///// <summary>
        ///// Returns a collection with all admin UserAccounts for the specified promoID
        ///// </summary>
        //public static List<UserAccount> GetAdminUserAccountsByPromoID(int PromoID, string cSortExpression)
        //{
        //    if (cSortExpression == null)
        //        cSortExpression = "";

        //    // provide default sort
        //    if (cSortExpression.Length == 0)
        //        cSortExpression = "cLastName";

        //    List<UserAccount> UserAccounts = null;
        //    string key = "UserAccounts_UserAccounts_Admin_" + cPromoCode.ToString() + cSortExpression.ToString();

        //    if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
        //    {
        //        UserAccounts = (List<UserAccount>)BizObject.Cache[key];
        //    }
        //    else
        //    {
        //        List<UserAccountInfo> recordset = SiteProvider.PR.GetAdminUserAccountsByPromoCode(cPromoCode, cSortExpression);
        //        UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
        //        BasePR.CacheData(key, UserAccounts);
        //    }
        //    return UserAccounts;
        //}

        /// <summary>
        /// Returns the number of total UserAccounts for a PromoID
        /// </summary>
        public static int GetUserAccountCountByPromoID(int PromoID)
        {
            int UserAccountCount = 0;
            string key = "UserAccounts_UserAccountCount_PromoID_" + PromoID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccountCount = (int)BizObject.Cache[key];
            }
            else
            {
                UserAccountCount = SiteProvider.PR2.GetUserAccountCountByPromoID(PromoID);
                BasePR.CacheData(key, UserAccountCount);
            }
            return UserAccountCount;
        }


        /// <summary>
        /// Returns a collection with all UserAccounts for the specified FacilityID
        /// </summary>
        public static List<UserAccount> GetUserAccountsByFacilityID(int FacilityID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cLastName";

            List<UserAccount> UserAccounts = null;
            string key = "UserAccounts_UserAccounts_FacilityID_" + FacilityID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo> recordset = SiteProvider.PR2.GetUserAccountsByFacilityID(FacilityID, cSortExpression);
                UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        public static List<UserAccount> GetUserAccountsByFacilityIDAndSuperCourseID(
            int facilityID, int superCourseID, string sortExpression)
        {
            if (sortExpression == null)
                sortExpression = "";

            // provide default sort
            if (sortExpression.Length == 0)
                sortExpression = "cLastName";

            List<UserAccount> userAccounts = null;
            string key = "UserAccounts_UserAccounts_FacilityID_And_SuperCourseID" +
                facilityID.ToString() + sortExpression;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                userAccounts = (List<UserAccount>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo> recordset = SiteProvider.PR2.GetUserAccountsByFacilityIDAndSuperCourseID(
                    facilityID, superCourseID, sortExpression);
                userAccounts = GetUserAccountListFromUserAccountInfoList(recordset);

                BasePR.CacheData(key, userAccounts);
            }
            return userAccounts;
        }

        /// <summary>
        /// Returns a collection with all admin UserAccounts for the specified FacilityID
        /// </summary>
        public static List<UserAccount> GetAdminUserAccountsByFacilityID(int FacilityID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cLastName";

            List<UserAccount> UserAccounts = null;
            string key = "UserAccounts_UserAccounts_Admin_" + FacilityID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo> recordset = SiteProvider.PR2.GetAdminUserAccountsByFacilityID(FacilityID, cSortExpression);
                UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        /// <summary>
        /// Returns the number of total UserAccounts for a FacilityID
        /// </summary>
        public static int GetUserAccountCountByFacilityID(int FacilityID)
        {
            int UserAccountCount = 0;
            string key = "UserAccounts_UserAccountCount_FacilityID_" + FacilityID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccountCount = (int)BizObject.Cache[key];
            }
            else
            {
                UserAccountCount = SiteProvider.PR2.GetUserAccountCountByFacilityID(FacilityID);
                BasePR.CacheData(key, UserAccountCount);
            }
            return UserAccountCount;
        }

        /// <summary>
        /// Updates an existing UserAccount admin field
        /// </summary>
        public static bool UpdateUserAccountAdminStatus(int iID, bool lAdmin)
        {
            bool ret = SiteProvider.PR2.UpdateUserAccountAdminStatus(iID, lAdmin);
            return ret;
        }

        ///<summary>
        ///
        public static int CreateRetailUser(string UserName, string Password, int iid)
        {
            int ret = SiteProvider.PR2.CreateRetailUser(UserName, Password, iid);
            return ret;
        }

        public static int TranferTrans(int iid, int rnid)
        {
            int ret = SiteProvider.PR2.TranferTrans(iid, rnid);
            return ret;
        }

        public static decimal GetTaxRateByUserId(int iID)
        {
            decimal ret = SiteProvider.PR2.GetTaxRateByUserId(iID);
            return ret;
        }
        public static DataSet GetUserNameWithRoleByFacilityId(string Facilityid, string roles, string csortExpression)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.GetUserNameWithRoleByFacilityId(Facilityid, roles, csortExpression);
        }
        public static DataSet GetUserNameWithRoleByFacilityId(string Facilityid, string csortExpression)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.GetUserNameWithRoleByFacilityId(Facilityid, csortExpression);
        }
        /// <summary>
        /// Updates an existing UserAccount lPrimary field
        /// </summary>
        public static bool UpdateUserAccountlPrimaryStatus(int iID, bool lPrimary)
        {
            bool ret = SiteProvider.PR2.UpdateUserAccountlPrimaryStatus(iID, lPrimary);
            return ret;
        }

        public static string GetEmailByUserID(int iid)
        {
            return SiteProvider.PR2.GetEmailByUserid(iid);
        }
    }

    ////////////////////////////////////////////////////////////
    /// <summary>
    /// UserAccount1 business object class
    /// </summary>
    public class UserAccount1 : BasePR
    {
        private int _iID = 0;
        public int iID
        {
            get { return _iID; }
            protected set { _iID = value; }
        }

        private string _cAddress1 = "";
        public string cAddress1
        {
            get { return _cAddress1; }
            set { _cAddress1 = value; }
        }

        private string _cAddress2 = "";
        public string cAddress2
        {
            get { return _cAddress2; }
            set { _cAddress2 = value; }
        }

        private bool _lAdmin = false;
        public bool lAdmin
        {
            get { return _lAdmin; }
            set { _lAdmin = value; }
        }

        private bool _lPrimary = false;
        public bool lPrimary
        {
            get { return _lPrimary; }
            set { _lPrimary = value; }
        }

        private string _cBirthDate = "";
        public string cBirthDate
        {
            get { return _cBirthDate; }
            set { _cBirthDate = value; }
        }

        private string _cCity = "";
        public string cCity
        {
            get { return _cCity; }
            set { _cCity = value; }
        }

        private string _cEmail = "";
        public string cEmail
        {
            get { return _cEmail; }
            set { _cEmail = value; }
        }

        private string _cExpires = "";
        public string cExpires
        {
            get { return _cExpires; }
            set { _cExpires = value; }
        }

        private string _cFirstName = "";
        public string cFirstName
        {
            get { return _cFirstName; }
            set { _cFirstName = value; }
        }

        private string _cFloridaNo = "";
        public string cFloridaNo
        {
            get { return _cFloridaNo; }
            set { _cFloridaNo = value; }
        }

        private string _cInstitute = "";
        public string cInstitute
        {
            get { return _cInstitute; }
            set { _cInstitute = value; }
        }

        private string _cLastName = "";
        public string cLastName
        {
            get { return _cLastName; }
            set { _cLastName = value; }
        }

        private string _cLectDate = "";
        public string cLectDate
        {
            get { return _cLectDate; }
            set { _cLectDate = value; }
        }

        private string _cMiddle = "";
        public string cMiddle
        {
            get { return _cMiddle; }
            set { _cMiddle = value; }
        }

        private string _cPhone = "";
        public string cPhone
        {
            get { return _cPhone; }
            set { _cPhone = value; }
        }

        private string _cPromoCode = "";
        public string cPromoCode
        {
            get { return _cPromoCode; }
            set { _cPromoCode = value; }
        }

        private int _PromoID = 0;
        public int PromoID
        {
            get { return _PromoID; }
            set { _PromoID = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }

        private string _facilityName = "";
        public string FacilityName
        {
            get { return _facilityName; }
            set { _facilityName = value; }
        }
        private bool _facilityActive = false;
        public bool FacilityActive
        {
            get { return _facilityActive; }
            set { _facilityActive = value; }
        }
        private string _cPW = "";
        public string cPW
        {
            get { return _cPW; }
            set { _cPW = value; }
        }

        private string _cRefCode = "";
        public string cRefCode
        {
            get { return _cRefCode; }
            set { _cRefCode = value; }
        }

        private string _cRegType = "";
        public string cRegType
        {
            get { return _cRegType; }
            set { _cRegType = value; }
        }

        private string _cSocial = "";
        public string cSocial
        {
            get { return _cSocial; }
            set { _cSocial = value; }
        }

        private string _cState = "";
        public string cState
        {
            get { return _cState; }
            set { _cState = value; }
        }

        private string _cUserName = "";
        public string cUserName
        {
            get { return _cUserName.ToLower(); }
            set { _cUserName = value.ToLower(); }
        }

        private string _cZipCode = "";
        public string cZipCode
        {
            get { return _cZipCode; }
            set { _cZipCode = value; }
        }

        private int _SpecID = 0;
        public int SpecID
        {
            get { return _SpecID; }
            set { _SpecID = value; }
        }

        private string _LCUserName = "";
        public string LCUserName
        {
            get { return _LCUserName.ToLower(); }
            set { _LCUserName = value.ToLower(); }
        }

        private DateTime _LastAct = System.DateTime.Now;
        public DateTime LastAct
        {
            get { return _LastAct; }
            set { _LastAct = value; }
        }

        private string _PassFmt = "";
        public string PassFmt
        {
            get { return _PassFmt; }
            set { _PassFmt = value; }
        }

        private string _PassSalt = "";
        public string PassSalt
        {
            get { return _PassSalt; }
            set { _PassSalt = value; }
        }

        private string _MobilePIN = "";
        public string MobilePIN
        {
            get { return _MobilePIN; }
            set { _MobilePIN = value; }
        }

        private string _LCEmail = "";
        public string LCEmail
        {
            get { return _LCEmail.ToLower(); }
            set { _LCEmail = value.ToLower(); }
        }

        private string _PWQuest = "";
        public string PWQuest
        {
            get { return _PWQuest; }
            set { _PWQuest = value; }
        }

        private string _PWAns = "";
        public string PWAns
        {
            get { return _PWAns; }
            set { _PWAns = value; }
        }

        private bool _IsApproved = false;
        public bool IsApproved
        {
            get { return _IsApproved; }
            set { _IsApproved = value; }
        }

        private bool _IsOnline = false;
        public bool IsOnline
        {
            get { return _IsOnline; }
            set { _IsOnline = value; }
        }

        private bool _IsLocked = false;
        public bool IsLocked
        {
            get { return _IsLocked; }
            set { _IsLocked = value; }
        }

        private DateTime _LastLogin = System.DateTime.MinValue;
        public DateTime LastLogin
        {
            get { return _LastLogin; }
            set { _LastLogin = value; }
        }

        private DateTime _LastPWChg = System.DateTime.MinValue;
        public DateTime LastPWChg
        {
            get { return _LastPWChg; }
            set { _LastPWChg = value; }
        }

        private DateTime _LastLock = System.DateTime.MinValue;
        public DateTime LastLock
        {
            get { return _LastLock; }
            set { _LastLock = value; }
        }

        private int _XPWAtt = 0;
        public int XPWAtt
        {
            get { return _XPWAtt; }
            set { _XPWAtt = value; }
        }

        private DateTime _XPWAttSt = System.DateTime.MinValue;
        public DateTime XPWAttSt
        {
            get { return _XPWAttSt; }
            set { _XPWAttSt = value; }
        }

        private int _XPWAnsAtt = 0;
        public int XPWAnsAtt
        {
            get { return _XPWAnsAtt; }
            set { _XPWAnsAtt = value; }
        }

        private DateTime _XPWAnsSt = System.DateTime.MinValue;
        public DateTime XPWAnsSt
        {
            get { return _XPWAnsSt; }
            set { _XPWAnsSt = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        private DateTime _Created = System.DateTime.Now;
        public DateTime Created
        {
            get { return _Created; }
            set { _Created = value; }
        }

        private int _AgeGroup = 0;
        public int AgeGroup
        {
            get { return _AgeGroup; }
            set { _AgeGroup = value; }
        }

        private int _RegTypeID = 0;
        public int RegTypeID
        {
            get { return _RegTypeID; }
            set { _RegTypeID = value; }
        }

        private int _RepID = 0;
        public int RepID
        {
            get { return _RepID; }
            set { _RepID = value; }
        }


        private string _Work_Phone = "";
        public string Work_Phone
        {
            get { return _Work_Phone; }
            set { _Work_Phone = value; }
        }
        private string _Badge_ID = "";
        public string Badge_ID
        {
            get { return _Badge_ID; }
            set { _Badge_ID = value; }
        }

        private DateTime _Hire_Date = System.DateTime.MinValue;
        public DateTime Hire_Date
        {
            get { return _Hire_Date; }
            set { _Hire_Date = value; }
        }

        private DateTime _Termin_Date = System.DateTime.MinValue;
        public DateTime Termin_Date
        {
            get { return _Termin_Date; }
            set { _Termin_Date = value; }
        }

        private int _Dept_ID = 0;
        public int Dept_ID
        {
            get { return _Dept_ID; }
            set { _Dept_ID = value; }
        }

        private string _deptName = "";
        public string DeptName
        {
            get { return _deptName; }
            set { _deptName = value; }
        }

        private string _roleNames = "";
        public string RoleNames
        {
            get { return _roleNames; }
            set { _roleNames = value; }
        }

        private int _Position_ID = 0;
        public int Position_ID
        {
            get { return _Position_ID; }
            set { _Position_ID = value; }
        }

        private int _Clinical_Ind = 0;
        public int Clinical_Ind
        {
            get { return _Clinical_Ind; }
            set { _Clinical_Ind = value; }
        }

        private string _Title = "";
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private bool _Giftcard_Ind = false;
        public bool Giftcard_Ind
        {
            get { return _Giftcard_Ind; }
            set { _Giftcard_Ind = value; }
        }

        private decimal _Giftcard_Chour = 0;
        public decimal Giftcard_Chour
        {
            get { return _Giftcard_Chour; }
            set { _Giftcard_Chour = value; }
        }

        private decimal _Giftcard_Uhour = 0;
        public decimal Giftcard_Uhour
        {
            get { return _Giftcard_Uhour; }
            set { _Giftcard_Uhour = value; }
        }

        private int _UniqueID = 0;
        public int UniqueID
        {
            get { return _UniqueID; }
            set { _UniqueID = value; }
        }

        private bool _FirstLogin_Ind = false;
        public bool FirstLogin_Ind
        {
            get { return _FirstLogin_Ind; }
            set { _FirstLogin_Ind = value; }
        }

        public UserAccount1(int iID, string cFirstName, string cLastName)
        {
            this.iID = iID;
            this.cFirstName = cFirstName;
            this.cLastName = cLastName;
        }
        public UserAccount1(int iID, string cAddress1, string cAddress2, bool lAdmin, bool lPrimary, string cBirthDate,
                  string cCity, string cEmail, string cExpires, string cFirstName, string cFloridaNo,
           string cInstitute, string cLastName, string cLectDate, string cMiddle, string cPhone,
           string cPromoCode, int PromoID, int FacilityID, string FacilityName, bool FacilityActive, string cPW, string cRefCode,
           string cRegType, string cSocial, string cState, string cUserName, string cZipCode,
           int SpecID, string LCUserName, DateTime LastAct, string PassFmt,
           string PassSalt, string MobilePIN, string LCEmail, string PWQuest, string PWAns,
           bool IsApproved, bool IsOnline, bool IsLocked, DateTime LastLogin, DateTime LastPWChg,
           DateTime LastLock, int XPWAtt, DateTime XPWAttSt, int XPWAnsAtt, DateTime XPWAnsSt,
           string Comment, DateTime Created, int AgeGroup, int RegTypeID, int RepID, string Work_Phone, string Badge_ID,
           DateTime Hire_Date, DateTime Termin_Date, int Dept_ID, string Dept_Name, string roleNames, int Position_ID, int Clinical_Ind, string Title, bool Giftcard_Ind,
           decimal Giftcard_Chour, decimal Giftcard_Uhour, int UniqueID, bool FirstLogin_Ind)
        {
            this.iID = iID;
            this.cAddress1 = cAddress1;
            this.cAddress2 = cAddress2;
            this.lAdmin = lAdmin;
            this.lPrimary = lPrimary;
            this.cBirthDate = cBirthDate;
            this.cCity = cCity;
            this.cEmail = cEmail;
            this.cExpires = cExpires;
            this.cFirstName = cFirstName;
            this.cFloridaNo = cFloridaNo;
            this.cInstitute = cInstitute;
            this.cLastName = cLastName;
            this.cLectDate = cLectDate;
            this.cMiddle = cMiddle;
            this.cPhone = cPhone;
            this.cPromoCode = cPromoCode;
            this.PromoID = PromoID;
            this.FacilityID = FacilityID;
            this.FacilityName = FacilityName;
            this.FacilityActive = FacilityActive;
            this.cPW = cPW;
            this.cRefCode = cRefCode;
            this.cRegType = cRegType;
            this.cSocial = cSocial;
            this.cState = cState;
            this.cUserName = cUserName.ToLower();
            this.cZipCode = cZipCode;
            this.SpecID = SpecID;
            this.LCUserName = LCUserName.ToLower();
            this.LastAct = LastAct;
            this.PassFmt = PassFmt;
            this.PassSalt = PassSalt;
            this.MobilePIN = MobilePIN;
            this.LCEmail = LCEmail;
            this.PWQuest = PWQuest;
            this.PWAns = PWAns;
            this.IsApproved = IsApproved;
            this.IsOnline = IsOnline;
            this.IsLocked = IsLocked;
            this.LastLogin = LastLogin;
            this.LastPWChg = LastPWChg;
            this.LastLock = LastLock;
            this.XPWAtt = XPWAtt;
            this.XPWAttSt = XPWAttSt;
            this.XPWAnsAtt = XPWAnsAtt;
            this.XPWAnsSt = XPWAnsSt;
            this.Comment = Comment;
            this.Created = Created;
            this.AgeGroup = AgeGroup;
            this.RegTypeID = RegTypeID;
            this.RepID = RepID;
            this.Work_Phone = Work_Phone;
            this.Badge_ID = Badge_ID;
            this.Hire_Date = Hire_Date;
            this.Termin_Date = Termin_Date;
            this.Dept_ID = Dept_ID;
            this.DeptName = Dept_Name;
            this.RoleNames = roleNames;
            this.Position_ID = Position_ID;
            this.Clinical_Ind = Clinical_Ind;
            this.Title = Title;
            this.Giftcard_Ind = Giftcard_Ind;
            this.Giftcard_Chour = Giftcard_Chour;
            this.Giftcard_Uhour = Giftcard_Uhour;
            this.UniqueID = UniqueID;
            this.FirstLogin_Ind = FirstLogin_Ind;

        }

        private static UserAccount1 GetUserAccountFromUserAccountInfo1(UserAccountInfo1 record)
        {
            if (record == null)
                return null;
            else
            {

                return new UserAccount1(record.iID, record.cAddress1, record.cAddress2, record.lAdmin,
                record.lPrimary, record.cBirthDate, record.cCity, record.cEmail, record.cExpires, record.cFirstName, record.cFloridaNo,
                record.cInstitute, record.cLastName, record.cLectDate, record.cMiddle, record.cPhone,
                record.cPromoCode, record.PromoID, record.FacilityID, record.FacilityName, record.FacilityActive, record.cPW, record.cRefCode,
                record.cRegType, record.cSocial, record.cState, record.cUserName.ToLower(), record.cZipCode,
                record.SpecID, record.LCUserName.ToLower(), record.LastAct, record.PassFmt,
                record.PassSalt, record.MobilePIN, record.LCEmail, record.PWQuest, record.PWAns,
                record.IsApproved, record.IsOnline, record.IsLocked, record.LastLogin, record.LastPWChg,
                record.LastLock, record.XPWAtt, record.XPWAttSt, record.XPWAnsAtt, record.XPWAnsSt,
                record.Comment, record.Created, record.AgeGroup, record.RegTypeID, record.RepID,
                record.Work_Phone, record.Badge_ID, record.Hire_Date, record.Termin_Date, record.Dept_ID, record.DeptName, record.RoleNames,
                record.Position_ID, record.Clinical_Ind, record.Title, record.Giftcard_Ind,
                record.Giftcard_Chour, record.Giftcard_Uhour, record.UniqueID, record.FirstLogin_Ind);
            }
        }

        

        public static bool Switchfacility(int iID, int facid)
        {
           // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.Switchfacility(iID, facid);
        }

        public static bool Delete(int iID, int facid)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            SiteProvider.PR2.DeleteUser(iID, facid);

            return false;
        }

        //public bool Update()
        //{
        //    return UserAccount1.Update(this.IsLocked);
        //}

        public static bool DeActivateUser(int userID)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.DeActivateUser(userID);
        }
        public static bool DeActivateMutiUser(string userIDs)
        {
            return SiteProvider.PR2.DeActivateMutiUser(userIDs);
        }

        public static bool ActivateUser(int userID)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.ActivateUser(userID);
        }
        private static List<UserAccount1> GetUserAccountListFromUserAccountInfoList1(List<UserAccountInfo1> recordset)
        {
            List<UserAccount1> UserAccounts = new List<UserAccount1>();
            foreach (UserAccountInfo1 record in recordset)
                UserAccounts.Add(GetUserAccountFromUserAccountInfo1(record));
            return UserAccounts;
        }

       

        public static UserAccount1 GetUserAccountByUserName(string cUserName)
        {
            if (cUserName == null)
                //                cUserName = "a";
                return null;

            cUserName = cUserName.ToLower();
            UserAccount1 user = null;
            string key = "UserAccounts_UserAccountsByUserName_" + cUserName.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                user = (UserAccount1)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                UserAccountInfo1 recordset = SiteProvider.PR2.GetUserAccountByUserName(cUserName);
                user = GetUserAccountFromUserAccountInfo1(recordset);
                BasePR.CacheData(key, user);
            }
            return user;
        }


        // NOTE; The following method is used only for GiftCard login, and uses the "short"
        // version of the username with no faciltiyid attached to it.
        // This works because gift card codes are unique regardless of any facility related to it.
        public static UserAccount1 GetUserAccountByGiftCardRedemptionCode(string RedemptionCode)
        {
            if (RedemptionCode == null)
                //                cUserName = "a";
                return null;

            RedemptionCode = RedemptionCode.ToLower();
            UserAccount1 user = null;
            string key = "UserAccounts_UserAccountByGiftCardRedemptionCode_" + RedemptionCode.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                user = (UserAccount1)BizObject.Cache[key];
            }
            else
            {
               // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                UserAccountInfo1 recordset = SiteProvider.PR2.GetUserAccountByGiftCardRedemptionCode(RedemptionCode);
                user = GetUserAccountFromUserAccountInfo1(recordset);
                BasePR.CacheData(key, user);
            }
            return user;
        }

        public static DataSet GetDuplicatesByFirstAndLastname(string facilityid)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.GetDuplicatesByFirstAndLastname(facilityid);
        }
        public static DataSet GetDuplicatesByUniqueid(string facilityid)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.GetDuplicatesByUniqueid(facilityid);
        }
        public static DataSet GetMergeRecs(string fname, string lname, string facilityid)
        {
           // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.GetMergeRecs(fname, lname, facilityid);
        }

        public static DataSet GetAssetUrlByTopicID(string TopicID)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.GetAssetUrlByTopicID(TopicID);
        }


        public static System.Data.DataSet GetLicenseTypes()
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.GetLicenseTypes();

        }

       
        public static List<UserAccount1> GetUserAccountsBySearchCriteria(string cSortExpression, string firstName, string lastName,
             string cUserName, int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID, string roleName, int startRowIndex, int maximumRows)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cLastName";

            List<UserAccount1> UserAccounts = null;
            string key = "UserAccounts_UserAccountsBySearchCriteria" + "_" + firstName + "_" +
                            lastName + "_" + cUserName + "_" + facilityid + "_" + badge_id + "_" + cSocial + "_" + isApproved + "_" + isNotApproved + "_" + Dept_ID.ToString() + "_" + roleName + "_" + startRowIndex.ToString() + "_" + maximumRows.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount1>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<UserAccountInfo1> userAccounts = SiteProvider.PR2.GetUserAccountsBySearchCriteria(cSortExpression, firstName, lastName, cUserName, facilityid, badge_id, cSocial, isApproved, isNotApproved, Dept_ID, roleName, startRowIndex, maximumRows);
                UserAccounts = GetUserAccountListFromUserAccountInfoList1(userAccounts);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        public static List<UserAccount1> GetUserAccountsBySearchCriteria(string cSortExpression, string firstName, string lastName,
                string cUserName, int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID,
               string roleName, int startRowIndex, int maximumRows, int rolepriority, int currentUserID, bool isActiveFacilityManager)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cLastName";

            List<UserAccount1> UserAccounts = null;
            string key = "UserAccounts_UserAccountsBySearchCriteria" + "_" + firstName + "_" +
                            lastName + "_" + cUserName + "_" + facilityid + "_" + badge_id + "_" + cSocial + "_" + isApproved + "_" + isNotApproved + "_" + Dept_ID.ToString() + "_" + roleName + "_" + startRowIndex.ToString() + "_" + maximumRows.ToString() + "_" + rolepriority + "_" + currentUserID;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount1>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<UserAccountInfo1> userAccounts = SiteProvider.PR2.GetUserAccountsBySearchCriteria(cSortExpression, firstName, lastName, cUserName, facilityid, badge_id, cSocial, isApproved, isNotApproved, Dept_ID, roleName, startRowIndex, maximumRows, rolepriority, currentUserID, isActiveFacilityManager);
                UserAccounts = GetUserAccountListFromUserAccountInfoList1(userAccounts);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        public static List<UserAccount1> GetUsersBySearchCriteria(string cSortExpression, string firstName, string lastName,
                string cUserName, int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID,
               string roleName, int startRowIndex, int maximumRows, int rolepriority, int currentUserID, bool isActiveFacilityManager, ref int numResults)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            
            if (cSortExpression.Length == 0)
                cSortExpression = "cLastName";

            List<UserAccount1> UserAccounts = null;
            string key = "UserAccounts_UsersBySearchCriteria" + "_" + firstName + "_" +
                            lastName + "_" + cUserName + "_" + facilityid + "_" + badge_id + "_" + cSocial + "_" + isApproved + "_" + isNotApproved + "_" + Dept_ID.ToString() + "_" + roleName + "_" + startRowIndex.ToString() + "_" + maximumRows.ToString() + "_" + rolepriority + "_" + currentUserID;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount1>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo1> userAccounts = SiteProvider.PR2.GetUsersBySearchCriteria(cSortExpression, firstName, lastName, cUserName, facilityid, badge_id, cSocial, isApproved, isNotApproved, Dept_ID, roleName, startRowIndex, maximumRows, rolepriority, currentUserID, isActiveFacilityManager, ref numResults);
                UserAccounts = GetUserAccountListFromUserAccountInfoList1(userAccounts);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        
        /// <summary>
        /// CE retail Users
        /// </summary>

        public static List<UserAccount1> GetCERetailUsersBySearchCriteria(string cSortExpression, string cfirstName, string clastName,
                 string cUserName, string cpw, string cAddress1, string cEmail, string uniqueid,
                string cState, string state, string cZipCode, string verifNumber)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cLastName";

            List<UserAccount1> UserAccounts = null;
            string key = "UserAccounts_UserAccountsBySearchCriteria" + "_" + cfirstName + "_" +
                            clastName + "_" + cUserName + "_" + cpw + "_" + cAddress1 + "_" + cEmail + "_" + uniqueid + "_" + cState + "_" + state + "_" + cZipCode;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount1>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<UserAccountInfo1> userAccounts = SiteProvider.PR2.GetCERetailUsersBySearchCriteria(cSortExpression, cfirstName, clastName, cUserName, cpw, cAddress1, cEmail, uniqueid, cState, state, cZipCode, verifNumber);
                UserAccounts = GetUserAccountListFromUserAccountInfoList1(userAccounts);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }
        public static List<UserAccount1> GetPearlsUsersBySearchCriteria(string cSortExpression, string cfirstName, string clastName,
                 string cUserName, string cpw, string cAddress1, string cEmail, string cState, string cZipCode, bool isApproved, bool isNotApproved) 
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cLastName";

            List<UserAccount1> UserAccounts = null;
            string key = "UserAccounts_UserAccountsBySearchCriteria" + "_" + cfirstName + "_" +
                            clastName + "_" + cUserName + "_" + cpw + "_" + cAddress1 + "_" + cEmail + "_" +  cState + "_" + cZipCode + "_" + isApproved + "_" +   isNotApproved;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount1>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<UserAccountInfo1> userAccounts = SiteProvider.PR2.GetPearlsUsersBySearchCriteria(cSortExpression, cfirstName, clastName, cUserName, cpw, cAddress1, cEmail, cState, cZipCode, isApproved, isNotApproved);
                UserAccounts = GetUserAccountListFromUserAccountInfoList1(userAccounts);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        public static List<UserAccount1> GetMicrositeUsersBySearchCriteria(string cSortExpression, string cfirstName, string clastName,
         string cUserName, string cpw, string cAddress1, string cEmail, string cState, string cZipCode)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cLastName";

            List<UserAccount1> UserAccounts = null;
            string key = "UserAccounts_UserAccountsBySearchCriteria" + "_" + cfirstName + "_" + clastName + "_" + 
                cUserName + "_" + cpw + "_" + cAddress1 + "_" + cEmail + "_" + cState + "_" + cZipCode;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount1>)BizObject.Cache[key];
            }
            else
            {
                List<UserAccountInfo1> userAccounts = SiteProvider.PR2.GetMicrositeUsersBySearchCriteria(
                    cSortExpression, cfirstName, clastName, cUserName, cpw, cAddress1, cEmail, cState, cZipCode);
                UserAccounts = GetUserAccountListFromUserAccountInfoList1(userAccounts);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }

        public static DataSet GetNonUnlimitedOrders(string cSortExpression, string cfirstName, string clastName, string cUserName,
        string cpw, string cAddress1, string cEmail, string license_number, string cState, string state, string cZipCode)
        {

            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.GetNonUnlimitedOrders(cSortExpression, cfirstName, clastName, cUserName,
            cpw, cAddress1, cEmail, license_number, cState, state, cZipCode);

        }
        public static DataSet GetTranscripts(string cSortExpression, string cfirstName, string clastName, string cUserName,
         string cpw, string cAddress1, string cEmail, string uniqueid, string cState, string state, string cZipCode)
        {

            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.GetTranscripts(cSortExpression, cfirstName, clastName, cUserName,
            cpw, cAddress1, cEmail, uniqueid, cState, state, cZipCode);

        }

        //public static bool InsertUser(string username, string firstname, string middlename, string lastname, string title, string password, string address1,
        //    string address2, string city, string state, string zip, string homephone, string workphone, string birthdate, string badgeid, string ssn, string hiredate, string termindate, int deptid, int positionid, string pwdqs, string pwdans, string rolename, int facilityid)
        //{
        //    PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
        //    return obj.InsertUser(username, firstname, middlename, lastname, title, password, address1, address2, city, state,
        //        zip, homephone, workphone, birthdate, badgeid, ssn, hiredate, termindate, deptid, positionid, pwdqs, pwdans, rolename, facilityid);

        //}

        public static int GetUserAccountsCount(string firstName, string lastName, string cUserName, int facilityid,
               string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID, string cSortExpression)
        {
            int totalCount = 0;
            string key = "UserAccounts_UserAccountsCount" + "_" + firstName + "_" +
                            lastName + "_" + cUserName + "_" + facilityid + "_" + badge_id + "_" + cSocial + "_" + isApproved + "_" + isNotApproved + "_" + Dept_ID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                totalCount = (int)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                totalCount = SiteProvider.PR2.GetUserAccountsCount(firstName, lastName, cUserName, facilityid, badge_id, cSocial, isApproved, isNotApproved, Dept_ID);
                BasePR.CacheData(key, totalCount);
            }
            return totalCount;
        }

        

        public static List<UserAccount1> GetUserAccountsByDeptName(String dept_name, string cSortExpression)
        {
            if (dept_name == null)
                dept_name = "";
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)

                cSortExpression = "cLastName";

            List<UserAccount1> UserAccounts = null;
            string key = "UserAccounts_UserAccount1" + dept_name.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccounts = (List<UserAccount1>)BizObject.Cache[key];
            }
            else
            {
               // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();

                List<UserAccountInfo1> recordset = SiteProvider.PR2.GetUserAccountsByDeptName(dept_name, cSortExpression);
                UserAccounts = GetUserAccountListFromUserAccountInfoList1(recordset);
                BasePR.CacheData(key, UserAccounts);
            }
            return UserAccounts;
        }


        public static void UpdatePrimaryDepartmentForUserID(int deptID, int userID)
        {
           // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            SiteProvider.PR2.UpdatePrimaryDepartmentForUserID(deptID, userID);
        }


        public static List<Department> GetUserDepartmentsByUserID(int userID)
        {
            List<Department> UserDepartments = null;
            string key = "UserAccounts_UserDepartments" + userID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserDepartments = (List<Department>)BizObject.Cache[key];
            }
            else
            {


                List<DepartmentInfo> recordset = SiteProvider.PR2.GetDepartmentsByUserID(userID);
                UserDepartments = Department.GetDepartmentListfromDepartmentInfo(recordset);
                BasePR.CacheData(key, UserDepartments);
            }
            return UserDepartments;
        }

        public static void UpdateDepartmentsForUserID(List<DepartmentInfo> departments, int userID)
        {

            SiteProvider.PR2.UpdateDepartmentsForUserID(departments, userID);
        }
       


        public static Boolean IsUnlimtedByUserId(int UserId)
        {
             return SiteProvider.PR2.IsUnlimtedByUserId(UserId);
        }

        public static int DailyPRUserAccountExpireDate()
        {
            return SiteProvider.PR2.DailyPRUserAccountExpireDate();
        }

        public static DataSet GetPRUserAccountExpireDate()
        {
            return SiteProvider.PR2.GetPRUserAccountExpireDate();
        }


        public static DataSet GetPRUserAccountExpireDateWithoutRenew()
        {
            return SiteProvider.PR2.GetPRUserAccountExpireDateWithoutRenew();
        }
        
        public static DataSet GetFollowupUserAccount()
        {
            return SiteProvider.PR2.GetFollowupUserAccount();
        }

        public static DataSet GetFollowupReminderUserAccount()
        {
            return SiteProvider.PR2.GetFollowupReminderUserAccount();
        }

    }

    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// UserEnrollment business object class
    ///// </summary>
    //public class UserEnrollment : BasePR
    //{
    //    public int testid { get; set; }
    //    public int iid { get; set; }
    //    private string _cfirstname = "";
    //    public string cfirstname { 
    //        get{return _cfirstname;}
    //        set { _cfirstname = value; }
    //    }
    //    private string _clastname = "";
    //    public string clastname
    //    {
    //        get { return _clastname; }
    //        set { _clastname = value; }
    //    }
    //    public int facilityid { get; set; }
    //    public string facilityname { get; set;}
    //    public int deptid { get; set;}
    //    private string _deptname = "";
    //    public string deptname
    //    {
    //        get { return _deptname; }
    //        set { _deptname = value; }
    //    }     
        
    //    public int topicid { get; set; }
    //    public string topicname { get; set; }
    //    public string coursenumber { get; set; }
    //    public string deadline { get; set; }
    //    public string complete { get; set; }
    //    public string enrolled { get; set; }
    //    public string cusername { get; set; }
    //    private string _email = "";
    //    public string email { 
    //        get{return _email;}
    //        set{_email=value;}
    //             }
    //    public UserEnrollment()
    //    {}

    //    public UserEnrollment(int iid, int testid, string firstname, string lastname, int fid, string fname, int deptid, string deptname,
    //        int topicid, string topicname, string coursenumber, string deadline,string complete )
    //    {
    //        this.iid = iid;
    //        this.testid = testid;
    //        this.cfirstname = firstname;
    //        this.clastname = lastname;
    //        this.facilityid = fid;
    //        this.facilityname = fname;
    //        this.deptid = deptid;
    //        this.deptname = deptname;
    //        this.topicid = topicid;
    //        this.topicname = topicname;
    //        this.coursenumber = coursenumber;
    //        this.deadline = deadline;
    //        this.complete = complete;

    //    }

    //    public UserEnrollment(int iid,string username, string firstname, string lastname, string deptname,string email,int facilityid)
    //    {
    //        this.iid = iid;
    //        this.cfirstname = firstname;
    //        this.clastname = lastname;
    //        this.deptname = deptname;
    //        this.cusername = username;
    //        this.facilityid = facilityid;
    //        this.email = email;
    //    }

    //    public UserEnrollment(string cfirstname, string clastname, string deptname, string email)
    //    {
    //        this.cfirstname = cfirstname;
    //        this.clastname = clastname;
    //        this.deptname = deptname;
    //        this.email = email;
    //    }

    //    public void AddEnrollStatus(string enrolled)
    //    {
    //        this.enrolled = enrolled;
    //    }

    //    public void AddTestid(int testid)
    //    {
    //        this.testid = testid;
    //    }

    //    private static UserEnrollment GetUserEnrollmentFromUserEnrollmentInfo(UserEnrollmentInfo record)
    //    {
    //        string deadline = record.deadline;
    //        DateTime dt = Convert.ToDateTime(deadline);
    //        record.deadline = dt.ToString("d");
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new UserEnrollment(
    //                record.iid,
    //                record.testid,
    //                record.cfirstname,
    //                record.clastname,
    //                record.facilityid,
    //                record.facilityname,
    //                record.deptid,
    //                record.deptname,
    //                record.topicid,
    //                record.topicname,
    //                record.coursenumber,
    //                record.deadline,
    //                record.complete
    //                );
    //        }
    //    }

    //    private static UserEnrollment GetStaffEnrollmentFromStaffEnrollmentInfo(UserEnrollmentInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new UserEnrollment(
    //                record.iid,
    //                record.cusername,
    //                record.cfirstname,
    //                record.clastname,
    //                record.deptname,    
    //                record.email,
    //                record.facilityid
    //                );
    //        }
    //    }


    //    private static UserEnrollment GetEnrolleeFromEnrolleeInfo(UserEnrollmentInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            if (record.email == null)
    //                record.email = "No email";
    //            return new UserEnrollment(            
    //                record.cfirstname,
    //                record.clastname,
    //                record.deptname,
    //                record.email
    //                );
    //        }
    //    }

    //    public static DataSet GetEnrolleesByUserid(ArrayList tests) 
    //    {
    //        DataSet result = new DataSet();
    //        foreach (int iid in tests)
    //        {
    //            DataSet temp = SiteProvider.PR2.GetEnrolleeByUserid(iid);
    //            result.Merge(temp);               
    //        }
    //        return result;
    //    }

    //    //private void AppendDataSet(DataSet target, DataSet source)
    //    //{
    //    //    foreach (DataRow row in source.Tables["enrollee"].Rows)
    //    //    {
               
    //    //    }
    //    //}

    //    //private static List<UserEnrollment> GetUserEnrollmentListFromUserEnrollmentInfoList(List<UserEnrollmentInfo> recordset)
    //    //{
    //    //    List<UserEnrollment> userEnrollments = new List<UserEnrollment>();
    //    //    foreach (UserEnrollmentInfo record in recordset)
    //    //        userEnrollments.Add(GetUserEnrollmentFromUserEnrollmentInfo(record));
    //    //    return userEnrollments;
    //    //}

    //    //private static List<UserEnrollment> GetStaffEnrollmentListFromStaffEnrollmentInfoList(List<UserEnrollmentInfo> recordset)
    //    //{
    //    //    List<UserEnrollment> userEnrollments = new List<UserEnrollment>();
    //    //    foreach (UserEnrollmentInfo record in recordset)
    //    //        userEnrollments.Add(GetStaffEnrollmentFromStaffEnrollmentInfo(record));
    //    //    return userEnrollments;
    //    //}

    //    //public static List<UserEnrollment> GetUserEnrollmentBySearchCriteria(string firstname, string lastname, int facilityid, int deptid, int topicid,
    //    //    int startrow, int maximumrows, string sortexpression,ref int numResults)
    //    //{
    //    //    if (sortexpression == null)
    //    //        sortexpression = "";

    //    //    if (sortexpression.Length == 0)
    //    //        sortexpression = "cLastName";

    //    //    List<UserEnrollmentInfo> userEnrollmentInfos = SiteProvider.PR2.GetUserEnrollmentsBySearechCriteria(firstname, lastname, facilityid, deptid, topicid,
    //    //    startrow, maximumrows, sortexpression,ref numResults);
    //    //     List<UserEnrollment> userEnrollments= GetUserEnrollmentListFromUserEnrollmentInfoList(userEnrollmentInfos);
    //    //     return userEnrollments;
    //    //}

    
    //    //public static DataSet GetStaffEnrollmentBySearchCriteria(string firstname, string lastname, int facilityid, int deptid, int topicid,
    //    //    int startrow, int maximumrows, string sortexpression, ref int numResults)
    //    //{
    //    //    if (sortexpression == null || sortexpression=="enrolled")
    //    //        sortexpression = "";

    //    //    if (sortexpression.Length == 0)
    //    //        sortexpression = "cLastName";

    //    //    DataSet userEnrollments = SiteProvider.PR2.GetStaffEnrollmentsBySearechCriteria(firstname, lastname, facilityid, deptid, topicid,
    //    //    startrow, maximumrows, sortexpression, ref numResults);
    //    //    UpdateTopicID(userEnrollments,topicid);
    //    //    AppendEnrollmentStatus(userEnrollments);
    //    //    if (sortexpression == "enrolled")
    //    //        userEnrollments.Tables[0].DefaultView.Sort = "enrolled";
    //    //    return userEnrollments;
    //    //}
    //    //update topic id to selected topic
    //    //private static void UpdateTopicID(DataSet userEnrollments, int topicid)
    //    //{
    //    //    userEnrollments.Tables[0].Columns.Add("topicid",typeof(int));

    //    //    foreach (DataRow  row in userEnrollments.Tables[0].Rows)
    //    //    {
                
    //    //          row["topicid"]  =topicid;
    //    //    }
    //    //}

    //    //private static void AppendEnrollmentStatus(DataSet userEnrollments)
    //    //{
    //    //    string enrolled;
    //    //    userEnrollments.Tables[0].Columns.Add("enrolled", typeof(string));
    //    //    userEnrollments.Tables[0].Columns.Add("testid", typeof(int));


    //    //    foreach (DataRow row in userEnrollments.Tables[0].Rows)
    //    //    {
    //    //       enrolled= SiteProvider.PR2.GetEnrollmentStatus(Convert.ToInt32(row["topicid"]), Convert.ToInt32(row["facilityid"]), Convert.ToInt32(row["iid"]));
    //    //       row["enrolled"] = enrolled;

    //    //       if (enrolled == "yes")
    //    //       {
    //    //           row["testid"]=SiteProvider.PR2.GetTestid(Convert.ToInt32(row["topicid"]), Convert.ToInt32(row["facilityid"]), Convert.ToInt32(row["iid"]));
    //    //       }
    //    //    }
    //    //}

    //    //public static bool DelelteEnrollment(int testid, int topicid)
    //    //{
    //    //    bool ret = SiteProvider.PR2.DeleteEnrollment(testid, topicid);

    //    //    return ret;
    //    //}
    //}
}