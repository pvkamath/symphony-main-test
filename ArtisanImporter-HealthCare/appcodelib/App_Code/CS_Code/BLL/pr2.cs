﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.QTI;

namespace PearlsReview.BLL
{
    //#region Class Discount

    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// Summary description for DiscountInfo
    ///// Bhaskar N
    ///// </summary>    
    //public class Discount : BasePR
    //{

    //    #region Private Variables &  Properties (Discount)

    //    private int _discountid = 0;
    //    public int DiscountId
    //    {
    //        get { return _discountid; }
    //        protected set { _discountid = value; }
    //    }

    //    private int _topicid = 0;
    //    public int TopicId
    //    {
    //        get { return _topicid; }
    //        protected set { _topicid = value; }
    //    }

    //    private string _course_number = "";
    //    public string course_number
    //    {
    //        get { return _course_number; }
    //        private set { _course_number = value; }
    //    }

    //    private string _tagline = "";
    //    public string TagLine
    //    {
    //        get { return _tagline; }
    //        private set { _tagline = value; }
    //    }

    //    private DateTime _startdate = System.DateTime.Now;
    //    public DateTime StartDate
    //    {
    //        get { return _startdate; }
    //        private set { _startdate = value; }
    //    }

    //    private DateTime _enddate = System.DateTime.Now;
    //    public DateTime EndDate
    //    {
    //        get { return _enddate; }
    //        private set { _enddate = value; }
    //    }


    //    private decimal _discount_price = 0.00M;
    //    public decimal discount_price
    //    {
    //        get { return _discount_price; }
    //        private set { _discount_price = value; }
    //    }

    //    private string _discount_type = "";
    //    public string discount_type
    //    {
    //        get { return _discount_type; }
    //        private set { _discount_type = value; }
    //    }

    //    private string _sponsor_text = "";
    //    public string Sponsor_Text
    //    {
    //        get { return _sponsor_text; }
    //        private set { _sponsor_text = value; }
    //    }

    //    private string _img_file = "";
    //    public string Img_File
    //    {
    //        get { return _img_file; }
    //        private set { _img_file = value; }
    //    }

    //    private string _discount_url = "";
    //    public string Discount_Url
    //    {
    //        get { return _discount_url; }
    //        private set { _discount_url = value; }
    //    }

    //    private bool _survey_ind = true;
    //    public bool Survey_Ind
    //    {
    //        get { return _survey_ind; }
    //        private set { _survey_ind = value; }
    //    }

    //    private int _impression = 0;
    //    public int Impression
    //    {
    //        get { return _impression; }
    //        protected set { _impression = value; }
    //    }

    //    private int _clickthrough = 0;
    //    public int ClickThrough
    //    {
    //        get { return _clickthrough; }
    //        protected set { _clickthrough = value; }
    //    }

    //    private string _username = "";
    //    public string UserName
    //    {
    //        get { return _username; }
    //        private set { _username = value; }
    //    }

    //    private string _userpassword = "";
    //    public string UserPassword
    //    {
    //        get { return _userpassword; }
    //        private set { _userpassword = value; }
    //    }


    //    private bool _section_ind = true;
    //    public bool Section_Ind
    //    {
    //        get { return _section_ind; }
    //        private set { _section_ind = value; }
    //    }


    //    //decimal(6, 2)..promotionsifo
    //    private decimal _offline_price = 0.00M;
    //    public decimal Offline_Price
    //    {
    //        get { return _offline_price; }
    //        private set { _offline_price = value; }
    //    }

    //    private bool _virtualurl_ind = true;
    //    public bool Virtualurl_Ind
    //    {
    //        get { return _virtualurl_ind; }
    //        private set { _virtualurl_ind = value; }
    //    }   

    //     public Discount(int discountid, int topicid,string course_number, string tagline, DateTime startdate, DateTime enddate, decimal discount_price,
    //        string discount_type, string sponsor_text, string img_file, string discount_url, bool survey_ind, int impression,
    //        int clickthrough, string username,  string userpassword, bool section_ind, decimal offline_price, bool virtualurl_ind)
    //    {
    //        this.DiscountId = discountid;
    //        this.TopicId = topicid;
    //        this.course_number = course_number;
    //        this.TagLine = tagline;
    //        this.StartDate = startdate;
    //        this.EndDate = enddate;
    //        this.discount_price = discount_price;
    //        this.discount_type = discount_type;
    //        this.Sponsor_Text = sponsor_text;
    //        this.Img_File = img_file;
    //        this.Discount_Url = discount_url;
    //        this.Survey_Ind = survey_ind;
    //        this.Impression = impression;
    //        this.ClickThrough = clickthrough;
    //        this.UserName = username;
    //        this.UserPassword = userpassword;
    //        this.Section_Ind = section_ind;
    //        this.Offline_Price = offline_price;
    //        this.Virtualurl_Ind = virtualurl_ind;
    //   }


    //     public bool Delete()
    //     {
    //         bool success = Discount.DeleteDiscount(this.DiscountId);
    //         if (success)
    //             this.DiscountId = 0;
    //         return success;
    //     }

    //     public bool Update()
    //     {
    //         return Discount.UpdateDiscount(this.DiscountId, this.TopicId , this.TagLine, this.StartDate, this.EndDate , this.discount_price ,
    //        this.discount_type, this.Sponsor_Text, this.Img_File, this.Discount_Url, this.Survey_Ind,this.Impression,
    //        this.ClickThrough , this.UserName, this.UserPassword,this.Section_Ind,this.Offline_Price ,this.Virtualurl_Ind);
    //     }

    //#endregion

    //    #region Static methods (Discount)  
        
    //     /***********************************
    //    * Static methods
    //    ************************************/

    //    //<summary>
    //    //Returns a collection with all Discounts
    //    //</summary>
    //     public static List<Discount> GetDiscounts(string cSortExpression)
    //     {
    //         if (cSortExpression == null)
    //             cSortExpression = "";

    //         // provide default sort
    //         if (cSortExpression.Length == 0)
    //             cSortExpression = "course_number";

    //         List<Discount> Discounts = null;
    //         string key = "Discounts_Discounts_" + cSortExpression.ToString();

    //         if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //         {
    //             Discounts = (List<Discount>)BizObject.Cache[key];
    //         }
    //         else
    //         {
    //             List<DiscountInfo> recordset = SiteProvider.PR.GetDiscounts(cSortExpression);
    //             Discounts = GetDiscountListFromDiscountInfoList(recordset);
    //             BasePR.CacheData(key, Discounts);
    //         }
    //         return Discounts;
    //     }

    //     /// <summary>
    //     /// Returns a Discount object with the specified ID
    //     /// </summary>
    //     public static Discount GetDiscountByID(int DiscountId)
    //     {
    //         Discount Discount = null;
    //         string key = "Discounts_Discount_" + DiscountId.ToString();

    //         if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //         {
    //             Discount = (Discount)BizObject.Cache[key];
    //         }
    //         else
    //         {
    //             Discount = GetDiscountFromDiscountInfo(SiteProvider.PR.GetDiscountByID(DiscountId));
    //             BasePR.CacheData(key, Discount);
    //         }
    //         return Discount;
    //     }

    //     public static DataSet GetDiscountByTopicID(string TopicID, int UrlIdentity, string UserName)
    //     {
    //         PearlsReview.DAL.SQLClient.SQLPRProvider obj = new PearlsReview.DAL.SQLClient.SQLPRProvider();
    //         return obj.GetDiscountByTopicID(TopicID, UrlIdentity, UserName);
    //     }

    //     /// <summary>
    //     /// Creates a new Discount
    //     /// </summary>
    //     public static int InsertDiscount(int topicid, string tagline, DateTime startdate, DateTime enddate, decimal discount_price,
    //        string discount_type, string sponsor_text, string img_file, string discount_url, bool survey_ind, int impression,
    //        int clickthrough, string username,  string userpassword, bool section_ind, decimal offline_price, bool virtualurl_ind)
    //     {
    //         //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
    //         //Comment = BizObject.ConvertNullToEmptyString(Comment);

    //         DiscountInfo record = new DiscountInfo(0,topicid,"", tagline, startdate, enddate, discount_price,
    //         discount_type, sponsor_text, img_file, discount_url, survey_ind, impression,
    //         clickthrough, username, userpassword, section_ind, offline_price, virtualurl_ind);
    //         int ret = SiteProvider.PR.InsertDiscount(record);

    //         BizObject.PurgeCacheItems("Discounts_Discount");
    //         return ret;
    //     }


    //     /// <summary>
    //     /// Updates an existing Discount
    //     /// </summary>
    //     public static bool UpdateDiscount(int discountid, int topicid, string tagline, DateTime startdate, DateTime enddate, decimal discount_price,
    //        string discount_type, string sponsor_text, string img_file, string discount_url, bool survey_ind, int impression,
    //        int clickthrough, string username,  string userpassword, bool section_ind, decimal offline_price, bool virtualurl_ind)
    //     {
    //         //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
    //         //Comment = BizObject.ConvertNullToEmptyString(Comment);

    //         DiscountInfo record = new DiscountInfo(discountid, topicid,"", tagline, startdate,enddate, discount_price,
    //         discount_type,sponsor_text, img_file,discount_url,survey_ind,impression,
    //         clickthrough, username,  userpassword, section_ind, offline_price, virtualurl_ind);
    //         bool ret = SiteProvider.PR.UpdateDiscount(record);

    //         BizObject.PurgeCacheItems("Discounts_Discount_" + discountid.ToString());
    //         BizObject.PurgeCacheItems("Discounts_Discounts");
    //         return ret;
    //     }

    //     /// <summary>
    //     /// Updates an existing Discount (Impression)
    //     /// </summary>
    //     public static bool UpdateDiscountByImpression(int Discountid, int Impression)
    //     {
    //         bool ret = SiteProvider.PR.UpdateDiscountByImpression(Discountid, Impression);
    //         return ret;
    //     }

    //     /// <summary>
    //     /// Updates an existing Discount (Impression)
    //     /// </summary>
    //     public static bool UpdateDiscountByClickThrough(int Discountid, int ClickThrough)
    //     {
    //         bool ret = SiteProvider.PR.UpdateDiscountByClickThrough(Discountid, ClickThrough);
    //         return ret;
    //     }

    //     /// <summary>
    //     /// Deletes an existing Discount, but first checks if OK to delete
    //     /// </summary>
    //     public static bool DeleteDiscount(int DiscountId)
    //     {
    //         bool IsOKToDelete = OKToDelete(DiscountId);
    //         if (IsOKToDelete)
    //         {
    //             return (bool)DeleteDiscount(DiscountId, true);
    //         }
    //         else
    //         {
    //             return false;
    //         }
    //     }

    //     /// <summary>
    //     /// Deletes an existing Discount - second param forces skip of OKToDelete
    //     /// (assuming that the calling program has already called that if it's
    //     /// passing the second param as true)
    //     /// </summary>
    //     public static bool DeleteDiscount(int DiscountId, bool SkipOKToDelete)
    //     {
    //         if (!SkipOKToDelete)
    //             return false;

    //         bool ret = SiteProvider.PR.DeleteDiscount(DiscountId);
    //         BizObject.PurgeCacheItems("Discount_Discount");
    //         return ret;
    //     }

    //     /// <summary>
    //     /// Checks to see if a Discount can be deleted safely
    //     /// (This default method just returns true. Certain entities
    //     /// might set datadict_tables.lNoOKDel to eliminate this
    //     /// default method and provide a custom version in
    //     /// datadict_tables.boExtra)
    //     /// </summary>
    //     public static bool OKToDelete(int EDefID)
    //     {
    //         return true;
    //     }



    //     /// <summary>
    //     /// Returns a Discount object filled with the data taken from the input DiscountInfo
    //     /// </summary>
    //     private static Discount GetDiscountFromDiscountInfo(DiscountInfo record)
    //     {
    //         if (record == null)
    //             return null;
    //         else
    //         {
    //             return new Discount(record.DiscountId, record.TopicId,record.course_number, record.TagLine, record.StartDate, record.EndDate,record.discount_price,
    //                 record.discount_type, record.Sponsor_Text, record.Img_File, record.Discount_Url, record.Survey_Ind, record.Impression,
    //                 record.ClickThrough, record.UserName, record.UserPassword, record.Section_Ind, record.Offline_Price, record.Virtualurl_Ind);
    //         }
    //     }

    //     /// <summary>
    //     /// Returns a list of Discount objects filled with the data taken from the input list of DiscountInfo
    //     /// </summary>
    //     private static List<Discount> GetDiscountListFromDiscountInfoList(List<DiscountInfo> recordset)
    //     {
    //         List<Discount> Discounts = new List<Discount>();
    //         foreach (DiscountInfo record in recordset)
    //             Discounts.Add(GetDiscountFromDiscountInfo(record));
    //         return Discounts;
    //     }

    //     /// <summary>
    //     /// Get Discount Id based On Topic Id
    //     /// </summary>
    //     public static int GetDiscountIdByTopicId(int TopicId)
    //     {
    //         int ret = SiteProvider.PR.GetDiscountIdByTopicId(TopicId);
    //         return ret;
    //     }
    //    #endregion
    //}        

    //#endregion

    //#region Class Position


    ///// <summary>
    ///// Summary description for pr2
    ///// </summary>
    //public class Position : BasePR
    //{
    //    private int _PositionID = 0;
    //    public int PositionID
    //    {
    //        get { return _PositionID; }
    //        set { _PositionID = value; }
    //    }

    //    private int _FacID = 0;
    //    public int FacID
    //    {
    //        get { return _FacID; }
    //        set { _FacID = value; }
    //    }
        
    //    private string _Position_name = "";
    //    public string Position_name
    //    {
    //        get { return _Position_name; }
    //        set { _Position_name = value; }
    //    }

    //    private bool _Position_active_ind = true;
    //    public bool  Position_active_ind
    //    {
    //        get { return _Position_active_ind; }
    //        set { _Position_active_ind = value; }
    //    }


    //   public Position(int PositionID, int FacID, string Position_name, bool Position_active_ind)
    //  {
    //        this.PositionID = PositionID;
    //        this.FacID = FacID;
    //        this.Position_name = Position_name;
    //        this.Position_active_ind = Position_active_ind;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = Position.DeletePosition(this.PositionID);
    //        if (success)
    //            this.PositionID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return Position.UpdatePosition(this.PositionID, this.FacID,this.Position_name,this.Position_active_ind);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all Positions
    //    /// </summary>
    //    public static List<Position> GetPositions(string fac_id,string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "position_name";

    //        List<Position> Positions = null;
    //        string key = "Position_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Positions = (List<Position>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<PositionInfo> recordset = SiteProvider.PR.GetPositions(fac_id,cSortExpression);
    //            Positions = GetPositionListFromPositionInfoList(recordset);
    //            BasePR.CacheData(key, Positions);
    //        }
    //        return Positions;
    //    }


    //    /// <summary>
    //    /// Returns the number of total Positions
    //    /// </summary>
    //    public static int GetPositionCount(int FacID)
    //    {
    //        int PositionCount = 0;
    //        string key = "Position_Count";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            PositionCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PositionCount = SiteProvider.PR.GetPositionCount(FacID);
    //            BasePR.CacheData(key, PositionCount);
    //        }
    //        return PositionCount;
    //    }

    //    /// <summary>
    //    /// Returns a Position object with the specified ID
    //    /// </summary>
    //    public static Position GetPositionByID(int PositionID)
    //    {
    //        Position Position = null;
    //        string key = "Positions_" + PositionID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Position = (Position)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            Position = GetPositionFromPositionInfo(SiteProvider.PR.GetPositionByID(PositionID));
    //            BasePR.CacheData(key, Position);
    //        }
    //        return Position;
    //    }

    //    /// <summary>
    //    /// Updates an existing Position
    //    /// </summary>
    //    public static bool UpdatePosition(int PositionID,int FacID, string Position_name,bool Position_active_ind)
    //    {
    //        Position_name = BizObject.ConvertNullToEmptyString(Position_name);


    //        PositionInfo record = new PositionInfo(PositionID, FacID, Position_name,Position_active_ind);
    //        bool ret = SiteProvider.PR.UpdatePosition(record);

    //        BizObject.PurgeCacheItems("Positions_" + PositionID.ToString());
    //        BizObject.PurgeCacheItems("Positions_Positions");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new Position
    //    /// </summary>
    //    public static int InsertPosition(int FacID,string Position_name,bool Position_active_ind)
    //    {
    //        Position_name = BizObject.ConvertNullToEmptyString(Position_name);


    //        PositionInfo record = new PositionInfo(0, FacID,Position_name,Position_active_ind);
    //        int ret = SiteProvider.PR.InsertPosition(record);

    //        BizObject.PurgeCacheItems("Positions_Position");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing Position, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeletePosition(int PositionID)
    //    {
    //        bool IsOKToDelete = OKToDelete(PositionID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeletePosition(PositionID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing Position - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeletePosition(int PositionID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeletePosition(PositionID);
    //        //         new RecordDeletedEvent("Position", PositionID, null).Raise();
    //        BizObject.PurgeCacheItems("Positions_Position");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a Position can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int PositionID)
    //    {
    //        return true;
    //    }


    //    private static Position GetPositionFromPositionInfo(PositionInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new Position(record.PositionID, record.FacID,record.Position_name,record.Position_active_ind);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of Position objects filled with the data taken from the input list of PositionInfo
    //    /// </summary>
    //    private static List<Position> GetPositionListFromPositionInfoList(List<PositionInfo> recordset)
    //    {
    //        List<Position> Positions = new List<Position>();
    //        foreach (PositionInfo record in recordset)
    //            Positions.Add(GetPositionFromPositionInfo(record));
    //        return Positions;
    //    }

    //}

    //#endregion

    //#region Class Parent Organization


    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// ParentOrganization business object class
    ///// ***** CEDIRECT VERSION ************
    ///// </summary>
    //public class ParentOrganization : BasePR
    //{
    //    private int _Parent_id = 0;
    //    public int Parent_id
    //    {
    //        get { return _Parent_id; }
    //        protected set { _Parent_id = value; }
    //    }

    //    private string _Parent_name = "";
    //    public string Parent_name
    //    {
    //        get { return _Parent_name; }
    //        set { _Parent_name = value; }
    //    }

    //    private string _address1 = "";
    //    public string address1
    //    {
    //        get { return _address1; }
    //        set { _address1 = value; }
    //    }

    //    private string _address2 = "";
    //    public string address2
    //    {
    //        get { return _address2; }
    //        set { _address2 = value; }
    //    }

    //    private string _city = "";
    //    public string city
    //    {
    //        get { return _city; }
    //        set { _city = value; }
    //    }

    //    private string _state = "";
    //    public string state
    //    {
    //        get { return _state; }
    //        set { _state = value; }
    //    }

    //    private string _zipcode = "";
    //    public string zipcode
    //    {
    //        get { return _zipcode; }
    //        set { _zipcode = value; }
    //    }

    //    private string _phone = "";
    //    public string phone
    //    {
    //        get { return _phone; }
    //        set { _phone = value; }
    //    }

    //    private string _contact_name = "";
    //    public string contact_name
    //    {
    //        get { return _contact_name; }
    //        set { _contact_name = value; }
    //    }

    //    private string _contact_phone = "";
    //    public string contact_phone
    //    {
    //        get { return _contact_phone; }
    //        set { _contact_phone = value; }
    //    }

    //    private string _contact_ext = "";
    //    public string contact_ext
    //    {
    //        get { return _contact_ext; }
    //        set { _contact_ext = value; }
    //    }

    //    private string _contact_email = "";
    //    public string contact_email
    //    {
    //        get { return _contact_email; }
    //        set { _contact_email = value; }
    //    }

    //    private bool _active = true;
    //    public bool active
    //    {
    //        get { return _active; }
    //        set { _active = value; }
    //    }

    //    private string _Comment = "";
    //    public string Comment
    //    {
    //        get { return _Comment; }
    //        set { _Comment = value; }
    //    }

    //    private int _MaxUsers = 1000;
    //    public int MaxUsers
    //    {
    //        get { return _MaxUsers; }
    //        set { _MaxUsers = value; }
    //    }

    //    private DateTime _Started = System.DateTime.MinValue;
    //    public DateTime Started
    //    {
    //        get { return _Started; }
    //        set { _Started = value; }
    //    }

    //    private DateTime _Expires = System.DateTime.MaxValue;
    //    public DateTime Expires
    //    {
    //        get { return _Expires; }
    //        set { _Expires = value; }
    //    }


    //    public ParentOrganization(int Parent_id, string Parent_name, string address1, string address2, string city, string state,
    //       string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
    //       string contact_email, bool active, string comment, int MaxUsers, DateTime Started,
    //        DateTime Expires)
    //    {
    //        this.Parent_id = Parent_id;
    //        this.Parent_name = Parent_name;
    //        this.address1 = address1;
    //        this.address2 = address2;
    //        this.city = city;
    //        this.state = state;
    //        this.zipcode = zipcode;
    //        this.phone = phone;
    //        this.contact_name = contact_name;
    //        this.contact_phone = contact_phone;
    //        this.contact_ext = contact_ext;
    //        this.contact_email = contact_email;
    //        this.active = active;
    //        this.Comment = comment;
    //        this.MaxUsers = MaxUsers;
    //        this.Started = Started;
    //        this.Expires = Expires;
    //    }

    //    public bool Delete()
    //    {
    //        bool success = ParentOrganization.DeleteParentOrganization(this.Parent_id);
    //        if (success)
    //            this.Parent_id = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return ParentOrganization.UpdateParentOrganization(this.Parent_id, this.Parent_name, this.address1, this.address2, this.city, this.state,
    //       this.zipcode, this.phone, this.contact_name, this.contact_phone, this.contact_ext,
    //       this.contact_email, this.active, this.Comment,this.MaxUsers, 
    //            this.Started, this.Expires);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all ParentOrganizations
    //    /// </summary>
    //    public static List<ParentOrganization> GetParentOrganizations(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "Parent_name";

    //        List<ParentOrganization> ParentOrganizations = null;
    //        string key = "ParentOrganizations_ParentOrganizations_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ParentOrganizations = (List<ParentOrganization>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<ParentOrganizationInfo> recordset = SiteProvider.PR.GetParentOrganizations(cSortExpression);
    //            ParentOrganizations = GetParentOrganizationListFromParentOrganizationInfoList(recordset);
    //            BasePR.CacheData(key, ParentOrganizations);
    //        }
    //        return ParentOrganizations;
    //    }

    //    /// <summary>
    //    /// Returns the number of total ParentOrganizations
    //    /// </summary>
    //    public static int GetParentOrganizationCount()
    //    {
    //        int ParentOrganizationCount = 0;
    //        string key = "ParentOrganizations_ParentOrganizationCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ParentOrganizationCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            ParentOrganizationCount = SiteProvider.PR.GetParentOrganizationCount();
    //            BasePR.CacheData(key, ParentOrganizationCount);
    //        }
    //        return ParentOrganizationCount;
    //    }

    //    /// <summary>
    //    /// Returns a ParentOrganization object with the specified ID
    //    /// </summary>
    //    public static ParentOrganization GetParentOrganizationByID(int Parent_id)
    //    {
    //        ParentOrganization ParentOrganization = null;
    //        string key = "ParentOrganizations_ParentOrganization_" + Parent_id.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ParentOrganization = (ParentOrganization)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            ParentOrganization = GetParentOrganizationFromParentOrganizationInfo(SiteProvider.PR.GetParentOrganizationByID(Parent_id));
    //            BasePR.CacheData(key, ParentOrganization);
    //        }
    //        return ParentOrganization;
    //    }

    //    /// <summary>
    //    /// Updates an existing ParentOrganization
    //    /// </summary>
    //    public static bool UpdateParentOrganization(int Parent_id, string Parent_name, string address1, string address2, string city, string state,
    //       string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
    //       string contact_email, bool active, string Comment, int MaxUsers, DateTime Started,
    //        DateTime Expires)
    //    {
    //        Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
    //        Comment = BizObject.ConvertNullToEmptyString(Comment);

    //        ParentOrganizationInfo record = new ParentOrganizationInfo(Parent_id, Parent_name, address1, address2, city, state,
    //           zipcode, phone, contact_name, contact_phone, contact_ext,
    //           contact_email, active, Comment, MaxUsers, Started,
    //            Expires);
    //        bool ret = SiteProvider.PR.UpdateParentOrganization(record);

    //        BizObject.PurgeCacheItems("ParentOrganizations_ParentOrganization_" + Parent_id.ToString());
    //        BizObject.PurgeCacheItems("ParentOrganizations_ParentOrganizations");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new ParentOrganization
    //    /// </summary>
    //    public static int InsertParentOrganization(string Parent_name, string address1, string address2, string city, string state,
    //       string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
    //       string contact_email, bool active, string Comment, int MaxUsers, DateTime Started,
    //        DateTime Expires)
    //    {
    //        Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
    //        Comment = BizObject.ConvertNullToEmptyString(Comment);

    //        ParentOrganizationInfo record = new ParentOrganizationInfo(0, Parent_name, address1, address2, city, state,
    //           zipcode, phone, contact_name, contact_phone, contact_ext,
    //           contact_email, active, Comment, MaxUsers, Started,
    //            Expires);
    //        int ret = SiteProvider.PR.InsertParentOrganization(record);

    //        BizObject.PurgeCacheItems("ParentOrganizations_ParentOrganization");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing ParentOrganization, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteParentOrganization(int Parent_id)
    //    {
    //        bool IsOKToDelete = OKToDelete(Parent_id);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteParentOrganization(Parent_id, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing ParentOrganization - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteParentOrganization(int Parent_id, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteParentOrganization(Parent_id);
    //        BizObject.PurgeCacheItems("ParentOrganizations_ParentOrganization");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a ParentOrganization can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int Parent_id)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a ParentOrganization object filled with the data taken from the input ParentOrganizationInfo
    //    /// </summary>
    //    private static ParentOrganization GetParentOrganizationFromParentOrganizationInfo(ParentOrganizationInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new ParentOrganization(record.Parent_id, record.Parent_name, record.address1, record.address2, record.city, record.state,
    //       record.zipcode, record.phone, record.contact_name, record.contact_phone, record.contact_ext,
    //       record.contact_email, record.active, record.Comment, record.MaxUsers,
    //            record.Started, record.Expires);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of ParentOrganization objects filled with the data taken from the input list of ParentOrganizationInfo
    //    /// </summary>
    //    private static List<ParentOrganization> GetParentOrganizationListFromParentOrganizationInfoList(List<ParentOrganizationInfo> recordset)
    //    {
    //        List<ParentOrganization> ParentOrganizations = new List<ParentOrganization>();
    //        foreach (ParentOrganizationInfo record in recordset)
    //            ParentOrganizations.Add(GetParentOrganizationFromParentOrganizationInfo(record));
    //        return ParentOrganizations;
    //    }

    //    /// <summary>
    //    /// Returns the number of total UserAccounts for a FacilityID
    //    /// </summary>
    //    public static int GetUserAccountCountByParentFacilityID(int FacilityID)
    //    {
    //        int UserAccountCount = 0;
    //        string key = "ParentOrganization_UserAccountCount_FacilityID_" + FacilityID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserAccountCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UserAccountCount = SiteProvider.PR.GetUserAccountCountByParentFacilityID(FacilityID);
    //            BasePR.CacheData(key, UserAccountCount);
    //        }
    //        return UserAccountCount;
    //    }

    //}

    //#endregion  

    //#region Class SponsorSurveyQue

    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// Summary description for SponsorSurveyQueInfo
    ///// Bhaskar N
    ///// </summary>    
    //public class SponsorSurveyQue : BasePR
    //{

    //    #region Private Variables &  Properties (SponsorSurveyQue)

    //    private int _questionid = 0;
    //    public int questionid
    //    {
    //        get { return _questionid; }
    //        protected set { _questionid = value; }
    //    }

    //    private string _survey_text = "";
    //    public string survey_text
    //    {
    //        get { return _survey_text; }
    //        private set { _survey_text = value; }
    //    }

    //    private bool _optional_ind = true;
    //    public bool optional_ind
    //    {
    //        get { return _optional_ind; }
    //        private set { _optional_ind = value; }
    //    }

    //    public SponsorSurveyQue(int questionid, string survey_text, bool optional_ind)
    //    {
    //        this.questionid = questionid;
    //        this.survey_text = survey_text;
    //        this.optional_ind = optional_ind;  
    //    }


    //    public bool Delete()
    //    {
    //        bool success = SponsorSurveyQue.DeleteSponsorSurveyQue(this.questionid);
    //        if (success)
    //            this.questionid = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return SponsorSurveyQue.UpdateSponsorSurveyQue(this.questionid, this.survey_text, this.optional_ind);
    //    }

    //    #endregion

    //    #region Static methods (SponsorSurveyQue)

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    //<summary>
    //    //Returns a collection with all SponsorSurveyQues
    //    //</summary>
    //    public static List<SponsorSurveyQue> GetSponsorSurveyQues(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "questionid";

    //        List<SponsorSurveyQue> SponsorSurveyQues = null;
    //        string key = "SponsorSurveyQues_SponsorSurveyQues_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SponsorSurveyQues = (List<SponsorSurveyQue>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SponsorSurveyQueInfo> recordset = SiteProvider.PR.GetSponsorSurveyQues(cSortExpression);
    //            SponsorSurveyQues = GetSponsorSurveyQueListFromSponsorSurveyQueInfoList(recordset);
    //            BasePR.CacheData(key, SponsorSurveyQues);
    //        }
    //        return SponsorSurveyQues;
    //    }

    //    //Returns a collection with all SponsorSurveyQues
    //    //</summary>
    //    //public static List<SponsorSurveyQue> GetSponsorSurveyQuesByDiscountId(string cSortExpression)
    //    public static List<SponsorSurveyQue> GetSponsorSurveyQuesByDiscountId(string cSortExpression, Int32 discountid)        
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "questionid";
    //        //int discountid = 1;

    //        List<SponsorSurveyQue> SponsorSurveyQues = null;
    //        string key = "SponsorSurveyQues_SponsorSurveyQues_" + discountid.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SponsorSurveyQues = (List<SponsorSurveyQue>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SponsorSurveyQueInfo> recordset = SiteProvider.PR.GetSponsorSurveyQuesByDiscountId(cSortExpression, discountid);
    //            SponsorSurveyQues = GetSponsorSurveyQueListFromSponsorSurveyQueInfoList(recordset);
    //            BasePR.CacheData(key, SponsorSurveyQues);
    //        }
    //        return SponsorSurveyQues;
    //    }

    //    /// <summary>
    //    /// Returns a SponsorSurveyQue object with the specified ID
    //    /// </summary>
    //    public static SponsorSurveyQue GetSponsorSurveyQueByID(int questionid)
    //    {
    //        SponsorSurveyQue SponsorSurveyQue = null;
    //        string key = "SponsorSurveyQues_SponsorSurveyQue_" + questionid.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SponsorSurveyQue = (SponsorSurveyQue)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            SponsorSurveyQue = GetSponsorSurveyQueFromSponsorSurveyQueInfo(SiteProvider.PR.GetSponsorSurveyQueByID(questionid));
    //            BasePR.CacheData(key, SponsorSurveyQue);
    //        }
    //        return SponsorSurveyQue;
    //    }

    //    /// <summary>
    //    /// Creates a new SponsorSurveyQue
    //    /// </summary>
    //    public static int InsertSponsorSurveyQue(string survey_text, bool optional_ind)
    //    {
    //        //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
    //        //Comment = BizObject.ConvertNullToEmptyString(Comment);

    //        SponsorSurveyQueInfo record = new SponsorSurveyQueInfo(0,survey_text,optional_ind);
    //        int ret = SiteProvider.PR.InsertSponsorSurveyQue(record);

    //        BizObject.PurgeCacheItems("SponsorSurveyQues_SponsorSurveyQue");
    //        return ret;
    //    }


    //    /// <summary>
    //    /// Updates an existing SponsorSurveyQue
    //    /// </summary>
    //    public static bool UpdateSponsorSurveyQue(int questionid, string survey_text, bool optional_ind)
    //    {
    //        //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
    //        //Comment = BizObject.ConvertNullToEmptyString(Comment);

    //        SponsorSurveyQueInfo record = new SponsorSurveyQueInfo(questionid, survey_text, optional_ind);
    //        bool ret = SiteProvider.PR.UpdateSponsorSurveyQue(record);

    //        BizObject.PurgeCacheItems("SponsorSurveyQues_SponsorSurveyQue_" + questionid.ToString());
    //        BizObject.PurgeCacheItems("SponsorSurveyQues_SponsorSurveyQues");
    //        return ret;
    //    }


    //    /// <summary>
    //    /// Deletes an existing SponsorSurveyQue, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteSponsorSurveyQue(int questionid)
    //    {
    //        bool IsOKToDelete = OKToDelete(questionid);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteSponsorSurveyQue(questionid, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing SponsorSurveyQue - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteSponsorSurveyQue(int questionid, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteSponsorSurveyQue(questionid);
    //        BizObject.PurgeCacheItems("SponsorSurveyQue_SponsorSurveyQue");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Checks to see if a SponsorSurveyQue can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int EDefID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a SponsorSurveyQue object filled with the data taken from the input SponsorSurveyQueInfo
    //    /// </summary>
    //    private static SponsorSurveyQue GetSponsorSurveyQueFromSponsorSurveyQueInfo(SponsorSurveyQueInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new SponsorSurveyQue(record.questionid, record.survey_text, record.optional_ind);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of SponsorSurveyQue objects filled with the data taken from the input list of SponsorSurveyQueInfo
    //    /// </summary>
    //    private static List<SponsorSurveyQue> GetSponsorSurveyQueListFromSponsorSurveyQueInfoList(List<SponsorSurveyQueInfo> recordset)
    //    {
    //        List<SponsorSurveyQue> SponsorSurveyQues = new List<SponsorSurveyQue>();
    //        foreach (SponsorSurveyQueInfo record in recordset)
    //            SponsorSurveyQues.Add(GetSponsorSurveyQueFromSponsorSurveyQueInfo(record));
    //        return SponsorSurveyQues;
    //    }
    //    #endregion
    //}

    //#endregion

    //#region Class SponsorSurveyAns

    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// Summary description for SponsorSurveyAnsInfo
    ///// Bhaskar N
    ///// </summary>    
    //public class SponsorSurveyAns : BasePR
    //{

    //    #region Private Variables &  Properties (SponsorSurveyAns)

    //    private int _answerid = 0;
    //    public int answerid
    //    {
    //        get { return _answerid; }
    //        protected set { _answerid = value; }
    //    }

    //    private int _questionid = 0;
    //    public int questionid
    //    {
    //        get { return _questionid; }
    //        protected set { _questionid = value; }
    //    }

    //    private string _answer_text = "";
    //    public string answer_text
    //    {
    //        get { return _answer_text; }
    //        private set { _answer_text = value; }
    //    }

    //    private bool _share_ind = true;
    //    public bool share_ind
    //    {
    //        get { return _share_ind; }
    //        private set { _share_ind = value; }
    //    }

    //    public SponsorSurveyAns(int answerid, int questionid, string answer_text, bool share_ind)
    //    {
    //        this.answerid = answerid;
    //        this.questionid = questionid;
    //        this.answer_text = answer_text;
    //        this.share_ind = share_ind;   
    //    }


    //    public bool Delete()
    //    {
    //        bool success = SponsorSurveyAns.DeleteSponsorSurveyAns(this.questionid);
    //        if (success)
    //            this.questionid = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return SponsorSurveyAns.UpdateSponsorSurveyAns(this.answerid,this.questionid, this.answer_text, this.share_ind);
    //    }

    //    #endregion

    //    #region Static methods (SponsorSurveyAns)

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    //<summary>
    //    //Returns a collection with all SponsorSurveyAns
    //    //</summary>
    //    public static List<SponsorSurveyAns> GetSponsorSurveyAns(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "answerid";

    //        List<SponsorSurveyAns> SponsorSurveyAns = null;
    //        string key = "SponsorSurveyAns_SponsorSurveyAns_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SponsorSurveyAns = (List<SponsorSurveyAns>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SponsorSurveyAnsInfo> recordset = SiteProvider.PR.GetSponsorSurveyAns(cSortExpression);
    //            SponsorSurveyAns = GetSponsorSurveyAnsListFromSponsorSurveyAnsInfoList(recordset);
    //            BasePR.CacheData(key, SponsorSurveyAns);
    //        }
    //        return SponsorSurveyAns;
    //    }

    //    //<summary>
    //    //Returns a collection with all SponsorSurveyAns
    //    //</summary>
    //    public static List<SponsorSurveyAns> GetSponsorSurveyAnsByQuestionId(int questionid)
    //    {

    //        List<SponsorSurveyAns> SponsorSurveyAns = null;
    //        string key = "SponsorSurveyAns_SponsorSurveyAns_" + questionid.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SponsorSurveyAns = (List<SponsorSurveyAns>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SponsorSurveyAnsInfo> recordset = SiteProvider.PR.GetSponsorSurveyAnsByQuestionId(questionid);
    //            SponsorSurveyAns = GetSponsorSurveyAnsListFromSponsorSurveyAnsInfoList(recordset);
    //            BasePR.CacheData(key, SponsorSurveyAns);
    //        }
    //        return SponsorSurveyAns;
    //    }
    //    /// <summary>
    //    /// Returns a SponsorSurveyAns object with the specified ID
    //    /// </summary>
    //    public static SponsorSurveyAns GetSponsorSurveyAnsByID(int questionid)
    //    {
    //        SponsorSurveyAns SponsorSurveyAns = null;
    //        string key = "SponsorSurveyAns_SponsorSurveyAns_" + questionid.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SponsorSurveyAns = (SponsorSurveyAns)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            SponsorSurveyAns = GetSponsorSurveyAnsFromSponsorSurveyAnsInfo(SiteProvider.PR.GetSponsorSurveyAnsByID(questionid));
    //            BasePR.CacheData(key, SponsorSurveyAns);
    //        }
    //        return SponsorSurveyAns;
    //    }

    //    /// <summary>
    //    /// Creates a new SponsorSurveyAns
    //    /// </summary>
    //    public static int InsertSponsorSurveyAns(int questionid, string answer_text, bool share_ind)
    //    {
    //        //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
    //        //Comment = BizObject.ConvertNullToEmptyString(Comment);

    //        SponsorSurveyAnsInfo record = new SponsorSurveyAnsInfo(0, questionid, answer_text, share_ind);
    //        int ret = SiteProvider.PR.InsertSponsorSurveyAns(record);

    //        BizObject.PurgeCacheItems("SponsorSurveyAns_SponsorSurveyAns");
    //        return ret;
    //    }


    //    /// <summary>
    //    /// Updates an existing SponsorSurveyAns
    //    /// </summary>
    //    public static bool UpdateSponsorSurveyAns(int answerid, int questionid, string answer_text, bool share_ind)
    //    {
    //        //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
    //        //Comment = BizObject.ConvertNullToEmptyString(Comment);

    //        SponsorSurveyAnsInfo record = new SponsorSurveyAnsInfo(answerid, questionid, answer_text, share_ind);
    //        bool ret = SiteProvider.PR.UpdateSponsorSurveyAns(record);

    //        BizObject.PurgeCacheItems("SponsorSurveyAns_SponsorSurveyAns_" + questionid.ToString());
    //        BizObject.PurgeCacheItems("SponsorSurveyAns_SponsorSurveyAns");
    //        return ret;
    //    }


    //    /// <summary>
    //    /// Deletes an existing SponsorSurveyAns, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteSponsorSurveyAns(int questionid)
    //    {
    //        bool IsOKToDelete = OKToDelete(questionid);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteSponsorSurveyAns(questionid, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing SponsorSurveyAns - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteSponsorSurveyAns(int questionid, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteSponsorSurveyAns(questionid);
    //        BizObject.PurgeCacheItems("SponsorSurveyAns_SponsorSurveyAns");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Checks to see if a SponsorSurveyAns can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int EDefID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a SponsorSurveyAns object filled with the data taken from the input SponsorSurveyAnsInfo
    //    /// </summary>
    //    private static SponsorSurveyAns GetSponsorSurveyAnsFromSponsorSurveyAnsInfo(SponsorSurveyAnsInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new SponsorSurveyAns(record.answerid ,record.questionid, record.answer_text, record.share_ind);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of SponsorSurveyAns objects filled with the data taken from the input list of SponsorSurveyAnsInfo
    //    /// </summary>
    //    private static List<SponsorSurveyAns> GetSponsorSurveyAnsListFromSponsorSurveyAnsInfoList(List<SponsorSurveyAnsInfo> recordset)
    //    {
    //        List<SponsorSurveyAns> SponsorSurveyAns = new List<SponsorSurveyAns>();
    //        foreach (SponsorSurveyAnsInfo record in recordset)
    //            SponsorSurveyAns.Add(GetSponsorSurveyAnsFromSponsorSurveyAnsInfo(record));
    //        return SponsorSurveyAns;
    //    }
    //    #endregion
    //}

    //#endregion

    //#region Class SponsorSurveys

    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// Summary description for SponsorSurveysInfo
    ///// Bhaskar N
    ///// </summary>    
    //public class SponsorSurveys : BasePR
    //{

    //    #region Private Variables &  Properties (SponsorSurveys)

    //    private int _discountid = 0;
    //    public int discountid
    //    {
    //        get { return _discountid; }
    //        protected set { _discountid = value; }
    //    }

    //    private int _questionid = 0;
    //    public int questionid
    //    {
    //        get { return _questionid; }
    //        protected set { _questionid = value; }
    //    }  

    //    public SponsorSurveys(int discountid, int questionid)
    //    {
    //        this.discountid = discountid;
    //        this.questionid = questionid;            
    //    }


    //    public bool Delete()
    //    {
    //        bool success = SponsorSurveys.DeleteSponsorSurveysByDiscountidAndQuestionid(this.discountid, this.questionid);
    //        if (success)
    //            this.questionid = 0;
    //        return success;
    //    }        

    //    #endregion

    //    #region Static methods (SponsorSurveys)

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    //<summary>
    //    //Returns a collection with all SponsorSurveys
    //    //</summary>
    //    public static List<SponsorSurveys> GetSponsorSurveys(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "questionid";

    //        List<SponsorSurveys> SponsorSurveys = null;
    //        string key = "SponsorSurveys_SponsorSurveys_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SponsorSurveys = (List<SponsorSurveys>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SponsorSurveysInfo> recordset = SiteProvider.PR.GetSponsorSurveys(cSortExpression);
    //            SponsorSurveys = GetSponsorSurveysListFromSponsorSurveysInfoList(recordset);
    //            BasePR.CacheData(key, SponsorSurveys);
    //        }
    //        return SponsorSurveys;
    //    }

    //    //<summary>
    //    //Returns a collection with all SponsorSurveys
    //    //</summary>
    //    public static List<SponsorSurveys> GetSponsorSurveysByDiscountIdAndQuestionId(int discountid, int questionid)
    //    {

    //        List<SponsorSurveys> SponsorSurveys = null;
    //        string key = "SponsorSurveys_SponsorSurveys_" + questionid.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SponsorSurveys = (List<SponsorSurveys>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SponsorSurveysInfo> recordset = SiteProvider.PR.GetSponsorSurveysByDiscountIdAndQuestionId(discountid,questionid);
    //            SponsorSurveys = GetSponsorSurveysListFromSponsorSurveysInfoList(recordset);
    //            BasePR.CacheData(key, SponsorSurveys);
    //        }
    //        return SponsorSurveys;
    //    }

    //    //<summary>
    //    //Returns a collection with all SponsorSurveys
    //    //</summary>
    //    public static List<SponsorSurveys> GetSponsorSurveysByDiscountId(int discountid)
    //    {

    //        List<SponsorSurveys> SponsorSurveys = null;
    //        string key = "SponsorSurveys_SponsorSurveys_" + discountid.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SponsorSurveys = (List<SponsorSurveys>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SponsorSurveysInfo> recordset = SiteProvider.PR.GetSponsorSurveysByDiscountId(discountid);
    //            SponsorSurveys = GetSponsorSurveysListFromSponsorSurveysInfoList(recordset);
    //            BasePR.CacheData(key, SponsorSurveys);
    //        }
    //        return SponsorSurveys;
    //    } 

    //    /// <summary>
    //    /// Creates a new SponsorSurveys
    //    /// </summary>
    //    public static bool InsertSponsorSurveys(int discountid,int questionid)
    //    {
    //        //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
    //        //Comment = BizObject.ConvertNullToEmptyString(Comment);

    //        SponsorSurveysInfo record = new SponsorSurveysInfo(discountid, questionid);
    //        bool ret = SiteProvider.PR.InsertSponsorSurveys(record);

    //        BizObject.PurgeCacheItems("SponsorSurveys_SponsorSurveys");
    //        return ret;
    //    }


    //    /// <summary>
    //    /// Deletes an existing SponsorSurveys, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteSponsorSurveysByDiscountidAndQuestionid(int discountid, int questionid)
    //    {
    //        bool IsOKToDelete = OKToDelete(questionid);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteSponsorSurveysByDiscountidAndQuestionid(discountid,questionid, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing SponsorSurveys - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteSponsorSurveysByDiscountidAndQuestionid(int discountid, int questionid, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteSponsorSurveysByDiscountidAndQuestionid(discountid,questionid);
    //        BizObject.PurgeCacheItems("SponsorSurveys_SponsorSurveys");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Checks to see if a SponsorSurveys can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int EDefID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a SponsorSurveys object filled with the data taken from the input SponsorSurveysInfo
    //    /// </summary>
    //    private static SponsorSurveys GetSponsorSurveysFromSponsorSurveysInfo(SponsorSurveysInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new SponsorSurveys(record.discountid, record.questionid);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of SponsorSurveys objects filled with the data taken from the input list of SponsorSurveysInfo
    //    /// </summary>
    //    private static List<SponsorSurveys> GetSponsorSurveysListFromSponsorSurveysInfoList(List<SponsorSurveysInfo> recordset)
    //    {
    //        List<SponsorSurveys> SponsorSurveys = new List<SponsorSurveys>();
    //        foreach (SponsorSurveysInfo record in recordset)
    //            SponsorSurveys.Add(GetSponsorSurveysFromSponsorSurveysInfo(record));
    //        return SponsorSurveys;
    //    }
    //    #endregion
    //}

    //#endregion

    //#region Class SponsorSurveyRsp

    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// Summary description for SponsorSurveyRsp
    ///// Bhaskar N
    ///// </summary>    
    //public class SponsorSurveyRsp : BasePR
    //{

    //    #region Private Variables &  Properties (SponsorSurveyRsp)
        
    //    #endregion

    //    #region Static methods (SponsorSurveyRsp)

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public static bool InsertSponsorSurveyRsp(int userid, int orderitemid, int questionid, int answerid, int discountid)
    //    {
    //        bool ret = SiteProvider.PR.InsertSponsorSurveyRsp(userid, orderitemid, questionid, answerid, discountid);
    //        // TODO: release cache?
    //        return ret;
    //    }

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public static bool UpdateSponsorSurveyRspByOrderitemId(int userid, int orderitemid, int discountid)
    //    {
    //        bool ret = SiteProvider.PR.UpdateSponsorSurveyRspByOrderitemId(userid, orderitemid, discountid);
    //        // TODO: release cache?
    //        return ret;
    //    }

    //    #endregion
    //}

    //#endregion

    //#region Class StateReq

    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// Summary description for StateReq
    ///// Bhaskar N
    ///// </summary>    
    //public class StateReq : BasePR
    //{

    //    #region Private Variables &  Properties (StateReq)

    //    private string _stateabr = "";
    //    public string StateAbr
    //    {
    //        get { return _stateabr; }
    //        private set { _stateabr = value; }
    //    }

    //    private string _statename = "";
    //    public string StateName
    //    {
    //        get { return _statename; }
    //        private set { _statename = value; }
    //    }

    //    private string _cereq = "";
    //    public string CeReq
    //    {
    //        get { return _cereq; }
    //        private set { _cereq = value; }
    //    }

    //    private string _licagency = "";
    //    public string LicAgency
    //    {
    //        get { return _licagency; }
    //        private set { _licagency = value; }
    //    }

    //    private string _permlic = "";
    //    public string Permlic
    //    {
    //        get { return _permlic; }
    //        private set { _permlic = value; }
    //    }

    //    private string _templic = "";
    //    public string TempLic
    //    {
    //        get { return _templic; }
    //        private set { _templic = value; }
    //    }

    //    private string _extralic = "";
    //    public string ExtraLic
    //    {
    //        get { return _extralic; }
    //        private set { _extralic = value; }
    //    }

    //    private string _email = "";
    //    public string Email
    //    {
    //        get { return _email; }
    //        private set { _email = value; }
    //    }

    //    private string _url = "";
    //    public string Url
    //    {
    //        get { return _url; }
    //        private set { _url = value; }
    //    }

    //    private string _lpn = "";
    //    public string Lpn
    //    {
    //        get { return _lpn; }
    //        private set { _lpn = value; }
    //    }

    //    private DateTime _lastupdate = System.DateTime.Now;
    //    public DateTime LastUpdate
    //    {
    //        get { return _lastupdate; }
    //        private set { _lastupdate = value; }
    //    }

    //    private int _categoryid = 0;
    //    public int CategoryId
    //    {
    //        get { return _categoryid; }
    //        protected set { _categoryid = value; }
    //    }

    //    private decimal _taxrate = 0.00M;
    //    public decimal TaxRate
    //    {
    //        get { return _taxrate; }
    //        private set { _taxrate = value; }
    //    }

    //    private bool _inter_ind = true;
    //    public bool Inter_Ind
    //    {
    //        get { return _inter_ind; }
    //        private set { _inter_ind = value; }
    //    }

    //    private string _req_header = "";
    //    public string req_header
    //    {
    //        get { return _req_header; }
    //        private set { _req_header = value; }
    //    }

    //    private string _course_recommend = "";
    //    public string course_recommend
    //    {
    //        get { return _course_recommend; }
    //        private set { _course_recommend = value; }
    //    }

    //    public StateReq(string stateabr, string statename, string cereq, string licagency, string permlic, string templic,
    //      string extralic, string email, string url, string lpn, DateTime lastupdate, int categoryid, decimal taxrate, bool inter_ind, string req_header, string course_recommend)
    //    {
    //        this.StateAbr = stateabr;
    //        this.StateName = statename;
    //        this.CeReq = cereq;
    //        this.LicAgency = licagency;
    //        this.Permlic = permlic;
    //        this.TempLic = templic;
    //        this.ExtraLic = extralic;
    //        this.Email = email;
    //        this.Url = url;
    //        this.Lpn = lpn;
    //        this.LastUpdate = lastupdate;
    //        this.CategoryId = categoryid;
    //        this.TaxRate = taxrate;
    //        this.Inter_Ind = inter_ind;
    //        this.req_header = req_header;
    //        this.course_recommend = course_recommend;
    //    }
    //    public bool Delete()
    //    {
    //        bool success = StateReq.DeleteStateReq(this.StateAbr);
    //        if (success)
    //            this.StateAbr = "";
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return StateReq.UpdateStateReq(this.StateAbr, this.StateName, this.CeReq, this.LicAgency, this.Permlic, this.TempLic,
    //       this.ExtraLic, this.Email, this.Url, this.Lpn, this.LastUpdate, this.CategoryId, this.TaxRate, this.Inter_Ind, this.req_header, this.course_recommend);
    //    }

    //    #endregion


    //    #region Static methods (StateReq)

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    //<summary>
    //    //Returns a collection with all StateReqs
    //    //</summary>
    //    public static List<StateReq> GetStateReqs(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "statename";

    //        List<StateReq> StateReqs = null;
    //        string key = "StateReqs_StateReqs_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            StateReqs = (List<StateReq>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<StateReqInfo> recordset = SiteProvider.PR.GetStateReqs(cSortExpression);
    //            StateReqs = GetStateReqListFromStateReqInfoList(recordset);
    //            BasePR.CacheData(key, StateReqs);
    //        }
    //        return StateReqs;
    //    }

    //    /// <summary>
    //    /// Returns a StateReq object with the specified ID
    //    /// </summary>
    //    public static StateReq GetStateReqByStateAbr(string StateAbr)
    //    {
    //        StateReq StateReq = null;
    //        string key = "StateReqs_StateReq_" + StateAbr.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            StateReq = (StateReq)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            StateReq = GetStateReqFromStateReqInfo(SiteProvider.PR.GetStateReqByStateAbr(StateAbr));
    //            BasePR.CacheData(key, StateReq);
    //        }
    //        return StateReq;
    //    }


    //    /// <summary>
    //    /// Updates an existing StateReq
    //    /// </summary>
    //    public static bool UpdateStateReq(string stateabr, string statename, string cereq, string licagency, string permlic, string templic,
    //       string extralic, string email, string url, string lpn, DateTime lastupdate, int categoryid, decimal taxrate, bool inter_ind, string req_header, string course_recommend)
    //    {
    //        //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
    //        //Comment = BizObject.ConvertNullToEmptyString(Comment);

    //        StateReqInfo record = new StateReqInfo(stateabr, statename, cereq, licagency, permlic, templic,
    //        extralic, email, url, lpn, lastupdate, categoryid, taxrate, inter_ind, req_header, course_recommend);
    //        bool ret = SiteProvider.PR.UpdateStateReq(record);

    //        BizObject.PurgeCacheItems("StateReqs_StateReq_" + stateabr.ToString());
    //        BizObject.PurgeCacheItems("StateReqs_StateReqs");
    //        return ret;
    //    }


    //    public static bool UpdateStateReqByStateAbr(string Cereq, string Email, string Url, string LicAgency, string Lpn, string Stateabr, int CategoryId, string req_header, string course_recommend)
    //    {
    //        bool ret = SiteProvider.PR.UpdateStateReqByStateAbr(Cereq, Email, Url, LicAgency, Lpn, Stateabr, CategoryId, req_header, course_recommend);
    //        // TODO: release cache?
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing StateReq, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteStateReq(string stateabr)
    //    {
    //        bool IsOKToDelete = OKToDelete(stateabr);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteStateReq(stateabr, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing StateReq - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteStateReq(string stateabr, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteStateReq(stateabr);
    //        BizObject.PurgeCacheItems("StateReq_StateReq");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Checks to see if a StateReq can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(string EDefID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a StateReq object filled with the data taken from the input StateReqInfo
    //    /// </summary>
    //    private static StateReq GetStateReqFromStateReqInfo(StateReqInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new StateReq(record.StateAbr, record.StateName, record.CeReq, record.LicAgency, record.Permlic, record.TempLic,
    //                record.ExtraLic, record.Email, record.Url, record.Lpn, record.LastUpdate, record.CategoryId, record.TaxRate, record.Inter_Ind, record.req_header, record.course_recommend);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of StateReq objects filled with the data taken from the input list of StateReqInfo
    //    /// </summary>
    //    private static List<StateReq> GetStateReqListFromStateReqInfoList(List<StateReqInfo> recordset)
    //    {
    //        List<StateReq> StateReqs = new List<StateReq>();
    //        foreach (StateReqInfo record in recordset)
    //            StateReqs.Add(GetStateReqFromStateReqInfo(record));
    //        return StateReqs;
    //    }


    //    /// <summary>
    //    /// Returns a StateReq object with the specified Category ID
    //    /// </summary>
    //    public static StateReq GetStateReqByCategoryId(int CategoryId)
    //    {
    //        StateReq StateReq = null;
    //        string key = "StateReqs_StateReq_" + CategoryId.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            StateReq = (StateReq)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            StateReq = GetStateReqFromStateReqInfo(SiteProvider.PR.GetStateReqByCategoryId(CategoryId));
    //            BasePR.CacheData(key, StateReq);
    //        }
    //        return StateReq;
    //    }
      


    //    #endregion

    //}
    //#endregion


    //#region Class SurveyQuestion
    ///////////////////////////////////////////////////////////////
    ///// <summary>
    ///// SurveyQuestion business object class
    ///// </summary>
    //public class SurveyQuestion : BasePR
    //{
    //    #region Private Variables &  Properties (SurveyQuestion)

    //    private int _SurveyID = 0;
    //    public int SurveyID
    //    {
    //        get { return _SurveyID; }
    //        protected set { _SurveyID = value; }
    //    }

    //    private int _QuestionNum = 0;
    //    public int QuestionNum
    //    {
    //        get { return _QuestionNum; }
    //        set { _QuestionNum = value; }
    //    }

    //    private string _Question = "";
    //    public string Question
    //    {
    //        get { return _Question; }
    //        set { _Question = value; }
    //    }

    //    private string _Answer_A = "";
    //    public string Answer_A
    //    {
    //        get { return _Answer_A; }
    //        set { _Answer_A = value; }
    //    }

    //    private string _Answer_B = "";
    //    public string Answer_B
    //    {
    //        get { return _Answer_B; }
    //        set { _Answer_B = value; }
    //    }

    //    private string _Answer_C = "";
    //    public string Answer_C
    //    {
    //        get { return _Answer_C; }
    //        set { _Answer_C = value; }
    //    }

    //    private string _Answer_D = "";
    //    public string Answer_D
    //    {
    //        get { return _Answer_D; }
    //        set { _Answer_D = value; }
    //    }

    //    private string _Answer_E = "";
    //    public string Answer_E
    //    {
    //        get { return _Answer_E; }
    //        set { _Answer_E = value; }
    //    }

    //    private string _Answer_F = "";
    //    public string Answer_F
    //    {
    //        get { return _Answer_F; }
    //        set { _Answer_F = value; }
    //    }

    //    private string _RecordOrder = "";
    //    public string RecordOrder
    //    {
    //        get { return _RecordOrder; }
    //        set { _RecordOrder = value; }
    //    }

    //    private string _Control_Type = "";
    //    public string Control_Type
    //    {
    //        get { return _Control_Type; }
    //        set { _Control_Type = value; }
    //    }

    //    public SurveyQuestion(int SurveyID, int QuestionNum, string Question, string Answer_A, string Answer_B, string Answer_C,
    //        string Answer_D, string Answer_E, string Answer_F, string RecordOrder, string Control_Type)
    //    {
    //        this.SurveyID = SurveyID;
    //        this.QuestionNum = QuestionNum;
    //        this.Question = Question;
    //        this.Answer_A = Answer_A;
    //        this.Answer_B = Answer_B;
    //        this.Answer_C = Answer_C;
    //        this.Answer_D = Answer_D;
    //        this.Answer_E = Answer_E;
    //        this.Answer_F = Answer_F;
    //        this.RecordOrder = RecordOrder;
    //        this.Control_Type = Control_Type;
    //    }

    //    #endregion

    //    #region Static methods (SurveyQuestion)

    //    /// <summary>
    //    /// Returns a collection with all Survey Questions
    //    /// </summary>
    //    public static List<SurveyQuestion> GetSurveyQuestionsByID(int SurveyId)
    //    {
    //        List<SurveyQuestion> SurveyQuestions = new List<SurveyQuestion>();
    //        SQTIUtils oSQTIUtils = new SQTIUtils();
    //        SurveyDefinition SurveyDef = SurveyDefinition.GetSurveyDefinitionByID(SurveyId);
    //        List<SQTIQuestionObject> SQTIQuestions = new List<SQTIQuestionObject>();
    //        if (SurveyDef == null || SurveyDef.XMLSurvey.ToString() == "")
    //        {
    //            SQTIQuestionObject SQTIQuestion;
    //            SQTIQuestion = new SQTIQuestionObject();
    //        }
    //        else
    //        {
    //            SQTIQuestions = oSQTIUtils.ConvertQTITestXMLToSQTIQuestionObjectList(SurveyDef.XMLSurvey.ToString());
    //        }
    //        if (SQTIQuestions.Count > 0)
    //        {
    //            for (int i = 1; i <= SQTIQuestions.Count; i++)
    //            {
    //                SurveyQuestions.Add(CreateNewMultipleChoiceQuestion(SurveyId, i));
    //            }
    //            int iOrder = 0;
    //            foreach (SQTIQuestionObject SQTIQuestion in SQTIQuestions)
    //            {
    //                SurveyQuestions[iOrder].Question = SQTIQuestion.cQuestionText;
    //                SurveyQuestions[iOrder].Answer_A = SQTIQuestion.cAnswer_A_Text;
    //                SurveyQuestions[iOrder].Answer_B = SQTIQuestion.cAnswer_B_Text;
    //                SurveyQuestions[iOrder].Answer_C = SQTIQuestion.cAnswer_C_Text;
    //                SurveyQuestions[iOrder].Answer_D = SQTIQuestion.cAnswer_D_Text;
    //                SurveyQuestions[iOrder].Answer_E = SQTIQuestion.cAnswer_E_Text;
    //                SurveyQuestions[iOrder].Answer_F = SQTIQuestion.cAnswer_F_Text;
    //                SurveyQuestions[iOrder].RecordOrder = SQTIQuestion.cRecordOrder;
    //                SurveyQuestions[iOrder].Control_Type = SQTIQuestion.cControl_Type;
    //                iOrder++;
    //            }
    //        }
    //        return SurveyQuestions;
    //    }

    //    /// <summary>
    //    /// Returns a new multiple choice SurveyQuestion object
    //    /// </summary>
    //    private static SurveyQuestion CreateNewMultipleChoiceQuestion(int SurveyID, int QuestionNum)
    //    {
    //        SurveyQuestion objSurveyQuestion = new SurveyQuestion(SurveyID, QuestionNum, "", "",
    //            "", "", "", "", "", "", "");

    //        return objSurveyQuestion;
    //    }

    //    public static bool UpdateSurveyQuestion(int SurveyID, int QuestionNum, string Question, string Answer_A, string Answer_B,
    //        string Answer_C, string Answer_D, string Answer_E, string RecordOrder, string Control_Type)
    //    {
    //        List<SurveyQuestion> SurveyQuestions = GetSurveyQuestionsByID(SurveyID);
    //        int iQuestionPointer;
    //        if (QuestionNum > SurveyQuestions.Count)
    //        {
    //            SurveyQuestion objSurveyQuestion = new SurveyQuestion(SurveyID, QuestionNum, Question, Answer_A,
    //                    Answer_B, Answer_C, Answer_D, Answer_E, "", RecordOrder, Control_Type);

    //            SurveyQuestions.Add(objSurveyQuestion);
    //        }
    //        else
    //        {
    //            iQuestionPointer = QuestionNum - 1;

    //            SurveyQuestions[iQuestionPointer].Question = Question;
    //            SurveyQuestions[iQuestionPointer].Answer_A = Answer_A;
    //            SurveyQuestions[iQuestionPointer].Answer_B = Answer_B;
    //            SurveyQuestions[iQuestionPointer].Answer_C = Answer_C;
    //            SurveyQuestions[iQuestionPointer].Answer_D = Answer_D;
    //            SurveyQuestions[iQuestionPointer].Answer_E = Answer_E;
    //            SurveyQuestions[iQuestionPointer].Answer_F = "";
    //            SurveyQuestions[iQuestionPointer].RecordOrder = RecordOrder;
    //            SurveyQuestions[iQuestionPointer].Control_Type = Control_Type;
    //        }

    //        // insert the entire SurveyQuestions list (it first deletes
    //        // the existing Surveydetail records)
    //        bool ret = InsertSurveyQuestionsFromList(SurveyQuestions);
    //        return ret;

    //    }


    //    public static bool InsertSurveyQuestion(int SurveyID, int QuestionNum, string Question, string Answer_A, string Answer_B, string Answer_C,
    //        string Answer_D, string Answer_E, string RecordOrder, string Control_Type)
    //    {
    //        List<SurveyQuestion> SurveyQuestions = GetSurveyQuestionsByID(SurveyID);
    //        SurveyQuestion objSurveyQuestion = new SurveyQuestion(SurveyID, QuestionNum, Question, Answer_A,
    //            Answer_B, Answer_C, Answer_D, Answer_E, "", RecordOrder, Control_Type);

    //        SurveyQuestions.Add(objSurveyQuestion);

    //        // insert the entire SurveyQuestions list (it first deletes
    //        // the existing Surveydetail records)
    //        bool ret = InsertSurveyQuestionsFromList(SurveyQuestions);
    //        return ret;
    //    }

    //    public static bool DeleteSurveyQuestion(int SurveyID, int QuestionNum)
    //    {
    //        List<SurveyQuestion> SurveyQuestions = GetSurveyQuestionsByID(SurveyID);
    //        SurveyQuestions.RemoveAt(QuestionNum - 1);
    //        int i;
    //        for (i = QuestionNum; i < SurveyQuestions.Count; i++)
    //        {
    //            SurveyQuestions[i].QuestionNum = i - 1;
    //        }
    //        bool ret = true;

    //        if (SurveyQuestions.Count > 0)
    //        {
    //            ret = InsertSurveyQuestionsFromList(SurveyQuestions);
    //        }
    //        return ret;
    //    }

    //    public static bool InsertSurveyQuestionsFromList(List<SurveyQuestion> SurveyQuestions)
    //    {
    //        List<SQTIQuestionObject> QTIQuestions = new List<SQTIQuestionObject>();
    //        int iSurveyID = 0;
    //        if (SurveyQuestions.Count > 0)
    //        {
    //            int iOrder = 0;
    //            foreach (SurveyQuestion record in SurveyQuestions)
    //            {
    //                iOrder++;
    //                if (iOrder == 1)
    //                    iSurveyID = record.SurveyID;
    //                SQTIQuestionObject QTIQuestion = new SQTIQuestionObject();
    //                QTIQuestion.cQuestionID = "Survey_" + iSurveyID.ToString() + "_Question_" + iOrder.ToString();
    //                QTIQuestion.cQuestionTitle = "PearlsReview.com Question # " + iOrder.ToString() +
    //                    " for Survey # " + iSurveyID.ToString();
    //                QTIQuestion.cQuestionText = record.Question;
    //                QTIQuestion.cAnswer_A_Text = record.Answer_A;
    //                QTIQuestion.cAnswer_B_Text = record.Answer_B;
    //                QTIQuestion.cAnswer_C_Text = record.Answer_C;
    //                QTIQuestion.cAnswer_D_Text = record.Answer_D;
    //                QTIQuestion.cAnswer_E_Text = record.Answer_E;
    //                QTIQuestion.cAnswer_F_Text = record.Answer_F;
    //                QTIQuestion.cRecordOrder = record.RecordOrder;
    //                QTIQuestion.cUserAnswer = "";
    //                QTIQuestion.cControl_Type = record.Control_Type;
    //                QTIQuestions.Add(QTIQuestion);
    //            }
    //        }

    //        string cXML = "";
    //        bool ret = false;

    //        if (QTIQuestions.Count > 0 && iSurveyID > 0)
    //        {
    //            SQTIUtils oSQTIUtils = new SQTIUtils();
    //            cXML = oSQTIUtils.ConvertSQTIQuestionObjectListToQTITestXMLString(QTIQuestions);
    //            SurveyDefinition oSurveyDefinition = SurveyDefinition.GetSurveyDefinitionByID(iSurveyID);
    //            if (oSurveyDefinition == null)
    //            {
    //                // inserting new record
    //                int newID = SurveyDefinition.InsertSurveyDefinition("", cXML, 1, 0);
    //                if (newID > 0)
    //                    ret = true;
    //            }
    //            else
    //            {
    //                // updating existing record
    //                //ret = SurveyDefinition.UpdateSurveyDefinition(oSurveyDefinition.SurveyID,cXML, oSurveyDefinition.Version + 1,0);
    //                ret = SurveyDefinition.UpdateSurveyDefinition(oSurveyDefinition.SurveyID, cXML, 1, 0);
    //            }
    //        }
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Returns a list of test question objects for the specified TestID        
    //    /// </summary>
    //    public static List<SQTIQuestionObject> GetSurveyQuestionObjectListByTopicID(int TopicID)
    //    {
    //        SurveyDefinition oSurveyDefinition = null;
    //        SQTIUtils oSQTIUtils = new SQTIUtils();
    //        List<SQTIQuestionObject> SQTIQuestionObjects = null;
    //        // first, get the test record so we have access to topicid and user's responses
    //        //Test oTest = Test.GetTestByID(TestID);
    //        //if (oTest != null)
    //        //{
    //            // get the test definition record for this topic
    //            Topic oTopic = Topic.GetTopicByID(TopicID);
    //            if (oTopic != null)
    //            {
    //                oSurveyDefinition = SurveyDefinition.GetSurveyDefinitionByID(oTopic.SurveyID);
    //            }
    //            if (oSurveyDefinition != null)
    //            {
    //                // get the QTIQuestionObjectList from the TestDefinition XML
    //                SQTIQuestionObjects = oSQTIUtils.ConvertQTITestXMLToSQTIQuestionObjectList(oSurveyDefinition.XMLSurvey);
    //            }
    //        //}
    //        return SQTIQuestionObjects;
    //    }
    //    #endregion
    //}
    //#endregion

}