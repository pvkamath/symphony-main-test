﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for Snippet
/// </summary>
    public class Snippet : BasePR
    {
        #region Variables and Properties

        private int _SnippetID = 0;
        public int SnippetID
        {
            get { return _SnippetID; }
            protected set { _SnippetID = value; }
        }

        private string _SnippetKey = "";
        public string SnippetKey
        {
            get { return _SnippetKey; }
            set { _SnippetKey = value; }
        }

        private string _SnippetText = "";
        public string SnippetText
        {
            get { return _SnippetText; }
            set { _SnippetText = value; }
        }

        public Snippet(int SnippetID, string SnippetKey, string SnippetText)
        {
            this.SnippetID = SnippetID;
            this.SnippetKey = SnippetKey;
            this.SnippetText = SnippetText;
        }

        public bool Delete()
        {
            bool success = Snippet.DeleteSnippet(this.SnippetID);
            if (success)
                this.SnippetID = 0;
            return success;
        }

        public bool Update()
        {
            return Snippet.UpdateSnippet(this.SnippetID, this.SnippetKey, this.SnippetText);
        }
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Snippets
        /// </summary>
        public static List<Snippet> GetSnippets(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "SnippetKey";

            List<Snippet> Snippets = null;
            string key = "Snippets_Snippets_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Snippets = (List<Snippet>)BizObject.Cache[key];
            }
            else
            {
                List<SnippetInfo> recordset = SiteProvider.PR2.GetSnippets(cSortExpression);
                Snippets = GetSnippetListFromSnippetInfoList(recordset);
                BasePR.CacheData(key, Snippets);
            }
            return Snippets;
        }


        /// <summary>
        /// Returns the number of total Snippets
        /// </summary>
        public static int GetSnippetCount()
        {
            int SnippetCount = 0;
            string key = "Snippets_SnippetCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SnippetCount = (int)BizObject.Cache[key];
            }
            else
            {
                SnippetCount = SiteProvider.PR2.GetSnippetCount();
                BasePR.CacheData(key, SnippetCount);
            }
            return SnippetCount;
        }

        /// <summary>
        /// Returns a Snippet object with the specified ID
        /// </summary>
        public static Snippet GetSnippetByID(int SnippetID)
        {
            Snippet Snippet = null;
            string key = "Snippets_Snippet_" + SnippetID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Snippet = (Snippet)BizObject.Cache[key];
            }
            else
            {
                Snippet = GetSnippetFromSnippetInfo(SiteProvider.PR2.GetSnippetByID(SnippetID));
                BasePR.CacheData(key, Snippet);
            }
            return Snippet;
        }

        /// <summary>
        /// Returns a Snippet object with the specified MessageKey value
        /// </summary>
        public static Snippet GetSnippetBySnippetKey(string SnippetKey)
        {
            Snippet Snippet = null;
            string key = "Snippets_Snippet_" + SnippetKey.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Snippet = (Snippet)BizObject.Cache[key];
            }
            else
            {
                Snippet = GetSnippetFromSnippetInfo(SiteProvider.PR2.GetSnippetBySnippetKey(SnippetKey));
                BasePR.CacheData(key, Snippet);
            }
            return Snippet;
        }

        /// <summary>
        /// Updates an existing Snippet
        /// </summary>
        public static bool UpdateSnippet(int SnippetID, string SnippetKey, string SnippetText)
        {
            SnippetText = BizObject.ConvertNullToEmptyString(SnippetText);


            SnippetInfo record = new SnippetInfo(SnippetID, SnippetKey, SnippetText);
            bool ret = SiteProvider.PR2.UpdateSnippet(record);

            BizObject.PurgeCacheItems("Snippets_Snippet_" + SnippetID.ToString());
            BizObject.PurgeCacheItems("Snippets_Snippets");
            return ret;
        }

        /// <summary>
        /// Creates a new Snippet
        /// </summary>
        public static int InsertSnippet(string SnippetKey, string SnippetText)
        {
            SnippetText = BizObject.ConvertNullToEmptyString(SnippetText);


            SnippetInfo record = new SnippetInfo(0, SnippetKey, SnippetText);
            int ret = SiteProvider.PR2.InsertSnippet(record);

            BizObject.PurgeCacheItems("Snippets_Snippet");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Snippet, but first checks if OK to delete
        /// </summary>
        public static bool DeleteSnippet(int SnippetID)
        {
            bool IsOKToDelete = OKToDelete(SnippetID);
            if (IsOKToDelete)
            {
                return (bool)DeleteSnippet(SnippetID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Snippet - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteSnippet(int SnippetID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteSnippet(SnippetID);
            //         new RecordDeletedEvent("Snippet", InterestID, null).Raise();
            BizObject.PurgeCacheItems("Snippets_Snippet");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Snippet can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int SnippetID)
        {
            return true;
        }


        /// <summary>
        /// Returns a Snippet object filled with the data taken from the input SnippetInfo
        /// </summary>
        private static Snippet GetSnippetFromSnippetInfo(SnippetInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Snippet(record.SnippetID, record.SnippetKey, record.SnippetText);
            }
        }

        /// <summary>
        /// Returns a list of Snippet objects filled with the data taken from the input list of SnippetInfo
        /// </summary>
        private static List<Snippet> GetSnippetListFromSnippetInfoList(List<SnippetInfo> recordset)
        {
            List<Snippet> Snippets = new List<Snippet>();
            foreach (SnippetInfo record in recordset)
                Snippets.Add(GetSnippetFromSnippetInfo(record));
            return Snippets;
        }

        #endregion
    }
}
