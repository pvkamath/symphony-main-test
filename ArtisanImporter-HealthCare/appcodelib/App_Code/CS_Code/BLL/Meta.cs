﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using System.Collections.Generic;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for Meta
    /// </summary>
    public class Meta :BasePR
    {
        
        #region Variables and Properties
        public Meta(int metaID, string path, string title, string description, string keyword, string h1)
        {
            this.metaID = metaID;
            this.path = path;
            this.title = title;
            this.description = description;
            this.keyword = keyword;
            this.h1 = h1;
        }

        public int metaID
        {
            get;
            set;
        }
        public string path
        {
            get;
            set;
        }
        public string title
        {
            get;
            set;
        }
        public string description
        {
            get;
            set;
        }
        public string keyword
        {
            get;
            set;
        }
        public string h1
        {
            get;
            set;
        }
        #endregion
        #region Methods
        private static List<Meta> GetMetaListFromMetaInfoList(List<MetaInfo> recordset)
        {
            List<Meta> Metas = new List<Meta>();
            foreach (MetaInfo record in recordset)
                Metas.Add(GetMetaFromMetaInfo(record));
            return Metas;
        }

        private static Meta GetMetaFromMetaInfo(MetaInfo record)
        {
            if (record == null)
                return null;
            else
            {

                return new Meta(record.metaID, record.path, record.title, record.description, record.keyword,
                    record.h1);

            }
        }


        /// <summary>
        /// Returns a Topic object with the specified ID
        /// </summary>
        public static Meta GetMetaByPath(String Path)
        {
            Meta Meta = null;
            string key = "Metas_Meta_" + Path.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Meta = (Meta)BizObject.Cache[key];
            }
            else
            {
                Meta = GetMetaFromMetaInfo(SiteProvider.PR2.GetMetaByPath(Path));
                BasePR.CacheData(key, Meta);
            }
            return Meta;
        }

        #endregion

    }
}
