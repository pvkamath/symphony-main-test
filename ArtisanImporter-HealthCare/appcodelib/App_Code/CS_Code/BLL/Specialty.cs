﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for Specialty
/// </summary>
public class Specialty: BasePR
    {
        #region Variables and Properties

        private int _SpecID = 0;
        public int SpecID
        {
            get { return _SpecID; }
            protected set { _SpecID = value; }
        }

        private string _SpecTitle = "";
        public string SpecTitle
        {
            get { return _SpecTitle; }
            set { _SpecTitle = value; }
        }



        public Specialty(int SpecID, string SpecTitle)
        {
            this.SpecID = SpecID;
            this.SpecTitle = SpecTitle;
        }

        public bool Delete()
        {
            bool success = Specialty.DeleteSpecialty(this.SpecID);
            if (success)
                this.SpecID = 0;
            return success;
        }

        public bool Update()
        {
            return Specialty.UpdateSpecialty(this.SpecID, this.SpecTitle);
        }
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Specialties
        /// </summary>
        public static List<Specialty> GetSpecialties(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "SpecTitle";

            List<Specialty> Specialties = null;
            string key = "Specialties_Specialties_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Specialties = (List<Specialty>)BizObject.Cache[key];
            }
            else
            {
                List<SpecialtyInfo> recordset = SiteProvider.PR2.GetSpecialties(cSortExpression);
                Specialties = GetSpecialtyListFromSpecialtyInfoList(recordset);
                BasePR.CacheData(key, Specialties);
            }
            return Specialties;
        }


        /// <summary>
        /// Returns the number of total Specialties
        /// </summary>
        public static int GetSpecialtyCount()
        {
            int SpecialtyCount = 0;
            string key = "Specialties_SpecialtyCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SpecialtyCount = (int)BizObject.Cache[key];
            }
            else
            {
                SpecialtyCount = SiteProvider.PR2.GetSpecialtyCount();
                BasePR.CacheData(key, SpecialtyCount);
            }
            return SpecialtyCount;
        }

        /// <summary>
        /// Returns a Specialty object with the specified ID
        /// </summary>
        public static Specialty GetSpecialtyByID(int SpecID)
        {
            Specialty Specialty = null;
            string key = "Specialties_Specialty_" + SpecID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Specialty = (Specialty)BizObject.Cache[key];
            }
            else
            {
                Specialty = GetSpecialtyFromSpecialtyInfo(SiteProvider.PR2.GetSpecialtyByID(SpecID));
                BasePR.CacheData(key, Specialty);
            }
            return Specialty;
        }

        /// <summary>
        /// Updates an existing Specialty
        /// </summary>
        public static bool UpdateSpecialty(int SpecID, string SpecTitle)
        {
            SpecTitle = BizObject.ConvertNullToEmptyString(SpecTitle);


            SpecialtyInfo record = new SpecialtyInfo(SpecID, SpecTitle);
            bool ret = SiteProvider.PR2.UpdateSpecialty(record);

            BizObject.PurgeCacheItems("Specialties_Specialty_" + SpecID.ToString());
            BizObject.PurgeCacheItems("Specialties_Specialties");
            return ret;
        }

        /// <summary>
        /// Creates a new Specialty
        /// </summary>
        public static int InsertSpecialty(string SpecTitle)
        {
            SpecTitle = BizObject.ConvertNullToEmptyString(SpecTitle);


            SpecialtyInfo record = new SpecialtyInfo(0, SpecTitle);
            int ret = SiteProvider.PR2.InsertSpecialty(record);

            BizObject.PurgeCacheItems("Specialties_Specialty");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Specialty, but first checks if OK to delete
        /// </summary>
        public static bool DeleteSpecialty(int SpecID)
        {
            bool IsOKToDelete = OKToDelete(SpecID);
            if (IsOKToDelete)
            {
                return (bool)DeleteSpecialty(SpecID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Specialty - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteSpecialty(int SpecID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteSpecialty(SpecID);
            //         new RecordDeletedEvent("Specialty", SpecID, null).Raise();
            BizObject.PurgeCacheItems("Specialties_Specialty");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Specialty can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int SpecID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Specialty object filled with the data taken from the input SpecialtyInfo
        /// </summary>
        private static Specialty GetSpecialtyFromSpecialtyInfo(SpecialtyInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Specialty(record.SpecID, record.SpecTitle);
            }
        }

        /// <summary>
        /// Returns a list of Specialty objects filled with the data taken from the input list of SpecialtyInfo
        /// </summary>
        private static List<Specialty> GetSpecialtyListFromSpecialtyInfoList(List<SpecialtyInfo> recordset)
        {
            List<Specialty> Specialties = new List<Specialty>();
            foreach (SpecialtyInfo record in recordset)
                Specialties.Add(GetSpecialtyFromSpecialtyInfo(record));
            return Specialties;
        }


        #endregion
    }
}
