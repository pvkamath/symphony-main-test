﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 
    /// <summary>
    /// Summary description for Document
    /// </summary>
    public class Document : BasePR
    {
        #region Variables and Properties
        ////////////////////////////////////////////////////////////   
        private int _DocumentID = 0;
        public int DocumentID
        {
            get { return _DocumentID; }
            protected set { _DocumentID = value; }
        }

        private string _FileName = "";
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }

        private string _Description = "";
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        private string _Created = "";
        public string Created
        {
            get { return _Created; }
            set { _Created = value; }
        }

        private DateTime _Uploaded = System.DateTime.Now;
        public DateTime Uploaded
        {
            get { return _Uploaded; }
            set { _Uploaded = value; }
        }

        private string _UploadedBy = "";
        public string UploadedBy
        {
            get { return _UploadedBy; }
            set { _UploadedBy = value; }
        }

        public Document(int DocumentID, string FileName, string Description, 
           string Created, DateTime Uploaded, string UploadedBy)
      {
			this.DocumentID = DocumentID;
            this.FileName = FileName;
            this.Description = Description;
            this.Created = Created;
            this.Uploaded = Uploaded;
            this.UploadedBy = UploadedBy;
      }

        public bool Delete()
        {
            bool success = Document.DeleteDocument(this.DocumentID);
            if (success)
                this.DocumentID = 0;
            return success;
        }

        public bool Update()
        {
            return Document.UpdateDocument(this.DocumentID, this.FileName, this.Description, 
           this.Created, this.Uploaded, this.UploadedBy);
        }

        #endregion

        #region Methods            
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Documents
        /// </summary>
        public static List<Document> GetDocuments(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "PageName";

            List<Document> Documents = null;
            string key = "Documents_Documents_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Documents = (List<Document>)BizObject.Cache[key];
            }
            else
            {
                List<DocumentInfo> recordset = SiteProvider.PR2.GetDocuments(cSortExpression);
                Documents = GetDocumentListFromDocumentInfoList(recordset);
                BasePR.CacheData(key, Documents);
            }
            return Documents;
        }

        /// <summary>
        /// Returns a collection with all Documents linked to a relationID for a certain type
        /// (Types: resource person="R", topic="T")
        /// </summary>
        public static List<Document> GetDocumentsByRelationIDAndType(int RelationID,
            string RelationType, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "uploaded desc";

            List<Document> Documents = null;
            string key = "Documents_Documents_" + RelationID.ToString() + "_" + RelationType.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Documents = (List<Document>)BizObject.Cache[key];
            }
            else
            {
                List<DocumentInfo> recordset = SiteProvider.PR2.GetDocumentsByRelationIDAndType(RelationID, RelationType, cSortExpression);
                Documents = GetDocumentListFromDocumentInfoList(recordset);
                BasePR.CacheData(key, Documents);
            }
            return Documents;
        }

        /// <summary>
        /// Returns the number of total Documents
        /// </summary>
        public static int GetDocumentCount()
        {
            int DocumentCount = 0;
            string key = "Documents_DocumentCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                DocumentCount = (int)BizObject.Cache[key];
            }
            else
            {
                DocumentCount = SiteProvider.PR2.GetDocumentCount();
                BasePR.CacheData(key, DocumentCount);
            }
            return DocumentCount;
        }

        /// <summary>
        /// Returns a Document object with the specified ID
        /// </summary>
        public static Document GetDocumentByID(int DocumentID)
        {
            Document Document = null;
            string key = "Documents_Document_" + DocumentID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Document = (Document)BizObject.Cache[key];
            }
            else
            {
                Document = GetDocumentFromDocumentInfo(SiteProvider.PR2.GetDocumentByID(DocumentID));
                BasePR.CacheData(key, Document);
            }
            return Document;
        }

        /// <summary>
        /// Updates an existing Document
        /// </summary>
        public static bool UpdateDocument(int DocumentID, string FileName, string Description,
           string Created, DateTime Uploaded, string UploadedBy)
        {
            FileName = BizObject.ConvertNullToEmptyString(FileName);
            Description = BizObject.ConvertNullToEmptyString(Description);
            Created = BizObject.ConvertNullToEmptyString(Created);
            UploadedBy = BizObject.ConvertNullToEmptyString(UploadedBy);

            DocumentInfo record = new DocumentInfo(DocumentID, FileName, Description,
                Created, Uploaded, UploadedBy);
            bool ret = SiteProvider.PR2.UpdateDocument(record);

            BizObject.PurgeCacheItems("Documents_Document_" + DocumentID.ToString());
            BizObject.PurgeCacheItems("Documents_Documents");
            return ret;
        }

        /// <summary>
        /// Creates a new Document
        /// </summary>
        public static int InsertDocument(string FileName, string Description,
           string Created, DateTime Uploaded, string UploadedBy)
        {
            FileName = BizObject.ConvertNullToEmptyString(FileName);
            Description = BizObject.ConvertNullToEmptyString(Description);
            Created = BizObject.ConvertNullToEmptyString(Created);
            UploadedBy = BizObject.ConvertNullToEmptyString(UploadedBy);

            DocumentInfo record = new DocumentInfo(0, FileName, Description,
                Created, Uploaded, UploadedBy);
            int ret = SiteProvider.PR2.InsertDocument(record);

            BizObject.PurgeCacheItems("Documents_Document");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Document, but first checks if OK to delete
        /// </summary>
        public static bool DeleteDocument(int DocumentID)
        {
            bool IsOKToDelete = OKToDelete(DocumentID);
            if (IsOKToDelete)
            {
                return (bool)DeleteDocument(DocumentID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Document - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteDocument(int DocumentID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteDocument(DocumentID);
            //         new RecordDeletedEvent("Document", DocumentID, null).Raise();
            BizObject.PurgeCacheItems("Documents_Document");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Document can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int DocumentID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Document object filled with the data taken from the input DocumentInfo
        /// </summary>
        private static Document GetDocumentFromDocumentInfo(DocumentInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Document(record.DocumentID, record.FileName, record.Description,
           record.Created, record.Uploaded, record.UploadedBy);
            }
        }

        /// <summary>
        /// Returns a list of Document objects filled with the data taken from the input list of DocumentInfo
        /// </summary>
        private static List<Document> GetDocumentListFromDocumentInfoList(List<DocumentInfo> recordset)
        {
            List<Document> Documents = new List<Document>();
            foreach (DocumentInfo record in recordset)
                Documents.Add(GetDocumentFromDocumentInfo(record));
            return Documents;
        }    
        #endregion
    }
}
