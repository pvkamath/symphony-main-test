﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr1
    /// </summary>
    //public class Department : BasePR
    //{
    //    public Department()
    //    {
    //    }

    //    public int dept_id
    //    {
    //        get;
    //        set;
    //    }

    //    public string dept_name
    //    {
    //        get;
    //        set;
    //    }

    //    public string dept_abbrev
    //    {
    //        get;
    //        set;
    //    }

    //    public string dept_number
    //    {
    //        get;
    //        set;
    //    }

    //    public int dept_bed_total
    //    {
    //        get;
    //        set;
    //    }


    //    public int facilityid
    //    {
    //        get;
    //        set;
    //    }

    //    public bool dept_active_ind
    //    {
    //        get;
    //        set;
    //    }


    //    public bool Update()
    //    {
    //        return Department.Update(this.dept_id, this.dept_name, this.dept_abbrev,
    //                                    this.dept_number, this.dept_bed_total, this.facilityid, this.dept_active_ind);
    //    }

    //    public static bool Update(int dept_id, string dept_name, string dept_abbrev, string dept_number,
    //                                int dept_bed_total, int facilityid, bool dept_active_ind)
    //    {
    //        DepartmentInfo deptInfo = new DepartmentInfo(dept_id, dept_name, dept_abbrev, dept_number, dept_bed_total,
    //                                           facilityid, dept_active_ind);

    //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //        obj.UpdateDepartment(deptInfo);


    //        return false;
    //    }


        



    //    public bool Insert()
    //    {
    //        return Department.Insert(this.dept_id, this.dept_name, this.dept_abbrev, this.dept_number,
    //                                    this.dept_bed_total, this.facilityid, this.dept_active_ind);
    //    }

    //    public static bool Insert(int dept_id, string dept_name, string dept_abbrev, string dept_number,
    //                                int dept_bed_total, int facilityid, bool dept_active_ind)
    //    {
    //        DepartmentInfo deptInfo = new DepartmentInfo(dept_id, dept_name, dept_abbrev, dept_number, dept_bed_total,
    //                                           facilityid, dept_active_ind);


    //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //        int id = obj.InsertDepartment(deptInfo);


    //        return id > 0;
    //    }


    //    public bool Delete()
    //    {
    //        return Department.Delete(this.dept_id);
    //    }

    //    public static bool Delete(int dept_id)
    //    {
    //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //        obj.DeleteDepartment(dept_id);

    //        return false;
    //    }


    //    public static List<Department> GetDepartments(string fac_id, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "dept_name";

    //        List<Department> Dept = null;
    //        string key = "Dept_dept_name" + cSortExpression;

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Dept = (List<Department>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            List<DepartmentInfo> recordset = obj.GetDepartments(fac_id, cSortExpression);
    //            Dept = GetDepartmentListfromDepartmentInfo(recordset);
    //            BasePR.CacheData(key, Dept);
    //        }
    //        return Dept;
    //    }
    //    public static List<Department> GetDepartmentListfromDepartmentInfo(List<DepartmentInfo> recordset)
    //    {
    //        List<Department> departments = new List<Department>();
    //        foreach (DepartmentInfo deptInfo in recordset)
    //        {
    //            Department department = GetDepartmentFromDepartmentInfo(deptInfo);
    //            departments.Add(department);
    //        }
    //        return departments;
    //    }
    //    private static Department GetDepartmentFromDepartmentInfo(DepartmentInfo deptInfo)
    //    {
    //        Department dept = new Department();
    //        dept.dept_id = deptInfo.dept_id;
    //        dept.dept_name = deptInfo.dept_name;
    //        dept.dept_abbrev = deptInfo.dept_abbrev;
    //        dept.dept_number = deptInfo.dept_number;
    //        dept.dept_bed_total = deptInfo.dept_bed_total;
    //        dept.facilityid = deptInfo.facilityid;
    //        dept.dept_active_ind = deptInfo.dept_active_ind;

    //        return dept;

    //    }




    //    //method to get the Department by uniqueID 

    //    public static Department GetDepartmentByID(int dept_id)
    //    {
    //        Department Dept = null;
    //        string key = "Dept_dept_id" + dept_id;

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Dept = (Department)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            DepartmentInfo recordset = obj.GetDepartmentByID(dept_id);
    //            Dept = GetDepartmentFromDepartmentInfo(recordset);
    //            BasePR.CacheData(key, Dept);
    //        }
    //        return Dept;
    //    }
    //}

  //  public class UserAccount1 : BasePR
  //  {
  //      private int _iID = 0;
  //      public int iID
  //      {
  //          get { return _iID; }
  //          protected set { _iID = value; }
  //      }

  //      private string _cAddress1 = "";
  //      public string cAddress1
  //      {
  //          get { return _cAddress1; }
  //          set { _cAddress1 = value; }
  //      }

  //      private string _cAddress2 = "";
  //      public string cAddress2
  //      {
  //          get { return _cAddress2; }
  //          set { _cAddress2 = value; }
  //      }

  //      private bool _lAdmin = false;
  //      public bool lAdmin
  //      {
  //          get { return _lAdmin; }
  //          set { _lAdmin = value; }
  //      }

  //      private bool _lPrimary = false;
  //      public bool lPrimary
  //      {
  //          get { return _lPrimary; }
  //          set { _lPrimary = value; }
  //      }

  //      private string _cBirthDate = "";
  //      public string cBirthDate
  //      {
  //          get { return _cBirthDate; }
  //          set { _cBirthDate = value; }
  //      }

  //      private string _cCity = "";
  //      public string cCity
  //      {
  //          get { return _cCity; }
  //          set { _cCity = value; }
  //      }

  //      private string _cEmail = "";
  //      public string cEmail
  //      {
  //          get { return _cEmail; }
  //          set { _cEmail = value; }
  //      }

  //      private string _cExpires = "";
  //      public string cExpires
  //      {
  //          get { return _cExpires; }
  //          set { _cExpires = value; }
  //      }

  //      private string _cFirstName = "";
  //      public string cFirstName
  //      {
  //          get { return _cFirstName; }
  //          set { _cFirstName = value; }
  //      }

  //      private string _cFloridaNo = "";
  //      public string cFloridaNo
  //      {
  //          get { return _cFloridaNo; }
  //          set { _cFloridaNo = value; }
  //      }

  //      private string _cInstitute = "";
  //      public string cInstitute
  //      {
  //          get { return _cInstitute; }
  //          set { _cInstitute = value; }
  //      }

  //      private string _cLastName = "";
  //      public string cLastName
  //      {
  //          get { return _cLastName; }
  //          set { _cLastName = value; }
  //      }

  //      private string _cLectDate = "";
  //      public string cLectDate
  //      {
  //          get { return _cLectDate; }
  //          set { _cLectDate = value; }
  //      }

  //      private string _cMiddle = "";
  //      public string cMiddle
  //      {
  //          get { return _cMiddle; }
  //          set { _cMiddle = value; }
  //      }

  //      private string _cPhone = "";
  //      public string cPhone
  //      {
  //          get { return _cPhone; }
  //          set { _cPhone = value; }
  //      }

  //      private string _cPromoCode = "";
  //      public string cPromoCode
  //      {
  //          get { return _cPromoCode; }
  //          set { _cPromoCode = value; }
  //      }

  //      private int _PromoID = 0;
  //      public int PromoID
  //      {
  //          get { return _PromoID; }
  //          set { _PromoID = value; }
  //      }

  //      private int _FacilityID = 0;
  //      public int FacilityID
  //      {
  //          get { return _FacilityID; }
  //          set { _FacilityID = value; }
  //      }

  //      private string _facilityName = "";
  //      public string FacilityName
  //      {
  //          get { return _facilityName; }
  //          set { _facilityName = value; }
  //      }

  //      private string _cPW = "";
  //      public string cPW
  //      {
  //          get { return _cPW; }
  //          set { _cPW = value; }
  //      }

  //      private string _cRefCode = "";
  //      public string cRefCode
  //      {
  //          get { return _cRefCode; }
  //          set { _cRefCode = value; }
  //      }

  //      private string _cRegType = "";
  //      public string cRegType
  //      {
  //          get { return _cRegType; }
  //          set { _cRegType = value; }
  //      }

  //      private string _cSocial = "";
  //      public string cSocial
  //      {
  //          get { return _cSocial; }
  //          set { _cSocial = value; }
  //      }

  //      private string _cState = "";
  //      public string cState
  //      {
  //          get { return _cState; }
  //          set { _cState = value; }
  //      }

  //      private string _cUserName = "";
  //      public string cUserName
  //      {
  //          get { return _cUserName.ToLower(); }
  //          set { _cUserName = value.ToLower(); }
  //      }

  //      private string _cZipCode = "";
  //      public string cZipCode
  //      {
  //          get { return _cZipCode; }
  //          set { _cZipCode = value; }
  //      }

  //      private int _SpecID = 0;
  //      public int SpecID
  //      {
  //          get { return _SpecID; }
  //          set { _SpecID = value; }
  //      }

  //      private string _LCUserName = "";
  //      public string LCUserName
  //      {
  //          get { return _LCUserName.ToLower(); }
  //          set { _LCUserName = value.ToLower(); }
  //      }

  //      private DateTime _LastAct = System.DateTime.Now;
  //      public DateTime LastAct
  //      {
  //          get { return _LastAct; }
  //          set { _LastAct = value; }
  //      }

  //      private string _PassFmt = "";
  //      public string PassFmt
  //      {
  //          get { return _PassFmt; }
  //          set { _PassFmt = value; }
  //      }

  //      private string _PassSalt = "";
  //      public string PassSalt
  //      {
  //          get { return _PassSalt; }
  //          set { _PassSalt = value; }
  //      }

  //      private string _MobilePIN = "";
  //      public string MobilePIN
  //      {
  //          get { return _MobilePIN; }
  //          set { _MobilePIN = value; }
  //      }

  //      private string _LCEmail = "";
  //      public string LCEmail
  //      {
  //          get { return _LCEmail.ToLower(); }
  //          set { _LCEmail = value.ToLower(); }
  //      }

  //      private string _PWQuest = "";
  //      public string PWQuest
  //      {
  //          get { return _PWQuest; }
  //          set { _PWQuest = value; }
  //      }

  //      private string _PWAns = "";
  //      public string PWAns
  //      {
  //          get { return _PWAns; }
  //          set { _PWAns = value; }
  //      }

  //      private bool _IsApproved = false;
  //      public bool IsApproved
  //      {
  //          get { return _IsApproved; }
  //          set { _IsApproved = value; }
  //      }

  //      private bool _IsOnline = false;
  //      public bool IsOnline
  //      {
  //          get { return _IsOnline; }
  //          set { _IsOnline = value; }
  //      }

  //      private bool _IsLocked = false;
  //      public bool IsLocked
  //      {
  //          get { return _IsLocked; }
  //          set { _IsLocked = value; }
  //      }

  //      private DateTime _LastLogin = System.DateTime.MinValue;
  //      public DateTime LastLogin
  //      {
  //          get { return _LastLogin; }
  //          set { _LastLogin = value; }
  //      }

  //      private DateTime _LastPWChg = System.DateTime.MinValue;
  //      public DateTime LastPWChg
  //      {
  //          get { return _LastPWChg; }
  //          set { _LastPWChg = value; }
  //      }

  //      private DateTime _LastLock = System.DateTime.MinValue;
  //      public DateTime LastLock
  //      {
  //          get { return _LastLock; }
  //          set { _LastLock = value; }
  //      }

  //      private int _XPWAtt = 0;
  //      public int XPWAtt
  //      {
  //          get { return _XPWAtt; }
  //          set { _XPWAtt = value; }
  //      }

  //      private DateTime _XPWAttSt = System.DateTime.MinValue;
  //      public DateTime XPWAttSt
  //      {
  //          get { return _XPWAttSt; }
  //          set { _XPWAttSt = value; }
  //      }

  //      private int _XPWAnsAtt = 0;
  //      public int XPWAnsAtt
  //      {
  //          get { return _XPWAnsAtt; }
  //          set { _XPWAnsAtt = value; }
  //      }

  //      private DateTime _XPWAnsSt = System.DateTime.MinValue;
  //      public DateTime XPWAnsSt
  //      {
  //          get { return _XPWAnsSt; }
  //          set { _XPWAnsSt = value; }
  //      }

  //      private string _Comment = "";
  //      public string Comment
  //      {
  //          get { return _Comment; }
  //          set { _Comment = value; }
  //      }

  //      private DateTime _Created = System.DateTime.Now;
  //      public DateTime Created
  //      {
  //          get { return _Created; }
  //          set { _Created = value; }
  //      }

  //      private int _AgeGroup = 0;
  //      public int AgeGroup
  //      {
  //          get { return _AgeGroup; }
  //          set { _AgeGroup = value; }
  //      }

  //      private int _RegTypeID = 0;
  //      public int RegTypeID
  //      {
  //          get { return _RegTypeID; }
  //          set { _RegTypeID = value; }
  //      }

  //      private int _RepID = 0;
  //      public int RepID
  //      {
  //          get { return _RepID; }
  //          set { _RepID = value; }
  //      }


  //      private string _Work_Phone = "";
  //      public string Work_Phone 
  //      {
  //          get { return _Work_Phone; }
  //          set { _Work_Phone = value; }
  //      }
  //      private string _Badge_ID = "";
  //      public string Badge_ID
  //      {
  //          get { return _Badge_ID; }
  //          set { _Badge_ID = value; }
  //      }

  //      private DateTime _Hire_Date = System.DateTime.MinValue;
  //      public DateTime Hire_Date
  //      {
  //          get { return _Hire_Date; }
  //          set { _Hire_Date = value; }
  //      }

  //      private DateTime _Termin_Date = System.DateTime.MinValue;
  //      public DateTime Termin_Date
  //      {
  //          get { return _Termin_Date; }
  //          set { _Termin_Date = value; }
  //      }

  //      private int _Dept_ID = 0;
  //      public int Dept_ID
  //      {
  //          get { return _Dept_ID; }
  //          set { _Dept_ID = value; }
  //      }

  //      private string _deptName = "";
  //      public string DeptName
  //      {
  //          get { return _deptName; }
  //          set { _deptName = value; }
  //      }

  //      private string _roleNames = "";
  //      public string RoleNames
  //      {
  //          get { return _roleNames; }
  //          set { _roleNames = value; }
  //      }

  //      private int _Position_ID = 0;
  //      public int Position_ID
  //      {
  //          get { return _Position_ID; }
  //          set { _Position_ID = value; }
  //      }

  //      private int _Clinical_Ind = 0;
  //      public int Clinical_Ind
  //      {
  //          get { return _Clinical_Ind; }
  //          set { _Clinical_Ind = value; }
  //      }

  //      private  string _Title = "";
  //      public string Title
  //      {
  //          get { return _Title; }
  //          set { _Title = value; }
  //      }

  //      private bool _Giftcard_Ind = false;
  //      public bool Giftcard_Ind
  //      {
  //          get { return _Giftcard_Ind; }
  //          set { _Giftcard_Ind = value; }
  //      }

  //      private  decimal _Giftcard_Chour = 0;
  //      public decimal Giftcard_Chour
  //      {
  //          get { return _Giftcard_Chour; }
  //          set { _Giftcard_Chour = value; }
  //      }

  //      private decimal _Giftcard_Uhour = 0;
  //      public decimal Giftcard_Uhour
  //      {
  //          get { return _Giftcard_Uhour; }
  //          set { _Giftcard_Uhour = value; }
  //      }

  //      private int _UniqueID = 0;
  //      public int UniqueID
  //      {
  //          get { return _UniqueID; }
  //          set { _UniqueID = value; }
  //      }

  //      private bool _FirstLogin_Ind = false;
  //      public bool FirstLogin_Ind
  //      {
  //          get { return _FirstLogin_Ind; }
  //          set { _FirstLogin_Ind = value; }
  //      }

  //      public UserAccount1(int iID, string cFirstName, string cLastName)
  //      {
  //          this.iID = iID;
  //          this.cFirstName = cFirstName;
  //          this.cLastName = cLastName;
  //      }
  //      public UserAccount1(int iID, string cAddress1, string cAddress2, bool lAdmin, bool lPrimary, string cBirthDate,
  //                string cCity, string cEmail, string cExpires, string cFirstName, string cFloridaNo,
  //         string cInstitute, string cLastName, string cLectDate, string cMiddle, string cPhone,
  //         string cPromoCode, int PromoID, int FacilityID, string FacilityName, string cPW, string cRefCode,
  //         string cRegType, string cSocial, string cState, string cUserName, string cZipCode,
  //         int SpecID, string LCUserName, DateTime LastAct, string PassFmt,
  //         string PassSalt, string MobilePIN, string LCEmail, string PWQuest, string PWAns,
  //         bool IsApproved, bool IsOnline, bool IsLocked, DateTime LastLogin, DateTime LastPWChg,
  //         DateTime LastLock, int XPWAtt, DateTime XPWAttSt, int XPWAnsAtt, DateTime XPWAnsSt,
  //         string Comment, DateTime Created, int AgeGroup, int RegTypeID, int RepID, string Work_Phone, string Badge_ID,
  //         DateTime Hire_Date, DateTime Termin_Date, int Dept_ID, string Dept_Name, string roleNames, int Position_ID,int Clinical_Ind,string Title,bool Giftcard_Ind,
  //         decimal Giftcard_Chour, decimal Giftcard_Uhour, int UniqueID, bool FirstLogin_Ind)
  //      {
  //          this.iID = iID;
  //          this.cAddress1 = cAddress1;
  //          this.cAddress2 = cAddress2;
  //          this.lAdmin = lAdmin;
  //          this.lPrimary = lPrimary;
  //          this.cBirthDate = cBirthDate;
  //          this.cCity = cCity;
  //          this.cEmail = cEmail;
  //          this.cExpires = cExpires;
  //          this.cFirstName = cFirstName;
  //          this.cFloridaNo = cFloridaNo;
  //          this.cInstitute = cInstitute;
  //          this.cLastName = cLastName;
  //          this.cLectDate = cLectDate;
  //          this.cMiddle = cMiddle;
  //          this.cPhone = cPhone;
  //          this.cPromoCode = cPromoCode;
  //          this.PromoID = PromoID;
  //          this.FacilityID = FacilityID;
  //          this.FacilityName = FacilityName;
  //          this.cPW = cPW;
  //          this.cRefCode = cRefCode;
  //          this.cRegType = cRegType;
  //          this.cSocial = cSocial;
  //          this.cState = cState;
  //          this.cUserName = cUserName.ToLower();
  //          this.cZipCode = cZipCode;
  //          this.SpecID = SpecID;
  //          this.LCUserName = LCUserName.ToLower();
  //          this.LastAct = LastAct;
  //          this.PassFmt = PassFmt;
  //          this.PassSalt = PassSalt;
  //          this.MobilePIN = MobilePIN;
  //          this.LCEmail = LCEmail;
  //          this.PWQuest = PWQuest;
  //          this.PWAns = PWAns;
  //          this.IsApproved = IsApproved;
  //          this.IsOnline = IsOnline;
  //          this.IsLocked = IsLocked;
  //          this.LastLogin = LastLogin;
  //          this.LastPWChg = LastPWChg;
  //          this.LastLock = LastLock;
  //          this.XPWAtt = XPWAtt;
  //          this.XPWAttSt = XPWAttSt;
  //          this.XPWAnsAtt = XPWAnsAtt;
  //          this.XPWAnsSt = XPWAnsSt;
  //          this.Comment = Comment;
  //          this.Created = Created;
  //          this.AgeGroup = AgeGroup;
  //          this.RegTypeID = RegTypeID;
  //          this.RepID = RepID;
  //          this.Work_Phone=Work_Phone;
  //          this.Badge_ID=Badge_ID;
  //          this.Hire_Date=Hire_Date;
  //          this.Termin_Date=Termin_Date;
  //          this.Dept_ID=Dept_ID;
  //          this.DeptName = Dept_Name;
  //          this.RoleNames = roleNames;
  //          this.Position_ID=Position_ID;
  //          this.Clinical_Ind=Clinical_Ind;
  //          this.Title=Title;
  //          this.Giftcard_Ind=Giftcard_Ind;
  //          this.Giftcard_Chour=Giftcard_Chour;
  //          this.Giftcard_Uhour=Giftcard_Uhour;
  //          this.UniqueID = UniqueID;
  //          this.FirstLogin_Ind = FirstLogin_Ind;
            
  //      }
 
  //      private static UserAccount1 GetUserAccountFromUserAccountInfo1(UserAccountInfo1 record)
  //      {
  //          if (record == null)
  //              return null;
  //          else
  //          {
               
  //        return new UserAccount1(record.iID, record.cAddress1, record.cAddress2, record.lAdmin,
  //        record.lPrimary, record.cBirthDate, record.cCity, record.cEmail, record.cExpires, record.cFirstName, record.cFloridaNo,
  //        record.cInstitute, record.cLastName, record.cLectDate, record.cMiddle, record.cPhone,
  //        record.cPromoCode, record.PromoID, record.FacilityID, record.FacilityName, record.cPW, record.cRefCode,
  //        record.cRegType, record.cSocial, record.cState, record.cUserName.ToLower(), record.cZipCode,
  //        record.SpecID, record.LCUserName.ToLower(), record.LastAct, record.PassFmt,
  //        record.PassSalt, record.MobilePIN, record.LCEmail, record.PWQuest, record.PWAns,
  //        record.IsApproved, record.IsOnline, record.IsLocked, record.LastLogin, record.LastPWChg,
  //        record.LastLock, record.XPWAtt, record.XPWAttSt, record.XPWAnsAtt, record.XPWAnsSt,
  //        record.Comment, record.Created, record.AgeGroup, record.RegTypeID, record.RepID, 
  //        record.Work_Phone,record.Badge_ID,record.Hire_Date,record.Termin_Date,record.Dept_ID, record.DeptName, record.RoleNames,
  //        record.Position_ID,record.Clinical_Ind,record.Title,record.Giftcard_Ind,
  //        record.Giftcard_Chour, record.Giftcard_Uhour, record.UniqueID, record.FirstLogin_Ind);
  //          }
  //      }

  //      public bool Delete()
  //      {
  //          return Department.Delete(this.iID);
  //      }
  //      public static bool Switchfacility(int iID, int facid)
  //      {
  //          PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //          return  obj.Switchfacility(iID, facid);        }

  //      public static bool Delete(int iID)
  //      {
  //          PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //          obj.DeleteUser(iID);

  //          return false;
  //      }

  //      //public bool Update()
  //      //{
  //      //    return UserAccount1.Update(this.IsLocked);
  //      //}

  //      public static bool DeActivateUser(int userID)
  //      {
  //          PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //          return obj.DeActivateUser(userID);
  //      }

  //      public static bool ActivateUser(int userID)
  //      {
  //          PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //          return obj.ActivateUser(userID);
  //      }
  //      private static List<UserAccount1> GetUserAccountListFromUserAccountInfoList1(List<UserAccountInfo1> recordset)
  //      {
  //          List<UserAccount1> UserAccounts = new List<UserAccount1>();
  //          foreach (UserAccountInfo1 record in recordset)
  //              UserAccounts.Add(GetUserAccountFromUserAccountInfo1(record));
  //          return UserAccounts;
  //      }


  //      public static UserAccount1 GetUserAccountByUserName(string cUserName)
  //      {
  //          if (cUserName == null)
  //              //                cUserName = "a";
  //              return null;

  //          cUserName = cUserName.ToLower();
  //          UserAccount1 user = null;
  //          string key = "UserAccounts_UserAccountsByUserName_" + cUserName.ToString();

  //          if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
  //          {
  //              user = (UserAccount1)BizObject.Cache[key];
  //          }
  //          else
  //          {
  //              PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //              UserAccountInfo1 recordset = obj.GetUserAccountByUserName(cUserName);
  //              user = GetUserAccountFromUserAccountInfo1(recordset);
  //              BasePR.CacheData(key, user);
  //          }
  //          return user;
  //      }

  //      // NOTE; The following method is used only for GiftCard login, and uses the "short"
  //      // version of the username with no faciltiyid attached to it.
  //      // This works because gift card codes are unique regardless of any facility related to it.
  //      public static UserAccount1 GetUserAccountByGiftCardRedemptionCode(string RedemptionCode)
  //      {
  //          if (RedemptionCode == null)
  //              //                cUserName = "a";
  //              return null;

  //          RedemptionCode = RedemptionCode.ToLower();
  //          UserAccount1 user = null;
  //          string key = "UserAccounts_UserAccountByGiftCardRedemptionCode_" + RedemptionCode.ToString();

  //          if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
  //          {
  //              user = (UserAccount1)BizObject.Cache[key];
  //          }
  //          else
  //          {
  //              PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //              UserAccountInfo1 recordset = obj.GetUserAccountByGiftCardRedemptionCode(RedemptionCode);
  //              user = GetUserAccountFromUserAccountInfo1(recordset);
  //              BasePR.CacheData(key, user);
  //          }
  //          return user;
  //      }

  //      public static DataSet GetDuplicatesByFirstAndLastname(string facilityid)
  //      {
  //          PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //          return obj.GetDuplicatesByFirstAndLastname(facilityid);        
  //      }
  //      public static DataSet GetDuplicatesByUniqueid(string facilityid)
  //      {
  //          PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //          return obj.GetDuplicatesByUniqueid(facilityid);
  //      }
  //      public static DataSet GetMergeRecs(string fname, string lname, string facilityid)
  //      {
  //          PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //          return obj.GetMergeRecs(fname, lname, facilityid);
  //      }
  //      public static DataSet GetAssetUrlByTopicID(string TopicID)
  //      {
  //          PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //          return obj.GetAssetUrlByTopicID(TopicID);
  //      }
            
  //          public static List<UserAccount1> GetUserAccountsBySearchCriteria(string cSortExpression, string firstName, string lastName,
  //               string cUserName, int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID, string roleName, int startRowIndex, int maximumRows)
  //      {
  //          if (cSortExpression == null)
  //              cSortExpression = "";

  //          // provide default sort
  //          if (cSortExpression.Length == 0)
  //              cSortExpression = "cLastName";

  //          List<UserAccount1> UserAccounts = null;
  //          string key = "UserAccounts_UserAccountsBySearchCriteria" +"_" + firstName + "_" +
  //                          lastName + "_" + cUserName + "_" + facilityid + "_" + badge_id + "_" + cSocial + "_" + isApproved + "_" + isNotApproved + "_" + Dept_ID.ToString() + "_" + roleName + "_" + startRowIndex.ToString() + "_" + maximumRows.ToString();

  //          if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
  //          {
  //              UserAccounts = (List<UserAccount1>)BizObject.Cache[key];
  //          }
  //          else
  //          {
  //              PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //              List<UserAccountInfo1> userAccounts = obj.GetUserAccountsBySearchCriteria(cSortExpression, firstName, lastName, cUserName, facilityid, badge_id, cSocial, isApproved, isNotApproved, Dept_ID, roleName, startRowIndex, maximumRows);
  //              UserAccounts = GetUserAccountListFromUserAccountInfoList1(userAccounts);
  //              BasePR.CacheData(key, UserAccounts);
  //          }
  //          return UserAccounts;
  //      }

  //       public static List<UserAccount1> GetUserAccountsBySearchCriteria(string cSortExpression, string firstName, string lastName,
  //               string cUserName, int facilityid, string badge_id, string cSocial, bool isApproved, bool isNotApproved, int Dept_ID, 
  //              string roleName, int startRowIndex, int maximumRows, int rolepriority, int currentUserID, bool isActiveFacilityManager)
  //       {
  //           if (cSortExpression == null)
  //               cSortExpression = "";

  //           // provide default sort
  //           if (cSortExpression.Length == 0)
  //               cSortExpression = "cLastName";

  //           List<UserAccount1> UserAccounts = null;
  //           string key = "UserAccounts_UserAccountsBySearchCriteria" + "_" + firstName + "_" +
  //                           lastName + "_" + cUserName + "_" + facilityid + "_" + badge_id + "_" + cSocial + "_" + isApproved + "_" + isNotApproved + "_" + Dept_ID.ToString() + "_" + roleName + "_" + startRowIndex.ToString() + "_" + maximumRows.ToString() + "_" + rolepriority + "_" + currentUserID;

  //           if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
  //           {
  //               UserAccounts = (List<UserAccount1>)BizObject.Cache[key];
  //           }
  //           else
  //           {
  //               PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //               List<UserAccountInfo1> userAccounts = obj.GetUserAccountsBySearchCriteria(cSortExpression, firstName, lastName, cUserName, facilityid, badge_id, cSocial, isApproved, isNotApproved, Dept_ID, roleName, startRowIndex, maximumRows, rolepriority, currentUserID, isActiveFacilityManager);
  //               UserAccounts = GetUserAccountListFromUserAccountInfoList1(userAccounts);
  //               BasePR.CacheData(key, UserAccounts);
  //           }
  //           return UserAccounts;
  //       }
  //       /// <summary>
  //       /// CE retail Users
  //       /// </summary>

  //       public static List<UserAccount1> GetCERetailUsersBySearchCriteria(string cSortExpression, string cfirstName, string clastName,
  //                string cUserName, string cpw, string cAddress1, string cEmail, string license_number, string cState, string state, string cZipCode)
  //       {
  //           if (cSortExpression == null)
  //               cSortExpression = "";

  //           // provide default sort
  //           if (cSortExpression.Length == 0)
  //               cSortExpression = "cLastName";

  //           List<UserAccount1> UserAccounts = null;
  //           string key = "UserAccounts_UserAccountsBySearchCriteria" + "_" + cfirstName + "_" +
  //                           clastName + "_" + cUserName + "_" + cpw + "_" + cAddress1 + "_" + cEmail + "_" + license_number + "_" + cState + "_" + state + "_" + cZipCode;

  //           if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
  //           {
  //               UserAccounts = (List<UserAccount1>)BizObject.Cache[key];
  //           }
  //           else
  //           {
  //               PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //               List<UserAccountInfo1> userAccounts = obj.GetCERetailUsersBySearchCriteria(cSortExpression, cfirstName, clastName, cUserName, cpw, cAddress1, cEmail, license_number, cState, state, cZipCode);
  //               UserAccounts = GetUserAccountListFromUserAccountInfoList1(userAccounts);
  //               BasePR.CacheData(key, UserAccounts);
  //           }
  //           return UserAccounts;
  //       }

  //       public static DataSet GetNonUnlimitedOrders(string cSortExpression, string cfirstName, string clastName, string cUserName,
  //       string cpw, string cAddress1, string cEmail, string license_number, string cState, string state, string cZipCode)
  //       {

  //           PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //           return obj.GetNonUnlimitedOrders(cSortExpression, cfirstName, clastName, cUserName,
  //           cpw, cAddress1, cEmail, license_number, cState, state, cZipCode);
             
  //       }
  //       public static DataSet GetTranscripts(string cSortExpression, string cfirstName, string clastName, string cUserName,
  //        string cpw, string cAddress1, string cEmail, string license_number, string cState, string state, string cZipCode)
  //       {

  //           PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //           return obj.GetTranscripts(cSortExpression, cfirstName, clastName, cUserName,
  //           cpw, cAddress1, cEmail, license_number, cState, state, cZipCode);

  //       }
              
  //       //public static bool InsertUser(string username, string firstname, string middlename, string lastname, string title, string password, string address1,
  //       //    string address2, string city, string state, string zip, string homephone, string workphone, string birthdate, string badgeid, string ssn, string hiredate, string termindate, int deptid, int positionid, string pwdqs, string pwdans, string rolename, int facilityid)
  //       //{
  //       //    PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //       //    return obj.InsertUser(username, firstname, middlename, lastname, title, password, address1, address2, city, state,
  //       //        zip, homephone, workphone, birthdate, badgeid, ssn, hiredate, termindate, deptid, positionid, pwdqs, pwdans, rolename, facilityid);

  //       //}

  //       public static int GetUserAccountsCount(string firstName, string lastName, string cUserName, int facilityid,
  //              string badge_id, string cSocial, bool isApproved, bool isNotApproved,int Dept_ID, string cSortExpression)
  //       {
  //           int totalCount = 0;
  //           string key = "UserAccounts_UserAccountsCount" + "_" + firstName + "_" +
  //                           lastName + "_" + cUserName + "_" + facilityid + "_" + badge_id + "_" + cSocial + "_" + isApproved + "_" + isNotApproved + "_" + Dept_ID.ToString();

  //           if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
  //           {
  //               totalCount = (int)BizObject.Cache[key];
  //           }
  //           else
  //           {
  //               PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //               totalCount = obj.GetUserAccountsCount(firstName, lastName, cUserName, facilityid, badge_id, cSocial, isApproved, isNotApproved, Dept_ID);
  //               BasePR.CacheData(key, totalCount);
  //           }
  //           return totalCount;
  //       }
        
  //    public static List<UserAccount1> GetUserAccountsByDeptName(String dept_name, string cSortExpression)
  //      {
  //          if (dept_name == null)
  //              dept_name = "";
  //          if (cSortExpression == null)
  //              cSortExpression = "";

  //          // provide default sort
  //          if (cSortExpression.Length == 0)

  //              cSortExpression = "cLastName";

  //          List<UserAccount1> UserAccounts = null;
  //          string key = "UserAccounts_UserAccount1" + dept_name.ToString() + cSortExpression.ToString();

  //          if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
  //          {
  //              UserAccounts = (List<UserAccount1>)BizObject.Cache[key];
  //          }
  //          else
  //          {
  //              PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            
  //              List<UserAccountInfo1> recordset = obj.GetUserAccountsByDeptName(dept_name, cSortExpression);
  //              UserAccounts = GetUserAccountListFromUserAccountInfoList1(recordset);
  //              BasePR.CacheData(key, UserAccounts);
  //          }
  //          return UserAccounts;
  //      }

  //      public static List<Department> GetUserDepartmentsByUserID(int userID)
  //      {
  //          List<Department> UserDepartments = null;
  //          string key = "UserAccounts_UserDepartments" + userID.ToString();

  //          if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
  //          {
  //              UserDepartments = (List<Department>)BizObject.Cache[key];
  //          }
  //          else
  //          {
  //              PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();

  //              List<DepartmentInfo> recordset = obj.GetDepartmentsByUserID(userID);
  //              UserDepartments = Department.GetDepartmentListfromDepartmentInfo(recordset);
  //              BasePR.CacheData(key, UserDepartments);
  //          }
  //          return UserDepartments;
  //      }

  //      public static void UpdateDepartmentsForUserID(List<DepartmentInfo> departments, int userID)
  //      {
  //          PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //          obj.UpdateDepartmentsForUserID(departments, userID);
  //      }
  //      public static void UpdatePrimaryDepartmentForUserID(int deptID, int userID)
  //      {
  //          PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
  //          obj.UpdatePrimaryDepartmentForUserID(deptID, userID);
  //      }

  //}


    //public class Orders : BasePR
    //{
    //    public Orders()
    //    { }

    //    private int _OrderID = 0;
    //    public int OrderID
    //    {
    //        get { return _OrderID; }
    //        private set { _OrderID = value; }
    //    }

    //    private int _UserID = 0;
    //    public int UserID
    //    {
    //        get { return _UserID; }
    //        private set { _UserID = value; }
    //    }

    //    private string _FirstName = "";
    //    public string FirstName
    //    {
    //        get { return _FirstName.ToLower(); }
    //        private set { _FirstName = value.ToLower(); }
    //    }

    //    private string _LastName = "";
    //    public string LastName
    //    {
    //        get { return _LastName.ToLower(); }
    //        private set { _LastName = value.ToLower(); }
    //    }

    //    private string _Address1 = "";
    //    public string Address1
    //    {
    //        get { return _Address1.ToLower(); }
    //        private set { _Address1 = value.ToLower(); }
    //    }


    //    private string _Address2 = "";
    //    public string Address2
    //    {
    //        get { return _Address2.ToLower(); }
    //        private set { _Address2 = value.ToLower(); }
    //    }

    //    private string _City = "";
    //    public string City
    //    {
    //        get { return _City.ToLower(); }
    //        private set { _City = value.ToLower(); }
    //    }

    //    private string _State = "";
    //    public string State
    //    {
    //        get { return _State.ToLower(); }
    //        private set { _State = value.ToLower(); }
    //    }

    //    private string _Zip = "";
    //    public string Zip
    //    {
    //        get { return _Zip.ToLower(); }
    //        private set { _Zip = value.ToLower(); }
    //    }


    //    private string _Country = "";
    //    public string Country
    //    {
    //        get { return _Country.ToLower(); }
    //        private set { _Country = value.ToLower(); }
    //    }


    //    private string _Email = "";
    //    public string Email
    //    {
    //        get { return _Email.ToLower(); }
    //        private set { _Email = value.ToLower(); }
    //    }

    //    private decimal _SubTotal = 0;
    //    public decimal SubTotal
    //    {
    //        get { return _SubTotal; }
    //        set { _SubTotal = value; }
    //    }

    //    private decimal _ShipCost = 0;
    //    public decimal ShipCost
    //    {
    //        get { return _ShipCost; }
    //        set { _ShipCost = value; }
    //    }

    //    private decimal _Tax = 0;
    //    public decimal Tax
    //    {
    //        get { return _Tax; }
    //        set { _Tax = value; }
    //    }

    //    private decimal _TotalCost = 0;
    //    public decimal TotalCost
    //    {
    //        get { return _TotalCost; }
    //        set { _TotalCost = value; }
    //    }

    //    private bool _Validate_Ind = false;
    //    public bool Validate_Ind
    //    {
    //        get { return _Validate_Ind; }
    //        set { _Validate_Ind = value; }
    //    }

    //    private string _Verification = "";
    //    public string Verification
    //    {
    //        get { return _Verification.ToLower(); }
    //        private set { _Verification = value.ToLower(); }
    //    }

    //    private bool _Ship_Ind = false;
    //    public bool Ship_Ind
    //    {
    //        get { return _Ship_Ind; }
    //        set { _Ship_Ind = value; }
    //    }

    //    private DateTime _OrderDate = System.DateTime.MinValue;
    //    public DateTime OrderDate
    //    {
    //        get { return _OrderDate; }
    //        set { _OrderDate = value; }
    //    }


    //    private string _Comment = "";
    //    public string Comment
    //    {
    //        get { return _Comment.ToLower(); }
    //        private set { _Comment = value.ToLower(); }
    //    }

    //    private int _CouponId = 0;
    //    public int CouponId
    //    {
    //        get { return _CouponId; }
    //        private set { _CouponId = value; }
    //    }

    //    private decimal _Couponamount = 0;
    //    public decimal Couponamount
    //    {
    //        get { return _Couponamount; }
    //        set { _Couponamount = value; }
    //    }

    //    public Orders(int OrderID, int UserID, string FirstName, string LastName, string Address1, string Address2,
    //    string City, string State, string Zip, string Country, string Email, decimal SubTotal, decimal ShipCost, decimal Tax, decimal TotalCost, bool Validate_Ind,
    //    string Verification, bool Ship_Ind, DateTime OrderDate, string Comment, int CouponId, decimal Couponamount)
    //    {

    //        this.OrderID = OrderID;
    //        this.UserID = UserID;
    //        this.FirstName = FirstName;
    //        this.LastName = LastName;
    //        this.Address1 = Address1.ToString();
    //        this.Address2 = Address2;
    //        this.City = City;
    //        this.State = State;
    //        this.Zip = Zip;
    //        this.Country = Country;
    //        this.Email = Email;
    //        this.SubTotal = SubTotal;
    //        this.ShipCost = ShipCost;
    //        this.Tax = Tax;
    //        this.TotalCost = TotalCost;
    //        this.Validate_Ind = Validate_Ind;
    //        this.Verification = Verification;
    //        this.Ship_Ind = Ship_Ind;
    //        this.OrderDate = OrderDate;
    //        this.Comment = Comment;
    //        this.CouponId = CouponId;
    //        this.Couponamount = Couponamount;

    //    }

    //    public bool Update()
    //    {
    //        return Orders.UpdateOrders(this.OrderID, this.UserID, this.FirstName, this.LastName, this.Address1, this.Address2,
    //    this.City, this.State, this.Zip, this.Country, this.Email, this.SubTotal, this.ShipCost, this.Tax, this.TotalCost, this.Validate_Ind,
    //    this.Verification, this.Ship_Ind, this.OrderDate, this.Comment, this.CouponId, this.Couponamount);
    //    }

    //    public bool Delete()
    //    {
    //        bool success = Orders.DeleteOrders(this.OrderID);
    //        if (success)
    //            this.OrderID = 0;
    //        return success;

    //    }


    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all Orders
    //    /// </summary>
    //    public static List<Orders> GetOrderbyDate(string cSortExpression, string selDate, bool IsPending, bool isShipped)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "orderid";

    //        List<Orders> Orders = null;
    //        string key = "Orders_Orders" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Orders = (List<Orders>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            List<OrdersInfo> recordset = obj.GetOrdersbyDate(cSortExpression, selDate, IsPending, isShipped);
    //            Orders = GetOrdersListFromOrdersInfoList(recordset);
    //            BasePR.CacheData(key, Orders);
    //        }
    //        return Orders;
    //    }



    //    /// <summary>
    //    /// Returns a collection with all Orders
    //    /// </summary>
    //    public static List<Orders> GetOrders(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "orderid";

    //        List<Orders> Orders = null;
    //        string key = "Orders_Orders" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Orders = (List<Orders>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            List<OrdersInfo> recordset = obj.GetOrders(cSortExpression);
    //            Orders = GetOrdersListFromOrdersInfoList(recordset);
    //            BasePR.CacheData(key, Orders);
    //        }
    //        return Orders;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Orders
    //    /// </summary>
    //    public static List<Orders> GetPendingOrders(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "orderid";

    //        List<Orders> Orders = null;
    //        string key = "Orders_Orders" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Orders = (List<Orders>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            List<OrdersInfo> recordset = obj.GetPendingOrders(cSortExpression);
    //            Orders = GetOrdersListFromOrdersInfoList(recordset);
    //            BasePR.CacheData(key, Orders);
    //        }
    //        return Orders;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Orders
    //    /// </summary>
    //    public static List<Orders> GetShippedOrders(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "orderid";

    //        List<Orders> Orders = null;
    //        string key = "Orders_Orders" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Orders = (List<Orders>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            List<OrdersInfo> recordset = obj.GetShippedOrders(cSortExpression);
    //            Orders = GetOrdersListFromOrdersInfoList(recordset);
    //            BasePR.CacheData(key, Orders);
    //        }
    //        return Orders;
    //    }

    //    /// <summary>
    //    /// Returns Orders object with the specified USERID
    //    /// </summary>
    //    public static Orders GetOrdersbyUserID(int userid)
    //    {
    //        List<Orders> Orders = null;
    //        string key = "Orders_Orders" + userid.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Orders = (List<Orders>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            List<OrdersInfo> recordset = (obj.GetOrdersbyUserID(userid));

    //            Orders = GetOrdersListFromOrdersInfoList(recordset);
    //            BasePR.CacheData(key, Orders);
    //        }
    //        return Orders[0];

    //    }

    //    public static List<Orders> GetAllOrdersbyUserID(int userid)
    //    {
    //        List<Orders> Orders = null;
    //        string key = "Orders_Orders" + userid.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Orders = (List<Orders>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            List<OrdersInfo> recordset = (obj.GetOrdersbyUserID(userid));

    //            Orders = GetOrdersListFromOrdersInfoList(recordset);
    //            BasePR.CacheData(key, Orders);
    //        }
    //        return Orders;

    //    }


    //    /// <summary>
    //    /// Returns Orders object with the specified ID
    //    /// </summary>
    //    public static Orders GetOrdersByID(int OrderID)
    //    {
    //        List<Orders> Orders = null;
    //        string key = "Orders_Orders" + OrderID.ToString();


    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Orders = (List<Orders>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<OrdersInfo> recordset = new List<OrdersInfo>();
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            OrdersInfo recordsetinfo = (obj.GetOrdersByID(OrderID));

    //            recordset.Add(recordsetinfo);
    //            Orders = GetOrdersListFromOrdersInfoList(recordset);
    //            //Orders = obj.GetOrdersByID(OrderID);
    //            BasePR.CacheData(key, Orders);
    //        }
    //        return Orders[0];
    //    }



    //    /// <summary>
    //    /// Creates new orders
    //    /// </summary>
    //    public static int InsertOrders(int OrderID, int UserID, string FirstName, string LastName, string Address1, string Address2,
    //    string City, string State, string Zip, string Country, string Email, decimal SubTotal, decimal ShipCost, decimal Tax, decimal TotalCost, bool Validate_Ind,
    //    string Verification, bool Ship_Ind, DateTime OrderDate, string Comment, int CouponId, decimal Couponamount)
    //    {
    //        OrdersInfo record = new OrdersInfo(0, UserID, FirstName, LastName, Address1, Address2,
    //        City, State, Zip, Country, Email, SubTotal, ShipCost, Tax, TotalCost, Validate_Ind,
    //        Verification, Ship_Ind, OrderDate, Comment, CouponId, Couponamount);

    //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //        int ret = obj.InsertOrders(record);

    //        BizObject.PurgeCacheItems("Orders_Orders");
    //        return ret;
    //    }


    //    /// <summary>
    //    /// Updates an existing Order
    //    /// </summary>
    //    public static bool UpdateOrders(int OrderID, int UserID, string FirstName, string LastName, string Address1, string Address2,
    //    string City, string State, string Zip, string Country, string Email, decimal SubTotal, decimal ShipCost, decimal Tax, decimal TotalCost, bool Validate_Ind,
    //    string Verification, bool Ship_Ind, DateTime OrderDate, string Comment, int CouponId, decimal Couponamount)
    //    {
    //        OrdersInfo record = new OrdersInfo(OrderID, UserID, FirstName, LastName, Address1, Address2,
    //        City, State, Zip, Country, Email, SubTotal, ShipCost, Tax, TotalCost, Validate_Ind,
    //        Verification, Ship_Ind, OrderDate, Comment, CouponId, Couponamount);

    //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //        obj.UpdateOrders(record);
    //        return false;

    //        //bool ret = SiteProvider1.PR.UpdateOrders(record);

    //        //BizObject.PurgeCacheItems("Orders_Orders" + OrderID.ToString());
    //        //BizObject.PurgeCacheItems("Orders_Orders");
    //        //return ret;
    //    }


    //    /// <summary>
    //    /// Deletes an existing Order, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteOrders(int OrdersID)
    //    {
    //        bool IsOKToDelete = OKToDelete(OrdersID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteOrders(OrdersID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }


    //    /// <summary>
    //    /// Deletes an existing Orders - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteOrders(int OrdersID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider1.PR.DeleteOrders(OrdersID);
    //        BizObject.PurgeCacheItems("Orders_Orders");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Checks to see if a Orders can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int EDefID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a Discount object filled with the data taken from the input DiscountInfo
    //    /// </summary>
    //    private static Orders GetOrdersFromOrdersInfo(OrdersInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new Orders(record.OrderID, record.UserID, record.FirstName, record.LastName, record.Address1, record.Address2,
    //       record.City, record.State, record.Zip, record.Country, record.Email, record.SubTotal, record.ShipCost, record.Tax, record.TotalCost, record.Validate_Ind,
    //       record.Verification, record.Ship_Ind, record.OrderDate, record.Comment, record.CouponId, record.Couponamount);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of Orders objects filled with the data taken from the input list of DiscountInfo
    //    /// </summary>
    //    private static List<Orders> GetOrdersListFromOrdersInfoList(List<OrdersInfo> recordset)
    //    {
    //        List<Orders> Orders = new List<Orders>();
    //        foreach (OrdersInfo record in recordset)
    //            Orders.Add(GetOrdersFromOrdersInfo(record));
    //        return Orders;
    //    }

    //    public static void UpdateOrdersForOrderID(OrdersInfo Orders, int OrderID)
    //    {
    //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //        obj.UpdateOrdersForOrderID(Orders, OrderID);
    //    }


    //}
        //public class OrderItem : BasePR
        //{
        //    public OrderItem()
        //    { }

        //    private int _OrderItemID = 0;
        //    public int OrderItemID
        //    {
        //        get { return _OrderItemID; }
        //        private set { _OrderItemID = value; }
        //    }

        //    private int _OrderID = 0;
        //    public int OrderID
        //    {
        //        get { return _OrderID; }
        //        private set { _OrderID = value; }
        //    }

        //    private int _TopicID = 0;
        //    public int TopicID
        //    {
        //        get { return _TopicID; }
        //        private set { _TopicID = value; }
        //    }

        //    private decimal _Cost = 0;
        //    public decimal Cost
        //    {
        //        get { return _Cost; }
        //        set { _Cost = value; }
        //    }

        //    private string _Media_Type = "";
        //    public string Media_Type
        //    {
        //        get { return _Media_Type.ToLower(); }
        //        private set { _Media_Type = value.ToLower(); }
        //    }

        //    private int _Quantity = 0;
        //    public int Quantity
        //    {
        //        get { return _Quantity; }
        //        private set { _Quantity = value; }
        //    }

        //    private int _DiscountID = 0;
        //    public int DiscountID
        //    {
        //        get { return _DiscountID; }
        //        private set { _DiscountID = value; }
        //    }

        //    private bool _Optin_Ind = false;
        //    public bool Optin_Ind
        //    {
        //        get { return _Optin_Ind; }
        //        set { _Optin_Ind = value; }
        //    }

        //    private string _topicName = "";
        //    public string TopicName
        //    {
        //        get { return _topicName; }
        //        set { _topicName = value; }
        //    }

        //    private decimal _Credits = 0;
        //    public decimal Credits
        //    {
        //        get { return _Credits; }
        //        set { _Credits = value; }
        //    }

        //    private string _Course_Number = "";
        //    public string Course_Number
        //    {
        //        get { return _Course_Number; }
        //        set { _Course_Number = value; }
        //    }




        //    public OrderItem(int OrderItemID, int OrderID, int TopicID, Decimal Cost, string Media_Type,
        //    int Quantity, int DiscountID, bool Optin_Ind)
        //      {
        //          this.OrderItemID = OrderItemID;
        //          this.OrderID = OrderID;
        //          this.TopicID = TopicID;
        //          this.Cost = Cost;
        //          this.Media_Type = Media_Type;
        //          this.Quantity = Quantity;
        //          this.DiscountID = DiscountID;
        //          this.Optin_Ind = Optin_Ind;
        //          this.TopicName = Topic.GetTopicByID(TopicID).TopicName;
        //          this.Credits = Topic.GetTopicByID(TopicID).Hours;
        //          this.Course_Number = Topic.GetTopicByID(TopicID).Course_Number;

        //       }


        //    private static OrderItem  GetOrderItemFromOrderItemInfo(OrderItemInfo record)
        //    {
        //        if (record == null)
        //            return null;
        //        else
        //        {
        //            return new OrderItem(record.OrderItemID, record.OrderID, record.TopicID, record.Cost, record.Media_Type, record.Quantity,
        //       record.DiscountID, record.Optin_Ind);
        //        }
        //    }

        //    /// <summary>
        //    /// Returns a list of Orders objects filled with the data taken from the input list of DiscountInfo
        //    /// </summary>
        //    private static List<OrderItem> GetOrderItemFromOrderItemInfoList(List<OrderItemInfo> recordset)
        //    {
        //        List<OrderItem> Orders = new List<OrderItem>();
        //        foreach (OrderItemInfo record in recordset)
        //            Orders.Add(GetOrderItemFromOrderItemInfo(record));
        //        return Orders;
        //    }

        //    public static List<OrderItem> GetOrderDetails(int OrderID)
        //    {
        //        List<OrderItem> Orders = null;
        //        string key = "Orders_Orders" + OrderID.ToString();


        //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
        //        {
        //            Orders = (List<OrderItem>)BizObject.Cache[key];
        //        }
        //        else
        //        {
        //            List<OrderItemInfo> recordset = new List<OrderItemInfo>();
        //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
        //            recordset = (obj.GetOrderDetails(OrderID));

        //            //recordset.Add(recordsetinfo);
        //            Orders = GetOrderItemFromOrderItemInfoList(recordset);                    
        //            BasePR.CacheData(key, Orders);
        //        }
        //        return Orders;
        //    }

        //    /// <summary>
        //    /// Creates new orders
        //    /// </summary>
        //    public static int InsertOrderItem(int OrderItemID, int OrderID, int TopicID, Decimal Cost, string Media_Type,
        //    int Quantity, int DiscountID, bool Optin_Ind)
        //    {
        //        OrderItemInfo record = new OrderItemInfo(0, OrderID, TopicID, Cost, Media_Type, Quantity,
        //        DiscountID, Optin_Ind);

        //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
        //        int ret = obj.InsertOrderItem(record);
        //        BizObject.PurgeCacheItems("OrderItems_OrderItems");
        //        return ret;
        //    }


        //} 

    

    //public class MembershipLog : BasePR
    //{

    //    public MembershipLog()
    //    {
    //    }

    //private int _mlid = 0;
    //public int mlid
    //{
    //    get { return _mlid; }
    //    protected set { _mlid = value; }
    //}

    //private int _userid = 0;
    //public int userid
    //{
    //    get { return _userid; }
    //    protected set { _userid = value; }
    //}

    //private DateTime  _logdate;
    //public DateTime logdate
    //{
    //    get { return _logdate; }
    //    protected set { _logdate = value; }
    //}

    //private DateTime _expdate;
    //public DateTime expdate
    //{
    //    get { return _expdate; }
    //    protected set { _expdate = value; }
    //}

    //private string _verification;
    //public string verification
    //{
    //    get { return _verification; }
    //    protected set { _verification = value; }
    //}


    //private string _profileid;
    //public string profileid
    //{
    //    get { return _profileid; }
    //    protected set { _profileid = value; }
    //}

    //private bool _renew_ind = false;
    //public bool  renew_ind
    //{
    //    get { return _renew_ind; }
    //    private set { _renew_ind = value; }
    //}

    //private bool _active_ind = false;
    //public bool  active_ind
    //{
    //    get { return _active_ind; }
    //    private set { _active_ind = value; }
    //}

    //private decimal _amount = 0;
    //public decimal amount
    //{
    //    get { return _amount; }
    //    set { _amount = value; }
    //}

    //private string _comment;
    //public string comment
    //{
    //    get { return _comment; }
    //    protected set { _comment = value; }
    //}

    //private string _ccnum;
    //public string ccnum
    //{
    //    get { return _ccnum; }
    //    protected set { _ccnum = value; }
    //}

    //private bool _firstrec;
    //public bool FirstRec
    //{
    //    get { return _firstrec; }
    //   set { _firstrec = value; }
    //}


    //public MembershipLog(int mlid, int userid, DateTime logdate, DateTime expdate, string verification, string profileid,
    //                     bool renew_ind, bool active_ind, decimal amount, string comment, string ccnum)
    //{
    //    this.mlid = mlid;
    //    this.userid = userid;
    //    this.logdate = logdate;
    //    this.expdate = expdate;
    //    this.verification = verification;
    //    this.profileid = profileid;
    //    this.renew_ind = renew_ind;
    //    this.active_ind = active_ind;
    //    this.amount = amount;
    //    this.comment = comment;
    //    this.ccnum = ccnum;

    //}

    //public bool Update()
    //{
    //return MembershipLog.UpdateMembershipLog(this.mlid, this.userid, this.logdate, this.expdate, this.verification, this.profileid,
    //this.renew_ind, this.active_ind, this.amount, this.comment, this.ccnum);
    //}
    //    ///Static Methods///

    //    /// <summary>
    //    /// Updates the membershiplog
    //    /// </summary>
      
    //public static bool UpdateMembershipLog(int mlid, int userid, DateTime logdate, DateTime expdate, string verification, string profileid,
    //                     bool renew_ind, bool active_ind, decimal amount, string comment, string ccnum)
    //{
    //    MembershipLogInfo record = new MembershipLogInfo(mlid, userid, logdate, expdate, verification, profileid, renew_ind, active_ind, amount, comment, ccnum);
    //    PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //    obj.UpdateMembershipLog(record);
    //    return false;
    //}

    //public static bool InsertMembershipLog( int userid, DateTime logdate, DateTime expdate, string verification, string profileid,
    //                   bool renew_ind, bool active_ind, decimal amount, string comment, string ccnum)
    //{
    //    MembershipLogInfo record = new MembershipLogInfo(userid, logdate, expdate, verification, profileid, renew_ind, active_ind, amount, comment, ccnum);
    //    PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //    obj.InsertMembershipLog(record);
    //    return false;
    //}
    //public static List<MembershipLogInfo> GetMembershipLogListByUserID(int userid)
    //{
    //    List<MembershipLogInfo> memlogList = new List<MembershipLogInfo>();
       
    //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //        memlogList = obj.GetMembershipLogListByUserID(userid);// GetMembershipLogFromMembershipLogInfo(recordset);
    //        //BasePR.CacheData(key, memlog);

    //        return memlogList;
    //}
        
    //    public static List<MembershipLog> GetMembershipLogListfromMembershipLogInfo(List<MembershipLogInfo> recordset)
    //{
    //    List<MembershipLog> memlogs = new List<MembershipLog>();
    //    foreach (MembershipLogInfo memloginfo in recordset)
    //    {
    //        MembershipLog memlog = GetMembershipLogFromMembershipLogInfo(memloginfo);
    //        memlogs.Add(memlog);
    //    }
    //    return memlogs;
    //}
    //private static MembershipLog GetMembershipLogFromMembershipLogInfo(MembershipLogInfo memlog)
    //{
    //    MembershipLog mem = new MembershipLog();
    //    mem.mlid = memlog.mlid;
    //    mem.userid = memlog.userid;
    //    mem.logdate = memlog.logdate;
    //    mem.expdate = memlog.expdate;
    //    mem.verification = memlog.verification;
    //    mem.profileid = memlog.profileid;
    //    mem.renew_ind = memlog.renew_ind;
    //    mem.active_ind = memlog.active_ind;
    //    mem.amount = memlog.amount;
    //    mem.comment = memlog.comment;
    //    mem.ccnum = memlog.ccnum;

    //    return mem;

    //}

    //      public static MembershipLog GetMembershipLogByUserID(int userid)
    //    {
    //        MembershipLog memlog = null;
    //        string key = "memlog_MembershipLog" + userid.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            memlog = (MembershipLog)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            MembershipLogInfo recordset = obj.GetMembershipLogByUserID(userid);            
    //            memlog = GetMembershipLogFromMembershipLogInfo(recordset);
    //            BasePR.CacheData(key, memlog);
    //        }
    //        return memlog;
    //    }

    //      public static bool CancelMembershipLog(int userid)
    //      {
    //          PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //          return obj.CancelMembershipLog(userid);
    //      }


    //}

    //public class Coupons : BasePR
    //{
    //    public Coupons()
    //    {
    //    }


    //    private int _CID = 0;
    //    public int CID
    //    {
    //        get { return _CID; }
    //        set { _CID = value; }
    //    }

    //    private string _CouponCode = "";
    //    public string CouponCode
    //    {
    //        get { return _CouponCode.ToLower(); }
    //        set { _CouponCode = value.ToLower(); }
    //    }

    //    private string _CouponDesc = "";
    //    public string CouponDesc
    //    {
    //        get { return _CouponDesc.ToLower(); }
    //        set { _CouponDesc = value.ToLower(); }
    //    }

    //    private decimal _CAmount = 0;
    //    public decimal CAmount
    //    {
    //        get { return _CAmount; }
    //        set { _CAmount = value; }
    //    }

    //    private DateTime _ExpireDate = System.DateTime.MinValue;
    //    public DateTime ExpireDate
    //    {
    //        get { return _ExpireDate; }
    //        set { _ExpireDate = value; }
    //    }

    //    private int _FacilityID = 0;
    //    public int FacilityID
    //    {
    //        get { return _FacilityID; }
    //        set { _FacilityID = value; }
    //    }

    //    private string _CouponType = "";
    //    public string CouponType
    //    {
    //        get { return _CouponType; }
    //        set { _CouponType = value; }
    //    }

    //    private int _TopicID = 0;
    //    public int TopicID
    //    {
    //        get { return _TopicID; }
    //        set { _TopicID = value; }
    //    }


    //    private string _MediaType = "";
    //    public string MediaType
    //    {
    //        get { return _MediaType; }
    //        set { _MediaType = value; }
    //    }


    //    public static List<Coupons> GetCoupons(string facilityid, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";
    //        List<Coupons> Coup = null;
    //        string key = "Coup_couponcode" + cSortExpression;
    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Coup = (List<Coupons>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            List<CouponsInfo> recordset = obj.GetCoupons(facilityid, cSortExpression);
    //            Coup = GetCouponsListfromCouponsInfo(recordset);
    //            BasePR.CacheData(key, Coup);
    //        }
    //        return Coup;
    //    }

    //    public static List<Coupons> GetCouponsListfromCouponsInfo(List<CouponsInfo> recordset)
    //    {

    //        List<Coupons> coupns = new List<Coupons>();
    //        foreach (CouponsInfo coupinfo in recordset)
    //        {
    //            Coupons coup = GetCouponsFromCouponsInfo(coupinfo);

    //            coupns.Add(coup);
    //        }
    //        return coupns;
    //    }


    //    private static Coupons GetCouponsFromCouponsInfo(CouponsInfo coupinfo)
    //    {
    //        Coupons coup = new Coupons();
    //        coup.CID = coupinfo.CID;
    //        coup.CouponCode = coupinfo.CouponCode;
    //        coup.CouponDesc = coupinfo.CouponDesc;
    //        coup.CAmount = coupinfo.CAmount;
    //        coup.ExpireDate = coupinfo.ExpireDate;
    //        coup.FacilityID = coupinfo.FacilityID;
    //        coup.CouponType = coupinfo.CouponType;
    //        coup.TopicID = coup.TopicID;
    //        coup.MediaType = coup.MediaType;

    //        return coup;
    //    }

    //    public static Coupons GetCouponByFacID(int facilityid)
    //    {
    //        List<Coupons> Coupons = null;
    //        string key = "Coupons_Coupons" + facilityid.ToString();


    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Coupons = (List<Coupons>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<CouponsInfo> recordset = new List<CouponsInfo>();
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            CouponsInfo recordsetinfo = (obj.GetCouponByFacID(facilityid));
    //            if (recordsetinfo != null)
    //            {
    //                recordset.Add(recordsetinfo);
    //                Coupons = GetCouponsListFromCouponsInfoList(recordset);
    //                //Orders = obj.GetOrdersByID(OrderID);
    //                BasePR.CacheData(key, Coupons);
    //            }
    //            else
    //            {
    //                return null;
    //            }
    //        }
    //        return Coupons[0];
    //    }

    //    private static List<Coupons> GetCouponsListFromCouponsInfoList(List<CouponsInfo> recordset)
    //    {
    //        List<Coupons> Coupons = new List<Coupons>();
    //        foreach (CouponsInfo record in recordset)
    //            Coupons.Add(GetCouponsFromCouponsInfo(record));
    //        return Coupons;
    //    }


    //    public static int GetCouponIdByCode(string Code)
    //    {
    //        int iCouponid;
    //           PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //           iCouponid = obj.GetCouponIdByCode(Code);
    //           return iCouponid;
    //    }


    //}

    //public class UCEmembership : BasePR
    //{

    //    public UCEmembership()
    //    {
    //    }

    //    private int _UmID = 0;
    //    public int UmID
    //    {
    //        get { return _UmID; }
    //        set { _UmID = value; }
    //    }

    //    private int _RnID = 0;
    //    public int RnID
    //    {
    //        get { return _RnID; }
    //        set { _RnID = value; }
    //    }

    //    private DateTime _LogDate = System.DateTime.MinValue;
    //    public DateTime LogDate
    //    {
    //        get { return _LogDate; }
    //        set { _LogDate = value; }
    //    }

    //    private DateTime _ExpDate = System.DateTime.MinValue;
    //    public DateTime ExpDate
    //    {
    //        get { return _ExpDate; }
    //        set { _ExpDate = value; }
    //    }

    //    private string _Verification = "";
    //    public string Verification
    //    {
    //        get { return _Verification; }
    //        set { _Verification = value; }
    //    }

    //    private string _ProfileID = "";
    //    public string ProfileID
    //    {
    //        get { return _ProfileID.ToLower(); }
    //        set { _ProfileID = value.ToLower(); }
    //    }

    //    private bool _Renew = false;
    //    public bool Renew
    //    {
    //        get { return _Renew; }
    //        set { _Renew = value; }
    //    }

    //    private bool _Active = false;
    //    public bool Active
    //    {
    //        get { return _Active; }
    //        set { _Active = value; }
    //    }

    //    private string _Comment = "";
    //    public string Comment
    //    {
    //        get { return _Comment.ToLower(); }
    //        set { _Comment = value.ToLower(); }
    //    }

    //    private float _Amount = 0;
    //    public float Amount
    //    {
    //        get { return _Amount; }
    //        set { _Amount = value; }
    //    }
    //    private string _CcNum = "";
    //    public string CcNum
    //    {
    //        get { return _CcNum; }
    //        set { _CcNum = value; }
    //    }


    //    private bool _FirstRec;
    //    public bool FirstRec
    //    {
    //        get { return _FirstRec; }
    //        set { _FirstRec = value; }
    //    }


    //    public UCEmembership(int UmID, int RnID, DateTime LogDate, DateTime ExpDate, string Verification, string ProfileID, bool Renew, bool Active, string Comment,
    //         float Amount, string CcNum)
    //    {

    //        this.UmID = UmID;
    //        this.RnID = RnID;
    //        this.LogDate = LogDate;
    //        this.ExpDate = ExpDate;
    //        this.Verification = Verification;
    //        this.ProfileID = ProfileID;
    //        this.Renew = Renew;
    //        this.Active = Active;
    //        this.Comment = Comment;
    //        this.Amount = Amount;
    //        this.CcNum = CcNum;

    //    }

    //    public bool Update()
    //    {
    //        return UCEmembership.UpdateUCEmembership(this.UmID, this.RnID, this.LogDate, this.ExpDate, this.Verification, this.ProfileID, this.Renew, this.Active,
    //            this.Comment, this.Amount, this.CcNum);
    //    }

    //    //******Static Methods******//

    //    /// <summary>
    //    /// Updates the UCEmembership
    //    /// </summary>
    //    /// 

    //    public static bool UpdateUCEmembership(int UmID, int RnID, DateTime LogDate, DateTime ExpDate, string Verification, string ProfileID, bool Renew, bool Active, string Comment,
    //         float Amount, string CcNum)
    //    {
    //        UCEmembershipInfo record = new UCEmembershipInfo(UmID, RnID, LogDate, ExpDate, Verification, ProfileID, Renew, Active, Comment, Amount, CcNum);
    //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //        obj.UpdateUCEmembership(record);
    //        return false;
    //    }

    //    public static bool InsertUCEmembership(int RnID, DateTime LogDate, DateTime ExpDate, string Verification, string ProfileID, bool Renew, bool Active, string Comment,
    //         float Amount, string CcNum)
    //    {
    //        UCEmembershipInfo record = new UCEmembershipInfo(RnID, LogDate, ExpDate, Verification, ProfileID, Renew, Active, Comment, Amount, CcNum);
    //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //        return(obj.InsertUCEmembership(record));
           
    //    }

    //    public static List<UCEmembershipInfo> GetUCEmembershipListByRnID(int RnID)
    //    {
    //        List<UCEmembershipInfo> ucememList = new List<UCEmembershipInfo>();
    //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //        ucememList = obj.GetUCEmembershipListByRnID(RnID);

    //        return ucememList;
    //    }
    //    public static List<UCEmembership> GetUCEmembershipListfromUCEmembershipInfo(List<UCEmembershipInfo> recordset)
    //    {
    //        List<UCEmembership> ucemems = new List<UCEmembership>();

    //        foreach (UCEmembershipInfo ucememinfo in recordset)
    //        {
    //            UCEmembership ucemem = GetUCEmembershipFromUCEmembershipInfo(ucememinfo);
    //            ucemems.Add(ucemem);
    //        }
    //        return ucemems;

    //    }

    //    private static UCEmembership GetUCEmembershipFromUCEmembershipInfo(UCEmembershipInfo ucemem)
    //    {
    //        UCEmembership uce = new UCEmembership();
    //        uce.UmID = ucemem.UmID;
    //        uce.RnID = ucemem.RnID;
    //        uce.LogDate = ucemem.LogDate;
    //        uce.ExpDate = ucemem.ExpDate;
    //        uce.Verification = ucemem.Verification;
    //        uce.ProfileID = ucemem.ProfileID;
    //        uce.Renew = ucemem.Renew;
    //        uce.Active = ucemem.Active;
    //        uce.Comment = ucemem.Comment;
    //        uce.Amount = ucemem.Amount;
    //        uce.CcNum = ucemem.CcNum;
    //        return uce;

    //    }

    //    public static UCEmembership GetUCEmembershipByRnID(int RnID)
    //    {
    //        UCEmembership ucememship = null;
    //        string key = " ucememship_UCEmembership" + RnID.ToString();
    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ucememship = (UCEmembership)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //            UCEmembershipInfo recordset = obj.GetUCEmembershipByRnID(RnID);
    //            ucememship = GetUCEmembershipFromUCEmembershipInfo(recordset);
    //            BasePR.CacheData(key, ucememship);
    //        }
    //        return ucememship;

    //    }


    //    public static bool CancelUCEmembership(int RnID)
    //    {
    //        PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
    //        return obj.CancelUCEmembership(RnID);

    //    }

    //}

    }



        

        




