﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using PearlsReview.QTI;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr1
    /// </summary>

    public class ICEProgress : BasePR
    {
        public ICEProgress(int ipid, int testid, int ip_topicid, DateTime? ip_lastmod, int? ip_score, string ip_status, bool ip_vignettecomplete, string ip_XMLTest, int ip_TestVers)
        {
            this.Ipid = ipid;
            this.Testid = testid;
            this.Ip_topicid = ip_topicid;
            this.Ip_lastmod = ip_lastmod;
            this.Ip_score = ip_score;
            this.Ip_status = ip_status;
            this.Ip_vignettecomplete = ip_vignettecomplete;
            this.Ip_XMLTest = ip_XMLTest;
            this.Ip_TestVers = ip_TestVers;
        }


        #region Variables and Properties
        private int _ipid = 0;
        public int Ipid
        {
            get { return _ipid; }
            set { _ipid = value; }
        }
        private int _testid = 0;
        public int Testid
        {
            get { return _testid; }
            set { _testid = value; }
        }
        private int _ip_topicid = 0;
        public int Ip_topicid
        {
            get { return _ip_topicid; }
            set { _ip_topicid = value; }
        }
        private DateTime? _ip_lastmod;
        public DateTime? Ip_lastmod
        {
            get { return _ip_lastmod; }
            set { _ip_lastmod = value; }
        }
        private int? _ip_score = 0;
        public int? Ip_score
        {
            get { return _ip_score; }
            set { _ip_score = value; }
        }
        private string _ip_status = "";
        public string Ip_status
        {
            get { return _ip_status; }
            set { _ip_status = value; }
        }
        private bool _ip_vignettecomplete = false;
        public bool Ip_vignettecomplete
        {
            get { return _ip_vignettecomplete; }
            set { _ip_vignettecomplete = value; }
        }
        private string _ip_XMLTest = "";
        public string Ip_XMLTest
        {
            get { return _ip_XMLTest; }
            set { _ip_XMLTest = value; }
        }
        private int _ip_TestVers = 0;
        public int Ip_TestVers
        {
            get { return _ip_TestVers; }
            set { _ip_TestVers = value; }
        }

        #endregion Variables and Properties

        #region Methods
        public static int insertICEProgress(int testid, int ip_topicid, DateTime? ip_lastmod, int? ip_score, string ip_status, string ip_XMLTest, int ip_TestVers)
        {
            ICEProgressInfo record = new ICEProgressInfo(0, testid, ip_topicid, ip_lastmod, ip_score, ip_status, false, ip_XMLTest, ip_TestVers);            
            int ret = SiteProvider.PR2.insertICEProgress(record);
            BizObject.PurgeCacheItems("ICEProgress_ICEProgress_Insert");
            return ret;
        }

        public int insert()
        {
            return insertICEProgress(this.Testid, this.Ip_topicid, this.Ip_lastmod, this.Ip_score, this.Ip_status, this.Ip_XMLTest, this.Ip_TestVers);
        }

        public static bool updateICEProgress(int ipid, int testid, int ip_topicid, DateTime? ip_lastmod, int? ip_score, string ip_status, bool ip_vignettecomplete, string ip_XMLTest, int ip_TestVers)
        {
            ICEProgressInfo record = new ICEProgressInfo(ipid, testid, ip_topicid, ip_lastmod, ip_score, ip_status, ip_vignettecomplete, ip_XMLTest, ip_TestVers);            
            bool ret = SiteProvider.PR2.updateICEProgress(record);
            BizObject.PurgeCacheItems("ICEProgress_ICEProgress_Update" + ipid.ToString());
            return ret;
        }

        public static int GetICEProgressCompletionPercentageP(int testid)
        {
            return SiteProvider.PR2.GetICEProgressCompletionPercentageP(testid);
        }

        public bool update()
        {
            return updateICEProgress(this.Ipid, this.Testid, this.Ip_topicid, this.Ip_lastmod, this.Ip_score, this.Ip_status, this.Ip_vignettecomplete, this.Ip_XMLTest, this.Ip_TestVers);
        }

        public bool deleteICEProgress(int ipid)
        {
            bool ret = SiteProvider.PR2.deleteICEProgress(ipid);
            BizObject.PurgeCacheItems("ICEProgress_ICEProgress_Delete" + ipid.ToString());
            return ret;
        }

        public static ICEProgress GetICEProgressByID(int ipid)
        {
            ICEProgress iceProgress = null;
            string key = "ICEProgress_GetICEProgressByID_" + ipid.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                iceProgress = (ICEProgress)BizObject.Cache[key];
            }
            else
            {
                iceProgress = GetICEProgressFromICEProgressInfo(SiteProvider.PR2.GetICEProgressByID(ipid));
                BasePR.CacheData(key, iceProgress);
            }
            return iceProgress;
        }

        public static ICEProgress GetICEProgressByUserIDAndTopicID(int userid, int topicid)
        {
            ICEProgress iceProgress = null;
            string key = "ICEProgress_GetICEProgressByUserIDAndTopicID_" + userid.ToString() + topicid.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                iceProgress = (ICEProgress)BizObject.Cache[key];
            }
            else
            {
                iceProgress = GetICEProgressFromICEProgressInfo(SiteProvider.PR2.GetICEProgressByUserIDAndTopicID(userid, topicid));
                BasePR.CacheData(key, iceProgress);
            }
            return iceProgress;
        }        

        public static DataSet GetICEProgressListByICESectionID(int isid, int userid)
        {
            DataSet iceProgressList = null;
            string key = "ICEProgress_GetICEProgressListByICESectionID_" + isid.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                iceProgressList = (DataSet)BizObject.Cache[key];
            }
            else
            {
                iceProgressList = SiteProvider.PR2.GetICEProgressListByICESectionID(isid, userid);
                BasePR.CacheData(key, iceProgressList);
            }
            return iceProgressList;
        }

        public static DataSet GetICEProgressListByICESectionIDP(int isid, int userid,int testid)
        {
            DataSet iceProgressList = null;
            string key = "ICEProgress_GetICEProgressListByICESectionID_" + isid.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                iceProgressList = (DataSet)BizObject.Cache[key];
            }
            else
            {
                iceProgressList = SiteProvider.PR2.GetICEProgressListByICESectionIDP(isid, userid,testid);
                BasePR.CacheData(key, iceProgressList);
            }
            return iceProgressList;
        }

        public static bool EnrollICEProgressByTestId(int testid, bool reduceseat)
        {
            return SiteProvider.PR2.EnrollICEProgressByTestId(testid, reduceseat);
        }

        public static int GetICEProgressCompletionPercentage(int topicid, int userid) 
        {
            return SiteProvider.PR2.GetICEProgressCompletionPercentage(topicid, userid);
        }
               
        /// <summary>
        /// Returns a list of vignette question objects for the specified Ipid
        /// </summary>
        public static List<QTIQuestionObject> GetVignetteQuestionObjectListByIpid(int ipid, string sUserAnswers)
        {
            ViDefinition oViDefinition = null;
            QTIUtils oQTIUtils = new QTIUtils();

            // NOTE: No caching of these items, because they contain user responses
            // along with test questions
            List<QTIQuestionObject> QTIQuestionObjects = null;

            // first, get the test record so we have access to topicid and user's responses
            ICEProgress oICEProgress = ICEProgress.GetICEProgressByID(ipid);

            if (oICEProgress != null)
            {
                // get the vignette definition record for this topic
                oViDefinition = ViDefinition.GetViDefinitionByTopicID(oICEProgress.Ip_topicid);

                if (oViDefinition != null)
                {
                    // get the QTIQuestionObjectList from the ViDefinition XML
                    QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oViDefinition.XMLTest);
                }
            }

            if (QTIQuestionObjects != null)
            {
                // go a list of QTIQuestionObjects, so now add the
                // user responses, if any

                // first, if the user completed the test, we will show the completed test
                // with user's answers filled in -- but we will always show the latest version
                // of the test and will fill in the correct answers automatically so it always
                // matches the current version of the test

                // If NOT completed yet,
                // if the user's test record shows version = 0, there are no user responses yet
                // (this is the condition for all tests in progress migrated from the old system,
                // as well as for new tests added to MyCurriculum and not worked on yet).
                // In this case, just fill in all user responses with "X" for M/C and T/F question types
                // and with blank for all others

                // also do this if the test version in the TestDefinition record is different from the
                // version recorded with the answers so far in the Test record. This will "reset" the
                // user's answers to unanswered status so it matches the new test questions.
                if (oICEProgress.Ip_status == "C")
                {
                    // completed test, so just fill in the correct answers for the current version 
                    // of the test
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                        {
                            QTIQuestion.cUserAnswer = QTIQuestion.cCorrectAnswer;
                        }
                        else
                        {
                            QTIQuestion.cUserAnswer = "";
                        }
                    }
                }               
                else if (sUserAnswers == null)
                {
                    // incomplete test that does not match the current vignette definition version number
                    // set to default X and blank answers
                    // NOTE: This is also the case with new test record that has not had the vignette
                    // shown yet -- it will start out with vignetteversion field = 0 to this
                    // comparison will fail and we will insert all empty answers
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                        {
                            QTIQuestion.cUserAnswer = "X";
                        }
                        else
                        {
                            QTIQuestion.cUserAnswer = "";
                        }
                    }
                }              
                else
                {
                    // incomplete test that matches the current test definition version number
                    // so it may have some user responses stored. Grab them from the test XMLVignette field
                    // and populate them here.
                    List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(sUserAnswers);
                    String[] Answers = UserAnswers.ToArray();

                    int iLoop = 0;
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        QTIQuestion.cUserAnswer = Answers[iLoop];
                        // be sure we're not empty -- if so, set to X
                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                            QTIQuestion.cUserAnswer = "X";
                        iLoop++;
                    }
                }
            }
            return QTIQuestionObjects;
        }

        public static List<QTIQuestionObject> GetTestQuestionObjectListByIpid(int Ipid)
        {
            TestDefinition oTestDefinition = null;
            QTIUtils oQTIUtils = new QTIUtils();

            // NOTE: No caching of these items, because they contain user responses
            // along with test questions
            List<QTIQuestionObject> QTIQuestionObjects = null;

            // first, get the test record so we have access to topicid and user's responses
            ICEProgress oICEProgress = ICEProgress.GetICEProgressByID(Ipid);

            if (oICEProgress != null)
            {
                // get the test definition record for this topic
                oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(oICEProgress.Ip_topicid);

                if (oTestDefinition != null)
                {
                    // get the QTIQuestionObjectList from the TestDefinition XML
                    QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
                    // workaround until XMl is correct
                    //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
                }

                if (QTIQuestionObjects != null)
                {
                    #region "description"
                    // go a list of QTIQuestionObjects, so now add the
                    // user responses, if any

                    // first, if the user completed the test, we will show the completed test
                    // with user's answers filled in -- but we will always show the latest version
                    // of the test and will fill in the correct answers automatically so it always
                    // matches the current version of the test

                    // If NOT completed yet,
                    // if the user's test record shows version = 0, there are no user responses yet
                    // (this is the condition for all tests in progress migrated from the old system,
                    // as well as for new tests added to MyCurriculum and not worked on yet).
                    // In this case, just fill in all user responses with "X" for M/C and T/F question types
                    // and with blank for all others

                    // also do this if the test version in the TestDefinition record is different from the
                    // version recorded with the answers so far in the Test record. This will "reset" the
                    // user's answers to unanswered status so it matches the new test questions. 
                    #endregion


                    if (oICEProgress.Ip_TestVers != oTestDefinition.Version)
                    {
                        #region " version not equals"

                        List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oICEProgress.Ip_XMLTest);
                        String[] Answers = UserAnswers.ToArray();

                        int iLoop = 0;
                        foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                        {
                            if (Convert.ToInt32(QTIQuestion.cHeaderID) > 0 && iLoop < Answers.Length)
                            {
                                QTIQuestion.cUserAnswer = Answers[iLoop];
                                // be sure we're not empty -- if so, set to X
                                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                                    QTIQuestion.cUserAnswer = "X";
                                iLoop++;
                            }
                        }                       
                        #endregion
                    }
                    else if (oICEProgress.Ip_status == "C")
                    {
                        #region "version equals and test completed"                      

                        // completed test ??? that matches the current test definition version number ???
                        // so it may have some user responses stored. Grab them from the test XMLTest field
                        // and populate them here.
                        List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oICEProgress.Ip_XMLTest);
                        String[] Answers = UserAnswers.ToArray();

                        int iLoop = 0;
                        foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                        {
                            if (Convert.ToInt32(QTIQuestion.cHeaderID) > 0)
                            {
                                QTIQuestion.cUserAnswer = Answers[iLoop];
                                // be sure we're not empty -- if so, set to X
                                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                                    QTIQuestion.cUserAnswer = "X";
                                iLoop++;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        // incomplete test that matches the current test definition version number
                        // so it may have some user responses stored. Grab them from the test XMLTest field
                        // and populate them here.
                        List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oICEProgress.Ip_XMLTest);
                        String[] Answers = UserAnswers.ToArray();

                        int iLoop = 0;
                        foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                        {
                            if (Convert.ToInt32(QTIQuestion.cHeaderID) > 0 && iLoop < Answers.Length)
                            {
                                QTIQuestion.cUserAnswer = Answers[iLoop];
                                // be sure we're not empty -- if so, set to X
                                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                                    QTIQuestion.cUserAnswer = "X";
                                iLoop++;
                            }


                        }
                    }
                }
            }
            return QTIQuestionObjects;
        }



        public static List<ICEProgress> GetICEProgressListfromICEProgressInfo(List<ICEProgressInfo> recordset)
        {
            List<ICEProgress> iceProgressList = new List<ICEProgress>();
            foreach (ICEProgressInfo iceProgressninfo in recordset)
            {
                ICEProgress iceProgress = GetICEProgressFromICEProgressInfo(iceProgressninfo);

                iceProgressList.Add(iceProgress);
            }
            return iceProgressList;
        }


        private static ICEProgress GetICEProgressFromICEProgressInfo(ICEProgressInfo iceProgressninfo)
        {
            if (iceProgressninfo == null)
                return null;

            ICEProgress iceProgress = new ICEProgress(iceProgressninfo.Ipid, iceProgressninfo.Testid, iceProgressninfo.Ip_topicid,
                iceProgressninfo.Ip_lastmod, iceProgressninfo.Ip_score, iceProgressninfo.Ip_status, iceProgressninfo.Ip_vignettecomplete, iceProgressninfo.Ip_XMLTest, iceProgressninfo.Ip_TestVers);
            return iceProgress;
        }

        private static List<ICEProgress> GetICEProgressListFromICEProgressInfoList(List<ICEProgressInfo> recordset)
        {
            List<ICEProgress> iceProgressList = new List<ICEProgress>();
            foreach (ICEProgressInfo record in recordset)
                iceProgressList.Add(GetICEProgressFromICEProgressInfo(record));
            return iceProgressList;
        }

        #endregion Methods

    }

}
