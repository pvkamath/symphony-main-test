﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

/// <summary>
/// Summary description for EmailQueue
/// </summary>
public class EmailQueue: BasePR
    {
        #region Variables and Properties

        private int _EQueueID = 0;
        public int EQueueID
        {
            get { return _EQueueID; }
            protected set { _EQueueID = value; }
        }

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private int _EDefID = 0;
        public int EDefID
        {
            get { return _EDefID; }
            set { _EDefID = value; }
        }

        private string _SentDate = "";
        public string SentDate
        {
            get { return _SentDate; }
            set { _SentDate = value; }
        }


        public EmailQueue(int EQueueID, int UserID, int EDefID, string SentDate)
        {
            this.EQueueID = EQueueID;
            this.UserID = UserID;
            this.EDefID = EDefID;
            this.SentDate = SentDate;
        }

        public bool Delete()
        {
            bool success = EmailQueue.DeleteEmailQueue(this.EQueueID);
            if (success)
                this.EQueueID = 0;
            return success;
        }

        public bool Update()
        {
            return EmailQueue.UpdateEmailQueue(this.EQueueID, this.UserID, this.EDefID, this.SentDate);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all EmailQueue
        /// </summary>
        public static List<EmailQueue> GetEmailQueue(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "SentDate";

            List<EmailQueue> EmailQueue = null;
            string key = "EmailQueue_EmailQueue_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                EmailQueue = (List<EmailQueue>)BizObject.Cache[key];
            }
            else
            {
                List<EmailQueueInfo> recordset = SiteProvider.PR2.GetEmailQueue(cSortExpression);
                EmailQueue = GetEmailQueueListFromEmailQueueInfoList(recordset);
                BasePR.CacheData(key, EmailQueue);
            }
            return EmailQueue;
        }


        /// <summary>
        /// Returns the number of total EmailQueue
        /// </summary>
        public static int GetEmailQueueCount()
        {
            int EmailQueueCount = 0;
            string key = "EmailQueue_EmailQueueCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                EmailQueueCount = (int)BizObject.Cache[key];
            }
            else
            {
                EmailQueueCount = SiteProvider.PR2.GetEmailQueueCount();
                BasePR.CacheData(key, EmailQueueCount);
            }
            return EmailQueueCount;
        }

        /// <summary>
        /// Returns a EmailQueue object with the specified ID
        /// </summary>
        public static EmailQueue GetEmailQueueByID(int EQueueID)
        {
            EmailQueue EmailQueue = null;
            string key = "EmailQueue_EmailQueue_" + EQueueID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                EmailQueue = (EmailQueue)BizObject.Cache[key];
            }
            else
            {
                EmailQueue = GetEmailQueueFromEmailQueueInfo(SiteProvider.PR2.GetEmailQueueByID(EQueueID));
                BasePR.CacheData(key, EmailQueue);
            }
            return EmailQueue;
        }

        /// <summary>
        /// Updates an existing EmailQueue
        /// </summary>
        public static bool UpdateEmailQueue(int EQueueID, int UserID, int EDefID, string SentDate)
        {


            EmailQueueInfo record = new EmailQueueInfo(EQueueID, UserID, EDefID, SentDate);
            bool ret = SiteProvider.PR2.UpdateEmailQueue(record);

            BizObject.PurgeCacheItems("EmailQueue_EmailQueue_" + EQueueID.ToString());
            BizObject.PurgeCacheItems("EmailQueue_EmailQueue");
            return ret;
        }

        /// <summary>
        /// Creates a new EmailQueue
        /// </summary>
        public static int InsertEmailQueue(int UserID, int EDefID, string SentDate)
        {


            EmailQueueInfo record = new EmailQueueInfo(0, UserID, EDefID, SentDate);
            int ret = SiteProvider.PR2.InsertEmailQueue(record);

            BizObject.PurgeCacheItems("EmailQueue_EmailQueue");
            return ret;
        }

        /// <summary>
        /// Deletes an existing EmailQueue, but first checks if OK to delete
        /// </summary>
        public static bool DeleteEmailQueue(int EQueueID)
        {
            bool IsOKToDelete = OKToDelete(EQueueID);
            if (IsOKToDelete)
            {
                return (bool)DeleteEmailQueue(EQueueID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing EmailQueue - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteEmailQueue(int EQueueID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteEmailQueue(EQueueID);
            //         new RecordDeletedEvent("EmailQueue", EQueueID, null).Raise();
            BizObject.PurgeCacheItems("EmailQueue_EmailQueue");
            return ret;
        }



        /// <summary>
        /// Checks to see if a EmailQueue can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EQueueID)
        {
            return true;
        }



        /// <summary>
        /// Returns a EmailQueue object filled with the data taken from the input EmailQueueInfo
        /// </summary>
        private static EmailQueue GetEmailQueueFromEmailQueueInfo(EmailQueueInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new EmailQueue(record.EQueueID, record.UserID, record.EDefID, record.SentDate);
            }
        }

        /// <summary>
        /// Returns a list of EmailQueue objects filled with the data taken from the input list of EmailQueueInfo
        /// </summary>
        private static List<EmailQueue> GetEmailQueueListFromEmailQueueInfoList(List<EmailQueueInfo> recordset)
        {
            List<EmailQueue> EmailQueue = new List<EmailQueue>();
            foreach (EmailQueueInfo record in recordset)
                EmailQueue.Add(GetEmailQueueFromEmailQueueInfo(record));
            return EmailQueue;
        }

        #endregion
    }
}
