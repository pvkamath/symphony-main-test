﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for EmailDefinition
/// </summary>
    public class EmailDefinition : BasePR
    {
        #region Variables and Properties

        private int _EDefID = 0;
        public int EDefID
        {
            get { return _EDefID; }
            protected set { _EDefID = value; }
        }

        private string _EDefName = "";
        public string EDefName
        {
            get { return _EDefName; }
            set { _EDefName = value; }
        }

        private string _EDefText = "";
        public string EDefText
        {
            get { return _EDefText; }
            set { _EDefText = value; }
        }


        public EmailDefinition(int EDefID, string EDefName, string EDefText)
        {
            this.EDefID = EDefID;
            this.EDefName = EDefName;
            this.EDefText = EDefText;
        }

        public bool Delete()
        {
            bool success = EmailDefinition.DeleteEmailDefinition(this.EDefID);
            if (success)
                this.EDefID = 0;
            return success;
        }

        public bool Update()
        {
            return EmailDefinition.UpdateEmailDefinition(this.EDefID, this.EDefName, this.EDefText);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all EmailDefinitions
        /// </summary>
        public static List<EmailDefinition> GetEmailDefinitions(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "EDefName";

            List<EmailDefinition> EmailDefinitions = null;
            string key = "EmailDefinitions_EmailDefinitions_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                EmailDefinitions = (List<EmailDefinition>)BizObject.Cache[key];
            }
            else
            {
                List<EmailDefinitionInfo> recordset = SiteProvider.PR2.GetEmailDefinitions(cSortExpression);
                EmailDefinitions = GetEmailDefinitionListFromEmailDefinitionInfoList(recordset);
                BasePR.CacheData(key, EmailDefinitions);
            }
            return EmailDefinitions;
        }


        /// <summary>
        /// Returns the number of total EmailDefinitions
        /// </summary>
        public static int GetEmailDefinitionCount()
        {
            int EmailDefinitionCount = 0;
            string key = "EmailDefinitions_EmailDefinitionCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                EmailDefinitionCount = (int)BizObject.Cache[key];
            }
            else
            {
                EmailDefinitionCount = SiteProvider.PR2.GetEmailDefinitionCount();
                BasePR.CacheData(key, EmailDefinitionCount);
            }
            return EmailDefinitionCount;
        }

        /// <summary>
        /// Returns a EmailDefinition object with the specified ID
        /// </summary>
        public static EmailDefinition GetEmailDefinitionByID(int EDefID)
        {
            EmailDefinition EmailDefinition = null;
            string key = "EmailDefinitions_EmailDefinition_" + EDefID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                EmailDefinition = (EmailDefinition)BizObject.Cache[key];
            }
            else
            {
                EmailDefinition = GetEmailDefinitionFromEmailDefinitionInfo(SiteProvider.PR2.GetEmailDefinitionByID(EDefID));
                BasePR.CacheData(key, EmailDefinition);
            }
            return EmailDefinition;
        }

        /// <summary>
        /// Returns a EmailDefinition object with the specified ID
        /// </summary>
        public static EmailDefinition GetEmailDefinitionByType(String EType)
        {
            EmailDefinition EmailDefinition = null;
            string key = "EmailDefinitions_EmailDefinition_" + EType.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                EmailDefinition = (EmailDefinition)BizObject.Cache[key];
            }
            else
            {
                EmailDefinition = GetEmailDefinitionFromEmailDefinitionInfo(SiteProvider.PR2.GetEmailDefinitionByType(EType));
                BasePR.CacheData(key, EmailDefinition);
            }
            return EmailDefinition;
        }


        /// <summary>
        /// Updates an existing EmailDefinition
        /// </summary>
        public static bool UpdateEmailDefinition(int EDefID, string EDefName, string EDefText)
        {
            EDefName = BizObject.ConvertNullToEmptyString(EDefName);
            EDefText = BizObject.ConvertNullToEmptyString(EDefText);


            EmailDefinitionInfo record = new EmailDefinitionInfo(EDefID, EDefName, EDefText);
            bool ret = SiteProvider.PR2.UpdateEmailDefinition(record);

            BizObject.PurgeCacheItems("EmailDefinitions_EmailDefinition_" + EDefID.ToString());
            BizObject.PurgeCacheItems("EmailDefinitions_EmailDefinitions");
            return ret;
        }

        /// <summary>
        /// Creates a new EmailDefinition
        /// </summary>
        public static int InsertEmailDefinition(string EDefName, string EDefText)
        {
            EDefName = BizObject.ConvertNullToEmptyString(EDefName);
            EDefText = BizObject.ConvertNullToEmptyString(EDefText);


            EmailDefinitionInfo record = new EmailDefinitionInfo(0, EDefName, EDefText);
            int ret = SiteProvider.PR2.InsertEmailDefinition(record);

            BizObject.PurgeCacheItems("EmailDefinitions_EmailDefinition");
            return ret;
        }

        /// <summary>
        /// Deletes an existing EmailDefinition, but first checks if OK to delete
        /// </summary>
        public static bool DeleteEmailDefinition(int EDefID)
        {
            bool IsOKToDelete = OKToDelete(EDefID);
            if (IsOKToDelete)
            {
                return (bool)DeleteEmailDefinition(EDefID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing EmailDefinition - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteEmailDefinition(int EDefID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteEmailDefinition(EDefID);
            //         new RecordDeletedEvent("EmailDefinition", EDefID, null).Raise();
            BizObject.PurgeCacheItems("EmailDefinitions_EmailDefinition");
            return ret;
        }



        /// <summary>
        /// Checks to see if a EmailDefinition can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }



        /// <summary>
        /// Returns a EmailDefinition object filled with the data taken from the input EmailDefinitionInfo
        /// </summary>
        private static EmailDefinition GetEmailDefinitionFromEmailDefinitionInfo(EmailDefinitionInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new EmailDefinition(record.EDefID, record.EDefName, record.EDefText);
            }
        }

        /// <summary>
        /// Returns a list of EmailDefinition objects filled with the data taken from the input list of EmailDefinitionInfo
        /// </summary>
        private static List<EmailDefinition> GetEmailDefinitionListFromEmailDefinitionInfoList(List<EmailDefinitionInfo> recordset)
        {
            List<EmailDefinition> EmailDefinitions = new List<EmailDefinition>();
            foreach (EmailDefinitionInfo record in recordset)
                EmailDefinitions.Add(GetEmailDefinitionFromEmailDefinitionInfo(record));
            return EmailDefinitions;
        }

        #endregion
    }
}