﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

    /// <summary>
    /// Summary description for TestDefinition
    /// </summary>
    public class TestDefinition : BasePR
    {
        #region Variables and Properties

        private int _TestDefID = 0;
        public int TestDefID
        {
            get { return _TestDefID; }
            protected set { _TestDefID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private string _XMLTest = "";
        public string XMLTest
        {
            get { return _XMLTest; }
            set { _XMLTest = value; }
        }

        private int _Version = 0;
        public int Version
        {
            get { return _Version; }
            set { _Version = value; }
        }


        public TestDefinition(int TestDefID, int TopicID, string XMLTest, int Version)
        {
            this.TestDefID = TestDefID;
            this.TopicID = TopicID;
            this.XMLTest = XMLTest;
            this.Version = Version;
        }

        public bool Delete()
        {
            bool success = TestDefinition.DeleteTestDefinition(this.TestDefID);
            if (success)
                this.TestDefID = 0;
            return success;
        }

        public bool Update()
        {
            return TestDefinition.UpdateTestDefinition(this.TestDefID, this.TopicID, this.XMLTest, this.Version);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all TestDefinitions
        /// </summary>
        public static List<TestDefinition> GetTestDefinitions(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Version";

            List<TestDefinition> TestDefinitions = null;
            string key = "TestDefinitions_TestDefinitions_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TestDefinitions = (List<TestDefinition>)BizObject.Cache[key];
            }
            else
            {
                List<TestDefinitionInfo> recordset = SiteProvider.PR2.GetTestDefinitions(cSortExpression);
                TestDefinitions = GetTestDefinitionListFromTestDefinitionInfoList(recordset);
                BasePR.CacheData(key, TestDefinitions);
            }
            return TestDefinitions;
        }


        /// <summary>
        /// Returns the number of total TestDefinitions
        /// </summary>
        public static int GetTestDefinitionCount()
        {
            int TestDefinitionCount = 0;
            string key = "TestDefinitions_TestDefinitionCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TestDefinitionCount = (int)BizObject.Cache[key];
            }
            else
            {
                TestDefinitionCount = SiteProvider.PR2.GetTestDefinitionCount();
                BasePR.CacheData(key, TestDefinitionCount);
            }
            return TestDefinitionCount;
        }

        /// <summary>
        /// Returns a TestDefinition object with the specified ID
        /// </summary>
        public static TestDefinition GetTestDefinitionByID(int TestDefID)
        {
            TestDefinition TestDefinition = null;
            string key = "TestDefinitions_TestDefinition_" + TestDefID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TestDefinition = (TestDefinition)BizObject.Cache[key];
            }
            else
            {
                TestDefinition = GetTestDefinitionFromTestDefinitionInfo(SiteProvider.PR2.GetTestDefinitionByID(TestDefID));
                BasePR.CacheData(key, TestDefinition);
            }
            return TestDefinition;
        }

        /// <summary>
        /// Returns a TestDefinition object associated with the specified TopicID
        /// </summary>
        public static TestDefinition GetTestDefinitionByTopicID(int TopicID)
        {
            TestDefinition TestDefinition = null;
            string key = "TestDefinitions_TestDefinition_TopicID_" + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TestDefinition = (TestDefinition)BizObject.Cache[key];
            }
            else
            {
                TestDefinition = GetTestDefinitionFromTestDefinitionInfo(SiteProvider.PR2.GetTestDefinitionByTopicID(TopicID));
                BasePR.CacheData(key, TestDefinition);
            }
            return TestDefinition;
        }

        /// <summary>
        /// Updates an existing TestDefinition
        /// </summary>
        public static bool UpdateTestDefinition(int TestDefID, int TopicID, string XMLTest, int Version)
        {
            XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);


            TestDefinitionInfo record = new TestDefinitionInfo(TestDefID, TopicID, XMLTest, Version);
            bool ret = SiteProvider.PR2.UpdateTestDefinition(record);

            BizObject.PurgeCacheItems("TestDefinitions_TestDefinition_" + TestDefID.ToString());
            BizObject.PurgeCacheItems("TestDefinitions_TestDefinitions");
            return ret;
        }

        /// <summary>
        /// Creates a new TestDefinition
        /// </summary>
        public static int InsertTestDefinition(int TopicID, string XMLTest, int Version)
        {
            XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);


            TestDefinitionInfo record = new TestDefinitionInfo(0, TopicID, XMLTest, Version);
            int ret = SiteProvider.PR2.InsertTestDefinition(record);

            BizObject.PurgeCacheItems("TestDefinitions_TestDefinition");
            return ret;
        }

        /// <summary>
        /// Deletes an existing TestDefinition, but first checks if OK to delete
        /// </summary>
        public static bool DeleteTestDefinition(int TestDefID)
        {
            bool IsOKToDelete = OKToDelete(TestDefID);
            if (IsOKToDelete)
            {
                return (bool)DeleteTestDefinition(TestDefID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing TestDefinition - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteTestDefinition(int TestDefID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteTestDefinition(TestDefID);
            //         new RecordDeletedEvent("TestDefinition", TestDefID, null).Raise();
            BizObject.PurgeCacheItems("TestDefinitions_TestDefinition");
            return ret;
        }



        /// <summary>
        /// Checks to see if a TestDefinition can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int TestDefID)
        {
            return true;
        }



        /// <summary>
        /// Returns a TestDefinition object filled with the data taken from the input TestDefinitionInfo
        /// </summary>
        private static TestDefinition GetTestDefinitionFromTestDefinitionInfo(TestDefinitionInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new TestDefinition(record.TestDefID, record.TopicID, record.XMLTest, record.Version);
            }
        }

        /// <summary>
        /// Returns a list of TestDefinition objects filled with the data taken from the input list of TestDefinitionInfo
        /// </summary>
        private static List<TestDefinition> GetTestDefinitionListFromTestDefinitionInfoList(List<TestDefinitionInfo> recordset)
        {
            List<TestDefinition> TestDefinitions = new List<TestDefinition>();
            foreach (TestDefinitionInfo record in recordset)
                TestDefinitions.Add(GetTestDefinitionFromTestDefinitionInfo(record));
            return TestDefinitions;
        }

        #endregion
    }
}