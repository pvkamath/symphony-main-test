﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 
    /// <summary>
    /// Summary description for ViDefinition
    /// </summary>
    public class ViDefinition: BasePR
    {
        #region Variables and Properties
        private int _ViDefID = 0;
        public int ViDefID
        {
            get { return _ViDefID; }
            protected set { _ViDefID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private string _XMLTest = "";
        public string XMLTest
        {
            get { return _XMLTest; }
            set { _XMLTest = value; }
        }

        private string _Vignette_Desc = "";
        public string Vignette_Desc
        {
            get { return _Vignette_Desc; }
            set { _Vignette_Desc = value; }
        }

        private int _Version = 0;
        public int Version
        {
            get { return _Version; }
            set { _Version = value; }
        }


        public ViDefinition(int ViDefID, int TopicID, string XMLTest, string Vignette_Desc, int Version)
        {
            this.ViDefID = ViDefID;
            this.TopicID = TopicID;
            this.XMLTest = XMLTest;
            this.Vignette_Desc = Vignette_Desc;
            this.Version = Version;
        }

        public bool Delete()
        {
            bool success = ViDefinition.DeleteViDefinition(this.ViDefID);
            if (success)
                this.ViDefID = 0;
            return success;
        }

        public bool Update()
        {
            return ViDefinition.UpdateViDefinition(this.ViDefID, this.TopicID, this.XMLTest, this.Vignette_Desc, this.Version);
        }

        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all ViDefinitions
        /// </summary>
        public static List<ViDefinition> GetViDefinitions(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Version";

            List<ViDefinition> ViDefinitions = null;
            string key = "ViDefinitions_ViDefinitions_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ViDefinitions = (List<ViDefinition>)BizObject.Cache[key];
            }
            else
            {
                List<ViDefinitionInfo> recordset = SiteProvider.PR2.GetViDefinitions(cSortExpression);
                ViDefinitions = GetViDefinitionListFromViDefinitionInfoList(recordset);
                BasePR.CacheData(key, ViDefinitions);
            }
            return ViDefinitions;
        }


        /// <summary>
        /// Returns the number of total ViDefinitions
        /// </summary>
        public static int GetViDefinitionCount()
        {
            int ViDefinitionCount = 0;
            string key = "ViDefinitions_ViDefinitionCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ViDefinitionCount = (int)BizObject.Cache[key];
            }
            else
            {
                ViDefinitionCount = SiteProvider.PR2.GetViDefinitionCount();
                BasePR.CacheData(key, ViDefinitionCount);
            }
            return ViDefinitionCount;
        }

        /// <summary>
        /// Returns a ViDefinition object with the specified ID
        /// </summary>
        public static ViDefinition GetViDefinitionByID(int ViDefID)
        {
            ViDefinition ViDefinition = null;
            string key = "ViDefinitions_ViDefinition_" + ViDefID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ViDefinition = (ViDefinition)BizObject.Cache[key];
            }
            else
            {
                ViDefinition = GetViDefinitionFromViDefinitionInfo(SiteProvider.PR2.GetViDefinitionByID(ViDefID));
                BasePR.CacheData(key, ViDefinition);
            }
            return ViDefinition;
        }

        /// <summary>
        /// Returns a ViDefinition object associated with the specified TopicID
        /// </summary>
        public static ViDefinition GetViDefinitionByTopicID(int TopicID)
        {
            ViDefinition ViDefinition = null;
            string key = "ViDefinitions_ViDefinition_TopicID_" + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ViDefinition = (ViDefinition)BizObject.Cache[key];
            }
            else
            {
                ViDefinition = GetViDefinitionFromViDefinitionInfo(SiteProvider.PR2.GetViDefinitionByTopicID(TopicID));
                BasePR.CacheData(key, ViDefinition);
            }
            return ViDefinition;
        }

        /// <summary>
        /// Updates an existing ViDefinition
        /// </summary>
        public static bool UpdateViDefinition(int ViDefID, int TopicID, string XMLTest, string Vignette_Desc, int Version)
        {
            XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);


            ViDefinitionInfo record = new ViDefinitionInfo(ViDefID, TopicID, XMLTest, Vignette_Desc, Version);
            bool ret = SiteProvider.PR2.UpdateViDefinition(record);

            BizObject.PurgeCacheItems("ViDefinitions_ViDefinition_" + ViDefID.ToString());
            BizObject.PurgeCacheItems("ViDefinitions_ViDefinitions");
            return ret;
        }

        /// <summary>
        /// Creates a new ViDefinition
        /// </summary>
        public static int InsertViDefinition(int TopicID, string XMLTest, string Vignette_Desc, int Version)
        {
            XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);


            ViDefinitionInfo record = new ViDefinitionInfo(0, TopicID, XMLTest, Vignette_Desc, Version);
            int ret = SiteProvider.PR2.InsertViDefinition(record);

            BizObject.PurgeCacheItems("ViDefinitions_ViDefinition");
            return ret;
        }

        /// <summary>
        /// Deletes an existing ViDefinition, but first checks if OK to delete
        /// </summary>
        public static bool DeleteViDefinition(int ViDefID)
        {
            bool IsOKToDelete = OKToDelete(ViDefID);
            if (IsOKToDelete)
            {
                return (bool)DeleteViDefinition(ViDefID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing ViDefinition - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteViDefinition(int ViDefID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteViDefinition(ViDefID);
            //         new RecordDeletedEvent("ViDefinition", ViDefID, null).Raise();
            BizObject.PurgeCacheItems("ViDefinitions_ViDefinition");
            return ret;
        }



        /// <summary>
        /// Checks to see if a ViDefinition can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int ViDefID)
        {
            return true;
        }



        /// <summary>
        /// Returns a ViDefinition object filled with the data taken from the input ViDefinitionInfo
        /// </summary>
        private static ViDefinition GetViDefinitionFromViDefinitionInfo(ViDefinitionInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new ViDefinition(record.ViDefID, record.TopicID, record.XMLTest, record.Vignette_Desc, record.Version);
            }
        }

        /// <summary>
        /// Returns a list of ViDefinition objects filled with the data taken from the input list of ViDefinitionInfo
        /// </summary>
        private static List<ViDefinition> GetViDefinitionListFromViDefinitionInfoList(List<ViDefinitionInfo> recordset)
        {
            List<ViDefinition> ViDefinitions = new List<ViDefinition>();
            foreach (ViDefinitionInfo record in recordset)
                ViDefinitions.Add(GetViDefinitionFromViDefinitionInfo(record));
            return ViDefinitions;
        }

        #endregion
    }
}
