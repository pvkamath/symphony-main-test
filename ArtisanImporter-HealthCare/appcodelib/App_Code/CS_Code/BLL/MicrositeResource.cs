﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for MicrositeResource
    /// </summary>
    public class MicrositeResource : BasePR
    {
        #region Variables and Properties

        private int _id = 0;
        public int ID
        {
            get { return _id; }
            protected set { _id = value; }
        }
        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }
        private string _resource_name = "";
        public string Resource_name
        {
            get { return _resource_name; }
            private set { _resource_name = value; }
        }
        private string _file_name = "";
        public string File_name
        {
            get { return _file_name; }
            private set { _file_name = value; }
        }
        private int _resource_order = 0;
        public int Resource_order
        {
            get { return _resource_order; }
            private set { _resource_order = value; }
        }
        private string _resource_title = "";
        public string Resource_title
        {
            get { return _resource_title; }
            private set { _resource_title = value; }
        }
        private string _resource_type = "";
        public string Resource_type
        {
            get { return _resource_type; }
            private set { _resource_type = value; }
        }
        private string _resource_html = "";
        public string Resource_html
        {
            get { return _resource_html; }
            private set { _resource_html = value; }
        }
        public MicrositeResource()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public MicrositeResource(int id, int msid, string resource_name, string file_name, int resource_order, string resource_title, string resource_type, string resource_html)
        {
            this.ID = id;
            this.Msid = msid;
            this.Resource_name = resource_name;
            this.File_name = file_name;
            this.Resource_order = resource_order;
            this.Resource_title = resource_title;
            this.Resource_type = resource_type;
            this.Resource_html = resource_html;
        }
        public bool Delete()
        {
            bool success = MicrositeResource.DeleteMicrositeResource(this.ID);
            if (success)
                this.Msid = 0;
            return success;
        }

        public bool Update()
        {
            return MicrositeResource.UpdateMicrositeResource(this.ID, this.Msid, this.Resource_name, this.File_name, this.Resource_order, this.Resource_title, this.Resource_type, this.Resource_html);
        }

        #endregion
        #region Methods
        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all MicrositeResource
        //</summary>
        public static List<MicrositeResource> GetMicrositeResource(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "file_name";

            List<MicrositeResource> MicrositeResources = null;
            string key = "MicrositeResources_MicrositeResources_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeResources = (List<MicrositeResource>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeResourceInfo> recordset = SiteProvider.PR2.GetMicrositeResource(cSortExpression);
                MicrositeResources = GetMicrositeResourceListFromMicrositeResourceInfoList(recordset);
                BasePR.CacheData(key, MicrositeResources);
            }
            return MicrositeResources;
        }
        public static List<MicrositeResource> GetMicrositeResourceByMsid(int Msid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Resource_name";

            List<MicrositeResource> MicrositeResources = null;
            string key = "MicrositeResources_MicrositeResources_" + Msid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeResources = (List<MicrositeResource>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeResourceInfo> recordset = SiteProvider.PR2.GetMicrositeResourceByMsid(Msid, cSortExpression);
                MicrositeResources = GetMicrositeResourceListFromMicrositeResourceInfoList(recordset);
                BasePR.CacheData(key, MicrositeResources);
            }
            return MicrositeResources;
        }
        /// <summary>
        /// Returns a MicrositeResource object with the specified ID
        /// </summary>
        public static MicrositeResource GetMicrositeResourceByID(int id)
        {
            MicrositeResource MicrositeResources = null;
            string key = "MicrositeResources_MicrositeResources_" + id.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeResources = (MicrositeResource)BizObject.Cache[key];
            }
            else
            {
                MicrositeResources = GetMicrositeResourceFromMicrositeResourceInfo(SiteProvider.PR2.GetMicrositeResourceByID(id));
                BasePR.CacheData(key, MicrositeResources);
            }
            return MicrositeResources;
        }
        public static MicrositeResource GetMicrositeResourceByID(int Msid, string resource_name)
        {
            MicrositeResource MicrositeResources = null;
            string key = "MicrositeResources_MicrositeResources_" + Msid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeResources = (MicrositeResource)BizObject.Cache[key];
            }
            else
            {
                MicrositeResources = GetMicrositeResourceFromMicrositeResourceInfo(SiteProvider.PR2.GetMicrositeResourceByID(Msid, resource_name));
                BasePR.CacheData(key, MicrositeResources);
            }
            return MicrositeResources;
        }
        public static int InsertMicrositeResources(int msid, string resource_name, string file_name, int resource_order, string resource_title, string resource_type, string resource_html)
        {
            resource_name = BizObject.ConvertNullToEmptyString(resource_name);
            file_name = BizObject.ConvertNullToEmptyString(file_name);

            MicrositeResourceInfo record = new MicrositeResourceInfo(0, msid, resource_name, file_name, resource_order, resource_title, resource_type, resource_html);
            int ret = SiteProvider.PR2.InsertMicrositeResource(record);

            BizObject.PurgeCacheItems("MicrositeResources_MicrositeResources");
            return ret;
        }
        /// <summary>
        /// Updates an existing MicrositeResource
        /// </summary>
        public static bool UpdateMicrositeResource(int ID, int Msid, string resource_name, string file_name, int resource_order, string resource_title, string resource_type, string resource_html)
        {
            resource_name = BizObject.ConvertNullToEmptyString(resource_name);
            file_name = BizObject.ConvertNullToEmptyString(file_name);

            MicrositeResourceInfo record = new MicrositeResourceInfo(ID, Msid, resource_name, file_name, resource_order, resource_title, resource_type, resource_html);
            bool ret = SiteProvider.PR2.UpdateMicrositeResource(record);

            BizObject.PurgeCacheItems("MicrositeResources_MicrositeResources_" + Msid.ToString());
            BizObject.PurgeCacheItems("MicrositeResources_MicrositeResources");
            return ret;
        }
        public static int GetMicrositeResourceMaxOrder(int msid)
        {
            return SiteProvider.PR2.GetMicrositeResourceMaxOrder(msid);
        }
        /// <summary>
        /// Deletes an existing MicrositeResource, but first checks if OK to delete
        /// </summary>
        public static bool DeleteMicrositeResource(int id)
        {
            bool IsOKToDelete = OKToDelete(id);
            if (IsOKToDelete)
            {
                return (bool)DeleteMicrositeResource(id, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing MicrositeResource - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteMicrositeResource(int id, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteMicrositeResource(id);
            BizObject.PurgeCacheItems("MicrositeResources_MicrositeResources");
            return ret;
        }

        /// <summary>
        /// Checks to see if a MicrositeResource can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }


        /// <summary>
        /// Returns a MicrositeResource object filled with the data taken from the input MicrositeResourceInfo
        /// </summary>
        private static MicrositeResource GetMicrositeResourceFromMicrositeResourceInfo(MicrositeResourceInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new MicrositeResource(record.ID, record.Msid, record.Resource_name, record.File_name, record.Resource_order, record.Resource_title, record.Resource_type, record.Resource_html);
            }
        }

        /// <summary>
        /// Returns a list of MicrositeResource objects filled with the data taken from the input list of MicrositeResourceInfo
        /// </summary>
        private static List<MicrositeResource> GetMicrositeResourceListFromMicrositeResourceInfoList(List<MicrositeResourceInfo> recordset)
        {
            List<MicrositeResource> MicrositeResources = new List<MicrositeResource>();
            foreach (MicrositeResourceInfo record in recordset)
                MicrositeResources.Add(GetMicrositeResourceFromMicrositeResourceInfo(record));
            return MicrositeResources;
        }
        #endregion
    }
}