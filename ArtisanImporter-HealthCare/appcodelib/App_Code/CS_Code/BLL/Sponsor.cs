﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

    /// <summary>
    /// Summary description for Sponsor
    /// </summary>
public class Sponsor: BasePR
    {
        #region Variables and Properties

        private int _SponsorID = 0;
        public int SponsorID
        {
            get { return _SponsorID; }
            protected set { _SponsorID = value; }
        }

        private string _SponsorName = "";
        public string SponsorName
        {
            get { return _SponsorName; }
            private set { _SponsorName = value; }
        }

        private string _UserName = "";
        public string UserName
        {
            get { return _UserName; }
            private set { _UserName = value; }
        }

        private string _UserPassword = "";
        public string UserPassword
        {
            get { return _UserPassword; }
            private set { _UserPassword = value; }
        }

        private bool _Survey_Ind = true;
        public bool Survey_Ind
        {
            get { return _Survey_Ind; }
            private set { _Survey_Ind = value; }
        }


        public Sponsor(int SponsorID, string SponsorName, string UserName, string UserPassword, bool Survey_Ind)
        {
            this.SponsorID = SponsorID;
            this.SponsorName = SponsorName;
            this.UserName = UserName;
            this.UserPassword = UserPassword;
            this.Survey_Ind = Survey_Ind;
        }

        public bool Delete()
        {
            bool success = Sponsor.DeleteSponsor(this.SponsorID);
            if (success)
                this.SponsorID = 0;
            return success;
        }

        public bool Update()
        {
            return Sponsor.UpdateSponsor(this.SponsorID, this.SponsorName,
                this.UserName, this.UserPassword, this.Survey_Ind);
        }

        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Sponsors
        /// </summary>
        public static List<Sponsor> GetSponsors(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "SponsorName";

            List<Sponsor> Sponsors = null;
            string key = "Sponsors_Sponsors_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Sponsors = (List<Sponsor>)BizObject.Cache[key];
            }
            else
            {
                List<SponsorInfo> recordset = SiteProvider.PR2.GetSponsors(cSortExpression);
                Sponsors = GetSponsorListFromSponsorInfoList(recordset);
                BasePR.CacheData(key, Sponsors);
            }
            return Sponsors;
        }


        /// <summary>
        /// Returns a Sponsor object with the specified ID
        /// </summary>
        public static Sponsor GetSponsorByID(int SponsorID)
        {
            Sponsor Sponsor = null;
            string key = "Sponsors_Sponsor_" + SponsorID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Sponsor = (Sponsor)BizObject.Cache[key];
            }
            else
            {
                Sponsor = GetSponsorFromSponsorInfo(SiteProvider.PR2.GetSponsorByID(SponsorID));
                BasePR.CacheData(key, Sponsor);
            }
            return Sponsor;
        }
        public static List<Sponsor> GetUnSelectedSponsorByInitID(int InitID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "sponsorname";

            List<Sponsor> Sponsor = null;
            string key = "Sponsor_Sponsor_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Sponsor = (List<Sponsor>)BizObject.Cache[key];
            }
            else
            {
                List<SponsorInfo> recordset = SiteProvider.PR2.GetUnSelectedSponsorByInitID(InitID, cSortExpression);
                Sponsor = GetSponsorListFromSponsorInfoList(recordset);
                BasePR.CacheData(key, Sponsor);
            }
            return Sponsor;
        }
        public static List<Sponsor> GetSponsorByInitiatvieID(string cSortExpression, int InitID)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "sponsorname";

            List<Sponsor> Sponsors = null;
            string key = "Sponsors_Sponsors_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Sponsors = (List<Sponsor>)BizObject.Cache[key];
            }
            else
            {
                List<SponsorInfo> recordset = SiteProvider.PR2.GetSponsorByInitiatvieID(InitID, cSortExpression);
                Sponsors = GetSponsorListFromSponsorInfoList(recordset);
                BasePR.CacheData(key, Sponsors);
            }
            return Sponsors;
        }
        /// <summary>
        /// Updates an existing Sponsor
        /// </summary>
        public static bool UpdateSponsor(int SponsorID, string SponsorName, string UserName,
            string UserPassword, bool Survey_Ind)
        {
            SponsorName = BizObject.ConvertNullToEmptyString(SponsorName);
            UserName = BizObject.ConvertNullToEmptyString(UserName);
            UserPassword = BizObject.ConvertNullToEmptyString(UserPassword);


            SponsorInfo record = new SponsorInfo(SponsorID, SponsorName, UserName,
                UserPassword, Survey_Ind);
            bool ret = SiteProvider.PR2.UpdateSponsor(record);

            BizObject.PurgeCacheItems("Sponsors_Sponsor_" + SponsorID.ToString());
            BizObject.PurgeCacheItems("Sponsors_Sponsors");
            return ret;
        }

        /// <summary>
        /// Creates a new Sponsor
        /// </summary>
        public static int InsertSponsor(string SponsorName, string UserName, string UserPassword,
            bool Survey_Ind)
        {
            SponsorName = BizObject.ConvertNullToEmptyString(SponsorName);
            UserName = BizObject.ConvertNullToEmptyString(UserName);
            UserPassword = BizObject.ConvertNullToEmptyString(UserPassword);

            SponsorInfo record = new SponsorInfo(0, SponsorName, UserName, UserPassword, Survey_Ind);
            int ret = SiteProvider.PR2.InsertSponsor(record);

            BizObject.PurgeCacheItems("Sponsors_Sponsor");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Sponsor, but first checks if OK to delete
        /// </summary>
        public static bool DeleteSponsor(int SponsorID)
        {
            bool IsOKToDelete = OKToDelete(SponsorID);
            if (IsOKToDelete)
            {
                return (bool)DeleteSponsor(SponsorID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Sponsor - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteSponsor(int SponsorID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteSponsor(SponsorID);
            //         new RecordDeletedEvent("Sponsor", SponsorID, null).Raise();
            BizObject.PurgeCacheItems("Sponsors_Sponsor");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Sponsor can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int SponsorID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Sponsor object filled with the data taken from the input SponsorInfo
        /// </summary>
        private static Sponsor GetSponsorFromSponsorInfo(SponsorInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Sponsor(record.SponsorID, record.SponsorName, record.UserName,
                    record.UserPassword, record.Survey_Ind);
            }
        }

        /// <summary>
        /// Returns a list of Sponsor objects filled with the data taken from the input list of SponsorInfo
        /// </summary>
        private static List<Sponsor> GetSponsorListFromSponsorInfoList(List<SponsorInfo> recordset)
        {
            List<Sponsor> Sponsors = new List<Sponsor>();
            foreach (SponsorInfo record in recordset)
                Sponsors.Add(GetSponsorFromSponsorInfo(record));
            return Sponsors;
        }

        #endregion
    }
}
