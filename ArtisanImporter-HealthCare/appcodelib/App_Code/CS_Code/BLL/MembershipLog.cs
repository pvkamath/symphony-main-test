﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using System.Collections.Generic;

namespace PearlsReview.BLL
{

    public class MembershipLog : BasePR
    {

        #region variables and Properties
        private int _mlid = 0;
        public int mlid
        {
            get { return _mlid; }
            protected set { _mlid = value; }
        }

        private int _userid = 0;
        public int userid
        {
            get { return _userid; }
            protected set { _userid = value; }
        }

        private DateTime _logdate;
        public DateTime logdate
        {
            get { return _logdate; }
            protected set { _logdate = value; }
        }

        private DateTime _expdate;
        public DateTime expdate
        {
            get { return _expdate; }
            protected set { _expdate = value; }
        }

        private string _verification;
        public string verification
        {
            get { return _verification; }
            protected set { _verification = value; }
        }


        private string _profileid;
        public string profileid
        {
            get { return _profileid; }
            protected set { _profileid = value; }
        }

        private bool _renew_ind = false;
        public bool renew_ind
        {
            get { return _renew_ind; }
            private set { _renew_ind = value; }
        }

        private bool _active_ind = false;
        public bool active_ind
        {
            get { return _active_ind; }
            private set { _active_ind = value; }
        }

        private decimal _amount = 0;
        public decimal amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private string _comment;
        public string comment
        {
            get { return _comment; }
            protected set { _comment = value; }
        }

        private string _ccnum;
        public string ccnum
        {
            get { return _ccnum; }
            protected set { _ccnum = value; }
        }

        private bool _firstrec;
        public bool FirstRec
        {
            get { return _firstrec; }
            set { _firstrec = value; }
        }


        public MembershipLog(int mlid, int userid, DateTime logdate, DateTime expdate, string verification, string profileid,
                             bool renew_ind, bool active_ind, decimal amount, string comment, string ccnum)
        {
            this.mlid = mlid;
            this.userid = userid;
            this.logdate = logdate;
            this.expdate = expdate;
            this.verification = verification;
            this.profileid = profileid;
            this.renew_ind = renew_ind;
            this.active_ind = active_ind;
            this.amount = amount;
            this.comment = comment;
            this.ccnum = ccnum;

        }

        public MembershipLog()
        {
        }

        public bool Update()
        {
            return MembershipLog.UpdateMembershipLog(this.mlid, this.userid, this.logdate, this.expdate, this.verification, this.profileid,
            this.renew_ind, this.active_ind, this.amount, this.comment, this.ccnum);
        }
        #endregion

        #region Methods


        public static bool UpdateMembershipLog(int mlid, int userid, DateTime logdate, DateTime expdate, string verification, string profileid,
                         bool renew_ind, bool active_ind, decimal amount, string comment, string ccnum)
        {
            MembershipLogInfo record = new MembershipLogInfo(mlid, userid, logdate, expdate, verification, profileid, renew_ind, active_ind, amount, comment, ccnum);
         
            SiteProvider.PR2.UpdateMembershipLog(record);
            return false;
        }

        public static bool InsertMembershipLog(int userid, DateTime logdate, DateTime expdate, string verification, string profileid,
                           bool renew_ind, bool active_ind, decimal amount, string comment, string ccnum)
        {
            MembershipLogInfo record = new MembershipLogInfo(userid, logdate, expdate, verification, profileid, renew_ind, active_ind, amount, comment, ccnum);
           
         SiteProvider.PR2.InsertMembershipLog(record);
            return false;
        }
        public static List<MembershipLogInfo> GetMembershipLogListByUserID(int userid)
        {
            List<MembershipLogInfo> memlogList = new List<MembershipLogInfo>();

         
            memlogList = SiteProvider.PR2.GetMembershipLogListByUserID(userid);// GetMembershipLogFromMembershipLogInfo(recordset);
            //BasePR.CacheData(key, memlog);

            return memlogList;
        }

        public static List<MembershipLog> GetMembershipLogListfromMembershipLogInfo(List<MembershipLogInfo> recordset)
        {
            List<MembershipLog> memlogs = new List<MembershipLog>();
            foreach (MembershipLogInfo memloginfo in recordset)
            {
                MembershipLog memlog = GetMembershipLogFromMembershipLogInfo(memloginfo);
                memlogs.Add(memlog);
            }
            return memlogs;
        }
        private static MembershipLog GetMembershipLogFromMembershipLogInfo(MembershipLogInfo memlog)
        {
            MembershipLog mem = new MembershipLog();
            mem.mlid = memlog.mlid;
            mem.userid = memlog.userid;
            mem.logdate = memlog.logdate;
            mem.expdate = memlog.expdate;
            mem.verification = memlog.verification;
            mem.profileid = memlog.profileid;
            mem.renew_ind = memlog.renew_ind;
            mem.active_ind = memlog.active_ind;
            mem.amount = memlog.amount;
            mem.comment = memlog.comment;
            mem.ccnum = memlog.ccnum;

            return mem;

        }

        public static MembershipLog GetMembershipLogByUserID(int userid)
        {
            MembershipLog memlog = null;
            string key = "memlog_MembershipLog" + userid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                memlog = (MembershipLog)BizObject.Cache[key];
            }
            else
            {
                
                MembershipLogInfo recordset = SiteProvider.PR2.GetMembershipLogByUserID(userid);
                memlog = GetMembershipLogFromMembershipLogInfo(recordset);
                BasePR.CacheData(key, memlog);
            }
            return memlog;
        }

        public static bool CancelMembershipLog(int userid)
        {
           
            return SiteProvider.PR2.CancelMembershipLog(userid);
        }



        #endregion
    }
}
