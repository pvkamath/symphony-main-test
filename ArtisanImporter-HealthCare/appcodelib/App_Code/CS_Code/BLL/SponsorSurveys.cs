﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for SponsorSurveys
/// </summary>
    public class SponsorSurveys : BasePR
    {
        #region Variables and Properties 

        private int _discountid = 0;
        public int discountid
        {
            get { return _discountid; }
            protected set { _discountid = value; }
        }

        private int _questionid = 0;
        public int questionid
        {
            get { return _questionid; }
            protected set { _questionid = value; }
        }

        public SponsorSurveys(int discountid, int questionid)
        {
            this.discountid = discountid;
            this.questionid = questionid;
        }


        public bool Delete()
        {
            bool success = SponsorSurveys.DeleteSponsorSurveysByDiscountidAndQuestionid(this.discountid, this.questionid);
            if (success)
                this.questionid = 0;
            return success;
        }        
        #endregion

        #region Methods
        
        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all SponsorSurveys
        //</summary>
        public static List<SponsorSurveys> GetSponsorSurveys(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "questionid";

            List<SponsorSurveys> SponsorSurveys = null;
            string key = "SponsorSurveys_SponsorSurveys_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SponsorSurveys = (List<SponsorSurveys>)BizObject.Cache[key];
            }
            else
            {
                List<SponsorSurveysInfo> recordset = SiteProvider.PR2.GetSponsorSurveys(cSortExpression);
                SponsorSurveys = GetSponsorSurveysListFromSponsorSurveysInfoList(recordset);
                BasePR.CacheData(key, SponsorSurveys);
            }
            return SponsorSurveys;
        }

        //<summary>
        //Returns a collection with all SponsorSurveys
        //</summary>
        public static List<SponsorSurveys> GetSponsorSurveysByDiscountIdAndQuestionId(int discountid, int questionid)
        {

            List<SponsorSurveys> SponsorSurveys = null;
            string key = "SponsorSurveys_SponsorSurveys_" + questionid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SponsorSurveys = (List<SponsorSurveys>)BizObject.Cache[key];
            }
            else
            {
                List<SponsorSurveysInfo> recordset = SiteProvider.PR2.GetSponsorSurveysByDiscountIdAndQuestionId(discountid, questionid);
                SponsorSurveys = GetSponsorSurveysListFromSponsorSurveysInfoList(recordset);
                BasePR.CacheData(key, SponsorSurveys);
            }
            return SponsorSurveys;
        }

        /// <summary>
        /// Creates a new SponsorSurveys
        /// </summary>
        public static bool InsertSponsorSurveys(int discountid, int questionid)
        {
            //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
            //Comment = BizObject.ConvertNullToEmptyString(Comment);

            SponsorSurveysInfo record = new SponsorSurveysInfo(discountid, questionid);
            bool ret = SiteProvider.PR2.InsertSponsorSurveys(record);

            BizObject.PurgeCacheItems("SponsorSurveys_SponsorSurveys");
            return ret;
        }


        /// <summary>
        /// Deletes an existing SponsorSurveys, but first checks if OK to delete
        /// </summary>
        public static bool DeleteSponsorSurveysByDiscountidAndQuestionid(int discountid, int questionid)
        {
            bool IsOKToDelete = OKToDelete(questionid);
            if (IsOKToDelete)
            {
                return (bool)DeleteSponsorSurveysByDiscountidAndQuestionid(discountid, questionid, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing SponsorSurveys - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteSponsorSurveysByDiscountidAndQuestionid(int discountid, int questionid, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteSponsorSurveysByDiscountidAndQuestionid(discountid, questionid);
            BizObject.PurgeCacheItems("SponsorSurveys_SponsorSurveys");
            return ret;
        }

        /// <summary>
        /// Checks to see if a SponsorSurveys can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }



        /// <summary>
        /// Returns a SponsorSurveys object filled with the data taken from the input SponsorSurveysInfo
        /// </summary>
        private static SponsorSurveys GetSponsorSurveysFromSponsorSurveysInfo(SponsorSurveysInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new SponsorSurveys(record.discountid, record.questionid);
            }
        }

        /// <summary>
        /// Returns a list of SponsorSurveys objects filled with the data taken from the input list of SponsorSurveysInfo
        /// </summary>
        private static List<SponsorSurveys> GetSponsorSurveysListFromSponsorSurveysInfoList(List<SponsorSurveysInfo> recordset)
        {
            List<SponsorSurveys> SponsorSurveys = new List<SponsorSurveys>();
            foreach (SponsorSurveysInfo record in recordset)
                SponsorSurveys.Add(GetSponsorSurveysFromSponsorSurveysInfo(record));
            return SponsorSurveys;
        }

        //<summary>
        //Returns a collection with all SponsorSurveys
        //</summary>
        public static List<SponsorSurveys> GetSponsorSurveysByDiscountId(int discountid)
        {

            List<SponsorSurveys> SponsorSurveys = null;
            string key = "SponsorSurveys_SponsorSurveys_" + discountid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SponsorSurveys = (List<SponsorSurveys>)BizObject.Cache[key];
            }
            else
            {
                List<SponsorSurveysInfo> recordset = SiteProvider.PR2.GetSponsorSurveysByDiscountId(discountid);
                SponsorSurveys = GetSponsorSurveysListFromSponsorSurveysInfoList(recordset);
                BasePR.CacheData(key, SponsorSurveys);
            }
            return SponsorSurveys;
        } 

       

       
        #endregion
    }
}
