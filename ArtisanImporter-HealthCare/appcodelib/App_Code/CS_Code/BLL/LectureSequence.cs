﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using System.Collections.Generic;

namespace PearlsReview.BLL
{

/// <summary>
/// Summary description for LectureSequence
/// </summary>
    public class LectureSequence : BasePR
    {
        #region Variables and Properties
       

        private int _LectSeqID = 0;
        public int LectSeqID
        {
            get { return _LectSeqID; }
            set { _LectSeqID = value; }
        }

        private int _Sequence = 0;
        public int Sequence
        {
            get { return _Sequence; }
            set { _Sequence = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _ResourceID1 = 0;
        public int ResourceID1
        {
            get { return _ResourceID1; }
            set { _ResourceID1 = value; }
        }

        private int _ResourceID2 = 0;
        public int ResourceID2
        {
            get { return _ResourceID2; }
            set { _ResourceID2 = value; }
        }

        private int _ResourceID3 = 0;
        public int ResourceID3
        {
            get { return _ResourceID3; }
            set { _ResourceID3 = value; }
        }

        public LectureSequence(int LectSeqID, int Sequence, int TopicID, int ResourceID1, int ResourceID2, int ResourceID3)
        {
            this.LectSeqID = LectSeqID;
            this.Sequence = Sequence;
            this.TopicID = TopicID;
            this.ResourceID1 = ResourceID1;
            this.ResourceID2 = ResourceID2;
            this.ResourceID3 = ResourceID3;

        }

#endregion

        #region Methods

        /// <summary>
        /// Inserts a new LectureSequence
        /// </summary>
        public static int InsertLectureSequence(int LectSeqID, int Sequence, int TopicID, int ResourceID1, int ResourceID2, int ResourceID3)
       {

        LectureSequenceInfo record = new LectureSequenceInfo(0, Sequence, TopicID, ResourceID1, ResourceID2, ResourceID3);
        int ret = SiteProvider.PR2.InsertLectureSequence(record);

            BizObject.PurgeCacheItems("LectureSequences_LectureSequence");
            return ret;
        }

        public static List<LectureSequence> GetLectureSequenceDetailsByTopicID(int topicID)
        {
            //if (cSortExpression == null)
            //    cSortExpression = "";

            //// provide default sort
            //if (cSortExpression.Length == 0)
            //    cSortExpression = "topicID";

            List<LectureSequence> LectureSequences = null;
            string key = "LectureSequencess_LectureSequencess_" + topicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureSequences = (List<LectureSequence>)BizObject.Cache[key];
            }
            else
            {
                List<LectureSequenceInfo> recordset = SiteProvider.PR2.GetLectureSequenceDetailsByTopicID(topicID);
                LectureSequences = GetLectureSequenceListFromLectureSequenceInfoList(recordset);
                BasePR.CacheData(key, LectureSequences);
            }
            return LectureSequences;
        
        }


        public static List<LectureSequence> GetLectureSequenceDetailsBySequence(int sequence)
        {
            //if (cSortExpression == null)
            //    cSortExpression = "";

            //// provide default sort
            //if (cSortExpression.Length == 0)
            //    cSortExpression = "topicID";

            List<LectureSequence> LectureSequences = null;
            string key = "LectureSequencess_LectureSequencess_" + sequence.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureSequences = (List<LectureSequence>)BizObject.Cache[key];
            }
            else
            {
                List<LectureSequenceInfo> recordset = SiteProvider.PR2.GetLectureSequenceDetailsBySequence(sequence);
                LectureSequences = GetLectureSequenceListFromLectureSequenceInfoList(recordset);
                BasePR.CacheData(key, LectureSequences);
            }
            return LectureSequences;

        }

        /// <summary>
        /// Updates an existing Discount
        /// </summary>
        public static bool UpdateLectureSequence(int LectSeqID, int Sequence, int TopicID, int ResourceID1, int ResourceID2, int ResourceID3)
        {

            LectureSequenceInfo record = new LectureSequenceInfo(LectSeqID, Sequence, TopicID, ResourceID1, ResourceID2, ResourceID3);
            bool ret = SiteProvider.PR2.UpdateLectureSequence(record);

            BizObject.PurgeCacheItems("LectureSequences_LectureSequence" + LectSeqID.ToString());
            BizObject.PurgeCacheItems("LectureSequences_LectureSequence");
            return ret;
        }


        /// <summary>
        /// Returns a LectureSequence object filled with the data taken from the input LectureSequenceInfo
        /// </summary>
        private static LectureSequence GetLectureSequenceFromLectureSequenceInfo(LectureSequenceInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new LectureSequence(record.LectSeqID, record.Sequence, record.TopicID, record.ResourceID1, record.ResourceID2, record.ResourceID3);
            }
        }

        /// <summary>
        /// Returns a list of LectureSequence objects filled with the data taken from the input list of LectureSequenceInfo
        /// </summary>
        private static List<LectureSequence> GetLectureSequenceListFromLectureSequenceInfoList(List<LectureSequenceInfo> recordset)
        {
            List<LectureSequence> LectureSequences = new List<LectureSequence>();
            foreach (LectureSequenceInfo record in recordset)
                LectureSequences.Add(GetLectureSequenceFromLectureSequenceInfo(record));
            return LectureSequences;
        }



        #endregion

    }
}
