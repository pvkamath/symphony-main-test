﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for MicrositeSpeciality
    /// </summary>
    public class MicrositeSpeciality : BasePR
    {
        #region Variables and Properties

        private int _dsid = 0;
        public int Dsid
        {
            get { return _dsid; }
            set { _dsid = value; }
        }
        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            set { _msid = value; }
        }
        private int _disciplineid = 0;
        public int Disciplineid
        {
            get { return _disciplineid; }
            set { _disciplineid = value; }
        }
        private Boolean _primary_ind = false;
        public Boolean Primary_ind
        {
            get { return _primary_ind; }
            set { _primary_ind = value; }
        }
        public MicrositeSpeciality()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public MicrositeSpeciality(int dsid, int msid, int disciplineid, bool primary_ind)
        {
            this.Dsid = dsid;
            this.Msid = msid;
            this.Disciplineid = disciplineid;
            this.Primary_ind = primary_ind;
        }
        public bool Delete()
        {
            bool success = MicrositeSpeciality.DeleteMicrositeSpeciality(this.Dsid);
            if (success)
                this.Dsid = 0;
            return success;
        }

        public bool Update()
        {
            return MicrositeSpeciality.UpdateMicrositeSpeciality(this.Dsid, this.Msid, this.Disciplineid, this.Primary_ind);
        }

        #endregion
        #region Methods
        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all MicrositeSpecialitys
        //</summary>
        public static List<MicrositeSpeciality> GetMicrositeSpeciality(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "disciplineid";

            List<MicrositeSpeciality> MicrositeSpecialitys = null;
            string key = "MicrositeSpecialitys_MicrositeSpecialitys_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeSpecialitys = (List<MicrositeSpeciality>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeSpecialityInfo> recordset = SiteProvider.PR2.GetMicrositeSpeciality(cSortExpression);
                MicrositeSpecialitys = GetMicrositeSpecialityListFromMicrositeSpecialityInfoList(recordset);
                BasePR.CacheData(key, MicrositeSpecialitys);
            }
            return MicrositeSpecialitys;
        }
        public static List<MicrositeSpeciality> GetMicrositeSpecialityByMsid(int Msid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "disciplineid";

            List<MicrositeSpeciality> MicrositeSpecialitys = null;
            string key = "MicrositeSpecialitys_MicrositeSpecialitys_" + Msid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeSpecialitys = (List<MicrositeSpeciality>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeSpecialityInfo> recordset = SiteProvider.PR2.GetMicrositeSpecialityByMsid(Msid, cSortExpression);
                MicrositeSpecialitys = GetMicrositeSpecialityListFromMicrositeSpecialityInfoList(recordset);
                BasePR.CacheData(key, MicrositeSpecialitys);
            }
            return MicrositeSpecialitys;
        }
        /// <summary>
        /// Returns a MicrositeSpecialitys object with the specified ID
        /// </summary>
        public static MicrositeSpeciality GetMicrositeSpecialityByID(int id)
        {
            MicrositeSpeciality MicrositeSpecialitys = null;
            string key = "MicrositeSpecialitys_MicrositeSpecialitys_" + id.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeSpecialitys = (MicrositeSpeciality)BizObject.Cache[key];
            }
            else
            {
                MicrositeSpecialitys = GetMicrositeSpecialityFromMicrositeSpecialityInfo(SiteProvider.PR2.GetMicrositeSpecialityByID(id));
                BasePR.CacheData(key, MicrositeSpecialitys);
            }
            return MicrositeSpecialitys;
        }
       public static int InsertMicrositeSpecialitys(int dsid, int msid, int disciplineid, bool primary_ind)
        {
            MicrositeSpecialityInfo record = new MicrositeSpecialityInfo(0, msid, disciplineid, primary_ind);
            int ret = SiteProvider.PR2.InsertMicrositeSpeciality(record);

            BizObject.PurgeCacheItems("MicrositeSpecialitys_MicrositeSpecialitys");
            return ret;
        }
       public static bool UpdateMicrositeSpecialityLists(List<MicrositeSpeciality> Specialities, int Msid)
       {
           return SiteProvider.PR2.UpdateMicrositeSpecialityLists(GetMicrositeSpecialityInfoListFromMicrositeSpecialityList(Specialities), Msid);
       }
        /// <summary>
        /// Updates an existing MicrositeSpeciality
        /// </summary>
        public static bool UpdateMicrositeSpeciality(int dsid, int msid, int disciplineid, bool primary_ind)
        {
            MicrositeSpecialityInfo record = new MicrositeSpecialityInfo(dsid, msid, disciplineid, primary_ind);
            bool ret = SiteProvider.PR2.UpdateMicrositeSpeciality(record);

            BizObject.PurgeCacheItems("MicrositeSpecialitys_MicrositeSpecialitys_" + msid.ToString());
            BizObject.PurgeCacheItems("MicrositeSpecialitys_MicrositeSpecialitys");
            return ret;
        }
        public static bool DeleteMicrositeSpeciality(int dsid)
        {
            bool IsOKToDelete = OKToDelete(dsid);
            if (IsOKToDelete)
            {
                return (bool)DeleteMicrositeSpeciality(dsid, true);
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Deletes an existing MicrositeSpeciality - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteMicrositeSpeciality(int dsid, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteMicrositeSpeciality(dsid);
            BizObject.PurgeCacheItems("MicrositeSpecialitys_MicrositeSpecialitys");
            return ret;
        }

        /// <summary>
        /// Checks to see if a MicrositeSpeciality can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }


        /// <summary>
        /// Returns a MicrositeSpeciality object filled with the data taken from the input MicrositeSpecialityInfo
        /// </summary>
        private static MicrositeSpeciality GetMicrositeSpecialityFromMicrositeSpecialityInfo(MicrositeSpecialityInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new MicrositeSpeciality(record.Dsid, record.Msid, record.Disciplineid, record.Primary_ind);
            }
        }
        private static MicrositeSpecialityInfo GetMicrositeSpecialityInfoFromMicrositeSpeciality(MicrositeSpeciality record)
        {
            if (record == null)
                return null;
            else
            {
                return new MicrositeSpecialityInfo(record.Dsid, record.Msid, record.Disciplineid, record.Primary_ind);
            }
        }
        /// <summary>
        /// Returns a list of MicrositeSpeciality objects filled with the data taken from the input list of MicrositeSpecialityInfo
        /// </summary>
        private static List<MicrositeSpeciality> GetMicrositeSpecialityListFromMicrositeSpecialityInfoList(List<MicrositeSpecialityInfo> recordset)
        {
            List<MicrositeSpeciality> MicrositeSpecialitys = new List<MicrositeSpeciality>();
            foreach (MicrositeSpecialityInfo record in recordset)
                MicrositeSpecialitys.Add(GetMicrositeSpecialityFromMicrositeSpecialityInfo(record));
            return MicrositeSpecialitys;
        }
        private static List<MicrositeSpecialityInfo> GetMicrositeSpecialityInfoListFromMicrositeSpecialityList(List<MicrositeSpeciality> recordset)
        {
            List<MicrositeSpecialityInfo> MicrositeSpecialities = new List<MicrositeSpecialityInfo>();
            foreach (MicrositeSpeciality record in recordset)
                MicrositeSpecialities.Add(GetMicrositeSpecialityInfoFromMicrositeSpeciality(record));
            return MicrositeSpecialities;
        }
        #endregion
    }
}