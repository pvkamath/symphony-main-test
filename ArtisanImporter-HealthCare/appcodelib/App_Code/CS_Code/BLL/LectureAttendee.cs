﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 
/// <summary>
/// Summary description for LectureAttendee
/// </summary>
public class LectureAttendee: BasePR
    {
        #region Variables and Properties
        private int _LectAttID = 0;
        public int LectAttID
        {
            get { return _LectAttID; }
            protected set { _LectAttID = value; }
        }

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            set { _LectEvtID = value; }
        }

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _UserName = "";
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        private string _PW = "";
        public string PW
        {
            get { return _PW; }
            set { _PW = value; }
        }

        private string _FirstName = "";
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        private string _LastName = "";
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        private string _MI = "";
        public string MI
        {
            get { return _MI; }
            set { _MI = value; }
        }

        private string _Address1 = "";
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        private string _Address2 = "";
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        private string _City = "";
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }

        private string _State = "";
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        private string _Zip = "";
        public string Zip
        {
            get { return _Zip; }
            set { _Zip = value; }
        }

        private string _Email = "";
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private DateTime _DateAdded = System.DateTime.Now;
        public DateTime DateAdded
        {
            get { return _DateAdded; }
            set { _DateAdded = value; }
        }

        private bool _Completed = false;
        public bool Completed
        {
            get { return _Completed; }
            set { _Completed = value; }
        }


        public LectureAttendee(int LectAttID, int LectEvtID, int UserID, string UserName, string PW, string FirstName, string LastName, string MI, string Address1, string Address2, string City, string State, string Zip, string Email, DateTime DateAdded, bool Completed)
        {
            this.LectAttID = LectAttID;
            this.LectEvtID = LectEvtID;
            this.UserID = UserID;
            this.UserName = UserName;
            this.PW = PW;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.MI = MI;
            this.Address1 = Address1;
            this.Address2 = Address2;
            this.City = City;
            this.State = State;
            this.Zip = Zip;
            this.Email = Email;
            this.DateAdded = DateAdded;
            this.Completed = Completed;
        }

        public bool Delete()
        {
            bool success = LectureAttendee.DeleteLectureAttendee(this.LectAttID);
            if (success)
                this.LectAttID = 0;
            return success;
        }

        public bool Update()
        {
            return LectureAttendee.UpdateLectureAttendee(this.LectAttID, this.LectEvtID, this.UserID, this.UserName, this.PW, this.FirstName, this.LastName, this.MI, this.Address1, this.Address2, this.City, this.State, this.Zip, this.Email, this.DateAdded, this.Completed);
        }

        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all LectureAttendees
        /// </summary>
        public static List<LectureAttendee> GetLectureAttendees(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "DateAdded";

            List<LectureAttendee> LectureAttendees = null;
            string key = "LectureAttendees_LectureAttendees_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureAttendees = (List<LectureAttendee>)BizObject.Cache[key];
            }
            else
            {
                List<LectureAttendeeInfo> recordset = SiteProvider.PR2.GetLectureAttendees(cSortExpression);
                LectureAttendees = GetLectureAttendeeListFromLectureAttendeeInfoList(recordset);
                BasePR.CacheData(key, LectureAttendees);
            }
            return LectureAttendees;
        }


        /// <summary>
        /// Returns the number of total LectureAttendees
        /// </summary>
        public static int GetLectureAttendeeCount()
        {
            int LectureAttendeeCount = 0;
            string key = "LectureAttendees_LectureAttendeeCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureAttendeeCount = (int)BizObject.Cache[key];
            }
            else
            {
                LectureAttendeeCount = SiteProvider.PR2.GetLectureAttendeeCount();
                BasePR.CacheData(key, LectureAttendeeCount);
            }
            return LectureAttendeeCount;
        }

        /// <summary>
        /// Returns a LectureAttendee object with the specified ID
        /// </summary>
        public static LectureAttendee GetLectureAttendeeByID(int LectAttID)
        {
            LectureAttendee LectureAttendee = null;
            string key = "LectureAttendees_LectureAttendee_" + LectAttID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureAttendee = (LectureAttendee)BizObject.Cache[key];
            }
            else
            {
                LectureAttendee = GetLectureAttendeeFromLectureAttendeeInfo(SiteProvider.PR2.GetLectureAttendeeByID(LectAttID));
                BasePR.CacheData(key, LectureAttendee);
            }
            return LectureAttendee;
        }

        /// <summary>
        /// Updates an existing LectureAttendee
        /// </summary>
        public static bool UpdateLectureAttendee(int LectAttID, int LectEvtID, int UserID, string UserName, string PW,
            string FirstName, string LastName, string MI, string Address1, string Address2, string City, string State,
            string Zip, string Email, DateTime DateAdded, bool Completed)
        {
            UserName = BizObject.ConvertNullToEmptyString(UserName);
            PW = BizObject.ConvertNullToEmptyString(PW);
            FirstName = BizObject.ConvertNullToEmptyString(FirstName);
            LastName = BizObject.ConvertNullToEmptyString(LastName);
            MI = BizObject.ConvertNullToEmptyString(MI);
            Address1 = BizObject.ConvertNullToEmptyString(Address1);
            Address2 = BizObject.ConvertNullToEmptyString(Address2);
            City = BizObject.ConvertNullToEmptyString(City);
            State = BizObject.ConvertNullToEmptyString(State);
            Zip = BizObject.ConvertNullToEmptyString(Zip);
            Email = BizObject.ConvertNullToEmptyString(Email);


            LectureAttendeeInfo record = new LectureAttendeeInfo(LectAttID, LectEvtID, UserID, UserName, PW, FirstName, LastName, MI, Address1, Address2, City, State, Zip, Email, DateAdded, Completed);
            bool ret = SiteProvider.PR2.UpdateLectureAttendee(record);

            BizObject.PurgeCacheItems("LectureAttendees_LectureAttendee_" + LectAttID.ToString());
            BizObject.PurgeCacheItems("LectureAttendees_LectureAttendees");
            return ret;
        }

        /// <summary>
        /// Creates a new LectureAttendee
        /// </summary>
        public static int InsertLectureAttendee(int LectEvtID, int UserID, string UserName, string PW, string FirstName, 
            string LastName, string MI, string Address1, string Address2, string City, string State, string Zip,
            string Email, DateTime DateAdded, bool Completed)
        {
            UserName = BizObject.ConvertNullToEmptyString(UserName);
            PW = BizObject.ConvertNullToEmptyString(PW);
            FirstName = BizObject.ConvertNullToEmptyString(FirstName);
            LastName = BizObject.ConvertNullToEmptyString(LastName);
            MI = BizObject.ConvertNullToEmptyString(MI);
            Address1 = BizObject.ConvertNullToEmptyString(Address1);
            Address2 = BizObject.ConvertNullToEmptyString(Address2);
            City = BizObject.ConvertNullToEmptyString(City);
            State = BizObject.ConvertNullToEmptyString(State);
            Zip = BizObject.ConvertNullToEmptyString(Zip);
            Email = BizObject.ConvertNullToEmptyString(Email);

           
            LectureAttendeeInfo record = new LectureAttendeeInfo(0, LectEvtID, UserID, UserName, PW, FirstName, LastName, MI, Address1, Address2, City, State, Zip, Email, DateAdded, Completed);
            int ret = SiteProvider.PR2.InsertLectureAttendee(record);
            BizObject.PurgeCacheItems("LectureAttendees_LectureAttendee");
       
            return ret;
        }


        public static int InsertLectureAttendee(int LectEvtID, int UserID, string UserName, string PW, string FirstName,
            string LastName, string MI, string Address1, string Address2, string City, string State, string Zip,
            string Email,  bool Completed)
        {
            UserName = BizObject.ConvertNullToEmptyString(UserName);
            PW = BizObject.ConvertNullToEmptyString(PW);
            FirstName = BizObject.ConvertNullToEmptyString(FirstName);
            LastName = BizObject.ConvertNullToEmptyString(LastName);
            MI = BizObject.ConvertNullToEmptyString(MI);
            Address1 = BizObject.ConvertNullToEmptyString(Address1);
            Address2 = BizObject.ConvertNullToEmptyString(Address2);
            City = BizObject.ConvertNullToEmptyString(City);
            State = BizObject.ConvertNullToEmptyString(State);
            Zip = BizObject.ConvertNullToEmptyString(Zip);
            Email = BizObject.ConvertNullToEmptyString(Email);


            LectureAttendeeInfo record = new LectureAttendeeInfo(0, LectEvtID, UserID, UserName, PW, FirstName, LastName, MI, Address1, Address2, City, State, Zip, Email, DateTime.Now, Completed);
            int ret = SiteProvider.PR2.InsertLectureAttendee(record);

            BizObject.PurgeCacheItems("LectureAttendees_LectureAttendee");
            return ret;
        }

    
    
    /// <summary>
        /// Deletes an existing LectureAttendee, but first checks if OK to delete
        /// </summary>
        public static bool DeleteLectureAttendee(int LectAttID)
        {
            bool IsOKToDelete = OKToDelete(LectAttID);
            if (IsOKToDelete)
            {
                return (bool)DeleteLectureAttendee(LectAttID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing LectureAttendee - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteLectureAttendee(int LectAttID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteLectureAttendee(LectAttID);
            //         new RecordDeletedEvent("LectureAttendee", LectAttID, null).Raise();
            BizObject.PurgeCacheItems("LectureAttendees_LectureAttendee");
            return ret;
        }



        /// <summary>
        /// Checks to see if a LectureAttendee can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int LectAttID)
        {
            return true;
        }



        /// <summary>
        /// Returns a LectureAttendee object filled with the data taken from the input LectureAttendeeInfo
        /// </summary>
        private static LectureAttendee GetLectureAttendeeFromLectureAttendeeInfo(LectureAttendeeInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new LectureAttendee(record.LectAttID, record.LectEvtID, record.UserID, record.UserName, record.PW, record.FirstName, record.LastName, record.MI, record.Address1, record.Address2, record.City, record.State, record.Zip, record.Email, record.DateAdded, record.Completed);
            }
        }

        /// <summary>
        /// Returns a list of LectureAttendee objects filled with the data taken from the input list of LectureAttendeeInfo
        /// </summary>
        private static List<LectureAttendee> GetLectureAttendeeListFromLectureAttendeeInfoList(List<LectureAttendeeInfo> recordset)
        {
            List<LectureAttendee> LectureAttendees = new List<LectureAttendee>();
            foreach (LectureAttendeeInfo record in recordset)
                LectureAttendees.Add(GetLectureAttendeeFromLectureAttendeeInfo(record));
            return LectureAttendees;
        }


        /// <summary>
        /// Returns a collection with all LectureAttendees for a Lecture Event
        /// </summary>
        public static List<LectureAttendee> GetLectureAttendeesByLectEvtID(int LectEvtID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "DateAdded";

            List<LectureAttendee> LectureAttendees = null;
            string key = "LectureAttendees_LectureAttendees_LectEvtID_" + LectEvtID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureAttendees = (List<LectureAttendee>)BizObject.Cache[key];
            }
            else
            {
                List<LectureAttendeeInfo> recordset = SiteProvider.PR2.GetLectureAttendeesByLectEvtID(LectEvtID, cSortExpression);
                LectureAttendees = GetLectureAttendeeListFromLectureAttendeeInfoList(recordset);
                BasePR.CacheData(key, LectureAttendees);
            }
            return LectureAttendees;
        }

        #endregion
    }
}

