﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

//rid, testid, license_number, course_id, roster_id, licensecode, cb_message, cb_time
//int rid, int testid, string license_number, string course_id, string roster_id, string licensecode, string cb_message, DateTime cb_time
namespace PearlsReview.BLL
{
    public partial class RosterInfo : BasePR
    {
        #region variables
        public int rid
        {
            get;
            set;
        }

        public int testid
        {
            get;
            set;
        }

        public string license_number
        {
            get;
            set;
        }

        public string course_id
        {
            get;
            set;
        }

        public string roster_id
        {
            get;
            set;
        }

        public string licensecode
        {
            get;
            set;
        }

        public string cb_message
        {
            get;
            set;
        }

        public DateTime cb_time
        {
            get;
            set;
        }
        #endregion

        #region Methods

        public RosterInfo()
        { }

        public RosterInfo(int rid, int testid, string license_number, string course_id, string roster_id,
            string licensecode, string cb_message, DateTime cb_time)
        {
            this.rid = rid;
            this.testid = testid;
            this.license_number = license_number;
            this.course_id = course_id;
            this.roster_id = roster_id;
            this.licensecode = licensecode;
            this.cb_message = cb_message;
            this.cb_time = cb_time;
        }

        public bool Update()
        {
            return Update(this.rid, this.testid, this.license_number,
                this.course_id, this.roster_id, this.licensecode, this.cb_message, DateTime.Now);
        }

        public static bool Update(int rid, int testid, string license_number, string course_id, 
            string roster_id, string licensecode, string cb_message, DateTime cb_time)
        {
            RosterInfo rosterInfo = new RosterInfo(rid, testid, license_number, course_id, roster_id, licensecode, cb_message, DateTime.Now);
            SiteProvider.PR2.UpdateRoster(rosterInfo);
            return true;
        }

        public bool Insert()
        {
            return Insert(this.rid, this.testid, this.license_number,
                this.course_id, this.roster_id, this.licensecode, this.cb_message, DateTime.Today);
        }

        public static bool Insert(int rid, int testid, string license_number, string course_id, 
            string roster_id, string licensecode, string cb_message, DateTime cb_time)
        {
            RosterInfo rosterInfo = new RosterInfo(rid, testid, license_number, course_id, roster_id, licensecode, cb_message, DateTime.Now);
            return SiteProvider.PR2.InsertRoster(rosterInfo) > 0;
        }

        public bool Delete()
        {
            return Delete(this.rid);
        }

        public static bool Delete(int rid)
        {
            SiteProvider.PR2.DeleteRoster(rid);
            return true;
        }

        public static List<RosterInfo> GetRosters(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "rid";

            return SiteProvider.PR2.GetRosters(cSortExpression);
        }

        //method to get the roster by uniqueID 
        public static RosterInfo GetRosterByID(int rid)
        {
            return SiteProvider.PR2.GetRosterByID(rid);
        }

       #endregion
    }
}