﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for Sidebar
/// </summary>
public class Sidebar: BasePR
    {
        #region Variables and Properties

        private int _SB_ID = 0;
        public int SB_ID
        {
            get { return _SB_ID; }
            protected set { _SB_ID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private string _Title = "";
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private string _Body = "";
        public string Body
        {
            get { return _Body; }
            set { _Body = value; }
        }

        public Sidebar(int SB_ID, int TopicID, string Title, string Body)
        {
            this.SB_ID = SB_ID;
            this.TopicID = TopicID;
            this.Title = Title;
            this.Body = Body;
        }

        public bool Delete()
        {
            bool success = Sidebar.DeleteSidebar(this.SB_ID);
            if (success)
                this.SB_ID = 0;
            return success;
        }

        public bool Update()
        {
            return Sidebar.UpdateSidebar(this.SB_ID, this.TopicID, this.Title, this.Body);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Sidebars
        /// </summary>
        public static List<Sidebar> GetSidebars(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Sidebar> Sidebars = null;
            string key = "Sidebars_Sidebars_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Sidebars = (List<Sidebar>)BizObject.Cache[key];
            }
            else
            {
                List<SidebarInfo> recordset = SiteProvider.PR2.GetSidebars(cSortExpression);
                Sidebars = GetSidebarListFromSidebarInfoList(recordset);
                BasePR.CacheData(key, Sidebars);
            }
            return Sidebars;
        }


        /// <summary>
        /// Returns the number of total Sidebars
        /// </summary>
        public static int GetSidebarCount()
        {
            int SidebarCount = 0;
            string key = "Sidebars_SidebarCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SidebarCount = (int)BizObject.Cache[key];
            }
            else
            {
                SidebarCount = SiteProvider.PR2.GetSidebarCount();
                BasePR.CacheData(key, SidebarCount);
            }
            return SidebarCount;
        }

        /// <summary>
        /// Returns a Sidebar object with the specified ID
        /// </summary>
        public static Sidebar GetSidebarByID(int SB_ID)
        {
            Sidebar Sidebar = null;
            string key = "Sidebars_Sidebar_" + SB_ID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Sidebar = (Sidebar)BizObject.Cache[key];
            }
            else
            {
                Sidebar = GetSidebarFromSidebarInfo(SiteProvider.PR2.GetSidebarByID(SB_ID));
                BasePR.CacheData(key, Sidebar);
            }
            return Sidebar;
        }

        /// <summary>
        /// Returns a collection with all Sidebars associated with the specified TopicID
        /// </summary>
        public static List<Sidebar> GetSidebarsByTopicID(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Sidebar> Sidebars = null;
            string key = "Sidebars_Sidebars_TopicID_" + TopicID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Sidebars = (List<Sidebar>)BizObject.Cache[key];
            }
            else
            {
                List<SidebarInfo> recordset = SiteProvider.PR2.GetSidebarsByTopicID(TopicID, cSortExpression);
                Sidebars = GetSidebarListFromSidebarInfoList(recordset);
                BasePR.CacheData(key, Sidebars);
            }
            return Sidebars;
        }


        /// <summary>
        /// Returns the number of total Sidebars associated with the specified TopicID
        /// </summary>
        public static int GetSidebarCountByTopicID(int TopicID)
        {
            int SidebarCount = 0;
            string key = "Sidebars_SidebarCount_TopicID_" + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SidebarCount = (int)BizObject.Cache[key];
            }
            else
            {
                SidebarCount = SiteProvider.PR2.GetSidebarCountByTopicID(TopicID);
                BasePR.CacheData(key, SidebarCount);
            }
            return SidebarCount;
        }




        /// <summary>
        /// Updates an existing Sidebar
        /// </summary>
        public static bool UpdateSidebar(int SB_ID, int TopicID, string Title, string Body)
        {
            Title = BizObject.ConvertNullToEmptyString(Title);


            SidebarInfo record = new SidebarInfo(SB_ID, TopicID, Title, Body);
            bool ret = SiteProvider.PR2.UpdateSidebar(record);

            BizObject.PurgeCacheItems("Sidebars_Sidebar_" + SB_ID.ToString());
            BizObject.PurgeCacheItems("Sidebars_Sidebars");
            return ret;
        }

        /// <summary>
        /// Creates a new Sidebar
        /// </summary>
        public static int InsertSidebar(int TopicID, string Title, string Body)
        {
            Title = BizObject.ConvertNullToEmptyString(Title);


            SidebarInfo record = new SidebarInfo(0, TopicID, Title, Body);
            int ret = SiteProvider.PR2.InsertSidebar(record);

            BizObject.PurgeCacheItems("Sidebars_Sidebar");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Sidebar, but first checks if OK to delete
        /// </summary>
        public static bool DeleteSidebar(int SB_ID)
        {
            bool IsOKToDelete = OKToDelete(SB_ID);
            if (IsOKToDelete)
            {
                return (bool)DeleteSidebar(SB_ID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Sidebar - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteSidebar(int SB_ID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteSidebar(SB_ID);
            //         new RecordDeletedEvent("Sidebar", SB_ID, null).Raise();
            BizObject.PurgeCacheItems("Sidebars_Sidebar");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Sidebar can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom Body in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int SB_ID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Sidebar object filled with the data taken from the input SidebarInfo
        /// </summary>
        private static Sidebar GetSidebarFromSidebarInfo(SidebarInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Sidebar(record.SB_ID, record.TopicID, record.Title, record.Body);
            }
        }

        /// <summary>
        /// Returns a list of Sidebar objects filled with the data taken from the input list of SidebarInfo
        /// </summary>
        private static List<Sidebar> GetSidebarListFromSidebarInfoList(List<SidebarInfo> recordset)
        {
            List<Sidebar> Sidebars = new List<Sidebar>();
            foreach (SidebarInfo record in recordset)
                Sidebars.Add(GetSidebarFromSidebarInfo(record));
            return Sidebars;
        }

        #endregion
    }
}
