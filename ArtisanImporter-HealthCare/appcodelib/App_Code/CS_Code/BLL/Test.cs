﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using PearlsReview.QTI;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for Test
    /// </summary>

    public class Test : BasePR
    {
        #region Variables and Properties

        private int _TestID = 0;
        public int TestID
        {
            get { return _TestID; }
            set { _TestID = value; }
        }

        private string _OldTestID = "";
        public string OldTestID
        {
            get { return _OldTestID; }
            set { _OldTestID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        //private string _Subtitle;
        //public string Subtitle
        //{
        //    get { return _Subtitle; }
        //    private set { _Subtitle = value; }
        //}

        //private string _Course_Number;
        //public string Course_Number
        //{
        //    get { return _Course_Number; }
        //    private set { _Course_Number = value; }
        //}

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private DateTime _BuyDate = System.DateTime.Now;
        public DateTime BuyDate
        {
            get { return _BuyDate; }
            set { _BuyDate = value; }
        }

        private decimal _Credits = 0.00M;
        public decimal Credits
        {
            get { return _Credits; }
            set { _Credits = value; }
        }

        private DateTime _LastMod = System.DateTime.Now;
        public DateTime LastMod
        {
            get { return _LastMod; }
            set { _LastMod = value; }
        }

        private string _Name = "";
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private bool _NoSurvey = false;
        public bool NoSurvey
        {
            get { return _NoSurvey; }
            set { _NoSurvey = value; }
        }

        private string _Status = "";
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        private string _UserName = "";
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        private string _XMLTest = "";
        public string XMLTest
        {
            get { return _XMLTest; }
            set { _XMLTest = value; }
        }

        private string _XMLSurvey = "";
        public string XMLSurvey
        {
            get { return _XMLSurvey; }
            set { _XMLSurvey = value; }
        }

        private int _TestVers = 0;
        public int TestVers
        {
            get { return _TestVers; }
            set { _TestVers = value; }
        }

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            set { _LectEvtID = value; }
        }

        private bool _SurveyComplete = false;
        public bool SurveyComplete
        {
            get { return _SurveyComplete; }
            set { _SurveyComplete = value; }
        }

        private bool _VignetteComplete = false;
        public bool VignetteComplete
        {
            get { return _VignetteComplete; }
            set { _VignetteComplete = value; }
        }

        //private string _XMLVignette = "";
        //public string XMLVignette
        //{
        //    get { return _XMLVignette; }
        //    set { _XMLVignette = value; }
        //}

        //private int _VignetteVersion = 0;
        //public int VignetteVersion
        //{
        //    get { return _VignetteVersion; }
        //    set { _VignetteVersion = value; }
        //}

        //private int _VignetteScore = 0;
        //public int VignetteScore
        //{
        //    get { return _VignetteScore; }
        //    set { _VignetteScore = value; }
        //}

        private int _Score = 0;
        public int Score
        {
            get { return _Score; }
            set { _Score = value; }
        }

        private DateTime _Printed_Date = System.DateTime.Now;
        public DateTime Printed_Date
        {
            get { return _Printed_Date; }
            set { _Printed_Date = value; }
        }

        private DateTime _Emailed_Date = System.DateTime.Now;
        public DateTime Emailed_Date
        {
            get { return _Emailed_Date; }
            set { _Emailed_Date = value; }
        }

        private DateTime _Reported_Date = System.DateTime.Now;
        public DateTime Reported_Date
        {
            get { return _Reported_Date; }
            set { _Reported_Date = value; }
        }

        private DateTime _View_Date = System.DateTime.Now;
        public DateTime View_Date
        {
            get { return _View_Date; }
            set { _View_Date = value; }
        }

        private int _UniqueID = 0;
        public int UniqueID
        {
            get { return _UniqueID; }
            set { _UniqueID = value; }
        }

        private int _AlterID = 0;
        public int AlterID
        {
            get { return _AlterID; }
            set { _AlterID = value; }
        }

        private bool _GCCharged = false;
        public bool GCCharged
        {
            get { return _GCCharged; }
            set { _GCCharged = value; }
        }
        private int _facilityid = 0;
        public int facilityid
        {
            get { return _facilityid; }
            set { _facilityid = value; }
        }

        public Test() { }

        public Test(int TestID, string OldTestID, int TopicID, int UserID, DateTime BuyDate,
            decimal Credits, DateTime LastMod, string Name, bool NoSurvey, string Status, string UserName,
            string XMLTest, string XMLSurvey, int TestVers, int LectEvtID, bool SurveyComplete,
            bool VignetteComplete,
            //string XMLVignette, int VignetteVersion, int VignetteScore,
            int Score,
            DateTime Printed_Date,
            DateTime Emailed_Date, DateTime Reported_Date,
            DateTime View_Date, int UniqueID, int AlterID, bool GCCharged, int facilityId)
        {
            this.TestID = TestID;
            this.OldTestID = OldTestID;
            this.TopicID = TopicID;
            //this.Subtitle = Subtitle;
            //this.Course_Number = Course_Number;
            this.UserID = UserID;
            this.BuyDate = BuyDate;
            this.Credits = Credits;
            this.LastMod = LastMod;
            this.Name = Name;
            this.NoSurvey = NoSurvey;
            this.Status = Status;
            this.UserName = UserName;
            this.XMLTest = XMLTest;
            this.XMLSurvey = XMLSurvey;
            this.TestVers = TestVers;
            this.LectEvtID = LectEvtID;
            this.SurveyComplete = SurveyComplete;
            this.VignetteComplete = VignetteComplete;
            //this.VignetteVersion = VignetteVersion;
            //this.XMLVignette = XMLVignette;
            //this.VignetteScore = VignetteScore;
            this.Score = Score;
            this.Printed_Date = Printed_Date;
            this.Emailed_Date = Emailed_Date;
            this.Reported_Date = Reported_Date;
            this.View_Date = View_Date;
            this.UniqueID = UniqueID;
            this.AlterID = AlterID;
            this.GCCharged = GCCharged;
            this.facilityid = facilityId;

        }

        public Test(int topicid, int userid, DateTime buydate, string topicname, string username, string status, DateTime
             printeddate, DateTime emaileddate, DateTime reporteddate, int uniqueid, int facilityid)
        {
            //this.TopicID = topicid;
            //this.UserID = userid;
            //this.BuyDate = buydate;
            //this.Name = topicname;
            //this.UserName = username;
            //this.Status = status;
            //this.Printed_Date = printeddate;
            //this.Emailed_Date = emaileddate;
            //this.Reported_Date = reporteddate;
            //this.UniqueID = uniqueid;
            //this.facilityid = facilityid;
        }



        public bool Delete()
        {
            bool success = Test.DeleteTest(this.TestID);
            if (success)
                this.TestID = 0;
            return success;
        }

        public bool Update()
        {
            return Test.UpdateTest(this.TestID, this.OldTestID, this.TopicID, this.UserID, this.BuyDate,
                this.Credits, this.LastMod, this.Name, this.NoSurvey, this.Status, this.UserName,
                this.XMLTest, this.XMLSurvey, this.TestVers, this.LectEvtID, this.SurveyComplete,
                this.VignetteComplete,
                //this.XMLVignette, this.VignetteVersion, this.VignetteScore,
                this.Score, this.View_Date, this.UniqueID, this.AlterID, this.GCCharged, this.facilityid, this.Printed_Date,
                this.Emailed_Date, this.Reported_Date);
        }
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        public static int EnrollStaff(int topicid, int userid, DateTime buydate, string topicname, string username, string status, DateTime
            printeddate, DateTime emaileddate, DateTime reporteddate, int uniqueid, int facilityid)
        {
            TestInfo testinfo = new TestInfo(topicid, userid, buydate, topicname, username, status,
             printeddate, emaileddate, reporteddate, uniqueid, facilityid);

            return SiteProvider.PR2.EnrollStaff(testinfo);
        }
        /// <summary>
        /// Returns a collection with all Tests
        /// </summary>
        public static List<Test> GetTests(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod";

            List<Test> Tests = null;
            string key = "Tests_Tests_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Tests = (List<Test>)BizObject.Cache[key];
            }
            else
            {
                List<TestInfo> recordset = SiteProvider.PR2.GetTests(cSortExpression);
                Tests = GetTestListFromTestInfoList(recordset);
                BasePR.CacheData(key, Tests);
            }
            return Tests;
        }


        /// <summary>
        /// Returns the number of total Tests
        /// </summary>
        public static int GetTestCount()
        {
            int TestCount = 0;
            string key = "Tests_TestCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TestCount = (int)BizObject.Cache[key];
            }
            else
            {
                TestCount = SiteProvider.PR2.GetTestCount();
                BasePR.CacheData(key, TestCount);
            }
            return TestCount;
        }

        /// <summary>
        /// Returns the number of Completed Tests for a FacilityID
        /// </summary>
        public static int GetCompletedTestCountByFacilityID(int FacilityID)
        {
            int TestCount = 0;
            string key = "Tests_CompletedTestCountByFacilityID_" + FacilityID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TestCount = (int)BizObject.Cache[key];
            }
            else
            {
                TestCount = SiteProvider.PR2.GetCompletedTestCountByFacilityID(FacilityID);
                BasePR.CacheData(key, TestCount);
            }
            return TestCount;
        }

        /// <summary>
        /// Returns the number of Completed Tests for a UserID
        /// </summary>
        public static int GetCompletedTestCountByUserID(int UserID)
        {
            int TestCount = 0;
            string key = "Tests_CompletedTestCountByUserID_" + UserID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TestCount = (int)BizObject.Cache[key];
            }
            else
            {
                TestCount = SiteProvider.PR2.GetCompletedTestCountByUserID(UserID);
                BasePR.CacheData(key, TestCount);
            }
            return TestCount;
        }

        /// <summary>
        /// Returns the users need to send email to remind enrollment confirm or course complete
        /// </summary>
        public static DataSet GetUnconfirmedorUncompletedUsers(char status)
        {
            return SiteProvider.PR2.GetUnconfirmedorUncompletedUsers(status);
        }

        /// <summary>
        /// Returns a Test object with the specified ID
        /// </summary>
        public static Test GetTestByID(int TestID)
        {
            Test Test = null;
            string key = "Tests_Test_" + TestID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Test = (Test)BizObject.Cache[key];
            }
            else
            {
                Test = GetTestFromTestInfo(SiteProvider.PR2.GetTestByID(TestID));
                BasePR.CacheData(key, Test);
            }
            return Test;
        }



        /// <summary>
        /// Updates an existing Test
        /// </summary>
        public static bool UpdateTest(int TestID, string OldTestID, int TopicID, int UserID, DateTime BuyDate,
            decimal Credits, DateTime LastMod, string Name, bool NoSurvey, string Status, string UserName,
            string XMLTest, string XMLSurvey, int TestVers, int LectEvtID, bool SurveyComplete,
           bool VignetteComplete,
            //string XMLVignette, int VignetteVersion, int VignetteScore,
            int Score,
            DateTime View_Date, int UniqueID, int AlterID, bool GCCharged, int facilityid, DateTime Printed_Date,
            DateTime Emailed_Date, DateTime Reported_Date)
        {
            OldTestID = BizObject.ConvertNullToEmptyString(OldTestID);
            Name = BizObject.ConvertNullToEmptyString(Name);
            Status = BizObject.ConvertNullToEmptyString(Status);
            UserName = BizObject.ConvertNullToEmptyString(UserName);
            XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);
            XMLSurvey = BizObject.ConvertNullToEmptyString(XMLSurvey);
            //XMLVignette = BizObject.ConvertNullToEmptyString(XMLVignette);


            TestInfo record = new TestInfo(TestID, OldTestID, TopicID, UserID, BuyDate,
                Credits, LastMod, Name, NoSurvey, Status, UserName,
                XMLTest, XMLSurvey, TestVers, LectEvtID, SurveyComplete,
                VignetteComplete,
                //XMLVignette, VignetteVersion, VignetteScore,
                Score,
                Printed_Date,
                Emailed_Date, Reported_Date,
                View_Date, UniqueID, AlterID, GCCharged, facilityid);
            bool ret = SiteProvider.PR2.UpdateTest(record);

            BizObject.PurgeCacheItems("Tests_Test_" + TestID.ToString());
            BizObject.PurgeCacheItems("Tests_Tests");
            return ret;
        }
        public static bool UpdateMergedIDs(string Ids, int AccountMergeTo)
        {
            return SiteProvider.PR2.UpdateMergedIDs(Ids, AccountMergeTo);
        }
        /// <summary>
        /// Updates all Tests' TopicName for a certain TopicID
        /// </summary>
        public static bool UpdateTestTopicNameByTopicID(int TopicID, string TopicName)
        {
            TopicName = BizObject.ConvertNullToEmptyString(TopicName);
            bool ret = SiteProvider.PR2.UpdateTestTopicNameByTopicID(TopicID, TopicName);

            BizObject.PurgeCacheItems("Tests_Tests");
            return ret;
        }

        public static bool UpdateTestFollowUpSurvey(int testid, bool value)
        {
            return SiteProvider.PR2.UpdateTestFollowUpSurvey(testid, value);
        }

        public static bool UnEnrollByUserIdAndTopicId(int userid, int topicid)
        {
            return SiteProvider.PR2.UnEnrollByUserIdAndTopicId(userid, topicid);
        }


        public static bool IsEnrolledByTopicIdAndUserId(int userid, int topicid)
        {
            return SiteProvider.PR2.IsEnrolledByTopicIdAndUserId(userid, topicid);
        }


        public static bool HasEnrolledStuffByTopicIdAndEnrolledById(int enrolledbyid, int topicid)
        {
            return SiteProvider.PR2.HasEnrolledStuffByTopicIdAndEnrolledById(enrolledbyid, topicid);
        }


        /// <summary>
        /// Creates a new Test
        /// </summary>
        public static int InsertTest(string OldTestID, int TopicID, int UserID, DateTime BuyDate,
            decimal Credits, DateTime LastMod, string Name, bool NoSurvey, string Status, string UserName,
            string XMLTest, string XMLSurvey, int TestVers, int LectEvtID, bool SurveyComplete,
           bool VignetteComplete,
            //string XMLVignette, int VignetteVersion, int VignetteScore,
            int Score,
            //DateTime Printed_Date,
            //DateTime Emailed_Date, DateTime Reported_Date,
            DateTime View_Date, int UniqueID, int AlterID, bool GCCharged, int facilityid)
        {
            OldTestID = BizObject.ConvertNullToEmptyString(OldTestID);
            Name = BizObject.ConvertNullToEmptyString(Name);
            Status = BizObject.ConvertNullToEmptyString(Status);
            UserName = BizObject.ConvertNullToEmptyString(UserName);
            XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);
            XMLSurvey = BizObject.ConvertNullToEmptyString(XMLSurvey);
            //XMLVignette = BizObject.ConvertNullToEmptyString(XMLVignette);

            TestInfo record = new TestInfo(0, OldTestID, TopicID, UserID, BuyDate,
                Credits, LastMod, Name, NoSurvey, Status, UserName,
                XMLTest, XMLSurvey, TestVers, LectEvtID, SurveyComplete,
                VignetteComplete,
                //XMLVignette, VignetteVersion, VignetteScore,
                Score,
                DateTime.MinValue,
                DateTime.MinValue, DateTime.MinValue,
                View_Date, UniqueID, AlterID, GCCharged, facilityid);
            int ret = SiteProvider.PR2.InsertTest(record);

            BizObject.PurgeCacheItems("Tests_Test");
            return ret;
        }

        /// <summary>
        /// Creates a new Test
        /// </summary>
        public static int InsertTest(string OldTestID, int TopicID, int UserID, DateTime BuyDate,
            decimal Credits, DateTime LastMod, string Name, bool NoSurvey, string Status, string UserName,
            string XMLTest, string XMLSurvey, int TestVers, int LectEvtID, bool SurveyComplete,
           bool VignetteComplete,
            //string XMLVignette, int VignetteVersion, int VignetteScore,
            int Score,
            DateTime View_Date, int UniqueID, int AlterID, bool GCCharged, int facilityid, DateTime Printed_Date,
            DateTime Emailed_Date, DateTime Reported_Date)
        {
            OldTestID = BizObject.ConvertNullToEmptyString(OldTestID);
            Name = BizObject.ConvertNullToEmptyString(Name);
            Status = BizObject.ConvertNullToEmptyString(Status);
            UserName = BizObject.ConvertNullToEmptyString(UserName);
            XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);
            XMLSurvey = BizObject.ConvertNullToEmptyString(XMLSurvey);
            //XMLVignette = BizObject.ConvertNullToEmptyString(XMLVignette);

            TestInfo record = new TestInfo(0, OldTestID, TopicID, UserID, BuyDate,
                Credits, LastMod, Name, NoSurvey, Status, UserName,
                XMLTest, XMLSurvey, TestVers, LectEvtID, SurveyComplete,
                VignetteComplete,
                //XMLVignette, VignetteVersion, VignetteScore,
                Score,
                Printed_Date,
                Emailed_Date, Reported_Date,
                View_Date, UniqueID, AlterID, GCCharged, facilityid);
            int ret = SiteProvider.PR2.InsertTest(record);

            BizObject.PurgeCacheItems("Tests_Test");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Test, but first checks if OK to delete
        /// </summary>
        public static bool DeleteTest(int TestID)
        {
            bool IsOKToDelete = OKToDelete(TestID);
            if (IsOKToDelete)
            {
                return (bool)DeleteTest(TestID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Test - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteTest(int TestID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteTest(TestID);
            //         new RecordDeletedEvent("Test", TestID, null).Raise();
            BizObject.PurgeCacheItems("Tests_Test");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Test can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int TestID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Test object filled with the data taken from the input TestInfo
        /// </summary>
        private static Test GetTestFromTestInfo(TestInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Test(record.TestID, record.OldTestID, record.TopicID, record.UserID, record.BuyDate,
                record.Credits, record.LastMod, record.Name, record.NoSurvey, record.Status, record.UserName,
                record.XMLTest, record.XMLSurvey, record.TestVers, record.LectEvtID, record.SurveyComplete,
                record.VignetteComplete,
                    //record.XMLVignette, record.VignetteVersion, record.VignetteScore,
                record.Score,
                record.Printed_Date,
                record.Emailed_Date, record.Reported_Date,
                record.View_Date, record.UniqueID, record.AlterID, record.GCCharged, record.facilityid);
            }
        }

        /// <summary>
        /// Returns a list of Test objects filled with the data taken from the input list of TestInfo
        /// </summary>
        private static List<Test> GetTestListFromTestInfoList(List<TestInfo> recordset)
        {
            List<Test> Tests = new List<Test>();
            foreach (TestInfo record in recordset)
                Tests.Add(GetTestFromTestInfo(record));
            return Tests;
        }

        /// <summary>
        /// Returns a collection with all Tests for the specified UserName
        /// </summary>
        public static List<Test> GetTestsByUserName(string UserName, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            List<Test> Tests = null;
            string key = "Tests_Tests_" + UserName.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Tests = (List<Test>)BizObject.Cache[key];
            }
            else
            {
                // "normal" Tests by category ID
                List<TestInfo> recordset = SiteProvider.PR2.GetTestsByUserName(UserName);
                Tests = GetTestListFromTestInfoList(recordset);
                BasePR.CacheData(key, Tests);

            }
            return Tests;
        }

        /// <summary>
        /// Returns a collection with Completed Tests for the specified UserName
        /// </summary>
        public static List<Test> GetCompletedTestsByUserName(string UserName, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            List<Test> Tests = null;
            string key = "Tests_CompletedTests_" + UserName.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Tests = (List<Test>)BizObject.Cache[key];
            }
            else
            {
                // "normal" Tests by category ID
                List<TestInfo> recordset = SiteProvider.PR2.GetCompletedTestsByUserName(UserName);
                Tests = GetTestListFromTestInfoList(recordset);
                BasePR.CacheData(key, Tests);

            }
            return Tests;
        }

        /// <summary>
        /// Returns a collection with Incomplete Tests for the specified UserName
        /// </summary>
        public static List<Test> GetIncompleteTestsByUserName(string UserName, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            List<Test> Tests = null;
            //            string key = "Tests_IncompleteTests_" + UserName.ToString() + cSortExpression.ToString();

            //            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            //            {
            //                Tests = (List<Test>)BizObject.Cache[key];
            //            }
            //            else
            //            {
            // don't query if username is blank

            if (UserName.Trim().Length > 0)
            {
                List<TestInfo> recordset = SiteProvider.PR2.GetIncompleteTestsByUserName(UserName);
                Tests = GetTestListFromTestInfoList(recordset);
                //                BasePR.CacheData(key, Tests);
            }
            //            }
            return Tests;
        }

        /// <summary>
        /// Returns a collection with all Tests for the specified UserID
        /// </summary>
        public static List<Test> GetTestsByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            List<Test> Tests = null;
            //string key = "Tests_Tests_" + UserID.ToString() + cSortExpression.ToString();

            //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            //{
            //    Tests = (List<Test>)BizObject.Cache[key];
            //}
            //else
            //{
            List<TestInfo> recordset = SiteProvider.PR2.GetTestsByUserID(UserID);
            Tests = GetTestListFromTestInfoList(recordset);
            //BasePR.CacheData(key, Tests);

            //}
            return Tests;
        }
        /// <summary>
        /// Returns a collection with Completed Tests for the specified UserID
        /// </summary>
        public static System.Data.DataSet GetCompletedTestsByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;
            //string key = "Tests_CompletedTests_UserID_" + UserID.ToString() + cSortExpression.ToString();

            //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            //{
            //    Tests = (List<Test>)BizObject.Cache[key];
            //}
            //else
            //{
            if (cSortExpression.Length == 0)
            {
                cSortExpression = "LastMod DESC";
                Tests = SiteProvider.PR2.GetCompletedTestsByUserID(UserID);
            }
            else
            {
                Tests = SiteProvider.PR2.GetCompletedTestsByUserID(UserID, cSortExpression);

            }
            //Tests = GetTestListFromTestInfoList(recordset);
            //BasePR.CacheData(key, Tests);

            //}
            return Tests;
        }

        /// <summary>
        /// Returns a collection with Completed Tests for the specified UserID
        /// </summary>
        public static System.Data.DataSet GetTop3CompletedTestsByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;

            Tests = SiteProvider.PR2.GetTop3CompletedTestsByUserID(UserID, cSortExpression);

            return Tests;
        }
        public static System.Data.DataSet GetMicrositeCompletedTestsByUserID(int UserID, int DomainID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;
            //string key = "Tests_CompletedTests_UserID_" + UserID.ToString() + cSortExpression.ToString();

            //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            //{
            //    Tests = (List<Test>)BizObject.Cache[key];
            //}
            //else
            //{
            if (cSortExpression.Length == 0)
            {
                cSortExpression = "LastMod DESC";
                Tests = SiteProvider.PR2.GetMicrositeCompletedTestsByUserID(UserID, DomainID);
            }
            else
            {
                Tests = SiteProvider.PR2.GetMicrositeCompletedTestsByUserID(UserID, DomainID, cSortExpression);

            }
            //Tests = GetTestListFromTestInfoList(recordset);
            //BasePR.CacheData(key, Tests);

            //}
            return Tests;
        }

        public static System.Data.DataSet GetMSPaidCoursesInProgressByUserId(int userID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;
            Tests = SiteProvider.PR2.GetMSPaidCoursesInProgressByUserId(userID, cSortExpression);
            return Tests;
        }


        public static System.Data.DataSet GetPRRetailCoursesInProgressByUserId(int userID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;
            Tests = SiteProvider.PR2.GetPRRetailCoursesInProgressByUserId(userID, cSortExpression);
            return Tests;
        }

        //added for ahDomain

        public static System.Data.DataSet GetMSPaidCoursesInProgressByUserIdandAhDomainId(int userID, int ahDomainId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;
            Tests = SiteProvider.PR2.GetMSPaidCoursesInProgressByUserIdAndAhDomainId(userID, ahDomainId, cSortExpression);
            return Tests;
        }

        public static System.Data.DataSet GetTop3MSPaidCoursesInProgressByUserId(int userID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;
            Tests = SiteProvider.PR2.GetTop3MSPaidCoursesInProgressByUserId(userID, cSortExpression);
            return Tests;
        }

        //PRRetail

        public static System.Data.DataSet GetTop3PRRetailCoursesInProgressByUserId(int userID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;
            Tests = SiteProvider.PR2.GetTop3PRRetailCoursesInProgressByUserId(userID, cSortExpression);
            return Tests;
        }


        //added for ahdomain

        public static System.Data.DataSet GetTop3MSPaidCoursesInProgressByUserIdandAhDomain(int userID, int ahDomainID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;
            Tests = SiteProvider.PR2.GetTop3MSPaidCoursesInProgressByUserIdandAhDomain(userID, ahDomainID, cSortExpression);
            return Tests;
        }
        /// <summary>
        /// Returns top 5 collection with Completed Tests for the specified UserID
        /// </summary>
        public static System.Data.DataSet GetTopFiveCompletedTestsByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;
            Tests = SiteProvider.PR2.GetTopFiveCompletedTestsByUserID(UserID);
            return Tests;
        }


        /// <summary>
        /// Returns a collection with Completed Tests for the specified UserID
        /// </summary>
        public static System.Data.DataSet GetCompletedTestsByUserIDandFacID(int UserID, int facilityID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;
            //string key = "Tests_CompletedTests_UserID_" + UserID.ToString() + cSortExpression.ToString();

            //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            //{
            //    Tests = (List<Test>)BizObject.Cache[key];
            //}
            //else
            //{
            Tests = SiteProvider.PR2.GetCompletedTestsByUserIDandFacID(UserID, facilityID, cSortExpression);
            //Tests = GetTestListFromTestInfoList(recordset);
            //BasePR.CacheData(key, Tests);

            //}
            return Tests;
        }

        /// <summary>
        /// Returns a completed test object with the specified userID and testid
        /// </summary>
        public static Test GetCompletedTestByTestIDUserID(int UserID, int TestID)
        {
            Test Test = null;
            string key = "Tests_Test_" + UserID.ToString() + TestID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Test = (Test)BizObject.Cache[key];
            }
            else
            {
                Test = GetTestFromTestInfo(SiteProvider.PR2.GetCompletedTestByTestIDUserID(UserID, TestID));
                BasePR.CacheData(key, Test);
            }
            return Test;
        }

        /// <summary>
        /// Returns the number of Completed Tests for a FacilityID
        /// </summary>
        public static int GetCompletedTestYearCountByUserID(int UserID)
        {
            int TestCount = 0;
            string key = "Tests_GetCompletedTestYearCountByUserID_" + UserID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TestCount = (int)BizObject.Cache[key];
            }
            else
            {
                TestCount = SiteProvider.PR2.GetCompletedTestYearCountByUserID(UserID);
                BasePR.CacheData(key, TestCount);
            }
            return TestCount;
        }

        public static decimal GetTotalContactHoursYearCountByUserID(int UserID)
        {
            decimal ContactHoursCount = 0;
            string key = "Tests_GetTotalContactHoursYearCountByUserID_" + UserID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ContactHoursCount = (decimal)BizObject.Cache[key];
            }
            else
            {
                ContactHoursCount = SiteProvider.PR2.GetTotalContactHoursYearCountByUserID(UserID);
                BasePR.CacheData(key, ContactHoursCount);
            }
            return ContactHoursCount;
        }

        /// <summary>
        /// Returns a collection with InCompleted Need Survey Tests for the specified UserID
        /// </summary>
        public static System.Data.DataSet GetNeedSurveyTestsByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            System.Data.DataSet Tests = null;
            //string key = "Tests_CompletedTests_UserID_" + UserID.ToString() + cSortExpression.ToString();

            //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            //{
            //    Tests = (List<Test>)BizObject.Cache[key];
            //}
            //else
            //{
            Tests = SiteProvider.PR2.GetNeedSurveyTestsByUserID(UserID);
            //Tests = GetTestListFromTestInfoList(recordset);
            //BasePR.CacheData(key, Tests);

            //}
            return Tests;
        }

        /// <summary>
        /// Returns a Count of  InCompleted Need Survey Tests for the specified UserID
        /// </summary>
        public static int GetNeedSurveyTestsCountByUserID(int UserID)
        {
            int TestCount = 0;
            string key = "Tests_GetNeedSurveyTestsCountByUserID_" + UserID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TestCount = (int)BizObject.Cache[key];
            }
            else
            {
                TestCount = SiteProvider.PR2.GetNeedSurveyTestsCountByUserID(UserID);
                BasePR.CacheData(key, TestCount);
            }
            return TestCount;

        }


        /// <summary>
        /// Returns a collection with Incomplete Tests for the specified UserID
        /// </summary>
        public static List<Test> GetIncompleteTestsByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            List<Test> Tests = null;
            //            string key = "Tests_IncompleteTests_" + UserName.ToString() + cSortExpression.ToString();

            //            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            //            {
            //                Tests = (List<Test>)BizObject.Cache[key];
            //            }
            //            else
            //            {
            // don't query if UserID is bad

            List<TestInfo> recordset = SiteProvider.PR2.GetIncompleteTestsByUserID(UserID);
            Tests = GetTestListFromTestInfoList(recordset);
            //                BasePR.CacheData(key, Tests);
            //            }
            return Tests;
        }

        /// <summary>
        /// Returns a collection with Incomplete Tests for the specified UserID adn TopicID
        /// </summary>
        public static List<Test> GetIncompleteTestsByUserIDAndTopicID(int UserID, int TopicID)
        {
            //if (cSortExpression == null)
            //    cSortExpression = "";

            //// provide default sort
            //if (cSortExpression.Length == 0)
            //    cSortExpression = "BuyDate DESC";

            List<Test> Tests = null;
            //            string key = "Tests_IncompleteTests_" + UserName.ToString() + cSortExpression.ToString();

            //            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            //            {
            //                Tests = (List<Test>)BizObject.Cache[key];
            //            }
            //            else
            //            {
            // don't query if UserID is bad

            List<TestInfo> recordset = SiteProvider.PR2.GetIncompleteTestsByUserIDAndTopicID(UserID, TopicID);
            Tests = GetTestListFromTestInfoList(recordset);
            //                BasePR.CacheData(key, Tests);
            //            }
            return Tests;
        }
        /// <summary>
        /// Returns Test By UserID And TopicID
        /// </summary>
        public static Test GetTestByUserIDAndTopicID(int UserID, int TopicID)
        {
            Test Test = null;
            string key = "Tests_Test_" + UserID.ToString() + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Test = (Test)BizObject.Cache[key];
            }
            else
            {
                Test = GetTestFromTestInfo(SiteProvider.PR2.GetTestByUserIDAndTopicID(UserID, TopicID));
                BasePR.CacheData(key, Test);
            }
            return Test;
        }
        /// <summary>
        /// Returns Test By UserID And TopicID without assigning status
        /// </summary>
        public static Test GetTestByUserIDAndTopicIDG(int UserID, int TopicID)
        {
            Test Test = null;
            string key = "Tests_Test_" + UserID.ToString() + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Test = (Test)BizObject.Cache[key];
            }
            else
            {
                Test = GetTestFromTestInfo(SiteProvider.PR2.GetTestByUserIDAndTopicIDG(UserID, TopicID));
                BasePR.CacheData(key, Test);
            }
            return Test;
        }
        /// <summary>
        /// Returns Test By UserID And TopicID
        /// </summary>
        public static Test GetIncompleteICETestByUserIDAndTopicID(int UserID, int TopicID)
        {
            Test Test = null;
            string key = "Tests_Test_ICETest_" + UserID.ToString() + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Test = (Test)BizObject.Cache[key];
            }
            else
            {
                Test = GetTestFromTestInfo(SiteProvider.PR2.GetIncompleteICETestByUserIDAndTopicID(UserID, TopicID));
                BasePR.CacheData(key, Test);
            }
            return Test;
        }
        
        /// <summary>
        /// Returns Test By UserID And TopicID, ViewProgress use only
        /// </summary>
        public static Test GetTestByUserIDAndTopicIDP(int UserID, int TopicID)
        {
            Test Test = null;
            string key = "Tests_Test_" + UserID.ToString() + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Test = (Test)BizObject.Cache[key];
            }
            else
            {
                Test = GetTestFromTestInfo(SiteProvider.PR2.GetTestByUserIDAndTopicIDP(UserID, TopicID));
                BasePR.CacheData(key, Test);
            }
            return Test;
        }

        /// <summary>
        /// Returns Enrolled Test By UserID And TopicID
        /// </summary>
        public static Test GetEnrolledTestByUserIDAndTopicID(int UserID, int TopicID)
        {
            Test Test = null;
            string key = "Tests_Test_" + UserID.ToString() + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Test = (Test)BizObject.Cache[key];
            }
            else
            {
                Test = GetTestFromTestInfo(SiteProvider.PR2.GetEnrolledTestByUserIDAndTopicID(UserID, TopicID));
                BasePR.CacheData(key, Test);
            }
            return Test;
        }
        /// <summary>
        /// Returns Failed Test By UserID And TopicID
        /// </summary>
        public static Test GetFailedTestByUserIDAndTopicID(int UserID, int TopicID)
        {
            Test Test = null;
            string key = "Tests_Test_F" + UserID.ToString() + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Test = (Test)BizObject.Cache[key];
            }
            else
            {
                Test = GetTestFromTestInfo(SiteProvider.PR2.GetFailedTestByUserIDAndTopicID(UserID, TopicID));
                BasePR.CacheData(key, Test);
            }
            return Test;
        }
        public static bool IsPrepaidMSCoursePaid(int UserID, int TopicID, int ProfessionID)
        {
            Test Test = null;
            string key = "Tests_Test_" + UserID.ToString() + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Test = (Test)BizObject.Cache[key];
            }
            else
            {
                Test = GetTestFromTestInfo(SiteProvider.PR2.GetPaidPrepaidMSTestByUserIDAndTopicID(UserID, TopicID, ProfessionID));
                BasePR.CacheData(key, Test);
            }
            return (Test == null) ? false : true;
        }

        public static Boolean IsPrepaidMSCourseInProgress(int userID, int topicID, int professionID)
        {
            Test Test = null;
            string key = "Tests_IsPrepaidMSCourseInProgress_" +
                userID.ToString() + topicID.ToString() + professionID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Test = (Test)BizObject.Cache[key];
            }
            else
            {
                Test = GetTestFromTestInfo(
                    SiteProvider.PR2.GetInProgressTestByUserIDTopicIDAndMsid(userID, topicID, professionID));
                BasePR.CacheData(key, Test);
            }
            return (Test == null) ? false : true;
        }

        public static Test GetPrePaidTestByUserIDAndTopicID(int UserID, int TopicID, int FacilityID)
        {
            return GetTestFromTestInfo(SiteProvider.PR2.GetPrePaidTestByUserIDAndTopicID(UserID, TopicID, FacilityID));
        }

        //public static bool IsUserTakePrePaidTopic(int UserID, int TopicID, int FacilityID)
        //{
        //    return (SiteProvider.PR2.IsUserTakePrePaidTopic(UserID, TopicID, FacilityID));
        //}

        ///// <summary>
        ///// Returns a list of test question objects for the specified TestID
        ///// </summary>
        //public static List<QTIQuestionObject> GetTestQuestionObjectListByTestID(int TestID)
        //{
        //    TestDefinition oTestDefinition = null;
        //    QTIUtils oQTIUtils = new QTIUtils();

        //    // NOTE: No caching of these items, because they contain user responses
        //    // along with test questions
        //    List<QTIQuestionObject> QTIQuestionObjects = null;

        //    // first, get the test record so we have access to topicid and user's responses
        //    Test oTest = Test.GetTestByID(TestID);

        //    if (oTest != null)
        //    {
        //        // get the test definition record for this topic
        //        oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(oTest.TopicID);

        //        if (oTestDefinition != null)
        //        {
        //            // get the QTIQuestionObjectList from the TestDefinition XML
        //            QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
        //            // workaround until XMl is correct
        //            //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
        //        }
        //    }

        //    if (QTIQuestionObjects != null)
        //    {
        //        // go a list of QTIQuestionObjects, so now add the
        //        // user responses, if any

        //        // first, if the user completed the test, we will show the completed test
        //        // with user's answers filled in -- but we will always show the latest version
        //        // of the test and will fill in the correct answers automatically so it always
        //        // matches the current version of the test

        //        // If NOT completed yet,
        //        // if the user's test record shows version = 0, there are no user responses yet
        //        // (this is the condition for all tests in progress migrated from the old system,
        //        // as well as for new tests added to MyCurriculum and not worked on yet).
        //        // In this case, just fill in all user responses with "X" for M/C and T/F question types
        //        // and with blank for all others

        //        // also do this if the test version in the TestDefinition record is different from the
        //        // version recorded with the answers so far in the Test record. This will "reset" the
        //        // user's answers to unanswered status so it matches the new test questions.
        //        if (oTest.Status == "C")
        //        {
        //            //// completed test, so just fill in the correct answers for the current version 
        //            //// of the test
        //            //foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
        //            //{
        //            //    if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
        //            //        QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
        //            //    {
        //            //        QTIQuestion.cUserAnswer = QTIQuestion.cCorrectAnswer;
        //            //    }
        //            //    else
        //            //    {
        //            //        QTIQuestion.cUserAnswer = "";
        //            //    }
        //            //}

        //            // completed test ??? that matches the current test definition version number ???
        //            // so it may have some user responses stored. Grab them from the test XMLTest field
        //            // and populate them here.
        //            List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
        //            String[] Answers = UserAnswers.ToArray();

        //            int iLoop = 0;
        //            foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
        //            {
        //                QTIQuestion.cUserAnswer = Answers[iLoop];
        //                // be sure we're not empty -- if so, set to X
        //                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
        //                    QTIQuestion.cUserAnswer = "X";
        //                iLoop++;
        //            }
        //        }
        //        else if (oTest.TestVers != oTestDefinition.Version)
        //        {
        //            // incomplete test that does not match the current test definition version number
        //            // set to default X and blank answers
        //            foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
        //            {
        //                if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
        //                    QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
        //                {
        //                    QTIQuestion.cUserAnswer = "X";
        //                }
        //                else
        //                {
        //                    QTIQuestion.cUserAnswer = "";
        //                }
        //            }
        //        }
        //        else
        //        {
        //            // incomplete test that matches the current test definition version number
        //            // so it may have some user responses stored. Grab them from the test XMLTest field
        //            // and populate them here.
        //            List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
        //            String[] Answers = UserAnswers.ToArray();

        //            int iLoop = 0;
        //            foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
        //            {
        //                QTIQuestion.cUserAnswer = Answers[iLoop];
        //                // be sure we're not empty -- if so, set to X
        //                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
        //                    QTIQuestion.cUserAnswer = "X";
        //                iLoop++;
        //            }
        //        }
        //    }

        //    return QTIQuestionObjects;
        //}

        //<summary>
        //Returns a list of test question objects for the specified TestID
        //NOTE: 12/01/2008 New version of this method with new logic
        //NOTE: 25/01/2010 corrected the logic-Swetha
        //</summary>
        public static List<QTIQuestionObject> GetTestQuestionObjectListByTestID(int TestID)
        {
            TestDefinition oTestDefinition = null;
            QTIUtils oQTIUtils = new QTIUtils();

            // NOTE: No caching of these items, because they contain user responses
            // along with test questions
            List<QTIQuestionObject> QTIQuestionObjects = null;

            // first, get the test record so we have access to topicid and user's responses
            Test oTest = Test.GetTestByID(TestID);

            if (oTest != null)
            {
                // get the test definition record for this topic
                oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(oTest.TopicID);

                if (oTestDefinition != null)
                {
                    // get the QTIQuestionObjectList from the TestDefinition XML
                    QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
                    // workaround until XMl is correct
                    //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
                }

                if (QTIQuestionObjects != null)
                {
                    #region "description"
                    // go a list of QTIQuestionObjects, so now add the
                    // user responses, if any

                    // first, if the user completed the test, we will show the completed test
                    // with user's answers filled in -- but we will always show the latest version
                    // of the test and will fill in the correct answers automatically so it always
                    // matches the current version of the test

                    // If NOT completed yet,
                    // if the user's test record shows version = 0, there are no user responses yet
                    // (this is the condition for all tests in progress migrated from the old system,
                    // as well as for new tests added to MyCurriculum and not worked on yet).
                    // In this case, just fill in all user responses with "X" for M/C and T/F question types
                    // and with blank for all others

                    // also do this if the test version in the TestDefinition record is different from the
                    // version recorded with the answers so far in the Test record. This will "reset" the
                    // user's answers to unanswered status so it matches the new test questions. 
                    #endregion


                    if (oTest.TestVers != oTestDefinition.Version)
                    {
                        #region " version not equals"

                        List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
                        String[] Answers = UserAnswers.ToArray();

                        int iLoop = 0;
                        foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                        {
                            if (Convert.ToInt32(QTIQuestion.cHeaderID) > 0 && iLoop < Answers.Length)
                            {
                                QTIQuestion.cUserAnswer = Answers[iLoop];
                                // be sure we're not empty -- if so, set to X
                                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                                    QTIQuestion.cUserAnswer = "X";
                                iLoop++;
                            }


                        }

                        //// incomplete test that does not match the current test definition version number
                        //// set to default X and blank answers
                        //foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                        //{
                        //    if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                        //        QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                        //    {
                        //        QTIQuestion.cUserAnswer = "X";
                        //    }
                        //    else
                        //    {
                        //        QTIQuestion.cUserAnswer = "";
                        //    }
                        //}
                        #endregion
                    }
                    else if (oTest.Status == "C")
                    {
                        #region "version equals and test completed"

                        //// completed test, so just fill in the correct answers for the current version 
                        //// of the test
                        //foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                        //{
                        //    if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                        //        QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                        //    {
                        //        QTIQuestion.cUserAnswer = QTIQuestion.cCorrectAnswer;
                        //    }
                        //    else
                        //    {
                        //        QTIQuestion.cUserAnswer = "";
                        //    }
                        //}

                        // completed test ??? that matches the current test definition version number ???
                        // so it may have some user responses stored. Grab them from the test XMLTest field
                        // and populate them here.
                        List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
                        String[] Answers = UserAnswers.ToArray();

                        int iLoop = 0;
                        foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                        {
                            if (Convert.ToInt32(QTIQuestion.cHeaderID) > 0)
                            {
                                QTIQuestion.cUserAnswer = Answers[iLoop];
                                // be sure we're not empty -- if so, set to X
                                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                                    QTIQuestion.cUserAnswer = "X";
                                iLoop++;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        // incomplete test that matches the current test definition version number
                        // so it may have some user responses stored. Grab them from the test XMLTest field
                        // and populate them here.
                        List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
                        String[] Answers = UserAnswers.ToArray();

                        int iLoop = 0;
                        foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                        {
                            if (Convert.ToInt32(QTIQuestion.cHeaderID) > 0 && iLoop < Answers.Length)
                            {
                                QTIQuestion.cUserAnswer = Answers[iLoop];
                                // be sure we're not empty -- if so, set to X
                                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                                    QTIQuestion.cUserAnswer = "X";
                                iLoop++;
                            }


                        }
                    }
                }
            }
            return QTIQuestionObjects;
        }


        ///// <summary>
        ///// Returns a list of test Survey question objects for the specified TestID
        ///// Bhaskar N
        ///// </summary>
        //public static List<SQTIQuestionObject> GetSurveyQuestionObjectListByTestID(int TestID)
        //{
        //    SurveyDefinition oSurveyDefinition = null;
        //    SQTIUtils oSQTIUtils = new SQTIUtils();

        //    // NOTE: No caching of these items, because they contain user responses
        //    // along with test questions
        //    List<SQTIQuestionObject> SQTIQuestionObjects = null;

        //    // first, get the test record so we have access to topicid and user's responses
        //    Test oTest = Test.GetTestByID(TestID);
        //    //oTest.TopicID = 2; //bsk test

        //    if (oTest != null)
        //    {
        //        // get the test definition record for this topic                
        //        oSurveyDefinition = SurveyDefinition.GetSurveyDefinitionByTopicID(oTest.TopicID);

        //        if (oSurveyDefinition != null)
        //        {
        //            // get the QTIQuestionObjectList from the TestDefinition XML                                    
        //            SQTIQuestionObjects = oSQTIUtils.ConvertQTITestXMLToSQTIQuestionObjectList(oSurveyDefinition.XMLSurvey);                    
        //        }
        //    }
        //    return SQTIQuestionObjects;
        //}

        /// <summary>
        /// Bhaskar N
        /// </summary>
        public static List<QTIQuestionObject> GetAllTestsByTopicId(int TopicId, string FromDate, string Todate, string SortExpression)
        {
            TestDefinition oTestDefinition = null;
            QTIUtils oQTIUtils = new QTIUtils();
            List<QTIQuestionObject> QTIQuestionObjects = null;


            List<Test> l_Tests = null;
            string key = "Tests_Tests_" + TopicId;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                l_Tests = (List<Test>)BizObject.Cache[key];
            }
            else
            {
                List<TestInfo> recordset = SiteProvider.PR2.GetAllTestsByTopicId(TopicId, FromDate, Todate, SortExpression);
                l_Tests = GetTestListFromTestInfoList(recordset);
                BasePR.CacheData(key, l_Tests);
            }

            int jLoop = 0;
            int QTDispaly = 0;
            foreach (Test TestIds in l_Tests)
            {
                int TestID = TestIds.TestID;
                // first, get the test record so we have access to topicid and user's responses
                Test oTest = Test.GetTestByID(TestID);

                if (QTDispaly == 0)
                {
                    if (oTest != null)
                    {
                        // get the test definition record for this topic
                        oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(oTest.TopicID);

                        if (oTestDefinition != null)
                        {
                            // get the QTIQuestionObjectList from the TestDefinition XML
                            QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
                        }
                    }
                }
                if (QTIQuestionObjects != null)
                {
                    if (oTest.Status == "C")
                    {
                        List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
                        String[] Answers = UserAnswers.ToArray();

                        int iLoop = 0;
                        foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                        {
                            //check ...Question is having only header
                            if (Convert.ToInt32(QTIQuestion.cHeaderID) >= 0)
                            {
                                if (iLoop < UserAnswers.Count)
                                {
                                    if (Answers[iLoop].ToString() == "A")
                                    {
                                        QTIQuestion.cAnswerACount = QTIQuestion.cAnswerACount + 1;
                                    }
                                    else if (Answers[iLoop].ToString() == "B")
                                    {
                                        QTIQuestion.cAnswerBCount = QTIQuestion.cAnswerBCount + 1;
                                    }
                                    else if (Answers[iLoop].ToString() == "C")
                                    {
                                        QTIQuestion.cAnswerCCount = QTIQuestion.cAnswerCCount + 1;
                                    }
                                    else if (Answers[iLoop].ToString() == "D")
                                    {
                                        QTIQuestion.cAnswerDCount = QTIQuestion.cAnswerDCount + 1;
                                    }
                                    else if (Answers[iLoop].ToString() == "E")
                                    {
                                        QTIQuestion.cAnswerECount = QTIQuestion.cAnswerECount + 1;
                                    }
                                    else if (Answers[iLoop].ToString() == "F")
                                    {
                                        QTIQuestion.cAnswerFCount = QTIQuestion.cAnswerFCount + 1;
                                    }

                                    //Total count
                                    if ((Answers[iLoop].ToString().Trim()).Length != 0)
                                    {
                                        QTIQuestion.cAnswerTotalCount = QTIQuestion.cAnswerTotalCount + 1;
                                    }
                                }
                                iLoop++;
                            }

                        }
                    }
                }
                QTDispaly = 1;
                jLoop++;
            }

            return QTIQuestionObjects;
        }

        // Changed By navya
        /// <summary>
        /// Returns a list of vignette question objects for the specified TestID
        /// </summary>
        public static List<QTIQuestionObject> GetVignetteQuestionObjectListByTestID(int TestID, string sUserAnswers)
        {
            ViDefinition oViDefinition = null;
            QTIUtils oQTIUtils = new QTIUtils();

            // NOTE: No caching of these items, because they contain user responses
            // along with test questions
            List<QTIQuestionObject> QTIQuestionObjects = null;

            // first, get the test record so we have access to topicid and user's responses
            Test oTest = Test.GetTestByID(TestID);

            if (oTest != null)
            {
                // get the vignette definition record for this topic
                oViDefinition = ViDefinition.GetViDefinitionByTopicID(oTest.TopicID);

                if (oViDefinition != null)
                {
                    // get the QTIQuestionObjectList from the ViDefinition XML
                    QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oViDefinition.XMLTest);
                }
            }

            if (QTIQuestionObjects != null)
            {
                // go a list of QTIQuestionObjects, so now add the
                // user responses, if any

                // first, if the user completed the test, we will show the completed test
                // with user's answers filled in -- but we will always show the latest version
                // of the test and will fill in the correct answers automatically so it always
                // matches the current version of the test

                // If NOT completed yet,
                // if the user's test record shows version = 0, there are no user responses yet
                // (this is the condition for all tests in progress migrated from the old system,
                // as well as for new tests added to MyCurriculum and not worked on yet).
                // In this case, just fill in all user responses with "X" for M/C and T/F question types
                // and with blank for all others

                // also do this if the test version in the TestDefinition record is different from the
                // version recorded with the answers so far in the Test record. This will "reset" the
                // user's answers to unanswered status so it matches the new test questions.
                if (oTest.Status == "C")
                {
                    // completed test, so just fill in the correct answers for the current version 
                    // of the test
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                        {
                            QTIQuestion.cUserAnswer = QTIQuestion.cCorrectAnswer;
                        }
                        else
                        {
                            QTIQuestion.cUserAnswer = "";
                        }
                    }
                }
                // * chnaged By navya
                else if (sUserAnswers == null)
                {
                    // incomplete test that does not match the current vignette definition version number
                    // set to default X and blank answers
                    // NOTE: This is also the case with new test record that has not had the vignette
                    // shown yet -- it will start out with vignetteversion field = 0 to this
                    // comparison will fail and we will insert all empty answers
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                        {
                            QTIQuestion.cUserAnswer = "X";
                        }
                        else
                        {
                            QTIQuestion.cUserAnswer = "";
                        }
                    }
                }
                // * chnaged By navya
                else
                {
                    // incomplete test that matches the current test definition version number
                    // so it may have some user responses stored. Grab them from the test XMLVignette field
                    // and populate them here.
                    List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(sUserAnswers);
                    String[] Answers = UserAnswers.ToArray();

                    int iLoop = 0;
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        QTIQuestion.cUserAnswer = Answers[iLoop];
                        // be sure we're not empty -- if so, set to X
                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                            QTIQuestion.cUserAnswer = "X";
                        iLoop++;
                    }
                }

                if (oTest.VignetteComplete == true)
                {
                    // completed Vignette, so just fill in the correct answers for the current version 
                    // of the test
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                        {
                            QTIQuestion.cUserAnswer = QTIQuestion.cCorrectAnswer;
                        }
                        else
                        {
                            QTIQuestion.cUserAnswer = "";
                        }
                    }
                }
            }

            return QTIQuestionObjects;
        }






        // Changed By navya
        /// <summary>
        /// Returns a list of vignette question objects for the specified TestID
        /// </summary>
        public static List<QTIQuestionObject> GetVignetteQuestionObjectListByTopicID(int TopicID, string sUserAnswers)
        {
            ViDefinition oViDefinition = null;
            QTIUtils oQTIUtils = new QTIUtils();

            // NOTE: No caching of these items, because they contain user responses
            // along with test questions
            List<QTIQuestionObject> QTIQuestionObjects = null;

            // get the vignette definition record for this topic
            oViDefinition = ViDefinition.GetViDefinitionByTopicID(TopicID);

            if (oViDefinition != null)
            {
                // get the QTIQuestionObjectList from the ViDefinition XML
                QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oViDefinition.XMLTest);
            }


            if (QTIQuestionObjects != null)
            {
                // go a list of QTIQuestionObjects, so now add the
                // user responses, if any

                // first, if the user completed the test, we will show the completed test
                // with user's answers filled in -- but we will always show the latest version
                // of the test and will fill in the correct answers automatically so it always
                // matches the current version of the test

                // If NOT completed yet,
                // if the user's test record shows version = 0, there are no user responses yet
                // (this is the condition for all tests in progress migrated from the old system,
                // as well as for new tests added to MyCurriculum and not worked on yet).
                // In this case, just fill in all user responses with "X" for M/C and T/F question types
                // and with blank for all others

                // also do this if the test version in the TestDefinition record is different from the
                // version recorded with the answers so far in the Test record. This will "reset" the
                // user's answers to unanswered status so it matches the new test questions.
                //if (oTest.Status == "C")
                //{
                //    // completed test, so just fill in the correct answers for the current version 
                //    // of the test
                //    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                //    {
                //        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                //            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                //        {
                //            QTIQuestion.cUserAnswer = QTIQuestion.cCorrectAnswer;
                //        }
                //        else
                //        {
                //            QTIQuestion.cUserAnswer = "";
                //        }
                //    }
                //}
                // * chnaged By navya
                if (string.IsNullOrEmpty(sUserAnswers))
                {
                    // incomplete test that does not match the current vignette definition version number
                    // set to default X and blank answers
                    // NOTE: This is also the case with new test record that has not had the vignette
                    // shown yet -- it will start out with vignetteversion field = 0 to this
                    // comparison will fail and we will insert all empty answers
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                        {
                            QTIQuestion.cUserAnswer = "X";
                        }
                        else
                        {
                            QTIQuestion.cUserAnswer = "";
                        }
                    }
                }
                // * chnaged By navya
                else
                {
                    // incomplete test that matches the current test definition version number
                    // so it may have some user responses stored. Grab them from the test XMLVignette field
                    // and populate them here.
                    List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(sUserAnswers);
                    String[] Answers = UserAnswers.ToArray();

                    int iLoop = 0;
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        QTIQuestion.cUserAnswer = Answers[iLoop];
                        // be sure we're not empty -- if so, set to X
                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                            QTIQuestion.cUserAnswer = "X";
                        iLoop++;
                    }
                }
            }

            return QTIQuestionObjects;
        }


        ///// <summary>
        ///// Returns a list of test question objects for the specified TestID
        ///// NOTE: 12/01/2008 New version of this method with new logic
        ///// </summary>
        //public static List<QTIQuestionObject> GetTestQuestionObjectListByTestID(int TestID)
        //{
        //    TestDefinition oTestDefinition = null;
        //    QTIUtils oQTIUtils = new QTIUtils();

        //    // NOTE: No caching of these items, because they contain user responses
        //    // along with test questions
        //    List<QTIQuestionObject> QTIQuestionObjects = null;

        //    // first, get the test record so we have access to topicid and user's responses
        //    Test oTest = Test.GetTestByID(TestID);

        //    if (oTest != null)
        //    {
        //        // get the test definition record for this topic
        //        oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(oTest.TopicID);

        //        if (oTestDefinition != null)
        //        {
        //            // get the QTIQuestionObjectList from the TestDefinition XML
        //            QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
        //            // workaround until XMl is correct
        //            //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
        //        }
        //    }

        //    if (QTIQuestionObjects != null)
        //    {
        //        // go a list of QTIQuestionObjects, so now add the
        //        // user responses, if any

        //        // first, if the user completed the test, we will show the completed test
        //        // with user's answers filled in -- but we will always show the latest version
        //        // of the test and will fill in the correct answers automatically so it always
        //        // matches the current version of the test

        //        // If NOT completed yet,
        //        // if the user's test record shows version = 0, there are no user responses yet
        //        // (this is the condition for all tests in progress migrated from the old system,
        //        // as well as for new tests added to MyCurriculum and not worked on yet).
        //        // In this case, just fill in all user responses with "X" for M/C and T/F question types
        //        // and with blank for all others

        //        // also do this if the test version in the TestDefinition record is different from the
        //        // version recorded with the answers so far in the Test record. This will "reset" the
        //        // user's answers to unanswered status so it matches the new test questions.
        //        if (oTest.Status == "C")
        //        {
        //            //// completed test, so just fill in the correct answers for the current version 
        //            //// of the test
        //            //foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
        //            //{
        //            //    if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
        //            //        QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
        //            //    {
        //            //        QTIQuestion.cUserAnswer = QTIQuestion.cCorrectAnswer;
        //            //    }
        //            //    else
        //            //    {
        //            //        QTIQuestion.cUserAnswer = "";
        //            //    }
        //            //}

        //            // completed test ??? that matches the current test definition version number ???
        //            // so it may have some user responses stored. Grab them from the test XMLTest field
        //            // and populate them here.
        //            List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
        //            String[] Answers = UserAnswers.ToArray();

        //            int iLoop = 0;
        //            foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
        //            {
        //                QTIQuestion.cUserAnswer = Answers[iLoop];
        //                // be sure we're not empty -- if so, set to X
        //                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
        //                    QTIQuestion.cUserAnswer = "X";
        //                iLoop++;
        //            }
        //        }
        //        else if (oTest.TestVers != oTestDefinition.Version)
        //        {
        //            // incomplete test that does not match the current test definition version number
        //            // set to default X and blank answers
        //            foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
        //            {
        //                if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
        //                    QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
        //                {
        //                    QTIQuestion.cUserAnswer = "X";
        //                }
        //                else
        //                {
        //                    QTIQuestion.cUserAnswer = "";
        //                }
        //            }
        //        }
        //        else
        //        {
        //            // incomplete test that matches the current test definition version number
        //            // so it may have some user responses stored. Grab them from the test XMLTest field
        //            // and populate them here.
        //            List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
        //            String[] Answers = UserAnswers.ToArray();

        //            int iLoop = 0;
        //            foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
        //            {
        //                QTIQuestion.cUserAnswer = Answers[iLoop];
        //                // be sure we're not empty -- if so, set to X
        //                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
        //                    QTIQuestion.cUserAnswer = "X";
        //                iLoop++;
        //            }
        //        }
        //    }

        //    return QTIQuestionObjects;
        //}


        public static DataSet GetStaffEnrollmentBySearchCriteria(bool isAdmin,int uniqueid,string firstname, string lastname, int facilityid, int deptid, int topicid,
            int startrow, int maximumrows, string sortexpression, ref int numResults)
        {
        

            if (sortexpression == null)
                sortexpression = "";

            if (sortexpression.Length == 0)
                sortexpression = "cLastName";

            DataSet userEnrollments = SiteProvider.PR2.GetStaffEnrollmentsBySearechCriteria(isAdmin,uniqueid,firstname, lastname, facilityid, deptid, topicid,
            startrow, maximumrows, sortexpression, ref numResults);

            return userEnrollments;
        }

        public static int UpdateEnrollment(int testid, int uniqueid, DateTime printed_date, DateTime email_date, string status)
        {
            return SiteProvider.PR2.UpdateEnrollment(testid,uniqueid,printed_date,email_date,status);
        }

         public static DataSet GetEnrolleesByUserid(List<int> tests,int topicid, int facilityid)
        {
            DataSet result = new DataSet();
            foreach (int iid in tests)
            {
                DataSet temp = SiteProvider.PR2.GetEnrolleeByUserid(iid,topicid,facilityid);
                result.Merge(temp);
            }
            return result;
        }

         public static DataSet GetOTCompletions()
         {
             DataSet result = new DataSet();
             result = SiteProvider.PR2.GetOTCompletions();                 
             
             return result;
         }

        public static bool DelelteEnrollment(int testid, int topicid)
        {
            bool ret = SiteProvider.PR2.DeleteEnrollment(testid, topicid);

            return ret;
        }

        

     

        ///// <summary>
        ///// Returns a list of test question objects for the specified CartID
        ///// NOTE: 12/01/2008 New version of this method with new logic
        ///// </summary>
        public static List<QTIQuestionObject> GetTestQuestionObjectListByCartID(int CartID)
        {
            TestDefinition oTestDefinition = null;
            QTIUtils oQTIUtils = new QTIUtils();

            // NOTE: No caching of these items, because they contain user responses
            // along with test questions
            List<QTIQuestionObject> QTIQuestionObjects = null;

            // first, get the test record so we have access to topicid and user's responses
            Cart oCart = Cart.GetCartByID(CartID);

            if (oCart != null)
            {
                // get the test definition record for this topic
                oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(oCart.TopicID);

                if (oTestDefinition != null)
                {
                    // get the QTIQuestionObjectList from the TestDefinition XML
                    QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
                    // workaround until XMl is correct
                    //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
                }
            }

            if (QTIQuestionObjects != null)
            {
                // go a list of QTIQuestionObjects, so now add the
                // user responses, if any

                // first, if the user completed the test, we will show the completed test
                // with user's answers filled in -- but we will always show the latest version
                // of the test and will fill in the correct answers automatically so it always
                // matches the current version of the test

                // If NOT completed yet,
                // if the user's test record shows version = 0, there are no user responses yet
                // (this is the condition for all tests in progress migrated from the old system,
                // as well as for new tests added to MyCurriculum and not worked on yet).
                // In this case, just fill in all user responses with "X" for M/C and T/F question types
                // and with blank for all others

                // also do this if the test version in the TestDefinition record is different from the
                // version recorded with the answers so far in the Test record. This will "reset" the
                // user's answers to unanswered status so it matches the new test questions.
                if (oCart.Score == 0)
                {
                    // incomplete test that does not match the current test definition version number
                    // set to default X and blank answers
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                        {
                            QTIQuestion.cUserAnswer = "X";
                        }
                        else
                        {
                            QTIQuestion.cUserAnswer = "";
                        }
                    }
                }
                else
                {
                    // incomplete test that matches the current test definition version number
                    // so it may have some user responses stored. Grab them from the test XMLTest field
                    // and populate them here.
                    List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oCart.XmlTest);
                    String[] Answers = UserAnswers.ToArray();
                    int iLoop = 0;
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        QTIQuestion.cUserAnswer = Answers[iLoop];
                        // be sure we're not empty -- if so, set to X
                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                            QTIQuestion.cUserAnswer = "X";
                        iLoop++;
                    }
                }
            }

            return QTIQuestionObjects;
        }
        /// <summary>
        /// Get enrollments
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="facilityid"></param>
        /// <param name="deptid"></param>
        /// <param name="topicid"></param>
        /// <param name="startrow"></param>
        /// <param name="maximumrows"></param>
        /// <param name="sortexpression"></param>
        /// <param name="numResults"></param>
        /// <returns></returns>
        public static DataSet GetUserEnrollmentBySearchCriteria(int uniqueid, string firstname, string lastname, int facilityid, int deptid, int topicid,
           int startrow, int maximumrows,string status, string sortexpression, ref int numResults)
        {
            if (sortexpression == null)
                sortexpression = "";

            if (sortexpression.Length == 0)
                sortexpression = "cLastName";

            return SiteProvider.PR2.GetUserEnrollmentsBySearechCriteria(uniqueid,firstname, lastname, facilityid, deptid, topicid,
            startrow, maximumrows, status,sortexpression, ref numResults);
        }


        /// <summary>
        /// Returns a list of test Survey question objects for the specified TestID
        /// Bhaskar N
        /// </summary>
        public static List<SQTIQuestionObject> GetSurveyQuestionObjectListByTestID(int TestID)
        {
            SurveyDefinition oSurveyDefinition = null;
            SQTIUtils oSQTIUtils = new SQTIUtils();

            // NOTE: No caching of these items, because they contain user responses
            // along with test questions
            List<SQTIQuestionObject> SQTIQuestionObjects = null;

            // first, get the test record so we have access to topicid and user's responses
            Test oTest = Test.GetTestByID(TestID);
            //oTest.TopicID = 2; //bsk test

            if (oTest != null)
            {
                // get the test definition record for this topic                
                oSurveyDefinition = SurveyDefinition.GetSurveyDefinitionByTopicID(oTest.TopicID);

                if (oSurveyDefinition != null)
                {
                    // get the QTIQuestionObjectList from the TestDefinition XML                                    
                    SQTIQuestionObjects = oSQTIUtils.ConvertQTITestXMLToSQTIQuestionObjectList(oSurveyDefinition.XMLSurvey);
                }
            }
            return SQTIQuestionObjects;
        }



        #endregion
    }
}
