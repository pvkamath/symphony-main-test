﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for SalesRep
/// </summary>
    public class SalesRep : BasePR
    {
        #region Variables and Properties

        private int _RepID = 0;
        public int RepID
        {
            get { return _RepID; }
            protected set { _RepID = value; }
        }

        private string _RepCode = "";
        public string RepCode
        {
            get { return _RepCode; }
            set { _RepCode = value; }
        }

        private string _RepName = "";
        public string RepName
        {
            get { return _RepName; }
            set { _RepName = value; }
        }

        private string _RepComment = "";
        public string RepComment
        {
            get { return _RepComment; }
            set { _RepComment = value; }
        }


        public SalesRep(int RepID, string RepCode, string RepName, string RepComment)
        {
            this.RepID = RepID;
            this.RepCode = RepCode;
            this.RepName = RepName;
            this.RepComment = RepComment;
        }

        public bool Delete()
        {
            bool success = SalesRep.DeleteSalesRep(this.RepID);
            if (success)
                this.RepID = 0;
            return success;
        }

        public bool Update()
        {
            return SalesRep.UpdateSalesRep(this.RepID, this.RepCode, this.RepName, this.RepComment);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all SalesReps
        /// </summary>
        public static List<SalesRep> GetSalesReps(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "RepName";

            List<SalesRep> SalesReps = null;
            string key = "SalesReps_SalesReps_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SalesReps = (List<SalesRep>)BizObject.Cache[key];
            }
            else
            {
                List<SalesRepInfo> recordset = SiteProvider.PR2.GetSalesReps(cSortExpression);
                SalesReps = GetSalesRepListFromSalesRepInfoList(recordset);
                BasePR.CacheData(key, SalesReps);
            }
            return SalesReps;
        }


        /// <summary>
        /// Returns the number of total SalesReps
        /// </summary>
        public static int GetSalesRepCount()
        {
            int SalesRepCount = 0;
            string key = "SalesReps_SalesRepCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SalesRepCount = (int)BizObject.Cache[key];
            }
            else
            {
                SalesRepCount = SiteProvider.PR2.GetSalesRepCount();
                BasePR.CacheData(key, SalesRepCount);
            }
            return SalesRepCount;
        }

        /// <summary>
        /// Returns a SalesRep object with the specified ID
        /// </summary>
        public static SalesRep GetSalesRepByID(int RepID)
        {
            SalesRep SalesRep = null;
            string key = "SalesReps_SalesRep_" + RepID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SalesRep = (SalesRep)BizObject.Cache[key];
            }
            else
            {
                SalesRep = GetSalesRepFromSalesRepInfo(SiteProvider.PR2.GetSalesRepByID(RepID));
                BasePR.CacheData(key, SalesRep);
            }
            return SalesRep;
        }

        /// <summary>
        /// Updates an existing SalesRep
        /// </summary>
        public static bool UpdateSalesRep(int RepID, string RepCode, string RepName, string RepComment)
        {
            RepCode = BizObject.ConvertNullToEmptyString(RepCode);
            RepName = BizObject.ConvertNullToEmptyString(RepName);
            RepComment = BizObject.ConvertNullToEmptyString(RepComment);


            SalesRepInfo record = new SalesRepInfo(RepID, RepCode, RepName, RepComment);
            bool ret = SiteProvider.PR2.UpdateSalesRep(record);

            BizObject.PurgeCacheItems("SalesReps_SalesRep_" + RepID.ToString());
            BizObject.PurgeCacheItems("SalesReps_SalesReps");
            return ret;
        }

        /// <summary>
        /// Creates a new SalesRep
        /// </summary>
        public static int InsertSalesRep(string RepCode, string RepName, string RepComment)
        {
            RepCode = BizObject.ConvertNullToEmptyString(RepCode);
            RepName = BizObject.ConvertNullToEmptyString(RepName);
            RepComment = BizObject.ConvertNullToEmptyString(RepComment);


            SalesRepInfo record = new SalesRepInfo(0, RepCode, RepName, RepComment);
            int ret = SiteProvider.PR2.InsertSalesRep(record);

            BizObject.PurgeCacheItems("SalesReps_SalesRep");
            return ret;
        }

        /// <summary>
        /// Deletes an existing SalesRep, but first checks if OK to delete
        /// </summary>
        public static bool DeleteSalesRep(int RepID)
        {
            bool IsOKToDelete = OKToDelete(RepID);
            if (IsOKToDelete)
            {
                return (bool)DeleteSalesRep(RepID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing SalesRep - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteSalesRep(int RepID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteSalesRep(RepID);
            //         new RecordDeletedEvent("SalesRep", RepID, null).Raise();
            BizObject.PurgeCacheItems("SalesReps_SalesRep");
            return ret;
        }



        /// <summary>
        /// Checks to see if a SalesRep can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int RepID)
        {
            return true;
        }



        /// <summary>
        /// Returns a SalesRep object filled with the data taken from the input SalesRepInfo
        /// </summary>
        private static SalesRep GetSalesRepFromSalesRepInfo(SalesRepInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new SalesRep(record.RepID, record.RepCode, record.RepName, record.RepComment);
            }
        }

        /// <summary>
        /// Returns a list of SalesRep objects filled with the data taken from the input list of SalesRepInfo
        /// </summary>
        private static List<SalesRep> GetSalesRepListFromSalesRepInfoList(List<SalesRepInfo> recordset)
        {
            List<SalesRep> SalesReps = new List<SalesRep>();
            foreach (SalesRepInfo record in recordset)
                SalesReps.Add(GetSalesRepFromSalesRepInfo(record));
            return SalesReps;
        }
        #endregion
    }
}