﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 
    /// <summary>
    /// Summary description for NewGiftcard
    /// </summary>
    public class NewGiftcard: BasePR
    {
        #region Variables and Properties
        private int _GcID = 0;
        public int GcID
        {
            get { return _GcID; }
            protected set { _GcID = value; }
        }

        private string _GcNumber = "";

        public string GcNumber
        {
            get { return _GcNumber; }
            set { _GcNumber = value; }
        }

        private int _GiftCardCHour = 0;

        public int GiftCardCHour
        {
            get { return _GiftCardCHour; }
            set { _GiftCardCHour = value; }
        }

        private int _FacilityID = 0;

        public int FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }

        private string _FacilityName = "";

        public string FacilityName
        {
            get { return _FacilityName; }
            set { _FacilityName = value; }
        }

        public NewGiftcard(int GcID, string GcNumber, int GiftCardCHour,
          int FacilityID, string FacilityName)
        {
            this.GcID = GcID;
            this.GcNumber = GcNumber;
            this.GiftCardCHour = GiftCardCHour;
            this.FacilityID = FacilityID;
            this.FacilityName = FacilityName;

        }

        #endregion

        #region Methods

        /// <summary>
        /// Insert a batch of TopicDetail records from a List
        /// </summary>
        /// <summary>
        /// Creates a new LicenseType
        /// </summary>
        /// <summary>
        /// Creates a new Test
        /// </summary>
        public static int InsertNewGiftcard(int GcID, string GcNumber, int GiftCardCHour,
          int FacilityID, string FacilityName)
        {
            //GcID = BizObject.ConvertNullToEmptyString(GcID);
            GcNumber = BizObject.ConvertNullToEmptyString(GcNumber);
            //GiftCardCHour = BizObject.ConvertNullToEmptyString(GiftCardCHour);
            //FacilityID = BizObject.ConvertNullToEmptyString(FacilityID);
            FacilityName = BizObject.ConvertNullToEmptyString(FacilityName);

            NewGiftcardInfo record = new NewGiftcardInfo(0, GcNumber, GiftCardCHour, FacilityID, FacilityName);
            int ret = SiteProvider.PR2.InsertNewGiftcard(record);

            BizObject.PurgeCacheItems("NewGiftcards_NewGiftcard");
            return ret;
        }

        public static NewGiftcard GetNewGiftcardInfoByGcNumber(string GcNumber, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "gcnumber DESC";

            List<NewGiftcard> NewGiftcards = null;
            //string key = "Tests_Tests_" + UserID.ToString() + cSortExpression.ToString();

            //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            //{
            //    Tests = (List<Test>)BizObject.Cache[key];
            //}
            //else
            //{
            List<NewGiftcardInfo> recordset = SiteProvider.PR2.GetNewGiftcardByGcNumber(GcNumber);
            NewGiftcards = GetNewGiftcardInfoListFromGetNewGiftcardInfoList(recordset);
            //BasePR.CacheData(key, Tests);

            //}
            if (NewGiftcards.Count > 0)
            {
                return NewGiftcards[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns a list of Test objects filled with the data taken from the input list of TestInfo
        /// </summary>
        private static List<NewGiftcard> GetNewGiftcardInfoListFromGetNewGiftcardInfoList(List<NewGiftcardInfo> recordset)
        {
            List<NewGiftcard> NewGiftcards = new List<NewGiftcard>();
            foreach (NewGiftcardInfo record in recordset)
                NewGiftcards.Add(GetNewGiftcardFromNewGiftcardInfo(record));
            return NewGiftcards;
        }


        /// <summary>
        /// Returns a Test object filled with the data taken from the input TestInfo
        /// </summary>
        private static NewGiftcard GetNewGiftcardFromNewGiftcardInfo(NewGiftcardInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new NewGiftcard(record.GcID, record.GcNumber, record.GiftCardCHour, record.FacilityID, record.FacilityName);
            }
        }

        #endregion
    }
}