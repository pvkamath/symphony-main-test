﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for Discount
    /// </summary>
    public class Discount : BasePR
    {
        #region Variables and Properties
       

        private int _discountid = 0;
        public int DiscountId
        {
            get { return _discountid; }
            protected set { _discountid = value; }
        }
        private int _facilityid = 0;
        public int FacilityId
        {
            get { return _facilityid; }
            protected set { _facilityid = value; }
        }
        private int _topicid = 0;
        public int TopicId
        {
            get { return _topicid; }
            protected set { _topicid = value; }
        }

        private string _course_number = "";
        public string course_number
        {
            get { return _course_number; }
            private set { _course_number = value; }
        }
        private string _coursename = "";
        public string coursename
        {
            get { return _coursename; }
            private set { _coursename = value; }
        }
        private string _tagline = "";
        public string TagLine
        {
            get { return _tagline; }
            private set { _tagline = value; }
        }

        private DateTime _startdate = System.DateTime.Now;
        public DateTime StartDate
        {
            get { return _startdate; }
            private set { _startdate = value; }
        }

        private DateTime _enddate = System.DateTime.Now;
        public DateTime EndDate
        {
            get { return _enddate; }
            private set { _enddate = value; }
        }


        private decimal _discount_price = 0.00M;
        public decimal discount_price
        {
            get { return _discount_price; }
            private set { _discount_price = value; }
        }

        private string _discount_type = "";
        public string discount_type
        {
            get { return _discount_type; }
            private set { _discount_type = value; }
        }

        private string _sponsor_text = "";
        public string Sponsor_Text
        {
            get { return _sponsor_text; }
            private set { _sponsor_text = value; }
        }

        private string _free_html = "";
        public string Free_Html
        {
            get { return _free_html; }
            private set { _free_html = value; }
        }

        private string _img_file = "";
        public string Img_File
        {
            get { return _img_file; }
            private set { _img_file = value; }
        }

        private string _discount_url = "";
        public string Discount_Url
        {
            get { return _discount_url; }
            private set { _discount_url = value; }
        }

        private bool _survey_ind = true;
        public bool Survey_Ind
        {
            get { return _survey_ind; }
            private set { _survey_ind = value; }
        }

        private int _impression = 0;
        public int Impression
        {
            get { return _impression; }
            protected set { _impression = value; }
        }

         int _clickthrough = 0;
        public int ClickThrough
        {
            get { return _clickthrough; }
             set { _clickthrough = value; }
        }

        private string _username = "";
        public string UserName
        {
            get { return _username; }
            private set { _username = value; }
        }

        private string _userpassword = "";
        public string UserPassword
        {
            get { return _userpassword; }
            private set { _userpassword = value; }
        }


        private bool _section_ind = true;
        public bool Section_Ind
        {
            get { return _section_ind; }
            private set { _section_ind = value; }
        }


        //decimal(6, 2)..promotionsifo
        private decimal _offline_price = 0.00M;
        public decimal Offline_Price
        {
            get { return _offline_price; }
            private set { _offline_price = value; }
        }

        private bool _virtualurl_ind = true;
        public bool Virtualurl_Ind
        {
            get { return _virtualurl_ind; }
             set { _virtualurl_ind = value; }
        }
        private int _initid = 0;
        public int InitID
        {
            get { return _initid; }
            private set { _initid = value; }
        }
        private string _sponsor_ad_html = "";
        public string Sponsor_Ad_Html
        {
            get { return _sponsor_ad_html; }
            private set { _sponsor_ad_html = value; }
        }



        public Discount(int discountid, int facilityid, int topicid, string course_number, string coursename, string tagline, DateTime startdate, DateTime enddate, decimal discount_price,
           string discount_type, string sponsor_text, string free_html, string img_file, string discount_url, bool survey_ind, int impression,
           int clickthrough, string username, string userpassword, bool section_ind, decimal offline_price, int InitID, bool virtualurl_ind, string sponsor_ad_html)
        {
            this.DiscountId = discountid;
            this.FacilityId = facilityid;
            this.TopicId = topicid;
            this.course_number = course_number;
            this.coursename = coursename;
            this.TagLine = tagline;
            this.StartDate = startdate;
            this.EndDate = enddate;
            this.discount_price = discount_price;
            this.discount_type = discount_type;
            this.Sponsor_Text = sponsor_text;
            this.Free_Html = free_html;
            this.Img_File = img_file;
            this.Discount_Url = discount_url;
            this.Survey_Ind = survey_ind;
            this.Impression = impression;
            this.ClickThrough = clickthrough;
            this.UserName = username;
            this.UserPassword = userpassword;
            this.Section_Ind = section_ind;
            this.Offline_Price = offline_price;
            this.InitID = InitID;
            this.Virtualurl_Ind = virtualurl_ind;
            this.Sponsor_Ad_Html = sponsor_ad_html;
        }


        public bool Delete()
        {
            bool success = Discount.DeleteDiscount(this.DiscountId);
            if (success)
                this.DiscountId = 0;
            return success;
        }

        public bool Update()
        {
            return Discount.UpdateDiscount(this.DiscountId, this.FacilityId, this.TopicId, this.TagLine, this.StartDate, this.EndDate, this.discount_price,
           this.discount_type, this.Sponsor_Text, this.Free_Html, this.Img_File, this.Discount_Url, this.Survey_Ind, this.Impression,
           this.ClickThrough, this.UserName, this.UserPassword, this.Section_Ind, this.Offline_Price, this.InitID, this.Virtualurl_Ind,this.Sponsor_Ad_Html);
        }

        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all Discounts
        //</summary>
        public static List<Discount> GetDiscounts(string cSortExpression, int Active)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "course_number";

            List<Discount> Discounts = null;
            string key = "Discounts_Discounts_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Discounts = (List<Discount>)BizObject.Cache[key];
            }
            else
            {
                List<DiscountInfo> recordset = SiteProvider.PR2.GetDiscounts(cSortExpression, Active);
                Discounts = GetDiscountListFromDiscountInfoList(recordset);
                BasePR.CacheData(key, Discounts);
            }
            return Discounts;
        }

        /// <summary>
        /// Returns a Discount object with the specified ID
        /// </summary>
        public static Discount GetDiscountByID(int DiscountId)
        {
            Discount Discount = null;
            string key = "Discounts_Discount_" + DiscountId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Discount = (Discount)BizObject.Cache[key];
            }
            else
            {
                Discount = GetDiscountFromDiscountInfo(SiteProvider.PR2.GetDiscountByID(DiscountId));
                BasePR.CacheData(key, Discount);
            }
            return Discount;
        }

        /// <summary>
        /// Returns a Discount object with the specified ID
        /// </summary>
        public static Discount GetDiscountByUserNameAndPassword(string UserName, string UserPassword)
        {
            Discount Discount = null;
            string key = "Discounts_Discount_" + UserName.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Discount = (Discount)BizObject.Cache[key];
            }
            else
            {
                Discount = GetDiscountFromDiscountInfo(SiteProvider.PR2.GetDiscountByUserNameAndPassword(UserName, UserPassword));
                BasePR.CacheData(key, Discount);
            }
            return Discount;
        }


        public static DataSet GetDiscountByTopicID(string TopicID, int UrlIdentity)
        {
            PearlsReview.DAL.SQLClient.SQL2PRProvider obj = new PearlsReview.DAL.SQLClient.SQL2PRProvider();
            return obj.GetDiscountByTopicID(TopicID, UrlIdentity);
        }

        public static DataTable GetDiscountBySponsorID(string SponsorID, string cSortExpression)
        {
            if (cSortExpression.Length == 0)
                cSortExpression = " Tagline ";

            PearlsReview.DAL.SQLClient.SQL2PRProvider obj = new PearlsReview.DAL.SQLClient.SQL2PRProvider();
            return obj.GetDiscountBySponsorID(SponsorID, cSortExpression);
        }
        public static DataSet GetDiscountByInitiativeIDs(string initIDs, string cSortExpression)
        {
            if (cSortExpression.Length == 0)
                cSortExpression = " Tagline ";

            PearlsReview.DAL.SQLClient.SQL2PRProvider obj = new PearlsReview.DAL.SQLClient.SQL2PRProvider();
            return obj.GetDiscountByInitiativeIDs(initIDs, cSortExpression);
        }
        //<summary>
        //Returns a collection with all Discounts
        //</summary>
        public static List<Discount> GetSponsorDiscounts(string cSortExpression, int SponsorID)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "course_number";

            List<Discount> Discounts = null;
            string key = "Discounts_Sponsor_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Discounts = (List<Discount>)BizObject.Cache[key];
            }
            else
            {
                List<DiscountInfo> recordset = SiteProvider.PR2.GetSponsorDiscounts(cSortExpression, SponsorID);
                Discounts = GetDiscountListFromDiscountInfoList(recordset);
                BasePR.CacheData(key, Discounts);
            }
            return Discounts;
        }

        public static void UpdateUCEIndicatorByTopicid(bool hide, int topicid)
        {
            SiteProvider.PR2.UpdateUCEIndicatorByTopicid(hide,topicid);
        }

        public static List<Discount> GetSponsorDiscountsByInitiativeID(string cSortExpression, int InitID)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "course_number";

            List<Discount> Discounts = null;
            string key = "Discounts_Sponsor_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Discounts = (List<Discount>)BizObject.Cache[key];
            }
            else
            {
                List<DiscountInfo> recordset = SiteProvider.PR2.GetSponsorDiscountsByInitiativeID(cSortExpression, InitID);
                Discounts = GetDiscountListFromDiscountInfoList(recordset);
                BasePR.CacheData(key, Discounts);
            }
            return Discounts;
        }
        public static DataSet GetDiscountByTopicID(string TopicID, int UrlIdentity, string UserName)
        {
          
            return SiteProvider.PR2.GetDiscountByTopicID(TopicID, UrlIdentity, UserName);
        }

        /// <summary>
        /// Returns a Discount object with the specified ID
        /// </summary>
        public static Discount GetDiscountByTopicIdAndMicroSiteId(int TopicId, int MicroSiteId)
        {
            Discount Discount = null;
            string key = "Discounts_Discount_" + TopicId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Discount = (Discount)BizObject.Cache[key];
            }
            else
            {
                Discount = GetDiscountFromDiscountInfo(SiteProvider.PR2.GetDiscountByTopicIdAndMicroSiteId(TopicId,MicroSiteId));
                BasePR.CacheData(key, Discount);
            }
            return Discount;
        }

        /// <summary>
        /// Creates a new Discount
        /// </summary>
        public static int InsertDiscount(int facilityid, int topicid, string tagline, DateTime startdate, DateTime enddate, decimal discount_price,
           string discount_type, string sponsor_text, string free_html, string img_file, string discount_url, bool survey_ind, int impression,
           int clickthrough, string username, string userpassword, bool section_ind, decimal offline_price, int InitID, bool virtualurl_ind, string sponsor_ad_html)
        {
            tagline = BizObject.ConvertNullToEmptyString(tagline);
            discount_type = BizObject.ConvertNullToEmptyString(discount_type);
            sponsor_text = BizObject.ConvertNullToEmptyString(sponsor_text);
            free_html = BizObject.ConvertNullToEmptyString(free_html);
            img_file = BizObject.ConvertNullToEmptyString(img_file);
            discount_url = BizObject.ConvertNullToEmptyString(discount_url);
            username = BizObject.ConvertNullToEmptyString(username);
            userpassword = BizObject.ConvertNullToEmptyString(userpassword);
            sponsor_ad_html = BizObject.ConvertNullToEmptyString(sponsor_ad_html);

            DiscountInfo record = new DiscountInfo(0, facilityid, topicid, "", "", tagline, startdate, enddate, discount_price,
            discount_type, sponsor_text, free_html, img_file, discount_url, survey_ind, impression,
            clickthrough, username, userpassword, section_ind, offline_price, InitID, virtualurl_ind,sponsor_ad_html);
            int ret = SiteProvider.PR2.InsertDiscount(record);

            BizObject.PurgeCacheItems("Discounts_Discount");
            return ret;
        }


        /// <summary>
        /// Updates an existing Discount
        /// </summary>
        public static bool UpdateDiscount(int discountid, int facilityid, int topicid, string tagline, DateTime startdate, DateTime enddate, decimal discount_price,
           string discount_type, string sponsor_text, string free_html, string img_file, string discount_url, bool survey_ind, int impression,
           int clickthrough, string username, string userpassword, bool section_ind, decimal offline_price, int InitID, bool virtualurl_ind, string sponsor_ad_html)
        {
            tagline = BizObject.ConvertNullToEmptyString(tagline);
            discount_type = BizObject.ConvertNullToEmptyString(discount_type);
            sponsor_text = BizObject.ConvertNullToEmptyString(sponsor_text);
            free_html = BizObject.ConvertNullToEmptyString(free_html);
            img_file = BizObject.ConvertNullToEmptyString(img_file);
            discount_url = BizObject.ConvertNullToEmptyString(discount_url);
            username = BizObject.ConvertNullToEmptyString(username);
            userpassword = BizObject.ConvertNullToEmptyString(userpassword);
            sponsor_ad_html = BizObject.ConvertNullToEmptyString(sponsor_ad_html);

            //if (virtualurl_ind == true)
            //{
            //    section_ind = false;
            //}
            //else
            //{
            //    if (section_ind == true)
            //    {
            //        section_ind = true;
            //    }
            //    else
            //    {
            //        section_ind = false;
            //    }
            //}

            DiscountInfo record = new DiscountInfo(discountid, facilityid, topicid, "", "", tagline, startdate, enddate, discount_price,
            discount_type, sponsor_text, free_html, img_file, discount_url, survey_ind, impression,
            clickthrough, username, userpassword, section_ind, offline_price, InitID, virtualurl_ind,sponsor_ad_html);
            bool ret = SiteProvider.PR2.UpdateDiscount(record);

            BizObject.PurgeCacheItems("Discounts_Discount_" + discountid.ToString());
            BizObject.PurgeCacheItems("Discounts_Discounts");
            return ret;
        }

        /// <summary>
        /// Updates an existing Discount (Impression)
        /// </summary>
        public static bool UpdateDiscountByImpression(int Discountid, int Impression)
        {
            bool ret = SiteProvider.PR2.UpdateDiscountByImpression(Discountid, Impression);
            return ret;
        }

        /// <summary>
        /// Updates an existing Discount (Impression)
        /// </summary>
        public static bool UpdateDiscountByClickThrough(int Discountid, int ClickThrough)
        {
            bool ret = SiteProvider.PR2.UpdateDiscountByClickThrough(Discountid, ClickThrough);
            return ret;
        }
        public static bool UpdateInitiativeField(int DiscountId, int InitID)
        {
            bool ret = SiteProvider.PR2.UpdateInitiativeField(DiscountId, InitID);
            return ret;
        }
        /// <summary>
        /// Deletes an existing Discount, but first checks if OK to delete
        /// </summary>
        public static bool DeleteDiscount(int DiscountId)
        {
            bool IsOKToDelete = OKToDelete(DiscountId);
            if (IsOKToDelete)
            {
                return (bool)DeleteDiscount(DiscountId, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Discount - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteDiscount(int DiscountId, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteDiscount(DiscountId);
            BizObject.PurgeCacheItems("Discount_Discount");
            return ret;
        }

        /// <summary>
        /// Checks to see if a Discount can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }

        /// <summary>
        /// Get Discount Id based On Topic Id
        /// </summary>
        public static int GetDiscountIdByTopicId(int TopicId)
        {
            int ret = SiteProvider.PR2.GetDiscountIdByTopicId(TopicId);
            return ret;
        }

        public static bool GetUCEIndcatorByTopicID(int Discountid)
        {
            PearlsReview.DAL.SQLClient.SQL2PRProvider obj = new PearlsReview.DAL.SQLClient.SQL2PRProvider();
            return obj.GetUCEIndcatorByTopicID(Discountid);
        }




        /// <summary>
        /// Returns a Discount object filled with the data taken from the input DiscountInfo
        /// </summary>
        private static Discount GetDiscountFromDiscountInfo(DiscountInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Discount(record.DiscountId, record.FacilityId, record.TopicId, record.course_number, record.coursename, record.TagLine, record.StartDate, record.EndDate, record.discount_price,
                    record.discount_type, record.Sponsor_Text, record.Free_Html, record.Img_File, record.Discount_Url, record.Survey_Ind, record.Impression,
                    record.ClickThrough, record.UserName, record.UserPassword, record.Section_Ind, record.Offline_Price, record.InitID, record.Virtualurl_Ind,record.Sponsor_Ad_Html);
            }
        }

        /// <summary>
        /// Returns a list of Discount objects filled with the data taken from the input list of DiscountInfo
        /// </summary>
        private static List<Discount> GetDiscountListFromDiscountInfoList(List<DiscountInfo> recordset)
        {
            List<Discount> Discounts = new List<Discount>();
            foreach (DiscountInfo record in recordset)
                Discounts.Add(GetDiscountFromDiscountInfo(record));
            return Discounts;
        }

        public static int CheckDiscountWithTopicId(int discountId, int TopicId, bool section_ind, bool virtualurl_ind)
        {
            return SiteProvider.PR2.CheckDiscountWithTopicId(discountId, TopicId, section_ind, virtualurl_ind);
        }

        #endregion
    }
}
