﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using System.Collections.Generic;
using PearlsReview.QTI;

/// <summary>
/// Summary description for Cart
/// </summary>
namespace PearlsReview.BLL
{ 
	public class Cart:BasePR
    {
        #region Variables and Properties
        private int _CartID = 0;
        public int CartID
        {
            get { return _CartID; }
            set { _CartID = value; }
        }
        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }
        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _MediaType = "";
        public string MediaType
        {
            get { return _MediaType; }
            set { _MediaType = value; }
        }
        private int _Score = 0;
        public int Score
        {
            get { return _Score; }
            set { _Score = value; }
        }
        private DateTime _Lastmod = System.DateTime.Now;
        public DateTime Lastmod
        {
            get { return _Lastmod; }
            set { _Lastmod = value; }
        }
        private string _XmlTest = "";
        public string XmlTest
        {
            get { return _XmlTest; }
            set { _XmlTest = value; }
        }
        private string _XmlSurvey = "";
        public string XmlSurvey
        {
            get { return _XmlSurvey; }
            set { _XmlSurvey = value; }
        }
        private int _Quantity = 0;
        public int Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        private int _DiscountId = 0;
        public int DiscountId
        {
            get { return _DiscountId; }
            set { _DiscountId = value; }
        }

        private bool _Process_Ind = false;
        public bool Process_Ind
        {
            get { return _Process_Ind; }
            set { _Process_Ind = value; }
        }
        private bool _Option_Id = false;
        public bool Option_Id
        {
            get { return _Option_Id; }
            set { _Option_Id = value; }
        }
        private int _facilityId = 0;
        public int facilityId
        {
            get { return _facilityId; }
            set { _facilityId = value; }

        }
        private bool _cart_prepaid = false;
        public bool cart_prepaid
        {
            get { return _cart_prepaid; }
            set { _cart_prepaid = value; }
        }
        
        public Cart(int CartID, int TopicID, int UserID, string MediaType, int Score, DateTime Lastmod, string XmlTest,
            string XmlSurvey, int Quantity, int DiscountId, bool Process_Ind, bool Option_Id, int facilityId,bool cart_prepaid)
        {
            this.CartID = CartID;
            this.TopicID = TopicID;
            this.UserID = UserID;
            this.MediaType = MediaType;
            this.Score = Score;
            this.Lastmod = Lastmod;
            this.XmlTest = XmlTest;
            this.XmlSurvey = XmlSurvey;
            this.Quantity = Quantity;
            this.DiscountId = DiscountId;
            this.Process_Ind = Process_Ind;
            this.Option_Id = Option_Id;
            this.facilityId = facilityId;
            this.cart_prepaid = cart_prepaid;
        }
   

        #endregion

        #region Methods

        /// <summary>
        /// Creates a new cart
        /// </summary>
        public static int InsertCart(int CartID, int TopicID, int UserID, string MediaType, int Score, DateTime Lastmod, string XmlTest,
            string XmlSurvey, int Quantity, int DiscountId, bool Process_Ind, bool Option_Id, int facilityId,bool cart_prepaid)
        {
            MediaType = BizObject.ConvertNullToEmptyString(MediaType);
            XmlTest = BizObject.ConvertNullToEmptyString(XmlTest);
            XmlSurvey = BizObject.ConvertNullToEmptyString(XmlSurvey);

            CartInfo record = new CartInfo(0, TopicID, UserID, MediaType, Score, Lastmod, XmlTest,
                XmlSurvey, Quantity, DiscountId, Process_Ind, Option_Id, facilityId,cart_prepaid);

            int ret = SiteProvider.PR2.InsertCart(record);
            BizObject.PurgeCacheItems("Carts_Cart");
            return ret;
        }


        /// <summary>
        /// Updates an existing Test
        /// </summary>
        public static bool UpdateCart(int CartID, int TopicID, int UserID, string MediaType, int Score, DateTime Lastmod, string XmlTest,
            string XmlSurvey, int Quantity, int DiscountId, bool Process_Ind, bool Option_Id,int facilityId,bool cart_prepaid)
        {
            MediaType = BizObject.ConvertNullToEmptyString(MediaType);
            XmlTest = BizObject.ConvertNullToEmptyString(XmlTest);
            XmlSurvey = BizObject.ConvertNullToEmptyString(XmlSurvey);

            CartInfo record = new CartInfo(CartID, TopicID, UserID, MediaType, Score, Lastmod, XmlTest,
                XmlSurvey, Quantity, DiscountId, Process_Ind, Option_Id, facilityId,cart_prepaid);
            bool ret = SiteProvider.PR2.UpdateCart(record);

            BizObject.PurgeCacheItems("Carts_Cart" + CartID.ToString());
            BizObject.PurgeCacheItems("Carts_Cart");
            return ret;
        }

        /// <summary>
        /// Returns a collection with all Carts for the specified UserID
        /// </summary>
        public static List<Cart> GetCartsByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            List<Cart> Carts = null;
            List<CartInfo> recordset = SiteProvider.PR2.GetCartsByUserID(UserID);
            Carts = GetCartListFromCartInfoList(recordset);

            return Carts;
        }

        /// <summary>
        /// Returns a collection with all Carts for the specified UserID
        /// </summary>
        public static List<Cart> GetCartsByUserIDAndDomainId(int UserID, int DomainId,string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastMod DESC";

            List<Cart> Carts = null;
            List<CartInfo> recordset = SiteProvider.PR2.GetCartsByUserIDAndDomainId(UserID,DomainId,cSortExpression);
            Carts = GetCartListFromCartInfoList(recordset);

            return Carts;
        }


        ///<summary>
        /// updates the quantity based on cartid
        ///</summary>
        ///
        public static Boolean UpdateCartQByCartId(int Cartid, int Quantity)
        {
            Cart objCart = GetCartByID(Cartid);
            Boolean ret = UpdateCart(Cartid, objCart.TopicID, objCart.UserID, objCart.MediaType,
                objCart.Score, objCart.Lastmod, objCart.XmlTest, objCart.XmlSurvey, Quantity,
                objCart.DiscountId, objCart.Process_Ind, objCart.Option_Id,objCart.facilityId,objCart.cart_prepaid);

            return ret;

        }


        /// <summary>
        /// Returns a Cart object filled with the data taken from the input CartInfo
        /// </summary>
        private static Cart GetCartFromCartInfo(CartInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Cart(record.CartID, record.TopicID, record.UserID, record.MediaType,
                record.Score, record.Lastmod, record.XmlTest, record.XmlSurvey, record.Quantity, record.DiscountId, record.Process_Ind,
                record.Option_Id,record.facilityId,record.cart_prepaid);
            }
        }

        /// <summary>
        /// Returns a list of Cart objects filled with the data taken from the input list of CartInfo
        /// </summary>
        private static List<Cart> GetCartListFromCartInfoList(List<CartInfo> recordset)
        {
            List<Cart> Carts = new List<Cart>();
            foreach (CartInfo record in recordset)
                Carts.Add(GetCartFromCartInfo(record));
            return Carts;
        }

        /// <summary>
        /// Returns a collection with Incomplete Carts for the specified UserID and TopicID
        /// </summary>
        public static List<Cart> GetIncompleteCartsByUserIDAndTopicID(int UserID, int TopicID)
        {

            List<Cart> Carts = null;
            List<CartInfo> recordset = SiteProvider.PR2.GetIncompleteCartsByUserIDAndTopicID(UserID, TopicID);
            Carts = GetCartListFromCartInfoList(recordset);

            return Carts;
        }

        public static Cart GetIncompleteCartByUserIDAndTopicIDAndMediaType(
            int userID, int topicID, string mediaType, int DomainID)
        {

            Cart cart = null;
            CartInfo recordset = 
                SiteProvider.PR2.GetIncompleteCartByUserIDAndTopicIDAndMediaType(userID, topicID, mediaType, DomainID);
            cart = GetCartFromCartInfo(recordset);

            return cart;
        }

        /// <summary>
        /// Returns a collection with Incomplete Carts for the specified UserID and TopicID
        /// </summary>
        public static List<Cart> GetcompleteCartsByUserIDAndTopicID(int UserID, int TopicID)
        {

            List<Cart> Carts = null;
            List<CartInfo> recordset = SiteProvider.PR2.GetcompleteCartsByUserIDAndTopicID(UserID, TopicID);
            Carts = GetCartListFromCartInfoList(recordset);

            return Carts;
        }

        /// <summary>
        /// Returns a collection with Incomplete Carts for the specified UserID and DomainId
        /// </summary>
        public static List<Cart> GetInCompleteCartsByUserIdAndDomainId(int UserId, int DomainId)
        {
            List<CartInfo> recordset = SiteProvider.PR2.GetInCompleteCartsByUserIdAndDomainId(UserId, DomainId);
            List<Cart> Carts = null;
            Carts = GetCartListFromCartInfoList(recordset);
            return Carts;
        }

        public static List<Cart> GetTop5InCompleteCartsByUserId(int UserId)
        {
            List<CartInfo> recordset = SiteProvider.PR2.GetTop5InCompleteCartsByUserId(UserId);
            List<Cart> Carts = null;
            Carts = GetCartListFromCartInfoList(recordset);
            return Carts;
        }

        /// <summary>
        /// Returns a Cart object with the specified ID
        /// </summary>
        public static Cart GetCartByID(int CartID)
        {
            Cart Cart = null;
            string key = "Carts_Cart_" + CartID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Cart = (Cart)BizObject.Cache[key];
            }
            else
            {
                Cart = GetCartFromCartInfo(SiteProvider.PR2.GetCartByID(CartID));
                BasePR.CacheData(key, Cart);
            }
            return Cart;
        }


        public bool Update()
        {
            return Cart.UpdateCart(this.CartID, this.TopicID, this.UserID, this.MediaType, this.Score, this.Lastmod, this.XmlTest, this.XmlSurvey,
                this.Quantity, this.DiscountId, this.Process_Ind, this.Option_Id,this.facilityId,this.cart_prepaid);
        }

        /// <summary>
        /// Returns a Cart Price object with the specified ID
        /// </summary>
        public static DataSet GetCartPriceByUserId(int UserId)
        {
            DataSet dsCart = (SiteProvider.PR2.GetCartPriceByUserId(UserId));

            return dsCart;
        }

        /// <summary>
        /// Returns a Cart Price object with the specified UserID and DomainId
        /// </summary>
        public static DataSet GetCartPriceUserIdAndDomainId(int UserId, int DomainId)
        {
            DataSet dsCart = SiteProvider.PR2.GetCartPriceUserIdAndDomainId(UserId, DomainId);
            return dsCart;

        }

        /// <summary>
        /// Returns a Cart Price object with the specified UserID and DomainId without CEPRO
        /// </summary>
        public static DataSet GetCartPriceUserIdAndDomainIdWithoutCEPRO(int UserId, int DomainId)
        {
            DataSet dsCart = SiteProvider.PR2.GetCartPriceUserIdAndDomainIdWithoutCEPRO(UserId, DomainId);
            return dsCart;

        }

        public static decimal GetShipCostByUserIdAndDomainId(int UserId, int DomainId, bool Expedited)
        {
            return SiteProvider.PR2.GetShipCostByUserIdAndDomainId(UserId, DomainId, Expedited);

        }
        public static decimal GetTotalCEPROSavingByUseridDomainId(int UserId, int DomainId)
        {
            return SiteProvider.PR2.GetTotalCEPROSavingByUseridDomainId(UserId, DomainId);
        }
        public static decimal GetTotalCostByUseridDomainId(int UserId, int DomainId)
        {
            return SiteProvider.PR2.GetTotalCostByUseridDomainId(UserId, DomainId);
        }
        public static decimal GetTotalCostWithCouponByUseridDomainId(int UserId, int DomainId, decimal CouponAmount)
        {
            return SiteProvider.PR2.GetTotalCostWithCouponByUseridDomainId(UserId, DomainId, CouponAmount);
        }
        public static decimal GetSubTotalByUseridDomainIdWithoutCEPRO(int UserId, int DomainId, decimal CouponAmount)
        {
            return SiteProvider.PR2.GetSubTotalByUseridDomainIdWithoutCEPRO(UserId, DomainId, CouponAmount);
        }        
        /// <summary>
        /// Returns a Cart Price object with the specified ID
        /// </summary>
        public static DataSet GetUnlimCartPriceByUserId(int UserId)
        {
            DataSet dsCart = (SiteProvider.PR2.GetUnlimCartPriceByUserId(UserId));

            return dsCart;
        }


        public static DataSet GetUnlimCartPrice()
        {
            DataSet dsCart = (SiteProvider.PR2.GetUnlimCartPrice());

            return dsCart;
        }



        /// <summary>
        /// Returns a Cart Price object with the specified cart ID
        /// </summary>
        public static Decimal GetCartPriceByCartId(int CartId)
        {
            Decimal dPrice = (SiteProvider.PR2.GetCartPriceByCartId(CartId));

            return dPrice;
        }


        public static bool ISCarthasUnlim(int Userid, int DomainId)
        {
            return SiteProvider.PR2.ISCarthasUnlim(Userid, DomainId);
        }


        public static Decimal GetMSCartPriceByCartId(int CartId)
        {
            return SiteProvider.PR2.GetMSCartPriceByCartId(CartId);
        }

        /// <summary>
        /// Returns a Cart tax  with the specified User ID
        /// </summary>
        public static Decimal GetCartTaxByUserIdAndDomainId(int UserId, int DomainId,Boolean NextDay_Ind)
        {
            return SiteProvider.PR2.GetCartTaxByUserIdAndDomainId(UserId, DomainId,NextDay_Ind);

        }

        /// <summary>
        /// Returns a number of Cart object with the specified cart ID
        /// </summary>
        public static int GetCartCountByUserId(int UserId)
        {
            int iCount = (SiteProvider.PR2.GetCartCountByUserId(UserId));

            return iCount;
        }


        /// <summary>
        /// Returns number pharmacy courses the user took, before processing order
        /// </summary>
        public static int CheckNumberOfPharmacy(int UserId)
        {
            int number = (SiteProvider.PR2.CheckNumberOfPharmacy(UserId));

            return number;
        }


        /// <summary>
        /// Returns a Cart price object with the specified ID
        /// </summary>
        public static DataSet GetOrderPriceByUserId(int UserId)
        {
            DataSet dsCart = (SiteProvider.PR2.GetOrderPriceByUserId(UserId));

            return dsCart;
        }

        public static DataSet GetUnlimOrderPriceByUserId(int UserId)
        {
            DataSet dsOrder = (SiteProvider.PR2.GetUnlimOrderPriceByUserId(UserId));

            return dsOrder;
        }

        public static DataSet GetOnlyUnlimOrderPrice()
        {
            DataSet dsOrder = (SiteProvider.PR2.GetOnlyUnlimOrderPrice());

            return dsOrder;
        }



        /// <summary>
        /// Deletes an existing Cart, but first checks if OK to delete
        /// </summary>
        public static bool DeleteCart(int CartID)
        {
            bool IsOKToDelete = OKToDelete(CartID);
            if (IsOKToDelete)
            {
                return (bool)DeleteCart(CartID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Test - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteCart(int CartID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteCart(CartID);
            //         new RecordDeletedEvent("Test", TestID, null).Raise();
            BizObject.PurgeCacheItems("Carts_Cart");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Test can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int CartID)
        {
            return true;
        }


        ///<summary>
        ///Check all online
        ///</summary>
        ///
        public static int CheckAllOnline(int UserId,int DomainId)
        {
            int ret = SiteProvider.PR2.CheckAllOnline(UserId,DomainId);
            return ret;
        }

        /// <summary>
        /// Returns a collection with complete Carts for the specified UserID and DomainId
        /// </summary>
        public static List<Cart> GetcompleteCartsByUserIDAndTopicID(int UserID, int TopicID, int DomainID)
        {
            return GetCartListFromCartInfoList(SiteProvider.PR2.GetcompleteCartsByUserIDAndTopicID(UserID,TopicID,DomainID));
            
        }
        /// <summary>
        /// Returns a collection with INcomplete Carts for the specified UserID and DomainId
        /// </summary>
        public static bool IsOnlineCourseInCart(int UserID, int TopicID, int DomainID)
        {
            List<Cart> list = GetCartListFromCartInfoList(SiteProvider.PR2.GetIncompleteCartsByUserIDAndTopicID(UserID, TopicID, DomainID));
            if (list.Count > 0)
            {
                Cart cart = list.Where(r => r.MediaType == "O").SingleOrDefault();
                return cart == null ? false : true;
            }
            return false;
        }
        /// <summary>
        /// Returns a collection with INcomplete Carts for the specified UserID and DomainId
        /// </summary>
        public static List<Cart> GetIncompleteCartsByUserIDAndTopicID(int UserID, int TopicID, int DomainID)
        {
            return GetCartListFromCartInfoList(SiteProvider.PR2.GetIncompleteCartsByUserIDAndTopicID(UserID, TopicID, DomainID));
        }

        public static Cart GetcompleteCartByUserIDAndTopicID(int UserID, int TopicID, int facilityID)
        {
            return GetCartFromCartInfo(SiteProvider.PR2.GetcompleteCartByUserIDAndTopicID(UserID, TopicID, facilityID));
        }
        ///// <summary>
        ///// Returns a list of test question objects for the specified CartID
        ///// NOTE: 12/01/2008 New version of this method with new logic
        ///// </summary>
        public static List<QTIQuestionObject> GetTestQuestionObjectListByTopicID(int TopicID)
        {
            TestDefinition oTestDefinition = null;
            QTIUtils oQTIUtils = new QTIUtils();

            // NOTE: No caching of these items, because they contain user responses
            // along with test questions
            List<QTIQuestionObject> QTIQuestionObjects = null;

            // get the test definition record for this topic
            oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(TopicID);

            if (oTestDefinition != null)
            {
                // get the QTIQuestionObjectList from the TestDefinition XML
                QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
                // workaround until XMl is correct
                //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
            }
            if (QTIQuestionObjects != null)
            {
                // incomplete test that does not match the current test definition version number
                // set to default X and blank answers
                foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                {
                    if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                        QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                    {
                        QTIQuestion.cUserAnswer = "X";
                    }
                    else
                    {
                        QTIQuestion.cUserAnswer = "";
                    }
                }
            }

            return QTIQuestionObjects;
        }



        public static List<QTIQuestionObject> GetTestQuestionObjectListByTopicIdAndAnswers(int TopicId, string SAnswers)
        {
            TestDefinition oTestDefinition = null;
            QTIUtils oQTIUtils = new QTIUtils();

            // NOTE: No caching of these items, because they contain user responses
            // along with test questions
            List<QTIQuestionObject> QTIQuestionObjects = null;
            // get the test definition record for this topic
            oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(TopicId);

            if (oTestDefinition != null)
            {
                // get the QTIQuestionObjectList from the TestDefinition XML
                QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
                // workaround until XMl is correct
                //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
            }

            if (QTIQuestionObjects != null)
            {
                List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(SAnswers);
                String[] Answers = UserAnswers.ToArray();
                int iLoop = 0;
                foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                {
                    if (Convert.ToInt32(QTIQuestion.cHeaderID) > 0)
                    {
                        QTIQuestion.cUserAnswer = Answers[iLoop];
                        // be sure we're not empty -- if so, set to X
                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                            QTIQuestion.cUserAnswer = "X";
                        iLoop++;
                    }
                }
            }

            return QTIQuestionObjects;
        }


        public static decimal GetCouponAmountByUserIdandCode(int UserId, string CouponCode, string Unlimited, int FacilityId, string TopicId)
        {
            return (SiteProvider.PR2.GetCouponAmountByUserIdandCode(UserId, CouponCode,Unlimited,FacilityId, TopicId));
        }
        public static decimal GetMSCouponAmountByUserIdandCode(int UserId, string CouponCode, string Unlimited, string FacilityId, string TopicId)
        {
            return (SiteProvider.PR2.GetMSCouponAmountByUserIdandCode(UserId, CouponCode, Unlimited, FacilityId, TopicId));
        }
        public static decimal GetCouponAmountByTopicIdandCode(int TopicId, string CouponCode)
        {
            return (SiteProvider.PR2.GetCouponAmountByTopicIdandCode(TopicId, CouponCode));
        }

        public static decimal GetPRCouponAmountByUserIdandCode(int UserId, string CouponCode, string Unlimited, int facilityid)
        {
            return (SiteProvider.PR2.GetPRCouponAmountByUserIdandCode(UserId,CouponCode,Unlimited,facilityid));
        }

        public static bool HasNutritionDimensionCourse(int UserID, int DomainID) 
        {
            return (SiteProvider.PR2.HasNutritionDimensionCourse(UserID, DomainID));
        }

        /// <summary>
        /// Returns a Saved Cart object with the specified ID
        /// </summary>
        public static DataSet GetSavedCartPriceByUserId(int UserId)
        {
            DataSet dsCart = (SiteProvider.PR2.GetSavedCartPriceByUserId(UserId));

            return dsCart;
        }


        ///<summary>
        ///Retuns a newcartid when moved the savedcart object from savedcart tbale
        ///</summary>
        public static int Move_To_Cart(int SavedCartId)
        {
            return (SiteProvider.PR2.Move_To_Cart(SavedCartId));
        }

        ///<summary>
        ///Retuns a new saved cartid when moved the cart object from cart tbale
        ///</summary>
        public static int Move_To_SavedCart(int CartId)
        {
            return (SiteProvider.PR2.Move_To_SavedCart(CartId));
        }

        /// <summary>
        /// Deletes an existing SavedCart
        /// </summary>
        public static bool DeleteSavedCart(int CartID)
        {

            return (bool)(SiteProvider.PR2.DeleteSavedCart(CartID));
           
        }

        #endregion

    }
}
