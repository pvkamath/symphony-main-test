﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for Credits
    /// </summary>
    public class Credits : BasePR
    {
        #region Variables and Properties

        private int _creditID = 0;
        public int CreditID
        {
            get { return _creditID; }
            protected set { _creditID = value; }
        }

        private int _userID ;
        public int UserID
        {
            get { return _userID; }
            private set { _userID = value; }
        }

        private int _cmsID ;
        public int CmsID
        {
            get { return _cmsID; }
            private set { _cmsID = value; }
        }

        private int _hourLimit ;
        public int HourLimit
        {
            get { return _hourLimit; }
            private set { _hourLimit = value; }
        }

        private DateTime? _expiredate = null;
        public DateTime? Expiredate
        {
            get { return _expiredate; }
            set { _expiredate = value; }
        }

        private int _creditNumber ;
        public int CreditNumber
        {
            get { return _creditNumber; }
            private set { _creditNumber = value; }
        }
        
        public Credits(int CreditID, int UserID, int CmsID, int HourLimit, DateTime? Expiredate, int CreditNumber)
        {
            this.CreditID = CreditID;
            this.UserID = UserID;
            this.CmsID = CmsID;
            this.HourLimit = HourLimit;
            this.Expiredate = Expiredate;
            this.CreditNumber = CreditNumber;
        }

        public bool Delete()
        {
            bool success = Credits.DeleteCredits(this.CreditID);
            //if (success)
              //  this.CreditID = 0;
            return success;
        }

        public bool Update()
        {
            return Credits.UpdateCredits(this.CreditID, this.UserID, this.CmsID, HourLimit, Expiredate, CreditNumber);
        }
        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Credits
        /// </summary>
        public static List<Credits> GetCredits(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<Credits> Credits = null;
            string key = "Credits_Credits_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Credits = (List<Credits>)BizObject.Cache[key];
            }
            else
            {
                List<CreditsInfo> recordset = SiteProvider.PR2.GetCredits(cSortExpression);
                Credits = GetCreditsListFromCreditsInfoList(recordset);
                BasePR.CacheData(key, Credits);
            }
            return Credits;
        }
        
        /// <summary>
        /// Returns a collection with all Credits
        /// </summary>
        public static List<Credits> GetCreditsByUserId(int UserId)
        {
          
            List<Credits> Credits = null;
            string key = "Credits_CreditsByUserId_" + UserId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Credits = (List<Credits>)BizObject.Cache[key];
            }
            else
            {
                List<CreditsInfo> recordset = SiteProvider.PR2.GetCreditsByUserId(UserId);
                Credits = GetCreditsListFromCreditsInfoList(recordset);
                BasePR.CacheData(key, Credits);
            }
            return Credits;
        }

        /// <summary>
        /// Returns a collection with all Credits
        /// </summary>
        public static List<Credits> GetAllCreditsByUserId(int UserId)
        {            
            List<Credits> Credits = null;
            string key = "Credits_AllCreditsByUserID_" + UserId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Credits = (List<Credits>)BizObject.Cache[key];
            }
            else
            {
                List<CreditsInfo> recordset = SiteProvider.PR2.GetAllCreditsByUserId(UserId);
                Credits = GetCreditsListFromCreditsInfoList(recordset);
                BasePR.CacheData(key, Credits);
            }
            return Credits;
        }

        /// <summary>
        /// Returns the number of total Credits
        /// </summary>
        public static int GetCreditsCount()
        {
            int CreditsCount = 0;
            string key = "Credits_CreditsCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CreditsCount = (int)BizObject.Cache[key];
            }
            else
            {
                CreditsCount = SiteProvider.PR2.GetCreditsCount();
                BasePR.CacheData(key, CreditsCount);
            }
            return CreditsCount;
        }

        /// <summary>
        /// Returns a Credits object with the specified ID
        /// </summary>
        public static Credits GetCreditsByID(int CreditID)
        {
            Credits Credits = null;
            string key = "Credits_CreditsByID_" + CreditID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Credits = (Credits)BizObject.Cache[key];
            }
            else
            {
                Credits = GetCreditsFromCreditsInfo(SiteProvider.PR2.GetCreditsByID(CreditID));
                BasePR.CacheData(key, Credits);
            }
            return Credits;
        }


        /// <summary>
        /// Returns a Credits object with the specified domain, hour, and expiration
        /// </summary>
        public static Credits GetCreditsByCriteria(int CreditID, int UserId,int Cmsid, int HourLimit, DateTime? Expiredate)
        {
            Credits Credits = null;
            string key = "Credits_CreditsByCriteria_" + UserId.ToString() + Cmsid.ToString() + HourLimit.ToString() + Expiredate == null ? "" : Expiredate.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Credits = (Credits)BizObject.Cache[key];
            }
            else
            {
                Credits = GetCreditsFromCreditsInfo(SiteProvider.PR2.GetCreditsByCriteria(CreditID, UserId,Cmsid, HourLimit, Expiredate));
                BasePR.CacheData(key, Credits);
            }
            return Credits;
        }
        
        /// <summary>
        /// Returns a MyCredits by Id
        /// </summary>
        public static DataSet GetMyCreditsById(int UserId)
        {
            DataSet dsCredits = (SiteProvider.PR2.GetMyCreditsById(UserId));

            return dsCredits;
        }

        /// <summary>
        /// Returns a Credits Cart BY UserId
        /// </summary>
        public static DataSet GetCreditsCartByUserId(int UserId, int DomainID)
        {
            DataSet dsCreditsCart = (SiteProvider.PR2.GetCreditsCartByUserId(UserId, DomainID));

            return dsCreditsCart;
        }

        /// <summary>
        /// Returns a Credits Cart BY UserId
        /// </summary>
        public static DataSet GetCreditsByUserIDAndDomainIdAndHrs(int UserID, int DomainId, decimal Hours, string DomainName, string ContactHrs)
        {
            DataSet dsCredits = (SiteProvider.PR2.GetCreditsByUserIDAndDomainIdAndHrs(UserID, DomainId, Hours, DomainName, ContactHrs));

            return dsCredits;
        }

        /// <summary>
        /// Returns the number of total Topics, except obsolete ones
        /// </summary>
        public static int GetCredtsCreditNumberByUserIDAndDomainIdAndHrs(int UserID, int DomainId, decimal Hours)
        {
            int CreditNumber = 0;
            string key = "Credts_CreditNumber";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CreditNumber = (int)BizObject.Cache[key];
            }
            else
            {
                CreditNumber = SiteProvider.PR2.GetCredtsCreditNumberByUserIDAndDomainIdAndHrs(UserID,  DomainId, Hours);
                BasePR.CacheData(key, CreditNumber);
            }
            return CreditNumber;
        }
        /// <summary>
        /// Updates an existing Credits
        /// </summary>
        public static bool UpdateCredits(int CreditID, int UserID, int CmsID, int HourLimit, DateTime? Expiredate, int CreditNumber)
        {


            CreditsInfo record = new CreditsInfo(CreditID, UserID, CmsID, HourLimit, Expiredate, CreditNumber);
            bool ret = SiteProvider.PR2.UpdateCredits(record);

            BizObject.PurgeCacheItems("Credits_UpdateCredits_" + CreditID.ToString());
            BizObject.PurgeCacheItems("Credits_UpdateCredits");
            return ret;
        }
        /// <summary>
        /// Transfer Credits
        /// </summary>
        public static bool TransferCredits(int CreditID, string R_username, int CreditNumber)
        {
            bool ret = SiteProvider.PR2.TransferCredits(CreditID, R_username, CreditNumber);

            BizObject.PurgeCacheItems("Credits_TransferCredits_" + CreditID.ToString());
            BizObject.PurgeCacheItems("Credits_TransferCredits");
            return ret;
        }

        /// <summary>
        /// Creates a new Credits
        /// </summary>
        public static int InsertCredits(int UserID, int CmsID, int HourLimit, DateTime? Expiredate, int CreditNumber)
        {


            CreditsInfo record = new CreditsInfo(0, UserID, CmsID, HourLimit, Expiredate, CreditNumber);
            int ret = SiteProvider.PR2.InsertCredits(record);

            BizObject.PurgeCacheItems("Credits_InsertCredits");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Credits, but first checks if OK to delete
        /// </summary>
        public static bool DeleteCredits(int CreditID)
        {
            bool IsOKToDelete = OKToDelete(CreditID);
            if (IsOKToDelete)
            {
                return (bool)DeleteCredits(CreditID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Credits - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteCredits(int CreditID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteCredits(CreditID);
            //         new RecordDeletedEvent("Credits", CreditID, null).Raise();
            BizObject.PurgeCacheItems("Credits_DeleteCredits");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Credits can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int CreditID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Credits object filled with the data taken from the input CreditsInfo
        /// </summary>
        private static Credits GetCreditsFromCreditsInfo(CreditsInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Credits(record.CreditID, record.UserID, record.CmsID, record.HourLimit,record.Expiredate,record.CreditNumber);
            }
        }

        /// <summary>
        /// Returns a list of Credits objects filled with the data taken from the input list of CreditsInfo
        /// </summary>
        private static List<Credits> GetCreditsListFromCreditsInfoList(List<CreditsInfo> recordset)
        {
            List<Credits> Credits = new List<Credits>();
            foreach (CreditsInfo record in recordset)
                Credits.Add(GetCreditsFromCreditsInfo(record));
            return Credits;
        }
        #endregion
    }
}
