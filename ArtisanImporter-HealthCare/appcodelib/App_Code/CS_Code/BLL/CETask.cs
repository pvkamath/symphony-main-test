﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    public class CETask : BasePR
    {


         public CETask(int TaskID, DateTime RequestDate, DateTime ReviewDate, DateTime CompleteDate,
                             string TaskType, string TaskArea, string TaskName, string TaskDetail, string ReviewComment,
                             string CompleteComment)
        {
            this.TaskID = TaskID;
            this.RequestDate = RequestDate;
            this.ReviewDate = ReviewDate;
            this.CompleteDate = CompleteDate;
            this.TaskType = TaskType;
            this.TaskArea = TaskArea;
            this.TaskName = TaskName;
            this.TaskDetail = TaskDetail;
            this.ReviewComment = ReviewComment;
            this.CompleteComment = CompleteComment;
        }

        #region Variables and Properties



        public int _TaskID = 0;
        public int TaskID
        {
            get { return _TaskID; }
            set { _TaskID = value; }
        }

        private DateTime _RequestDate = System.DateTime.MinValue;
        public DateTime RequestDate
        {
            get { return _RequestDate; }
            set { _RequestDate = value; }
        }

        private DateTime _ReviewDate = System.DateTime.MinValue;
        public DateTime ReviewDate
        {
            get { return _ReviewDate; }
            set { _ReviewDate = value; }
        }

        private DateTime _CompleteDate = System.DateTime.MinValue;
        public DateTime CompleteDate
        {
            get { return _CompleteDate; }
            set { _CompleteDate = value; }
        }

        private string _TaskType = "";
        public string TaskType
        {
            get { return _TaskType; }
            set { _TaskType = value; }
        }

        private string _TaskArea = "";
        public string TaskArea
        {
            get { return _TaskArea; }
            set { _TaskArea = value; }
        }

        private string _TaskName = "";
        public string TaskName
        {
            get { return _TaskName; }
            set { _TaskName = value; }
        }

        private string _TaskDetail = "";
        public string TaskDetail
        {
            get { return _TaskDetail; }
            set { _TaskDetail = value; }
        }

        private string _ReviewComment = "";
        public string ReviewComment
        {
            get { return _ReviewComment; }
            set { _ReviewComment = value; }
        }

        private string _CompleteComment = "";
        public string CompleteComment
        {
            get { return _CompleteComment; }
            set { _CompleteComment = value; }
        }
       
        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all CETasks
        /// </summary>
        public static List<CETask> GetCETask(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TaskName";

            List<CETask> CETask= null;
            string key = "CETask_CETask_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CETask = (List<CETask>)BizObject.Cache[key];
            }
            else
            {
                List<CETaskInfo> recordset = SiteProvider.PR2.GetCETask(cSortExpression);
                CETask = GetCETaskListFromCETaskInfoList(recordset);
                BasePR.CacheData(key, CETask);
            }
            return CETask;
        }

        /// <summary>
        /// Returns a collection with all CETasks
        /// </summary>
        public static List<CETask> GetAllCETask(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TaskName";

            List<CETask> CETask = null;
            string key = "CETask_CETask_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CETask = (List<CETask>)BizObject.Cache[key];
            }
            else
            {
                List<CETaskInfo> recordset = SiteProvider.PR2.GetAllCETask(cSortExpression);
                CETask = GetCETaskListFromCETaskInfoList(recordset);
                BasePR.CacheData(key, CETask);
            }
            return CETask;
        }


        /// <summary>
        /// Returns the number of total CETask
        /// </summary>
        public static int GetCETaskCount()
        {
            int CETaskCount = 0;
            string key = "CETask_CETaskCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CETaskCount = (int)BizObject.Cache[key];
            }
            else
            {
                CETaskCount = SiteProvider.PR2.GetCETaskCount();
                BasePR.CacheData(key, CETaskCount);
            }
            return CETaskCount;
        }

        /// <summary>
        /// Returns a CETask object with the specified ID
        /// </summary>
        public static CETask GetCETaskByID(int TaskID)
        {
            CETask CETask = null;
            string key = "CETask_CETask_" + TaskID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CETask = (CETask)BizObject.Cache[key];
            }
            else
            {
                CETask = GetCETaskFromCETaskInfo(SiteProvider.PR2.GetCETaskByID(TaskID));
                BasePR.CacheData(key, CETask);
            }
            return CETask;
        }

        /// <summary>
        /// Updates an existing CETask
        /// </summary>
        public static bool UpdateCETask(int TaskID, DateTime RequestDate, DateTime ReviewDate, DateTime CompleteDate,
                             string TaskType, string TaskArea, string TaskName, string TaskDetail, string ReviewComment,
                             string CompleteComment)
        {
            TaskName = BizObject.ConvertNullToEmptyString(TaskName);


            CETaskInfo record = new CETaskInfo(TaskID, RequestDate, ReviewDate, CompleteDate,
                             TaskType, TaskArea, TaskName, TaskDetail, ReviewComment, CompleteComment);
            bool ret = SiteProvider.PR2.UpdateCETask(record);

            BizObject.PurgeCacheItems("CETask_CETask_" + TaskID.ToString());
            BizObject.PurgeCacheItems("CETask_CETask");
            return ret;
        }

        /// <summary>
        /// Creates a new CETask
        /// </summary>
        public static int InsertCETask(int TaskID, DateTime RequestDate, DateTime ReviewDate, DateTime CompleteDate,
                             string TaskType, string TaskArea, string TaskName, string TaskDetail, string ReviewComment,
                             string CompleteComment)
        {
            TaskName = BizObject.ConvertNullToEmptyString(TaskName);


            CETaskInfo record = new CETaskInfo(0, RequestDate, ReviewDate, CompleteDate,
                             TaskType, TaskArea, TaskName, TaskDetail, ReviewComment,
                             CompleteComment);
            int ret = SiteProvider.PR2.InsertCETask(record);

            BizObject.PurgeCacheItems("CETask_CETask");
            return ret;
        }

        /// <summary>
        /// Deletes an existing CETask, but first checks if OK to delete
        /// </summary>
        public static bool DeleteCETask(int TaskID)
        {
            bool IsOKToDelete = OKToDelete(TaskID);
            if (IsOKToDelete)
            {
                return (bool)DeleteCETask(TaskID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing CETask- second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteCETask(int TaskID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteCETask(TaskID);
            BizObject.PurgeCacheItems("CETask_CETask");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Role can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int TaskID)
        {
            return true;
        }



        /// <summary>
        /// Returns a CETask object filled with the data taken from the input RoleInfo
        /// </summary>
        private static CETask GetCETaskFromCETaskInfo(CETaskInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new CETask(record.TaskID, record.RequestDate, record.ReviewDate, record.CompleteDate, record.TaskType,
                    record.TaskArea, record.TaskName, record.TaskDetail, record.ReviewComment, record.CompleteComment); 
            }
        }

        /// <summary>
        /// Returns a list of CETask objects filled with the data taken from the input list of RoleInfo
        /// </summary>
        private static List<CETask> GetCETaskListFromCETaskInfoList(List<CETaskInfo> recordset)
        {
            List<CETask> CETask = new List<CETask>();
            foreach (CETaskInfo record in recordset)
                CETask.Add(GetCETaskFromCETaskInfo(record));
            return CETask;
        }

        #endregion

    }
}