﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr1
    /// </summary>
    /// 

    public class OrderItem : BasePR
    {
        #region variables and properties

        public OrderItem()
        { }

        private int _OrderItemID = 0;
        public int OrderItemID
        {
            get { return _OrderItemID; }
            private set { _OrderItemID = value; }
        }

        private int _OrderID = 0;
        public int OrderID
        {
            get { return _OrderID; }
            private set { _OrderID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

        private decimal _Cost = 0;
        public decimal Cost
        {
            get { return _Cost; }
            set { _Cost = value; }
        }

        private string _Media_Type = "";
        public string Media_Type
        {
            get { return _Media_Type.ToLower(); }
            private set { _Media_Type = value.ToLower(); }
        }

        private int _Quantity = 0;
        public int Quantity
        {
            get { return _Quantity; }
            private set { _Quantity = value; }
        }

        private int _DiscountID = 0;
        public int DiscountID
        {
            get { return _DiscountID; }
            private set { _DiscountID = value; }
        }

        private bool _Optin_Ind = false;
        public bool Optin_Ind
        {
            get { return _Optin_Ind; }
            set { _Optin_Ind = value; }
        }

        private string _topicName = "";
        public string TopicName
        {
            get { return _topicName; }
            set { _topicName = value; }
        }

        private decimal _Credits = 0;
        public decimal Credits
        {
            get { return _Credits; }
            set { _Credits = value; }
        }

        private string _Course_Number = "";
        public string Course_Number
        {
            get { return _Course_Number; }
            set { _Course_Number = value; }
        }
        #endregion

        #region Methods

        public OrderItem(int OrderItemID, int OrderID, int TopicID, Decimal Cost, string Media_Type,
        int Quantity, int DiscountID, bool Optin_Ind)
        {
            this.OrderItemID = OrderItemID;
            this.OrderID = OrderID;
            this.TopicID = TopicID;
            this.Cost = Cost;
            this.Media_Type = Media_Type;
            this.Quantity = Quantity;
            this.DiscountID = DiscountID;
            this.Optin_Ind = Optin_Ind;
            this.TopicName = Topic.GetTopicByID(TopicID).TopicName;
            this.Credits = Topic.GetTopicByID(TopicID).Hours;
            this.Course_Number = Topic.GetTopicByID(TopicID).Course_Number;

        }


        private static OrderItem GetOrderItemFromOrderItemInfo(OrderItemInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new OrderItem(record.OrderItemID, record.OrderID, record.TopicID, record.Cost, record.Media_Type, record.Quantity,
           record.DiscountID, record.Optin_Ind);
            }
        }

        /// <summary>
        /// Returns a list of Orders objects filled with the data taken from the input list of DiscountInfo
        /// </summary>
        private static List<OrderItem> GetOrderItemFromOrderItemInfoList(List<OrderItemInfo> recordset)
        {
            List<OrderItem> Orders = new List<OrderItem>();
            foreach (OrderItemInfo record in recordset)
                Orders.Add(GetOrderItemFromOrderItemInfo(record));
            return Orders;
        }

        public static List<OrderItem> GetOrderDetails(int OrderID)
        {
            List<OrderItem> Orders = null;
            string key = "Orders_Orders" + OrderID.ToString();


            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Orders = (List<OrderItem>)BizObject.Cache[key];
            }
            else
            {
                List<OrderItemInfo> recordset = new List<OrderItemInfo>();
               // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                recordset = (SiteProvider.PR2.GetOrderDetails(OrderID));

                //recordset.Add(recordsetinfo);
                Orders = GetOrderItemFromOrderItemInfoList(recordset);
                BasePR.CacheData(key, Orders);
            }
            return Orders;
        }

        /// <summary>
        /// Creates new orders
        /// </summary>
        public static int InsertOrderItem(int OrderItemID, int OrderID, int TopicID, Decimal Cost, string Media_Type,
        int Quantity, int DiscountID, bool Optin_Ind)
        {
            OrderItemInfo record = new OrderItemInfo(0, OrderID, TopicID, Cost, Media_Type, Quantity,
            DiscountID, Optin_Ind);

           
            int ret = SiteProvider.PR2.InsertOrderItem(record);
            BizObject.PurgeCacheItems("OrderItems_OrderItems");
            return ret;
        }

         /// <summary>
        /// Creates new orders
        /// </summary>
        public static int InsertMicrositeOrderItem(int OrderItemID, int OrderID, int TopicID, Decimal Cost, string Media_Type,
        int Quantity, int DiscountID, bool Optin_Ind)
        {
            OrderItemInfo record = new OrderItemInfo(0, OrderID, TopicID, Cost, Media_Type, Quantity,
            DiscountID, Optin_Ind);

           
            int ret = SiteProvider.PR2.InsertMicrositeOrderItem(record);
            BizObject.PurgeCacheItems("OrderItems_OrderItems");
            return ret;
        }

        /// <summary>
        /// Creates new orders
        /// </summary>
        public static int InsertMicrositeProOrderItem(int OrderItemID, int OrderID, int TopicID, Decimal Cost, string Media_Type,
        int Quantity, int DiscountID, bool Optin_Ind)
        {
            OrderItemInfo record = new OrderItemInfo(0, OrderID, TopicID, Cost, Media_Type, Quantity,
            DiscountID, Optin_Ind);

            int ret = SiteProvider.PR2.InsertMicrositeProOrderItem(record);
            BizObject.PurgeCacheItems("OrderItems_OrderItems_InsertMicrositeProOrderItem");
            return ret;
        }

        /// <summary>
        /// Creates pay now new orders
        /// </summary>
        public static int InsertMicrositePayNowOrderItem(int OrderItemID, int OrderID, int TopicID, Decimal Cost, string Media_Type,
        int Quantity, int DiscountID, bool Optin_Ind)
        {
            OrderItemInfo record = new OrderItemInfo(0, OrderID, TopicID, Cost, Media_Type, Quantity,
            DiscountID, Optin_Ind);


            int ret = SiteProvider.PR2.InsertMicrositePayNowOrderItem(record);
            BizObject.PurgeCacheItems("OrderItems_OrderItems");
            return ret;
        }
        public static decimal GetPriceByCartId(int CartId)
        {
            return SiteProvider.PR2.GetPriceByCartId(CartId);
        }
        public static decimal GetMSPriceByCartId(int CartId)
        {
            return SiteProvider.PR2.GetMSPriceByCartId(CartId);
        }

     #endregion

    }


}