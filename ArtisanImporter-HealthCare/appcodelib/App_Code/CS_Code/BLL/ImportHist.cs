﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
/// <summary>
/// Summary description for ImportHist
/// </summary>
public class ImportHist : BasePR
{
    public ImportHist(DateTime loadtime, int loadrows, int ignorerows, int ihid, string verify_comment, string upload_comment, string facilityname, string importtype)
    {
        this.Loadtime = loadtime;
        this.Loadrows = loadrows;
        this.Ignorerows = ignorerows;
        this.Ihid = ihid;
        this.Verify_comment = verify_comment;
        this.Upload_comment = upload_comment;
        this.Facilityname = facilityname;
        this.Importtype = importtype;
    }
    #region Variables and Properties
    private DateTime _loadtime = System.DateTime.MinValue;
    public DateTime Loadtime
    {
        get { return _loadtime; }
        set { _loadtime = value; }
    }
    private string _verify_comment = "";
    public string Verify_comment
    {
        get { return _verify_comment; }
        set { _verify_comment = value; }
    }
    private string _upload_comment = "";
    public string Upload_comment
    {
        get { return _upload_comment; }
        set { _upload_comment = value; }
    }
    private string _facilityname = "";
    public string Facilityname
    {
        get { return _facilityname; }
        set { _facilityname = value; }
    }
    private string _importtype = "";
    public string Importtype
    {
        get { return _importtype; }
        set { _importtype = value; }
    }
    private int _ihid;
    public int Ihid
    {
        get { return _ihid; }
        set { _ihid = value; }
    }
    private int _loadrows;
    public int Loadrows
    {
        get { return _loadrows; }
        set { _loadrows = value; }
    }
    private int _ignorerows;
    public int Ignorerows
    {
        get { return _ignorerows; }
        set { _ignorerows = value; }
    }
    #endregion 
    #region Methods
    /***********************************
        * Static methods
        ************************************/

    /// <summary>
    /// Returns a collection with all ImportHist
    /// </summary>
    public static List<ImportHist> GetImportHist(string cSortExpression)
    {
        if (cSortExpression == null)
            cSortExpression = "";

        // provide default sort
        if (cSortExpression.Length == 0)
            cSortExpression = "ihid";

        List<ImportHist> ImportHist = null;
        string key = "ImportHist_ImportHist_" + cSortExpression.ToString();

        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
        {
            ImportHist = (List<ImportHist>)BizObject.Cache[key];
        }
        else
        {
            List<ImportHistInfo> recordset = SiteProvider.PR2.GetImportHist(cSortExpression);
            ImportHist = GetImportHistListFromImportHistInfoList(recordset);
            BasePR.CacheData(key, ImportHist);
        }
        return ImportHist;
    }
    private static ImportHist GetImportHistFromCETaskInfo(ImportHistInfo record)
    {
        if (record == null)
            return null;
        else
        {
            return new ImportHist(record.Loadtime, record.Loadrows, record.Ignorerows, record.Ihid, record.Verify_comment, record.Upload_comment,
                record.Facilityname, record.Importtype);
        }
    }
    private static List<ImportHist> GetImportHistListFromImportHistInfoList(List<ImportHistInfo> recordset)
    {
        List<ImportHist> ImportHist = new List<ImportHist>();
        foreach (ImportHistInfo record in recordset)
            ImportHist.Add(GetImportHistFromCETaskInfo(record));
        return ImportHist;
    }

    #endregion
    public ImportHist()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}
