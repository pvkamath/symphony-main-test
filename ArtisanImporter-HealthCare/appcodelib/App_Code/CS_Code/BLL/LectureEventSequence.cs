﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using System.Collections.Generic;

namespace PearlsReview.BLL
{

/// <summary>
/// Summary description for LectureEventSequence
/// </summary>
    public class LectureEventSequence : BasePR
    {
        #region Variables and Properties
       

        private int _LectSeqID = 0;
        public int LectSeqID
        {
            get { return _LectSeqID; }
            set { _LectSeqID = value; }
        }

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            protected set { _LectEvtID = value; }
        }

        private int _Sequence = 0;
        public int Sequence
        {
            get { return _Sequence; }
            set { _Sequence = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _ResourceID1 = 0;
        public int ResourceID1
        {
            get { return _ResourceID1; }
            set { _ResourceID1 = value; }
        }

        private int _ResourceID2 = 0;
        public int ResourceID2
        {
            get { return _ResourceID2; }
            set { _ResourceID2 = value; }
        }

        private int _ResourceID3 = 0;
        public int ResourceID3
        {
            get { return _ResourceID3; }
            set { _ResourceID3 = value; }
        }

        private int _ResourceID4 = 0;
        public int ResourceID4
        {
            get { return _ResourceID4; }
            set { _ResourceID4 = value; }
        }

        private int _ResourceID5 = 0;
        public int ResourceID5
        {
            get { return _ResourceID5; }
            set { _ResourceID5 = value; }
        }

        private int _ResourceID6 = 0;
        public int ResourceID6
        {
            get { return _ResourceID6; }
            set { _ResourceID6 = value; }
        }

        private int _ResourceID7 = 0;
        public int ResourceID7
        {
            get { return _ResourceID7; }
            set { _ResourceID7 = value; }
        }

        private int _ResourceID8 = 0;
        public int ResourceID8
        {
            get { return _ResourceID8; }
            set { _ResourceID8 = value; }
        }

        private int _ResourceID9 = 0;
        public int ResourceID9
        {
            get { return _ResourceID9; }
            set { _ResourceID9 = value; }
        }

        private int _ResourceID10 = 0;
        public int ResourceID10
        {
            get { return _ResourceID10; }
            set { _ResourceID10 = value; }
        }

        public LectureEventSequence(int LectSeqID, int LectEvtID, int Sequence, int TopicID, int ResourceID1, int ResourceID2,
            int ResourceID3, int ResourceID4, int ResourceID5, int resourceID6, int resourceID7,
            int resourceID8, int resourceID9, int resourceID10)
        {
            this.LectSeqID = LectSeqID;
            this.LectEvtID = LectEvtID;
            this.Sequence = Sequence;
            this.TopicID = TopicID;
            this.ResourceID1 = ResourceID1;
            this.ResourceID2 = ResourceID2;
            this.ResourceID3 = ResourceID3;
            this.ResourceID4 = ResourceID4;
            this.ResourceID5 = ResourceID5;
            this.ResourceID6 = resourceID6;
            this.ResourceID7 = resourceID7;
            this.ResourceID8 = resourceID8;
            this.ResourceID9 = resourceID9;
            this.ResourceID10 = resourceID10;
        }

#endregion

        #region Methods

        /// <summary>
        /// Inserts a new LectureEventSequence
        /// </summary>
        public static int InsertLectureEventSequence(int LectSeqID, int LectEvtID, int Sequence, int TopicID,
            int ResourceID1, int ResourceID2, int ResourceID3, int ResourceID4, int ResourceID5, int resourceID6, int resourceID7,
            int resourceID8, int resourceID9, int resourceID10)
        {
            LectureEventSequenceInfo record = new LectureEventSequenceInfo(0, LectEvtID, Sequence, TopicID, ResourceID1, 
                ResourceID2, ResourceID3, ResourceID4, ResourceID5, resourceID6, resourceID7, resourceID8, resourceID9, resourceID10);
            int ret = SiteProvider.PR2.InsertLectureEventSequence(record);

            BizObject.PurgeCacheItems("LectureEventSequences_LectureEventSequence");
            return ret;
        }

        public static List<LectureEventSequence> GetLectureEventSequenceDetailsByTopicID(int topicID)
        {
            List<LectureEventSequence> LectureEventSequences = null;
            string key = "LectureEventSequencess_LectureEventSequencess_" + topicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEventSequences = (List<LectureEventSequence>)BizObject.Cache[key];
            }
            else
            {
                List<LectureEventSequenceInfo> recordset = SiteProvider.PR2.GetLectureEventSequenceDetailsByTopicID(topicID);
                LectureEventSequences = GetLectureEventSequenceListFromLectureEventSequenceInfoList(recordset);
                BasePR.CacheData(key, LectureEventSequences);
            }
            return LectureEventSequences;
        
        }


        public static List<LectureEventSequence> GetLectureEventSequenceDetailsBylectevtid(int lectevtid)
        {

            List<LectureEventSequence> LectureEventSequences = null;
            string key = "LectureEventSequencess_LectureEventSequencess_" + lectevtid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEventSequences = (List<LectureEventSequence>)BizObject.Cache[key];
            }
            else
            {
                List<LectureEventSequenceInfo> recordset = SiteProvider.PR2.GetLectureEventSequenceDetailsBylectevtid(lectevtid);
                LectureEventSequences = GetLectureEventSequenceListFromLectureEventSequenceInfoList(recordset);
                BasePR.CacheData(key, LectureEventSequences);
            }
            return LectureEventSequences;

        }

        public static  DataSet GetLectureTopicAndResourceDetailsByEventID(int lectEvtID)

        {
            return SiteProvider.PR2.GetLectureTopicAndResourceDetailsByEventID(lectEvtID);
        }

        public static DataSet GetLectureTopicAndResourceInfoByLecEvtID(int lectEvtID)
        {
            return SiteProvider.PR2.GetLectureTopicAndResourceInfoByLecEvtID(lectEvtID);
        }


        public static bool DeleteLectureSequence(int lectSeqID)
        {
            bool ret = SiteProvider.PR2.DeleteLectureSequence(lectSeqID);

            BizObject.PurgeCacheItems("LectureEventSequences_LectureEventSequence" + lectSeqID.ToString());
            BizObject.PurgeCacheItems("LectureEventSequences_LectureEventSequence");
            return ret;
        }

        public static bool DeleteLectureEventSequenceByTopicID(int TopicID)
        {
            bool ret = SiteProvider.PR2.DeleteLectureEventSequenceByTopicID(TopicID);

            BizObject.PurgeCacheItems("LectureEventSequences_LectureEventSequence" + TopicID.ToString());
            BizObject.PurgeCacheItems("LectureEventSequences_LectureEventSequence");
            return ret;
        }

        public static bool UnAssignResourcesForLectEvtID(int ResourceID)
        {
            bool ret = SiteProvider.PR2.UnAssignResourcesForLectEvtID(ResourceID);

            BizObject.PurgeCacheItems("LectureEventSequences_LectureEventSequence" + ResourceID.ToString());
            BizObject.PurgeCacheItems("LectureEventSequences_LectureEventSequence");
            return ret;
        }

        /// <summary>
        /// Updates an existing LectureEventSequence
        /// </summary>
        public static bool UpdateLectureEventSequence(int LectSeqID, int LectEvtID, int Sequence, int TopicID, int ResourceID1,
            int ResourceID2, int ResourceID3, int ResourceID4, int ResourceID5, int resourceID6, int resourceID7,
            int resourceID8, int resourceID9, int resourceID10)
        {

            LectureEventSequenceInfo record = new LectureEventSequenceInfo(LectSeqID, LectEvtID, Sequence, TopicID, ResourceID1, 
                ResourceID2, ResourceID3, ResourceID4, ResourceID5, resourceID6, resourceID7, resourceID8, resourceID9, resourceID10);
            bool ret = SiteProvider.PR2.UpdateLectureEventSequence(record);

            BizObject.PurgeCacheItems("LectureEventSequences_LectureEventSequence" + LectSeqID.ToString());
            BizObject.PurgeCacheItems("LectureEventSequences_LectureEventSequence");
            return ret;
        }


        /// <summary>
        /// Returns a LectureEventSequence object filled with the data taken from the input LectureEventSequenceInfo
        /// </summary>
        private static LectureEventSequence GetLectureEventSequenceFromLectureEventSequenceInfo(LectureEventSequenceInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new LectureEventSequence(record.LectSeqID, record.LectEvtID, record.Sequence, record.TopicID, record.ResourceID1, 
                    record.ResourceID2, record.ResourceID3, record.ResourceID4, record.ResourceID5, record.ResourceID6, 
                    record.ResourceID7, record.ResourceID8, record.ResourceID9, record.ResourceID10);
            }
        }

        /// <summary>
        /// Returns a list of LectureEventSequence objects filled with the data taken from the input list of LectureEventSequenceInfo
        /// </summary>
        private static List<LectureEventSequence> GetLectureEventSequenceListFromLectureEventSequenceInfoList(List<LectureEventSequenceInfo> recordset)
        {
            List<LectureEventSequence> LectureEventSequences = new List<LectureEventSequence>();
            foreach (LectureEventSequenceInfo record in recordset)
                LectureEventSequences.Add(GetLectureEventSequenceFromLectureEventSequenceInfo(record));
            return LectureEventSequences;
        }



        #endregion

    }
}
