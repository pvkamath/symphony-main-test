﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.DAL;
using PearlsReview.BLL.PRBase;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for TopicQuestion
    /// </summary>
    public class TopicSurveyLink : BasePR
    {
        #region Variables and Properties
        private int _tslID = 0;
        public int TSLID
        {
            get { return _tslID; }
            set { _tslID = value; }
        }

        private int _topicID = 0;
        public int TopicID
        {
            get { return _topicID; }
            set { _topicID = value; }
        }

        private int _surveyID = 0;
        public int SurveyID
        {
            get { return _surveyID; }
            set { _surveyID = value; }
        }

        private string _surveyType = "";
        public string SurveyType
        {
            get { return _surveyType; }
            set { _surveyType = value; }
        }

        public TopicSurveyLink(int tslID, int topicID, int surveyID, string surveyType)
        {
            this.TSLID = tslID;
            this.TopicID = topicID;
            this.SurveyID = surveyID;
            this.SurveyType = surveyType;
        }

        #endregion Variables and Properties

        #region Methods
        public static int InsertTopicSurveyLink(int topicID, int surveyID, string surveyType)
        {
            surveyType = BizObject.ConvertNullToEmptyString(surveyType);

            TopicSurveyLinkInfo topicSurveyLink = new TopicSurveyLinkInfo(0, topicID, surveyID, surveyType);
            return SiteProvider.PR2.InsertTopicSurveyLink(topicSurveyLink);
        }

        public static bool UpdateTopicSurveyLinkByTopicIDAndSurveyType(int surveyID, int topicID, string surveyType)
        {
            surveyType = BizObject.ConvertNullToEmptyString(surveyType);

            return SiteProvider.PR2.UpdateTopicSurveyLinkByTopicIDAndSurveyType(surveyID, topicID, surveyType);
        }

        public static TopicSurveyLink GetTopicSurveyLinkByTopicIDAndSurveyType(int topicID, string surveyType)
        {
            TopicSurveyLink topicSurveyLink = null;
            string key = "TopicSurveyLinks_TopicSurveyLink_TopicID_" + topicID.ToString() + "_SurveyType_" + surveyType;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                topicSurveyLink = (TopicSurveyLink)BizObject.Cache[key];
            }
            else
            {
                topicSurveyLink = GetTopicSurveyLinkFromTopicSurveyLinkInfo(
                    SiteProvider.PR2.GetTopicSurveyLinkByTopicIDAndSurveyType(topicID, surveyType));
                BasePR.CacheData(key, topicSurveyLink);
            }
            return topicSurveyLink;
        }

        private static TopicSurveyLink GetTopicSurveyLinkFromTopicSurveyLinkInfo(TopicSurveyLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new TopicSurveyLink(record.TSLID, record.TopicID, record.SurveyID, record.SurveyType);
            }
        }

        #endregion Methods
    }
}
