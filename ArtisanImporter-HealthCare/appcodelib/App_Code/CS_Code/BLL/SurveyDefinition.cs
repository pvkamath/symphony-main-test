﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for SurveyDefinition
/// </summary>
    public class SurveyDefinition : BasePR
    {
        #region Variables and Properties

        private int _SurveyID = 0;
        public int SurveyID
        {
            get { return _SurveyID; }
            protected set { _SurveyID = value; }
        }

        private string _SurveyName = "";
        public string SurveyName
        {
            get { return _SurveyName; }
            set { _SurveyName = value; }
        }

        private string _XMLSurvey = "";
        public string XMLSurvey
        {
            get { return _XMLSurvey; }
            set { _XMLSurvey = value; }
        }

        private int _Version = 0;
        public int Version
        {
            get { return _Version; }
            set { _Version = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }


        public SurveyDefinition(int SurveyID, string SurveyName, string XMLSurvey, int Version, int FacilityID)
        {
            this.SurveyID = SurveyID;
            this.SurveyName = SurveyName;
            this.XMLSurvey = XMLSurvey;
            this.Version = Version;
            this.FacilityID = FacilityID;
        }

        public bool Delete()
        {
            bool success = SurveyDefinition.DeleteSurveyDefinition(this.SurveyID);
            if (success)
                this.SurveyID = 0;
            return success;
        }

        public bool Update()
        {
            return SurveyDefinition.UpdateSurveyDefinition(this.SurveyID, this.SurveyName, this.XMLSurvey,
                this.Version, this.FacilityID);
        }
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all SurveyDefinitions
        /// </summary>
        public static List<SurveyDefinition> GetSurveyDefinitions(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "SurveyName";

            List<SurveyDefinition> SurveyDefinitions = null;
            string key = "SurveyDefinitions_SurveyDefinitions_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SurveyDefinitions = (List<SurveyDefinition>)BizObject.Cache[key];
            }
            else
            {
                List<SurveyDefinitionInfo> recordset = SiteProvider.PR2.GetSurveyDefinitions(cSortExpression);
                SurveyDefinitions = GetSurveyDefinitionListFromSurveyDefinitionInfoList(recordset);
                BasePR.CacheData(key, SurveyDefinitions);
            }
            return SurveyDefinitions;
        }


        /// <summary>
        /// Returns the number of total SurveyDefinitions
        /// </summary>
        public static int GetSurveyDefinitionCount()
        {
            int SurveyDefinitionCount = 0;
            string key = "SurveyDefinitions_SurveyDefinitionCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SurveyDefinitionCount = (int)BizObject.Cache[key];
            }
            else
            {
                SurveyDefinitionCount = SiteProvider.PR2.GetSurveyDefinitionCount();
                BasePR.CacheData(key, SurveyDefinitionCount);
            }
            return SurveyDefinitionCount;
        }

        /// <summary>
        /// Returns a SurveyDefinition object with the specified ID
        /// </summary>
        public static SurveyDefinition GetSurveyDefinitionByID(int SurveyID)
        {
            SurveyDefinition SurveyDefinition = null;
            string key = "SurveyDefinitions_SurveyDefinition_" + SurveyID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SurveyDefinition = (SurveyDefinition)BizObject.Cache[key];
            }
            else
            {
                SurveyDefinition = GetSurveyDefinitionFromSurveyDefinitionInfo(SiteProvider.PR2.GetSurveyDefinitionByID(SurveyID));
                BasePR.CacheData(key, SurveyDefinition);
            }
            return SurveyDefinition;
        }

        /// <summary>
        /// Returns a SurveyDefinition object with the specified ID
        /// </summary>
        public static SurveyDefinition GetSurveyDefinitionBySurveyName(string SurveyName)
        {
            SurveyDefinition SurveyDefinition = null;
            string key = "SurveyDefinitions_SurveyDefinition_" + SurveyName.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SurveyDefinition = (SurveyDefinition)BizObject.Cache[key];
            }
            else
            {                
                SurveyDefinition = GetSurveyDefinitionFromSurveyDefinitionInfo(SiteProvider.PR2.GetSurveyDefinitionBySurveyName(SurveyName));
                BasePR.CacheData(key, SurveyDefinition);
            }
            return SurveyDefinition;
        }

        ///// <summary>
        ///// Bhaskar 
        ///// Returns a TestDefinition object associated with the specified TopicID
        ///// </summary>
        public static SurveyDefinition GetSurveyDefinitionByTopicID(int TopicID)
        {
            SurveyDefinition SurveyDefinition = null;
            string key = "SurveyDefinitions_SurveyDefinition_TopicID_" + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SurveyDefinition = (SurveyDefinition)BizObject.Cache[key];
            }
            else
            {
                SurveyDefinition = GetSurveyDefinitionFromSurveyDefinitionInfo(SiteProvider.PR2.GetSurveyDefinitionByTopicID(TopicID));
                BasePR.CacheData(key, SurveyDefinition);
            }
            return SurveyDefinition;
        }

        /// <summary>
        /// Updates an existing SurveyDefinition
        /// </summary>
        public static bool UpdateSurveyDefinition(int SurveyID, string SurveyName, string XMLSurvey,
            int Version, int FacilityID)
        {
            XMLSurvey = BizObject.ConvertNullToEmptyString(XMLSurvey);


            SurveyDefinitionInfo record = new SurveyDefinitionInfo(SurveyID, SurveyName, XMLSurvey, Version, FacilityID);
            bool ret = SiteProvider.PR2.UpdateSurveyDefinition(record);

            BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinition_" + SurveyID.ToString());
            BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinitions");
            return ret;
        }

        /// <summary>
        /// Creates a new SurveyDefinition
        /// </summary>
        public static int InsertSurveyDefinition(string SurveyName, string XMLSurvey, int Version, int FacilityID)
        {
            SurveyName = BizObject.ConvertNullToEmptyString(SurveyName);
            XMLSurvey = BizObject.ConvertNullToEmptyString(XMLSurvey);


            SurveyDefinitionInfo record = new SurveyDefinitionInfo(0, SurveyName, XMLSurvey, Version, 0);
            int ret = SiteProvider.PR2.InsertSurveyDefinition(record);

            BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinition");
            return ret;
        }

        /// <summary>
        /// Deletes an existing SurveyDefinition, but first checks if OK to delete
        /// </summary>
        public static bool DeleteSurveyDefinition(int SurveyID)
        {
            bool IsOKToDelete = OKToDelete(SurveyID);
            if (IsOKToDelete)
            {
                return (bool)DeleteSurveyDefinition(SurveyID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Updates an existing SurveyDefinition
        /// </summary>
        public static bool UpdateSurveyDefinition(int SurveyID, string XMLSurvey, int Version, int FacilityID)
        {
            XMLSurvey = BizObject.ConvertNullToEmptyString(XMLSurvey);


            //SurveyDefinitionInfo record = new SurveyDefinitionInfo(SurveyID, XMLSurvey, Version, FacilityID);
            bool ret = SiteProvider.PR2.UpdateSurveyDefinition(SurveyID, XMLSurvey, Version, FacilityID);

            BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinition1_" + SurveyID.ToString());
            BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinitions");
            return ret;
        }

        /// <summary>
        /// Updates an existing SurveyDefinition
        /// </summary>
        public static bool UpdateSurveyDefBySurveyName(int SurveyID, string SurveyName, int Version, int FacilityID)
        {

            //SurveyDefinitionInfo record = new SurveyDefinitionInfo(SurveyID, XMLSurvey, Version, FacilityID);
            bool ret = SiteProvider.PR2.UpdateSurveyDefBySurveyName(SurveyID, SurveyName, Version, FacilityID);

            BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinitionName_" + SurveyID.ToString());
            BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinitionName");
            return ret;
        }

        /// <summary>
        /// Deletes an existing SurveyDefinition - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteSurveyDefinition(int SurveyID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteSurveyDefinition(SurveyID);
            //         new RecordDeletedEvent("SurveyDefinition", SurveyID, null).Raise();
            BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinition");
            return ret;
        }



        /// <summary>
        /// Checks to see if a SurveyDefinition can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int SurveyID)
        {
            return true;
        }



        /// <summary>
        /// Returns a SurveyDefinition object filled with the data taken from the input SurveyDefinitionInfo
        /// </summary>
        private static SurveyDefinition GetSurveyDefinitionFromSurveyDefinitionInfo(SurveyDefinitionInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new SurveyDefinition(record.SurveyID, record.SurveyName, record.XMLSurvey,
                    record.Version, record.FacilityID);
            }
        }

        /// <summary>
        /// Returns a list of SurveyDefinition objects filled with the data taken from the input list of SurveyDefinitionInfo
        /// </summary>
        private static List<SurveyDefinition> GetSurveyDefinitionListFromSurveyDefinitionInfoList(List<SurveyDefinitionInfo> recordset)
        {
            List<SurveyDefinition> SurveyDefinitions = new List<SurveyDefinition>();
            foreach (SurveyDefinitionInfo record in recordset)
                SurveyDefinitions.Add(GetSurveyDefinitionFromSurveyDefinitionInfo(record));
            return SurveyDefinitions;
        }

        #endregion
    }
}
