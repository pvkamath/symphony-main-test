﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    ////////////////////////////////////////////////////////////
    /// <summary>
    /// LectureEventResourceLink business object class
    /// </summary>
    public class LectureEventResourceLink : BasePR
    {
        #region Variables and Properties

        private int _LectEvtResID = 0;
        public int LectEvtResID
        {
            get { return _LectEvtResID; }
            protected set { _LectEvtResID = value; }
        }

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            set { _LectEvtID = value; }
        }

        private int _ResourceID = 0;
        public int ResourceID
        {
            get { return _ResourceID; }
            set { _ResourceID = value; }
        }

        private int _Sequence = 0;
        public int Sequence
        {
            get { return _Sequence; }
            set { _Sequence = value; }
        }


        #endregion

        #region Methods

        public LectureEventResourceLink(int LectEvtResID, int LectEvtID, int ResourceID)
        {
            this.LectEvtResID = LectEvtResID;
            this.LectEvtID = LectEvtID;
            this.ResourceID = ResourceID;
           
        }

        public bool Delete()
        {
            bool success = LectureEventResourceLink.DeleteLectureEventResourceLink(this.LectEvtResID);
            if (success)
                this.LectEvtResID = 0;
            return success;
        }

        public bool Update()
        {
            return LectureEventResourceLink.UpdateLectureEventResourceLink(this.LectEvtResID, this.LectEvtID, this.ResourceID);
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all LectureEventResourceLinks
        /// </summary>
        public static List<LectureEventResourceLink> GetLectureEventResourceLinks(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<LectureEventResourceLink> LectureEventResourceLinks = null;
            string key = "LectureEventResourceLinks_LectureEventResourceLinks_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEventResourceLinks = (List<LectureEventResourceLink>)BizObject.Cache[key];
            }
            else
            {
                List<LectureEventResourceLinkInfo> recordset = SiteProvider.PR2.GetLectureEventResourceLinks(cSortExpression);
                LectureEventResourceLinks = GetLectureEventResourceLinkListFromLectureEventResourceLinkInfoList(recordset);
                BasePR.CacheData(key, LectureEventResourceLinks);
            }
            return LectureEventResourceLinks;
        }


        public static List<LectureEventResourceLink> GetLectureEventResourceLinksByLectEvtID(int LectEvtID)
        {
          

            List<LectureEventResourceLink> LectureEventResourceLinks = null;
            string key = "LectureEventResourceLinks_LectureEventResourceLinks_" + LectEvtID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEventResourceLinks = (List<LectureEventResourceLink>)BizObject.Cache[key];
            }
            else
            {
                List<LectureEventResourceLinkInfo> recordset = SiteProvider.PR2.GetLectureEventResourceLinksByLectEvtID(LectEvtID);
                LectureEventResourceLinks = GetLectureEventResourceLinkListFromLectureEventResourceLinkInfoList(recordset);
                BasePR.CacheData(key, LectureEventResourceLinks);
            }
            return LectureEventResourceLinks;
        }



        /// <summary>
        /// Returns the number of total LectureEventResourceLinks
        /// </summary>
        public static int GetLectureEventResourceLinkCount()
        {
            int LectureEventResourceLinkCount = 0;
            string key = "LectureEventResourceLinks_LectureEventResourceLinkCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEventResourceLinkCount = (int)BizObject.Cache[key];
            }
            else
            {
                LectureEventResourceLinkCount = SiteProvider.PR2.GetLectureEventResourceLinkCount();
                BasePR.CacheData(key, LectureEventResourceLinkCount);
            }
            return LectureEventResourceLinkCount;
        }

        /// <summary>
        /// Returns a LectureEventResourceLink object with the specified ID
        /// </summary>
        public static LectureEventResourceLink GetLectureEventResourceLinkByID(int LectEvtResID)
        {
            LectureEventResourceLink LectureEventResourceLink = null;
            string key = "LectureEventResourceLinks_LectureEventResourceLink_" + LectEvtResID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEventResourceLink = (LectureEventResourceLink)BizObject.Cache[key];
            }
            else
            {
                LectureEventResourceLink = GetLectureEventResourceLinkFromLectureEventResourceLinkInfo(SiteProvider.PR2.GetLectureEventResourceLinkByID(LectEvtResID));
                BasePR.CacheData(key, LectureEventResourceLink);
            }
            return LectureEventResourceLink;
        }

        /// <summary>
        /// Updates an existing LectureEventResourceLink
        /// </summary>
        public static bool UpdateLectureEventResourceLink(int LectEvtResID, int LectEvtID, int ResourceID)
        {


            LectureEventResourceLinkInfo record = new LectureEventResourceLinkInfo(LectEvtResID, LectEvtID, ResourceID);
            bool ret = SiteProvider.PR2.UpdateLectureEventResourceLink(record);

            BizObject.PurgeCacheItems("LectureEventResourceLinks_LectureEventResourceLink_" + LectEvtResID.ToString());
            BizObject.PurgeCacheItems("LectureEventResourceLinks_LectureEventResourceLinks");
            return ret;
        }

        public static bool UpdateLectureEventResourcePersonAssignments(int LectEvtID, string ResourceIDAssignments)
        {
            bool ret = SiteProvider.PR2.UpdateLectureEventResourcePersonAssignments(LectEvtID, ResourceIDAssignments);
            // TODO: release cache?
            return ret;
        }

        public static int CheckForLectureEventResource(string LectEvtID)
        {
            int ret = SiteProvider.PR2.CheckForLectureEventResource(LectEvtID);
            return ret;
        }


        /// <summary>
        /// Creates a new LectureEventResourceLink
        /// </summary>
        public static int InsertLectureEventResourceLink(int LectEvtID, int ResourceID)
        {


            LectureEventResourceLinkInfo record = new LectureEventResourceLinkInfo(0, LectEvtID, ResourceID);
            int ret = SiteProvider.PR2.InsertLectureEventResourceLink(record);

            BizObject.PurgeCacheItems("LectureEventResourceLinks_LectureEventResourceLink");
            return ret;
        }

        //public static int InsertLectureEventResourceLink(int lectEvtID, int resourceID)

        //{
        //   int ret = SiteProvider.PR2.InsertLectureEventResourceLink(lectEvtID,resourceID);

        //    return ret;
        
        
        //}
        /// <summary>
        /// Deletes an existing LectureEventResourceLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteLectureEventResourceLink(int LectEvtResID)
        {
            bool IsOKToDelete = OKToDelete(LectEvtResID);
            if (IsOKToDelete)
            {
                return (bool)DeleteLectureEventResourceLink(LectEvtResID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing LectureEventResourceLink - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteLectureEventResourceLink(int LectEvtResID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteLectureEventResourceLink(LectEvtResID);
            //         new RecordDeletedEvent("LectureEventResourceLink", LectEvtResID, null).Raise();
            BizObject.PurgeCacheItems("LectureEventResourceLinks_LectureEventResourceLink");
            return ret;
        }



        /// <summary>
        /// Checks to see if a LectureEventResourceLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int LectEvtResID)
        {
            return true;
        }



        /// <summary>
        /// Returns a LectureEventResourceLink object filled with the data taken from the input LectureEventResourceLinkInfo
        /// </summary>
        private static LectureEventResourceLink GetLectureEventResourceLinkFromLectureEventResourceLinkInfo(LectureEventResourceLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new LectureEventResourceLink(record.LectEvtResID, record.LectEvtID, record.ResourceID);
            }
        }

        /// <summary>
        /// Returns a list of LectureEventResourceLink objects filled with the data taken from the input list of LectureEventResourceLinkInfo
        /// </summary>
        private static List<LectureEventResourceLink> GetLectureEventResourceLinkListFromLectureEventResourceLinkInfoList(List<LectureEventResourceLinkInfo> recordset)
        {
            List<LectureEventResourceLink> LectureEventResourceLinks = new List<LectureEventResourceLink>();
            foreach (LectureEventResourceLinkInfo record in recordset)
                LectureEventResourceLinks.Add(GetLectureEventResourceLinkFromLectureEventResourceLinkInfo(record));
            return LectureEventResourceLinks;
        }


        #endregion
    }
}