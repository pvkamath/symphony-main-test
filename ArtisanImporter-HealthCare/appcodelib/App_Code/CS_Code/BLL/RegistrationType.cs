﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 
/// <summary>
/// Summary description for RegistrationType
/// </summary>
public class RegistrationType: BasePR
    {
        #region Variables and Properties

        private int _RegTypeID = 0;
        public int RegTypeID
        {
            get { return _RegTypeID; }
            protected set { _RegTypeID = value; }
        }

        private string _RegType = "";
        public string RegType
        {
            get { return _RegType; }
            set { _RegType = value; }
        }



        public RegistrationType(int RegTypeID, string RegType)
        {
            this.RegTypeID = RegTypeID;
            this.RegType = RegType;
        }

        public bool Delete()
        {
            bool success = RegistrationType.DeleteRegistrationType(this.RegTypeID);
            if (success)
                this.RegTypeID = 0;
            return success;
        }

        public bool Update()
        {
            return RegistrationType.UpdateRegistrationType(this.RegTypeID, this.RegType);
        }
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all RegistrationTypes
        /// </summary>
        public static List<RegistrationType> GetRegistrationTypes(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "RegType";

            List<RegistrationType> RegistrationTypes = null;
            string key = "RegistrationTypes_RegistrationTypes_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                RegistrationTypes = (List<RegistrationType>)BizObject.Cache[key];
            }
            else
            {
                List<RegistrationTypeInfo> recordset = SiteProvider.PR2.GetRegistrationTypes(cSortExpression);
                RegistrationTypes = GetRegistrationTypeListFromRegistrationTypeInfoList(recordset);
                BasePR.CacheData(key, RegistrationTypes);
            }
            return RegistrationTypes;
        }


        /// <summary>
        /// Returns the number of total RegistrationTypes
        /// </summary>
        public static int GetRegistrationTypeCount()
        {
            int RegistrationTypeCount = 0;
            string key = "RegistrationTypes_RegistrationTypeCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                RegistrationTypeCount = (int)BizObject.Cache[key];
            }
            else
            {
                RegistrationTypeCount = SiteProvider.PR2.GetRegistrationTypeCount();
                BasePR.CacheData(key, RegistrationTypeCount);
            }
            return RegistrationTypeCount;
        }

        /// <summary>
        /// Returns a RegistrationType object with the specified ID
        /// </summary>
        public static RegistrationType GetRegistrationTypeByID(int RegTypeID)
        {
            RegistrationType RegistrationType = null;
            string key = "RegistrationTypes_RegistrationType_" + RegTypeID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                RegistrationType = (RegistrationType)BizObject.Cache[key];
            }
            else
            {
                RegistrationType = GetRegistrationTypeFromRegistrationTypeInfo(SiteProvider.PR2.GetRegistrationTypeByID(RegTypeID));
                BasePR.CacheData(key, RegistrationType);
            }
            return RegistrationType;
        }

        /// <summary>
        /// Updates an existing RegistrationType
        /// </summary>
        public static bool UpdateRegistrationType(int RegTypeID, string RegType)
        {
            RegType = BizObject.ConvertNullToEmptyString(RegType);


            RegistrationTypeInfo record = new RegistrationTypeInfo(RegTypeID, RegType);
            bool ret = SiteProvider.PR2.UpdateRegistrationType(record);

            BizObject.PurgeCacheItems("RegistrationTypes_RegistrationType_" + RegTypeID.ToString());
            BizObject.PurgeCacheItems("RegistrationTypes_RegistrationTypes");
            return ret;
        }

        /// <summary>
        /// Creates a new RegistrationType
        /// </summary>
        public static int InsertRegistrationType(string RegType)
        {
            RegType = BizObject.ConvertNullToEmptyString(RegType);


            RegistrationTypeInfo record = new RegistrationTypeInfo(0, RegType);
            int ret = SiteProvider.PR2.InsertRegistrationType(record);

            BizObject.PurgeCacheItems("RegistrationTypes_RegistrationType");
            return ret;
        }

        /// <summary>
        /// Deletes an existing RegistrationType, but first checks if OK to delete
        /// </summary>
        public static bool DeleteRegistrationType(int RegTypeID)
        {
            bool IsOKToDelete = OKToDelete(RegTypeID);
            if (IsOKToDelete)
            {
                return (bool)DeleteRegistrationType(RegTypeID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing RegistrationType - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteRegistrationType(int RegTypeID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteRegistrationType(RegTypeID);
            //         new RecordDeletedEvent("RegistrationType", RegTypeID, null).Raise();
            BizObject.PurgeCacheItems("RegistrationTypes_RegistrationType");
            return ret;
        }



        /// <summary>
        /// Checks to see if a RegistrationType can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int RegTypeID)
        {
            return true;
        }



        /// <summary>
        /// Returns a RegistrationType object filled with the data taken from the input RegistrationTypeInfo
        /// </summary>
        private static RegistrationType GetRegistrationTypeFromRegistrationTypeInfo(RegistrationTypeInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new RegistrationType(record.RegTypeID, record.RegType);
            }
        }

        /// <summary>
        /// Returns a list of RegistrationType objects filled with the data taken from the input list of RegistrationTypeInfo
        /// </summary>
        private static List<RegistrationType> GetRegistrationTypeListFromRegistrationTypeInfoList(List<RegistrationTypeInfo> recordset)
        {
            List<RegistrationType> RegistrationTypes = new List<RegistrationType>();
            foreach (RegistrationTypeInfo record in recordset)
                RegistrationTypes.Add(GetRegistrationTypeFromRegistrationTypeInfo(record));
            return RegistrationTypes;
        }

        #endregion
    }
}
