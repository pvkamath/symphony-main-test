﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for AreaType
/// </summary>
public class AreaType: BasePR
    {
        #region Variables and Properties

        private int _areaid = 0;
        public int areaid
        {
            get { return _areaid; }
            protected set { _areaid = value; }
        }

        private string _areaname = "";
        public string areaname
        {
            get { return _areaname; }
            set { _areaname = value; }

        }

        private string _tabname = "";
        public string tabname
        {
            get { return _tabname; }
            set { _tabname = value; }

        }

        private string _instruction = "";
        public string instruction
        {
            get { return _instruction; }
            set { _instruction = value; }
        }

        public AreaType(int areaid, string areaname, string tabname, string instruction)
        {
            this.areaid = areaid;
            this.areaname = areaname;
            this.tabname = tabname;
            this.instruction = instruction;
        }

        #endregion

        #region Methods

        /***********************************
       * Static methods
       ************************************/

        /// <summary>
        /// Returns a collection with all AreaTypes
        /// </summary>
        public static List<AreaType> GetAreaTypes(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypes(cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }

     /// <summary>
        /// Returns a collection with all AreaTypes
        /// </summary>
        public static List<AreaType> GetAreaTypesForDropDown(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "tabname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_DD" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypesForDropDown(cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }

        /// <summary>
        /// Returns a collection with all AreaTypes
        /// </summary>
        public static List<AreaType> GetCategoryAreaTypesForDropDown(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "tabname";

            List<AreaType> AreaTypes = null;
            string key = "Category_AreaTypes_AreaTypes_DD" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetCategoryAreaTypesForDropDown(cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }

        public static List<AreaType> GetUniqueAreaTypes(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "Unique_AreaTypes" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetUniqueAreaTypes(cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }


        /// <summary>
        /// Returns a collection with all Facility AreaTypes
        /// </summary>
        public static List<AreaType> GetFacilityAreaTypesForDropDown(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "tabname";

            List<AreaType> AreaTypes = null;
            string key = "Category_Facility_AreaTypes_DD" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetFacilityAreaTypesForDropDown(cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }
    

        /// <summary>
        /// Returns a collection with all AreaTypes
        /// </summary>
        public static List<AreaType> GetFacilityAreaTypes(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "FacAreaTypes_AreaTypes_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetFacilityAreaTypes(cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }


        public static List<AreaType> GetAreaTypesByAreaID(int AreaID,string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_" + AreaID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypesByAreaID(AreaID,cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }


        public static List<AreaType> GetAreaTypesByAreaName(String AreaName, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_" + AreaName.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypesByAreaName(AreaName,cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }

        public static List<AreaType> GetAreaTypesByAreaNames(String AreaNames, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_" + AreaNames.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypesByAreaNames(AreaNames, cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }

        public static List<AreaType> GetAreaTypesByAreaNamesForCEDirect(String AreaNames, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_" + AreaNames.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypesByAreaNamesForCEDirect(AreaNames, cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }

        public static List<AreaType> GetAreaTypesByAreaNameAndFacId(String AreaName,int FacId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_" + FacId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypesByAreaNameAndFacId(AreaName,FacId, cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }

        public static List<AreaType> GetAreaTypesAndAreaNamesByFacId(int FacId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_" + FacId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypesAndAreaNamesByFacId(FacId, cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }


        public static List<AreaType> GetAreaTypesAndTabNamesByAreaName(String AreaName, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_TabNames_" + AreaName.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypesAndTabNamesByAreaName(AreaName, cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }

        public static List<AreaType> GetAreaTypeInstructions(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_Instructions_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypeInstructions(cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }

        public static List<AreaType> GetAreaTypesByDomainId(int DomainId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_" + DomainId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypesByDomainId(DomainId, cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }

        public static List<AreaType> GetAreaTypesByDomainIdAndUserId(int DomainId, int UserId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_" + DomainId.ToString() + UserId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypesByDomainIdAndUserId(DomainId,UserId, cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }
        public static List<AreaType> GetAreaTypeInstructionsByUserId(int UserId,string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "areaname";

            List<AreaType> AreaTypes = null;
            string key = "AreaTypes_AreaTypes_Instructions_" + UserId.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaTypes = (List<AreaType>)BizObject.Cache[key];
            }
            else
            {
                List<AreaTypeInfo> recordset = SiteProvider.PR2.GetAreaTypeInstructionsByUserId(UserId,cSortExpression);
                AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
                BasePR.CacheData(key, AreaTypes);
            }
            return AreaTypes;
        }


        /// <summary>
        /// Returns a AreaType object filled with the data taken from the input AreaTypeInfo
        /// </summary>
        private static AreaType GetAreaTypeFromAreaTypeInfo(AreaTypeInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new AreaType(record.areaid, record.areaname, record.tabname,record.instruction);
            }
        }

        /// <summary>
        /// Returns a list of AreaType objects filled with the data taken from the input list of AreaTypeInfo
        /// </summary>
        private static List<AreaType> GetAreaTypeListFromAreaTypeInfoList(List<AreaTypeInfo> recordset)
        {
            List<AreaType> AreaTypes = new List<AreaType>();
            foreach (AreaTypeInfo record in recordset)
                AreaTypes.Add(GetAreaTypeFromAreaTypeInfo(record));
            return AreaTypes;
        }

        #endregion
    }
}
