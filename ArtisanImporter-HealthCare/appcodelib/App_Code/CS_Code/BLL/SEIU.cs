﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using PearlsReview.Constants;
/// <summary>
/// Summary description for SEIU
/// </summary>
public class SEIU
{
	public SEIU()
	{
        XmlSerializer xmlserializer = new XmlSerializer(typeof(SEIUEmployer));
        XmlTextReader xml_reader = default(XmlTextReader);
        xml_reader = new XmlTextReader(File.OpenRead(HttpContext.Current.Server.MapPath(CommonConstants.SEIUXMLPath)));
        object DeserializedObject;
        if ((xmlserializer.CanDeserialize(xml_reader)))
        {
            DeserializedObject = xmlserializer.Deserialize(xml_reader);
        }
 
	}   
}

public class SEIUEmployer
{
    [XmlElement("Region")]
    public string Region { get; set; }

    [XmlElement("EmployerName")]
    public string EmployerName { get; set; }  
}



public class SEIUFacility
{
    [XmlElement("Employer")]
    public string Employer { get; set; }

    [XmlElement("FacilityName")]
    public string FacilityName { get; set; }
}
