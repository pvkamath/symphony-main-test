﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for ParentOrganization
    /// </summary>
    public class ParentOrganization : BasePR
    {
        #region Variables and Properties

        private int _Parent_id = 0;
        public int Parent_id
        {
            get { return _Parent_id; }
            protected set { _Parent_id = value; }
        }

        private string _Parent_name = "";
        public string Parent_name
        {
            get { return _Parent_name; }
            set { _Parent_name = value; }
        }

        private string _address1 = "";
        public string address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        private string _address2 = "";
        public string address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        private string _city = "";
        public string city
        {
            get { return _city; }
            set { _city = value; }
        }

        private string _state = "";
        public string state
        {
            get { return _state; }
            set { _state = value; }
        }

        private string _zipcode = "";
        public string zipcode
        {
            get { return _zipcode; }
            set { _zipcode = value; }
        }

        private string _phone = "";
        public string phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        private string _contact_name = "";
        public string contact_name
        {
            get { return _contact_name; }
            set { _contact_name = value; }
        }

        private string _contact_phone = "";
        public string contact_phone
        {
            get { return _contact_phone; }
            set { _contact_phone = value; }
        }

        private string _contact_ext = "";
        public string contact_ext
        {
            get { return _contact_ext; }
            set { _contact_ext = value; }
        }

        private string _contact_email = "";
        public string contact_email
        {
            get { return _contact_email; }
            set { _contact_email = value; }
        }

        private bool _active = true;
        public bool active
        {
            get { return _active; }
            set { _active = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        private int _MaxUsers = 1000;
        public int MaxUsers
        {
            get { return _MaxUsers; }
            set { _MaxUsers = value; }
        }

        private DateTime _Started = System.DateTime.MinValue;
        public DateTime Started
        {
            get { return _Started; }
            set { _Started = value; }
        }

        private DateTime _Expires = System.DateTime.MaxValue;
        public DateTime Expires
        {
            get { return _Expires; }
            set { _Expires = value; }
        }


        public ParentOrganization(int Parent_id, string Parent_name, string address1, string address2, string city, string state,
           string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
           string contact_email, bool active, string comment, int MaxUsers, DateTime Started,
            DateTime Expires)
        {
            this.Parent_id = Parent_id;
            this.Parent_name = Parent_name;
            this.address1 = address1;
            this.address2 = address2;
            this.city = city;
            this.state = state;
            this.zipcode = zipcode;
            this.phone = phone;
            this.contact_name = contact_name;
            this.contact_phone = contact_phone;
            this.contact_ext = contact_ext;
            this.contact_email = contact_email;
            this.active = active;
            this.Comment = comment;
            this.MaxUsers = MaxUsers;
            this.Started = Started;
            this.Expires = Expires;
        }

        public bool Delete()
        {
            bool success = ParentOrganization.DeleteParentOrganization(this.Parent_id);
            if (success)
                this.Parent_id = 0;
            return success;
        }

        public bool Update()
        {
            return ParentOrganization.UpdateParentOrganization(this.Parent_id, this.Parent_name, this.address1, this.address2, this.city, this.state,
           this.zipcode, this.phone, this.contact_name, this.contact_phone, this.contact_ext,
           this.contact_email, this.active, this.Comment, this.MaxUsers,
                this.Started, this.Expires);
        }
        #endregion

        #region Methods

        /// <summary>
        /// Returns a collection with all ParentOrganizations
        /// </summary>
        public static List<ParentOrganization> GetParentOrganizations(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Parent_name";

            List<ParentOrganization> ParentOrganizations = null;
            string key = "ParentOrganizations_ParentOrganizations_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ParentOrganizations = (List<ParentOrganization>)BizObject.Cache[key];
            }
            else
            {
                List<ParentOrganizationInfo> recordset = SiteProvider.PR2.GetParentOrganizations(cSortExpression);
                ParentOrganizations = GetParentOrganizationListFromParentOrganizationInfoList(recordset);
                BasePR.CacheData(key, ParentOrganizations);
            }
            return ParentOrganizations;
        }

        /// <summary>
        /// Returns the number of total ParentOrganizations
        /// </summary>
        public static int GetParentOrganizationCount()
        {
            int ParentOrganizationCount = 0;
            string key = "ParentOrganizations_ParentOrganizationCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ParentOrganizationCount = (int)BizObject.Cache[key];
            }
            else
            {
                ParentOrganizationCount = SiteProvider.PR2.GetParentOrganizationCount();
                BasePR.CacheData(key, ParentOrganizationCount);
            }
            return ParentOrganizationCount;
        }

        /// <summary>
        /// Returns a ParentOrganization object with the specified ID
        /// </summary>
        public static ParentOrganization GetParentOrganizationByID(int Parent_id)
        {
            ParentOrganization ParentOrganization = null;
            string key = "ParentOrganizations_ParentOrganization_" + Parent_id.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ParentOrganization = (ParentOrganization)BizObject.Cache[key];
            }
            else
            {
                ParentOrganization = GetParentOrganizationFromParentOrganizationInfo(SiteProvider.PR2.GetParentOrganizationByID(Parent_id));
                BasePR.CacheData(key, ParentOrganization);
            }
            return ParentOrganization;
        }

        /// <summary>
        /// Updates an existing ParentOrganization
        /// </summary>
        public static bool UpdateParentOrganization(int Parent_id, string Parent_name, string address1, string address2, string city, string state,
           string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
           string contact_email, bool active, string Comment, int MaxUsers, DateTime Started,
            DateTime Expires)
        {
            Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
            Comment = BizObject.ConvertNullToEmptyString(Comment);

            ParentOrganizationInfo record = new ParentOrganizationInfo(Parent_id, Parent_name, address1, address2, city, state,
               zipcode, phone, contact_name, contact_phone, contact_ext,
               contact_email, active, Comment, MaxUsers, Started,
                Expires);
            bool ret = SiteProvider.PR2.UpdateParentOrganization(record);

            BizObject.PurgeCacheItems("ParentOrganizations_ParentOrganization_" + Parent_id.ToString());
            BizObject.PurgeCacheItems("ParentOrganizations_ParentOrganizations");
            return ret;
        }

        /// <summary>
        /// Creates a new ParentOrganization
        /// </summary>
        public static int InsertParentOrganization(string Parent_name, string address1, string address2, string city, string state,
           string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
           string contact_email, bool active, string Comment, int MaxUsers, DateTime Started,
            DateTime Expires)
        {
            Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
            Comment = BizObject.ConvertNullToEmptyString(Comment);

            ParentOrganizationInfo record = new ParentOrganizationInfo(0, Parent_name, address1, address2, city, state,
               zipcode, phone, contact_name, contact_phone, contact_ext,
               contact_email, active, Comment, MaxUsers, Started,
                Expires);
            int ret = SiteProvider.PR2.InsertParentOrganization(record);

            BizObject.PurgeCacheItems("ParentOrganizations_ParentOrganization");
            return ret;
        }

        /// <summary>
        /// Deletes an existing ParentOrganization, but first checks if OK to delete
        /// </summary>
        public static bool DeleteParentOrganization(int Parent_id)
        {
            bool IsOKToDelete = OKToDelete(Parent_id);
            if (IsOKToDelete)
            {
                return (bool)DeleteParentOrganization(Parent_id, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing ParentOrganization - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteParentOrganization(int Parent_id, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteParentOrganization(Parent_id);
            BizObject.PurgeCacheItems("ParentOrganizations_ParentOrganization");
            return ret;
        }



        /// <summary>
        /// Checks to see if a ParentOrganization can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int Parent_id)
        {
            return true;
        }



        /// <summary>
        /// Returns a ParentOrganization object filled with the data taken from the input ParentOrganizationInfo
        /// </summary>
        private static ParentOrganization GetParentOrganizationFromParentOrganizationInfo(ParentOrganizationInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new ParentOrganization(record.Parent_id, record.Parent_name, record.address1, record.address2, record.city, record.state,
           record.zipcode, record.phone, record.contact_name, record.contact_phone, record.contact_ext,
           record.contact_email, record.active, record.Comment, record.MaxUsers,
                record.Started, record.Expires);
            }
        }

        /// <summary>
        /// Returns a list of ParentOrganization objects filled with the data taken from the input list of ParentOrganizationInfo
        /// </summary>
        private static List<ParentOrganization> GetParentOrganizationListFromParentOrganizationInfoList(List<ParentOrganizationInfo> recordset)
        {
            List<ParentOrganization> ParentOrganizations = new List<ParentOrganization>();
            foreach (ParentOrganizationInfo record in recordset)
                ParentOrganizations.Add(GetParentOrganizationFromParentOrganizationInfo(record));
            return ParentOrganizations;
        }

        /// <summary>
        /// Returns the number of total UserAccounts for a FacilityID
        /// </summary>
        public static int GetUserAccountCountByParentFacilityID(int FacilityID)
        {
            int UserAccountCount = 0;
            string key = "ParentOrganization_UserAccountCount_FacilityID_" + FacilityID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserAccountCount = (int)BizObject.Cache[key];
            }
            else
            {
                UserAccountCount = SiteProvider.PR2.GetUserAccountCountByParentFacilityID(FacilityID);
                BasePR.CacheData(key, UserAccountCount);
            }
            return UserAccountCount;
        }


        #endregion
    }
}
