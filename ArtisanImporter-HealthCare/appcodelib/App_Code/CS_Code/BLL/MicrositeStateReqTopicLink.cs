﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using System.Collections.Generic;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for MicrositeStateReqTopicLink
    /// </summary>
    public class MicrositeStateReqTopicLink : BasePR
    {
        #region Variables and Properties
        private int _msrtID = 0;
        public int MsrtID
        {
            get { return _msrtID; }
            set { _msrtID = value; }
        }

        private int _msrID = 0;
        public int MsrID
        {
            get { return _msrID; }
            set { _msrID = value; }
        }

        private int _topicID = 0;
        public int TopicID
        {
            get { return _topicID; }
            set { _topicID = value; }
        }
        #endregion Variables and Properties

        #region Methods
        public MicrositeStateReqTopicLink() { }

        public MicrositeStateReqTopicLink(int msrtID, int msrID, int topicID) 
        {
            this.MsrtID = msrtID;
            this.MsrID = msrID;
            this.TopicID = topicID;
        }

        public static int insertMicrositeStateReqTopicLink(int msrID, int topicID)
        {
            MicrositeStateReqTopicLinkInfo msStateReqTopicLinkInfo = new MicrositeStateReqTopicLinkInfo(0, msrID, topicID);
            int ret = SiteProvider.PR2.insertMicrositeStateReqTopicLink(msStateReqTopicLinkInfo);

            BizObject.PurgeCacheItems("MicrositeStateReqTopicLink_MicrositeStateReqTopicLink");
            return ret;
        }

        public int insert()
        {
            return MicrositeStateReqTopicLink.insertMicrositeStateReqTopicLink(this.MsrID, this.TopicID);
        }

        public static bool deleteMicrositeStateReqTopicLink(int msrtID)
        {
            bool ret = SiteProvider.PR2.deleteMicrositeStateReqTopicLink(msrtID);
            BizObject.PurgeCacheItems("MicrositeStateReqTopicLink_MicrositeStateReqTopicLink");
            return ret;
        }

        public bool delete()
        {
            return MicrositeStateReqTopicLink.deleteMicrositeStateReqTopicLink(this.MsrtID);
        }

        public static MicrositeStateReqTopicLink getMicrositeStateReqTopicLinkByMsrIDAndTopicID(int msrID, int topicID)
        {
            MicrositeStateReqTopicLink msStateReqTopicLink = null;
            string key = "MicrositeStateReqTopicLink_MicrositeStateReqTopicLink_" + msrID.ToString() + "_" + topicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                msStateReqTopicLink = (MicrositeStateReqTopicLink)BizObject.Cache[key];
            }
            else
            {
                msStateReqTopicLink = getMicrositeStateReqTopicLinkFromInfo(
                    SiteProvider.PR2.getMicrositeStateReqTopicLinkByMsrIDAndTopicID(msrID, topicID));
                BasePR.CacheData(key, msStateReqTopicLink);
            }
            return msStateReqTopicLink;
        }

        private static MicrositeStateReqTopicLink getMicrositeStateReqTopicLinkFromInfo(MicrositeStateReqTopicLinkInfo info)
        {
            if (info == null)
                return null;
            else
                return new MicrositeStateReqTopicLink(info.MsrtID, info.MsrID, info.TopicID);
        }

        private static List<MicrositeStateReqTopicLink> getMicrositeStateReqTopicLinkListFromInfoList(
            List<MicrositeStateReqTopicLinkInfo> infoList)
        {
            List<MicrositeStateReqTopicLink> ret = new List<MicrositeStateReqTopicLink>();
            foreach (MicrositeStateReqTopicLinkInfo info in infoList)
                ret.Add(getMicrositeStateReqTopicLinkFromInfo(info));
            return ret;
        }
        #endregion Methods
    }
}
