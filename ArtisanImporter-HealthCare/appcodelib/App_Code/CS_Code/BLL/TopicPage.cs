﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

/// <summary>
/// Summary description for TopicPage
/// </summary>
public class TopicPage: BasePR
    {
        #region Variables and Properties

        private int _PageID = 0;
        public int PageID
        {
            get { return _PageID; }
            protected set { _PageID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _Page_Num = 0;
        public int Page_Num
        {
            get { return _Page_Num; }
            set { _Page_Num = value; }
        }

        private string _Page_Content = "";
        public string Page_Content
        {
            get { return _Page_Content; }
            set { _Page_Content = value; }
        }



        public TopicPage(int PageID, int TopicID, int Page_Num, string Page_Content)
        {
            this.PageID = PageID;
            this.TopicID = TopicID;
            this.Page_Num = Page_Num;
            this.Page_Content = Page_Content;
        }

        public bool Delete()
        {
            bool success = TopicPage.DeleteTopicPage(this.PageID);
            if (success)
                this.PageID = 0;
            return success;
        }

        public bool Update()
        {
            return TopicPage.UpdateTopicPage(this.PageID, this.TopicID, this.Page_Num, this.Page_Content);
        }
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all TopicPages
        /// </summary>
        public static List<TopicPage> GetTopicPages(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<TopicPage> TopicPages = null;
            string key = "TopicPages_TopicPages_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicPages = (List<TopicPage>)BizObject.Cache[key];
            }
            else
            {
                List<TopicPageInfo> recordset = SiteProvider.PR2.GetTopicPages(cSortExpression);
                TopicPages = GetTopicPageListFromTopicPageInfoList(recordset);
                BasePR.CacheData(key, TopicPages);
            }
            return TopicPages;
        }


        /// <summary>
        /// Returns the number of total TopicPages
        /// </summary>
        public static int GetTopicPageCount()
        {
            int TopicPageCount = 0;
            string key = "TopicPages_TopicPageCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicPageCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicPageCount = SiteProvider.PR2.GetTopicPageCount();
                BasePR.CacheData(key, TopicPageCount);
            }
            return TopicPageCount;
        }


        /// <summary>
        /// Returns the number of total TopicPages for a TopicID
        /// </summary>
        public static int GetTopicPageCountByTopicID(int TopicID)
        {
            int TopicPageCount = 0;
            string key = "TopicPages_TopicPageCountByTopicID_" + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicPageCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicPageCount = SiteProvider.PR2.GetTopicPageCountByTopicID(TopicID);
                BasePR.CacheData(key, TopicPageCount);
            }
            return TopicPageCount;
        }


        /// <summary>
        /// Returns a collection with all TopicPages for a TopicID
        /// </summary>
        public static List<TopicPage> GetTopicPagesByTopicID(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<TopicPage> TopicPages = null;
            string key = "TopicPages_TopicPagesByTopicID_" + TopicID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicPages = (List<TopicPage>)BizObject.Cache[key];
            }
            else
            {
                List<TopicPageInfo> recordset = SiteProvider.PR2.GetTopicPagesByTopicID(TopicID, cSortExpression);
                TopicPages = GetTopicPageListFromTopicPageInfoList(recordset);
                BasePR.CacheData(key, TopicPages);
            }
            return TopicPages;
        }


        /// <summary>
        /// Returns a TopicPage object with the specified ID
        /// </summary>
        public static TopicPage GetTopicPageByTopicIDAndPageNum(int TopicID, int Page_Num)
        {
            TopicPage TopicPage = null;
            string key = "TopicPages_TopicPageByTopicIDAndPageNum_" + TopicID.ToString() + "_" + Page_Num.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicPage = (TopicPage)BizObject.Cache[key];
            }
            else
            {
                TopicPage = GetTopicPageFromTopicPageInfo(SiteProvider.PR2.GetTopicPageByTopicIDAndPageNum(TopicID, Page_Num));
                BasePR.CacheData(key, TopicPage);
            }
            return TopicPage;
        }


        /// <summary>
        /// Returns a TopicPage object with the specified ID
        /// </summary>
        public static TopicPage GetTopicPageByID(int PageID)
        {
            TopicPage TopicPage = null;
            string key = "TopicPages_TopicPage_" + PageID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicPage = (TopicPage)BizObject.Cache[key];
            }
            else
            {
                TopicPage = GetTopicPageFromTopicPageInfo(SiteProvider.PR2.GetTopicPageByID(PageID));
                BasePR.CacheData(key, TopicPage);
            }
            return TopicPage;
        }

        /// <summary>
        /// Updates an existing TopicPage
        /// </summary>
        public static bool UpdateTopicPage(int PageID, int TopicID, int Page_Num, string Page_Content)
        {
            //Bsk test remove space..when you save..page content
            //Page_Content = Page_Content.Replace("<p>&nbsp;</p>", "");

            TopicPageInfo record = new TopicPageInfo(PageID, TopicID, Page_Num, Page_Content);
            bool ret = SiteProvider.PR2.UpdateTopicPage(record);

            BizObject.PurgeCacheItems("TopicPages_TopicPage_" + PageID.ToString());
            BizObject.PurgeCacheItems("TopicPages_TopicPages");
            return ret;
        }


        /// <summary>
        /// Creates a new TopicPage
        /// </summary>
        public static int InsertTopicPage(int TopicID, int Page_Num, string Page_Content)
        {


            TopicPageInfo record = new TopicPageInfo(0, TopicID, Page_Num, Page_Content);
            int ret = SiteProvider.PR2.InsertTopicPage(record);

            BizObject.PurgeCacheItems("TopicPages_TopicPage");
            return ret;
        }

        /// <summary>
        /// Deletes an existing TopicPage, but first checks if OK to delete
        /// </summary>
        public static bool DeleteTopicPage(int PageID)
        {
            bool IsOKToDelete = OKToDelete(PageID);
            if (IsOKToDelete)
            {


                TopicPage ObjTopicPage = TopicPage.GetTopicPageByID(PageID);
                List<TopicPage> objTopicPages = TopicPage.GetTopicPagesByTopicID(ObjTopicPage.TopicID, "page_num");
                if (objTopicPages.Count == ObjTopicPage.Page_Num)
                {
                    return (bool)DeleteTopicPage(PageID, true);
                }
                else
                {
                    for (int i = ObjTopicPage.Page_Num + 1; i <= objTopicPages.Count; i++)
                    {
                        TopicPage.UpdateTopicPage(objTopicPages[i - 1]._PageID, objTopicPages[i - 1].TopicID, i - 1, objTopicPages[i - 1].Page_Content);
                    }

                    return (bool)DeleteTopicPage(PageID, true);

                }

            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing TopicPage - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteTopicPage(int PageID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteTopicPage(PageID);
            //         new RecordDeletedEvent("TopicPage", PageID, null).Raise();
            BizObject.PurgeCacheItems("TopicPages_TopicPage");
            return ret;
        }



        /// <summary>
        /// Checks to see if a TopicPage can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int PageID)
        {
            return true;
        }



        /// <summary>
        /// Returns a TopicPage object filled with the data taken from the input TopicPageInfo
        /// </summary>
        private static TopicPage GetTopicPageFromTopicPageInfo(TopicPageInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new TopicPage(record.PageID, record.TopicID, record.Page_Num, record.Page_Content);
            }
        }

        /// <summary>
        /// Returns a list of TopicPage objects filled with the data taken from the input list of TopicPageInfo
        /// </summary>
        private static List<TopicPage> GetTopicPageListFromTopicPageInfoList(List<TopicPageInfo> recordset)
        {
            List<TopicPage> TopicPage = new List<TopicPage>();
            foreach (TopicPageInfo record in recordset)
                TopicPage.Add(GetTopicPageFromTopicPageInfo(record));
            return TopicPage;
        }

        #endregion
    }
}
