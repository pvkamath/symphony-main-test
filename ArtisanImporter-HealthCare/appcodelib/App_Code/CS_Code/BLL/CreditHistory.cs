﻿/*************************************************************************
 * OnCourse Learning,Inc
 * 
 * File Description:  
 * 
 * Author   :   Budgerel T
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for CreditHistory
    /// </summary>
    public class CreditHistory : BasePR
    {
        #region Variables and Properties

        private int _chID = 0;
        public int ChID
        {
            get { return _chID; }
            protected set { _chID = value; }
        }

        private int _creditID = 0;
        public int CreditID
        {
            get { return _creditID; }
            protected set { _creditID = value; }
        }

        private int _ruserID;
        public int r_UserID
        {
            get { return _ruserID; }
            private set { _ruserID = value; }
        }

        private DateTime _logDate;
        public DateTime LogDate
        {
            get { return _logDate; }
            private set { _logDate = value; }
        }

        private string _historyType;
        public string HistoryType
        {
            get { return _historyType; }
            private set { _historyType = value; }
        }

        private int _creditNumber;
        public int CreditNumber 
        {
            get { return _creditNumber; }
            private set { _creditNumber = value; }
        }

        private string _comment;
        public string Comment
        {
            get { return _comment; }
            private set { _comment = value; }
        }

        public CreditHistory(int ChID, int CreditID, int r_UserID, DateTime LogDate, string HistoryType, int CreditNumber, string Comment)
        {
            this.ChID = ChID;
            this.CreditID = CreditID;
            this.r_UserID = r_UserID;
            this.LogDate = LogDate;
            this.HistoryType = HistoryType;
            this.CreditNumber = CreditNumber;
            this.Comment = Comment;
        }

        public bool Delete()
        {
            bool success = CreditHistory.DeleteCreditHistory(this.ChID);
            if (success)
                this.ChID = 0;
            return success;
        }

        public bool Update()
        {
            return CreditHistory.UpdateCreditHistory(this.ChID, this.CreditID, this.r_UserID, this.LogDate, this.HistoryType, this.CreditNumber, this.Comment);
        }
        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all CreditHistory
        /// </summary>
        public static List<CreditHistory> GetCreditHistory(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<CreditHistory> CreditHistory = null;
            string key = "CreditHistory_CreditHistory_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CreditHistory = (List<CreditHistory>)BizObject.Cache[key];
            }
            else
            {
                List<CreditHistoryInfo> recordset = SiteProvider.PR2.GetCreditHistory(cSortExpression);
                CreditHistory = GetCreditHistoryListFromCreditHistoryInfoList(recordset);
                BasePR.CacheData(key, CreditHistory);
            }
            return CreditHistory;
        }

        /// <summary>
        /// Returns a collection with all CreditHistory
        /// </summary>
        public static List<CreditHistory> GetCreditHistoryByUserId(int UserId)
        {

            List<CreditHistory> CreditHistory = null;
            string key = "CreditHistory_CreditHistory_" + UserId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)

            {
                CreditHistory = (List<CreditHistory>)BizObject.Cache[key];
            }
            else
            {
                List<CreditHistoryInfo> recordset = SiteProvider.PR2.GetCreditHistoryByUserId(UserId);
                CreditHistory = GetCreditHistoryListFromCreditHistoryInfoList(recordset);
                BasePR.CacheData(key, CreditHistory);
            }
            return CreditHistory;
        }

        /// <summary>
        /// Returns the number of total CreditHistory
        /// </summary>
        public static int GetCreditHistoryCount()
        {
            int CreditHistoryCount = 0;
            string key = "CreditHistory_CreditHistoryCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CreditHistoryCount = (int)BizObject.Cache[key];
            }
            else
            {
                CreditHistoryCount = SiteProvider.PR2.GetCreditHistoryCount();
                BasePR.CacheData(key, CreditHistoryCount);
            }
            return CreditHistoryCount;
        }

        /// <summary>
        /// Returns a CreditHistory list with the specified credit
        /// </summary>
        public static List<CreditHistory> GetCreditHistoryByCredit(int CreditID)
        {
            List<CreditHistory> CreditHistory = null;
            string key = "CreditHistory_CreditHistoryByCredit_" + CreditID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CreditHistory = (List<CreditHistory>)BizObject.Cache[key];
            }
            else
            {
                CreditHistory = GetCreditHistoryListFromCreditHistoryInfoList(SiteProvider.PR2.GetCreditHistoryByCredit(CreditID));
                BasePR.CacheData(key, CreditHistory);
            }
            return CreditHistory;
        }


        /// <summary>
        /// Returns a CreditHistory object with the specified id
        /// </summary>
        public static CreditHistory GetCreditHistoryByID(int ChID)
        {
            CreditHistory CreditHistory = null;
            string key = "CreditHistory_CreditHistoryByID_" + ChID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CreditHistory = (CreditHistory)BizObject.Cache[key];
            }
            else
            {
                CreditHistory = GetCreditHistoryFromCreditHistoryInfo(SiteProvider.PR2.GetCreditHistoryByID(ChID));
                BasePR.CacheData(key, CreditHistory);
            }
            return CreditHistory;
        }

        /// <summary>
        /// Returns a CreditHistory By Credit
        /// </summary>
        public static DataSet GetCreditHistoryByCreditID(int CreditID)
        {
            DataSet dsCreditHistory = (SiteProvider.PR2.GetCreditHistoryByCreditID(CreditID));

            return dsCreditHistory;
        }

        /// <summary>
        /// Updates an existing CreditHistory
        /// </summary>
        public static bool UpdateCreditHistory(int ChID, int CreditID, int r_UserID, DateTime LogDate, string HistoryType, int CreditNumber, string Comment)
        {
            CreditHistoryInfo record = new CreditHistoryInfo(ChID, CreditID, r_UserID, LogDate, HistoryType, CreditNumber, Comment);
            bool ret = SiteProvider.PR2.UpdateCreditHistory(record);

            BizObject.PurgeCacheItems("CreditHistory_UpdateCreditHistory_" + ChID.ToString());
            BizObject.PurgeCacheItems("CreditHistory_UpdateCreditHistory");
            return ret;
        }

        /// <summary>
        /// Creates a new CreditHistory
        /// </summary>
        public static int InsertCreditHistory(int CreditID, int r_UserID, DateTime LogDate, string HistoryType, int CreditNumber, string Comment)
        {
            CreditHistoryInfo record = new CreditHistoryInfo(0, CreditID, r_UserID, LogDate, HistoryType, CreditNumber, Comment);
            int ret = SiteProvider.PR2.InsertCreditHistory(record);

            BizObject.PurgeCacheItems("CreditHistory_InsertCreditHistory");
            return ret;
        }

        /// <summary>
        /// Deletes an existing CreditHistory, but first checks if OK to delete
        /// </summary>
        public static bool DeleteCreditHistory(int ChID)
        {
            bool IsOKToDelete = OKToDelete(ChID);
            if (IsOKToDelete)
            {
                return (bool)DeleteCreditHistory(ChID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing CreditHistory - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteCreditHistory(int ChID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteCreditHistory(ChID);
            BizObject.PurgeCacheItems("CreditHistory_DeleteCreditHistory");
            return ret;
        }


        ///// <summary>
        ///// Checks to see if a CreditHistory can be deleted safely
        ///// (This default method just returns true. Certain entities
        ///// might set datadict_tables.lNoOKDel to eliminate this
        ///// default method and provide a custom version in
        ///// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int CreditID)
        {
            return true;
        }

        /// <summary>
        /// Returns a CreditHistory object filled with the data taken from the input CreditHistoryInfo
        /// </summary>
        private static CreditHistory GetCreditHistoryFromCreditHistoryInfo(CreditHistoryInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new CreditHistory(record.ChID, record.CreditID, record.r_UserID, record.LogDate, record.HistoryType, record.CreditNumber, record.Comment);
            }
        }

        /// <summary>
        /// Returns a list of CreditHistory objects filled with the data taken from the input list of CreditHistoryInfo
        /// </summary>
        private static List<CreditHistory> GetCreditHistoryListFromCreditHistoryInfoList(List<CreditHistoryInfo> recordset)
        {
            List<CreditHistory> CreditHistory = new List<CreditHistory>();
            foreach (CreditHistoryInfo record in recordset)
                CreditHistory.Add(GetCreditHistoryFromCreditHistoryInfo(record));
            return CreditHistory;
        }
        #endregion
    }
}
