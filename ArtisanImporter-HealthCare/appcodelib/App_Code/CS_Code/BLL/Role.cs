﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for Role
    /// </summary>
    public class Role : BasePR
    {
        #region Variables and Properties

        private int _RoleID = 0;
        public int RoleID
        {
            get { return _RoleID; }
            protected set { _RoleID = value; }
        }

        private string _RoleName = "";
        public string RoleName
        {
            get { return _RoleName; }
            set { _RoleName = value; }
        }

        private string _DisplayName = "";
        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }


        public Role(int RoleID, string RoleName)
        {
            this.RoleID = RoleID;
            this.RoleName = RoleName;
        }

        public Role(int RoleID, string RoleName, string DisplayName)
        {
            this.RoleID = RoleID;
            this.RoleName = RoleName;
            this.DisplayName = DisplayName;
        }

        public bool Delete()
        {
            bool success = Role.DeleteRole(this.RoleID);
            if (success)
                this.RoleID = 0;
            return success;
        }

        public bool Update()
        {
            return Role.UpdateRole(this.RoleID, this.RoleName);
        }
        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Roles
        /// </summary>
        public static List<Role> GetRoles(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "RoleName";

            List<Role> Roles = null;
            string key = "Roles_Roles_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Roles = (List<Role>)BizObject.Cache[key];
            }
            else
            {
                List<RoleInfo> recordset = SiteProvider.PR2.GetRoles(cSortExpression);
                Roles = GetRoleListFromRoleInfoList(recordset);
                BasePR.CacheData(key, Roles);
            }
            return Roles;
        }

        /// <summary>
        /// Returns a collection with all system Roles 
        /// </summary>
        public static List<Role> GetSystemRoles(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "RoleName";

            List<Role> Roles = null;
            string key = "Roles_Roles_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Roles = (List<Role>)BizObject.Cache[key];
            }
            else
            {
                List<RoleInfo> recordset = SiteProvider.PR2.GetSystemRoles(cSortExpression);
                Roles = GetRoleListFromRoleInfoList(recordset);
                BasePR.CacheData(key, Roles);
            }
            return Roles;
        }

        /// <summary>
        /// Returns the number of total Roles
        /// </summary>
        public static int GetRoleCount()
        {
            int RoleCount = 0;
            string key = "Roles_RoleCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                RoleCount = (int)BizObject.Cache[key];
            }
            else
            {
                RoleCount = SiteProvider.PR2.GetRoleCount();
                BasePR.CacheData(key, RoleCount);
            }
            return RoleCount;
        }

        /// <summary>
        /// Returns a Role object with the specified ID
        /// </summary>
        public static Role GetRoleByID(int RoleID)
        {
            Role Role = null;
            string key = "Roles_Role_" + RoleID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Role = (Role)BizObject.Cache[key];
            }
            else
            {
                Role = GetRoleFromRoleInfo(SiteProvider.PR2.GetRoleByID(RoleID));
                BasePR.CacheData(key, Role);
            }
            return Role;
        }

        /// <summary>
        /// Updates an existing Role
        /// </summary>
        public static bool UpdateRole(int RoleID, string RoleName)
        {
            RoleName = BizObject.ConvertNullToEmptyString(RoleName);


            RoleInfo record = new RoleInfo(RoleID, RoleName);
            bool ret = SiteProvider.PR2.UpdateRole(record);

            BizObject.PurgeCacheItems("Roles_Role_" + RoleID.ToString());
            BizObject.PurgeCacheItems("Roles_Roles");
            return ret;
        }

        /// <summary>
        /// Creates a new Role
        /// </summary>
        public static int InsertRole(string RoleName)
        {
            RoleName = BizObject.ConvertNullToEmptyString(RoleName);


            RoleInfo record = new RoleInfo(0, RoleName);
            int ret = SiteProvider.PR2.InsertRole(record);

            BizObject.PurgeCacheItems("Roles_Role");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Role, but first checks if OK to delete
        /// </summary>
        public static bool DeleteRole(int RoleID)
        {
            bool IsOKToDelete = OKToDelete(RoleID);
            if (IsOKToDelete)
            {
                return (bool)DeleteRole(RoleID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Role - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteRole(int RoleID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteRole(RoleID);
            //         new RecordDeletedEvent("Role", RoleID, null).Raise();
            BizObject.PurgeCacheItems("Roles_Role");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Role can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int RoleID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Role object filled with the data taken from the input RoleInfo
        /// </summary>
        private static Role GetRoleFromRoleInfo(RoleInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Role(record.RoleID, record.RoleName, record.DisplayName);
            }
        }

        /// <summary>
        /// Returns a list of Role objects filled with the data taken from the input list of RoleInfo
        /// </summary>
        private static List<Role> GetRoleListFromRoleInfoList(List<RoleInfo> recordset)
        {
            List<Role> Roles = new List<Role>();
            foreach (RoleInfo record in recordset)
                Roles.Add(GetRoleFromRoleInfo(record));
            return Roles;
        }

        #endregion
    }
}
