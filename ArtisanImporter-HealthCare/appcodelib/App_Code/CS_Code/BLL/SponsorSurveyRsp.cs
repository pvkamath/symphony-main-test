﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for SponsorSurveyRsp
/// </summary>
    public class SponsorSurveyRsp : BasePR
    {
        #region Variables and Properties
         public SponsorSurveyRsp(int responseid, int userid, int orderitemid, int questionid, int answerid, int discountid)
        {
            this.ResponseID  = responseid;
            this.UserID = userid;
            this.OrderitemID = orderitemid;
            this.QuestionID = questionid;
            this.AnswerID = answerid;
            this.DiscountID = discountid;
        }

        private int _responseid = 0;
        public int ResponseID
        {
            get { return _responseid; }
            set { _responseid = value; }
        }

        private int _userid = 0;
        public int UserID
        {
            get { return _userid; }
            set { _userid = value; }
        }

        private int _orderitemid = 0;
        public int OrderitemID
        {
            get { return _orderitemid; }
            set { _orderitemid = value; }
        }

        private int _questionid = 0;
        public int QuestionID
        {
            get { return _questionid; }
            set { _questionid = value; }
        }

        private int _answerid = 0;
        public int AnswerID
        {
            get { return _answerid; }
            set { _answerid = value; }
        }

        private int _discountid = 0;
        public int DiscountID
        {
            get { return _discountid; }
            set { _discountid = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public static bool InsertSponsorSurveyRsp(int userid, int orderitemid, int questionid, int answerid, int discountid)
        {
            bool ret = SiteProvider.PR2.InsertSponsorSurveyRsp(userid, orderitemid, questionid, answerid, discountid);            
            return ret;
        }


        public static bool UpdateSponsorSurveyRspByOrderitemId(int userid, int orderitemid, int discountid)
        {
            bool ret = SiteProvider.PR2.UpdateSponsorSurveyRspByOrderitemId(userid, orderitemid, discountid);
            // TODO: release cache?
            return ret;
        }

        #endregion
    }
}

