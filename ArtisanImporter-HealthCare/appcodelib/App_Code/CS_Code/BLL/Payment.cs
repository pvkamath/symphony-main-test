﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// Payment business object class
    /// </summary>
    public class Payment : BasePR
    {

        #region Properties

        private int _PaymentID = 0;
        public int PaymentID
        {
            get { return _PaymentID; }
            protected set { _PaymentID = value; }
        }

        private DateTime _PayDate = System.DateTime.Now;
        public DateTime PayDate
        {
            get { return _PayDate; }
            set { _PayDate = value; }
        }

        private decimal _PayAmount = 0.00M;
        public decimal PayAmount
        {
            get { return _PayAmount; }
            set { _PayAmount = value; }
        }

        private string _PayStatus = "";
        public string PayStatus
        {
            get { return _PayStatus; }
            set { _PayStatus = value; }
        }

        private string _PayType = "";
        public string PayType
        {
            get { return _PayType; }
            set { _PayType = value; }
        }

        private string _PayDesc = "";
        public string PayDesc
        {
            get { return _PayDesc; }
            set { _PayDesc = value; }
        }

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }

        private string _CCError = "";
        public string CCError
        {
            get { return _CCError; }
            set { _CCError = value; }
        }

        private string _TransID = "";
        public string TransID
        {
            get { return _TransID; }
            set { _TransID = value; }
        }

        private string _AuthCode = "";
        public string AuthCode
        {
            get { return _AuthCode; }
            set { _AuthCode = value; }
        }
        private DateTime _ExpDate = System.DateTime.Now;
        public DateTime ExpDate 
        {
            get { return _ExpDate; }
            set { _ExpDate = value; }
        }
        private string _ProfileID = "";
        public string ProfileID
        {
            get { return _ProfileID; }
            set { _ProfileID = value; }
        }
        private bool _Renew = false;
        public bool Renew
        {
            get { return _Renew; }
            set { _Renew = value; }
        }
        private bool _Active = false;
        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }
        private decimal _CouponAmount = (Decimal)0;
        public decimal CouponAmount
        {
            get { return _CouponAmount; }
            set { _CouponAmount = value; }
        }
        private int _CouponId = 0;
        public int CouponId
        {
            get { return _CouponId; }
            set { _CouponId = value; }
        }
        private string _CCLast = string.Empty;
        public string CCLast
        {
            get { return _CCLast; }
            set { _CCLast = value; }
        }
        private DateTime _ReminderDate = System.DateTime.Now;
        public DateTime ReminderDate
        {
            get { return _ReminderDate; }
            set { _ReminderDate = value; }
        }

        public Payment(int PaymentID, DateTime PayDate, decimal PayAmount, string PayStatus,
           string PayType, string PayDesc, int UserID, int FacilityID, string CCError, string TransID, string AuthCode,DateTime ExpDate,string ProfileID,bool Renew,bool Active,
            decimal CouponAmount, int CouponId, string CCLast)
        {
            this.PaymentID = PaymentID;
            this.PayDate = PayDate;
            this.PayAmount = PayAmount;
            this.PayStatus = PayStatus;
            this.PayType = PayType;
            this.PayDesc = PayDesc;
            this.UserID = UserID;
            this.FacilityID = FacilityID;
            this.CCError = CCError;
            this.TransID = TransID;
            this.AuthCode = AuthCode;
            this.ExpDate = ExpDate;
            this.ProfileID = ProfileID;
            this.Renew = Renew;
            this.Active = Active;
            this.CouponAmount = CouponAmount;
            this.CouponId = CouponId;
            this.CCLast = CCLast;            
        }

        public bool Delete()
        {
            bool success = Payment.DeletePayment(this.PaymentID);
            if (success)
                this.PaymentID = 0;
            return success;
        }

        public bool Update()
        {
            return Payment.UpdatePayment(this.PaymentID, this.PayDate, this.PayAmount, this.PayStatus,
           this.PayType, this.PayDesc, this.UserID, this.FacilityID, this.CCError, this.TransID, this.AuthCode,this.ExpDate,this.ProfileID,this.Renew,this.Active,
           this.CouponAmount, this.CouponId, this.CCLast);
        }
#endregion

        #region Static Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Payments
        /// </summary>
        public static List<Payment> GetPayments(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "PaymentID";

            List<Payment> Payments = null;
            string key = "Payments_Payments_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Payments = (List<Payment>)BizObject.Cache[key];
            }
            else
            {
                List<PaymentInfo> recordset = SiteProvider.PR2.GetPayments(cSortExpression);
                Payments = GetPaymentListFromPaymentInfoList(recordset);
                BasePR.CacheData(key, Payments);
            }
            return Payments;
        }


        /// <summary>
        /// Returns the number of total Payments
        /// </summary>
        public static int GetPaymentCount()
        {
            int PaymentCount = 0;
            string key = "Payments_PaymentCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                PaymentCount = (int)BizObject.Cache[key];
            }
            else
            {
                PaymentCount = SiteProvider.PR2.GetPaymentCount();
                BasePR.CacheData(key, PaymentCount);
            }
            return PaymentCount;
        }

        /// <summary>
        /// Returns a Payment object with the specified ID
        /// </summary>
        public static Payment GetPaymentByID(int PaymentID)
        {
            Payment Payment = null;
            string key = "Payments_Payment_" + PaymentID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Payment = (Payment)BizObject.Cache[key];
            }
            else
            {
                Payment = GetPaymentFromPaymentInfo(SiteProvider.PR2.GetPaymentByID(PaymentID));
                BasePR.CacheData(key, Payment);
            }
            return Payment; 
        }

        /// <summary>
        /// Returns a Payment object with the specified UserID
        /// </summary>
        public static Payment GetPaymentByUserID(int UserID)
        {
            Payment Payment = null;
            string key = "Payments_Payment_" + UserID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Payment = (Payment)BizObject.Cache[key];
            }
            else
            {
                Payment = GetPaymentFromPaymentInfo(SiteProvider.PR2.GetPaymentByUserID(UserID));
                BasePR.CacheData(key, Payment);
            }
            return Payment;
        }

        /// <summary>
        /// Returns a Payment object with the specified authcode
        /// </summary>
        public static Payment GetPaymentByAuthcode(string AuthCode)
        {
            Payment Payment = null;
            string key = "Payments_Payment_" + AuthCode.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Payment = (Payment)BizObject.Cache[key];
            }
            else
            {
                Payment = GetPaymentFromPaymentInfo(SiteProvider.PR2.GetPaymentByAuthcode(AuthCode));
                BasePR.CacheData(key, Payment);
            }
            return Payment;

        }

        /// <summary>
        /// Returns a Payment object with the specified UserID
        /// </summary>
        public static Payment GetPaymentByUserIDAndDomainId(int UserID, int DomaInId)
        {
            return  GetPaymentFromPaymentInfo(SiteProvider.PR2.GetPaymentByUserIDAndDomainId(UserID, DomaInId));
                   
        }

        public static DataSet GetCCExpiredUsers()
        {
            return SiteProvider.PR2.GetCCExpiredUsers();
        }
        /// <summary>
        /// Returns  Payment object list with the specified UserID
        /// </summary>
        public static List<Payment> GetPaymentsByUserID(int UserID)
        {
           
               List<PaymentInfo> paymentInfolist =  SiteProvider.PR2.GetPaymentsByUserID(UserID);

               List<Payment> paymentList = new List<Payment>();

               foreach (PaymentInfo pinfo in paymentInfolist)
               { 
               paymentList.Add(GetPaymentFromPaymentInfo(pinfo));
               
               }

            return paymentList;
         
        }

        public static Payment GetActivePaymentByUserIdandDomainId(int UserId, int DomainId)
        {
            return GetPaymentFromPaymentInfo(SiteProvider.PR2.GetActivePaymentByUserIdandDomainId(UserId, DomainId));
        }

        public static List<Payment> GetActivePaymentsByUserID(int UserId)
        {
            List<PaymentInfo> paymentInfolist = SiteProvider.PR2.GetActivePaymentsByUserID(UserId);

            List<Payment> paymentList = new List<Payment>();

            foreach (PaymentInfo pinfo in paymentInfolist)
            {
                paymentList.Add(GetPaymentFromPaymentInfo(pinfo));
            }

            return paymentList;
        }

        public static List<Payment> GetActivePRPaymentsByExpDateInThisMonth()
        {
            List<PaymentInfo> paymentInfolist = SiteProvider.PR2.GetActivePRPaymentsByExpDateInThisMonth();

            List<Payment> paymentList = new List<Payment>();

            foreach (PaymentInfo pinfo in paymentInfolist)
            {
                paymentList.Add(GetPaymentFromPaymentInfo(pinfo));
            }

            return paymentList;
        }

        public static List<Payment> GetBDSPaymentByUserIdandFacid(int UserId, int Facid)
        {
            List<PaymentInfo> paymentInfolist = SiteProvider.PR2.GetBDSPaymentByUserIdandFacid(UserId, Facid);

            List<Payment> paymentList = new List<Payment>();

            foreach (PaymentInfo pinfo in paymentInfolist)
            {
                paymentList.Add(GetPaymentFromPaymentInfo(pinfo));
            }

            return paymentList;
        }

        public static List<Payment> DailyPRPaymentUpdate()
        {
            List<PaymentInfo> paymentInfolist = SiteProvider.PR2.DailyPRPaymentUpdate();

            List<Payment> paymentList = new List<Payment>();

            foreach (PaymentInfo pinfo in paymentInfolist)
            {
                paymentList.Add(GetPaymentFromPaymentInfo(pinfo));

            }

            return paymentList;
        }

        public static List<Payment> DailyPRPaymentCancelledUpdate()
        {
            List<PaymentInfo> paymentInfoList = SiteProvider.PR2.DailyPRPaymentCancelledUpdate();
            List<Payment> PaymentList = new List<Payment>();
            foreach (PaymentInfo pinfo in paymentInfoList)
            {
                PaymentList.Add(GetPaymentFromPaymentInfo(pinfo));
            }

            return PaymentList;
        }

      

        public static Boolean DailyPRUserAccountUpdate(int userid)
        {
            return SiteProvider.PR2.DailyPRUserAccountUpdate(userid);
        }


        /// <summary>
        /// Updates an existing Payment
        /// </summary>
        public static bool UpdatePayment(int PaymentID, DateTime PayDate, decimal PayAmount, string PayStatus,
           string PayType, string PayDesc, int UserID, int FacilityID, string CCError, string TransID, string AuthCode,DateTime ExpDate, string ProfileID, bool renew, bool active,
            decimal CouponAmount, int CouponId, string CCLast)
        {
            PayStatus = BizObject.ConvertNullToEmptyString(PayStatus);
            PayType = BizObject.ConvertNullToEmptyString(PayType);
            PayDesc = BizObject.ConvertNullToEmptyString(PayDesc);
            CCError = BizObject.ConvertNullToEmptyString(CCError);
            TransID = BizObject.ConvertNullToEmptyString(TransID);
            AuthCode = BizObject.ConvertNullToEmptyString(AuthCode);
            ProfileID = BizObject.ConvertNullToEmptyString(ProfileID);


            PaymentInfo record = new PaymentInfo(PaymentID, PayDate, PayAmount, PayStatus,
           PayType, PayDesc, UserID, FacilityID, CCError, TransID, AuthCode,ExpDate,ProfileID,renew,active,CouponAmount,CouponId,CCLast);
            bool ret = SiteProvider.PR2.UpdatePayment(record);

            BizObject.PurgeCacheItems("Payments_Payment_" + PaymentID.ToString());
            BizObject.PurgeCacheItems("Payments_Payments");
            return ret;
        }

        /// <summary>
        /// Creates a new Payment
        /// </summary>
        public static int InsertPayment(DateTime PayDate, decimal PayAmount, string PayStatus,
           string PayType, string PayDesc, int UserID, int FacilityID, string CCError, string TransID, string AuthCode, DateTime ExpDate, string ProfileID, bool renew, bool active,
            decimal CouponAmount, int CouponId, string CCLast)
        {
            PayStatus = BizObject.ConvertNullToEmptyString(PayStatus);
            PayType = BizObject.ConvertNullToEmptyString(PayType);
            PayDesc = BizObject.ConvertNullToEmptyString(PayDesc);
            CCError = BizObject.ConvertNullToEmptyString(CCError);
            TransID = BizObject.ConvertNullToEmptyString(TransID);
            AuthCode = BizObject.ConvertNullToEmptyString(AuthCode);
            ProfileID = BizObject.ConvertNullToEmptyString(ProfileID);
            PaymentInfo record = new PaymentInfo(0, PayDate, PayAmount, PayStatus,
           PayType, PayDesc, UserID, FacilityID, CCError, TransID, AuthCode,ExpDate,ProfileID,renew,active,CouponAmount,CouponId,CCLast);
            int ret = SiteProvider.PR2.InsertPayment(record);

            BizObject.PurgeCacheItems("Payments_Payment");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Payment, but first checks if OK to delete
        /// </summary>
        public static bool DeletePayment(int PaymentID)
        {
            bool IsOKToDelete = OKToDelete(PaymentID);
            if (IsOKToDelete)
            {
                return (bool)DeletePayment(PaymentID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Update Payment Reminder Date based on payment Id.
        /// </summary>
        /// <param name="PaymentID"></param>
        /// <param name="ReminderDate"></param>
        /// <returns></returns>
        public static bool UpdatePaymentReminderDate(int PaymentID, DateTime ReminderDate)
        {
            return SiteProvider.PR2.UpdatePaymentReminderDate(ReminderDate, PaymentID);
        }

        /// <summary>
        /// Deletes an existing Payment - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeletePayment(int PaymentID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeletePayment(PaymentID);
            //         new RecordDeletedEvent("Payment", PaymentID, null).Raise();
            BizObject.PurgeCacheItems("Payments_Payment");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Payment can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int PaymentID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Payment object filled with the data taken from the input PaymentInfo
        /// </summary>
        private static Payment GetPaymentFromPaymentInfo(PaymentInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Payment(record.PaymentID, record.PayDate, record.PayAmount, record.PayStatus,
           record.PayType, record.PayDesc, record.UserID, record.FacilityID, record.CCError,
           record.TransID, record.AuthCode,record.ExpDate,record.ProfileID,record.Renew,record.Active,record.CouponAmount,record.CouponId,record.CCLast);
            }
        }

        /// <summary>
        /// Returns a list of Payment objects filled with the data taken from the input list of PaymentInfo
        /// </summary>
        private static List<Payment> GetPaymentListFromPaymentInfoList(List<PaymentInfo> recordset)
        {
            List<Payment> Payments = new List<Payment>();
            foreach (PaymentInfo record in recordset)
                Payments.Add(GetPaymentFromPaymentInfo(record));
            return Payments;
        }
        #endregion

    }    
    

}