﻿
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using System.Data;

/// <summary>
/// Summary description for AlliedProfLink
/// </summary>
namespace PearlsReview.BLL
{
    public class AlliedProfLink : BasePR
    {
        #region  Variables and Properties

        private int _apl_id = 0;
        public int Apl_id
        {
            get { return _apl_id; }
            protected set { _apl_id = value; }
        }

        private int _ad_id = 0;
        public int Ad_id
        {
            get { return _ad_id; }
            protected set { _ad_id = value; }
        }

        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }



        private string _apl_comment = "";
        public string Apl_comment
        {
            get { return _apl_comment; }
            protected set { _apl_comment = value; }
        }

      

        public AlliedProfLink()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public AlliedProfLink(int apl_id, int ad_id, int msid, string apl_comment)
        {

            this.Apl_id = apl_id;
            this.Ad_id = ad_id;
            this.Msid = msid;
            this.Apl_comment = apl_comment;
            
        }
        public bool Delete()
        {
            bool success = AlliedProfLink.DeleteAlliedProfLink(this.Apl_id);
            if (success)
                this.Apl_id = 0;
            return success;
        }

        public bool Update()
        {
            return AlliedProfLink.UpdateAlliedProfLink(this.Apl_id, this.Ad_id,this.Msid, this.Apl_comment );
        }
        #endregion  Variables and Properties
        #region Methods
        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all AlliedProfLinks
        //</summary>
        public static List<AlliedProfLink> GetAlliedProfLink(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "ad_id";

            List<AlliedProfLink> AlliedProfLinks = null;
            string key = "AlliedProfLink_AlliedProfLink_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AlliedProfLinks = (List<AlliedProfLink>)BizObject.Cache[key];
            }
            else
            {
                List<AlliedProfLinkInfo> recordset = SiteProvider.PR2.GetAlliedProfLink(cSortExpression);
                AlliedProfLinks = GetAlliedProfLinkListFromAlliedProfLinkInfoList(recordset);
                BasePR.CacheData(key, AlliedProfLinks);
            }
            return AlliedProfLinks;
        }


        /// <summary>
        /// Returns a AlliedProfLinks object with the specified ID
        /// </summary>
        public static AlliedProfLink GetAlliedProfLinkByID(int Apl_id)
        {
            AlliedProfLink AlliedProfLinks = null;
            string key = "AlliedProfLinks_AlliedProfLinks_" + Apl_id.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AlliedProfLinks = (AlliedProfLink)BizObject.Cache[key];
            }
            else
            {
                AlliedProfLinks = GetAlliedProfLinkFromAlliedProfLinkInfo(SiteProvider.PR2.GetAlliedProfLinkByID(Apl_id));
                BasePR.CacheData(key, AlliedProfLinks);
            }
            return AlliedProfLinks;
        }
        public static DataTable GetAlliedProfLinksTableByAdId(int Ad_id)
        {
            

          
            DataTable recordset = (DataTable)SiteProvider.PR2.GetAlliedProfLinkTableByAdID(Ad_id);



            return recordset;
        }

        public static DataTable GetAlliedProfLinkTableByID(int Apl_id)
        {



            DataTable recordset = (DataTable)SiteProvider.PR2.GetAlliedProfLinkTableByID(Apl_id);



            return recordset;
        }


        


        public static List<AlliedProfLink> GetAlliedProfLinksByAdId(int Ad_id)
        {

            List<AlliedProfLink> AlliedProfLinks = null;
            List<AlliedProfLinkInfo> recordset = SiteProvider.PR2.GetAlliedProfLinksByAdID(Ad_id);
            AlliedProfLinks = GetAlliedProfLinkListFromAlliedProfLinkInfoList(recordset);



            return AlliedProfLinks;
        }

        //public static int InsertAlliedProfLink(int msid, int ad_id, string comment)
        //{
        //    AlliedProfLinkInfo record = new AlliedProfLinkInfo(msid, ad_id, comment);
        //    int ret = SiteProvider.PR2.InsertCategoryMicrositeLink(record);

        //    BizObject.PurgeCacheItems("CategoryMicrositeLinks_CategoryMicrositeLinks");
        //    return ret;
        //}
        public static bool UpdateAlliedProfLinkLists(List<string> Professions, int Ad_id)
        {
            return SiteProvider.PR2.UpdateAlliedProfLinkLists(Professions, Ad_id);
        }




        #endregion




        /// <summary>
        /// Updates an existing AlliedProfLink
        /// </summary>
        public static bool UpdateAlliedProfLink(int apl_id, int ad_id, int msid, string apl_comment)
        {

            apl_comment = BizObject.ConvertNullToEmptyString(apl_comment);


            AlliedProfLinkInfo record = new AlliedProfLinkInfo(apl_id, ad_id, msid, apl_comment);
            bool ret = SiteProvider.PR2.UpdateAlliedProfLink(record);

            BizObject.PurgeCacheItems("AlliedProfLinks_AlliedProfLinks_" + apl_id.ToString());
            BizObject.PurgeCacheItems("AlliedProfLink_AlliedProfLinks");
            return ret;
        }


        /// <summary>
        /// Deletes an existing AlliedProfLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteAlliedProfLink(int Apl_id)
        {
            bool IsOKToDelete = OKToDelete(Apl_id);
            if (IsOKToDelete)
            {
                return (bool)DeleteAlliedProfLink(Apl_id, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing ahDomain - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteAlliedProfLink(int Apl_id, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteAlliedProfLink(Apl_id);

            BizObject.PurgeCacheItems("AlliedProfLinks_AlliedProfLinks_" + Apl_id.ToString());
            BizObject.PurgeCacheItems("AlliedProfLink_AlliedProfLinks");
            return ret;
        }
        /// <summary>
        /// Checks to see if a AlliedProfLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }



        private static List<AlliedProfLink> GetAlliedProfLinkListFromAlliedProfLinkInfoList(List<AlliedProfLinkInfo> recordset)
        {
            List<AlliedProfLink> AlliedProfLinks = new List<AlliedProfLink>();
            foreach (AlliedProfLinkInfo record in recordset)
                AlliedProfLinks.Add(GetAlliedProfLinkFromAlliedProfLinkInfo(record));
            return AlliedProfLinks;
        }
        private static AlliedProfLink GetAlliedProfLinkFromAlliedProfLinkInfo(AlliedProfLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new AlliedProfLink(record.Apl_id, record.Ad_id,record.Msid, record.Apl_comment);
            }
        }

    }


 

}


