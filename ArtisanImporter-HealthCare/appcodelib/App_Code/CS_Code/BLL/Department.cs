﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;


namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr1
    /// </summary>
    public class Department : BasePR
    {
        #region Variables and Properties

        public Department()
        {
        }

        public int dept_id
        {
            get;
            set;
        }

        public string dept_name
        {
            get;
            set;
        }

        public string dept_abbrev
        {
            get;
            set;
        }

        public string dept_number
        {
            get;
            set;
        }

        public int dept_bed_total
        {
            get;
            set;
        }


        public int facilityid
        {
            get;
            set;
        }

        public bool dept_active_ind
        {
            get;
            set;
        }
        #endregion

        #region Methods

        public bool Update()
        {
            return Department.Update(this.dept_id, this.dept_name, this.dept_abbrev,
                                        this.dept_number, this.dept_bed_total, this.facilityid, this.dept_active_ind);
        }

        public static bool Update(int dept_id, string dept_name, string dept_abbrev, string dept_number,
                                    int dept_bed_total, int facilityid, bool dept_active_ind)
        {
            DepartmentInfo deptInfo = new DepartmentInfo(dept_id, dept_name, dept_abbrev, dept_number, dept_bed_total,
                                               facilityid, dept_active_ind);

            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            SiteProvider.PR2.UpdateDepartment(deptInfo);


            return false;
        }






        public bool Insert()
        {
            return Department.Insert(this.dept_id, this.dept_name, this.dept_abbrev, this.dept_number,
                                        this.dept_bed_total, this.facilityid, this.dept_active_ind) > 0;
        }

        //public static bool Insert(int dept_id, string dept_name, string dept_abbrev, string dept_number,
        //                            int dept_bed_total, int facilityid, bool dept_active_ind)
        //{
        //    DepartmentInfo deptInfo = new DepartmentInfo(dept_id, dept_name, dept_abbrev, dept_number, dept_bed_total,
        //                                       facilityid, dept_active_ind);


        //   // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
        //    int id = SiteProvider.PR2.InsertDepartment(deptInfo);


        //    return id > 0;
        //}

        public static int Insert(int dept_id, string dept_name, string dept_abbrev, string dept_number,
                                  int dept_bed_total, int facilityid, bool dept_active_ind)
        {
            DepartmentInfo deptInfo = new DepartmentInfo(dept_id, dept_name, dept_abbrev, dept_number, dept_bed_total,
                                               facilityid, dept_active_ind);


            // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return  SiteProvider.PR2.InsertDepartment(deptInfo);
        }



        public bool Delete()
        {
            return Department.Delete(this.dept_id);
        }

        public static bool Delete(int dept_id)
        {
           // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            SiteProvider.PR2.DeleteDepartment(dept_id);

            return false;
        }


        public static List<Department> GetDepartments(string fac_id, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "dept_name";

            List<Department> Dept = null;
            string key = "Dept_dept_name" + cSortExpression;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Dept = (List<Department>)BizObject.Cache[key];
            }
            else
            {
               // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<DepartmentInfo> recordset = SiteProvider.PR2.GetDepartments(fac_id, cSortExpression);
                Dept = GetDepartmentListfromDepartmentInfo(recordset);
                BasePR.CacheData(key, Dept);
            }
            return Dept;
        }
        public static List<Department> GetDepartmentListfromDepartmentInfo(List<DepartmentInfo> recordset)
        {
            List<Department> departments = new List<Department>();
            foreach (DepartmentInfo deptInfo in recordset)
            {
                Department department = GetDepartmentFromDepartmentInfo(deptInfo);
                departments.Add(department);
            }
            return departments;
        }
        private static Department GetDepartmentFromDepartmentInfo(DepartmentInfo deptInfo)
        {
            Department dept = new Department();
            dept.dept_id = deptInfo.dept_id;
            dept.dept_name = deptInfo.dept_name;
            dept.dept_abbrev = deptInfo.dept_abbrev;
            dept.dept_number = deptInfo.dept_number;
            dept.dept_bed_total = deptInfo.dept_bed_total;
            dept.facilityid = deptInfo.facilityid;
            dept.dept_active_ind = deptInfo.dept_active_ind;

            return dept;

        }

        //method to get the Department by uniqueID 

        public static Department GetDepartmentByID(int dept_id)
        {
            Department Dept = null;
            string key = "Dept_dept_id" + dept_id;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Dept = (Department)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                DepartmentInfo recordset = SiteProvider.PR2.GetDepartmentByID(dept_id);
                Dept = GetDepartmentFromDepartmentInfo(recordset);
                BasePR.CacheData(key, Dept);
            }
            return Dept;
        }



        public static List<Department> GetUserDepartmentsByUserID(int userID)
        {
            List<Department> UserDepartments = null;
            string key = "UserAccounts_UserDepartments" + userID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserDepartments = (List<Department>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<DepartmentInfo> recordset = SiteProvider.PR2.GetDepartmentsByUserID(userID);
                UserDepartments = Department.GetDepartmentListfromDepartmentInfo(recordset);
                BasePR.CacheData(key, UserDepartments);
            }
            return UserDepartments;
        }

        public static Department GetUserDepartmentByDeptName(string DeptName)
        {
            Department dept = null;
            string key = "Departments_DeptByNumber" + DeptName.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                dept = (Department)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                DepartmentInfo recordset = SiteProvider.PR2.GetUserDepartmentByDeptName(DeptName);
                dept = GetDepartmentFromDepartmentInfo(recordset);
                BasePR.CacheData(key, dept);
            }
            if (dept.dept_id == 0)
                return null;
            return dept;
        }

        public static void UpdateDepartmentsForUserID(List<DepartmentInfo> departments, int userID)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            SiteProvider.PR2.UpdateDepartmentsForUserID(departments, userID);
        }

       #endregion

    }
}