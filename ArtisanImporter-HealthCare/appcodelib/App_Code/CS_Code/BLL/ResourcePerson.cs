﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for ResourcePerson
/// </summary>
    public class ResourcePerson : BasePR
    {
        #region Variables and Properties

        private int _ResourceID = 0;
        public int ResourceID
        {
            get { return _ResourceID; }
            protected set { _ResourceID = value; }
        }

        private string _photo="";
        public string Photo
        {
            get { return _photo; }
            set { _photo = value; }
        }

        private string _Fullname = "";
        public string Fullname
        {
            get { return _Fullname; }
            set { _Fullname = value; }
        }

        private string _LastFirst = "";
        public string LastFirst
        {
            get { return _LastFirst; }
            set { _LastFirst = value; }
        }

        private string _Address = "";
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        private string _City = "";
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }

        private string _State = "";
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        private string _Zip = "";
        public string Zip
        {
            get { return _Zip; }
            set { _Zip = value; }
        }

        private string _Country = "";
        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        private string _Phone = "";
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }

        private string _Email = "";
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _SSN = "";
        public string SSN
        {
            get { return _SSN; }
            set { _SSN = value; }
        }

        private string _Specialty = "";
        public string Specialty
        {
            get { return _Specialty; }
            set { _Specialty = value; }
        }

        private string _OtherNotes = "";
        public string OtherNotes
        {
            get { return _OtherNotes; }
            set { _OtherNotes = value; }
        }

        private bool _ContrRecd = false;
        public bool ContrRecd
        {
            get { return _ContrRecd; }
            set { _ContrRecd = value; }
        }

        private String _DateDiscl = "";
        public String DateDiscl
        {
            get { return _DateDiscl; }
            set { _DateDiscl = value; }
        }

        //private DateTime _DateDiscl = System.DateTime.MinValue;
        //public DateTime DateDiscl
        //{
        //    get { return _DateDiscl; }
        //    set { _DateDiscl = value; }
        //}

        private string _Disclosure = "I have nothing to disclose.";
        public string Disclosure
        {
            get { return _Disclosure; }
            set { _Disclosure = value; }
        }

        private string _StateLic = "";
        public string StateLic
        {
            get { return _StateLic; }
            set { _StateLic = value; }
        }
        private string _Degrees = "";
        public string Degrees
        {
            get { return _Degrees; }
            set { _Degrees = value; }
        }
        private string _Position = "";
        public string Position
        {
            get { return _Position; }
            set { _Position = value; }
        }
        private string _Experience = "";
        public string Experience
        {
            get { return _Experience; }
            set { _Experience = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }

        private int _topicResID = 0;
        public int TopicResID
        {
            get { return _topicResID; }
            set { _topicResID = value; }
        }

        private int _sortOrder = 0;
        public int SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }
        }

        private string _RFirstName = "";
        public string RFirstName
        {
            get { return _RFirstName; }
            set { _RFirstName = value; }
        }

        private string _RLastName = "";
        public string RLastName
        {
            get { return _RLastName; }
            set { _RLastName = value; }
        }

        private string _RMiddle = "";
        public string RMiddle
        {
            get { return _RMiddle; }
            set { _RMiddle = value; }
        }

        public ResourcePerson(int ResourceID, string Fullname, string LastFirst, string Address, string City,
            string State, string Zip, string Country, string Phone, string Email, string SSN, string Specialty,
            string OtherNotes, bool ContrRecd, String DateDiscl, string Disclosure, string StateLic,
            string Degrees, string Position, string Experience, int FacilityID, string RFirstName, string RLastName, string RMiddle,string Photo)
        {
            this.ResourceID = ResourceID;
            this.Fullname = Fullname;
            this.LastFirst = LastFirst;
            this.Address = Address;
            this.City = City;
            this.State = State;
            this.Zip = Zip;
            this.Country = Country;
            this.Phone = Phone;
            this.Email = Email;
            this.SSN = SSN;
            this.Specialty = Specialty;
            this.OtherNotes = OtherNotes;
            this.ContrRecd = ContrRecd;
            this.DateDiscl = DateDiscl;
            this.Disclosure = Disclosure;
            this.StateLic = StateLic;
            this.Degrees = Degrees;
            this.Position = Position;
            this.Experience = Experience;
            this.FacilityID = FacilityID;
            this.RFirstName = RFirstName;
            this.RLastName = RLastName;
            this.RMiddle = RMiddle;
            this.Photo = Photo;
        }

        public ResourcePerson(int TopicResID, int SortOrder, int ResourceID, string Fullname, string LastFirst, string Address, string City,
          string State, string Zip, string Country, string Phone, string Email, string SSN, string Specialty,
          string OtherNotes, bool ContrRecd, String DateDiscl, string Disclosure, string StateLic,
          string Degrees, string Position, string Experience, int FacilityID, string RFirstName, string RLastName, string RMiddle,string Photo)
        {
            this.TopicResID = TopicResID;
            this.SortOrder = SortOrder;
            this.ResourceID = ResourceID;
            this.Fullname = Fullname;
            this.LastFirst = LastFirst;
            this.Address = Address;
            this.City = City;
            this.State = State;
            this.Zip = Zip;
            this.Country = Country;
            this.Phone = Phone;
            this.Email = Email;
            this.SSN = SSN;
            this.Specialty = Specialty;
            this.OtherNotes = OtherNotes;
            this.ContrRecd = ContrRecd;
            this.DateDiscl = DateDiscl;
            this.Disclosure = Disclosure;
            this.StateLic = StateLic;
            this.Degrees = Degrees;
            this.Position = Position;
            this.Experience = Experience;
            this.FacilityID = FacilityID;
            this.RFirstName = RFirstName;
            this.RLastName = RLastName;
            this.RMiddle = RMiddle;
            this.Photo = Photo;
        }

        public bool Delete()
        {
            bool success = ResourcePerson.DeleteResourcePerson(this.ResourceID);
            if (success)
                this.ResourceID = 0;
            return success;
        }

        public bool Update()
        {
            return ResourcePerson.UpdateResourcePerson(this.ResourceID, this.Fullname, this.LastFirst,
                this.Address, this.City, this.State, this.Zip, this.Country, this.Phone, this.Email,
                this.SSN, this.Specialty, this.OtherNotes, this.ContrRecd, this.DateDiscl, this.Disclosure,
                this.StateLic, this.Degrees, this.Position, this.Experience, this.FacilityID, this.RFirstName, this.RLastName, this.RMiddle,this.Photo);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all ResourcePersons
        /// </summary>
        public static List<ResourcePerson> GetResourcePersons(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastFirst";

            List<ResourcePerson> ResourcePersons = null;
            string key = "ResourcePersons_ResourcePersons_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
            }
            else
            {
                List<ResourcePersonInfo> recordset = SiteProvider.PR2.GetResourcePersons(cSortExpression);
                ResourcePersons = GetResourcePersonListFromResourcePersonInfoList(recordset);
                BasePR.CacheData(key, ResourcePersons);
            }
            return ResourcePersons;
        }



        public static List<ResourcePerson> GetUnAssignedResourcesForLectEventID(int LectEvtID, String searchText)
        {
            List<ResourcePerson> ResourcePersons = null;
            string key = "ResourcePersons_ResourcePersons_" + LectEvtID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
            }
            else
            {
                List<ResourcePersonInfo> recordset = SiteProvider.PR2.GetUnAssignedResourcesForLectEventID(LectEvtID, searchText);
                ResourcePersons = GetResourcePersonListFromResourcePersonInfoList(recordset);
                BasePR.CacheData(key, ResourcePersons);
            }
            return ResourcePersons;
        }



        public static DataSet GetLectureEventsDetails(String cSortExpression)
        {
            DataSet ds = new DataSet();
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastFirst";

          
                ds = SiteProvider.PR2.GetLectureEventsDetails(cSortExpression);
              
          
            return ds;
        
        }

        /// <summary>
        /// Returns a collection with all ResourcePersons linked to a topic for a certain type
        /// (author="A", editor="E", nurse planner="N")
        /// </summary>
        public static List<ResourcePerson> GetResourcePersonsByTopicIDAndType(int TopicID,
            string ResourceType, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastFirst";

            List<ResourcePerson> ResourcePersons = null;
            string key = "ResourcePersons_ResourcePersons_" + TopicID.ToString() + "_" + ResourceType.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
            }
            else
            {
                List<ResourcePersonInfo> recordset = SiteProvider.PR2.GetResourcePersonsByTopicIDAndType(TopicID, ResourceType, cSortExpression);
                ResourcePersons = GetResourcePersonListInfoFromResourcePersonInfoList(recordset);
                BasePR.CacheData(key, ResourcePersons);
            }
            return ResourcePersons;
        }

        public static DataSet  GetResourcePersonsByTopicID(int TopicID)
        {
            return SiteProvider.PR2.GetResourcePersonsByTopicID(TopicID);
        }
        /// <summary>
        /// Returns the number of total ResourcePersons
        /// </summary>
        public static int GetResourcePersonCount()
        {
            int ResourcePersonCount = 0;
            string key = "ResourcePersons_ResourcePersonCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ResourcePersonCount = (int)BizObject.Cache[key];
            }
            else
            {
                ResourcePersonCount = SiteProvider.PR2.GetResourcePersonCount();
                BasePR.CacheData(key, ResourcePersonCount);
            }
            return ResourcePersonCount;
        }

        /// <summary>
        /// Returns a ResourcePerson object with the specified ID
        /// </summary>
        public static ResourcePerson GetResourcePersonByID(int ResourceID)
        {
            ResourcePerson ResourcePerson = null;
            string key = "ResourcePersons_ResourcePerson_" + ResourceID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ResourcePerson = (ResourcePerson)BizObject.Cache[key];
            }
            else
            {
                ResourcePerson = GetResourcePersonFromResourcePersonInfo(SiteProvider.PR2.GetResourcePersonByID(ResourceID));
                BasePR.CacheData(key, ResourcePerson);
            }
            return ResourcePerson;
        }


        /// <summary>
        /// Returns a ResourcePerson object with the Facility Id
        /// </summary>
        public static List<ResourcePerson> GetResourcePersonByFacilityID(int FacilityID)
        {
            List<ResourcePerson> ResourcePersons = null;
            string key = "ResourcePersons_ResourcePerson_" + FacilityID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
            }
            else
            {
                List<ResourcePersonInfo> recordset = SiteProvider.PR2.GetResourcePersonByFacilityID(FacilityID);
                ResourcePersons = GetResourcePersonListFromResourcePersonInfoList(recordset);
                BasePR.CacheData(key, ResourcePersons);
            }
            return ResourcePersons;
        }

        /// <summary>
        /// Bhaskar N
        /// Retrieves all Selected ResourcePersons for a TopicID 
        /// </summary>
        public static List<ResourcePerson> GetAllSelectedResourcePersonsByTopicID(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "fullname";

            List<ResourcePerson> ResourcePersons = null;
            string key = "ResourcePersons_SelectedResourcePersons_" + TopicID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
            }
            else
            {
                List<ResourcePersonInfo> recordset = SiteProvider.PR2.GetAllSelectedResourcePersonsByTopicID(TopicID, cSortExpression);
                ResourcePersons = GetResourcePersonListInfoFromResourcePersonInfoList(recordset);
                BasePR.CacheData(key, ResourcePersons);
            }
            return ResourcePersons;
        }

        public static DataSet GetAllSelectedResourcePersonInfoByTopicID(int TopicID, string cSortExpression)
        {
            DataSet ds = new DataSet();
            // provide default sort

            if (cSortExpression == null)
                cSortExpression = "";


            if (cSortExpression.Length == 0)
                cSortExpression = "fullname";

            ds = SiteProvider.PR2.GetAllSelectedResourcePersonInfoByTopicID(TopicID, cSortExpression);


            return ds;
        }
    

        /// <summary>
        /// Bhaskar N
        /// Retrieves all Not Selected ResourcePersons for a TopicID 
        /// </summary>
        public static List<ResourcePerson> GetAllNotSelectedResourcePersonsByTopicID(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "lastfirst";

            List<ResourcePerson> ResourcePersons = null;
            string key = "ResourcePersons_NotSelectedResourcePersons_" + TopicID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
            }
            else
            {
                List<ResourcePersonInfo> recordset = SiteProvider.PR2.GetAllNotSelectedResourcePersonsByTopicID(TopicID, cSortExpression);
                ResourcePersons = GetResourcePersonListInfoFromResourcePersonInfoList(recordset);
                BasePR.CacheData(key, ResourcePersons);
            }
            return ResourcePersons;
        }

        /// <summary>
        /// Updates an existing ResourcePerson
        /// </summary>
        public static bool UpdateResourcePerson(int ResourceID, string Fullname, string LastFirst,
            string Address, string City,
            string State, string Zip, string Country, string Phone, string Email, string SSN, string Specialty,
            string OtherNotes, bool ContrRecd, String DateDiscl, string Disclosure, string StateLic,
            string Degrees, string Position, string Experience, int FacilityID, string RFirstName, string RLastName, string RMiddle,string Photo)
        {
            Fullname = BizObject.ConvertNullToEmptyString(Fullname);
            LastFirst = BizObject.ConvertNullToEmptyString(LastFirst);
            Address = BizObject.ConvertNullToEmptyString(Address);
            City = BizObject.ConvertNullToEmptyString(City);
            State = BizObject.ConvertNullToEmptyString(State);
            Zip = BizObject.ConvertNullToEmptyString(Zip);
            Country = BizObject.ConvertNullToEmptyString(Country);
            Phone = BizObject.ConvertNullToEmptyString(Phone);
            Email = BizObject.ConvertNullToEmptyString(Email);
            SSN = BizObject.ConvertNullToEmptyString(SSN);
            Specialty = BizObject.ConvertNullToEmptyString(Specialty);
            OtherNotes = BizObject.ConvertNullToEmptyString(OtherNotes);
            Disclosure = BizObject.ConvertNullToEmptyString(Disclosure);
            DateDiscl = BizObject.ConvertNullToEmptyString(DateDiscl);
            StateLic = BizObject.ConvertNullToEmptyString(StateLic);
            Degrees = BizObject.ConvertNullToEmptyString(Degrees);
            Position = BizObject.ConvertNullToEmptyString(Position);
            Experience = BizObject.ConvertNullToEmptyString(Experience);
            Photo = BizObject.ConvertNullToEmptyString(Photo);



            ResourcePersonInfo record = new ResourcePersonInfo(ResourceID, Fullname, LastFirst,
                Address, City, State, Zip, Country, Phone, Email, SSN, Specialty,
                OtherNotes, ContrRecd, DateDiscl, Disclosure, StateLic,
                Degrees, Position, Experience, FacilityID, RFirstName, RLastName, RMiddle,Photo);
            bool ret = SiteProvider.PR2.UpdateResourcePerson(record);

            BizObject.PurgeCacheItems("ResourcePersons_ResourcePerson_" + ResourceID.ToString());
            BizObject.PurgeCacheItems("ResourcePersons_ResourcePersons");
            return ret;
        }

        /// <summary>
        /// Creates a new ResourcePerson
        /// </summary>
        public static int InsertResourcePerson(string Fullname, string LastFirst,
            string Address, string City,
            string State, string Zip, string Country, string Phone, string Email, string SSN, string Specialty,
            string OtherNotes, bool ContrRecd, String DateDiscl, string Disclosure, string StateLic,
            string Degrees, string Position, string Experience, int FacilityID, string RFirstName, string RLastName, string RMiddle,string Photo)
        {
            Fullname = BizObject.ConvertNullToEmptyString(Fullname);
            LastFirst = BizObject.ConvertNullToEmptyString(LastFirst);
            Address = BizObject.ConvertNullToEmptyString(Address);
            City = BizObject.ConvertNullToEmptyString(City);
            State = BizObject.ConvertNullToEmptyString(State);
            Zip = BizObject.ConvertNullToEmptyString(Zip);
            Country = BizObject.ConvertNullToEmptyString(Country);
            Phone = BizObject.ConvertNullToEmptyString(Phone);
            Email = BizObject.ConvertNullToEmptyString(Email);
            SSN = BizObject.ConvertNullToEmptyString(SSN);
            Specialty = BizObject.ConvertNullToEmptyString(Specialty);
            OtherNotes = BizObject.ConvertNullToEmptyString(OtherNotes);
            DateDiscl = BizObject.ConvertNullToEmptyString(DateDiscl);
            Disclosure = BizObject.ConvertNullToEmptyString(Disclosure);
            StateLic = BizObject.ConvertNullToEmptyString(StateLic);
            Degrees = BizObject.ConvertNullToEmptyString(Degrees);
            Position = BizObject.ConvertNullToEmptyString(Position);
            Experience = BizObject.ConvertNullToEmptyString(Experience);
            Photo = BizObject.ConvertNullToEmptyString(Photo);


            ResourcePersonInfo record = new ResourcePersonInfo(0, Fullname, LastFirst,
                Address, City, State, Zip, Country, Phone, Email, SSN, Specialty,
                OtherNotes, ContrRecd, DateDiscl, Disclosure, StateLic,
                Degrees, Position, Experience, FacilityID, RFirstName, RLastName, RMiddle,Photo);

            int ret = SiteProvider.PR2.InsertResourcePerson(record);

            BizObject.PurgeCacheItems("ResourcePersons_ResourcePerson");
            return ret;
        }

        /// <summary>
        /// Deletes an existing ResourcePerson, but first checks if OK to delete
        /// </summary>
        public static bool DeleteResourcePerson(int ResourceID)
        {
            bool IsOKToDelete = OKToDelete(ResourceID);
            if (IsOKToDelete)
            {
                return (bool)DeleteResourcePerson(ResourceID, true);
            }
            else
            {
                return false;
            }
        }

        public static int IsResourcePersonExists(string ResourceID)
        {
            int ret = SiteProvider.PR2.IsResourcePersonExists(ResourceID);
            return ret;
        }

        /// <summary>
        /// Deletes an existing ResourcePerson - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteResourcePerson(int ResourceID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteResourcePerson(ResourceID);
            //         new RecordDeletedEvent("ResourcePerson", ResourceID, null).Raise();
            BizObject.PurgeCacheItems("ResourcePersons_ResourcePerson");
            return ret;
        }



        /// <summary>
        /// Checks to see if a ResourcePerson can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int ResourceID)
        {
            return true;
        }
        /// <summary>
        /// Returns a ResourcePerson object filled with the data taken from the input ResourcePersonInfo
        /// </summary>
        private static ResourcePerson GetResourcePersonFromResourcePersonInfo(ResourcePersonInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new ResourcePerson(record.ResourceID, record.Fullname, record.LastFirst,
                   record.Address, record.City, record.State, record.Zip, record.Country, record.Phone, record.Email,
                   record.SSN, record.Specialty, record.OtherNotes, record.ContrRecd, record.DateDiscl, record.Disclosure,
                   record.StateLic, record.Degrees, record.Position, record.Experience, record.FacilityID, record.RFirstName, record.RLastName, record.RMiddle,record.Photo);
                //return new ResourcePerson(record.ResourceID, record.Fullname, record.LastFirst,
                //    record.Address, record.City, record.State, record.Zip, record.Country, record.Phone, record.Email,
                //    record.SSN, record.Specialty, record.OtherNotes, record.ContrRecd, record.DateDiscl, record.Disclosure,
                //    record.StateLic, record.Degrees, record.Position, record.Experience, record.FacilityID);
            }
        }



        /// <summary>
        /// Returns a ResourcePerson object filled with the data taken from the input ResourcePersonInfo
        /// </summary>
        private static ResourcePerson GetResourcePersonInfoFromResourcePersonInfo(ResourcePersonInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new ResourcePerson(record.TopicResID, record.SortOrder, record.ResourceID, record.Fullname, record.LastFirst,
                   record.Address, record.City, record.State, record.Zip, record.Country, record.Phone, record.Email,
                   record.SSN, record.Specialty, record.OtherNotes, record.ContrRecd, record.DateDiscl, record.Disclosure,
                   record.StateLic, record.Degrees, record.Position, record.Experience, record.FacilityID, record.RFirstName, record.RLastName, record.RMiddle,record.Photo);
                //return new ResourcePerson(record.ResourceID, record.Fullname, record.LastFirst,
                //    record.Address, record.City, record.State, record.Zip, record.Country, record.Phone, record.Email,
                //    record.SSN, record.Specialty, record.OtherNotes, record.ContrRecd, record.DateDiscl, record.Disclosure,
                //    record.StateLic, record.Degrees, record.Position, record.Experience, record.FacilityID);
            }
        }

        /// <summary>
        /// Returns a list of ResourcePerson objects filled with the data taken from the input list of ResourcePersonInfo
        /// </summary>
        private static List<ResourcePerson> GetResourcePersonListFromResourcePersonInfoList(List<ResourcePersonInfo> recordset)
        {
            List<ResourcePerson> ResourcePersons = new List<ResourcePerson>();
            foreach (ResourcePersonInfo record in recordset)
                ResourcePersons.Add(GetResourcePersonFromResourcePersonInfo(record));
            return ResourcePersons;
        }

        /// <summary>
        /// Returns a list of ResourcePerson objects filled with the data taken from the input list of ResourcePersonInfo
        /// </summary>
        private static List<ResourcePerson> GetResourcePersonListInfoFromResourcePersonInfoList(List<ResourcePersonInfo> recordset)
        {
            List<ResourcePerson> ResourcePersons = new List<ResourcePerson>();
            foreach (ResourcePersonInfo record in recordset)
                ResourcePersons.Add(GetResourcePersonInfoFromResourcePersonInfo(record));
            return ResourcePersons;
        }

        /// <summary>
        /// Returns a string with authors for a topicid with optional bios (from specialty field)
        /// </summary>
        public static string GetAuthorsByTopicID(int TopicID, bool IncludeBio)
        {
            string cResources = "";
            string cWorkString = "";
            string cDegrees = "";
            int iCount = 0;

            List<ResourcePerson> Authors = ResourcePerson.GetResourcePersonsByTopicIDAndType(TopicID, "A", "");
            foreach (ResourcePerson Author in Authors)
            {
                iCount++;
                cWorkString = cWorkString + "<b>";
                if (iCount > 1 & IncludeBio == false)
                    cWorkString = cWorkString + " and ";

                cWorkString = cWorkString + Author.Fullname.Trim();
                cDegrees = Author.Degrees.Trim();
                if (!String.IsNullOrEmpty(cDegrees))
                    cWorkString = cWorkString + ", " + cDegrees;
                // if bio needed, add newline and insert bio
                if (IncludeBio == true)
                {
                    cWorkString = cWorkString + "</b><br />";
                    if (!String.IsNullOrEmpty(Author.Specialty))
                        cWorkString = cWorkString + Author.Specialty + "<br /><br />";
                }
                else
                {
                    cWorkString = cWorkString + "</b>";
                }
            }
            cResources = cResources + cWorkString;
            return cResources;

        }
        /// <summary>
        /// Returns a string with Editors for a topicid with optional bios (from specialty field)
        /// </summary>
        public static string GetEditorsByTopicID(int TopicID, bool IncludeBio)
        {
            string cResources = "";
            string cWorkString = "";
            string cDegrees = "";
            int iCount = 0;

            List<ResourcePerson> Editors = ResourcePerson.GetResourcePersonsByTopicIDAndType(TopicID, "E", "");
            foreach (ResourcePerson Editor in Editors)
            {
                iCount++;
                cWorkString = cWorkString + "<b>";
                if (iCount > 1 & IncludeBio == false)
                    cWorkString = cWorkString + " and ";

                cWorkString = cWorkString + Editor.Fullname.Trim();
                cDegrees = Editor.Degrees.Trim();
                if (!String.IsNullOrEmpty(cDegrees))
                    cWorkString = cWorkString + ", " + cDegrees;
                // if bio needed, add newline and insert bio
                if (IncludeBio == true)
                {
                    cWorkString = cWorkString + "</b><br />";
                    if (!String.IsNullOrEmpty(Editor.Specialty))
                        cWorkString = cWorkString + Editor.Specialty + "<br /><br />";
                }
                else
                {
                    cWorkString = cWorkString + "</b>";
                }
            }
            cResources = cResources + cWorkString;
            return cResources;

        }

        /// <summary>
        /// Returns a collection with all ResourcePersons linked to a lecture event for a certain sequence number
        /// (sequence number is string holding a numeral 1 - 8, indicating which session in sequence the resource is assigned to)
        /// </summary>
        public static List<ResourcePerson> GetResourcePersonsByLectEvtIDAndSequence(int LectEvtID,
            int Sequence, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LastFirst";

            List<ResourcePerson> ResourcePersons = null;
            string key = "ResourcePersons_ResourcePersons_LectEvt_" + LectEvtID.ToString() + "_" + Sequence.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
            }
            else
            {
                List<ResourcePersonInfo> recordset = SiteProvider.PR2.GetResourcePersonsByLectEvtIDAndSequence(LectEvtID,
                    Sequence, cSortExpression);
                ResourcePersons = GetResourcePersonListFromResourcePersonInfoList(recordset);
                BasePR.CacheData(key, ResourcePersons);
            }
            return ResourcePersons;
        }


        public static List<ResourcePerson> GetResourcePersonsByLectEvtID(int LectEvtID)
        {
          

            List<ResourcePerson> ResourcePersons = null;
            string key = "ResourcePersons_ResourcePersons_LectEvt_" + LectEvtID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
            }
            else
            {
                List<ResourcePersonInfo> recordset = SiteProvider.PR2.GetResourcePersonsByLectEvtID(LectEvtID);
                ResourcePersons = GetResourcePersonListFromResourcePersonInfoList(recordset);
                BasePR.CacheData(key, ResourcePersons);
            }
            return ResourcePersons;
        }

        public static List<ResourcePerson> GetResourcePersonsByResID(int ResourceID)
        {
          

            List<ResourcePerson> ResourcePersons = null;
            string key = "ResourcePersons_ResourcePersons_ResourceID_" + ResourceID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
            }
            else
            {
                List<ResourcePersonInfo> recordset = SiteProvider.PR2.GetResourcePersonsByResID(ResourceID);
                ResourcePersons = GetResourcePersonListFromResourcePersonInfoList(recordset);
                BasePR.CacheData(key, ResourcePersons);
            }
            return ResourcePersons;
        }

        

        #endregion
    }
}
