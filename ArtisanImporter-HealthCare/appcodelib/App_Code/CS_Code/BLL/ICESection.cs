﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr1
    /// </summary>

    public class ICESection : BasePR
    {
        public ICESection(int isid, int topicid, DateTime is_start, DateTime is_end, int maxenroll, int available_seat, DateTime? enroll_close, string lore_url, string ori_url, string is_comment,string facilitator, string facilitator_phone,string facilitator_email)
        {
            this.Isid = isid;
            this.Topicid = topicid;
            this.Is_start = is_start;
            this.Is_end = is_end;
            this.Maxenroll = maxenroll;
            this.Available_seat = available_seat;
            this.Enroll_close = enroll_close;
            this.Lore_url = lore_url;
            this.Ori_url = ori_url;
            this.Is_comment = is_comment;
            this.Facilitator = facilitator;
            this.Facilitator_phone = facilitator_phone;
            this.Facilitator_email = facilitator_email;
        }


        #region Variables and Properties
        private int _isid = 0;
        public int Isid
        {
            get { return _isid; }
            set { _isid = value; }
        }
        private int _topicid = 0;
        public int Topicid
        {
            get { return _topicid; }
            set { _topicid = value; }
        }
        private DateTime _is_start;
        public DateTime Is_start
        {
            get { return _is_start; }
            set { _is_start = value; }
        }
        private DateTime _is_end;
        public DateTime Is_end
        {
            get { return _is_end; }
            set { _is_end = value; }
        }
        private int _maxenroll = 0;
        public int Maxenroll
        {
            get { return _maxenroll; }
            set { _maxenroll = value; }
        }
        private int _available_seat = 0;
        public int Available_seat
        {
            get { return _available_seat; }
            set { _available_seat = value; }
        }
        private DateTime? _enroll_close;
        public DateTime? Enroll_close
        {
            get { return _enroll_close; }
            set { _enroll_close = value; }
        }
        private string _lore_url = "";
        public string Lore_url
        {
            get { return _lore_url; }
            set { _lore_url = value; }
        }
        private string _ori_url = "";
        public string Ori_url
        {
            get { return _ori_url; }
            set { _ori_url = value; }
        }
        private string _is_comment = "";
        public string Is_comment
        {
            get { return _is_comment; }
            set { _is_comment = value; }
        }  
        private string _facilitator = "";
        public string Facilitator
        {
            get { return _facilitator; }
            set { _facilitator = value; }
        }

        private string _facilitator_phone = "";
        public string Facilitator_phone
        {
            get { return _facilitator_phone; }
            set { _facilitator_phone = value; }
        }

        private string _facilitator_email = "";
        public string Facilitator_email
        {
            get { return _facilitator_email; }
            set { _facilitator_email = value; }
        }  
        


        #endregion Variables and Properties

        #region Methods
        public static int insertICESection(int topicid, DateTime is_start, DateTime is_end, int maxenroll, int available_seat, DateTime? enroll_close, string lore_url,string ori_url, string is_comment, string facilitator, string facilitator_phone, string facilitator_email)
        {
            ICESectionInfo record = new ICESectionInfo(0, topicid, is_start, is_end, maxenroll, available_seat, enroll_close, lore_url, ori_url, is_comment, facilitator,  facilitator_phone, facilitator_email);            
            int ret = SiteProvider.PR2.insertICESection(record);
            BizObject.PurgeCacheItems("ICESection_ICESection_Insert");
            return ret;
        }

        public int insert()
        {
            return insertICESection(this.Topicid, this.Is_start, this.Is_end, this.Maxenroll, this.Available_seat,
                this.Enroll_close, this.Lore_url,this.Ori_url, this.Is_comment,this.Facilitator, this.Facilitator_phone,this.Facilitator_email);
        }

        public static bool updateICESection(int isid, int topicid, DateTime is_start, DateTime is_end, int maxenroll, int available_seat, DateTime? enroll_close, string lore_url,string ori_url, string is_comment, string facilitator, string facilitator_phone, string facilitator_email)
        {
            ICESectionInfo record = new ICESectionInfo(isid, topicid, is_start, is_end, maxenroll, available_seat, enroll_close, lore_url,ori_url, is_comment, facilitator, facilitator_phone, facilitator_email);            
            bool ret = SiteProvider.PR2.updateICESection(record);
            BizObject.PurgeCacheItems("ICESection_ICESection_Update" + isid.ToString());
            return ret;
        }

        public bool update()
        {
            return updateICESection(this.Isid, this.Topicid, this.Is_start, this.Is_end, this.Maxenroll, this.Available_seat, this.Enroll_close, this.Lore_url, this.Ori_url, this.Is_comment, this.Facilitator, this.Facilitator_phone, this.Facilitator_email);                
        }

        public bool deleteICESection(int isid)
        {
            bool ret = SiteProvider.PR2.deleteICESection(isid);
            BizObject.PurgeCacheItems("ICESection_ICESection_Delete" + isid.ToString());
            return ret;
        }

        public static ICESection GetICESectionByID(int isid)
        {
            ICESection ICESection = null;
            string key = "ICESection_GetICESectionByID_" + isid.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ICESection = (ICESection)BizObject.Cache[key];
            }
            else
            {
                ICESection = GetICESectionFromICESectionInfo(SiteProvider.PR2.GetICESectionByID(isid));
                BasePR.CacheData(key, ICESection);
            }
            return ICESection;
        }

        public static DataSet GetEnrolleesByTopicID(int topicid, ref string reported_date, ref string topicname)
        {
            return SiteProvider.PR2.GetEnrolleesByTopicID(topicid,ref reported_date,ref topicname);
        }

        public static DataSet GetSeriesAboutToStart()
        {
            return SiteProvider.PR2.GetSeriesAboutToStart();
        }

        public static ICESection GetICESectionByTopicID(int topicid)
        {
            ICESection ICESection = null;
            string key = "ICESection_GetICESectionByTopicID_" + topicid.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ICESection = (ICESection)BizObject.Cache[key];
            }
            else
            {
                ICESection = GetICESectionFromICESectionInfo(SiteProvider.PR2.GetICESectionByTopicID(topicid));
                BasePR.CacheData(key, ICESection);
            }
            return ICESection;
        }

        public static bool IsEnrolledICE(int isid, int userid) 
        {
            return SiteProvider.PR2.IsEnrolledICE(isid, userid);
        }

        public static bool IsEnrolledOrCompletedICE(int isid, int userid)
        {
            return SiteProvider.PR2.IsEnrolledOrCompletedICE(isid, userid);
        }

        public static List<ICESection> GetICESectionListfromICESectionInfo(List<ICESectionInfo> recordset)
        {
            List<ICESection> ICESectionList = new List<ICESection>();
            foreach (ICESectionInfo ICESectionninfo in recordset)
            {
                ICESection ICESection = GetICESectionFromICESectionInfo(ICESectionninfo);

                ICESectionList.Add(ICESection);
            }
            return ICESectionList;
        }
        

        private static ICESection GetICESectionFromICESectionInfo(ICESectionInfo ICESectionninfo)
        {
            if (ICESectionninfo == null)
                return null;

            ICESection ICESection = new ICESection(ICESectionninfo.Isid, ICESectionninfo.Topicid,
                ICESectionninfo.Is_start, ICESectionninfo.Is_end, ICESectionninfo.Maxenroll, ICESectionninfo.Available_seat, ICESectionninfo.Enroll_close, ICESectionninfo.Lore_url, ICESectionninfo.Ori_url, 
                ICESectionninfo.Is_comment, ICESectionninfo.Facilitator, ICESectionninfo.Facilitator_phone, ICESectionninfo.Facilitator_email);
            return ICESection;
        }
        //
        private static List<ICESection> GetICESectionListFromICESectionInfoList(List<ICESectionInfo> recordset)
        {
            List<ICESection> ICESectionList = new List<ICESection>();
            foreach (ICESectionInfo record in recordset)
                ICESectionList.Add(GetICESectionFromICESectionInfo(record));
            return ICESectionList;
        }

        #endregion Methods

    }

}
