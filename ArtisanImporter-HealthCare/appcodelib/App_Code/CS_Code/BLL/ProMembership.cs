﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

/// <summary>
/// Summary description for ProMembership
/// </summary>

namespace PearlsReview.BLL
{
    public class ProMembership : BasePR
    {
        public ProMembership(int memberid, int userid, DateTime paydate, DateTime expiredate, decimal payamount,
                int facilityid, string authcode, string profileid, bool active, bool renew, decimal couponamount, int couponid,
                string cclast, string comment, DateTime lastupdate)
        {
            this.Memberid = memberid;
            this.Userid = userid;
            this.Paydate = paydate;
            this.Expiredate = expiredate;
            this.Payamount = payamount;
            this.Facilityid = facilityid;
            this.Authcode = authcode;
            this.profileid = profileid;
            this.Active = active;
            this.Renew = renew;
            this.Couponamount = couponamount;
            this.Couponid = couponid;
            this.Cclast = cclast;
            this.Comment = comment;
            this.Lastupdate = lastupdate;
        }
        #region Variables and Properties
        private int _memberid = 0;
        public int Memberid
        {
            get { return _memberid; }
            set { _memberid = value; }
        }
        private int _userid = 0;
        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }
        private DateTime _paydate = System.DateTime.MinValue;
        public DateTime Paydate
        {
            get { return _paydate; }
            set { _paydate = value; }
        }
        private DateTime _expiredate = System.DateTime.MinValue;
        public DateTime Expiredate
        {
            get { return _expiredate; }
            set { _expiredate = value; }
        }
        private decimal _payamount = 0;
        public decimal Payamount
        {
            get { return _payamount; }
            set { _payamount = value; }
        }
        private int _facilityid = 0;
        public int Facilityid
        {
            get { return _facilityid; }
            set { _facilityid = value; }
        }
        private string _authcode;
        public string Authcode
        {
            get { return _authcode; }
            set { _authcode = value; }
        }
        private string _profileid;
        public string profileid
        {
            get { return _profileid; }
            set { _profileid = value; }
        }
        private Boolean _active = false;
        public Boolean Active
        {
            get { return _active; }
            set { _active = value; }
        }
        private Boolean _renew = false;
        public Boolean Renew
        {
            get { return _renew; }
            set { _renew = value; }
        }
        private decimal _couponamount = 0;
        public decimal Couponamount
        {
            get { return _couponamount; }
            set { _couponamount = value; }
        }
        private int _couponid = 0;
        public int Couponid
        {
            get { return _couponid; }
            set { _couponid = value; }
        }
        private string _cclast;
        public string Cclast
        {
            get { return _cclast; }
            set { _cclast = value; }
        }
        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }
        private DateTime _lastupdate = System.DateTime.MinValue;
        public DateTime Lastupdate
        {
            get { return _lastupdate; }
            set { _lastupdate = value; }
        }
        #endregion Variables and Properties
        
        #region Methods
        public static int insertProMembership(int userid, DateTime paydate, DateTime expiredate, decimal payamount,
                int facilityid, string authcode, string profileid, bool active, bool renew, decimal couponamount, int couponid,
                string cclast, string comment, DateTime lastupdate)
        {
            ProMembershipInfo record = new ProMembershipInfo(0, userid, paydate, expiredate, payamount,
                facilityid, authcode, profileid, active, renew, couponamount, couponid,
                cclast, comment, lastupdate);

            int ret = SiteProvider.PR2.insertProMembership(record);
            BizObject.PurgeCacheItems("insertProMembership_Insert");
            return ret;
        }

        public int insert()
        {
            return insertProMembership(this.Userid, this.Paydate, this.Expiredate, this.Payamount, this.Facilityid,
                this.Authcode, this.profileid, this.Active,  this.Renew, this.Couponamount, this.Couponid, this.Cclast,this.Comment, this.Lastupdate);
        }

        public static bool updateProMembership(int memberid, int userid, DateTime paydate, DateTime expiredate, decimal payamount,
                int facilityid, string authcode, string profileid, bool active, bool renew, decimal couponamount, int couponid,
                string cclast, string comment, DateTime lastupdate)
        {
            ProMembershipInfo record = new ProMembershipInfo(memberid, userid, paydate, expiredate, payamount,
                facilityid, authcode, profileid, active, renew, couponamount, couponid, cclast, comment, lastupdate);

            bool ret = SiteProvider.PR2.updateProMembership(record);
            BizObject.PurgeCacheItems("updateProMembership_Update" + memberid.ToString());
            return ret;
        }

        public bool update()
        {
            return updateProMembership(this.Memberid, this.Userid, this.Paydate, this.Expiredate, this.Payamount, this.Facilityid,
                this.Authcode, this.profileid, this.Active, this.Renew, this.Couponamount, this.Couponid, this.Cclast, this.Comment, this.Lastupdate);
        }

        public bool deleteProMembership(int UserID)
        {
            bool ret = SiteProvider.PR2.deleteProMembership(UserID);
            BizObject.PurgeCacheItems("deleteProMembership_Delete" + UserID.ToString());
            return ret;
        }

        public static ProMembership GetProMemeberShipByMemberID(int MemberID)
        {
            ProMembership proMembership = null;
            string key = "ProMembershipInfo_GetProMemeberShipByMemberID_" + MemberID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                proMembership = (ProMembership)BizObject.Cache[key];
            }
            else
            {
                proMembership = GetProMembershipFromProMembershipInfo(SiteProvider.PR2.GetProMemeberShipByMemberID(MemberID));
                BasePR.CacheData(key, proMembership);
            }
            return proMembership;
        }
        public static ProMembership GetProMemeberShipByUserID(int UserID)
        {
            ProMembership proMembership = null;
            string key = "ProMembershipInfo_GetProMemeberShipByUserID_" + UserID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                proMembership = (ProMembership)BizObject.Cache[key];
            }
            else
            {
                proMembership = GetProMembershipFromProMembershipInfo(SiteProvider.PR2.GetProMemeberShipByUserID(UserID));
                BasePR.CacheData(key, proMembership);
            }
            return proMembership;
        }
        public static ProMembership GetExpiredMemeberShipByUserID(int UserID)
        {
            ProMembership proMembership = null;
            string key = "ProMembershipInfo_GetExpiredMemeberShipByUserID_" + UserID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                proMembership = (ProMembership)BizObject.Cache[key];
            }
            else
            {
                proMembership = GetProMembershipFromProMembershipInfo(SiteProvider.PR2.GetExpiredMemeberShipByUserID(UserID));
                BasePR.CacheData(key, proMembership);
            }
            return proMembership;
        }
        public static ProMembership GetActiveProMemeberShipByUserID(int UserID)
        {
            ProMembership proMembership = null;
            string key = "ProMembershipInfo_active_" + UserID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                proMembership = (ProMembership)BizObject.Cache[key];
            }
            else
            {
                proMembership = GetProMembershipFromProMembershipInfo(SiteProvider.PR2.GetActiveProMemeberShipByUserID(UserID));
                BasePR.CacheData(key, proMembership);
            }
            return proMembership;
        }
        public static List<ProMembership> DailyCEmembershipUpdate()
        {
            return GetProMembershipListFromProMembershipInfoList(SiteProvider.PR2.DailyCEmembershipUpdate());
        }
        public static List<ProMembership> DailyCEMembershipExpiredMembershipNoAutoRenew()
        {
            return GetProMembershipListFromProMembershipInfoList(SiteProvider.PR2.DailyCEMembershipExpiredMembershipNoAutoRenew());
        }
        public static List<ProMembership> DailyCEMembershipCancelledAutoRenewUpdate()
        {
            return GetProMembershipListFromProMembershipInfoList(SiteProvider.PR2.DailyCEMembershipCancelledAutoRenewUpdate());
        }
        public static DataTable GetCEProMembershipSearchResult(int UserID, string FirstName, string LastName, string Paydate, string ExpireDate, string Active, string Renew, string OrderBy)
        {
            return SiteProvider.PR2.GetCEProMembershipSearchResult(UserID, FirstName, LastName, Paydate, ExpireDate, Active, Renew, OrderBy);
        }
        public static DataSet GetCEUserAccountExpireDate()
        {
            return SiteProvider.PR2.GetCEUserAccountExpireDate();
        }

        public static DataSet GetCEUserAccountExpireDateWithoutRenew()
        {
            return SiteProvider.PR2.GetCEUserAccountExpireDateWithoutRenew();
        }
        public static int UpdateDailyCEMembershipGracePeriodExpireDate()
        {
            return SiteProvider.PR2.UpdateDailyCEMembershipGracePeriodExpireDate();
        }
        public static int UpdateFailedRenewDailyCEMembership(int userid)
        {
            return SiteProvider.PR2.UpdateFailedRenewDailyCEMembership(userid);
        }
        public static bool DailyCEUserAccountUpdate(int userid)
        {
            return SiteProvider.PR2.DailyCEUserAccountUpdate(userid);
        }
        public static decimal GetCEPROSavedTotalByUserID(int UserID)
        {
            return SiteProvider.PR2.GetCEPROSavedTotalByUserID(UserID);
        }
        public static decimal GetCEPROMissedSavedTotalByUserID(int UserID)
        {
            return SiteProvider.PR2.GetCEPROMissedSavedTotalByUserID(UserID);
        }
        public static int RenewCEMembership(int userid, string AuthorizationCode, string ProfileId, string cCCRightFour,
            decimal CEPROPrice, DateTime ExpireDate, decimal couponAmount, int couponID, bool renew)
        {
            return SiteProvider.PR2.RenewCEMembership(userid, AuthorizationCode, ProfileId, 
                cCCRightFour, CEPROPrice, ExpireDate, couponAmount, couponID, renew);
        }

        public static int CEMembershipCancelAutoRenew(int userid, string comment)
        {
            if (String.IsNullOrEmpty(comment))
                comment = "CEPRO auto renew cancelled";

            return SiteProvider.PR2.CEMembershipCancelAutoRenew(userid, comment);
        }

        public static int CEMembershipCancelMembership(int userid, string comment)
        {
            if (String.IsNullOrEmpty(comment))
                comment = "CEPRO membership cancelled";

            return SiteProvider.PR2.CEMembershipCancelMembership(userid, comment);
        }
        public static int CEMembershipActiveAutoRenewNewProfile(int userid, string ProfileID, string CCLast)
        {
            return SiteProvider.PR2.CEMembershipActiveAutoRenewNewProfile(userid, ProfileID, CCLast);
        }
        public static int CEMembershipActiveAutoRenew(int userid)
        {
            return SiteProvider.PR2.CEMembershipActiveAutoRenew(userid);
        }
        public static int CEMembershipActiveAutoRenew(int userid, string profileid)
        {
            return SiteProvider.PR2.CEMembershipActiveAutoRenew(userid, profileid);
        }
        private static ProMembership GetProMembershipFromProMembershipInfo(ProMembershipInfo promembershipinfo)
        {
            if (promembershipinfo == null)
                return null;

            ProMembership proMembership = new ProMembership(promembershipinfo.Memberid, promembershipinfo.Userid, promembershipinfo.Paydate,
                promembershipinfo.Expiredate, promembershipinfo.Payamount, promembershipinfo.Facilityid, promembershipinfo.Authcode, promembershipinfo.profileid,
                promembershipinfo.Active, promembershipinfo.Renew, promembershipinfo.Couponamount, promembershipinfo.Couponid, promembershipinfo.Cclast,
                promembershipinfo.Comment, promembershipinfo.Lastupdate);

            return proMembership;
        }

        private static List<ProMembership> GetProMembershipListFromProMembershipInfoList(List<ProMembershipInfo> recordset)
        {
            List<ProMembership> proMembership = new List<ProMembership>();
            foreach (ProMembershipInfo record in recordset)
                proMembership.Add(GetProMembershipFromProMembershipInfo(record));
            return proMembership;
        }
        #endregion Methods
    }
}
