﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for MicrositeAds
    /// </summary>
    public class MicrositeAds : BasePR
    {
        #region Variables and Properties

        private int _id = 0;
        public int ID
        {
            get { return _id; }
            protected set { _id = value; }
        }
        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }
        private string _file_name = "";
        public string File_name
        {
            get { return _file_name; }
            private set { _file_name = value; }
        }
        private string _ads_url = "";
        public string Ads_url
        {
            get { return _ads_url; }
            private set { _ads_url = value; }
        }
        private string _ads_desc = "";
        public string Ads_desc
        {
            get { return _ads_desc; }
            private set { _ads_desc = value; }
        }
        private int _ads_order = 0;
        public int Ads_order
        {
            get { return _ads_order; }
            protected set { _ads_order = value; }
        }
        private string _ads_type = "";
        public string Ads_type
        {
            get { return _ads_type; }
            private set { _ads_type = value; }
        }
        private string _ads_html = "";
        public string Ads_html
        {
            get { return _ads_html; }
            private set { _ads_html = value; }
        }
        public MicrositeAds()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public MicrositeAds(int id, int msid, string file_name, string ads_url, string ads_desc, int ads_order, string ads_type, string ads_html)
        {
            this.ID = id;
            this.Msid = msid;
            this.File_name = file_name;
            this.Ads_url = ads_url;
            this.Ads_desc = ads_desc;
            this.Ads_order = ads_order;
            this.Ads_type = ads_type;
            this.Ads_html = ads_html;
        }
        public bool Delete()
        {
            bool success = MicrositeAds.DeleteMicrositeAds(this.ID);
            if (success)
                this.Msid = 0;
            return success;
        }

        public bool Update()
        {
            return MicrositeAds.UpdateMicrositeAds(this.ID, this.Msid, this.File_name, this.Ads_url, this.Ads_desc, this.Ads_order, this.Ads_type, this.Ads_html);
        }

        #endregion
        #region Methods
        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all MicrositeDomains
        //</summary>
        public static List<MicrositeAds> GetMicrositeAds(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "file_name";

            List<MicrositeAds> MicrositeAdss = null;
            string key = "MicrositeAdss_MicrositeAdss_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeAdss = (List<MicrositeAds>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeAdsInfo> recordset = SiteProvider.PR2.GetMicrositeAds(cSortExpression);
                MicrositeAdss = GetMicrositeAdsListFromMicrositeAdsInfoList(recordset);
                BasePR.CacheData(key, MicrositeAdss);
            }
            return MicrositeAdss;
        }
        public static int GetMicrositeAdsMaxOrder(int msid)
        {
            return SiteProvider.PR2.GetMicrositeAdsMaxOrder(msid);
        }
        public static List<MicrositeAds> GetMicrositeAdsByMsid(int Msid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "file_name";

            List<MicrositeAds> MicrositeAdss = null;
            string key = "MicrositeAdss_MicrositeAdss_" + Msid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeAdss = (List<MicrositeAds>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeAdsInfo> recordset = SiteProvider.PR2.GetMicrositeAdsByMsid(Msid, cSortExpression);
                MicrositeAdss = GetMicrositeAdsListFromMicrositeAdsInfoList(recordset);
                BasePR.CacheData(key, MicrositeAdss);
            }
            return MicrositeAdss;
        }
        /// <summary>
        /// Returns a MicrositeDomains object with the specified ID
        /// </summary>
        public static MicrositeAds GetMicrositeAdsByID(int ID)
        {
            MicrositeAds MicrositeAdss = null;
            string key = "MicrositeAdss_MicrositeAdss_" + ID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeAdss = (MicrositeAds)BizObject.Cache[key];
            }
            else
            {
                MicrositeAdss = GetMicrositeAdsFromMicrositeAdsInfo(SiteProvider.PR2.GetMicrositeAdsByID(ID));
                BasePR.CacheData(key, MicrositeAdss);
            }
            return MicrositeAdss;
        }
        public static MicrositeAds GetMicrositeAdsByID(int Msid, string file_name)
        {
            MicrositeAds MicrositeAdss = null;
            string key = "MicrositeAdss_MicrositeAdss_" + Msid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeAdss = (MicrositeAds)BizObject.Cache[key];
            }
            else
            {
                MicrositeAdss = GetMicrositeAdsFromMicrositeAdsInfo(SiteProvider.PR2.GetMicrositeAdsByID(Msid, file_name));
                BasePR.CacheData(key, MicrositeAdss);
            }
            return MicrositeAdss;
        }
        public static int InsertMicrositeAdss(int msid, string file_name, string ads_url, string ads_desc, int ads_order, string ads_type, string ads_html)
        {
            file_name = BizObject.ConvertNullToEmptyString(file_name);
            ads_url = BizObject.ConvertNullToEmptyString(ads_url);

            MicrositeAdsInfo record = new MicrositeAdsInfo(0, msid, file_name, ads_url, ads_desc, ads_order, ads_type, ads_html);
            int ret = SiteProvider.PR2.InsertMicrositeAds(record);

            BizObject.PurgeCacheItems("MicrositeAdss_MicrositeAdss");
            return ret;
        }
        /// <summary>
        /// Updates an existing MicrositeDomain
        /// </summary>
        public static bool UpdateMicrositeAds(int id, int Msid, string file_name, string ads_url, string ads_desc, int ads_order, string ads_type, string ads_html)
        {
            file_name = BizObject.ConvertNullToEmptyString(file_name);
            ads_url = BizObject.ConvertNullToEmptyString(ads_url);

            MicrositeAdsInfo record = new MicrositeAdsInfo(id, Msid, file_name, ads_url, ads_desc, ads_order, ads_type, ads_html);
            bool ret = SiteProvider.PR2.UpdateMicrositeAds(record);

            BizObject.PurgeCacheItems("MicrositeAdss_MicrositeAdss_" + Msid.ToString());
            BizObject.PurgeCacheItems("MicrositeAdss_MicrositeAdss");
            return ret;
        }

        /// <summary>
        /// Deletes an existing MicrositeDomain, but first checks if OK to delete
        /// </summary>
        public static bool DeleteMicrositeAds(int id)
        {
            bool IsOKToDelete = OKToDelete(id);
            if (IsOKToDelete)
            {
                return (bool)DeleteMicrositeAds(id, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing MicrositeDomain - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteMicrositeAds(int id, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteMicrositeAds(id);
            BizObject.PurgeCacheItems("MicrositeAdss_MicrositeAdss");
            return ret;
        }

        /// <summary>
        /// Checks to see if a MicrositeDomain can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }


        /// <summary>
        /// Returns a MicrositeDomain object filled with the data taken from the input MicrositeDomainInfo
        /// </summary>
        private static MicrositeAds GetMicrositeAdsFromMicrositeAdsInfo(MicrositeAdsInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new MicrositeAds(record.ID, record.Msid, record.File_name, record.Ads_url, record.Ads_desc, record.Ads_order, record.Ads_type, record.Ads_html);
            }
        }

        /// <summary>
        /// Returns a list of MicrositeDomain objects filled with the data taken from the input list of MicrositeDomainInfo
        /// </summary>
        private static List<MicrositeAds> GetMicrositeAdsListFromMicrositeAdsInfoList(List<MicrositeAdsInfo> recordset)
        {
            List<MicrositeAds> MicrositeAdss = new List<MicrositeAds>();
            foreach (MicrositeAdsInfo record in recordset)
                MicrositeAdss.Add(GetMicrositeAdsFromMicrositeAdsInfo(record));
            return MicrositeAdss;
        }
        #endregion
    }
}