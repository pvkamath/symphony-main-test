﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for CategoryLink
/// </summary>
    public class CategoryLink : BasePR
    {
        #region Variables and Properties

        private int _CatTopicID = 0;
        public int CatTopicID
        {
            get { return _CatTopicID; }
            protected set { _CatTopicID = value; }
        }

        private int _CategoryID = 0;
        public int CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }


        public CategoryLink(int CatTopicID, int CategoryID, int TopicID)
        {
            this.CatTopicID = CatTopicID;
            this.CategoryID = CategoryID;
            this.TopicID = TopicID;
        }

        public bool Delete()
        {
            bool success = CategoryLink.DeleteCategoryLink(this.CatTopicID);
            if (success)
                this.CatTopicID = 0;
            return success;
        }

        public bool Update()
        {
            return CategoryLink.UpdateCategoryLink(this.CatTopicID, this.CategoryID, this.TopicID);
        }
        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all CategoryLinks
        /// </summary>
        public static List<CategoryLink> GetCategoryLinks(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<CategoryLink> CategoryLinks = null;
            string key = "CategoryLinks_CategoryLinks_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CategoryLinks = (List<CategoryLink>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryLinkInfo> recordset = SiteProvider.PR2.GetCategoryLinks(cSortExpression);
                CategoryLinks = GetCategoryLinkListFromCategoryLinkInfoList(recordset);
                BasePR.CacheData(key, CategoryLinks);
            }
            return CategoryLinks;
        }


        /// <summary>
        /// Returns the number of total CategoryLinks
        /// </summary>
        public static int GetCategoryLinkCount()
        {
            int CategoryLinkCount = 0;
            string key = "CategoryLinks_CategoryLinkCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CategoryLinkCount = (int)BizObject.Cache[key];
            }
            else
            {
                CategoryLinkCount = SiteProvider.PR2.GetCategoryLinkCount();
                BasePR.CacheData(key, CategoryLinkCount);
            }
            return CategoryLinkCount;
        }

        /// <summary>
        /// Returns a CategoryLink object with the specified ID
        /// </summary>
        public static CategoryLink GetCategoryLinkByID(int CatTopicID)
        {
            CategoryLink CategoryLink = null;
            string key = "CategoryLinks_CategoryLink_" + CatTopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CategoryLink = (CategoryLink)BizObject.Cache[key];
            }
            else
            {
                CategoryLink = GetCategoryLinkFromCategoryLinkInfo(SiteProvider.PR2.GetCategoryLinkByID(CatTopicID));
                BasePR.CacheData(key, CategoryLink);
            }
            return CategoryLink;
        }

        /// <summary>
        /// Updates an existing CategoryLink
        /// </summary>
        public static bool UpdateCategoryLink(int CatTopicID, int CategoryID, int TopicID)
        {


            CategoryLinkInfo record = new CategoryLinkInfo(CatTopicID, CategoryID, TopicID);
            bool ret = SiteProvider.PR2.UpdateCategoryLink(record);

            BizObject.PurgeCacheItems("CategoryLinks_CategoryLink_" + CatTopicID.ToString());
            BizObject.PurgeCacheItems("CategoryLinks_CategoryLinks");
            return ret;
        }

        /// <summary>
        /// Creates a new CategoryLink
        /// </summary>
        public static int InsertCategoryLink(int CategoryID, int TopicID)
        {


            CategoryLinkInfo record = new CategoryLinkInfo(0, CategoryID, TopicID);
            int ret = SiteProvider.PR2.InsertCategoryLink(record);

            BizObject.PurgeCacheItems("CategoryLinks_CategoryLink");
            return ret;
        }

        /// <summary>
        /// Deletes an existing CategoryLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteCategoryLink(int CatTopicID)
        {
            bool IsOKToDelete = OKToDelete(CatTopicID);
            if (IsOKToDelete)
            {
                return (bool)DeleteCategoryLink(CatTopicID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing CategoryLink - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteCategoryLink(int CatTopicID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteCategoryLink(CatTopicID);
            //         new RecordDeletedEvent("CategoryLink", CatTopicID, null).Raise();
            BizObject.PurgeCacheItems("CategoryLinks_CategoryLink");
            return ret;
        }



        /// <summary>
        /// Checks to see if a CategoryLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int CatTopicID)
        {
            return true;
        }



        /// <summary>
        /// Returns a CategoryLink object filled with the data taken from the input CategoryLinkInfo
        /// </summary>
        private static CategoryLink GetCategoryLinkFromCategoryLinkInfo(CategoryLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new CategoryLink(record.CatTopicID, record.CategoryID, record.TopicID);
            }
        }

        /// <summary>
        /// Returns a list of CategoryLink objects filled with the data taken from the input list of CategoryLinkInfo
        /// </summary>
        private static List<CategoryLink> GetCategoryLinkListFromCategoryLinkInfoList(List<CategoryLinkInfo> recordset)
        {
            List<CategoryLink> CategoryLinks = new List<CategoryLink>();
            foreach (CategoryLinkInfo record in recordset)
                CategoryLinks.Add(GetCategoryLinkFromCategoryLinkInfo(record));
            return CategoryLinks;
        }    
        #endregion
    }
}
