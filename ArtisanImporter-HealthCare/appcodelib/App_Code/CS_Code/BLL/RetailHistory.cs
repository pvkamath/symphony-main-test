﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr1
    /// </summary>

    public class RetailHistory : BasePR
    {
        public RetailHistory(int rhid, int facid, DateTime paydate, int seat, decimal payamount, string paystatus, string membertype, string transid, string paycomment, decimal couponamount, int couponid, string cclast)
        {
            this.Rhid = rhid;
            this.FacId = facid;
            this.Paydate = paydate;
            this.Seat = seat;
            this.PayAmount = payamount;
            this.Paystatus = paystatus;
            this.MemberType = membertype;
            this.TransId = transid;
            this.PayComment = paycomment;
            this.CouponAmount = couponamount;
            this.CouponId = couponid;
            this.CCLast = cclast;
            this.Description = this.PayComment + " " + this.Paydate.ToShortDateString();
        }


        #region Variables and Properties
        private int _rhid = 0;
        public int Rhid
        {
            get { return _rhid; }
            set { _rhid = value; }
        }
        private int _facid = 0;
        public int FacId
        {
            get { return _facid; }
            set { _facid = value; }
        }
        private DateTime _paydate = System.DateTime.MinValue;
        public DateTime Paydate
        {
            get { return _paydate; }
            set { _paydate = value; }
        }
        private int _seat = 0;
        public int Seat
        {
            get { return _seat; }
            set { _seat = value; }
        }
        private decimal _payamount = 0;
        public decimal PayAmount
        {
            get { return _payamount; }
            set { _payamount = value; }
        }
        private string _paystatus = "";
        public string Paystatus
        {
            get { return _paystatus; }
            set { _paystatus = value; }
        }
        private string _membertype = "";
        public string MemberType
        {
            get { return _membertype; }
            set { _membertype = value; }
        }
        private string _transid = "";
        public string TransId
        {
            get { return _transid; }
            set { _transid = value; }
        }
        private string _paycomment = "";
        public string PayComment
        {
            get { return _paycomment; }
            set { _paycomment = value; }
        }
        private decimal _couponamount = 0;
        public decimal CouponAmount
        {
            get { return _couponamount; }
            set { _couponamount = value; }
        }
        private int _couponid = 0;
        public int CouponId
        {
            get { return _couponid; }
            set { _couponid = value; }
        }
        private string _cclast = "";
        public string CCLast
        {
            get { return _cclast; }
            set { _cclast = value; }
        }
        private string _description = "";
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion Variables and Properties

        #region Methods
        /// <summary>
        /// Returns a collection with all Retail History by FacilityID
        /// </summary>
        public static List<RetailHistory> GetRetailHistoryListByFacID(int facid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "PayDate desc";

            List<RetailHistory> rHistoryList = null;
            string key = "RetailHistory_HistoryByFacid_" + facid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                rHistoryList = (List<RetailHistory>)BizObject.Cache[key];
            }
            else
            {
                List<RetailHistoryInfo> recordset = SiteProvider.PR2.GetRetailHistoryListByFacID(facid, cSortExpression);
                rHistoryList = GetRetailHistoryListFromRetailHistoryInfoList(recordset);
                BasePR.CacheData(key, rHistoryList);
            }
            return rHistoryList;
        }
        /// <summary>
        /// Returns a collection with all Retail History by FacilityID
        /// </summary>
        public static RetailHistory GetLastRetailHistoryByFacID(int facid)
        {
            RetailHistory rHistory = null;
            string key = "RetailHistory_LastHistoryByFacid_" + facid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                rHistory = (RetailHistory)BizObject.Cache[key];
            }
            else
            {
                RetailHistoryInfo record = SiteProvider.PR2.GetLastRetailHistoryByFacID(facid);
                rHistory = GetRetailHistoryFromRetailHistoryInfo(record);
                BasePR.CacheData(key, rHistory);
            }
            return rHistory;
        }


        public static int insertRetailHistory(int facid, DateTime paydate, int seat, decimal payamount, string paystatus, string membertype, string transid, string paycomment, decimal couponamount, int couponid, string cclast)
        {
            RetailHistoryInfo record = new RetailHistoryInfo(0, facid, paydate, seat, payamount, paystatus, membertype, transid, paycomment, couponamount, couponid, cclast);
            int ret = SiteProvider.PR2.insertRetailHistory(record);
            BizObject.PurgeCacheItems("RetailHistory_RetailHistory_Insert");
            return ret;
        }

        public int insert()
        {
            return insertRetailHistory(this.FacId, this.Paydate, this.Seat, this.PayAmount, this.Paystatus, this.MemberType, this.TransId, this.PayComment, this.CouponAmount, this.CouponId, this.CCLast);
        }

        public static bool updateRetailHistory(int rhid, int facid, DateTime paydate, int seat, decimal payamount, string paystatus, string membertype, string transid, string paycomment, decimal couponamount, int couponid, string cclast)
        {
            RetailHistoryInfo record = new RetailHistoryInfo(rhid, facid, paydate, seat, payamount, paystatus, membertype, transid, paycomment, couponamount, couponid, cclast);
            bool ret = SiteProvider.PR2.updateRetailHistory(record);
            BizObject.PurgeCacheItems("RetailHistory_RetailHistory_Update" + rhid.ToString());
            return ret;
        }

        public bool update()
        {
            return updateRetailHistory(this.Rhid, this.FacId, this.Paydate, this.Seat, this.PayAmount, this.Paystatus, this.MemberType, this.TransId, this.PayComment, this.CouponAmount, this.CouponId, this.CCLast);
        }

        public bool deleteRetailHistory(int rhid)
        {
            bool ret = SiteProvider.PR2.deleteRetailHistory(rhid);
            BizObject.PurgeCacheItems("RetailHistory_RetailHistory_Delete" + rhid.ToString());
            return ret;
        }

        public static RetailHistory GetRetailHistoryByID(int rhid)
        {
            RetailHistory retailhistory = null;
            string key = "RetailHistory_GetRetailHistoryByID_" + rhid.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                retailhistory = (RetailHistory)BizObject.Cache[key];
            }
            else
            {
                retailhistory = GetRetailHistoryFromRetailHistoryInfo(SiteProvider.PR2.GetRetailHistoryByID(rhid));
                BasePR.CacheData(key, retailhistory);
            }
            return retailhistory;
        }

        public static List<RetailHistory> GetRetailHistoryListfromRetailHistoryInfo(List<RetailHistoryInfo> recordset)
        {
            List<RetailHistory> retailhistoryList = new List<RetailHistory>();
            foreach (RetailHistoryInfo retailhistoryinfo in recordset)
            {
                RetailHistory retailhistory = GetRetailHistoryFromRetailHistoryInfo(retailhistoryinfo);

                retailhistoryList.Add(retailhistory);
            }
            return retailhistoryList;
        }


        private static RetailHistory GetRetailHistoryFromRetailHistoryInfo(RetailHistoryInfo retailhistoryinfo)
        {
            if (retailhistoryinfo == null)
                return null;

            RetailHistory retailhistory = new RetailHistory(retailhistoryinfo.Rhid, retailhistoryinfo.FacId, retailhistoryinfo.Paydate, retailhistoryinfo.Seat, retailhistoryinfo.PayAmount, retailhistoryinfo.Paystatus, retailhistoryinfo.MemberType, retailhistoryinfo.TransId, retailhistoryinfo.PayComment, retailhistoryinfo.CouponAmount, retailhistoryinfo.CouponId, retailhistoryinfo.CCLast);
            return retailhistory;
        }

        private static List<RetailHistory> GetRetailHistoryListFromRetailHistoryInfoList(List<RetailHistoryInfo> recordset)
        {
            List<RetailHistory> retailhistoryList = new List<RetailHistory>();
            foreach (RetailHistoryInfo record in recordset)
                retailhistoryList.Add(GetRetailHistoryFromRetailHistoryInfo(record));
            return retailhistoryList;
        }

        #endregion Methods

    }

}
