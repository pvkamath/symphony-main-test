﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for UserHistory
/// </summary>
namespace PearlsReview.BLL
{
    public class UserHistory : BasePR
    {

        #region Variables and Properties

        private int _Uh_ID = 0;
        public int Uh_ID
        {
            get { return _Uh_ID; }
            protected set { _Uh_ID = value; }
        }

        private int _User_ID = 0;
        public int User_ID
        {
            get { return _User_ID; }
            protected set { _User_ID = value; }
        }

        private DateTime _Action_Date =  System.DateTime.Now;
        public DateTime Action_Date
        {
            get { return _Action_Date; }
            protected set { _Action_Date = value; }
        }

        private string _Action_Type = "";
        public string Action_Type
        {
            get { return _Action_Type; }
            set { _Action_Type = value; }
        }

        private string _Action_Comment = "";
        public string Action_Comment
        {
            get { return _Action_Comment; }
            set { _Action_Comment = value; }
        }

          public UserHistory(int Uh_ID, int User_ID, DateTime Action_Date, string Action_Type, string Action_Comment)
        {
            this.Uh_ID = Uh_ID;
            this.User_ID = User_ID;
            this.Action_Date = Action_Date;
            this.Action_Type = Action_Type;
            this.Action_Comment = Action_Comment;
        }
              

      #endregion

       #region Methods
           /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all AdClicks
        /// </summary>
        public static List<UserHistory> GetUserHistories(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "action_date";

            List<UserHistory> UserHistories = null;
            string key = "UserHistory_UserHistory_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserHistories = (List<UserHistory>)BizObject.Cache[key];
            }
            else
            {
                List<UserHistoryInfo> recordset = SiteProvider.PR2.GetUserHistories(cSortExpression);
                UserHistories = GetUserHistoryListFromUserHistoryInfoList(recordset);
                BasePR.CacheData(key, UserHistories);
            }
            return UserHistories;
        }


         /// <summary>
        /// Returns a AdClick object with the specified ID
        /// </summary>
        public static UserHistory GetUserHistoryByID(int UserHistoryID)
        {
            UserHistory UserHistory = null;
            string key = "UserHistorys_UserHistory_" + UserHistoryID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserHistory = (UserHistory)BizObject.Cache[key];
            }
            else
            {
                UserHistory = GetUserHistoryFromUserHistoryInfo(SiteProvider.PR2.GetUserHistoryByID(UserHistoryID));
                BasePR.CacheData(key, UserHistory);
            }
            return UserHistory;
        }
        

        /// <summary>
        /// Returns a AdClick object with the specified ID
        /// </summary>
        public static DataSet GetUserHistoryByParam(DateTime StartDate, DateTime EndDate, string ActionType, string cSortExpression)
        {
            DataSet UserHistory = null;
            UserHistory = (SiteProvider.PR2.GetUserHistoryByParam( StartDate,  EndDate,  ActionType,  cSortExpression));
            return UserHistory;
        }

         /// <summary>
        /// Creates a new AdClick
        /// </summary>
        public static int InsertUserHistory(int User_ID, DateTime Action_Date, string Action_Type, string Action_Comment)
        {
            Action_Type = BizObject.ConvertNullToEmptyString(Action_Type);
            Action_Comment = BizObject.ConvertNullToEmptyString(Action_Comment);
            UserHistoryInfo record = new UserHistoryInfo(0, User_ID, Action_Date, Action_Type,Action_Comment);
            int ret = SiteProvider.PR2.InsertUserHistory(record);

            BizObject.PurgeCacheItems("UserHistories_UserHistory");
            return ret;
        }


         /// <summary>
        /// Returns a AdClick object filled with the data taken from the input AdClickInfo
        /// </summary>
        private static UserHistory GetUserHistoryFromUserHistoryInfo(UserHistoryInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new UserHistory(record.Uh_ID, record.User_ID, record.Action_Date, record.Action_Type, record.Action_Comment);
            }
        }

        /// <summary>
        /// Returns a list of AdClick objects filled with the data taken from the input list of AdClickInfo
        /// </summary>
        private static List<UserHistory> GetUserHistoryListFromUserHistoryInfoList(List<UserHistoryInfo> recordset)
        {
            List<UserHistory> UserHistories = new List<UserHistory>();
            foreach (UserHistoryInfo record in recordset)
                UserHistories.Add(GetUserHistoryFromUserHistoryInfo(record));
            return UserHistories;
        }


         #endregion
    }
}
