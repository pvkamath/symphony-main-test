﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for Topic
    /// </summary>
    [Serializable]
    public class Topic : BasePR
    {
        #region Variables and Properties

        private DateTime? _webinar_start = null;
        public DateTime? Webinar_start
        {
            get { return _webinar_start; }
            set { _webinar_start = value; }
        }

        private DateTime? _webinar_end = null;
        public DateTime? Webinar_end
        {
            get { return _webinar_end; }
            set { _webinar_end = value; }
        }



        private int _Sequence = 0;
        public int Sequence
        {
            get { return _Sequence; }
            protected set { _Sequence = value; }
        }
        public bool _CorLecture = false;
        public bool CorLecture
        {
            get { return _CorLecture; }
            set { _CorLecture = value; }
        }

        public string _Fla_CEType = "";
        public string Fla_CEType
        {
            get { return _Fla_CEType; }
            set { _Fla_CEType = value; }
        }

        public string _Fla_IDNo = "";
        public string Fla_IDNo
        {
            get { return _Fla_IDNo; }
            set { _Fla_IDNo = value; }
        }

        public string _FolderName = "";
        public string FolderName
        {
            get { return _FolderName; }
            set { _FolderName = value; }
        }

        public string _HTML = "";
        public string HTML
        {
            get { return _HTML; }
            set { _HTML = value; }
        }

        private DateTime? _releaseDate = null;
        public DateTime? ReleaseDate
        {
            get { return _releaseDate; }
            set { _releaseDate = value; }
        }

        private DateTime? _expireDate = null;
        public DateTime? ExpireDate
        {
            get { return _expireDate; }
            set { _expireDate = value; }
        }

        public string _LastUpdate = DateTime.Now.ToShortDateString();
        public string LastUpdate
        {
            get { return _LastUpdate; }
            set { _LastUpdate = value; }
        }

        public bool _NoSurvey = false;
        public bool NoSurvey
        {
            get { return _NoSurvey; }
            set { _NoSurvey = value; }
        }

        public int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        public string _TopicName = "";
        public string TopicName
        {
            get { return _TopicName; }
            set { _TopicName = value; }
        }

        public int _SurveyID = 0;
        public int SurveyID
        {
            get { return _SurveyID; }
            set { _SurveyID = value; }
        }

        public bool _Compliance = false;
        public bool Compliance
        {
            get { return _Compliance; }
            set { _Compliance = value; }
        }

        public string _MetaKW = "";
        public string MetaKW
        {
            get { return _MetaKW; }
            set { _MetaKW = value; }
        }

        public string _MetaDesc = "";
        public string MetaDesc
        {
            get { return _MetaDesc; }
            set { _MetaDesc = value; }
        }

        public decimal _Hours = 0.00M;
        public decimal Hours
        {
            get { return _Hours; }
            set { _Hours = value; }
        }

        public string _MediaType = "";
        public string MediaType
        {
            get { return _MediaType; }
            set { _MediaType = value; }
        }

        public string _Objectives = "";
        public string Objectives
        {
            get { return _Objectives; }
            set { _Objectives = value; }
        }

        public string _Content = "";
        public string Content
        {
            get { return _Content; }
            set { _Content = value; }
        }

        public bool _Textbook = false;
        public bool Textbook
        {
            get { return _Textbook; }
            set { _Textbook = value; }
        }

        public int _CertID = 0;
        public int CertID
        {
            get { return _CertID; }
            set { _CertID = value; }
        }

        public string _Method = "";
        public string Method
        {
            get { return _Method; }
            set { _Method = value; }
        }

        public string _GrantBy = "";
        public string GrantBy
        {
            get { return _GrantBy; }
            set { _GrantBy = value; }
        }

        public string _DOCXFile = "";
        public string DOCXFile
        {
            get { return _DOCXFile; }
            set { _DOCXFile = value; }
        }
        public string _DOCXHoFile = "";
        public string DOCXHoFile
        {
            get { return _DOCXHoFile; }
            set { _DOCXHoFile = value; }
        }

        public bool _Obsolete = false;
        public bool Obsolete
        {
            get { return _Obsolete; }
            set { _Obsolete = value; }
        }

        public int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }

        public string _Course_Number = "";
        public string Course_Number
        {
            get { return _Course_Number; }
            set { _Course_Number = value; }
        }

        //private string _CeRef = "";
        //public string CeRef
        //{
        //    get { return _CeRef; }
        //    set { _CeRef = value; }
        //}

        //private DateTime _Release_Date = System.DateTime.MinValue;
        //public DateTime Release_Date
        //{
        //    get { return _Release_Date; }
        //    set { _Release_Date = value; }
        //}



        public int _Minutes = 0;
        public int Minutes
        {
            get { return _Minutes; }
            set { _Minutes = value; }
        }

        public bool _Audio_Ind = false;
        public bool Audio_Ind
        {
            get { return _Audio_Ind; }
            set { _Audio_Ind = value; }
        }

        public bool _Apn_Ind = false;
        public bool Apn_Ind
        {
            get { return _Apn_Ind; }
            set { _Apn_Ind = value; }
        }

        public bool _Icn_Ind = false;
        public bool Icn_Ind
        {
            get { return _Icn_Ind; }
            set { _Icn_Ind = value; }
        }

        public bool _Jcaho_Ind = false;
        public bool Jcaho_Ind
        {
            get { return _Jcaho_Ind; }
            set { _Jcaho_Ind = value; }
        }

        public bool _Magnet_Ind = false;
        public bool Magnet_Ind
        {
            get { return _Magnet_Ind; }
            set { _Magnet_Ind = value; }
        }

        public bool _Active_Ind = false;
        public bool Active_Ind
        {
            get { return _Active_Ind; }
            set { _Active_Ind = value; }
        }

        public bool _Video_Ind = false;
        public bool Video_Ind
        {
            get { return _Video_Ind; }
            set { _Video_Ind = value; }
        }

        public bool _Online_Ind = false;
        public bool Online_Ind
        {
            get { return _Online_Ind; }
            set { _Online_Ind = value; }
        }

        public bool _Ebp_Ind = false;
        public bool Ebp_Ind
        {
            get { return _Ebp_Ind; }
            set { _Ebp_Ind = value; }
        }

        public bool _Ccm_Ind = false;
        public bool Ccm_Ind
        {
            get { return _Ccm_Ind; }
            set { _Ccm_Ind = value; }
        }

        public bool _Avail_Ind = false;
        public bool Avail_Ind
        {
            get { return _Avail_Ind; }
            set { _Avail_Ind = value; }
        }

        public bool _notest = false;
        public bool notest
        {
            get { return _notest; }
            set { _notest = value; }
        }

        public bool _uce_Ind = false;
        public bool uce_Ind
        {
            get { return _uce_Ind; }
            set { _uce_Ind = value; }
        }


        public string _Cert_Cerp = "";
        public string Cert_Cerp
        {
            get { return _Cert_Cerp; }
            set { _Cert_Cerp = value; }
        }

        public string _Ref_Html = "";
        public string Ref_Html
        {
            get { return _Ref_Html; }
            set { _Ref_Html = value; }
        }

        public int _AlterID = 0;
        public int AlterID
        {
            get { return _AlterID; }
            set { _AlterID = value; }
        }

        public int _Pass_Score = 0;
        public int Pass_Score
        {
            get { return _Pass_Score; }
            set { _Pass_Score = value; }
        }

        public string _Subtitle = "";
        public string Subtitle
        {
            get { return _Subtitle; }
            set { _Subtitle = value; }
        }

        //private decimal _Cost = 0.00M;
        //public decimal Cost
        //{
        //    get { return _Cost; }
        //    set { _Cost = value; }
        //}

        public string _Topic_Type = "";
        public string Topic_Type
        {
            get { return _Topic_Type; }
            set { _Topic_Type = value; }
        }

        public string _Img_Name = "";
        public string Img_Name
        {
            get { return _Img_Name; }
            set { _Img_Name = value; }
        }

        public string _Img_Credit = "";
        public string Img_Credit
        {
            get { return _Img_Credit; }
            set { _Img_Credit = value; }
        }

        public string _Img_Caption = "";
        public string Img_Caption
        {
            get { return _Img_Caption; }
            set { _Img_Caption = value; }
        }

        public string _Accreditation = "";
        public string Accreditation
        {
            get { return _Accreditation; }
            set { _Accreditation = value; }
        }

        public int _Views = 0;
        public int Views
        {
            get { return _Views; }
            set { _Views = value; }
        }

        public int _PrimaryViews = 0;
        public int PrimaryViews
        {
            get { return _PrimaryViews; }
            set { _PrimaryViews = value; }
        }

        public int _PageReads = 0;
        public int PageReads
        {
            get { return _PageReads; }
            set { _PageReads = value; }
        }

        public bool _Featured = false;
        public bool Featured
        {
            get { return _Featured; }
            set { _Featured = value; }
        }
        public bool _Deleted_Ind = false;
        public bool Deleted_Ind
        {
            get { return _Deleted_Ind; }
            set { _Deleted_Ind = value; }
        }
        public decimal _online_Cost = 0.00M;
        public decimal online_Cost
        {
            get { return _online_Cost; }
            set { _online_Cost = value; }
        }
        public decimal _offline_Cost = 0.00M;
        public decimal offline_Cost
        {
            get { return _offline_Cost; }
            set { _offline_Cost = value; }
        }
        public string _rev = "";
        public string rev
        {
            get { return _rev; }
            set { _rev = value; }
        }
        public string _urlmark = "";
        public string urlmark
        {
            get { return _urlmark; }
            set { _urlmark = value; }
        }

        private string _shortname = "";
        public string shortname
        {
            get { return _shortname; }
            set { _shortname = value; }
        }

        private int _categoryid = 0;
        public int categoryid
        {
            get { return _categoryid; }
            set { _categoryid = value; }
        }

        private bool _hold_ind = false;
        public bool hold_ind
        {
            get { return _hold_ind; }
            set { _hold_ind = value; }
        }
        private decimal _rating_avg = (decimal)0.0;
        public decimal rating_avg
        {
            get { return _rating_avg; }
            set { _rating_avg = value; }
        }

        private int _rating_num_comment = 0;
        public int rating_num_comment
        {
            get { return _rating_num_comment; }
            set { _rating_num_comment = value; }
        }
        private int _rating_total = 0;
        public int rating_total
        {
            get { return _rating_total; }
            set { _rating_total = value; }
        }
        private bool _prepaid_ind;
        public bool prepaid_ind
        {
            get { return _prepaid_ind; }
            set { _prepaid_ind = value; }
        }
        private string _topic_summary = "";
        public string topic_summary
        {
            get { return _topic_summary; }
            set { _topic_summary = value; }
        }

        private bool _CMEInd = false;
        public bool CMEInd
        {
            get { return _CMEInd; }
            set { _CMEInd = value; }
        }

        private bool _CMESponsorInd = false;
        public bool CMESponsorInd
        {
            get { return _CMESponsorInd; }
            set { _CMESponsorInd = value; }
        }

        private string _CMESponsorName = "";
        public string CMESponsorName
        {
            get { return _CMESponsorName; }
            set { _CMESponsorName = value; }
        }
        private string _topic_hourlong = "";
        public string Topic_hourlong
        {
            get { return _topic_hourlong; }
            set { _topic_hourlong = value; }
        }
        private string _topic_hourshort = "";
        public string Topic_hourshort
        {
            get { return _topic_hourshort; }
            set { _topic_hourshort = value; }
        }

        public Topic() { }
        public Topic(bool CorLecture, string Fla_CEType, string Fla_IDNo, string FolderName, string HTML,
            DateTime? releaseDate, DateTime? expireDate, string LastUpdate, bool NoSurvey, int TopicID, string TopicName,
            int SurveyID, bool Compliance, string MetaKW, string MetaDesc, decimal Hours, string MediaType,
            string Objectives, string Content, bool Textbook, int CertID, string Method, string GrantBy,
            string DOCXFile, string DOCXHoFile, bool Obsolete, int FacilityID, string Course_Number,//string CeRef,//DateTime Release_Date, 
            int Minutes, bool Audio_Ind, bool Apn_Ind, bool Icn_Ind, bool Jcaho_Ind, bool Magnet_Ind, bool Active_Ind, bool Video_Ind,
            bool Online_Ind, bool Ebp_Ind, bool Ccm_Ind, bool Avail_Ind, string Cert_Cerp, string Ref_Html, int AlterID, int Pass_Score, string Subtitle,//decimal Cost,
            string Topic_Type, string Img_Name, string Img_Credit, string Img_Caption, string Accreditation,
            int Views, int PrimaryViews, int PageReads, bool Featured, string rev, string urlmark,
            decimal offline_Cost, decimal online_Cost, bool notest, bool uce_Ind, string shortname,
            int categoryid, bool hold_ind, decimal rating_avg, int rating_num_comment, int rating_total,
            bool prepaid_ind, string topic_summary, bool cmeInd, bool cmeSponsorInd, string cmeSponsorName, string topic_hourlong, string topic_hourshort, DateTime? webinar_start, DateTime? webinar_end)
        {
            this.CorLecture = CorLecture;
            this.Fla_CEType = Fla_CEType;
            this.Fla_IDNo = Fla_IDNo;
            this.FolderName = FolderName;
            this.HTML = HTML;
            this.ReleaseDate = releaseDate;
            this.ExpireDate = expireDate;
            this.LastUpdate = LastUpdate;
            this.NoSurvey = NoSurvey;
            this.TopicID = TopicID;
            this.TopicName = TopicName;
            this.SurveyID = SurveyID;
            this.Compliance = Compliance;
            this.MetaKW = MetaKW;
            this.MetaDesc = MetaDesc;
            this.Hours = Hours;
            this.MediaType = MediaType;
            this.Objectives = Objectives;
            this.Content = Content;
            this.Textbook = Textbook;
            this.CertID = CertID;
            this.Method = Method;
            this.GrantBy = GrantBy;
            this.DOCXFile = DOCXFile;
            this.DOCXHoFile = DOCXHoFile;
            this.Obsolete = Obsolete;
            this.FacilityID = FacilityID;
            this.Course_Number = Course_Number;
            //this.CeRef = CeRef;
            //this.Release_Date = Release_Date;
            this.Minutes = Minutes;
            this.Audio_Ind = Audio_Ind;
            this.Apn_Ind = Apn_Ind;
            this.Icn_Ind = Icn_Ind;
            this.Jcaho_Ind = Jcaho_Ind;
            this.Magnet_Ind = Magnet_Ind;
            this.Active_Ind = Active_Ind;
            this.Video_Ind = Video_Ind;
            this.Online_Ind = Online_Ind;
            this.Ebp_Ind = Ebp_Ind;
            this.Ccm_Ind = Ccm_Ind;
            this.Avail_Ind = Avail_Ind;
            this.Cert_Cerp = Cert_Cerp;
            this.Ref_Html = Ref_Html;
            this.AlterID = AlterID;
            this.Pass_Score = Pass_Score;
            this.Subtitle = Subtitle;
            //this.Cost = Cost;
            this.Topic_Type = Topic_Type;
            this.Img_Name = Img_Name;
            this.Img_Credit = Img_Credit;
            this.Img_Caption = Img_Caption;
            this.Accreditation = Accreditation;
            this.Views = Views;
            this.PrimaryViews = PrimaryViews;
            this.PageReads = PageReads;
            this.Featured = Featured;
            this.rev = rev;
            this.urlmark = urlmark;
            this.notest = notest;
            this.online_Cost = online_Cost;
            this.offline_Cost = offline_Cost;
            this.uce_Ind = uce_Ind;
            this.shortname = shortname;
            this.categoryid = categoryid;
            this.hold_ind = hold_ind;
            this.rating_avg = rating_avg;
            this.rating_num_comment = rating_num_comment;
            this.rating_total = rating_total;
            this.prepaid_ind = prepaid_ind;
            this.topic_summary = topic_summary;
            this.CMEInd = cmeInd;
            this.CMESponsorInd = cmeSponsorInd;
            this.CMESponsorName = cmeSponsorName;
            this.Topic_hourlong = topic_hourlong;
            this.Topic_hourshort = topic_hourshort;
            this.Webinar_start = webinar_start;
            this.Webinar_end = webinar_end;
        }

        public bool Delete()
        {
            bool success = Topic.DeleteTopic(this.TopicID);
            if (success)
                this.TopicID = 0;
            return success;
        }

        public bool Update()
        {
            return Topic.UpdateTopic(this.CorLecture, this.Fla_CEType, this.Fla_IDNo, this.FolderName,
                this.HTML, this.ReleaseDate, this.ExpireDate, this.LastUpdate, this.NoSurvey, this.TopicID, this.TopicName,
                this.SurveyID, this.Compliance, this.MetaKW, this.MetaDesc, this.Hours, this.MediaType,
                this.Objectives, this.Content, this.Textbook, this.CertID, this.Method, this.GrantBy, this.DOCXFile,
                this.DOCXHoFile, this.Obsolete, this.FacilityID, this.Course_Number,// this.CeRef,//this.Release_Date, 
                this.Minutes, this.Audio_Ind, this.Apn_Ind, this.Icn_Ind, this.Jcaho_Ind, this.Magnet_Ind, this.Active_Ind, this.Video_Ind,
                this.Online_Ind, this.Ebp_Ind, this.Ccm_Ind, this.Avail_Ind, this.Cert_Cerp, this.Ref_Html, this.AlterID, this.Pass_Score, this.Subtitle, //this.Cost,
                this.Topic_Type, this.Img_Name, this.Img_Credit, this.Img_Caption, this.Accreditation, this.Views, this.PrimaryViews,
                this.PageReads, this.Featured, this.rev, this.urlmark, this.offline_Cost, this.online_Cost, this.notest, this.uce_Ind,
                this.shortname, this.categoryid, this.hold_ind, this.prepaid_ind, this.topic_summary,
                this.CMEInd, this.CMESponsorInd, this.CMESponsorName, this.Webinar_start, this.Webinar_end);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Topics, except obsolete ones
        /// </summary>
        public static List<Topic> GetTopics(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_Topics_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetTopics(cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        public static List<Topic> GetTopicsFromLookupDtos(List<LookupDTO> dtos)
        {
            List<TopicInfo> topicinfos = SiteProvider.PR2.GetTopicInfosFromLookupDtos(dtos);

            List<Topic> topics = GetTopicListFromTopicInfoList(topicinfos);

            return topics;

        }

        public static List<Topic> GetUnAssignedTopicsByResID(int ResourceID)
        {
            List<Topic> Topics = null;
            string key = "Topics_Unlim_Topics_" + ResourceID;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetUnAssignedTopicsByResID(ResourceID);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }
        public static List<Topic> GetTopicsByMicrositeDomain(int DomainID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_Unlim_Topics_" + DomainID;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetTopicsByMicrositeDomain(DomainID, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Unlimited Course Categorys
        /// </summary>
        public static List<Topic> GetRetailCourseByCategoryID(int CategoryId)
        {


            List<Topic> Topics = null;
            string key = "Topics_Topics_" + CategoryId;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetRetailCourseByCategoryID(CategoryId);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }       

        /// <summary>
        /// Returns a collection with all Unlimited Course Categorys
        /// </summary>
        public static List<Topic> GetRetailUnlimCourseByCategoryID(int CategoryId)
        {


            List<Topic> Topics = null;
            string key = "Topics_Unlim_Topics_" + CategoryId;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetRetailUnlimCourseByCategoryID(CategoryId);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Featured Topics (Featured = true), except obsolete ones
        /// </summary>
        public static List<Topic> GetFeaturedTopics(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_FeaturedTopics_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetFeaturedTopics(cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Audio Topics , except obsolete ones
        /// </summary>
        public static List<Topic> GetAudioTopics(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_AudioTopics_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetAudioTopics(cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Audio Topics by Categoryid, except obsolete ones
        /// </summary>
        public static List<Topic> GetAudioTopicsByCategoryId(int CategoryId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_AudioTopics_" + CategoryId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetAudioTopicsByCategoryId(CategoryId, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Audio Topics by Categoryid, except obsolete ones
        /// </summary>
        public static List<Topic> GetTopicsByCategoryIdAndMediaType(int CategoryId, string MediaType, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_AudioTopics_" + CategoryId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetTopicsByCategoryIdAndMediaType(CategoryId, MediaType, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        public static bool withFollowUpSurvey(int topicid)
        {
            return SiteProvider.PR2.withFollowUpSurvey(topicid);
        }

        /// <summary>
        /// Returns a collection with all Featured Topics (Featured = true), except obsolete ones by facility ID
        /// </summary>
        public static List<Topic> GetFeaturedTopicsByFacilityId(int facilityid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_FeaturedTopics_" + facilityid + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetFeaturedTopicsByFacilityId(facilityid, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Featured Topics (Featured = true), except obsolete ones by facility ID
        /// </summary>
        public static List<Topic> GetFeaturedTopicsByTabname(string Tabname, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_FeaturedTopics_" + Tabname + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetFeaturedTopicsByTabname(Tabname, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Featured Topics (Featured = true), except obsolete ones by facility ID
        /// </summary>
        public static List<Topic> GetFeaturedTopicsByAreaname(string Areaname, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_FeaturedTopics_" + Areaname + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetFeaturedTopicsByAreaname(Areaname, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        /// <summary>
        /// Returns all chapters  (Bhaskar N)
        /// </summary>
        public static List<Topic> GetAllChaptersByTopicId(int topicid)
        {
            List<Topic> Topics = null;
            string key = "Topics_Chapters_" + topicid;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetAllChaptersByTopicId(topicid);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Textbook Topics (Textbook = true), except obsolete ones
        /// </summary>
        public static List<Topic> GetTextbookTopics(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_TextbookTopics_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetTextbookTopics(cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Textbook Topics (Textbook = true), except obsolete ones
        /// </summary>
        public static List<Topic> GetNonTextbookTopics(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_NonTextbookTopics_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetNonTextbookTopics(cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Textbook Topics (Textbook = true), except obsolete ones by FacilityId
        /// </summary>
        public static List<Topic> GetNonTextbookTopicsByFacilityId(int facilityid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_NonTextbookTopics_" + facilityid + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetNonTextbookTopicsByFacilityId(facilityid, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Textbook Topics (Textbook = true), except obsolete ones by FacilityId
        /// </summary>
        public static List<Topic> GetAnthologyTopicsByTopicId(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            //if (cSortExpression.Length == 0)
            //    cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_AnthologyTopics_" + TopicID + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetAnthologyTopicsByTopicId(TopicID, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Obsolete Topics 
        /// </summary>
        public static List<Topic> GetObsoleteTopics(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_ObsoleteTopics_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetObsoleteTopics(cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Obsolete Textbook Topics (Textbook = true)
        /// </summary>
        public static List<Topic> GetObsoleteTextbookTopics(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_ObsoleteTextbookTopics_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetObsoleteTextbookTopics(cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }




        /// <summary>
        /// Returns a collection with all Textbook Topics (Textbook = true)
        /// </summary>
        public static List<Topic> GetTopicsByType(string CourseNumber, string Title, string category, int Areaid, string active, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_TopicsByType_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetTopicsByType(CourseNumber, Title, category, Areaid, active, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Obsolete NonTextbook Topics (Textbook = false)
        /// </summary>
        public static List<Topic> GetObsoleteNonTextbookTopics(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_ObsoleteNonTextbookTopics_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetObsoleteNonTextbookTopics(cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Compliance Topics (compliance = true), except obsolete ones
        /// NOTE: Because this is used by facility admins to customize compliance topics, we will
        /// eliminate from the list any that are not assigned to a category, since they are not
        /// available to the public
        /// </summary>
        public static List<Topic> GetComplianceTopics(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_ComplianceTopics_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetComplianceTopics(cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Topics (textbook and non-textbook), plus info about those assigned to a CategoryID
        /// , except obsolete ones
        /// </summary>
        public static List<CheckBoxListTopicInfo> GetAllTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<CheckBoxListTopicInfo> CheckBoxListTopics = null;
            string key = "Topics_CheckBoxListTopics_" + CategoryID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CheckBoxListTopics = (List<CheckBoxListTopicInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CheckBoxListTopicInfo> recordset = SiteProvider.PR2.GetAllTopicsPlusCategoryAssignments(CategoryID, cSortExpression);
                CheckBoxListTopics = GetCheckBoxListTopicListFromCheckBoxListTopicInfoList(recordset);
                BasePR.CacheData(key, CheckBoxListTopics);
            }
            return CheckBoxListTopics;

        }

        /// <summary>
        /// Returns a collection with all Topics, plus info about those assigned to a CategoryID, except obsolete ones
        /// </summary>
        public static List<CheckBoxListTopicInfo> GetAllNonTextbookTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<CheckBoxListTopicInfo> CheckBoxListTopics = null;
            string key = "Topics_NonTextbook_CheckBoxListTopics_" + CategoryID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CheckBoxListTopics = (List<CheckBoxListTopicInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CheckBoxListTopicInfo> recordset = SiteProvider.PR2.GetAllNonTextbookTopicsPlusCategoryAssignments(CategoryID, cSortExpression);
                CheckBoxListTopics = GetCheckBoxListTopicListFromCheckBoxListTopicInfoList(recordset);
                BasePR.CacheData(key, CheckBoxListTopics);
            }
            return CheckBoxListTopics;

        }

        /// <summary>
        /// Returns a collection with all Textbook Topics, plus info about those assigned to a CategoryID, except obsolete ones
        /// </summary>
        public static List<CheckBoxListTopicInfo> GetAllTextbookTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<CheckBoxListTopicInfo> CheckBoxListTopics = null;
            string key = "Topics_Textbook_CheckBoxListTopics_" + CategoryID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CheckBoxListTopics = (List<CheckBoxListTopicInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CheckBoxListTopicInfo> recordset = SiteProvider.PR2.GetAllTextbookTopicsPlusCategoryAssignments(CategoryID, cSortExpression);
                CheckBoxListTopics = GetCheckBoxListTopicListFromCheckBoxListTopicInfoList(recordset);
                BasePR.CacheData(key, CheckBoxListTopics);
            }
            return CheckBoxListTopics;

        }

        /// <summary>
        /// Returns the number of total Topics, except obsolete ones
        /// </summary>
        public static int GetTopicCount()
        {
            int TopicCount = 0;
            string key = "Topics_TopicCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicCount = SiteProvider.PR2.GetTopicCount();
                BasePR.CacheData(key, TopicCount);
            }
            return TopicCount;
        }

        /// <summary>
        /// Returns the number of total Topics, except obsolete ones
        /// </summary>
        public static int GetTextbookTopicCount()
        {
            int TopicCount = 0;
            string key = "Topics_TextbookTopicCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicCount = SiteProvider.PR2.GetTextbookTopicCount();
                BasePR.CacheData(key, TopicCount);
            }
            return TopicCount;
        }

        /// <summary>
        /// Returns the number of total Topics, except obsolete ones
        /// </summary>
        public static int GetNonTextbookTopicCount()
        {
            int TopicCount = 0;
            string key = "Topics_NonTextbookTopicCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicCount = SiteProvider.PR2.GetNonTextbookTopicCount();
                BasePR.CacheData(key, TopicCount);
            }
            return TopicCount;
        }

        /// <summary>
        /// Returns the number of obsolete topics
        /// </summary>
        public static int GetObsoleteTopicCount()
        {
            int TopicCount = 0;
            string key = "Topics_ObsoleteTopicCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicCount = SiteProvider.PR2.GetObsoleteTopicCount();
                BasePR.CacheData(key, TopicCount);
            }
            return TopicCount;
        }

        /// <summary>
        /// Returns the number of obsolete textbook topics
        /// </summary>
        public static int GetObsoleteTextbookTopicCount()
        {
            int TopicCount = 0;
            string key = "Topics_ObsoleteTextbookTopicCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicCount = SiteProvider.PR2.GetObsoleteTextbookTopicCount();
                BasePR.CacheData(key, TopicCount);
            }
            return TopicCount;
        }

        /// <summary>
        /// Returns the number of obsolete Non-textbook topics
        /// </summary>
        public static int GetObsoleteNonTextbookTopicCount()
        {
            int TopicCount = 0;
            string key = "Topics_ObsoleteNonTextbookTopicCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicCount = SiteProvider.PR2.GetObsoleteNonTextbookTopicCount();
                BasePR.CacheData(key, TopicCount);
            }
            return TopicCount;
        }

        /// <summary>
        /// Returns the Areatype By TopicId
        /// </summary>
        public static int GetTopicAreatypeByTopicId(int TopicID)
        {
            int AreaId = 0;
            string key = "Topics_GetTopicAreatypeByTopicId";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AreaId = (int)BizObject.Cache[key];
            }
            else
            {
                AreaId = SiteProvider.PR2.GetTopicAreatypeByTopicId(TopicID);
                BasePR.CacheData(key, AreaId);
            }
            return AreaId;
        }

        /// <summary>
        /// Returns a collection of nurse or pr topics publish on current day
        /// </summary>
        public static DataSet GetNewPublishedTopics()
        {
            return SiteProvider.PR2.GetNewPublishedTopics();
        }
        /// <summary>
        /// Returns a Topic object with the specified ID
        /// </summary>
        public static Topic GetTopicByID(int TopicID)
        {
            Topic Topic = null;
            string key = "Topics_Topic_" + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topic = (Topic)BizObject.Cache[key];
            }
            else
            {
                Topic = GetTopicFromTopicInfo(SiteProvider.PR2.GetTopicByID(TopicID));
                BasePR.CacheData(key, Topic);
            }
            return Topic;
        }

        /// <summary>
        /// Returns a Topic object with the specified Course_Number
        /// Bsk
        /// </summary>
        public static Topic GetTopicByCourseNumber(string Course_Number)
        {
            Topic Topic = null;
            string key = "Topics_Topic_" + Course_Number.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topic = (Topic)BizObject.Cache[key];
            }
            else
            {
                Topic = GetTopicFromTopicInfo(SiteProvider.PR2.GetTopicByCourseNumber(Course_Number));
                BasePR.CacheData(key, Topic);
            }
            return Topic;
        }

        /// <summary>
        /// Returns a Topic object with the specified Course_Number and DomainId
        /// </summary>
        public static Topic GetTopicByCourseNumberAndDomainId(string Course_Number, int DomainId)
        {
            Topic Topic = null;
            string key = "Topics_Topic_" + Course_Number.ToString() + DomainId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topic = (Topic)BizObject.Cache[key];
            }
            else
            {
                Topic = GetTopicFromTopicInfo(SiteProvider.PR2.GetTopicByCourseNumberAndDomainId(Course_Number, DomainId));
                BasePR.CacheData(key, Topic);
            }
            return Topic;


        }

        /// <summary>
        /// Returns a Topic object with the specified Course_Number
        /// Bsk
        /// </summary>
        public static Topic GetTopicByUserName(string UserName)
        {
            Topic Topic = null;
            string key = "Topics_Topic_" + UserName.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topic = (Topic)BizObject.Cache[key];
            }
            else
            {
                Topic = GetTopicFromTopicInfo(SiteProvider.PR2.GetTopicByUserName(UserName));
                BasePR.CacheData(key, Topic);
            }
            return Topic;
        }

        /// <summary>
        /// Updates an existing Topic
        /// </summary>
        public static bool UpdateTopic(bool CorLecture, string Fla_CEType, string Fla_IDNo, string FolderName,
            string HTML, DateTime? releaseDate, DateTime? expireDate, string LastUpdate, bool NoSurvey, int TopicID, string TopicName,
            int SurveyID, bool Compliance, string MetaKW, string MetaDesc, decimal Hours,
            string MediaType, string Objectives, string Content, bool Textbook, int CertID, string Method, string GrantBy,
            string DOCXFile, string DOCXHoFile, bool Obsolete, int FacilityID, string Course_Number, // string CeRef,//DateTime Release_Date, 
            int Minutes, bool Audio_Ind, bool Apn_Ind, bool Icn_Ind, bool Jcaho_Ind, bool Magnet_Ind, bool Active_Ind, bool Video_Ind,
            bool Online_Ind, bool Ebp_Ind, bool Ccm_Ind, bool Avail_Ind, string Cert_Cerp, string Ref_Html,
            int AlterID, int Pass_Score, string Subtitle,//decimal Cost,
            string Topic_Type, string Img_Name, string Img_Credit, string Img_Caption, string Accreditation, int Views, int PrimaryViews,
            int PageReads, bool Featured, string rev, string urlmark, decimal offline_Cost, decimal online_Cost, bool notest, bool uce_Ind,
            string shortname, int categoryid, bool hold_ind, bool prepaid_ind, string topic_summary,
            bool cmeInd, bool cmeSponsorInd, string cmeSponsorName, DateTime? webinar_start, DateTime? webinar_end)
        {
            Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
            Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
            FolderName = BizObject.ConvertNullToEmptyString(FolderName);
            HTML = BizObject.ConvertNullToEmptyString(HTML);
            LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
            TopicName = BizObject.ConvertNullToEmptyString(TopicName);
            MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
            MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
            MediaType = BizObject.ConvertNullToEmptyString(MediaType);
            Objectives = BizObject.ConvertNullToEmptyString(Objectives);
            Content = BizObject.ConvertNullToEmptyString(Content);
            Method = BizObject.ConvertNullToEmptyString(Method);
            GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
            DOCXFile = BizObject.ConvertNullToEmptyString(DOCXFile);
            DOCXHoFile = BizObject.ConvertNullToEmptyString(DOCXHoFile);
            Course_Number = BizObject.ConvertNullToEmptyString(Course_Number.Trim());
            //CeRef = BizObject.ConvertNullToEmptyString(CeRef);

            //Release_Date = BizObject.ConvertNullToEmptyString(Release_Date);
            //Minutes = BizObject.ConvertNullToEmptyString(Minutes);
            //Audio_Ind = BizObject.ConvertNullToEmptyString(Audio_Ind);
            //Apn_Ind = BizObject.ConvertNullToEmptyString(Apn_Ind);
            //Icn_Ind = BizObject.ConvertNullToEmptyString(Icn_Ind);
            //Jcaho_Ind = BizObject.ConvertNullToEmptyString(Jcaho_Ind);
            //Magnet_Ind = BizObject.ConvertNullToEmptyString(Magnet_Ind);
            //Active_Ind = BizObject.ConvertNullToEmptyString(Active_Ind);
            //Video_Ind = BizObject.ConvertNullToEmptyString(Video_Ind);
            //Online_Ind = BizObject.ConvertNullToEmptyString(Online_Ind);
            //Course_Number = BizObject.ConvertNullToEmptyString(Course_Number);
            //Ebp_Ind = BizObject.ConvertNullToEmptyString(Ebp_Ind);


            //Ccm_Ind = BizObject.ConvertNullToEmptyString(Ccm_Ind);
            //Avail_Ind = BizObject.ConvertNullToEmptyString(Avail_Ind);
            Cert_Cerp = BizObject.ConvertNullToEmptyString(Cert_Cerp);
            Ref_Html = BizObject.ConvertNullToEmptyString(Ref_Html);
            //AlterID = BizObject.ConvertNullToEmptyString(AlterID);
            //Pass_Score = BizObject.ConvertNullToEmptyString(Pass_Score);
            Subtitle = BizObject.ConvertNullToEmptyString(Subtitle);

            //Cost = BizObject.ConvertNullToEmptyString(Cost);
            //Topic_Type = BizObject.ConvertNullToEmptyString(Topic_Type);
            Img_Name = BizObject.ConvertNullToEmptyString(Img_Name);
            Img_Credit = BizObject.ConvertNullToEmptyString(Img_Credit);
            Img_Caption = BizObject.ConvertNullToEmptyString(Img_Caption);
            Accreditation = BizObject.ConvertNullToEmptyString(Accreditation);
            rev = BizObject.ConvertNullToEmptyString(rev);
            urlmark = BizObject.ConvertNullToEmptyString(urlmark);
            topic_summary = BizObject.ConvertNullToEmptyString(topic_summary);
            shortname = BizObject.ConvertNullToEmptyString(shortname);
            cmeSponsorName = BizObject.ConvertNullToEmptyString(cmeSponsorName);

            if (shortname.Length == 0)
            {
                if (TopicName.Length > 30)
                {
                    shortname = TopicName.Substring(0, 29);
                }
                else
                {
                    shortname = TopicName;
                }
            }



            //if (FacilityID == -2)
            //{
            //    Textbook = true;
            //}
            //else
            //{
            //    Textbook = false;
            //}

            Obsolete = !(Avail_Ind);

            //Obsolete = !(Active_Ind);

            TopicInfo record = new TopicInfo(CorLecture, Fla_CEType, Fla_IDNo, FolderName, HTML, releaseDate, expireDate, LastUpdate,
                NoSurvey, TopicID, TopicName, SurveyID, Compliance, MetaKW, MetaDesc, Hours,
                MediaType, Objectives, Content, Textbook, CertID, Method, GrantBy, DOCXFile, DOCXHoFile,
                Obsolete, FacilityID, Course_Number,//CeRef,Release_Date,
                Minutes, Audio_Ind, Apn_Ind, Icn_Ind, Jcaho_Ind, Magnet_Ind, Active_Ind, Video_Ind,
                Online_Ind, Ebp_Ind, Ccm_Ind, Avail_Ind, Cert_Cerp, Ref_Html, AlterID, Pass_Score, Subtitle, //Cost,
                Topic_Type, Img_Name, Img_Credit, Img_Caption, Accreditation, Views, PrimaryViews, PageReads, Featured,
                rev, urlmark, offline_Cost, online_Cost, notest, uce_Ind, shortname, categoryid, hold_ind, 0, 0, 0,
                prepaid_ind, topic_summary, cmeInd, cmeSponsorInd, cmeSponsorName, "", "", webinar_start, webinar_end);
            bool ret = SiteProvider.PR2.UpdateTopic(record);

            BizObject.PurgeCacheItems("Topics_Topic_" + TopicID.ToString());
            BizObject.PurgeCacheItems("Topics_Topics");
            return ret;
        }

        /// <summary>
        /// Creates a new Topic
        /// </summary>
        public static int InsertTopic(bool CorLecture, string Fla_CEType, string Fla_IDNo, string FolderName,
            string HTML, DateTime? releaseDate, DateTime? expireDate, string LastUpdate, bool NoSurvey, string TopicName,
            int SurveyID, bool Compliance, string MetaKW, string MetaDesc, decimal Hours, string MediaType,
            string Objectives, string Content, bool Textbook, int CertID, string Method, string GrantBy,
            string DOCXFile, string DOCXHoFile, bool Obsolete, int FacilityID, string Course_Number, string CeRef,
            DateTime Release_Date, int Minutes, bool Audio_Ind, bool Apn_Ind, bool Icn_Ind, bool Jcaho_Ind,
            bool Magnet_Ind, bool Active_Ind, bool Video_Ind, bool Online_Ind, bool Ebp_Ind, bool Ccm_Ind, bool Avail_Ind,
            string Cert_Cerp, string Ref_Html, int AlterID, int Pass_Score, string Subtitle, decimal Cost, string Topic_Type,
            string Img_Name, string Img_Credit, string Img_Caption, string Accreditation, int Views, int PrimaryViews,
            int PageReads, bool Featured, string rev, string urlmark, decimal offline_Cost, decimal online_Cost,
            bool notest, bool uce_Ind, string shortname, int categoryid, bool hold_ind, bool prepaid_ind,
            string topic_summary, bool cmeInd, bool cmeSponsorInd, string cmeSponsorName, DateTime? webinar_start, DateTime? webinar_end)
        {
            Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
            Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
            FolderName = BizObject.ConvertNullToEmptyString(FolderName);
            HTML = BizObject.ConvertNullToEmptyString(HTML);
            LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
            TopicName = BizObject.ConvertNullToEmptyString(TopicName);
            MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
            MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
            MediaType = BizObject.ConvertNullToEmptyString(MediaType);
            Objectives = BizObject.ConvertNullToEmptyString(Objectives);
            Content = BizObject.ConvertNullToEmptyString(Content);
            Method = BizObject.ConvertNullToEmptyString(Method);
            GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
            DOCXFile = BizObject.ConvertNullToEmptyString(DOCXFile);
            DOCXHoFile = BizObject.ConvertNullToEmptyString(DOCXHoFile);
            Course_Number = BizObject.ConvertNullToEmptyString(Course_Number.Trim());
            CeRef = BizObject.ConvertNullToEmptyString(CeRef);

            //Release_Date = BizObject.ConvertNullToEmptyString(Release_Date);
            //Minutes = BizObject.ConvertNullToEmptyString(Minutes);
            //Audio_Ind = BizObject.ConvertNullToEmptyString(Audio_Ind);
            //Apn_Ind = BizObject.ConvertNullToEmptyString(Apn_Ind);
            //Icn_Ind = BizObject.ConvertNullToEmptyString(Icn_Ind);
            //Jcaho_Ind = BizObject.ConvertNullToEmptyString(Jcaho_Ind);
            //Magnet_Ind = BizObject.ConvertNullToEmptyString(Magnet_Ind);
            //Active_Ind = BizObject.ConvertNullToEmptyString(Active_Ind);
            //Video_Ind = BizObject.ConvertNullToEmptyString(Video_Ind);
            //Online_Ind = BizObject.ConvertNullToEmptyString(Online_Ind);
            //Course_Number = BizObject.ConvertNullToEmptyString(Course_Number);
            //Ebp_Ind = BizObject.ConvertNullToEmptyString(Ebp_Ind);


            //Ccm_Ind = BizObject.ConvertNullToEmptyString(Ccm_Ind);
            //Avail_Ind = BizObject.ConvertNullToEmptyString(Avail_Ind);
            Cert_Cerp = BizObject.ConvertNullToEmptyString(Cert_Cerp);
            Ref_Html = BizObject.ConvertNullToEmptyString(Ref_Html);
            //AlterID = BizObject.ConvertNullToEmptyString(AlterID);
            //Pass_Score = BizObject.ConvertNullToEmptyString(Pass_Score);
            Subtitle = BizObject.ConvertNullToEmptyString(Subtitle);

            //Cost = BizObject.ConvertNullToEmptyString(Cost);
            //Topic_Type = BizObject.ConvertNullToEmptyString(Topic_Type);
            Img_Name = BizObject.ConvertNullToEmptyString(Img_Name);
            Img_Credit = BizObject.ConvertNullToEmptyString(Img_Credit);
            Img_Caption = BizObject.ConvertNullToEmptyString(Img_Caption);
            Accreditation = BizObject.ConvertNullToEmptyString(Accreditation);
            topic_summary = BizObject.ConvertNullToEmptyString(topic_summary);
            rev = BizObject.ConvertNullToEmptyString(rev);
            urlmark = BizObject.ConvertNullToEmptyString(urlmark);
            cmeSponsorName = BizObject.ConvertNullToEmptyString(cmeSponsorName);

            shortname = BizObject.ConvertNullToEmptyString(shortname);
            if (shortname.Length == 0)
            {
                if (TopicName.Length > 30)
                {
                    shortname = TopicName.Substring(0, 29);
                }
                else
                {
                    shortname = TopicName;
                }
            }

            if (FacilityID == -2)
            {
                Textbook = true;
            }
            else
            {
                Textbook = false;
            }
            Obsolete = !(Avail_Ind);

            //Views = BizObject.ConvertNullToEmptyString(Views);
            //PrimaryViews = BizObject.ConvertNullToEmptyString(PrimaryViews);
            //PageReads = BizObject.ConvertNullToEmptyString(PageReads);
            //Featured = BizObject.ConvertNullToEmptyString(Featured);
            // NOTE: We add an extra parameter at the end that is not passed into this method:
            // By Default, we force FacilityID to be 0 for system-side topics
            TopicInfo record = new TopicInfo(CorLecture, Fla_CEType, Fla_IDNo, FolderName, HTML, releaseDate, expireDate, LastUpdate,
                NoSurvey, 0, TopicName, SurveyID, Compliance, MetaKW, MetaDesc, Hours,
                MediaType, Objectives, Content, Textbook, CertID, Method, GrantBy, DOCXFile, DOCXHoFile,
                Obsolete, FacilityID, Course_Number, //     CeRef, //Release_Date, 
                Minutes, Audio_Ind, Apn_Ind, Icn_Ind, Jcaho_Ind, Magnet_Ind, Active_Ind, Video_Ind,
                Online_Ind, Ebp_Ind, Ccm_Ind, Avail_Ind, Cert_Cerp, Ref_Html, AlterID, Pass_Score, Subtitle,//Cost,
                Topic_Type, Img_Name, Img_Credit, Img_Caption, Accreditation, Views, PrimaryViews, PageReads,
                Featured, rev, urlmark, offline_Cost, online_Cost, notest, uce_Ind, shortname, categoryid,
                hold_ind, 0, 0, 0, prepaid_ind, topic_summary, cmeInd, cmeSponsorInd, cmeSponsorName, "", "", webinar_start, webinar_end);
            int ret = SiteProvider.PR2.InsertTopic(record);

            BizObject.PurgeCacheItems("Topics_Topic");
            return ret;
        }

        /// <summary>
        /// Creates a new Topic
        /// </summary>
        //public static int InsertTextbookTopic(bool CorLecture, string Fla_CEType, string Fla_IDNo,
        //    string FolderName, string HTML, string LastUpdate, bool NoSurvey, string TopicName,
        //    int SurveyID, bool Compliance, string MetaKW, string MetaDesc, decimal Hours, string MediaType,
        //    string Objectives, string Content, bool Textbook, int CertID, string Method, string GrantBy,
        //    string DOCXFile, string DOCXHoFile, bool Obsolete, int FacilityID, string Course_Number, string CeRef,
        //    DateTime Release_Date, int Minutes, bool Audio_Ind, bool Apn_Ind, bool Icn_Ind, bool Jcaho_Ind, bool Magnet_Ind,
        //    bool Active_Ind, bool Video_Ind, bool Online_Ind, bool Ebp_Ind, bool Ccm_Ind, bool Avail_Ind, string Cert_Cerp, string Ref_Html,
        //    int AlterID, int Pass_Score, string Subtitle, decimal Cost, string Topic_Type, string Img_Name, string Img_Credit,
        //    string Img_Caption, string Accreditation, int Views, int PrimaryViews, int PageReads, bool Featured, string rev,
        //    string urlmark, decimal offline_Cost, decimal online_Cost, bool notest, bool uce_Ind, 
        //    string shortname, int categoryid, bool hold_ind, bool prepaid_ind, string topic_summary, 
        //    bool cmeInd, bool cmeSponsorInd, string cmeSponsorName)
        //{
        //    Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
        //    Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
        //    FolderName = BizObject.ConvertNullToEmptyString(FolderName);
        //    HTML = BizObject.ConvertNullToEmptyString(HTML);
        //    LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
        //    TopicName = BizObject.ConvertNullToEmptyString(TopicName);
        //    MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
        //    MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
        //    MediaType = BizObject.ConvertNullToEmptyString(MediaType);
        //    Objectives = BizObject.ConvertNullToEmptyString(Objectives);
        //    Content = BizObject.ConvertNullToEmptyString(Content);
        //    Method = BizObject.ConvertNullToEmptyString(Method);
        //    GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
        //    DOCXFile = BizObject.ConvertNullToEmptyString(DOCXFile);
        //    DOCXHoFile = BizObject.ConvertNullToEmptyString(DOCXHoFile);
        //    rev = BizObject.ConvertNullToEmptyString(rev);
        //    urlmark = BizObject.ConvertNullToEmptyString(urlmark);
        //    cmeSponsorName = BizObject.ConvertNullToEmptyString(cmeSponsorName);

        //    // NOTE: We add an extra parameter at the end that is not passed into this method:
        //    // By Default, we force FacilityID to be 0 for system-side topics
        //    TopicInfo record = new TopicInfo(CorLecture, Fla_CEType, Fla_IDNo, FolderName, HTML, LastUpdate,
        //        NoSurvey, 0, TopicName, SurveyID, Compliance, MetaKW, MetaDesc, Hours, MediaType,
        //        Objectives, Content, true, CertID, Method, GrantBy, DOCXFile, DOCXHoFile, Obsolete, 0, Course_Number, //     CeRef,//Release_Date,
        //        Minutes, Audio_Ind, Apn_Ind, Icn_Ind, Jcaho_Ind, Magnet_Ind, Active_Ind, Video_Ind,
        //        Online_Ind, Ebp_Ind, Ccm_Ind, Avail_Ind, Cert_Cerp, Ref_Html, AlterID, Pass_Score, Subtitle, //Cost,
        //        Topic_Type, Img_Name, Img_Credit, Img_Caption, Accreditation, Views, PrimaryViews, PageReads,
        //        Featured, rev, urlmark, offline_Cost, online_Cost, notest, uce_Ind, shortname, categoryid, 
        //        hold_ind,0,0,0,prepaid_ind, topic_summary, cmeInd, cmeSponsorInd, cmeSponsorName,"","");
        //    int ret = SiteProvider.PR2.InsertTopic(record);

        //    BizObject.PurgeCacheItems("Topics_Topic");
        //    return ret;
        //}

        //public static int InsertNonTextbookTopic(bool CorLecture, string Fla_CEType, string Fla_IDNo,
        //    string FolderName, string HTML, string LastUpdate, bool NoSurvey, string TopicName,
        //    int SurveyID, bool Compliance, string MetaKW, string MetaDesc, decimal Hours, string MediaType,
        //    string Objectives, string Content, bool Textbook, int CertID, string Method, string GrantBy,
        //    string DOCXFile, string DOCXHoFile, bool Obsolete, int FacilityID, string Course_Number, string CeRef,
        //    DateTime Release_Date, int Minutes, bool Audio_Ind, bool Apn_Ind, bool Icn_Ind, bool Jcaho_Ind, bool Magnet_Ind,
        //    bool Active_Ind, bool Video_Ind, bool Online_Ind, bool Ebp_Ind, bool Ccm_Ind, bool Avail_Ind, string Cert_Cerp, string Ref_Html,
        //    int AlterID, int Pass_Score, string Subtitle, decimal Cost, string Topic_Type, string Img_Name, string Img_Credit,
        //    string Img_Caption, string Accreditation, int Views, int PrimaryViews, int PageReads, bool Featured, string rev,
        //    string urlmark, decimal offline_Cost, decimal online_Cost, bool notest, bool uce_Ind, 
        //    string shortname, int categoryid, bool hold_ind, bool prepaid_ind, string topic_summary, 
        //    bool cmeInd, bool cmeSponsorInd, string cmeSponsorName)
        //{
        //    Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
        //    Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
        //    FolderName = BizObject.ConvertNullToEmptyString(FolderName);
        //    HTML = BizObject.ConvertNullToEmptyString(HTML);
        //    LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
        //    TopicName = BizObject.ConvertNullToEmptyString(TopicName);
        //    MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
        //    MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
        //    MediaType = BizObject.ConvertNullToEmptyString(MediaType);
        //    Objectives = BizObject.ConvertNullToEmptyString(Objectives);
        //    Content = BizObject.ConvertNullToEmptyString(Content);
        //    Method = BizObject.ConvertNullToEmptyString(Method);
        //    GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
        //    DOCXFile = BizObject.ConvertNullToEmptyString(DOCXFile);
        //    DOCXHoFile = BizObject.ConvertNullToEmptyString(DOCXHoFile);
        //    rev = BizObject.ConvertNullToEmptyString(rev);
        //    urlmark = BizObject.ConvertNullToEmptyString(urlmark);
        //    cmeSponsorName = BizObject.ConvertNullToEmptyString(cmeSponsorName);

        //    // NOTE: We add an extra parameter at the end that is not passed into this method:
        //    // By Default, we force FacilityID to be 0 for system-side topics
        //    TopicInfo record = new TopicInfo(CorLecture, Fla_CEType, Fla_IDNo, FolderName, HTML, LastUpdate,
        //        NoSurvey, 0, TopicName, SurveyID, Compliance, MetaKW, MetaDesc, Hours, MediaType,
        //        Objectives, Content, false, CertID, Method, GrantBy, DOCXFile, DOCXHoFile, Obsolete, 0, Course_Number, //     CeRef,//Release_Date, 
        //        Minutes, Audio_Ind, Apn_Ind, Icn_Ind, Jcaho_Ind, Magnet_Ind, Active_Ind, Video_Ind,
        //        Online_Ind, Ebp_Ind, Ccm_Ind, Avail_Ind, Cert_Cerp, Ref_Html, AlterID, Pass_Score, Subtitle, //Cost, 
        //        Topic_Type, Img_Name, Img_Credit, Img_Caption, Accreditation, Views, PrimaryViews, PageReads, Featured, rev,
        //        urlmark, offline_Cost, online_Cost, notest, uce_Ind, shortname, categoryid, hold_ind,0,0,0,
        //        prepaid_ind, topic_summary, cmeInd, cmeSponsorInd, cmeSponsorName,"","");
        //    int ret = SiteProvider.PR2.InsertTopic(record);

        //    BizObject.PurgeCacheItems("Topics_Topic");
        //    return ret;
        //}

        /// Bsk New 
        /// Creates a new Topic Using Existing Topic Data
        /// </summary>
        public static int InsertTopicFromExistingTopicData(int TopicID, string New_TopicName, string New_Course_Number)
        {
            int ret = SiteProvider.PR2.InsertTopicFromExistingTopicData(TopicID, New_TopicName, New_Course_Number);
            return ret;
        }


        /// <summary>
        /// Deletes an existing Topic, but first checks if OK to delete
        /// </summary>
        public static bool DeleteTopic(int TopicID)
        {
            bool IsOKToDelete = OKToDelete(TopicID);
            if (IsOKToDelete)
            {
                return (bool)DeleteTopic(TopicID, true);
            }
            else
            {
                return false;
            }
        }





        /// <summary>
        /// Returns a Topic object filled with the data taken from the input TopicInfo
        /// </summary>
        private static Topic GetTopicFromTopicInfo(TopicInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Topic(record.CorLecture, record.Fla_CEType, record.Fla_IDNo, record.FolderName,
                    record.HTML, record.ReleaseDate, record.ExpireDate, record.LastUpdate, record.NoSurvey, record.TopicID, record.TopicName,
                    record.SurveyID, record.Compliance, record.MetaKW, record.MetaDesc, record.Hours,
                    record.MediaType, record.Objectives, record.Content, record.Textbook, record.CertID,
                    record.Method, record.GrantBy, record.DOCXFile, record.DOCXHoFile, record.Obsolete,
                    record.FacilityID, record.Course_Number,//record.CeRef,record.Release_Date,
                    record.Minutes, record.Audio_Ind, record.Apn_Ind, record.Icn_Ind, record.Jcaho_Ind, record.Magnet_Ind,
                    record.Active_Ind, record.Video_Ind, record.Online_Ind, record.Ebp_Ind, record.Ccm_Ind, record.Avail_Ind,
                    record.Cert_Cerp, record.Ref_Html, record.AlterID, record.Pass_Score, record.Subtitle,//record.Cost, 
                    record.Topic_Type, record.Img_Name, record.Img_Credit, record.Img_Caption, record.Accreditation,
                    record.Views, record.PrimaryViews, record.PageReads, record.Featured, record.rev, record.urlmark,
                    record.offline_Cost, record.online_Cost, record.notest, record.uce_Ind, record.shortname, record.categoryid,
                    record.hold_ind, record.rating_avg, record.rating_num_comment, record.rating_total,
                    record.prepaid_ind, record.topic_summary, record.CMEInd, record.CMESponsorInd,
                    record.CMESponsorName, record.Topic_hourlong, record.Topic_hourshort, record.Webinar_start, record.Webinar_end);
            }
        }

        /// <summary>
        /// Returns a list of Topic objects filled with the data taken from the input list of TopicInfo
        /// </summary>
        private static List<Topic> GetTopicListFromTopicInfoList(List<TopicInfo> recordset)
        {
            List<Topic> Topics = new List<Topic>();
            foreach (TopicInfo record in recordset)
                Topics.Add(GetTopicFromTopicInfo(record));
            return Topics;
        }



        /// <summary>
        /// Returns a CheckBoxListTopic object filled with the data taken from the input CheckBoxListTopicInfo
        /// </summary>
        private static CheckBoxListTopicInfo GetCheckBoxListTopicFromCheckBoxListTopicInfo(CheckBoxListTopicInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new CheckBoxListTopicInfo(record.TopicID, record.TopicName, record.Selected);
            }
        }

        /// <summary>
        /// Returns a list of CheckBoxListTopic objects filled with the data taken from the input list of CheckBoxListTopicInfo
        /// </summary>
        private static List<CheckBoxListTopicInfo> GetCheckBoxListTopicListFromCheckBoxListTopicInfoList(List<CheckBoxListTopicInfo> recordset)
        {
            List<CheckBoxListTopicInfo> CheckBoxListTopics = new List<CheckBoxListTopicInfo>();
            foreach (CheckBoxListTopicInfo record in recordset)
                CheckBoxListTopics.Add(GetCheckBoxListTopicFromCheckBoxListTopicInfo(record));
            return CheckBoxListTopics;
        }


        public static List<Topic> GetTopicsByCategoryWebsite(string Website, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset = SiteProvider.PR2.GetTopicsByCategoryWebsite(Website, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);
            return Topics;


        }


        public static List<Topic> GettwoPearlsNewTopics()
        {
            List<Topic> Topics = null;

            List<TopicInfo> recordset = SiteProvider.PR2.GettwoPearlsNewTopics();
            Topics = GetTopicListFromTopicInfoList(recordset);
            return Topics;
        }

        public static void UpdateExpiringCourses()
        {
            SiteProvider.PR2.UpdateExpiringCourses();
        }

        public static void UpdateDaystoexpire()
        {
            SiteProvider.PR2.UpdateDaystoexpire();
        }

        public static void DeactiveExpiredCourses()
        {
            SiteProvider.PR2.DeactiveExpiredCourses();
        }

        public static void RemoveExpiredCourses()
       {
           SiteProvider.PR2.RemoveExpiredCourses();
       }
        /// <summary>
        /// Returns a collection with all Topics for the specified Category
        /// </summary>
        public static List<Topic> GetTopicsByCategoryID(int CategoryID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_Topics_" + CategoryID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset;
                switch (CategoryID)
                {
                    case 1000001:
                        // ALL non-textbook topics (not obsolete)
                        recordset = SiteProvider.PR2.GetNonTextbookTopics(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                    case 1000002:
                        // Non-textbook topics with NO Categories (not obsolete)
                        recordset = SiteProvider.PR2.GetNonTextbookTopicsWithNoCategories(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                    case 1000003:
                        // Obsolete Non-textbook topics
                        recordset = SiteProvider.PR2.GetObsoleteNonTextbookTopics(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                    case 1000004:
                        // ALL textbook topics (not obsolete) 
                        recordset = SiteProvider.PR2.GetTextbookTopics(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                    case 1000005:
                        // Textbook topics with NO Categories (not obsolete) 
                        recordset = SiteProvider.PR2.GetTextbookTopicsWithNoCategories(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                    case 1000006:
                        // Obsolete Textbook topics
                        recordset = SiteProvider.PR2.GetObsoleteTextbookTopics(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;

                    case 1000007:
                        //CEDirect Topics
                        //The facility Id is '0' for CEDiect Topics
                        recordset = SiteProvider.PR2.GetNonTextbookTopicsByFacilityId(0, cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;

                    case 1000008:
                        //PearlsReview Topics
                        //The facility Id is '-1' for CEDiect Topics
                        recordset = SiteProvider.PR2.GetNonTextbookTopicsByFacilityId(-1, cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;

                    case 1000009:
                        //Complaince Topics
                        //The facility Id is '-3' for CEDiect Topics
                        recordset = SiteProvider.PR2.GetNonTextbookTopicsByFacilityId(-3, cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;


                    default:
                        // ALL topics assigned to a categoryID 
                        recordset = SiteProvider.PR2.GetTopicsByCategoryID(CategoryID, cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                }

            }
            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Topics for the specified Category, with test info for userid
        /// </summary>
        public static List<Topic> GetTopicsByCategoryIDAndUserIDWithTestInfo(int CategoryID, int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_Topics_" + CategoryID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset;
                switch (CategoryID)
                {
                    case 1000001:
                        // ALL non-textbook topics (not obsolete)
                        recordset = SiteProvider.PR2.GetNonTextbookTopics(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                    case 1000002:
                        // Non-textbook topics with NO Categories (not obsolete)
                        recordset = SiteProvider.PR2.GetNonTextbookTopicsWithNoCategories(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                    case 1000003:
                        // Obsolete Non-textbook topics
                        recordset = SiteProvider.PR2.GetObsoleteNonTextbookTopics(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                    case 1000004:
                        // ALL textbook topics (not obsolete) 
                        recordset = SiteProvider.PR2.GetTextbookTopics(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                    case 1000005:
                        // Textbook topics with NO Categories (not obsolete) 
                        recordset = SiteProvider.PR2.GetTextbookTopicsWithNoCategories(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                    case 1000006:
                        // Obsolete Textbook topics
                        recordset = SiteProvider.PR2.GetObsoleteTextbookTopics(cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                    default:
                        // ALL topics assigned to a categoryID 
                        recordset = SiteProvider.PR2.GetTopicsByCategoryID(CategoryID, cSortExpression);
                        Topics = GetTopicListFromTopicInfoList(recordset);
                        BasePR.CacheData(key, Topics);
                        break;
                }

            }
            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Topics which are text book by category id
        /// </summary>
        public static List<Topic> GetTextBookTopicsByCategoryId(int categoryid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetTextBookTopicsByCategoryId(categoryid, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Topics which are text book by category id
        /// </summary>
        public static List<Topic> GetTopicsByFacilityId(int FacilityId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetTopicsByFacilityId(FacilityId, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Topics which have fac id <=0
        /// </summary>
        public static List<Topic> GetTopicsByFacIDLessthanEqualsZero(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetTopicsByFacIDLessthanEqualsZero(cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Topics which are text book by category id
        /// </summary>
        public static List<Topic> GetTopicsByCategoryName(string categoryname, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetTopicsByCategoryName(categoryname, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Topics which are text book by category id for retail site
        /// </summary>
        public static List<Topic> GetTopicsByCategoryNameForRetail(string categoryname, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetTopicsByCategoryNameForRetail(categoryname, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Topics which are text book by category id
        /// </summary>
        public static List<Topic> GetTextAndNonTextTopicsByCategoryId(int categoryid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetTextAndNonTextTopicsByCategoryId(categoryid, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }



        /// <summary>
        /// Returns a collection with all Topics which are text book by category id
        /// </summary>
        public static List<Topic> GetFacilityTopics()
        {
            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetFacilityTopics();
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }


        /// <summary>
        /// Returns a Dataset with all Topics in progress for userid
        /// </summary>
        public static DataSet GetTopicsBySearchText(string cSearchText, string cSortExpression)
        {
            DataSet recordset = null;
            if (cSortExpression == null)
                cSortExpression = "";



            //List<Topic> Topics = null;
            string key = "Topics_TopicsBySearchText_" + cSearchText.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetTopicsBySearchText(cSearchText, cSortExpression);

            }
            return recordset;
        }

        //public static DataSet GetTopicsBySearchAndSubsearchTextAndDomainID(
        //    string search, string subSearch, int domainID, string sortExpression)
        //{
        //    DataSet recordset = null;
        //    if (sortExpression == null)
        //        sortExpression = "";

        //    string key = "Topics_TopicsBySearchAndSubsearchTextAndDomainID_" + search 
        //        + "_" + subSearch + "_" + domainID.ToString() + sortExpression;

        //    if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
        //    {

        //    }
        //    else
        //    {
        //        recordset = SiteProvider.PR2.GetTopicsBySearchAndSubsearchTextAndDomainID(
        //            search, subSearch, domainID, sortExpression);

        //    }
        //    return recordset;
        //}

        /// <summary>
        /// Returns a collection with all CEDirect Topics where search text is found in TopicName, subtitle or objectives
        /// </summary>
        public static DataSet GetTopicsBySearchTextandFacility(string cSearchText, int facilityid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            DataSet recordset = null;
            recordset = SiteProvider.PR2.GetTopicsBySearchTextandFacility(cSearchText, facilityid, cSortExpression);
            return recordset;
        }





        /// <summary>
        /// Returns a collection with all Topics in progress for userid
        /// </summary>
        public static List<Topic> GetTopicsInProgressByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;

            // ALL topics assigned to a categoryID 
            recordset = SiteProvider.PR2.GetTopicsInProgressByUserID(UserID, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Topics in progress for userid
        /// </summary>
        public static List<Topic> GetTopicsInProgressByUserIDAndCount(int UserID, int Count, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "lastmod";

            List<Topic> Topics = null;
            string key = "Topics_InProgressByUserIDAndCount_" + UserID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {

                List<TopicInfo> recordset = SiteProvider.PR2.GetTopicsInProgressByUserIDAndCount(UserID, Count, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }

            return Topics;
        }


        /// <summary>
        /// Returns a collection with all Topics for the specified LectureDefinition
        /// </summary>
        public static List<Topic> GetTopicsByLectureID(int LectureID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_TopicsByLecture_" + LectureID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                List<TopicInfo> recordset = SiteProvider.PR2.GetTopicsByLectureID(LectureID, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        public static DataSet GetTopicsByLectID(int LectureID, string cSortExpression)
        {
            DataSet recordset = null;
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            //List<Topic> Topics = null;
            string key = "Topics_TopicsByLecture_" + LectureID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetTopicsByLectID(LectureID, cSortExpression);

            }
            return recordset;
        }
        ///// <summary>
        ///// Returns a collection with all Topics for the specified Category
        ///// </summary>
        //public static List<Topic> GetAvailableTopicsByCategoryIDAndUserName(int CategoryID, string UserName, string cSortExpression)

        //{
        //    if (cSortExpression == null)
        //        cSortExpression = "";

        //    // provide default sort
        //    if (cSortExpression.Length == 0)
        //        cSortExpression = "TopicName";

        //    List<Topic> Topics = null;
        //    string key = "Topics_AvailableTopics_" + CategoryID.ToString() + UserName.Trim().ToString()+cSortExpression.ToString();

        //    if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
        //    {
        //        Topics = (List<Topic>)BizObject.Cache[key];
        //    }
        //    else
        //    {
        //        List<TopicInfo> recordset = SiteProvider.PR2.GetAvailableTopicsByCategoryIDAndUserName(CategoryID, UserName, cSortExpression);
        //        Topics = GetTopicListFromTopicInfoList(recordset);
        //        BasePR.CacheData(key, Topics);

        //    }
        //    return Topics;
        //}

        /// <summary>
        /// Returns a collection with all Topics for the specified Category that are not already in a user's
        /// portfolio
        /// </summary>
        public static List<Topic> GetAvailableTopicsByCategoryIDAndUserID(int CategoryID, int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_AvailableTopics_" + CategoryID.ToString() + "_UserID_" + UserID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetAvailableTopicsByCategoryIDAndUserID(CategoryID, UserID, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);

            }
            return Topics;
        }

        /// <summary>
        /// Returns the highest TopicID
        /// (used to find the last ID inserted because all other methods
        /// are failing)
        /// </summary>
        public static int GetHighestTopicID()
        {
            int TopicID = 0;
            TopicID = SiteProvider.PR2.GetHighestTopicID();
            return TopicID;
        }

        /// <summary>
        /// Returns the number of total Topics for a Category, except Obsolete ones
        /// </summary>
        public static int GetTopicCount(int CategoryID)
        {
            int TopicCount = 0;
            string key = "Topics_TopicCount_" + CategoryID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicCount = SiteProvider.PR2.GetTopicCountByCategoryID(CategoryID);
                BasePR.CacheData(key, TopicCount);
            }
            return TopicCount;
        }

        /// <summary>
        /// Returns a Topic object with the specified ID
        /// This overloaded method adds the website base URL to all
        /// URLs in the HTML field that begin with PP/, PDF/, images/
        /// </summary>
        public static Topic GetTopicByIDWithFixedURLs(int TopicID)
        {
            Topic Topic = GetTopicByID(TopicID);
            Topic.HTML = Topic.HTML.Replace("/PP/", "PP/");
            Topic.HTML = Topic.HTML.Replace("/PDF/", "PDF/");
            Topic.HTML = Topic.HTML.Replace("/IMAGES/", "IMAGES/");
            Topic.HTML = Topic.HTML.Replace("/pp/", "pp/");
            Topic.HTML = Topic.HTML.Replace("/pdf/", "pdf/");
            Topic.HTML = Topic.HTML.Replace("/images/", "images/");
            Topic.HTML = Topic.HTML.Replace("/bib.htm", "bib.htm");
            Topic.HTML = Topic.HTML.Replace("/disclaimer.htm", "disclaimer.htm");

            Topic.HTML = Topic.HTML.Replace("PP/", "http://www.pearlsreview.com/PP/");
            Topic.HTML = Topic.HTML.Replace("PDF/", "http://www.pearlsreview.com/PDF/");
            Topic.HTML = Topic.HTML.Replace("IMAGES/", "http://www.pearlsreview.com/IMAGES/");
            Topic.HTML = Topic.HTML.Replace("pp/", "http://www.pearlsreview.com/pp/");
            Topic.HTML = Topic.HTML.Replace("pdf/", "http://www.pearlsreview.com/pdf/");
            Topic.HTML = Topic.HTML.Replace("images/", "http://www.pearlsreview.com/images/");
            Topic.HTML = Topic.HTML.Replace("bib.htm", "http://www.pearlsreview.com/bib.htm");
            Topic.HTML = Topic.HTML.Replace("disclaimer.htm", "http://www.pearlsreview.com/disclaimer.htm");

            return Topic;
        }

        /// </summary>
        public static bool UpdateTopicHTML(int TopicID, string HTML, string LastUpdate, string FolderName)
        {
            bool ret = SiteProvider.PR2.UpdateTopicHTML(TopicID, HTML, LastUpdate, FolderName);
            return ret;
        }
        public static bool UpdateTopicImage(int TopicID, string image)
        {
            bool ret = SiteProvider.PR2.UpdateTopicImage(TopicID, image);
            return ret;
        }

        public static bool UpdateTextbookTopicHTML(int TopicID, string HTML, string LastUpdate, string FolderName,
            string DOCXFile, string DOCXHoFile)
        {
            bool ret = SiteProvider.PR2.UpdateTextbookTopicHTML(TopicID, HTML, LastUpdate, FolderName, DOCXFile, DOCXHoFile);
            return ret;
        }

        public static bool UpdateTopicEditableFields(int TopicID, bool CorLecture,
            string Fla_CEType, string Fla_IDNo, string LastUpdate, bool NoSurvey,
            int SurveyID, bool Compliance, string MetaKW, string MetaDesc,
            decimal Hours, string MediaType, string Objectives, string Content,
            bool Textbook, int CertID, string Method, string GrantBy, bool Obsolete, string TopicName)
        {
            Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
            Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
            LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
            MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
            MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
            MediaType = BizObject.ConvertNullToEmptyString(MediaType);
            Objectives = BizObject.ConvertNullToEmptyString(Objectives);
            Content = BizObject.ConvertNullToEmptyString(Content);
            Method = BizObject.ConvertNullToEmptyString(Method);
            GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
            TopicName = BizObject.ConvertNullToEmptyString(TopicName);

            bool ret = SiteProvider.PR2.UpdateTopicEditableFields(TopicID, CorLecture,
                Fla_CEType, Fla_IDNo, LastUpdate, NoSurvey,
                SurveyID, Compliance, MetaKW, MetaDesc,
                Hours, MediaType, Objectives, Content,
                Textbook, CertID, Method, GrantBy, Obsolete, TopicName);
            return ret;
        }

        public static bool UpdateTextbookTopicEditableFields(int TopicID, bool CorLecture,
            string Fla_CEType, string Fla_IDNo, string LastUpdate, bool NoSurvey,
            int SurveyID, bool Compliance, string MetaKW, string MetaDesc,
            decimal Hours, string MediaType, string Objectives, string Content,
            bool Textbook, int CertID, string Method, string GrantBy, bool Obsolete, string TopicName)
        {
            Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
            Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
            LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
            MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
            MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
            MediaType = BizObject.ConvertNullToEmptyString(MediaType);
            Objectives = BizObject.ConvertNullToEmptyString(Objectives);
            Content = BizObject.ConvertNullToEmptyString(Content);
            Method = BizObject.ConvertNullToEmptyString(Method);
            GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
            TopicName = BizObject.ConvertNullToEmptyString(TopicName);

            bool ret = SiteProvider.PR2.UpdateTopicEditableFields(TopicID, CorLecture,
                Fla_CEType, Fla_IDNo, LastUpdate, NoSurvey,
                SurveyID, Compliance, MetaKW, MetaDesc,
                Hours, MediaType, Objectives, Content,
                true, CertID, Method, GrantBy, Obsolete, TopicName);
            return ret;
        }


        public static bool UpdateNonTextbookTopicEditableFields(int TopicID, bool CorLecture,
            string Fla_CEType, string Fla_IDNo, string LastUpdate, bool NoSurvey,
            int SurveyID, bool Compliance, string MetaKW, string MetaDesc,
            decimal Hours, string MediaType, string Objectives, string Content,
            bool Textbook, int CertID, string Method, string GrantBy, bool Obsolete, string TopicName)
        {
            Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
            Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
            LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
            MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
            MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
            MediaType = BizObject.ConvertNullToEmptyString(MediaType);
            Objectives = BizObject.ConvertNullToEmptyString(Objectives);
            Content = BizObject.ConvertNullToEmptyString(Content);
            Method = BizObject.ConvertNullToEmptyString(Method);
            GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
            TopicName = BizObject.ConvertNullToEmptyString(TopicName);

            bool ret = SiteProvider.PR2.UpdateTopicEditableFields(TopicID, CorLecture,
                Fla_CEType, Fla_IDNo, LastUpdate, NoSurvey,
                SurveyID, Compliance, MetaKW, MetaDesc,
                Hours, MediaType, Objectives, Content,
                false, CertID, Method, GrantBy, Obsolete, TopicName);
            return ret;
        }

        public static bool UpdateTopicCategoryAssignments(int TopicID, string CategoryIDAssignments)
        {
            bool ret = SiteProvider.PR2.UpdateTopicCategoryAssignments(TopicID, CategoryIDAssignments);
            // TODO: release cache?
            return ret;
        }

        public static bool UpdateTopicActiveStatus(bool status, int topicid)
        {
            bool ret = SiteProvider.PR2.UpdateTopicActiveStatus(status, topicid);
            // TODO: release cache?
            return ret;
        }

        public static int UpdateTopicInProgressByUserID(int UserID, int TopicID)
        {
            int ret = SiteProvider.PR2.UpdateTopicInProgressByUserID(UserID, TopicID);
            // TODO: release cache?
            return ret;
        }
        /// <summary>
        /// Checks to see if a Topic can be deleted safely
        /// (NO if it has any activity involving it in other tables,
        /// except for Attached Categories, which can be safely
        /// detached during the topic delete and topicdetails records
        /// which can be safely deleted also)
        /// </summary>
        public static bool OKToDelete(int TopicID)
        {
            int TestRecords = SiteProvider.PR2.GetTestCountByTopicID(TopicID);
            return (TestRecords == 0);
        }

        /// <summary>
        /// Deletes an existing Topic and breaks any links to categories
        /// and deletes any TopicDetails records
        /// (questions and answers stored in coursedetail.dbf)
        ///
        /// NOTE: This overload's second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteTopic(int TopicID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            // TODO: Need some kind of transaction around all of this

            // get rid of question and answer records

            // TODO: We need to see about deleting from the TestDefinition table
            // now that we're using that structure instead of the old TopicDetails approach.

            // NOTE: I'm calling the DAL directly here for TopicDetails
            //SiteProvider.PR2.DeleteTopicDetailsByTopicID(TopicID);
            // break links to all categories
            DetachTopicFromAllCategories(TopicID);

            bool ret = SiteProvider.PR2.DeleteTopic(TopicID);
            //         new RecordDeletedEvent("Topic", iID, null).Raise();
            BizObject.PurgeCacheItems("Topics_Topic");
            return ret;
        }

        /// <summary>
        /// Assign Topic to a Category
        /// </summary>
        public static bool AssignTopicToCategory(int TopicID, int CategoryID)
        {
            bool ret = SiteProvider.PR2.AssignTopicToCategory(TopicID, CategoryID);
            // TODO: release cache?
            return ret;
        }

        /// <summary>
        /// Detach Topic Assignment from a Category
        /// </summary>
        public static bool DetachTopicFromCategory(int TopicID, int CategoryID)
        {
            bool ret = SiteProvider.PR2.DetachTopicFromCategory(TopicID, CategoryID);
            // TODO: release cache?
            return ret;
        }

        /// <summary>
        /// Detach Topic Assignment from ALL Categories
        /// (used just before deleting a topic completely so there is
        /// no leftover garbage -- we don't have cascading delete at the moment)
        /// </summary>
        public static bool DetachTopicFromAllCategories(int TopicID)
        {
            bool ret = SiteProvider.PR2.DetachTopicFromAllCategories(TopicID);
            // TODO: release cache?
            return ret;
        }

        /// <summary>
        /// Returns a collection with all Topics for the specified LectureEvent
        /// </summary>
        public static List<Topic> GetTopicsByLectEvtID(int LectEvtID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "Topics_TopicsByLectureEvent_" + LectEvtID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                List<TopicInfo> recordset = SiteProvider.PR2.GetTopicsByLectEvtID(LectEvtID, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);
            }
            return Topics;
        }

        public static System.Data.DataSet GetRecommendationTopicsByTopicId(int TopicID)
        {
            DataSet dsTopic = SiteProvider.PR2.GetRecommendationTopicsByTopicId(TopicID);
            return dsTopic;
        }

        public static int IsPTOTTopic(int TopicId)
        {
            int iReturn = SiteProvider.PR2.IsPTOTTopic(TopicId);
            return iReturn;
        }

        /// <summary>
        /// Returns a collection with all CE Retail Topics where search text is found in TopicName, subtitle or objectives
        /// </summary>
        public static List<Topic> GetRetailTopicsBySearchTextandFacility(string cSearchText, int facilityid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetRetailTopicsBySearchTextandFacility(cSearchText, facilityid, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }

        public static List<Topic> GetPearlsReviewTopicsBySearchText(string cSearchText, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetPearlsReviewTopicsBySearchText(cSearchText,cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;

        }

        /// <summary>
        /// Returns a collection with all CE Retail Topics where search text is found in TopicName, subtitle or objectives
        /// </summary>
        public static List<Topic> GetFacilityTopicsByFacilityId(int FacilityId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetFacilityTopicsByFacilityId(FacilityId, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }


        /// <summary>
        /// Returns a collection with all CE Retail Topics where search text is found in TopicName, subtitle or objectives
        /// </summary>
        public static List<Topic> GetWebinarTopics(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetWebinarTopics(cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }

        /// <summary>
        /// Returns a collection with all CE Retail Topics where search text is found in TopicName, subtitle or objectives
        /// </summary>
        public static List<Topic> GetNewTopicsByCount(int Count, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetNewTopicsByCount(Count, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }

        public static DataSet GetExpiringCourses(String cSortExpression )
        {
            return SiteProvider.PR2.GetExpiringCourses(cSortExpression);
        }

        public static void LoadExpiringCourses()
        {
            SiteProvider.PR2.LoadExpiringCourses();
        }

        public static void UpdateExpiringCourses(int topicid, bool deactive)
        {
            SiteProvider.PR2.UpdateExpiringCourses(topicid, deactive);
        }
        /// <summary>
        /// Returns a collection with all CE Retail Topics where search text is found in TopicName, subtitle or objectives
        /// </summary>
        public static List<Topic> GetFacilityTopicsByFacilityIdAndMediaType(int FacilityId, string MediaType, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;

            List<TopicInfo> recordset;
            recordset = SiteProvider.PR2.GetFacilityTopicsByFacilityIdAndMediaType(FacilityId, MediaType, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;

        }

        /// <summary>
        /// Returns a collection with all Topics in progress for userid
        /// </summary>
        public static List<Topic> GetTopicsInProgressByUserIDAndAreaName(int UserID, string AreaName, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            //string key = "Topics_Topics_" + CategoryID.ToString() + cSortExpression.ToString();

            //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            //{
            //    Topics = (List<Topic>)BizObject.Cache[key];
            //}
            //else
            //{

            List<TopicInfo> recordset;

            // ALL topics assigned to a categoryID 
            recordset = SiteProvider.PR2.GetTopicsInProgressByUserIDAndAreaName(UserID, AreaName, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }

        /// <summary>
        /// Returns a Dataset with all Topics in progress for userid
        /// </summary>
        public static DataSet GetTopicsInProgressByUserIDAndFacilityID(int UserID, int FacilityID, string cSortExpression)
        {
            DataSet recordset = null;
            if (cSortExpression == null)
                cSortExpression = "";



            //List<Topic> Topics = null;
            string key = "Topics_TopicsByLecture_" + UserID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetTopicsInProgressByUserIDAndFacilityID(UserID, FacilityID, cSortExpression);

            }
            return recordset;
        }

        /// <summary>
        /// Returns a Dataset with all Topics in progress for userid
        /// </summary>
        public static DataSet GetMyEnrolledTopicsByUserIdAndFacilityId(int UserID, int FacilityID, string cSortExpression)
        {
            DataSet recordset = null;
            if (cSortExpression == null)
                cSortExpression = "";



            //List<Topic> Topics = null;
            string key = "Topics_GetMyEnrolledTopicsByUserIdAndFacilityId_" + UserID.ToString() + FacilityID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetMyEnrolledTopicsByUserIdAndFacilityId(UserID, FacilityID, cSortExpression);

            }
            return recordset;
        }
        /// <summary>
        /// Returns a collection with all Topics in progress for userid
        /// </summary>
        public static List<Topic> GetActiveFeaturedTopicsByFacID(int FacID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            List<TopicInfo> recordset;
            // ALL topics assigned to a categoryID 
            recordset = SiteProvider.PR2.GetActiveFeaturedTopicsByFacID(FacID, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }

        /// <summary>
        /// Returns a collection with all Topics in progress for userid
        /// </summary>
        public static List<Topic> GetNewTopicsByFacID(int FacID, int Count, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            List<TopicInfo> recordset;
            // ALL topics assigned to a categoryID 
            recordset = SiteProvider.PR2.GetNewTopicsByFacID(FacID, Count, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }

        /// <summary>
        /// Returns a collection with all new Topics for welcome page
        /// </summary>
        public static List<Topic> GetNewTopicsFromPickListByFacID(int FacID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            List<TopicInfo> recordset;
            // ALL topics assigned to a categoryID 
            recordset = SiteProvider.PR2.GetNewTopicsFromPickListByFacID(FacID, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }

        /// <summary>
        /// Returns a collection with all popular Topics for welcome page
        /// </summary>
        public static List<Topic> GetPopularTopicsFromPickListByFacID(int FacID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            List<TopicInfo> recordset;
            // ALL topics assigned to a categoryID 
            recordset = SiteProvider.PR2.GetPopularTopicsFromPickListByFacID(FacID, cSortExpression);
            Topics = GetTopicListFromTopicInfoList(recordset);

            return Topics;
        }

        /// <summary>
        /// Returns a Dataset with all Topics in progress for userid
        /// </summary>
        public static DataSet GetTopicsByCategoryID(int CategoryID)
        {
            DataSet recordset = null;

            //List<Topic> Topics = null;
            string key = "Topics_TopicsByCategoryID_" + CategoryID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetTopicsByCategoryID(CategoryID);

            }
            return recordset;
        }

        /// Returns a Dataset with all ice Topics in progress for userid
        /// </summary>
        public static DataSet GetICETopicsByCategoryID(int CategoryID, string cSortExpression, bool Upcoming)
        {
            DataSet recordset = null;

            //List<Topic> Topics = null;
            string key = "Topics_ICETopicsByCategoryID_" + CategoryID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetICETopicsByCategoryID(CategoryID, cSortExpression, Upcoming);

            }
            return recordset;
        }

        /// Returns a Dataset with all Topics in progress for userid
        /// </summary>
        public static DataSet GetEnrolledTopicsByUserId(int userid, string cSortExpression)
        {
            DataSet recordset = null;

            //List<Topic> Topics = null;
            string key = "Topics_GetEnrolledTopicsByUserId_" + userid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetEnrolledTopicsByUserId(userid, cSortExpression);

            }
            return recordset;
        }

        /// Returns a Dataset with all Topics in progress for userid
        /// </summary>
        public static DataSet GetAssignedTopicsByUserId(int userid, string cSortExpression)
        {
            DataSet recordset = null;

            //List<Topic> Topics = null;
            string key = "Topics_GetAssignedTopicsByUserId_" + userid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetAssignedTopicsByUserId(userid, cSortExpression);

            }
            return recordset;
        }



        /// <summary>
        /// Returns a Dataset with all Topics in progress for userid
        /// </summary>
        public static DataSet GetFacilityTopicsByFacID(int FacID)
        {
            DataSet recordset = null;

            //List<Topic> Topics = null;
            string key = "Topics_TopicsByFacID_" + FacID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetFacilityTopicsByFacID(FacID);

            }
            return recordset;
        }

        public static DataSet GetCEModulesByICETopicID(int topicid,bool isWebinar)
        {
            return SiteProvider.PR2.GetCEModulesByICETopicID(topicid,isWebinar);
        }

        public static DataSet GetEnrolledTopicsByFacIDAndDeptIDs(int FacID,int uniqueid,int deptid)
        {
            DataSet recordset = null;

            //List<Topic> Topics = null;
            string key = "Topics_TopicsByFacID_" + FacID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetEnrolledTopicsByFacIDAndDeptIDs(FacID,uniqueid,deptid);

            }
            return recordset;
        }


        /// <summary>
        /// Returns a Dataset with all Topics in progress for userid
        /// </summary>
        public static List<Topic> GetWebinarTopicsByFacID(int FacID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicName";

            List<Topic> Topics = null;
            string key = "GetWebinarTopicsByFacID_" + FacID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                List<TopicInfo> recordset = SiteProvider.PR2.GetWebinarTopicsByFacID(FacID, cSortExpression);
                Topics = GetTopicListFromTopicInfoList(recordset);
                BasePR.CacheData(key, Topics);

            }
            return Topics;
        }


        public static List<Topic> GetTopicsByTopicIds(string TopicIds, string cSortExpression)
        {
            List<TopicInfo> recordset = SiteProvider.PR2.GetTopicsByTopicIds(TopicIds, cSortExpression);
            List<Topic> Topics = GetTopicListFromTopicInfoList(recordset);
            return Topics;
        }

        public static bool IsTopicPAChildAbuse(string Course_Number) 
        {
            return Course_Number == "60213" || Course_Number == "CMEZ60213" || Course_Number == "SW60213" || Course_Number == "PSY60213" ||
                           Course_Number == "AAQL6213" || Course_Number == "OT60213" || Course_Number == "PT60213" || Course_Number == "MT60213" || Course_Number == "0513-0000-15-177-H01-P" ||
                           Course_Number == "AT60213" || Course_Number == "RT60213";
        }

        public static bool ISTopicInDomain(int TopicId, int DomainId)
        {
            return SiteProvider.PR2.ISTopicInDomain(TopicId, DomainId);
        }

        public static bool ISTopicInAhDomain(int TopicId, int ahDomainId)
        {
            return SiteProvider.PR2.ISTopicInAhDomain(TopicId, ahDomainId);
        }

        public static bool isCMECourse(int topicID)
        {
            //TopicCMEAudit cme = TopicCMEAudit.GetTopicCMEAuditByID(topicID);
            Topic topic = Topic.GetTopicByID(topicID);
            bool isCMEJointSponsored = topic.CMESponsorInd;
            return (isCMEJointSponsored);
        }
        public string CleanTopicName()
        {

            return SiteProvider.PR2.CleanTopicName(this.TopicName);
        }
        #region MicroSite Methods
        public static DataSet GetTopicsByMicroSiteDomainId(int DomainId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            // Provide default Sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Topic.TopicName, Topic.Subtitle";
            DataSet Topics = null;
            string key = "GetTopicsByMSDomain_" + DomainId.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (DataSet)BizObject.Cache[key];
            }
            else
            {
                Topics = SiteProvider.PR2.GetTopicsByMicroSiteDomainId(DomainId, cSortExpression);
            }
            return Topics;

        }

        public static DataSet GetTopicsByMicroSiteDomainIdWithBundle(int DomainId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            // Provide default Sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Topic.TopicName, Topic.Subtitle";
            DataSet Topics = null;
            string key = "GetTopicsByMSDomain_" + DomainId.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (DataSet)BizObject.Cache[key];
            }
            else
            {
                Topics = SiteProvider.PR2.GetTopicsByMicroSiteDomainIdWithBundle(DomainId, cSortExpression);
            }
            return Topics;

        }

        public static DataSet GetMostPopularTopicsByMicroSiteDomainID(int domainID)
        {
            DataSet Topics = null;
            string key = "GetMostPopularTopicsByMSDomain_" + domainID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (DataSet)BizObject.Cache[key];
            }
            else
            {
                Topics = SiteProvider.PR2.GetMostPopularTopicsByMicroSiteDomainID(domainID);
            }
            return Topics;
        }

        public static DataSet GetSponsoredTopicsByDomainId(int domainID)
        {
            DataSet Topics = null;
            string key = "GetSponsoredTopicsByMSDomain_" + domainID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (DataSet)BizObject.Cache[key];
            }
            else
            {
                Topics = SiteProvider.PR2.GetSponsoredTopicsByDomainId(domainID);
            }
            return Topics;
        }
        public static DataSet GetMicrositeStateReqTopicsByDomainIDAndStateAbr(int domainID, string stateabr)
        {
            DataSet Topics = null;
            string key = "GetTopicsBySearchTextandDomainId_" + domainID.ToString() + stateabr;
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (DataSet)BizObject.Cache[key];
            }
            else
            {
                Topics = SiteProvider.PR2.GetMicrositeStateReqTopicsByDomainIDAndStateAbr(domainID, stateabr);
            }
            return Topics;
        }

        /// <summary>
        /// Returns course objective
        /// </summary>
        public static string GetCourseObjectiveByTopicID(int topicid)
        {
            return SiteProvider.PR2.GetCourseObjectiveByTopicID(topicid);
        }


        public static DataSet GetMicrositeStateReqTopicsByDomainIDAndStateAbrWithBundleFirst(int domainID, string stateabr)
        {
            DataSet Topics = null;
            string key = "GetTopicsBySearchTextandDomainId_" + domainID.ToString() + stateabr;
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (DataSet)BizObject.Cache[key];
            }
            else
            {
                Topics = SiteProvider.PR2.GetMicrositeStateReqTopicsByDomainIDAndStateAbrWithBundleFirst(domainID, stateabr);
            }
            return Topics;
        }


        public static DataSet GetTopicsBySearchFilters(string msid, int categoryID, string mediaType, string releaseOrUpdateDate,
            string searchText, string sortExpression)
        {
            if (sortExpression == null)
                sortExpression = "";
            // Provide default Sort
            if (sortExpression.Length == 0)
                sortExpression = "TopicName";

            if (String.IsNullOrEmpty(searchText))
                searchText = "";
            else
                searchText = searchText.Replace("'", "''").Trim();

            if (mediaType == null)
                mediaType = "";

            if (releaseOrUpdateDate == null)
                releaseOrUpdateDate = "";

            //List<Topic> Topics = null;
            DataSet Topics = null;
            string key = "GetTopicsBySearchFilters_" + searchText + msid.ToString() +
                categoryID.ToString() + mediaType + releaseOrUpdateDate.ToString() + sortExpression;
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (DataSet)BizObject.Cache[key];
            }
            else
            {
                Topics = SiteProvider.PR2.GetTopicsBySearchFilters(
                    msid, categoryID, mediaType, releaseOrUpdateDate, searchText, sortExpression);
                //Topics = GetTopicListFromTopicInfoList(recordset);
            }
            return Topics;
        }

        public static DataSet GetTopicCountsBySearchFilters(string msid, int categoryID, string mediaType,
            string releaseOrUpdateDate, string searchText)
        {
            if (String.IsNullOrEmpty(searchText))
                searchText = "";
            else
                searchText = searchText.Replace("'", "''").Trim();

            if (mediaType == null)
                mediaType = "";

            if (releaseOrUpdateDate == null)
                releaseOrUpdateDate = "";

            DataSet TopicCounts = null;
            string key = "GetTopicCountsBySearchFilters_" + searchText +
                categoryID.ToString() + mediaType + releaseOrUpdateDate.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicCounts = (DataSet)BizObject.Cache[key];
            }
            else
            {
                TopicCounts = SiteProvider.PR2.GetTopicCountsBySearchFilters(
                    msid, categoryID, mediaType, releaseOrUpdateDate, searchText);
            }
            return TopicCounts;
        }

        public static DataTable GetStateMandatedTopicsByDomainId(int DomainId, string cSortExpression)
        {
            return SiteProvider.PR2.GetStateMandatedTopicsByDomainId(DomainId, cSortExpression);
        }

        public static bool CheckPharmacistTopicByTopicId(int TopicID)
        {
            return SiteProvider.PR2.CheckPharmacistTopicByTopicId(TopicID);
        }

        #endregion



        #endregion
    }
}
