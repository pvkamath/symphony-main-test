﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace PearlsReview.Constants
{

    public static class PaypalAccConstants
    {
        public const string Vendor = "OnCourseCE";
        public const string Partner = "VeriSign";
        public const string User = "Website";
        public const string Pwd = "zcTxLpF7EKx7ZBX";
    }
    /// <summary>
    /// Summary description for Constants
    /// </summary>
    public static class TopicConstants
    {
        public const string TopicID = "TopicID";
        public const string QuestionID = "QID";
        public const string QuestionNumber = "QNumber";
        public const string HeaderNumber = "HNumber";
        public const string HeaderName = "HName";
        public const string Question = "Question";
        public const string Answers = "Answers";
        public const string AnsA = "AnsA";
        public const string AnsB = "AnsB";
        public const string AnsC = "AnsC";
        public const string AnsD = "AnsD";
        public const string AnsE = "AnsE";
        public const string AnsF = "AnsF";
        public const string CorrectAns = "CorrectAns";

    }
    public static class CommonConstants
    {
        public const String TestImagesPath = "/CourseImages/TestImages/";
        public const String TempImagesPath = "/CourseImages/TempImages/";
        public const String FacilityGroupLogosPath = "/CourseImages/FacilityGroupLogos/";
        public const String SEIUXMLPath = "~/XML/";
    }


    public static class ValidationConstants
    {
        public const int V_Pass = 0;
        public const int V_Fail = 1;
        public const int V_FileExists = 2;
        public const int V_NotImage = 3;
        public const int V_CancelUpload = 4;

        public static readonly Dictionary<int, String> ValidationMessages = new Dictionary<int, string>()
    {
        {V_Pass,"Image Uploaded Successfully"},
        {V_Fail,"Image Upload Failed"},
        {V_FileExists,"A file with that name already exists"},
        {V_NotImage,"Please Upload an Image file"},
        {V_CancelUpload,""}
    };
    }

    public static class UnlimitedCEConstants
    {
        public const double UCE_1Year = 44.95;
        public const double UCE_2Year = 79.99;
        public const double UCE_3Year = 109.85;
        public const double UCEPR_1Year = 88.95;
        public const double UCEPR_2Year = 149.00;
        public const double UCEPR_3Year = 199.00;

        public static List<UnlimiteCE> UnlimitedCEOptions()
        {
            List<UnlimiteCE> unlimitedCEOptions = new List<UnlimiteCE>();
            unlimitedCEOptions.Add(new UnlimiteCE { Description = "Unlimited CE Signup", Price = UCE_1Year, Id = 1, Comment ="One Year", UCEPrice=UCE_1Year });
            unlimitedCEOptions.Add(new UnlimiteCE { Description = "2 years: $79.99, Save $10! ", Price = UCE_2Year, Id = 2, Comment = "Two Year", UCEPrice=UCE_2Year });
            unlimitedCEOptions.Add(new UnlimiteCE { Description = "3 years: $109.85, Save $25! ", Price = UCE_3Year, Id = 3, Comment = "Three Year",UCEPrice=UCE_3Year });
            return unlimitedCEOptions;
        }

        public static List<UnlimiteCE> UnlimitedCEAndPROptions()
        {
            List<UnlimiteCE> unlimitedCEAndPROptions = new List<UnlimiteCE>();
            unlimitedCEAndPROptions.Add(new UnlimiteCE { Description = "Unlimited CE + Pearls Review Signup", Price = UCEPR_1Year, Id = 1 ,Comment ="One Year+PR", UCEPrice=UCE_1Year, PRPrice=44});
            unlimitedCEAndPROptions.Add(new UnlimiteCE { Description = "2 years:  UCE + PR =$149, Save $78! ", Price = UCEPR_2Year, Id = 2,Comment = "Two Year+PR" , UCEPrice=UCE_2Year, PRPrice=20});
            unlimitedCEAndPROptions.Add(new UnlimiteCE { Description = "3 years: UCE + PR = $199, Save $142! ", Price = UCEPR_3Year, Id = 3, Comment = "Three Year+PR", UCEPrice=UCE_3Year, PRPrice=30});
            return unlimitedCEAndPROptions;
        }
    }

  public class UnlimiteCE{
           public string Description { get; set;}
           public double Price { get; set;}
           public int Id { get; set;}
           public string Comment { get; set; }
           public double UCEPrice { get; set; }
           public double PRPrice { get; set; }
        }      


    public static class ReportConstants
    {
        public const int TotalSales = 3;

        public const int CERetail_Unlimited = 1;
        public const int CERetail_Online = 2;
        public const int CERetail_Renew = 4;
        public const int CERetail_NewMember = 5;

        public const int Microsite_CEPRO = 6;
        public const int Microsite_Online = 7;
        public const int Microsite_CEPRORenew = 8;
        public const int Microsite_CEPRONewMember = 9;
        public const int Microsite_TodayinPT = 10;
        public const int Microsite_TodayinOT = 11;

        public static readonly Dictionary<int, String> ChartLegendLabels = new Dictionary<int, string>()
    {
        {CERetail_Unlimited,"UCESales"},
        {CERetail_Online,"CE.com OnlineSales"},
        {CERetail_Renew,"RenewSales"},
        {CERetail_NewMember,"NewMemberSales"},
        {TotalSales,"CE.com Total Sales"},
        {Microsite_CEPRO,"CEProSales"},
        {Microsite_Online,"OnlineSales"},
        {Microsite_CEPRONewMember,"CEProNewMember"},
        {Microsite_CEPRORenew,"CEProRenew"},
        {Microsite_TodayinPT,"TodayinPT"},
        {Microsite_TodayinOT,"TodayinOT"}
    };

        public static readonly Dictionary<int, String> Titles = new Dictionary<int, string>()
    {
        {CERetail_Unlimited,"Unlimited Membership Sales"},
        {CERetail_Online,"CE.com Online Sales"},
        {CERetail_Renew,"Renew Membership Sales"},
        {CERetail_NewMember,"New Membership Sales"},
        {TotalSales,"CE.com Total Sales"},
        {Microsite_CEPRO,"CEPRO Sales"},
        {Microsite_Online,"CE.com Online Sales"},
        {Microsite_CEPRONewMember,"CEPRO New Member"},
        {Microsite_CEPRORenew,"CEPRO Renew"},
        {Microsite_TodayinPT,"Today in PT"},
        {Microsite_TodayinOT,"Today in OT"}
    };  
       
       
    }

}
