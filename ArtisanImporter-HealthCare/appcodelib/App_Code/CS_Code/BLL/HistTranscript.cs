﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr1
    /// </summary>
    
    public class HistTranscript : BasePR
    {
        
        #region variables and properties
        public HistTranscript()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public HistTranscript(int TranscriptID, int ItemID, int Score, DateTime DateComplete, string FirstName,
            string LastName, string RnState, string RnNum, string RnCode, string RnState2, string RnNum2, string RnCode2,
            string CourseName, string CourseNum, Decimal Credits)
        {
            this.TranscriptID = TranscriptID;
            this.ItemID = ItemID;
            this.Score = Score;
            this.DateComplete = DateComplete;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.RnState = RnState;
            this.RnNum = RnNum;
            this.RnCode = RnCode;
            this.RnState2 = RnState2;
            this.RnNum2 = RnNum2;
            this.RnCode2 = RnCode2;
            this.CourseName = CourseName;
            this.CourseNum = CourseNum;
            this.Credits = Credits;

        }
        private int _TranscriptID = 0;
        public int TranscriptID
        {
            get { return _TranscriptID; }
            set { _TranscriptID = value; }
        }

        private int _ItemID = 0;
        public int ItemID
        {
            get { return _ItemID; }
            set { _ItemID = value; }
        }

        private int _Score = 0;
        public int Score
        {
            get { return _Score; }
            set { _Score = value; }
        }

        private DateTime _DateComplete = System.DateTime.MinValue;
        public DateTime DateComplete
        {
            get { return _DateComplete; }
            set { _DateComplete = value; }
        }

        private string _FirstName = "";
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }

        private string _LastName = "";
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        private string _RnState = "";
        public string RnState
        {
            get { return _RnState; }
            set { _RnState = value; }
        }

        private string _RnNum = "";
        public string RnNum
        {
            get { return _RnNum; }
            set { _RnNum = value; }
        }

        private string _RnCode = "";
        public string RnCode
        {
            get { return _RnCode; }
            set { _RnCode = value; }
        }

        private string _RnState2 = "";
        public string RnState2
        {
            get { return _RnState2; }
            set { _RnState2 = value; }
        }

        private string _RnNum2 = "";
        public string RnNum2
        {
            get { return _RnNum2; }
            set { _RnNum2 = value; }
        }

        private string _RnCode2 = "";
        public string RnCode2
        {
            get { return _RnCode2; }
            set { _RnCode2 = value; }
        }

        private string _CourseName = "";
        public string CourseName
        {
            get { return _CourseName; }
            set { _CourseName = value; }
        }

        private string _CourseNum = "";
        public string CourseNum
        {
            get { return _CourseNum; }
            set { _CourseNum = value; }

        }

        private decimal _Credits = 0;
        public decimal Credits
        {
            get { return _Credits; }
            set { _Credits = value; }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Returns a HistTranscript object with the HistTranscript ID
        /// </summary>
        public static HistTranscript GetHistTranscriptByID(int Histtransid)
        {
            HistTranscript HistTranscript = null;
            string key = "HistTranscripts_HistTranscript_" + Histtransid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                HistTranscript = (HistTranscript)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                HistTranscriptInfo recordset = SiteProvider.PR2.GetHistTranscriptByID(Histtransid);
                HistTranscript = GetHistTranscriptFromHistTranscriptInfo(recordset);
                BasePR.CacheData(key, HistTranscript);
            }
            return HistTranscript;
        }


        /// <summary>
        /// Returns a HistTranscript object with the HistTranscript ID
        /// </summary>
        public static List<HistTranscript> GetHistTranscriptByFirstLastName(string fname, string lname)
        {
            List<HistTranscript> HistTranscript = new List<HistTranscript>();
            string key = "HistTranscripts_HistTranscript_" + fname + lname;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                HistTranscript = (List<HistTranscript>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<HistTranscriptInfo> recordset = SiteProvider.PR2.GetHistTranscriptByFirstLastName(fname, lname);
                foreach (HistTranscriptInfo histInfo in recordset)
                {
                    HistTranscript.Add(GetHistTranscriptFromHistTranscriptInfo(histInfo));

                }
                BasePR.CacheData(key, HistTranscript);
            }
            return HistTranscript;
        }

        /// <summary>
        /// Returns a HistTranscript object filled with the data taken from the input HisTranscriptInfo
        /// </summary>
        private static HistTranscript GetHistTranscriptFromHistTranscriptInfo(HistTranscriptInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new HistTranscript(record.TranscriptID, record.ItemID, record.Score, record.DateComplete,
                    record.FirstName, record.LastName, record.RnState, record.RnNum, record.RnCode, record.RnState2, record.RnNum2,
                    record.RnCode2, record.CourseName, record.CourseNum, record.Credits);
            }
        }

        public static bool UpdateHistTranscript(HistTranscriptInfo record)
        {
            HistTranscriptInfo hist = new HistTranscriptInfo(record.TranscriptID, record.ItemID, record.Score, record.DateComplete,
                    record.FirstName, record.LastName, record.RnState, record.RnNum, record.RnCode, record.RnState2, record.RnNum2,
                    record.RnCode2, record.CourseName, record.CourseNum, record.Credits);

            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            SiteProvider.PR2.UpdateHistTranscript(hist);
            return false;
        }


        public static List<HistTranscript> GetHistTranscriptRecords(string cSortExpression, int TranscriptID, int ItemID, int Score, DateTime DateComplete,
            string FirstName, string LastName, string RNState, string RNNum, string RNCode, string RNState2, string RNNum2, string RNCode2, string CourseName,
            string CourseNum, decimal Credits)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            if (cSortExpression.Length == 0)
                cSortExpression = "Lastname";
            List<HistTranscript> HistTranscripts = null;
            string key = "HistTranscripts_HistTranscriptsBySearch" + "_" + FirstName + "_" + LastName + "_" + RNState + "_" + RNNum +
                "_" + RNCode + "_" + RNState2 + "_" + RNNum2 + "_" + RNCode2 + "_" + CourseName + "_" + Credits;
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                HistTranscripts = (List<HistTranscript>)BizObject.Cache[key];
            }
            else
            {
                
                List<HistTranscriptInfo> Histtrans = SiteProvider.PR2.GetHistTranscriptRecords(cSortExpression, TranscriptID, ItemID, Score, DateComplete,
                FirstName, LastName, RNState, RNNum, RNCode, RNState2, RNNum2, RNCode2, CourseName, CourseNum, Credits);
                HistTranscripts = GetHistTranscriptListFromHistTranscriptInfoList(Histtrans);
                BasePR.CacheData(key, HistTranscripts);
            }
            return HistTranscripts;
        }


        private static List<HistTranscript> GetHistTranscriptListFromHistTranscriptInfoList(List<HistTranscriptInfo> recordset)
        {
            List<HistTranscript> HistTranscripts = new List<HistTranscript>();
            foreach (HistTranscriptInfo record in recordset)
                HistTranscripts.Add(GetHistTranscriptFromHistTranscriptInfo(record));
            return HistTranscripts;
        }

 #endregion
    }
   
}
