﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

/// <summary>
/// Summary description for ProHistory
/// </summary>

namespace PearlsReview.BLL
{
    public class ProHistory : BasePR
    {
        public ProHistory(int phid, int userid, int memberid, DateTime logdate, string historytype, string comment,DateTime lastupdate)
        {
            this.Phid = phid;
            this.Memberid = memberid;
            this.Userid = userid;
            this.Logdate = logdate;
            this.Historytype = historytype;
            this.Comment = comment;
            this.Lastupdate = lastupdate;
        }
        #region Variables and Properties
        private int _phid = 0;
        public int Phid
        {
            get { return _phid; }
            set { _phid = value; }
        }
        private int _memberid = 0;
        public int Memberid
        {
            get { return _memberid; }
            set { _memberid = value; }
        }
        private int _userid = 0;
        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }
        private DateTime _logdate = System.DateTime.MinValue;
        public DateTime Logdate
        {
            get { return _logdate; }
            set { _logdate = value; }
        }
        private string _historytype;
        public string Historytype
        {
            get { return _historytype; }
            set { _historytype = value; }
        }
        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }
        private DateTime _lastupdate = System.DateTime.MinValue;
        public DateTime Lastupdate
        {
            get { return _lastupdate; }
            set { _lastupdate = value; }
        }
        #endregion Variables and Properties

        #region Methods
        public static int insertProHistory(int userid, int memberid, DateTime logdate, string historytype, string comment, DateTime lastupdate)
        {
            ProHistoryInfo record = new ProHistoryInfo(0, userid, memberid, logdate, historytype, comment, lastupdate);

            int ret = SiteProvider.PR2.insertProHistory(record);
            BizObject.PurgeCacheItems("insertProHistory_Insert");
            return ret;
        }

        public int insert()
        {
            return insertProHistory(this.Userid, this.Memberid, this.Logdate, this.Historytype, this.Comment, this.Lastupdate);
        }

        public static List<ProHistory> GetProHistoryByMemberID(int MemberID)
        {
            List<ProHistory> ProHistory = null;
            string key = "ProHistoryInfo_" + MemberID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ProHistory = (List<ProHistory>)BizObject.Cache[key];
            }
            else
            {
                ProHistory = GetProHistoryListFromProHistoryInfoList(SiteProvider.PR2.GetProHistoryByMemberID(MemberID));
                BasePR.CacheData(key, ProHistory);
            }
            return ProHistory;
        }
        public static List<ProHistory> GetProHistoryByUserID(int UserID)
        {
            List<ProHistory> ProHistory = null;
            string key = "ProHistoryInfo_" + UserID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ProHistory = (List<ProHistory>)BizObject.Cache[key];
            }
            else
            {
                ProHistory = GetProHistoryListFromProHistoryInfoList(SiteProvider.PR2.GetProHistoryByUserID(UserID));
                BasePR.CacheData(key, ProHistory);
            }
            return ProHistory;
        }

        private static ProHistory GetProHistoryFromProHistoryInfo(ProHistoryInfo ProHistoryinfo)
        {
            if (ProHistoryinfo == null)
                return null;

            ProHistory ProHistory = new ProHistory(ProHistoryinfo.Phid, ProHistoryinfo.Userid, ProHistoryinfo.Memberid, ProHistoryinfo.Logdate,
                ProHistoryinfo.Historytype, ProHistoryinfo.Comment, ProHistoryinfo.Lastupdate);

            return ProHistory;
        }

        private static List<ProHistory> GetProHistoryListFromProHistoryInfoList(List<ProHistoryInfo> recordset)
        {
            List<ProHistory> ProHistory = new List<ProHistory>();
            foreach (ProHistoryInfo record in recordset)
                ProHistory.Add(GetProHistoryFromProHistoryInfo(record));
            return ProHistory;
        }
        #endregion Methods
    }
}
