﻿/*************************************************************************
 * Gannett Heatlh Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for CategoryAreaLink
    /// </summary>
    public class CategoryAreaLink : BasePR
    {
        #region Variables and Properties

        private int _areaid = 0;
        public int areaid
        {
            get { return _areaid; }
            protected set { _areaid = value; }
        }

        private int _categoryid = 0;
        public int categoryid
        {
            get { return _categoryid; }
            protected set { _categoryid = value; }
        }

        public CategoryAreaLink(int categoryid, int areaid)
        {
            this.areaid = areaid;
            this.categoryid = categoryid;
        }

        #endregion

        #region Methods       
        public static List<CategoryAreaLink> GetCategoryAreaLinkByCategoryId(int Categoryid)
        {
            List<CategoryAreaLink> CategoryAreaLinks = null;

            List<CategoryAreaLinkInfo> recordset = SiteProvider.PR2.GetCategoryAreaLinkByCategoryId(Categoryid);
            CategoryAreaLinks = GetCategoryAreaLinkListFromGetCategoryAreaLinkInfoList(recordset);
            return CategoryAreaLinks;

        }


        private static List<CategoryAreaLink> GetCategoryAreaLinkListFromGetCategoryAreaLinkInfoList(List<CategoryAreaLinkInfo> recordset)
        {
            List<CategoryAreaLink> CategoryAreaLinks = new List<CategoryAreaLink>();
            foreach (CategoryAreaLinkInfo record in recordset)
                CategoryAreaLinks.Add(GetCategoryAreaLinkFromCategoryAreaLink(record));
            return CategoryAreaLinks;
        }

        private static CategoryAreaLink GetCategoryAreaLinkFromCategoryAreaLink(CategoryAreaLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new CategoryAreaLink(record.categoryid, record.areaid);
            }
        }

        public static int UpdateCategoryAreaLink(int Categoryid, int areaid)
        {
            CategoryAreaLinkInfo record = new CategoryAreaLinkInfo(Categoryid, areaid);
            int ret = SiteProvider.PR2.UpdateCategoryAreaLink(record);
            BizObject.PurgeCacheItems("CategoryAreaLinks_CategoryAreaLink_" + Categoryid.ToString());
            BizObject.PurgeCacheItems("CategoryAreaLinks_CategoryAreaLinks");
            return ret;
        }


        public static bool DeleteCategoryAreaLink(int Categoryid, int areaid)
        {

            bool ret = SiteProvider.PR2.DeleteCategoryAreaLink(Categoryid, areaid);
            BizObject.PurgeCacheItems("CategoryAreaLinks_CategoryAreaLink");
            return ret;
        }
        #endregion
    }
}
