﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

    /// <summary>
    /// Summary description for TopicDetail
    /// </summary>
    public class TopicDetail: BasePR
    {
        #region Variables and Properties

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            protected set { _TopicID = value; }
        }

        private string _Type = "";
        public string Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        private string _Data = "";
        public string Data
        {
            get { return _Data; }
            set { _Data = value; }
        }

        private string _CorrectAns = "";
        public string CorrectAns
        {
            get { return _CorrectAns; }
            set { _CorrectAns = value; }
        }

        private int _Order = 0;
        public int Order
        {
            get { return _Order; }
            set { _Order = value; }
        }

        private string _Discussion = "";
        public string Discussion
        {
            get { return _Discussion; }
            set { _Discussion = value; }
        }

        public TopicDetail(int TopicID, string Type, string Data,
            string CorrectAns, int Order, string Discussion)
        {
            this.TopicID = TopicID;
            this.Type = Type;
            this.Data = Data;
            this.CorrectAns = CorrectAns;
            this.Order = Order;
            this.Discussion = Discussion;
        }

        //public bool Delete()
        //{
        //    bool success = TopicDetail.DeleteTopicDetail(this.TopicID);
        //    if (success)
        //        this.TopicID = 0;
        //    return success;
        //}

        //public bool Update()
        //{
        //    return Topic.UpdateTopic(this.TopicID, this.Type, this.Data,
        //   this.CorrectAns, this.Order, this.Discussion);
        //}

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Topics
        /// </summary>
        public static List<TopicDetail> GetTopicDetailsByTopicID(int TopicID)
        {
            List<TopicDetail> TopicDetails = null;
            string key = "TopicDetails_TopicDetails_" + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicDetails = (List<TopicDetail>)BizObject.Cache[key];
            }
            else
            {
                List<TopicDetailInfo> recordset = SiteProvider.PR2.GetTopicDetailsByTopicID(TopicID);
                TopicDetails = GetTopicDetailListFromTopicDetailInfoList(recordset);
                BasePR.CacheData(key, TopicDetails);
            }
            return TopicDetails;
        }


        /// <summary>
        /// Returns the number of total Topics
        /// </summary>
        public static int GetTopicDetailCount()
        {
            int TopicDetailCount = 0;
            string key = "TopicDetails_TopicDetailCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicDetailCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicDetailCount = SiteProvider.PR2.GetTopicDetailCount();
                BasePR.CacheData(key, TopicDetailCount);
            }
            return TopicDetailCount;
        }

        /// <summary>
        /// Returns the number of total TopicDetails for a TopicID
        /// </summary>
        public static int GetTopicDetailCount(int TopicID)
        {
            int TopicDetailCount = 0;
            string key = "TopicDetails_TopicDetailCount_" + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicDetailCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicDetailCount = SiteProvider.PR2.GetTopicDetailCountByTopicID(TopicID);
                BasePR.CacheData(key, TopicDetailCount);
            }
            return TopicDetailCount;
        }


        ///// <summary>
        ///// Updates an existing Topic
        ///// </summary>
        //public static bool UpdateTopic(int TopicID, string TopicName, string HTML,
        //   string Questions, string Answers, int Version, bool CorLecture,
        //   string Fla_CEType, int Fla_IDNo)
        //{
        //    TopicName = BizObject.ConvertNullToEmptyString(TopicName);
        //    HTML = BizObject.ConvertNullToEmptyString(HTML);
        //    Questions = BizObject.ConvertNullToEmptyString(Questions);
        //    Answers = BizObject.ConvertNullToEmptyString(Answers);
        //    Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);

        //    TopicInfo record = new TopicInfo(TopicID, TopicName, HTML,
        //   Questions, Answers, Version, CorLecture,
        //   Fla_CEType, Fla_IDNo);
        //    bool ret = SiteProvider.PR.UpdateTopic(record);

        //    BizObject.PurgeCacheItems("Topics_Topic_" + iID.ToString());
        //    BizObject.PurgeCacheItems("Topics_Topics");
        //    return ret;
        //}

        ///// <summary>
        ///// Creates a new Topic
        ///// </summary>
        //public static int InsertTopic(string TopicName, string HTML,
        //   string Questions, string Answers, int Version, bool CorLecture,
        //   string Fla_CEType, int Fla_IDNo)
        //{
        //    TopicName = BizObject.ConvertNullToEmptyString(TopicName);
        //    HTML = BizObject.ConvertNullToEmptyString(HTML);
        //    Questions = BizObject.ConvertNullToEmptyString(Questions);
        //    Answers = BizObject.ConvertNullToEmptyString(Answers);
        //    Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);

        //    TopicInfo record = new TopicInfo(0, TopicName, HTML,
        //   Questions, Answers, Version, CorLecture,
        //   Fla_CEType, Fla_IDNo);
        //    int ret = SiteProvider.PR.InsertTopic(record);

        //    BizObject.PurgeCacheItems("Topics_Topic");
        //    return ret;
        //}

        /// <summary>
        /// Deletes all existing TopicDetail records for a TopicID
        /// </summary>
        public static bool DeleteTopicDetailsByTopicID(int TopicID)
        {
            bool ret = SiteProvider.PR2.DeleteTopicDetailsByTopicID(TopicID);
            //         new RecordDeletedEvent("Topic", iID, null).Raise();
            BizObject.PurgeCacheItems("TopicDetails_TopicDetail");
            return ret;
        }

        /// <summary>
        /// Insert a batch of TopicDetail records from a List
        /// </summary>
        public static bool InsertTopicDetailsFromTopicDetailList(
          List<TopicDetailInfo> TopicDetails)
        {

            bool ret = false;

            // check the count to be sure it's 34 records
            if (TopicDetails.Count == 34)
            {

                int TopicID = TopicDetails[0].TopicID;

                // delete all existing records for this TopicID
                bool delsuccess = DeleteTopicDetailsByTopicID(TopicID);

                // insert all new records
                ret = SiteProvider.PR2.InsertTopicDetailsFromTopicDetailList(
                  TopicDetails);

                ////////////////////////
                // not sure about this purge -- need to check it out
                BizObject.PurgeCacheItems("TopicDetails_TopicDetail");
                ////////////////////////
            }

            return ret;

        }

        /// <summary>
        /// Returns a TopicDetail object filled with the data taken from the input TopicDetailInfo
        /// </summary>
        private static TopicDetail GetTopicDetailFromTopicDetailInfo(TopicDetailInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new TopicDetail(record.TopicID, record.Type, record.Data,
           record.CorrectAns, record.Order, record.Discussion);
            }
        }

        /// <summary>
        /// Returns a list of TopicDetail objects filled with the data taken from the input list of TopicDetailInfo
        /// </summary>
        private static List<TopicDetail> GetTopicDetailListFromTopicDetailInfoList(List<TopicDetailInfo> recordset)
        {
            List<TopicDetail> TopicDetails = new List<TopicDetail>();
            foreach (TopicDetailInfo record in recordset)
                TopicDetails.Add(GetTopicDetailFromTopicDetailInfo(record));
            return TopicDetails;
        }        

        #endregion
    }
}
