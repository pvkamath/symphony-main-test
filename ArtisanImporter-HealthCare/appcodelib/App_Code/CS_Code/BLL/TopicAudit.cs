﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for TopicAudit
/// </summary>
    public class TopicAudit : BasePR
    {
        #region Variables and Properties
        ////////////////////////////////////////////////////////////  

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private string _plan_comm = "";
        public string plan_comm
        {
            get { return _plan_comm; }
            set { _plan_comm = value; }
        }

        private string _dates_pub = "";
        public string dates_pub
        {
            get { return _dates_pub; }
            set { _dates_pub = value; }
        }

        private bool _media_learner_ind = true;
        public bool media_learner_ind
        {
            get { return _media_learner_ind; }
            set { _media_learner_ind = value; }
        }

        private bool _media_provider_ind = false;
        public bool media_provider_ind
        {
            get { return _media_provider_ind; }
            set { _media_provider_ind = value; }
        }

        private string _media_other = "";
        public string media_other
        {
            get { return _media_other; }
            set { _media_other = value; }
        }

        private string _goal_purpose = "";
        public string goal_purpose
        {
            get { return _goal_purpose; }
            set { _goal_purpose = value; }
        }

        private string _lead_nurse_planner = "";
        public string lead_nurse_planner
        {
            get { return _lead_nurse_planner; }
            set { _lead_nurse_planner = value; }
        }

        private string _former_planners = "";
        public string former_planners
        {
            get { return _former_planners; }
            set { _former_planners = value; }
        }

        private string _target_audience = "";
        public string target_audience
        {
            get { return _target_audience; }
            set { _target_audience = value; }
        }

        private bool _nau_feedback_ind = true;
        public bool nau_feedback_ind
        {
            get { return _nau_feedback_ind; }
            set { _nau_feedback_ind = value; }
        }

        private bool _nau_survey_ind = true;
        public bool nau_survey_ind
        {
            get { return _nau_survey_ind; }
            set { _nau_survey_ind = value; }
        }

        private bool _nau_advances_ind = true;
        public bool nau_advances_ind
        {
            get { return _nau_advances_ind; }
            set { _nau_advances_ind = value; }
        }

        private bool _nau_emerging_ind = true;
        public bool nau_emerging_ind
        {
            get { return _nau_emerging_ind; }
            set { _nau_emerging_ind = value; }
        }

        private bool _nau_deficits_ind = true;
        public bool nau_deficits_ind
        {
            get { return _nau_deficits_ind; }
            set { _nau_deficits_ind = value; }
        }

        private string _nau_other = "";
        public string nau_other
        {
            get { return _nau_other; }
            set { _nau_other = value; }
        }

        private string _na_findings = "";
        public string na_findings
        {
            get { return _na_findings; }
            set { _na_findings = value; }
        }


        private string _comm_support = "";
        public string comm_support
        {
            get { return _comm_support; }
            set { _comm_support = value; }
        }

        private string _eval_method = "";
        public string eval_method
        {
            get { return _eval_method; }
            set { _eval_method = value; }
        }

        private string _eval_tools = "";
        public string eval_tools
        {
            get { return _eval_tools; }
            set { _eval_tools = value; }
        }

        private bool _eval_cat_sat_ind = true;
        public bool eval_cat_sat_ind
        {
            get { return _eval_cat_sat_ind; }
            set { _eval_cat_sat_ind = value; }
        }

        private bool _eval_cat_know_ind = true;
        public bool eval_cat_know_ind
        {
            get { return _eval_cat_know_ind; }
            set { _eval_cat_know_ind = value; }
        }

        private bool _eval_cat_skill_ind = true;
        public bool eval_cat_skill_ind
        {
            get { return _eval_cat_skill_ind; }
            set { _eval_cat_skill_ind = value; }
        }

        private bool _eval_cat_practice_ind = true;
        public bool eval_cat_practice_ind
        {
            get { return _eval_cat_practice_ind; }
            set { _eval_cat_practice_ind = value; }
        }

        private bool _eval_cat_rel_ind = true;
        public bool eval_cat_rel_ind
        {
            get { return _eval_cat_rel_ind; }
            set { _eval_cat_rel_ind = value; }
        }

        private string _eval_cat_other = "";
        public string eval_cat_other
        {
            get { return _eval_cat_other; }
            set { _eval_cat_other = value; }
        }

        private bool _eval_data_refine_ind = true;
        public bool eval_data_refine_ind
        {
            get { return _eval_data_refine_ind; }
            set { _eval_data_refine_ind = value; }
        }

        private bool _eval_data_create_ind = true;
        public bool eval_data_create_ind
        {
            get { return _eval_data_create_ind; }
            set { _eval_data_create_ind = value; }
        }

        private bool _eval_data_dis_ind = true;
        public bool eval_data_dis_ind
        {
            get { return _eval_data_dis_ind; }
            set { _eval_data_dis_ind = value; }
        }

        private string _eval_data_other = "";
        public string eval_data_other
        {
            get { return _eval_data_other; }
            set { _eval_data_other = value; }
        }

        private string _how_feedback = "";
        public string how_feedback
        {
            get { return _how_feedback; }
            set { _how_feedback = value; }
        }

        private string _verif_compl = "";
        public string verif_compl
        {
            get { return _verif_compl; }
            set { _verif_compl = value; }
        }

        private bool _crit_compl_form_ind = true;
        public bool crit_compl_form_ind
        {
            get { return _crit_compl_form_ind; }
            set { _crit_compl_form_ind = value; }
        }

        private bool _crit_compl_score_ind = true;
        public bool crit_compl_score_ind
        {
            get { return _crit_compl_score_ind; }
            set { _crit_compl_score_ind = value; }
        }

        private string _crit_compl_other = "";
        public string crit_compl_other
        {
            get { return _crit_compl_other; }
            set { _crit_compl_other = value; }
        }

        private string _doc_compl = "";
        public string doc_compl
        {
            get { return _doc_compl; }
            set { _doc_compl = value; }
        }

        private bool _discl_compl_ind = true;
        public bool discl_compl_ind
        {
            get { return _discl_compl_ind; }
            set { _discl_compl_ind = value; }
        }

        private bool _discl_conflicts_ind = true;
        public bool discl_conflicts_ind
        {
            get { return _discl_conflicts_ind; }
            set { _discl_conflicts_ind = value; }
        }

        private bool _discl_comm_ind = true;
        public bool discl_comm_ind
        {
            get { return _discl_comm_ind; }
            set { _discl_comm_ind = value; }
        }

        private bool _discl_products_ind = true;
        public bool discl_products_ind
        {
            get { return _discl_products_ind; }
            set { _discl_products_ind = value; }
        }

        private bool _discl_offlabel_ind = true;
        public bool discl_offlabel_ind
        {
            get { return _discl_offlabel_ind; }
            set { _discl_offlabel_ind = value; }
        }

        private bool _discl_bias_ind = true;
        public bool discl_bias_ind
        {
            get { return _discl_bias_ind; }
            set { _discl_bias_ind = value; }
        }

        private bool _discl_disability_ind = true;
        public bool discl_disability_ind
        {
            get { return _discl_disability_ind; }
            set { _discl_disability_ind = value; }
        }

        private string _discl_other = "";
        public string discl_other
        {
            get { return _discl_other; }
            set { _discl_other = value; }
        }

        private bool _contact_calc_pilot_ind = true;
        public bool contact_calc_pilot_ind
        {
            get { return _contact_calc_pilot_ind; }
            set { _contact_calc_pilot_ind = value; }
        }

        private bool _contact_calc_author_ind = true;
        public bool contact_calc_author_ind
        {
            get { return _contact_calc_author_ind; }
            set { _contact_calc_author_ind = value; }
        }

        private bool _contact_calc_exp_ind = true;
        public bool contact_calc_exp_ind
        {
            get { return _contact_calc_exp_ind; }
            set { _contact_calc_exp_ind = value; }
        }

        private bool _contact_calc_rev_ind = true;
        public bool contact_calc_rev_ind
        {
            get { return _contact_calc_rev_ind; }
            set { _contact_calc_rev_ind = value; }
        }

        private bool _contact_calc_peer_ind = true;
        public bool contact_calc_peer_ind
        {
            get { return _contact_calc_peer_ind; }
            set { _contact_calc_peer_ind = value; }
        }

        private string _contact_calc_other = "";
        public string contact_calc_other
        {
            get { return _contact_calc_other; }
            set { _contact_calc_other = value; }
        }

        private bool _adv_web_ind = true;
        public bool adv_web_ind
        {
            get { return _adv_web_ind; }
            set { _adv_web_ind = value; }
        }

        private bool _adv_flyers_ind = true;
        public bool adv_flyers_ind
        {
            get { return _adv_flyers_ind; }
            set { _adv_flyers_ind = value; }
        }

        private bool _adv_journals_ind = true;
        public bool adv_journals_ind
        {
            get { return _adv_journals_ind; }
            set { _adv_journals_ind = value; }
        }

        private bool _adv_accred_ind = true;
        public bool adv_accred_ind
        {
            get { return _adv_accred_ind; }
            set { _adv_accred_ind = value; }
        }

        private string _adv_other = "";
        public string adv_other
        {
            get { return _adv_other; }
            set { _adv_other = value; }
        }

        private string _record_system = "";
        public string record_system
        {
            get { return _record_system; }
            set { _record_system = value; }
        }

        private string _topic_notes = "";
        public string topic_notes
        {
            get { return _topic_notes; }
            set { _topic_notes = value; }
        }

        private string _off_label_use = "";
        public string off_label_use
        {
            get { return _off_label_use; }
            set { _off_label_use = value; }
        }

        private string _conflict_of_interest = "";
        public string conflict_of_interest
        {
            get { return _conflict_of_interest; }
            set { _conflict_of_interest = value; }
        }

        private string _gapAnalysis = "";
        public string GapAnalysis
        {
            get { return _gapAnalysis; }
            set { _gapAnalysis = value; }
        }

        private bool _coi_not_app_ind = false;
        public bool coi_not_app_ind
        {
            get { return _coi_not_app_ind; }
            private set { _coi_not_app_ind = value; }
        }

        private bool _coi_removed_ind = false;
        public bool coi_removed_ind
        {
            get { return _coi_removed_ind; }
            private set { _coi_removed_ind = value; }
        }

        private bool _coi_revised_ind = false;
        public bool coi_revised_ind
        {
            get { return _coi_revised_ind; }
            private set { _coi_revised_ind = value; }
        }

        private bool _coi_not_award_ind = false;
        public bool coi_not_award_ind
        {
            get { return _coi_not_award_ind; }
            private set { _coi_not_award_ind = value; }
        }

        private bool _coi_undertaking1_ind = false;
        public bool coi_undertaking1_ind
        {
            get { return _coi_undertaking1_ind; }
            private set { _coi_undertaking1_ind = value; }
        }

        private bool _coi_undertaking2_ind = false;
        public bool coi_undertaking2_ind
        {
            get { return _coi_undertaking2_ind; }
            private set { _coi_undertaking2_ind = value; }
        }

        private bool _core_comp_value_ind = false;
        public bool core_comp_value_ind
        {
            get { return _core_comp_value_ind; }
            private set { _core_comp_value_ind = value; }
        }

        private bool _core_comp_role_ind = false;
        public bool core_comp_role_ind
        {
            get { return _core_comp_role_ind; }
            private set { _core_comp_role_ind = value; }
        }

        private bool _core_comp_com_ind = false;
        public bool core_comp_com_ind
        {
            get { return _core_comp_com_ind; }
            private set { _core_comp_com_ind = value; }
        }

        private bool _core_comp_team_ind = false;
        public bool core_comp_team_ind
        {
            get { return _core_comp_team_ind; }
            private set { _core_comp_team_ind = value; }
        }

        private string _core_comp_other = "";
        public string core_comp_other
        {
            get { return _core_comp_other; }
            private set { _core_comp_other = value; }
        }

        public TopicAudit(int TopicID, string plan_comm, string dates_pub,
            bool media_learner_ind, bool media_provider_ind, string media_other, string goal_purpose,
            string lead_nurse_planner, string former_planners, string target_audience,
            bool nau_feedback_ind, bool nau_survey_ind, bool nau_advances_ind,
            bool nau_emerging_ind, bool nau_deficits_ind, string nau_other, string na_findings,
            string comm_support, string eval_method, string eval_tools,
            bool eval_cat_sat_ind, bool eval_cat_know_ind, bool eval_cat_skill_ind,
            bool eval_cat_practice_ind, bool eval_cat_rel_ind, string eval_cat_other,
            bool eval_data_refine_ind, bool eval_data_create_ind, bool eval_data_dis_ind,
            string eval_data_other, string how_feedback, string verif_compl,
            bool crit_compl_form_ind, bool crit_compl_score_ind, string crit_compl_other,
            string doc_compl, bool discl_compl_ind, bool discl_conflicts_ind,
            bool discl_comm_ind, bool discl_products_ind, bool discl_offlabel_ind,
            bool discl_bias_ind, bool discl_disability_ind, string discl_other,
            bool contact_calc_pilot_ind, bool contact_calc_author_ind,
            bool contact_calc_exp_ind, bool contact_calc_rev_ind, bool contact_calc_peer_ind,
            string contact_calc_other, bool adv_web_ind, bool adv_flyers_ind,
            bool adv_journals_ind, bool adv_accred_ind, string adv_other,
            string record_system, string topic_notes, string off_label_use, string conflict_of_interest, string gapAnalysis,
            bool coi_not_app_ind, bool coi_removed_ind, bool coi_revised_ind,
            bool coi_not_award_ind, bool coi_undertaking1_ind, bool coi_undertaking2_ind, bool core_comp_value_ind, bool core_comp_role_ind, bool core_comp_com_ind,bool
            core_comp_team_ind, string core_comp_other)
        {
            this.TopicID = TopicID;
            this.plan_comm = plan_comm;
            this.dates_pub = dates_pub;
            this.media_learner_ind = media_learner_ind;
            this.media_provider_ind = media_provider_ind;
            this.media_other = media_other;
            this.goal_purpose = goal_purpose;
            this.lead_nurse_planner = lead_nurse_planner;
            this.former_planners = former_planners;
            this.target_audience = target_audience;
            this.nau_feedback_ind = nau_feedback_ind;
            this.nau_survey_ind = nau_survey_ind;
            this.nau_advances_ind = nau_advances_ind;
            this.nau_emerging_ind = nau_emerging_ind;
            this.nau_deficits_ind = nau_deficits_ind;
            this.nau_other = nau_other;
            this.na_findings = na_findings;
            this.comm_support = comm_support;
            this.eval_method = eval_method;
            this.eval_tools = eval_tools;
            this.eval_cat_sat_ind = eval_cat_sat_ind;
            this.eval_cat_know_ind = eval_cat_know_ind;
            this.eval_cat_skill_ind = eval_cat_skill_ind;
            this.eval_cat_practice_ind = eval_cat_practice_ind;
            this.eval_cat_rel_ind = eval_cat_rel_ind;
            this.eval_cat_other = eval_cat_other;
            this.eval_data_refine_ind = eval_data_refine_ind;
            this.eval_data_create_ind = eval_data_create_ind;
            this.eval_data_dis_ind = eval_data_dis_ind;
            this.eval_data_other = eval_data_other;
            this.how_feedback = how_feedback;
            this.verif_compl = verif_compl;
            this.crit_compl_form_ind = crit_compl_form_ind;
            this.crit_compl_score_ind = crit_compl_score_ind;
            this.crit_compl_other = crit_compl_other;
            this.doc_compl = doc_compl;
            this.discl_compl_ind = discl_compl_ind;
            this.discl_conflicts_ind = discl_conflicts_ind;
            this.discl_comm_ind = discl_comm_ind;
            this.discl_products_ind = discl_products_ind;
            this.discl_offlabel_ind = discl_offlabel_ind;
            this.discl_bias_ind = discl_bias_ind;
            this.discl_disability_ind = discl_disability_ind;
            this.discl_other = discl_other;
            this.contact_calc_pilot_ind = contact_calc_pilot_ind;
            this.contact_calc_author_ind = contact_calc_author_ind;
            this.contact_calc_exp_ind = contact_calc_exp_ind;
            this.contact_calc_rev_ind = contact_calc_rev_ind;
            this.contact_calc_peer_ind = contact_calc_peer_ind;
            this.contact_calc_other = contact_calc_other;
            this.adv_web_ind = adv_web_ind;
            this.adv_flyers_ind = adv_flyers_ind;
            this.adv_journals_ind = adv_journals_ind;
            this.adv_accred_ind = adv_accred_ind;
            this.adv_other = adv_other;
            this.record_system = record_system;
            this.topic_notes = topic_notes;
            this.off_label_use = off_label_use;
            this.conflict_of_interest = conflict_of_interest;
            this.GapAnalysis = gapAnalysis;
            this.coi_not_app_ind = coi_not_app_ind;
            this.coi_removed_ind = coi_removed_ind;
            this.coi_revised_ind = coi_revised_ind;
            this.coi_not_award_ind = coi_not_award_ind;
            this.coi_undertaking1_ind = coi_undertaking1_ind;
            this.coi_undertaking2_ind = coi_undertaking2_ind;
            this.core_comp_value_ind = core_comp_value_ind;
            this.core_comp_role_ind = core_comp_role_ind;
            this.core_comp_com_ind = core_comp_com_ind;
            this.core_comp_team_ind = core_comp_team_ind;
            this.core_comp_other = core_comp_other;
        }

        public bool Update()
        {
            return TopicAudit.UpdateTopicAudit(this.TopicID, this.plan_comm, this.dates_pub,
                this.media_learner_ind, this.media_provider_ind, this.media_other, this.goal_purpose,
                this.lead_nurse_planner, this.former_planners, this.target_audience,
                this.nau_feedback_ind, this.nau_survey_ind, this.nau_advances_ind,
                this.nau_emerging_ind, this.nau_deficits_ind, this.nau_other,
                this.na_findings, this.comm_support, this.eval_method, this.eval_tools,
                this.eval_cat_sat_ind, this.eval_cat_know_ind, this.eval_cat_skill_ind,
                this.eval_cat_practice_ind, this.eval_cat_rel_ind, this.eval_cat_other,
                this.eval_data_refine_ind, this.eval_data_create_ind, this.eval_data_dis_ind,
                this.eval_data_other, this.how_feedback, this.verif_compl,
                this.crit_compl_form_ind, this.crit_compl_score_ind, this.crit_compl_other,
                this.doc_compl, this.discl_compl_ind, this.discl_conflicts_ind,
                this.discl_comm_ind, this.discl_products_ind, this.discl_offlabel_ind,
                this.discl_bias_ind, this.discl_disability_ind, this.discl_other,
                this.contact_calc_pilot_ind, this.contact_calc_author_ind,
                this.contact_calc_exp_ind, this.contact_calc_rev_ind, this.contact_calc_peer_ind,
                this.contact_calc_other, this.adv_web_ind, this.adv_flyers_ind,
                this.adv_journals_ind, this.adv_accred_ind, this.adv_other,
                this.record_system, this.topic_notes, this.off_label_use, this.conflict_of_interest, this.GapAnalysis, 
                this.coi_not_app_ind, this.coi_removed_ind, this.coi_revised_ind, this.coi_not_award_ind, 
                this.coi_undertaking1_ind, this.coi_undertaking2_ind,this.core_comp_value_ind,this.core_comp_role_ind,this.core_comp_com_ind,this.core_comp_team_ind,this.core_comp_other);
        }

        public bool Delete()
        {
            bool success = TopicAudit.DeleteTopicAudit(this.TopicID);
            if (success)
                this.TopicID = 0;
            return success;
        }

        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/


        /// <summary>
        /// Returns a collection with all TopicAudits, except obsolete ones
        /// </summary>
        public static List<TopicAudit> GetTopicAudits(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "TopicAuditName";

            List<TopicAudit> TopicAudits = null;
            string key = "TopicAudits_TopicAudits_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicAudits = (List<TopicAudit>)BizObject.Cache[key];
            }
            else
            {
                List<TopicAuditInfo> recordset = SiteProvider.PR2.GetTopicAudit(cSortExpression);
                TopicAudits = GetTopicAuditListFromTopicAuditInfoList(recordset);
                BasePR.CacheData(key, TopicAudits);
            }
            return TopicAudits;
        }

        /// <summary>
        /// Returns a TopicAudit object with the specified ID
        /// </summary>
        public static TopicAudit GetTopicAuditByID(int TopicID)
        {
            TopicAudit TopicAudit = null;
            string key = "TopicAudits_TopicAudit_" + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicAudit = (TopicAudit)BizObject.Cache[key];
            }
            else
            {
                TopicAudit = GetTopicAuditFromTopicAuditInfo(SiteProvider.PR2.GetTopicAuditByID(TopicID));
                BasePR.CacheData(key, TopicAudit);
            }
            return TopicAudit;
        }

        /// <summary>
        /// Creates a new TopicAudit
        /// </summary>
        // NOTE: FacilityID is not passed in as a parameter (forced to 0)
        public static int InsertTopicAudit(int TopicID, string plan_comm, string dates_pub,
           bool media_learner_ind, bool media_provider_ind, string media_other, string goal_purpose,
           string lead_nurse_planner, string former_planners, string target_audience,
           bool nau_feedback_ind, bool nau_survey_ind, bool nau_advances_ind,
           bool nau_emerging_ind, bool nau_deficits_ind, string nau_other, string na_findings,
           string comm_support, string eval_method, string eval_tools,
           bool eval_cat_sat_ind, bool eval_cat_know_ind, bool eval_cat_skill_ind,
           bool eval_cat_practice_ind, bool eval_cat_rel_ind, string eval_cat_other,
           bool eval_data_refine_ind, bool eval_data_create_ind, bool eval_data_dis_ind,
           string eval_data_other, string how_feedback, string verif_compl,
           bool crit_compl_form_ind, bool crit_compl_score_ind, string crit_compl_other,
           string doc_compl, bool discl_compl_ind, bool discl_conflicts_ind,
           bool discl_comm_ind, bool discl_products_ind, bool discl_offlabel_ind,
           bool discl_bias_ind, bool discl_disability_ind, string discl_other,
           bool contact_calc_pilot_ind, bool contact_calc_author_ind,
           bool contact_calc_exp_ind, bool contact_calc_rev_ind, bool contact_calc_peer_ind,
           string contact_calc_other, bool adv_web_ind, bool adv_flyers_ind,
           bool adv_journals_ind, bool adv_accred_ind, string adv_other,
           string record_system, string topic_notes, string off_label_use, string conflict_of_interest, string gapAnalysis,
            bool coi_not_app_ind, bool coi_removed_ind, bool coi_revised_ind,
            bool coi_not_award_ind, bool coi_undertaking1_ind, bool coi_undertaking2_ind,
            bool core_comp_value_ind, bool core_comp_role_ind, bool core_comp_com_ind, 
            bool core_comp_team_ind, string core_comp_other)
        {
            plan_comm = BizObject.ConvertNullToEmptyString(plan_comm);
            dates_pub = BizObject.ConvertNullToEmptyString(dates_pub);
            media_other = BizObject.ConvertNullToEmptyString(media_other);
            goal_purpose = BizObject.ConvertNullToEmptyString(goal_purpose);
            lead_nurse_planner = BizObject.ConvertNullToEmptyString(lead_nurse_planner);
            former_planners = BizObject.ConvertNullToEmptyString(former_planners);
            target_audience = BizObject.ConvertNullToEmptyString(target_audience);
            nau_other = BizObject.ConvertNullToEmptyString(nau_other);
            na_findings = BizObject.ConvertNullToEmptyString(na_findings);
            comm_support = BizObject.ConvertNullToEmptyString(comm_support);
            eval_method = BizObject.ConvertNullToEmptyString(eval_method);
            eval_tools = BizObject.ConvertNullToEmptyString(eval_tools);
            eval_cat_other = BizObject.ConvertNullToEmptyString(eval_cat_other);
            eval_data_other = BizObject.ConvertNullToEmptyString(eval_data_other);
            how_feedback = BizObject.ConvertNullToEmptyString(how_feedback);
            verif_compl = BizObject.ConvertNullToEmptyString(verif_compl);
            crit_compl_other = BizObject.ConvertNullToEmptyString(crit_compl_other);
            doc_compl = BizObject.ConvertNullToEmptyString(doc_compl);
            discl_other = BizObject.ConvertNullToEmptyString(discl_other);
            contact_calc_other = BizObject.ConvertNullToEmptyString(contact_calc_other);
            adv_other = BizObject.ConvertNullToEmptyString(adv_other);
            record_system = BizObject.ConvertNullToEmptyString(record_system);
            topic_notes = BizObject.ConvertNullToEmptyString(topic_notes);
            off_label_use = BizObject.ConvertNullToEmptyString(off_label_use);
            conflict_of_interest = BizObject.ConvertNullToEmptyString(conflict_of_interest);
            gapAnalysis = BizObject.ConvertNullToEmptyString(gapAnalysis);
            core_comp_other = BizObject.ConvertNullToEmptyString(core_comp_other);

            TopicAuditInfo record = new TopicAuditInfo(TopicID, plan_comm, dates_pub,
                media_learner_ind, media_provider_ind, media_other, goal_purpose,
                lead_nurse_planner, former_planners, target_audience,
                nau_feedback_ind, nau_survey_ind, nau_advances_ind,
                nau_emerging_ind, nau_deficits_ind, nau_other, na_findings,
                comm_support, eval_method, eval_tools,
                eval_cat_sat_ind, eval_cat_know_ind, eval_cat_skill_ind,
                eval_cat_practice_ind, eval_cat_rel_ind, eval_cat_other,
                eval_data_refine_ind, eval_data_create_ind, eval_data_dis_ind,
                eval_data_other, how_feedback, verif_compl,
                crit_compl_form_ind, crit_compl_score_ind, crit_compl_other,
                doc_compl, discl_compl_ind, discl_conflicts_ind,
                discl_comm_ind, discl_products_ind, discl_offlabel_ind,
                discl_bias_ind, discl_disability_ind, discl_other,
                contact_calc_pilot_ind, contact_calc_author_ind,
                contact_calc_exp_ind, contact_calc_rev_ind, contact_calc_peer_ind,
                contact_calc_other, adv_web_ind, adv_flyers_ind,
                adv_journals_ind, adv_accred_ind, adv_other,
                record_system, topic_notes, off_label_use, conflict_of_interest, gapAnalysis,
                coi_not_app_ind, coi_removed_ind, coi_revised_ind, coi_not_award_ind, coi_undertaking1_ind,
                coi_undertaking2_ind,core_comp_value_ind,core_comp_role_ind,core_comp_com_ind,core_comp_team_ind,
                core_comp_other);
            int ret = SiteProvider.PR2.InsertTopicAudit(record);

            BizObject.PurgeCacheItems("TopicAudits_TopicAudit");
            return ret;
        }

        /// <summary>
        /// Updates an existing TopicAudit
        /// </summary>
        public static bool UpdateTopicAudit(int TopicID, string plan_comm, string dates_pub,
           bool media_learner_ind, bool media_provider_ind, string media_other, string goal_purpose,
           string lead_nurse_planner, string former_planners, string target_audience,
           bool nau_feedback_ind, bool nau_survey_ind, bool nau_advances_ind,
           bool nau_emerging_ind, bool nau_deficits_ind, string nau_other, string na_findings,
           string comm_support, string eval_method, string eval_tools,
           bool eval_cat_sat_ind, bool eval_cat_know_ind, bool eval_cat_skill_ind,
           bool eval_cat_practice_ind, bool eval_cat_rel_ind, string eval_cat_other,
           bool eval_data_refine_ind, bool eval_data_create_ind, bool eval_data_dis_ind,
           string eval_data_other, string how_feedback, string verif_compl,
           bool crit_compl_form_ind, bool crit_compl_score_ind, string crit_compl_other,
           string doc_compl, bool discl_compl_ind, bool discl_conflicts_ind,
           bool discl_comm_ind, bool discl_products_ind, bool discl_offlabel_ind,
           bool discl_bias_ind, bool discl_disability_ind, string discl_other,
           bool contact_calc_pilot_ind, bool contact_calc_author_ind,
           bool contact_calc_exp_ind, bool contact_calc_rev_ind, bool contact_calc_peer_ind,
           string contact_calc_other, bool adv_web_ind, bool adv_flyers_ind,
           bool adv_journals_ind, bool adv_accred_ind, string adv_other,
           string record_system, string topic_notes, string off_label_use, string conflict_of_interest, string gapAnalysis,
            bool coi_not_app_ind, bool coi_removed_ind, bool coi_revised_ind,
            bool coi_not_award_ind, bool coi_undertaking1_ind, bool coi_undertaking2_ind, bool core_comp_value_ind, bool core_comp_role_ind, bool core_comp_com_ind, 
            bool core_comp_team_ind, string core_comp_other)
        {
            plan_comm = BizObject.ConvertNullToEmptyString(plan_comm);
            dates_pub = BizObject.ConvertNullToEmptyString(dates_pub);
            media_other = BizObject.ConvertNullToEmptyString(media_other);
            goal_purpose = BizObject.ConvertNullToEmptyString(goal_purpose);
            lead_nurse_planner = BizObject.ConvertNullToEmptyString(lead_nurse_planner);
            former_planners = BizObject.ConvertNullToEmptyString(former_planners);
            target_audience = BizObject.ConvertNullToEmptyString(target_audience);
            nau_other = BizObject.ConvertNullToEmptyString(nau_other);
            na_findings = BizObject.ConvertNullToEmptyString(na_findings);
            comm_support = BizObject.ConvertNullToEmptyString(comm_support);
            eval_method = BizObject.ConvertNullToEmptyString(eval_method);
            eval_tools = BizObject.ConvertNullToEmptyString(eval_tools);
            eval_cat_other = BizObject.ConvertNullToEmptyString(eval_cat_other);
            eval_data_other = BizObject.ConvertNullToEmptyString(eval_data_other);
            how_feedback = BizObject.ConvertNullToEmptyString(how_feedback);
            verif_compl = BizObject.ConvertNullToEmptyString(verif_compl);
            crit_compl_other = BizObject.ConvertNullToEmptyString(crit_compl_other);
            doc_compl = BizObject.ConvertNullToEmptyString(doc_compl);
            discl_other = BizObject.ConvertNullToEmptyString(discl_other);
            contact_calc_other = BizObject.ConvertNullToEmptyString(contact_calc_other);
            adv_other = BizObject.ConvertNullToEmptyString(adv_other);
            record_system = BizObject.ConvertNullToEmptyString(record_system);
            topic_notes = BizObject.ConvertNullToEmptyString(topic_notes);
            off_label_use = BizObject.ConvertNullToEmptyString(off_label_use);
            conflict_of_interest = BizObject.ConvertNullToEmptyString(conflict_of_interest);
            gapAnalysis = BizObject.ConvertNullToEmptyString(gapAnalysis);
            core_comp_other = BizObject.ConvertNullToEmptyString(core_comp_other);

            TopicAuditInfo record = new TopicAuditInfo(TopicID, plan_comm, dates_pub,
                media_learner_ind, media_provider_ind, media_other, goal_purpose,
                lead_nurse_planner, former_planners, target_audience,
                nau_feedback_ind, nau_survey_ind, nau_advances_ind,
                nau_emerging_ind, nau_deficits_ind, nau_other, na_findings,
                comm_support, eval_method, eval_tools,
                eval_cat_sat_ind, eval_cat_know_ind, eval_cat_skill_ind,
                eval_cat_practice_ind, eval_cat_rel_ind, eval_cat_other,
                eval_data_refine_ind, eval_data_create_ind, eval_data_dis_ind,
                eval_data_other, how_feedback, verif_compl,
                crit_compl_form_ind, crit_compl_score_ind, crit_compl_other,
                doc_compl, discl_compl_ind, discl_conflicts_ind,
                discl_comm_ind, discl_products_ind, discl_offlabel_ind,
                discl_bias_ind, discl_disability_ind, discl_other,
                contact_calc_pilot_ind, contact_calc_author_ind,
                contact_calc_exp_ind, contact_calc_rev_ind, contact_calc_peer_ind,
                contact_calc_other, adv_web_ind, adv_flyers_ind,
                adv_journals_ind, adv_accred_ind, adv_other,
                record_system, topic_notes, off_label_use, conflict_of_interest, gapAnalysis, 
                coi_not_app_ind, coi_removed_ind, coi_revised_ind, coi_not_award_ind, coi_undertaking1_ind,
                coi_undertaking2_ind, core_comp_value_ind, core_comp_role_ind, core_comp_com_ind, core_comp_team_ind, core_comp_other);
            bool ret = SiteProvider.PR2.UpdateTopicAudit(record);

            BizObject.PurgeCacheItems("TopicAudits_TopicAudit_" + TopicID.ToString());
            BizObject.PurgeCacheItems("TopicAudits_TopicAudits");
            return ret;
        }


        /// <summary>
        /// Deletes an existing TopicAudit, but first checks if OK to delete
        /// </summary>
        public static bool DeleteTopicAudit(int TopicID)
        {
            bool IsOKToDelete = OKToDelete(TopicID);
            if (IsOKToDelete)
            {
                return (bool)DeleteTopicAudit(TopicID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing TopicAudit - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteTopicAudit(int TopicID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteTopicAudit(TopicID);
            //new RecordDeletedEvent("TopicAudit", TopicID, null).Raise();
            BizObject.PurgeCacheItems("TopicAudits_TopicAudit");
            return ret;
        }



        /// <summary>
        /// Checks to see if a TopicAudit can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int TopicID)
        {
            return true;
        }


        /// <summary>
        /// Returns a list of TopicAudit objects filled with the data taken from the input list of TopicAuditInfo
        /// </summary>
        private static List<TopicAudit> GetTopicAuditListFromTopicAuditInfoList(List<TopicAuditInfo> recordset)
        {
            List<TopicAudit> TopicAudits = new List<TopicAudit>();
            foreach (TopicAuditInfo record in recordset)
                TopicAudits.Add(GetTopicAuditFromTopicAuditInfo(record));
            return TopicAudits;
        }


        /// <summary>
        /// Returns a TopicAudit object filled with the data taken from the input TopicAuditInfo
        /// </summary>
        private static TopicAudit GetTopicAuditFromTopicAuditInfo(TopicAuditInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new TopicAudit(record.TopicID, record.plan_comm, record.dates_pub,
                    record.media_learner_ind, record.media_provider_ind, record.media_other, record.goal_purpose,
                    record.lead_nurse_planner, record.former_planners, record.target_audience,
                    record.nau_feedback_ind, record.nau_survey_ind, record.nau_advances_ind,
                    record.nau_emerging_ind, record.nau_deficits_ind, record.nau_other, record.na_findings,
                    record.comm_support, record.eval_method, record.eval_tools,
                    record.eval_cat_sat_ind, record.eval_cat_know_ind, record.eval_cat_skill_ind,
                    record.eval_cat_practice_ind, record.eval_cat_rel_ind, record.eval_cat_other,
                    record.eval_data_refine_ind, record.eval_data_create_ind, record.eval_data_dis_ind,
                    record.eval_data_other, record.how_feedback, record.verif_compl,
                    record.crit_compl_form_ind, record.crit_compl_score_ind, record.crit_compl_other,
                    record.doc_compl, record.discl_compl_ind, record.discl_conflicts_ind,
                    record.discl_comm_ind, record.discl_products_ind, record.discl_offlabel_ind,
                    record.discl_bias_ind, record.discl_disability_ind, record.discl_other,
                    record.contact_calc_pilot_ind, record.contact_calc_author_ind,
                    record.contact_calc_exp_ind, record.contact_calc_rev_ind, record.contact_calc_peer_ind,
                    record.contact_calc_other, record.adv_web_ind, record.adv_flyers_ind,
                    record.adv_journals_ind, record.adv_accred_ind, record.adv_other,
                    record.record_system, record.topic_notes, record.off_label_use, record.conflict_of_interest, 
                    record.GapAnalysis, record.coi_not_app_ind, record.coi_removed_ind, record.coi_revised_ind, 
                    record.coi_not_award_ind, record.coi_undertaking1_ind, record.coi_undertaking2_ind,record.core_comp_value_ind,record.core_comp_role_ind,record.core_comp_com_ind,record.core_comp_team_ind,
                    record.core_comp_other);
            }
        }



        #endregion
    }
}
