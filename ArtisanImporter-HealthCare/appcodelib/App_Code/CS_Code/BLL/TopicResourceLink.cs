﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for TopicResourceLink
    /// </summary>
    public class TopicResourceLink : BasePR
    {
        #region Variables and Properties

        private int _TopicResID = 0;
        public int TopicResID
        {
            get { return _TopicResID; }
            protected set { _TopicResID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _ResourceID = 0;
        public int ResourceID
        {
            get { return _ResourceID; }
            set { _ResourceID = value; }
        }

        private string _ResType = "";
        public string ResType
        {
            get { return _ResType; }
            set { _ResType = value; }
        }



        public TopicResourceLink(int TopicResID, int TopicID, int ResourceID, string ResType)
        {
            this.TopicResID = TopicResID;
            this.TopicID = TopicID;
            this.ResourceID = ResourceID;
            this.ResType = ResType;
        }

        public bool Delete()
        {
            bool success = TopicResourceLink.DeleteTopicResourceLink(this.TopicResID);
            if (success)
                this.TopicResID = 0;
            return success;
        }

        public bool Update()
        {
            return TopicResourceLink.UpdateTopicResourceLink(this.TopicResID, this.TopicID, this.ResourceID, this.ResType);
        }
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all TopicResourceLinks
        /// </summary>
        public static List<TopicResourceLink> GetTopicResourceLinks(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<TopicResourceLink> TopicResourceLinks = null;
            string key = "TopicResourceLinks_TopicResourceLinks_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicResourceLinks = (List<TopicResourceLink>)BizObject.Cache[key];
            }
            else
            {
                List<TopicResourceLinkInfo> recordset = SiteProvider.PR2.GetTopicResourceLinks(cSortExpression);
                TopicResourceLinks = GetTopicResourceLinkListFromTopicResourceLinkInfoList(recordset);
                BasePR.CacheData(key, TopicResourceLinks);
            }
            return TopicResourceLinks;
        }

        /// <summary>
        /// Returns a dataset with all ResTypes
        /// </summary>

        public static System.Data.DataSet GetResTypesFromTopicResourceLink()
        {
            return SiteProvider.PR2.GetResTypesFromTopicResourceLink();

        }



        /// <summary>
        /// Returns the number of total TopicResourceLinks
        /// </summary>
        public static int GetTopicResourceLinkCount()
        {
            int TopicResourceLinkCount = 0;
            string key = "TopicResourceLinks_TopicResourceLinkCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicResourceLinkCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicResourceLinkCount = SiteProvider.PR2.GetTopicResourceLinkCount();
                BasePR.CacheData(key, TopicResourceLinkCount);
            }
            return TopicResourceLinkCount;
        }

        /// <summary>
        /// Returns the number of total TopicResourceLinks for a TopicID
        /// </summary>
        public static int GetTopicResourceLinkCountByTopicID(int TopicID)
        {
            int TopicResourceLinkCount = 0;
            string key = "TopicResourceLinks_TopicResourceLinkCountByTopicID_" + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicResourceLinkCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicResourceLinkCount = SiteProvider.PR2.GetTopicResourceLinkCountByTopicID(TopicID);
                BasePR.CacheData(key, TopicResourceLinkCount);
            }
            return TopicResourceLinkCount;
        }

        /// <summary>
        /// Returns a TopicResourceLink object with the specified ID
        /// </summary>
        public static TopicResourceLink GetTopicResourceLinkByID(int TopicResID)
        {
            TopicResourceLink TopicResourceLink = null;
            string key = "TopicResourceLinks_TopicResourceLink_" + TopicResID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicResourceLink = (TopicResourceLink)BizObject.Cache[key];
            }
            else
            {
                TopicResourceLink = GetTopicResourceLinkFromTopicResourceLinkInfo(SiteProvider.PR2.GetTopicResourceLinkByID(TopicResID));
                BasePR.CacheData(key, TopicResourceLink);
            }
            return TopicResourceLink;
        }

        /// <summary>
        /// Updates an existing TopicResourceLink
        /// </summary>
        public static bool UpdateTopicResourceLink(int TopicResID, int TopicID, int ResourceID, string ResType)
        {


            TopicResourceLinkInfo record = new TopicResourceLinkInfo(TopicResID, TopicID, ResourceID, ResType);
            bool ret = SiteProvider.PR2.UpdateTopicResourceLink(record);

            BizObject.PurgeCacheItems("TopicResourceLinks_TopicResourceLink_" + TopicResID.ToString());
            BizObject.PurgeCacheItems("TopicResourceLinks_TopicResourceLinks");
            return ret;
        }

        /// <summary>
        /// Updates an existing TopicResourceLink
        /// </summary>
        public static bool UpdateTopicResourceLinkSortOrder(int TopicResID, int SortOrder)
        {


            //TopicResourceLinkInfo record = new TopicResourceLinkInfo(TopicResID, TopicID, ResourceID, ResType);
            bool ret = SiteProvider.PR2.UpdateTopicResourceLinkSortOrder(TopicResID, SortOrder);

            BizObject.PurgeCacheItems("TopicResourceLinks_TopicResourceLink_" + TopicResID.ToString());
            BizObject.PurgeCacheItems("TopicResourceLinks_TopicResourceLinks");
            return ret;
        }
        public static bool UpdateResourcePersonAssignments(int TopicID, string ResourceIDAssignments, string ResourceType)
        {
            bool ret = SiteProvider.PR2.UpdateResourcePersonAssignments(TopicID, ResourceIDAssignments, ResourceType);
            // TODO: release cache?
            return ret;
        }


        public static TopicResourceLink GetTopicrResourcesByDetails(int TopicID, int ResourceID, String resType)
        {
            TopicResourceLink TopicResourceLinks = null;

            TopicResourceLinkInfo recordset = SiteProvider.PR2.GetTopicrResourcesByDetails(TopicID, ResourceID, resType);
            TopicResourceLinks = GetTopicResourceLinkFromTopicResourceLinkInfo(recordset);

            return TopicResourceLinks;
        }

        public static int DeleteTopicResourceLinkByUnselection(int TopicID, String ResourceType, String ResourceIDAssignments)
        { 
          int ret = SiteProvider.PR2.DeleteTopicResourceLinkByUnselection(TopicID,  ResourceType,ResourceIDAssignments);
            // TODO: release cache?
            return ret;
        }
        
       
       

        //Bhaskar N
        public static bool AddResourcePersonAssignments(int TopicID, string ResourceIDAssignments, string ResourceType)
        {
            bool ret = SiteProvider.PR2.AddResourcePersonAssignments(TopicID, ResourceIDAssignments, ResourceType);
            // TODO: release cache?
            return ret;
        }

        public static DataSet GetTopicDetailsByResID(int ResourceID)
        {
            DataSet ds = new DataSet();
            ds = SiteProvider.PR2.GetTopicDetailsByResID(ResourceID);
            return ds;
        }

        /// <summary>
        /// Creates a new TopicResourceLink
        /// </summary>
        public static int InsertTopicResourceLink(int TopicID, int ResourceID, string ResType)
        {


            TopicResourceLinkInfo record = new TopicResourceLinkInfo(0, TopicID, ResourceID, ResType);
            int ret = SiteProvider.PR2.InsertTopicResourceLink(record);

            BizObject.PurgeCacheItems("TopicResourceLinks_TopicResourceLink");
            return ret;
        }

        /// <summary>
        /// Deletes an existing TopicResourceLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteTopicResourceLink(int TopicResID)
        {
            bool IsOKToDelete = OKToDelete(TopicResID);
            if (IsOKToDelete)
            {
                return (bool)DeleteTopicResourceLink(TopicResID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing TopicResourceLink - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteTopicResourceLink(int TopicResID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteTopicResourceLink(TopicResID);
            //         new RecordDeletedEvent("TopicResourceLink", TopicResID, null).Raise();
            BizObject.PurgeCacheItems("TopicResourceLinks_TopicResourceLink");
            return ret;
        }



        /// <summary>
        /// Checks to see if a TopicResourceLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int TopicResID)
        {
            return true;
        }



        /// <summary>
        /// Returns a TopicResourceLink object filled with the data taken from the input TopicResourceLinkInfo
        /// </summary>
        private static TopicResourceLink GetTopicResourceLinkFromTopicResourceLinkInfo(TopicResourceLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new TopicResourceLink(record.TopicResID, record.TopicID, record.ResourceID, record.ResType);
            }
        }

        /// <summary>
        /// Returns a list of TopicResourceLink objects filled with the data taken from the input list of TopicResourceLinkInfo
        /// </summary>
        private static List<TopicResourceLink> GetTopicResourceLinkListFromTopicResourceLinkInfoList(List<TopicResourceLinkInfo> recordset)
        {
            List<TopicResourceLink> TopicResourceLinks = new List<TopicResourceLink>();
            foreach (TopicResourceLinkInfo record in recordset)
                TopicResourceLinks.Add(GetTopicResourceLinkFromTopicResourceLinkInfo(record));
            return TopicResourceLinks;
        }

        #endregion
    }
}