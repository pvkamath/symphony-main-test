﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for ExProfile
    /// </summary>
    public class ExProfile : BasePR
    {
        #region Variables and Properties

        public int EpID { get; set; }
        public int UserID { get; set; }
        public string ProfileType { get; set; }
        public string ProfileID { get; set; }
        public DateTime? AddDate { get; set; }
        public bool CurStatus { get; set; }
        public DateTime? DisableDate { get; set; }

        public ExProfile(){}
        public ExProfile(int epID, int userID, string profileType, string profileID, DateTime? addDate, bool curStatus, DateTime? disableDate)
        {
            this.EpID = EpID;
            this.UserID = userID;
            this.ProfileType = profileType;
            this.ProfileID = profileID;
            this.AddDate = addDate;
            this.CurStatus = curStatus;
            this.DisableDate = disableDate;
        }

        public bool Delete()
        {
            return true;
        }

        public bool Update()
        {
            return ExProfile.UpdateExProfile(EpID, UserID,ProfileType,ProfileID, AddDate, CurStatus, DisableDate);
        }
        #endregion

        #region Methods
       /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all ExProfiles
        /// </summary>
        public static List<ExProfile> GetEXProfile(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "adddate";

            List<ExProfile> EXProfile = null;
            string key = "EXProfile_EXProfile_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                EXProfile = (List<ExProfile>)BizObject.Cache[key];
            }
            else
            {
                List<ExProfileInfo> recordset = SiteProvider.PR2.GetExProfile(cSortExpression);
                EXProfile = GetExProfileListFromExProfileInfoList(recordset);
                BasePR.CacheData(key, EXProfile);
            }
            return EXProfile;
        }

        /// <summary>
        /// Updates an existing ExProfile
        /// </summary>
        public static bool UpdateExProfile(int epID, int userID, string profileType, string profileID, DateTime? addDate, bool curStatus, DateTime? disableDate)
        {
            profileType = BizObject.ConvertNullToEmptyString(profileType);
            profileID = BizObject.ConvertNullToEmptyString(profileID);

            ExProfileInfo record = new ExProfileInfo(epID, userID, profileType, profileID, addDate, curStatus, disableDate);
            bool ret = SiteProvider.PR2.UpdateExProfile(record);

            /// TODO: need to clear out the textbook version also
            BizObject.PurgeCacheItems("ExProfiles_ExProfile_" + epID.ToString());
            BizObject.PurgeCacheItems("ExProfiles_ExProfiles");
            return ret;
        }

        /// <summary>
        /// Returns a list of ExProfile objects filled with the data taken from the input list of ExProfileInfo
        /// </summary>
        private static List<ExProfile> GetExProfileListFromExProfileInfoList(List<ExProfileInfo> recordset)
        {
            List<ExProfile> Categories = new List<ExProfile>();
            foreach (ExProfileInfo record in recordset)
                Categories.Add(GetExProfileFromExProfileInfo(record));
            return Categories;
        }

        /// <summary>
        /// Returns a ExProfile object filled with the data taken from the input ExProfileInfo
        /// </summary>
        private static ExProfile GetExProfileFromExProfileInfo(ExProfileInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new ExProfile(record.EpID, record.UserID, record.ProfileType, record.ProfileID, record.AddDate, record.CurStatus, record.DisableDate);
            }
        }
        #endregion
    }
}

    

