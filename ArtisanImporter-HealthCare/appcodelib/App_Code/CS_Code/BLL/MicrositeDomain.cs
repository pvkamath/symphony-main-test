﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for MicrositeDomain
    /// </summary>
    public class MicrositeDomain : BasePR
    {
        #region Variables and Properties


        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }
        private string _domain_name = "";
        public string Domain_name
        {
            get { return _domain_name; }
            private set { _domain_name = value; }
        }
        private string _index_title = "";
        public string Index_title
        {
            get { return _index_title; }
            private set { _index_title = value; }
        }
        private string _logo_filename = "";
        public string Logo_filename
        {
            get { return _logo_filename; }
            private set { _logo_filename = value; }
        }
        private string _logo_desc = "";
        public string Logo_Desc
        {
            get { return _logo_desc; }
            private set { _logo_desc = value; }
        }
        private string _banner_filename = "";
        public string Banner_filename
        {
            get { return _banner_filename; }
            private set { _banner_filename = value; }
        }
        private string _banner_desc = "";
        public string Banner_Desc
        {
            get { return _banner_desc; }
            private set { _banner_desc = value; }
        }
        private string _meta_kw = "";
        public string Meta_kw
        {
            get { return _meta_kw; }
            private set { _meta_kw = value; }
        }
        private string _meta_desc = "";
        public string Meta_desc
        {
            get { return _meta_desc; }
            private set { _meta_desc = value; }
        }
        private decimal _membership_cost;
        public decimal Membership_cost
        {
            get { return _membership_cost; }
            private set { _membership_cost = value; }
        }
        private string _market_text = "";
        public string Market_text
        {
            get { return _market_text; }
            private set { _market_text = value; }
        }
        private string _folder_name = "";
        public string Folder_name
        {
            get { return _folder_name; }
            private set { _folder_name = value; }
        }
        private bool _membership_ind;
        public bool Membership_ind
        {
            get { return _membership_ind; }
            private set { _membership_ind = value; }
        }
        private string _membership_image = "";
        public string Membership_image
        {
            get { return _membership_image; }
            private set { _membership_image = value; }
        }
        private string _membership_desc = "";
        public string Membership_desc
        {
            get { return _membership_desc; }
            private set { _membership_desc = value; }
        }
        private string _contacthour_longname = "";
        public string Contacthour_longname
        {
            get { return _contacthour_longname; }
            private set { _contacthour_longname = value; }
        }
        private string _contacthour_shortname = "";
        public string Contacthour_shortname
        {
            get { return _contacthour_shortname; }
            private set { _contacthour_shortname = value; }
        }
        private string _course_cat_header = "";
        public string Course_cat_header
        {
            get { return _course_cat_header; }
            private set { _course_cat_header = value; }
        }
        private string _state_mandated_header = "";
        public string State_mandated_header
        {
            get { return _state_mandated_header; }
            private set { _state_mandated_header = value; }
        }
        private string _accreditation_statement = "";
        public string Accreditation_statement
        {
            get { return _accreditation_statement; }
            private set { _accreditation_statement = value; }
        }
        private string _membership_ads = "";
        public string Membership_ads
        {
            get { return _membership_ads; }
            private set { _membership_ads = value; }
        }
        private bool _active_ind;
        public bool Active_ind
        {
            get { return _active_ind; }
            private set { _active_ind = value; }
        }
        private string _facebook_link = "";
        public string Facebook_link
        {
            get { return _facebook_link; }
            private set { _facebook_link = value; }
        }
        private string _google_plus_link = "";
        public string Google_plus_link
        {
            get { return _google_plus_link; }
            private set { _google_plus_link = value; }
        }
        private string _twitter_link = "";
        public string Twitter_link
        {
            get { return _twitter_link; }
            private set { _twitter_link = value; }
        }
        private string _disclaimer = "";
        public string disclaimer
        {
            get { return _disclaimer; }
            private set { _disclaimer = value; }
        }
        public MicrositeDomain()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public MicrositeDomain(int msid, string domain_name, string index_title, string logo_filename,
            string logo_desc, string banner_filename, string banner_desc, string meta_kw, string meta_desc,
            decimal membership_cost, bool membership_ind, string membership_image, string membership_desc,
            string market_text, string folder_name, string contacthour_longname, string contacthour_shortname,
            string course_cat_header, string state_mandated_header, string accreditation_statement, string membership_ads, bool active_ind,
            string facebook_link, string google_plus_link, string twitter_link, string disclaimer)
        {
            this.Msid = msid;
            this.Domain_name = domain_name;
            this.Index_title = index_title;
            this.Logo_filename = logo_filename;
            this.Logo_Desc = logo_desc;
            this.Banner_filename = banner_filename;
            this.Banner_Desc = banner_desc;
            this.Meta_kw = meta_kw;
            this.Meta_desc = meta_desc;
            this.Membership_cost = membership_cost;
            this.Membership_ind = membership_ind;
            this.Membership_image = membership_image;
            this.Membership_desc = membership_desc;
            this.Market_text = market_text;
            this.Folder_name = folder_name;
            this.Contacthour_longname = contacthour_longname;
            this.Contacthour_shortname = contacthour_shortname;
            this.Course_cat_header = course_cat_header;
            this.State_mandated_header = state_mandated_header;
            this.Accreditation_statement = accreditation_statement;
            this.Membership_ads = membership_ads;
            this.Active_ind = active_ind;
            this.Facebook_link = facebook_link;
            this.Google_plus_link = google_plus_link;
            this.Twitter_link = twitter_link;
            this.disclaimer = disclaimer;
        }
        public bool Delete()
        {
            bool success = MicrositeDomain.DeleteMicrositeDomain(this.Msid);
            if (success)
                this.Msid = 0;
            return success;
        }

        public bool Update()
        {
            return MicrositeDomain.UpdateMicrositeDomain(this.Msid, this.Domain_name, this.Index_title, this.Logo_filename, this.Logo_Desc,
                this.Banner_filename, this.Banner_Desc, this.Meta_kw, this.Meta_desc, this.Membership_cost, this.Membership_ind,
                this.Membership_image, this.Membership_desc, this.Market_text, this.Folder_name, this.Contacthour_longname,
                this.Contacthour_shortname, this.Course_cat_header, this.State_mandated_header, this.Accreditation_statement, this.Membership_ads, this.Active_ind,
                this.Facebook_link, this.Google_plus_link, this.Twitter_link, this.disclaimer);
        }

        #endregion
        #region Methods
        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all MicrositeDomains
        //</summary>
        public static List<MicrositeDomain> GetMicrositeDomain(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "domain_name";

            List<MicrositeDomain> MicrositeDomains = null;
            string key = "MicrositeDomain_MicrositeDomain_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeDomains = (List<MicrositeDomain>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeDomainInfo> recordset = SiteProvider.PR2.GetMicrositeDomain(cSortExpression);
                MicrositeDomains = GetMicrositeDomainListFromMicrositeDomainInfoList(recordset);
                BasePR.CacheData(key, MicrositeDomains);
            }
            return MicrositeDomains;
        }

        public static List<MicrositeDomain> GetMicrositeDomainAdmin(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "domain_name";

            List<MicrositeDomain> MicrositeDomains = null;
            string key = "MicrositeDomain_MicrositeDomainAdmin_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeDomains = (List<MicrositeDomain>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeDomainInfo> recordset = SiteProvider.PR2.GetMicrositeDomainAdmin(cSortExpression);
                MicrositeDomains = GetMicrositeDomainListFromMicrositeDomainInfoList(recordset);
                BasePR.CacheData(key, MicrositeDomains);
            }
            return MicrositeDomains;
        }

        public static List<MicrositeDomain> GetMicrositeDomain(string cSortExpression, bool IsActive, int domainId)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "domain_name";

            List<MicrositeDomain> MicrositeDomains = null;
            string key = "MicrositeDomain_MicrositeDomain_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeDomains = (List<MicrositeDomain>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeDomainInfo> recordset = SiteProvider.PR2.GetMicrositeDomain(cSortExpression, IsActive, domainId);
                MicrositeDomains = GetMicrositeDomainListFromMicrositeDomainInfoList(recordset);
                BasePR.CacheData(key, MicrositeDomains);
            }
            return MicrositeDomains;
        }

        /// <summary>
        /// Returns a collection with all MicrositeDomains BY UserID
        /// </summary>
        public static System.Data.DataSet GetMicrositeDomainByUserId(int userID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";


            if (cSortExpression.Length == 0)
                cSortExpression = "domain_name";

            System.Data.DataSet MicrositeDomain = null;
            MicrositeDomain = SiteProvider.PR2.GetMicrositeDomainByUserId(userID, cSortExpression);



            return MicrositeDomain;
        }

        /// <summary>
        /// Returns a MicrositeDomains object with the specified ID
        /// </summary>
        public static MicrositeDomain GetMicrositeDomainByID(int Msid)
        {
            MicrositeDomain MicrositeDomains = null;
            string key = "MicrositeDomains_MicrositeDomains_" + Msid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeDomains = (MicrositeDomain)BizObject.Cache[key];
            }
            else
            {
                MicrositeDomains = GetMicrositeDomainFromMicrositeDomainInfo(SiteProvider.PR2.GetMicrositeDomainByID(Msid));
                BasePR.CacheData(key, MicrositeDomains);
            }
            return MicrositeDomains;
        }
        public static MicrositeDomain GetMicrositeDomainIDByName(string DomainName)
        {
            MicrositeDomain MicrositeDomains = null;
            string key = "MicrositeDomains_MicrositeDomains_" + DomainName.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeDomains = (MicrositeDomain)BizObject.Cache[key];
            }
            else
            {
                MicrositeDomains = GetMicrositeDomainFromMicrositeDomainInfo(SiteProvider.PR2.GetMicrositeDomainIDByName(DomainName));
                BasePR.CacheData(key, MicrositeDomains);
            }
            return MicrositeDomains;
        }
        public static int InsertMicrositeDomain(string Domain_name, string Index_title, string Logo_filename, string Logo_Desc,
            string Banner_filename, string Banner_Desc, string Meta_kw, string Meta_desc, decimal Membership_cost, bool Membership_ind,
            string Membership_image, string Membership_desc, string Market_text, string Folder_name, string Contacthour_longname,
            string Contacthour_shortname, string Course_cat_header, string State_mandated_header, string Accreditation_statement, string Membership_ads, bool Active_ind,
            string Facebook_link, string Google_plus_link, string Twitter_link, string Disclaimer)
        {
            Domain_name = BizObject.ConvertNullToEmptyString(Domain_name);
            Index_title = BizObject.ConvertNullToEmptyString(Index_title);
            Logo_filename = BizObject.ConvertNullToEmptyString(Logo_filename);
            Logo_Desc = BizObject.ConvertNullToEmptyString(Logo_Desc);
            Banner_filename = BizObject.ConvertNullToEmptyString(Banner_filename);
            Banner_Desc = BizObject.ConvertNullToEmptyString(Banner_Desc);
            Meta_kw = BizObject.ConvertNullToEmptyString(Meta_kw);
            Meta_desc = BizObject.ConvertNullToEmptyString(Meta_desc);
            Market_text = BizObject.ConvertNullToEmptyString(Market_text);
            Folder_name = BizObject.ConvertNullToEmptyString(Folder_name);
            Contacthour_longname = BizObject.ConvertNullToEmptyString(Contacthour_longname);
            Contacthour_shortname = BizObject.ConvertNullToEmptyString(Contacthour_shortname);
            Disclaimer = BizObject.ConvertNullToEmptyString(Disclaimer);

            MicrositeDomainInfo record = new MicrositeDomainInfo(0, Domain_name, Index_title, Logo_filename, Logo_Desc, Banner_filename,
                Banner_Desc, Meta_kw, Meta_desc, Membership_cost, Membership_ind, Membership_image, Membership_desc, Market_text, Folder_name,
                Contacthour_longname, Contacthour_shortname, Course_cat_header, State_mandated_header, Accreditation_statement, Membership_ads, Active_ind,
                Facebook_link, Google_plus_link, Twitter_link, Disclaimer);
            int ret = SiteProvider.PR2.InsertMicrositeDomain(record);

            BizObject.PurgeCacheItems("MicrositeDomains_MicrositeDomains");
            return ret;
        }
        /// <summary>
        /// Updates an existing MicrositeDomain
        /// </summary>
        public static bool UpdateMicrositeDomain(int Msid, string Domain_name, string Index_title, string Logo_filename, string Logo_Desc,
            string Banner_filename, string Banner_Desc, string Meta_kw, string Meta_desc, decimal Membership_cost, bool Membership_ind,
            string Membership_image, string Membership_desc, string Market_text, string Folder_name, string Contacthour_longname,
            string Contacthour_shortname, string Course_cat_header, string State_mandated_header, string Accreditation_statement, string Membership_ads, bool Active_ind,
            string Facebook_link, string Google_plus_link, string Twitter_link, string Disclaimer)
        {
            Domain_name = BizObject.ConvertNullToEmptyString(Domain_name);
            Index_title = BizObject.ConvertNullToEmptyString(Index_title);
            Logo_filename = BizObject.ConvertNullToEmptyString(Logo_filename);
            Logo_Desc = BizObject.ConvertNullToEmptyString(Logo_Desc);
            Banner_filename = BizObject.ConvertNullToEmptyString(Banner_filename);
            Banner_Desc = BizObject.ConvertNullToEmptyString(Banner_Desc);
            Meta_kw = BizObject.ConvertNullToEmptyString(Meta_kw);
            Meta_desc = BizObject.ConvertNullToEmptyString(Meta_desc);
            Market_text = BizObject.ConvertNullToEmptyString(Market_text);
            Folder_name = BizObject.ConvertNullToEmptyString(Folder_name);
            Contacthour_longname = BizObject.ConvertNullToEmptyString(Contacthour_longname);
            Contacthour_shortname = BizObject.ConvertNullToEmptyString(Contacthour_shortname);
            Disclaimer = BizObject.ConvertNullToEmptyString(Disclaimer);

            MicrositeDomainInfo record = new MicrositeDomainInfo(Msid, Domain_name, Index_title, Logo_filename,
                Logo_Desc, Banner_filename, Banner_Desc, Meta_kw, Meta_desc, Membership_cost, Membership_ind,
                Membership_image, Membership_desc, Market_text, Folder_name, Contacthour_longname, Contacthour_shortname,
                Course_cat_header, State_mandated_header, Accreditation_statement, Membership_ads, Active_ind,
                Facebook_link, Google_plus_link, Twitter_link, Disclaimer);
            bool ret = SiteProvider.PR2.UpdateMicrositeDomain(record);

            BizObject.PurgeCacheItems("MicrositeDomains_MicrositeDomains_" + Msid.ToString());
            BizObject.PurgeCacheItems("MicrositeDomain_MicrositeDomains");
            return ret;
        }

        /// <summary>
        /// Deletes an existing MicrositeDomain, but first checks if OK to delete
        /// </summary>
        public static bool DeleteMicrositeDomain(int Msid)
        {
            bool IsOKToDelete = OKToDelete(Msid);
            if (IsOKToDelete)
            {
                return (bool)DeleteMicrositeDomain(Msid, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing MicrositeDomain - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteMicrositeDomain(int Msid, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteMicrositeDomain(Msid);
            BizObject.PurgeCacheItems("MicrositeDomain_MicrositeDomain");
            return ret;
        }

        public static List<MicrositeDomain> GetMicrositeDomainByTopicID(int topicID, string cSortExpression)
        {
            return GetMicrositeDomainListFromMicrositeDomainInfoList(SiteProvider.PR2.GetMicrositeDomainByTopicID(topicID, cSortExpression));
        }

        /// <summary>
        /// Checks to see if a MicrositeDomain can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }


        /// <summary>
        /// Returns a MicrositeDomain object filled with the data taken from the input MicrositeDomainInfo
        /// </summary>
        private static MicrositeDomain GetMicrositeDomainFromMicrositeDomainInfo(MicrositeDomainInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new MicrositeDomain(record.Msid, record.Domain_name, record.Index_title, record.Logo_filename, record.Logo_Desc,
                    record.Banner_filename, record.Banner_Desc, record.Meta_kw, record.Meta_desc, record.Membership_cost, record.Membership_ind,
                    record.Membership_image, record.Membership_desc, record.Market_text, record.Folder_name, record.Contacthour_longname,
                    record.Contacthour_shortname, record.Course_cat_header, record.State_mandated_header, record.Accreditation_statement,
                    record.Membership_ads, record.Active_ind, record.Facebook_link, record.Google_plus_link, record.Twitter_link, record.Disclaimer);
            }
        }

        /// <summary>
        /// Returns a list of MicrositeDomain objects filled with the data taken from the input list of MicrositeDomainInfo
        /// </summary>
        private static List<MicrositeDomain> GetMicrositeDomainListFromMicrositeDomainInfoList(List<MicrositeDomainInfo> recordset)
        {
            List<MicrositeDomain> MicrositeDomains = new List<MicrositeDomain>();
            foreach (MicrositeDomainInfo record in recordset)
                MicrositeDomains.Add(GetMicrositeDomainFromMicrositeDomainInfo(record));
            return MicrositeDomains;
        }
        #endregion
    }
}
