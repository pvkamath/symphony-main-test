﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using PearlsReview.QTI;
using PearlsReview.Constants;
using System.Reflection;

namespace PearlsReview.BLL
{ 

    /// <summary>
    /// Summary description for TopicQuestion
    /// </summary>
    public partial class TopicQuestion : BasePR
    {
        #region Variables and Properties

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _QuestionNum = 0;
        public int QuestionNum
        {
            get { return _QuestionNum; }
            set { _QuestionNum = value; }
        }

        //Bsk New
        private int _HeaderNum = 0;
        public int HeaderNum
        {
            get { return _HeaderNum; }
            set { _HeaderNum = value; }
        }

        //Bsk New
        private string _HeaderName = "";
        public string HeaderName
        {
            get { return _HeaderName; }
            set { _HeaderName = value; }
        }

        private string _Question = "";
        public string Question
        {
            get { return _Question; }
            set { _Question = value; }
        }

        private bool _A_Correct = false;
        public bool A_Correct
        {
            get { return _A_Correct; }
            set { _A_Correct = value; }
        }

        private string _Answer_A = "";
        public string Answer_A
        {
            get { return _Answer_A; }
            set { _Answer_A = value; }
        }

        private bool _B_Correct = false;
        public bool B_Correct
        {
            get { return _B_Correct; }
            set { _B_Correct = value; }
        }

        private string _Answer_B = "";
        public string Answer_B
        {
            get { return _Answer_B; }
            set { _Answer_B = value; }
        }

        private bool _C_Correct = false;
        public bool C_Correct
        {
            get { return _C_Correct; }
            set { _C_Correct = value; }
        }

        private string _Answer_C = "";
        public string Answer_C
        {
            get { return _Answer_C; }
            set { _Answer_C = value; }
        }

        private bool _D_Correct = false;
        public bool D_Correct
        {
            get { return _D_Correct; }
            set { _D_Correct = value; }
        }

        private string _Answer_D = "";
        public string Answer_D
        {
            get { return _Answer_D; }
            set { _Answer_D = value; }
        }

        private bool _E_Correct = false;
        public bool E_Correct
        {
            get { return _E_Correct; }
            set { _E_Correct = value; }
        }

        private string _Answer_E = "";
        public string Answer_E
        {
            get { return _Answer_E; }
            set { _Answer_E = value; }
        }

        private bool _F_Correct = false;
        public bool F_Correct
        {
            get { return _F_Correct; }
            set { _F_Correct = value; }
        }

        private string _Answer_F = "";
        public string Answer_F
        {
            get { return _Answer_F; }
            set { _Answer_F = value; }
        }

        private string _Discussion = "";
        public string Discussion
        {
            get { return _Discussion; }
            set { _Discussion = value; }
        }

        public TopicQuestion()
        {
        }
      

        public TopicQuestion(int TopicID,
            int QuestionNum, int HeaderNum, string HeaderName, string Question,
            bool A_Correct, string Answer_A,
            bool B_Correct, string Answer_B,
            bool C_Correct, string Answer_C,
            bool D_Correct, string Answer_D,
            bool E_Correct, string Answer_E,
            bool F_Correct, string Answer_F,
            string Discussion)
        {
            this.TopicID = TopicID;
            this.QuestionNum = QuestionNum;
            this.HeaderNum = HeaderNum;
            this.HeaderName = HeaderName;
            this.Question = Question;
            this.A_Correct = A_Correct;
            this.Answer_A = Answer_A;
            this.B_Correct = B_Correct;
            this.Answer_B = Answer_B;
            this.C_Correct = C_Correct;
            this.Answer_C = Answer_C;
            this.D_Correct = D_Correct;
            this.Answer_D = Answer_D;
            this.E_Correct = E_Correct;
            this.Answer_E = Answer_E;
            this.F_Correct = F_Correct;
            this.Answer_F = Answer_F;
            this.Discussion = Discussion;
        }

        public TopicQuestion(TopicQuestion tp)
        {
            this.TopicID = tp.TopicID;
            this.QuestionNum = tp.QuestionNum;
            this.HeaderNum = tp.HeaderNum;
            this.HeaderName = tp.HeaderName;
            this.Question = tp.Question;
            this.A_Correct = tp.A_Correct;
            this.Answer_A = tp.Answer_A;
            this.B_Correct = tp.B_Correct;
            this.Answer_B = tp.Answer_B;
            this.C_Correct = tp.C_Correct;
            this.Answer_C = tp.Answer_C;
            this.D_Correct = tp.D_Correct;
            this.Answer_D = tp.Answer_D;
            this.E_Correct = tp.E_Correct;
            this.Answer_E = tp.Answer_E;
            this.F_Correct = tp.F_Correct;
            this.Answer_F = tp.Answer_F;
            this.Discussion = tp.Discussion;
        }

        //public bool Delete()
        //{
        //    bool success = TopicDetail.DeleteTopicDetail(this.TopicID);
        //    if (success)
        //        this.TopicID = 0;
        //    return success;
        //}

        //public bool Update()
        //{
        //    return Topic.UpdateTopic(this.TopicID, this.Type, this.Data,
        //   this.CorrectAns, this.Order, this.Discussion);
        //}

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Topic Questions
        /// </summary>
        public static List<TopicQuestion> GetTopicQuestionsByTopicID(int TopicID)
        {
            //            List<TopicQuestion> TopicQuestions = null;

            // set up the TopicQuestions list containing 6
            // new default TopicQuestion objects
            //TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, 1));
            //TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, 2));
            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 3));
            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 4));
            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 5));
            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 6));

            // get list of TopicDetail records matching the TopicID
            //List<TopicDetail> TopicDetails = TopicDetail.GetTopicDetailsByTopicID(TopicID);
            List<TopicQuestion> TopicQuestions = new List<TopicQuestion>();
            QTIUtils oQTIUtils = new QTIUtils();
            TestDefinition TestDef = TestDefinition.GetTestDefinitionByTopicID(TopicID);
            List<QTIQuestionObject> QTIQuestions = new List<QTIQuestionObject>();
            if (TestDef == null)
            {
                // new item being added
                QTIQuestionObject QTIQuestion;
                QTIQuestion = new QTIQuestionObject();
                //QTIQuestions.Add(QTIQuestion);
                //QTIQuestions.Add(QTIQuestion);
                //QTIQuestions.Add(QTIQuestion);
                //QTIQuestions.Add(QTIQuestion);
                //QTIQuestions.Add(QTIQuestion);
                //QTIQuestions.Add(QTIQuestion);
            }
            else
            {
                QTIQuestions = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(TestDef.XMLTest.ToString());
            }

            // check the count to be sure it's 6 question objects
            if (QTIQuestions.Count > 0)
            {
                // 6 objects found, so put the info into the 6
                // TopicQuestion objects in the TopicQuestions list
                int J = 1;
                int K = 1;
                string HeaderName = "";

                for (int i = 1; i <= QTIQuestions.Count; i++)
                {
                    //TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, i));

                    //Bsk New
                    int HeaderNum = Convert.ToInt32(QTIQuestions[i - 1].cHeaderID);

                    if (HeaderNum < 0)
                    {
                        HeaderNum = -J;
                        HeaderName = "";
                        J = J + 1;
                    }
                    else
                    {
                        HeaderNum = K;
                        HeaderName = "Question # " + K;
                        K = K + 1;
                    }
                    TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, i, HeaderNum, HeaderName));

                }

                int iOrder = 0;
                foreach (QTIQuestionObject QTIQuestion in QTIQuestions)
                {
                    TopicQuestions[iOrder].Question = QTIQuestion.cQuestionText;
                    TopicQuestions[iOrder].Answer_A = QTIQuestion.cAnswer_A_Text;
                    TopicQuestions[iOrder].Answer_B = QTIQuestion.cAnswer_B_Text;
                    TopicQuestions[iOrder].Answer_C = QTIQuestion.cAnswer_C_Text;
                    TopicQuestions[iOrder].Answer_D = QTIQuestion.cAnswer_D_Text;
                    TopicQuestions[iOrder].Answer_E = QTIQuestion.cAnswer_E_Text;
                    TopicQuestions[iOrder].Answer_F = QTIQuestion.cAnswer_F_Text;

                    // 8-13-2008 Workaround to force True and False into first two options
                    // on questions 3, 4, 5, 6 (zero-based collection = 2,3,4,5)
                    //if (iOrder > 1)
                    //{
                    //    TopicQuestions[iOrder].Answer_A = "True";
                    //    TopicQuestions[iOrder].Answer_B = "False";
                    //}
                    // end workaround

                    // Correct Answer
                    if (QTIQuestion.cCorrectAnswer == "A")
                        TopicQuestions[iOrder].A_Correct = true;
                    if (QTIQuestion.cCorrectAnswer == "B")
                        TopicQuestions[iOrder].B_Correct = true;
                    if (QTIQuestion.cCorrectAnswer == "C")
                        TopicQuestions[iOrder].C_Correct = true;
                    if (QTIQuestion.cCorrectAnswer == "D")
                        TopicQuestions[iOrder].D_Correct = true;
                    if (QTIQuestion.cCorrectAnswer == "E")
                        TopicQuestions[iOrder].E_Correct = true;
                    if (QTIQuestion.cCorrectAnswer == "F")
                        TopicQuestions[iOrder].F_Correct = true;

                    // Discussion
                    TopicQuestions[iOrder].Discussion = QTIQuestion.cCorrectAnswerFeedback;

                    iOrder++;

                }
            }
            return TopicQuestions;
        }




        /// <summary>
        /// Returns a collection with all Topic Questions
        /// </summary>
        public static List<TopicQuestion> GetVIQuestionsByTopicID(int TopicID)
        {
            //            List<TopicQuestion> TopicQuestions = null;

            // set up the TopicQuestions list containing 6
            // new default TopicQuestion objects
            //TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, 1));
            //TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, 2));
            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 3));
            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 4));
            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 5));
            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 6));

            // get list of TopicDetail records matching the TopicID
            //List<TopicDetail> TopicDetails = TopicDetail.GetTopicDetailsByTopicID(TopicID);
            List<TopicQuestion> TopicQuestions = new List<TopicQuestion>();
            QTIUtils oQTIUtils = new QTIUtils();
            ViDefinition TestDef = ViDefinition.GetViDefinitionByTopicID(TopicID);
            List<QTIQuestionObject> QTIQuestions = new List<QTIQuestionObject>();
            if (TestDef == null)
            {
                // new item being added
                QTIQuestionObject QTIQuestion;
                QTIQuestion = new QTIQuestionObject();
                //QTIQuestions.Add(QTIQuestion);
                //QTIQuestions.Add(QTIQuestion);
                //QTIQuestions.Add(QTIQuestion);
                //QTIQuestions.Add(QTIQuestion);
                //QTIQuestions.Add(QTIQuestion);
                //QTIQuestions.Add(QTIQuestion);
            }
            else
            {
                QTIQuestions = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(TestDef.XMLTest.ToString());
            }

            // check the count to be sure it's 6 question objects
            if (QTIQuestions.Count > 0)
            {
                // 6 objects found, so put the info into the 6
                // TopicQuestion objects in the TopicQuestions list


                for (int i = 1; i <= QTIQuestions.Count; i++)
                {
                    //TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, i));

                    //                    TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, i));
                    TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, i, 0, "")); //bsk
                }

                int iOrder = 0;
                foreach (QTIQuestionObject QTIQuestion in QTIQuestions)
                {
                    TopicQuestions[iOrder].Question = QTIQuestion.cQuestionText;
                    TopicQuestions[iOrder].Answer_A = QTIQuestion.cAnswer_A_Text;
                    TopicQuestions[iOrder].Answer_B = QTIQuestion.cAnswer_B_Text;
                    TopicQuestions[iOrder].Answer_C = QTIQuestion.cAnswer_C_Text;
                    TopicQuestions[iOrder].Answer_D = QTIQuestion.cAnswer_D_Text;
                    TopicQuestions[iOrder].Answer_E = QTIQuestion.cAnswer_E_Text;
                    TopicQuestions[iOrder].Answer_F = QTIQuestion.cAnswer_F_Text;

                    // 8-13-2008 Workaround to force True and False into first two options
                    // on questions 3, 4, 5, 6 (zero-based collection = 2,3,4,5)
                    //if (iOrder > 1)
                    //{
                    //    TopicQuestions[iOrder].Answer_A = "True";
                    //    TopicQuestions[iOrder].Answer_B = "False";
                    //}
                    // end workaround

                    // Correct Answer
                    if (QTIQuestion.cCorrectAnswer == "A")
                        TopicQuestions[iOrder].A_Correct = true;
                    if (QTIQuestion.cCorrectAnswer == "B")
                        TopicQuestions[iOrder].B_Correct = true;
                    if (QTIQuestion.cCorrectAnswer == "C")
                        TopicQuestions[iOrder].C_Correct = true;
                    if (QTIQuestion.cCorrectAnswer == "D")
                        TopicQuestions[iOrder].D_Correct = true;
                    if (QTIQuestion.cCorrectAnswer == "E")
                        TopicQuestions[iOrder].E_Correct = true;
                    if (QTIQuestion.cCorrectAnswer == "F")
                        TopicQuestions[iOrder].F_Correct = true;

                    // Discussion
                    TopicQuestions[iOrder].Discussion = QTIQuestion.cCorrectAnswerFeedback;

                    iOrder++;

                }
            }
            return TopicQuestions;
        }



        /// <summary>
        /// Returns a new multiple choice TopicQuestion object
        /// </summary>
        private static TopicQuestion CreateNewMultipleChoiceQuestion(int TopicID, int QuestionNum, int HeaderNum, string HeaderName)
        {
            TopicQuestion objTopicQuestion =
              new TopicQuestion(TopicID,
                QuestionNum, HeaderNum, HeaderName, "",
                false, "",
                false, "",
                false, "",
                false, "",
                false, "",
                false, "",
                "");
            return objTopicQuestion;
        }

        /// <summary>
        /// Returns a new true/false TopicQuestion object
        /// </summary>
        private static TopicQuestion CreateNewTrueFalseQuestion(int TopicID, int QuestionNum)
        {
            TopicQuestion objTopicQuestion =
              new TopicQuestion(TopicID,
                QuestionNum, 0, "", "",
                false, "True",
                false, "False",
                false, "",
                false, "",
                false, "",
                false, "",
                "");
            return objTopicQuestion;
        }



        public static bool DeleteTopicQuestion(int TopicID, int QuestionNum)
        {

            List<TopicQuestion> TopicQuestions = GetTopicQuestionsByTopicID(TopicID);
            TopicQuestions.RemoveAt(QuestionNum - 1);

            int i;
            for (i = QuestionNum; i < TopicQuestions.Count; i++)
            {
                TopicQuestions[i].QuestionNum = i - 1;
            }
            bool ret;

            if (TopicQuestions.Count <= 0)
            {

                TestDefinition objtest = TestDefinition.GetTestDefinitionByTopicID(TopicID);
                //if (objtest is Nullable)
                if (objtest == null) //bsk
                {
                    ret = false;

                }
                else
                {
                    ret = TestDefinition.DeleteTestDefinition(objtest.TestDefID);
                }
            }
            else
            {

                ret = InsertTopicQuestionsFromList(TopicQuestions);
            }

            return ret;

        }


        public static bool DeleteVIQuestion(int TopicID, int QuestionNum)
        {

            List<TopicQuestion> TopicQuestions = GetVIQuestionsByTopicID(TopicID);
            TopicQuestions.RemoveAt(QuestionNum - 1);

            int i;
            for (i = QuestionNum; i < TopicQuestions.Count; i++)
            {
                TopicQuestions[i].QuestionNum = i - 1;
            }
            bool ret;

            if (TopicQuestions.Count <= 0)
            {

                ViDefinition objtest = ViDefinition.GetViDefinitionByTopicID(TopicID);
                //if (objtest is Nullable) 
                if (objtest == null) //bsk
                {
                    ret = false;

                }
                else
                {
                    ret = ViDefinition.DeleteViDefinition(objtest.ViDefID);
                }
            }
            else
            {

                ret = InsertVIQuestionsFromList(TopicQuestions);
            }

            return ret;

        }

        public static bool UpdateTopicQuestion(int TopicID,
            int QuestionNum, string Question,
            bool A_Correct, string Answer_A,
            bool B_Correct, string Answer_B,
            bool C_Correct, string Answer_C,
            bool D_Correct, string Answer_D,
            bool E_Correct, string Answer_E,
            string Discussion)

//            bool E_Correct, string Answer_E,
        //            bool F_Correct, string Answer_F,
        {
            // get the complete set of 6 questions from the database
            List<TopicQuestion> TopicQuestions = GetTopicQuestionsByTopicID(TopicID);

            int iQuestionPointer;

            if (QuestionNum > TopicQuestions.Count)
            {

                TopicQuestion objTopicQuestion = new TopicQuestion(TopicID, QuestionNum, 0, "", Question, A_Correct, Answer_A,
                   B_Correct, Answer_B, C_Correct, Answer_C, D_Correct, Answer_D, E_Correct, Answer_E, false, "", Discussion);
                TopicQuestions.Add(objTopicQuestion);
            }
            else
            {
                iQuestionPointer = QuestionNum - 1;
                // update the record for the question number passed in
                TopicQuestions[iQuestionPointer].Question = Question;
                TopicQuestions[iQuestionPointer].A_Correct = A_Correct;
                TopicQuestions[iQuestionPointer].Answer_A = Answer_A;
                TopicQuestions[iQuestionPointer].B_Correct = B_Correct;
                TopicQuestions[iQuestionPointer].Answer_B = Answer_B;
                TopicQuestions[iQuestionPointer].C_Correct = C_Correct;
                TopicQuestions[iQuestionPointer].Answer_C = Answer_C;
                TopicQuestions[iQuestionPointer].D_Correct = D_Correct;
                TopicQuestions[iQuestionPointer].Answer_D = Answer_D;
                TopicQuestions[iQuestionPointer].E_Correct = E_Correct;
                TopicQuestions[iQuestionPointer].Answer_E = Answer_E;
                //TopicQuestions[iQuestionPointer].F_Correct = F_Correct;
                //TopicQuestions[iQuestionPointer].Answer_F = Answer_F;
                //TopicQuestions[iQuestionPointer].E_Correct = false;
                //TopicQuestions[iQuestionPointer].Answer_E = "";
                TopicQuestions[iQuestionPointer].F_Correct = false;
                TopicQuestions[iQuestionPointer].Answer_F = "";

                TopicQuestions[iQuestionPointer].Discussion = Discussion;
            }

            // insert the entire TopicQuestions list (it first deletes
            // the existing topicdetail records)
            bool ret = InsertTopicQuestionsFromList(TopicQuestions);

            return ret;

        }

        //Bsk New 
        public static bool InsertTopicQuestion(int TopicID,
           int QuestionNum, int HeaderNum, string Question,
           bool A_Correct, string Answer_A,
           bool B_Correct, string Answer_B,
           bool C_Correct, string Answer_C,
           bool D_Correct, string Answer_D,
           bool E_Correct, string Answer_E,
           string Discussion)
        {
            // get the complete set of 6 questions from the database
            List<TopicQuestion> TopicQuestions = GetTopicQuestionsByTopicID(TopicID);

            TopicQuestion objTopicQuestion = new TopicQuestion(TopicID, QuestionNum, HeaderNum, "", Question, A_Correct, Answer_A,
               B_Correct, Answer_B, C_Correct, Answer_C, D_Correct, Answer_D, E_Correct, Answer_E, false, "", Discussion);
            TopicQuestions.Add(objTopicQuestion);

            // insert the entire TopicQuestions list (it first deletes
            // the existing topicdetail records)
            bool ret = InsertTopicQuestionsFromList(TopicQuestions);

            return ret;

        }


        public static bool InsertTopicQuestion(TopicQuestion topicQuestion)
        {
            List<TopicQuestion> TopicQuestions = GetTopicQuestionsByTopicID(topicQuestion.TopicID);

            TopicQuestions.Add(topicQuestion);

            bool ret = InsertTopicQuestionsFromList(TopicQuestions);

            return ret;
        }

        public static bool UpdateVIQuestion(int TopicID,
           int QuestionNum, string Question,
           bool A_Correct, string Answer_A,
           bool B_Correct, string Answer_B,
           bool C_Correct, string Answer_C,
           bool D_Correct, string Answer_D,
           bool E_Correct, string Answer_E,
           string Discussion)

//            bool E_Correct, string Answer_E,
        //            bool F_Correct, string Answer_F,
        {
            // get the complete set of 6 questions from the database
            List<TopicQuestion> TopicQuestions = GetVIQuestionsByTopicID(TopicID);

            int iQuestionPointer;

            if (QuestionNum > TopicQuestions.Count)
            {
                TopicQuestion objTopicQuestion = new TopicQuestion(TopicID, QuestionNum, 0, "", Question, A_Correct, Answer_A,
                   B_Correct, Answer_B, C_Correct, Answer_C, D_Correct, Answer_D, E_Correct, Answer_E, false, "", Discussion);
                TopicQuestions.Add(objTopicQuestion);
            }
            else
            {
                iQuestionPointer = QuestionNum - 1;
                // update the record for the question number passed in
                TopicQuestions[iQuestionPointer].Question = Question;
                TopicQuestions[iQuestionPointer].A_Correct = A_Correct;
                TopicQuestions[iQuestionPointer].Answer_A = Answer_A;
                TopicQuestions[iQuestionPointer].B_Correct = B_Correct;
                TopicQuestions[iQuestionPointer].Answer_B = Answer_B;
                TopicQuestions[iQuestionPointer].C_Correct = C_Correct;
                TopicQuestions[iQuestionPointer].Answer_C = Answer_C;
                TopicQuestions[iQuestionPointer].D_Correct = D_Correct;
                TopicQuestions[iQuestionPointer].Answer_D = Answer_D;
                TopicQuestions[iQuestionPointer].E_Correct = E_Correct;
                TopicQuestions[iQuestionPointer].Answer_E = Answer_E;
                //TopicQuestions[iQuestionPointer].F_Correct = F_Correct;
                //TopicQuestions[iQuestionPointer].Answer_F = Answer_F;
                //TopicQuestions[iQuestionPointer].E_Correct = false;
                //TopicQuestions[iQuestionPointer].Answer_E = "";
                TopicQuestions[iQuestionPointer].F_Correct = false;
                TopicQuestions[iQuestionPointer].Answer_F = "";

                TopicQuestions[iQuestionPointer].Discussion = Discussion;
            }

            // insert the entire TopicQuestions list (it first deletes
            // the existing topicdetail records)
            bool ret = InsertVIQuestionsFromList(TopicQuestions);

            return ret;

        }



        public static bool InsertVIQuestionsFromList(
            List<TopicQuestion> TopicQuestions)
        {

            List<QTIQuestionObject> QTIQuestions = new List<QTIQuestionObject>();

            int iTopicID = 0;

            // check the count to be sure it's 6 records
            if (TopicQuestions.Count > 0)
            {
                // 6 recs found, so put the info into 6
                // QTIQuestionObject objects in the QTIQuestions list
                int iOrder = 0;
                //string cCorrectAns = "";
                foreach (TopicQuestion record in TopicQuestions)
                {

                    iOrder++;
                    if (iOrder == 1)
                        iTopicID = record.TopicID;

                    QTIQuestionObject QTIQuestion = new QTIQuestionObject();

                    QTIQuestion.cQuestionID = "Topic_" + iTopicID.ToString() + "_Question_" + iOrder.ToString();
                    QTIQuestion.cQuestionTitle = "PearlsReview.com Question # " + iOrder.ToString() +
                        " for Topic # " + iTopicID.ToString();


                    // TODO: Make this dynamic instead of 2 MC and 4 TF
                    if (record.Answer_C == "")
                    {
                        QTIQuestion.cResponseID = "TF_0" + iOrder.ToString();

                    }
                    else
                    {
                        QTIQuestion.cResponseID = "MC_0" + iOrder.ToString();
                    }

                    QTIQuestion.cQuestionText = record.Question;
                    QTIQuestion.cAnswer_A_Text = record.Answer_A;
                    QTIQuestion.cAnswer_B_Text = record.Answer_B;
                    QTIQuestion.cAnswer_C_Text = record.Answer_C;
                    QTIQuestion.cAnswer_D_Text = record.Answer_D;
                    QTIQuestion.cAnswer_E_Text = record.Answer_E;
                    QTIQuestion.cAnswer_F_Text = record.Answer_F;
                    if (record.A_Correct == true)
                        QTIQuestion.cCorrectAnswer = "A";
                    if (record.B_Correct == true)
                        QTIQuestion.cCorrectAnswer = "B";
                    if (record.C_Correct == true)
                        QTIQuestion.cCorrectAnswer = "C";
                    if (record.D_Correct == true)
                        QTIQuestion.cCorrectAnswer = "D";
                    if (record.E_Correct == true)
                        QTIQuestion.cCorrectAnswer = "E";
                    if (record.F_Correct == true)
                        QTIQuestion.cCorrectAnswer = "F";
                    QTIQuestion.cCorrectAnswerFeedback = record.Discussion;
                    QTIQuestion.cUserAnswer = "";

                    QTIQuestions.Add(QTIQuestion);

                }


            }

            string cXML = "";
            bool ret = false;

            if (QTIQuestions.Count > 0 && iTopicID > 0)
            {
                QTIUtils oQTIUtils = new QTIUtils();
                cXML = oQTIUtils.ConvertQTIQuestionObjectListToQTITestXMLString(QTIQuestions);
                ViDefinition oVIDefinition = ViDefinition.GetViDefinitionByTopicID(iTopicID);
                if (oVIDefinition == null)
                {
                    // inserting new record
                    int newID = ViDefinition.InsertViDefinition(iTopicID,
                        cXML, "", 1);
                    if (newID > 0)
                        ret = true;
                }
                else
                {
                    // updating existing record
                    ret = ViDefinition.UpdateViDefinition(oVIDefinition.ViDefID, iTopicID,
                        cXML, oVIDefinition.Vignette_Desc, oVIDefinition.Version + 1);
                }
            }

            return ret;
        }



        public static bool InsertTopicQuestionsFromList(
            List<TopicQuestion> TopicQuestions)
        {

            List<QTIQuestionObject> QTIQuestions = new List<QTIQuestionObject>();

            int iTopicID = 0;

            // check the count to be sure it's 6 records
            if (TopicQuestions.Count > 0)
            {
                // 6 recs found, so put the info into 6
                // QTIQuestionObject objects in the QTIQuestions list
                int iOrder = 0;
                //string cCorrectAns = "";
                foreach (TopicQuestion record in TopicQuestions)
                {

                    iOrder++;
                    //Bsk New
                    //iOrder = record.QuestionNum;
                    if (iOrder == 1)
                        iTopicID = record.TopicID;

                    QTIQuestionObject QTIQuestion = new QTIQuestionObject();

                    QTIQuestion.cQuestionID = "Topic_" + iTopicID.ToString() + "_Question_" + iOrder.ToString();
                    QTIQuestion.cQuestionTitle = "PearlsReview.com Question # " + iOrder.ToString() +
                        " for Topic # " + iTopicID.ToString();

                    //Bsk New 
                    QTIQuestion.cHeaderID = record.HeaderNum.ToString();

                    // TODO: Make this dynamic instead of 2 MC and 4 TF
                    if (record.Answer_C == "")
                    {
                        QTIQuestion.cResponseID = "TF_0" + iOrder.ToString();

                    }
                    else
                    {
                        QTIQuestion.cResponseID = "MC_0" + iOrder.ToString();
                    }

                    QTIQuestion.cQuestionText = record.Question;
                    QTIQuestion.cAnswer_A_Text = record.Answer_A;
                    QTIQuestion.cAnswer_B_Text = record.Answer_B;
                    QTIQuestion.cAnswer_C_Text = record.Answer_C;
                    QTIQuestion.cAnswer_D_Text = record.Answer_D;
                    QTIQuestion.cAnswer_E_Text = record.Answer_E;
                    QTIQuestion.cAnswer_F_Text = record.Answer_F;
                    if (record.A_Correct == true)
                        QTIQuestion.cCorrectAnswer = "A";
                    if (record.B_Correct == true)
                        QTIQuestion.cCorrectAnswer = "B";
                    if (record.C_Correct == true)
                        QTIQuestion.cCorrectAnswer = "C";
                    if (record.D_Correct == true)
                        QTIQuestion.cCorrectAnswer = "D";
                    if (record.E_Correct == true)
                        QTIQuestion.cCorrectAnswer = "E";
                    if (record.F_Correct == true)
                        QTIQuestion.cCorrectAnswer = "F";
                    QTIQuestion.cCorrectAnswerFeedback = record.Discussion;
                    QTIQuestion.cUserAnswer = "";

                    QTIQuestions.Add(QTIQuestion);

                }


            }

            string cXML = "";
            bool ret = false;

            if (QTIQuestions.Count > 0 && iTopicID > 0)
            {
                QTIUtils oQTIUtils = new QTIUtils();
                cXML = oQTIUtils.ConvertQTIQuestionObjectListToQTITestXMLString(QTIQuestions);
                TestDefinition oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(iTopicID);
                if (oTestDefinition == null)
                {
                    // inserting new record
                    int newID = TestDefinition.InsertTestDefinition(iTopicID,
                        cXML, 1);
                    if (newID > 0)
                        ret = true;
                }
                else
                {
                    // updating existing record
                    ret = TestDefinition.UpdateTestDefinition(oTestDefinition.TestDefID, iTopicID,
                        cXML, oTestDefinition.Version + 1);
                }
            }

            return ret;
        }

        ///// <summary>
        ///// Returns a new TopicDetail object
        ///// </summary>
        //private static TopicDetailInfo CreateNewTopicDetailObject(
        //  int TopicID, string Type, string Data, string CorrectAns,
        //    int Order, string Discussion)
        //{
        //    TopicDetailInfo objTopicDetail =
        //      new TopicDetailInfo(TopicID, Type, Data,
        //      CorrectAns, Order, Discussion);

        //    return objTopicDetail;
        //}


        public void UpdateQuestionNumAndTopicID(int Qnum,int id)
        {
            this.QuestionNum = Qnum;
            this.TopicID = id;
        }

        #endregion
    }
}