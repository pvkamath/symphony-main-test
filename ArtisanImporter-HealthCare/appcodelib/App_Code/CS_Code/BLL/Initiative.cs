﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

    /// <summary>
    /// Summary description for Initiative
    /// </summary>
public class Initiative: BasePR
    {
        #region Variables and Properties

        private int _InitID = 0;
        public int InitID
        {
            get { return _InitID; }
            protected set { _InitID = value; }
        }

        private string _InitName = "";
        public string InitName
        {
            get { return _InitName; }
            private set { _InitName = value; }
        }

        private DateTime _StartDate;
        public DateTime StartDate
        {
            get { return _StartDate; }
            private set { _StartDate = value; }
        }

        private DateTime _EndDate;
        public DateTime EndDate
        {
            get { return _EndDate; }
            private set { _EndDate = value; }
        }


        public Initiative(int InitID, string InitName, DateTime StartDate, DateTime EndDate)
        {
            this.InitID = InitID;
            this.InitName = InitName;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
        }


        public bool Delete()
        {
            bool success = Initiative.DeleteInitiative(this.InitID);
            if (success)
                this.InitID = 0;
            return success;
        }

        public bool Update()
        {
            return Initiative.UpdateInitiative(this.InitID, this.InitName, StartDate, EndDate);
        }

        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Initiatives
        /// </summary>
        public static List<Initiative> GetInitiative(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "initname";

            List<Initiative> Initiative = null;
            string key = "Initiative_Initiative_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Initiative = (List<Initiative>)BizObject.Cache[key];
            }
            else
            {
                List<InitiativeInfo> recordset = SiteProvider.PR2.GetInitiative(cSortExpression);
                Initiative = GetInitiativeFromInitiativeInfoList(recordset);
                BasePR.CacheData(key, Initiative);
            }
            return Initiative;
        }
        public static List<Initiative> GetInitiativeBySponsorID(int SponsorID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "initname";

            List<Initiative> Initiative = null;
            string key = "Initiative_Initiative_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Initiative = (List<Initiative>)BizObject.Cache[key];
            }
            else
            {
                List<InitiativeInfo> recordset = SiteProvider.PR2.GetInitiativeBySponsorID(SponsorID, cSortExpression);
                Initiative = GetInitiativeFromInitiativeInfoList(recordset);
                BasePR.CacheData(key, Initiative);
            }
            return Initiative;
        }
        public static List<Initiative> GetUnSelectedInitiativeBySponsorID(int SponsorID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "initname";

            List<Initiative> Initiative = null;
            string key = "Initiative_Initiative_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Initiative = (List<Initiative>)BizObject.Cache[key];
            }
            else
            {
                List<InitiativeInfo> recordset = SiteProvider.PR2.GetUnSelectedInitiativeBySponsorID(SponsorID, cSortExpression);
                Initiative = GetInitiativeFromInitiativeInfoList(recordset);
                BasePR.CacheData(key, Initiative);
            }
            return Initiative;
        }
        /// <summary>
        /// Returns an Initiative object with the specified ID
        /// </summary>
        public static Initiative GetInitiativeByID(int InitID)
        {
            Initiative Initiative = null;
            string key = "Initiative_Initiative_" + InitID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Initiative = (Initiative)BizObject.Cache[key];
            }
            else
            {
                Initiative = GetInitiativeFromInitiativeInfo(SiteProvider.PR2.GetInitiativeByID(InitID));
                BasePR.CacheData(key, Initiative);
            }
            return Initiative;
        }

        /// <summary>
        /// Updates an existing Initiative
        /// </summary>
        public static bool UpdateInitiative(int InitID, string InitName, DateTime StartDate, DateTime EndDate)
        {
            InitName = BizObject.ConvertNullToEmptyString(InitName);
            
            InitiativeInfo record = new InitiativeInfo(InitID, InitName, StartDate, EndDate);
            bool ret = SiteProvider.PR2.UpdateInitiative(record);

            BizObject.PurgeCacheItems("Initiative_Initiative_" + InitID.ToString());
            BizObject.PurgeCacheItems("Initiative_Initiative");
            return ret;
        }

        /// <summary>
        /// Creates a new Initiative
        /// </summary>
        public static int InsertInitiative(int InitID, string InitName, DateTime StartDate, DateTime EndDate)
        {
            InitName = BizObject.ConvertNullToEmptyString(InitName);

            InitiativeInfo record = new InitiativeInfo(InitID, InitName, StartDate, EndDate);
            int ret = SiteProvider.PR2.InsertInitiative(record);

            BizObject.PurgeCacheItems("Initiative_Initiative");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Initiative, but first checks if OK to delete
        /// </summary>
        public static bool DeleteInitiative(int InitID)
        {
            bool IsOKToDelete = OKToDelete(InitID);
            if (IsOKToDelete)
            {
                return (bool)DeleteInitiative(InitID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Initiative - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteInitiative(int InitID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteInitiative(InitID);
            BizObject.PurgeCacheItems("Initiative_Initiative");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Initiative can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int InitID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Initiative object filled with the data taken from the input InitiativeInfo
        /// </summary>
        private static Initiative GetInitiativeFromInitiativeInfo(InitiativeInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Initiative(record.InitID, record.InitName, record.StartDate, record.EndDate);
            }
        }

        /// <summary>
        /// Returns a list of Initiative objects filled with the data taken from the input list of InitiativeInfo
        /// </summary>
        private static List<Initiative> GetInitiativeFromInitiativeInfoList(List<InitiativeInfo> recordset)
        {
            List<Initiative> Initiative = new List<Initiative>();
            foreach (InitiativeInfo record in recordset)
                Initiative.Add(GetInitiativeFromInitiativeInfo(record));
            return Initiative;
        }

        #endregion
    }
}
