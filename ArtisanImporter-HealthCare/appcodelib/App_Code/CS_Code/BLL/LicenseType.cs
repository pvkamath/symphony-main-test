﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for LicenseType
    /// </summary>
    public class LicenseType : BasePR
    {
        #region Variables and Properties

        private int _License_Type_ID = 0;
        public int License_Type_ID
        {
            get { return _License_Type_ID; }
            protected set { _License_Type_ID = value; }
        }

        private string _Description = "";
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        //added for edit

        private string _LicenseCode = "";
        public string LicenseCode
        {
            get { return _LicenseCode; }
            private set { _LicenseCode = value; }
        }



        private string _Instruction = "";
        public string Instruction
        {
            get { return _Instruction; }
            set { _Instruction = value; }
        }

        public LicenseType(int License_Type_ID, string Description, string LicenseCode, string instruction)
        {
            this.License_Type_ID = License_Type_ID;
            this.LicenseCode = LicenseCode;
            this.Description = Description;
            this.Instruction = instruction;
        }

        public bool Delete()
        {
            bool success = LicenseType.DeleteLicenseType(this.License_Type_ID);
            if (success)
                this.License_Type_ID = 0;
            return success;
        }

        public bool Update()
        {
            return LicenseType.UpdateLicenseType(this.License_Type_ID, this.Description, this.LicenseCode, this.Instruction);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all LicenseTypes
        /// </summary>
        public static List<LicenseType> GetLicenseTypes(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Description";

            List<LicenseType> LicenseTypes = null;
            string key = "LicenseTypes_LicenseTypes_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LicenseTypes = (List<LicenseType>)BizObject.Cache[key];
            }
            else
            {
                List<LicenseTypeInfo> recordset = SiteProvider.PR2.GetLicenseTypes(cSortExpression);
                LicenseTypes = GetLicenseTypeListFromLicenseTypeInfoList(recordset);
                BasePR.CacheData(key, LicenseTypes);
            }
            return LicenseTypes;
        }



        /// <summary>
        /// Returns a collection with all LicenseTypes for edit added 11/1/2012
        /// </summary>
        public static List<LicenseType> GetLicenseTypesForEdit(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Description";

            List<LicenseType> LicenseTypes = null;
            string key = "LicenseTypes_LicenseTypes_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LicenseTypes = (List<LicenseType>)BizObject.Cache[key];
            }
            else
            {
                List<LicenseTypeInfo> recordset = SiteProvider.PR2.GetLicenseTypesForEdit(cSortExpression);
                LicenseTypes = GetLicenseTypeListFromLicenseTypeInfoList(recordset);
                BasePR.CacheData(key, LicenseTypes);
            }
            return LicenseTypes;
        }


        /// <summary>
        /// Returns the number of total LicenseTypes
        /// </summary>
        public static int GetLicenseTypeCount()
        {
            int LicenseTypeCount = 0;
            string key = "LicenseTypes_LicenseTypeCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LicenseTypeCount = (int)BizObject.Cache[key];
            }
            else
            {
                LicenseTypeCount = SiteProvider.PR2.GetLicenseTypeCount();
                BasePR.CacheData(key, LicenseTypeCount);
            }
            return LicenseTypeCount;
        }

        /// <summary>
        /// Returns a LicenseType object with the specified ID
        /// </summary>
        public static LicenseType GetLicenseTypeByID(int License_Type_ID)
        {
            LicenseType LicenseType = null;
            string key = "LicenseTypes_LicenseType_" + License_Type_ID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LicenseType = (LicenseType)BizObject.Cache[key];
            }
            else
            {
                LicenseType = GetLicenseTypeFromLicenseTypeInfo(SiteProvider.PR2.GetLicenseTypeByID(License_Type_ID));
                BasePR.CacheData(key, LicenseType);
            }
            return LicenseType;
        }

        /// <summary>
        /// Updates an existing LicenseType
        /// </summary>
        public static bool UpdateLicenseType(int License_Type_ID, string Description, string LicenseCode, string instruction)
        {
            Description = BizObject.ConvertNullToEmptyString(Description);
            LicenseCode = BizObject.ConvertNullToEmptyString(LicenseCode);
            instruction = BizObject.ConvertNullToEmptyString(instruction);

            LicenseTypeInfo record = new LicenseTypeInfo(License_Type_ID, Description, LicenseCode, instruction);
            bool ret = SiteProvider.PR2.UpdateLicenseType(record);

            BizObject.PurgeCacheItems("LicenseTypes_LicenseType_" + License_Type_ID.ToString());
            BizObject.PurgeCacheItems("LicenseTypes_LicenseTypes");
            return ret;
        }

        /// <summary>
        /// Creates a new LicenseType
        /// </summary>
        public static int InsertLicenseType(string Description, string LicenseCode, string instruction)
        {
            Description = BizObject.ConvertNullToEmptyString(Description);
            LicenseCode = BizObject.ConvertNullToEmptyString(LicenseCode);
            instruction = BizObject.ConvertNullToEmptyString(instruction);

            LicenseTypeInfo record = new LicenseTypeInfo(0, Description, LicenseCode, instruction);
            int ret = SiteProvider.PR2.InsertLicenseType(record);

            BizObject.PurgeCacheItems("LicenseTypes_LicenseTypes");
            return ret;
        }

        /// <summary>
        /// Deletes an existing LicenseType, but first checks if OK to delete
        /// </summary>
        public static bool DeleteLicenseType(int License_Type_ID)
        {
            bool IsOKToDelete = OKToDelete(License_Type_ID);
            if (IsOKToDelete)
            {
                return (bool)DeleteLicenseType(License_Type_ID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing LicenseType - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteLicenseType(int License_Type_ID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteLicenseType(License_Type_ID);
            //         new RecordDeletedEvent("LicenseType", License_Type_ID, null).Raise();
            BizObject.PurgeCacheItems("LicenseTypes_LicenseTypes");
            return ret;
        }



        /// <summary>
        /// Checks to see if a LicenseType can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int License_Type_ID)
        {
            return true;
        }



        /// <summary>
        /// Returns a LicenseType object filled with the data taken from the input LicenseTypeInfo
        /// </summary>
        private static LicenseType GetLicenseTypeFromLicenseTypeInfo(LicenseTypeInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new LicenseType(record.License_Type_ID, record.Description, record.LicenseCode, record.Instruction);
            }
        }

        /// <summary>
        /// Returns a list of LicenseType objects filled with the data taken from the input list of LicenseTypeInfo
        /// </summary>
        private static List<LicenseType> GetLicenseTypeListFromLicenseTypeInfoList(List<LicenseTypeInfo> recordset)
        {
            List<LicenseType> LicenseTypes = new List<LicenseType>();
            foreach (LicenseTypeInfo record in recordset)
                LicenseTypes.Add(GetLicenseTypeFromLicenseTypeInfo(record));
            return LicenseTypes;
        }

        #endregion
    }
}
