﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

    /// <summary>
    /// Summary description for DocumentLink
    /// </summary>
    public class DocumentLink : BasePR
    {
        #region Variables and Properties

        private int _DocumentLinkID = 0;
        public int DocumentLinkID
        {
            get { return _DocumentLinkID; }
            protected set { _DocumentLinkID = value; }
        }

        private int _DocumentID = 0;
        public int DocumentID
        {
            get { return _DocumentID; }
            set { _DocumentID = value; }
        }

        private int _RelationID = 0;
        public int RelationID
        {
            get { return _RelationID; }
            set { _RelationID = value; }
        }

        private string _RelationType = "";
        public string RelationType
        {
            get { return _RelationType; }
            set { _RelationType = value; }
        }

        public DocumentLink(int DocumentLinkID, int DocumentID, int RelationID, string RelationType)
        {
            this.DocumentLinkID = DocumentLinkID;
            this.DocumentID = DocumentID;
            this.RelationID = RelationID;
            this.RelationType = RelationType;
        }

        public bool Delete()
        {
            bool success = DocumentLink.DeleteDocumentLink(this.DocumentLinkID);
            if (success)
                this.DocumentLinkID = 0;
            return success;
        }

        public bool Update()
        {
            return DocumentLink.UpdateDocumentLink(this.DocumentLinkID, this.DocumentID, this.RelationID,

            this.RelationType);
        }
        #endregion
        

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all DocumentLinks
        /// </summary>
        public static List<DocumentLink> GetDocumentLinks(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<DocumentLink> DocumentLinks = null;
            string key = "DocumentLinks_DocumentLinks_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                DocumentLinks = (List<DocumentLink>)BizObject.Cache[key];
            }
            else
            {
                List<DocumentLinkInfo> recordset = SiteProvider.PR2.GetDocumentLinks(cSortExpression);
                DocumentLinks = GetDocumentLinkListFromDocumentLinkInfoList(recordset);
                BasePR.CacheData(key, DocumentLinks);
            }
            return DocumentLinks;
        }

        public static List<DocumentLink> GetDocumentLinksByTopicId(int ceTopicId)
        {

            List<DocumentLink> DocumentLinks = null;
            string key = "DocumentLinks_DocumentLinks_" + ceTopicId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                DocumentLinks = (List<DocumentLink>)BizObject.Cache[key];
            }
            else
            {
                List<DocumentLinkInfo> recordset = SiteProvider.PR2.GetDocumentsLinksByTopicId(ceTopicId);
                DocumentLinks = GetDocumentLinkListFromDocumentLinkInfoList(recordset);
                BasePR.CacheData(key, DocumentLinks);
            }
            return DocumentLinks;
 
        }


        /// <summary>
        /// Returns the number of total DocumentLinks
        /// </summary>
        public static int GetDocumentLinkCount()
        {
            int DocumentLinkCount = 0;
            string key = "DocumentLinks_DocumentLinkCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                DocumentLinkCount = (int)BizObject.Cache[key];
            }
            else
            {
                DocumentLinkCount = SiteProvider.PR2.GetDocumentLinkCount();
                BasePR.CacheData(key, DocumentLinkCount);
            }
            return DocumentLinkCount;
        }

        /// <summary>
        /// Returns the number of total DocumentLinks for a DocumentID
        /// </summary>
        public static int GetDocumentLinkCountByDocumentID(int DocumentID)
        {
            int DocumentLinkCount = 0;
            string key = "DocumentLinks_DocumentLinkCountByDocumentID_" + DocumentID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                DocumentLinkCount = (int)BizObject.Cache[key];
            }
            else
            {
                DocumentLinkCount = SiteProvider.PR2.GetDocumentLinkCountByDocumentID(DocumentID);
                BasePR.CacheData(key, DocumentLinkCount);
            }
            return DocumentLinkCount;
        }

        /// <summary>
        /// Returns a DocumentLink object with the specified ID
        /// </summary>
        public static DocumentLink GetDocumentLinkByID(int DocumentLinkID)
        {
            DocumentLink DocumentLink = null;
            string key = "DocumentLinks_DocumentLink_" + DocumentLinkID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                DocumentLink = (DocumentLink)BizObject.Cache[key];
            }
            else
            {
                DocumentLink = GetDocumentLinkFromDocumentLinkInfo(SiteProvider.PR2.GetDocumentLinkByID(DocumentLinkID));
                BasePR.CacheData(key, DocumentLink);
            }
            return DocumentLink;
        }

        /// <summary>
        /// Updates an existing DocumentLink
        /// </summary>
        public static bool UpdateDocumentLink(int DocumentLinkID, int DocumentID, int RelationID, string RelationType)
        {
            DocumentLinkInfo record = new DocumentLinkInfo(DocumentLinkID, DocumentID, RelationID, RelationType);
            bool ret = SiteProvider.PR2.UpdateDocumentLink(record);

            BizObject.PurgeCacheItems("DocumentLinks_DocumentLink_" + DocumentLinkID.ToString());
            BizObject.PurgeCacheItems("DocumentLinks_DocumentLinks");
            return ret;
        }


        /// <summary>
        /// Creates a new DocumentLink
        /// </summary>
        public static int InsertDocumentLink(int DocumentID, int RelationID, string RelationType)
        {
            DocumentLinkInfo record = new DocumentLinkInfo(0, DocumentID, RelationID, RelationType);
            int ret = SiteProvider.PR2.InsertDocumentLink(record);

            BizObject.PurgeCacheItems("DocumentLinks_DocumentLink");
            return ret;
        }

        /// <summary>
        /// Deletes an existing DocumentLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteDocumentLink(int DocumentLinkID)
        {
            bool IsOKToDelete = OKToDelete(DocumentLinkID);
            if (IsOKToDelete)
            {
                return (bool)DeleteDocumentLink(DocumentLinkID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing DocumentLink - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteDocumentLink(int DocumentLinkID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteDocumentLink(DocumentLinkID);
            //         new RecordDeletedEvent("DocumentLink", DocumentLinkID, null).Raise();
            BizObject.PurgeCacheItems("DocumentLinks_DocumentLink");
            return ret;
        }



        /// <summary>
        /// Checks to see if a DocumentLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int DocumentLinkID)
        {
            return true;
        }



        /// <summary>
        /// Returns a DocumentLink object filled with the data taken from the input DocumentLinkInfo
        /// </summary>
        private static DocumentLink GetDocumentLinkFromDocumentLinkInfo(DocumentLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new DocumentLink(record.DocumentLinkID, record.DocumentID, record.RelationID, record.RelationType);
            }
        }

        /// <summary>
        /// Returns a list of DocumentLink objects filled with the data taken from the input list of DocumentLinkInfo
        /// </summary>
        private static List<DocumentLink> GetDocumentLinkListFromDocumentLinkInfoList(List<DocumentLinkInfo> recordset)
        {
            List<DocumentLink> DocumentLinks = new List<DocumentLink>();
            foreach (DocumentLinkInfo record in recordset)
                DocumentLinks.Add(GetDocumentLinkFromDocumentLinkInfo(record));
            return DocumentLinks;
        }
    
        #endregion
    }
}

