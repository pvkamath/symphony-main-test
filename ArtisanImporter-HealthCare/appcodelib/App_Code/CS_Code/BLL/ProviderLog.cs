﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for ProviderLog
    /// </summary>
    public class ProviderLog
    {
        public ProviderLog()
        {
            //
            // TODO: Add constructor logic here
            //
        }


        private int _plid;
        public int plid
        {
            get
            {
                return _plid;
            }
            private set
            {
                _plid = value;
            }
        }

        private int _userid;

        public int userid
        {
            get
            {
                return _userid;
            }
            set
            {
                _userid = value;
            }
        }

        private int _orderitemid;

        public int orderitemid
        {
            get
            {
                return _orderitemid;
            }
            set
            {
                _orderitemid = value;
            }
        }

        private int _discountid;

        public int discountid
        {
            get
            {
                return _discountid;
            }
            set
            {
                _discountid = value;
            }
        }


        private DateTime _feeddate;
        public DateTime feeddate
        {
            get
            {
                return _feeddate;
            }

            set
            {
                _feeddate = value;
            }
        }

        private string _provider;
        public string provider
        {
            get
            {
                return _provider;
            }
            set
            {
                _provider = value;
            }
        }

        private string _responsetype;
        public string responsetype
        {
            get
            {
                return _responsetype;
            }
            set
            {
                _responsetype = value;
            }
        }

        private string _responsetext;
        public string responsetext
        {
            get
            {
                return _responsetext;
            }
            set
            {
                _responsetext = value;
            }
        }

        public ProviderLog(int plid, int userid, int orderitemid, int discountid, DateTime feeddate, string provider, string responsetype, string responsetext)
        {
            this.plid = plid;
            this.userid = userid;
            this.orderitemid = orderitemid;
            this.discountid = discountid;
            this.feeddate = feeddate;
            this.provider = provider;
            this.responsetype = responsetype;
            this.responsetext = responsetext;

        }


        /// <summary>
        /// Creates new orders
        /// </summary>
        public static int InsertProviderLog(int plid, int userid, int orderitemid, int discountid, DateTime feeddate, string provider, string responsetype, string responsetext)
        {
            ProviderLogInfo record = new ProviderLogInfo(0, userid, orderitemid, discountid, feeddate, provider,
            responsetype, responsetext);
            int ret = SiteProvider.PR2.InsertProviderLog(record);
           return ret;
        }  
    }
}
