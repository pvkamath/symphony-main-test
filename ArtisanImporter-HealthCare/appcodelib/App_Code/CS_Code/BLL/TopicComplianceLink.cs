﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for TopicComplianceLink
    /// </summary>
    public class TopicComplianceLink : BasePR
    {
        #region Variables and Properties

        private int _TopCompID = 0;
        public int TopCompID
        {
            get { return _TopCompID; }
            protected set { _TopCompID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _FacID = 0;
        public int FacID
        {
            get { return _FacID; }
            set { _FacID = value; }
        }

        private string _ComplFile = "";
        public string ComplFile
        {
            get { return _ComplFile; }
            set { _ComplFile = value; }
        }


        public TopicComplianceLink(int TopCompID, int TopicID, int FacID, string ComplFile)
        {
            this.TopCompID = TopCompID;
            this.TopicID = TopicID;
            this.FacID = FacID;
            this.ComplFile = ComplFile;
        }

        public bool Delete()
        {
            bool success = TopicComplianceLink.DeleteTopicComplianceLink(this.TopCompID);
            if (success)
                this.TopCompID = 0;
            return success;
        }

        public bool Update()
        {
            return TopicComplianceLink.UpdateTopicComplianceLink(this.TopCompID, this.TopicID, this.FacID, this.ComplFile);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all TopicComplianceLinks
        /// </summary>
        public static List<TopicComplianceLink> GetTopicComplianceLinks(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<TopicComplianceLink> TopicComplianceLinks = null;
            string key = "TopicComplianceLinks_TopicComplianceLinks_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicComplianceLinks = (List<TopicComplianceLink>)BizObject.Cache[key];
            }
            else
            {
                List<TopicComplianceLinkInfo> recordset = SiteProvider.PR2.GetTopicComplianceLinks(cSortExpression);
                TopicComplianceLinks = GetTopicComplianceLinkListFromTopicComplianceLinkInfoList(recordset);
                BasePR.CacheData(key, TopicComplianceLinks);
            }
            return TopicComplianceLinks;
        }

        /// <summary>
        /// Returns a collection with all TopicComplianceLinks for a FacilityID
        /// </summary>
        public static List<TopicComplianceLink> GetTopicComplianceLinksByFacilityID(int FacilityID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<TopicComplianceLink> TopicComplianceLinks = null;
            string key = "TopicComplianceLinks_TopicComplianceLinksByFacilityID_" +
                FacilityID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicComplianceLinks = (List<TopicComplianceLink>)BizObject.Cache[key];
            }
            else
            {
                List<TopicComplianceLinkInfo> recordset =
                    SiteProvider.PR2.GetTopicComplianceLinksByFacilityID(FacilityID, cSortExpression);
                TopicComplianceLinks = GetTopicComplianceLinkListFromTopicComplianceLinkInfoList(recordset);
                BasePR.CacheData(key, TopicComplianceLinks);
            }
            return TopicComplianceLinks;
        }


        /// <summary>
        /// Returns the number of total TopicComplianceLinks
        /// </summary>
        public static int GetTopicComplianceLinkCount()
        {
            int TopicComplianceLinkCount = 0;
            string key = "TopicComplianceLinks_TopicComplianceLinkCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicComplianceLinkCount = (int)BizObject.Cache[key];
            }
            else
            {
                TopicComplianceLinkCount = SiteProvider.PR2.GetTopicComplianceLinkCount();
                BasePR.CacheData(key, TopicComplianceLinkCount);
            }
            return TopicComplianceLinkCount;
        }

        /// <summary>
        /// Returns a TopicComplianceLink object with the specified ID
        /// </summary>
        public static TopicComplianceLink GetTopicComplianceLinkByID(int TopCompID)
        {
            TopicComplianceLink TopicComplianceLink = null;
            string key = "TopicComplianceLinks_TopicComplianceLink_" + TopCompID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicComplianceLink = (TopicComplianceLink)BizObject.Cache[key];
            }
            else
            {
                TopicComplianceLink = GetTopicComplianceLinkFromTopicComplianceLinkInfo(SiteProvider.PR2.GetTopicComplianceLinkByID(TopCompID));
                BasePR.CacheData(key, TopicComplianceLink);
            }
            return TopicComplianceLink;
        }

        /// <summary>
        /// Updates an existing TopicComplianceLink
        /// </summary>
        public static bool UpdateTopicComplianceLink(int TopCompID, int TopicID, int FacID, string ComplFile)
        {
            ComplFile = BizObject.ConvertNullToEmptyString(ComplFile);


            TopicComplianceLinkInfo record = new TopicComplianceLinkInfo(TopCompID, TopicID, FacID, ComplFile);
            bool ret = SiteProvider.PR2.UpdateTopicComplianceLink(record);

            BizObject.PurgeCacheItems("TopicComplianceLinks_TopicComplianceLink_" + TopCompID.ToString());
            BizObject.PurgeCacheItems("TopicComplianceLinks_TopicComplianceLinks");
            return ret;
        }

        /// <summary>
        /// Creates a new TopicComplianceLink
        /// </summary>
        public static int InsertTopicComplianceLink(int TopicID, int FacID, string ComplFile)
        {
            ComplFile = BizObject.ConvertNullToEmptyString(ComplFile);


            TopicComplianceLinkInfo record = new TopicComplianceLinkInfo(0, TopicID, FacID, ComplFile);
            int ret = SiteProvider.PR2.InsertTopicComplianceLink(record);

            BizObject.PurgeCacheItems("TopicComplianceLinks_TopicComplianceLink");
            return ret;
        }

        /// <summary>
        /// Deletes an existing TopicComplianceLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteTopicComplianceLink(int TopCompID)
        {
            bool IsOKToDelete = OKToDelete(TopCompID);
            if (IsOKToDelete)
            {
                return (bool)DeleteTopicComplianceLink(TopCompID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing TopicComplianceLink - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteTopicComplianceLink(int TopCompID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteTopicComplianceLink(TopCompID);
            //         new RecordDeletedEvent("TopicComplianceLink", TopCompID, null).Raise();
            BizObject.PurgeCacheItems("TopicComplianceLinks_TopicComplianceLink");
            return ret;
        }



        /// <summary>
        /// Checks to see if a TopicComplianceLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int TopCompID)
        {
            return true;
        }



        /// <summary>
        /// Returns a TopicComplianceLink object filled with the data taken from the input TopicComplianceLinkInfo
        /// </summary>
        private static TopicComplianceLink GetTopicComplianceLinkFromTopicComplianceLinkInfo(TopicComplianceLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new TopicComplianceLink(record.TopCompID, record.TopicID, record.FacID, record.ComplFile);
            }
        }

        /// <summary>
        /// Returns a list of TopicComplianceLink objects filled with the data taken from the input list of TopicComplianceLinkInfo
        /// </summary>
        private static List<TopicComplianceLink> GetTopicComplianceLinkListFromTopicComplianceLinkInfoList(List<TopicComplianceLinkInfo> recordset)
        {
            List<TopicComplianceLink> TopicComplianceLinks = new List<TopicComplianceLink>();
            foreach (TopicComplianceLinkInfo record in recordset)
                TopicComplianceLinks.Add(GetTopicComplianceLinkFromTopicComplianceLinkInfo(record));
            return TopicComplianceLinks;
        }

        #endregion
    }
}
