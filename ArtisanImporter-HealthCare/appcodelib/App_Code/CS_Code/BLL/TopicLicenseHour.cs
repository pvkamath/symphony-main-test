﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;


namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr2
    /// </summary>

    /// <summary>
    /// /////////Media Content
    /// </summary>
    public class TopicLicenseHour : BasePR
    {

        public TopicLicenseHour(int tlhid, int topicid, int Lpuid, decimal Th_hours, string Th_provider)
        {
            this.tlhid = tlhid;
            this.topicid = topicid;
            this.Lpuid = Lpuid;
            this.Th_hours = Th_hours;
            this.Th_provider = Th_provider;        
        }

        private int _tlhid = 0;
        public int tlhid
        {
            get { return _tlhid; }
            protected set { _tlhid = value; }
        }

        private int _topicid = 0;
        public int topicid
        {
            get { return _topicid; }
            protected set { _topicid = value; }
        }

        private int _Lpuid = 0;
        public int Lpuid
        {
            get { return _Lpuid; }
            protected set { _Lpuid = value; }
        }

        private decimal _Th_hours=0.00M;    
        public decimal Th_hours
        {
            get { return _Th_hours; }
            protected set { _Th_hours = value; }
        }
        private string _Th_provider = "";
        public string Th_provider
        {
            get { return _Th_provider; }
            protected set { _Th_provider = value; }
        }

        public static bool UpdateTopicLicenseHour(int tlhid, int topicid, int Lpuid, decimal Th_hours, string Th_provider)
        {
            TopicLicenseHourInfo record = new TopicLicenseHourInfo(tlhid,topicid, Lpuid, Th_hours, Th_provider);
            bool ret = SiteProvider.PR2.UpdateTopicLicenseHour(record);

            BizObject.PurgeCacheItems("TopicLicenseHours_TopicLicenseHour_" + tlhid.ToString());
            BizObject.PurgeCacheItems("TopicLicenseHours_TopicLicenseHours");
            return ret;
        }

        /// <summary>
        /// Returns a Topic object with the specified ID
        /// </summary>
        public static TopicLicenseHour GetTopicLicenseHourByID(int tlhid)
        {
            TopicLicenseHour TopicLicenseHour = null;
            string key = "TopicLicenseHours_TopicLicenseHour_" + tlhid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicLicenseHour = (TopicLicenseHour)BizObject.Cache[key];
            }
            else
            {
                TopicLicenseHour = GetTopicLicenseHourFromTopicLicenseHourInfo(SiteProvider.PR2.GetTopicLicenseHourByID(tlhid));
                BasePR.CacheData(key, TopicLicenseHour);
            }
            return TopicLicenseHour;
        }

        /// <summary>
        /// Creates a new TopicComplianceLink
        /// </summary>
        public static int InsertTopicLicenseHour(int topicid, int Lpuid, decimal Th_hours, string Th_provider)
        {
            TopicLicenseHourInfo record = new TopicLicenseHourInfo(0, topicid,  Lpuid,  Th_hours, Th_provider);
            int ret = SiteProvider.PR2.InsertTopicLicenseHour(record);

            BizObject.PurgeCacheItems("TopicLicenseHours_TopicLicenseHour");
            return ret;
        }

        /// <summary>
        /// Deletes an existing TopicComplianceLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteTopicLicenseHour(int tlhid)
        {
            bool IsOKToDelete = OKToDelete(tlhid);
            if (IsOKToDelete)
            {
                return (bool)DeleteTopicLicenseHour(tlhid, true);
            }
            else
            {
                return false;
            }
        }


        public static bool DeleteTopicLicenseHour(int tlhid, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteTopicLicenseHour(tlhid);           
            BizObject.PurgeCacheItems("TopicLicenseHours_TopicLicenseHour");
            return ret;
        }



        /// <summary>
        /// Checks to see if a TopicComplianceLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int Lpuid)
        {
            return true;

        }


        /// <summary>
        /// Returns a collection with all TopicComplianceLinks for a FacilityID
        /// </summary>
        public static List<TopicLicenseHour> GetTopicLicenseHourByTopicID(int TopicId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<TopicLicenseHour> TopicLicenseHours = null;
            string key = "TopicLicenseHours_TopicLicenseHourByTopicID_" +
                TopicId.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicLicenseHours = (List<TopicLicenseHour>)BizObject.Cache[key];
            }
            else
            {
                List<TopicLicenseHourInfo> recordset =
                    SiteProvider.PR2.GetTopicLicenseHourByTopicID(TopicId, cSortExpression);
                TopicLicenseHours = GetTopicLicenseHourListFromTopicLicenseHourInfoList(recordset);
                BasePR.CacheData(key, TopicLicenseHours);
            }
            return TopicLicenseHours;
        }

        public static System.Data.DataSet GetLicenseTypesbyLicenseProfUnit()
        {            
            return SiteProvider.PR2.GetLicenseTypesbyLicenseProfUnit();
        }
        public static System.Data.DataSet GetTopicLicenseHourAndLicenseByTopicID(int TopicID, string cSortExpression)
        {
            return SiteProvider.PR2.GetTopicLicenseHourAndLicenseByTopicID(TopicID, cSortExpression);
        }

        /// <summary>
        /// Returns a TopicComplianceLink object filled with the data taken from the input TopicComplianceLinkInfo
        /// </summary>
        private static TopicLicenseHour GetTopicLicenseHourFromTopicLicenseHourInfo(TopicLicenseHourInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new TopicLicenseHour(record.tlhid,record.topicid, record.Lpuid, record.Th_hours, record.Th_provider);
            }
        }

        /// <summary>
        /// Returns a list of TopicComplianceLink objects filled with the data taken from the input list of TopicComplianceLinkInfo
        /// </summary>
        private static List<TopicLicenseHour> GetTopicLicenseHourListFromTopicLicenseHourInfoList(List<TopicLicenseHourInfo> recordset)
        {
            List<TopicLicenseHour> TopicLicenseHours = new List<TopicLicenseHour>();
            foreach (TopicLicenseHourInfo record in recordset)
                TopicLicenseHours.Add(GetTopicLicenseHourFromTopicLicenseHourInfo(record));
            return TopicLicenseHours;
        }
    }

}