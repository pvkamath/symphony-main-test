﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for UserLicense
/// </summary>
    public class UserLicense : BasePR
    {
        #region Variables and Properties
        private int _LicenseID = 0;
        public int LicenseID
        {
            get { return _LicenseID; }
            protected set { _LicenseID = value; }
        }

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private int _License_Type_ID = 0;
        public int License_Type_ID
        {
            get { return _License_Type_ID; }
            set { _License_Type_ID = value; }
        }

        private string _State = "";
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        private string _License_Number = "";
        public string License_Number
        {
            get { return _License_Number; }
            set { _License_Number = value; }
        }

        private string _Issue_Date = "";
        public string Issue_Date
        {
            get { return _Issue_Date; }
            set { _Issue_Date = value; }
        }

        private string _Expire_Date = "";
        public string Expire_Date
        {
            get { return _Expire_Date; }
            set { _Expire_Date = value; }
        }

        private bool _Report_To_State = false;
        public bool Report_To_State
        {
            get { return _Report_To_State; }
            set { _Report_To_State = value; }
        }

        private string _comment1 = "";
        public string Comment1
        {
            get { return _comment1; }
            set { _comment1 = value; }
        }

        private string _comment2 = "";
        public string Comment2
        {
            get { return _comment2; }
            set { _comment2 = value; }
        }

        public UserLicense(int LicenseID, int UserID, int License_Type_ID, string State,
           string License_Number, string Issue_Date, string Expire_Date, bool Report_To_State, 
            string comment1, string comment2)
        {
            this.LicenseID = LicenseID;
            this.UserID = UserID;
            this.License_Type_ID = License_Type_ID;
            this.State = State;
            this.License_Number = License_Number;
            this.Issue_Date = Issue_Date;
            this.Expire_Date = Expire_Date;
            this.Report_To_State = Report_To_State;
            this.Comment1 = comment1;
            this.Comment2 = comment2;
        }



        public bool Delete()
        {
            bool success = UserLicense.DeleteUserLicense(this.LicenseID);
            if (success)
                this.LicenseID = 0;
            return success;
        }

        public bool Update()
        {
            return UserLicense.UpdateUserLicense(this.LicenseID, this.UserID, this.License_Type_ID, this.State,
           this.License_Number, this.Issue_Date, this.Expire_Date, this.Report_To_State, this.Comment1, this.Comment2);
        }
        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all UserLicenses
        /// </summary>
        public static List<UserLicense> GetUserLicenses(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<UserLicense> UserLicenses = null;
            string key = "UserLicenses_UserLicenses_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserLicenses = (List<UserLicense>)BizObject.Cache[key];
            }
            else
            {
                List<UserLicenseInfo> recordset = SiteProvider.PR2.GetUserLicenses(cSortExpression);
                UserLicenses = GetUserLicenseListFromUserLicenseInfoList(recordset);
                BasePR.CacheData(key, UserLicenses);
            }
            return UserLicenses;
        }

        /// <summary>
        /// Returns a collection with all UserLicenses for a UserID
        /// </summary>
        public static List<UserLicense> GetUserLicensesByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<UserLicense> UserLicenses = null;
            string key = "UserLicenses_UserLicensesByUserID_" + UserID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserLicenses = (List<UserLicense>)BizObject.Cache[key];
            }
            else
            {
                List<UserLicenseInfo> recordset = SiteProvider.PR2.GetUserLicensesByUserID(UserID, cSortExpression);
                UserLicenses = GetUserLicenseListFromUserLicenseInfoList(recordset);
                BasePR.CacheData(key, UserLicenses);
            }
            return UserLicenses;
        }

        /// <summary>
        /// Returns a collection with all UserLicenses for a UserID
        /// </summary>
        public static List<UserLicense> GetTop3UserLicensesByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<UserLicense> UserLicenses = null;
            string key = "UserLicenses_UserLicensesBytop3UserID_" + UserID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserLicenses = (List<UserLicense>)BizObject.Cache[key];
            }
            else
            {
                List<UserLicenseInfo> recordset = SiteProvider.PR2.GetTop3UserLicensesByUserID(UserID, cSortExpression);
                UserLicenses = GetUserLicenseListFromUserLicenseInfoList(recordset);
                BasePR.CacheData(key, UserLicenses);
            }
            return UserLicenses;
        }



        /// <summary>
        /// Returns a UserLicense object with the specified ID
        /// </summary>
        public static UserLicense GetUserLicenseByUserID(int UserID)
        {
            UserLicense UserLicense = null;
            string key = "UserLicenses_UserLicense_" + UserID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserLicense = (UserLicense)BizObject.Cache[key];
            }
            else
            {
                UserLicense = GetUserLicenseFromUserLicenseInfo(SiteProvider.PR2.GetUserLicenseByUserID(UserID));
                BasePR.CacheData(key, UserLicense);
            }
            return UserLicense;
        }      

        /// <summary>
        /// Returns the number of Completed Tests for a FacilityID
        /// </summary>
        public static int GetUserLicenseCountByUserID(int UserID)
        {
            int UserLicenseCount = 0;
            string key = "UserLicenses_UserLicense_" + UserID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserLicenseCount = (int)BizObject.Cache[key];
            }
            else
            {
                UserLicenseCount = SiteProvider.PR2.GetUserLicenseCountByUserID(UserID);
                BasePR.CacheData(key, UserLicenseCount);
            }
            return UserLicenseCount;
        }


        /// <summary>
        /// Returns the number of total UserLicenses
        /// </summary>
        public static int GetUserLicenseCount()
        {
            int UserLicenseCount = 0;
            string key = "UserLicenses_UserLicenseCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserLicenseCount = (int)BizObject.Cache[key];
            }
            else
            {
                UserLicenseCount = SiteProvider.PR2.GetUserLicenseCount();
                BasePR.CacheData(key, UserLicenseCount);
            }
            return UserLicenseCount;
        }

        /// <summary>
        /// Returns a UserLicense object with the specified ID
        /// </summary>
        public static UserLicense GetUserLicenseByID(int LicenseID)
        {
            UserLicense UserLicense = null;
            string key = "UserLicenses_UserLicense_" + LicenseID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserLicense = (UserLicense)BizObject.Cache[key];
            }
            else
            {
                UserLicense = GetUserLicenseFromUserLicenseInfo(SiteProvider.PR2.GetUserLicenseByID(LicenseID));
                BasePR.CacheData(key, UserLicense);
            }
            return UserLicense;
        }

        public static DataSet GetUserLicensesDatasetByUserID(int UserID, string cSortExpression)
        {
            DataSet recordset = null;
            if (cSortExpression == null)
                cSortExpression = "";



            //List<Topic> Topics = null;
            string key = "UserLicenses_UserLicense_Dataset_" + UserID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetUserLicensesDatasetByUserID(UserID, cSortExpression);

            }
            return recordset;
        }

        public static DataSet GetUserLicensesDatasetByUserIDAndState(int UserID, string State, string cSortExpression)
        {
            DataSet recordset = null;
            if (cSortExpression == null)
                cSortExpression = "";

            //List<Topic> Topics = null;
            string key = "UserLicenses_UserLicense_Dataset_" + UserID.ToString() + "_" + State;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                //Topics = (List<Topic>)BizObject.Cache[key];
            }
            else
            {
                // topics by Lecture ID
                recordset = SiteProvider.PR2.GetUserLicensesDatasetByUserID(UserID, cSortExpression);
                DataTable dt;
                if (recordset.Tables.Count > 0)
                {
                    DataRow[] drs = recordset.Tables[0].Select("State='PA'", cSortExpression);
                    if (drs.Length > 0)
                    {
                        dt = drs.CopyToDataTable();                                     
                    }
                    else
                    {
                        dt = recordset.Tables[0].Clone();                        
                    }
                    dt.AcceptChanges();           
                    recordset = new DataSet();
                    recordset.Tables.Add(dt);
                    recordset.AcceptChanges();
                }         
            }
            return recordset;
        }
       
        /// <summary>
        /// Updates an existing UserLicense
        /// </summary>
        public static bool UpdateUserLicense(int LicenseID, int UserID, int License_Type_ID, string State,
           string License_Number, string Issue_Date, string Expire_Date, bool Report_To_State, 
            string comment1, string comment2)
        {
            bool ret = false;
            if (comment1 == null)
                comment1 = "";
            if (comment2 == null)
                comment2 = "";

            UserLicenseInfo record = new UserLicenseInfo(LicenseID, UserID, License_Type_ID, State,
                  License_Number, Issue_Date, Expire_Date, Report_To_State, comment1, comment2);
            ret = SiteProvider.PR2.UpdateUserLicense(record);
            BizObject.PurgeCacheItems("UserLicenses_UserLicense_" + LicenseID.ToString());
            BizObject.PurgeCacheItems("UserLicenses_UserLicenses");

            return ret;

            ////Bhaskar Commented

            //  if (State == "FL")
            //  {
            //      int pos1 = License_Number.IndexOf(" ", 0);

            //      int pos = License_Number.IndexOf("-", 0);
            //      if (pos > 0 || pos1 > 0)
            //      {
            //          Exception ex = new Exception("Please correct the License Number");
            //          throw ex;
            //       }
            //      else
            //      {
            //          UserLicenseInfo record = new UserLicenseInfo(LicenseID, UserID, License_Type_ID, State,
            //         License_Number, Issue_Date, Expire_Date, Report_To_State);
            //          ret = SiteProvider.PR.UpdateUserLicense(record);
            //          BizObject.PurgeCacheItems("UserLicenses_UserLicense_" + LicenseID.ToString());
            //          BizObject.PurgeCacheItems("UserLicenses_UserLicenses");
            //      }
            //  }
            //  else
            //  {
            //      UserLicenseInfo record = new UserLicenseInfo(LicenseID, UserID, License_Type_ID, State,
            //      License_Number, Issue_Date, Expire_Date, Report_To_State);
            //      ret = SiteProvider.PR.UpdateUserLicense(record);
            //      BizObject.PurgeCacheItems("UserLicenses_UserLicense_" + LicenseID.ToString());
            //      BizObject.PurgeCacheItems("UserLicenses_UserLicenses");
            //  }

            //return ret;

            //return ret;
        }

        public static bool UpdateUserLicense(int LicenseID, int UserID, int License_Type_ID, string State,
           string License_Number, string Issue_Date, string Expire_Date, bool Report_To_State)
        {
            return UpdateUserLicense(LicenseID, UserID, License_Type_ID, State, 
                License_Number, Issue_Date, Expire_Date, Report_To_State, "", "");
        }

        /// <summary>
        /// Creates a new UserLicense
        /// </summary>
        public static int InsertUserLicense(int UserID, int License_Type_ID, string State,
           string License_Number, string Issue_Date, string Expire_Date, bool Report_To_State,
            string comment1, string comment2)
        {
            int ret = 0;
            if (comment1 == null)
                comment1 = "";
            if (comment2 == null)
                comment2 = "";

            UserLicenseInfo record = new UserLicenseInfo(0, UserID, License_Type_ID, State,
               License_Number, Issue_Date, Expire_Date, Report_To_State, comment1, comment2);
            ret = SiteProvider.PR2.InsertUserLicense(record);
            BizObject.PurgeCacheItems("UserLicenses_UserLicense");
            return ret;

            ////Bhaskar Commented

            //if (State == "FL")
            //{
            //    int pos1 = License_Number.IndexOf(" ", 0);

            //    int pos = License_Number.IndexOf("-",0);
            //    if (pos > 0 || pos1 > 0)
            //    {
            //        ret = -1;
            //        Exception ex = new Exception("Please correct the License Number");
            //        throw ex;
            //        BizObject.PurgeCacheItems("UserLicenses_UserLicense");

            //    }
            //    else
            //    {
            //        UserLicenseInfo record = new UserLicenseInfo(0, UserID, License_Type_ID, State,
            //       License_Number, Issue_Date, Expire_Date, Report_To_State);
            //        ret = SiteProvider.PR.InsertUserLicense(record);
            //        BizObject.PurgeCacheItems("UserLicenses_UserLicense");

            //    }
            //}
            //else
            //{

            //    UserLicenseInfo record = new UserLicenseInfo(0, UserID, License_Type_ID, State,
            //   License_Number, Issue_Date, Expire_Date, Report_To_State);
            //    ret = SiteProvider.PR.InsertUserLicense(record);
            //    BizObject.PurgeCacheItems("UserLicenses_UserLicense");

            //}

            //return ret;
        }

        public static int InsertUserLicense(int UserID, int License_Type_ID, string State,
           string License_Number, string Issue_Date, string Expire_Date, bool Report_To_State)
        {
            return InsertUserLicense(UserID, License_Type_ID, State, License_Number, 
                Issue_Date, Expire_Date, Report_To_State, "", "");
        }



        /// <summary>
        /// Deletes an existing UserLicense, but first checks if OK to delete
        /// </summary>
        public static bool DeleteUserLicense(int LicenseID)
        {
            bool IsOKToDelete = OKToDelete(LicenseID);
            if (IsOKToDelete)
            {
                return (bool)DeleteUserLicense(LicenseID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing UserLicense - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteUserLicense(int LicenseID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteUserLicense(LicenseID);
            //         new RecordDeletedEvent("UserLicense", UserIntID, null).Raise();
            BizObject.PurgeCacheItems("UserLicenses_UserLicense");
            return ret;
        }



        /// <summary>
        /// Checks to see if a UserLicense can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int LicenseID)
        {
            return true;
        }



        /// <summary>
        /// Returns a UserLicense object filled with the data taken from the input UserLicenseInfo
        /// </summary>
        private static UserLicense GetUserLicenseFromUserLicenseInfo(UserLicenseInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new UserLicense(record.LicenseID, record.UserID, record.License_Type_ID, record.State,
           record.License_Number, record.Issue_Date, record.Expire_Date, record.Report_To_State, record.Comment1, 
           record.Comment2);
            }
        }

        /// <summary>
        /// Returns a list of UserLicense objects filled with the data taken from the input list of UserInterestLinkInfo
        /// </summary>
        private static List<UserLicense> GetUserLicenseListFromUserLicenseInfoList(List<UserLicenseInfo> recordset)
        {
            List<UserLicense> UserLicenses = new List<UserLicense>();
            foreach (UserLicenseInfo record in recordset)
                UserLicenses.Add(GetUserLicenseFromUserLicenseInfo(record));
            return UserLicenses;
        }

        ///<summary>
        ///Returns a user license type hours
        ///</summary>
        public static string GetUserLicenseTypeHours(int testID) 
        {
            return SiteProvider.PR2.GetUserLicenseTypeHours(testID);
        }

        /// <summary>
        /// Returns the user licenses by license number
        /// </summary>
        public static List<UserLicense> GetUserLicenseByLicenseNumber(string LicenseNumber)
        {
            List<UserLicense> licenses;
            string key = "UserLicenses_UserLicenseByLicenseNumber_" + LicenseNumber;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                licenses = (List<UserLicense>) BizObject.Cache[key];
            }
            else
            {
                List<UserLicenseInfo> recordset = SiteProvider.PR2.GetUserLicenseByLicenseNumber(LicenseNumber);
                licenses = GetUserLicenseListFromUserLicenseInfoList(recordset);
                BasePR.CacheData(key, licenses);              
            }
            return licenses;
        }
        #endregion
    }
}
