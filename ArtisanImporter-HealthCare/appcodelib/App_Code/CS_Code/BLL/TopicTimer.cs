﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Fangbo Yang
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;


namespace PearlsReview.BLL
{
    public class TopicTimer : BasePR
    {
        public int topicid { get; set; }
        public int timin { get; set; }

        public static string GetTopicTimerByID(int topicid)
        {
            string tmin = SiteProvider.PR2.GetTopicTimerByID(topicid);

            if (tmin == "nothing" || tmin == "null")
                return "0";
            else
                return tmin;
        }

        public static void AddorUpdateTopicTimer(int topicid, int tmin)
        {
            SiteProvider.PR2.AddorUpdateTopicTimer(topicid,tmin);
        }
    }
}