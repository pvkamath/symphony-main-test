﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using System.Collections.Generic;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for MicrositeInterest
    /// </summary>
    public class MicrositeInterest : BasePR
    {
         #region Variables and Properties

        private int _miid= 0;
        public int miid
        {
            get { return _miid; }
            protected set { _miid = value; }
        }
        public int _msid = 0;
        public int msid
        {
            get { return _msid; }
            set { _msid = value; }
        }
        public string _interest_desc = "";
        public string interest_desc
        {
            get { return _interest_desc; }
            set { _interest_desc = value; }
        }        
        
        public MicrositeInterest(int miid, int msid, string interest_desc)
        {
            this.miid = miid;
            this.msid = msid;
            this.interest_desc = interest_desc;            
        }

       #endregion
        #region Methods


        /// <summary>
        /// Creates a new MicrositeInterest
        /// </summary>
        public static int InsertMicrositeInterest(int miid, int msid, string interest_desc)
        {

            MicrositeInterestInfo record = new MicrositeInterestInfo(0, msid, interest_desc);
            int ret = SiteProvider.PR2.InsertMicrositeInterest(record);
            BizObject.PurgeCacheItems("MicrositeInterests_MicrositeInterest");
            return ret;
        }


        /// <summary>
        /// Updates an existing MicrositeInterest
        /// </summary>
        public static bool UpdateMicrositeInterest(int miid, int msid, string interest_desc)
        {
            MicrositeInterestInfo record = new MicrositeInterestInfo(miid, msid, interest_desc);
            bool ret = SiteProvider.PR2.UpdateMicrositeInterest(record);
            BizObject.PurgeCacheItems("MicrositeInterest" + miid.ToString());
            return ret;
        }

        /// <summary>
        /// Returns a MicrositeInterest object with the specified ID
        /// </summary>
        public static MicrositeInterest GetMicrositeInterestByID(int id)
        {
            MicrositeInterest MicrositeInterests = null;
            string key = "MicrositeInterests_MicrositeInterests_" + id.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeInterests = (MicrositeInterest)BizObject.Cache[key];
            }
            else
            {
                MicrositeInterests = GetMicrositeInterestFromMicrositeInterestInfo(SiteProvider.PR2.GetMicrositeInterestByID(id));
                BasePR.CacheData(key, MicrositeInterests);
            }
            return MicrositeInterests;
        }

        /// <summary>
        /// Returns a MicrositeInterest object with the specified msid and Name
        /// </summary>
        public static MicrositeInterest GetMicrositeInterestBymsMsIdAndName(int msid, string Interest_desc)
        {
            MicrositeInterest MicrositeInterests = null;
            string key = "MicrositeInterests_MicrositeInterests_" + Interest_desc.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeInterests = (MicrositeInterest)BizObject.Cache[key];
            }
            else
            {
                MicrositeInterests = GetMicrositeInterestFromMicrositeInterestInfo(SiteProvider.PR2.GetMicrositeInterestBymsMsIdAndName(msid, Interest_desc));
                BasePR.CacheData(key, MicrositeInterests);
            }
            return MicrositeInterests;
        }

        /// <summary>
        /// Returns a collection with all MicrositeInterests for the specified msid
        /// </summary>
        public static List<MicrositeInterest> GetMicrositeInterestsBymsid(int msid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";
                     

            List<MicrositeInterest> MicrositeInterests = null;
            string key = "MicrositeInterests_MicrositeInterestInfo_" + msid.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeInterests = (List<MicrositeInterest>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeInterestInfo> recordset = SiteProvider.PR2.GetMicrositeInterestsBymsid(msid, cSortExpression);
                MicrositeInterests = GetMicrositeInterestListFromMicrositeInterestInfoList(recordset);
                BasePR.CacheData(key, MicrositeInterests);
            }
            return MicrositeInterests;
        }


        /// <summary>
        /// Returns a MicrositeInterest object filled with the data taken from the input MicrositeInterestInfo
        /// </summary>
        private static MicrositeInterest GetMicrositeInterestFromMicrositeInterestInfo(MicrositeInterestInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new MicrositeInterest(record.miid, record.msid, record.interest_desc);
            }
        }

        /// <summary>
        /// Returns a list of MicrositeInterest objects filled with the data taken from the input list of MicrositeInterestInfo
        /// </summary>
        private static List<MicrositeInterest> GetMicrositeInterestListFromMicrositeInterestInfoList(List<MicrositeInterestInfo> recordset)
        {
            List<MicrositeInterest> MicrositeInterests = new List<MicrositeInterest>();
            foreach (MicrositeInterestInfo record in recordset)
                MicrositeInterests.Add(GetMicrositeInterestFromMicrositeInterestInfo(record));
            return MicrositeInterests;
        }


        /// <summary>
        /// Deletes an existing MicrositeInterestId
        /// </summary>
        public static bool DeleteMicrositeInterestById(int miid)
        {
            bool ret = SiteProvider.PR2.DeleteMicrositeInterestById(miid);
            return ret;
        }

        # endregion

    }
}
