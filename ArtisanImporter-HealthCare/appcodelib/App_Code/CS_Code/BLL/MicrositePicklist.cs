﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for MicrositePicklist
    /// </summary>
    public class MicrositePicklist : BasePR
    {
        #region Variables and Properties


        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            set { _msid = value; }
        }
        private int _topicid = 0;
        public int Topicid
        {
            get { return _topicid; }
            set { _topicid = value; }
        }
        private string _pick_type = "";
        public string Pick_type
        {
            get { return _pick_type; }
            set { _pick_type = value; }
        }
        private int _pick_order = 0;
        public int Pick_order
        {
            get { return _pick_order; }
            set { _pick_order = value; }
        }
        public MicrositePicklist()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public MicrositePicklist(int msid, int topicid, string pick_type, int pick_order)
        {
            this.Msid = msid;
            this.Topicid = topicid;
            this.Pick_type = pick_type;
            this.Pick_order = pick_order;
        }
        public bool Delete()
        {
            bool success = MicrositePicklist.DeleteMicrositePicklist(this.Msid, this.Topicid);
            if (success)
                this.Msid = 0;
            return success;
        }

        public bool Update()
        {
            return MicrositePicklist.UpdateMicrositePicklist(this.Msid, this.Topicid, this.Pick_type, this.Pick_order);
        }

        #endregion
        #region Methods
        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all MicrositeDomains
        //</summary>
        public static List<MicrositePicklist> GetMicrositePicklist(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "pick_type";

            List<MicrositePicklist> MicrositePicklists = null;
            string key = "MicrositePicklists_MicrositePicklists_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositePicklists = (List<MicrositePicklist>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositePicklistInfo> recordset = SiteProvider.PR2.GetMicrositePicklist(cSortExpression);
                MicrositePicklists = GetMicrositePicklistListFromMicrositePicklistInfoList(recordset);
                BasePR.CacheData(key, MicrositePicklists);
            }
            return MicrositePicklists;
        }
        public static MicrositePicklist GetMicrositePicklist(int msid, int topicid, string PickType)
        {
            MicrositePicklist MicrositePicklists = null;
            string key = "MicrositePicklists_MicrositePicklists_" + msid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositePicklists = (MicrositePicklist)BizObject.Cache[key];
            }
            else
            {
                MicrositePicklists = GetMicrositePicklistFromMicrositePicklistInfo(SiteProvider.PR2.GetMicrositePicklist(msid, topicid, PickType));
                BasePR.CacheData(key, MicrositePicklists);
            }
            return MicrositePicklists;
        }
        public static DataTable GetMicrositePicklistByType(int Msid, string PickType, string cSortExpression)
        {
            return SiteProvider.PR2.GetMicrositePicklistByType(Msid, PickType, cSortExpression);
        }
        public static DataTable GetMicrositePicklistByTypes(int Msid, string[] PickTypes, string cSortExpression)
        {
            return SiteProvider.PR2.GetMicrositePicklistByTypes(Msid, PickTypes, cSortExpression);
        }
        public static DataTable GetMicrositePicklistByMsid(int Msid, string cSortExpression)
        {
            return SiteProvider.PR2.GetMicrositePicklistByMsid(Msid, cSortExpression);
        }
        public static int GetMicrositePicklistMaxOrder(int msid, string pick_type)
        {
            return SiteProvider.PR2.GetMicrositePicklistMaxOrder(msid, pick_type);
        }
        /// <summary>
        /// Returns a MicrositeDomains object with the specified ID
        /// </summary>
        public static MicrositePicklist GetMicrositePicklistByID(int Msid, int topicid, string pick_type)
        {
            MicrositePicklist MicrositePicklists = null;
            string key = "MicrositePicklists_MicrositePicklists_" + Msid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositePicklists = (MicrositePicklist)BizObject.Cache[key];
            }
            else
            {
                MicrositePicklists = GetMicrositePicklistFromMicrositePicklistInfo(SiteProvider.PR2.GetMicrositePicklistByID(Msid, topicid, pick_type));
                BasePR.CacheData(key, MicrositePicklists);
            }
            return MicrositePicklists;
        }
        public static void RefreshTopRated(int msid)
        {
            SiteProvider.PR2.RefreshTopRated(msid);
        }
        public static void RefreshMostPopular(int msid)
        {
            SiteProvider.PR2.RefreshMostPopular(msid);
        }
        public static int InsertMicrositePicklists(int msid, int topicid, string pick_type, int pick_order)
        {
            pick_type = BizObject.ConvertNullToEmptyString(pick_type);

            MicrositePicklistInfo record = new MicrositePicklistInfo(msid, topicid, pick_type, pick_order);
            int ret = SiteProvider.PR2.InsertMicrositePicklist(record);

            BizObject.PurgeCacheItems("MicrositePicklists_MicrositePicklists");
            return ret;
        }
        public static bool UpdateMicrositePickListLists(List<MicrositePicklist> Lists, int Msid, string PickType)
        {
            return SiteProvider.PR2.UpdateMicrositePickListLists(GetMicrositePicklistListInfoFromMicrositePicklistList(Lists), Msid, PickType);
        }
        /// <summary>
        /// Updates an existing MicrositeDomain
        /// </summary>
        public static bool UpdateMicrositePicklist(int Msid, int topicid, string pick_type, int pick_order)
        {
            pick_type = BizObject.ConvertNullToEmptyString(pick_type);

            MicrositePicklistInfo record = new MicrositePicklistInfo(Msid, topicid, pick_type, pick_order);
            bool ret = SiteProvider.PR2.UpdateMicrositePicklist(record);

            BizObject.PurgeCacheItems("MicrositePicklists_MicrositePicklists_" + Msid.ToString());
            BizObject.PurgeCacheItems("MicrositePicklists_MicrositePicklists");
            return ret;
        }

        /// <summary>
        /// Deletes an existing MicrositeDomain, but first checks if OK to delete
        /// </summary>
        public static bool DeleteMicrositePicklist(int Msid, int topicid)
        {
            bool IsOKToDelete = OKToDelete(Msid);
            if (IsOKToDelete)
            {
                return (bool)DeleteMicrositePicklist(Msid, topicid);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing MicrositeDomain - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteMicrositePicklist(int Msid, int topicid, string PickType, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteMicrositePicklist(Msid, topicid, PickType);
            BizObject.PurgeCacheItems("MicrositePicklists_MicrositePicklists");
            return ret;
        }

        /// <summary>
        /// Checks to see if a MicrositeDomain can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }


        //Check if the topic is unlimted in particular domain

        public static bool IsUnlimitedByTopicIdAndDomainId(int TopicId, int DomainId)
        {
            return SiteProvider.PR2.IsUnlimitedByTopicIdAndDomainId(TopicId, DomainId);
        }


        public static DataTable GetLimitedMicrositePicklistByType(int Msid, string PickType, string cSortExpression)
        {
            return SiteProvider.PR2.GetLimitedMicrositePicklistByType(Msid, PickType, cSortExpression);
        }


        /// <summary>
        /// Returns a MicrositeDomain object filled with the data taken from the input MicrositeDomainInfo
        /// </summary>
        private static MicrositePicklist GetMicrositePicklistFromMicrositePicklistInfo(MicrositePicklistInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new MicrositePicklist(record.Msid, record.Topicid, record.Pick_type, record.Pick_order);
            }
        }
        private static MicrositePicklistInfo GetMicrositePicklistInfoFromMicrositePicklist(MicrositePicklist record)
        {
            if (record == null)
                return null;
            else
            {
                return new MicrositePicklistInfo(record.Msid, record.Topicid, record.Pick_type, record.Pick_order);
            }
        }
        /// <summary>
        /// Returns a list of MicrositeDomain objects filled with the data taken from the input list of MicrositeDomainInfo
        /// </summary>
        private static List<MicrositePicklist> GetMicrositePicklistListFromMicrositePicklistInfoList(List<MicrositePicklistInfo> recordset)
        {
            List<MicrositePicklist> MicrositePicklists = new List<MicrositePicklist>();
            foreach (MicrositePicklistInfo record in recordset)
                MicrositePicklists.Add(GetMicrositePicklistFromMicrositePicklistInfo(record));
            return MicrositePicklists;
        }
        private static List<MicrositePicklistInfo> GetMicrositePicklistListInfoFromMicrositePicklistList(List<MicrositePicklist> recordset)
        {
            List<MicrositePicklistInfo> MicrositePicklists = new List<MicrositePicklistInfo>();
            foreach (MicrositePicklist record in recordset)
                MicrositePicklists.Add(GetMicrositePicklistInfoFromMicrositePicklist(record));
            return MicrositePicklists;
        }

        #endregion
    }
}