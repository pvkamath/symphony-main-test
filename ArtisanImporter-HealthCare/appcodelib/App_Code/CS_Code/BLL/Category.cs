﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for Category
    /// </summary>
    [Serializable]
    public class Category : BasePR
    {
        #region Variables and Properties
        private int _ID = 0;
        public int ID
        {
            get { return _ID; }
            protected set { _ID = value; }
        }

        private string _Title = "";
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        private bool _BuildPage = false;
        public bool BuildPage
        {
            get { return _BuildPage; }
            set { _BuildPage = value; }
        }

        private string _MetaKW = "";
        public string MetaKW
        {
            get { return _MetaKW; }
            set { _MetaKW = value; }
        }

        private string _MetaDesc = "";
        public string MetaDesc
        {
            get { return _MetaDesc; }
            set { _MetaDesc = value; }
        }

        private string _MetaTitle = "";
        public string MetaTitle
        {
            get { return _MetaTitle; }
            set { _MetaTitle = value; }
        }

        private string _PageText = "";
        public string PageText
        {
            get { return _PageText; }
            set { _PageText = value; }
        }

        private bool _Textbook = false;
        public bool Textbook
        {
            get { return _Textbook; }
            set { _Textbook = value; }
        }

        private bool _ShowInList = false;
        public bool ShowInList
        {
            get { return _ShowInList; }
            set { _ShowInList = value; }
        }

        private string _CertName = "";
        public string CertName
        {
            get { return _CertName; }
            set { _CertName = value; }
        }

        private string _CredAward = "";
        public string CredAward
        {
            get { return _CredAward; }
            set { _CredAward = value; }
        }

        private string _ExamType = "";
        public string ExamType
        {
            get { return _ExamType; }
            set { _ExamType = value; }
        }

        private string _ExamCost = "";
        public string ExamCost
        {
            get { return _ExamCost; }
            set { _ExamCost = value; }
        }

        private string _AdminOrg = "";
        public string AdminOrg
        {
            get { return _AdminOrg; }
            set { _AdminOrg = value; }
        }

        private string _Website = "";
        public string Website
        {
            get { return _Website; }
            set { _Website = value; }
        }

        private string _Reqments = "";
        public string Reqments
        {
            get { return _Reqments; }
            set { _Reqments = value; }
        }

        private string _EligCrit = "";
        public string EligCrit
        {
            get { return _EligCrit; }
            set { _EligCrit = value; }
        }

        private string _Image = "";
        public string Image
        {
            get { return _Image; }
            private set { _Image = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }

        public Category(){}
        public Category(int ID, string Title, bool BuildPage, string MetaKW, string MetaDesc,
            string MetaTitle, string PageText, bool Textbook, bool ShowInList, string CertName,
            string CredAward, string ExamType, string ExamCost, string AdminOrg, string Website,
            string Reqments, string EligCrit, string Image, int FacilityID)
        {
            this.ID = ID;
            this.Title = Title;
            this.BuildPage = BuildPage;
            this.MetaKW = MetaKW;
            this.MetaDesc = MetaDesc;
            this.MetaTitle = MetaTitle;
            this.PageText = PageText;
            this.Textbook = Textbook;
            this.ShowInList = ShowInList;
            this.CertName = CertName;
            this.CredAward = CredAward;
            this.ExamType = ExamType;
            this.ExamCost = ExamCost;
            this.AdminOrg = AdminOrg;
            this.Website = Website;
            this.Reqments = Reqments;
            this.EligCrit = EligCrit;
            this.Image = Image;
            this.FacilityID = FacilityID;
        }

        public bool Delete()
        {
            bool success = Category.DeleteCategory(this.ID);
            if (success)
                this.ID = 0;
            return success;
        }

        public bool Update()
        {
            return Category.UpdateCategory(this.ID, this.Title, this.BuildPage, this.MetaKW,
                this.MetaDesc, this.MetaTitle, this.PageText, this.Textbook,
                this.ShowInList, this.CertName, this.CredAward, this.ExamType, this.ExamCost,
                this.AdminOrg, this.Website, this.Reqments, this.EligCrit, this.Image, this.FacilityID);
        }
        #endregion

        #region Methods
       /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Categories
        /// </summary>
        public static List<Category> GetCategories(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_Categories_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetCategories(cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;
        }

       
        /// <summary>
        /// Returns a collection with all Compliance Categories
        /// </summary>
        public static List<Category> GetComplianceCategories(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_ComplianceCategories_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetComplianceCategories(cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;
        }

        /// <summary>
        /// Returns a collection with all Categories(Bsk New)
        /// </summary>
        public static List<Category> GetAllCategories(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_AllCategories_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetAllCategories(cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;
        }
        /// <summary>
        /// Returns a collection with all Categories(Bsk New)
        /// </summary>
        public static List<Category> GetAllPrimaryCategories(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_AllPrimaryCategories_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetAllPrimaryCategories(cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;
        }

        /// <summary>
        /// Returns a collection with all Textbook Categories
        /// </summary>
        public static List<Category> GetTextbookCategories(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_Categories_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetTextbookCategories(cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;
        }

        /// <summary>
        /// Returns a collection with all Textbook Categories - this is the list for display
        /// to website users (only those with ShowInList checked)
        /// </summary>
        public static List<Category> GetTextbookCategoriesForDisplay(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_Categories_ForDisplay_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetTextbookCategoriesForDisplay(cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;
        }


        /// <summary>
        /// Returns a collection with all Non-Textbook Categories
        /// </summary>
        public static List<Category> GetNonTextbookCategories(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_Categories_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetNonTextbookCategories(cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;
        }

        /// <summary>
        /// Returns a collection with all Non-Textbook Categories that have "ShowInList"
        /// checked -- these are the ones that should display to users of the website
        /// </summary>
        public static List<Category> GetNonTextbookCategoriesForDisplay(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_Categories_ForDisplay_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetNonTextbookCategoriesForDisplay(cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;
        }

        /// <summary>
        /// Returns a collection with all PearlsReview Categories that have "ShowInList"
        /// checked -- these are the ones that should display to users of the website
        /// </summary>
        /// 

        public static List<Category> GetPearlsCategoriesByFacilityId(int FacilityID, string cSortExpression)
        {
            if(cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Pearls_Categories_ForDisplay_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetPearlsCategoriesByFacilityId(FacilityID, cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;


        }





        /// <summary>
        /// Returns the number of total Categories
        /// </summary>
        public static int GetCategoryCount()
        {
            int CategoryCount = 0;
            string key = "Categories_CategoryCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CategoryCount = (int)BizObject.Cache[key];
            }
            else
            {
                CategoryCount = SiteProvider.PR2.GetCategoryCount();
                BasePR.CacheData(key, CategoryCount);
            }
            return CategoryCount;
        }

        /// <summary>
        /// Returns the number of total Textbook Categories
        /// </summary>
        public static int GetTextbookCategoryCount()
        {
            int CategoryCount = 0;
            string key = "Categories_CategoryCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CategoryCount = (int)BizObject.Cache[key];
            }
            else
            {
                CategoryCount = SiteProvider.PR2.GetTextbookCategoryCount();
                BasePR.CacheData(key, CategoryCount);
            }
            return CategoryCount;
        }

        /// <summary>
        /// Returns the number of total Non-Textbook Categories
        /// </summary>
        public static int GetNonTextbookCategoryCount()
        {
            int CategoryCount = 0;
            string key = "Categories_CategoryCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CategoryCount = (int)BizObject.Cache[key];
            }
            else
            {
                CategoryCount = SiteProvider.PR2.GetNonTextbookCategoryCount();
                BasePR.CacheData(key, CategoryCount);
            }
            return CategoryCount;
        }

        /// <summary>
        /// Returns a Category object with the specified ID
        /// </summary>
        public static Category GetCategoryByID(int ID)
        {
            Category Category = null;
            string key = "Categories_Category_" + ID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Category = (Category)BizObject.Cache[key];
            }
            else
            {
                Category = GetCategoryFromCategoryInfo(SiteProvider.PR2.GetCategoryByID(ID));
                BasePR.CacheData(key, Category);
            }
            return Category;
        }

        /// <summary>
        /// Updates an existing Category
        /// </summary>
        public static bool UpdateCategory(int ID, string Title, bool BuildPage, string MetaKW, 
            string MetaDesc, string MetaTitle, string PageText, bool Textbook,
            bool ShowInList, string CertName, string CredAward, string ExamType, string ExamCost, 
            string AdminOrg, string Website, string Reqments, string EligCrit,string Image, int FacilityID)
        {
			Title = BizObject.ConvertNullToEmptyString(Title);
			MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
			MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
			MetaTitle = BizObject.ConvertNullToEmptyString(MetaTitle);
			PageText = BizObject.ConvertNullToEmptyString(PageText);
            CertName = BizObject.ConvertNullToEmptyString(CertName);
            CredAward = BizObject.ConvertNullToEmptyString(CredAward);
            ExamType = BizObject.ConvertNullToEmptyString(ExamType);
            ExamCost = BizObject.ConvertNullToEmptyString(ExamCost);
            AdminOrg = BizObject.ConvertNullToEmptyString(AdminOrg);
            Website = BizObject.ConvertNullToEmptyString(Website);
            Reqments = BizObject.ConvertNullToEmptyString(Reqments);
            Image = BizObject.ConvertNullToEmptyString(Image);
            EligCrit = BizObject.ConvertNullToEmptyString(EligCrit);
            if (FacilityID == -2)
            {
                Textbook = true;
            }
            else
            {
                Textbook = false;
            }


            CategoryInfo record = new CategoryInfo(ID, Title, BuildPage, MetaKW, MetaDesc, 
                MetaTitle, PageText, Textbook, ShowInList, CertName, CredAward, ExamType, ExamCost,
                AdminOrg, Website, Reqments, EligCrit,Image, FacilityID);
            bool ret = SiteProvider.PR2.UpdateCategory(record);

            /// TODO: need to clear out the textbook version also
            BizObject.PurgeCacheItems("Categories_Category_" + ID.ToString());
            BizObject.PurgeCacheItems("Categories_Categories");
            return ret;
        }

        /// <summary>
        /// Creates a new Category
        /// </summary>
        public static int InsertCategory(string Title, bool BuildPage, string MetaKW, 
            string MetaDesc, string MetaTitle, string PageText, bool Textbook,
            bool ShowInList, string CertName, string CredAward, string ExamType, string ExamCost,
            string AdminOrg, string Website, string Reqments, string EligCrit,string Image, int FacilityID)
        {
			Title = BizObject.ConvertNullToEmptyString(Title);
			MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
			MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
			MetaTitle = BizObject.ConvertNullToEmptyString(MetaTitle);
			PageText = BizObject.ConvertNullToEmptyString(PageText);
            CertName = BizObject.ConvertNullToEmptyString(CertName);
            CredAward = BizObject.ConvertNullToEmptyString(CredAward);
            ExamType = BizObject.ConvertNullToEmptyString(ExamType);
            ExamCost = BizObject.ConvertNullToEmptyString(ExamCost);
            AdminOrg = BizObject.ConvertNullToEmptyString(AdminOrg);
            Website = BizObject.ConvertNullToEmptyString(Website);
            Reqments = BizObject.ConvertNullToEmptyString(Reqments);
            EligCrit = BizObject.ConvertNullToEmptyString(EligCrit);
            Image = BizObject.ConvertNullToEmptyString(Image);
            if (FacilityID == -2)
            {
                Textbook = true;
            }
            else
            {
                Textbook = false;
            }
            //if (Image == null)
            //{
            //    Image = "";
            //}
                CategoryInfo record = new CategoryInfo(0, Title, BuildPage, MetaKW, MetaDesc,
                MetaTitle, PageText, Textbook, ShowInList, CertName, CredAward, ExamType, ExamCost,
                AdminOrg, Website, Reqments, EligCrit,Image, FacilityID);
            int ret = SiteProvider.PR2.InsertCategory(record);

            BizObject.PurgeCacheItems("Categories_Category");
            return ret;
        }

        /// <summary>
        /// Creates a new Textbook Category
        /// </summary>
        public static int InsertTextbookCategory(string Title, bool BuildPage, string MetaKW, 
            string MetaDesc, string MetaTitle, string PageText, bool Textbook,
            bool ShowInList, string CertName, string CredAward, string ExamType, string ExamCost,
            string AdminOrg, string Website, string Reqments, string EligCrit, string Image)

        {
            Title = BizObject.ConvertNullToEmptyString(Title);
            MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
            MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
            MetaTitle = BizObject.ConvertNullToEmptyString(MetaTitle);
            PageText = BizObject.ConvertNullToEmptyString(PageText);
            CertName = BizObject.ConvertNullToEmptyString(CertName);
            CredAward = BizObject.ConvertNullToEmptyString(CredAward);
            ExamType = BizObject.ConvertNullToEmptyString(ExamType);
            ExamCost = BizObject.ConvertNullToEmptyString(ExamCost);
            AdminOrg = BizObject.ConvertNullToEmptyString(AdminOrg);
            Website = BizObject.ConvertNullToEmptyString(Website);
            Reqments = BizObject.ConvertNullToEmptyString(Reqments);
            EligCrit = BizObject.ConvertNullToEmptyString(EligCrit);
            Image = BizObject.ConvertNullToEmptyString(Image);


            // NOTE: We add an extra parameter at the end that is not passed into this method:
            // By Default, we force FacilityID to be 0 for system-side categories
            CategoryInfo record = new CategoryInfo(0, Title, BuildPage, MetaKW, MetaDesc,
                MetaTitle, PageText, true, ShowInList, CertName, CredAward, ExamType, ExamCost,
                AdminOrg, Website, Reqments, EligCrit,Image, 0);
            int ret = SiteProvider.PR2.InsertCategory(record);

            BizObject.PurgeCacheItems("Categories_Category");
            return ret;
        }

        /// <summary>
        /// Creates a new Non-Textbook Category
        /// </summary>
        public static int InsertNonTextbookCategory(string Title, bool BuildPage, string MetaKW,
            string MetaDesc, string MetaTitle, string PageText, bool Textbook,
            bool ShowInList, string CertName, string CredAward, string ExamType, string ExamCost,
            string AdminOrg, string Website, string Reqments, string EligCrit, string Image)
        {
            Title = BizObject.ConvertNullToEmptyString(Title);
            MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
            MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
            MetaTitle = BizObject.ConvertNullToEmptyString(MetaTitle);
            PageText = BizObject.ConvertNullToEmptyString(PageText);
            CertName = BizObject.ConvertNullToEmptyString(CertName);
            CredAward = BizObject.ConvertNullToEmptyString(CredAward);
            ExamType = BizObject.ConvertNullToEmptyString(ExamType);
            ExamCost = BizObject.ConvertNullToEmptyString(ExamCost);
            AdminOrg = BizObject.ConvertNullToEmptyString(AdminOrg);
            Website = BizObject.ConvertNullToEmptyString(Website);
            Reqments = BizObject.ConvertNullToEmptyString(Reqments);
            EligCrit = BizObject.ConvertNullToEmptyString(EligCrit);
            Image = BizObject.ConvertNullToEmptyString(Image);
            

            // NOTE: We add an extra parameter at the end that is not passed into this method:
            // By Default, we force FacilityID to be 0 for system-side categories
            CategoryInfo record = new CategoryInfo(0, Title, BuildPage, MetaKW, MetaDesc,
                MetaTitle, PageText, false, ShowInList, CertName, CredAward, ExamType, ExamCost,
                AdminOrg, Website, Reqments, EligCrit,Image, 0);
            int ret = SiteProvider.PR2.InsertCategory(record);

            BizObject.PurgeCacheItems("Categories_Category");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Category, but first checks if OK to delete
        /// </summary>
        public static bool DeleteCategory(int ID)
        {
            bool IsOKToDelete = OKToDelete(ID);
            if (IsOKToDelete)
            {
                return (bool)DeleteCategory(ID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Category - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteCategory(int ID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteCategory(ID);
            //         new RecordDeletedEvent("Category", ID, null).Raise();
            BizObject.PurgeCacheItems("Categories_Category");
            return ret;
        }





        /// <summary>
        /// Returns a Category object filled with the data taken from the input CategoryInfo
        /// </summary>
        private static Category GetCategoryFromCategoryInfo(CategoryInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Category(record.ID, record.Title, record.BuildPage, 
                    record.MetaKW, record.MetaDesc, record.MetaTitle, record.PageText, record.Textbook,
                    record.ShowInList, record.CertName, record.CredAward, record.ExamType, record.ExamCost,
                    record.AdminOrg, record.Website, record.Reqments, record.EligCrit,record.Image , record.FacilityID);
            }
        }

        /// <summary>
        /// Returns a list of Category objects filled with the data taken from the input list of CategoryInfo
        /// </summary>
        private static List<Category> GetCategoryListFromCategoryInfoList(List<CategoryInfo> recordset)
        {
            List<Category> Categories = new List<Category>();
            foreach (CategoryInfo record in recordset)
                Categories.Add(GetCategoryFromCategoryInfo(record));
            return Categories;
        }

        /// <summary>
        /// Returns a CheckBoxListCategory object filled with the data taken from the input CheckBoxListCategoryInfo
        /// </summary>
        private static CheckBoxListCategoryInfo GetCheckBoxListCategoryFromCheckBoxListCategoryInfo(CheckBoxListCategoryInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new CheckBoxListCategoryInfo(record.ID, record.Title, record.Selected);
            }
        }

        /// <summary>
        /// Returns a list of CheckBoxListCategory objects filled with the data taken from the input list of CheckBoxListCategoryInfo
        /// </summary>
        private static List<CheckBoxListCategoryInfo> GetCheckBoxListCategoryListFromCheckBoxListCategoryInfoList(List<CheckBoxListCategoryInfo> recordset)
        {
            List<CheckBoxListCategoryInfo> CheckBoxListCategories = new List<CheckBoxListCategoryInfo>();
            foreach (CheckBoxListCategoryInfo record in recordset)
                CheckBoxListCategories.Add(GetCheckBoxListCategoryFromCheckBoxListCategoryInfo(record));
            return CheckBoxListCategories;
        }

        //        /// <summary>
//        /// TEST: See if we can get just a paged subset of records
//        /// Returns a collection with all Categories
//        /// </summary>
//        public static List<Category> GetCategoriesX(string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";
//
//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";
//
//            List<Category> Categories = null;
//            string key = "Categories_Categories_" + cSortExpression.ToString();
//
//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//           {
//                Categories = (List<Category>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryInfo> recordset = SiteProvider.PR2.GetCategoriesX(cSortExpression);
//                Categories = GetCategoryListFromCategoryInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;
//        }

        /// <summary>
        /// Returns a collection with all Categories for a TopicID
        /// </summary>
        public static List<Category> GetCategoriesByTopicID(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_Categories_" + TopicID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetCategoriesByTopicID(TopicID, cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;
        }

        /// <summary>
        /// Returns a collection with all Categories, plus info about those assigned to a TopicID
        /// </summary>
        public static List<CheckBoxListCategoryInfo> GetAllCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<CheckBoxListCategoryInfo> CheckBoxListCategories = null;
            string key = "Categories_CheckBoxListCategories_" + TopicID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CheckBoxListCategories = (List<CheckBoxListCategoryInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CheckBoxListCategoryInfo> recordset = SiteProvider.PR2.GetAllCategoriesPlusTopicAssignments(TopicID, cSortExpression);
                CheckBoxListCategories = GetCheckBoxListCategoryListFromCheckBoxListCategoryInfoList(recordset);
                BasePR.CacheData(key, CheckBoxListCategories);
            }
            return CheckBoxListCategories;

        }

        /// <summary>
        /// Returns a collection with all Categories, plus info about those assigned to a TopicID
        /// </summary>
        public static List<CheckBoxListCategoryInfo> GetAllNonTextbookCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<CheckBoxListCategoryInfo> CheckBoxListCategories = null;
            string key = "Categories_NonTextbook_CheckBoxListCategories_" + TopicID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CheckBoxListCategories = (List<CheckBoxListCategoryInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CheckBoxListCategoryInfo> recordset = SiteProvider.PR2.GetAllNonTextbookCategoriesPlusTopicAssignments(TopicID, cSortExpression);
                CheckBoxListCategories = GetCheckBoxListCategoryListFromCheckBoxListCategoryInfoList(recordset);
                BasePR.CacheData(key, CheckBoxListCategories);
            }
            return CheckBoxListCategories;

        }

        /// <summary>
        /// Returns a collection with all Categories, plus info about those assigned to a TopicID
        /// </summary>
        public static List<CheckBoxListCategoryInfo> GetAllTextbookCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<CheckBoxListCategoryInfo> CheckBoxListCategories = null;
            string key = "Categories_Textbook_CheckBoxListCategories_" + TopicID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CheckBoxListCategories = (List<CheckBoxListCategoryInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CheckBoxListCategoryInfo> recordset = SiteProvider.PR2.GetAllTextbookCategoriesPlusTopicAssignments(TopicID, cSortExpression);
                CheckBoxListCategories = GetCheckBoxListCategoryListFromCheckBoxListCategoryInfoList(recordset);
                BasePR.CacheData(key, CheckBoxListCategories);
            }
            return CheckBoxListCategories;

        }

        /// <summary>
        /// Returns a collection with all Textbook Categories NOT assigned
        /// to a specific TopicID
        /// (used for assigning additional categories to a topic)
        /// </summary>
        public static List<Category> GetTextbookCategoriesNotAssignedToTopicID(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            List<CategoryInfo> recordset =
                SiteProvider.PR2.GetTextbookCategoriesNotAssignedToTopicID(TopicID, cSortExpression);
            Categories = GetCategoryListFromCategoryInfoList(recordset);
            return Categories;
        }

        /// <summary>
        /// Returns a collection with all Non-Textbook Categories NOT assigned
        /// to a specific TopicID
        /// (used for assigning additional categories to a topic)
        /// </summary>
        public static List<Category> GetNonTextbookCategoriesNotAssignedToTopicID(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            List<CategoryInfo> recordset =
                SiteProvider.PR2.GetNonTextbookCategoriesNotAssignedToTopicID(TopicID, cSortExpression);
            Categories = GetCategoryListFromCategoryInfoList(recordset);
            return Categories;
        }

        /// <summary>
        /// Returns the number of total Categories assigned to a Topic
        /// </summary>
        public static int GetCategoryCount(int TopicID)
        {
            int CategoryCount = 0;
            string key = "Categories_CategoryCount_" + TopicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CategoryCount = (int)BizObject.Cache[key];
            }
            else
            {
                CategoryCount = SiteProvider.PR2.GetCategoryCountByTopicID(TopicID);
                BasePR.CacheData(key, CategoryCount);
            }
            return CategoryCount;
        }

        //#region Add these methods def after adding topic
      

        /// <summary>
        /// Assign Topic to a Category
        /// </summary>
        public static bool AssignTopicToCategory(int TopicID, int CategoryID)
        {
            bool ret = SiteProvider.PR2.AssignTopicToCategory(TopicID, CategoryID);
            // TODO: release cache?
            return ret;
        }

        /// <summary>
        /// Detach Topic Assignment from a Category
        /// </summary>
        public static bool DetachTopicFromCategory(int TopicID, int CategoryID)
        {
            bool ret = SiteProvider.PR2.DetachTopicFromCategory(TopicID, CategoryID);
            // TODO: release cache?
            return ret;
        }

        public static bool UpdateCategoryTopicAssignments(int CategoryID, string TopicIDAssignments)
        {
            bool ret = SiteProvider.PR2.UpdateCategoryTopicAssignments(CategoryID, TopicIDAssignments);
            // TODO: release cache?
            return ret;
        }

        /// <summary>
        /// Checks to see if a Category can be deleted safely
        /// (NO if it has any Topics assigned to it)
        /// </summary>
        public static bool OKToDelete(int ID)
        {
            int AssignedTopics = SiteProvider.PR2.GetTopicCountByCategoryID(ID);
            return (AssignedTopics == 0);
        }
        #endregion


        /// <summary>
        /// Returns a collection with all non-textbook Categories with active topics, plus topic counts
        /// </summary>
        public static List<CategoryTopicCountInfo> GetNonTextbookCategoriesWithTopicCount(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<CategoryTopicCountInfo> Categories = null;
            string key = "Categories_CategoryTopicCountInfo_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryTopicCountInfo> recordset = SiteProvider.PR2.GetNonTextbookCategoriesWithTopicCount(cSortExpression);
                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;

        }



        /// <summary>
        /// Returns a collection with all non-textbook Categories with active topics, plus topic counts
        /// </summary>
        public static List<CategoryTopicCountInfo> GetRNonTextbookCategoriesWithTopicCount(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<CategoryTopicCountInfo> Categories = null;
            string key = "RCategories_CategoryTopicCountInfo_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryTopicCountInfo> recordset = SiteProvider.PR2.GetRNonTextbookCategoriesWithTopicCount(cSortExpression);
                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;

        }

        /// <summary>
        /// Returns a collection with all non-textbook Categories with active topics, plus topic counts
        /// </summary>
        public static List<CategoryTopicCountInfo> GetRNonTextbookUnlimitedCategoriesWithTopicCount(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<CategoryTopicCountInfo> Categories = null;
            string key = "RCategories_UnlCategoryTopicCountInfo_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryTopicCountInfo> recordset = SiteProvider.PR2.GetRNonTextbookUnlimitedCategoriesWithTopicCount(cSortExpression);
                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;

        }




        /// <summary>
        /// Returns a collection with all non-textbook Categories with active topics, plus topic counts by passing facilityid
        /// </summary>
        public static List<CategoryTopicCountInfo> GetNonTextbookCategoriesWithTopicCountByFacilityId(int facilityid,string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<CategoryTopicCountInfo> Categories = null;
            string key = "Categories_CategoryTopicCountInfo_" +facilityid+ cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryTopicCountInfo> recordset = SiteProvider.PR2.GetNonTextbookCategoriesWithTopicCountByFacilityId(facilityid,cSortExpression);
                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;

        }


        /// <summary>
        /// Returns a collection with all non-textbook Categories with active topics, plus topic counts by passing facilityid
        /// </summary>
        public static List<CategoryTopicCountInfo> GetTextbookCategoriesWithTopicCountByFacilityId(int facilityid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<CategoryTopicCountInfo> Categories = null;
            string key = "Categories_CategoryTextTopicCountInfo_" + facilityid + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryTopicCountInfo> recordset = SiteProvider.PR2.GetTextbookCategoriesWithTopicCountByFacilityId(facilityid, cSortExpression);
                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;

        }



        /// <summary>
        /// Returns a CategoryTopicCountInfo object filled with the data taken from the input CategoryTopicCountInfo
        /// </summary>
        private static CategoryTopicCountInfo GetCategoryTopicCountFromCategoryTopicCountInfo(CategoryTopicCountInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new CategoryTopicCountInfo(record.ID, record.Title, record.TopicCount,record.DTitle);
            }
        }

        /// <summary>
        /// Returns a list of CategoryTopicCountInfo objects filled with the data taken from the input list of CategoryTopicCountInfo
        /// </summary>
        private static List<CategoryTopicCountInfo> GetCategoryTopicCountListFromCategoryTopicCountInfoList(List<CategoryTopicCountInfo> recordset)
        {
            List<CategoryTopicCountInfo> Categories = new List<CategoryTopicCountInfo>();
            foreach (CategoryTopicCountInfo record in recordset)
                Categories.Add(GetCategoryTopicCountFromCategoryTopicCountInfo(record));
            return Categories;
        }

        /// <summary>
        /// Returns a collection with all non-textbook state Categories with active topics, plus topic counts
        /// </summary>
        public static List<CategoryTopicCountInfo> GetRNonTextbookStateCategoriesWithTopicCount()
        {

            List<CategoryTopicCountInfo> Categories = null;
            string key = "RStateCategories_CategoryTopicCountInfo_";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryTopicCountInfo> recordset = SiteProvider.PR2.GetRNonTextbookStateCategoriesWithTopicCount();
                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;

        }

        public static System.Data.DataSet GetCategoryByWebSite(string website)
        {
            System.Data.DataSet recordset = (SiteProvider.PR2.GetCategoryByWebSite(website));

            return recordset;
        }

        public static System.Data.DataSet GetCategoryByWebSitePRRetail(string website, string areaid)
        {
            System.Data.DataSet recordset = (SiteProvider.PR2.GetCategoryByWebSitePRRetail(website,areaid));

            return recordset;
        }


        public static Category GetPearlsCategoryByWebSite(string website)
        {
            Category recordset = GetCategoryFromCategoryInfo(SiteProvider.PR2.GetPearlsCategoryByWebSite(website));

            return recordset;
        }
        public static Category GetMSCatergoryByWebSite(string website)
        {

            Category recordset = GetCategoryFromCategoryInfo(SiteProvider.PR2.GetMSCategoryByWebSite(website));

            return recordset;

        }
        public static Category GetCatNameByTopicId(int TopicId)
        {
            Category recordset = GetCategoryFromCategoryInfo(SiteProvider.PR2.GetCatNameByTopicId(TopicId));
            return recordset;
        }

        public static Category GetPrimaryCatNameByTopicId(int TopicId)
        {
            Category recordset = GetCategoryFromCategoryInfo(SiteProvider.PR2.GetPrimaryCatNameByTopicId(TopicId));
            return recordset;
        }

        public static Category GetLastTopicCatByUserID(int UserId)
        {
            Category recordset = GetCategoryFromCategoryInfo(SiteProvider.PR2.GetLastTopicCatByUserID(UserId));
            return recordset;
        }

        public static string CheckCatNameByTopicId(int TopicId, string CatName)
        {
            string objReturn = SiteProvider.PR2.CheckCatNameByTopicId(TopicId, CatName);
            return objReturn;
        }

        /// <summary>
        /// Returns a collection with all active Categories by facilityid
        /// </summary>
        public static List<Category> GetCategoriesByFacilityID(int FacilityID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_CategoryInfo_" + FacilityID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetCategoriesByFacilityID(FacilityID, cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;

        }

        public static int GetICECategoryIDByUserID(int userid) 
        {
            return SiteProvider.PR2.GetICECategoryIDByUserID(userid);           
        }

        /// <summary>
        /// Returns a collection with all active Categories by facilityid
        /// </summary>
        public static List<Category> GetCategorieswithTopicByFacilityID(int FacilityID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_Topic_CategoryInfo_" + FacilityID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetCategorieswithTopicByFacilityID(FacilityID, cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;

        }

        /// <summary>
        /// Returns a collection with all active Categories by facilityid
        /// </summary>
        public static List<Category> GetCategorieswithTopicByAreaname(string Areaname, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_Topic_CategoryInfo_" + Areaname.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetCategorieswithTopicByAreaname(Areaname, cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;

        }



        /// <summary>
        /// Returns a collection with all active Categories by facilityid
        /// </summary>
        public static List<Category> GetCategorieswithTopicByTabname(string Tabname, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<Category> Categories = null;
            string key = "Categories_Topic_CategoryInfo_" + Tabname.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<Category>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryInfo> recordset = SiteProvider.PR2.GetCategorieswithTopicByTabname(Tabname, cSortExpression);
                Categories = GetCategoryListFromCategoryInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;

        }

        /// <summary>
        /// Returns a collection with all active Categories and  by facilityid
        /// </summary>

        public static System.Data.DataSet GetCategoriesandAreaTypeByAreaname(string Areaname, string cSortExpression)
        {
            System.Data.DataSet recordset = (SiteProvider.PR2.GetCategoriesandAreaTypeByAreaname(Areaname,cSortExpression));

            return recordset;
        }

        /// <summary>
        /// Returns a collection with all active Categories by facilityid
        /// </summary>
        public static System.Data.DataSet GetCategoriesandAreaTypeByTabname(string Tabname, string cSortExpression)
        {
            System.Data.DataSet recordset = (SiteProvider.PR2.GetCategoriesandAreaTypeByTabname(Tabname,cSortExpression));

            return recordset;
        }


        /// <summary>
        ///  /// Retrieves all  Categories with topic counts that should be displayed to the website users by tabname
        /// </summary>
        public static List<CategoryTopicCountInfo> GetCategorieswithTopicCountByTabName(string TabName, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Title";

            List<CategoryTopicCountInfo> Categories = null;
            string key = "Categories_CategoryTopicCountInfo_" + TabName.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryTopicCountInfo> recordset = SiteProvider.PR2.GetCategorieswithTopicCountByTabName(TabName,cSortExpression);
                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
                BasePR.CacheData(key, Categories);
            }
            return Categories;

        }

        public static DataSet GetCategoriesByMicroSiteDomainIdWithBundle(int DomainId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            // Provide default Sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Category.Title";
            DataSet Topics = null;
            string key = "GetCategoriesByMSDomain_" + DomainId.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Topics = (DataSet)BizObject.Cache[key];
            }
            else
            {
                Topics = SiteProvider.PR2.GetCategoriesByMicroSiteDomainIdWithBundle(DomainId, cSortExpression);
            }
            return Topics;

        }
       
    }
}

    

