﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

    /// <summary>
    /// Summary description for Nurse
    /// </summary>
    public class Nurse : BasePR
    {
        #region Variables and Properties
        private int _rnID = 0;
        public int rnID
        {
            get { return _rnID; }
            protected set { _rnID = value; }
        }

        private string _UserName = "";
        public string UserName
        {
            get { return _UserName; }
            protected set { _UserName = value; }
        }

        private string _Password = "";
        public string Password
        {
            get { return _Password; }
            protected set { _Password = value; }
        }

        private bool _Registered = false;
        public bool Registered
        {
            get { return _Registered; }
            protected set { _Registered = value; }
        }

        private string _FirstName = "";
        public string FirstName
        {
            get { return _FirstName; }
            protected set { _FirstName = value; }
        }
        private string _LastName = "";
        public string LastName
        {
            get { return _LastName; }
            protected set { _LastName = value; }
        }
        private string _Address1 = "";
        public string Address1
        {
            get { return _Address1; }
            protected set { _Address1 = value; }
        }
        private string _Address2 = "";
        public string Address2
        {
            get { return _Address2; }
            protected set { _Address2 = value; }
        }
        private string _City = "";
        public string City
        {
            get { return _City; }
            protected set { _City = value; }
        }
        private string _State = "";
        public string State
        {
            get { return _State; }
            protected set { _State = value; }
        }
        private string _Zip = "";
        public string Zip
        {
            get { return _Zip; }
            protected set { _Zip = value; }
        }
        private string _Country = "";
        public string Country
        {
            get { return _Country; }
            protected set { _Country = value; }
        }
        private string _SAddress1 = "";
        public string SAddress1
        {
            get { return _SAddress1; }
            protected set { _SAddress1 = value; }
        }
        private string _SAddress2 = "";
        public string SAddress2
        {
            get { return _SAddress2; }
            protected set { _SAddress2 = value; }
        }
        private string _SCity = "";
        public string SCity
        {
            get { return _SCity; }
            protected set { _SCity = value; }
        }
        private string _SState = "";
        public string SState
        {
            get { return _SState; }
            protected set { _SState = value; }
        }
        private string _SZip = "";
        public string SZip
        {
            get { return _SZip; }
            protected set { _SZip = value; }
        }
        private string _SCountry = "";
        public string SCountry
        {
            get { return _SCountry; }
            protected set { _SCountry = value; }
        }
        private string _Email = "";
        public string Email
        {
            get { return _Email; }
            protected set { _Email = value; }
        }
        private string _EmailPref = "";
        public string EmailPref
        {
            get { return _EmailPref; }
            protected set { _EmailPref = value; }
        }
        private bool _OptIn = false;
        public bool OptIn
        {
            get { return _OptIn; }
             set { _OptIn = value; }
        }
        private bool _Offers = false;
        public bool Offers
        {
            get { return _Offers; }
            protected set { _Offers = value; }
        }
        private string _RNstate = "";
        public string RNstate
        {
            get { return _RNstate; }
            protected set { _RNstate = value; }
        }
        private string _RNnum = "";
        public string RNnum
        {
            get { return _RNnum; }
            protected set { _RNnum = value; }
        }
        private string _RNCode = "";
        public string RNCode
        {
            get { return _RNCode; }
            protected set { _RNCode = value; }
        }
        private string _RNnumX = "";
        public string RNnumX
        {
            get { return _RNnumX; }
            protected set { _RNnumX = value; }
        }
        private string _RNstate2 = "";
        public string RNstate2
        {
            get { return _RNstate2; }
            protected set { _RNstate2 = value; }
        }
        private string _RNnum2 = "";
        public string RNnum2
        {
            get { return _RNnum2; }
            protected set { _RNnum2 = value; }
        }
        private string _RNCode2 = "";
        public string RNCode2
        {
            get { return _RNCode2; }
            protected set { _RNCode2 = value; }
        }
        private string _RNnumX2 = "";
        public string RNnumX2
        {
            get { return _RNnumX2; }
            protected set { _RNnumX2 = value; }
        }
        private int _SpecialtyID = 0;
        public int SpecialtyID
        {
            get { return _SpecialtyID; }
            protected set { _SpecialtyID = value; }
        }
        private string _SpecialtyIDlist = "";
        public string SpecialtyIDlist
        {
            get { return _SpecialtyIDlist; }
            protected set { _SpecialtyIDlist = value; }
        }
        private string _ImportedFrom = "";
        public string ImportedFrom
        {
            get { return _ImportedFrom; }
            protected set { _ImportedFrom = value; }
        }
        private int _FieldID = 0;
        public int FieldID
        {
            get { return _FieldID; }
            protected set { _FieldID = value; }
        }
        private int _AgeGroup = 0;
        public int AgeGroup
        {
            get { return _AgeGroup; }
            protected set { _AgeGroup = value; }
        }
        private string _BrowserAgent = "";
        public string BrowserAgent
        {
            get { return _BrowserAgent; }
            protected set { _BrowserAgent = value; }
        }
        private string _Phone = "";
        public string Phone
        {
            get { return _Phone; }
            protected set { _Phone = value; }
        }
        private string _PhoneType = "";
        public string PhoneType
        {
            get { return _PhoneType; }
            protected set { _PhoneType = value; }
        }
        private string _Specialty = "";
        public string Specialty
        {
            get { return _Specialty; }
            protected set { _Specialty = value; }
        }
        private string _Education = "";
        public string Education
        {
            get { return _Education; }
            protected set { _Education = value; }
        }
        private string _Position = "";
        public string Position
        {
            get { return _Position; }
            protected set { _Position = value; }
        }
        private DateTime _DateEntered = System.DateTime.Now;
        public DateTime DateEntered
        {
            get { return _DateEntered; }
            protected set { _DateEntered = value; }
        }
        private DateTime _UCEend = System.DateTime.Now;
        public DateTime UCEend
        {
            get { return _UCEend; }
             set { _UCEend = value; }
        }
        private DateTime _Updated = System.DateTime.Now;
        public DateTime Updated
        {
            get { return _Updated; }
            protected set { _Updated = value; }
        }
        private DateTime _LastLogin = System.DateTime.Now;
        public DateTime LastLogin
        {
            get { return _LastLogin; }
            protected set { _LastLogin = value; }
        }
        private string _IP = "";
        public string IP
        {
            get { return _IP; }
            protected set { _IP = value; }
        }
        private bool _Disabled = false;
        public bool Disabled
        {
            get { return _Disabled; }
            protected set { _Disabled = value; }
        }

        public Nurse(int rnID, string UserName, string Password, bool Registered, string FirstName, string LastName,
            string Address1, string Address2, string City, string State, string Zip,
            string Country, string SAddress1, string SAddress2, string SCity, string SState,
            string SZip, string SCountry, string Email, string EmailPref, bool OptIn,
            bool Offers, string RNstate, string RNnum, string RNCode, string RNnumX,
            string RNstate2, string RNnum2, string RNCode2, string RNnumX2,
            int SpecialtyID, string SpecialtyIDlist, string ImportedFrom, int FieldID, int AgeGroup,
            string BrowserAgent, string Phone, string PhoneType, string Specialty, string Education,
            string Position, DateTime DateEntered, DateTime UCEend, DateTime Updated, DateTime LastLogin,
            string IP, bool Disabled)
        {

            this.rnID = rnID;
            this.UserName = UserName;
            this.Password = Password;
            this.Registered = Registered;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Address1 = Address1;
            this.Address2 = Address2;
            this.City = City;
            this.State = State;
            this.Zip = Zip;
            this.Country = Country;
            this.SAddress1 = SAddress1;
            this.SAddress2 = SAddress2;
            this.SCity = SCity;
            this.SState = SState;
            this.SZip = SZip;
            this.SCountry = SCountry;
            this.Email = Email;
            this.EmailPref = EmailPref;
            this.OptIn = OptIn;
            this.Offers = Offers;
            this.RNstate = RNstate;
            this.RNnum = RNnum;
            this.RNCode = RNCode;
            this.RNnumX = RNnumX;
            this.RNstate2 = RNstate2;
            this.RNnum2 = RNnum2;
            this.RNCode2 = RNCode2;
            this.RNnumX2 = RNnumX2;
            this.SpecialtyID = SpecialtyID;
            this.SpecialtyIDlist = SpecialtyIDlist;
            this.ImportedFrom = ImportedFrom;
            this.FieldID = FieldID;
            this.AgeGroup = AgeGroup;
            this.BrowserAgent = BrowserAgent;
            this.Phone = Phone;
            this.PhoneType = PhoneType;
            this.Specialty = Specialty;
            this.Education = Education;
            this.Position = Position;
            this.DateEntered = DateEntered;
            this.UCEend = UCEend;
            this.Updated = Updated;
            this.LastLogin = LastLogin;
            this.IP = IP;
            this.Disabled = Disabled;

        }
        public static int InsertNurse(string FirstName, string LastName, string UserName, string Email, string Password)
        {
            FirstName = BizObject.ConvertNullToEmptyString(FirstName);
            LastName = BizObject.ConvertNullToEmptyString(LastName);
            UserName = BizObject.ConvertNullToEmptyString(UserName);
            Email = BizObject.ConvertNullToEmptyString(Email);
            Password = BizObject.ConvertNullToEmptyString(Password);
            // NOTE: Always send FirstLogin_Ind as true when inserting a new user record
            int ret = SiteProvider.PR2.InsertNurse(FirstName, LastName, UserName, Email, Password);
            BizObject.PurgeCacheItems("Nurses_Nurse");
            return ret;
        }
        public static bool InsertNurse(string UserName, string Password, bool Registered, string FirstName, string LastName,
         string Address1, string Address2, string City, string State, string Zip,
         string Country, string SAddress1, string SAddress2, string SCity, string SState,
         string SZip, string SCountry, string Email, string EmailPref, bool OptIn,
         bool Offers, string RNstate, string RNnum, string RNCode, string RNnumX,
         string RNstate2, string RNnum2, string RNCode2, string RNnumX2,
         int SpecialtyID, string SpecialtyIDlist, string ImportedFrom, int FieldID, int AgeGroup,
         string BrowserAgent, string Phone, string PhoneType, string Specialty, string Education,
         string Position, DateTime DateEntered, DateTime UCEend, DateTime Updated, DateTime LastLogin,
         string IP, bool Disabled)
        {
            NurseInfo record = new NurseInfo(0, UserName, Password, Registered, FirstName, LastName, Address1, Address2,
               City, State, Zip, Country, SAddress1, SAddress2, SCity, SState, SZip, SCountry, Email, EmailPref, OptIn,
               Offers, RNstate, RNnum, RNCode, RNnumX, RNstate2, RNnum2, RNCode2, RNnumX2, SpecialtyID, SpecialtyIDlist,
               ImportedFrom, FieldID, AgeGroup, BrowserAgent, Phone, PhoneType, Specialty, Education, Position, DateEntered,
               UCEend, Updated, LastLogin, IP, Disabled);

            PearlsReview.DAL.SQLClient.SQL2PRProvider obj = new PearlsReview.DAL.SQLClient.SQL2PRProvider();
            obj.InsertNurse(record);
            return false;
        }

        public static bool UpdateNurse(int rnID, string UserName, string Password, bool Registered, string FirstName, string LastName,
          string Address1, string Address2, string City, string State, string Zip,
          string Country, string SAddress1, string SAddress2, string SCity, string SState,
          string SZip, string SCountry, string Email, string EmailPref, bool OptIn,
          bool Offers, string RNstate, string RNnum, string RNCode, string RNnumX,
          string RNstate2, string RNnum2, string RNCode2, string RNnumX2,
          int SpecialtyID, string SpecialtyIDlist, string ImportedFrom, int FieldID, int AgeGroup,
          string BrowserAgent, string Phone, string PhoneType, string Specialty, string Education,
          string Position, DateTime DateEntered, DateTime UCEend, DateTime Updated, DateTime LastLogin,
          string IP, bool Disabled)
        {
            NurseInfo record = new NurseInfo(rnID, UserName, Password, Registered, FirstName, LastName, Address1, Address2,
                City, State, Zip, Country, SAddress1, SAddress2, SCity, SState, SZip, SCountry, Email, EmailPref, OptIn,
                Offers, RNstate, RNnum, RNCode, RNnumX, RNstate2, RNnum2, RNCode2, RNnumX2, SpecialtyID, SpecialtyIDlist,
                ImportedFrom, FieldID, AgeGroup, BrowserAgent, Phone, PhoneType, Specialty, Education, Position, DateEntered,
                UCEend, Updated, LastLogin, IP, Disabled);

            PearlsReview.DAL.SQLClient.SQL2PRProvider obj = new PearlsReview.DAL.SQLClient.SQL2PRProvider();
                        bool ret = obj.UpdateNurse(record);
            return ret;
        }

        public bool Update()
        {
            return Nurse.UpdateNurse(this.rnID, this.UserName, this.Password, this.Registered,
                this.FirstName, this.LastName, this.Address1, this.Address2, this.City,
                this.State, this.Zip, this.Country, this.SAddress1, this.SAddress2, SCity, SState, SZip, SCountry, Email, EmailPref, OptIn,
                 Offers, RNstate, RNnum, RNCode, RNnumX, RNstate2, RNnum2, RNCode2, RNnumX2, SpecialtyID, SpecialtyIDlist,
                 ImportedFrom, FieldID, AgeGroup, BrowserAgent, Phone, PhoneType, Specialty, Education, Position, DateEntered,
                 UCEend, Updated, LastLogin, IP, Disabled);
        }
       

        #endregion

        #region Methods

        public static int SetEmailSubscription(int rnid, bool job, bool ezine, bool education)
        {
            return SiteProvider.PR2.SetEmailSubscription(rnid, job, ezine, education);
        } 

        public static Nurse GetNurseByID(int rnID)
        {
            Nurse Nurse = null;
            string key = "Nurses_Nurse_" + rnID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Nurse = (Nurse)BizObject.Cache[key];
            }
            else
            {
                Nurse = GetNurseFromNurseInfo(SiteProvider.PR2.GetNurseByID(rnID));
                BasePR.CacheData(key, Nurse);
            }
            return Nurse;
        }

        public static int CheckUserLogin(string UserName, string PassWord)
        {
            int rnid = SiteProvider.PR2.CheckUserLogin(UserName, PassWord);
            return rnid;
        }

        public static int IsUsernameExists(string Username)
        {
            int ret = SiteProvider.PR2.IsUsernameExists(Username);
            return ret;
        }

        public static int IsExistingRetailUser(string Email)
        {
            int ret = SiteProvider.PR2.IsExistingRetailUser(Email);
            return ret;
        }
        
        ///// <summary>
        ///// Returns a list of Nurse objects filled with the data taken from the input list of NrseuInfo
        ///// </summary>
        //private static List<Nurse> GetNurseListFromUNurseInfoList(List<NurseInfo> recordset)
        //{
        //    List<Nurse> Nurses = new List<Nurse>();
        //    foreach (NurseInfo record in recordset)
        //        Nurses.Add(GetNurseFromNurseInfo(record));
        //    return Nurses;
        //}
        /// <summary>
        /// Returns a UserAccount object filled with the data taken from the input UserAccountInfo
        /// </summary>
        private static Nurse GetNurseFromNurseInfo(NurseInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Nurse(record.rnID, record.UserName, record.Password, record.Registered,
                    record.FirstName, record.LastName, record.Address1, record.Address2, record.City,
                    record.State, record.Zip, record.Country, record.SAddress1, record.SAddress2,
                    record.SCity, record.SState, record.SZip, record.SCountry, record.Email,
                    record.EmailPref, record.OptIn, record.Offers, record.RNstate, record.RNnum,
                    record.RNCode, record.RNnumX, record.RNstate2, record.RNnum2,
                    record.RNCode2, record.RNnumX2, record.SpecialtyID, record.SpecialtyIDlist, record.ImportedFrom,
                    record.FieldID, record.AgeGroup, record.BrowserAgent, record.Phone, record.PhoneType,
                    record.Specialty, record.Education, record.Position, record.DateEntered, record.UCEend,
                    record.Updated, record.LastLogin, record.IP, record.Disabled);
            }
        }
        #endregion
    }
}
