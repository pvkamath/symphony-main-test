﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for SponsorSurveyQue
    /// </summary>
    public class SponsorSurveyQue : BasePR
    {
        #region Variables and Properties 

        private int _questionid = 0;
        public int questionid
        {
            get { return _questionid; }
            protected set { _questionid = value; }
        }

        private string _survey_text = "";
        public string survey_text
        {
            get { return _survey_text; }
            private set { _survey_text = value; }
        }

        private bool _optional_ind = true;
        public bool optional_ind
        {
            get { return _optional_ind; }
            private set { _optional_ind = value; }
        }

        public SponsorSurveyQue(int questionid, string survey_text, bool optional_ind)
        {
            this.questionid = questionid;
            this.survey_text = survey_text;
            this.optional_ind = optional_ind;
        }


        public bool Delete()
        {
            bool success = SponsorSurveyQue.DeleteSponsorSurveyQue(this.questionid);
            if (success)
                this.questionid = 0;
            return success;
        }

        public bool Update()
        {
            return SponsorSurveyQue.UpdateSponsorSurveyQue(this.questionid, this.survey_text, this.optional_ind);
        }

        
        #endregion

        #region Methods  

        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all SponsorSurveyQues
        //</summary>
        public static List<SponsorSurveyQue> GetSponsorSurveyQues(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "questionid";

            List<SponsorSurveyQue> SponsorSurveyQues = null;
            string key = "SponsorSurveyQues_SponsorSurveyQues_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SponsorSurveyQues = (List<SponsorSurveyQue>)BizObject.Cache[key];
            }
            else
            {
                List<SponsorSurveyQueInfo> recordset = SiteProvider.PR2.GetSponsorSurveyQues(cSortExpression);
                SponsorSurveyQues = GetSponsorSurveyQueListFromSponsorSurveyQueInfoList(recordset);
                BasePR.CacheData(key, SponsorSurveyQues);
            }
            return SponsorSurveyQues;
        }

        //Returns a collection with all SponsorSurveyQues
        //</summary>
        //public static List<SponsorSurveyQue> GetSponsorSurveyQuesByDiscountId(string cSortExpression)
        public static List<SponsorSurveyQue> GetSponsorSurveyQuesByDiscountId(string cSortExpression, Int32 discountid)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "questionid";
            //int discountid = 1;

            List<SponsorSurveyQue> SponsorSurveyQues = null;
            string key = "SponsorSurveyQues_SponsorSurveyQues_" + discountid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SponsorSurveyQues = (List<SponsorSurveyQue>)BizObject.Cache[key];
            }
            else
            {
                List<SponsorSurveyQueInfo> recordset = SiteProvider.PR2.GetSponsorSurveyQuesByDiscountId(cSortExpression, discountid);
                SponsorSurveyQues = GetSponsorSurveyQueListFromSponsorSurveyQueInfoList(recordset);
                BasePR.CacheData(key, SponsorSurveyQues);
            }
            return SponsorSurveyQues;
        }

        /// <summary>
        /// Returns a SponsorSurveyQue object with the specified ID
        /// </summary>
        public static SponsorSurveyQue GetSponsorSurveyQueByID(int questionid)
        {
            SponsorSurveyQue SponsorSurveyQue = null;
            string key = "SponsorSurveyQues_SponsorSurveyQue_" + questionid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SponsorSurveyQue = (SponsorSurveyQue)BizObject.Cache[key];
            }
            else
            {
                SponsorSurveyQue = GetSponsorSurveyQueFromSponsorSurveyQueInfo(SiteProvider.PR2.GetSponsorSurveyQueByID(questionid));
                BasePR.CacheData(key, SponsorSurveyQue);
            }
            return SponsorSurveyQue;
        }

        /// <summary>
        /// Creates a new SponsorSurveyQue
        /// </summary>
        public static int InsertSponsorSurveyQue(string survey_text, bool optional_ind)
        {
            //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
            //Comment = BizObject.ConvertNullToEmptyString(Comment);

            SponsorSurveyQueInfo record = new SponsorSurveyQueInfo(0, survey_text, optional_ind);
            int ret = SiteProvider.PR2.InsertSponsorSurveyQue(record);

            BizObject.PurgeCacheItems("SponsorSurveyQues_SponsorSurveyQue");
            return ret;
        }


        /// <summary>
        /// Updates an existing SponsorSurveyQue
        /// </summary>
        public static bool UpdateSponsorSurveyQue(int questionid, string survey_text, bool optional_ind)
        {
            //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
            //Comment = BizObject.ConvertNullToEmptyString(Comment);

            SponsorSurveyQueInfo record = new SponsorSurveyQueInfo(questionid, survey_text, optional_ind);
            bool ret = SiteProvider.PR2.UpdateSponsorSurveyQue(record);

            BizObject.PurgeCacheItems("SponsorSurveyQues_SponsorSurveyQue_" + questionid.ToString());
            BizObject.PurgeCacheItems("SponsorSurveyQues_SponsorSurveyQues");
            return ret;
        }


        /// <summary>
        /// Deletes an existing SponsorSurveyQue, but first checks if OK to delete
        /// </summary>
        public static bool DeleteSponsorSurveyQue(int questionid)
        {
            bool IsOKToDelete = OKToDelete(questionid);
            if (IsOKToDelete)
            {
                return (bool)DeleteSponsorSurveyQue(questionid, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing SponsorSurveyQue - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteSponsorSurveyQue(int questionid, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteSponsorSurveyQue(questionid);
            BizObject.PurgeCacheItems("SponsorSurveyQue_SponsorSurveyQue");
            return ret;
        }

        /// <summary>
        /// Checks to see if a SponsorSurveyQue can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }



        /// <summary>
        /// Returns a SponsorSurveyQue object filled with the data taken from the input SponsorSurveyQueInfo
        /// </summary>
        private static SponsorSurveyQue GetSponsorSurveyQueFromSponsorSurveyQueInfo(SponsorSurveyQueInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new SponsorSurveyQue(record.questionid, record.survey_text, record.optional_ind);
            }
        }

        /// <summary>
        /// Returns a list of SponsorSurveyQue objects filled with the data taken from the input list of SponsorSurveyQueInfo
        /// </summary>
        private static List<SponsorSurveyQue> GetSponsorSurveyQueListFromSponsorSurveyQueInfoList(List<SponsorSurveyQueInfo> recordset)
        {
            List<SponsorSurveyQue> SponsorSurveyQues = new List<SponsorSurveyQue>();
            foreach (SponsorSurveyQueInfo record in recordset)
                SponsorSurveyQues.Add(GetSponsorSurveyQueFromSponsorSurveyQueInfo(record));
            return SponsorSurveyQues;
        }
       
        #endregion
    }
}
