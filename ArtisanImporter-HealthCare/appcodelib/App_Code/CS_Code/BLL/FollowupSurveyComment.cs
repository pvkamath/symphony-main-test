﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    public class FollowupSurveyComment : BasePR
    {
        #region Variables and Properties
        private int _FSID = 0;
        public int FSID
        {
            get { return _FSID; }
            set { _FSID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _TestID = 0;
        public int TestID
        {
            get { return _TestID; }
            set { _TestID = value; }
        }

        private int _QNumber = 0;
        public int QNumber
        {
            get { return _QNumber; }
            set { _QNumber = value; }
        }      

        private DateTime _SurveyDate = DateTime.Now;
        public DateTime SurveyDate
        {
            get { return _SurveyDate; }
            set { _SurveyDate = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }
        #endregion

        #region Methods
        public static int InsertFollowupSurveyComment(int topicID, int testID, DateTime surveyDate, int qNumber, string comment)
        {            
            comment = BizObject.ConvertNullToEmptyString(comment);

            int ret = SiteProvider.PR2.InsertFollowupSurveyComment(topicID, testID, surveyDate,  qNumber, comment);
            BizObject.PurgeCacheItems("FollowupSurveyComments_FollowupSurveyComment");
            return ret;
        }

        /// <summary>
        /// DeleteFollowupSurveyCommentByTestId
        /// </summary>
        public static bool DeleteFollowupSurveyCommentByTestId(int TestId)
        {
            bool ret = SiteProvider.PR2.DeleteFollowupSurveyCommentByTestId(TestId);
            return ret;
        }
        #endregion Methods
    }
}