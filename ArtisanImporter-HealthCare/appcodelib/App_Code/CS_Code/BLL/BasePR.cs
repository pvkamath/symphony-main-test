using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using PearlsReview.DAL;

namespace PearlsReview.BLL.PRBase
{
   public abstract class BasePR : BizObject
   {
      protected static PRElement Settings
      {
         get 
         {
             PRElement prElement = new PRElement();
             prElement.ConnectionStringName = "HealthCare";
             return prElement;
             //return Globals.Settings.PR; 
         }
      }

      /// <summary>
      /// Cache the input data, if caching is enabled
      /// </summary>
      protected static void CacheData(string key, object data)
      {
         if (Settings.EnableCaching && data != null)
         {
            BizObject.Cache.Insert(key, data, null,
               DateTime.Now.AddSeconds(Settings.CacheDuration), TimeSpan.Zero);
         }
      }

      ///// <summary>
      ///// Overridable (virtual) method that just returns true
      ///// to allow deletion. This will be overridden in subclasses
      ///// as needed, so deletion can be prohibited under certain conditions
      ///// </summary>
      //public virtual bool OKToDelete()
      //{
      //    return true;
      //}

      ///// <summary>
      ///// Overridable (virtual) method that just returns true
      ///// to allow deletion. This will be overridden in subclasses
      ///// as needed, so deletion can be prohibited under certain conditions
      /////
      ///// NOTE: This is a version with an integer PK passed in (most subclassed
      ///// versions will need this signature)
      ///// </summary>
      //public virtual bool OKToDelete(int PK)
      //{
      //    return true;
      //}
 
   }
}