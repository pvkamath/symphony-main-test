﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Alex Chen
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    public class SurveyComment : BasePR
    {
        #region Variables and Properties
        private int _SID = 0;
        public int SID
        {
            get { return _SID; }
            set { _SID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private long _TestID = 0;
        public long TestID
        {
            get { return _TestID; }
            set { _TestID = value; }
        }

        private int _QNumber = 0;
        public int QNumber
        {
            get { return _QNumber; }
            set { _QNumber = value; }
        }

        private string _QText = "";
        public string QText
        {
            get { return _QText; }
            set { _QText = value; }
        }

        private DateTime _SurveyDate = DateTime.Now;
        public DateTime SurveyDate
        {
            get { return _SurveyDate; }
            set { _SurveyDate = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }
        #endregion

        #region Methods
        public static int InsertSurveyComment(int topicID, long testID, DateTime surveyDate, string qText, int qNumber, string comment)
        {
            qText = BizObject.ConvertNullToEmptyString(qText);
            comment = BizObject.ConvertNullToEmptyString(comment);

            int ret = SiteProvider.PR2.InsertSurveyComment(topicID, testID, surveyDate, qText, qNumber, comment);
            BizObject.PurgeCacheItems("SurveyComments_SurveyComment");
            return ret;
        }
        #endregion Methods
    }
}