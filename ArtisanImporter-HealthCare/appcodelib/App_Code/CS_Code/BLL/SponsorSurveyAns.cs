﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for SponsorSurveyAns
    /// </summary>
    public class SponsorSurveyAns : BasePR
    {
        #region Variables and Properties 

        private int _answerid = 0;
        public int answerid
        {
            get { return _answerid; }
            protected set { _answerid = value; }
        }

        private int _questionid = 0;
        public int questionid
        {
            get { return _questionid; }
            protected set { _questionid = value; }
        }

        private string _answer_text = "";
        public string answer_text
        {
            get { return _answer_text; }
            private set { _answer_text = value; }
        }

        private bool _share_ind = true;
        public bool share_ind
        {
            get { return _share_ind; }
            private set { _share_ind = value; }
        }

        public SponsorSurveyAns(int answerid, int questionid, string answer_text, bool share_ind)
        {
            this.answerid = answerid;
            this.questionid = questionid;
            this.answer_text = answer_text;
            this.share_ind = share_ind;
        }


        public bool Delete()
        {
            bool success = SponsorSurveyAns.DeleteSponsorSurveyAns(this.questionid);
            if (success)
                this.questionid = 0;
            return success;
        }

        public bool Update()
        {
            return SponsorSurveyAns.UpdateSponsorSurveyAns(this.answerid, this.questionid, this.answer_text, this.share_ind);
        }

        
        #endregion

        #region Method 

        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all SponsorSurveyAns
        //</summary>
        public static List<SponsorSurveyAns> GetSponsorSurveyAns(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "answerid";

            List<SponsorSurveyAns> SponsorSurveyAns = null;
            string key = "SponsorSurveyAns_SponsorSurveyAns_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SponsorSurveyAns = (List<SponsorSurveyAns>)BizObject.Cache[key];
            }
            else
            {
                List<SponsorSurveyAnsInfo> recordset = SiteProvider.PR2.GetSponsorSurveyAns(cSortExpression);
                SponsorSurveyAns = GetSponsorSurveyAnsListFromSponsorSurveyAnsInfoList(recordset);
                BasePR.CacheData(key, SponsorSurveyAns);
            }
            return SponsorSurveyAns;
        }

        //<summary>
        //Returns a collection with all SponsorSurveyAns
        //</summary>
        public static List<SponsorSurveyAns> GetSponsorSurveyAnsByQuestionId(int questionid)
        {

            List<SponsorSurveyAns> SponsorSurveyAns = null;
            string key = "SponsorSurveyAns_SponsorSurveyAns_" + questionid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SponsorSurveyAns = (List<SponsorSurveyAns>)BizObject.Cache[key];
            }
            else
            {
                List<SponsorSurveyAnsInfo> recordset = SiteProvider.PR2.GetSponsorSurveyAnsByQuestionId(questionid);
                SponsorSurveyAns = GetSponsorSurveyAnsListFromSponsorSurveyAnsInfoList(recordset);
                BasePR.CacheData(key, SponsorSurveyAns);
            }
            return SponsorSurveyAns;
        }
        //<summary>
        //Returns a collection with all SponsorSurveyAns
        //</summary>
        public static List<SponsorSurveyAns> GetSponsorSurveyAnsByQuestionIdAndShareInd(int questionid, bool share_ind)
        {

            List<SponsorSurveyAns> SponsorSurveyAns = null;
            string key = "SponsorSurveyAns_SponsorSurveyAns_" + questionid.ToString() + share_ind;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SponsorSurveyAns = (List<SponsorSurveyAns>)BizObject.Cache[key];
            }
            else
            {
                List<SponsorSurveyAnsInfo> recordset = SiteProvider.PR2.GetSponsorSurveyAnsByQuestionIdAndShareInd(questionid, share_ind);
                SponsorSurveyAns = GetSponsorSurveyAnsListFromSponsorSurveyAnsInfoList(recordset);
                BasePR.CacheData(key, SponsorSurveyAns);
            }
            return SponsorSurveyAns;
        }

        /// <summary>
        /// Updates an existing SponsorSurveyAns
        /// </summary>
        public static bool UpdateSponsorSurveysByDiscountidAndAnsweridAndShareInd(int questionid, int Answerid, bool ShareInd)
        {

            bool ret = SiteProvider.PR2.UpdateSponsorSurveysByDiscountidAndAnsweridAndShareInd(questionid, Answerid, ShareInd);

            BizObject.PurgeCacheItems("SponsorSurveyAns_SponsorSurveyAns_" + Answerid.ToString());
            BizObject.PurgeCacheItems("SponsorSurveyAns_SponsorSurveyAns");
            return ret;
        }


        /// <summary>
        /// Returns a SponsorSurveyAns object with the specified ID
        /// </summary>
        public static SponsorSurveyAns GetSponsorSurveyAnsByID(int AnswerId)
        {
            SponsorSurveyAns SponsorSurveyAns = null;
            string key = "SponsorSurveyAns_SponsorSurveyAns_" + AnswerId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SponsorSurveyAns = (SponsorSurveyAns)BizObject.Cache[key];
            }
            else
            {
                SponsorSurveyAns = GetSponsorSurveyAnsFromSponsorSurveyAnsInfo(SiteProvider.PR2.GetSponsorSurveyAnsByID(AnswerId));
                BasePR.CacheData(key, SponsorSurveyAns);
            }
            return SponsorSurveyAns;
        }

       public static SponsorSurveyAns GetSponsorSurveyAnsBySponserDetails(int userid, int discountid,int questionid)
        {
            SponsorSurveyAns SpnsorSurveyAns = null;
            string key = "SponsorSurveyAns_SponsorSurveyAns_" + questionid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                SpnsorSurveyAns = (SponsorSurveyAns)BizObject.Cache[key];
            }
            else
            {
                SpnsorSurveyAns = GetSponsorSurveyAnsFromSponsorSurveyAnsInfo(SiteProvider.PR2.GetSponsorSurveyAnsBySponserDetails(userid, discountid, questionid));
                BasePR.CacheData(key, SpnsorSurveyAns);
            }
            return SpnsorSurveyAns;
        }


        /// <summary>
        /// Creates a new SponsorSurveyAns
        /// </summary>
        public static int InsertSponsorSurveyAns(int questionid, string answer_text, bool share_ind)
        {
            //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
            //Comment = BizObject.ConvertNullToEmptyString(Comment);

            SponsorSurveyAnsInfo record = new SponsorSurveyAnsInfo(0, questionid, answer_text, share_ind);
            int ret = SiteProvider.PR2.InsertSponsorSurveyAns(record);

            BizObject.PurgeCacheItems("SponsorSurveyAns_SponsorSurveyAns");
            return ret;
        }


        /// <summary>
        /// Updates an existing SponsorSurveyAns
        /// </summary>
        public static bool UpdateSponsorSurveyAns(int answerid, int questionid, string answer_text, bool share_ind)
        {
            //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
            //Comment = BizObject.ConvertNullToEmptyString(Comment);

            SponsorSurveyAnsInfo record = new SponsorSurveyAnsInfo(answerid, questionid, answer_text, share_ind);
            bool ret = SiteProvider.PR2.UpdateSponsorSurveyAns(record);

            BizObject.PurgeCacheItems("SponsorSurveyAns_SponsorSurveyAns_" + questionid.ToString());
            BizObject.PurgeCacheItems("SponsorSurveyAns_SponsorSurveyAns");
            return ret;
        }


        /// <summary>
        /// Deletes an existing SponsorSurveyAns, but first checks if OK to delete
        /// </summary>
        public static bool DeleteSponsorSurveyAns(int questionid)
        {
            bool IsOKToDelete = OKToDelete(questionid);
            if (IsOKToDelete)
            {
                return (bool)DeleteSponsorSurveyAns(questionid, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing SponsorSurveyAns - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteSponsorSurveyAns(int questionid, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteSponsorSurveyAns(questionid);
            BizObject.PurgeCacheItems("SponsorSurveyAns_SponsorSurveyAns");
            return ret;
        }

        /// <summary>
        /// Checks to see if a SponsorSurveyAns can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }



        /// <summary>
        /// Returns a SponsorSurveyAns object filled with the data taken from the input SponsorSurveyAnsInfo
        /// </summary>
        private static SponsorSurveyAns GetSponsorSurveyAnsFromSponsorSurveyAnsInfo(SponsorSurveyAnsInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new SponsorSurveyAns(record.answerid, record.questionid, record.answer_text, record.share_ind);
            }
        }

        /// <summary>
        /// Returns a list of SponsorSurveyAns objects filled with the data taken from the input list of SponsorSurveyAnsInfo
        /// </summary>
        private static List<SponsorSurveyAns> GetSponsorSurveyAnsListFromSponsorSurveyAnsInfoList(List<SponsorSurveyAnsInfo> recordset)
        {
            List<SponsorSurveyAns> SponsorSurveyAns = new List<SponsorSurveyAns>();
            foreach (SponsorSurveyAnsInfo record in recordset)
                SponsorSurveyAns.Add(GetSponsorSurveyAnsFromSponsorSurveyAnsInfo(record));
            return SponsorSurveyAns;
        }
            

        #endregion
    }
}
