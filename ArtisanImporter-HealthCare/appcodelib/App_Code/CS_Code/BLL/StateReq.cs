﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for StateReq
/// </summary>
    public class StateReq : BasePR
    {
        #region Variables and Properties 

        private string _stateabr = "";
        public string StateAbr
        {
            get { return _stateabr; }
            private set { _stateabr = value; }
        }

        private string _statename = "";
        public string StateName
        {
            get { return _statename; }
            private set { _statename = value; }
        }

        private string _cereq = "";
        public string CeReq
        {
            get { return _cereq; }
            private set { _cereq = value; }
        }

        private string _licagency = "";
        public string LicAgency
        {
            get { return _licagency; }
            private set { _licagency = value; }
        }

        private string _permlic = "";
        public string Permlic
        {
            get { return _permlic; }
            private set { _permlic = value; }
        }

        private string _templic = "";
        public string TempLic
        {
            get { return _templic; }
            private set { _templic = value; }
        }

        private string _extralic = "";
        public string ExtraLic
        {
            get { return _extralic; }
            private set { _extralic = value; }
        }

        private string _email = "";
        public string Email
        {
            get { return _email; }
            private set { _email = value; }
        }

        private string _url = "";
        public string Url
        {
            get { return _url; }
            private set { _url = value; }
        }

        private string _lpn = "";
        public string Lpn
        {
            get { return _lpn; }
            private set { _lpn = value; }
        }

        private DateTime _lastupdate = System.DateTime.Now;
        public DateTime LastUpdate
        {
            get { return _lastupdate; }
            private set { _lastupdate = value; }
        }

        private int _categoryid = 0;
        public int CategoryId
        {
            get { return _categoryid; }
            protected set { _categoryid = value; }
        }

        private decimal _taxrate = 0.00M;
        public decimal TaxRate
        {
            get { return _taxrate; }
            private set { _taxrate = value; }
        }

        private bool _inter_ind = true;
        public bool Inter_Ind
        {
            get { return _inter_ind; }
            private set { _inter_ind = value; }
        }

        private string _req_header = "";
        public string req_header
        {
            get { return _req_header; }
            private set { _req_header = value; }
        }

        private string _course_recommend = "";
        public string course_recommend
        {
            get { return _course_recommend; }
            private set { _course_recommend = value; }
        }

        private string _region = "";
        public string Region
        {
            get { return _region; }
            private set { _region = value; }
        }

        public StateReq(string stateabr, string statename, string cereq, string licagency, string permlic, string templic,
           string extralic, string email, string url, string lpn, DateTime lastupdate, int categoryid, decimal taxrate, bool inter_ind, string req_header, string course_recommend, string region)
        {
            this.StateAbr = stateabr;
            this.StateName = statename;
            this.CeReq = cereq;
            this.LicAgency = licagency;
            this.Permlic = permlic;
            this.TempLic = templic;
            this.ExtraLic = extralic;
            this.Email = email;
            this.Url = url;
            this.Lpn = lpn;
            this.LastUpdate = lastupdate;
            this.CategoryId = categoryid;
            this.TaxRate = taxrate;
            this.Inter_Ind = inter_ind;
            this.req_header = req_header;
            this.course_recommend = course_recommend;
            this.Region = region;
        }


        public bool Delete()
        {
            bool success = StateReq.DeleteStateReq(this.StateAbr);
            if (success)
                this.StateAbr = "";
            return success;
        }

        public bool Update()
        {
            return StateReq.UpdateStateReq(this.StateAbr, this.StateName, this.CeReq, this.LicAgency, this.Permlic, this.TempLic,
           this.ExtraLic, this.Email, this.Url, this.Lpn, this.LastUpdate, this.CategoryId, this.TaxRate, this.Inter_Ind, this.req_header, this.course_recommend, this.Region);
        }

       
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all StateReqs
        //</summary>
        public static List<StateReq> GetStateReqs(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "statename";

            List<StateReq> StateReqs = null;
            string key = "StateReqs_StateReqs_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                StateReqs = (List<StateReq>)BizObject.Cache[key];
            }
            else
            {
                List<StateReqInfo> recordset = SiteProvider.PR2.GetStateReqs(cSortExpression);
                StateReqs = GetStateReqListFromStateReqInfoList(recordset);
                BasePR.CacheData(key, StateReqs);
            }
            return StateReqs;
        }

        public static List<StateReq> GetAllStates(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "ord, statename";

            List<StateReq> StateReqs = null;
            string key = "StateReqs_StateReqs_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                StateReqs = (List<StateReq>)BizObject.Cache[key];
            }
            else
            {
                List<StateReqInfo> recordset = SiteProvider.PR2.GetAllStates(cSortExpression);
                StateReqs = GetStateReqListFromStateReqInfoList(recordset);
                BasePR.CacheData(key, StateReqs);
            }
            return StateReqs;
        }
        public static List<StateReq> GetMicrositeStatesReq(int msid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "statename";

            List<StateReq> StateReqs = null;
            string key = "StateReqs_StateReqs_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                StateReqs = (List<StateReq>)BizObject.Cache[key];
            }
            else
            {
                List<StateReqInfo> recordset = SiteProvider.PR2.GetMicrositeStatesReq(msid, cSortExpression);
                StateReqs = GetStateReqListFromStateReqInfoList(recordset);
                BasePR.CacheData(key, StateReqs);
            }
            return StateReqs;
        }
        public static List<StateReq> GetStates(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "statename";

            List<StateReq> StateReqs = null;
            string key = "StateReqs_StateReqs_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                StateReqs = (List<StateReq>)BizObject.Cache[key];
            }
            else
            {
                List<StateReqInfo> recordset = SiteProvider.PR2.GetStates(cSortExpression);
                StateReqs = GetStateReqListFromStateReqInfoList(recordset);
                BasePR.CacheData(key, StateReqs);
            }
            return StateReqs;
        }
        public static string GetStateLongName(string StateAbbr)
        {
            List<StateReq> list = GetStateReqListFromStateReqInfoList(SQL2PRProvider.Instance.GetAllStates("statename"));
            return list.Where(x =>x.StateAbr == StateAbbr).First().StateName;
        }
        public static List<StateReq> GetStatesByRegion(string region)
        {
            List<StateReq> StateReqs = null;
            string key = "StateReqs_StateReqs_" + region;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                StateReqs = (List<StateReq>)BizObject.Cache[key];
            }
            else
            {
                List<StateReqInfo> recordset = SiteProvider.PR2.GetStatesByRegion(region);
                StateReqs = GetStateReqListFromStateReqInfoList(recordset);
                BasePR.CacheData(key, StateReqs);
            }
            return StateReqs;
        }
        //<summary>
        //Returns a collection with all StateReqs
        //</summary>
        public static List<StateReq> GetMicrositeStateReqsWithLink(int msid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "statename";

            List<StateReq> StateReqs = null;
            string key = "MicrositeStateReqsWithLink_StateReqs_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                StateReqs = (List<StateReq>)BizObject.Cache[key];
            }
            else
            {
                List<StateReqInfo> recordset = SiteProvider.PR2.GetMicrositeStateReqsWithLink(msid, cSortExpression);
                StateReqs = GetStateReqListFromStateReqInfoList(recordset);
                BasePR.CacheData(key, StateReqs);
            }
            return StateReqs;
        }
        //<summary>
        //Returns a collection with all StateReqs
        //</summary>
        public static List<StateReq> GetStateReqsWithLink(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "statename";

            List<StateReq> StateReqs = null;
            string key = "StateReqsWithLink_StateReqs_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                StateReqs = (List<StateReq>)BizObject.Cache[key];
            }
            else
            {
                List<StateReqInfo> recordset = SiteProvider.PR2.GetStateReqsWithLink(cSortExpression);
                StateReqs = GetStateReqListFromStateReqInfoList(recordset);
                BasePR.CacheData(key, StateReqs);
            }
            return StateReqs;
        }
        /// <summary>
        /// Returns a StateReq object with the specified ID
        /// </summary>
        public static StateReq GetStateReqByStateAbr(string StateAbr)
        {
            StateReq StateReq = null;
            string key = "StateReqs_StateReq_" + StateAbr.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                StateReq = (StateReq)BizObject.Cache[key];
            }
            else
            {
                StateReq = GetStateReqFromStateReqInfo(SiteProvider.PR2.GetStateReqByStateAbr(StateAbr));
                BasePR.CacheData(key, StateReq);
            }
            return StateReq;
        }

        /// Returns a StateReq object with the specified ID
        /// </summary>
        public static StateReq GetStateReqByStateName(string Statename)
        {
            StateReq StateReq = null;
            string key = "StateReqs_StateReq_" + Statename.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                StateReq = (StateReq)BizObject.Cache[key];
            }
            else
            {
                StateReq = GetStateReqFromStateReqInfo(SiteProvider.PR2.GetStateReqByStateName(Statename));
                BasePR.CacheData(key, StateReq);
            }
            return StateReq;
        }

        /// Returns a StateReq object with the specified ID
        /// </summary>
        public static StateReq GetStateReqByStateNameorAbr(string StatenameOrAbr)
        {
            StateReq StateReq = null;
            string key = "StateReqs_StateReq_" + StatenameOrAbr.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                StateReq = (StateReq)BizObject.Cache[key];
            }
            else
            {
                StateReq = GetStateReqFromStateReqInfo(SiteProvider.PR2.GetStateReqByStateNameorAbr(StatenameOrAbr));
                BasePR.CacheData(key, StateReq);
            }
            return StateReq;
        }


        /// <summary>
        /// Updates an existing StateReq
        /// </summary>
        public static bool UpdateStateReq(string stateabr, string statename, string cereq, string licagency, string permlic, string templic,
           string extralic, string email, string url, string lpn, DateTime lastupdate, int categoryid, decimal taxrate, bool inter_ind, string req_header, string course_recommend, string region)
        {
            //Parent_name = BizObject.ConvertNullToEmptyString(Parent_name);
            //Comment = BizObject.ConvertNullToEmptyString(Comment);

            StateReqInfo record = new StateReqInfo(stateabr, statename, cereq, licagency, permlic, templic,
            extralic, email, url, lpn, lastupdate, categoryid, taxrate, inter_ind, req_header, course_recommend, region);
            bool ret = SiteProvider.PR2.UpdateStateReq(record);

            BizObject.PurgeCacheItems("StateReqs_StateReq_" + stateabr.ToString());
            BizObject.PurgeCacheItems("StateReqs_StateReqs");
            return ret;
        }


        public static bool UpdateStateReqByStateAbr(string Cereq, string Email, string Url, string LicAgency, string Lpn, string Stateabr, int CategoryId, string req_header, string course_recommend, string region)
        {
            bool ret = SiteProvider.PR2.UpdateStateReqByStateAbr(Cereq, Email, Url, LicAgency, Lpn, Stateabr, CategoryId, req_header, course_recommend, region);
            // TODO: release cache?
            return ret;
        }

        /// <summary>
        /// Deletes an existing StateReq, but first checks if OK to delete
        /// </summary>
        public static bool DeleteStateReq(string stateabr)
        {
            bool IsOKToDelete = OKToDelete(stateabr);
            if (IsOKToDelete)
            {
                return (bool)DeleteStateReq(stateabr, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing StateReq - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteStateReq(string stateabr, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteStateReq(stateabr);
            BizObject.PurgeCacheItems("StateReq_StateReq");
            return ret;
        }

        /// <summary>
        /// Checks to see if a StateReq can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(string EDefID)
        {
            return true;
        }



        /// <summary>
        /// Returns a StateReq object filled with the data taken from the input StateReqInfo
        /// </summary>
        private static StateReq GetStateReqFromStateReqInfo(StateReqInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new StateReq(record.StateAbr, record.StateName, record.CeReq, record.LicAgency, record.Permlic, record.TempLic,
                    record.ExtraLic, record.Email, record.Url, record.Lpn, record.LastUpdate, record.CategoryId, record.TaxRate, record.Inter_Ind, record.req_header, record.course_recommend, record.Region);
            }
        }

        /// <summary>
        /// Returns a list of StateReq objects filled with the data taken from the input list of StateReqInfo
        /// </summary>
        private static List<StateReq> GetStateReqListFromStateReqInfoList(List<StateReqInfo> recordset)
        {
            List<StateReq> StateReqs = new List<StateReq>();
            foreach (StateReqInfo record in recordset)
                StateReqs.Add(GetStateReqFromStateReqInfo(record));
            return StateReqs;
        }

        /// <summary>
        /// Returns a StateReq object with the specified Category ID
        /// </summary>
        public static StateReq GetStateReqByCategoryId(int CategoryId)
        {
            StateReq StateReq = null;
            string key = "StateReqs_StateReq_" + CategoryId.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                StateReq = (StateReq)BizObject.Cache[key];
            }
            else
            {
                StateReq = GetStateReqFromStateReqInfo(SiteProvider.PR2.GetStateReqByCategoryId(CategoryId));
                BasePR.CacheData(key, StateReq);
            }
            return StateReq;
        }
       
        #endregion
    }
}