﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

/// <summary>
/// Summary description for Promotion
/// </summary>
    public class Promotion : BasePR
    {
        #region Variables and Properties

        private int _ID = 0;
        public int ID
        {
            get { return _ID; }
            protected set { _ID = value; }
        }

        private string _cCode = "";
        public string cCode
        {
            get { return _cCode; }
            set { _cCode = value; }
        }

        private string _PromoType = "";
        public string PromoType
        {
            get { return _PromoType; }
            set { _PromoType = value; }
        }

        private string _PromoName = "";
        public string PromoName
        {
            get { return _PromoName; }
            set { _PromoName = value; }
        }

        private bool _lActive = false;
        public bool lActive
        {
            get { return _lActive; }
            set { _lActive = value; }
        }

        private bool _lOneTime = false;
        public bool lOneTime
        {
            get { return _lOneTime; }
            set { _lOneTime = value; }
        }

        private decimal _nDollars = 0.00M;
        public decimal nDollars
        {
            get { return _nDollars; }
            set { _nDollars = value; }
        }

        private string _Notes = "";
        public string Notes
        {
            get { return _Notes; }
            set { _Notes = value; }
        }

        private decimal _nPercent = 0.00M;
        public decimal nPercent
        {
            get { return _nPercent; }
            set { _nPercent = value; }
        }

        private decimal _nUnits = 0.00M;
        public decimal nUnits
        {
            get { return _nUnits; }
            set { _nUnits = value; }
        }

        private DateTime _tExpires = System.DateTime.Now.AddYears(1);
        public DateTime tExpires
        {
            get { return _tExpires; }
            set { _tExpires = value; }
        }

        private DateTime _tStart = System.DateTime.Now;
        public DateTime tStart
        {
            get { return _tStart; }
            set { _tStart = value; }
        }

        private int _RepID = 0;
        public int RepID
        {
            get { return _RepID; }
            set { _RepID = value; }
        }


        public Promotion(int ID, string cCode, string PromoType, string PromoName, bool lActive,
           bool lOneTime, decimal nDollars, string Notes, decimal nPercent, decimal nUnits, DateTime tExpires,
           DateTime tStart, int RepID)
        {
            this.ID = ID;
            this.cCode = cCode;
            this.PromoType = PromoType;
            this.PromoName = PromoName;
            this.lActive = lActive;
            this.lOneTime = lOneTime;
            this.nDollars = nDollars;
            this.Notes = Notes;
            this.nPercent = nPercent;
            this.nUnits = nUnits;
            this.tExpires = tExpires;
            this.tStart = tStart;
            this.RepID = RepID;
        }

        public bool Delete()
        {
            bool success = Promotion.DeletePromotion(this.ID);
            if (success)
                this.ID = 0;
            return success;
        }

        public bool Update()
        {
            return Promotion.UpdatePromotion(this.ID, this.cCode, this.PromoType, this.PromoName,
                this.lActive, this.lOneTime, this.nDollars, this.Notes, this.nPercent, this.nUnits,
                this.tExpires, this.tStart, this.RepID);
        }
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Promotions
        /// </summary>
        public static List<Promotion> GetPromotions(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "cCode";

            List<Promotion> Promotions = null;
            string key = "Promotions_Promotions_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Promotions = (List<Promotion>)BizObject.Cache[key];
            }
            else
            {
                List<PromotionInfo> recordset = SiteProvider.PR2.GetPromotions(cSortExpression);
                Promotions = GetPromotionListFromPromotionInfoList(recordset);
                BasePR.CacheData(key, Promotions);
            }
            return Promotions;
        }


        /// <summary>
        /// Returns the number of total Promotions
        /// </summary>
        public static int GetPromotionCount()
        {
            int PromotionCount = 0;
            string key = "Promotions_PromotionCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                PromotionCount = (int)BizObject.Cache[key];
            }
            else
            {
                PromotionCount = SiteProvider.PR2.GetPromotionCount();
                BasePR.CacheData(key, PromotionCount);
            }
            return PromotionCount;
        }

        /// <summary>
        /// Returns a Promotion object with the specified ID
        /// </summary>
        public static Promotion GetPromotionByID(int ID)
        {
            Promotion Promotion = null;
            string key = "Promotions_Promotion_" + ID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Promotion = (Promotion)BizObject.Cache[key];
            }
            else
            {
                Promotion = GetPromotionFromPromotionInfo(SiteProvider.PR2.GetPromotionByID(ID));
                BasePR.CacheData(key, Promotion);
            }
            return Promotion;
        }

        /// <summary>
        /// Updates an existing Promotion
        /// </summary>
        public static bool UpdatePromotion(int ID, string cCode, string PromoType, string PromoName, bool lActive,
           bool lOneTime, decimal nDollars, string Notes, decimal nPercent, decimal nUnits, DateTime tExpires,
           DateTime tStart, int RepID)
        {
            cCode = BizObject.ConvertNullToEmptyString(cCode);
            Notes = BizObject.ConvertNullToEmptyString(Notes);
            PromoType = BizObject.ConvertNullToEmptyString(PromoType);
            PromoName = BizObject.ConvertNullToEmptyString(PromoName);

            PromotionInfo record = new PromotionInfo(ID, cCode, PromoType, PromoName, lActive,
           lOneTime, nDollars, Notes, nPercent, nUnits, tExpires,
           tStart, RepID);
            bool ret = SiteProvider.PR2.UpdatePromotion(record);

            BizObject.PurgeCacheItems("Promotions_Promotion_" + ID.ToString());
            BizObject.PurgeCacheItems("Promotions_Promotions");
            return ret;
        }

        /// <summary>
        /// Creates a new Promotion
        /// </summary>
        public static int InsertPromotion(string cCode, string PromoType, string PromoName, bool lActive,
           bool lOneTime, decimal nDollars, string Notes, decimal nPercent, decimal nUnits, DateTime tExpires,
           DateTime tStart, int RepID)
        {
            cCode = BizObject.ConvertNullToEmptyString(cCode);
            Notes = BizObject.ConvertNullToEmptyString(Notes);
            PromoType = BizObject.ConvertNullToEmptyString(PromoType);
            PromoName = BizObject.ConvertNullToEmptyString(PromoName);

            PromotionInfo record = new PromotionInfo(0, cCode, PromoType, PromoName, lActive,
           lOneTime, nDollars, Notes, nPercent, nUnits, tExpires,
           tStart, RepID);
            int ret = SiteProvider.PR2.InsertPromotion(record);

            BizObject.PurgeCacheItems("Promotions_Promotion");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Promotion, but first checks if OK to delete
        /// </summary>
        public static bool DeletePromotion(int ID)
        {
            bool IsOKToDelete = OKToDelete(ID);
            if (IsOKToDelete)
            {
                return (bool)DeletePromotion(ID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Promotion - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeletePromotion(int ID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeletePromotion(ID);
            //         new RecordDeletedEvent("Promotion", ID, null).Raise();
            BizObject.PurgeCacheItems("Promotions_Promotion");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Promotion can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int ID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Promotion object filled with the data taken from the input PromotionInfo
        /// </summary>
        private static Promotion GetPromotionFromPromotionInfo(PromotionInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Promotion(record.ID, record.cCode, record.PromoType, record.PromoName,
                record.lActive, record.lOneTime, record.nDollars, record.Notes, record.nPercent, record.nUnits,
                record.tExpires, record.tStart, record.RepID);
            }
        }

        /// <summary>
        /// Returns a list of Promotion objects filled with the data taken from the input list of PromotionInfo
        /// </summary>
        private static List<Promotion> GetPromotionListFromPromotionInfoList(List<PromotionInfo> recordset)
        {
            List<Promotion> Promotions = new List<Promotion>();
            foreach (PromotionInfo record in recordset)
                Promotions.Add(GetPromotionFromPromotionInfo(record));
            return Promotions;
        }
        #endregion
    }
}
