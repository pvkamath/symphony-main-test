﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr1
    /// </summary>

    public class RetailFacility : BasePR
    {
        public RetailFacility(int rfacid, int facid, DateTime expiredate, bool renew, bool active, int seat, decimal recuramount, string membertype, string transid, string rfcomment, string cclast, string profileid)
        {
            this.Rfacid = rfacid;
            this.Facid = facid;
            this.Expiredate = expiredate;
            this.Renew = renew;
            this.Active = active;
            this.Seat = seat;
            this.RecurAmount = recuramount;
            this.ProfileID = profileid;
            this.MemberType = membertype;
            this.TransId = transid;
            this.Rfcomment = rfcomment;
            this.CCLast = cclast;
            this.ProfileID = profileid;
        }


        #region Variables and Properties
        private int _rfacid = 0;
        public int Rfacid
        {
            get { return _rfacid; }
            set { _rfacid = value; }
        }
        private int _facid = 0;
        public int Facid
        {
            get { return _facid; }
            set { _facid = value; }
        }
        private DateTime _expiredate = System.DateTime.MinValue;
        public DateTime Expiredate
        {
            get { return _expiredate; }
            set { _expiredate = value; }
        }
        private bool _renew = false;
        public bool Renew
        {
            get { return _renew; }
            set { _renew = value; }
        }
        private bool _active = false;
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
        private int _seat = 0;
        public int Seat
        {
            get { return _seat; }
            set { _seat = value; }
        }
        private decimal _recurAmount = 0;
        public decimal RecurAmount
        {
            get { return _recurAmount; }
            set { _recurAmount = value; }
        }
        private string _profileid = "";
        public string ProfileID
        {
            get { return _profileid; }
            set { _profileid = value; }
        }
        private string _membertype = "";
        public string MemberType
        {
            get { return _membertype; }
            set { _membertype = value; }
        }
        private string _transid = "";
        public string TransId
        {
            get { return _transid; }
            set { _transid = value; }
        }
        private string _rfcomment = "";
        public string Rfcomment
        {
            get { return _rfcomment; }
            set { _rfcomment = value; }
        }
        private string _cclast = "";
        public string CCLast
        {
            get { return _cclast; }
            set { _cclast = value; }
        }

        #endregion Variables and Properties

        #region Methods

        public static int insertRetailFacility(int facid, DateTime expiredate, bool renew, bool active, int seat, decimal recurAmount, string membertype, string transid, string rfcomment, string cclast, string profileid)
        {
            RetailFacilityInfo record = new RetailFacilityInfo(0, facid, expiredate, renew, active, seat, recurAmount, membertype, transid, rfcomment, cclast, profileid);
            int ret = SiteProvider.PR2.insertRetailFacility(record);
            BizObject.PurgeCacheItems("RetailFacility_RetailFacility_Insert");
            return ret;
        }

        /*
                public int insert()
                {
                    return insertRetailFacility(this.Expiredate, this.Renew, this.Active, this.Seat, this.MemberType, this.TransId, this.Rfcomment, this.CCLast);
                }
        */
        public int insert()
        {
            return insertRetailFacility(this.Facid, this.Expiredate, this.Renew, this.Active, this.Seat, this.RecurAmount, this.MemberType, this.TransId, this.Rfcomment, this.CCLast, this.ProfileID);
        }

        public static bool updateRetailFacility(int rfacid, int facid, DateTime expiredate, bool renew, bool active, int seat, decimal recuramount, string membertype, string transid, string rfcomment, string cclast, string profileid)
        {
            RetailFacilityInfo record = new RetailFacilityInfo(rfacid, facid, expiredate, renew, active, seat, recuramount, membertype, transid, rfcomment, cclast, profileid);
            bool ret = SiteProvider.PR2.updateRetailFacility(record);
            BizObject.PurgeCacheItems("RetailFacility_RetailFacility_Update" + rfacid.ToString());
            return ret;
        }

        public bool update()
        {
            return updateRetailFacility(this.Rfacid, this.Facid, this.Expiredate, this.Renew, this.Active, this.Seat, this.RecurAmount, this.MemberType, this.TransId, this.Rfcomment, this.CCLast, this.ProfileID);
        }

        public bool deleteRetailFacility(int rfacid)
        {
            bool ret = SiteProvider.PR2.deleteRetailFacility(rfacid);
            BizObject.PurgeCacheItems("RetailFacility_RetailFacility_Delete" + rfacid.ToString());
            return ret;
        }

        public static RetailFacility GetRetailFacilityByID(int rfacid)
        {
            RetailFacility retailfacility = null;
            string key = "RetailFacility_GetRetailFacilityByID_" + rfacid.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                retailfacility = (RetailFacility)BizObject.Cache[key];
            }
            else
            {
                retailfacility = GetRetailFacilityFromRetailFacilityInfo(SiteProvider.PR2.GetRetailFacilityByID(rfacid));
                BasePR.CacheData(key, retailfacility);
            }
            return retailfacility;
        }


        public static RetailFacility GetRetailFacilityByFacID(int facid)
        {
            RetailFacility retailfacility = null;
            string key = "RetailFacility_GetRetailFacilityByFacID_" + facid.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                retailfacility = (RetailFacility)BizObject.Cache[key];
            }
            else
            {
                retailfacility = GetRetailFacilityFromRetailFacilityInfo(SiteProvider.PR2.GetRetailFacilityByFacID(facid));
                BasePR.CacheData(key, retailfacility);
            }
            return retailfacility;
        }

        public static List<RetailFacility> DailyCEDPaymentCancelledUpdate()
        {
            List<RetailFacility> rfacilities = null;
            string key = "RetailFacility_DailyCEDPaymentCancelledUpdate";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                rfacilities = (List<RetailFacility>)BizObject.Cache[key];
            }
            else
            {
                List<RetailFacilityInfo> recordset = SiteProvider.PR2.DailyCEDPaymentCancelledUpdate();
                rfacilities = GetRetailFacilityListFromRetailFacilityInfoList(recordset);
                BasePR.CacheData(key, rfacilities);
            }
            return rfacilities;
        }
        public static List<RetailFacility> DailyCEDPaymentUpdate()
        {
            List<RetailFacility> rfacilities = null;
            string key = "RetailFacility_DailyCEDPaymentUpdate";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                rfacilities = (List<RetailFacility>)BizObject.Cache[key];
            }
            else
            {
                List<RetailFacilityInfo> recordset = SiteProvider.PR2.DailyCEDPaymentUpdate();
                rfacilities = GetRetailFacilityListFromRetailFacilityInfoList(recordset);
                BasePR.CacheData(key, rfacilities);
            }
            return rfacilities;
        }

        public static Boolean DailyCEDFacilityAccountUpdate(int facilityid)
        {
            return SiteProvider.PR2.DailyCEDFacilityAccountUpdate(facilityid);
        }



        public static List<RetailFacility> GetRetailFacilityListfromRetailFacilityInfo(List<RetailFacilityInfo> recordset)
        {
            List<RetailFacility> retailfacilityList = new List<RetailFacility>();
            foreach (RetailFacilityInfo retailfacilityinfo in recordset)
            {
                RetailFacility retailfacility = GetRetailFacilityFromRetailFacilityInfo(retailfacilityinfo);

                retailfacilityList.Add(retailfacility);
            }
            return retailfacilityList;
        }


        private static RetailFacility GetRetailFacilityFromRetailFacilityInfo(RetailFacilityInfo retailfacilityinfo)
        {
            if (retailfacilityinfo == null)
                return null;

            RetailFacility retailfacility = new RetailFacility(retailfacilityinfo.Rfacid, retailfacilityinfo.Facid, retailfacilityinfo.Expiredate, retailfacilityinfo.Renew, retailfacilityinfo.Active, retailfacilityinfo.Seat, retailfacilityinfo.RecurAmount, retailfacilityinfo.MemberType, retailfacilityinfo.TransId, retailfacilityinfo.Rfcomment, retailfacilityinfo.CCLast, retailfacilityinfo.ProfileID);
            return retailfacility;
        }

        private static List<RetailFacility> GetRetailFacilityListFromRetailFacilityInfoList(List<RetailFacilityInfo> recordset)
        {
            List<RetailFacility> retailfacilityList = new List<RetailFacility>();
            foreach (RetailFacilityInfo record in recordset)
                retailfacilityList.Add(GetRetailFacilityFromRetailFacilityInfo(record));
            return retailfacilityList;
        }

        #endregion Methods

    }

}
