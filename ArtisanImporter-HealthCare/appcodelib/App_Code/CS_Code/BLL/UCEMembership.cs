﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;


namespace PearlsReview.BLL
{

/// <summary>
/// Summary description for UCEMembership
/// </summary>
 
    public class UCEmembership : BasePR
    {

        public UCEmembership()
        {
        }

        private int _UmID = 0;
        public int UmID
        {
            get { return _UmID; }
            set { _UmID = value; }
        }

        private int _RnID = 0;
        public int RnID
        {
            get { return _RnID; }
            set { _RnID = value; }
        }

        private DateTime _LogDate = System.DateTime.MinValue;
        public DateTime LogDate
        {
            get { return _LogDate; }
            set { _LogDate = value; }
        }

        private DateTime _ExpDate = System.DateTime.MinValue;
        public DateTime ExpDate
        {
            get { return _ExpDate; }
            set { _ExpDate = value; }
        }

        private string _Verification = "";
        public string Verification
        {
            get { return _Verification; }
            set { _Verification = value; }
        }

        private string _ProfileID = "";
        public string ProfileID
        {
            get { return _ProfileID; }
            set { _ProfileID = value; }
        }

        private bool _Renew = false;
        public bool Renew
        {
            get { return _Renew; }
            set { _Renew = value; }
        }

        private bool _Active = false;
        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment.ToLower(); }
            set { _Comment = value.ToLower(); }
        }

        private float _Amount = 0;
        public float Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        private string _CcNum = "";
        public string CcNum
        {
            get { return _CcNum; }
            set { _CcNum = value; }
        }


        private bool _FirstRec;
        public bool FirstRec
        {
            get { return _FirstRec; }
            set { _FirstRec = value; }
        }


        public UCEmembership(int UmID, int RnID, DateTime LogDate, DateTime ExpDate, string Verification, string ProfileID, bool Renew, bool Active, string Comment,
             float Amount, string CcNum)
        {

            this.UmID = UmID;
            this.RnID = RnID;
            this.LogDate = LogDate;
            this.ExpDate = ExpDate;
            this.Verification = Verification;
            this.ProfileID = ProfileID;
            this.Renew = Renew;
            this.Active = Active;
            this.Comment = Comment;
            this.Amount = Amount;
            this.CcNum = CcNum;

        }

        public bool Update()
        {
            return UCEmembership.UpdateUCEmembership(this.UmID, this.RnID, this.LogDate, this.ExpDate, this.Verification, this.ProfileID, this.Renew, this.Active,
                this.Comment, this.Amount, this.CcNum);
        }

        //******Static Methods******//

        /// <summary>
        /// Updates the UCEmembership
        /// </summary>
        /// 

        public static bool UpdateUCEmembership(int UmID, int RnID, DateTime LogDate, DateTime ExpDate, string Verification, string ProfileID, bool Renew, bool Active, string Comment,
             float Amount, string CcNum)
        {
            UCEmembershipInfo record = new UCEmembershipInfo(UmID, RnID, LogDate, ExpDate, Verification, ProfileID, Renew, Active, Comment, Amount, CcNum);
          // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
           SiteProvider.PR2.UpdateUCEmembership(record);
            return false;
        }

        public static bool UpdateUCEmembershipRenewWithRnID(int rnID, bool Renew)
        {
            return SiteProvider.PR2.UpdateUCEmembershipRenewWithRnID(rnID, Renew);            
        }
        public static bool UpdateUCEmembershipActiveWithRnID(int rnID, bool active)
        {
            return SiteProvider.PR2.UpdateUCEmembershipActiveWithRnID(rnID, active);            
        }
        
        public static bool InsertUCEmembership(int RnID, DateTime LogDate, DateTime ExpDate, string Verification, string ProfileID, bool Renew, bool Active, string Comment,
             float Amount, string CcNum)
        {
            UCEmembershipInfo record = new UCEmembershipInfo(RnID, LogDate, ExpDate, Verification, ProfileID, Renew, Active, Comment, Amount, CcNum);
           // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return (SiteProvider.PR2.InsertUCEmembership(record));

        }

        public static List<UCEmembershipInfo> GetUCEmembershipListByRnID(int RnID)
        {
            List<UCEmembershipInfo> ucememList = new List<UCEmembershipInfo>();
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            ucememList = SiteProvider.PR2.GetUCEmembershipListByRnID(RnID);

            return ucememList;
        }
        public static List<UCEmembership> GetUCEmembershipListfromUCEmembershipInfo(List<UCEmembershipInfo> recordset)
        {
            List<UCEmembership> ucemems = new List<UCEmembership>();

            foreach (UCEmembershipInfo ucememinfo in recordset)
            {
                UCEmembership ucemem = GetUCEmembershipFromUCEmembershipInfo(ucememinfo);
                ucemems.Add(ucemem);
            }
            return ucemems;

        }

        private static UCEmembership GetUCEmembershipFromUCEmembershipInfo(UCEmembershipInfo ucemem)
        {
            UCEmembership uce = new UCEmembership();
            uce.UmID = ucemem.UmID;
            uce.RnID = ucemem.RnID;
            uce.LogDate = ucemem.LogDate;
            uce.ExpDate = ucemem.ExpDate;
            uce.Verification = ucemem.Verification;
            uce.ProfileID = ucemem.ProfileID;
            uce.Renew = ucemem.Renew;
            uce.Active = ucemem.Active;
            uce.Comment = ucemem.Comment;
            uce.Amount = ucemem.Amount;
            uce.CcNum = ucemem.CcNum;
            return uce;

        }

        public static UCEmembership GetUCEmembershipByRnID(int RnID)
        {
            UCEmembership ucememship = null;
            string key = " ucememship_UCEmembership" + RnID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ucememship = (UCEmembership)BizObject.Cache[key];
            }
            else
            {//
                PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                UCEmembershipInfo recordset = SiteProvider.PR2.GetUCEmembershipByRnID(RnID);
                ucememship = GetUCEmembershipFromUCEmembershipInfo(recordset);
                BasePR.CacheData(key, ucememship);
            }
            return ucememship;

        }

        public static UCEmembership GetUCEmembershipByRnIDAndVerification(int RnID, string Verification)
        {
            UCEmembership ucememship = null;
            string key = " ucememship_UCEmembership" + RnID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ucememship = (UCEmembership)BizObject.Cache[key];
            }
            else
            {//
                PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                UCEmembershipInfo recordset = SiteProvider.PR2.GetUCEmembershipByRnIDAndVerification(RnID, Verification);
                ucememship = GetUCEmembershipFromUCEmembershipInfo(recordset);
                BasePR.CacheData(key, ucememship);
            }
            return ucememship;

        }

        public static UCEmembership GetUCEmembershipByProfileID(string ProfileID)
        {
            UCEmembership ucemembership = null;
            string key = " ucememship_UCEmembershipByProfileID_" + ProfileID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ucemembership = (UCEmembership)BizObject.Cache[key];
            }
            else
            {//
                PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                UCEmembershipInfo recordset = SiteProvider.PR2.GetUCEmembershipByProfileID(ProfileID);
                if (recordset != null)
                {
                    ucemembership = GetUCEmembershipFromUCEmembershipInfo(recordset);
                    BasePR.CacheData(key, ucemembership);
                }
            }
            return ucemembership;

        }

        public static UCEmembership GetUCEmembershipByUmID(int UmID)
        {
            UCEmembership ucememship = null;
            string key = " ucememship_UCEmembership" + UmID.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                ucememship = (UCEmembership)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                UCEmembershipInfo recordset = SiteProvider.PR2.GetUCEmembershipByUmID(UmID);
                ucememship = GetUCEmembershipFromUCEmembershipInfo(recordset);
                BasePR.CacheData(key, ucememship);
            }
            return ucememship;

        }



        public static bool CancelUCEmembership(int RnID)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.CancelUCEmembership(RnID);

        }
        public static bool CancelUCEmembershipUpdateNurse(int RnID)
        {
           // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.CancelUCEmembershipUpdateNurse(RnID);

        }


        public static bool AddUCEmembership(int RnID, DateTime ExpDate)
        {
           // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.AddUCEmembership(RnID, ExpDate);

        }


        public static bool HaltUCEMemRenewals(int RnID)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.HaltUCEMemRenewals(RnID);

        }
        public static bool HaltUCEMemRenewalsUpdateNurse(int RnID)
        {
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            return SiteProvider.PR2.HaltUCEMemRenewalsUpdateNurse(RnID);

        }

        public static List<UCEmembershipInfo> GetActiveUCEMembershipsByExpDateInThisMonth()
        {
            List<UCEmembershipInfo> ucememList = new List<UCEmembershipInfo>();
            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            ucememList = SiteProvider.PR2.GetActiveUCEMembershipsByExpDateInThisMonth();

            return ucememList;
        }

        public static bool UpdateUCEMemberShipReminderDate(DateTime reminderDate, int umID)
        {
            return SiteProvider.PR2.UpdateUCEMemberShipReminderDate(reminderDate, umID);
        }

        public static DataSet GetUCECCExpiredUsers()
        {
            return SiteProvider.PR2.GetUCECCExpiredUsers();
        }

      

    }
}
