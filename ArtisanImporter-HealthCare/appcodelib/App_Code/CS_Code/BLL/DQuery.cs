﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr1
    /// </summary>
    /// 


    public class DQuery : BasePR
    {

        public DQuery()
        { }

        public DQuery(int QueryID, string QueryName, string QueryType, string QueryDescription, string QueryContent,
            string FirstPrompt, string SecondPrompt, string ThirdPrompt)
        {
            this.QueryID = QueryID;
            this.QueryName = QueryName;
            this.QueryType = QueryType;
            this.QueryDescription = QueryDescription;
            this.QueryContent = QueryContent;
            this.FirstPrompt = FirstPrompt;
            this.SecondPrompt = SecondPrompt;
            this.ThirdPrompt = ThirdPrompt;
        }

        private int _QueryID = 0;
        public int QueryID
        {
            get { return _QueryID; }
            set { _QueryID = value; }
        }

        private string _QueryName = "";
        public string QueryName
        {
            get { return _QueryName; }
            set { _QueryName = value; }
        }

        private string _QueryType = "";
        public string QueryType
        {
            get { return _QueryType; }
            set { _QueryType = value; }
        }
        private string _QueryDescription = "";
        public string QueryDescription
        {
            get { return _QueryDescription; }
            set { _QueryDescription = value; }
        }

        private string _QueryContent = "";
        public string QueryContent
        {
            get { return _QueryContent; }
            set { _QueryContent = value; }
        }

        private string _FirstPrompt = "";
        public string FirstPrompt
        {
            get { return _FirstPrompt; }
            set { _FirstPrompt = value; }
        }

        private string _SecondPrompt = "";
        public string SecondPrompt
        {
            get { return _SecondPrompt; }
            set { _SecondPrompt = value; }
        }

        private string _ThirdPrompt = "";
        public string ThirdPrompt
        {
            get { return _ThirdPrompt; }
            set { _ThirdPrompt = value; }
        }

        private static List<DQuery> GetDQueryListFromDQueryInfoList(List<DQueryInfo> recordset)
        {
            List<DQuery> dqry = new List<DQuery>();
            foreach (DQueryInfo record in recordset)
                dqry.Add(GetDQueryFromDQueryInfo(record));
            return dqry;
        }

        private static DQuery GetDQueryFromDQueryInfo(DQueryInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new DQuery(record.QueryID, record.QueryName, record.QueryType, record.QueryDescription, record.QueryContent,
                    record.FirstPrompt, record.SecondPrompt, record.ThirdPrompt);
            }
        }

        public static List<DQuery> GetAllDQueryByQueryId(int QueryID)
        {
            List<DQuery> dqrys = null;
            string key = "DQuery_DQuery_" + QueryID;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                dqrys = (List<DQuery>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<DQueryInfo> recordset = SiteProvider.PR2.GetAllDQueryByQueryId(QueryID);
                dqrys = GetDQueryListFromDQueryInfoList(recordset);
                BasePR.CacheData(key, dqrys);
            }
            return dqrys;
        }
        public static List<DQuery> GetAllDQuerys()
        {
            List<DQuery> dqrys = null;
            string key = "DQuery_DQuery_";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                dqrys = (List<DQuery>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<DQueryInfo> recordset = SiteProvider.PR2.GetAllDQuerys();
                dqrys = GetDQueryListFromDQueryInfoList(recordset);
                BasePR.CacheData(key, dqrys);
            }
            return dqrys;
        }

        public static DataSet FetchDQueryResults(DQuery qry, string input1, string input2, string input3)
        {
            DataSet ds = null;

            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            ds = SiteProvider.PR2.FetchDQueryResults(qry.QueryContent, qry.QueryType, qry.FirstPrompt, qry.SecondPrompt, qry.ThirdPrompt, input1, input2, input3);
            return ds;
        }

    }

}