﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using PearlsReview.QTI;

namespace PearlsReview.BLL
{

/// <summary>
/// Summary description for SurveyQuestion
/// </summary>
    public class SurveyQuestion : BasePR
    {
        #region Variables and Properties
        
        private int _SurveyID = 0;
        public int SurveyID
        {
            get { return _SurveyID; }
            protected set { _SurveyID = value; }
        }

        private int _QuestionNum = 0;
        public int QuestionNum
        {
            get { return _QuestionNum; }
            set { _QuestionNum = value; }
        }

        private string _Question = "";
        public string Question
        {
            get { return _Question; }
            set { _Question = value; }
        }

        private string _Answer_A = "";
        public string Answer_A
        {
            get { return _Answer_A; }
            set { _Answer_A = value; }
        }

        private string _Answer_B = "";
        public string Answer_B
        {
            get { return _Answer_B; }
            set { _Answer_B = value; }
        }

        private string _Answer_C = "";
        public string Answer_C
        {
            get { return _Answer_C; }
            set { _Answer_C = value; }
        }

        private string _Answer_D = "";
        public string Answer_D
        {
            get { return _Answer_D; }
            set { _Answer_D = value; }
        }

        private string _Answer_E = "";
        public string Answer_E
        {
            get { return _Answer_E; }
            set { _Answer_E = value; }
        }

        private string _Answer_F = "";
        public string Answer_F
        {
            get { return _Answer_F; }
            set { _Answer_F = value; }
        }

        private string _RecordOrder = "";
        public string RecordOrder
        {
            get { return _RecordOrder; }
            set { _RecordOrder = value; }
        }

        private string _Control_Type = "";
        public string Control_Type
        {
            get { return _Control_Type; }
            set { _Control_Type = value; }
        }

        public SurveyQuestion(int SurveyID, int QuestionNum, string Question, string Answer_A, string Answer_B, string Answer_C,
            string Answer_D, string Answer_E, string Answer_F, string RecordOrder, string Control_Type)
        {
            this.SurveyID = SurveyID;
            this.QuestionNum = QuestionNum;
            this.Question = Question;
            this.Answer_A = Answer_A;
            this.Answer_B = Answer_B;
            this.Answer_C = Answer_C;
            this.Answer_D = Answer_D;
            this.Answer_E = Answer_E;
            this.Answer_F = Answer_F;
            this.RecordOrder = RecordOrder;
            this.Control_Type = Control_Type;
        }
        
        #endregion

        #region Methods
        #region Static methods (SurveyQuestion)

        /// <summary>
        /// Returns a collection with all Survey Questions
        /// </summary>
        public static List<SurveyQuestion> GetSurveyQuestionsByID(int SurveyId)
        {
            List<SurveyQuestion> SurveyQuestions = new List<SurveyQuestion>();
            SQTIUtils oSQTIUtils = new SQTIUtils();
            SurveyDefinition SurveyDef = SurveyDefinition.GetSurveyDefinitionByID(SurveyId);
            List<SQTIQuestionObject> SQTIQuestions = new List<SQTIQuestionObject>();
            if (SurveyDef == null || SurveyDef.XMLSurvey.ToString() == "")
            {
                SQTIQuestionObject SQTIQuestion;
                SQTIQuestion = new SQTIQuestionObject();
            }
            else
            {
                SQTIQuestions = oSQTIUtils.ConvertQTITestXMLToSQTIQuestionObjectList(SurveyDef.XMLSurvey.ToString());
            }
            if (SQTIQuestions.Count > 0)
            {
                for (int i = 1; i <= SQTIQuestions.Count; i++)
                {
                    SurveyQuestions.Add(CreateNewMultipleChoiceQuestion(SurveyId, i));
                }
                int iOrder = 0;
                foreach (SQTIQuestionObject SQTIQuestion in SQTIQuestions)
                {
                    SurveyQuestions[iOrder].Question = SQTIQuestion.cQuestionText;
                    SurveyQuestions[iOrder].Answer_A = SQTIQuestion.cAnswer_A_Text;
                    SurveyQuestions[iOrder].Answer_B = SQTIQuestion.cAnswer_B_Text;
                    SurveyQuestions[iOrder].Answer_C = SQTIQuestion.cAnswer_C_Text;
                    SurveyQuestions[iOrder].Answer_D = SQTIQuestion.cAnswer_D_Text;
                    SurveyQuestions[iOrder].Answer_E = SQTIQuestion.cAnswer_E_Text;
                    SurveyQuestions[iOrder].Answer_F = SQTIQuestion.cAnswer_F_Text;
                    SurveyQuestions[iOrder].RecordOrder = SQTIQuestion.cRecordOrder;
                    SurveyQuestions[iOrder].Control_Type = SQTIQuestion.cControl_Type;
                    iOrder++;
                }
            }
            return SurveyQuestions;
        }

        /// <summary>
        /// Returns a new multiple choice SurveyQuestion object
        /// </summary>
        private static SurveyQuestion CreateNewMultipleChoiceQuestion(int SurveyID, int QuestionNum)
        {
            SurveyQuestion objSurveyQuestion = new SurveyQuestion(SurveyID, QuestionNum, "", "",
                "", "", "", "", "", "", "");

            return objSurveyQuestion;
        }

        public static bool UpdateSurveyQuestion(int SurveyID, int QuestionNum, string Question, string Answer_A, string Answer_B,
            string Answer_C, string Answer_D, string Answer_E, string RecordOrder, string Control_Type)
        {
            List<SurveyQuestion> SurveyQuestions = GetSurveyQuestionsByID(SurveyID);
            int iQuestionPointer;
            if (QuestionNum > SurveyQuestions.Count)
            {
                SurveyQuestion objSurveyQuestion = new SurveyQuestion(SurveyID, QuestionNum, Question, Answer_A,
                        Answer_B, Answer_C, Answer_D, Answer_E, "", RecordOrder, Control_Type);

                SurveyQuestions.Add(objSurveyQuestion);
            }
            else
            {
                iQuestionPointer = QuestionNum - 1;

                SurveyQuestions[iQuestionPointer].Question = Question;
                SurveyQuestions[iQuestionPointer].Answer_A = Answer_A;
                SurveyQuestions[iQuestionPointer].Answer_B = Answer_B;
                SurveyQuestions[iQuestionPointer].Answer_C = Answer_C;
                SurveyQuestions[iQuestionPointer].Answer_D = Answer_D;
                SurveyQuestions[iQuestionPointer].Answer_E = Answer_E;
                SurveyQuestions[iQuestionPointer].Answer_F = "";
                SurveyQuestions[iQuestionPointer].RecordOrder = RecordOrder;
                SurveyQuestions[iQuestionPointer].Control_Type = Control_Type;
            }

            // insert the entire SurveyQuestions list (it first deletes
            // the existing Surveydetail records)
            bool ret = InsertSurveyQuestionsFromList(SurveyQuestions);
            return ret;

        }


        public static bool InsertSurveyQuestion(int SurveyID, int QuestionNum, string Question, string Answer_A, string Answer_B, string Answer_C,
            string Answer_D, string Answer_E, string RecordOrder, string Control_Type)
        {
            List<SurveyQuestion> SurveyQuestions = GetSurveyQuestionsByID(SurveyID);
            SurveyQuestion objSurveyQuestion = new SurveyQuestion(SurveyID, QuestionNum, Question, Answer_A,
                Answer_B, Answer_C, Answer_D, Answer_E, "", RecordOrder, Control_Type);

            SurveyQuestions.Add(objSurveyQuestion);

            // insert the entire SurveyQuestions list (it first deletes
            // the existing Surveydetail records)
            bool ret = InsertSurveyQuestionsFromList(SurveyQuestions);
            return ret;
        }

        public static bool DeleteSurveyQuestion(int SurveyID, int QuestionNum)
        {
            List<SurveyQuestion> SurveyQuestions = GetSurveyQuestionsByID(SurveyID);
            SurveyQuestions.RemoveAt(QuestionNum - 1);
            int i;
            for (i = QuestionNum; i < SurveyQuestions.Count; i++)
            {
                SurveyQuestions[i].QuestionNum = i - 1;
            }
            bool ret = true;

            if (SurveyQuestions.Count > 0)
            {
                ret = InsertSurveyQuestionsFromList(SurveyQuestions);
            }
            return ret;
        }

        public static bool InsertSurveyQuestionsFromList(List<SurveyQuestion> SurveyQuestions)
        {
            List<SQTIQuestionObject> QTIQuestions = new List<SQTIQuestionObject>();
            int iSurveyID = 0;
            if (SurveyQuestions.Count > 0)
            {
                int iOrder = 0;
                foreach (SurveyQuestion record in SurveyQuestions)
                {
                    iOrder++;
                    if (iOrder == 1)
                        iSurveyID = record.SurveyID;
                    SQTIQuestionObject QTIQuestion = new SQTIQuestionObject();
                    QTIQuestion.cQuestionID = "Survey_" + iSurveyID.ToString() + "_Question_" + iOrder.ToString();
                    QTIQuestion.cQuestionTitle = "PearlsReview.com Question # " + iOrder.ToString() +
                        " for Survey # " + iSurveyID.ToString();
                    QTIQuestion.cQuestionText = record.Question;
                    QTIQuestion.cAnswer_A_Text = record.Answer_A;
                    QTIQuestion.cAnswer_B_Text = record.Answer_B;
                    QTIQuestion.cAnswer_C_Text = record.Answer_C;
                    QTIQuestion.cAnswer_D_Text = record.Answer_D;
                    QTIQuestion.cAnswer_E_Text = record.Answer_E;
                    QTIQuestion.cAnswer_F_Text = record.Answer_F;
                    QTIQuestion.cRecordOrder = record.RecordOrder;
                    QTIQuestion.cUserAnswer = "";
                    QTIQuestion.cControl_Type = record.Control_Type;
                    QTIQuestions.Add(QTIQuestion);
                }
            }

            string cXML = "";
            bool ret = false;

            if (QTIQuestions.Count > 0 && iSurveyID > 0)
            {
                SQTIUtils oSQTIUtils = new SQTIUtils();
                cXML = oSQTIUtils.ConvertSQTIQuestionObjectListToQTITestXMLString(QTIQuestions);
                SurveyDefinition oSurveyDefinition = SurveyDefinition.GetSurveyDefinitionByID(iSurveyID);
                if (oSurveyDefinition == null)
                {
                    // inserting new record
                    int newID = SurveyDefinition.InsertSurveyDefinition("", cXML, 1, 0);
                    if (newID > 0)
                        ret = true;
                }
                else
                {
                    // updating existing record
                    //ret = SurveyDefinition.UpdateSurveyDefinition(oSurveyDefinition.SurveyID,cXML, oSurveyDefinition.Version + 1,0);
                    ret = SurveyDefinition.UpdateSurveyDefinition(oSurveyDefinition.SurveyID, cXML, 1, 0);
                }
            }
            return ret;
        }

        /// <summary>
        /// Returns a list of test question objects for the specified TestID        
        /// </summary>
        public static List<SQTIQuestionObject> GetSurveyQuestionObjectListByTestID(int TestID)
        {
            SurveyDefinition oSurveyDefinition = null;
            SQTIUtils oSQTIUtils = new SQTIUtils();
            List<SQTIQuestionObject> SQTIQuestionObjects = null;
            // first, get the test record so we have access to topicid and user's responses
            Test oTest = Test.GetTestByID(TestID);
            if (oTest != null)
            {
                // get the test definition record for this topic
                Topic oTopic = Topic.GetTopicByID(oTest.TopicID);
                if (oTopic != null)
                {
                    oSurveyDefinition = SurveyDefinition.GetSurveyDefinitionByID(oTopic.SurveyID);
                }
                if (oSurveyDefinition != null)
                {
                    // get the QTIQuestionObjectList from the TestDefinition XML
                    SQTIQuestionObjects = oSQTIUtils.ConvertQTITestXMLToSQTIQuestionObjectList(oSurveyDefinition.XMLSurvey);
                }
            }
            return SQTIQuestionObjects;
        }

        /// <summary>
        /// Returns a list of test question objects for the specified TestID        
        /// </summary>
        public static List<SQTIQuestionObject> Get3MonthFollwUpSurveyByTestID(int TestID)
        {
            SurveyDefinition oSurveyDefinition = null;
            SQTIUtils oSQTIUtils = new SQTIUtils();
            List<SQTIQuestionObject> SQTIQuestionObjects = null;
            // first, get the test record so we have access to topicid and user's responses
            Test oTest = Test.GetTestByID(TestID);
            if (oTest != null)
            {
                // get the test definition record for this topic
                Topic oTopic = Topic.GetTopicByID(oTest.TopicID);
                if (oTopic != null)
                {
                    string surveyType = "3-Month Follow-up Survey";
                    TopicSurveyLink topicSurveyLink = TopicSurveyLink.GetTopicSurveyLinkByTopicIDAndSurveyType(oTopic.TopicID, surveyType);
                    if (topicSurveyLink == null)
                        oSurveyDefinition = SurveyDefinition.GetSurveyDefinitionBySurveyName("3-Month Follow-up Survey");
                    else
                        oSurveyDefinition = SurveyDefinition.GetSurveyDefinitionByID(topicSurveyLink.SurveyID);
                }
                if (oSurveyDefinition != null)
                {
                    // get the QTIQuestionObjectList from the TestDefinition XML
                    SQTIQuestionObjects = oSQTIUtils.ConvertQTITestXMLToSQTIQuestionObjectList(oSurveyDefinition.XMLSurvey);
                }
            }
            return SQTIQuestionObjects;
        }


        /// <summary>
        /// Returns a list of test question objects for the specified TestID        
        /// </summary>
        public static List<SQTIQuestionObject> GetSurveyQuestionObjectListByTopicID(int TopicID)
        {
            SurveyDefinition oSurveyDefinition = null;
            SQTIUtils oSQTIUtils = new SQTIUtils();
            List<SQTIQuestionObject> SQTIQuestionObjects = null;
            // first, get the test record so we have access to topicid and user's responses
            //Test oTest = Test.GetTestByID(TestID);
            //if (oTest != null)
            //{
            // get the test definition record for this topic
            Topic oTopic = Topic.GetTopicByID(TopicID);
            if (oTopic != null)
            {
                oSurveyDefinition = SurveyDefinition.GetSurveyDefinitionByID(oTopic.SurveyID);
            }
            if (oSurveyDefinition != null)
            {
                // get the QTIQuestionObjectList from the TestDefinition XML
                SQTIQuestionObjects = oSQTIUtils.ConvertQTITestXMLToSQTIQuestionObjectList(oSurveyDefinition.XMLSurvey);
            }
            //}
            return SQTIQuestionObjects;
        }
        #endregion

        #endregion
    }
}