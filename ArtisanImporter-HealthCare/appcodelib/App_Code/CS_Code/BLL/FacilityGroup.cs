﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;


namespace PearlsReview.BLL
{

/// <summary>
/// Summary description for FacilityGroup
/// </summary>
    public class FacilityGroup : BasePR
    {
        #region Variables and Properties

        private int _FacID = 0;
        public int FacID
        {
            get { return _FacID; }
            protected set { _FacID = value; }
        }

        private string _FacCode = "";
        public string FacCode
        {
            get { return _FacCode; }
            set { _FacCode = value; }
        }

        private string _FacName = "";
        public string FacName
        {
            get { return _FacName; }
            set { _FacName = value; }
        }

        private int _MaxUsers = 1000;
        public int MaxUsers
        {
            get { return _MaxUsers; }
            set { _MaxUsers = value; }
        }

        private DateTime _Started = System.DateTime.MinValue;
        public DateTime Started
        {
            get { return _Started; }
            set { _Started = value; }
        }

        private DateTime _Expires = System.DateTime.MaxValue;
        public DateTime Expires
        {
            get { return _Expires; }
            set { _Expires = value; }
        }

        private int _RepID = 0;
        public int RepID
        {
            get { return _RepID; }
            set { _RepID = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        private bool _Compliance = false;
        public bool Compliance
        {
            get { return _Compliance; }
            set { _Compliance = value; }
        }

        private bool _CustomTopics = false;
        public bool CustomTopics
        {
            get { return _CustomTopics; }
            set { _CustomTopics = value; }
        }

        private bool _QuizBowl = false;
        public bool QuizBowl
        {
            get { return _QuizBowl; }
            set { _QuizBowl = value; }
        }

        private string _homeurl = "";
        public string homeurl
        {
            get { return _homeurl; }
            set { _homeurl = value; }
        }

        private string _tagline = "";
        public string tagline
        {
            get { return _tagline; }
            set { _tagline = value; }
        }

        private string _address1 = "";
        public string address1
        {
            get { return _address1; }
            set { _address1 = value; }
        }

        private string _address2 = "";
        public string address2
        {
            get { return _address2; }
            set { _address2 = value; }
        }

        private string _city = "";
        public string city
        {
            get { return _city; }
            set { _city = value; }
        }

        private string _state = "";
        public string state
        {
            get { return _state; }
            set { _state = value; }
        }

        private string _zipcode = "";
        public string zipcode
        {
            get { return _zipcode; }
            set { _zipcode = value; }
        }

        private string _phone = "";
        public string phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        private string _contact_name = "";
        public string contact_name
        {
            get { return _contact_name; }
            set { _contact_name = value; }
        }

        private string _contact_phone = "";
        public string contact_phone
        {
            get { return _contact_phone; }
            set { _contact_phone = value; }
        }

        private string _contact_ext = "";
        public string contact_ext
        {
            get { return _contact_ext; }
            set { _contact_ext = value; }
        }

        private string _contact_email = "";
        public string contact_email
        {
            get { return _contact_email; }
            set { _contact_email = value; }
        }

        private int _parent_id = 0;
        public int parent_id
        {
            get { return _parent_id; }
            set { _parent_id = value; }
        }

        private string _welcome_pg = "";
        public string welcome_pg
        {
            get { return _welcome_pg; }
            set { _welcome_pg = value; }
        }

        private string _login_help = "";
        public string login_help
        {
            get { return _login_help; }
            set { _login_help = value; }
        }

        private string _logo_img = "";
        public string logo_img
        {
            get { return _logo_img; }
            set { _logo_img = value; }
        }

        private bool _active = true;
        public bool active
        {
            get { return _active; }
            set { _active = value; }
        }

        private string _support_email = "";
        public string support_email
        {
            get { return _support_email; }
            set { _support_email = value; }
        }

        private string _feedback_email = "";
        public string feedback_email
        {
            get { return _feedback_email; }
            set { _feedback_email = value; }
        }

        private string _default_password = "";
        public string default_password
        {
            get { return _default_password; }
            set { _default_password = value; }
        }

        private bool _forceinfocheck = false;
        public bool forceinfocheck
        {
            get { return _forceinfocheck; }
            set { _forceinfocheck = value; }
        }

        private int _salemanager_id = 0;
        public int salemanager_id
        {
            get { return _salemanager_id; }
            set { _salemanager_id = value; }
        }

        private int _accountmanager_id = 0;
        public int accountmanager_id
        {
            get { return _accountmanager_id; }
            set { _accountmanager_id = value; }
        }

        private bool _dataupload_ind = false;
        public bool dataupload_ind
        {
            get { return _dataupload_ind; }
            set {  _dataupload_ind = value; }
        }

        private bool _datadownload_ind = false;
        public bool datadownload_ind
        {
            get { return _datadownload_ind; }
            set { _datadownload_ind = value; }
        }

        public FacilityGroup(int FacID, string FacCode, string FacName, int MaxUsers, DateTime Started,
            DateTime Expires, int RepID, string Comment, bool Compliance, bool CustomTopics, bool QuizBowl,
           string homeurl, string tagline, string address1, string address2, string city, string state,
           string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
           string contact_email, int parent_id, string welcome_pg, string login_help, string logo_img,
           bool active, string support_email, string feedback_email, string default_password, bool forceinfocheck,
           int salemanager_id, int accountmanager_id, bool dataupload_ind, bool datadownload_ind)
        {
            this.FacID = FacID;
            this.FacCode = FacCode;
            this.FacName = FacName;
            this.MaxUsers = MaxUsers;
            this.Started = Started;
            this.Expires = Expires;
            this.RepID = RepID;
            this.Comment = Comment;
            this.Compliance = Compliance;
            this.CustomTopics = CustomTopics;
            this.QuizBowl = QuizBowl;
            this.homeurl = homeurl;
            this.tagline = tagline;
            this.address1 = address1;
            this.address2 = address2;
            this.city = city;
            this.state = state;
            this.zipcode = zipcode;
            this.phone = phone;
            this.contact_name = contact_name;
            this.contact_phone = contact_phone;
            this.contact_ext = contact_ext;
            this.contact_email = contact_email;
            this.parent_id = parent_id;
            this.welcome_pg = welcome_pg;
            this.login_help = login_help;
            this.logo_img = logo_img;
            this.active = active;
            this.support_email = support_email;
            this.feedback_email = feedback_email;
            this.default_password = default_password;
            this.forceinfocheck = forceinfocheck;
            this.salemanager_id = salemanager_id;
            this.accountmanager_id = accountmanager_id;
            this.datadownload_ind = datadownload_ind;
            this.dataupload_ind = dataupload_ind;
        }

        public bool Delete()
        {
            bool success = FacilityGroup.DeleteFacilityGroup(this.FacID);
            if (success)
                this.FacID = 0;
            return success;
        }

        public bool Update()
        {
            if (this.Started != DateTime.MinValue && this.Expires != DateTime.MinValue)
            {
                return FacilityGroup.UpdateFacilityGroup(this.FacID, this.FacCode, this.FacName, this.MaxUsers,
                    this.Started, this.Expires, this.RepID, this.Comment, this.Compliance, this.CustomTopics,
                    this.QuizBowl,
               this.homeurl, this.tagline, this.address1, this.address2, this.city, this.state,
               this.zipcode, this.phone, this.contact_name, this.contact_phone, this.contact_ext,
               this.contact_email, this.parent_id, this.welcome_pg, this.login_help, this.logo_img,
               this.active, this.support_email, this.feedback_email, this.default_password, this.forceinfocheck,
               this.salemanager_id, this.accountmanager_id, this.dataupload_ind, this.datadownload_ind);
            }
            else
            {
                return false;
            }
        }

        #endregion

        public const int BANNERPARENTID = 26;   //the banner parent id

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all FacilityGroups
        /// </summary>
        public static List<FacilityGroup> GetFacilityGroups(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "FacName";

            List<FacilityGroup> FacilityGroups = null;
            string key = "FacilityGroups_FacilityGroups_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroups = (List<FacilityGroup>)BizObject.Cache[key];
            }
            else
            {
                List<FacilityGroupInfo> recordset = SiteProvider.PR2.GetFacilityGroups(cSortExpression);
                FacilityGroups = GetFacilityGroupListFromFacilityGroupInfoList(recordset);
                BasePR.CacheData(key, FacilityGroups);
            }
            return FacilityGroups;
        }


        /// <summary>
        /// Returns a collection with all FacilityGroups except giftcard users
        /// </summary>
        public static List<FacilityGroup> GetFacilityGroupsExceptGiftCard(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "FacName";

            List<FacilityGroup> FacilityGroups = null;
            string key = "FacilityGroupsExceptGiftCard_FacilityGroupsExceptGiftCard_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroups = (List<FacilityGroup>)BizObject.Cache[key];
            }
            else
            {
                List<FacilityGroupInfo> recordset = SiteProvider.PR2.GetFacilityGroupsExceptGiftCard(cSortExpression);
                FacilityGroups = GetFacilityGroupListFromFacilityGroupInfoList(recordset);
                BasePR.CacheData(key, FacilityGroups);
            }
            return FacilityGroups;
        }

        /// <summary>
        /// Returns a collection with all FacilityGroups related to the parent of a FacilityID
        /// </summary>
        public static List<FacilityGroup> GetFacilityGroupsByParentOfFacilityID(int FacilityID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "FacCode";

            List<FacilityGroup> FacilityGroups = null;
            string key = "FacilityGroups_FacilityGroupsByParentOfFacilityID_" + FacilityID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroups = (List<FacilityGroup>)BizObject.Cache[key];
            }
            else
            {
                List<FacilityGroupInfo> recordset = SiteProvider.PR2.GetFacilityGroupsByParentOfFacilityID(FacilityID, cSortExpression);
                FacilityGroups = GetFacilityGroupListFromFacilityGroupInfoList(recordset);
                BasePR.CacheData(key, FacilityGroups);
            }
            return FacilityGroups;
        }

        /// <summary>
        /// Returns a collection with all FacilityGroups related to the parent of a FacilityID
        /// </summary>
        public static List<FacilityGroup> GetFacilityGroupsByParentID(int ParentID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "FacName";

            List<FacilityGroup> FacilityGroups = null;
            string key = "FacilityGroups_FacilityGroupsByParentID_" + ParentID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroups = (List<FacilityGroup>)BizObject.Cache[key];
            }
            else
            {
                List<FacilityGroupInfo> recordset = SiteProvider.PR2.GetFacilityGroupsByParentID(ParentID, cSortExpression);
                FacilityGroups = GetFacilityGroupListFromFacilityGroupInfoList(recordset);
                BasePR.CacheData(key, FacilityGroups);
            }
            return FacilityGroups;
        }


        /// <summary>
        /// Returns the number of total FacilityGroups
        /// </summary>
        public static int GetFacilityGroupCount()
        {
            int FacilityGroupCount = 0;
            string key = "FacilityGroups_FacilityGroupCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroupCount = (int)BizObject.Cache[key];
            }
            else
            {
                FacilityGroupCount = SiteProvider.PR2.GetFacilityGroupCount();
                BasePR.CacheData(key, FacilityGroupCount);
            }
            return FacilityGroupCount;
        }
        public static DataTable GetFacilitiesByOrgID(int OrgID)
        {
            return SiteProvider.PR2.GetFacilitiesByOrgID(OrgID);
        }
        /// <summary>
        /// Returns a FacilityGroup object with the specified ID
        /// </summary>
        public static FacilityGroup GetFacilityGroupByID(int FacID)
        {
            FacilityGroup FacilityGroup = null;
            string key = "FacilityGroups_FacilityGroup_" + FacID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroup = (FacilityGroup)BizObject.Cache[key];
            }
            else
            {
                FacilityGroup = GetFacilityGroupFromFacilityGroupInfo(SiteProvider.PR2.GetFacilityGroupByID(FacID));
                BasePR.CacheData(key, FacilityGroup);
            }
            return FacilityGroup;
        }

        /// <summary>
        /// Returns a FacilityGroup object with the specified IP
        /// </summary>
        public static FacilityGroup GetFacilityGroupByIP(string ip)
        {
            FacilityGroup FacilityGroup = null;
            string key = "FacilityGroups_FacilityGroup_" + ip;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroup = (FacilityGroup)BizObject.Cache[key];
            }
            else
            {
                FacilityGroup = GetFacilityGroupFromFacilityGroupInfo(SiteProvider.PR2.GetFacilityGroupByIP(ip));
                BasePR.CacheData(key, FacilityGroup);
            }
            return FacilityGroup;
        }

        /// <summary>
        /// Returns a FacilityGroup object with the specified Domain
        /// </summary>
        public static FacilityGroup GetFacilityGroupByDomain(string domain)
        {
            FacilityGroup FacilityGroup = null;
            string key = "FacilityGroups_FacilityGroup_" + domain;

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroup = (FacilityGroup)BizObject.Cache[key];
            }
            else
            {
                FacilityGroup = GetFacilityGroupFromFacilityGroupInfo(SiteProvider.PR2.GetFacilityGroupByDomain(domain));
                BasePR.CacheData(key, FacilityGroup);
            }
            return FacilityGroup;
        }

        /// <summary>
        /// Returns a FacilityGroup object with the specified Facility code
        /// </summary>
        public static FacilityGroup GetFacilityGroupByFacilityCode(string FacCode)
        {
            FacilityGroup FacilityGroup = null;
            string key = "FacilityGroups_FacilityGroup_FacCode_" + FacCode.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroup = (FacilityGroup)BizObject.Cache[key];
            }
            else
            {
                FacilityGroup = GetFacilityGroupFromFacilityGroupInfo(SiteProvider.PR2.GetFacilityGroupByFacilityCode(FacCode));
                BasePR.CacheData(key, FacilityGroup);
            }
            return FacilityGroup;
        }

        /// <summary>
        /// Returns a FacilityGroup object with the specified Facility Name
        /// </summary>

        public static FacilityGroup GetFacilityGroupByFacName(string FacName)
        {
            FacilityGroup FacilityGroup = null;
            string key = "FacilityGroups_FacilityGroup_FacName_" + FacName.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroup = (FacilityGroup)BizObject.Cache[key];
            }
            else
            {
                FacilityGroup = GetFacilityGroupFromFacilityGroupInfo(SiteProvider.PR2.GetFacilityGroupByFacName(FacName));
                BasePR.CacheData(key, FacilityGroup);
            }
            return FacilityGroup;
        }

        public static DataSet GetExpireRetailFacility()
        {
            return SiteProvider.PR2.GetExpireRetailFacility();
        }
        /// <summary>
        /// Retrieves the sub facilities of parentorganization by parentorgadmin
        /// </summary>

        public static List<FacilityGroup> GetFacilityGroupByOrgAdmin(int ParentOrgAdminID)
        {
            List<FacilityGroup> FacilityGroups = null;
            string key = "FacilityGroups_FacilityGroup_ParentOrgAdminID_" + ParentOrgAdminID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroups = (List<FacilityGroup>)BizObject.Cache[key];
            }
            else
            {
                List<FacilityGroupInfo> recordset = SiteProvider.PR2.GetFacilityGroupByOrgAdmin(ParentOrgAdminID);
                FacilityGroups = GetFacilityGroupListFromFacilityGroupInfoList(recordset);                
                BasePR.CacheData(key, FacilityGroups);
            }
            return FacilityGroups;
        }


        /// <summary>
        /// Updates an existing FacilityGroup
        /// </summary>
        public static bool UpdateFacilityGroup(int FacID, string FacCode, string FacName, int MaxUsers, DateTime Started,
            DateTime Expires, int RepID, string Comment, bool Compliance, bool CustomTopics, bool QuizBowl,
           string homeurl, string tagline, string address1, string address2, string city, string state,
           string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
           string contact_email, int parent_id, string welcome_pg, string login_help, string logo_img,
           bool active, string support_email, string feedback_email, string default_password, bool forceinfocheck, 
           int salemanager_id, int accountmanager_id, bool dataupload_ind, bool datadownload_ind)
        {
            FacCode = BizObject.ConvertNullToEmptyString(FacCode);
            FacName = BizObject.ConvertNullToEmptyString(FacName);
            Comment = BizObject.ConvertNullToEmptyString(Comment);
            bool ret = false;

            FacilityGroupInfo record = new FacilityGroupInfo(FacID, FacCode, FacName, MaxUsers, Started,
                Expires, RepID, Comment, Compliance, CustomTopics, QuizBowl,
               homeurl, tagline, address1, address2, city, state,
               zipcode, phone, contact_name, contact_phone, contact_ext,
               contact_email, parent_id, welcome_pg, login_help, logo_img,
               active, support_email, feedback_email, default_password, forceinfocheck, salemanager_id, accountmanager_id,
               dataupload_ind,datadownload_ind);
            ret = SiteProvider.PR2.UpdateFacilityGroup(record);

            BizObject.PurgeCacheItems("FacilityGroups_FacilityGroup_" + FacID.ToString());
            BizObject.PurgeCacheItems("FacilityGroups_FacilityGroups");
            return ret;
        }

        /// <summary>
        /// Creates a new FacilityGroup
        /// </summary>
        public static int InsertFacilityGroup(string FacCode, string FacName, int MaxUsers, DateTime Started,
            DateTime Expires, int RepID, string Comment, bool Compliance, bool CustomTopics, bool QuizBowl,
           string homeurl, string tagline, string address1, string address2, string city, string state,
           string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
           string contact_email, int parent_id, string welcome_pg, string login_help, string logo_img,
           bool active, string support_email, string feedback_email, string default_password, 
           int salemanager_id, int accountmanager_id, bool forceinfocheck, bool dataupload_ind, bool datadownload_ind)
        {
            FacCode = BizObject.ConvertNullToEmptyString(FacCode);
            FacName = BizObject.ConvertNullToEmptyString(FacName);
            Comment = BizObject.ConvertNullToEmptyString(Comment);

            FacilityGroupInfo record = new FacilityGroupInfo(0, FacCode, FacName, MaxUsers, Started,
                Expires, RepID, Comment, Compliance, CustomTopics, QuizBowl,
               homeurl, tagline, address1, address2, city, state,
               zipcode, phone, contact_name, contact_phone, contact_ext,
               contact_email, parent_id, welcome_pg, login_help, logo_img,
               active, support_email, feedback_email, default_password, forceinfocheck,
               salemanager_id, accountmanager_id, dataupload_ind, datadownload_ind);
            int ret = SiteProvider.PR2.InsertFacilityGroup(record);

            BizObject.PurgeCacheItems("FacilityGroups_FacilityGroup");
            return ret;
        }

        /// <summary>
        /// Deletes an existing FacilityGroup, but first checks if OK to delete
        /// </summary>
        public static bool DeleteFacilityGroup(int FacID)
        {
            bool IsOKToDelete = OKToDelete(FacID);
            if (IsOKToDelete)
            {
                return (bool)DeleteFacilityGroup(FacID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing FacilityGroup - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteFacilityGroup(int FacID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteFacilityGroup(FacID);
            //         new RecordDeletedEvent("FacilityGroup", FacID, null).Raise();
            BizObject.PurgeCacheItems("FacilityGroups_FacilityGroup");
            return ret;
        }

        /// <summary>
        /// Checks to see if a FacilityGroup can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int FacID)
        {
            return true;
        }



        /// <summary>
        /// Returns a FacilityGroup object filled with the data taken from the input FacilityGroupInfo
        /// </summary>
        private static FacilityGroup GetFacilityGroupFromFacilityGroupInfo(FacilityGroupInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new FacilityGroup(record.FacID, record.FacCode, record.FacName, record.MaxUsers,
                record.Started, record.Expires, record.RepID, record.Comment, record.Compliance,
                record.CustomTopics, record.QuizBowl,
           record.homeurl, record.tagline, record.address1, record.address2, record.city, record.state,
           record.zipcode, record.phone, record.contact_name, record.contact_phone, record.contact_ext,
           record.contact_email, record.parent_id, record.welcome_pg, record.login_help, record.logo_img,
           record.active, record.support_email, record.feedback_email, record.default_password, 
            record.forceinfocheck, record.salemanager_id, record.accountmanager_id, record.dataupload_ind,record.datadownload_ind);
            }
        }

        /// <summary>
        /// Returns a list of FacilityGroup objects filled with the data taken from the input list of FacilityGroupInfo
        /// </summary>
        private static List<FacilityGroup> GetFacilityGroupListFromFacilityGroupInfoList(List<FacilityGroupInfo> recordset)
        {
            List<FacilityGroup> FacilityGroups = new List<FacilityGroup>();
            foreach (FacilityGroupInfo record in recordset)
                FacilityGroups.Add(GetFacilityGroupFromFacilityGroupInfo(record));
            return FacilityGroups;
        }

        //edit by Chuan
        #region Banner
        public FacilityGroup()
        { }

        /// Returns a collection with all FacilityGroups except giftcard users
        public static List<FacilityGroup> GetFacilityGroupsExceptGiftCardBanner(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            if (cSortExpression.Length == 0)
                cSortExpression = "FacName";

            List<FacilityGroupInfo> recordset = SiteProvider.PR2.GetFacilityGroupsExceptGiftCardBanner(BANNERPARENTID, cSortExpression);
            return GetFacilityGroupListFromFacilityGroupInfoList(recordset);
        }
        /// Returns a random collection with all FacilityGroups except giftcard users
        public static List<FacilityGroup> GetRandomFacilityGroupsExceptGiftCardBanner()
        {
           List<FacilityGroupInfo> recordset = SiteProvider.PR2.GetRandomFacilityGroupsExceptGiftCard();
            return GetFacilityGroupListFromFacilityGroupInfoList(recordset);
        }
        public static int GetBannerFacilityID()
        {
            return SiteProvider.PR2.GetBannerFacilityID();
        }

        public static int GetBannerFacilityIDFromUsername(string username)
        {
            return SiteProvider.PR2.GetBannerFacilityIDFromUsername(username, BANNERPARENTID);
        }

        public static int GetBannerFacilityIDFromUserNamePassWD(string username, string passwd)
        {
            return SiteProvider.PR2.GetBannerFacilityIDFromUserNamePassWD(username, passwd, BANNERPARENTID);
        }
        /// <summary>
        /// Get facilities By Username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static List<FacilityGroup> GetFacilityGroupsByUsername(string username)
        {
            List<FacilityGroup> FacilityGroups = null;
            string key = "FacilityGroups_FacilityGroupsByUsername_" + username.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FacilityGroups = (List<FacilityGroup>)BizObject.Cache[key];
            }
            else
            {
                List<FacilityGroupInfo> recordset = SiteProvider.PR2.GetFacilityGroupByUserName(username);
                FacilityGroups = GetFacilityGroupListFromFacilityGroupInfoList(recordset);
                BasePR.CacheData(key, FacilityGroups);
            }
            return FacilityGroups;
        }

        #endregion

        #endregion
    }
}