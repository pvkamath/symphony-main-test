﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Fangbo Yang
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

/// <summary>
/// Summary description for CredentialFacility
/// </summary>
/// 

namespace PearlsReview.BLL
{
    public class CredentialFacility: BasePR
    {
        public CredentialFacility()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public CredentialFacility(int id, string desc, string code, string url)
        {
            this.CredentialID = id;
            this.CredentialDescription = desc;
            this.CredentialCode = code;
            this.CredentialURL = url;
        }

        #region Variables and Properties
        public int CredentialID { get; set; }
        public string  CredentialDescription { get; set; }
        public string CredentialCode { get; set; }
        public string  CredentialURL { get; set; }
        #endregion

        #region Methods

     

        public static int Insert(string cre_desc, string cre_code,string cre_URL)
        {
            CredentialFacilityInfo creinfo = new CredentialFacilityInfo(0,cre_desc,cre_code,cre_URL);

            int id = SiteProvider.PR2.InsertCredential(creinfo);

            return id;
        }

        public static bool Update(int cre_id, string cre_desc, string cre_code, string cre_URL)
        {
            CredentialFacilityInfo cinfo = new CredentialFacilityInfo(cre_id,cre_desc,cre_code,cre_URL);
            bool ret = SiteProvider.PR2.UpdateCredential(cinfo);

            return ret;
        }

        public static CredentialFacility GetCredentialFacilityByID(int cid)
        {
            CredentialFacility cre = null;
            string key = "CredentialFacility_CredentialFacility_" + cid.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                cre = (CredentialFacility)BizObject.Cache[key];
            }
            else
            {
                cre = GetCredentialFacilityFromInfo(SiteProvider.PR2.GetCredentialByID(cid));
                BasePR.CacheData(key,cre);
            }
            return cre;
        }

        private static CredentialFacility GetCredentialFacilityFromInfo(CredentialFacilityInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new CredentialFacility(record.CredentialID, record.CredentialDescription, record.CredentialCode, record.CredentialURL);
            }
        }

        public static List<CredentialFacility> GetCredentials(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            if (cSortExpression.Length == 0)
                cSortExpression = "cre_desc";

            List<CredentialFacility> cres = null;
            string key = "CredentialFacility_CredentialFacility_" +cSortExpression.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                cres = (List<CredentialFacility>)BizObject.Cache[key];
            }
            else
            {
                List<CredentialFacilityInfo> recordset = SiteProvider.PR2.GetCredentials(cSortExpression);
                cres = GetCredentialListFromCredentialFacilityInfoList(recordset);
                BasePR.CacheData(key,cres);
            }
            return cres;
        }

        private static List<CredentialFacility> GetCredentialListFromCredentialFacilityInfoList(List<CredentialFacilityInfo> recordset)
        {
            List<CredentialFacility> cres = new List<CredentialFacility>();
            foreach (CredentialFacilityInfo record in recordset)
                cres.Add(GetCredentialFacilityFromInfo(record));
            return cres;
        }

        public static bool DeleteCredential(int cre_id)
        {

            return (bool)DeleteCredential(cre_id,true);  
        }

        public static bool DeleteCredential(int cre_id, bool SkipOKtoDelete)
       {
        if(!SkipOKtoDelete)
        return false;
        bool ret=SiteProvider.PR2.DeleteCredential(cre_id);
        BizObject.PurgeCacheItems("CredentialFacilities_CredentialFacilities");
        return ret;
       }
        #endregion 
    }
}