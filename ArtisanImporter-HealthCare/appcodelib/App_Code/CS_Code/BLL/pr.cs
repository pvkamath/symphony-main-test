using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using PearlsReview.DAL;
using PearlsReview.BLL.PRBase;
using PearlsReview.QTI;
using System.Xml.Linq;

namespace PearlsReview.BLL
{
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// AdClick business object class
    /// </summary>
    //public class AdClick : BasePR
    //{
    //    private int _AdClickID = 0;
    //    public int AdClickID
    //    {
    //        get { return _AdClickID; }
    //        protected set { _AdClickID = value; }
    //    }

    //    private int _AdID = 0;
    //    public int AdID
    //    {
    //        get { return _AdID; }
    //        set { _AdID = value; }
    //    }

    //    private DateTime _Date = System.DateTime.Now;
    //    public DateTime Date
    //    {
    //        get { return _Date; }
    //        set { _Date = value; }
    //    }

    //    private string _PageName = "";
    //    public string PageName
    //    {
    //        get { return _PageName; }
    //        set { _PageName = value; }
    //    }


    //   public AdClick(int AdClickID, int AdID, DateTime Date, string PageName)
    //  {
    //        this.AdClickID = AdClickID;
    //        this.AdID = AdID;
    //        this.Date = Date;
    //        this.PageName = PageName;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = AdClick.DeleteAdClick(this.AdClickID);
    //        if (success)
    //            this.AdClickID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return AdClick.UpdateAdClick(this.AdClickID, this.AdID, this.Date, this.PageName);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all AdClicks
    //    /// </summary>
    //    public static List<AdClick> GetAdClicks(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "PageName";

    //        List<AdClick> AdClicks = null;
    //        string key = "AdClicks_AdClicks_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            AdClicks = (List<AdClick>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<AdClickInfo> recordset = SiteProvider.PR.GetAdClicks(cSortExpression);
    //            AdClicks = GetAdClickListFromAdClickInfoList(recordset);
    //            BasePR.CacheData(key, AdClicks);
    //        }
    //        return AdClicks;
    //    }


    //    /// <summary>
    //    /// Returns the number of total AdClicks
    //    /// </summary>
    //    public static int GetAdClickCount()
    //    {
    //        int AdClickCount = 0;
    //        string key = "AdClicks_AdClickCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            AdClickCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            AdClickCount = SiteProvider.PR.GetAdClickCount();
    //            BasePR.CacheData(key, AdClickCount);
    //        }
    //        return AdClickCount;
    //    }

    //    /// <summary>
    //    /// Returns a AdClick object with the specified ID
    //    /// </summary>
    //    public static AdClick GetAdClickByID(int AdClickID)
    //    {
    //        AdClick AdClick = null;
    //        string key = "AdClicks_AdClick_" + AdClickID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            AdClick = (AdClick)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            AdClick = GetAdClickFromAdClickInfo(SiteProvider.PR.GetAdClickByID(AdClickID));
    //            BasePR.CacheData(key, AdClick);
    //        }
    //        return AdClick;
    //    }

    //    /// <summary>
    //    /// Updates an existing AdClick
    //    /// </summary>
    //    public static bool UpdateAdClick(int AdClickID, int AdID, DateTime Date, string PageName)
    //    {
    //        PageName = BizObject.ConvertNullToEmptyString(PageName);


    //        AdClickInfo record = new AdClickInfo(AdClickID, AdID, Date, PageName);
    //        bool ret = SiteProvider.PR.UpdateAdClick(record);

    //        BizObject.PurgeCacheItems("AdClicks_AdClick_" + AdClickID.ToString());
    //        BizObject.PurgeCacheItems("AdClicks_AdClicks");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new AdClick
    //    /// </summary>
    //    public static int InsertAdClick(int AdID, DateTime Date, string PageName)
    //    {
    //        PageName = BizObject.ConvertNullToEmptyString(PageName);


    //        AdClickInfo record = new AdClickInfo(0, AdID, Date, PageName);
    //        int ret = SiteProvider.PR.InsertAdClick(record);

    //        BizObject.PurgeCacheItems("AdClicks_AdClick");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing AdClick, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteAdClick(int AdClickID)
    //    {
    //        bool IsOKToDelete = OKToDelete(AdClickID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteAdClick(AdClickID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing AdClick - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteAdClick(int AdClickID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteAdClick(AdClickID);
    //        //         new RecordDeletedEvent("AdClick", AdClickID, null).Raise();
    //        BizObject.PurgeCacheItems("AdClicks_AdClick");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a AdClick can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int AdClickID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a AdClick object filled with the data taken from the input AdClickInfo
    //    /// </summary>
    //    private static AdClick GetAdClickFromAdClickInfo(AdClickInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new AdClick(record.AdClickID, record.AdID, record.Date, record.PageName);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of AdClick objects filled with the data taken from the input list of AdClickInfo
    //    /// </summary>
    //    private static List<AdClick> GetAdClickListFromAdClickInfoList(List<AdClickInfo> recordset)
    //    {
    //        List<AdClick> AdClicks = new List<AdClick>();
    //        foreach (AdClickInfo record in recordset)
    //            AdClicks.Add(GetAdClickFromAdClickInfo(record));
    //        return AdClicks;
    //    }



    //}
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// Advertisement business object class
    /// </summary>
    //public class Advertisement : BasePR
    //{
    //    private int _AdID = 0;
    //    public int AdID
    //    {
    //        get { return _AdID; }
    //        protected set { _AdID = value; }
    //    }

    //    private string _AdName = "";
    //    public string AdName
    //    {
    //        get { return _AdName; }
    //        set { _AdName = value; }
    //    }

    //    private string _AdType = "";
    //    public string AdType
    //    {
    //        get { return _AdType; }
    //        set { _AdType = value; }
    //    }

    //    private string _AdDest = "";
    //    public string AdDest
    //    {
    //        get { return _AdDest; }
    //        set { _AdDest = value; }
    //    }

    //    private string _AdText = "";
    //    public string AdText
    //    {
    //        get { return _AdText; }
    //        set { _AdText = value; }
    //    }

    //    private string _AdFilename = "";
    //    public string AdFilename
    //    {
    //        get { return _AdFilename; }
    //        set { _AdFilename = value; }
    //    }

    //    private string _AdUnitType = "";
    //    public string AdUnitType
    //    {
    //        get { return _AdUnitType; }
    //        set { _AdUnitType = value; }
    //    }


    //   public Advertisement(int AdID, string AdName, string AdType, string AdDest, string AdText, string AdFilename, string AdUnitType)
    //  {
    //        this.AdID = AdID;
    //        this.AdName = AdName;
    //        this.AdType = AdType;
    //        this.AdDest = AdDest;
    //        this.AdText = AdText;
    //        this.AdFilename = AdFilename;
    //        this.AdUnitType = AdUnitType;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = Advertisement.DeleteAdvertisement(this.AdID);
    //        if (success)
    //            this.AdID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return Advertisement.UpdateAdvertisement(this.AdID, this.AdName, this.AdType, this.AdDest, this.AdText, this.AdFilename, this.AdUnitType);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all Advertisements
    //    /// </summary>
    //    public static List<Advertisement> GetAdvertisements(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "AdName";

    //        List<Advertisement> Advertisements = null;
    //        string key = "Advertisements_Advertisements_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Advertisements = (List<Advertisement>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<AdvertisementInfo> recordset = SiteProvider.PR.GetAdvertisements(cSortExpression);
    //            Advertisements = GetAdvertisementListFromAdvertisementInfoList(recordset);
    //            BasePR.CacheData(key, Advertisements);
    //        }
    //        return Advertisements;
    //    }


    //    /// <summary>
    //    /// Returns the number of total Advertisements
    //    /// </summary>
    //    public static int GetAdvertisementCount()
    //    {
    //        int AdvertisementCount = 0;
    //        string key = "Advertisements_AdvertisementCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            AdvertisementCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            AdvertisementCount = SiteProvider.PR.GetAdvertisementCount();
    //            BasePR.CacheData(key, AdvertisementCount);
    //        }
    //        return AdvertisementCount;
    //    }

    //    /// <summary>
    //    /// Returns a Advertisement object with the specified ID
    //    /// </summary>
    //    public static Advertisement GetAdvertisementByID(int AdID)
    //    {
    //        Advertisement Advertisement = null;
    //        string key = "Advertisements_Advertisement_" + AdID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Advertisement = (Advertisement)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            Advertisement = GetAdvertisementFromAdvertisementInfo(SiteProvider.PR.GetAdvertisementByID(AdID));
    //            BasePR.CacheData(key, Advertisement);
    //        }
    //        return Advertisement;
    //    }

    //    /// <summary>
    //    /// Updates an existing Advertisement
    //    /// </summary>
    //    public static bool UpdateAdvertisement(int AdID, string AdName, string AdType, string AdDest, string AdText, string AdFilename, string AdUnitType)
    //    {
    //        AdName = BizObject.ConvertNullToEmptyString(AdName);
    //        AdType = BizObject.ConvertNullToEmptyString(AdType);
    //        AdDest = BizObject.ConvertNullToEmptyString(AdDest);
    //        AdText = BizObject.ConvertNullToEmptyString(AdText);
    //        AdFilename = BizObject.ConvertNullToEmptyString(AdFilename);
    //        AdUnitType = BizObject.ConvertNullToEmptyString(AdUnitType);


    //        AdvertisementInfo record = new AdvertisementInfo(AdID, AdName, AdType, AdDest, AdText, AdFilename, AdUnitType);
    //        bool ret = SiteProvider.PR.UpdateAdvertisement(record);

    //        BizObject.PurgeCacheItems("Advertisements_Advertisement_" + AdID.ToString());
    //        BizObject.PurgeCacheItems("Advertisements_Advertisements");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new Advertisement
    //    /// </summary>
    //    public static int InsertAdvertisement(string AdName, string AdType, string AdDest, string AdText, string AdFilename, string AdUnitType)
    //    {
    //        AdName = BizObject.ConvertNullToEmptyString(AdName);
    //        AdType = BizObject.ConvertNullToEmptyString(AdType);
    //        AdDest = BizObject.ConvertNullToEmptyString(AdDest);
    //        AdText = BizObject.ConvertNullToEmptyString(AdText);
    //        AdFilename = BizObject.ConvertNullToEmptyString(AdFilename);
    //        AdUnitType = BizObject.ConvertNullToEmptyString(AdUnitType);


    //        AdvertisementInfo record = new AdvertisementInfo(0, AdName, AdType, AdDest, AdText, AdFilename, AdUnitType);
    //        int ret = SiteProvider.PR.InsertAdvertisement(record);

    //        BizObject.PurgeCacheItems("Advertisements_Advertisement");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing Advertisement, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteAdvertisement(int AdID)
    //    {
    //        bool IsOKToDelete = OKToDelete(AdID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteAdvertisement(AdID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing Advertisement - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteAdvertisement(int AdID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteAdvertisement(AdID);
    //        //         new RecordDeletedEvent("Advertisement", AdID, null).Raise();
    //        BizObject.PurgeCacheItems("Advertisements_Advertisement");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a Advertisement can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int AdID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a Advertisement object filled with the data taken from the input AdvertisementInfo
    //    /// </summary>
    //    private static Advertisement GetAdvertisementFromAdvertisementInfo(AdvertisementInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new Advertisement(record.AdID, record.AdName, record.AdType, record.AdDest, record.AdText, record.AdFilename, record.AdUnitType);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of Advertisement objects filled with the data taken from the input list of AdvertisementInfo
    //    /// </summary>
    //    private static List<Advertisement> GetAdvertisementListFromAdvertisementInfoList(List<AdvertisementInfo> recordset)
    //    {
    //        List<Advertisement> Advertisements = new List<Advertisement>();
    //        foreach (AdvertisementInfo record in recordset)
    //            Advertisements.Add(GetAdvertisementFromAdvertisementInfo(record));
    //        return Advertisements;
    //    }



    //}
 
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// Category business object class
    /// </summary>
//    public class Category : BasePR
//    {
//        private int _ID = 0;
//        public int ID
//        {
//            get { return _ID; }
//            protected set { _ID = value; }
//        }

//        private string _Title = "";
//        public string Title
//        {
//            get { return _Title; }
//            set { _Title = value; }
//        }

//        private bool _BuildPage = false;
//        public bool BuildPage
//        {
//            get { return _BuildPage; }
//            set { _BuildPage = value; }
//        }

//        private string _MetaKW = "";
//        public string MetaKW
//        {
//            get { return _MetaKW; }
//            set { _MetaKW = value; }
//        }

//        private string _MetaDesc = "";
//        public string MetaDesc
//        {
//            get { return _MetaDesc; }
//            set { _MetaDesc = value; }
//        }

//        private string _MetaTitle = "";
//        public string MetaTitle
//        {
//            get { return _MetaTitle; }
//            set { _MetaTitle = value; }
//        }

//        private string _PageText = "";
//        public string PageText
//        {
//            get { return _PageText; }
//            set { _PageText = value; }
//        }

//        private bool _Textbook = false;
//        public bool Textbook
//        {
//            get { return _Textbook; }
//            set { _Textbook = value; }
//        }

//        private bool _ShowInList = false;
//        public bool ShowInList
//        {
//            get { return _ShowInList; }
//            set { _ShowInList = value; }
//        }

//        private string _CertName = "";
//        public string CertName
//        {
//            get { return _CertName; }
//            set { _CertName = value; }
//        }

//        private string _CredAward = "";
//        public string CredAward
//        {
//            get { return _CredAward; }
//            set { _CredAward = value; }
//        }

//        private string _ExamType = "";
//        public string ExamType
//        {
//            get { return _ExamType; }
//            set { _ExamType = value; }
//        }

//        private string _ExamCost = "";
//        public string ExamCost
//        {
//            get { return _ExamCost; }
//            set { _ExamCost = value; }
//        }

//        private string _AdminOrg = "";
//        public string AdminOrg
//        {
//            get { return _AdminOrg; }
//            set { _AdminOrg = value; }
//        }

//        private string _Website = "";
//        public string Website
//        {
//            get { return _Website; }
//            set { _Website = value; }
//        }

//        private string _Reqments = "";
//        public string Reqments
//        {
//            get { return _Reqments; }
//            set { _Reqments = value; }
//        }

//        private string _EligCrit = "";
//        public string EligCrit
//        {
//            get { return _EligCrit; }
//            set { _EligCrit = value; }
//        }

//        private int _FacilityID = 0;
//        public int FacilityID
//        {
//            get { return _FacilityID; }
//            set { _FacilityID = value; }
//        }


//        public Category(int ID, string Title, bool BuildPage, string MetaKW, string MetaDesc,
//            string MetaTitle, string PageText, bool Textbook, bool ShowInList, string CertName,
//            string CredAward, string ExamType, string ExamCost, string AdminOrg, string Website,
//            string Reqments, string EligCrit, int FacilityID)
//      {
//            this.ID = ID;
//            this.Title = Title;
//            this.BuildPage = BuildPage;
//            this.MetaKW = MetaKW;
//            this.MetaDesc = MetaDesc;
//            this.MetaTitle = MetaTitle;
//            this.PageText = PageText;
//            this.Textbook = Textbook;
//            this.ShowInList = ShowInList;
//            this.CertName = CertName;
//            this.CredAward = CredAward;
//            this.ExamType = ExamType;
//            this.ExamCost = ExamCost;
//            this.AdminOrg = AdminOrg;
//            this.Website = Website;
//            this.Reqments = Reqments;
//            this.EligCrit = EligCrit;
//            this.FacilityID = FacilityID;
//        }

//        public bool Delete()
//        {
//            bool success = Category.DeleteCategory(this.ID);
//            if (success)
//                this.ID = 0;
//            return success;
//        }

//        public bool Update()
//        {
//            return Category.UpdateCategory(this.ID, this.Title, this.BuildPage, this.MetaKW, 
//                this.MetaDesc, this.MetaTitle, this.PageText, this.Textbook,
//                this.ShowInList, this.CertName, this.CredAward, this.ExamType, this.ExamCost,
//                this.AdminOrg, this.Website, this.Reqments, this.EligCrit, this.FacilityID);
//        }

//        /***********************************
//        * Static methods
//        ************************************/

//        /// <summary>
//        /// Returns a collection with all Categories
//        /// </summary>
//        public static List<Category> GetCategories(string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<Category> Categories = null;
//            string key = "Categories_Categories_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<Category>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryInfo> recordset = SiteProvider.PR.GetCategories(cSortExpression);
//                Categories = GetCategoryListFromCategoryInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;
//        }

       
//        /// <summary>
//        /// Returns a collection with all Compliance Categories
//        /// </summary>
//        public static List<Category> GetComplianceCategories(string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<Category> Categories = null;
//            string key = "Categories_ComplianceCategories_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<Category>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryInfo> recordset = SiteProvider.PR.GetComplianceCategories(cSortExpression);
//                Categories = GetCategoryListFromCategoryInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;
//        }

//        /// <summary>
//        /// Returns a collection with all Textbook Categories
//        /// </summary>
//        public static List<Category> GetTextbookCategories(string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<Category> Categories = null;
//            string key = "Categories_Categories_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<Category>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryInfo> recordset = SiteProvider.PR.GetTextbookCategories(cSortExpression);
//                Categories = GetCategoryListFromCategoryInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;
//        }

//        /// <summary>
//        /// Returns a collection with all Textbook Categories - this is the list for display
//        /// to website users (only those with ShowInList checked)
//        /// </summary>
//        public static List<Category> GetTextbookCategoriesForDisplay(string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<Category> Categories = null;
//            string key = "Categories_Categories_ForDisplay_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<Category>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryInfo> recordset = SiteProvider.PR.GetTextbookCategoriesForDisplay(cSortExpression);
//                Categories = GetCategoryListFromCategoryInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;
//        }


//        /// <summary>
//        /// Returns a collection with all Non-Textbook Categories
//        /// </summary>
//        public static List<Category> GetNonTextbookCategories(string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<Category> Categories = null;
//            string key = "Categories_Categories_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<Category>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryInfo> recordset = SiteProvider.PR.GetNonTextbookCategories(cSortExpression);
//                Categories = GetCategoryListFromCategoryInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;
//        }

//        /// <summary>
//        /// Returns a collection with all Non-Textbook Categories that have "ShowInList"
//        /// checked -- these are the ones that should display to users of the website
//        /// </summary>
//        public static List<Category> GetNonTextbookCategoriesForDisplay(string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<Category> Categories = null;
//            string key = "Categories_Categories_ForDisplay_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<Category>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryInfo> recordset = SiteProvider.PR.GetNonTextbookCategoriesForDisplay(cSortExpression);
//                Categories = GetCategoryListFromCategoryInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;
//        }

//        /// <summary>
//        /// Returns the number of total Categories
//        /// </summary>
//        public static int GetCategoryCount()
//        {
//            int CategoryCount = 0;
//            string key = "Categories_CategoryCount";

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                CategoryCount = (int)BizObject.Cache[key];
//            }
//            else
//            {
//                CategoryCount = SiteProvider.PR.GetCategoryCount();
//                BasePR.CacheData(key, CategoryCount);
//            }
//            return CategoryCount;
//        }

//        /// <summary>
//        /// Returns the number of total Textbook Categories
//        /// </summary>
//        public static int GetTextbookCategoryCount()
//        {
//            int CategoryCount = 0;
//            string key = "Categories_CategoryCount";

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                CategoryCount = (int)BizObject.Cache[key];
//            }
//            else
//            {
//                CategoryCount = SiteProvider.PR.GetTextbookCategoryCount();
//                BasePR.CacheData(key, CategoryCount);
//            }
//            return CategoryCount;
//        }

//        /// <summary>
//        /// Returns the number of total Non-Textbook Categories
//        /// </summary>
//        public static int GetNonTextbookCategoryCount()
//        {
//            int CategoryCount = 0;
//            string key = "Categories_CategoryCount";

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                CategoryCount = (int)BizObject.Cache[key];
//            }
//            else
//            {
//                CategoryCount = SiteProvider.PR.GetNonTextbookCategoryCount();
//                BasePR.CacheData(key, CategoryCount);
//            }
//            return CategoryCount;
//        }

//        /// <summary>
//        /// Returns a Category object with the specified ID
//        /// </summary>
//        public static Category GetCategoryByID(int ID)
//        {
//            Category Category = null;
//            string key = "Categories_Category_" + ID.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Category = (Category)BizObject.Cache[key];
//            }
//            else
//            {
//                Category = GetCategoryFromCategoryInfo(SiteProvider.PR.GetCategoryByID(ID));
//                BasePR.CacheData(key, Category);
//            }
//            return Category;
//        }

//        /// <summary>
//        /// Updates an existing Category
//        /// </summary>
//        public static bool UpdateCategory(int ID, string Title, bool BuildPage, string MetaKW, 
//            string MetaDesc, string MetaTitle, string PageText, bool Textbook,
//            bool ShowInList, string CertName, string CredAward, string ExamType, string ExamCost, 
//            string AdminOrg, string Website, string Reqments, string EligCrit, int FacilityID)
//        {
//            Title = BizObject.ConvertNullToEmptyString(Title);
//            MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
//            MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
//            MetaTitle = BizObject.ConvertNullToEmptyString(MetaTitle);
//            PageText = BizObject.ConvertNullToEmptyString(PageText);
//            CertName = BizObject.ConvertNullToEmptyString(CertName);
//            CredAward = BizObject.ConvertNullToEmptyString(CredAward);
//            ExamType = BizObject.ConvertNullToEmptyString(ExamType);
//            ExamCost = BizObject.ConvertNullToEmptyString(ExamCost);
//            AdminOrg = BizObject.ConvertNullToEmptyString(AdminOrg);
//            Website = BizObject.ConvertNullToEmptyString(Website);
//            Reqments = BizObject.ConvertNullToEmptyString(Reqments);
//            EligCrit = BizObject.ConvertNullToEmptyString(EligCrit);
//            if (FacilityID == -2)
//            {
//                Textbook = true;
//            }
//            else
//            {
//                Textbook = false;
//            }


//            CategoryInfo record = new CategoryInfo(ID, Title, BuildPage, MetaKW, MetaDesc, 
//                MetaTitle, PageText, Textbook, ShowInList, CertName, CredAward, ExamType, ExamCost,
//                AdminOrg, Website, Reqments, EligCrit, FacilityID);
//            bool ret = SiteProvider.PR.UpdateCategory(record);

//            /// TODO: need to clear out the textbook version also
//            BizObject.PurgeCacheItems("Categories_Category_" + ID.ToString());
//            BizObject.PurgeCacheItems("Categories_Categories");
//            return ret;
//        }

//        /// <summary>
//        /// Creates a new Category
//        /// </summary>
//        public static int InsertCategory(string Title, bool BuildPage, string MetaKW, 
//            string MetaDesc, string MetaTitle, string PageText, bool Textbook,
//            bool ShowInList, string CertName, string CredAward, string ExamType, string ExamCost,
//            string AdminOrg, string Website, string Reqments, string EligCrit, int FacilityID)
//        {
//            Title = BizObject.ConvertNullToEmptyString(Title);
//            MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
//            MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
//            MetaTitle = BizObject.ConvertNullToEmptyString(MetaTitle);
//            PageText = BizObject.ConvertNullToEmptyString(PageText);
//            CertName = BizObject.ConvertNullToEmptyString(CertName);
//            CredAward = BizObject.ConvertNullToEmptyString(CredAward);
//            ExamType = BizObject.ConvertNullToEmptyString(ExamType);
//            ExamCost = BizObject.ConvertNullToEmptyString(ExamCost);
//            AdminOrg = BizObject.ConvertNullToEmptyString(AdminOrg);
//            Website = BizObject.ConvertNullToEmptyString(Website);
//            Reqments = BizObject.ConvertNullToEmptyString(Reqments);
//            EligCrit = BizObject.ConvertNullToEmptyString(EligCrit);
//            if (FacilityID == -2)
//            {
//                Textbook = true;
//            }
//            else
//            {
//                Textbook = false;
//            }
//                CategoryInfo record = new CategoryInfo(0, Title, BuildPage, MetaKW, MetaDesc,
//                MetaTitle, PageText, Textbook, ShowInList, CertName, CredAward, ExamType, ExamCost,
//                AdminOrg, Website, Reqments, EligCrit, FacilityID);
//            int ret = SiteProvider.PR.InsertCategory(record);

//            BizObject.PurgeCacheItems("Categories_Category");
//            return ret;
//        }

//        /// <summary>
//        /// Creates a new Textbook Category
//        /// </summary>
//        public static int InsertTextbookCategory(string Title, bool BuildPage, string MetaKW, 
//            string MetaDesc, string MetaTitle, string PageText, bool Textbook,
//            bool ShowInList, string CertName, string CredAward, string ExamType, string ExamCost,
//            string AdminOrg, string Website, string Reqments, string EligCrit)

//        {
//            Title = BizObject.ConvertNullToEmptyString(Title);
//            MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
//            MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
//            MetaTitle = BizObject.ConvertNullToEmptyString(MetaTitle);
//            PageText = BizObject.ConvertNullToEmptyString(PageText);
//            CertName = BizObject.ConvertNullToEmptyString(CertName);
//            CredAward = BizObject.ConvertNullToEmptyString(CredAward);
//            ExamType = BizObject.ConvertNullToEmptyString(ExamType);
//            ExamCost = BizObject.ConvertNullToEmptyString(ExamCost);
//            AdminOrg = BizObject.ConvertNullToEmptyString(AdminOrg);
//            Website = BizObject.ConvertNullToEmptyString(Website);
//            Reqments = BizObject.ConvertNullToEmptyString(Reqments);
//            EligCrit = BizObject.ConvertNullToEmptyString(EligCrit);


//            // NOTE: We add an extra parameter at the end that is not passed into this method:
//            // By Default, we force FacilityID to be 0 for system-side categories
//            CategoryInfo record = new CategoryInfo(0, Title, BuildPage, MetaKW, MetaDesc,
//                MetaTitle, PageText, true, ShowInList, CertName, CredAward, ExamType, ExamCost,
//                AdminOrg, Website, Reqments, EligCrit, 0);
//            int ret = SiteProvider.PR.InsertCategory(record);

//            BizObject.PurgeCacheItems("Categories_Category");
//            return ret;
//        }

//        /// <summary>
//        /// Creates a new Non-Textbook Category
//        /// </summary>
//        public static int InsertNonTextbookCategory(string Title, bool BuildPage, string MetaKW,
//            string MetaDesc, string MetaTitle, string PageText, bool Textbook,
//            bool ShowInList, string CertName, string CredAward, string ExamType, string ExamCost,
//            string AdminOrg, string Website, string Reqments, string EligCrit)
//        {
//            Title = BizObject.ConvertNullToEmptyString(Title);
//            MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
//            MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
//            MetaTitle = BizObject.ConvertNullToEmptyString(MetaTitle);
//            PageText = BizObject.ConvertNullToEmptyString(PageText);
//            CertName = BizObject.ConvertNullToEmptyString(CertName);
//            CredAward = BizObject.ConvertNullToEmptyString(CredAward);
//            ExamType = BizObject.ConvertNullToEmptyString(ExamType);
//            ExamCost = BizObject.ConvertNullToEmptyString(ExamCost);
//            AdminOrg = BizObject.ConvertNullToEmptyString(AdminOrg);
//            Website = BizObject.ConvertNullToEmptyString(Website);
//            Reqments = BizObject.ConvertNullToEmptyString(Reqments);
//            EligCrit = BizObject.ConvertNullToEmptyString(EligCrit);

//            // NOTE: We add an extra parameter at the end that is not passed into this method:
//            // By Default, we force FacilityID to be 0 for system-side categories
//            CategoryInfo record = new CategoryInfo(0, Title, BuildPage, MetaKW, MetaDesc,
//                MetaTitle, PageText, false, ShowInList, CertName, CredAward, ExamType, ExamCost,
//                AdminOrg, Website, Reqments, EligCrit, 0);
//            int ret = SiteProvider.PR.InsertCategory(record);

//            BizObject.PurgeCacheItems("Categories_Category");
//            return ret;
//        }

//        /// <summary>
//        /// Deletes an existing Category, but first checks if OK to delete
//        /// </summary>
//        public static bool DeleteCategory(int ID)
//        {
//            bool IsOKToDelete = OKToDelete(ID);
//            if (IsOKToDelete)
//            {
//                return (bool)DeleteCategory(ID, true);
//            }
//            else
//            {
//                return false;
//            }
//        }

//        /// <summary>
//        /// Deletes an existing Category - second param forces skip of OKToDelete
//        /// (assuming that the calling program has already called that if it's
//        /// passing the second param as true)
//        /// </summary>
//        public static bool DeleteCategory(int ID, bool SkipOKToDelete)
//        {
//            if (!SkipOKToDelete)
//                return false;

//            bool ret = SiteProvider.PR.DeleteCategory(ID);
//            //         new RecordDeletedEvent("Category", ID, null).Raise();
//            BizObject.PurgeCacheItems("Categories_Category");
//            return ret;
//        }





//        /// <summary>
//        /// Returns a Category object filled with the data taken from the input CategoryInfo
//        /// </summary>
//        private static Category GetCategoryFromCategoryInfo(CategoryInfo record)
//        {
//            if (record == null)
//                return null;
//            else
//            {
//                return new Category(record.ID, record.Title, record.BuildPage, 
//                    record.MetaKW, record.MetaDesc, record.MetaTitle, record.PageText, record.Textbook,
//                    record.ShowInList, record.CertName, record.CredAward, record.ExamType, record.ExamCost,
//                    record.AdminOrg, record.Website, record.Reqments, record.EligCrit, record.FacilityID);
//            }
//        }

//        /// <summary>
//        /// Returns a list of Category objects filled with the data taken from the input list of CategoryInfo
//        /// </summary>
//        private static List<Category> GetCategoryListFromCategoryInfoList(List<CategoryInfo> recordset)
//        {
//            List<Category> Categories = new List<Category>();
//            foreach (CategoryInfo record in recordset)
//                Categories.Add(GetCategoryFromCategoryInfo(record));
//            return Categories;
//        }

//        /// <summary>
//        /// Returns a CheckBoxListCategory object filled with the data taken from the input CheckBoxListCategoryInfo
//        /// </summary>
//        private static CheckBoxListCategoryInfo GetCheckBoxListCategoryFromCheckBoxListCategoryInfo(CheckBoxListCategoryInfo record)
//        {
//            if (record == null)
//                return null;
//            else
//            {
//                return new CheckBoxListCategoryInfo(record.ID, record.Title, record.Selected);
//            }
//        }

//        /// <summary>
//        /// Returns a list of CheckBoxListCategory objects filled with the data taken from the input list of CheckBoxListCategoryInfo
//        /// </summary>
//        private static List<CheckBoxListCategoryInfo> GetCheckBoxListCategoryListFromCheckBoxListCategoryInfoList(List<CheckBoxListCategoryInfo> recordset)
//        {
//            List<CheckBoxListCategoryInfo> CheckBoxListCategories = new List<CheckBoxListCategoryInfo>();
//            foreach (CheckBoxListCategoryInfo record in recordset)
//                CheckBoxListCategories.Add(GetCheckBoxListCategoryFromCheckBoxListCategoryInfo(record));
//            return CheckBoxListCategories;
//        }

//        //        /// <summary>
////        /// TEST: See if we can get just a paged subset of records
////        /// Returns a collection with all Categories
////        /// </summary>
////        public static List<Category> GetCategoriesX(string cSortExpression)
////        {
////            if (cSortExpression == null)
////                cSortExpression = "";
////
////            // provide default sort
////            if (cSortExpression.Length == 0)
////                cSortExpression = "Title";
////
////            List<Category> Categories = null;
////            string key = "Categories_Categories_" + cSortExpression.ToString();
////
////            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
////           {
////                Categories = (List<Category>)BizObject.Cache[key];
////            }
////            else
////            {
////                List<CategoryInfo> recordset = SiteProvider.PR.GetCategoriesX(cSortExpression);
////                Categories = GetCategoryListFromCategoryInfoList(recordset);
////                BasePR.CacheData(key, Categories);
////            }
////            return Categories;
////        }

//        /// <summary>
//        /// Returns a collection with all Categories for a TopicID
//        /// </summary>
//        public static List<Category> GetCategoriesByTopicID(int TopicID, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<Category> Categories = null;
//            string key = "Categories_Categories_" + TopicID.ToString() + "_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<Category>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryInfo> recordset = SiteProvider.PR.GetCategoriesByTopicID(TopicID, cSortExpression);
//                Categories = GetCategoryListFromCategoryInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;
//        }

//        /// <summary>
//        /// Returns a collection with all Categories, plus info about those assigned to a TopicID
//        /// </summary>
//        public static List<CheckBoxListCategoryInfo> GetAllCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<CheckBoxListCategoryInfo> CheckBoxListCategories = null;
//            string key = "Categories_CheckBoxListCategories_" + TopicID.ToString() + "_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                CheckBoxListCategories = (List<CheckBoxListCategoryInfo>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CheckBoxListCategoryInfo> recordset = SiteProvider.PR.GetAllCategoriesPlusTopicAssignments(TopicID, cSortExpression);
//                CheckBoxListCategories = GetCheckBoxListCategoryListFromCheckBoxListCategoryInfoList(recordset);
//                BasePR.CacheData(key, CheckBoxListCategories);
//            }
//            return CheckBoxListCategories;

//        }

//        /// <summary>
//        /// Returns a collection with all Categories, plus info about those assigned to a TopicID
//        /// </summary>
//        public static List<CheckBoxListCategoryInfo> GetAllNonTextbookCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<CheckBoxListCategoryInfo> CheckBoxListCategories = null;
//            string key = "Categories_NonTextbook_CheckBoxListCategories_" + TopicID.ToString() + "_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                CheckBoxListCategories = (List<CheckBoxListCategoryInfo>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CheckBoxListCategoryInfo> recordset = SiteProvider.PR.GetAllNonTextbookCategoriesPlusTopicAssignments(TopicID, cSortExpression);
//                CheckBoxListCategories = GetCheckBoxListCategoryListFromCheckBoxListCategoryInfoList(recordset);
//                BasePR.CacheData(key, CheckBoxListCategories);
//            }
//            return CheckBoxListCategories;

//        }

//        /// <summary>
//        /// Returns a collection with all Categories, plus info about those assigned to a TopicID
//        /// </summary>
//        public static List<CheckBoxListCategoryInfo> GetAllTextbookCategoriesPlusTopicAssignments(int TopicID, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<CheckBoxListCategoryInfo> CheckBoxListCategories = null;
//            string key = "Categories_Textbook_CheckBoxListCategories_" + TopicID.ToString() + "_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                CheckBoxListCategories = (List<CheckBoxListCategoryInfo>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CheckBoxListCategoryInfo> recordset = SiteProvider.PR.GetAllTextbookCategoriesPlusTopicAssignments(TopicID, cSortExpression);
//                CheckBoxListCategories = GetCheckBoxListCategoryListFromCheckBoxListCategoryInfoList(recordset);
//                BasePR.CacheData(key, CheckBoxListCategories);
//            }
//            return CheckBoxListCategories;

//        }

//        /// <summary>
//        /// Returns a collection with all Textbook Categories NOT assigned
//        /// to a specific TopicID
//        /// (used for assigning additional categories to a topic)
//        /// </summary>
//        public static List<Category> GetTextbookCategoriesNotAssignedToTopicID(int TopicID, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<Category> Categories = null;
//            List<CategoryInfo> recordset =
//                SiteProvider.PR.GetTextbookCategoriesNotAssignedToTopicID(TopicID, cSortExpression);
//            Categories = GetCategoryListFromCategoryInfoList(recordset);
//            return Categories;
//        }

//        /// <summary>
//        /// Returns a collection with all Non-Textbook Categories NOT assigned
//        /// to a specific TopicID
//        /// (used for assigning additional categories to a topic)
//        /// </summary>
//        public static List<Category> GetNonTextbookCategoriesNotAssignedToTopicID(int TopicID, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<Category> Categories = null;
//            List<CategoryInfo> recordset =
//                SiteProvider.PR.GetNonTextbookCategoriesNotAssignedToTopicID(TopicID, cSortExpression);
//            Categories = GetCategoryListFromCategoryInfoList(recordset);
//            return Categories;
//        }

//        /// <summary>
//        /// Returns the number of total Categories assigned to a Topic
//        /// </summary>
//        public static int GetCategoryCount(int TopicID)
//        {
//            int CategoryCount = 0;
//            string key = "Categories_CategoryCount_" + TopicID.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                CategoryCount = (int)BizObject.Cache[key];
//            }
//            else
//            {
//                CategoryCount = SiteProvider.PR.GetCategoryCountByTopicID(TopicID);
//                BasePR.CacheData(key, CategoryCount);
//            }
//            return CategoryCount;
//        }

//        /// <summary>
//        /// Assign Topic to a Category
//        /// </summary>
//        public static bool AssignTopicToCategory(int TopicID, int CategoryID)
//        {
//            bool ret = SiteProvider.PR.AssignTopicToCategory(TopicID, CategoryID);
//            // TODO: release cache?
//            return ret;
//        }

//        /// <summary>
//        /// Detach Topic Assignment from a Category
//        /// </summary>
//        public static bool DetachTopicFromCategory(int TopicID, int CategoryID)
//        {
//            bool ret = SiteProvider.PR.DetachTopicFromCategory(TopicID, CategoryID);
//            // TODO: release cache?
//            return ret;
//        }

        //public static bool UpdateCategoryTopicAssignments(int CategoryID, string TopicIDAssignments)
        //{
        //    bool ret = SiteProvider.PR.UpdateCategoryTopicAssignments(CategoryID, TopicIDAssignments);
        //    // TODO: release cache?
        //    return ret;
        //}

//        /// <summary>
//        /// Checks to see if a Category can be deleted safely
//        /// (NO if it has any Topics assigned to it)
//        /// </summary>
//        public static bool OKToDelete(int ID)
//        {
//            int AssignedTopics = SiteProvider.PR.GetTopicCountByCategoryID(ID);
//            return (AssignedTopics == 0);
//        }


//        /// <summary>
//        /// Returns a collection with all non-textbook Categories with active topics, plus topic counts
//        /// </summary>
//        public static List<CategoryTopicCountInfo> GetNonTextbookCategoriesWithTopicCount(string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<CategoryTopicCountInfo> Categories = null;
//            string key = "Categories_CategoryTopicCountInfo_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryTopicCountInfo> recordset = SiteProvider.PR.GetNonTextbookCategoriesWithTopicCount(cSortExpression);
//                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;

//        }



//        /// <summary>
//        /// Returns a collection with all non-textbook Categories with active topics, plus topic counts
//        /// </summary>
//        public static List<CategoryTopicCountInfo> GetRNonTextbookCategoriesWithTopicCount(string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<CategoryTopicCountInfo> Categories = null;
//            string key = "RCategories_CategoryTopicCountInfo_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryTopicCountInfo> recordset = SiteProvider.PR.GetRNonTextbookCategoriesWithTopicCount(cSortExpression);
//                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;

//        }



//        /// <summary>
//        /// Returns a collection with all non-textbook Categories with active topics, plus topic counts
//        /// </summary>
//        public static List<CategoryTopicCountInfo> GetRNonTextbookStateCategoriesWithTopicCount()
//        {
            
//            List<CategoryTopicCountInfo> Categories = null;
//            string key = "RStateCategories_CategoryTopicCountInfo_";

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryTopicCountInfo> recordset = SiteProvider.PR.GetRNonTextbookStateCategoriesWithTopicCount();
//                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;

//        }





//        /// <summary>
//        /// Returns a collection with all non-textbook Categories with active topics, plus topic counts by passing facilityid
//        /// </summary>
//        public static List<CategoryTopicCountInfo> GetNonTextbookCategoriesWithTopicCountByFacilityId(int facilityid,string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<CategoryTopicCountInfo> Categories = null;
//            string key = "Categories_CategoryTopicCountInfo_" +facilityid+ cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryTopicCountInfo> recordset = SiteProvider.PR.GetNonTextbookCategoriesWithTopicCountByFacilityId(facilityid,cSortExpression);
//                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;

//        }


//        /// <summary>
//        /// Returns a collection with all non-textbook Categories with active topics, plus topic counts by passing facilityid
//        /// </summary>
//        public static List<CategoryTopicCountInfo> GetTextbookCategoriesWithTopicCountByFacilityId(int facilityid, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "Title";

//            List<CategoryTopicCountInfo> Categories = null;
//            string key = "Categories_CategoryTextTopicCountInfo_" + facilityid + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Categories = (List<CategoryTopicCountInfo>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<CategoryTopicCountInfo> recordset = SiteProvider.PR.GetTextbookCategoriesWithTopicCountByFacilityId(facilityid, cSortExpression);
//                Categories = GetCategoryTopicCountListFromCategoryTopicCountInfoList(recordset);
//                BasePR.CacheData(key, Categories);
//            }
//            return Categories;

//        }



//        /// <summary>
//        /// Returns a CategoryTopicCountInfo object filled with the data taken from the input CategoryTopicCountInfo
//        /// </summary>
//        private static CategoryTopicCountInfo GetCategoryTopicCountFromCategoryTopicCountInfo(CategoryTopicCountInfo record)
//        {
//            if (record == null)
//                return null;
//            else
//            {
//                return new CategoryTopicCountInfo(record.ID, record.Title, record.TopicCount,record.DTitle);
//            }
//        }

//        /// <summary>
//        /// Returns a list of CategoryTopicCountInfo objects filled with the data taken from the input list of CategoryTopicCountInfo
//        /// </summary>
//        private static List<CategoryTopicCountInfo> GetCategoryTopicCountListFromCategoryTopicCountInfoList(List<CategoryTopicCountInfo> recordset)
//        {
//            List<CategoryTopicCountInfo> Categories = new List<CategoryTopicCountInfo>();
//            foreach (CategoryTopicCountInfo record in recordset)
//                Categories.Add(GetCategoryTopicCountFromCategoryTopicCountInfo(record));
//            return Categories;
//        }


//        public static System.Data.DataSet GetCategoryByWebSite(string website)
//        {
//            System.Data.DataSet recordset =(SiteProvider.PR.GetCategoryByWebSite(website));

//            return recordset;
//        }

//        public static Category GetCatNameByTopicId(int TopicId)
//        {
//            Category recordset = GetCategoryFromCategoryInfo(SiteProvider.PR.GetCatNameByTopicId(TopicId));
//            return recordset;
//        }

//        public static Category GetPrimaryCatNameByTopicId(int TopicId)
//        {
//            Category recordset = GetCategoryFromCategoryInfo(SiteProvider.PR.GetPrimaryCatNameByTopicId(TopicId));
//            return recordset;
//        }

//        public static string CheckCatNameByTopicId(int TopicId, string CatName)
//        {
//            string objReturn = SiteProvider.PR.CheckCatNameByTopicId(TopicId, CatName);
//            return objReturn;
//        }

//    }

    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// CategoryLink business object class
    ///// </summary>
    //public class CategoryLink : BasePR
    //{
    //    private int _CatTopicID = 0;
    //    public int CatTopicID
    //    {
    //        get { return _CatTopicID; }
    //        protected set { _CatTopicID = value; }
    //    }

    //    private int _CategoryID = 0;
    //    public int CategoryID
    //    {
    //        get { return _CategoryID; }
    //        set { _CategoryID = value; }
    //    }

    //    private int _TopicID = 0;
    //    public int TopicID
    //    {
    //        get { return _TopicID; }
    //        set { _TopicID = value; }
    //    }


    //   public CategoryLink(int CatTopicID, int CategoryID, int TopicID)
    //  {
    //        this.CatTopicID = CatTopicID;
    //        this.CategoryID = CategoryID;
    //        this.TopicID = TopicID;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = CategoryLink.DeleteCategoryLink(this.CatTopicID);
    //        if (success)
    //            this.CatTopicID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return CategoryLink.UpdateCategoryLink(this.CatTopicID, this.CategoryID, this.TopicID);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all CategoryLinks
    //    /// </summary>
    //    public static List<CategoryLink> GetCategoryLinks(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<CategoryLink> CategoryLinks = null;
    //        string key = "CategoryLinks_CategoryLinks_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            CategoryLinks = (List<CategoryLink>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<CategoryLinkInfo> recordset = SiteProvider.PR.GetCategoryLinks(cSortExpression);
    //            CategoryLinks = GetCategoryLinkListFromCategoryLinkInfoList(recordset);
    //            BasePR.CacheData(key, CategoryLinks);
    //        }
    //        return CategoryLinks;
    //    }


    //    /// <summary>
    //    /// Returns the number of total CategoryLinks
    //    /// </summary>
    //    public static int GetCategoryLinkCount()
    //    {
    //        int CategoryLinkCount = 0;
    //        string key = "CategoryLinks_CategoryLinkCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            CategoryLinkCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            CategoryLinkCount = SiteProvider.PR.GetCategoryLinkCount();
    //            BasePR.CacheData(key, CategoryLinkCount);
    //        }
    //        return CategoryLinkCount;
    //    }

    //    /// <summary>
    //    /// Returns a CategoryLink object with the specified ID
    //    /// </summary>
    //    public static CategoryLink GetCategoryLinkByID(int CatTopicID)
    //    {
    //        CategoryLink CategoryLink = null;
    //        string key = "CategoryLinks_CategoryLink_" + CatTopicID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            CategoryLink = (CategoryLink)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            CategoryLink = GetCategoryLinkFromCategoryLinkInfo(SiteProvider.PR.GetCategoryLinkByID(CatTopicID));
    //            BasePR.CacheData(key, CategoryLink);
    //        }
    //        return CategoryLink;
    //    }

    //    /// <summary>
    //    /// Updates an existing CategoryLink
    //    /// </summary>
    //    public static bool UpdateCategoryLink(int CatTopicID, int CategoryID, int TopicID)
    //    {


    //        CategoryLinkInfo record = new CategoryLinkInfo(CatTopicID, CategoryID, TopicID);
    //        bool ret = SiteProvider.PR.UpdateCategoryLink(record);

    //        BizObject.PurgeCacheItems("CategoryLinks_CategoryLink_" + CatTopicID.ToString());
    //        BizObject.PurgeCacheItems("CategoryLinks_CategoryLinks");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new CategoryLink
    //    /// </summary>
    //    public static int InsertCategoryLink(int CategoryID, int TopicID)
    //    {


    //        CategoryLinkInfo record = new CategoryLinkInfo(0, CategoryID, TopicID);
    //        int ret = SiteProvider.PR.InsertCategoryLink(record);

    //        BizObject.PurgeCacheItems("CategoryLinks_CategoryLink");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing CategoryLink, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteCategoryLink(int CatTopicID)
    //    {
    //        bool IsOKToDelete = OKToDelete(CatTopicID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteCategoryLink(CatTopicID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing CategoryLink - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteCategoryLink(int CatTopicID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteCategoryLink(CatTopicID);
    //        //         new RecordDeletedEvent("CategoryLink", CatTopicID, null).Raise();
    //        BizObject.PurgeCacheItems("CategoryLinks_CategoryLink");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a CategoryLink can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int CatTopicID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a CategoryLink object filled with the data taken from the input CategoryLinkInfo
    //    /// </summary>
    //    private static CategoryLink GetCategoryLinkFromCategoryLinkInfo(CategoryLinkInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new CategoryLink(record.CatTopicID, record.CategoryID, record.TopicID);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of CategoryLink objects filled with the data taken from the input list of CategoryLinkInfo
    //    /// </summary>
    //    private static List<CategoryLink> GetCategoryLinkListFromCategoryLinkInfoList(List<CategoryLinkInfo> recordset)
    //    {
    //        List<CategoryLink> CategoryLinks = new List<CategoryLink>();
    //        foreach (CategoryLinkInfo record in recordset)
    //            CategoryLinks.Add(GetCategoryLinkFromCategoryLinkInfo(record));
    //        return CategoryLinks;
    //    }



    //}
    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// CertificateDefinition business object class
    ///// </summary>
    //public class CertificateDefinition : BasePR
    //{
    //    private int _CertID = 0;
    //    public int CertID
    //    {
    //        get { return _CertID; }
    //        protected set { _CertID = value; }
    //    }

    //    private string _CertName = "";
    //    public string CertName
    //    {
    //        get { return _CertName; }
    //        set { _CertName = value; }
    //    }

    //    private string _CertBody = "";
    //    public string CertBody
    //    {
    //        get { return _CertBody; }
    //        set { _CertBody = value; }
    //    }

    //    private string _CertType = "";
    //    public string CertType
    //    {
    //        get { return _CertType; }
    //        set { _CertType = value; }
    //    }

    //    private int _FacilityID = 0;
    //    public int FacilityID
    //    {
    //        get { return _FacilityID; }
    //        set { _FacilityID = value; }
    //    }


    //    public CertificateDefinition(int CertID, string CertName, string CertBody, string CertType, int FacilityID)
    //  {
    //        this.CertID = CertID;
    //        this.CertName = CertName;
    //        this.CertBody = CertBody;
    //        this.CertType = CertType;
    //        this.FacilityID = FacilityID;
    //    }

    //    public bool Delete()
    //    {
    //        bool success = CertificateDefinition.DeleteCertificateDefinition(this.CertID);
    //        if (success)
    //            this.CertID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return CertificateDefinition.UpdateCertificateDefinition(this.CertID, this.CertName, 
    //            this.CertBody, this.CertType, this.FacilityID);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all CertificateDefinitions
    //    /// </summary>
    //    public static List<CertificateDefinition> GetCertificateDefinitions(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "CertName";

    //        List<CertificateDefinition> CertificateDefinitions = null;
    //        string key = "CertificateDefinitions_CertificateDefinitions_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            CertificateDefinitions = (List<CertificateDefinition>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<CertificateDefinitionInfo> recordset = SiteProvider.PR.GetCertificateDefinitions(cSortExpression);
    //            CertificateDefinitions = GetCertificateDefinitionListFromCertificateDefinitionInfoList(recordset);
    //            BasePR.CacheData(key, CertificateDefinitions);
    //        }
    //        return CertificateDefinitions;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all CertificateDefinitions by CertType
    //    /// CertType: L=Lecture, C=CE Credit
    //    /// </summary>
    //    public static List<CertificateDefinition> GetCertificateDefinitionsByCertType(string cCertType, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "CertName";

    //        List<CertificateDefinition> CertificateDefinitions = null;
    //        string key = "CertificateDefinitions_CertificateDefinitionsByCertType_" +
    //            cCertType.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            CertificateDefinitions = (List<CertificateDefinition>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<CertificateDefinitionInfo> recordset =
    //                SiteProvider.PR.GetCertificateDefinitionsByCertType(cCertType, cSortExpression);
    //            CertificateDefinitions = GetCertificateDefinitionListFromCertificateDefinitionInfoList(recordset);
    //            BasePR.CacheData(key, CertificateDefinitions);
    //        }
    //        return CertificateDefinitions;
    //    }



    //    /// <summary>
    //    /// Returns the number of total CertificateDefinitions
    //    /// </summary>
    //    public static int GetCertificateDefinitionCount()
    //    {
    //        int CertificateDefinitionCount = 0;
    //        string key = "CertificateDefinitions_CertificateDefinitionCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            CertificateDefinitionCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            CertificateDefinitionCount = SiteProvider.PR.GetCertificateDefinitionCount();
    //            BasePR.CacheData(key, CertificateDefinitionCount);
    //        }
    //        return CertificateDefinitionCount;
    //    }

    //    /// <summary>
    //    /// Returns a CertificateDefinition object with the specified ID
    //    /// </summary>
    //    public static CertificateDefinition GetCertificateDefinitionByID(int CertID)
    //    {
    //        CertificateDefinition CertificateDefinition = null;
    //        string key = "CertificateDefinitions_CertificateDefinition_" + CertID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            CertificateDefinition = (CertificateDefinition)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            CertificateDefinition = GetCertificateDefinitionFromCertificateDefinitionInfo(SiteProvider.PR.GetCertificateDefinitionByID(CertID));
    //            BasePR.CacheData(key, CertificateDefinition);
    //        }
    //        return CertificateDefinition;
    //    }

    //    /// <summary>
    //    /// Updates an existing CertificateDefinition
    //    /// </summary>
    //    public static bool UpdateCertificateDefinition(int CertID, string CertName, string CertBody, 
    //        string CertType, int FacilityID)
    //    {
    //        CertName = BizObject.ConvertNullToEmptyString(CertName);
    //        CertBody = BizObject.ConvertNullToEmptyString(CertBody);
    //        CertType = BizObject.ConvertNullToEmptyString(CertType);


    //        CertificateDefinitionInfo record = new CertificateDefinitionInfo(CertID, CertName, CertBody, 
    //            CertType, FacilityID);
    //        bool ret = SiteProvider.PR.UpdateCertificateDefinition(record);

    //        BizObject.PurgeCacheItems("CertificateDefinitions_CertificateDefinition_" + CertID.ToString());
    //        BizObject.PurgeCacheItems("CertificateDefinitions_CertificateDefinitions");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new CertificateDefinition
    //    /// </summary>
    //    public static int InsertCertificateDefinition(string CertName, string CertBody, string CertType, 
    //        int FacilityID)
    //    {
    //        CertName = BizObject.ConvertNullToEmptyString(CertName);
    //        CertBody = BizObject.ConvertNullToEmptyString(CertBody);
    //        CertType = BizObject.ConvertNullToEmptyString(CertType);

    //        CertificateDefinitionInfo record = new CertificateDefinitionInfo(0, CertName, CertBody, CertType, 0);
    //        int ret = SiteProvider.PR.InsertCertificateDefinition(record);

    //        BizObject.PurgeCacheItems("CertificateDefinitions_CertificateDefinition");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing CertificateDefinition, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteCertificateDefinition(int CertID)
    //    {
    //        bool IsOKToDelete = OKToDelete(CertID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteCertificateDefinition(CertID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing CertificateDefinition - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteCertificateDefinition(int CertID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteCertificateDefinition(CertID);
    //        //         new RecordDeletedEvent("CertificateDefinition", CertID, null).Raise();
    //        BizObject.PurgeCacheItems("CertificateDefinitions_CertificateDefinition");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a CertificateDefinition can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int CertID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a CertificateDefinition object filled with the data taken from the input CertificateDefinitionInfo
    //    /// </summary>
    //    private static CertificateDefinition GetCertificateDefinitionFromCertificateDefinitionInfo(CertificateDefinitionInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new CertificateDefinition(record.CertID, record.CertName, record.CertBody, 
    //                record.CertType, record.FacilityID);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of CertificateDefinition objects filled with the data taken from the input list of CertificateDefinitionInfo
    //    /// </summary>
    //    private static List<CertificateDefinition> GetCertificateDefinitionListFromCertificateDefinitionInfoList(List<CertificateDefinitionInfo> recordset)
    //    {
    //        List<CertificateDefinition> CertificateDefinitions = new List<CertificateDefinition>();
    //        foreach (CertificateDefinitionInfo record in recordset)
    //            CertificateDefinitions.Add(GetCertificateDefinitionFromCertificateDefinitionInfo(record));
    //        return CertificateDefinitions;
    //    }        

    //}   


    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// EmailDefinition business object class
    ///// </summary>
    //public class EmailDefinition : BasePR
    //{
    //    private int _EDefID = 0;
    //    public int EDefID
    //    {
    //        get { return _EDefID; }
    //        protected set { _EDefID = value; }
    //    }

    //    private string _EDefName = "";
    //    public string EDefName
    //    {
    //        get { return _EDefName; }
    //        set { _EDefName = value; }
    //    }

    //    private string _EDefText = "";
    //    public string EDefText
    //    {
    //        get { return _EDefText; }
    //        set { _EDefText = value; }
    //    }


    //   public EmailDefinition(int EDefID, string EDefName, string EDefText)
    //  {
    //        this.EDefID = EDefID;
    //        this.EDefName = EDefName;
    //        this.EDefText = EDefText;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = EmailDefinition.DeleteEmailDefinition(this.EDefID);
    //        if (success)
    //            this.EDefID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return EmailDefinition.UpdateEmailDefinition(this.EDefID, this.EDefName, this.EDefText);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all EmailDefinitions
    //    /// </summary>
    //    public static List<EmailDefinition> GetEmailDefinitions(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "EDefName";

    //        List<EmailDefinition> EmailDefinitions = null;
    //        string key = "EmailDefinitions_EmailDefinitions_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            EmailDefinitions = (List<EmailDefinition>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<EmailDefinitionInfo> recordset = SiteProvider.PR.GetEmailDefinitions(cSortExpression);
    //            EmailDefinitions = GetEmailDefinitionListFromEmailDefinitionInfoList(recordset);
    //            BasePR.CacheData(key, EmailDefinitions);
    //        }
    //        return EmailDefinitions;
    //    }


    //    /// <summary>
    //    /// Returns the number of total EmailDefinitions
    //    /// </summary>
    //    public static int GetEmailDefinitionCount()
    //    {
    //        int EmailDefinitionCount = 0;
    //        string key = "EmailDefinitions_EmailDefinitionCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            EmailDefinitionCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            EmailDefinitionCount = SiteProvider.PR.GetEmailDefinitionCount();
    //            BasePR.CacheData(key, EmailDefinitionCount);
    //        }
    //        return EmailDefinitionCount;
    //    }

    //    /// <summary>
    //    /// Returns a EmailDefinition object with the specified ID
    //    /// </summary>
    //    public static EmailDefinition GetEmailDefinitionByID(int EDefID)
    //    {
    //        EmailDefinition EmailDefinition = null;
    //        string key = "EmailDefinitions_EmailDefinition_" + EDefID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            EmailDefinition = (EmailDefinition)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            EmailDefinition = GetEmailDefinitionFromEmailDefinitionInfo(SiteProvider.PR.GetEmailDefinitionByID(EDefID));
    //            BasePR.CacheData(key, EmailDefinition);
    //        }
    //        return EmailDefinition;
    //    }

    //    /// <summary>
    //    /// Updates an existing EmailDefinition
    //    /// </summary>
    //    public static bool UpdateEmailDefinition(int EDefID, string EDefName, string EDefText)
    //    {
    //        EDefName = BizObject.ConvertNullToEmptyString(EDefName);
    //        EDefText = BizObject.ConvertNullToEmptyString(EDefText);


    //        EmailDefinitionInfo record = new EmailDefinitionInfo(EDefID, EDefName, EDefText);
    //        bool ret = SiteProvider.PR.UpdateEmailDefinition(record);

    //        BizObject.PurgeCacheItems("EmailDefinitions_EmailDefinition_" + EDefID.ToString());
    //        BizObject.PurgeCacheItems("EmailDefinitions_EmailDefinitions");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new EmailDefinition
    //    /// </summary>
    //    public static int InsertEmailDefinition(string EDefName, string EDefText)
    //    {
    //        EDefName = BizObject.ConvertNullToEmptyString(EDefName);
    //        EDefText = BizObject.ConvertNullToEmptyString(EDefText);


    //        EmailDefinitionInfo record = new EmailDefinitionInfo(0, EDefName, EDefText);
    //        int ret = SiteProvider.PR.InsertEmailDefinition(record);

    //        BizObject.PurgeCacheItems("EmailDefinitions_EmailDefinition");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing EmailDefinition, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteEmailDefinition(int EDefID)
    //    {
    //        bool IsOKToDelete = OKToDelete(EDefID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteEmailDefinition(EDefID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing EmailDefinition - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteEmailDefinition(int EDefID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteEmailDefinition(EDefID);
    //        //         new RecordDeletedEvent("EmailDefinition", EDefID, null).Raise();
    //        BizObject.PurgeCacheItems("EmailDefinitions_EmailDefinition");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a EmailDefinition can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int EDefID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a EmailDefinition object filled with the data taken from the input EmailDefinitionInfo
    //    /// </summary>
    //    private static EmailDefinition GetEmailDefinitionFromEmailDefinitionInfo(EmailDefinitionInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new EmailDefinition(record.EDefID, record.EDefName, record.EDefText);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of EmailDefinition objects filled with the data taken from the input list of EmailDefinitionInfo
    //    /// </summary>
    //    private static List<EmailDefinition> GetEmailDefinitionListFromEmailDefinitionInfoList(List<EmailDefinitionInfo> recordset)
    //    {
    //        List<EmailDefinition> EmailDefinitions = new List<EmailDefinition>();
    //        foreach (EmailDefinitionInfo record in recordset)
    //            EmailDefinitions.Add(GetEmailDefinitionFromEmailDefinitionInfo(record));
    //        return EmailDefinitions;
    //    }



    //}
    ////////////////////////////////////////////////////////////
    ///// <summary>
    ///// EmailQueue business object class
    ///// </summary>
    //public class EmailQueue : BasePR
    //{
    //    private int _EQueueID = 0;
    //    public int EQueueID
    //    {
    //        get { return _EQueueID; }
    //        protected set { _EQueueID = value; }
    //    }

    //    private int _UserID = 0;
    //    public int UserID
    //    {
    //        get { return _UserID; }
    //        set { _UserID = value; }
    //    }

    //    private int _EDefID = 0;
    //    public int EDefID
    //    {
    //        get { return _EDefID; }
    //        set { _EDefID = value; }
    //    }

    //    private string _SentDate = "";
    //    public string SentDate
    //    {
    //        get { return _SentDate; }
    //        set { _SentDate = value; }
    //    }


    //   public EmailQueue(int EQueueID, int UserID, int EDefID, string SentDate)
    //  {
    //        this.EQueueID = EQueueID;
    //        this.UserID = UserID;
    //        this.EDefID = EDefID;
    //        this.SentDate = SentDate;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = EmailQueue.DeleteEmailQueue(this.EQueueID);
    //        if (success)
    //            this.EQueueID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return EmailQueue.UpdateEmailQueue(this.EQueueID, this.UserID, this.EDefID, this.SentDate);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all EmailQueue
    //    /// </summary>
    //    public static List<EmailQueue> GetEmailQueue(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "SentDate";

    //        List<EmailQueue> EmailQueue = null;
    //        string key = "EmailQueue_EmailQueue_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            EmailQueue = (List<EmailQueue>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<EmailQueueInfo> recordset = SiteProvider.PR.GetEmailQueue(cSortExpression);
    //            EmailQueue = GetEmailQueueListFromEmailQueueInfoList(recordset);
    //            BasePR.CacheData(key, EmailQueue);
    //        }
    //        return EmailQueue;
    //    }


    //    /// <summary>
    //    /// Returns the number of total EmailQueue
    //    /// </summary>
    //    public static int GetEmailQueueCount()
    //    {
    //        int EmailQueueCount = 0;
    //        string key = "EmailQueue_EmailQueueCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            EmailQueueCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            EmailQueueCount = SiteProvider.PR.GetEmailQueueCount();
    //            BasePR.CacheData(key, EmailQueueCount);
    //        }
    //        return EmailQueueCount;
    //    }

    //    /// <summary>
    //    /// Returns a EmailQueue object with the specified ID
    //    /// </summary>
    //    public static EmailQueue GetEmailQueueByID(int EQueueID)
    //    {
    //        EmailQueue EmailQueue = null;
    //        string key = "EmailQueue_EmailQueue_" + EQueueID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            EmailQueue = (EmailQueue)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            EmailQueue = GetEmailQueueFromEmailQueueInfo(SiteProvider.PR.GetEmailQueueByID(EQueueID));
    //            BasePR.CacheData(key, EmailQueue);
    //        }
    //        return EmailQueue;
    //    }

    //    /// <summary>
    //    /// Updates an existing EmailQueue
    //    /// </summary>
    //    public static bool UpdateEmailQueue(int EQueueID, int UserID, int EDefID, string SentDate)
    //    {


    //        EmailQueueInfo record = new EmailQueueInfo(EQueueID, UserID, EDefID, SentDate);
    //        bool ret = SiteProvider.PR.UpdateEmailQueue(record);

    //        BizObject.PurgeCacheItems("EmailQueue_EmailQueue_" + EQueueID.ToString());
    //        BizObject.PurgeCacheItems("EmailQueue_EmailQueue");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new EmailQueue
    //    /// </summary>
    //    public static int InsertEmailQueue(int UserID, int EDefID, string SentDate)
    //    {


    //        EmailQueueInfo record = new EmailQueueInfo(0, UserID, EDefID, SentDate);
    //        int ret = SiteProvider.PR.InsertEmailQueue(record);

    //        BizObject.PurgeCacheItems("EmailQueue_EmailQueue");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing EmailQueue, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteEmailQueue(int EQueueID)
    //    {
    //        bool IsOKToDelete = OKToDelete(EQueueID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteEmailQueue(EQueueID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing EmailQueue - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteEmailQueue(int EQueueID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteEmailQueue(EQueueID);
    //        //         new RecordDeletedEvent("EmailQueue", EQueueID, null).Raise();
    //        BizObject.PurgeCacheItems("EmailQueue_EmailQueue");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a EmailQueue can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int EQueueID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a EmailQueue object filled with the data taken from the input EmailQueueInfo
    //    /// </summary>
    //    private static EmailQueue GetEmailQueueFromEmailQueueInfo(EmailQueueInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new EmailQueue(record.EQueueID, record.UserID, record.EDefID, record.SentDate);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of EmailQueue objects filled with the data taken from the input list of EmailQueueInfo
    //    /// </summary>
    //    private static List<EmailQueue> GetEmailQueueListFromEmailQueueInfoList(List<EmailQueueInfo> recordset)
    //    {
    //        List<EmailQueue> EmailQueue = new List<EmailQueue>();
    //        foreach (EmailQueueInfo record in recordset)
    //            EmailQueue.Add(GetEmailQueueFromEmailQueueInfo(record));
    //        return EmailQueue;
    //    }

    //}
    ////////////////////////////////////////////////////////////
    ///// <summary>
    ///// FacilityGroup business object class
    ///// ***** CEDIRECT VERSION ************
    ///// </summary>
    //public class FacilityGroup : BasePR
    //{
    //    private int _FacID = 0;
    //    public int FacID
    //    {
    //        get { return _FacID; }
    //        protected set { _FacID = value; }
    //    }

    //    private string _FacCode = "";
    //    public string FacCode
    //    {
    //        get { return _FacCode; }
    //        set { _FacCode = value; }
    //    }

    //    private string _FacName = "";
    //    public string FacName
    //    {
    //        get { return _FacName; }
    //        set { _FacName = value; }
    //    }

    //    private int _MaxUsers = 1000;
    //    public int MaxUsers
    //    {
    //        get { return _MaxUsers; }
    //        set { _MaxUsers = value; }
    //    }

    //    private DateTime _Started = System.DateTime.MinValue;
    //    public DateTime Started
    //    {
    //        get { return _Started; }
    //        set { _Started = value; }
    //    }

    //    private DateTime _Expires = System.DateTime.MaxValue;
    //    public DateTime Expires
    //    {
    //        get { return _Expires; }
    //        set { _Expires = value; }
    //    }

    //    private int _RepID = 0;
    //    public int RepID
    //    {
    //        get { return _RepID; }
    //        set { _RepID = value; }
    //    }

    //    private string _Comment = "";
    //    public string Comment
    //    {
    //        get { return _Comment; }
    //        set { _Comment = value; }
    //    }

    //    private bool _Compliance = false;
    //    public bool Compliance
    //    {
    //        get { return _Compliance; }
    //        set { _Compliance = value; }
    //    }

    //    private bool _CustomTopics = false;
    //    public bool CustomTopics
    //    {
    //        get { return _CustomTopics; }
    //        set { _CustomTopics = value; }
    //    }

    //    private bool _QuizBowl = false;
    //    public bool QuizBowl
    //    {
    //        get { return _QuizBowl; }
    //        set { _QuizBowl = value; }
    //    }

    //    private string _homeurl = "";
    //    public string homeurl
    //    {
    //        get { return _homeurl; }
    //        set { _homeurl = value; }
    //    }

    //    private string _tagline = "";
    //    public string tagline
    //    {
    //        get { return _tagline; }
    //        set { _tagline = value; }
    //    }

    //    private string _address1 = "";
    //    public string address1
    //    {
    //        get { return _address1; }
    //        set { _address1 = value; }
    //    }

    //    private string _address2 = "";
    //    public string address2
    //    {
    //        get { return _address2; }
    //        set { _address2 = value; }
    //    }

    //    private string _city = "";
    //    public string city
    //    {
    //        get { return _city; }
    //        set { _city = value; }
    //    }

    //    private string _state = "";
    //    public string state
    //    {
    //        get { return _state; }
    //        set { _state = value; }
    //    }

    //    private string _zipcode = "";
    //    public string zipcode
    //    {
    //        get { return _zipcode; }
    //        set { _zipcode = value; }
    //    }

    //    private string _phone = "";
    //    public string phone
    //    {
    //        get { return _phone; }
    //        set { _phone = value; }
    //    }

    //    private string _contact_name = "";
    //    public string contact_name
    //    {
    //        get { return _contact_name; }
    //        set { _contact_name = value; }
    //    }

    //    private string _contact_phone = "";
    //    public string contact_phone
    //    {
    //        get { return _contact_phone; }
    //        set { _contact_phone = value; }
    //    }

    //    private string _contact_ext = "";
    //    public string contact_ext
    //    {
    //        get { return _contact_ext; }
    //        set { _contact_ext = value; }
    //    }

    //    private string _contact_email = "";
    //    public string contact_email
    //    {
    //        get { return _contact_email; }
    //        set { _contact_email = value; }
    //    }

    //    private int _parent_id = 0;
    //    public int parent_id
    //    {
    //        get { return _parent_id; }
    //        set { _parent_id = value; }
    //    }

    //    private string _welcome_pg = "";
    //    public string welcome_pg
    //    {
    //        get { return _welcome_pg; }
    //        set { _welcome_pg = value; }
    //    }

    //    private string _login_help = "";
    //    public string login_help
    //    {
    //        get { return _login_help; }
    //        set { _login_help = value; }
    //    }

    //    private string _logo_img = "";
    //    public string logo_img
    //    {
    //        get { return _logo_img; }
    //        set { _logo_img = value; }
    //    }

    //    private bool _active = true;
    //    public bool active
    //    {
    //        get { return _active; }
    //        set { _active = value; }
    //    }

    //    private string _support_email = "";
    //    public string support_email
    //    {
    //        get { return _support_email; }
    //        set { _support_email = value; }
    //    }

    //    private string _feedback_email = "";
    //    public string feedback_email
    //    {
    //        get { return _feedback_email; }
    //        set { _feedback_email = value; }
    //    }

    //    private string _default_password = "";
    //    public string default_password
    //    {
    //        get { return _default_password; }
    //        set { _default_password = value; }
    //    }

    //    public FacilityGroup(int FacID, string FacCode, string FacName, int MaxUsers, DateTime Started,
    //        DateTime Expires, int RepID, string Comment, bool Compliance, bool CustomTopics, bool QuizBowl, 
    //       string homeurl, string tagline, string address1, string address2, string city, string state,
    //       string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
    //       string contact_email, int parent_id, string welcome_pg, string login_help, string logo_img,
    //       bool active, string support_email, string feedback_email, string default_password)
    //  {
    //      this.FacID = FacID;
    //      this.FacCode = FacCode;
    //      this.FacName = FacName;
    //      this.MaxUsers = MaxUsers;
    //      this.Started = Started;
    //      this.Expires = Expires;
    //      this.RepID = RepID;
    //      this.Comment = Comment;
    //      this.Compliance = Compliance;
    //      this.CustomTopics = CustomTopics;
    //      this.QuizBowl = QuizBowl;
    //      this.homeurl = homeurl;
    //      this.tagline = tagline;
    //      this.address1 = address1;
    //      this.address2 = address2;
    //      this.city = city;
    //      this.state = state;
    //      this.zipcode = zipcode;
    //      this.phone = phone;
    //      this.contact_name = contact_name;
    //      this.contact_phone = contact_phone;
    //      this.contact_ext = contact_ext;
    //      this.contact_email = contact_email;
    //      this.parent_id = parent_id;
    //      this.welcome_pg = welcome_pg;
    //      this.login_help = login_help;
    //      this.logo_img = logo_img;
    //      this.active = active;
    //      this.support_email = support_email;
    //      this.feedback_email = feedback_email;
    //      this.default_password = default_password;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = FacilityGroup.DeleteFacilityGroup(this.FacID);
    //        if (success)
    //            this.FacID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return FacilityGroup.UpdateFacilityGroup(this.FacID, this.FacCode, this.FacName, this.MaxUsers, 
    //            this.Started, this.Expires, this.RepID, this.Comment, this.Compliance, this.CustomTopics, 
    //            this.QuizBowl, 
    //       this.homeurl, this.tagline, this.address1, this.address2, this.city, this.state,
    //       this.zipcode, this.phone, this.contact_name, this.contact_phone, this.contact_ext,
    //       this.contact_email, this.parent_id, this.welcome_pg, this.login_help, this.logo_img,
    //       this.active, this.support_email, this.feedback_email, this.default_password);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all FacilityGroups
    //    /// </summary>
    //    public static List<FacilityGroup> GetFacilityGroups(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "FacName";

    //        List<FacilityGroup> FacilityGroups = null;
    //        string key = "FacilityGroups_FacilityGroups_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FacilityGroups = (List<FacilityGroup>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<FacilityGroupInfo> recordset = SiteProvider.PR.GetFacilityGroups(cSortExpression);
    //            FacilityGroups = GetFacilityGroupListFromFacilityGroupInfoList(recordset);
    //            BasePR.CacheData(key, FacilityGroups);
    //        }
    //        return FacilityGroups;
    //    }


    //    /// <summary>
    //    /// Returns a collection with all FacilityGroups except giftcard users
    //    /// </summary>
    //    public static List<FacilityGroup> GetFacilityGroupsExceptGiftCard(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "FacName";

    //        List<FacilityGroup> FacilityGroups = null;
    //        string key = "FacilityGroupsExceptGiftCard_FacilityGroupsExceptGiftCard_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FacilityGroups = (List<FacilityGroup>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<FacilityGroupInfo> recordset = SiteProvider.PR.GetFacilityGroupsExceptGiftCard(cSortExpression);
    //            FacilityGroups = GetFacilityGroupListFromFacilityGroupInfoList(recordset);
    //            BasePR.CacheData(key, FacilityGroups);
    //        }
    //        return FacilityGroups;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all FacilityGroups related to the parent of a FacilityID
    //    /// </summary>
    //    public static List<FacilityGroup> GetFacilityGroupsByParentOfFacilityID(int FacilityID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "FacCode";

    //        List<FacilityGroup> FacilityGroups = null;
    //        string key = "FacilityGroups_FacilityGroupsByParentOfFacilityID_" + FacilityID.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FacilityGroups = (List<FacilityGroup>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<FacilityGroupInfo> recordset = SiteProvider.PR.GetFacilityGroupsByParentOfFacilityID(FacilityID, cSortExpression);
    //            FacilityGroups = GetFacilityGroupListFromFacilityGroupInfoList(recordset);
    //            BasePR.CacheData(key, FacilityGroups);
    //        }
    //        return FacilityGroups;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all FacilityGroups related to the parent of a FacilityID
    //    /// </summary>
    //    public static List<FacilityGroup> GetFacilityGroupsByParentID(int ParentID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "FacName";

    //        List<FacilityGroup> FacilityGroups = null;
    //        string key = "FacilityGroups_FacilityGroupsByParentID_" + ParentID.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FacilityGroups = (List<FacilityGroup>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<FacilityGroupInfo> recordset = SiteProvider.PR.GetFacilityGroupsByParentID(ParentID, cSortExpression);
    //            FacilityGroups = GetFacilityGroupListFromFacilityGroupInfoList(recordset);
    //            BasePR.CacheData(key, FacilityGroups);
    //        }
    //        return FacilityGroups;
    //    }


    //    /// <summary>
    //    /// Returns the number of total FacilityGroups
    //    /// </summary>
    //    public static int GetFacilityGroupCount()
    //    {
    //        int FacilityGroupCount = 0;
    //        string key = "FacilityGroups_FacilityGroupCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FacilityGroupCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            FacilityGroupCount = SiteProvider.PR.GetFacilityGroupCount();
    //            BasePR.CacheData(key, FacilityGroupCount);
    //        }
    //        return FacilityGroupCount;
    //    }

    //    /// <summary>
    //    /// Returns a FacilityGroup object with the specified ID
    //    /// </summary>
    //    public static FacilityGroup GetFacilityGroupByID(int FacID)
    //    {
    //        FacilityGroup FacilityGroup = null;
    //        string key = "FacilityGroups_FacilityGroup_" + FacID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FacilityGroup = (FacilityGroup)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            FacilityGroup = GetFacilityGroupFromFacilityGroupInfo(SiteProvider.PR.GetFacilityGroupByID(FacID));
    //            BasePR.CacheData(key, FacilityGroup);
    //        }
    //        return FacilityGroup;
    //    }

    //    /// <summary>
    //    /// Returns a FacilityGroup object with the specified Facility code
    //    /// </summary>
    //    public static FacilityGroup GetFacilityGroupByFacilityCode(string FacCode)
    //    {
    //        FacilityGroup FacilityGroup = null;
    //        string key = "FacilityGroups_FacilityGroup_FacCode_" + FacCode.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FacilityGroup = (FacilityGroup)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            FacilityGroup = GetFacilityGroupFromFacilityGroupInfo(SiteProvider.PR.GetFacilityGroupByFacilityCode(FacCode));
    //            BasePR.CacheData(key, FacilityGroup);
    //        }
    //        return FacilityGroup;
    //    }

    //    /// <summary>
    //    /// Returns a FacilityGroup object with the specified Facility Name
    //    /// </summary>

    //    public static FacilityGroup GetFacilityGroupByFacName(string FacName)
    //    {
    //        FacilityGroup FacilityGroup = null;
    //        string key = "FacilityGroups_FacilityGroup_FacName_" + FacName.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FacilityGroup = (FacilityGroup)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            FacilityGroup = GetFacilityGroupFromFacilityGroupInfo(SiteProvider.PR.GetFacilityGroupByFacName(FacName));
    //            BasePR.CacheData(key, FacilityGroup);
    //        }
    //        return FacilityGroup;
    //    }

    //    /// <summary>
    //    /// Updates an existing FacilityGroup
    //    /// </summary>
    //    public static bool UpdateFacilityGroup(int FacID, string FacCode, string FacName, int MaxUsers, DateTime Started,
    //        DateTime Expires, int RepID, string Comment, bool Compliance, bool CustomTopics, bool QuizBowl,
    //       string homeurl, string tagline, string address1, string address2, string city, string state,
    //       string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
    //       string contact_email, int parent_id, string welcome_pg, string login_help, string logo_img,
    //       bool active, string support_email, string feedback_email, string default_password)
    //    {
    //        FacCode = BizObject.ConvertNullToEmptyString(FacCode);
    //        FacName = BizObject.ConvertNullToEmptyString(FacName);
    //        Comment = BizObject.ConvertNullToEmptyString(Comment);

    //        FacilityGroupInfo record = new FacilityGroupInfo(FacID, FacCode, FacName, MaxUsers, Started,
    //            Expires, RepID, Comment, Compliance, CustomTopics, QuizBowl, 
    //           homeurl, tagline, address1, address2, city, state,
    //           zipcode, phone, contact_name, contact_phone, contact_ext,
    //           contact_email, parent_id, welcome_pg, login_help, logo_img,
    //           active, support_email, feedback_email, default_password);
    //        bool ret = SiteProvider.PR.UpdateFacilityGroup(record);

    //        BizObject.PurgeCacheItems("FacilityGroups_FacilityGroup_" + FacID.ToString());
    //        BizObject.PurgeCacheItems("FacilityGroups_FacilityGroups");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new FacilityGroup
    //    /// </summary>
    //    public static int InsertFacilityGroup(string FacCode, string FacName, int MaxUsers, DateTime Started,
    //        DateTime Expires, int RepID, string Comment, bool Compliance, bool CustomTopics, bool QuizBowl,
    //       string homeurl, string tagline, string address1, string address2, string city, string state,
    //       string zipcode, string phone, string contact_name, string contact_phone, string contact_ext,
    //       string contact_email, int parent_id, string welcome_pg, string login_help, string logo_img,
    //       bool active, string support_email, string feedback_email, string default_password)
    //    {
    //        FacCode = BizObject.ConvertNullToEmptyString(FacCode);
    //        FacName = BizObject.ConvertNullToEmptyString(FacName);
    //        Comment = BizObject.ConvertNullToEmptyString(Comment);

    //        FacilityGroupInfo record = new FacilityGroupInfo(0, FacCode, FacName, MaxUsers, Started,
    //            Expires, RepID, Comment, Compliance, CustomTopics, QuizBowl,
    //           homeurl, tagline, address1, address2, city, state,
    //           zipcode, phone, contact_name, contact_phone, contact_ext,
    //           contact_email, parent_id, welcome_pg, login_help, logo_img,
    //           active, support_email, feedback_email, default_password);
    //        int ret = SiteProvider.PR.InsertFacilityGroup(record);

    //        BizObject.PurgeCacheItems("FacilityGroups_FacilityGroup");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing FacilityGroup, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteFacilityGroup(int FacID)
    //    {
    //        bool IsOKToDelete = OKToDelete(FacID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteFacilityGroup(FacID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing FacilityGroup - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteFacilityGroup(int FacID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteFacilityGroup(FacID);
    //        //         new RecordDeletedEvent("FacilityGroup", FacID, null).Raise();
    //        BizObject.PurgeCacheItems("FacilityGroups_FacilityGroup");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a FacilityGroup can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int FacID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a FacilityGroup object filled with the data taken from the input FacilityGroupInfo
    //    /// </summary>
    //    private static FacilityGroup GetFacilityGroupFromFacilityGroupInfo(FacilityGroupInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new FacilityGroup(record.FacID, record.FacCode, record.FacName, record.MaxUsers,
    //            record.Started, record.Expires, record.RepID, record.Comment, record.Compliance,
    //            record.CustomTopics, record.QuizBowl,
    //       record.homeurl, record.tagline, record.address1, record.address2, record.city, record.state,
    //       record.zipcode, record.phone, record.contact_name, record.contact_phone, record.contact_ext,
    //       record.contact_email, record.parent_id, record.welcome_pg, record.login_help, record.logo_img,
    //       record.active, record.support_email, record.feedback_email, record.default_password);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of FacilityGroup objects filled with the data taken from the input list of FacilityGroupInfo
    //    /// </summary>
    //    private static List<FacilityGroup> GetFacilityGroupListFromFacilityGroupInfoList(List<FacilityGroupInfo> recordset)
    //    {
    //        List<FacilityGroup> FacilityGroups = new List<FacilityGroup>();
    //        foreach (FacilityGroupInfo record in recordset)
    //            FacilityGroups.Add(GetFacilityGroupFromFacilityGroupInfo(record));
    //        return FacilityGroups;
    //    }



    //}

    ////////////////////////////////////////////////////////////
    ///// <summary>
    ///// FieldOfInterest business object class
    ///// </summary>
    //public class FieldOfInterest : BasePR
    //{
    //    private int _InterestID = 0;
    //    public int InterestID
    //    {
    //        get { return _InterestID; }
    //        protected set { _InterestID = value; }
    //    }

    //    private string _IntDescr = "";
    //    public string IntDescr
    //    {
    //        get { return _IntDescr; }
    //        set { _IntDescr = value; }
    //    }



    //   public FieldOfInterest(int InterestID, string IntDescr)
    //  {
    //        this.InterestID = InterestID;
    //        this.IntDescr = IntDescr;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = FieldOfInterest.DeleteFieldOfInterest(this.InterestID);
    //        if (success)
    //            this.InterestID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return FieldOfInterest.UpdateFieldOfInterest(this.InterestID, this.IntDescr);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all FieldsOfInterest
    //    /// </summary>
    //    public static List<FieldOfInterest> GetFieldsOfInterest(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "IntDescr";

    //        List<FieldOfInterest> FieldsOfInterest = null;
    //        string key = "FieldsOfInterest_FieldsOfInterest_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FieldsOfInterest = (List<FieldOfInterest>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<FieldOfInterestInfo> recordset = SiteProvider.PR.GetFieldsOfInterest(cSortExpression);
    //            FieldsOfInterest = GetFieldOfInterestListFromFieldOfInterestInfoList(recordset);
    //            BasePR.CacheData(key, FieldsOfInterest);
    //        }
    //        return FieldsOfInterest;
    //    }


    //    /// <summary>
    //    /// Returns the number of total FieldsOfInterest
    //    /// </summary>
    //    public static int GetFieldOfInterestCount()
    //    {
    //        int FieldOfInterestCount = 0;
    //        string key = "FieldsOfInterest_FieldOfInterestCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FieldOfInterestCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            FieldOfInterestCount = SiteProvider.PR.GetFieldOfInterestCount();
    //            BasePR.CacheData(key, FieldOfInterestCount);
    //        }
    //        return FieldOfInterestCount;
    //    }

    //    /// <summary>
    //    /// Returns a FieldOfInterest object with the specified ID
    //    /// </summary>
    //    public static FieldOfInterest GetFieldOfInterestByID(int InterestID)
    //    {
    //        FieldOfInterest FieldOfInterest = null;
    //        string key = "FieldsOfInterest_FieldOfInterest_" + InterestID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FieldOfInterest = (FieldOfInterest)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            FieldOfInterest = GetFieldOfInterestFromFieldOfInterestInfo(SiteProvider.PR.GetFieldOfInterestByID(InterestID));
    //            BasePR.CacheData(key, FieldOfInterest);
    //        }
    //        return FieldOfInterest;
    //    }

    //    /// <summary>
    //    /// Updates an existing FieldOfInterest
    //    /// </summary>
    //    public static bool UpdateFieldOfInterest(int InterestID, string IntDescr)
    //    {
    //        IntDescr = BizObject.ConvertNullToEmptyString(IntDescr);


    //        FieldOfInterestInfo record = new FieldOfInterestInfo(InterestID, IntDescr);
    //        bool ret = SiteProvider.PR.UpdateFieldOfInterest(record);

    //        BizObject.PurgeCacheItems("FieldsOfInterest_FieldOfInterest_" + InterestID.ToString());
    //        BizObject.PurgeCacheItems("FieldsOfInterest_FieldsOfInterest");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new FieldOfInterest
    //    /// </summary>
    //    public static int InsertFieldOfInterest(string IntDescr)
    //    {
    //        IntDescr = BizObject.ConvertNullToEmptyString(IntDescr);


    //        FieldOfInterestInfo record = new FieldOfInterestInfo(0, IntDescr);
    //        int ret = SiteProvider.PR.InsertFieldOfInterest(record);

    //        BizObject.PurgeCacheItems("FieldsOfInterest_FieldOfInterest");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing FieldOfInterest, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteFieldOfInterest(int InterestID)
    //    {
    //        bool IsOKToDelete = OKToDelete(InterestID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteFieldOfInterest(InterestID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing FieldOfInterest - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteFieldOfInterest(int InterestID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteFieldOfInterest(InterestID);
    //        //         new RecordDeletedEvent("FieldOfInterest", InterestID, null).Raise();
    //        BizObject.PurgeCacheItems("FieldsOfInterest_FieldOfInterest");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a FieldOfInterest can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int InterestID)
    //    {
    //        return true;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all FieldsOfInterest assigned to a User
    //    /// </summary>
    //    public static List<FieldOfInterest> GetFieldsOfInterestByUserID(int UserID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "IntDescr";

    //        List<FieldOfInterest> FieldsOfInterest = null;
    //        string key = "FieldsOfInterest_FieldsOfInterest_UserID_" + UserID.ToString() + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            FieldsOfInterest = (List<FieldOfInterest>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<FieldOfInterestInfo> recordset = SiteProvider.PR.GetFieldsOfInterestByUserID(UserID, cSortExpression);
    //            FieldsOfInterest = GetFieldOfInterestListFromFieldOfInterestInfoList(recordset);
    //            BasePR.CacheData(key, FieldsOfInterest);
    //        }
    //        return FieldsOfInterest;
    //    }



    //    /// <summary>
    //    /// Returns a FieldOfInterest object filled with the data taken from the input FieldOfInterestInfo
    //    /// </summary>
    //    private static FieldOfInterest GetFieldOfInterestFromFieldOfInterestInfo(FieldOfInterestInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new FieldOfInterest(record.InterestID, record.IntDescr);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of FieldOfInterest objects filled with the data taken from the input list of FieldOfInterestInfo
    //    /// </summary>
    //    private static List<FieldOfInterest> GetFieldOfInterestListFromFieldOfInterestInfoList(List<FieldOfInterestInfo> recordset)
    //    {
    //        List<FieldOfInterest> FieldsOfInterest = new List<FieldOfInterest>();
    //        foreach (FieldOfInterestInfo record in recordset)
    //            FieldsOfInterest.Add(GetFieldOfInterestFromFieldOfInterestInfo(record));
    //        return FieldsOfInterest;
    //    }



    //}
    ////////////////////////////////////////////////////////////
    ///// <summary>
    ///// LectureAttendee business object class
    ///// </summary>
    //public class LectureAttendee : BasePR
    //{
    //    private int _LectAttID = 0;
    //    public int LectAttID
    //    {
    //        get { return _LectAttID; }
    //        protected set { _LectAttID = value; }
    //    }

    //    private int _LectEvtID = 0;
    //    public int LectEvtID
    //    {
    //        get { return _LectEvtID; }
    //        set { _LectEvtID = value; }
    //    }

    //    private int _UserID = 0;
    //    public int UserID
    //    {
    //        get { return _UserID; }
    //        set { _UserID = value; }
    //    }

    //    private string _UserName = "";
    //    public string UserName
    //    {
    //        get { return _UserName; }
    //        set { _UserName = value; }
    //    }

    //    private string _PW = "";
    //    public string PW
    //    {
    //        get { return _PW; }
    //        set { _PW = value; }
    //    }

    //    private string _FirstName = "";
    //    public string FirstName
    //    {
    //        get { return _FirstName; }
    //        set { _FirstName = value; }
    //    }

    //    private string _LastName = "";
    //    public string LastName
    //    {
    //        get { return _LastName; }
    //        set { _LastName = value; }
    //    }

    //    private string _MI = "";
    //    public string MI
    //    {
    //        get { return _MI; }
    //        set { _MI = value; }
    //    }

    //    private string _Address1 = "";
    //    public string Address1
    //    {
    //        get { return _Address1; }
    //        set { _Address1 = value; }
    //    }

    //    private string _Address2 = "";
    //    public string Address2
    //    {
    //        get { return _Address2; }
    //        set { _Address2 = value; }
    //    }

    //    private string _City = "";
    //    public string City
    //    {
    //        get { return _City; }
    //        set { _City = value; }
    //    }

    //    private string _State = "";
    //    public string State
    //    {
    //        get { return _State; }
    //        set { _State = value; }
    //    }

    //    private string _Zip = "";
    //    public string Zip
    //    {
    //        get { return _Zip; }
    //        set { _Zip = value; }
    //    }

    //    private string _Email = "";
    //    public string Email
    //    {
    //        get { return _Email; }
    //        set { _Email = value; }
    //    }

    //    private DateTime _DateAdded = System.DateTime.Now;
    //    public DateTime DateAdded
    //    {
    //        get { return _DateAdded; }
    //        set { _DateAdded = value; }
    //    }

    //    private bool _Completed = false;
    //    public bool Completed
    //    {
    //        get { return _Completed; }
    //        set { _Completed = value; }
    //    }


    //   public LectureAttendee(int LectAttID, int LectEvtID, int UserID, string UserName, string PW, string FirstName, string LastName, string MI, string Address1, string Address2, string City, string State, string Zip, string Email, DateTime DateAdded, bool Completed)
    //  {
    //        this.LectAttID = LectAttID;
    //        this.LectEvtID = LectEvtID;
    //        this.UserID = UserID;
    //        this.UserName = UserName;
    //        this.PW = PW;
    //        this.FirstName = FirstName;
    //        this.LastName = LastName;
    //        this.MI = MI;
    //        this.Address1 = Address1;
    //        this.Address2 = Address2;
    //        this.City = City;
    //        this.State = State;
    //        this.Zip = Zip;
    //        this.Email = Email;
    //        this.DateAdded = DateAdded;
    //        this.Completed = Completed;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = LectureAttendee.DeleteLectureAttendee(this.LectAttID);
    //        if (success)
    //            this.LectAttID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return LectureAttendee.UpdateLectureAttendee(this.LectAttID, this.LectEvtID, this.UserID, this.UserName, this.PW, this.FirstName, this.LastName, this.MI, this.Address1, this.Address2, this.City, this.State, this.Zip, this.Email, this.DateAdded, this.Completed);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all LectureAttendees
    //    /// </summary>
    //    public static List<LectureAttendee> GetLectureAttendees(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "DateAdded";

    //        List<LectureAttendee> LectureAttendees = null;
    //        string key = "LectureAttendees_LectureAttendees_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureAttendees = (List<LectureAttendee>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<LectureAttendeeInfo> recordset = SiteProvider.PR.GetLectureAttendees(cSortExpression);
    //            LectureAttendees = GetLectureAttendeeListFromLectureAttendeeInfoList(recordset);
    //            BasePR.CacheData(key, LectureAttendees);
    //        }
    //        return LectureAttendees;
    //    }


    //    /// <summary>
    //    /// Returns the number of total LectureAttendees
    //    /// </summary>
    //    public static int GetLectureAttendeeCount()
    //    {
    //        int LectureAttendeeCount = 0;
    //        string key = "LectureAttendees_LectureAttendeeCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureAttendeeCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            LectureAttendeeCount = SiteProvider.PR.GetLectureAttendeeCount();
    //            BasePR.CacheData(key, LectureAttendeeCount);
    //        }
    //        return LectureAttendeeCount;
    //    }

    //    /// <summary>
    //    /// Returns a LectureAttendee object with the specified ID
    //    /// </summary>
    //    public static LectureAttendee GetLectureAttendeeByID(int LectAttID)
    //    {
    //        LectureAttendee LectureAttendee = null;
    //        string key = "LectureAttendees_LectureAttendee_" + LectAttID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureAttendee = (LectureAttendee)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            LectureAttendee = GetLectureAttendeeFromLectureAttendeeInfo(SiteProvider.PR.GetLectureAttendeeByID(LectAttID));
    //            BasePR.CacheData(key, LectureAttendee);
    //        }
    //        return LectureAttendee;
    //    }

    //    /// <summary>
    //    /// Updates an existing LectureAttendee
    //    /// </summary>
    //    public static bool UpdateLectureAttendee(int LectAttID, int LectEvtID, int UserID, string UserName, string PW, string FirstName, string LastName, string MI, string Address1, string Address2, string City, string State, string Zip, string Email, DateTime DateAdded, bool Completed)
    //    {
    //        UserName = BizObject.ConvertNullToEmptyString(UserName);
    //        PW = BizObject.ConvertNullToEmptyString(PW);
    //        FirstName = BizObject.ConvertNullToEmptyString(FirstName);
    //        LastName = BizObject.ConvertNullToEmptyString(LastName);
    //        MI = BizObject.ConvertNullToEmptyString(MI);
    //        Address1 = BizObject.ConvertNullToEmptyString(Address1);
    //        Address2 = BizObject.ConvertNullToEmptyString(Address2);
    //        City = BizObject.ConvertNullToEmptyString(City);
    //        State = BizObject.ConvertNullToEmptyString(State);
    //        Zip = BizObject.ConvertNullToEmptyString(Zip);
    //        Email = BizObject.ConvertNullToEmptyString(Email);


    //        LectureAttendeeInfo record = new LectureAttendeeInfo(LectAttID, LectEvtID, UserID, UserName, PW, FirstName, LastName, MI, Address1, Address2, City, State, Zip, Email, DateAdded, Completed);
    //        bool ret = SiteProvider.PR.UpdateLectureAttendee(record);

    //        BizObject.PurgeCacheItems("LectureAttendees_LectureAttendee_" + LectAttID.ToString());
    //        BizObject.PurgeCacheItems("LectureAttendees_LectureAttendees");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new LectureAttendee
    //    /// </summary>
    //    public static int InsertLectureAttendee(int LectEvtID, int UserID, string UserName, string PW, string FirstName, string LastName, string MI, string Address1, string Address2, string City, string State, string Zip, string Email, DateTime DateAdded, bool Completed)
    //    {
    //        UserName = BizObject.ConvertNullToEmptyString(UserName);
    //        PW = BizObject.ConvertNullToEmptyString(PW);
    //        FirstName = BizObject.ConvertNullToEmptyString(FirstName);
    //        LastName = BizObject.ConvertNullToEmptyString(LastName);
    //        MI = BizObject.ConvertNullToEmptyString(MI);
    //        Address1 = BizObject.ConvertNullToEmptyString(Address1);
    //        Address2 = BizObject.ConvertNullToEmptyString(Address2);
    //        City = BizObject.ConvertNullToEmptyString(City);
    //        State = BizObject.ConvertNullToEmptyString(State);
    //        Zip = BizObject.ConvertNullToEmptyString(Zip);
    //        Email = BizObject.ConvertNullToEmptyString(Email);


    //        LectureAttendeeInfo record = new LectureAttendeeInfo(0, LectEvtID, UserID, UserName, PW, FirstName, LastName, MI, Address1, Address2, City, State, Zip, Email, DateAdded, Completed);
    //        int ret = SiteProvider.PR.InsertLectureAttendee(record);

    //        BizObject.PurgeCacheItems("LectureAttendees_LectureAttendee");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing LectureAttendee, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteLectureAttendee(int LectAttID)
    //    {
    //        bool IsOKToDelete = OKToDelete(LectAttID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteLectureAttendee(LectAttID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing LectureAttendee - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteLectureAttendee(int LectAttID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteLectureAttendee(LectAttID);
    //        //         new RecordDeletedEvent("LectureAttendee", LectAttID, null).Raise();
    //        BizObject.PurgeCacheItems("LectureAttendees_LectureAttendee");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a LectureAttendee can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int LectAttID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a LectureAttendee object filled with the data taken from the input LectureAttendeeInfo
    //    /// </summary>
    //    private static LectureAttendee GetLectureAttendeeFromLectureAttendeeInfo(LectureAttendeeInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new LectureAttendee(record.LectAttID, record.LectEvtID, record.UserID, record.UserName, record.PW, record.FirstName, record.LastName, record.MI, record.Address1, record.Address2, record.City, record.State, record.Zip, record.Email, record.DateAdded, record.Completed);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of LectureAttendee objects filled with the data taken from the input list of LectureAttendeeInfo
    //    /// </summary>
    //    private static List<LectureAttendee> GetLectureAttendeeListFromLectureAttendeeInfoList(List<LectureAttendeeInfo> recordset)
    //    {
    //        List<LectureAttendee> LectureAttendees = new List<LectureAttendee>();
    //        foreach (LectureAttendeeInfo record in recordset)
    //            LectureAttendees.Add(GetLectureAttendeeFromLectureAttendeeInfo(record));
    //        return LectureAttendees;
    //    }



    //}
    ////////////////////////////////////////////////////////////
    /// <summary>
    ///// LectureDefinition business object class
    ///// </summary>
    //public class LectureDefinition : BasePR
    //{
    //    private int _LectureID = 0;
    //    public int LectureID
    //    {
    //        get { return _LectureID; }
    //        protected set { _LectureID = value; }
    //    }

    //    private string _LectTitle = "";
    //    public string LectTitle
    //    {
    //        get { return _LectTitle; }
    //        set { _LectTitle = value; }
    //    }

    //    private int _CertID = 0;
    //    public int CertID
    //    {
    //        get { return _CertID; }
    //        set { _CertID = value; }
    //    }

    //    private string _Comment = "";
    //    public string Comment
    //    {
    //        get { return _Comment; }
    //        set { _Comment = value; }
    //    }


    //   public LectureDefinition(int LectureID, string LectTitle, int CertID, string Comment)
    //  {
    //        this.LectureID = LectureID;
    //        this.LectTitle = LectTitle;
    //        this.CertID = CertID;
    //        this.Comment = Comment;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = LectureDefinition.DeleteLectureDefinition(this.LectureID);
    //        if (success)
    //            this.LectureID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return LectureDefinition.UpdateLectureDefinition(this.LectureID, this.LectTitle, this.CertID, this.Comment);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all LectureDefinitions
    //    /// </summary>
    //    public static List<LectureDefinition> GetLectureDefinitions(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "LectTitle";

    //        List<LectureDefinition> LectureDefinitions = null;
    //        string key = "LectureDefinitions_LectureDefinitions_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureDefinitions = (List<LectureDefinition>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<LectureDefinitionInfo> recordset = SiteProvider.PR.GetLectureDefinitions(cSortExpression);
    //            LectureDefinitions = GetLectureDefinitionListFromLectureDefinitionInfoList(recordset);
    //            BasePR.CacheData(key, LectureDefinitions);
    //        }
    //        return LectureDefinitions;
    //    }


    //    /// <summary>
    //    /// Returns the number of total LectureDefinitions
    //    /// </summary>
    //    public static int GetLectureDefinitionCount()
    //    {
    //        int LectureDefinitionCount = 0;
    //        string key = "LectureDefinitions_LectureDefinitionCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureDefinitionCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            LectureDefinitionCount = SiteProvider.PR.GetLectureDefinitionCount();
    //            BasePR.CacheData(key, LectureDefinitionCount);
    //        }
    //        return LectureDefinitionCount;
    //    }

    //    /// <summary>
    //    /// Returns a LectureDefinition object with the specified ID
    //    /// </summary>
    //    public static LectureDefinition GetLectureDefinitionByID(int LectureID)
    //    {
    //        LectureDefinition LectureDefinition = null;
    //        string key = "LectureDefinitions_LectureDefinition_" + LectureID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureDefinition = (LectureDefinition)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            LectureDefinition = GetLectureDefinitionFromLectureDefinitionInfo(SiteProvider.PR.GetLectureDefinitionByID(LectureID));
    //            BasePR.CacheData(key, LectureDefinition);
    //        }
    //        return LectureDefinition;
    //    }

    //    /// <summary>
    //    /// Updates an existing LectureDefinition
    //    /// </summary>
    //    public static bool UpdateLectureDefinition(int LectureID, string LectTitle, int CertID, string Comment)
    //    {
    //        LectTitle = BizObject.ConvertNullToEmptyString(LectTitle);
    //        Comment = BizObject.ConvertNullToEmptyString(Comment);


    //        LectureDefinitionInfo record = new LectureDefinitionInfo(LectureID, LectTitle, CertID, Comment);
    //        bool ret = SiteProvider.PR.UpdateLectureDefinition(record);

    //        BizObject.PurgeCacheItems("LectureDefinitions_LectureDefinition_" + LectureID.ToString());
    //        BizObject.PurgeCacheItems("LectureDefinitions_LectureDefinitions");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new LectureDefinition
    //    /// </summary>
    //    public static int InsertLectureDefinition(string LectTitle, int CertID, string Comment)
    //    {
    //        LectTitle = BizObject.ConvertNullToEmptyString(LectTitle);
    //        Comment = BizObject.ConvertNullToEmptyString(Comment);


    //        LectureDefinitionInfo record = new LectureDefinitionInfo(0, LectTitle, CertID, Comment);
    //        int ret = SiteProvider.PR.InsertLectureDefinition(record);

    //        BizObject.PurgeCacheItems("LectureDefinitions_LectureDefinition");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing LectureDefinition, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteLectureDefinition(int LectureID)
    //    {
    //        bool IsOKToDelete = OKToDelete(LectureID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteLectureDefinition(LectureID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing LectureDefinition - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteLectureDefinition(int LectureID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteLectureDefinition(LectureID);
    //        //         new RecordDeletedEvent("LectureDefinition", LectureID, null).Raise();
    //        BizObject.PurgeCacheItems("LectureDefinitions_LectureDefinition");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a LectureDefinition can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int LectureID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a LectureDefinition object filled with the data taken from the input LectureDefinitionInfo
    //    /// </summary>
    //    private static LectureDefinition GetLectureDefinitionFromLectureDefinitionInfo(LectureDefinitionInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new LectureDefinition(record.LectureID, record.LectTitle, record.CertID, record.Comment);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of LectureDefinition objects filled with the data taken from the input list of LectureDefinitionInfo
    //    /// </summary>
    //    private static List<LectureDefinition> GetLectureDefinitionListFromLectureDefinitionInfoList(List<LectureDefinitionInfo> recordset)
    //    {
    //        List<LectureDefinition> LectureDefinitions = new List<LectureDefinition>();
    //        foreach (LectureDefinitionInfo record in recordset)
    //            LectureDefinitions.Add(GetLectureDefinitionFromLectureDefinitionInfo(record));
    //        return LectureDefinitions;
    //    }



    //}
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// LectureEvent business object class
    /// </summary>
    //public class LectureEvent : BasePR
    //{
    //    private int _LectEvtID = 0;
    //    public int LectEvtID
    //    {
    //        get { return _LectEvtID; }
    //        protected set { _LectEvtID = value; }
    //    }

    //    private int _LectureID = 0;
    //    public int LectureID
    //    {
    //        get { return _LectureID; }
    //        set { _LectureID = value; }
    //    }

    //    private DateTime _StartDate = System.DateTime.Now;
    //    public DateTime StartDate
    //    {
    //        get { return _StartDate; }
    //        set { _StartDate = value; }
    //    }

    //    private int _Capacity = 0;
    //    public int Capacity
    //    {
    //        get { return _Capacity; }
    //        set { _Capacity = value; }
    //    }

    //    private int _Attendees = 0;
    //    public int Attendees
    //    {
    //        get { return _Attendees; }
    //        set { _Attendees = value; }
    //    }

    //    private string _Comment = "";
    //    public string Comment
    //    {
    //        get { return _Comment; }
    //        set { _Comment = value; }
    //    }

    //    private string _Facility = "";
    //    public string Facility
    //    {
    //        get { return _Facility; }
    //        set { _Facility = value; }
    //    }

    //    private string _City = "";
    //    public string City
    //    {
    //        get { return _City; }
    //        set { _City = value; }
    //    }

    //    private string _State = "";
    //    public string State
    //    {
    //        get { return _State; }
    //        set { _State = value; }
    //    }


    //   public LectureEvent(int LectEvtID, int LectureID, DateTime StartDate, int Capacity, int Attendees, string Comment,
    //       string Facility, string City, string State)
    //  {
    //        this.LectEvtID = LectEvtID;
    //        this.LectureID = LectureID;
    //        this.StartDate = StartDate;
    //        this.Capacity = Capacity;
    //        this.Attendees = Attendees;
    //        this.Comment = Comment;
    //        this.Facility = Facility;
    //        this.City = City;
    //        this.State = State;
    //    }

    //    public bool Delete()
    //    {
    //        bool success = LectureEvent.DeleteLectureEvent(this.LectEvtID);
    //        if (success)
    //            this.LectEvtID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return LectureEvent.UpdateLectureEvent(this.LectEvtID, this.LectureID, this.StartDate, this.Capacity, this.Attendees, this.Comment, 
    //            this.Facility, this.City, this.State);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all LectureEvents
    //    /// </summary>
    //    public static List<LectureEvent> GetLectureEvents(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "StartDate";

    //        List<LectureEvent> LectureEvents = null;
    //        string key = "LectureEvents_LectureEvents_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureEvents = (List<LectureEvent>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<LectureEventInfo> recordset = SiteProvider.PR.GetLectureEvents(cSortExpression);
    //            LectureEvents = GetLectureEventListFromLectureEventInfoList(recordset);
    //            BasePR.CacheData(key, LectureEvents);
    //        }
    //        return LectureEvents;
    //    }


    //    /// <summary>
    //    /// Returns the number of total LectureEvents
    //    /// </summary>
    //    public static int GetLectureEventCount()
    //    {
    //        int LectureEventCount = 0;
    //        string key = "LectureEvents_LectureEventCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureEventCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            LectureEventCount = SiteProvider.PR.GetLectureEventCount();
    //            BasePR.CacheData(key, LectureEventCount);
    //        }
    //        return LectureEventCount;
    //    }

    //    /// <summary>
    //    /// Returns a LectureEvent object with the specified ID
    //    /// </summary>
    //    public static LectureEvent GetLectureEventByID(int LectEvtID)
    //    {
    //        LectureEvent LectureEvent = null;
    //        string key = "LectureEvents_LectureEvent_" + LectEvtID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureEvent = (LectureEvent)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            LectureEvent = GetLectureEventFromLectureEventInfo(SiteProvider.PR.GetLectureEventByID(LectEvtID));
    //            BasePR.CacheData(key, LectureEvent);
    //        }
    //        return LectureEvent;
    //    }

    //    /// <summary>
    //    /// Updates an existing LectureEvent
    //    /// </summary>
    //    public static bool UpdateLectureEvent(int LectEvtID, int LectureID, DateTime StartDate, int Capacity, int Attendees, string Comment, 
    //        string Facility, string City, string State)
    //    {
    //        Comment = BizObject.ConvertNullToEmptyString(Comment);


    //        LectureEventInfo record = new LectureEventInfo(LectEvtID, LectureID, StartDate, Capacity, Attendees, Comment, 
    //            Facility, City, State);
    //        bool ret = SiteProvider.PR.UpdateLectureEvent(record);

    //        BizObject.PurgeCacheItems("LectureEvents_LectureEvent_" + LectEvtID.ToString());
    //        BizObject.PurgeCacheItems("LectureEvents_LectureEvents");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new LectureEvent
    //    /// </summary>
    //    public static int InsertLectureEvent(int LectureID, DateTime StartDate, int Capacity, int Attendees, string Comment, 
    //        string Facility, string City, string State)
    //    {
    //        Comment = BizObject.ConvertNullToEmptyString(Comment);


    //        LectureEventInfo record = new LectureEventInfo(0, LectureID, StartDate, Capacity, Attendees, Comment, Facility, City, State);
    //        int ret = SiteProvider.PR.InsertLectureEvent(record);

    //        BizObject.PurgeCacheItems("LectureEvents_LectureEvent");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing LectureEvent, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteLectureEvent(int LectEvtID)
    //    {
    //        bool IsOKToDelete = OKToDelete(LectEvtID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteLectureEvent(LectEvtID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing LectureEvent - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteLectureEvent(int LectEvtID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteLectureEvent(LectEvtID);
    //        //         new RecordDeletedEvent("LectureEvent", LectEvtID, null).Raise();
    //        BizObject.PurgeCacheItems("LectureEvents_LectureEvent");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a LectureEvent can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int LectEvtID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a LectureEvent object filled with the data taken from the input LectureEventInfo
    //    /// </summary>
    //    private static LectureEvent GetLectureEventFromLectureEventInfo(LectureEventInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new LectureEvent(record.LectEvtID, record.LectureID, record.StartDate, record.Capacity, record.Attendees, record.Comment, 
    //                record.Facility, record.City, record.State);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of LectureEvent objects filled with the data taken from the input list of LectureEventInfo
    //    /// </summary>
    //    private static List<LectureEvent> GetLectureEventListFromLectureEventInfoList(List<LectureEventInfo> recordset)
    //    {
    //        List<LectureEvent> LectureEvents = new List<LectureEvent>();
    //        foreach (LectureEventInfo record in recordset)
    //            LectureEvents.Add(GetLectureEventFromLectureEventInfo(record));
    //        return LectureEvents;
    //    }



    //}
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// LectureTopicLink business object class
    /// </summary>
    //public class LectureTopicLink : BasePR
    //{
    //    private int _LectTopID = 0;
    //    public int LectTopID
    //    {
    //        get { return _LectTopID; }
    //        protected set { _LectTopID = value; }
    //    }

    //    private int _LectureID = 0;
    //    public int LectureID
    //    {
    //        get { return _LectureID; }
    //        set { _LectureID = value; }
    //    }

    //    private int _TopicID = 0;
    //    public int TopicID
    //    {
    //        get { return _TopicID; }
    //        set { _TopicID = value; }
    //    }


    //   public LectureTopicLink(int LectTopID, int LectureID, int TopicID)
    //  {
    //        this.LectTopID = LectTopID;
    //        this.LectureID = LectureID;
    //        this.TopicID = TopicID;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = LectureTopicLink.DeleteLectureTopicLink(this.LectTopID);
    //        if (success)
    //            this.LectTopID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return LectureTopicLink.UpdateLectureTopicLink(this.LectTopID, this.LectureID, this.TopicID);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all LectureTopicLinks
    //    /// </summary>
    //    public static List<LectureTopicLink> GetLectureTopicLinks(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "LectureID";

    //        List<LectureTopicLink> LectureTopicLinks = null;
    //        string key = "LectureTopicLinks_LectureTopicLinks_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureTopicLinks = (List<LectureTopicLink>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<LectureTopicLinkInfo> recordset = SiteProvider.PR.GetLectureTopicLinks(cSortExpression);
    //            LectureTopicLinks = GetLectureTopicLinkListFromLectureTopicLinkInfoList(recordset);
    //            BasePR.CacheData(key, LectureTopicLinks);
    //        }
    //        return LectureTopicLinks;
    //    }

    //    /// <summary>
    //    /// Returns the number of total LectureTopicLinks
    //    /// </summary>
    //    public static int GetLectureTopicLinkCount()
    //    {
    //        int LectureTopicLinkCount = 0;
    //        string key = "LectureTopicLinks_LectureTopicLinkCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureTopicLinkCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            LectureTopicLinkCount = SiteProvider.PR.GetLectureTopicLinkCount();
    //            BasePR.CacheData(key, LectureTopicLinkCount);
    //        }
    //        return LectureTopicLinkCount;
    //    }

    //    /// <summary>
    //    /// Returns a LectureTopicLink object with the specified ID
    //    /// </summary>
    //    public static LectureTopicLink GetLectureTopicLinkByID(int LectTopID)
    //    {
    //        LectureTopicLink LectureTopicLink = null;
    //        string key = "LectureTopicLinks_LectureTopicLink_" + LectTopID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LectureTopicLink = (LectureTopicLink)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            LectureTopicLink = GetLectureTopicLinkFromLectureTopicLinkInfo(SiteProvider.PR.GetLectureTopicLinkByID(LectTopID));
    //            BasePR.CacheData(key, LectureTopicLink);
    //        }
    //        return LectureTopicLink;
    //    }

    //    /// <summary>
    //    /// Updates an existing LectureTopicLink
    //    /// </summary>
    //    public static bool UpdateLectureTopicLink(int LectTopID, int LectureID, int TopicID)
    //    {


    //        LectureTopicLinkInfo record = new LectureTopicLinkInfo(LectTopID, LectureID, TopicID);
    //        bool ret = SiteProvider.PR.UpdateLectureTopicLink(record);

    //        BizObject.PurgeCacheItems("LectureTopicLinks_LectureTopicLink_" + LectTopID.ToString());
    //        BizObject.PurgeCacheItems("LectureTopicLinks_LectureTopicLinks");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new LectureTopicLink
    //    /// </summary>
    //    public static int InsertLectureTopicLink(int LectureID, int TopicID)
    //    {


    //        LectureTopicLinkInfo record = new LectureTopicLinkInfo(0, LectureID, TopicID);
    //        int ret = SiteProvider.PR.InsertLectureTopicLink(record);

    //        BizObject.PurgeCacheItems("LectureTopicLinks_LectureTopicLink");
    //        return ret;
    //    }

    //    public static bool UpdateLectureTopicLinkAssignments(int LectureID, string TopicIDAssignments)
    //    {
    //        bool ret = SiteProvider.PR.UpdateLectureTopicLinkAssignments(LectureID, TopicIDAssignments);
    //        // TODO: release cache?
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing LectureTopicLink, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteLectureTopicLink(int LectTopID)
    //    {
    //        bool IsOKToDelete = OKToDelete(LectTopID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteLectureTopicLink(LectTopID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing LectureTopicLink - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteLectureTopicLink(int LectTopID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteLectureTopicLink(LectTopID);
    //        //         new RecordDeletedEvent("LectureTopicLink", LectTopID, null).Raise();
    //        BizObject.PurgeCacheItems("LectureTopicLinks_LectureTopicLink");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a LectureTopicLink can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int LectTopID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a LectureTopicLink object filled with the data taken from the input LectureTopicLinkInfo
    //    /// </summary>
    //    private static LectureTopicLink GetLectureTopicLinkFromLectureTopicLinkInfo(LectureTopicLinkInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new LectureTopicLink(record.LectTopID, record.LectureID, record.TopicID);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of LectureTopicLink objects filled with the data taken from the input list of LectureTopicLinkInfo
    //    /// </summary>
    //    private static List<LectureTopicLink> GetLectureTopicLinkListFromLectureTopicLinkInfoList(List<LectureTopicLinkInfo> recordset)
    //    {
    //        List<LectureTopicLink> LectureTopicLinks = new List<LectureTopicLink>();
    //        foreach (LectureTopicLinkInfo record in recordset)
    //            LectureTopicLinks.Add(GetLectureTopicLinkFromLectureTopicLinkInfo(record));
    //        return LectureTopicLinks;
    //    }

    //}

    ///////////////////////////////////////////////////////////////
    ///// <summary>
    /////  AreaType business object class
    /////  </summary>
    /////  
    //public class AreaType : BasePR
    //{
    //    private int _areaid = 0;
    //    public int areaid
    //    {
    //        get { return _areaid; }
    //        protected set { _areaid = value; }
    //    }

    //    private string _areaname = "";
    //    public string areaname
    //    {
    //        get { return _areaname; }
    //        set { _areaname = value; }

    //    }

    //    private string _tabname = "";
    //    public string tabname
    //    {
    //        get { return _tabname; }
    //        set { _tabname = value; }

    //    }

    //    public AreaType(int areaid,string areaname,string tabname)
    //    {
    //        this.areaid=areaid;
    //        this.areaname= areaname;
    //        this.tabname=tabname;
    //    }

    //    /***********************************
    //   * Static methods
    //   ************************************/

    //    /// <summary>
    //    /// Returns a collection with all AreaTypes
    //    /// </summary>
    //    public static List<AreaType> GetAreaTypes(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "areaname";

    //        List<AreaType> AreaTypes = null;
    //        string key = "AreaTypes_AreaTypes_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            AreaTypes = (List<AreaType>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<AreaTypeInfo> recordset = SiteProvider.PR.GetAreaTypes(cSortExpression);
    //           AreaTypes = GetAreaTypeListFromAreaTypeInfoList(recordset);
    //            BasePR.CacheData(key, AreaTypes);
    //        }
    //        return AreaTypes;
    //    }


    //    /// <summary>
    //    /// Returns a AreaType object filled with the data taken from the input AreaTypeInfo
    //    /// </summary>
    //    private static AreaType GetAreaTypeFromAreaTypeInfo(AreaTypeInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new AreaType(record.areaid,record.areaname,record.tabname);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of AreaType objects filled with the data taken from the input list of AreaTypeInfo
    //    /// </summary>
    //    private static List<AreaType> GetAreaTypeListFromAreaTypeInfoList(List<AreaTypeInfo> recordset)
    //    {
    //        List<AreaType> AreaTypes = new List<AreaType>();
    //        foreach (AreaTypeInfo record in recordset)
    //            AreaTypes.Add(GetAreaTypeFromAreaTypeInfo(record));
    //        return AreaTypes;
    //    }


    //}


    ////////////////////////////////////////////////////////////
    /// <summary>
    /// LicenseType business object class
    ///// </summary>
    //public class LicenseType : BasePR
    //{
    //    private int _License_Type_ID = 0;
    //    public int License_Type_ID
    //    {
    //        get { return _License_Type_ID; }
    //        protected set { _License_Type_ID = value; }
    //    }

    //    private string _Description = "";
    //    public string Description
    //    {
    //        get { return _Description; }
    //        set { _Description = value; }
    //    }



    //    public LicenseType(int License_Type_ID, string Description)
    //    {
    //        this.License_Type_ID = License_Type_ID;
    //        this.Description = Description;
    //    }

    //    public bool Delete()
    //    {
    //        bool success = LicenseType.DeleteLicenseType(this.License_Type_ID);
    //        if (success)
    //            this.License_Type_ID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return LicenseType.UpdateLicenseType(this.License_Type_ID, this.Description);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all LicenseTypes
    //    /// </summary>
    //    public static List<LicenseType> GetLicenseTypes(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "Description";

    //        List<LicenseType> LicenseTypes = null;
    //        string key = "LicenseTypes_LicenseTypes_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LicenseTypes = (List<LicenseType>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<LicenseTypeInfo> recordset = SiteProvider.PR.GetLicenseTypes(cSortExpression);
    //            LicenseTypes = GetLicenseTypeListFromLicenseTypeInfoList(recordset);
    //            BasePR.CacheData(key, LicenseTypes);
    //        }
    //        return LicenseTypes;
    //    }


    //    /// <summary>
    //    /// Returns the number of total LicenseTypes
    //    /// </summary>
    //    public static int GetLicenseTypeCount()
    //    {
    //        int LicenseTypeCount = 0;
    //        string key = "LicenseTypes_LicenseTypeCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LicenseTypeCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            LicenseTypeCount = SiteProvider.PR.GetLicenseTypeCount();
    //            BasePR.CacheData(key, LicenseTypeCount);
    //        }
    //        return LicenseTypeCount;
    //    }

    //    /// <summary>
    //    /// Returns a LicenseType object with the specified ID
    //    /// </summary>
    //    public static LicenseType GetLicenseTypeByID(int License_Type_ID)
    //    {
    //        LicenseType LicenseType = null;
    //        string key = "LicenseTypes_LicenseType_" + License_Type_ID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            LicenseType = (LicenseType)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            LicenseType = GetLicenseTypeFromLicenseTypeInfo(SiteProvider.PR.GetLicenseTypeByID(License_Type_ID));
    //            BasePR.CacheData(key, LicenseType);
    //        }
    //        return LicenseType;
    //    }

    //    /// <summary>
    //    /// Updates an existing LicenseType
    //    /// </summary>
    //    public static bool UpdateLicenseType(int License_Type_ID, string Description)
    //    {
    //        Description = BizObject.ConvertNullToEmptyString(Description);


    //        LicenseTypeInfo record = new LicenseTypeInfo(License_Type_ID, Description);
    //        bool ret = SiteProvider.PR.UpdateLicenseType(record);

    //        BizObject.PurgeCacheItems("LicenseTypes_LicenseType_" + License_Type_ID.ToString());
    //        BizObject.PurgeCacheItems("LicenseTypes_LicenseTypes");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new LicenseType
    //    /// </summary>
    //    public static int InsertLicenseType(string Description)
    //    {
    //        Description = BizObject.ConvertNullToEmptyString(Description);


    //        LicenseTypeInfo record = new LicenseTypeInfo(0, Description);
    //        int ret = SiteProvider.PR.InsertLicenseType(record);

    //        BizObject.PurgeCacheItems("LicenseTypes_LicenseTypes");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing LicenseType, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteLicenseType(int License_Type_ID)
    //    {
    //        bool IsOKToDelete = OKToDelete(License_Type_ID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteLicenseType(License_Type_ID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing LicenseType - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteLicenseType(int License_Type_ID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteLicenseType(License_Type_ID);
    //        //         new RecordDeletedEvent("LicenseType", License_Type_ID, null).Raise();
    //        BizObject.PurgeCacheItems("LicenseTypes_LicenseTypes");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a LicenseType can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int License_Type_ID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a LicenseType object filled with the data taken from the input LicenseTypeInfo
    //    /// </summary>
    //    private static LicenseType GetLicenseTypeFromLicenseTypeInfo(LicenseTypeInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new LicenseType(record.License_Type_ID, record.Description);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of LicenseType objects filled with the data taken from the input list of LicenseTypeInfo
    //    /// </summary>
    //    private static List<LicenseType> GetLicenseTypeListFromLicenseTypeInfoList(List<LicenseTypeInfo> recordset)
    //    {
    //        List<LicenseType> LicenseTypes = new List<LicenseType>();
    //        foreach (LicenseTypeInfo record in recordset)
    //            LicenseTypes.Add(GetLicenseTypeFromLicenseTypeInfo(record));
    //        return LicenseTypes;
    //    }

    //}

    ////////////////////////////////////////////////////////////
    /// <summary>
    /// TopicPage business object class
    /// </summary>
    //public class TopicPage : BasePR
    //{
    //    private int _PageID = 0;
    //    public int PageID
    //    {
    //        get { return _PageID; }
    //        protected set { _PageID = value; }
    //    }

    //    private int _TopicID = 0;
    //    public int TopicID
    //    {
    //        get { return _TopicID; }
    //        set { _TopicID = value; }
    //    }

    //    private int _Page_Num = 0;
    //    public int Page_Num
    //    {
    //        get { return _Page_Num; }
    //        set { _Page_Num = value; }
    //    }

    //    private string _Page_Content = "";
    //    public string Page_Content
    //    {
    //        get { return _Page_Content; }
    //        set { _Page_Content = value; }
    //    }



    //    public TopicPage(int PageID, int TopicID, int Page_Num, string Page_Content)
    //    {
    //        this.PageID = PageID;
    //        this.TopicID = TopicID;
    //        this.Page_Num = Page_Num;
    //        this.Page_Content = Page_Content;
    //    }

    //    public bool Delete()
    //    {
    //        bool success = TopicPage.DeleteTopicPage(this.PageID);
    //        if (success)
    //            this.PageID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return TopicPage.UpdateTopicPage(this.PageID, this.TopicID, this.Page_Num, this.Page_Content);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all TopicPages
    //    /// </summary>
    //    public static List<TopicPage> GetTopicPages(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<TopicPage> TopicPages = null;
    //        string key = "TopicPages_TopicPages_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicPages = (List<TopicPage>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicPageInfo> recordset = SiteProvider.PR.GetTopicPages(cSortExpression);
    //            TopicPages = GetTopicPageListFromTopicPageInfoList(recordset);
    //            BasePR.CacheData(key, TopicPages);
    //        }
    //        return TopicPages;
    //    }


    //    /// <summary>
    //    /// Returns the number of total TopicPages
    //    /// </summary>
    //    public static int GetTopicPageCount()
    //    {
    //        int TopicPageCount = 0;
    //        string key = "TopicPages_TopicPageCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicPageCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicPageCount = SiteProvider.PR.GetTopicPageCount();
    //            BasePR.CacheData(key, TopicPageCount);
    //        }
    //        return TopicPageCount;
    //    }


    //    /// <summary>
    //    /// Returns the number of total TopicPages for a TopicID
    //    /// </summary>
    //    public static int GetTopicPageCountByTopicID(int TopicID)
    //    {
    //        int TopicPageCount = 0;
    //        string key = "TopicPages_TopicPageCountByTopicID_" + TopicID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicPageCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicPageCount = SiteProvider.PR.GetTopicPageCountByTopicID(TopicID);
    //            BasePR.CacheData(key, TopicPageCount);
    //        }
    //        return TopicPageCount;
    //    }


    //    /// <summary>
    //    /// Returns a collection with all TopicPages for a TopicID
    //    /// </summary>
    //    public static List<TopicPage> GetTopicPagesByTopicID(int TopicID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<TopicPage> TopicPages = null;
    //        string key = "TopicPages_TopicPagesByTopicID_" + TopicID.ToString() + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicPages = (List<TopicPage>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicPageInfo> recordset = SiteProvider.PR.GetTopicPagesByTopicID(TopicID, cSortExpression);
    //            TopicPages = GetTopicPageListFromTopicPageInfoList(recordset);
    //            BasePR.CacheData(key, TopicPages);
    //        }
    //        return TopicPages;
    //    }


    //    /// <summary>
    //    /// Returns a TopicPage object with the specified ID
    //    /// </summary>
    //    public static TopicPage GetTopicPageByTopicIDAndPageNum(int TopicID, int Page_Num)
    //    {
    //        TopicPage TopicPage = null;
    //        string key = "TopicPages_TopicPageByTopicIDAndPageNum_" + TopicID.ToString() + "_" + Page_Num.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicPage = (TopicPage)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicPage = GetTopicPageFromTopicPageInfo(SiteProvider.PR.GetTopicPageByTopicIDAndPageNum(TopicID, Page_Num));
    //            BasePR.CacheData(key, TopicPage);
    //        }
    //        return TopicPage;
    //    }


    //    /// <summary>
    //    /// Returns a TopicPage object with the specified ID
    //    /// </summary>
    //    public static TopicPage GetTopicPageByID(int PageID)
    //    {
    //        TopicPage TopicPage = null;
    //        string key = "TopicPages_TopicPage_" + PageID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicPage = (TopicPage)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicPage = GetTopicPageFromTopicPageInfo(SiteProvider.PR.GetTopicPageByID(PageID));
    //            BasePR.CacheData(key, TopicPage);
    //        }
    //        return TopicPage;
    //    }

    //    /// <summary>
    //    /// Updates an existing TopicPage
    //    /// </summary>
    //    public static bool UpdateTopicPage(int PageID, int TopicID, int Page_Num, string Page_Content)
    //    {


    //        TopicPageInfo record = new TopicPageInfo(PageID, TopicID, Page_Num, Page_Content);
    //        bool ret = SiteProvider.PR.UpdateTopicPage(record);

    //        BizObject.PurgeCacheItems("TopicPages_TopicPage_" + PageID.ToString());
    //        BizObject.PurgeCacheItems("TopicPages_TopicPages");
    //        return ret;
    //    }


    //    /// <summary>
    //    /// Creates a new TopicPage
    //    /// </summary>
    //    public static int InsertTopicPage(int TopicID, int Page_Num, string Page_Content)
    //    {


    //        TopicPageInfo record = new TopicPageInfo(0, TopicID, Page_Num, Page_Content);
    //        int ret = SiteProvider.PR.InsertTopicPage(record);

    //        BizObject.PurgeCacheItems("TopicPages_TopicPage");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing TopicPage, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteTopicPage(int PageID)
    //    {
    //        bool IsOKToDelete = OKToDelete(PageID);
    //        if (IsOKToDelete)
    //        {
              

    //            TopicPage ObjTopicPage = TopicPage.GetTopicPageByID(PageID);
    //            List<TopicPage> objTopicPages = TopicPage.GetTopicPagesByTopicID(ObjTopicPage.TopicID,"page_num");
    //            if (objTopicPages.Count == ObjTopicPage.Page_Num)
    //            {
    //                return (bool)DeleteTopicPage(PageID, true);
    //            }
    //            else
    //            {
    //                for (int i = ObjTopicPage.Page_Num + 1; i <= objTopicPages.Count; i++)
    //                {
    //                   TopicPage.UpdateTopicPage(objTopicPages[i-1]._PageID,objTopicPages[i-1].TopicID,i-1,objTopicPages[i-1].Page_Content);
    //                }

    //                return (bool)DeleteTopicPage(PageID, true);
                   
    //            }
                
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing TopicPage - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteTopicPage(int PageID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteTopicPage(PageID);
    //        //         new RecordDeletedEvent("TopicPage", PageID, null).Raise();
    //        BizObject.PurgeCacheItems("TopicPages_TopicPage");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a TopicPage can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int PageID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a TopicPage object filled with the data taken from the input TopicPageInfo
    //    /// </summary>
    //    private static TopicPage GetTopicPageFromTopicPageInfo(TopicPageInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new TopicPage(record.PageID, record.TopicID, record.Page_Num, record.Page_Content);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of TopicPage objects filled with the data taken from the input list of TopicPageInfo
    //    /// </summary>
    //    private static List<TopicPage> GetTopicPageListFromTopicPageInfoList(List<TopicPageInfo> recordset)
    //    {
    //        List<TopicPage> TopicPage = new List<TopicPage>();
    //        foreach (TopicPageInfo record in recordset)
    //            TopicPage.Add(GetTopicPageFromTopicPageInfo(record));
    //        return TopicPage;
    //    }



    //}

    
    
    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// Promotion business object class
    ///// </summary>
    //public class Promotion : BasePR
    //{
    //    private int _ID = 0;
    //    public int ID
    //    {
    //        get { return _ID; }
    //        protected set { _ID = value; }
    //    }

    //    private string _cCode = "";
    //    public string cCode
    //    {
    //        get { return _cCode; }
    //        set { _cCode = value; }
    //    }

    //    private string _PromoType = "";
    //    public string PromoType
    //    {
    //        get { return _PromoType; }
    //        set { _PromoType = value; }
    //    }

    //    private string _PromoName = "";
    //    public string PromoName
    //    {
    //        get { return _PromoName; }
    //        set { _PromoName = value; }
    //    }

    //    private bool _lActive = false;
    //    public bool lActive
    //    {
    //        get { return _lActive; }
    //        set { _lActive = value; }
    //    }

    //    private bool _lOneTime = false;
    //    public bool lOneTime
    //    {
    //        get { return _lOneTime; }
    //        set { _lOneTime = value; }
    //    }

    //    private decimal _nDollars = 0.00M;
    //    public decimal nDollars
    //    {
    //        get { return _nDollars; }
    //        set { _nDollars = value; }
    //    }

    //    private string _Notes = "";
    //    public string Notes
    //    {
    //        get { return _Notes; }
    //        set { _Notes = value; }
    //    }

    //    private decimal _nPercent = 0.00M;
    //    public decimal nPercent
    //    {
    //        get { return _nPercent; }
    //        set { _nPercent = value; }
    //    }

    //    private decimal _nUnits = 0.00M;
    //    public decimal nUnits
    //    {
    //        get { return _nUnits; }
    //        set { _nUnits = value; }
    //    }

    //    private DateTime _tExpires = System.DateTime.Now.AddYears(1);
    //    public DateTime tExpires
    //    {
    //        get { return _tExpires; }
    //        set { _tExpires = value; }
    //    }

    //    private DateTime _tStart = System.DateTime.Now;
    //    public DateTime tStart
    //    {
    //        get { return _tStart; }
    //        set { _tStart = value; }
    //    }

    //    private int _RepID = 0;
    //    public int RepID
    //    {
    //        get { return _RepID; }
    //        set { _RepID = value; }
    //    }


    //    public Promotion(int ID, string cCode, string PromoType, string PromoName, bool lActive,
    //       bool lOneTime, decimal nDollars, string Notes, decimal nPercent, decimal nUnits, DateTime tExpires,
    //       DateTime tStart, int RepID)
    //  {
    //      this.ID = ID;
    //      this.cCode = cCode;
    //      this.PromoType = PromoType;
    //      this.PromoName = PromoName;
    //      this.lActive = lActive;
    //      this.lOneTime = lOneTime;
    //      this.nDollars = nDollars;
    //      this.Notes = Notes;
    //      this.nPercent = nPercent;
    //      this.nUnits = nUnits;
    //      this.tExpires = tExpires;
    //      this.tStart = tStart;
    //      this.RepID = RepID;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = Promotion.DeletePromotion(this.ID);
    //        if (success)
    //            this.ID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return Promotion.UpdatePromotion(this.ID, this.cCode, this.PromoType, this.PromoName, 
    //            this.lActive, this.lOneTime, this.nDollars, this.Notes, this.nPercent, this.nUnits, 
    //            this.tExpires, this.tStart, this.RepID);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all Promotions
    //    /// </summary>
    //    public static List<Promotion> GetPromotions(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "cCode";

    //        List<Promotion> Promotions = null;
    //        string key = "Promotions_Promotions_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Promotions = (List<Promotion>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<PromotionInfo> recordset = SiteProvider.PR.GetPromotions(cSortExpression);
    //            Promotions = GetPromotionListFromPromotionInfoList(recordset);
    //            BasePR.CacheData(key, Promotions);
    //        }
    //        return Promotions;
    //    }


    //    /// <summary>
    //    /// Returns the number of total Promotions
    //    /// </summary>
    //    public static int GetPromotionCount()
    //    {
    //        int PromotionCount = 0;
    //        string key = "Promotions_PromotionCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            PromotionCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            PromotionCount = SiteProvider.PR.GetPromotionCount();
    //            BasePR.CacheData(key, PromotionCount);
    //        }
    //        return PromotionCount;
    //    }

    //    /// <summary>
    //    /// Returns a Promotion object with the specified ID
    //    /// </summary>
    //    public static Promotion GetPromotionByID(int ID)
    //    {
    //        Promotion Promotion = null;
    //        string key = "Promotions_Promotion_" + ID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Promotion = (Promotion)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            Promotion = GetPromotionFromPromotionInfo(SiteProvider.PR.GetPromotionByID(ID));
    //            BasePR.CacheData(key, Promotion);
    //        }
    //        return Promotion;
    //    }

    //    /// <summary>
    //    /// Updates an existing Promotion
    //    /// </summary>
    //    public static bool UpdatePromotion(int ID, string cCode, string PromoType, string PromoName, bool lActive,
    //       bool lOneTime, decimal nDollars, string Notes, decimal nPercent, decimal nUnits, DateTime tExpires,
    //       DateTime tStart, int RepID)
    //    {
    //        cCode = BizObject.ConvertNullToEmptyString(cCode);
    //        Notes = BizObject.ConvertNullToEmptyString(Notes);
    //        PromoType = BizObject.ConvertNullToEmptyString(PromoType);
    //        PromoName = BizObject.ConvertNullToEmptyString(PromoName);

    //        PromotionInfo record = new PromotionInfo(ID, cCode, PromoType, PromoName, lActive,
    //       lOneTime, nDollars, Notes, nPercent, nUnits, tExpires,
    //       tStart, RepID);
    //        bool ret = SiteProvider.PR.UpdatePromotion(record);

    //        BizObject.PurgeCacheItems("Promotions_Promotion_" + ID.ToString());
    //        BizObject.PurgeCacheItems("Promotions_Promotions");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new Promotion
    //    /// </summary>
    //    public static int InsertPromotion(string cCode, string PromoType, string PromoName, bool lActive,
    //       bool lOneTime, decimal nDollars, string Notes, decimal nPercent, decimal nUnits, DateTime tExpires,
    //       DateTime tStart, int RepID)
    //    {
    //        cCode = BizObject.ConvertNullToEmptyString(cCode);
    //        Notes = BizObject.ConvertNullToEmptyString(Notes);
    //        PromoType = BizObject.ConvertNullToEmptyString(PromoType);
    //        PromoName = BizObject.ConvertNullToEmptyString(PromoName);

    //        PromotionInfo record = new PromotionInfo(0, cCode, PromoType, PromoName, lActive,
    //       lOneTime, nDollars, Notes, nPercent, nUnits, tExpires,
    //       tStart, RepID);
    //        int ret = SiteProvider.PR.InsertPromotion(record);

    //        BizObject.PurgeCacheItems("Promotions_Promotion");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing Promotion, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeletePromotion(int ID)
    //    {
    //        bool IsOKToDelete = OKToDelete(ID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeletePromotion(ID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing Promotion - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeletePromotion(int ID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeletePromotion(ID);
    //        //         new RecordDeletedEvent("Promotion", ID, null).Raise();
    //        BizObject.PurgeCacheItems("Promotions_Promotion");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a Promotion can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int ID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a Promotion object filled with the data taken from the input PromotionInfo
    //    /// </summary>
    //    private static Promotion GetPromotionFromPromotionInfo(PromotionInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new Promotion(record.ID, record.cCode, record.PromoType, record.PromoName,
    //            record.lActive, record.lOneTime, record.nDollars, record.Notes, record.nPercent, record.nUnits,
    //            record.tExpires, record.tStart, record.RepID);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of Promotion objects filled with the data taken from the input list of PromotionInfo
    //    /// </summary>
    //    private static List<Promotion> GetPromotionListFromPromotionInfoList(List<PromotionInfo> recordset)
    //    {
    //        List<Promotion> Promotions = new List<Promotion>();
    //        foreach (PromotionInfo record in recordset)
    //            Promotions.Add(GetPromotionFromPromotionInfo(record));
    //        return Promotions;
    //    }



    //}
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// UserAccount business object class
    /// </summary>
    //public class UserAccount : BasePR
    //{
    //  private int _iID = 0;
    //  public int iID
    //  {
    //      get { return _iID; }
    //      protected set { _iID = value; }
    //  }

    //  private string _cAddress1 = "";
    //    public string cAddress1
    //    {
    //        get { return _cAddress1; }
    //        set { _cAddress1 = value; }
    //    }

    //    private string _cAddress2 = "";
    //    public string cAddress2
    //    {
    //        get { return _cAddress2; }
    //        set { _cAddress2 = value; }
    //    }

    //    private bool _lAdmin = false;
    //    public bool lAdmin
    //    {
    //        get { return _lAdmin; }
    //        set { _lAdmin = value; }
    //    }

    //    private bool _lPrimary = false;
    //    public bool lPrimary
    //    {
    //        get { return _lPrimary; }
    //        set { _lPrimary = value; }
    //    }

    //    private string _cBirthDate = "";
    //    public string cBirthDate
    //    {
    //        get { return _cBirthDate; }
    //        set { _cBirthDate = value; }
    //    }

    //    private string _cCity = "";
    //    public string cCity
    //    {
    //        get { return _cCity; }
    //        set { _cCity = value; }
    //    }

    //    private string _cEmail = "";
    //    public string cEmail
    //    {
    //        get { return _cEmail; }
    //        set { _cEmail = value; }
    //    }

    //    private string _cExpires = "";
    //    public string cExpires
    //    {
    //        get { return _cExpires; }
    //        set { _cExpires = value; }
    //    }

    //    private string _cFirstName = "";
    //    public string cFirstName
    //    {
    //        get { return _cFirstName; }
    //        set { _cFirstName = value; }
    //    }

    //    private string _cFloridaNo = "";
    //    public string cFloridaNo
    //    {
    //        get { return _cFloridaNo; }
    //        set { _cFloridaNo = value; }
    //    }

    //    private string _cInstitute = "";
    //    public string cInstitute
    //    {
    //        get { return _cInstitute; }
    //        set { _cInstitute = value; }
    //    }

    //    private string _cLastName = "";
    //    public string cLastName
    //    {
    //        get { return _cLastName; }
    //        set { _cLastName = value; }
    //    }

    //    private string _cLectDate = "";
    //    public string cLectDate
    //    {
    //        get { return _cLectDate; }
    //        set { _cLectDate = value; }
    //    }

    //    private string _cMiddle = "";
    //    public string cMiddle
    //    {
    //        get { return _cMiddle; }
    //        set { _cMiddle = value; }
    //    }

    //    private string _cPhone = "";
    //    public string cPhone
    //    {
    //        get { return _cPhone; }
    //        set { _cPhone = value; }
    //    }

    //    private string _cPromoCode = "";
    //    public string cPromoCode
    //    {
    //        get { return _cPromoCode; }
    //        set { _cPromoCode = value; }
    //    }

    //    private int _PromoID = 0;
    //    public int PromoID
    //    {
    //        get { return _PromoID; }
    //        set { _PromoID = value; }
    //    }

    //    private int _FacilityID = 0;
    //    public int FacilityID
    //    {
    //        get { return _FacilityID; }
    //        set { _FacilityID = value; }
    //    }

    //    private string _cPW = "";
    //    public string cPW
    //    {
    //        get { return _cPW.ToLower(); }
    //        set { _cPW = value.ToLower(); }
    //    }

    //    private string _cRefCode = "";
    //    public string cRefCode
    //    {
    //        get { return _cRefCode; }
    //        set { _cRefCode = value; }
    //    }

    //    private string _cRegType = "";
    //    public string cRegType
    //    {
    //        get { return _cRegType; }
    //        set { _cRegType = value; }
    //    }

    //    private string _cSocial = "";
    //    public string cSocial
    //    {
    //        get { return _cSocial; }
    //        set { _cSocial = value; }
    //    }

    //    private string _cState = "";
    //    public string cState
    //    {
    //        get { return _cState; }
    //        set { _cState = value; }
    //    }

    //    private string _cUserName = "";
    //    public string cUserName
    //    {
    //        get { return _cUserName.ToLower(); }
    //        set { _cUserName = value.ToLower(); }
    //    }

    //    private string _cZipCode = "";
    //    public string cZipCode
    //    {
    //        get { return _cZipCode; }
    //        set { _cZipCode = value; }
    //    }

    //    private int _SpecID = 0;
    //    public int SpecID
    //    {
    //        get { return _SpecID; }
    //        set { _SpecID = value; }
    //    }

    //    private string _LCUserName = "";
    //    public string LCUserName
    //    {
    //        get { return _LCUserName.ToLower(); }
    //        set { _LCUserName = value.ToLower(); }
    //    }

    //    private DateTime _LastAct = System.DateTime.Now;
    //    public DateTime LastAct
    //    {
    //        get { return _LastAct; }
    //        set { _LastAct = value; }
    //    }

    //    private string _PassFmt = "";
    //    public string PassFmt
    //    {
    //        get { return _PassFmt; }
    //        set { _PassFmt = value; }
    //    }

    //    private string _PassSalt = "";
    //    public string PassSalt
    //    {
    //        get { return _PassSalt; }
    //        set { _PassSalt = value; }
    //    }

    //    private string _MobilePIN = "";
    //    public string MobilePIN
    //    {
    //        get { return _MobilePIN; }
    //        set { _MobilePIN = value; }
    //    }

    //    private string _LCEmail = "";
    //    public string LCEmail
    //    {
    //        get { return _LCEmail.ToLower(); }
    //        set { _LCEmail = value.ToLower(); }
    //    }

    //    private string _PWQuest = "";
    //    public string PWQuest
    //    {
    //        get { return _PWQuest; }
    //        set { _PWQuest = value; }
    //    }

    //    private string _PWAns = "";
    //    public string PWAns
    //    {
    //        get { return _PWAns.ToLower(); }
    //        set { _PWAns = value.ToLower(); }
    //    }

    //    private bool _IsApproved = false;
    //    public bool IsApproved
    //    {
    //        get { return _IsApproved; }
    //        set { _IsApproved = value; }
    //    }

    //    private bool _IsOnline = false;
    //    public bool IsOnline
    //    {
    //        get { return _IsOnline; }
    //        set { _IsOnline = value; }
    //    }

    //    private bool _IsLocked = false;
    //    public bool IsLocked
    //    {
    //        get { return _IsLocked; }
    //        set { _IsLocked = value; }
    //    }

    //    private DateTime _LastLogin = System.DateTime.MinValue;
    //    public DateTime LastLogin
    //    {
    //        get { return _LastLogin; }
    //        set { _LastLogin = value; }
    //    }

    //    private DateTime _LastPWChg = System.DateTime.MinValue;
    //    public DateTime LastPWChg
    //    {
    //        get { return _LastPWChg; }
    //        set { _LastPWChg = value; }
    //    }

    //    private DateTime _LastLock = System.DateTime.MinValue;
    //    public DateTime LastLock
    //    {
    //        get { return _LastLock; }
    //        set { _LastLock = value; }
    //    }

    //    private int _XPWAtt = 0;
    //    public int XPWAtt
    //    {
    //        get { return _XPWAtt; }
    //        set { _XPWAtt = value; }
    //    }

    //    private DateTime _XPWAttSt = System.DateTime.MinValue;
    //    public DateTime XPWAttSt
    //    {
    //        get { return _XPWAttSt; }
    //        set { _XPWAttSt = value; }
    //    }

    //    private int _XPWAnsAtt = 0;
    //    public int XPWAnsAtt
    //    {
    //        get { return _XPWAnsAtt; }
    //        set { _XPWAnsAtt = value; }
    //    }

    //    private DateTime _XPWAnsSt = System.DateTime.MinValue;
    //    public DateTime XPWAnsSt
    //    {
    //        get { return _XPWAnsSt; }
    //        set { _XPWAnsSt = value; }
    //    }

    //    private string _Comment = "";
    //    public string Comment
    //    {
    //        get { return _Comment; }
    //        set { _Comment = value; }
    //    }

    //    private DateTime _Created = System.DateTime.Now;
    //    public DateTime Created
    //    {
    //        get { return _Created; }
    //        set { _Created = value; }
    //    }

    //    private int _AgeGroup = 0;
    //    public int AgeGroup
    //    {
    //        get { return _AgeGroup; }
    //        set { _AgeGroup = value; }
    //    }

    //    private int _RegTypeID = 0;
    //    public int RegTypeID
    //    {
    //        get { return _RegTypeID; }
    //        set { _RegTypeID = value; }
    //    }

    //    private int _RepID = 0;
    //    public int RepID
    //    {
    //        get { return _RepID; }
    //        set { _RepID = value; }
    //    }

    //    private string _Work_Phone = "";
    //    public string Work_Phone
    //    {
    //        get { return _Work_Phone; }
    //        set { _Work_Phone = value; }
    //    }
    //    private string _Badge_ID = "";
    //    public string Badge_ID
    //    {
    //        get { return _Badge_ID; }
    //        set { _Badge_ID = value; }
    //    }

    //    private DateTime _Hire_Date = System.DateTime.MinValue;
    //    public DateTime Hire_Date
    //    {
    //        get { return _Hire_Date; }
    //        set { _Hire_Date = value; }
    //    }

    //    private DateTime _Termin_Date = System.DateTime.MinValue;
    //    public DateTime Termin_Date
    //    {
    //        get { return _Termin_Date; }
    //        set { _Termin_Date = value; }
    //    }

    //    private int _Dept_ID = 0;
    //    public int Dept_ID
    //    {
    //        get { return _Dept_ID; }
    //        set { _Dept_ID = value; }
    //    }

    //    private int _Position_ID = 0;
    //    public int Position_ID
    //    {
    //        get { return _Position_ID; }
    //        set { _Position_ID = value; }
    //    }

    //    private int _Clinical_Ind = 0;
    //    public int Clinical_Ind
    //    {
    //        get { return _Clinical_Ind; }
    //        set { _Clinical_Ind = value; }
    //    }

    //    private string _Title = "";
    //    public string Title
    //    {
    //        get { return _Title; }
    //        set { _Title = value; }
    //    }

    //    private bool _Giftcard_Ind = false;
    //    public bool Giftcard_Ind
    //    {
    //        get { return _Giftcard_Ind; }
    //        set { _Giftcard_Ind = value; }
    //    }

    //    private decimal _Giftcard_Chour = 0;
    //    public decimal Giftcard_Chour
    //    {
    //        get { return _Giftcard_Chour; }
    //        set { _Giftcard_Chour = value; }
    //    }

    //    private decimal _Giftcard_Uhour = 0;
    //    public decimal Giftcard_Uhour
    //    {
    //        get { return _Giftcard_Uhour; }
    //        set { _Giftcard_Uhour = value; }
    //    }

    //    private int _UniqueID = 0;
    //    public int UniqueID
    //    {
    //        get { return _UniqueID; }
    //        set { _UniqueID = value; }
    //    }

    //    private bool _FirstLogin_Ind = false;
    //    public bool FirstLogin_Ind
    //    {
    //        get { return _FirstLogin_Ind; }
    //        set { _FirstLogin_Ind = value; }
    //    }

    //    public UserAccount(int iID, string cAddress1, string cAddress2, bool lAdmin, bool lPrimary, string cBirthDate,
    //       string cCity, string cEmail, string cExpires, string cFirstName, string cFloridaNo,
    //       string cInstitute, string cLastName, string cLectDate, string cMiddle, string cPhone,
    //       string cPromoCode, int PromoID, int FacilityID, string cPW, string cRefCode,
    //       string cRegType, string cSocial, string cState, string cUserName, string cZipCode,
    //       int SpecID, string LCUserName, DateTime LastAct, string PassFmt,
    //       string PassSalt, string MobilePIN, string LCEmail, string PWQuest, string PWAns,
    //       bool IsApproved, bool IsOnline, bool IsLocked, DateTime LastLogin, DateTime LastPWChg,
    //       DateTime LastLock, int XPWAtt, DateTime XPWAttSt, int XPWAnsAtt, DateTime XPWAnsSt,
    //       string Comment, DateTime Created, int AgeGroup, int RegTypeID, int RepID, string Work_Phone, string Badge_ID,
    //       DateTime Hire_Date, DateTime Termin_Date, int Dept_ID, int Position_ID, int Clinical_Ind, string Title, bool Giftcard_Ind,
    //       decimal Giftcard_Chour, decimal Giftcard_Uhour, int UniqueID, bool FirstLogin_Ind)
    //  {
    //      this.iID = iID;
    //      this.cAddress1 = cAddress1;
    //      this.cAddress2 = cAddress2;
    //      this.lAdmin = lAdmin;
    //      this.lPrimary = lPrimary;
    //      this.cBirthDate = cBirthDate;
    //      this.cCity = cCity;
    //      this.cEmail = cEmail;
    //      this.cExpires = cExpires;
    //      this.cFirstName = cFirstName;
    //      this.cFloridaNo = cFloridaNo;
    //      this.cInstitute = cInstitute;
    //      this.cLastName = cLastName;
    //      this.cLectDate = cLectDate;
    //      this.cMiddle = cMiddle;
    //      this.cPhone = cPhone;
    //      this.cPromoCode = cPromoCode;
    //      this.PromoID = PromoID;
    //      this.FacilityID = FacilityID;
    //      this.cPW = cPW.ToLower();
    //      this.cRefCode = cRefCode;
    //      this.cRegType = cRegType;
    //      this.cSocial = cSocial;
    //      this.cState = cState;
    //      this.cUserName = cUserName.ToLower();
    //      this.cZipCode = cZipCode;
    //      this.SpecID = SpecID;
    //      this.LCUserName = LCUserName.ToLower();
    //      this.LastAct = LastAct;
    //      this.PassFmt = PassFmt;
    //      this.PassSalt = PassSalt;
    //      this.MobilePIN = MobilePIN;
    //      this.LCEmail = LCEmail;
    //      this.PWQuest = PWQuest;
    //      this.PWAns = PWAns.ToLower();
    //      this.IsApproved = IsApproved;
    //      this.IsOnline = IsOnline;
    //      this.IsLocked = IsLocked;
    //      this.LastLogin = LastLogin;
    //      this.LastPWChg = LastPWChg;
    //      this.LastLock = LastLock;
    //      this.XPWAtt = XPWAtt;
    //      this.XPWAttSt = XPWAttSt;
    //      this.XPWAnsAtt = XPWAnsAtt;
    //      this.XPWAnsSt = XPWAnsSt;
    //      this.Comment = Comment;
    //      this.Created = Created;
    //      this.AgeGroup = AgeGroup;
    //      this.RegTypeID = RegTypeID;
    //      this.RepID = RepID;
    //      this.Work_Phone = Work_Phone;
    //      this.Badge_ID = Badge_ID;
    //      this.Hire_Date = Hire_Date;
    //      this.Termin_Date = Termin_Date;
    //      this.Dept_ID = Dept_ID;
    //      this.Position_ID = Position_ID;
    //      this.Clinical_Ind = Clinical_Ind;
    //      this.Title = Title;
    //      this.Giftcard_Ind = Giftcard_Ind;
    //      this.Giftcard_Chour = Giftcard_Chour;
    //      this.Giftcard_Uhour = Giftcard_Uhour;
    //      this.UniqueID = UniqueID;
    //      this.FirstLogin_Ind = FirstLogin_Ind;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = UserAccount.DeleteUserAccount(this.iID);
    //        if (success)
    //            this.iID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return UserAccount.UpdateUserAccount(this.iID, this.cAddress1, this.cAddress2, this.lAdmin,
    //                this.lPrimary, this.cBirthDate,
    //      this.cCity, this.cEmail, this.cExpires, this.cFirstName, this.cFloridaNo,
    //      this.cInstitute, this.cLastName, this.cLectDate, this.cMiddle, this.cPhone,
    //      this.cPromoCode, this.PromoID, this.FacilityID, this.cPW, this.cRefCode,
    //      this.cRegType, this.cSocial, this.cState, this.cUserName, this.cZipCode,
    //      this.SpecID, this.LCUserName, this.LastAct, this.PassFmt,
    //      this.PassSalt, this.MobilePIN, this.LCEmail, this.PWQuest, this.PWAns,
    //      this.IsApproved, this.IsOnline, this.IsLocked, this.LastLogin, this.LastPWChg,
    //      this.LastLock, this.XPWAtt, this.XPWAttSt, this.XPWAnsAtt, this.XPWAnsSt,
    //      this.Comment, this.Created, this.AgeGroup, this.RegTypeID, this.RepID,
    //      this.Work_Phone, this.Badge_ID, this.Hire_Date,
    //      this.Termin_Date, this.Dept_ID, this.Position_ID,
    //      this.Clinical_Ind, this.Title, this.Giftcard_Ind,
    //      this.Giftcard_Chour, this.Giftcard_Uhour,
    //      this.UniqueID, this.FirstLogin_Ind);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all UserAccounts
    //    /// </summary>
    //    public static List<UserAccount> GetUserAccounts(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "cUserName";

    //        List<UserAccount> UserAccounts = null;
    //        string key = "UserAccounts_UserAccounts_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserAccounts = (List<UserAccount>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<UserAccountInfo> recordset = SiteProvider.PR.GetUserAccounts(cSortExpression);
    //            UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
    //            BasePR.CacheData(key, UserAccounts);
    //        }
    //        return UserAccounts;
    //    }


    //    /// <summary>
    //    /// Returns a collection with most recent UserAccounts
    //    /// </summary>
    //    public static List<UserAccount> GetRecentUserAccounts()
    //    {
    //        List<UserAccount> UserAccounts = null;
    //        string key = "UserAccounts_RecentUserAccounts" ;

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserAccounts = (List<UserAccount>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<UserAccountInfo> recordset = SiteProvider.PR.GetRecentUserAccounts();
    //            UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
    //            BasePR.CacheData(key, UserAccounts);
    //        }
    //        return UserAccounts;
    //    }

    //    /// <summary>
    //    /// Returns a collection with most recent UserAccounts
    //    /// </summary>
    //    public static int GetUserAccountByUniqueid(string uniqueid,string Password)
    //    {
    //        int userid = 0;
    //        string key = "UserAccounts_RecentUserAccountsByUniqueid";

    //        userid = SiteProvider.PR.GetUserAccountByUniqueid(uniqueid, Password);


    //            return userid;
    //    }

    //    /// <summary>
    //    /// Returns the number of total UserAccounts
    //    /// </summary>
    //    public static int GetUserAccountCount()
    //    {
    //        int UserAccountCount = 0;
    //        string key = "UserAccounts_UserAccountCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserAccountCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UserAccountCount = SiteProvider.PR.GetUserAccountCount();
    //            BasePR.CacheData(key, UserAccountCount);
    //        }
    //        return UserAccountCount;
    //    }

    //    /// <summary>
    //    /// Returns the Integer ID (primary key) of a user by username lookup
    //    /// </summary>
    //    public static int GetUserIDByUserName(string UserName)
    //    {
    //        int UserID = 0;
    //        string key = "UserID_UserName_" + UserName.Trim();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserID = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UserID = SiteProvider.PR.GetUserIDByUserName(UserName);
    //            BasePR.CacheData(key, UserID);
    //        }
    //        return UserID;
    //    }

    //    /// <summary>
    //    /// Returns the Integer ID (primary key) of a user by username lookup and FacilityID
    //    /// </summary>
    //    public static int GetUserIDByShortUserNameAndFacilityID(string ShortUserName, int FacilityID)
    //    {
    //        int UserID = 0;
    //        string key = "UserID_ShortUserName_" + ShortUserName.Trim() + "_FacilityID_" + FacilityID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserID = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UserID = SiteProvider.PR.GetUserIDByShortUserNameAndFacilityID(ShortUserName, FacilityID);
    //            BasePR.CacheData(key, UserID);
    //        }
    //        return UserID;
    //    }

    //    /// <summary>
    //    /// Returns a UserAccount object with the specified ID
    //    /// </summary>
    //    public static UserAccount GetUserAccountByID(int iID)
    //    {
    //        UserAccount UserAccount = null;
    //        string key = "UserAccounts_UserAccount_" + iID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserAccount = (UserAccount)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UserAccount = GetUserAccountFromUserAccountInfo(SiteProvider.PR.GetUserAccountByID(iID));
    //            BasePR.CacheData(key, UserAccount);
    //        }
    //        return UserAccount;
    //    }

    //    /// Returns a UserAccount object with the specified username and facilityid
    //    /// </summary>
    //    public static UserAccount GetUserAccountByShortUserNameAndFacilityID(string ShortUserName, int FacilityID)
    //    {
    //        UserAccount UserAccount = null;
    //        string key = "UserAccounts_UserAccount_ShortUserName_" + ShortUserName.Trim() + "_FacilityID_" + FacilityID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserAccount = (UserAccount)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UserAccount = GetUserAccountFromUserAccountInfo(SiteProvider.PR.GetUserAccountByShortUserNameAndFacilityID(ShortUserName, FacilityID));
    //            BasePR.CacheData(key, UserAccount);
    //        }
    //        return UserAccount;
    //    }

    //    public static bool UpdateUserAccountDeptByUserID(int userID, int deptId)
    //    {
    //        return SiteProvider.PR.UpdateUserAccountDeptByuserID(userID, deptId);
        
    //    }

    //    /// <summary>
    //    /// Updates an existing UserAccount
    //    /// </summary>
    //    public static bool UpdateUserAccount(int iID, string cAddress1, string cAddress2, bool lAdmin, bool lPrimary, string cBirthDate,
    //      string cCity, string cEmail, string cExpires, string cFirstName, string cFloridaNo,
    //      string cInstitute, string cLastName, string cLectDate, string cMiddle, string cPhone,
    //      string cPromoCode, int PromoID, int FacilityID, string cPW, string cRefCode,
    //      string cRegType, string cSocial, string cState, string cUserName, string cZipCode,
    //      int SpecID, string LCUserName, DateTime LastAct, string PassFmt,
    //      string PassSalt, string MobilePIN, string LCEmail, string PWQuest, string PWAns,
    //      bool IsApproved, bool IsOnline, bool IsLocked, DateTime LastLogin, DateTime LastPWChg,
    //      DateTime LastLock, int XPWAtt, DateTime XPWAttSt, int XPWAnsAtt, DateTime XPWAnsSt,
    //      string Comment, DateTime Created, int AgeGroup, int RegTypeID, int RepID, string Work_Phone, string Badge_ID,
    //       DateTime Hire_Date, DateTime Termin_Date, int Dept_ID, int Position_ID, int Clinical_Ind, string Title, bool Giftcard_Ind,
    //       decimal Giftcard_Chour, decimal Giftcard_Uhour, int UniqueID, bool FirstLogin_Ind)
    //    {
    //        cAddress1 = BizObject.ConvertNullToEmptyString(cAddress1);
    //        cAddress2 = BizObject.ConvertNullToEmptyString(cAddress2);
    //        cBirthDate = BizObject.ConvertNullToEmptyString(cBirthDate);
    //        cCity = BizObject.ConvertNullToEmptyString(cCity);
    //        cEmail = BizObject.ConvertNullToEmptyString(cEmail);
    //        cExpires = BizObject.ConvertNullToEmptyString(cExpires);
    //        cFirstName = BizObject.ConvertNullToEmptyString(cFirstName);
    //        cFloridaNo = BizObject.ConvertNullToEmptyString(cFloridaNo);
    //        cInstitute = BizObject.ConvertNullToEmptyString(cInstitute);
    //        cLastName = BizObject.ConvertNullToEmptyString(cLastName);
    //        cLectDate = BizObject.ConvertNullToEmptyString(cLectDate);
    //        cMiddle = BizObject.ConvertNullToEmptyString(cMiddle);
    //        cPhone = BizObject.ConvertNullToEmptyString(cPhone);
    //        cPromoCode = BizObject.ConvertNullToEmptyString(cPromoCode);
    //        cPW = BizObject.ConvertNullToEmptyString(cPW);
    //        cRefCode = BizObject.ConvertNullToEmptyString(cRefCode);
    //        cRegType = BizObject.ConvertNullToEmptyString(cRegType);
    //        cSocial = BizObject.ConvertNullToEmptyString(cSocial);
    //        cState = BizObject.ConvertNullToEmptyString(cState);
    //        cUserName = BizObject.ConvertNullToEmptyString(cUserName);
    //        cZipCode = BizObject.ConvertNullToEmptyString(cZipCode);
    //        LCUserName = BizObject.ConvertNullToEmptyString(LCUserName);
    //        PassFmt = BizObject.ConvertNullToEmptyString(PassFmt);
    //        PassSalt = BizObject.ConvertNullToEmptyString(PassSalt);
    //        MobilePIN = BizObject.ConvertNullToEmptyString(MobilePIN);
    //        LCEmail = BizObject.ConvertNullToEmptyString(LCEmail);
    //        PWQuest = BizObject.ConvertNullToEmptyString(PWQuest);
    //        PWAns = BizObject.ConvertNullToEmptyString(PWAns);
    //        Comment = BizObject.ConvertNullToEmptyString(Comment);
    //        //UniqueID = BizObject.ConvertNullToEmptyString(UniqueID);

    //        UserAccountInfo record = new UserAccountInfo(iID, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate,
    //      cCity, cEmail, cExpires, cFirstName, cFloridaNo,
    //      cInstitute, cLastName, cLectDate, cMiddle, cPhone,
    //      cPromoCode, PromoID, FacilityID, cPW, cRefCode,
    //      cRegType, cSocial, cState, cUserName, cZipCode,
    //      SpecID, LCUserName, LastAct, PassFmt,
    //      PassSalt, MobilePIN, LCEmail, PWQuest, PWAns,
    //      IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg,
    //      LastLock, XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt,
    //      Comment, Created, AgeGroup, RegTypeID, RepID, Work_Phone, Badge_ID,
    //       Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, Giftcard_Ind,
    //       Giftcard_Chour, Giftcard_Uhour, UniqueID, FirstLogin_Ind);
    //        bool ret = SiteProvider.PR.UpdateUserAccount(record);

    //        BizObject.PurgeCacheItems("UserAccounts_UserAccount_" + iID.ToString());
    //        BizObject.PurgeCacheItems("UserAccounts_UserAccounts");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new UserAccount
    //    /// </summary>
    //    public static int InsertUserAccount(string cAddress1, string cAddress2, bool lAdmin, bool lPrimary, string cBirthDate,
    //      string cCity, string cEmail, string cExpires, string cFirstName, string cFloridaNo,
    //      string cInstitute, string cLastName, string cLectDate, string cMiddle, string cPhone,
    //      string cPromoCode, int PromoID, int FacilityID, string cPW, string cRefCode,
    //      string cRegType, string cSocial, string cState, string cUserName, string cZipCode,
    //      int SpecID, string LCUserName, DateTime LastAct, string PassFmt,
    //      string PassSalt, string MobilePIN, string LCEmail, string PWQuest, string PWAns,
    //      bool IsApproved, bool IsOnline, bool IsLocked, DateTime LastLogin, DateTime LastPWChg,
    //      DateTime LastLock, int XPWAtt, DateTime XPWAttSt, int XPWAnsAtt, DateTime XPWAnsSt,
    //      string Comment, DateTime Created, int AgeGroup, int RegTypeID, int RepID, string Work_Phone, string Badge_ID,
    //       DateTime Hire_Date, DateTime Termin_Date, int Dept_ID, int Position_ID, int Clinical_Ind, string Title, bool Giftcard_Ind,
    //       decimal Giftcard_Chour, decimal Giftcard_Uhour, int UniqueID, bool FirstLogin_Ind)
    //    {
    //        cAddress1 = BizObject.ConvertNullToEmptyString(cAddress1);
    //        cAddress2 = BizObject.ConvertNullToEmptyString(cAddress2);
    //        cBirthDate = BizObject.ConvertNullToEmptyString(cBirthDate);
    //        cCity = BizObject.ConvertNullToEmptyString(cCity);
    //        cEmail = BizObject.ConvertNullToEmptyString(cEmail);
    //        cExpires = BizObject.ConvertNullToEmptyString(cExpires);
    //        cFirstName = BizObject.ConvertNullToEmptyString(cFirstName);
    //        cFloridaNo = BizObject.ConvertNullToEmptyString(cFloridaNo);
    //        cInstitute = BizObject.ConvertNullToEmptyString(cInstitute);
    //        cLastName = BizObject.ConvertNullToEmptyString(cLastName);
    //        cLectDate = BizObject.ConvertNullToEmptyString(cLectDate);
    //        cMiddle = BizObject.ConvertNullToEmptyString(cMiddle);
    //        cPhone = BizObject.ConvertNullToEmptyString(cPhone);
    //        cPromoCode = BizObject.ConvertNullToEmptyString(cPromoCode);
    //        cPW = BizObject.ConvertNullToEmptyString(cPW);
    //        cRefCode = BizObject.ConvertNullToEmptyString(cRefCode);
    //        cRegType = BizObject.ConvertNullToEmptyString(cRegType);
    //        cSocial = BizObject.ConvertNullToEmptyString(cSocial);
    //        cState = BizObject.ConvertNullToEmptyString(cState);
    //        cUserName = BizObject.ConvertNullToEmptyString(cUserName);
    //        cZipCode = BizObject.ConvertNullToEmptyString(cZipCode);
    //        LCUserName = BizObject.ConvertNullToEmptyString(LCUserName);
    //        PassFmt = BizObject.ConvertNullToEmptyString(PassFmt);
    //        PassSalt = BizObject.ConvertNullToEmptyString(PassSalt);
    //        MobilePIN = BizObject.ConvertNullToEmptyString(MobilePIN);
    //        LCEmail = BizObject.ConvertNullToEmptyString(LCEmail);
    //        PWQuest = BizObject.ConvertNullToEmptyString(PWQuest);
    //        PWAns = BizObject.ConvertNullToEmptyString(PWAns);
    //        Comment = BizObject.ConvertNullToEmptyString(Comment);
    //        //UniqueID = BizObject.ConvertNullToEmptyString(UniqueID);

    //      // NOTE: Always send FirstLogin_Ind as true when inserting a new user record
    //        UserAccountInfo record = new UserAccountInfo(0, cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate,
    //      cCity, cEmail, cExpires, cFirstName, cFloridaNo,
    //      cInstitute, cLastName, cLectDate, cMiddle, cPhone,
    //      cPromoCode, PromoID, FacilityID, cPW, cRefCode,
    //      cRegType, cSocial, cState, cUserName, cZipCode,
    //      SpecID, LCUserName, LastAct, PassFmt,
    //      PassSalt, MobilePIN, LCEmail, PWQuest, PWAns,
    //      IsApproved, IsOnline, IsLocked, LastLogin, LastPWChg,
    //      LastLock, XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt,
    //      Comment, Created, AgeGroup, RegTypeID, RepID, Work_Phone, Badge_ID,
    //       Hire_Date, Termin_Date, Dept_ID, Position_ID, Clinical_Ind, Title, Giftcard_Ind,
    //       Giftcard_Chour, Giftcard_Uhour, UniqueID, true);
    //        int ret = SiteProvider.PR.InsertUserAccount(record);

    //        BizObject.PurgeCacheItems("UserAccounts_UserAccount");
    //        return ret;
    //    }

    //    ///// <summary>
    //    ///// Returns the highest iID
    //    ///// (used to find the last ID inserted because all other methods
    //    ///// are failing)
    //    ///// </summary>
    //    //public static int GetNextUserAccountID()
    //    //{
    //    //    int iID = 0;
    //    //    iID = SiteProvider.PR.GetNextUserAccountID();
    //    //    return iID;
    //    //}

    //    /// <summary>
    //    /// Deletes an existing UserAccount, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteUserAccount(int iID)
    //    {
    //        bool IsOKToDelete = OKToDelete(iID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteUserAccount(iID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing UserAccount - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteUserAccount(int iID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteUserAccount(iID);
    //        //         new RecordDeletedEvent("UserAccount", iID, null).Raise();
    //        BizObject.PurgeCacheItems("UserAccounts_UserAccount");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a UserAccount can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int iID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a UserAccount object filled with the data taken from the input UserAccountInfo
    //    /// </summary>
    //    private static UserAccount GetUserAccountFromUserAccountInfo(UserAccountInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new UserAccount(record.iID, record.cAddress1, record.cAddress2, record.lAdmin,
    //                record.lPrimary, record.cBirthDate,
    //      record.cCity, record.cEmail, record.cExpires, record.cFirstName, record.cFloridaNo,
    //      record.cInstitute, record.cLastName, record.cLectDate, record.cMiddle, record.cPhone,
    //      record.cPromoCode, record.PromoID, record.FacilityID, record.cPW.ToLower(), record.cRefCode,
    //      record.cRegType, record.cSocial, record.cState, record.cUserName.ToLower(), record.cZipCode,
    //      record.SpecID, record.LCUserName.ToLower(), record.LastAct, record.PassFmt,
    //      record.PassSalt, record.MobilePIN, record.LCEmail, record.PWQuest, record.PWAns.ToLower(),
    //      record.IsApproved, record.IsOnline, record.IsLocked, record.LastLogin, record.LastPWChg,
    //      record.LastLock, record.XPWAtt, record.XPWAttSt, record.XPWAnsAtt, record.XPWAnsSt,
    //      record.Comment, record.Created, record.AgeGroup, record.RegTypeID, record.RepID, 
    //      record.Work_Phone, record.Badge_ID,record.Hire_Date,record.Termin_Date,
    //      record.Dept_ID, record.Position_ID, record.Clinical_Ind, record.Title, record.Giftcard_Ind,
    //       record.Giftcard_Chour, record.Giftcard_Uhour, record.UniqueID, record.FirstLogin_Ind);
    //        }
    //    }


    //    ///<summary>
    //    ///Returns a list of Gift Card Users
    //    ///</summary>
    //    ///

    //    public static bool GetGiftCardUsersFromUserAccountInfoList(string cGiftUsername)
    //    {
    //        bool ret = SiteProvider.PR.GetGiftCardUsersFromUserAccountInfoList(cGiftUsername);
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Returns a list of UserAccount objects filled with the data taken from the input list of UserAccountInfo
    //    /// </summary>
    //    private static List<UserAccount> GetUserAccountListFromUserAccountInfoList(List<UserAccountInfo> recordset)
    //    {
    //        List<UserAccount> UserAccounts = new List<UserAccount>();
    //        foreach (UserAccountInfo record in recordset)
    //            UserAccounts.Add(GetUserAccountFromUserAccountInfo(record));
    //        return UserAccounts;
    //    }

    //  /// <summary>
    //  /// Returns a collection with all UserAccounts for the specified promo code
    //  /// </summary>
    //  public static List<UserAccount> GetUserAccountsByPromoCode(string cPromoCode, string cSortExpression)
    //  {
    //      if (cPromoCode == null)
    //          cPromoCode = "";
    //      if (cSortExpression == null)
    //          cSortExpression = "";

    //      // provide default sort
    //      if (cSortExpression.Length == 0)
    //          cSortExpression = "cLastName";

    //      List<UserAccount> UserAccounts = null;
    //     string key = "UserAccounts_UserAccounts_" + cPromoCode.ToString() + cSortExpression.ToString();

    //     if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //     {
    //        UserAccounts = (List<UserAccount>)BizObject.Cache[key];
    //     }
    //     else
    //     {
    //        List<UserAccountInfo> recordset = SiteProvider.PR.GetUserAccountsByPromoCode(cPromoCode, cSortExpression);
    //        UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
    //        BasePR.CacheData(key, UserAccounts);
    //     }
    //     return UserAccounts;
    //  }

    //  /// <summary>
    //  /// Returns a collection with all UserAccounts for the specified lastname (can be portion)
    //  /// </summary>
    //  public static List<UserAccount> GetUserAccountsByLastName(string cLastName, string cSortExpression)
    //  {
    //      if (cLastName == null)
    //          cLastName = "A";
    //      if (cSortExpression == null)
    //          cSortExpression = "";

    //      // provide default sort
    //      if (cSortExpression.Length == 0)
    //          cSortExpression = "cLastName";

    //      List<UserAccount> UserAccounts = null;
    //      string key = "UserAccounts_UserAccountsByLastName_" + cLastName.ToString() + cSortExpression.ToString();

    //      if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //      {
    //          UserAccounts = (List<UserAccount>)BizObject.Cache[key];
    //      }
    //      else
    //      {
    //          List<UserAccountInfo> recordset = SiteProvider.PR.GetUserAccountsByLastName(cLastName, cSortExpression);
    //          UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
    //          BasePR.CacheData(key, UserAccounts);
    //      }
    //      return UserAccounts;
    //  }

    //  /// <summary>
    //  /// Returns a collection with all UserAccounts for the specified lastname (can be portion)
    //  /// </summary>
    //  public static List<UserAccount> GetUserAccountsByShortUserName(string ShortUserName, string cSortExpression)
    //  {
    //      if (ShortUserName == null)
    //          ShortUserName = "a";
    //      if (cSortExpression == null)
    //          cSortExpression = "";

    //      // provide default sort
    //      if (cSortExpression.Length == 0)
    //          cSortExpression = "cUserName";

    //      List<UserAccount> UserAccounts = null;
    //      string key = "UserAccounts_UserAccountsByShortUserName_" + ShortUserName.ToString() + cSortExpression.ToString();

    //      if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //      {
    //          UserAccounts = (List<UserAccount>)BizObject.Cache[key];
    //      }
    //      else
    //      {
    //          List<UserAccountInfo> recordset = SiteProvider.PR.GetUserAccountsByShortUserName(ShortUserName, cSortExpression);
    //          UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
    //          BasePR.CacheData(key, UserAccounts);
    //      }
    //      return UserAccounts;
    //  }

    //  /// <summary>
    //  /// Returns a collection with all UserAccounts for the specified lastname (can be portion)
    //  /// </summary>
    //  public static List<UserAccount> GetUserAccountsByEmail(string cEmail, string cSortExpression)
    //  {
    //      if (cEmail == null)
    //          cEmail = "a";
    //      if (cSortExpression == null)
    //          cSortExpression = "";

    //      // provide default sort
    //      if (cSortExpression.Length == 0)
    //          cSortExpression = "cEmail";

    //      List<UserAccount> UserAccounts = null;
    //      string key = "UserAccounts_UserAccountsByEmail_" + cEmail.ToString() + cSortExpression.ToString();

    //      if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //      {
    //          UserAccounts = (List<UserAccount>)BizObject.Cache[key];
    //      }
    //      else
    //      {
    //          List<UserAccountInfo> recordset = SiteProvider.PR.GetUserAccountsByEmail(cEmail, cSortExpression);
    //          UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
    //          BasePR.CacheData(key, UserAccounts);
    //      }
    //      return UserAccounts;
    //  }

    //  /// <summary>
    //  /// Returns the number of total UserAccounts for a Promo Code
    //  /// </summary>
    //  public static int GetUserAccountCountByPromoCode(string cPromoCode)
    //  {
    //      int UserAccountCount = 0;
    //      string key = "UserAccounts_UserAccountCount_PromoCode_" + cPromoCode.ToString();

    //      if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //      {
    //          UserAccountCount = (int)BizObject.Cache[key];
    //      }
    //      else
    //      {
    //          UserAccountCount = SiteProvider.PR.GetUserAccountCountByPromoCode(cPromoCode);
    //          BasePR.CacheData(key, UserAccountCount);
    //      }
    //      return UserAccountCount;
    //  }

    //    /// <summary>
    //  /// Returns a collection with all UserAccounts for the specified PromoID
    //  /// </summary>
    //  public static List<UserAccount> GetUserAccountsByPromoID(int PromoID, string cSortExpression)
    //  {
    //      if (cSortExpression == null)
    //          cSortExpression = "";

    //      // provide default sort
    //      if (cSortExpression.Length == 0)
    //          cSortExpression = "cLastName";

    //      List<UserAccount> UserAccounts = null;
    //      string key = "UserAccounts_UserAccounts_PromoID_" + PromoID.ToString() + cSortExpression.ToString();

    //      if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //      {
    //          UserAccounts = (List<UserAccount>)BizObject.Cache[key];
    //      }
    //      else
    //      {
    //          List<UserAccountInfo> recordset = SiteProvider.PR.GetUserAccountsByPromoID(PromoID, cSortExpression);
    //          UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
    //          BasePR.CacheData(key, UserAccounts);
    //      }
    //      return UserAccounts;
    //  }
    //  ///// <summary>
    //  ///// Returns a collection with all admin UserAccounts for the specified promoID
    //  ///// </summary>
    //  //public static List<UserAccount> GetAdminUserAccountsByPromoID(int PromoID, string cSortExpression)
    //  //{
    //  //    if (cSortExpression == null)
    //  //        cSortExpression = "";

    //  //    // provide default sort
    //  //    if (cSortExpression.Length == 0)
    //  //        cSortExpression = "cLastName";

    //  //    List<UserAccount> UserAccounts = null;
    //  //    string key = "UserAccounts_UserAccounts_Admin_" + cPromoCode.ToString() + cSortExpression.ToString();

    //  //    if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //  //    {
    //  //        UserAccounts = (List<UserAccount>)BizObject.Cache[key];
    //  //    }
    //  //    else
    //  //    {
    //  //        List<UserAccountInfo> recordset = SiteProvider.PR.GetAdminUserAccountsByPromoCode(cPromoCode, cSortExpression);
    //  //        UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
    //  //        BasePR.CacheData(key, UserAccounts);
    //  //    }
    //  //    return UserAccounts;
    //  //}

    //  /// <summary>
    //  /// Returns the number of total UserAccounts for a PromoID
    //  /// </summary>
    //  public static int GetUserAccountCountByPromoID(int PromoID)
    //  {
    //      int UserAccountCount = 0;
    //      string key = "UserAccounts_UserAccountCount_PromoID_" + PromoID.ToString();

    //      if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //      {
    //          UserAccountCount = (int)BizObject.Cache[key];
    //      }
    //      else
    //      {
    //          UserAccountCount = SiteProvider.PR.GetUserAccountCountByPromoID(PromoID);
    //          BasePR.CacheData(key, UserAccountCount);
    //      }
    //      return UserAccountCount;
    //  }


    //  /// <summary>
    //  /// Returns a collection with all UserAccounts for the specified FacilityID
    //  /// </summary>
    //  public static List<UserAccount> GetUserAccountsByFacilityID(int FacilityID, string cSortExpression)
    //  {
    //      if (cSortExpression == null)
    //          cSortExpression = "";

    //      // provide default sort
    //      if (cSortExpression.Length == 0)
    //          cSortExpression = "cLastName";

    //      List<UserAccount> UserAccounts = null;
    //      string key = "UserAccounts_UserAccounts_FacilityID_" + FacilityID.ToString() + cSortExpression.ToString();

    //      if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //      {
    //          UserAccounts = (List<UserAccount>)BizObject.Cache[key];
    //      }
    //      else
    //      {
    //          List<UserAccountInfo> recordset = SiteProvider.PR.GetUserAccountsByFacilityID(FacilityID, cSortExpression);
    //          UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
    //          BasePR.CacheData(key, UserAccounts);
    //      }
    //      return UserAccounts;
    //  }
    //  /// <summary>
    //  /// Returns a collection with all admin UserAccounts for the specified FacilityID
    //  /// </summary>
    //  public static List<UserAccount> GetAdminUserAccountsByFacilityID(int FacilityID, string cSortExpression)
    //  {
    //      if (cSortExpression == null)
    //          cSortExpression = "";

    //      // provide default sort
    //      if (cSortExpression.Length == 0)
    //          cSortExpression = "cLastName";

    //      List<UserAccount> UserAccounts = null;
    //      string key = "UserAccounts_UserAccounts_Admin_" + FacilityID.ToString() + cSortExpression.ToString();

    //      if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //      {
    //          UserAccounts = (List<UserAccount>)BizObject.Cache[key];
    //      }
    //      else
    //      {
    //          List<UserAccountInfo> recordset = SiteProvider.PR.GetAdminUserAccountsByFacilityID(FacilityID, cSortExpression);
    //          UserAccounts = GetUserAccountListFromUserAccountInfoList(recordset);
    //          BasePR.CacheData(key, UserAccounts);
    //      }
    //      return UserAccounts;
    //  }

    //  /// <summary>
    //  /// Returns the number of total UserAccounts for a FacilityID
    //  /// </summary>
    //  public static int GetUserAccountCountByFacilityID(int FacilityID)
    //  {
    //      int UserAccountCount = 0;
    //      string key = "UserAccounts_UserAccountCount_FacilityID_" + FacilityID.ToString();

    //      if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //      {
    //          UserAccountCount = (int)BizObject.Cache[key];
    //      }
    //      else
    //      {
    //          UserAccountCount = SiteProvider.PR.GetUserAccountCountByFacilityID(FacilityID);
    //          BasePR.CacheData(key, UserAccountCount);
    //      }
    //      return UserAccountCount;
    //  }

    //  /// <summary>
    //  /// Updates an existing UserAccount admin field
    //  /// </summary>
    //  public static bool UpdateUserAccountAdminStatus(int iID, bool lAdmin)
    //  {
    //      bool ret = SiteProvider.PR.UpdateUserAccountAdminStatus(iID, lAdmin);
    //      return ret;
    //  }


    //  ///<summary>
    //  /// Gets the Tax rate based on Userid
    //  /// </summary>
    //  /// 

    //  public static decimal GetTaxRateByUserId(int iID)
    //  {
    //      decimal ret = SiteProvider.PR.GetTaxRateByUserId(iID);
    //          return ret;
    //  }




    //}
    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// RegistrationType business object class
    ///// </summary>
    //public class RegistrationType : BasePR
    //{
    //    private int _RegTypeID = 0;
    //    public int RegTypeID
    //    {
    //        get { return _RegTypeID; }
    //        protected set { _RegTypeID = value; }
    //    }

    //    private string _RegType = "";
    //    public string RegType
    //    {
    //        get { return _RegType; }
    //        set { _RegType = value; }
    //    }



    //   public RegistrationType(int RegTypeID, string RegType)
    //  {
    //        this.RegTypeID = RegTypeID;
    //        this.RegType = RegType;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = RegistrationType.DeleteRegistrationType(this.RegTypeID);
    //        if (success)
    //            this.RegTypeID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return RegistrationType.UpdateRegistrationType(this.RegTypeID, this.RegType);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all RegistrationTypes
    //    /// </summary>
    //    public static List<RegistrationType> GetRegistrationTypes(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "RegType";

    //        List<RegistrationType> RegistrationTypes = null;
    //        string key = "RegistrationTypes_RegistrationTypes_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            RegistrationTypes = (List<RegistrationType>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<RegistrationTypeInfo> recordset = SiteProvider.PR.GetRegistrationTypes(cSortExpression);
    //            RegistrationTypes = GetRegistrationTypeListFromRegistrationTypeInfoList(recordset);
    //            BasePR.CacheData(key, RegistrationTypes);
    //        }
    //        return RegistrationTypes;
    //    }


    //    /// <summary>
    //    /// Returns the number of total RegistrationTypes
    //    /// </summary>
    //    public static int GetRegistrationTypeCount()
    //    {
    //        int RegistrationTypeCount = 0;
    //        string key = "RegistrationTypes_RegistrationTypeCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            RegistrationTypeCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            RegistrationTypeCount = SiteProvider.PR.GetRegistrationTypeCount();
    //            BasePR.CacheData(key, RegistrationTypeCount);
    //        }
    //        return RegistrationTypeCount;
    //    }

    //    /// <summary>
    //    /// Returns a RegistrationType object with the specified ID
    //    /// </summary>
    //    public static RegistrationType GetRegistrationTypeByID(int RegTypeID)
    //    {
    //        RegistrationType RegistrationType = null;
    //        string key = "RegistrationTypes_RegistrationType_" + RegTypeID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            RegistrationType = (RegistrationType)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            RegistrationType = GetRegistrationTypeFromRegistrationTypeInfo(SiteProvider.PR.GetRegistrationTypeByID(RegTypeID));
    //            BasePR.CacheData(key, RegistrationType);
    //        }
    //        return RegistrationType;
    //    }

    //    /// <summary>
    //    /// Updates an existing RegistrationType
    //    /// </summary>
    //    public static bool UpdateRegistrationType(int RegTypeID, string RegType)
    //    {
    //        RegType = BizObject.ConvertNullToEmptyString(RegType);


    //        RegistrationTypeInfo record = new RegistrationTypeInfo(RegTypeID, RegType);
    //        bool ret = SiteProvider.PR.UpdateRegistrationType(record);

    //        BizObject.PurgeCacheItems("RegistrationTypes_RegistrationType_" + RegTypeID.ToString());
    //        BizObject.PurgeCacheItems("RegistrationTypes_RegistrationTypes");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new RegistrationType
    //    /// </summary>
    //    public static int InsertRegistrationType(string RegType)
    //    {
    //        RegType = BizObject.ConvertNullToEmptyString(RegType);


    //        RegistrationTypeInfo record = new RegistrationTypeInfo(0, RegType);
    //        int ret = SiteProvider.PR.InsertRegistrationType(record);

    //        BizObject.PurgeCacheItems("RegistrationTypes_RegistrationType");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing RegistrationType, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteRegistrationType(int RegTypeID)
    //    {
    //        bool IsOKToDelete = OKToDelete(RegTypeID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteRegistrationType(RegTypeID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing RegistrationType - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteRegistrationType(int RegTypeID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteRegistrationType(RegTypeID);
    //        //         new RecordDeletedEvent("RegistrationType", RegTypeID, null).Raise();
    //        BizObject.PurgeCacheItems("RegistrationTypes_RegistrationType");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a RegistrationType can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int RegTypeID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a RegistrationType object filled with the data taken from the input RegistrationTypeInfo
    //    /// </summary>
    //    private static RegistrationType GetRegistrationTypeFromRegistrationTypeInfo(RegistrationTypeInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new RegistrationType(record.RegTypeID, record.RegType);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of RegistrationType objects filled with the data taken from the input list of RegistrationTypeInfo
    //    /// </summary>
    //    private static List<RegistrationType> GetRegistrationTypeListFromRegistrationTypeInfoList(List<RegistrationTypeInfo> recordset)
    //    {
    //        List<RegistrationType> RegistrationTypes = new List<RegistrationType>();
    //        foreach (RegistrationTypeInfo record in recordset)
    //            RegistrationTypes.Add(GetRegistrationTypeFromRegistrationTypeInfo(record));
    //        return RegistrationTypes;
    //    }



    //}
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// ResourcePerson business object class
    /// </summary>
    //public class ResourcePerson : BasePR
    //{
    //    private int _ResourceID = 0;
    //    public int ResourceID
    //    {
    //        get { return _ResourceID; }
    //        protected set { _ResourceID = value; }
    //    }

    //    private string _Fullname = "";
    //    public string Fullname
    //    {
    //        get { return _Fullname; }
    //        set { _Fullname = value; }
    //    }

    //    private string _LastFirst = "";
    //    public string LastFirst
    //    {
    //        get { return _LastFirst.ToLower(); }
    //        set { _LastFirst = value.ToLower(); }
    //    }

    //    private string _Address = "";
    //    public string Address
    //    {
    //        get { return _Address; }
    //        set { _Address = value; }
    //    }

    //    private string _City = "";
    //    public string City
    //    {
    //        get { return _City; }
    //        set { _City = value; }
    //    }

    //    private string _State = "";
    //    public string State
    //    {
    //        get { return _State; }
    //        set { _State = value; }
    //    }

    //    private string _Zip = "";
    //    public string Zip
    //    {
    //        get { return _Zip; }
    //        set { _Zip = value; }
    //    }

    //    private string _Country = "";
    //    public string Country
    //    {
    //        get { return _Country; }
    //        set { _Country = value; }
    //    }

    //    private string _Phone = "";
    //    public string Phone
    //    {
    //        get { return _Phone; }
    //        set { _Phone = value; }
    //    }

    //    private string _Email = "";
    //    public string Email
    //    {
    //        get { return _Email; }
    //        set { _Email = value; }
    //    }

    //    private string _SSN = "";
    //    public string SSN
    //    {
    //        get { return _SSN; }
    //        set { _SSN = value; }
    //    }

    //    private string _Specialty = "";
    //    public string Specialty
    //    {
    //        get { return _Specialty; }
    //        set { _Specialty = value; }
    //    }

    //    private string _OtherNotes = "";
    //    public string OtherNotes
    //    {
    //        get { return _OtherNotes; }
    //        set { _OtherNotes = value; }
    //    }

    //    private bool _ContrRecd = false;
    //    public bool ContrRecd
    //    {
    //        get { return _ContrRecd; }
    //        set { _ContrRecd = value; }
    //    }

    //    private String _DateDiscl = "";
    //    public String DateDiscl
    //    {
    //        get { return _DateDiscl; }
    //        set { _DateDiscl = value; }
    //    }

    //    //private DateTime _DateDiscl = System.DateTime.MinValue;
    //    //public DateTime DateDiscl
    //    //{
    //    //    get { return _DateDiscl; }
    //    //    set { _DateDiscl = value; }
    //    //}

    //    private string _Disclosure = "I have nothing to disclose.";
    //    public string Disclosure
    //    {
    //        get { return _Disclosure; }
    //        set { _Disclosure = value; }
    //    }
        
    //    private string _StateLic = "";
    //    public string StateLic
    //    {
    //        get { return _StateLic; }
    //        set { _StateLic = value; }
    //    }
    //    private string _Degrees = "";
    //    public string Degrees
    //    {
    //        get { return _Degrees; }
    //        set { _Degrees = value; }
    //    }
    //    private string _Position = "";
    //    public string Position
    //    {
    //        get { return _Position; }
    //        set { _Position = value; }
    //    }
    //    private string _Experience = "";
    //    public string Experience
    //    {
    //        get { return _Experience; }
    //        set { _Experience = value; }
    //    }

    //    private int _FacilityID = 0;
    //    public int FacilityID
    //    {
    //        get { return _FacilityID; }
    //        set { _FacilityID = value; }
    //    }


    //   public ResourcePerson(int ResourceID, string Fullname, string LastFirst, string Address, string City,
    //       string State, string Zip, string Country, string Phone, string Email, string SSN, string Specialty,
    //       string OtherNotes, bool ContrRecd, String DateDiscl, string Disclosure, string StateLic,
    //       string Degrees, string Position, string Experience, int FacilityID)
    // {
    //        this.ResourceID = ResourceID;
    //        this.Fullname = Fullname;
    //        this.LastFirst = LastFirst;
    //        this.Address = Address;
    //        this.City = City;
    //        this.State = State;
    //        this.Zip = Zip;
    //        this.Country = Country;
    //        this.Phone = Phone;
    //        this.Email = Email;
    //        this.SSN = SSN;
    //        this.Specialty = Specialty;
    //        this.OtherNotes = OtherNotes;
    //        this.ContrRecd = ContrRecd;
    //        this.DateDiscl = DateDiscl;
    //        this.Disclosure = Disclosure;
    //        this.StateLic = StateLic;
    //        this.Degrees = Degrees;
    //        this.Position = Position;
    //        this.Experience = Experience;
    //        this.FacilityID = FacilityID;
    //   }

    //    public bool Delete()
    //    {
    //        bool success = ResourcePerson.DeleteResourcePerson(this.ResourceID);
    //        if (success)
    //            this.ResourceID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return ResourcePerson.UpdateResourcePerson(this.ResourceID, this.Fullname, this.LastFirst,
    //            this.Address, this.City, this.State, this.Zip, this.Country, this.Phone, this.Email,
    //            this.SSN, this.Specialty, this.OtherNotes, this.ContrRecd, this.DateDiscl, this.Disclosure,
    //            this.StateLic, this.Degrees, this.Position, this.Experience, this.FacilityID );
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all ResourcePersons
    //    /// </summary>
    //    public static List<ResourcePerson> GetResourcePersons(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "LastFirst";

    //        List<ResourcePerson> ResourcePersons = null;
    //        string key = "ResourcePersons_ResourcePersons_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<ResourcePersonInfo> recordset = SiteProvider.PR.GetResourcePersons(cSortExpression);
    //            ResourcePersons = GetResourcePersonListFromResourcePersonInfoList(recordset);
    //            BasePR.CacheData(key, ResourcePersons);
    //        }
    //        return ResourcePersons;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all ResourcePersons linked to a topic for a certain type
    //    /// (author="A", editor="E", nurse planner="N")
    //    /// </summary>
    //    public static List<ResourcePerson> GetResourcePersonsByTopicIDAndType(int TopicID, 
    //        string ResourceType, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "LastFirst";

    //        List<ResourcePerson> ResourcePersons = null;
    //        string key = "ResourcePersons_ResourcePersons_" + TopicID.ToString() + "_" +ResourceType.ToString() + "_" +cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<ResourcePersonInfo> recordset = SiteProvider.PR.GetResourcePersonsByTopicIDAndType(TopicID, ResourceType, cSortExpression);
    //            ResourcePersons = GetResourcePersonListFromResourcePersonInfoList(recordset);
    //            BasePR.CacheData(key, ResourcePersons);
    //        }
    //        return ResourcePersons;
    //    }


    //    /// <summary>
    //    /// Returns the number of total ResourcePersons
    //    /// </summary>
    //    public static int GetResourcePersonCount()
    //    {
    //        int ResourcePersonCount = 0;
    //        string key = "ResourcePersons_ResourcePersonCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ResourcePersonCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            ResourcePersonCount = SiteProvider.PR.GetResourcePersonCount();
    //            BasePR.CacheData(key, ResourcePersonCount);
    //        }
    //        return ResourcePersonCount;
    //    }

    //    /// <summary>
    //    /// Returns a ResourcePerson object with the specified ID
    //    /// </summary>
    //    public static ResourcePerson GetResourcePersonByID(int ResourceID)
    //    {
    //        ResourcePerson ResourcePerson = null;
    //        string key = "ResourcePersons_ResourcePerson_" + ResourceID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ResourcePerson = (ResourcePerson)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            ResourcePerson = GetResourcePersonFromResourcePersonInfo(SiteProvider.PR.GetResourcePersonByID(ResourceID));
    //            BasePR.CacheData(key, ResourcePerson);
    //        }
    //        return ResourcePerson;
    //    }


    //    /// <summary>
    //    /// Returns a ResourcePerson object with the Facility Id
    //    /// </summary>
    //    public static List<ResourcePerson> GetResourcePersonByFacilityID(int FacilityID)
    //    {
    //        List<ResourcePerson> ResourcePersons = null;
    //        string key = "ResourcePersons_ResourcePerson_" + FacilityID.ToString();
            
    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ResourcePersons = (List<ResourcePerson>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<ResourcePersonInfo> recordset = SiteProvider.PR.GetResourcePersonByFacilityID(FacilityID);
    //            ResourcePersons = GetResourcePersonListFromResourcePersonInfoList(recordset);
    //            BasePR.CacheData(key, ResourcePersons);
    //        }
    //        return ResourcePersons;
    //    }

    //    /// <summary>
    //    /// Updates an existing ResourcePerson
    //    /// </summary>
    //    public static bool UpdateResourcePerson(int ResourceID, string Fullname, string LastFirst,
    //        string Address, string City,
    //        string State, string Zip, string Country, string Phone, string Email, string SSN, string Specialty,
    //        string OtherNotes, bool ContrRecd, String DateDiscl, string Disclosure, string StateLic,
    //        string Degrees, string Position, string Experience, int FacilityID)
    //    {
    //        Fullname = BizObject.ConvertNullToEmptyString(Fullname);
    //        LastFirst = BizObject.ConvertNullToEmptyString(LastFirst);
    //        Address = BizObject.ConvertNullToEmptyString(Address);
    //        City = BizObject.ConvertNullToEmptyString(City);
    //        State = BizObject.ConvertNullToEmptyString(State);
    //        Zip = BizObject.ConvertNullToEmptyString(Zip);
    //        Country = BizObject.ConvertNullToEmptyString(Country);
    //        Phone = BizObject.ConvertNullToEmptyString(Phone);
    //        Email = BizObject.ConvertNullToEmptyString(Email);
    //        SSN = BizObject.ConvertNullToEmptyString(SSN);
    //        Specialty = BizObject.ConvertNullToEmptyString(Specialty);
    //        OtherNotes = BizObject.ConvertNullToEmptyString(OtherNotes);
    //        Disclosure = BizObject.ConvertNullToEmptyString(Disclosure);
    //        DateDiscl = BizObject.ConvertNullToEmptyString(DateDiscl);
    //        StateLic = BizObject.ConvertNullToEmptyString(StateLic);
    //        Degrees = BizObject.ConvertNullToEmptyString(Degrees);
    //        Position = BizObject.ConvertNullToEmptyString(Position);
    //        Experience = BizObject.ConvertNullToEmptyString(Experience);


    //        ResourcePersonInfo record = new ResourcePersonInfo(ResourceID, Fullname, LastFirst,
    //            Address, City, State, Zip, Country, Phone, Email, SSN, Specialty,
    //            OtherNotes, ContrRecd, DateDiscl, Disclosure, StateLic,
    //            Degrees, Position, Experience, FacilityID);
    //        bool ret = SiteProvider.PR.UpdateResourcePerson(record);

    //        BizObject.PurgeCacheItems("ResourcePersons_ResourcePerson_" + ResourceID.ToString());
    //        BizObject.PurgeCacheItems("ResourcePersons_ResourcePersons");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new ResourcePerson
    //    /// </summary>
    //    public static int InsertResourcePerson(string Fullname, string LastFirst,
    //        string Address, string City,
    //        string State, string Zip, string Country, string Phone, string Email, string SSN, string Specialty,
    //        string OtherNotes, bool ContrRecd, String DateDiscl, string Disclosure, string StateLic,
    //        string Degrees, string Position, string Experience, int FacilityID)

    //    {
    //        Fullname = BizObject.ConvertNullToEmptyString(Fullname);
    //        LastFirst = BizObject.ConvertNullToEmptyString(LastFirst);
    //        Address = BizObject.ConvertNullToEmptyString(Address);
    //        City = BizObject.ConvertNullToEmptyString(City);
    //        State = BizObject.ConvertNullToEmptyString(State);
    //        Zip = BizObject.ConvertNullToEmptyString(Zip);
    //        Country = BizObject.ConvertNullToEmptyString(Country);
    //        Phone = BizObject.ConvertNullToEmptyString(Phone);
    //        Email = BizObject.ConvertNullToEmptyString(Email);
    //        SSN = BizObject.ConvertNullToEmptyString(SSN);
    //        Specialty = BizObject.ConvertNullToEmptyString(Specialty);
    //        OtherNotes = BizObject.ConvertNullToEmptyString(OtherNotes);
    //        DateDiscl = BizObject.ConvertNullToEmptyString(DateDiscl);
    //        Disclosure = BizObject.ConvertNullToEmptyString(Disclosure);
    //        StateLic = BizObject.ConvertNullToEmptyString(StateLic);
    //        Degrees = BizObject.ConvertNullToEmptyString(Degrees);
    //        Position = BizObject.ConvertNullToEmptyString(Position);
    //        Experience = BizObject.ConvertNullToEmptyString(Experience);


    //        ResourcePersonInfo record = new ResourcePersonInfo(0, Fullname, LastFirst,
    //            Address, City, State, Zip, Country, Phone, Email, SSN, Specialty,
    //            OtherNotes, ContrRecd, DateDiscl, Disclosure, StateLic,
    //            Degrees, Position, Experience, FacilityID);

    //        int ret = SiteProvider.PR.InsertResourcePerson(record);

    //        BizObject.PurgeCacheItems("ResourcePersons_ResourcePerson");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing ResourcePerson, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteResourcePerson(int ResourceID)
    //    {
    //        bool IsOKToDelete = OKToDelete(ResourceID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteResourcePerson(ResourceID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing ResourcePerson - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteResourcePerson(int ResourceID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteResourcePerson(ResourceID);
    //        //         new RecordDeletedEvent("ResourcePerson", ResourceID, null).Raise();
    //        BizObject.PurgeCacheItems("ResourcePersons_ResourcePerson");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a ResourcePerson can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int ResourceID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a ResourcePerson object filled with the data taken from the input ResourcePersonInfo
    //    /// </summary>
    //    private static ResourcePerson GetResourcePersonFromResourcePersonInfo(ResourcePersonInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new ResourcePerson(record.ResourceID, record.Fullname, record.LastFirst,
    //                record.Address, record.City, record.State, record.Zip, record.Country, record.Phone, record.Email,
    //                record.SSN, record.Specialty, record.OtherNotes, record.ContrRecd, record.DateDiscl, record.Disclosure,
    //                record.StateLic, record.Degrees, record.Position, record.Experience, record.FacilityID);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of ResourcePerson objects filled with the data taken from the input list of ResourcePersonInfo
    //    /// </summary>
    //    private static List<ResourcePerson> GetResourcePersonListFromResourcePersonInfoList(List<ResourcePersonInfo> recordset)
    //    {
    //        List<ResourcePerson> ResourcePersons = new List<ResourcePerson>();
    //        foreach (ResourcePersonInfo record in recordset)
    //            ResourcePersons.Add(GetResourcePersonFromResourcePersonInfo(record));
    //        return ResourcePersons;
    //    }

    //    /// <summary>
    //    /// Returns a string with authors for a topicid with optional bios (from specialty field)
    //    /// </summary>
    //    public static string GetAuthorsByTopicID(int TopicID, bool IncludeBio)
    //    {
    //        string cResources = "";
    //        string cWorkString = "";
    //        string cDegrees = "";
    //        int iCount = 0;
            
    //        List<ResourcePerson> Authors = ResourcePerson.GetResourcePersonsByTopicIDAndType(TopicID, "A", "sortorder");
    //        foreach (ResourcePerson Author in Authors)
    //        {
    //            iCount++;
    //            cWorkString = cWorkString + "<b>";
    //            if (iCount > 1 & IncludeBio == false )
    //                cWorkString = cWorkString + " and ";

    //            cWorkString = cWorkString + Author.Fullname.Trim();
    //            cDegrees = Author.Degrees.Trim();
    //            if (! String.IsNullOrEmpty(cDegrees) )
    //                cWorkString = cWorkString + ", " + cDegrees;
    //            // if bio needed, add newline and insert bio
    //            if (IncludeBio == true)
    //            {
    //                cWorkString = cWorkString + "</b><br />";
    //                if (! String.IsNullOrEmpty(Author.Specialty) )
    //                    cWorkString = cWorkString + Author.Specialty + "<br /><br />";
    //            }
    //            else
    //            {
    //                cWorkString = cWorkString + "</b>";
    //            }
    //        }
    //        cResources = cResources + cWorkString;
    //        return cResources;    

    //    }

    //}
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// Role business object class
    /// </summary>
    //public class Role : BasePR
    //{
    //    private int _RoleID = 0;
    //    public int RoleID
    //    {
    //        get { return _RoleID; }
    //        protected set { _RoleID = value; }
    //    }

    //    private string _RoleName = "";
    //    public string RoleName
    //    {
    //        get { return _RoleName; }
    //        set { _RoleName = value; }
    //    }


    //   public Role(int RoleID, string RoleName)
    //  {
    //        this.RoleID = RoleID;
    //        this.RoleName = RoleName;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = Role.DeleteRole(this.RoleID);
    //        if (success)
    //            this.RoleID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return Role.UpdateRole(this.RoleID, this.RoleName);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all Roles
    //    /// </summary>
    //    public static List<Role> GetRoles(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "RoleName";

    //        List<Role> Roles = null;
    //        string key = "Roles_Roles_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Roles = (List<Role>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<RoleInfo> recordset = SiteProvider.PR.GetRoles(cSortExpression);
    //            Roles = GetRoleListFromRoleInfoList(recordset);
    //            BasePR.CacheData(key, Roles);
    //        }
    //        return Roles;
    //    }


    //    /// <summary>
    //    /// Returns the number of total Roles
    //    /// </summary>
    //    public static int GetRoleCount()
    //    {
    //        int RoleCount = 0;
    //        string key = "Roles_RoleCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            RoleCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            RoleCount = SiteProvider.PR.GetRoleCount();
    //            BasePR.CacheData(key, RoleCount);
    //        }
    //        return RoleCount;
    //    }

    //    /// <summary>
    //    /// Returns a Role object with the specified ID
    //    /// </summary>
    //    public static Role GetRoleByID(int RoleID)
    //    {
    //        Role Role = null;
    //        string key = "Roles_Role_" + RoleID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Role = (Role)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            Role = GetRoleFromRoleInfo(SiteProvider.PR.GetRoleByID(RoleID));
    //            BasePR.CacheData(key, Role);
    //        }
    //        return Role;
    //    }

    //    /// <summary>
    //    /// Updates an existing Role
    //    /// </summary>
    //    public static bool UpdateRole(int RoleID, string RoleName)
    //    {
    //        RoleName = BizObject.ConvertNullToEmptyString(RoleName);


    //        RoleInfo record = new RoleInfo(RoleID, RoleName);
    //        bool ret = SiteProvider.PR.UpdateRole(record);

    //        BizObject.PurgeCacheItems("Roles_Role_" + RoleID.ToString());
    //        BizObject.PurgeCacheItems("Roles_Roles");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new Role
    //    /// </summary>
    //    public static int InsertRole(string RoleName)
    //    {
    //        RoleName = BizObject.ConvertNullToEmptyString(RoleName);


    //        RoleInfo record = new RoleInfo(0, RoleName);
    //        int ret = SiteProvider.PR.InsertRole(record);

    //        BizObject.PurgeCacheItems("Roles_Role");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing Role, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteRole(int RoleID)
    //    {
    //        bool IsOKToDelete = OKToDelete(RoleID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteRole(RoleID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing Role - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteRole(int RoleID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteRole(RoleID);
    //        //         new RecordDeletedEvent("Role", RoleID, null).Raise();
    //        BizObject.PurgeCacheItems("Roles_Role");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a Role can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int RoleID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a Role object filled with the data taken from the input RoleInfo
    //    /// </summary>
    //    private static Role GetRoleFromRoleInfo(RoleInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new Role(record.RoleID, record.RoleName);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of Role objects filled with the data taken from the input list of RoleInfo
    //    /// </summary>
    //    private static List<Role> GetRoleListFromRoleInfoList(List<RoleInfo> recordset)
    //    {
    //        List<Role> Roles = new List<Role>();
    //        foreach (RoleInfo record in recordset)
    //            Roles.Add(GetRoleFromRoleInfo(record));
    //        return Roles;
    //    }



    //}
    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// SalesRep business object class
    ///// </summary>
    //public class SalesRep : BasePR
    //{
    //    private int _RepID = 0;
    //    public int RepID
    //    {
    //        get { return _RepID; }
    //        protected set { _RepID = value; }
    //    }

    //    private string _RepCode = "";
    //    public string RepCode
    //    {
    //        get { return _RepCode; }
    //        set { _RepCode = value; }
    //    }

    //    private string _RepName = "";
    //    public string RepName
    //    {
    //        get { return _RepName; }
    //        set { _RepName = value; }
    //    }

    //    private string _RepComment = "";
    //    public string RepComment
    //    {
    //        get { return _RepComment; }
    //        set { _RepComment = value; }
    //    }


    //    public SalesRep(int RepID, string RepCode, string RepName, string RepComment)
    //    {
    //        this.RepID = RepID;
    //        this.RepCode = RepCode;
    //        this.RepName = RepName;
    //        this.RepComment = RepComment;
    //    }

    //    public bool Delete()
    //    {
    //        bool success = SalesRep.DeleteSalesRep(this.RepID);
    //        if (success)
    //            this.RepID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return SalesRep.UpdateSalesRep(this.RepID, this.RepCode, this.RepName, this.RepComment);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all SalesReps
    //    /// </summary>
    //    public static List<SalesRep> GetSalesReps(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "RepName";

    //        List<SalesRep> SalesReps = null;
    //        string key = "SalesReps_SalesReps_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SalesReps = (List<SalesRep>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SalesRepInfo> recordset = SiteProvider.PR.GetSalesReps(cSortExpression);
    //            SalesReps = GetSalesRepListFromSalesRepInfoList(recordset);
    //            BasePR.CacheData(key, SalesReps);
    //        }
    //        return SalesReps;
    //    }


    //    /// <summary>
    //    /// Returns the number of total SalesReps
    //    /// </summary>
    //    public static int GetSalesRepCount()
    //    {
    //        int SalesRepCount = 0;
    //        string key = "SalesReps_SalesRepCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SalesRepCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            SalesRepCount = SiteProvider.PR.GetSalesRepCount();
    //            BasePR.CacheData(key, SalesRepCount);
    //        }
    //        return SalesRepCount;
    //    }

    //    /// <summary>
    //    /// Returns a SalesRep object with the specified ID
    //    /// </summary>
    //    public static SalesRep GetSalesRepByID(int RepID)
    //    {
    //        SalesRep SalesRep = null;
    //        string key = "SalesReps_SalesRep_" + RepID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SalesRep = (SalesRep)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            SalesRep = GetSalesRepFromSalesRepInfo(SiteProvider.PR.GetSalesRepByID(RepID));
    //            BasePR.CacheData(key, SalesRep);
    //        }
    //        return SalesRep;
    //    }

    //    /// <summary>
    //    /// Updates an existing SalesRep
    //    /// </summary>
    //    public static bool UpdateSalesRep(int RepID, string RepCode, string RepName, string RepComment)
    //    {
    //        RepCode = BizObject.ConvertNullToEmptyString(RepCode);
    //        RepName = BizObject.ConvertNullToEmptyString(RepName);
    //        RepComment = BizObject.ConvertNullToEmptyString(RepComment);


    //        SalesRepInfo record = new SalesRepInfo(RepID, RepCode, RepName, RepComment);
    //        bool ret = SiteProvider.PR.UpdateSalesRep(record);

    //        BizObject.PurgeCacheItems("SalesReps_SalesRep_" + RepID.ToString());
    //        BizObject.PurgeCacheItems("SalesReps_SalesReps");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new SalesRep
    //    /// </summary>
    //    public static int InsertSalesRep(string RepCode, string RepName, string RepComment)
    //    {
    //        RepCode = BizObject.ConvertNullToEmptyString(RepCode);
    //        RepName = BizObject.ConvertNullToEmptyString(RepName);
    //        RepComment = BizObject.ConvertNullToEmptyString(RepComment);


    //        SalesRepInfo record = new SalesRepInfo(0, RepCode, RepName, RepComment);
    //        int ret = SiteProvider.PR.InsertSalesRep(record);

    //        BizObject.PurgeCacheItems("SalesReps_SalesRep");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing SalesRep, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteSalesRep(int RepID)
    //    {
    //        bool IsOKToDelete = OKToDelete(RepID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteSalesRep(RepID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing SalesRep - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteSalesRep(int RepID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteSalesRep(RepID);
    //        //         new RecordDeletedEvent("SalesRep", RepID, null).Raise();
    //        BizObject.PurgeCacheItems("SalesReps_SalesRep");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a SalesRep can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int RepID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a SalesRep object filled with the data taken from the input SalesRepInfo
    //    /// </summary>
    //    private static SalesRep GetSalesRepFromSalesRepInfo(SalesRepInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new SalesRep(record.RepID, record.RepCode, record.RepName, record.RepComment);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of SalesRep objects filled with the data taken from the input list of SalesRepInfo
    //    /// </summary>
    //    private static List<SalesRep> GetSalesRepListFromSalesRepInfoList(List<SalesRepInfo> recordset)
    //    {
    //        List<SalesRep> SalesReps = new List<SalesRep>();
    //        foreach (SalesRepInfo record in recordset)
    //            SalesReps.Add(GetSalesRepFromSalesRepInfo(record));
    //        return SalesReps;
    //    }

    //}

    ////////////////////////////////////////////////////////////
    ///// <summary>
    ///// Sidebar business object class
    ///// </summary>
    //public class Sidebar : BasePR
    //{
    //    private int _SB_ID = 0;
    //    public int SB_ID
    //    {
    //        get { return _SB_ID; }
    //        protected set { _SB_ID = value; }
    //    }

    //    private int _TopicID = 0;
    //    public int TopicID
    //    {
    //        get { return _TopicID; }
    //        set { _TopicID = value; }
    //    }

    //    private string _Title = "";
    //    public string Title
    //    {
    //        get { return _Title; }
    //        set { _Title = value; }
    //    }

    //    private string _Body = "";
    //    public string Body
    //    {
    //        get { return _Body; }
    //        set { _Body = value; }
    //    }

    //    public Sidebar(int SB_ID, int TopicID, string Title, string Body)
    //    {
    //        this.SB_ID = SB_ID;
    //        this.TopicID = TopicID;
    //        this.Title = Title;
    //        this.Body = Body;
    //    }

    //    public bool Delete()
    //    {
    //        bool success = Sidebar.DeleteSidebar(this.SB_ID);
    //        if (success)
    //            this.SB_ID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return Sidebar.UpdateSidebar(this.SB_ID, this.TopicID, this.Title, this.Body);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all Sidebars
    //    /// </summary>
    //    public static List<Sidebar> GetSidebars(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "Title";

    //        List<Sidebar> Sidebars = null;
    //        string key = "Sidebars_Sidebars_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Sidebars = (List<Sidebar>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SidebarInfo> recordset = SiteProvider.PR.GetSidebars(cSortExpression);
    //            Sidebars = GetSidebarListFromSidebarInfoList(recordset);
    //            BasePR.CacheData(key, Sidebars);
    //        }
    //        return Sidebars;
    //    }


    //    /// <summary>
    //    /// Returns the number of total Sidebars
    //    /// </summary>
    //    public static int GetSidebarCount()
    //    {
    //        int SidebarCount = 0;
    //        string key = "Sidebars_SidebarCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SidebarCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            SidebarCount = SiteProvider.PR.GetSidebarCount();
    //            BasePR.CacheData(key, SidebarCount);
    //        }
    //        return SidebarCount;
    //    }

    //    /// <summary>
    //    /// Returns a Sidebar object with the specified ID
    //    /// </summary>
    //    public static Sidebar GetSidebarByID(int SB_ID)
    //    {
    //        Sidebar Sidebar = null;
    //        string key = "Sidebars_Sidebar_" + SB_ID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Sidebar = (Sidebar)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            Sidebar = GetSidebarFromSidebarInfo(SiteProvider.PR.GetSidebarByID(SB_ID));
    //            BasePR.CacheData(key, Sidebar);
    //        }
    //        return Sidebar;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Sidebars associated with the specified TopicID
    //    /// </summary>
    //    public static List<Sidebar> GetSidebarsByTopicID(int TopicID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "Title";

    //        List<Sidebar> Sidebars = null;
    //        string key = "Sidebars_Sidebars_TopicID_" + TopicID.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Sidebars = (List<Sidebar>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SidebarInfo> recordset = SiteProvider.PR.GetSidebarsByTopicID(TopicID, cSortExpression);
    //            Sidebars = GetSidebarListFromSidebarInfoList(recordset);
    //            BasePR.CacheData(key, Sidebars);
    //        }
    //        return Sidebars;
    //    }


    //    /// <summary>
    //    /// Returns the number of total Sidebars associated with the specified TopicID
    //    /// </summary>
    //    public static int GetSidebarCountByTopicID(int TopicID)
    //    {
    //        int SidebarCount = 0;
    //        string key = "Sidebars_SidebarCount_TopicID_" + TopicID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SidebarCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            SidebarCount = SiteProvider.PR.GetSidebarCountByTopicID(TopicID);
    //            BasePR.CacheData(key, SidebarCount);
    //        }
    //        return SidebarCount;
    //    }




    //    /// <summary>
    //    /// Updates an existing Sidebar
    //    /// </summary>
    //    public static bool UpdateSidebar(int SB_ID, int TopicID, string Title, string Body)
    //    {
    //        Title = BizObject.ConvertNullToEmptyString(Title);


    //        SidebarInfo record = new SidebarInfo(SB_ID, TopicID, Title, Body);
    //        bool ret = SiteProvider.PR.UpdateSidebar(record);

    //        BizObject.PurgeCacheItems("Sidebars_Sidebar_" + SB_ID.ToString());
    //        BizObject.PurgeCacheItems("Sidebars_Sidebars");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new Sidebar
    //    /// </summary>
    //    public static int InsertSidebar(int TopicID, string Title, string Body)
    //    {
    //        Title = BizObject.ConvertNullToEmptyString(Title);


    //        SidebarInfo record = new SidebarInfo(0, TopicID, Title, Body);
    //        int ret = SiteProvider.PR.InsertSidebar(record);

    //        BizObject.PurgeCacheItems("Sidebars_Sidebar");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing Sidebar, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteSidebar(int SB_ID)
    //    {
    //        bool IsOKToDelete = OKToDelete(SB_ID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteSidebar(SB_ID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing Sidebar - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteSidebar(int SB_ID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteSidebar(SB_ID);
    //        //         new RecordDeletedEvent("Sidebar", SB_ID, null).Raise();
    //        BizObject.PurgeCacheItems("Sidebars_Sidebar");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a Sidebar can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom Body in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int SB_ID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a Sidebar object filled with the data taken from the input SidebarInfo
    //    /// </summary>
    //    private static Sidebar GetSidebarFromSidebarInfo(SidebarInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new Sidebar(record.SB_ID, record.TopicID, record.Title, record.Body);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of Sidebar objects filled with the data taken from the input list of SidebarInfo
    //    /// </summary>
    //    private static List<Sidebar> GetSidebarListFromSidebarInfoList(List<SidebarInfo> recordset)
    //    {
    //        List<Sidebar> Sidebars = new List<Sidebar>();
    //        foreach (SidebarInfo record in recordset)
    //            Sidebars.Add(GetSidebarFromSidebarInfo(record));
    //        return Sidebars;
    //    }

    //}




    ////////////////////////////////////////////////////////////
    /// <summary>
    /// Snippet business object class
    /// </summary>
    //public class Snippet : BasePR
    //{
    //    private int _SnippetID = 0;
    //    public int SnippetID
    //    {
    //        get { return _SnippetID; }
    //        protected set { _SnippetID = value; }
    //    }

    //    private string _SnippetKey = "";
    //    public string SnippetKey
    //    {
    //        get { return _SnippetKey; }
    //        set { _SnippetKey = value; }
    //    }

    //    private string _SnippetText = "";
    //    public string SnippetText
    //    {
    //        get { return _SnippetText; }
    //        set { _SnippetText = value; }
    //    }

    //    public Snippet(int SnippetID, string SnippetKey, string SnippetText)
    //  {
    //        this.SnippetID = SnippetID;
    //        this.SnippetKey = SnippetKey;
    //        this.SnippetText = SnippetText;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = Snippet.DeleteSnippet(this.SnippetID);
    //        if (success)
    //            this.SnippetID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return Snippet.UpdateSnippet(this.SnippetID, this.SnippetKey, this.SnippetText);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all Snippets
    //    /// </summary>
    //    public static List<Snippet> GetSnippets(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "SnippetKey";

    //        List<Snippet> Snippets = null;
    //        string key = "Snippets_Snippets_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Snippets = (List<Snippet>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SnippetInfo> recordset = SiteProvider.PR.GetSnippets(cSortExpression);
    //            Snippets = GetSnippetListFromSnippetInfoList(recordset);
    //            BasePR.CacheData(key, Snippets);
    //        }
    //        return Snippets;
    //    }


    //    /// <summary>
    //    /// Returns the number of total Snippets
    //    /// </summary>
    //    public static int GetSnippetCount()
    //    {
    //        int SnippetCount = 0;
    //        string key = "Snippets_SnippetCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SnippetCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            SnippetCount = SiteProvider.PR.GetSnippetCount();
    //            BasePR.CacheData(key, SnippetCount);
    //        }
    //        return SnippetCount;
    //    }

    //    /// <summary>
    //    /// Returns a Snippet object with the specified ID
    //    /// </summary>
    //    public static Snippet GetSnippetByID(int SnippetID)
    //    {
    //        Snippet Snippet = null;
    //        string key = "Snippets_Snippet_" + SnippetID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Snippet = (Snippet)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            Snippet = GetSnippetFromSnippetInfo(SiteProvider.PR.GetSnippetByID(SnippetID));
    //            BasePR.CacheData(key, Snippet);
    //        }
    //        return Snippet;
    //    }

    //    /// <summary>
    //    /// Returns a Snippet object with the specified MessageKey value
    //    /// </summary>
    //    public static Snippet GetSnippetBySnippetKey(string SnippetKey)
    //    {
    //        Snippet Snippet = null;
    //        string key = "Snippets_Snippet_" + SnippetKey.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Snippet = (Snippet)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            Snippet = GetSnippetFromSnippetInfo(SiteProvider.PR.GetSnippetBySnippetKey(SnippetKey));
    //            BasePR.CacheData(key, Snippet);
    //        }
    //        return Snippet;
    //    }

    //    /// <summary>
    //    /// Updates an existing Snippet
    //    /// </summary>
    //    public static bool UpdateSnippet(int SnippetID, string SnippetKey, string SnippetText)
    //    {
    //        SnippetText = BizObject.ConvertNullToEmptyString(SnippetText);


    //        SnippetInfo record = new SnippetInfo(SnippetID, SnippetKey, SnippetText);
    //        bool ret = SiteProvider.PR.UpdateSnippet(record);

    //        BizObject.PurgeCacheItems("Snippets_Snippet_" + SnippetID.ToString());
    //        BizObject.PurgeCacheItems("Snippets_Snippets");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new Snippet
    //    /// </summary>
    //    public static int InsertSnippet(string SnippetKey, string SnippetText)
    //    {
    //        SnippetText = BizObject.ConvertNullToEmptyString(SnippetText);


    //        SnippetInfo record = new SnippetInfo(0, SnippetKey, SnippetText);
    //        int ret = SiteProvider.PR.InsertSnippet(record);

    //        BizObject.PurgeCacheItems("Snippets_Snippet");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing Snippet, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteSnippet(int SnippetID)
    //    {
    //        bool IsOKToDelete = OKToDelete(SnippetID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteSnippet(SnippetID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing Snippet - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteSnippet(int SnippetID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteSnippet(SnippetID);
    //        //         new RecordDeletedEvent("Snippet", InterestID, null).Raise();
    //        BizObject.PurgeCacheItems("Snippets_Snippet");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a Snippet can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int SnippetID)
    //    {
    //        return true;
    //    }


    //    /// <summary>
    //    /// Returns a Snippet object filled with the data taken from the input SnippetInfo
    //    /// </summary>
    //    private static Snippet GetSnippetFromSnippetInfo(SnippetInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new Snippet(record.SnippetID, record.SnippetKey, record.SnippetText);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of Snippet objects filled with the data taken from the input list of SnippetInfo
    //    /// </summary>
    //    private static List<Snippet> GetSnippetListFromSnippetInfoList(List<SnippetInfo> recordset)
    //    {
    //        List<Snippet> Snippets = new List<Snippet>();
    //        foreach (SnippetInfo record in recordset)
    //            Snippets.Add(GetSnippetFromSnippetInfo(record));
    //        return Snippets;
    //    }

    //}

    ////////////////////////////////////////////////////////////
    ///// <summary>
    ///// Specialty business object class
    ///// </summary>
    //public class Specialty : BasePR
    //{
    //    private int _SpecID = 0;
    //    public int SpecID
    //    {
    //        get { return _SpecID; }
    //        protected set { _SpecID = value; }
    //    }

    //    private string _SpecTitle = "";
    //    public string SpecTitle
    //    {
    //        get { return _SpecTitle; }
    //        set { _SpecTitle = value; }
    //    }



    //   public Specialty(int SpecID, string SpecTitle)
    //  {
    //        this.SpecID = SpecID;
    //        this.SpecTitle = SpecTitle;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = Specialty.DeleteSpecialty(this.SpecID);
    //        if (success)
    //            this.SpecID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return Specialty.UpdateSpecialty(this.SpecID, this.SpecTitle);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all Specialties
    //    /// </summary>
    //    public static List<Specialty> GetSpecialties(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "SpecTitle";

    //        List<Specialty> Specialties = null;
    //        string key = "Specialties_Specialties_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Specialties = (List<Specialty>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SpecialtyInfo> recordset = SiteProvider.PR.GetSpecialties(cSortExpression);
    //            Specialties = GetSpecialtyListFromSpecialtyInfoList(recordset);
    //            BasePR.CacheData(key, Specialties);
    //        }
    //        return Specialties;
    //    }


    //    /// <summary>
    //    /// Returns the number of total Specialties
    //    /// </summary>
    //    public static int GetSpecialtyCount()
    //    {
    //        int SpecialtyCount = 0;
    //        string key = "Specialties_SpecialtyCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SpecialtyCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            SpecialtyCount = SiteProvider.PR.GetSpecialtyCount();
    //            BasePR.CacheData(key, SpecialtyCount);
    //        }
    //        return SpecialtyCount;
    //    }

    //    /// <summary>
    //    /// Returns a Specialty object with the specified ID
    //    /// </summary>
    //    public static Specialty GetSpecialtyByID(int SpecID)
    //    {
    //        Specialty Specialty = null;
    //        string key = "Specialties_Specialty_" + SpecID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Specialty = (Specialty)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            Specialty = GetSpecialtyFromSpecialtyInfo(SiteProvider.PR.GetSpecialtyByID(SpecID));
    //            BasePR.CacheData(key, Specialty);
    //        }
    //        return Specialty;
    //    }

    //    /// <summary>
    //    /// Updates an existing Specialty
    //    /// </summary>
    //    public static bool UpdateSpecialty(int SpecID, string SpecTitle)
    //    {
    //        SpecTitle = BizObject.ConvertNullToEmptyString(SpecTitle);


    //        SpecialtyInfo record = new SpecialtyInfo(SpecID, SpecTitle);
    //        bool ret = SiteProvider.PR.UpdateSpecialty(record);

    //        BizObject.PurgeCacheItems("Specialties_Specialty_" + SpecID.ToString());
    //        BizObject.PurgeCacheItems("Specialties_Specialties");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new Specialty
    //    /// </summary>
    //    public static int InsertSpecialty(string SpecTitle)
    //    {
    //        SpecTitle = BizObject.ConvertNullToEmptyString(SpecTitle);


    //        SpecialtyInfo record = new SpecialtyInfo(0, SpecTitle);
    //        int ret = SiteProvider.PR.InsertSpecialty(record);

    //        BizObject.PurgeCacheItems("Specialties_Specialty");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing Specialty, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteSpecialty(int SpecID)
    //    {
    //        bool IsOKToDelete = OKToDelete(SpecID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteSpecialty(SpecID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing Specialty - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteSpecialty(int SpecID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteSpecialty(SpecID);
    //        //         new RecordDeletedEvent("Specialty", SpecID, null).Raise();
    //        BizObject.PurgeCacheItems("Specialties_Specialty");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a Specialty can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int SpecID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a Specialty object filled with the data taken from the input SpecialtyInfo
    //    /// </summary>
    //    private static Specialty GetSpecialtyFromSpecialtyInfo(SpecialtyInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new Specialty(record.SpecID, record.SpecTitle);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of Specialty objects filled with the data taken from the input list of SpecialtyInfo
    //    /// </summary>
    //    private static List<Specialty> GetSpecialtyListFromSpecialtyInfoList(List<SpecialtyInfo> recordset)
    //    {
    //        List<Specialty> Specialties = new List<Specialty>();
    //        foreach (SpecialtyInfo record in recordset)
    //            Specialties.Add(GetSpecialtyFromSpecialtyInfo(record));
    //        return Specialties;
    //    }



    //}
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// SurveyDefinition business object class
    /// </summary>
    //public class SurveyDefinition : BasePR
    //{
    //    private int _SurveyID = 0;
    //    public int SurveyID
    //    {
    //        get { return _SurveyID; }
    //        protected set { _SurveyID = value; }
    //    }

    //    private string _SurveyName = "";
    //    public string SurveyName
    //    {
    //        get { return _SurveyName; }
    //        set { _SurveyName = value; }
    //    }

    //    private string _XMLSurvey = "";
    //    public string XMLSurvey
    //    {
    //        get { return _XMLSurvey; }
    //        set { _XMLSurvey = value; }
    //    }

    //    private int _Version = 0;
    //    public int Version
    //    {
    //        get { return _Version; }
    //        set { _Version = value; }
    //    }

    //    private int _FacilityID = 0;
    //    public int FacilityID
    //    {
    //        get { return _FacilityID; }
    //        set { _FacilityID = value; }
    //    }


    //   public SurveyDefinition(int SurveyID, string SurveyName, string XMLSurvey, int Version, int FacilityID)
    //  {
    //        this.SurveyID = SurveyID;
    //        this.SurveyName = SurveyName;	
    //       this.XMLSurvey = XMLSurvey;
    //        this.Version = Version;
    //        this.FacilityID = FacilityID;
    //   }

    //    public bool Delete()
    //    {
    //        bool success = SurveyDefinition.DeleteSurveyDefinition(this.SurveyID);
    //        if (success)
    //            this.SurveyID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return SurveyDefinition.UpdateSurveyDefinition(this.SurveyID, this.SurveyName, this.XMLSurvey, 
    //            this.Version, this.FacilityID);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all SurveyDefinitions
    //    /// </summary>
    //    public static List<SurveyDefinition> GetSurveyDefinitions(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "SurveyName";

    //        List<SurveyDefinition> SurveyDefinitions = null;
    //        string key = "SurveyDefinitions_SurveyDefinitions_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SurveyDefinitions = (List<SurveyDefinition>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<SurveyDefinitionInfo> recordset = SiteProvider.PR.GetSurveyDefinitions(cSortExpression);
    //            SurveyDefinitions = GetSurveyDefinitionListFromSurveyDefinitionInfoList(recordset);
    //            BasePR.CacheData(key, SurveyDefinitions);
    //        }
    //        return SurveyDefinitions;
    //    }
                

    //    /// <summary>
    //    /// Returns the number of total SurveyDefinitions
    //    /// </summary>
    //    public static int GetSurveyDefinitionCount()
    //    {
    //        int SurveyDefinitionCount = 0;
    //        string key = "SurveyDefinitions_SurveyDefinitionCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SurveyDefinitionCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            SurveyDefinitionCount = SiteProvider.PR.GetSurveyDefinitionCount();
    //            BasePR.CacheData(key, SurveyDefinitionCount);
    //        }
    //        return SurveyDefinitionCount;
    //    }

    //    /// <summary>
    //    /// Returns a SurveyDefinition object with the specified ID
    //    /// </summary>
    //    public static SurveyDefinition GetSurveyDefinitionByID(int SurveyID)
    //    {
    //        SurveyDefinition SurveyDefinition = null;
    //        string key = "SurveyDefinitions_SurveyDefinition_" + SurveyID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SurveyDefinition = (SurveyDefinition)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            SurveyDefinition = GetSurveyDefinitionFromSurveyDefinitionInfo(SiteProvider.PR.GetSurveyDefinitionByID(SurveyID));
    //            BasePR.CacheData(key, SurveyDefinition);
    //        }
    //        return SurveyDefinition;
    //    }

    //    /// <summary>
    //    /// Bhaskar 
    //    /// Returns a TestDefinition object associated with the specified TopicID
    //    /// </summary>
    //    public static SurveyDefinition GetSurveyDefinitionByTopicID(int TopicID)
    //    {
    //        SurveyDefinition SurveyDefinition = null;
    //        string key = "SurveyDefinitions_SurveyDefinition_TopicID_" + TopicID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            SurveyDefinition = (SurveyDefinition)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            SurveyDefinition = GetSurveyDefinitionFromSurveyDefinitionInfo(SiteProvider.PR.GetSurveyDefinitionByTopicID(TopicID));
    //            BasePR.CacheData(key, SurveyDefinition);
    //        }
    //        return SurveyDefinition;
    //    }

    //    /// <summary>
    //    /// Updates an existing SurveyDefinition
    //    /// </summary>
    //    public static bool UpdateSurveyDefinition(int SurveyID, string SurveyName, string XMLSurvey, 
    //        int Version, int FacilityID)
    //    {
    //        XMLSurvey = BizObject.ConvertNullToEmptyString(XMLSurvey);


    //        SurveyDefinitionInfo record = new SurveyDefinitionInfo(SurveyID, SurveyName, XMLSurvey, Version, FacilityID);
    //        bool ret = SiteProvider.PR.UpdateSurveyDefinition(record);

    //        BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinition_" + SurveyID.ToString());
    //        BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinitions");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new SurveyDefinition
    //    /// </summary>
    //    public static int InsertSurveyDefinition(string SurveyName, string XMLSurvey, int Version, int FacilityID)
    //    {
    //        SurveyName = BizObject.ConvertNullToEmptyString(SurveyName);
    //        XMLSurvey = BizObject.ConvertNullToEmptyString(XMLSurvey);


    //        SurveyDefinitionInfo record = new SurveyDefinitionInfo(0, SurveyName, XMLSurvey, Version, 0);
    //        int ret = SiteProvider.PR.InsertSurveyDefinition(record);

    //        BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinition");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing SurveyDefinition, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteSurveyDefinition(int SurveyID)
    //    {
    //        bool IsOKToDelete = OKToDelete(SurveyID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteSurveyDefinition(SurveyID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Updates an existing SurveyDefinition
    //    /// </summary>
    //    public static bool UpdateSurveyDefinition(int SurveyID, string XMLSurvey, int Version, int FacilityID)
    //    {
    //        XMLSurvey = BizObject.ConvertNullToEmptyString(XMLSurvey);


    //        //SurveyDefinitionInfo record = new SurveyDefinitionInfo(SurveyID, XMLSurvey, Version, FacilityID);
    //        bool ret = SiteProvider.PR.UpdateSurveyDefinition(SurveyID, XMLSurvey, Version, FacilityID);

    //        BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinition1_" + SurveyID.ToString());
    //        BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinitions");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing SurveyDefinition - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteSurveyDefinition(int SurveyID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteSurveyDefinition(SurveyID);
    //        //         new RecordDeletedEvent("SurveyDefinition", SurveyID, null).Raise();
    //        BizObject.PurgeCacheItems("SurveyDefinitions_SurveyDefinition");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a SurveyDefinition can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int SurveyID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a SurveyDefinition object filled with the data taken from the input SurveyDefinitionInfo
    //    /// </summary>
    //    private static SurveyDefinition GetSurveyDefinitionFromSurveyDefinitionInfo(SurveyDefinitionInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new SurveyDefinition(record.SurveyID, record.SurveyName, record.XMLSurvey, 
    //                record.Version, record.FacilityID);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of SurveyDefinition objects filled with the data taken from the input list of SurveyDefinitionInfo
    //    /// </summary>
    //    private static List<SurveyDefinition> GetSurveyDefinitionListFromSurveyDefinitionInfoList(List<SurveyDefinitionInfo> recordset)
    //    {
    //        List<SurveyDefinition> SurveyDefinitions = new List<SurveyDefinition>();
    //        foreach (SurveyDefinitionInfo record in recordset)
    //            SurveyDefinitions.Add(GetSurveyDefinitionFromSurveyDefinitionInfo(record));
    //        return SurveyDefinitions;
    //    }



    //}
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// Test business object class
    /// </summary>
//    public class Test : BasePR
//    {
//        private int _TestID = 0;
//        public int TestID
//        {
//            get { return _TestID; }
//            set { _TestID = value; }
//        }

//        private string _OldTestID = "";
//        public string OldTestID
//        {
//            get { return _OldTestID; }
//            set { _OldTestID = value; }
//        }

//        private int _TopicID = 0;
//        public int TopicID
//        {
//            get { return _TopicID; }
//            set { _TopicID = value; }
//        }

//        //private string _Subtitle;
//        //public string Subtitle
//        //{
//        //    get { return _Subtitle; }
//        //    private set { _Subtitle = value; }
//        //}

//        //private string _Course_Number;
//        //public string Course_Number
//        //{
//        //    get { return _Course_Number; }
//        //    private set { _Course_Number = value; }
//        //}

//        private int _UserID = 0;
//        public int UserID
//        {
//            get { return _UserID; }
//            set { _UserID = value; }
//        }

//        private DateTime _BuyDate = System.DateTime.Now;
//        public DateTime BuyDate
//        {
//            get { return _BuyDate; }
//            set { _BuyDate = value; }
//        }

//        private decimal _Credits = 0.00M;
//        public decimal Credits
//        {
//            get { return _Credits; }
//            set { _Credits = value; }
//        }

//        private DateTime _LastMod = System.DateTime.Now;
//        public DateTime LastMod
//        {
//            get { return _LastMod; }
//            set { _LastMod = value; }
//        }

//        private string _Name = "";
//        public string Name
//        {
//            get { return _Name; }
//            set { _Name = value; }
//        }

//        private bool _NoSurvey = false;
//        public bool NoSurvey
//        {
//            get { return _NoSurvey; }
//            set { _NoSurvey = value; }
//        }

//        private string _Status = "";
//        public string Status
//        {
//            get { return _Status; }
//            set { _Status = value; }
//        }

//        private string _UserName = "";
//        public string UserName
//        {
//            get { return _UserName; }
//            set { _UserName = value; }
//        }

//        private string _XMLTest = "";
//        public string XMLTest
//        {
//            get { return _XMLTest; }
//            set { _XMLTest = value; }
//        }

//        private string _XMLSurvey = "";
//        public string XMLSurvey
//        {
//            get { return _XMLSurvey; }
//            set { _XMLSurvey = value; }
//        }

//        private int _TestVers = 0;
//        public int TestVers
//        {
//            get { return _TestVers; }
//            set { _TestVers = value; }
//        }

//        private int _LectEvtID = 0;
//        public int LectEvtID
//        {
//            get { return _LectEvtID; }
//            set { _LectEvtID = value; }
//        }

//        private bool _SurveyComplete = false;
//        public bool SurveyComplete
//        {
//            get { return _SurveyComplete; }
//            set { _SurveyComplete = value; }
//        }

//        private bool _VignetteComplete = false;
//        public bool VignetteComplete
//        {
//            get { return _VignetteComplete; }
//            set { _VignetteComplete = value; }
//        }

//        //private string _XMLVignette = "";
//        //public string XMLVignette
//        //{
//        //    get { return _XMLVignette; }
//        //    set { _XMLVignette = value; }
//        //}

//        //private int _VignetteVersion = 0;
//        //public int VignetteVersion
//        //{
//        //    get { return _VignetteVersion; }
//        //    set { _VignetteVersion = value; }
//        //}

//        //private int _VignetteScore = 0;
//        //public int VignetteScore
//        //{
//        //    get { return _VignetteScore; }
//        //    set { _VignetteScore = value; }
//        //}

//        private int _Score = 0;
//        public int Score
//        {
//            get { return _Score; }
//            set { _Score = value; }
//        }

//        //private DateTime _Printed_Date = System.DateTime.Now;
//        //public DateTime Printed_Date
//        //{
//        //    get { return _Printed_Date; }
//        //    set { _Printed_Date = value; }
//        //}

//        //private DateTime _Emailed_Date = System.DateTime.Now;
//        //public DateTime Emailed_Date
//        //{
//        //    get { return _Emailed_Date; }
//        //    set { _Emailed_Date = value; }
//        //}

//        //private DateTime _Reported_Date = System.DateTime.Now;
//        //public DateTime Reported_Date
//        //{
//        //    get { return _Reported_Date; }
//        //    set { _Reported_Date = value; }
//        //}

//        private DateTime _View_Date = System.DateTime.Now;
//        public DateTime View_Date
//        {
//            get { return _View_Date; }
//            set { _View_Date = value; }
//        }

//        private int _UniqueID = 0;
//        public int UniqueID
//        {
//            get { return _UniqueID; }
//            set { _UniqueID = value; }
//        }

//        private int _AlterID = 0;
//        public int AlterID
//        {
//            get { return _AlterID; }
//            set { _AlterID = value; }
//        }

//        private bool _GCCharged = false;
//        public bool GCCharged
//        {
//            get { return _GCCharged; }
//            set { _GCCharged = value; }
//        }
//        private int _facilityid = 0;
//        public int facilityid
//        {
//            get { return _facilityid; }
//            set { _facilityid = value; }
//        }

//        public Test(int TestID, string OldTestID, int TopicID, int UserID, DateTime BuyDate,
//            decimal Credits, DateTime LastMod, string Name, bool NoSurvey, string Status, string UserName,
//            string XMLTest, string XMLSurvey, int TestVers, int LectEvtID, bool SurveyComplete,
//            bool VignetteComplete,
//            //string XMLVignette, int VignetteVersion, int VignetteScore,
//            int Score,
//            //DateTime Printed_Date,
//            //DateTime Emailed_Date, DateTime Reported_Date,
//            DateTime View_Date, int UniqueID, int AlterID, bool GCCharged)
//      {
//          this.TestID = TestID;
//          this.OldTestID = OldTestID;
//          this.TopicID = TopicID;
//          //this.Subtitle = Subtitle;
//          //this.Course_Number = Course_Number;
//          this.UserID = UserID;
//          this.BuyDate = BuyDate;
//          this.Credits = Credits;
//          this.LastMod = LastMod;
//          this.Name = Name;
//          this.NoSurvey = NoSurvey;
//          this.Status = Status;
//          this.UserName = UserName;
//          this.XMLTest = XMLTest;
//          this.XMLSurvey = XMLSurvey;
//          this.TestVers = TestVers;
//          this.LectEvtID = LectEvtID;
//          this.SurveyComplete = SurveyComplete;
//          this.VignetteComplete = VignetteComplete;
//          //this.VignetteVersion = VignetteVersion;
//          //this.XMLVignette = XMLVignette;
//          //this.VignetteScore = VignetteScore;
//          this.Score = Score;
//          //this.Printed_Date = Printed_Date;
//          //this.Emailed_Date = Emailed_Date;
//          //this.Reported_Date = Reported_Date;
//          this.View_Date = View_Date;
//          this.UniqueID = UniqueID;
//          this.AlterID = AlterID;
//          this.GCCharged = GCCharged;
          
//      }

//        public bool Delete()
//        {
//            bool success = Test.DeleteTest(this.TestID);
//            if (success)
//                this.TestID = 0;
//            return success;
//        }

//        public bool Update()
//        {
//            return Test.UpdateTest(this.TestID, this.OldTestID, this.TopicID, this.UserID, this.BuyDate, 
//                this.Credits, this.LastMod, this.Name, this.NoSurvey, this.Status, this.UserName,
//                this.XMLTest, this.XMLSurvey, this.TestVers, this.LectEvtID, this.SurveyComplete,
//                this.VignetteComplete,
//                //this.XMLVignette, this.VignetteVersion, this.VignetteScore,
//                this.Score,
//                //this.Printed_Date,
//                //this.Emailed_Date, this.Reported_Date,
//                this.View_Date, this.UniqueID, this.AlterID, this.GCCharged);
//        }

//        /***********************************
//        * Static methods
//        ************************************/

//        /// <summary>
//        /// Returns a collection with all Tests
//        /// </summary>
//        public static List<Test> GetTests(string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "LastMod";

//            List<Test> Tests = null;
//            string key = "Tests_Tests_" + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Tests = (List<Test>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<TestInfo> recordset = SiteProvider.PR.GetTests(cSortExpression);
//                Tests = GetTestListFromTestInfoList(recordset);
//                BasePR.CacheData(key, Tests);
//            }
//            return Tests;
//        }


//        /// <summary>
//        /// Returns the number of total Tests
//        /// </summary>
//        public static int GetTestCount()
//        {
//            int TestCount = 0;
//            string key = "Tests_TestCount";

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                TestCount = (int)BizObject.Cache[key];
//            }
//            else
//            {
//                TestCount = SiteProvider.PR.GetTestCount();
//                BasePR.CacheData(key, TestCount);
//            }
//            return TestCount;
//        }

//        /// <summary>
//        /// Returns the number of Completed Tests for a FacilityID
//        /// </summary>
//        public static int GetCompletedTestCountByFacilityID(int FacilityID)
//        {
//            int TestCount = 0;
//            string key = "Tests_CompletedTestCountByFacilityID_" + FacilityID.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                TestCount = (int)BizObject.Cache[key];
//            }
//            else
//            {
//                TestCount = SiteProvider.PR.GetCompletedTestCountByFacilityID(FacilityID);
//                BasePR.CacheData(key, TestCount);
//            }
//            return TestCount;
//        }

//        /// <summary>
//        /// Returns a Test object with the specified ID
//        /// </summary>
//        public static Test GetTestByID(int TestID)
//        {
//            Test Test = null;
//            string key = "Tests_Test_" + TestID.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Test = (Test)BizObject.Cache[key];
//            }
//            else
//            {
//                Test = GetTestFromTestInfo(SiteProvider.PR.GetTestByID(TestID));
//                BasePR.CacheData(key, Test);
//            }
//            return Test;
//        }

       

//        /// <summary>
//        /// Updates an existing Test
//        /// </summary>
//        public static bool UpdateTest(int TestID, string OldTestID, int TopicID, int UserID, DateTime BuyDate,
//            decimal Credits, DateTime LastMod, string Name, bool NoSurvey, string Status, string UserName,
//            string XMLTest, string XMLSurvey, int TestVers, int LectEvtID, bool SurveyComplete,
//           bool VignetteComplete,
//            //string XMLVignette, int VignetteVersion, int VignetteScore,
//            int Score,
//            //DateTime Printed_Date,
//            //DateTime Emailed_Date, DateTime Reported_Date,
//            DateTime View_Date, int UniqueID, int AlterID, bool GCCharged)
//        {
//            OldTestID = BizObject.ConvertNullToEmptyString(OldTestID);
//            Name = BizObject.ConvertNullToEmptyString(Name);
//            Status = BizObject.ConvertNullToEmptyString(Status);
//            UserName = BizObject.ConvertNullToEmptyString(UserName);
//            XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);
//            XMLSurvey = BizObject.ConvertNullToEmptyString(XMLSurvey);
//            //XMLVignette = BizObject.ConvertNullToEmptyString(XMLVignette);


//            TestInfo record = new TestInfo(TestID, OldTestID, TopicID, UserID, BuyDate,
//                Credits, LastMod, Name, NoSurvey, Status, UserName,
//                XMLTest, XMLSurvey, TestVers, LectEvtID, SurveyComplete,
//                VignetteComplete,
//                //XMLVignette, VignetteVersion, VignetteScore,
//                Score,
//                //Printed_Date,
//                //Emailed_Date, Reported_Date,
//                View_Date, UniqueID, AlterID, GCCharged);
//            bool ret = SiteProvider.PR.UpdateTest(record);

//            BizObject.PurgeCacheItems("Tests_Test_" + TestID.ToString());
//            BizObject.PurgeCacheItems("Tests_Tests");
//            return ret;
//        }

//        /// <summary>
//        /// Updates all Tests' TopicName for a certain TopicID
//        /// </summary>
//        public static bool UpdateTestTopicNameByTopicID(int TopicID, string TopicName)
//        {
//            TopicName = BizObject.ConvertNullToEmptyString(TopicName);
//            bool ret = SiteProvider.PR.UpdateTestTopicNameByTopicID(TopicID, TopicName);

//            BizObject.PurgeCacheItems("Tests_Tests");
//            return ret;
//        }

//        /// <summary>
//        /// Creates a new Test
//        /// </summary>
//        public static int InsertTest(string OldTestID, int TopicID, int UserID, DateTime BuyDate,
//            decimal Credits, DateTime LastMod, string Name, bool NoSurvey, string Status, string UserName,
//            string XMLTest, string XMLSurvey, int TestVers, int LectEvtID, bool SurveyComplete,
//           bool VignetteComplete,
//            //string XMLVignette, int VignetteVersion, int VignetteScore,
//            int Score,
//            //DateTime Printed_Date,
//            //DateTime Emailed_Date, DateTime Reported_Date,
//            DateTime View_Date, int UniqueID, int AlterID, bool GCCharged)
//        {
//            OldTestID = BizObject.ConvertNullToEmptyString(OldTestID);
//            Name = BizObject.ConvertNullToEmptyString(Name);
//            Status = BizObject.ConvertNullToEmptyString(Status);
//            UserName = BizObject.ConvertNullToEmptyString(UserName);
//            XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);
//            XMLSurvey = BizObject.ConvertNullToEmptyString(XMLSurvey);
//            //XMLVignette = BizObject.ConvertNullToEmptyString(XMLVignette);

//            TestInfo record = new TestInfo(0, OldTestID, TopicID, UserID, BuyDate,
//                Credits, LastMod, Name, NoSurvey, Status, UserName,
//                XMLTest, XMLSurvey, TestVers, LectEvtID, SurveyComplete,
//                VignetteComplete,
//                //XMLVignette, VignetteVersion, VignetteScore,
//                Score,
//                //Printed_Date,
//                //Emailed_Date, Reported_Date,
//                View_Date, UniqueID, AlterID, GCCharged);
//            int ret = SiteProvider.PR.InsertTest(record);

//            BizObject.PurgeCacheItems("Tests_Test");
//            return ret;
//        }

//        /// <summary>
//        /// Deletes an existing Test, but first checks if OK to delete
//        /// </summary>
//        public static bool DeleteTest(int TestID)
//        {
//            bool IsOKToDelete = OKToDelete(TestID);
//            if (IsOKToDelete)
//            {
//                return (bool)DeleteTest(TestID, true);
//            }
//            else
//            {
//                return false;
//            }
//        }

//        /// <summary>
//        /// Deletes an existing Test - second param forces skip of OKToDelete
//        /// (assuming that the calling program has already called that if it's
//        /// passing the second param as true)
//        /// </summary>
//        public static bool DeleteTest(int TestID, bool SkipOKToDelete)
//        {
//            if (!SkipOKToDelete)
//                return false;

//            bool ret = SiteProvider.PR.DeleteTest(TestID);
//            //         new RecordDeletedEvent("Test", TestID, null).Raise();
//            BizObject.PurgeCacheItems("Tests_Test");
//            return ret;
//        }



//        /// <summary>
//        /// Checks to see if a Test can be deleted safely
//        /// (This default method just returns true. Certain entities
//        /// might set datadict_tables.lNoOKDel to eliminate this
//        /// default method and provide a custom version in
//        /// datadict_tables.boExtra)
//        /// </summary>
//        public static bool OKToDelete(int TestID)
//        {
//            return true;
//        }



//        /// <summary>
//        /// Returns a Test object filled with the data taken from the input TestInfo
//        /// </summary>
//        private static Test GetTestFromTestInfo(TestInfo record)
//        {
//            if (record == null)
//                return null;
//            else
//            {
//                return new Test(record.TestID, record.OldTestID, record.TopicID, record.UserID, record.BuyDate,
//                record.Credits, record.LastMod, record.Name, record.NoSurvey, record.Status, record.UserName,
//                record.XMLTest, record.XMLSurvey, record.TestVers, record.LectEvtID, record.SurveyComplete,
//                record.VignetteComplete,
//                //record.XMLVignette, record.VignetteVersion, record.VignetteScore,
//                record.Score,
//                //record.Printed_Date,
//                //record.Emailed_Date, record.Reported_Date, 
//                record.View_Date, record.UniqueID, record.AlterID, record.GCCharged);
//            }
//        }

//        /// <summary>
//        /// Returns a list of Test objects filled with the data taken from the input list of TestInfo
//        /// </summary>
//        private static List<Test> GetTestListFromTestInfoList(List<TestInfo> recordset)
//        {
//            List<Test> Tests = new List<Test>();
//            foreach (TestInfo record in recordset)
//                Tests.Add(GetTestFromTestInfo(record));
//            return Tests;
//        }

//        /// <summary>
//        /// Returns a collection with all Tests for the specified UserName
//        /// </summary>
//        public static List<Test> GetTestsByUserName(string UserName, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "LastMod DESC";

//            List<Test> Tests = null;
//            string key = "Tests_Tests_" + UserName.ToString() + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Tests = (List<Test>)BizObject.Cache[key];
//            }
//            else
//            {
//                // "normal" Tests by category ID
//                List<TestInfo> recordset = SiteProvider.PR.GetTestsByUserName(UserName);
//                Tests = GetTestListFromTestInfoList(recordset);
//                BasePR.CacheData(key, Tests);

//            }
//            return Tests;
//        }

//        /// <summary>
//        /// Returns a collection with Completed Tests for the specified UserName
//        /// </summary>
//        public static List<Test> GetCompletedTestsByUserName(string UserName, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "LastMod DESC";

//            List<Test> Tests = null;
//            string key = "Tests_CompletedTests_" + UserName.ToString() + cSortExpression.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Tests = (List<Test>)BizObject.Cache[key];
//            }
//            else
//            {
//                // "normal" Tests by category ID
//                List<TestInfo> recordset = SiteProvider.PR.GetCompletedTestsByUserName(UserName);
//                Tests = GetTestListFromTestInfoList(recordset);
//                BasePR.CacheData(key, Tests);

//            }
//            return Tests;
//        }

//        /// <summary>
//        /// Returns a collection with Incomplete Tests for the specified UserName
//        /// </summary>
//        public static List<Test> GetIncompleteTestsByUserName(string UserName, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "LastMod DESC";

//            List<Test> Tests = null;
////            string key = "Tests_IncompleteTests_" + UserName.ToString() + cSortExpression.ToString();

////            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
////            {
////                Tests = (List<Test>)BizObject.Cache[key];
////            }
////            else
////            {
//                // don't query if username is blank

//            if (UserName.Trim().Length > 0)
//            {
//                List<TestInfo> recordset = SiteProvider.PR.GetIncompleteTestsByUserName(UserName);
//                Tests = GetTestListFromTestInfoList(recordset);
////                BasePR.CacheData(key, Tests);
//            }
////            }
//            return Tests;
//        }

//        /// <summary>
//        /// Returns a collection with all Tests for the specified UserID
//        /// </summary>
//        public static List<Test> GetTestsByUserID(int UserID, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "LastMod DESC";

//            List<Test> Tests = null;
//            //string key = "Tests_Tests_" + UserID.ToString() + cSortExpression.ToString();

//            //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            //{
//            //    Tests = (List<Test>)BizObject.Cache[key];
//            //}
//            //else
//            //{
//                List<TestInfo> recordset = SiteProvider.PR.GetTestsByUserID(UserID);
//                Tests = GetTestListFromTestInfoList(recordset);
//                //BasePR.CacheData(key, Tests);

//            //}
//            return Tests;
//        }

//        /// <summary>
//        /// Returns a collection with Completed Tests for the specified UserID
//        /// </summary>
//        public static System.Data.DataSet GetCompletedTestsByUserID(int UserID, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "LastMod DESC";

//            System.Data.DataSet Tests = null;
//            //string key = "Tests_CompletedTests_UserID_" + UserID.ToString() + cSortExpression.ToString();

//            //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            //{
//            //    Tests = (List<Test>)BizObject.Cache[key];
//            //}
//            //else
//            //{
//            Tests = SiteProvider.PR.GetCompletedTestsByUserID(UserID);
//                //Tests = GetTestListFromTestInfoList(recordset);
//                //BasePR.CacheData(key, Tests);

//            //}
//            return Tests;
//        }

//        /// <summary>
//        /// Returns a completed test object with the specified userID and testid
//        /// </summary>
//        public static Test GetCompletedTestByTestIDUserID(int UserID, int TestID)
//        {
//            Test Test = null;
//            string key = "Tests_Test_" + UserID.ToString() + TestID.ToString();

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                Test = (Test)BizObject.Cache[key];
//            }
//            else
//            {
//                Test = GetTestFromTestInfo(SiteProvider.PR.GetCompletedTestByTestIDUserID(UserID, TestID));
//                BasePR.CacheData(key, Test);
//            }
//            return Test;
//        }


//        /// <summary>
//        /// Returns a collection with InCompleted Need Survey Tests for the specified UserID
//        /// </summary>
//        public static System.Data.DataSet GetNeedSurveyTestsByUserID(int UserID, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "LastMod DESC";

//            System.Data.DataSet Tests = null;
//            //string key = "Tests_CompletedTests_UserID_" + UserID.ToString() + cSortExpression.ToString();

//            //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            //{
//            //    Tests = (List<Test>)BizObject.Cache[key];
//            //}
//            //else
//            //{
//           Tests = SiteProvider.PR.GetNeedSurveyTestsByUserID(UserID);
//            //Tests = GetTestListFromTestInfoList(recordset);
//            //BasePR.CacheData(key, Tests);

//            //}
//            return Tests;
//        }

//        /// <summary>
//        /// Returns a collection with Incomplete Tests for the specified UserID
//        /// </summary>
//        public static List<Test> GetIncompleteTestsByUserID(int UserID, string cSortExpression)
//        {
//            if (cSortExpression == null)
//                cSortExpression = "";

//            // provide default sort
//            if (cSortExpression.Length == 0)
//                cSortExpression = "LastMod DESC";

//            List<Test> Tests = null;
//            //            string key = "Tests_IncompleteTests_" + UserName.ToString() + cSortExpression.ToString();

//            //            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            //            {
//            //                Tests = (List<Test>)BizObject.Cache[key];
//            //            }
//            //            else
//            //            {
//            // don't query if UserID is bad

//                List<TestInfo> recordset = SiteProvider.PR.GetIncompleteTestsByUserID(UserID);
//                Tests = GetTestListFromTestInfoList(recordset);
//                //                BasePR.CacheData(key, Tests);
//            //            }
//            return Tests;
//        }

//        /// <summary>
//        /// Returns a collection with Incomplete Tests for the specified UserID adn TopicID
//        /// </summary>
//        public static List<Test> GetIncompleteTestsByUserIDAndTopicID(int UserID, int TopicID)
//        {
//            //if (cSortExpression == null)
//            //    cSortExpression = "";

//            //// provide default sort
//            //if (cSortExpression.Length == 0)
//            //    cSortExpression = "BuyDate DESC";

//            List<Test> Tests = null;
//            //            string key = "Tests_IncompleteTests_" + UserName.ToString() + cSortExpression.ToString();

//            //            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            //            {
//            //                Tests = (List<Test>)BizObject.Cache[key];
//            //            }
//            //            else
//            //            {
//            // don't query if UserID is bad

//            List<TestInfo> recordset = SiteProvider.PR.GetIncompleteTestsByUserIDAndTopicID(UserID, TopicID);
//            Tests = GetTestListFromTestInfoList(recordset);
//            //                BasePR.CacheData(key, Tests);
//            //            }
//            return Tests;
//        }


//        ///// <summary>
//        ///// Returns a list of test question objects for the specified TestID
//        ///// </summary>
//        //public static List<QTIQuestionObject> GetTestQuestionObjectListByTestID(int TestID)
//        //{
//        //    TestDefinition oTestDefinition = null;
//        //    QTIUtils oQTIUtils = new QTIUtils();

//        //    // NOTE: No caching of these items, because they contain user responses
//        //    // along with test questions
//        //    List<QTIQuestionObject> QTIQuestionObjects = null;
            
//        //    // first, get the test record so we have access to topicid and user's responses
//        //    Test oTest = Test.GetTestByID(TestID);

//        //    if (oTest != null)
//        //    {
//        //        // get the test definition record for this topic
//        //        oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(oTest.TopicID);

//        //        if (oTestDefinition != null)
//        //        {
//        //            // get the QTIQuestionObjectList from the TestDefinition XML
//        //            QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
//        //            // workaround until XMl is correct
//        //            //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
//        //        }
//        //    }

//        //    if (QTIQuestionObjects != null)
//        //    {
//        //        // go a list of QTIQuestionObjects, so now add the
//        //        // user responses, if any

//        //        // first, if the user completed the test, we will show the completed test
//        //        // with user's answers filled in -- but we will always show the latest version
//        //        // of the test and will fill in the correct answers automatically so it always
//        //        // matches the current version of the test

//        //        // If NOT completed yet,
//        //        // if the user's test record shows version = 0, there are no user responses yet
//        //        // (this is the condition for all tests in progress migrated from the old system,
//        //        // as well as for new tests added to MyCurriculum and not worked on yet).
//        //        // In this case, just fill in all user responses with "X" for M/C and T/F question types
//        //        // and with blank for all others

//        //        // also do this if the test version in the TestDefinition record is different from the
//        //        // version recorded with the answers so far in the Test record. This will "reset" the
//        //        // user's answers to unanswered status so it matches the new test questions.
//        //        if (oTest.Status == "C")
//        //        {
//        //            //// completed test, so just fill in the correct answers for the current version 
//        //            //// of the test
//        //            //foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//        //            //{
//        //            //    if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
//        //            //        QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
//        //            //    {
//        //            //        QTIQuestion.cUserAnswer = QTIQuestion.cCorrectAnswer;
//        //            //    }
//        //            //    else
//        //            //    {
//        //            //        QTIQuestion.cUserAnswer = "";
//        //            //    }
//        //            //}

//        //            // completed test ??? that matches the current test definition version number ???
//        //            // so it may have some user responses stored. Grab them from the test XMLTest field
//        //            // and populate them here.
//        //            List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
//        //            String[] Answers = UserAnswers.ToArray();

//        //            int iLoop = 0;
//        //            foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//        //            {
//        //                QTIQuestion.cUserAnswer = Answers[iLoop];
//        //                // be sure we're not empty -- if so, set to X
//        //                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
//        //                    QTIQuestion.cUserAnswer = "X";
//        //                iLoop++;
//        //            }
//        //        }
//        //        else if (oTest.TestVers != oTestDefinition.Version)
//        //        {
//        //            // incomplete test that does not match the current test definition version number
//        //            // set to default X and blank answers
//        //            foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//        //            {
//        //                if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
//        //                    QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
//        //                {
//        //                    QTIQuestion.cUserAnswer = "X";
//        //                }
//        //                else
//        //                {
//        //                    QTIQuestion.cUserAnswer = "";
//        //                }
//        //            }
//        //        }
//        //        else
//        //        {
//        //            // incomplete test that matches the current test definition version number
//        //            // so it may have some user responses stored. Grab them from the test XMLTest field
//        //            // and populate them here.
//        //            List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
//        //            String[] Answers = UserAnswers.ToArray();

//        //            int iLoop = 0;
//        //            foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//        //            {
//        //                QTIQuestion.cUserAnswer = Answers[iLoop];
//        //                // be sure we're not empty -- if so, set to X
//        //                if (QTIQuestion.cUserAnswer.Trim().Length == 0)
//        //                    QTIQuestion.cUserAnswer = "X";
//        //                iLoop++;
//        //            }
//        //        }
//        //    }
            
//        //    return QTIQuestionObjects;
//        //}

//        /// <summary>
//        /// Returns a list of test question objects for the specified TestID
//        /// NOTE: 12/01/2008 New version of this method with new logic
//        /// </summary>
//        public static List<QTIQuestionObject> GetTestQuestionObjectListByTestID(int TestID)
//        {
//            TestDefinition oTestDefinition = null;
//            QTIUtils oQTIUtils = new QTIUtils();

//            // NOTE: No caching of these items, because they contain user responses
//            // along with test questions
//            List<QTIQuestionObject> QTIQuestionObjects = null;

//            // first, get the test record so we have access to topicid and user's responses
//            Test oTest = Test.GetTestByID(TestID);

//            if (oTest != null)
//            {
//                // get the test definition record for this topic
//                oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(oTest.TopicID);

//                if (oTestDefinition != null)
//                {
//                    // get the QTIQuestionObjectList from the TestDefinition XML
//                    QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
//                    // workaround until XMl is correct
//                    //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
//                }
//            }

//            if (QTIQuestionObjects != null)
//            {
//                // go a list of QTIQuestionObjects, so now add the
//                // user responses, if any

//                // first, if the user completed the test, we will show the completed test
//                // with user's answers filled in -- but we will always show the latest version
//                // of the test and will fill in the correct answers automatically so it always
//                // matches the current version of the test

//                // If NOT completed yet,
//                // if the user's test record shows version = 0, there are no user responses yet
//                // (this is the condition for all tests in progress migrated from the old system,
//                // as well as for new tests added to MyCurriculum and not worked on yet).
//                // In this case, just fill in all user responses with "X" for M/C and T/F question types
//                // and with blank for all others

//                // also do this if the test version in the TestDefinition record is different from the
//                // version recorded with the answers so far in the Test record. This will "reset" the
//                // user's answers to unanswered status so it matches the new test questions.
//                if (oTest.Status == "C")
//                {
//                    //// completed test, so just fill in the correct answers for the current version 
//                    //// of the test
//                    //foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                    //{
//                    //    if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
//                    //        QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
//                    //    {
//                    //        QTIQuestion.cUserAnswer = QTIQuestion.cCorrectAnswer;
//                    //    }
//                    //    else
//                    //    {
//                    //        QTIQuestion.cUserAnswer = "";
//                    //    }
//                    //}

//                    // completed test ??? that matches the current test definition version number ???
//                    // so it may have some user responses stored. Grab them from the test XMLTest field
//                    // and populate them here.
//                    List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
//                    String[] Answers = UserAnswers.ToArray();

//                    int iLoop = 0;
//                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                    {
//                        QTIQuestion.cUserAnswer = Answers[iLoop];
//                        // be sure we're not empty -- if so, set to X
//                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
//                            QTIQuestion.cUserAnswer = "X";
//                        iLoop++;
//                    }
//                }
//                else if (oTest.TestVers != oTestDefinition.Version)
//                {
//                    // incomplete test that does not match the current test definition version number
//                    // set to default X and blank answers
//                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                    {
//                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
//                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
//                        {
//                            QTIQuestion.cUserAnswer = "X";
//                        }
//                        else
//                        {
//                            QTIQuestion.cUserAnswer = "";
//                        }
//                    }
//                }
//                else
//                {
//                    // incomplete test that matches the current test definition version number
//                    // so it may have some user responses stored. Grab them from the test XMLTest field
//                    // and populate them here.
//                    List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
//                    String[] Answers = UserAnswers.ToArray();

//                    int iLoop = 0;
//                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                    {
//                        QTIQuestion.cUserAnswer = Answers[iLoop];
//                        // be sure we're not empty -- if so, set to X
//                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
//                            QTIQuestion.cUserAnswer = "X";
//                        iLoop++;
//                    }
//                }
//            }

//            return QTIQuestionObjects;
//        }


//        ///// <summary>
//        ///// Returns a list of test question objects for the specified CartID
//        ///// NOTE: 12/01/2008 New version of this method with new logic
//        ///// </summary>
//        public static List<QTIQuestionObject> GetTestQuestionObjectListByCartID(int CartID)
//        {
//            TestDefinition oTestDefinition = null;
//            QTIUtils oQTIUtils = new QTIUtils();

//            // NOTE: No caching of these items, because they contain user responses
//            // along with test questions
//            List<QTIQuestionObject> QTIQuestionObjects = null;

//            // first, get the test record so we have access to topicid and user's responses
//            Cart oCart = Cart.GetCartByID(CartID);

//            if (oCart != null)
//            {
//                // get the test definition record for this topic
//                oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(oCart.TopicID);

//                if (oTestDefinition != null)
//                {
//                    // get the QTIQuestionObjectList from the TestDefinition XML
//                    QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
//                    // workaround until XMl is correct
//                    //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
//                }
//            }

//            if (QTIQuestionObjects != null)
//            {
//                // go a list of QTIQuestionObjects, so now add the
//                // user responses, if any

//                // first, if the user completed the test, we will show the completed test
//                // with user's answers filled in -- but we will always show the latest version
//                // of the test and will fill in the correct answers automatically so it always
//                // matches the current version of the test

//                // If NOT completed yet,
//                // if the user's test record shows version = 0, there are no user responses yet
//                // (this is the condition for all tests in progress migrated from the old system,
//                // as well as for new tests added to MyCurriculum and not worked on yet).
//                // In this case, just fill in all user responses with "X" for M/C and T/F question types
//                // and with blank for all others

//                // also do this if the test version in the TestDefinition record is different from the
//                // version recorded with the answers so far in the Test record. This will "reset" the
//                // user's answers to unanswered status so it matches the new test questions.
//                if (oCart.Score==0)
//                {
//                    // incomplete test that does not match the current test definition version number
//                    // set to default X and blank answers
//                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                    {
//                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
//                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
//                        {
//                            QTIQuestion.cUserAnswer = "X";
//                        }
//                        else
//                        {
//                            QTIQuestion.cUserAnswer = "";
//                        }
//                    }
//                }
//                else
//                {
//                    // incomplete test that matches the current test definition version number
//                    // so it may have some user responses stored. Grab them from the test XMLTest field
//                    // and populate them here.
//                    List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oCart.XmlTest);
//                    String[] Answers = UserAnswers.ToArray();
//                    int iLoop = 0;
//                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                    {
//                        QTIQuestion.cUserAnswer = Answers[iLoop];
//                        // be sure we're not empty -- if so, set to X
//                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
//                            QTIQuestion.cUserAnswer = "X";
//                        iLoop++;
//                    }
//                }
//            }

//            return QTIQuestionObjects;
//        }


//        /// <summary>
//        /// Returns a list of test Survey question objects for the specified TestID
//        /// Bhaskar N
//        /// </summary>
//        public static List<SQTIQuestionObject> GetSurveyQuestionObjectListByTestID(int TestID)
//        {
//            SurveyDefinition oSurveyDefinition = null;
//            SQTIUtils oSQTIUtils = new SQTIUtils();

//            // NOTE: No caching of these items, because they contain user responses
//            // along with test questions
//            List<SQTIQuestionObject> SQTIQuestionObjects = null;

//            // first, get the test record so we have access to topicid and user's responses
//            Test oTest = Test.GetTestByID(TestID);
//            //oTest.TopicID = 2; //bsk test

//            if (oTest != null)
//            {
//                // get the test definition record for this topic                
//                oSurveyDefinition = SurveyDefinition.GetSurveyDefinitionByTopicID(oTest.TopicID);

//                if (oSurveyDefinition != null)
//                {
//                    // get the QTIQuestionObjectList from the TestDefinition XML                                    
//                    SQTIQuestionObjects = oSQTIUtils.ConvertQTITestXMLToSQTIQuestionObjectList(oSurveyDefinition.XMLSurvey);                    
//                }
//            }
//            return SQTIQuestionObjects;
//        }
              
//        /// <summary>
//        /// Bhaskar N
//        /// </summary>
//        public static List<QTIQuestionObject> GetAllTestsByTopicId(int TopicId, string FromDate, string Todate, string SortExpression)
//        {
//            TestDefinition oTestDefinition = null;
//            QTIUtils oQTIUtils = new QTIUtils();            
//            List<QTIQuestionObject> QTIQuestionObjects = null;                     


//            List<Test> l_Tests = null;
//            string key = "Tests_Tests_" + TopicId;

//            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//            {
//                l_Tests = (List<Test>)BizObject.Cache[key];
//            }
//            else
//            {
//                List<TestInfo> recordset = SiteProvider.PR.GetAllTestsByTopicId(TopicId, FromDate, Todate, SortExpression);
//                l_Tests = GetTestListFromTestInfoList(recordset);
//                BasePR.CacheData(key, l_Tests);
//            }

//            int jLoop = 0;
//            int QTDispaly = 0;
//            foreach (Test TestIds in l_Tests)
//            {
//                int TestID = TestIds.TestID;
//                // first, get the test record so we have access to topicid and user's responses
//                Test oTest = Test.GetTestByID(TestID);

//                if (QTDispaly == 0)
//                {
//                    if (oTest != null)
//                    {
//                        // get the test definition record for this topic
//                        oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(oTest.TopicID);

//                        if (oTestDefinition != null)
//                        {
//                            // get the QTIQuestionObjectList from the TestDefinition XML
//                            QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
//                        }
//                    }
//                }
//                if (QTIQuestionObjects != null)
//                {
//                    if (oTest.Status == "C")
//                    {                   
//                        List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(oTest.XMLTest);
//                        String[] Answers = UserAnswers.ToArray();

//                        int iLoop = 0;
//                        foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                        {
//                            if (iLoop < UserAnswers.Count)
//                            {
//                                if (Answers[iLoop].ToString() == "A")
//                                {
//                                    QTIQuestion.cAnswerACount = QTIQuestion.cAnswerACount + 1;
//                                }
//                                else if (Answers[iLoop].ToString() == "B")
//                                {
//                                    QTIQuestion.cAnswerBCount = QTIQuestion.cAnswerBCount + 1;
//                                }
//                                else if (Answers[iLoop].ToString() == "C")
//                                {
//                                    QTIQuestion.cAnswerCCount = QTIQuestion.cAnswerCCount + 1;
//                                }
//                                else if (Answers[iLoop].ToString() == "D")
//                                {
//                                    QTIQuestion.cAnswerDCount = QTIQuestion.cAnswerDCount + 1;
//                                }
//                                else if (Answers[iLoop].ToString() == "E")
//                                {
//                                    QTIQuestion.cAnswerECount = QTIQuestion.cAnswerECount + 1;
//                                }
//                                else if (Answers[iLoop].ToString() == "F")
//                                {
//                                    QTIQuestion.cAnswerFCount = QTIQuestion.cAnswerFCount + 1;
//                                }

//                                //Total count
//                                if ((Answers[iLoop].ToString().Trim()).Length != 0)
//                                {                                    
//                                    QTIQuestion.cAnswerTotalCount = QTIQuestion.cAnswerTotalCount + 1;
//                                }                                
//                            }

//                            iLoop++;
//                        }
//                    }               
//                }
//            QTDispaly = 1;
//            jLoop++;
//            }

//            return QTIQuestionObjects;
//        }

//        // Changed By navya
//        /// <summary>
//        /// Returns a list of vignette question objects for the specified TestID
//        /// </summary>
//        public static List<QTIQuestionObject> GetVignetteQuestionObjectListByTestID(int TestID,string sUserAnswers)
//        {
//            ViDefinition oViDefinition = null;
//            QTIUtils oQTIUtils = new QTIUtils();

//            // NOTE: No caching of these items, because they contain user responses
//            // along with test questions
//            List<QTIQuestionObject> QTIQuestionObjects = null;

//            // first, get the test record so we have access to topicid and user's responses
//            Test oTest = Test.GetTestByID(TestID);

//            if (oTest != null)
//            {
//                // get the vignette definition record for this topic
//                oViDefinition = ViDefinition.GetViDefinitionByTopicID(oTest.TopicID);

//                if (oViDefinition != null)
//                {
//                    // get the QTIQuestionObjectList from the ViDefinition XML
//                    QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oViDefinition.XMLTest);
//                }
//            }

//            if (QTIQuestionObjects != null)
//            {
//                // go a list of QTIQuestionObjects, so now add the
//                // user responses, if any

//                // first, if the user completed the test, we will show the completed test
//                // with user's answers filled in -- but we will always show the latest version
//                // of the test and will fill in the correct answers automatically so it always
//                // matches the current version of the test

//                // If NOT completed yet,
//                // if the user's test record shows version = 0, there are no user responses yet
//                // (this is the condition for all tests in progress migrated from the old system,
//                // as well as for new tests added to MyCurriculum and not worked on yet).
//                // In this case, just fill in all user responses with "X" for M/C and T/F question types
//                // and with blank for all others

//                // also do this if the test version in the TestDefinition record is different from the
//                // version recorded with the answers so far in the Test record. This will "reset" the
//                // user's answers to unanswered status so it matches the new test questions.
//                if (oTest.Status == "C")
//                {
//                    // completed test, so just fill in the correct answers for the current version 
//                    // of the test
//                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                    {
//                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
//                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
//                        {
//                            QTIQuestion.cUserAnswer = QTIQuestion.cCorrectAnswer;
//                        }
//                        else
//                        {
//                            QTIQuestion.cUserAnswer = "";
//                        }
//                    }
//                }
//                    // * chnaged By navya
//                else if (sUserAnswers == null)
//                {
//                    // incomplete test that does not match the current vignette definition version number
//                    // set to default X and blank answers
//                    // NOTE: This is also the case with new test record that has not had the vignette
//                    // shown yet -- it will start out with vignetteversion field = 0 to this
//                    // comparison will fail and we will insert all empty answers
//                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                    {
//                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
//                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
//                        {
//                            QTIQuestion.cUserAnswer = "X";
//                        }
//                        else
//                        {
//                            QTIQuestion.cUserAnswer = "";
//                        }
//                    }
//                }
//                // * chnaged By navya
//                else
//                {
//                    // incomplete test that matches the current test definition version number
//                    // so it may have some user responses stored. Grab them from the test XMLVignette field
//                    // and populate them here.
//                    List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(sUserAnswers);
//                    String[] Answers = UserAnswers.ToArray();

//                    int iLoop = 0;
//                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                    {
//                        QTIQuestion.cUserAnswer = Answers[iLoop];
//                        // be sure we're not empty -- if so, set to X
//                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
//                            QTIQuestion.cUserAnswer = "X";
//                        iLoop++;
//                    }
//                }
//            }

//            return QTIQuestionObjects;
//        }






//        // Changed By navya
//        /// <summary>
//        /// Returns a list of vignette question objects for the specified TestID
//        /// </summary>
//        public static List<QTIQuestionObject> GetVignetteQuestionObjectListByTopicID(int TopicID, string sUserAnswers)
//        {
//            ViDefinition oViDefinition = null;
//            QTIUtils oQTIUtils = new QTIUtils();

//            // NOTE: No caching of these items, because they contain user responses
//            // along with test questions
//            List<QTIQuestionObject> QTIQuestionObjects = null;
                      
//                // get the vignette definition record for this topic
//                oViDefinition = ViDefinition.GetViDefinitionByTopicID(TopicID);

//                if (oViDefinition != null)
//                {
//                    // get the QTIQuestionObjectList from the ViDefinition XML
//                    QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oViDefinition.XMLTest);
//                }
          

//            if (QTIQuestionObjects != null)
//            {
//                // go a list of QTIQuestionObjects, so now add the
//                // user responses, if any

//                // first, if the user completed the test, we will show the completed test
//                // with user's answers filled in -- but we will always show the latest version
//                // of the test and will fill in the correct answers automatically so it always
//                // matches the current version of the test

//                // If NOT completed yet,
//                // if the user's test record shows version = 0, there are no user responses yet
//                // (this is the condition for all tests in progress migrated from the old system,
//                // as well as for new tests added to MyCurriculum and not worked on yet).
//                // In this case, just fill in all user responses with "X" for M/C and T/F question types
//                // and with blank for all others

//                // also do this if the test version in the TestDefinition record is different from the
//                // version recorded with the answers so far in the Test record. This will "reset" the
//                // user's answers to unanswered status so it matches the new test questions.
//                //if (oTest.Status == "C")
//                //{
//                //    // completed test, so just fill in the correct answers for the current version 
//                //    // of the test
//                //    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                //    {
//                //        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
//                //            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
//                //        {
//                //            QTIQuestion.cUserAnswer = QTIQuestion.cCorrectAnswer;
//                //        }
//                //        else
//                //        {
//                //            QTIQuestion.cUserAnswer = "";
//                //        }
//                //    }
//                //}
//                // * chnaged By navya
//                if (sUserAnswers == null)
//                {
//                    // incomplete test that does not match the current vignette definition version number
//                    // set to default X and blank answers
//                    // NOTE: This is also the case with new test record that has not had the vignette
//                    // shown yet -- it will start out with vignetteversion field = 0 to this
//                    // comparison will fail and we will insert all empty answers
//                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                    {
//                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
//                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
//                        {
//                            QTIQuestion.cUserAnswer = "X";
//                        }
//                        else
//                        {
//                            QTIQuestion.cUserAnswer = "";
//                        }
//                    }
//                }
//                // * chnaged By navya
//                else
//                {
//                    // incomplete test that matches the current test definition version number
//                    // so it may have some user responses stored. Grab them from the test XMLVignette field
//                    // and populate them here.
//                    List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(sUserAnswers);
//                    String[] Answers = UserAnswers.ToArray();

//                    int iLoop = 0;
//                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
//                    {
//                        QTIQuestion.cUserAnswer = Answers[iLoop];
//                        // be sure we're not empty -- if so, set to X
//                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
//                            QTIQuestion.cUserAnswer = "X";
//                        iLoop++;
//                    }
//                }
//            }

//            return QTIQuestionObjects;
//        }



//    }
        //chnaged By navya




 ////////////////////////////////////////////////////////////
   // /// <summary>
   // /// cart business object class
   // /// </summary>
   // public class Cart : BasePR
   // {

   //     private int _CartID = 0;
   //     public int CartID
   //     {
   //         get { return _CartID; }
   //         set { _CartID = value; }
   //     }
   //     private int _TopicID = 0;
   //     public int TopicID
   //     {
   //         get { return _TopicID; }
   //      set { _TopicID = value; }
   //     }
   //     private int _UserID = 0;
   //     public int UserID
   //     {
   //         get { return _UserID; }
   //         set { _UserID = value; }
   //     }

   //     private string _MediaType = "";
   //     public string MediaType
   //     {
   //         get { return _MediaType; }
   //        set { _MediaType = value; }
   //     }
   //     private int _Score = 0;
   //     public int Score
   //     {
   //         get { return _Score; }
   //         set { _Score = value; }
   //     }
   //     private DateTime _Lastmod = System.DateTime.Now;
   //     public DateTime Lastmod
   //     {
   //         get { return _Lastmod; }
   //        set { _Lastmod = value; }
   //     }
   //     private string _XmlTest = "";
   //     public string XmlTest
   //     {
   //         get { return _XmlTest; }
   //          set { _XmlTest = value; }
   //     }
   //     private string _XmlSurvey = "";
   //     public string XmlSurvey
   //     {
   //         get { return _XmlSurvey; }
   //         set { _XmlSurvey = value; }
   //     }
   //     private int _Quantity = 0;
   //     public int Quantity
   //     {
   //         get { return _Quantity; }
   //        set { _Quantity = value; }
   //     }
   //     private int _DiscountId = 0;
   //     public int DiscountId
   //     {
   //         get { return _DiscountId; }
   //         set { _DiscountId = value; }
   //     }

   //     private bool _Process_Ind = false;
   //     public bool Process_Ind
   //     {
   //         get { return _Process_Ind; }
   //        set { _Process_Ind = value; }
   //     }
   //     private bool _Option_Id = false;
   //     public bool Option_Id
   //     {
   //         get { return _Option_Id; }
   //       set { _Option_Id = value; }
   //     }

   //     public Cart(int CartID, int TopicID, int UserID, string MediaType, int Score, DateTime Lastmod, string XmlTest,
   //         string XmlSurvey, int Quantity, int DiscountId, bool Process_Ind, bool Option_Id)
   // {
   //     this.CartID = CartID;
   //     this.TopicID = TopicID;
   //     this.UserID = UserID;
   //     this.MediaType = MediaType;
   //     this.Score = Score;
   //     this.Lastmod = Lastmod;
   //     this.XmlTest = XmlTest;
   //     this.XmlSurvey = XmlSurvey;
   //     this.Quantity = Quantity;
   //     this.DiscountId = DiscountId;
   //     this.Process_Ind = Process_Ind;
   //     this.Option_Id = Option_Id;
   // }
   

   // /// <summary>
   // /// Creates a new cart
   // /// </summary>
   // public static int InsertCart(int CartID, int TopicID, int UserID, string MediaType, int Score, DateTime Lastmod, string XmlTest, 
   //     string XmlSurvey, int Quantity, int DiscountId, bool Process_Ind, bool Option_Id)
   // {
   //     MediaType = BizObject.ConvertNullToEmptyString(MediaType);
   //     XmlTest = BizObject.ConvertNullToEmptyString(XmlTest);
   //     XmlSurvey = BizObject.ConvertNullToEmptyString(XmlSurvey);

   //     CartInfo record = new CartInfo(0, TopicID, UserID, MediaType, Score, Lastmod, XmlTest,
   //         XmlSurvey, Quantity, DiscountId, Process_Ind, Option_Id);

   //     int ret = SiteProvider.PR.InsertCart(record);
   //     BizObject.PurgeCacheItems("Carts_Cart");
   //     return ret;
   // }


   // /// <summary>
   // /// Updates an existing Test
   // /// </summary>
   // public static bool UpdateCart(int CartID, int TopicID, int UserID, string MediaType, int Score, DateTime Lastmod, string XmlTest,
   //     string XmlSurvey, int Quantity, int DiscountId, bool Process_Ind, bool Option_Id)
   // {
   //     MediaType = BizObject.ConvertNullToEmptyString(MediaType);
   //     XmlTest = BizObject.ConvertNullToEmptyString(XmlTest);
   //     XmlSurvey = BizObject.ConvertNullToEmptyString(XmlSurvey);

   //     CartInfo record = new CartInfo(CartID, TopicID, UserID, MediaType, Score, Lastmod, XmlTest,
   //         XmlSurvey, Quantity, DiscountId, Process_Ind, Option_Id);
   //     bool ret = SiteProvider.PR.UpdateCart(record);

   //     BizObject.PurgeCacheItems("Carts_Cart" + CartID.ToString());
   //     BizObject.PurgeCacheItems("Carts_Cart");
   //     return ret;
   // }

   // /// <summary>
   // /// Returns a collection with all Carts for the specified UserID
   // /// </summary>
   // public static List<Cart> GetCartsByUserID(int UserID, string cSortExpression)
   // {
   //     if (cSortExpression == null)
   //         cSortExpression = "";

   //     // provide default sort
   //     if (cSortExpression.Length == 0)
   //         cSortExpression = "LastMod DESC";

   //     List<Cart> Carts = null;
   //     List<CartInfo> recordset = SiteProvider.PR.GetCartsByUserID(UserID);
   //      Carts = GetCartListFromCartInfoList(recordset);

   //      return Carts;
   // }

   /////<summary>
   ///// updates the quantity based on cartid
   /////</summary>
   /////
   // public static Boolean UpdateCartQByCartId(int Cartid,int Quantity)
   // {
   //     Cart objCart = GetCartByID(Cartid);
   //     Boolean ret=UpdateCart(Cartid, objCart.TopicID, objCart.UserID, objCart.MediaType, objCart.Score, objCart.Lastmod, objCart.XmlTest, objCart.XmlSurvey, Quantity, objCart.DiscountId, objCart.Process_Ind, objCart.Option_Id);

   //     return ret;
        
   // }


   // /// <summary>
   // /// Returns a Cart object filled with the data taken from the input CartInfo
   // /// </summary>
   // private static Cart GetCartFromCartInfo(CartInfo record)
   // {
   //     if (record == null)
   //         return null;
   //     else
   //     {
   //         return new Cart(record.CartID, record.TopicID, record.UserID, record.MediaType,
   //         record.Score,record.Lastmod, record.XmlTest, record.XmlSurvey, record.Quantity, record.DiscountId, record.Process_Ind,
   //         record.Option_Id);
   //     }
   // }

   // /// <summary>
   // /// Returns a list of Cart objects filled with the data taken from the input list of CartInfo
   // /// </summary>
   // private static List<Cart> GetCartListFromCartInfoList(List<CartInfo> recordset)
   // {
   //     List<Cart> Carts = new List<Cart>();
   //     foreach (CartInfo record in recordset)
   //         Carts.Add(GetCartFromCartInfo(record));
   //     return Carts;
   // }

   // /// <summary>
   // /// Returns a collection with Incomplete Carts for the specified UserID and TopicID
   // /// </summary>
   // public static List<Cart> GetIncompleteCartsByUserIDAndTopicID(int UserID, int TopicID)
   // {

   //     List<Cart> Carts = null;
   //     List<CartInfo> recordset = SiteProvider.PR.GetIncompleteCartsByUserIDAndTopicID(UserID, TopicID);
   //     Carts = GetCartListFromCartInfoList(recordset);
       
   //     return Carts;
   // }

   // /// <summary>
   // /// Returns a collection with Incomplete Carts for the specified UserID and TopicID
   // /// </summary>
   // public static List<Cart> GetcompleteCartsByUserIDAndTopicID(int UserID, int TopicID)
   // {

   //     List<Cart> Carts = null;
   //     List<CartInfo> recordset = SiteProvider.PR.GetcompleteCartsByUserIDAndTopicID(UserID, TopicID);
   //     Carts = GetCartListFromCartInfoList(recordset);

   //     return Carts;
   // }




   // /// <summary>
   // /// Returns a Cart object with the specified ID
   // /// </summary>
   // public static Cart GetCartByID(int CartID)
   // {
   //      Cart Cart = null;
   //     string key = "Carts_Cart_" + CartID.ToString();

   //     if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
   //     {
   //         Cart = (Cart)BizObject.Cache[key];
   //     }
   //     else
   //     {
   //         Cart = GetCartFromCartInfo(SiteProvider.PR.GetCartByID(CartID));
   //         BasePR.CacheData(key, Cart);
   //     }
   //     return Cart;
   // }


   // public bool Update()
   // {
   //     return Cart.UpdateCart(this.CartID,this.TopicID,this.UserID,this.MediaType,this.Score,this.Lastmod,this.XmlTest,this.XmlSurvey,
   //         this.Quantity,this.DiscountId,this.Process_Ind,this.Option_Id);
   // }

   // /// <summary>
   // /// Returns a Cart Price object with the specified ID
   // /// </summary>
   // public static DataSet GetCartPriceByUserId(int UserId)
   // {
   //     DataSet dsCart = (SiteProvider.PR.GetCartPriceByUserId(UserId));

   //     return dsCart;
   // }

   // /// <summary>
   // /// Returns a Cart Price object with the specified ID
   // /// </summary>
   // public static DataSet GetUnlimCartPriceByUserId(int UserId)
   // {
   //     DataSet dsCart = (SiteProvider.PR.GetUnlimCartPriceByUserId(UserId));

   //     return dsCart;
   // }


   // /// <summary>
   // /// Returns a Cart Price object with the specified cart ID
   // /// </summary>
   // public static Decimal GetCartPriceByCartId(int CartId)
   // {
   //     Decimal dPrice = (SiteProvider.PR.GetCartPriceByCartId(CartId));

   //     return dPrice;
   // }

   // /// <summary>
   // /// Returns number pharmacy courses the user took, before processing order
   // /// </summary>
   // public static int CheckNumberOfPharmacy(int UserId)
   // {
   //     int number = (SiteProvider.PR.CheckNumberOfPharmacy(UserId));

   //     return number;
   // }


   // /// <summary>
   // /// Returns a Cart price object with the specified ID
   // /// </summary>
   // public static DataSet GetOrderPriceByUserId(int UserId)
   // {
   //     DataSet dsCart = (SiteProvider.PR.GetOrderPriceByUserId(UserId));

   //     return dsCart;
   // }

   // public static DataSet GetUnlimOrderPriceByUserId(int UserId)
   // {
   //     DataSet dsOrder = (SiteProvider.PR.GetUnlimOrderPriceByUserId(UserId));

   //     return dsOrder;
   // }



   // /// <summary>
   // /// Deletes an existing Cart, but first checks if OK to delete
   // /// </summary>
   // public static bool DeleteCart(int CartID)
   // {
   //     bool IsOKToDelete = OKToDelete(CartID);
   //     if (IsOKToDelete)
   //     {
   //         return (bool)DeleteCart(CartID, true);
   //     }
   //     else
   //     {
   //         return false;
   //     }
   // }

   // /// <summary>
   // /// Deletes an existing Test - second param forces skip of OKToDelete
   // /// (assuming that the calling program has already called that if it's
   // /// passing the second param as true)
   // /// </summary>
   // public static bool DeleteCart(int CartID, bool SkipOKToDelete)
   // {
   //     if (!SkipOKToDelete)
   //         return false;

   //     bool ret = SiteProvider.PR.DeleteCart(CartID);
   //     //         new RecordDeletedEvent("Test", TestID, null).Raise();
   //     BizObject.PurgeCacheItems("Carts_Cart");
   //     return ret;
   // }



   // /// <summary>
   // /// Checks to see if a Test can be deleted safely
   // /// (This default method just returns true. Certain entities
   // /// might set datadict_tables.lNoOKDel to eliminate this
   // /// default method and provide a custom version in
   // /// datadict_tables.boExtra)
   // /// </summary>
   // public static bool OKToDelete(int CartID)
   // {
   //     return true;
   // }


   /////<summary>
   /////Check all online
   /////</summary>
   /////
   // public static int CheckAllOnline(int UserId)
   // {
   //     int ret = SiteProvider.PR.CheckAllOnline(UserId);
   //     return ret;
   // }

    

   // ///// <summary>
   // ///// Returns a list of test question objects for the specified CartID
   // ///// NOTE: 12/01/2008 New version of this method with new logic
   // ///// </summary>
   // public static List<QTIQuestionObject> GetTestQuestionObjectListByTopicID(int TopicID)
   // {
   //     TestDefinition oTestDefinition = null;
   //     QTIUtils oQTIUtils = new QTIUtils();

   //     // NOTE: No caching of these items, because they contain user responses
   //     // along with test questions
   //     List<QTIQuestionObject> QTIQuestionObjects = null;
               
   //         // get the test definition record for this topic
   //     oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(TopicID);

   //         if (oTestDefinition != null)
   //         {
   //             // get the QTIQuestionObjectList from the TestDefinition XML
   //             QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
   //             // workaround until XMl is correct
   //             //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
   //         }       
   //     if (QTIQuestionObjects != null)
   //     {         
   //             // incomplete test that does not match the current test definition version number
   //             // set to default X and blank answers
   //             foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
   //             {
   //                 if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
   //                     QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
   //                 {
   //                     QTIQuestion.cUserAnswer = "X";
   //                 }
   //                 else
   //                 {
   //                     QTIQuestion.cUserAnswer = "";
   //                 }
   //             }          
   //     }

   //     return QTIQuestionObjects;
   // }



   // public static List<QTIQuestionObject> GetTestQuestionObjectListByTopicIdAndAnswers(int TopicId, string SAnswers)
   // {
   //     TestDefinition oTestDefinition = null;
   //     QTIUtils oQTIUtils = new QTIUtils();

   //     // NOTE: No caching of these items, because they contain user responses
   //     // along with test questions
   //     List<QTIQuestionObject> QTIQuestionObjects = null;
   //         // get the test definition record for this topic
   //     oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(TopicId);

   //         if (oTestDefinition != null)
   //         {
   //             // get the QTIQuestionObjectList from the TestDefinition XML
   //             QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
   //             // workaround until XMl is correct
   //             //QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest + "</questestinterop>");
   //         }
      
   //     if (QTIQuestionObjects != null)
   //     {
   //            List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(SAnswers);
   //             String[] Answers = UserAnswers.ToArray();
   //              int iLoop = 0;
   //             foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
   //             {
   //                 if (Convert.ToInt32(QTIQuestion.cHeaderID) > 0)
   //                 {
   //                     QTIQuestion.cUserAnswer = Answers[iLoop];
   //                     // be sure we're not empty -- if so, set to X
   //                     if (QTIQuestion.cUserAnswer.Trim().Length == 0)
   //                         QTIQuestion.cUserAnswer = "X";
   //                     iLoop++;
   //                 }
   //             }
   //      }          

   //     return QTIQuestionObjects;
   // }


   // public static decimal GetCouponAmountByUserIdandCode(int UserId, string CouponCode)
   // {
   //     return (SiteProvider.PR.GetCouponAmountByUserIdandCode(UserId, CouponCode));
   // }

   // }    
    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// TestDefinition business object class
    ///// </summary>
    //public class TestDefinition : BasePR
    //{
    //    private int _TestDefID = 0;
    //    public int TestDefID
    //    {
    //        get { return _TestDefID; }
    //        protected set { _TestDefID = value; }
    //    }

    //    private int _TopicID = 0;
    //    public int TopicID
    //    {
    //        get { return _TopicID; }
    //        set { _TopicID = value; }
    //    }

    //    private string _XMLTest = "";
    //    public string XMLTest
    //    {
    //        get { return _XMLTest; }
    //        set { _XMLTest = value; }
    //    }

    //    private int _Version = 0;
    //    public int Version
    //    {
    //        get { return _Version; }
    //        set { _Version = value; }
    //    }


    //    public TestDefinition(int TestDefID, int TopicID, string XMLTest, int Version)
    //    {
    //        this.TestDefID = TestDefID;
    //        this.TopicID = TopicID;
    //        this.XMLTest = XMLTest;
    //        this.Version = Version;
    //    }

    //    public bool Delete()
    //    {
    //        bool success = TestDefinition.DeleteTestDefinition(this.TestDefID);
    //        if (success)
    //            this.TestDefID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return TestDefinition.UpdateTestDefinition(this.TestDefID, this.TopicID, this.XMLTest, this.Version);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all TestDefinitions
    //    /// </summary>
    //    public static List<TestDefinition> GetTestDefinitions(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "Version";

    //        List<TestDefinition> TestDefinitions = null;
    //        string key = "TestDefinitions_TestDefinitions_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TestDefinitions = (List<TestDefinition>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TestDefinitionInfo> recordset = SiteProvider.PR.GetTestDefinitions(cSortExpression);
    //            TestDefinitions = GetTestDefinitionListFromTestDefinitionInfoList(recordset);
    //            BasePR.CacheData(key, TestDefinitions);
    //        }
    //        return TestDefinitions;
    //    }


    //    /// <summary>
    //    /// Returns the number of total TestDefinitions
    //    /// </summary>
    //    public static int GetTestDefinitionCount()
    //    {
    //        int TestDefinitionCount = 0;
    //        string key = "TestDefinitions_TestDefinitionCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TestDefinitionCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TestDefinitionCount = SiteProvider.PR.GetTestDefinitionCount();
    //            BasePR.CacheData(key, TestDefinitionCount);
    //        }
    //        return TestDefinitionCount;
    //    }

    //    /// <summary>
    //    /// Returns a TestDefinition object with the specified ID
    //    /// </summary>
    //    public static TestDefinition GetTestDefinitionByID(int TestDefID)
    //    {
    //        TestDefinition TestDefinition = null;
    //        string key = "TestDefinitions_TestDefinition_" + TestDefID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TestDefinition = (TestDefinition)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TestDefinition = GetTestDefinitionFromTestDefinitionInfo(SiteProvider.PR.GetTestDefinitionByID(TestDefID));
    //            BasePR.CacheData(key, TestDefinition);
    //        }
    //        return TestDefinition;
    //    }

    //    /// <summary>
    //    /// Returns a TestDefinition object associated with the specified TopicID
    //    /// </summary>
    //    public static TestDefinition GetTestDefinitionByTopicID(int TopicID)
    //    {
    //        TestDefinition TestDefinition = null;
    //        string key = "TestDefinitions_TestDefinition_TopicID_" + TopicID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TestDefinition = (TestDefinition)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TestDefinition = GetTestDefinitionFromTestDefinitionInfo(SiteProvider.PR.GetTestDefinitionByTopicID(TopicID));
    //            BasePR.CacheData(key, TestDefinition);
    //        }
    //        return TestDefinition;
    //    }

    //    /// <summary>
    //    /// Updates an existing TestDefinition
    //    /// </summary>
    //    public static bool UpdateTestDefinition(int TestDefID, int TopicID, string XMLTest, int Version)
    //    {
    //        XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);


    //        TestDefinitionInfo record = new TestDefinitionInfo(TestDefID, TopicID, XMLTest, Version);
    //        bool ret = SiteProvider.PR.UpdateTestDefinition(record);

    //        BizObject.PurgeCacheItems("TestDefinitions_TestDefinition_" + TestDefID.ToString());
    //        BizObject.PurgeCacheItems("TestDefinitions_TestDefinitions");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new TestDefinition
    //    /// </summary>
    //    public static int InsertTestDefinition(int TopicID, string XMLTest, int Version)
    //    {
    //        XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);


    //        TestDefinitionInfo record = new TestDefinitionInfo(0, TopicID, XMLTest, Version);
    //        int ret = SiteProvider.PR.InsertTestDefinition(record);

    //        BizObject.PurgeCacheItems("TestDefinitions_TestDefinition");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing TestDefinition, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteTestDefinition(int TestDefID)
    //    {
    //        bool IsOKToDelete = OKToDelete(TestDefID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteTestDefinition(TestDefID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing TestDefinition - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteTestDefinition(int TestDefID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteTestDefinition(TestDefID);
    //        //         new RecordDeletedEvent("TestDefinition", TestDefID, null).Raise();
    //        BizObject.PurgeCacheItems("TestDefinitions_TestDefinition");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a TestDefinition can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int TestDefID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a TestDefinition object filled with the data taken from the input TestDefinitionInfo
    //    /// </summary>
    //    private static TestDefinition GetTestDefinitionFromTestDefinitionInfo(TestDefinitionInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new TestDefinition(record.TestDefID, record.TopicID, record.XMLTest, record.Version);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of TestDefinition objects filled with the data taken from the input list of TestDefinitionInfo
    //    /// </summary>
    //    private static List<TestDefinition> GetTestDefinitionListFromTestDefinitionInfoList(List<TestDefinitionInfo> recordset)
    //    {
    //        List<TestDefinition> TestDefinitions = new List<TestDefinition>();
    //        foreach (TestDefinitionInfo record in recordset)
    //            TestDefinitions.Add(GetTestDefinitionFromTestDefinitionInfo(record));
    //        return TestDefinitions;
    //    }

    //}

    ////////////////////////////////////////////////////////////
    /// <summary>
    /// ViDefinition business object class
    /// </summary>
    //public class ViDefinition : BasePR
    //{
    //    private int _ViDefID = 0;
    //    public int ViDefID
    //    {
    //        get { return _ViDefID; }
    //        protected set { _ViDefID = value; }
    //    }

    //    private int _TopicID = 0;
    //    public int TopicID
    //    {
    //        get { return _TopicID; }
    //        set { _TopicID = value; }
    //    }

    //    private string _XMLTest = "";
    //    public string XMLTest
    //    {
    //        get { return _XMLTest; }
    //        set { _XMLTest = value; }
    //    }

    //    private string _Vignette_Desc = "";
    //    public string Vignette_Desc
    //    {
    //        get { return _Vignette_Desc; }
    //        set { _Vignette_Desc = value; }
    //    }

    //    private int _Version = 0;
    //    public int Version
    //    {
    //        get { return _Version; }
    //        set { _Version = value; }
    //    }


    //   public ViDefinition(int ViDefID, int TopicID, string XMLTest, string Vignette_Desc, int Version)
    //  {
    //        this.ViDefID = ViDefID;
    //        this.TopicID = TopicID;
    //        this.XMLTest = XMLTest;
    //        this.Vignette_Desc = Vignette_Desc;
    //        this.Version = Version;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = ViDefinition.DeleteViDefinition(this.ViDefID);
    //        if (success)
    //            this.ViDefID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return ViDefinition.UpdateViDefinition(this.ViDefID, this.TopicID, this.XMLTest, this.Vignette_Desc, this.Version);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all ViDefinitions
    //    /// </summary>
    //    public static List<ViDefinition> GetViDefinitions(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "Version";

    //        List<ViDefinition> ViDefinitions = null;
    //        string key = "ViDefinitions_ViDefinitions_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ViDefinitions = (List<ViDefinition>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<ViDefinitionInfo> recordset = SiteProvider.PR.GetViDefinitions(cSortExpression);
    //            ViDefinitions = GetViDefinitionListFromViDefinitionInfoList(recordset);
    //            BasePR.CacheData(key, ViDefinitions);
    //        }
    //        return ViDefinitions;
    //    }


    //    /// <summary>
    //    /// Returns the number of total ViDefinitions
    //    /// </summary>
    //    public static int GetViDefinitionCount()
    //    {
    //        int ViDefinitionCount = 0;
    //        string key = "ViDefinitions_ViDefinitionCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ViDefinitionCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            ViDefinitionCount = SiteProvider.PR.GetViDefinitionCount();
    //            BasePR.CacheData(key, ViDefinitionCount);
    //        }
    //        return ViDefinitionCount;
    //    }

    //    /// <summary>
    //    /// Returns a ViDefinition object with the specified ID
    //    /// </summary>
    //    public static ViDefinition GetViDefinitionByID(int ViDefID)
    //    {
    //        ViDefinition ViDefinition = null;
    //        string key = "ViDefinitions_ViDefinition_" + ViDefID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ViDefinition = (ViDefinition)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            ViDefinition = GetViDefinitionFromViDefinitionInfo(SiteProvider.PR.GetViDefinitionByID(ViDefID));
    //            BasePR.CacheData(key, ViDefinition);
    //        }
    //        return ViDefinition;
    //    }

    //    /// <summary>
    //    /// Returns a ViDefinition object associated with the specified TopicID
    //    /// </summary>
    //    public static ViDefinition GetViDefinitionByTopicID(int TopicID)
    //    {
    //        ViDefinition ViDefinition = null;
    //        string key = "ViDefinitions_ViDefinition_TopicID_" + TopicID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            ViDefinition = (ViDefinition)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            ViDefinition = GetViDefinitionFromViDefinitionInfo(SiteProvider.PR.GetViDefinitionByTopicID(TopicID));
    //            BasePR.CacheData(key, ViDefinition);
    //        }
    //        return ViDefinition;
    //    }

    //    /// <summary>
    //    /// Updates an existing ViDefinition
    //    /// </summary>
    //    public static bool UpdateViDefinition(int ViDefID, int TopicID, string XMLTest, string Vignette_Desc, int Version)
    //    {
    //        XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);


    //        ViDefinitionInfo record = new ViDefinitionInfo(ViDefID, TopicID, XMLTest, Vignette_Desc, Version);
    //        bool ret = SiteProvider.PR.UpdateViDefinition(record);

    //        BizObject.PurgeCacheItems("ViDefinitions_ViDefinition_" + ViDefID.ToString());
    //        BizObject.PurgeCacheItems("ViDefinitions_ViDefinitions");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new ViDefinition
    //    /// </summary>
    //    public static int InsertViDefinition(int TopicID, string XMLTest, string Vignette_Desc, int Version)
    //    {
    //        XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);


    //        ViDefinitionInfo record = new ViDefinitionInfo(0, TopicID, XMLTest, Vignette_Desc, Version);
    //        int ret = SiteProvider.PR.InsertViDefinition(record);

    //        BizObject.PurgeCacheItems("ViDefinitions_ViDefinition");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing ViDefinition, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteViDefinition(int ViDefID)
    //    {
    //        bool IsOKToDelete = OKToDelete(ViDefID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteViDefinition(ViDefID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing ViDefinition - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteViDefinition(int ViDefID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteViDefinition(ViDefID);
    //        //         new RecordDeletedEvent("ViDefinition", ViDefID, null).Raise();
    //        BizObject.PurgeCacheItems("ViDefinitions_ViDefinition");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a ViDefinition can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int ViDefID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a ViDefinition object filled with the data taken from the input ViDefinitionInfo
    //    /// </summary>
    //    private static ViDefinition GetViDefinitionFromViDefinitionInfo(ViDefinitionInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new ViDefinition(record.ViDefID, record.TopicID, record.XMLTest, record.Vignette_Desc, record.Version);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of ViDefinition objects filled with the data taken from the input list of ViDefinitionInfo
    //    /// </summary>
    //    private static List<ViDefinition> GetViDefinitionListFromViDefinitionInfoList(List<ViDefinitionInfo> recordset)
    //    {
    //        List<ViDefinition> ViDefinitions = new List<ViDefinition>();
    //        foreach (ViDefinitionInfo record in recordset)
    //            ViDefinitions.Add(GetViDefinitionFromViDefinitionInfo(record));
    //        return ViDefinitions;
    //    }

    //}

    
    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// Topic business object class
    ///// </summary>
    //public class Topic : BasePR
    //{
    //    public bool _CorLecture = false;
    //    public bool CorLecture
    //    {
    //        get { return _CorLecture; }
    //        set { _CorLecture = value; }
    //    }

    //    public string _Fla_CEType = "";
    //    public string Fla_CEType
    //    {
    //        get { return _Fla_CEType; }
    //        set { _Fla_CEType = value; }
    //    }

    //    public string _Fla_IDNo = "";
    //    public string Fla_IDNo
    //    {
    //        get { return _Fla_IDNo; }
    //        set { _Fla_IDNo = value; }
    //    }

    //    public string _FolderName = "";
    //    public string FolderName
    //    {
    //        get { return _FolderName; }
    //        set { _FolderName = value; }
    //    }

    //    public string _HTML = "";
    //    public string HTML
    //    {
    //        get { return _HTML; }
    //        set { _HTML = value; }
    //    }

    //    public string _LastUpdate = DateTime.Now.ToShortDateString();
    //    public string LastUpdate
    //    {
    //        get { return _LastUpdate; }
    //        set { _LastUpdate = value; }
    //    }

    //    public bool _NoSurvey = false;
    //    public bool NoSurvey
    //    {
    //        get { return _NoSurvey; }
    //        set { _NoSurvey = value; }
    //    }

    //    public int _TopicID = 0;
    //    public int TopicID
    //    {
    //        get { return _TopicID; }
    //        set { _TopicID = value; }
    //    }

    //    public string _TopicName = "";
    //    public string TopicName
    //    {
    //        get { return _TopicName; }
    //        set { _TopicName = value; }
    //    }

    //    public int _SurveyID = 0;
    //    public int SurveyID
    //    {
    //        get { return _SurveyID; }
    //        set { _SurveyID = value; }
    //    }

    //    public bool _Compliance = false;
    //    public bool Compliance
    //    {
    //        get { return _Compliance; }
    //        set { _Compliance = value; }
    //    }

    //    public string _MetaKW = "";
    //    public string MetaKW
    //    {
    //        get { return _MetaKW; }
    //        set { _MetaKW = value; }
    //    }

    //    public string _MetaDesc = "";
    //    public string MetaDesc
    //    {
    //        get { return _MetaDesc; }
    //        set { _MetaDesc = value; }
    //    }

    //    public decimal _Hours = 0.00M;
    //    public decimal Hours
    //    {
    //        get { return _Hours; }
    //        set { _Hours = value; }
    //    }

    //    public string _MediaType = "";
    //    public string MediaType
    //    {
    //        get { return _MediaType; }
    //        set { _MediaType = value; }
    //    }

    //    public string _Objectives = "";
    //    public string Objectives
    //    {
    //        get { return _Objectives; }
    //        set { _Objectives = value; }
    //    }

    //    public string _Content = "";
    //    public string Content
    //    {
    //        get { return _Content; }
    //        set { _Content = value; }
    //    }

    //    public bool _Textbook = false;
    //    public bool Textbook
    //    {
    //        get { return _Textbook; }
    //        set { _Textbook = value; }
    //    }

    //    public int _CertID = 0;
    //    public int CertID
    //    {
    //        get { return _CertID; }
    //        set { _CertID = value; }
    //    }

    //    public string _Method = "";
    //    public string Method
    //    {
    //        get { return _Method; }
    //        set { _Method = value; }
    //    }

    //    public string _GrantBy = "";
    //    public string GrantBy
    //    {
    //        get { return _GrantBy; }
    //        set { _GrantBy = value; }
    //    }

    //    public string _DOCXFile = "";
    //    public string DOCXFile
    //    {
    //        get { return _DOCXFile; }
    //        set { _DOCXFile = value; }
    //    }
    //    public string _DOCXHoFile = "";
    //    public string DOCXHoFile
    //    {
    //        get { return _DOCXHoFile; }
    //        set { _DOCXHoFile = value; }
    //    }

    //    public bool _Obsolete = false;
    //    public bool Obsolete
    //    {
    //        get { return _Obsolete; }
    //        set { _Obsolete = value; }
    //    }

    //    public int _FacilityID = 0;
    //    public int FacilityID
    //    {
    //        get { return _FacilityID; }
    //        set { _FacilityID = value; }
    //    }

    //    public string _Course_Number = "";
    //    public string Course_Number
    //    {
    //        get { return _Course_Number; }
    //        set { _Course_Number = value; }
    //    }

    //    //private string _CeRef = "";
    //    //public string CeRef
    //    //{
    //    //    get { return _CeRef; }
    //    //    set { _CeRef = value; }
    //    //}

    //    //private DateTime _Release_Date = System.DateTime.MinValue;
    //    //public DateTime Release_Date
    //    //{
    //    //    get { return _Release_Date; }
    //    //    set { _Release_Date = value; }
    //    //}



    //    public int _Minutes = 0;
    //    public int Minutes
    //    {
    //        get { return _Minutes; }
    //        set { _Minutes = value; }
    //    }

    //    public bool _Audio_Ind = false;
    //    public bool Audio_Ind
    //    {
    //        get { return _Audio_Ind; }
    //        set { _Audio_Ind = value; }
    //    }

    //    public bool _Apn_Ind = false;
    //    public bool Apn_Ind
    //    {
    //        get { return _Apn_Ind; }
    //        set { _Apn_Ind = value; }
    //    }

    //    public bool _Icn_Ind = false;
    //    public bool Icn_Ind
    //    {
    //        get { return _Icn_Ind; }
    //        set { _Icn_Ind = value; }
    //    }

    //    public bool _Jcaho_Ind = false;
    //    public bool Jcaho_Ind
    //    {
    //        get { return _Jcaho_Ind; }
    //        set { _Jcaho_Ind = value; }
    //    }

    //    public bool _Magnet_Ind = false;
    //    public bool Magnet_Ind
    //    {
    //        get { return _Magnet_Ind; }
    //        set { _Magnet_Ind = value; }
    //    }

    //    public bool _Active_Ind = false;
    //    public bool Active_Ind
    //    {
    //        get { return _Active_Ind; }
    //        set { _Active_Ind = value; }
    //    }

    //    public bool _Video_Ind = false;
    //    public bool Video_Ind
    //    {
    //        get { return _Video_Ind; }
    //        set { _Video_Ind = value; }
    //    }

    //    public bool _Online_Ind = false;
    //    public bool Online_Ind
    //    {
    //        get { return _Online_Ind; }
    //        set { _Online_Ind = value; }
    //    }

    //    public bool _Ebp_Ind = false;
    //    public bool Ebp_Ind
    //    {
    //        get { return _Ebp_Ind; }
    //        set { _Ebp_Ind = value; }
    //    }

    //    public bool _Ccm_Ind = false;
    //    public bool Ccm_Ind
    //    {
    //        get { return _Ccm_Ind; }
    //        set { _Ccm_Ind = value; }
    //    }

    //    public bool _Avail_Ind = false;
    //    public bool Avail_Ind
    //    {
    //        get { return _Avail_Ind; }
    //        set { _Avail_Ind = value; }
    //    }

    //    public bool _notest = false;
    //    public bool notest
    //    {
    //        get { return _notest; }
    //        set { _notest = value; }
    //    }

    //    public bool _uce_Ind = false;
    //    public bool uce_Ind
    //    {
    //        get { return _uce_Ind; }
    //        set { _uce_Ind = value; }
    //    }


    //    public string _Cert_Cerp = "";
    //    public string Cert_Cerp
    //    {
    //        get { return _Cert_Cerp; }
    //        set { _Cert_Cerp = value; }
    //    }

    //    public string _Ref_Html = "";
    //    public string Ref_Html
    //    {
    //        get { return _Ref_Html; }
    //        set { _Ref_Html = value; }
    //    }

    //    public int _AlterID = 0;
    //    public int AlterID
    //    {
    //        get { return _AlterID; }
    //        set { _AlterID = value; }
    //    }

    //    public int _Pass_Score = 0;
    //    public int Pass_Score
    //    {
    //        get { return _Pass_Score; }
    //        set { _Pass_Score = value; }
    //    }

    //    public string _Subtitle = "";
    //    public string Subtitle
    //    {
    //        get { return _Subtitle; }
    //        set { _Subtitle = value; }
    //    }

    //    //private decimal _Cost = 0.00M;
    //    //public decimal Cost
    //    //{
    //    //    get { return _Cost; }
    //    //    set { _Cost = value; }
    //    //}

    //    public string _Topic_Type = "";
    //    public string Topic_Type
    //    {
    //        get { return _Topic_Type; }
    //        set { _Topic_Type = value; }
    //    }

    //    public string _Img_Name = "";
    //    public string Img_Name
    //    {
    //        get { return _Img_Name; }
    //        set { _Img_Name = value; }
    //    }

    //    public string _Img_Credit = "";
    //    public string Img_Credit
    //    {
    //        get { return _Img_Credit; }
    //        set { _Img_Credit = value; }
    //    }

    //    public string _Img_Caption = "";
    //    public string Img_Caption
    //    {
    //        get { return _Img_Caption; }
    //        set { _Img_Caption = value; }
    //    }

    //    public string _Accreditation = "";
    //    public string Accreditation
    //    {
    //        get { return _Accreditation; }
    //        set { _Accreditation = value; }
    //    }

    //    public int _Views = 0;
    //    public int Views
    //    {
    //        get { return _Views; }
    //        set { _Views = value; }
    //    }

    //    public int _PrimaryViews = 0;
    //    public int PrimaryViews
    //    {
    //        get { return _PrimaryViews; }
    //        set { _PrimaryViews = value; }
    //    }

    //    public int _PageReads = 0;
    //    public int PageReads
    //    {
    //        get { return _PageReads; }
    //        set { _PageReads = value; }
    //    }

    //    public bool _Featured = false;
    //    public bool Featured
    //    {
    //        get { return _Featured; }
    //        set { _Featured = value; }
    //    }
    //    public bool _Deleted_Ind = false;
    //    public bool Deleted_Ind
    //    {
    //        get { return _Deleted_Ind; }
    //        set { _Deleted_Ind = value; }
    //    }
    //    public decimal _online_Cost = 0.00M;
    //    public decimal online_Cost
    //    {
    //        get { return _online_Cost; }
    //        set { _online_Cost = value; }
    //    }
    //    public decimal _offline_Cost = 0.00M;
    //    public decimal offline_Cost
    //    {
    //        get { return _offline_Cost; }
    //        set { _offline_Cost = value; }
    //    }
    //    public string _rev = "";
    //    public string rev
    //    {
    //        get { return _rev; }
    //        set { _rev = value; }
    //    }
    //    public string _urlmark = "";
    //    public string urlmark
    //    {
    //         get { return _urlmark; }
    //        set { _urlmark = value; }
    //    }
    //    private string _shortname = "";
    //    public string shortname
    //    {
    //        get { return _shortname; }
    //        set { _shortname = value; }
    //    }

    //    private int _categoryid = 0;
    //    public int categoryid
    //    {
    //        get { return _categoryid; }
    //        set { _categoryid = value; }
    //    }

    //    private bool _hold_ind = false;
    //    public bool hold_ind
    //    {
    //        get { return _hold_ind; }
    //        set { _hold_ind = value; }
    //    }
        

    //   public Topic(bool CorLecture, string Fla_CEType, string Fla_IDNo, string FolderName, string HTML, 
    //       string LastUpdate, bool NoSurvey, int TopicID, string TopicName, 
    //       int SurveyID, bool Compliance, string MetaKW, string MetaDesc, decimal Hours, string MediaType, 
    //       string Objectives, string Content, bool Textbook, int CertID, string Method, string GrantBy,
    //       string DOCXFile, string DOCXHoFile, bool Obsolete, int FacilityID, string Course_Number,
    //       //string CeRef,
    //       //DateTime Release_Date, 
    //       int Minutes, bool Audio_Ind,
    //       bool Apn_Ind, bool Icn_Ind, bool Jcaho_Ind, bool Magnet_Ind, bool Active_Ind, bool Video_Ind,
    //       bool Online_Ind, bool Ebp_Ind, bool Ccm_Ind, bool Avail_Ind, string Cert_Cerp, string Ref_Html,
    //       int AlterID, int Pass_Score,
    //       string Subtitle,
    //       //decimal Cost,
    //       string Topic_Type, string Img_Name, string Img_Credit,
    //       string Img_Caption, string Accreditation, int Views, int PrimaryViews, int PageReads, bool Featured, string rev, string urlmark, decimal offline_Cost,
    //       decimal online_Cost, bool notest, bool uce_Ind, string shortname, int categoryid, bool hold_ind)
    //  {
    //        this.CorLecture = CorLecture;
    //        this.Fla_CEType = Fla_CEType;
    //        this.Fla_IDNo = Fla_IDNo;
    //        this.FolderName = FolderName;
    //        this.HTML = HTML;
    //        this.LastUpdate = LastUpdate;
    //        this.NoSurvey = NoSurvey;
    //        this.TopicID = TopicID;
    //        this.TopicName = TopicName;
    //        this.SurveyID = SurveyID;
    //        this.Compliance = Compliance;
    //        this.MetaKW = MetaKW;
    //        this.MetaDesc = MetaDesc;
    //        this.Hours = Hours;
    //        this.MediaType = MediaType;
    //        this.Objectives = Objectives;
    //        this.Content = Content;
    //        this.Textbook = Textbook;
    //        this.CertID = CertID;
    //        this.Method = Method;
    //        this.GrantBy = GrantBy;
    //        this.DOCXFile = DOCXFile;
    //        this.DOCXHoFile = DOCXHoFile;
    //        this.Obsolete = Obsolete;
    //        this.FacilityID = FacilityID;
    //       this.Course_Number = Course_Number;
    //       //this.CeRef = CeRef;
    //       //this.Release_Date = Release_Date;
    //       this.Minutes = Minutes;
    //       this.Audio_Ind = Audio_Ind;
    //       this.Apn_Ind = Apn_Ind;
    //       this.Icn_Ind = Icn_Ind;
    //       this.Jcaho_Ind = Jcaho_Ind;
    //       this.Magnet_Ind = Magnet_Ind;
    //       this.Active_Ind = Active_Ind;
    //       this.Video_Ind = Video_Ind;
    //       this.Online_Ind = Online_Ind;
    //       this.Ebp_Ind = Ebp_Ind;
    //       this.Ccm_Ind = Ccm_Ind;
    //       this.Avail_Ind = Avail_Ind;
    //       this.Cert_Cerp = Cert_Cerp;
    //       this.Ref_Html = Ref_Html;
    //       this.AlterID = AlterID;
    //       this.Pass_Score = Pass_Score;
    //       this.Subtitle = Subtitle;
    //       //this.Cost = Cost;
    //       this.Topic_Type = Topic_Type;
    //       this.Img_Name = Img_Name;
    //       this.Img_Credit = Img_Credit;
    //       this.Img_Caption = Img_Caption;
    //       this.Accreditation = Accreditation;
    //       this.Views = Views;
    //       this.PrimaryViews = PrimaryViews;
    //       this.PageReads = PageReads;
    //       this.Featured = Featured;
    //       this.rev = rev;
    //       this.urlmark = urlmark;
    //       this.notest = notest;
    //       this.online_Cost = online_Cost;
    //       this.offline_Cost = offline_Cost;
    //       this.uce_Ind = uce_Ind;
    //       this.shortname = shortname;
    //       this.categoryid = categoryid;
    //       this.hold_ind = hold_ind;
    //   }

    //    public bool Delete()
    //    {
    //        bool success = Topic.DeleteTopic(this.TopicID);
    //        if (success)
    //            this.TopicID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return Topic.UpdateTopic(this.CorLecture, this.Fla_CEType, this.Fla_IDNo, this.FolderName, 
    //            this.HTML, this.LastUpdate, this.NoSurvey, this.TopicID, this.TopicName, 
    //            this.SurveyID, this.Compliance, this.MetaKW, this.MetaDesc, this.Hours, this.MediaType, 
    //            this.Objectives, this.Content, this.Textbook, this.CertID, this.Method, this.GrantBy, this.DOCXFile,
    //            this.DOCXHoFile, this.Obsolete, this.FacilityID, this.Course_Number,
    //       //     this.CeRef,
    //       //this.Release_Date, 
    //       this.Minutes, this.Audio_Ind,
    //       this.Apn_Ind, this.Icn_Ind, this.Jcaho_Ind, this.Magnet_Ind, this.Active_Ind, this.Video_Ind,
    //       this.Online_Ind, this.Ebp_Ind, this.Ccm_Ind, this.Avail_Ind, this.Cert_Cerp, this.Ref_Html,
    //       this.AlterID, this.Pass_Score,
    //       this.Subtitle, 
    //       //this.Cost,
    //       this.Topic_Type, this.Img_Name, this.Img_Credit,
    //       this.Img_Caption, this.Accreditation, this.Views, this.PrimaryViews, this.PageReads, this.Featured,this.rev,this.urlmark,this.offline_Cost,this.online_Cost,this.notest,this.uce_Ind,
    //       this.shortname, this.categoryid, this.hold_ind);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all Topics, except obsolete ones
    //    /// </summary>
    //    public static List<Topic> GetTopics(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_Topics_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetTopics(cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }

       
    //    /// <summary>
    //    /// Returns a collection with all Featured Topics (Featured = true), except obsolete ones
    //    /// </summary>
    //    public static List<Topic> GetFeaturedTopics(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_FeaturedTopics_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetFeaturedTopics(cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Featured Topics (Featured = true), except obsolete ones
    //    /// </summary>
    //    public static List<Topic> GetAudioTopics(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_AudioTopics_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetAudioTopics(cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }



    //    /// <summary>
    //    /// Returns a collection with all Featured Topics (Featured = true), except obsolete ones by facility ID
    //    /// </summary>
    //    public static List<Topic> GetFeaturedTopicsByFacilityId(int facilityid,string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_FeaturedTopics_" +facilityid+ cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetFeaturedTopicsByFacilityId(facilityid,cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns all chapters  (Bhaskar N)
    //    /// </summary>
    //    public static List<Topic> GetAllChaptersByTopicId(int topicid)
    //    {  
    //        List<Topic> Topics = null;
    //        string key = "Topics_Chapters_" + topicid;

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetAllChaptersByTopicId(topicid);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Textbook Topics (Textbook = true), except obsolete ones
    //    /// </summary>
    //    public static List<Topic> GetTextbookTopics(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_TextbookTopics_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetTextbookTopics(cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Textbook Topics (Textbook = true), except obsolete ones
    //    /// </summary>
    //    public static List<Topic> GetNonTextbookTopics(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_NonTextbookTopics_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetNonTextbookTopics(cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }


    //    /// <summary>
    //    /// Returns a collection with all Textbook Topics (Textbook = true), except obsolete ones by FacilityId
    //    /// </summary>
    //    public static List<Topic> GetNonTextbookTopicsByFacilityId(int facilityid,string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_NonTextbookTopics_" + facilityid+ cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetNonTextbookTopicsByFacilityId(facilityid,cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Textbook Topics (Textbook = true), except obsolete ones by FacilityId
    //    /// </summary>
    //    public static List<Topic> GetAnthologyTopicsByTopicId(int TopicID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        //if (cSortExpression.Length == 0)
    //        //    cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_AnthologyTopics_" + TopicID + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetAnthologyTopicsByTopicId(TopicID, cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }


    //    /// <summary>
    //    /// Returns a collection with all Obsolete Topics 
    //    /// </summary>
    //    public static List<Topic> GetObsoleteTopics(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_ObsoleteTopics_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetObsoleteTopics(cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Obsolete Textbook Topics (Textbook = true)
    //    /// </summary>
    //    public static List<Topic> GetObsoleteTextbookTopics(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_ObsoleteTextbookTopics_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetObsoleteTextbookTopics(cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }




    //    /// <summary>
    //    /// Returns a collection with all Textbook Topics (Textbook = true)
    //    /// </summary>
    //    public static List<Topic> GetTopicsByType(string CourseNumber,string Title,string category,string active,string cSortExpression)
    //             {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_TopicsByType_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetTopicsByType(CourseNumber, Title, category, active,cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }


    //    /// <summary>
    //    /// Returns a collection with all Obsolete NonTextbook Topics (Textbook = false)
    //    /// </summary>
    //    public static List<Topic> GetObsoleteNonTextbookTopics(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_ObsoleteNonTextbookTopics_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetObsoleteNonTextbookTopics(cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Compliance Topics (compliance = true), except obsolete ones
    //    /// NOTE: Because this is used by facility admins to customize compliance topics, we will
    //    /// eliminate from the list any that are not assigned to a category, since they are not
    //    /// available to the public
    //    /// </summary>
    //    public static List<Topic> GetComplianceTopics(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_ComplianceTopics_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetComplianceTopics(cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Topics (textbook and non-textbook), plus info about those assigned to a CategoryID
    //    /// , except obsolete ones
    //    /// </summary>
    //    public static List<CheckBoxListTopicInfo> GetAllTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<CheckBoxListTopicInfo> CheckBoxListTopics = null;
    //        string key = "Topics_CheckBoxListTopics_" + CategoryID.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            CheckBoxListTopics = (List<CheckBoxListTopicInfo>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<CheckBoxListTopicInfo> recordset = SiteProvider.PR.GetAllTopicsPlusCategoryAssignments(CategoryID, cSortExpression);
    //            CheckBoxListTopics = GetCheckBoxListTopicListFromCheckBoxListTopicInfoList(recordset);
    //            BasePR.CacheData(key, CheckBoxListTopics);
    //        }
    //        return CheckBoxListTopics;

    //    }

    //    /// <summary>
    //    /// Returns a collection with all Topics, plus info about those assigned to a CategoryID, except obsolete ones
    //    /// </summary>
    //    public static List<CheckBoxListTopicInfo> GetAllNonTextbookTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<CheckBoxListTopicInfo> CheckBoxListTopics = null;
    //        string key = "Topics_NonTextbook_CheckBoxListTopics_" + CategoryID.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            CheckBoxListTopics = (List<CheckBoxListTopicInfo>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<CheckBoxListTopicInfo> recordset = SiteProvider.PR.GetAllNonTextbookTopicsPlusCategoryAssignments(CategoryID, cSortExpression);
    //            CheckBoxListTopics = GetCheckBoxListTopicListFromCheckBoxListTopicInfoList(recordset);
    //            BasePR.CacheData(key, CheckBoxListTopics);
    //        }
    //        return CheckBoxListTopics;

    //    }

    //    /// <summary>
    //    /// Returns a collection with all Textbook Topics, plus info about those assigned to a CategoryID, except obsolete ones
    //    /// </summary>
    //    public static List<CheckBoxListTopicInfo> GetAllTextbookTopicsPlusCategoryAssignments(int CategoryID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<CheckBoxListTopicInfo> CheckBoxListTopics = null;
    //        string key = "Topics_Textbook_CheckBoxListTopics_" + CategoryID.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            CheckBoxListTopics = (List<CheckBoxListTopicInfo>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<CheckBoxListTopicInfo> recordset = SiteProvider.PR.GetAllTextbookTopicsPlusCategoryAssignments(CategoryID, cSortExpression);
    //            CheckBoxListTopics = GetCheckBoxListTopicListFromCheckBoxListTopicInfoList(recordset);
    //            BasePR.CacheData(key, CheckBoxListTopics);
    //        }
    //        return CheckBoxListTopics;

    //    }

    //    /// <summary>
    //    /// Returns the number of total Topics, except obsolete ones
    //    /// </summary>
    //    public static int GetTopicCount()
    //    {
    //        int TopicCount = 0;
    //        string key = "Topics_TopicCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicCount = SiteProvider.PR.GetTopicCount();
    //            BasePR.CacheData(key, TopicCount);
    //        }
    //        return TopicCount;
    //    }

    //    /// <summary>
    //    /// Returns the number of total Topics, except obsolete ones
    //    /// </summary>
    //    public static int GetTextbookTopicCount()
    //    {
    //        int TopicCount = 0;
    //        string key = "Topics_TextbookTopicCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicCount = SiteProvider.PR.GetTextbookTopicCount();
    //            BasePR.CacheData(key, TopicCount);
    //        }
    //        return TopicCount;
    //    }

    //    /// <summary>
    //    /// Returns the number of total Topics, except obsolete ones
    //    /// </summary>
    //    public static int GetNonTextbookTopicCount()
    //    {
    //        int TopicCount = 0;
    //        string key = "Topics_NonTextbookTopicCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicCount = SiteProvider.PR.GetNonTextbookTopicCount();
    //            BasePR.CacheData(key, TopicCount);
    //        }
    //        return TopicCount;
    //    }

    //    /// <summary>
    //    /// Returns the number of obsolete topics
    //    /// </summary>
    //    public static int GetObsoleteTopicCount()
    //    {
    //        int TopicCount = 0;
    //        string key = "Topics_ObsoleteTopicCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicCount = SiteProvider.PR.GetObsoleteTopicCount();
    //            BasePR.CacheData(key, TopicCount);
    //        }
    //        return TopicCount;
    //    }

    //    /// <summary>
    //    /// Returns the number of obsolete textbook topics
    //    /// </summary>
    //    public static int GetObsoleteTextbookTopicCount()
    //    {
    //        int TopicCount = 0;
    //        string key = "Topics_ObsoleteTextbookTopicCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicCount = SiteProvider.PR.GetObsoleteTextbookTopicCount();
    //            BasePR.CacheData(key, TopicCount);
    //        }
    //        return TopicCount;
    //    }

    //    /// <summary>
    //    /// Returns the number of obsolete Non-textbook topics
    //    /// </summary>
    //    public static int GetObsoleteNonTextbookTopicCount()
    //    {
    //        int TopicCount = 0;
    //        string key = "Topics_ObsoleteNonTextbookTopicCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicCount = SiteProvider.PR.GetObsoleteNonTextbookTopicCount();
    //            BasePR.CacheData(key, TopicCount);
    //        }
    //        return TopicCount;
    //    }

    //    /// <summary>
    //    /// Returns a Topic object with the specified ID
    //    /// </summary>
    //    public static Topic GetTopicByID(int TopicID)
    //    {
    //        Topic Topic = null;
    //        string key = "Topics_Topic_" + TopicID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topic = (Topic)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            Topic = GetTopicFromTopicInfo(SiteProvider.PR.GetTopicByID(TopicID));
    //            BasePR.CacheData(key, Topic);
    //        }
    //        return Topic;
    //    }

    //    /// <summary>
    //    /// Returns a Topic object with the specified Course_Number
    //    /// Bsk
    //    /// </summary>
    //    public static Topic GetTopicByCourseNumber(string Course_Number)
    //    {
    //        Topic Topic = null;
    //        string key = "Topics_Topic_" + Course_Number.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topic = (Topic)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            Topic = GetTopicFromTopicInfo(SiteProvider.PR.GetTopicByCourseNumber(Course_Number));
    //            BasePR.CacheData(key, Topic);
    //        }
    //        return Topic;
    //    }

    //    /// <summary>
    //    /// Returns a Topic object with the specified Course_Number
    //    /// Bsk
    //    /// </summary>
    //    public static Topic GetTopicByUserName(string UserName)
    //    {
    //        Topic Topic = null;
    //        string key = "Topics_Topic_" + UserName.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topic = (Topic)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            Topic = GetTopicFromTopicInfo(SiteProvider.PR.GetTopicByUserName(UserName));
    //            BasePR.CacheData(key, Topic);
    //        }
    //        return Topic;
    //    }

    //    /// <summary>
    //    /// Updates an existing Topic
    //    /// </summary>
    //    public static bool UpdateTopic(bool CorLecture, string Fla_CEType, string Fla_IDNo, string FolderName, 
    //        string HTML, string LastUpdate, bool NoSurvey, int TopicID, string TopicName, 
    //        int SurveyID, bool Compliance, string MetaKW, string MetaDesc, decimal Hours,
    //        string MediaType, string Objectives, string Content, bool Textbook, int CertID, string Method, string GrantBy,
    //        string DOCXFile, string DOCXHoFile, bool Obsolete, int FacilityID, string Course_Number, 
    //       // string CeRef,
    //       //DateTime Release_Date, 
    //        int Minutes, bool Audio_Ind,
    //       bool Apn_Ind, bool Icn_Ind, bool Jcaho_Ind, bool Magnet_Ind, bool Active_Ind, bool Video_Ind,
    //       bool Online_Ind, bool Ebp_Ind, bool Ccm_Ind, bool Avail_Ind, string Cert_Cerp, string Ref_Html,
    //       int AlterID, int Pass_Score,
    //       string Subtitle, 
    //        //decimal Cost,
    //        string Topic_Type, string Img_Name, string Img_Credit,
    //       string Img_Caption, string Accreditation, int Views, int PrimaryViews, int PageReads, bool Featured, string rev,string urlmark, decimal offline_Cost, decimal online_Cost, bool notest,
    //        bool uce_Ind, string shortname, int categoryid, bool hold_ind)
    //    {
    //        Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
    //        Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
    //        FolderName = BizObject.ConvertNullToEmptyString(FolderName);
    //        HTML = BizObject.ConvertNullToEmptyString(HTML);
    //        LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
    //        TopicName = BizObject.ConvertNullToEmptyString(TopicName);
    //        MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
    //        MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
    //        MediaType = BizObject.ConvertNullToEmptyString(MediaType);
    //        Objectives = BizObject.ConvertNullToEmptyString(Objectives);
    //        Content = BizObject.ConvertNullToEmptyString(Content);
    //        Method = BizObject.ConvertNullToEmptyString(Method);
    //        GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
    //        DOCXFile = BizObject.ConvertNullToEmptyString(DOCXFile);
    //        DOCXHoFile = BizObject.ConvertNullToEmptyString(DOCXHoFile);
    //        Course_Number = BizObject.ConvertNullToEmptyString(Course_Number);
    //        //CeRef = BizObject.ConvertNullToEmptyString(CeRef);

    //        //Release_Date = BizObject.ConvertNullToEmptyString(Release_Date);
    //        //Minutes = BizObject.ConvertNullToEmptyString(Minutes);
    //        //Audio_Ind = BizObject.ConvertNullToEmptyString(Audio_Ind);
    //        //Apn_Ind = BizObject.ConvertNullToEmptyString(Apn_Ind);
    //        //Icn_Ind = BizObject.ConvertNullToEmptyString(Icn_Ind);
    //        //Jcaho_Ind = BizObject.ConvertNullToEmptyString(Jcaho_Ind);
    //        //Magnet_Ind = BizObject.ConvertNullToEmptyString(Magnet_Ind);
    //        //Active_Ind = BizObject.ConvertNullToEmptyString(Active_Ind);
    //        //Video_Ind = BizObject.ConvertNullToEmptyString(Video_Ind);
    //        //Online_Ind = BizObject.ConvertNullToEmptyString(Online_Ind);
    //        //Course_Number = BizObject.ConvertNullToEmptyString(Course_Number);
    //        //Ebp_Ind = BizObject.ConvertNullToEmptyString(Ebp_Ind);


    //        //Ccm_Ind = BizObject.ConvertNullToEmptyString(Ccm_Ind);
    //        //Avail_Ind = BizObject.ConvertNullToEmptyString(Avail_Ind);
    //        Cert_Cerp = BizObject.ConvertNullToEmptyString(Cert_Cerp);
    //        Ref_Html = BizObject.ConvertNullToEmptyString(Ref_Html);
    //        //AlterID = BizObject.ConvertNullToEmptyString(AlterID);
    //        //Pass_Score = BizObject.ConvertNullToEmptyString(Pass_Score);
    //        Subtitle = BizObject.ConvertNullToEmptyString(Subtitle);

    //        //Cost = BizObject.ConvertNullToEmptyString(Cost);
    //        Topic_Type = BizObject.ConvertNullToEmptyString(Topic_Type);
    //        Img_Name = BizObject.ConvertNullToEmptyString(Img_Name);
    //        Img_Credit = BizObject.ConvertNullToEmptyString(Img_Credit);
    //        Img_Caption = BizObject.ConvertNullToEmptyString(Img_Caption);
    //        Accreditation = BizObject.ConvertNullToEmptyString(Accreditation);
    //        rev = BizObject.ConvertNullToEmptyString(rev);
    //        urlmark = BizObject.ConvertNullToEmptyString(urlmark);

    //        if (FacilityID == -2)
    //        {
    //            Textbook = true;
    //        }
    //        else
    //        {
    //            Textbook = false;
    //        }
    //        Obsolete = !(Active_Ind);

    //        TopicInfo record = new TopicInfo(CorLecture, Fla_CEType, Fla_IDNo, FolderName, HTML, LastUpdate, 
    //            NoSurvey, TopicID, TopicName, SurveyID, Compliance, MetaKW, MetaDesc, Hours, 
    //            MediaType, Objectives, Content, Textbook, CertID, Method, GrantBy, DOCXFile, DOCXHoFile,
    //            Obsolete, FacilityID, Course_Number,
    //            //CeRef,
    //       //Release_Date,
    //       Minutes, Audio_Ind,
    //       Apn_Ind, Icn_Ind, Jcaho_Ind, Magnet_Ind, Active_Ind, Video_Ind,
    //       Online_Ind, Ebp_Ind, Ccm_Ind, Avail_Ind, Cert_Cerp, Ref_Html,
    //       AlterID, Pass_Score,
    //       Subtitle,
    //       //Cost,
    //       Topic_Type, Img_Name, Img_Credit,
    //       Img_Caption, Accreditation, Views, PrimaryViews, PageReads, Featured, rev, urlmark, offline_Cost, online_Cost, notest, uce_Ind, shortname, categoryid, hold_ind);
    //        bool ret = SiteProvider.PR.UpdateTopic(record);

    //        BizObject.PurgeCacheItems("Topics_Topic_" + TopicID.ToString());
    //        BizObject.PurgeCacheItems("Topics_Topics");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new Topic
    //    /// </summary>
    //    public static int InsertTopic(bool CorLecture, string Fla_CEType, string Fla_IDNo, string FolderName, 
    //        string HTML, string LastUpdate, bool NoSurvey, string TopicName, 
    //        int SurveyID, bool Compliance, string MetaKW, string MetaDesc, decimal Hours, string MediaType,
    //        string Objectives, string Content, bool Textbook, int CertID, string Method, string GrantBy,
    //        string DOCXFile, string DOCXHoFile, bool Obsolete, int FacilityID, string Course_Number, string CeRef,
    //       DateTime Release_Date, int Minutes, bool Audio_Ind,
    //       bool Apn_Ind, bool Icn_Ind, bool Jcaho_Ind, bool Magnet_Ind, bool Active_Ind, bool Video_Ind,
    //       bool Online_Ind, bool Ebp_Ind, bool Ccm_Ind, bool Avail_Ind, string Cert_Cerp, string Ref_Html,
    //       int AlterID, int Pass_Score,
    //       string Subtitle, decimal Cost, string Topic_Type, string Img_Name, string Img_Credit,
    //       string Img_Caption, string Accreditation, int Views, int PrimaryViews, int PageReads, bool Featured, string rev,string urlmark, decimal offline_Cost, decimal online_Cost, bool notest, bool uce_Ind,
    //        string shortname, int categoryid, bool hold_ind)
    //    {
    //        Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
    //        Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
    //        FolderName = BizObject.ConvertNullToEmptyString(FolderName);
    //        HTML = BizObject.ConvertNullToEmptyString(HTML);
    //        LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
    //        TopicName = BizObject.ConvertNullToEmptyString(TopicName);
    //        MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
    //        MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
    //        MediaType = BizObject.ConvertNullToEmptyString(MediaType);
    //        Objectives = BizObject.ConvertNullToEmptyString(Objectives);
    //        Content = BizObject.ConvertNullToEmptyString(Content);
    //        Method = BizObject.ConvertNullToEmptyString(Method);
    //        GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
    //        DOCXFile = BizObject.ConvertNullToEmptyString(DOCXFile);
    //        DOCXHoFile = BizObject.ConvertNullToEmptyString(DOCXHoFile);
    //        Course_Number = BizObject.ConvertNullToEmptyString(Course_Number);
    //        CeRef = BizObject.ConvertNullToEmptyString(CeRef);

    //        //Release_Date = BizObject.ConvertNullToEmptyString(Release_Date);
    //        //Minutes = BizObject.ConvertNullToEmptyString(Minutes);
    //        //Audio_Ind = BizObject.ConvertNullToEmptyString(Audio_Ind);
    //        //Apn_Ind = BizObject.ConvertNullToEmptyString(Apn_Ind);
    //        //Icn_Ind = BizObject.ConvertNullToEmptyString(Icn_Ind);
    //        //Jcaho_Ind = BizObject.ConvertNullToEmptyString(Jcaho_Ind);
    //        //Magnet_Ind = BizObject.ConvertNullToEmptyString(Magnet_Ind);
    //        //Active_Ind = BizObject.ConvertNullToEmptyString(Active_Ind);
    //        //Video_Ind = BizObject.ConvertNullToEmptyString(Video_Ind);
    //        //Online_Ind = BizObject.ConvertNullToEmptyString(Online_Ind);
    //        //Course_Number = BizObject.ConvertNullToEmptyString(Course_Number);
    //        //Ebp_Ind = BizObject.ConvertNullToEmptyString(Ebp_Ind);


    //        //Ccm_Ind = BizObject.ConvertNullToEmptyString(Ccm_Ind);
    //        //Avail_Ind = BizObject.ConvertNullToEmptyString(Avail_Ind);
    //        Cert_Cerp = BizObject.ConvertNullToEmptyString(Cert_Cerp);
    //        Ref_Html = BizObject.ConvertNullToEmptyString(Ref_Html);
    //        //AlterID = BizObject.ConvertNullToEmptyString(AlterID);
    //        //Pass_Score = BizObject.ConvertNullToEmptyString(Pass_Score);
    //        Subtitle = BizObject.ConvertNullToEmptyString(Subtitle);

    //        //Cost = BizObject.ConvertNullToEmptyString(Cost);
    //        Topic_Type = BizObject.ConvertNullToEmptyString(Topic_Type);
    //        Img_Name = BizObject.ConvertNullToEmptyString(Img_Name);
    //        Img_Credit = BizObject.ConvertNullToEmptyString(Img_Credit);
    //        Img_Caption = BizObject.ConvertNullToEmptyString(Img_Caption);
    //        Accreditation = BizObject.ConvertNullToEmptyString(Accreditation);
    //        rev = BizObject.ConvertNullToEmptyString(rev);
    //        urlmark = BizObject.ConvertNullToEmptyString(urlmark);

    //        if (FacilityID == -2)
    //        {
    //            Textbook = true;
    //        }
    //        else
    //        {
    //            Textbook = false;
    //        }
    //        Obsolete = !(Active_Ind);

    //        //Views = BizObject.ConvertNullToEmptyString(Views);
    //        //PrimaryViews = BizObject.ConvertNullToEmptyString(PrimaryViews);
    //        //PageReads = BizObject.ConvertNullToEmptyString(PageReads);
    //        //Featured = BizObject.ConvertNullToEmptyString(Featured);
    //        // NOTE: We add an extra parameter at the end that is not passed into this method:
    //        // By Default, we force FacilityID to be 0 for system-side topics
    //        TopicInfo record = new TopicInfo(CorLecture, Fla_CEType, Fla_IDNo, FolderName, HTML, LastUpdate, 
    //            NoSurvey, 0, TopicName, SurveyID, Compliance, MetaKW, MetaDesc, Hours,
    //            MediaType, Objectives, Content, Textbook, CertID, Method, GrantBy, DOCXFile, DOCXHoFile,
    //            Obsolete, FacilityID, Course_Number,
    //       //     CeRef,
    //       //Release_Date, 
    //       Minutes, Audio_Ind,
    //       Apn_Ind, Icn_Ind, Jcaho_Ind, Magnet_Ind, Active_Ind, Video_Ind,
    //       Online_Ind, Ebp_Ind, Ccm_Ind, Avail_Ind, Cert_Cerp, Ref_Html,
    //       AlterID, Pass_Score,
    //       Subtitle,
    //       //Cost,
    //       Topic_Type, Img_Name, Img_Credit,
    //       Img_Caption, Accreditation, Views, PrimaryViews, PageReads, Featured, rev, urlmark, offline_Cost, online_Cost, notest, uce_Ind, shortname, categoryid, hold_ind);
    //        int ret = SiteProvider.PR.InsertTopic(record);

    //        BizObject.PurgeCacheItems("Topics_Topic");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new Topic
    //    /// </summary>
    //    public static int InsertTextbookTopic(bool CorLecture, string Fla_CEType, string Fla_IDNo, 
    //        string FolderName, string HTML, string LastUpdate, bool NoSurvey, string TopicName, 
    //        int SurveyID, bool Compliance, string MetaKW, string MetaDesc, decimal Hours, string MediaType,
    //        string Objectives, string Content, bool Textbook, int CertID, string Method, string GrantBy,
    //        string DOCXFile, string DOCXHoFile, bool Obsolete, int FacilityID, string Course_Number, string CeRef,
    //       DateTime Release_Date, int Minutes, bool Audio_Ind,
    //       bool Apn_Ind, bool Icn_Ind, bool Jcaho_Ind, bool Magnet_Ind, bool Active_Ind, bool Video_Ind,
    //       bool Online_Ind, bool Ebp_Ind, bool Ccm_Ind, bool Avail_Ind, string Cert_Cerp, string Ref_Html,
    //       int AlterID, int Pass_Score,
    //       string Subtitle, decimal Cost, string Topic_Type, string Img_Name, string Img_Credit,
    //       string Img_Caption, string Accreditation, int Views, int PrimaryViews, int PageReads, bool Featured, string rev,string urlmark, decimal offline_Cost, decimal online_Cost, bool notest, bool uce_Ind,
    //        string shortname, int categoryid, bool hold_ind)
    //    {
    //        Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
    //        Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
    //        FolderName = BizObject.ConvertNullToEmptyString(FolderName);
    //        HTML = BizObject.ConvertNullToEmptyString(HTML);
    //        LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
    //        TopicName = BizObject.ConvertNullToEmptyString(TopicName);
    //        MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
    //        MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
    //        MediaType = BizObject.ConvertNullToEmptyString(MediaType);
    //        Objectives = BizObject.ConvertNullToEmptyString(Objectives);
    //        Content = BizObject.ConvertNullToEmptyString(Content);
    //        Method = BizObject.ConvertNullToEmptyString(Method);
    //        GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
    //        DOCXFile = BizObject.ConvertNullToEmptyString(DOCXFile);
    //        DOCXHoFile = BizObject.ConvertNullToEmptyString(DOCXHoFile);
    //        rev = BizObject.ConvertNullToEmptyString(rev);
    //        urlmark = BizObject.ConvertNullToEmptyString(urlmark);


    //        // NOTE: We add an extra parameter at the end that is not passed into this method:
    //        // By Default, we force FacilityID to be 0 for system-side topics
    //        TopicInfo record = new TopicInfo(CorLecture, Fla_CEType, Fla_IDNo, FolderName, HTML, LastUpdate, 
    //            NoSurvey, 0, TopicName, SurveyID, Compliance, MetaKW, MetaDesc, Hours, MediaType,
    //            Objectives, Content, true, CertID, Method, GrantBy, DOCXFile, DOCXHoFile, Obsolete, 0, Course_Number,
    //       //     CeRef,
    //       //Release_Date,
    //       Minutes, Audio_Ind,
    //       Apn_Ind, Icn_Ind, Jcaho_Ind, Magnet_Ind, Active_Ind, Video_Ind,
    //       Online_Ind, Ebp_Ind, Ccm_Ind, Avail_Ind, Cert_Cerp, Ref_Html,
    //       AlterID, Pass_Score,
    //       Subtitle, 
    //       //Cost,
    //       Topic_Type, Img_Name, Img_Credit,
    //       Img_Caption, Accreditation, Views, PrimaryViews, PageReads, Featured, rev, urlmark, offline_Cost, online_Cost, notest, uce_Ind, shortname, categoryid, hold_ind);
    //        int ret = SiteProvider.PR.InsertTopic(record);

    //        BizObject.PurgeCacheItems("Topics_Topic");
    //        return ret;
    //    }

    //    public static int InsertNonTextbookTopic(bool CorLecture, string Fla_CEType, string Fla_IDNo, 
    //        string FolderName, string HTML, string LastUpdate, bool NoSurvey, string TopicName, 
    //        int SurveyID, bool Compliance, string MetaKW, string MetaDesc, decimal Hours, string MediaType,
    //        string Objectives, string Content, bool Textbook, int CertID, string Method, string GrantBy,
    //        string DOCXFile, string DOCXHoFile, bool Obsolete, int FacilityID, string Course_Number, string CeRef,
    //       DateTime Release_Date, int Minutes, bool Audio_Ind,
    //       bool Apn_Ind, bool Icn_Ind, bool Jcaho_Ind, bool Magnet_Ind, bool Active_Ind, bool Video_Ind,
    //       bool Online_Ind, bool Ebp_Ind, bool Ccm_Ind, bool Avail_Ind, string Cert_Cerp, string Ref_Html,
    //       int AlterID, int Pass_Score,
    //       string Subtitle, decimal Cost, string Topic_Type, string Img_Name, string Img_Credit,
    //       string Img_Caption, string Accreditation, int Views, int PrimaryViews, int PageReads, bool Featured, string rev,string urlmark, decimal offline_Cost, decimal online_Cost, bool notest,
    //        bool uce_Ind, string shortname, int categoryid, bool hold_ind)
    //    {
    //        Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
    //        Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
    //        FolderName = BizObject.ConvertNullToEmptyString(FolderName);
    //        HTML = BizObject.ConvertNullToEmptyString(HTML);
    //        LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
    //        TopicName = BizObject.ConvertNullToEmptyString(TopicName);
    //        MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
    //        MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
    //        MediaType = BizObject.ConvertNullToEmptyString(MediaType);
    //        Objectives = BizObject.ConvertNullToEmptyString(Objectives);
    //        Content = BizObject.ConvertNullToEmptyString(Content);
    //        Method = BizObject.ConvertNullToEmptyString(Method);
    //        GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
    //        DOCXFile = BizObject.ConvertNullToEmptyString(DOCXFile);
    //        DOCXHoFile = BizObject.ConvertNullToEmptyString(DOCXHoFile);
    //        rev = BizObject.ConvertNullToEmptyString(rev);
    //        urlmark = BizObject.ConvertNullToEmptyString(urlmark);

    //        // NOTE: We add an extra parameter at the end that is not passed into this method:
    //        // By Default, we force FacilityID to be 0 for system-side topics
    //        TopicInfo record = new TopicInfo(CorLecture, Fla_CEType, Fla_IDNo, FolderName, HTML, LastUpdate, 
    //            NoSurvey, 0, TopicName, SurveyID, Compliance, MetaKW, MetaDesc, Hours, MediaType,
    //            Objectives, Content, false, CertID, Method, GrantBy, DOCXFile, DOCXHoFile, Obsolete, 0, Course_Number,
    //       //     CeRef,
    //       //Release_Date, 
    //       Minutes, Audio_Ind,
    //       Apn_Ind, Icn_Ind, Jcaho_Ind, Magnet_Ind, Active_Ind, Video_Ind,
    //       Online_Ind, Ebp_Ind, Ccm_Ind, Avail_Ind, Cert_Cerp, Ref_Html,
    //       AlterID, Pass_Score,
    //       Subtitle,
    //       //Cost, 
    //       Topic_Type, Img_Name, Img_Credit,
    //       Img_Caption, Accreditation, Views, PrimaryViews, PageReads, Featured, rev, urlmark, offline_Cost, online_Cost, notest, uce_Ind, shortname, categoryid, hold_ind);
    //        int ret = SiteProvider.PR.InsertTopic(record);

    //        BizObject.PurgeCacheItems("Topics_Topic");
    //        return ret;
    //    }
        
    //    /// <summary>
    //    /// Deletes an existing Topic, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteTopic(int TopicID)
    //    {
    //        bool IsOKToDelete = OKToDelete(TopicID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteTopic(TopicID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }





    //    /// <summary>
    //    /// Returns a Topic object filled with the data taken from the input TopicInfo
    //    /// </summary>
    //    private static Topic GetTopicFromTopicInfo(TopicInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new Topic(record.CorLecture, record.Fla_CEType, record.Fla_IDNo, record.FolderName, 
    //                record.HTML, record.LastUpdate, record.NoSurvey, record.TopicID, record.TopicName, 
    //                record.SurveyID, record.Compliance, record.MetaKW, record.MetaDesc, record.Hours, 
    //                record.MediaType, record.Objectives, record.Content, record.Textbook, record.CertID, record.Method,
    //                record.GrantBy, record.DOCXFile, record.DOCXHoFile, record.Obsolete, record.FacilityID,
    //                record.Course_Number, 
    //                //record.CeRef,
    //                //record.Release_Date,
    //                record.Minutes, record.Audio_Ind,
    //                record.Apn_Ind, record.Icn_Ind, record.Jcaho_Ind, record.Magnet_Ind, record.Active_Ind, record.Video_Ind,
    //       record.Online_Ind, record.Ebp_Ind, record.Ccm_Ind, record.Avail_Ind, record.Cert_Cerp, record.Ref_Html,
    //       record.AlterID, record.Pass_Score,
    //       record.Subtitle, 
    //       //record.Cost, 
    //       record.Topic_Type, record.Img_Name, record.Img_Credit,
    //       record.Img_Caption, record.Accreditation, record.Views, record.PrimaryViews, record.PageReads, record.Featured, record.rev, record.urlmark, record.offline_Cost, record.online_Cost, record.notest, record.uce_Ind
    //       , record.shortname, record.categoryid, record.hold_ind);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of Topic objects filled with the data taken from the input list of TopicInfo
    //    /// </summary>
    //    private static List<Topic> GetTopicListFromTopicInfoList(List<TopicInfo> recordset)
    //    {
    //        List<Topic> Topics = new List<Topic>();
    //        foreach (TopicInfo record in recordset)
    //            Topics.Add(GetTopicFromTopicInfo(record));
    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns a CheckBoxListTopic object filled with the data taken from the input CheckBoxListTopicInfo
    //    /// </summary>
    //    private static CheckBoxListTopicInfo GetCheckBoxListTopicFromCheckBoxListTopicInfo(CheckBoxListTopicInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new CheckBoxListTopicInfo(record.TopicID, record.TopicName, record.Selected);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of CheckBoxListTopic objects filled with the data taken from the input list of CheckBoxListTopicInfo
    //    /// </summary>
    //    private static List<CheckBoxListTopicInfo> GetCheckBoxListTopicListFromCheckBoxListTopicInfoList(List<CheckBoxListTopicInfo> recordset)
    //    {
    //        List<CheckBoxListTopicInfo> CheckBoxListTopics = new List<CheckBoxListTopicInfo>();
    //        foreach (CheckBoxListTopicInfo record in recordset)
    //            CheckBoxListTopics.Add(GetCheckBoxListTopicFromCheckBoxListTopicInfo(record));
    //        return CheckBoxListTopics;
    //    }

        
    //    /// <summary>
    //    /// Returns a collection with all Topics for the specified Category
    //    /// </summary>
    //    public static List<Topic> GetTopicsByCategoryID(int CategoryID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_Topics_" + CategoryID.ToString() + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset;
    //            switch (CategoryID)
    //            {
    //                case 1000001:
    //                    // ALL non-textbook topics (not obsolete)
    //                    recordset = SiteProvider.PR.GetNonTextbookTopics(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //                case 1000002:
    //                    // Non-textbook topics with NO Categories (not obsolete)
    //                    recordset = SiteProvider.PR.GetNonTextbookTopicsWithNoCategories(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //                case 1000003:
    //                    // Obsolete Non-textbook topics
    //                    recordset = SiteProvider.PR.GetObsoleteNonTextbookTopics(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //                case 1000004:
    //                    // ALL textbook topics (not obsolete) 
    //                    recordset = SiteProvider.PR.GetTextbookTopics(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //                case 1000005:
    //                    // Textbook topics with NO Categories (not obsolete) 
    //                    recordset = SiteProvider.PR.GetTextbookTopicsWithNoCategories(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //                case 1000006:
    //                    // Obsolete Textbook topics
    //                    recordset = SiteProvider.PR.GetObsoleteTextbookTopics(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;

    //                case 1000007:
    //                    //CEDirect Topics
    //                    //The facility Id is '0' for CEDiect Topics
    //                    recordset = SiteProvider.PR.GetNonTextbookTopicsByFacilityId(0, cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;

    //                case 1000008:
    //                    //PearlsReview Topics
    //                    //The facility Id is '-1' for CEDiect Topics
    //                    recordset = SiteProvider.PR.GetNonTextbookTopicsByFacilityId(-1, cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;

    //                case 1000009:
    //                    //Complaince Topics
    //                    //The facility Id is '-3' for CEDiect Topics
    //                    recordset = SiteProvider.PR.GetNonTextbookTopicsByFacilityId(-3, cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;


    //                default:
    //                    // ALL topics assigned to a categoryID 
    //                    recordset = SiteProvider.PR.GetTopicsByCategoryID(CategoryID, cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //            }

    //        }
    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Topics for the specified Category, with test info for userid
    //    /// </summary>
    //    public static List<Topic> GetTopicsByCategoryIDAndUserIDWithTestInfo(int CategoryID, int UserID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_Topics_" + CategoryID.ToString() + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset;
    //            switch (CategoryID)
    //            {
    //                case 1000001:
    //                    // ALL non-textbook topics (not obsolete)
    //                    recordset = SiteProvider.PR.GetNonTextbookTopics(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //                case 1000002:
    //                    // Non-textbook topics with NO Categories (not obsolete)
    //                    recordset = SiteProvider.PR.GetNonTextbookTopicsWithNoCategories(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //                case 1000003:
    //                    // Obsolete Non-textbook topics
    //                    recordset = SiteProvider.PR.GetObsoleteNonTextbookTopics(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //                case 1000004:
    //                    // ALL textbook topics (not obsolete) 
    //                    recordset = SiteProvider.PR.GetTextbookTopics(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //                case 1000005:
    //                    // Textbook topics with NO Categories (not obsolete) 
    //                    recordset = SiteProvider.PR.GetTextbookTopicsWithNoCategories(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //                case 1000006:
    //                    // Obsolete Textbook topics
    //                    recordset = SiteProvider.PR.GetObsoleteTextbookTopics(cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //                default:
    //                    // ALL topics assigned to a categoryID 
    //                    recordset = SiteProvider.PR.GetTopicsByCategoryID(CategoryID, cSortExpression);
    //                    Topics = GetTopicListFromTopicInfoList(recordset);
    //                    BasePR.CacheData(key, Topics);
    //                    break;
    //            }

    //        }
    //        return Topics;
    //    }


    //    /// <summary>
    //    /// Returns a collection with all Topics which are text book by category id
    //    /// </summary>
    //     public static List<Topic> GetTextBookTopicsByCategoryId(int categoryid, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;

    //        List<TopicInfo> recordset;
    //        recordset = SiteProvider.PR.GetTextBookTopicsByCategoryId(categoryid, cSortExpression);
    //        Topics = GetTopicListFromTopicInfoList(recordset);

    //        return Topics;
    //    }

    //     /// <summary>
    //     /// Returns a collection with all Topics which are text book by category id
    //     /// </summary>
    //     public static List<Topic> GetTopicsByCategoryName(string categoryname, string cSortExpression)
    //     {
    //         if (cSortExpression == null)
    //             cSortExpression = "";

    //         // provide default sort
    //         if (cSortExpression.Length == 0)
    //             cSortExpression = "TopicName";

    //         List<Topic> Topics = null;

    //         List<TopicInfo> recordset;
    //         recordset = SiteProvider.PR.GetTopicsByCategoryName(categoryname, cSortExpression);
    //         Topics = GetTopicListFromTopicInfoList(recordset);

    //         return Topics;
    //     }

    //     /// <summary>
    //    /// Returns a collection with all Topics which are text book by category id
    //    /// </summary>
    //     public static List<Topic> GetTextAndNonTextTopicsByCategoryId(int categoryid, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;

    //        List<TopicInfo> recordset;
    //        recordset = SiteProvider.PR.GetTextAndNonTextTopicsByCategoryId(categoryid, cSortExpression);
    //        Topics = GetTopicListFromTopicInfoList(recordset);

    //        return Topics;
    //    }



    //     /// <summary>
    //     /// Returns a collection with all Topics which are text book by category id
    //     /// </summary>
    //     public static List<Topic> GetFacilityTopics()
    //     {
    //          List<Topic> Topics = null;

    //         List<TopicInfo> recordset;
    //         recordset = SiteProvider.PR.GetFacilityTopics();
    //         Topics = GetTopicListFromTopicInfoList(recordset);

    //         return Topics;
    //     }


        

    //    /// <summary>
    //    /// Returns a collection with all Topics where search text is found in TopicName, subtitle or objectives
    //    /// </summary>
    //    public static List<Topic> GetTopicsBySearchText(string cSearchText, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;

    //        List<TopicInfo> recordset;
    //        recordset = SiteProvider.PR.GetTopicsBySearchText(cSearchText, cSortExpression);
    //        Topics = GetTopicListFromTopicInfoList(recordset);

    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all CEDirect Topics where search text is found in TopicName, subtitle or objectives
    //    /// </summary>
    //    public static List<Topic> GetTopicsBySearchTextandFacility(string cSearchText,int facilityid, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;

    //        List<TopicInfo> recordset;
    //        recordset = SiteProvider.PR.GetTopicsBySearchTextandFacility(cSearchText,facilityid, cSortExpression);
    //        Topics = GetTopicListFromTopicInfoList(recordset);

    //        return Topics;
    //    }


    //    /// <summary>
    //    /// Returns a collection with all CE Retail Topics where search text is found in TopicName, subtitle or objectives
    //    /// </summary>
    //    public static List<Topic> GetRetailTopicsBySearchTextandFacility(string cSearchText, int facilityid, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;

    //        List<TopicInfo> recordset;
    //        recordset = SiteProvider.PR.GetRetailTopicsBySearchTextandFacility(cSearchText, facilityid, cSortExpression);
    //        Topics = GetTopicListFromTopicInfoList(recordset);

    //        return Topics;
    //    }





    //    /// <summary>
    //    /// Returns a collection with all Topics in progress for userid
    //    /// </summary>
    //    public static List<Topic> GetTopicsInProgressByUserID(int UserID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        //string key = "Topics_Topics_" + CategoryID.ToString() + cSortExpression.ToString();

    //        //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        //{
    //        //    Topics = (List<Topic>)BizObject.Cache[key];
    //        //}
    //        //else
    //        //{

    //        List<TopicInfo> recordset;
 
    //        // ALL topics assigned to a categoryID 
    //        recordset = SiteProvider.PR.GetTopicsInProgressByUserID(UserID, cSortExpression);
    //        Topics = GetTopicListFromTopicInfoList(recordset);

    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all Topics for the specified LectureDefinition
    //    /// </summary>
    //    public static List<Topic> GetTopicsByLectureID(int LectureID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_TopicsByLecture_" + LectureID.ToString() + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            // topics by Lecture ID
    //            List<TopicInfo> recordset = SiteProvider.PR.GetTopicsByLectureID(LectureID, cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);
    //        }
    //            return Topics;
    //    }
        
    //    ///// <summary>
    //    ///// Returns a collection with all Topics for the specified Category
    //    ///// </summary>
    //    //public static List<Topic> GetAvailableTopicsByCategoryIDAndUserName(int CategoryID, string UserName, string cSortExpression)

    //    //{
    //    //    if (cSortExpression == null)
    //    //        cSortExpression = "";

    //    //    // provide default sort
    //    //    if (cSortExpression.Length == 0)
    //    //        cSortExpression = "TopicName";

    //    //    List<Topic> Topics = null;
    //    //    string key = "Topics_AvailableTopics_" + CategoryID.ToString() + UserName.Trim().ToString()+cSortExpression.ToString();

    //    //    if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //    //    {
    //    //        Topics = (List<Topic>)BizObject.Cache[key];
    //    //    }
    //    //    else
    //    //    {
    //    //        List<TopicInfo> recordset = SiteProvider.PR.GetAvailableTopicsByCategoryIDAndUserName(CategoryID, UserName, cSortExpression);
    //    //        Topics = GetTopicListFromTopicInfoList(recordset);
    //    //        BasePR.CacheData(key, Topics);

    //    //    }
    //    //    return Topics;
    //    //}

    //    /// <summary>
    //    /// Returns a collection with all Topics for the specified Category that are not already in a user's
    //    /// portfolio
    //    /// </summary>
    //    public static List<Topic> GetAvailableTopicsByCategoryIDAndUserID(int CategoryID, int UserID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "TopicName";

    //        List<Topic> Topics = null;
    //        string key = "Topics_AvailableTopics_" + CategoryID.ToString() + "_UserID_" + UserID.ToString() + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            Topics = (List<Topic>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicInfo> recordset = SiteProvider.PR.GetAvailableTopicsByCategoryIDAndUserID(CategoryID, UserID, cSortExpression);
    //            Topics = GetTopicListFromTopicInfoList(recordset);
    //            BasePR.CacheData(key, Topics);

    //        }
    //        return Topics;
    //    }

    //    /// <summary>
    //    /// Returns the highest TopicID
    //    /// (used to find the last ID inserted because all other methods
    //    /// are failing)
    //    /// </summary>
    //    public static int GetHighestTopicID()
    //    {
    //        int TopicID = 0;
    //        TopicID = SiteProvider.PR.GetHighestTopicID();
    //        return TopicID;
    //    }

    //    /// <summary>
    //    /// Returns the number of total Topics for a Category, except Obsolete ones
    //    /// </summary>
    //    public static int GetTopicCount(int CategoryID)
    //    {
    //        int TopicCount = 0;
    //        string key = "Topics_TopicCount_" + CategoryID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicCount = SiteProvider.PR.GetTopicCountByCategoryID(CategoryID);
    //            BasePR.CacheData(key, TopicCount);
    //        }
    //        return TopicCount;
    //    }

    //    /// <summary>
    //    /// Returns a Topic object with the specified ID
    //    /// This overloaded method adds the website base URL to all
    //    /// URLs in the HTML field that begin with PP/, PDF/, images/
    //    /// </summary>
    //    public static Topic GetTopicByIDWithFixedURLs(int TopicID)
    //    {
    //        Topic Topic = GetTopicByID(TopicID);
    //        Topic.HTML = Topic.HTML.Replace("/PP/", "PP/");
    //        Topic.HTML = Topic.HTML.Replace("/PDF/", "PDF/");
    //        Topic.HTML = Topic.HTML.Replace("/IMAGES/", "IMAGES/");
    //        Topic.HTML = Topic.HTML.Replace("/pp/", "pp/");
    //        Topic.HTML = Topic.HTML.Replace("/pdf/", "pdf/");
    //        Topic.HTML = Topic.HTML.Replace("/images/", "images/");
    //        Topic.HTML = Topic.HTML.Replace("/bib.htm", "bib.htm");
    //        Topic.HTML = Topic.HTML.Replace("/disclaimer.htm", "disclaimer.htm");

    //        Topic.HTML = Topic.HTML.Replace("PP/", "http://www.pearlsreview.com/PP/");
    //        Topic.HTML = Topic.HTML.Replace("PDF/", "http://www.pearlsreview.com/PDF/");
    //        Topic.HTML = Topic.HTML.Replace("IMAGES/", "http://www.pearlsreview.com/IMAGES/");
    //        Topic.HTML = Topic.HTML.Replace("pp/", "http://www.pearlsreview.com/pp/");
    //        Topic.HTML = Topic.HTML.Replace("pdf/", "http://www.pearlsreview.com/pdf/");
    //        Topic.HTML = Topic.HTML.Replace("images/", "http://www.pearlsreview.com/images/");
    //        Topic.HTML = Topic.HTML.Replace("bib.htm", "http://www.pearlsreview.com/bib.htm");
    //        Topic.HTML = Topic.HTML.Replace("disclaimer.htm", "http://www.pearlsreview.com/disclaimer.htm");

    //        return Topic;
    //    }

    //    /// </summary>
    //    public static bool UpdateTopicHTML(int TopicID, string HTML, string LastUpdate, string FolderName)
    //    {
    //        bool ret = SiteProvider.PR.UpdateTopicHTML(TopicID, HTML, LastUpdate, FolderName);
    //        return ret;
    //    }
    //    public static bool UpdateTopicImage(int TopicID, string image)
    //    {
    //        bool ret = SiteProvider.PR.UpdateTopicImage(TopicID,image);
    //        return ret;
    //    }

    //    public static bool UpdateTextbookTopicHTML(int TopicID, string HTML, string LastUpdate, string FolderName,
    //        string DOCXFile, string DOCXHoFile)
    //    {
    //        bool ret = SiteProvider.PR.UpdateTextbookTopicHTML(TopicID, HTML, LastUpdate, FolderName, DOCXFile, DOCXHoFile);
    //        return ret;
    //    }

    //    public static bool UpdateTopicEditableFields(int TopicID, bool CorLecture,
    //        string Fla_CEType, string Fla_IDNo, string LastUpdate, bool NoSurvey,
    //        int SurveyID, bool Compliance, string MetaKW, string MetaDesc,
    //        decimal Hours, string MediaType, string Objectives, string Content,
    //        bool Textbook, int CertID, string Method, string GrantBy, bool Obsolete, string TopicName)
    //    {
    //        Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
    //        Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
    //        LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
    //        MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
    //        MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
    //        MediaType = BizObject.ConvertNullToEmptyString(MediaType);
    //        Objectives = BizObject.ConvertNullToEmptyString(Objectives);
    //        Content = BizObject.ConvertNullToEmptyString(Content);
    //        Method = BizObject.ConvertNullToEmptyString(Method);
    //        GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
    //        TopicName = BizObject.ConvertNullToEmptyString(TopicName);

    //        bool ret = SiteProvider.PR.UpdateTopicEditableFields(TopicID, CorLecture,
    //            Fla_CEType, Fla_IDNo, LastUpdate, NoSurvey,
    //            SurveyID, Compliance, MetaKW, MetaDesc,
    //            Hours, MediaType, Objectives, Content,
    //            Textbook, CertID, Method, GrantBy, Obsolete, TopicName);
    //        return ret;
    //    }

    //    public static bool UpdateTextbookTopicEditableFields(int TopicID, bool CorLecture,
    //        string Fla_CEType, string Fla_IDNo, string LastUpdate, bool NoSurvey,
    //        int SurveyID, bool Compliance, string MetaKW, string MetaDesc,
    //        decimal Hours, string MediaType, string Objectives, string Content,
    //        bool Textbook, int CertID, string Method, string GrantBy, bool Obsolete, string TopicName)
    //    {
    //        Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
    //        Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
    //        LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
    //        MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
    //        MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
    //        MediaType = BizObject.ConvertNullToEmptyString(MediaType);
    //        Objectives = BizObject.ConvertNullToEmptyString(Objectives);
    //        Content = BizObject.ConvertNullToEmptyString(Content);
    //        Method = BizObject.ConvertNullToEmptyString(Method);
    //        GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
    //        TopicName = BizObject.ConvertNullToEmptyString(TopicName);

    //        bool ret = SiteProvider.PR.UpdateTopicEditableFields(TopicID, CorLecture,
    //            Fla_CEType, Fla_IDNo, LastUpdate, NoSurvey,
    //            SurveyID, Compliance, MetaKW, MetaDesc,
    //            Hours, MediaType, Objectives, Content,
    //            true, CertID, Method, GrantBy, Obsolete, TopicName);
    //        return ret;
    //    }


    //    public static bool UpdateNonTextbookTopicEditableFields(int TopicID, bool CorLecture,
    //        string Fla_CEType, string Fla_IDNo, string LastUpdate, bool NoSurvey,
    //        int SurveyID, bool Compliance, string MetaKW, string MetaDesc,
    //        decimal Hours, string MediaType, string Objectives, string Content,
    //        bool Textbook, int CertID, string Method, string GrantBy, bool Obsolete, string TopicName)
    //    {
    //        Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);
    //        Fla_IDNo = BizObject.ConvertNullToEmptyString(Fla_IDNo);
    //        LastUpdate = BizObject.ConvertNullToEmptyString(LastUpdate);
    //        MetaKW = BizObject.ConvertNullToEmptyString(MetaKW);
    //        MetaDesc = BizObject.ConvertNullToEmptyString(MetaDesc);
    //        MediaType = BizObject.ConvertNullToEmptyString(MediaType);
    //        Objectives = BizObject.ConvertNullToEmptyString(Objectives);
    //        Content = BizObject.ConvertNullToEmptyString(Content);
    //        Method = BizObject.ConvertNullToEmptyString(Method);
    //        GrantBy = BizObject.ConvertNullToEmptyString(GrantBy);
    //        TopicName = BizObject.ConvertNullToEmptyString(TopicName);

    //        bool ret = SiteProvider.PR.UpdateTopicEditableFields(TopicID, CorLecture,
    //            Fla_CEType, Fla_IDNo, LastUpdate, NoSurvey,
    //            SurveyID, Compliance, MetaKW, MetaDesc,
    //            Hours, MediaType, Objectives, Content,
    //            false, CertID, Method, GrantBy, Obsolete, TopicName);
    //        return ret;
    //    }

    //    public static bool UpdateTopicCategoryAssignments(int TopicID, string CategoryIDAssignments)
    //    {
    //        bool ret = SiteProvider.PR.UpdateTopicCategoryAssignments(TopicID, CategoryIDAssignments);
    //        // TODO: release cache?
    //        return ret;
    //    }

    //    public static bool UpdateTopicActiveStatus(bool status, int topicid)
    //    {
    //        bool ret = SiteProvider.PR.UpdateTopicActiveStatus(status,topicid);
    //        // TODO: release cache?
    //        return ret;
    //    }

    //    public static int UpdateTopicInProgressByUserID(int UserID, int TopicID)
    //    {
    //        int ret = SiteProvider.PR.UpdateTopicInProgressByUserID(UserID, TopicID);
    //        // TODO: release cache?
    //        return ret;
    //    }
    //    /// <summary>
    //    /// Checks to see if a Topic can be deleted safely
    //    /// (NO if it has any activity involving it in other tables,
    //    /// except for Attached Categories, which can be safely
    //    /// detached during the topic delete and topicdetails records
    //    /// which can be safely deleted also)
    //    /// </summary>
    //    public static bool OKToDelete(int TopicID)
    //    {
    //        int TestRecords = SiteProvider.PR.GetTestCountByTopicID(TopicID);
    //        return (TestRecords == 0);
    //    }

    //    /// <summary>
    //    /// Deletes an existing Topic and breaks any links to categories
    //    /// and deletes any TopicDetails records
    //    /// (questions and answers stored in coursedetail.dbf)
    //    ///
    //    /// NOTE: This overload's second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteTopic(int TopicID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        // TODO: Need some kind of transaction around all of this

    //        // get rid of question and answer records
            
    //        // TODO: We need to see about deleting from the TestDefinition table
    //        // now that we're using that structure instead of the old TopicDetails approach.

    //        // NOTE: I'm calling the DAL directly here for TopicDetails
    //        //SiteProvider.PR.DeleteTopicDetailsByTopicID(TopicID);
    //        // break links to all categories
    //        //DetachTopicFromAllCategories(TopicID);

    //        bool ret = SiteProvider.PR.DeleteTopic(TopicID);
    //        //         new RecordDeletedEvent("Topic", iID, null).Raise();
    //        BizObject.PurgeCacheItems("Topics_Topic");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Assign Topic to a Category
    //    /// </summary>
    //    public static bool AssignTopicToCategory(int TopicID, int CategoryID)
    //    {
    //        bool ret = SiteProvider.PR.AssignTopicToCategory(TopicID, CategoryID);
    //        // TODO: release cache?
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Detach Topic Assignment from a Category
    //    /// </summary>
    //    public static bool DetachTopicFromCategory(int TopicID, int CategoryID)
    //    {
    //        bool ret = SiteProvider.PR.DetachTopicFromCategory(TopicID, CategoryID);
    //        // TODO: release cache?
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Detach Topic Assignment from ALL Categories
    //    /// (used just before deleting a topic completely so there is
    //    /// no leftover garbage -- we don't have cascading delete at the moment)
    //    /// </summary>
    //    //public static bool DetachTopicFromAllCategories(int TopicID)
    //    //{
    //    //    bool ret = SiteProvider.PR.DetachTopicFromAllCategories(TopicID);
    //    //    // TODO: release cache?
    //    //    return ret;
    //    //}

    //    public static System.Data.DataSet GetRecommendationTopicsByTopicId(int TopicID)
    //    {
    //        DataSet dsTopic = SiteProvider.PR.GetRecommendationTopicsByTopicId(TopicID);
    //        return dsTopic;
    //    }

    //    public static int GetSectionDiscountIdByTopicId(int TopicID)
    //    {
    //        int iDiscountId = SiteProvider.PR.GetSectionDiscountIdByTopicId(TopicID);
    //            return iDiscountId;
    //    }

    //    public static int IsPTOTTopic(int TopicId)
    //    {
    //        int iReturn = SiteProvider.PR.IsPTOTTopic(TopicId);
    //        return iReturn;
    //    }



    //}
    /// <summary>
    /// /////////Media Content
    /// </summary>
    //public class MediaContent:BasePR
    //{
      
    //    public MediaContent(int mcid, int topicid, string mediatype, string asseturl, string description)
    //    {
    //        this.mcid = mcid;
    //        this.topicid = topicid;
    //        this.mediatype = mediatype;
    //        this.asseturl = asseturl;
    //        this.description = description;
    //    }

    //    private int _mcid = 0;
    //    public int mcid
    //    {
    //        get { return _mcid; }
    //        protected set { _mcid = value; }
    //    }
    //    private int _topicid = 0;
    //    public int topicid
    //    {
    //        get { return _topicid; }
    //        protected set { _topicid = value; }
    //    }
    //    private string _mediatype ;
    //    public string mediatype
    //    {
    //        get { return _mediatype; }
    //        protected set { _mediatype = value; }
    //    }
    //    private string _asseturl = "";
    //    public string asseturl
    //    {
    //        get { return _asseturl; }
    //        protected set { _asseturl = value; }
    //    }
    //    private string _description = "";
    //    public string description
    //    {
    //        get { return _description; }
    //        protected set { _description = value; }
    //    }



    //    public static bool UpdateMediaContent(int mcid, int topicid, string mediatype,string asseturl,string description)
    //    {
    //          MediaContentInfo record = new MediaContentInfo(mcid,topicid,mediatype,asseturl,description);
    //        bool ret = SiteProvider.PR.UpdateMediaContent(record);

    //        BizObject.PurgeCacheItems("MediaContents_MediaContent_" + mcid.ToString());
    //        BizObject.PurgeCacheItems("MediaContents_MediaContents");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new TopicComplianceLink
    //    /// </summary>
    //    public static int InsertMediaContent(int topicid, string mediatype, string asseturl, string description)
    //    {
    //         MediaContentInfo record = new MediaContentInfo(0, topicid, mediatype, asseturl, description);
    //        int ret = SiteProvider.PR.InsertMediaContent(record);

    //        BizObject.PurgeCacheItems("MediaContents_MediaContent");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing TopicComplianceLink, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteMediaContent(int mcid)
    //    {
    //        bool IsOKToDelete = OKToDelete(mcid);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteMediaContent(mcid, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }


    //    public static bool DeleteMediaContent(int mcid, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteMediaContent(mcid);
    //        //         new RecordDeletedEvent("TopicComplianceLink", TopCompID, null).Raise();
    //        BizObject.PurgeCacheItems("MediaContents_MediaContent");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a TopicComplianceLink can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int mcid)
    //    {
    //        return true;
            
    //    }


    //    /// <summary>
    //    /// Returns a collection with all TopicComplianceLinks for a FacilityID
    //    /// </summary>
    //    public static List<MediaContent> GetMediaContentByTopicID(int TopicId, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<MediaContent> MediaContents = null;
    //        string key = "MediaContents_MediaContentByTopicID_" +
    //            TopicId.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            MediaContents = (List<MediaContent>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<MediaContentInfo> recordset =
    //                SiteProvider.PR.GetMediaContentByTopicID(TopicId, cSortExpression);
    //            MediaContents = GetMediaContentListFromMediaContentInfoList(recordset);
    //            BasePR.CacheData(key, MediaContents);
    //        }
    //        return MediaContents;
    //    }


    //    /// <summary>
    //    /// Returns a TopicComplianceLink object filled with the data taken from the input TopicComplianceLinkInfo
    //    /// </summary>
    //    private static MediaContent GetMediaContentFromMediaContentInfo(MediaContentInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new MediaContent(record.mcid,record.topicid,record.mediatype,record.asseturl,record.description);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of TopicComplianceLink objects filled with the data taken from the input list of TopicComplianceLinkInfo
    //    /// </summary>
    //    private static List<MediaContent> GetMediaContentListFromMediaContentInfoList(List<MediaContentInfo> recordset)
    //    {
    //        List<MediaContent> MediaContents = new List<MediaContent>();
    //        foreach (MediaContentInfo record in recordset)
    //            MediaContents.Add(GetMediaContentFromMediaContentInfo(record));
    //        return MediaContents;
    //    }
    //}






    //////////////////////////////////////////////////////////
    ///<summary>
    ///Topic Collection
    ///</summary>
    ///
    //public class TopicCollection : BasePR
    //{
    //    private int _TopicID = 0;
    //    public int TopicID
    //    {
    //        get { return _TopicID; }
    //        set { _TopicID = value; }
    //    }
    //    private int _ChapterID = 0;
    //    public int ChapterID
    //    {
    //        get { return _ChapterID; }
    //        set { _ChapterID = value; }
    //    }
    //    private int _ChapterNumber = 0;
    //    public int ChapterNumber
    //    {
    //        get { return _ChapterNumber; }
    //        set { _ChapterNumber = value; }
    //    }
    //    private int _TCID=0;
    //    public int TCID
    //    {
    //        get { return _TCID; }
    //        set { _TCID = value; }
    //    }
    //    public TopicCollection(int TopicID, int ChapterID, int ChapterNumber,int TCID)
    //    {
    //        this.TopicID = TopicID;
    //        this.ChapterID = ChapterID;
    //        this.ChapterNumber = ChapterNumber;
    //        this.TCID = TCID;

    //    }

    //    /// <summary>
    //    /// Returns a collection with all TopicComplianceLinks for a FacilityID
    //    /// </summary>
    //    public static List<TopicCollection> GetTopicCollectionsByTopicID(int TopicID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<TopicCollection> TopicCollections = null;
    //        string key = "TopicCollections_TopicCollectionsByTopicID_" +
    //            TopicID.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicCollections = (List<TopicCollection>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicCollectionInfo> recordset =
    //                SiteProvider.PR.GetTopicCollectionsByTopicID(TopicID, cSortExpression);
    //            TopicCollections = GetTopicCollectionListFromTopicCollectionInfoList(recordset);
    //            BasePR.CacheData(key, TopicCollections);
    //        }
    //        return TopicCollections;
    //    }

    //    /// <summary>
    //    /// Returns a TopicComplianceLink object filled with the data taken from the input TopicComplianceLinkInfo
    //    /// </summary>
    //    private static TopicCollection GetTopicCollectionFromTopicCollectionInfo(TopicCollectionInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new TopicCollection(record.topicid, record.chapterid, record.chapternumber,record.tcid);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of TopicComplianceLink objects filled with the data taken from the input list of TopicComplianceLinkInfo
    //    /// </summary>
    //    private static List<TopicCollection> GetTopicCollectionListFromTopicCollectionInfoList(List<TopicCollectionInfo> recordset)
    //    {
    //        List<TopicCollection> TopicCollections = new List<TopicCollection>();
    //        foreach (TopicCollectionInfo record in recordset)
    //            TopicCollections.Add(GetTopicCollectionFromTopicCollectionInfo(record));
    //        return TopicCollections;
    //    }


    //    public static DataSet GetTopicCollectionsDataSetByTopicID(int TopicID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        DataSet TopicCollections = null;
    //        string key = "TopicCollectionsds_TopicCollectionsdsByTopicID_" +
    //            TopicID.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicCollections = (DataSet)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicCollections =
    //                SiteProvider.PR.GetTopicCollectionsDataSetByTopicID(TopicID, cSortExpression);
    //                        }
    //        return TopicCollections;
    //    }


    //    public static bool UpdateTopicCollections(int chapterid, int chapternumber,int tcid)
    //    {
    //        bool ret = SiteProvider.PR.UpdateTopicCollections(chapterid, chapternumber,tcid);
    //        // TODO: release cache?
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new TopicComplianceLink
    //    /// </summary>
    //    public static int InsertTopicCollection(int topicid, int chapterid, int chapternumber,int tcid)
    //    {
    //        TopicCollectionInfo record = new TopicCollectionInfo(topicid, chapterid, chapternumber,tcid);
    //        int ret = SiteProvider.PR.InsertTopicCollection(record);

    //        BizObject.PurgeCacheItems("TopicCollections_TopicCollection");
    //        return ret;
    //    }


     
    //    /// <summary>
    //    /// Deletes an existing TopicComplianceLink, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteTopicColection(int Topicid)
    //    {
    //        bool IsOKToDelete = OKToDelete(Topicid);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteTopicCollection(Topicid, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }


    //    public static bool DeleteTopicCollection(int Topicid, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteTopicCollection(Topicid);
    //        //         new RecordDeletedEvent("TopicComplianceLink", TopCompID, null).Raise();
    //        BizObject.PurgeCacheItems("DeleteTopicColections_DeleteTopicColection");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a TopicComplianceLink can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int Topicid)
    //    {
    //        return true;

    //    }

    //}





    ////////////////////////////////////////////////////////////
    /// <summary>
    /// TopicComplianceLink business object class
    /// </summary>
    //public class TopicComplianceLink : BasePR
    //{
    //    private int _TopCompID = 0;
    //    public int TopCompID
    //    {
    //        get { return _TopCompID; }
    //        protected set { _TopCompID = value; }
    //    }

    //    private int _TopicID = 0;
    //    public int TopicID
    //    {
    //        get { return _TopicID; }
    //        set { _TopicID = value; }
    //    }

    //    private int _FacID = 0;
    //    public int FacID
    //    {
    //        get { return _FacID; }
    //        set { _FacID = value; }
    //    }

    //    private string _ComplFile = "";
    //    public string ComplFile
    //    {
    //        get { return _ComplFile; }
    //        set { _ComplFile = value; }
    //    }


    //   public TopicComplianceLink(int TopCompID, int TopicID, int FacID, string ComplFile)
    //  {
    //        this.TopCompID = TopCompID;
    //        this.TopicID = TopicID;
    //        this.FacID = FacID;
    //        this.ComplFile = ComplFile;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = TopicComplianceLink.DeleteTopicComplianceLink(this.TopCompID);
    //        if (success)
    //            this.TopCompID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return TopicComplianceLink.UpdateTopicComplianceLink(this.TopCompID, this.TopicID, this.FacID, this.ComplFile);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all TopicComplianceLinks
    //    /// </summary>
    //    public static List<TopicComplianceLink> GetTopicComplianceLinks(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<TopicComplianceLink> TopicComplianceLinks = null;
    //        string key = "TopicComplianceLinks_TopicComplianceLinks_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicComplianceLinks = (List<TopicComplianceLink>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicComplianceLinkInfo> recordset = SiteProvider.PR.GetTopicComplianceLinks(cSortExpression);
    //            TopicComplianceLinks = GetTopicComplianceLinkListFromTopicComplianceLinkInfoList(recordset);
    //            BasePR.CacheData(key, TopicComplianceLinks);
    //        }
    //        return TopicComplianceLinks;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all TopicComplianceLinks for a FacilityID
    //    /// </summary>
    //    public static List<TopicComplianceLink> GetTopicComplianceLinksByFacilityID(int FacilityID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<TopicComplianceLink> TopicComplianceLinks = null;
    //        string key = "TopicComplianceLinks_TopicComplianceLinksByFacilityID_" + 
    //            FacilityID.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicComplianceLinks = (List<TopicComplianceLink>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicComplianceLinkInfo> recordset = 
    //                SiteProvider.PR.GetTopicComplianceLinksByFacilityID(FacilityID, cSortExpression);
    //            TopicComplianceLinks = GetTopicComplianceLinkListFromTopicComplianceLinkInfoList(recordset);
    //            BasePR.CacheData(key, TopicComplianceLinks);
    //        }
    //        return TopicComplianceLinks;
    //    }


    //    /// <summary>
    //    /// Returns the number of total TopicComplianceLinks
    //    /// </summary>
    //    public static int GetTopicComplianceLinkCount()
    //    {
    //        int TopicComplianceLinkCount = 0;
    //        string key = "TopicComplianceLinks_TopicComplianceLinkCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicComplianceLinkCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicComplianceLinkCount = SiteProvider.PR.GetTopicComplianceLinkCount();
    //            BasePR.CacheData(key, TopicComplianceLinkCount);
    //        }
    //        return TopicComplianceLinkCount;
    //    }

    //    /// <summary>
    //    /// Returns a TopicComplianceLink object with the specified ID
    //    /// </summary>
    //    public static TopicComplianceLink GetTopicComplianceLinkByID(int TopCompID)
    //    {
    //        TopicComplianceLink TopicComplianceLink = null;
    //        string key = "TopicComplianceLinks_TopicComplianceLink_" + TopCompID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicComplianceLink = (TopicComplianceLink)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicComplianceLink = GetTopicComplianceLinkFromTopicComplianceLinkInfo(SiteProvider.PR.GetTopicComplianceLinkByID(TopCompID));
    //            BasePR.CacheData(key, TopicComplianceLink);
    //        }
    //        return TopicComplianceLink;
    //    }

    //    /// <summary>
    //    /// Updates an existing TopicComplianceLink
    //    /// </summary>
    //    public static bool UpdateTopicComplianceLink(int TopCompID, int TopicID, int FacID, string ComplFile)
    //    {
    //        ComplFile = BizObject.ConvertNullToEmptyString(ComplFile);


    //        TopicComplianceLinkInfo record = new TopicComplianceLinkInfo(TopCompID, TopicID, FacID, ComplFile);
    //        bool ret = SiteProvider.PR.UpdateTopicComplianceLink(record);

    //        BizObject.PurgeCacheItems("TopicComplianceLinks_TopicComplianceLink_" + TopCompID.ToString());
    //        BizObject.PurgeCacheItems("TopicComplianceLinks_TopicComplianceLinks");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new TopicComplianceLink
    //    /// </summary>
    //    public static int InsertTopicComplianceLink(int TopicID, int FacID, string ComplFile)
    //    {
    //        ComplFile = BizObject.ConvertNullToEmptyString(ComplFile);


    //        TopicComplianceLinkInfo record = new TopicComplianceLinkInfo(0, TopicID, FacID, ComplFile);
    //        int ret = SiteProvider.PR.InsertTopicComplianceLink(record);

    //        BizObject.PurgeCacheItems("TopicComplianceLinks_TopicComplianceLink");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing TopicComplianceLink, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteTopicComplianceLink(int TopCompID)
    //    {
    //        bool IsOKToDelete = OKToDelete(TopCompID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteTopicComplianceLink(TopCompID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing TopicComplianceLink - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteTopicComplianceLink(int TopCompID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteTopicComplianceLink(TopCompID);
    //        //         new RecordDeletedEvent("TopicComplianceLink", TopCompID, null).Raise();
    //        BizObject.PurgeCacheItems("TopicComplianceLinks_TopicComplianceLink");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a TopicComplianceLink can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int TopCompID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a TopicComplianceLink object filled with the data taken from the input TopicComplianceLinkInfo
    //    /// </summary>
    //    private static TopicComplianceLink GetTopicComplianceLinkFromTopicComplianceLinkInfo(TopicComplianceLinkInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new TopicComplianceLink(record.TopCompID, record.TopicID, record.FacID, record.ComplFile);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of TopicComplianceLink objects filled with the data taken from the input list of TopicComplianceLinkInfo
    //    /// </summary>
    //    private static List<TopicComplianceLink> GetTopicComplianceLinkListFromTopicComplianceLinkInfoList(List<TopicComplianceLinkInfo> recordset)
    //    {
    //        List<TopicComplianceLink> TopicComplianceLinks = new List<TopicComplianceLink>();
    //        foreach (TopicComplianceLinkInfo record in recordset)
    //            TopicComplianceLinks.Add(GetTopicComplianceLinkFromTopicComplianceLinkInfo(record));
    //        return TopicComplianceLinks;
    //    }



    //}
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// TopicResourceLink business object class
    /// </summary>
    //public class TopicResourceLink : BasePR
    //{
    //    private int _TopicResID = 0;
    //    public int TopicResID
    //    {
    //        get { return _TopicResID; }
    //        protected set { _TopicResID = value; }
    //    }

    //    private int _TopicID = 0;
    //    public int TopicID
    //    {
    //        get { return _TopicID; }
    //        set { _TopicID = value; }
    //    }

    //    private int _ResourceID = 0;
    //    public int ResourceID
    //    {
    //        get { return _ResourceID; }
    //        set { _ResourceID = value; }
    //    }

    //    private string _ResType = "";
    //    public string ResType
    //    {
    //        get { return _ResType; }
    //        set { _ResType = value; }
    //    }



    //    public TopicResourceLink(int TopicResID, int TopicID, int ResourceID, string ResType)
    //  {
    //        this.TopicResID = TopicResID;
    //        this.TopicID = TopicID;
    //        this.ResourceID = ResourceID;
    //        this.ResType = ResType;
    //    }

    //    public bool Delete()
    //    {
    //        bool success = TopicResourceLink.DeleteTopicResourceLink(this.TopicResID);
    //        if (success)
    //            this.TopicResID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return TopicResourceLink.UpdateTopicResourceLink(this.TopicResID, this.TopicID, this.ResourceID, this.ResType);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all TopicResourceLinks
    //    /// </summary>
    //    public static List<TopicResourceLink> GetTopicResourceLinks(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<TopicResourceLink> TopicResourceLinks = null;
    //        string key = "TopicResourceLinks_TopicResourceLinks_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicResourceLinks = (List<TopicResourceLink>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicResourceLinkInfo> recordset = SiteProvider.PR.GetTopicResourceLinks(cSortExpression);
    //            TopicResourceLinks = GetTopicResourceLinkListFromTopicResourceLinkInfoList(recordset);
    //            BasePR.CacheData(key, TopicResourceLinks);
    //        }
    //        return TopicResourceLinks;
    //    }


    //    /// <summary>
    //    /// Returns the number of total TopicResourceLinks
    //    /// </summary>
    //    public static int GetTopicResourceLinkCount()
    //    {
    //        int TopicResourceLinkCount = 0;
    //        string key = "TopicResourceLinks_TopicResourceLinkCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicResourceLinkCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicResourceLinkCount = SiteProvider.PR.GetTopicResourceLinkCount();
    //            BasePR.CacheData(key, TopicResourceLinkCount);
    //        }
    //        return TopicResourceLinkCount;
    //    }

    //    /// <summary>
    //    /// Returns the number of total TopicResourceLinks for a TopicID
    //    /// </summary>
    //    public static int GetTopicResourceLinkCountByTopicID(int TopicID)
    //    {
    //        int TopicResourceLinkCount = 0;
    //        string key = "TopicResourceLinks_TopicResourceLinkCountByTopicID_" + TopicID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicResourceLinkCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicResourceLinkCount = SiteProvider.PR.GetTopicResourceLinkCountByTopicID(TopicID);
    //            BasePR.CacheData(key, TopicResourceLinkCount);
    //        }
    //        return TopicResourceLinkCount;
    //    }

    //    /// <summary>
    //    /// Returns a TopicResourceLink object with the specified ID
    //    /// </summary>
    //    public static TopicResourceLink GetTopicResourceLinkByID(int TopicResID)
    //    {
    //        TopicResourceLink TopicResourceLink = null;
    //        string key = "TopicResourceLinks_TopicResourceLink_" + TopicResID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicResourceLink = (TopicResourceLink)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicResourceLink = GetTopicResourceLinkFromTopicResourceLinkInfo(SiteProvider.PR.GetTopicResourceLinkByID(TopicResID));
    //            BasePR.CacheData(key, TopicResourceLink);
    //        }
    //        return TopicResourceLink;
    //    }

    //    /// <summary>
    //    /// Updates an existing TopicResourceLink
    //    /// </summary>
    //    public static bool UpdateTopicResourceLink(int TopicResID, int TopicID, int ResourceID, string ResType)
    //    {


    //        TopicResourceLinkInfo record = new TopicResourceLinkInfo(TopicResID, TopicID, ResourceID, ResType);
    //        bool ret = SiteProvider.PR.UpdateTopicResourceLink(record);

    //        BizObject.PurgeCacheItems("TopicResourceLinks_TopicResourceLink_" + TopicResID.ToString());
    //        BizObject.PurgeCacheItems("TopicResourceLinks_TopicResourceLinks");
    //        return ret;
    //    }

    //    public static bool UpdateResourcePersonAssignments(int TopicID, string ResourceIDAssignments, string ResourceType)
    //    {
    //        bool ret = SiteProvider.PR.UpdateResourcePersonAssignments(TopicID, ResourceIDAssignments, ResourceType);
    //        // TODO: release cache?
    //        return ret;
    //    }


    //    /// <summary>
    //    /// Creates a new TopicResourceLink
    //    /// </summary>
    //    public static int InsertTopicResourceLink(int TopicID, int ResourceID, string ResType)
    //    {


    //        TopicResourceLinkInfo record = new TopicResourceLinkInfo(0, TopicID, ResourceID, ResType);
    //        int ret = SiteProvider.PR.InsertTopicResourceLink(record);

    //        BizObject.PurgeCacheItems("TopicResourceLinks_TopicResourceLink");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing TopicResourceLink, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteTopicResourceLink(int TopicResID)
    //    {
    //        bool IsOKToDelete = OKToDelete(TopicResID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteTopicResourceLink(TopicResID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing TopicResourceLink - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteTopicResourceLink(int TopicResID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteTopicResourceLink(TopicResID);
    //        //         new RecordDeletedEvent("TopicResourceLink", TopicResID, null).Raise();
    //        BizObject.PurgeCacheItems("TopicResourceLinks_TopicResourceLink");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a TopicResourceLink can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int TopicResID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a TopicResourceLink object filled with the data taken from the input TopicResourceLinkInfo
    //    /// </summary>
    //    private static TopicResourceLink GetTopicResourceLinkFromTopicResourceLinkInfo(TopicResourceLinkInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new TopicResourceLink(record.TopicResID, record.TopicID, record.ResourceID, record.ResType);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of TopicResourceLink objects filled with the data taken from the input list of TopicResourceLinkInfo
    //    /// </summary>
    //    private static List<TopicResourceLink> GetTopicResourceLinkListFromTopicResourceLinkInfoList(List<TopicResourceLinkInfo> recordset)
    //    {
    //        List<TopicResourceLink> TopicResourceLinks = new List<TopicResourceLink>();
    //        foreach (TopicResourceLinkInfo record in recordset)
    //            TopicResourceLinks.Add(GetTopicResourceLinkFromTopicResourceLinkInfo(record));
    //        return TopicResourceLinks;
    //    }



    //}

    
    
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// UserInterestLink business object class
    /// </summary>
    //public class UserInterestLink : BasePR
    //{
    //    private int _UserIntID = 0;
    //    public int UserIntID
    //    {
    //        get { return _UserIntID; }
    //        protected set { _UserIntID = value; }
    //    }

    //    private int _UserID = 0;
    //    public int UserID
    //    {
    //        get { return _UserID; }
    //        set { _UserID = value; }
    //    }

    //    private int _InterestID = 0;
    //    public int InterestID
    //    {
    //        get { return _InterestID; }
    //        set { _InterestID = value; }
    //    }


    //   public UserInterestLink(int UserIntID, int UserID, int InterestID)
    //  {
    //        this.UserIntID = UserIntID;
    //        this.UserID = UserID;
    //        this.InterestID = InterestID;
    //  }

    //    public bool Delete()
    //    {
    //        bool success = UserInterestLink.DeleteUserInterestLink(this.UserIntID);
    //        if (success)
    //            this.UserIntID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return UserInterestLink.UpdateUserInterestLink(this.UserIntID, this.UserID, this.InterestID);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all UserInterestLinks
    //    /// </summary>
    //    public static List<UserInterestLink> GetUserInterestLinks(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<UserInterestLink> UserInterestLinks = null;
    //        string key = "UserInterestLinks_UserInterestLinks_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserInterestLinks = (List<UserInterestLink>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<UserInterestLinkInfo> recordset = SiteProvider.PR.GetUserInterestLinks(cSortExpression);
    //            UserInterestLinks = GetUserInterestLinkListFromUserInterestLinkInfoList(recordset);
    //            BasePR.CacheData(key, UserInterestLinks);
    //        }
    //        return UserInterestLinks;
    //    }


    //    /// <summary>
    //    /// Returns the number of total UserInterestLinks
    //    /// </summary>
    //    public static int GetUserInterestLinkCount()
    //    {
    //        int UserInterestLinkCount = 0;
    //        string key = "UserInterestLinks_UserInterestLinkCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserInterestLinkCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UserInterestLinkCount = SiteProvider.PR.GetUserInterestLinkCount();
    //            BasePR.CacheData(key, UserInterestLinkCount);
    //        }
    //        return UserInterestLinkCount;
    //    }

    //    /// <summary>
    //    /// Returns a UserInterestLink object with the specified ID
    //    /// </summary>
    //    public static UserInterestLink GetUserInterestLinkByID(int UserIntID)
    //    {
    //        UserInterestLink UserInterestLink = null;
    //        string key = "UserInterestLinks_UserInterestLink_" + UserIntID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserInterestLink = (UserInterestLink)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UserInterestLink = GetUserInterestLinkFromUserInterestLinkInfo(SiteProvider.PR.GetUserInterestLinkByID(UserIntID));
    //            BasePR.CacheData(key, UserInterestLink);
    //        }
    //        return UserInterestLink;
    //    }

    //    public static bool UpdateUserInterestAssignments(int UserID, string InterestIDAssignments) 
    //    {
    //        bool ret = SiteProvider.PR.UpdateUserInterestAssignments(UserID, InterestIDAssignments);
    //        // TODO: release cache?
    //        return ret;
    //    }
        
        
        
    //    /// <summary>
    //    /// Updates an existing UserInterestLink
    //    /// </summary>
    //    public static bool UpdateUserInterestLink(int UserIntID, int UserID, int InterestID)
    //    {


    //        UserInterestLinkInfo record = new UserInterestLinkInfo(UserIntID, UserID, InterestID);
    //        bool ret = SiteProvider.PR.UpdateUserInterestLink(record);

    //        BizObject.PurgeCacheItems("UserInterestLinks_UserInterestLink_" + UserIntID.ToString());
    //        BizObject.PurgeCacheItems("UserInterestLinks_UserInterestLinks");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new UserInterestLink
    //    /// </summary>
    //    public static int InsertUserInterestLink(int UserID, int InterestID)
    //    {


    //        UserInterestLinkInfo record = new UserInterestLinkInfo(0, UserID, InterestID);
    //        int ret = SiteProvider.PR.InsertUserInterestLink(record);

    //        BizObject.PurgeCacheItems("UserInterestLinks_UserInterestLink");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing UserInterestLink, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteUserInterestLink(int UserIntID)
    //    {
    //        bool IsOKToDelete = OKToDelete(UserIntID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteUserInterestLink(UserIntID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing UserInterestLink - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteUserInterestLink(int UserIntID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteUserInterestLink(UserIntID);
    //        //         new RecordDeletedEvent("UserInterestLink", UserIntID, null).Raise();
    //        BizObject.PurgeCacheItems("UserInterestLinks_UserInterestLink");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a UserInterestLink can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int UserIntID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a UserInterestLink object filled with the data taken from the input UserInterestLinkInfo
    //    /// </summary>
    //    private static UserInterestLink GetUserInterestLinkFromUserInterestLinkInfo(UserInterestLinkInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new UserInterestLink(record.UserIntID, record.UserID, record.InterestID);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of UserInterestLink objects filled with the data taken from the input list of UserInterestLinkInfo
    //    /// </summary>
    //    private static List<UserInterestLink> GetUserInterestLinkListFromUserInterestLinkInfoList(List<UserInterestLinkInfo> recordset)
    //    {
    //        List<UserInterestLink> UserInterestLinks = new List<UserInterestLink>();
    //        foreach (UserInterestLinkInfo record in recordset)
    //            UserInterestLinks.Add(GetUserInterestLinkFromUserInterestLinkInfo(record));
    //        return UserInterestLinks;
    //    }



    //}

    
    ////////////////////////////////////////////////////////////
    /// <summary>
    /// UserLicense business object class
    /// </summary>
    //public class UserLicense : BasePR
    //{
    //    private int _LicenseID = 0;
    //    public int LicenseID
    //    {
    //        get { return _LicenseID; }
    //        protected set { _LicenseID = value; }
    //    }

    //    private int _UserID = 0;
    //    public int UserID
    //    {
    //        get { return _UserID; }
    //        set { _UserID = value; }
    //    }

    //    private int _License_Type_ID = 0;
    //    public int License_Type_ID
    //    {
    //        get { return _License_Type_ID; }
    //        set { _License_Type_ID = value; }
    //    }

    //    private string _State = "";
    //    public string State
    //    {
    //        get { return _State; }
    //        set { _State = value; }
    //    }

    //    private string _License_Number = "";
    //    public string License_Number
    //    {
    //        get { return _License_Number; }
    //        set { _License_Number = value; }
    //    }

    //    private string _Issue_Date = "";
    //    public string Issue_Date
    //    {
    //        get { return _Issue_Date; }
    //        set { _Issue_Date = value; }
    //    }

    //    private string _Expire_Date = "";
    //    public string Expire_Date
    //    {
    //        get { return _Expire_Date; }
    //        set { _Expire_Date = value; }
    //    }

    //    private bool _Report_To_State = false;
    //    public bool Report_To_State
    //    {
    //        get { return _Report_To_State; }
    //        set { _Report_To_State = value; }
    //    }


    //    public UserLicense(int LicenseID, int UserID, int License_Type_ID, string State,
    //       string License_Number, string Issue_Date, string Expire_Date, bool Report_To_State)
    //  {
    //        this.LicenseID = LicenseID;
    //        this.UserID = UserID;
    //        this.License_Type_ID = License_Type_ID;
    //        this.State = State;
    //        this.License_Number = License_Number;
    //        this.Issue_Date = Issue_Date;
    //        this.Expire_Date = Expire_Date;
    //        this.Report_To_State = Report_To_State;
    //   }
   
    
    
    //    public bool Delete()
    //    {
    //        bool success = UserLicense.DeleteUserLicense(this.LicenseID);
    //        if (success)
    //            this.LicenseID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return UserLicense.UpdateUserLicense(this.LicenseID, this.UserID, this.License_Type_ID, this.State,
    //       this.License_Number, this.Issue_Date, this.Expire_Date, this.Report_To_State);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all UserLicenses
    //    /// </summary>
    //    public static List<UserLicense> GetUserLicenses(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<UserLicense> UserLicenses = null;
    //        string key = "UserLicenses_UserLicenses_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserLicenses = (List<UserLicense>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<UserLicenseInfo> recordset = SiteProvider.PR.GetUserLicenses(cSortExpression);
    //            UserLicenses = GetUserLicenseListFromUserLicenseInfoList(recordset);
    //            BasePR.CacheData(key, UserLicenses);
    //        }
    //        return UserLicenses;
    //    }

    //    /// <summary>
    //    /// Returns a collection with all UserLicenses for a UserID
    //    /// </summary>
    //    public static List<UserLicense> GetUserLicensesByUserID(int UserID, string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<UserLicense> UserLicenses = null;
    //        string key = "UserLicenses_UserLicensesByUserID_" + UserID.ToString() + "_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserLicenses = (List<UserLicense>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<UserLicenseInfo> recordset = SiteProvider.PR.GetUserLicensesByUserID(UserID, cSortExpression);
    //            UserLicenses = GetUserLicenseListFromUserLicenseInfoList(recordset);
    //            BasePR.CacheData(key, UserLicenses);
    //        }
    //        return UserLicenses;
    //    }


    //    /// <summary>
    //    /// Returns a UserLicense object with the specified ID
    //    /// </summary>
    //    public static UserLicense  GetUserLicenseByUserID(int UserID)
    //    {
    //        UserLicense UserLicense = null;
    //        string key = "UserLicenses_UserLicense_" + UserID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserLicense = (UserLicense)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UserLicense = GetUserLicenseFromUserLicenseInfo(SiteProvider.PR.GetUserLicenseByUserID(UserID));
    //            BasePR.CacheData(key, UserLicense);
    //        }
    //        return UserLicense;
    //    }



    //    /// <summary>
    //    /// Returns the number of total UserLicenses
    //    /// </summary>
    //    public static int GetUserLicenseCount()
    //    {
    //        int UserLicenseCount = 0;
    //        string key = "UserLicenses_UserLicenseCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserLicenseCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UserLicenseCount = SiteProvider.PR.GetUserLicenseCount();
    //            BasePR.CacheData(key, UserLicenseCount);
    //        }
    //        return UserLicenseCount;
    //    }

    //    /// <summary>
    //    /// Returns a UserLicense object with the specified ID
    //    /// </summary>
    //    public static UserLicense GetUserLicenseByID(int LicenseID)
    //    {
    //        UserLicense UserLicense = null;
    //        string key = "UserLicenses_UserLicense_" + LicenseID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UserLicense = (UserLicense)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UserLicense = GetUserLicenseFromUserLicenseInfo(SiteProvider.PR.GetUserLicenseByID(LicenseID));
    //            BasePR.CacheData(key, UserLicense);
    //        }
    //        return UserLicense;
    //    }

    //    /// <summary>
    //    /// Updates an existing UserLicense
    //    /// </summary>
    //    public static bool UpdateUserLicense(int LicenseID, int UserID, int License_Type_ID, string State,
    //       string License_Number, string Issue_Date, string Expire_Date, bool Report_To_State)
    //    {
    //        bool ret = false;
    //          if (State == "FL")
    //          {
    //              int pos1 = License_Number.IndexOf(" ", 0);

    //              int pos = License_Number.IndexOf("-", 0);
    //              if (pos > 0 || pos1 > 0)
    //              {
    //                  Exception ex = new Exception("Please correct the License Number");
    //                  throw ex;
    //               }
    //              else
    //              {
    //                  UserLicenseInfo record = new UserLicenseInfo(LicenseID, UserID, License_Type_ID, State,
    //                 License_Number, Issue_Date, Expire_Date, Report_To_State);
    //                  ret = SiteProvider.PR.UpdateUserLicense(record);
    //                  BizObject.PurgeCacheItems("UserLicenses_UserLicense_" + LicenseID.ToString());
    //                  BizObject.PurgeCacheItems("UserLicenses_UserLicenses");
    //              }
    //          }
    //          else
    //          {
    //              UserLicenseInfo record = new UserLicenseInfo(LicenseID, UserID, License_Type_ID, State,
    //              License_Number, Issue_Date, Expire_Date, Report_To_State);
    //              ret = SiteProvider.PR.UpdateUserLicense(record);
    //              BizObject.PurgeCacheItems("UserLicenses_UserLicense_" + LicenseID.ToString());
    //              BizObject.PurgeCacheItems("UserLicenses_UserLicenses");
    //          }
           
    //        return ret;

    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new UserLicense
    //    /// </summary>
    //    public static int InsertUserLicense(int UserID, int License_Type_ID, string State,
    //       string License_Number, string Issue_Date, string Expire_Date, bool Report_To_State)
    //    {
    //        int ret =0;
    //        if (State == "FL")
    //        {
    //            int pos1 = License_Number.IndexOf(" ", 0);

    //            int pos = License_Number.IndexOf("-",0);
    //            if (pos > 0 || pos1 > 0)
    //            {
    //                ret = -1;
    //                Exception ex = new Exception("Please correct the License Number");
    //                throw ex;
    //                BizObject.PurgeCacheItems("UserLicenses_UserLicense");
                    
    //            }
    //            else
    //            {
    //                UserLicenseInfo record = new UserLicenseInfo(0, UserID, License_Type_ID, State,
    //               License_Number, Issue_Date, Expire_Date, Report_To_State);
    //                ret = SiteProvider.PR.InsertUserLicense(record);
    //                BizObject.PurgeCacheItems("UserLicenses_UserLicense");
         
    //            }
    //        }
    //        else
    //        {

    //            UserLicenseInfo record = new UserLicenseInfo(0, UserID, License_Type_ID, State,
    //           License_Number, Issue_Date, Expire_Date, Report_To_State);
    //            ret = SiteProvider.PR.InsertUserLicense(record);
    //            BizObject.PurgeCacheItems("UserLicenses_UserLicense");
         
    //        }
           
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing UserLicense, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteUserLicense(int LicenseID)
    //    {
    //        bool IsOKToDelete = OKToDelete(LicenseID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteUserLicense(LicenseID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing UserLicense - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteUserLicense(int LicenseID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteUserLicense(LicenseID);
    //        //         new RecordDeletedEvent("UserLicense", UserIntID, null).Raise();
    //        BizObject.PurgeCacheItems("UserLicenses_UserLicense");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a UserLicense can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int LicenseID)
    //    {
    //        return true;
    //    }



    //    /// <summary>
    //    /// Returns a UserLicense object filled with the data taken from the input UserLicenseInfo
    //    /// </summary>
    //    private static UserLicense GetUserLicenseFromUserLicenseInfo(UserLicenseInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new UserLicense(record.LicenseID, record.UserID, record.License_Type_ID, record.State,
    //       record.License_Number, record.Issue_Date, record.Expire_Date, record.Report_To_State);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of UserLicense objects filled with the data taken from the input list of UserInterestLinkInfo
    //    /// </summary>
    //    private static List<UserLicense> GetUserLicenseListFromUserLicenseInfoList(List<UserLicenseInfo> recordset)
    //    {
    //        List<UserLicense> UserLicenses = new List<UserLicense>();
    //        foreach (UserLicenseInfo record in recordset)
    //            UserLicenses.Add(GetUserLicenseFromUserLicenseInfo(record));
    //        return UserLicenses;
    //    }



    //}
    
    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// UsersInRoles business object class
    ///// </summary>
    //public class UsersInRoles : BasePR
    //{
    //    private int _UserRoleID = 0;
    //    public int UserRoleID
    //    {
    //        get { return _UserRoleID; }
    //        protected set { _UserRoleID = value; }
    //    }

    //    private string _RoleName = "";
    //    public string RoleName
    //    {
    //        get { return _RoleName; }
    //        set { _RoleName = value; }
    //    }

    //    private string _UserName = "";
    //    public string UserName
    //    {
    //        get { return _UserName.ToLower(); }
    //        set { _UserName = value.ToLower(); }
    //    }


    //   public UsersInRoles(int UserRoleID, string RoleName, string UserName)
    //  {
    //        this.UserRoleID = UserRoleID;
    //        this.RoleName = RoleName;
    //        this.UserName = UserName.ToLower();
    //  }

    //    public bool Delete()
    //    {
    //        bool success = UsersInRoles.DeleteUsersInRoles(this.UserRoleID);
    //        if (success)
    //            this.UserRoleID = 0;
    //        return success;
    //    }

    //    public bool Update()
    //    {
    //        return UsersInRoles.UpdateUsersInRoles(this.UserRoleID, this.RoleName, this.UserName);
    //    }

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all UsersInRoles
    //    /// </summary>
    //    public static List<UsersInRoles> GetUsersInRoles(string cSortExpression)
    //    {
    //        if (cSortExpression == null)
    //            cSortExpression = "";

    //        // provide default sort
    //        if (cSortExpression.Length == 0)
    //            cSortExpression = "";

    //        List<UsersInRoles> UsersInRoles = null;
    //        string key = "UsersInRoles_UsersInRoles_" + cSortExpression.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UsersInRoles = (List<UsersInRoles>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<UsersInRolesInfo> recordset = SiteProvider.PR.GetUsersInRoles(cSortExpression);
    //            UsersInRoles = GetUsersInRolesListFromUsersInRolesInfoList(recordset);
    //            BasePR.CacheData(key, UsersInRoles);
    //        }
    //        return UsersInRoles;
    //    }


    //    /// <summary>
    //    /// Returns the number of total UsersInRoles
    //    /// </summary>
    //    public static int GetUsersInRolesCount()
    //    {
    //        int UsersInRolesCount = 0;
    //        string key = "UsersInRoles_UsersInRolesCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UsersInRolesCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UsersInRolesCount = SiteProvider.PR.GetUsersInRolesCount();
    //            BasePR.CacheData(key, UsersInRolesCount);
    //        }
    //        return UsersInRolesCount;
    //    }

    //    /// <summary>
    //    /// Returns a UsersInRoles object with the specified ID
    //    /// </summary>
    //    public static UsersInRoles GetUsersInRolesByID(int UserRoleID)
    //    {
    //        UsersInRoles UsersInRoles = null;
    //        string key = "UsersInRoles_UsersInRoles_" + UserRoleID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            UsersInRoles = (UsersInRoles)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            UsersInRoles = GetUsersInRolesFromUsersInRolesInfo(SiteProvider.PR.GetUsersInRolesByID(UserRoleID));
    //            BasePR.CacheData(key, UsersInRoles);
    //        }
    //        return UsersInRoles;
    //    }

    //    /// <summary>
    //    /// Updates an existing UsersInRoles
    //    /// </summary>
    //    public static bool UpdateUsersInRoles(int UserRoleID, string RoleName, string UserName)
    //    {
    //        RoleName = BizObject.ConvertNullToEmptyString(RoleName);
    //        UserName = BizObject.ConvertNullToEmptyString(UserName);


    //        UsersInRolesInfo record = new UsersInRolesInfo(UserRoleID, RoleName, UserName);
    //        bool ret = SiteProvider.PR.UpdateUsersInRoles(record);

    //        BizObject.PurgeCacheItems("UsersInRoles_UsersInRoles_" + UserRoleID.ToString());
    //        BizObject.PurgeCacheItems("UsersInRoles_UsersInRoles");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Creates a new UsersInRoles
    //    /// </summary>
    //    public static int InsertUsersInRoles(string RoleName, string UserName)
    //    {
    //        RoleName = BizObject.ConvertNullToEmptyString(RoleName);
    //        UserName = BizObject.ConvertNullToEmptyString(UserName);


    //        UsersInRolesInfo record = new UsersInRolesInfo(0, RoleName, UserName);
    //        int ret = SiteProvider.PR.InsertUsersInRoles(record);

    //        BizObject.PurgeCacheItems("UsersInRoles_UsersInRoles");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Deletes an existing UsersInRoles, but first checks if OK to delete
    //    /// </summary>
    //    public static bool DeleteUsersInRoles(int UserRoleID)
    //    {
    //        bool IsOKToDelete = OKToDelete(UserRoleID);
    //        if (IsOKToDelete)
    //        {
    //            return (bool)DeleteUsersInRoles(UserRoleID, true);
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    /// <summary>
    //    /// Deletes an existing UsersInRoles - second param forces skip of OKToDelete
    //    /// (assuming that the calling program has already called that if it's
    //    /// passing the second param as true)
    //    /// </summary>
    //    public static bool DeleteUsersInRoles(int UserRoleID, bool SkipOKToDelete)
    //    {
    //        if (!SkipOKToDelete)
    //            return false;

    //        bool ret = SiteProvider.PR.DeleteUsersInRoles(UserRoleID);
    //        //         new RecordDeletedEvent("UsersInRoles", UserRoleID, null).Raise();
    //        BizObject.PurgeCacheItems("UsersInRoles_UsersInRoles");
    //        return ret;
    //    }



    //    /// <summary>
    //    /// Checks to see if a UsersInRoles can be deleted safely
    //    /// (This default method just returns true. Certain entities
    //    /// might set datadict_tables.lNoOKDel to eliminate this
    //    /// default method and provide a custom version in
    //    /// datadict_tables.boExtra)
    //    /// </summary>
    //    public static bool OKToDelete(int UserRoleID)
    //    {
    //        return true;
    //    }

    //    /// Deletes all existing UsersInRoles for username
    //    /// </summary>
    //    public static bool DeleteUsersInRolesByUserName(string UserName)
    //    {
    //        bool ret = SiteProvider.PR.DeleteUsersInRolesByUserName(UserName);
    //        BizObject.PurgeCacheItems("UsersInRoles_UsersInRoles");
    //        return ret;
    //    }


    //    /// <summary>
    //    /// Returns a UsersInRoles object filled with the data taken from the input UsersInRolesInfo
    //    /// </summary>
    //    private static UsersInRoles GetUsersInRolesFromUsersInRolesInfo(UsersInRolesInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new UsersInRoles(record.UserRoleID, record.RoleName, record.UserName.ToLower());
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of UsersInRoles objects filled with the data taken from the input list of UsersInRolesInfo
    //    /// </summary>
    //    private static List<UsersInRoles> GetUsersInRolesListFromUsersInRolesInfoList(List<UsersInRolesInfo> recordset)
    //    {
    //        List<UsersInRoles> UsersInRoles = new List<UsersInRoles>();
    //        foreach (UsersInRolesInfo record in recordset)
    //            UsersInRoles.Add(GetUsersInRolesFromUsersInRolesInfo(record));
    //        return UsersInRoles;
    //    }



    //}
////////////////////////////////////////////////////
// ADDED AT THE BOTTOM FROM AddToBo.txt ////////////
////////////////////////////////////////////////////

//    /////////////////////////////////////////////////////////////
//    /// <summary>
//    /// TopicQuestion business object class
//    /// </summary>
//    public class TopicQuestion : BasePR
//    {
//        private int _TopicID = 0;
//        public int TopicID
//        {
//            get { return _TopicID; }
//            protected set { _TopicID = value; }
//        }

//        private int _QuestionNum = 0;
//        public int QuestionNum
//        {
//            get { return _QuestionNum; }
//            set { _QuestionNum = value; }
//        }

//        private string _Question = "";
//        public string Question
//        {
//            get { return _Question; }
//            set { _Question = value; }
//        }

//        private bool _A_Correct = false;
//        public bool A_Correct
//        {
//            get { return _A_Correct; }
//            set { _A_Correct = value; }
//        }

//        private string _Answer_A = "";
//        public string Answer_A
//        {
//            get { return _Answer_A; }
//            set { _Answer_A = value; }
//        }

//        private bool _B_Correct = false;
//        public bool B_Correct
//        {
//            get { return _B_Correct; }
//            set { _B_Correct = value; }
//        }

//        private string _Answer_B = "";
//        public string Answer_B
//        {
//            get { return _Answer_B; }
//            set { _Answer_B = value; }
//        }

//        private bool _C_Correct = false;
//        public bool C_Correct
//        {
//            get { return _C_Correct; }
//            set { _C_Correct = value; }
//        }

//        private string _Answer_C = "";
//        public string Answer_C
//        {
//            get { return _Answer_C; }
//            set { _Answer_C = value; }
//        }

//        private bool _D_Correct = false;
//        public bool D_Correct
//        {
//            get { return _D_Correct; }
//            set { _D_Correct = value; }
//        }

//        private string _Answer_D = "";
//        public string Answer_D
//        {
//            get { return _Answer_D; }
//            set { _Answer_D = value; }
//        }

//        private string _Discussion = "";
//        public string Discussion
//        {
//            get { return _Discussion; }
//            set { _Discussion = value; }
//        }

//        public TopicQuestion(int TopicID,
//            int QuestionNum, string Question,
//            bool A_Correct, string Answer_A,
//            bool B_Correct, string Answer_B,
//            bool C_Correct, string Answer_C,
//            bool D_Correct, string Answer_D,
//            string Discussion)
//        {
//            this.TopicID = TopicID;
//            this.QuestionNum = QuestionNum;
//            this.Question = Question;
//            this.A_Correct = A_Correct;
//            this.Answer_A = Answer_A;
//            this.B_Correct = B_Correct;
//            this.Answer_B = Answer_B;
//            this.C_Correct = C_Correct;
//            this.Answer_C = Answer_C;
//            this.D_Correct = D_Correct;
//            this.Answer_D = Answer_D;
//            this.Discussion = Discussion;
//        }

//        //public bool Delete()
//        //{
//        //    bool success = TopicDetail.DeleteTopicDetail(this.TopicID);
//        //    if (success)
//        //        this.TopicID = 0;
//        //    return success;
//        //}

//        //public bool Update()
//        //{
//        //    return Topic.UpdateTopic(this.TopicID, this.Type, this.Data,
//        //   this.CorrectAns, this.Order, this.Discussion);
//        //}

//        /***********************************
//        * Static methods
//        ************************************/

//        /// <summary>
//        /// Returns a collection with all Topic Questions
//        /// </summary>
//        public static List<TopicQuestion> GetTopicQuestionsByTopicID(int TopicID)
//        {
////            List<TopicQuestion> TopicQuestions = null;
//            List<TopicQuestion> TopicQuestions = new List<TopicQuestion>();
//            // set up the TopicQuestions list containing 6
//            // new default TopicQuestion objects
//            TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, 1));
//            TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, 2));
//            TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 3));
//            TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 4));
//            TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 5));
//            TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 6));

//            // get list of TopicDetail records matching the TopicID
//            List<TopicDetail> TopicDetails = TopicDetail.GetTopicDetailsByTopicID(TopicID);

//            // check the count to be sure it's 34 records
//            if (TopicDetails.Count == 34)
//            {
//                // 34 recs found, so put the info into the 6
//                // TopicQuestion objects in the TopicQuestions list
//                int iQuestionPointer = -1;
//                int iOrder = 0;
//                foreach (TopicDetail record in TopicDetails)
//                {
//                    iOrder += 1;

//                    // if starting new question, increment iQuestionPointer
//                    if (iOrder == 1 || iOrder == 8 || iOrder == 15 ||
//                        iOrder == 20 || iOrder == 25 || iOrder == 30)
//                    {
//                        iQuestionPointer += 1;
//                        // Question text
//                        TopicQuestions[iQuestionPointer].Question = record.Data;
//                    }

//                    if (iOrder == 2 || iOrder == 9 || iOrder == 16 ||
//                        iOrder == 21 || iOrder == 26 || iOrder == 31)
//                    {
//                        // Answer_A text
//                        TopicQuestions[iQuestionPointer].Answer_A = record.Data;
//                    }

//                    if (iOrder == 3 || iOrder == 10 || iOrder == 17 ||
//                        iOrder == 22 || iOrder == 27 || iOrder == 32)
//                    {
//                        // Answer_B text
//                        TopicQuestions[iQuestionPointer].Answer_B = record.Data;
//                    }

//                    if (iOrder == 4 || iOrder == 11)
//                    {
//                        // Answer_C text
//                        TopicQuestions[iQuestionPointer].Answer_C = record.Data;
//                    }

//                    if (iOrder == 5 || iOrder == 12)
//                    {
//                        // Answer_D text
//                        TopicQuestions[iQuestionPointer].Answer_D = record.Data;
//                    }

//                    if (iOrder == 6 || iOrder == 13 || iOrder == 18 ||
//                        iOrder == 23 || iOrder == 28 || iOrder == 33)
//                    {
//                        // Correct Answer
//                        if (record.CorrectAns == "A")
//                            TopicQuestions[iQuestionPointer].A_Correct = true;
//                        if (record.CorrectAns == "B")
//                            TopicQuestions[iQuestionPointer].B_Correct = true;
//                        if (record.CorrectAns == "C")
//                            TopicQuestions[iQuestionPointer].C_Correct = true;
//                        if (record.CorrectAns == "D")
//                            TopicQuestions[iQuestionPointer].D_Correct = true;
//                    }

//                    if (iOrder == 7 || iOrder == 14 || iOrder == 19 ||
//                        iOrder == 24 || iOrder == 29 || iOrder == 34)
//                    {
//                        // Discussion
//                        TopicQuestions[iQuestionPointer].Discussion = record.Discussion;
//                    }
//                }
//            }

//            return TopicQuestions;
//        }

//        /// <summary>
//        /// Returns a new multiple choice TopicQuestion object
//        /// </summary>
//        private static TopicQuestion CreateNewMultipleChoiceQuestion(int TopicID, int QuestionNum)
//        {
//            TopicQuestion objTopicQuestion =
//              new TopicQuestion(TopicID,
//                QuestionNum, "",
//                false, "",
//                false, "",
//                false, "",
//                false, "",
//                "" );
//            return objTopicQuestion;
//        }

//        /// <summary>
//        /// Returns a new true/false TopicQuestion object
//        /// </summary>
//        private static TopicQuestion CreateNewTrueFalseQuestion(int TopicID, int QuestionNum)
//        {
//            TopicQuestion objTopicQuestion =
//              new TopicQuestion(TopicID,
//                QuestionNum, "",
//                false, "True",
//                false, "False",
//                false, "",
//                false, "",
//                "" );
//            return objTopicQuestion;
//        }

//        public static bool UpdateTopicQuestion(int TopicID,
//            int QuestionNum, string Question,
//            bool A_Correct, string Answer_A,
//            bool B_Correct, string Answer_B,
//            bool C_Correct, string Answer_C,
//            bool D_Correct, string Answer_D,
//            string Discussion)
//        {
//            // get the complete set of 6 questions from the database
//            List<TopicQuestion> TopicQuestions = GetTopicQuestionsByTopicID(TopicID);

//            int iQuestionPointer = QuestionNum - 1;

//            // update the record for the question number passed in
//            TopicQuestions[iQuestionPointer].Question = Question;
//            TopicQuestions[iQuestionPointer].A_Correct = A_Correct;
//            TopicQuestions[iQuestionPointer].Answer_A = Answer_A;
//            TopicQuestions[iQuestionPointer].B_Correct = B_Correct;
//            TopicQuestions[iQuestionPointer].Answer_B = Answer_B;
//            TopicQuestions[iQuestionPointer].C_Correct = C_Correct;
//            TopicQuestions[iQuestionPointer].Answer_C = Answer_C;
//            TopicQuestions[iQuestionPointer].D_Correct = D_Correct;
//            TopicQuestions[iQuestionPointer].Answer_D = Answer_D;
//            TopicQuestions[iQuestionPointer].Discussion = Discussion;

//            // insert the entire TopicQuestions list (it first deletes
//            // the existing topicdetail records)
//            bool ret = InsertTopicQuestionsFromList(TopicQuestions);

//            return ret;

//        }

//        public static bool InsertTopicQuestionsFromList(
//            List<TopicQuestion> TopicQuestions)
//        {

//            List<TopicDetailInfo> TopicDetails = new List<TopicDetailInfo>();

//            // check the count to be sure it's 6 records
//            if (TopicQuestions.Count == 6)
//            {
//                // 6 recs found, so put the info into 34
//                // TopicDetailInfo objects in the TopicDetails list
//                int iOrder = 0;
//                string cCorrectAns = "";

//                foreach (TopicQuestion record in TopicQuestions)
//                {

//                    // question
//                    iOrder += 1;
//                    TopicDetails.Add(CreateNewTopicDetailObject(
//                      record.TopicID, "Q", record.Question, "", iOrder, ""));

//                    // answer A
//                    iOrder += 1;
//                    TopicDetails.Add(CreateNewTopicDetailObject(
//                      record.TopicID, "A", record.Answer_A, "", iOrder, ""));

//                    // answer B
//                    iOrder += 1;
//                    TopicDetails.Add(CreateNewTopicDetailObject(
//                      record.TopicID, "A", record.Answer_B, "", iOrder, ""));

//                    if (record.QuestionNum == 1 || record.QuestionNum == 2)
//                    {
//                        // answer C - only for question 1 or 2
//                        iOrder += 1;
//                        TopicDetails.Add(CreateNewTopicDetailObject(
//                          record.TopicID, "A", record.Answer_C, "", iOrder, ""));

//                        // answer D - only for question 1 or 2
//                        iOrder += 1;
//                        TopicDetails.Add(CreateNewTopicDetailObject(
//                          record.TopicID, "A", record.Answer_D, "", iOrder, ""));
//                    }

//                    // correct answer
//                    cCorrectAns = "";
//                    iOrder += 1;
//                    if (record.A_Correct)
//                        cCorrectAns = "A";
//                    if (record.B_Correct)
//                        cCorrectAns = "B";
//                    if (record.C_Correct)
//                        cCorrectAns = "C";
//                    if (record.D_Correct)
//                        cCorrectAns = "D";

//                    TopicDetails.Add(CreateNewTopicDetailObject(
//                      record.TopicID, "", "", cCorrectAns, iOrder, ""));

//                    // discussion
//                    iOrder += 1;
//                    TopicDetails.Add(CreateNewTopicDetailObject(
//                      record.TopicID, "D", "", "", iOrder, record.Discussion));
//                }
//            }

//            bool ret = TopicDetail.InsertTopicDetailsFromTopicDetailList(
//              TopicDetails);
//            return ret;
//        }

//        /// <summary>
//        /// Returns a new TopicDetail object
//        /// </summary>
//        private static TopicDetailInfo CreateNewTopicDetailObject(
//          int TopicID, string Type, string Data, string CorrectAns,
//            int Order, string Discussion)
//        {
//            TopicDetailInfo objTopicDetail =
//              new TopicDetailInfo(TopicID, Type, Data,
//              CorrectAns, Order, Discussion);

//            return objTopicDetail;
//        }

//    }

    /////////////////////////////////////////////////////////////
    /// <summary>
    /// TopicQuestion business object class
//    /// </summary>
//    public class TopicQuestion : BasePR
//    {
//        private int _TopicID = 0;
//        public int TopicID
//        {
//            get { return _TopicID; }
//            protected set { _TopicID = value; }
//        }

//        private int _QuestionNum = 0;
//        public int QuestionNum
//        {
//            get { return _QuestionNum; }
//            set { _QuestionNum = value; }
//        }

//        //Bsk New
//        private int _HeaderNum = 0;
//        public int HeaderNum
//        {
//            get { return _HeaderNum; }
//            set { _HeaderNum = value; }
//        }

//        //Bsk New
//        private string _HeaderName = "";
//        public string HeaderName
//        {
//            get { return _HeaderName; }
//            set { _HeaderName = value; }
//        } 

//        private string _Question = "";
//        public string Question
//        {
//            get { return _Question; }
//            set { _Question = value; }
//        }

//        private bool _A_Correct = false;
//        public bool A_Correct
//        {
//            get { return _A_Correct; }
//            set { _A_Correct = value; }
//        }

//        private string _Answer_A = "";
//        public string Answer_A
//        {
//            get { return _Answer_A; }
//            set { _Answer_A = value; }
//        }

//        private bool _B_Correct = false;
//        public bool B_Correct
//        {
//            get { return _B_Correct; }
//            set { _B_Correct = value; }
//        }

//        private string _Answer_B = "";
//        public string Answer_B
//        {
//            get { return _Answer_B; }
//            set { _Answer_B = value; }
//        }

//        private bool _C_Correct = false;
//        public bool C_Correct
//        {
//            get { return _C_Correct; }
//            set { _C_Correct = value; }
//        }

//        private string _Answer_C = "";
//        public string Answer_C
//        {
//            get { return _Answer_C; }
//            set { _Answer_C = value; }
//        }

//        private bool _D_Correct = false;
//        public bool D_Correct
//        {
//            get { return _D_Correct; }
//            set { _D_Correct = value; }
//        }

//        private string _Answer_D = "";
//        public string Answer_D
//        {
//            get { return _Answer_D; }
//            set { _Answer_D = value; }
//        }

//        private bool _E_Correct = false;
//        public bool E_Correct
//        {
//            get { return _E_Correct; }
//            set { _E_Correct = value; }
//        }

//        private string _Answer_E = "";
//        public string Answer_E
//        {
//            get { return _Answer_E; }
//            set { _Answer_E = value; }
//        }

//        private bool _F_Correct = false;
//        public bool F_Correct
//        {
//            get { return _F_Correct; }
//            set { _F_Correct = value; }
//        }

//        private string _Answer_F = "";
//        public string Answer_F
//        {
//            get { return _Answer_F; }
//            set { _Answer_F = value; }
//        }

//        private string _Discussion = "";
//        public string Discussion
//        {
//            get { return _Discussion; }
//            set { _Discussion = value; }
//        }

//        public TopicQuestion(int TopicID,
//             int QuestionNum, int HeaderNum, string HeaderName, string Question,
//             bool A_Correct, string Answer_A,
//             bool B_Correct, string Answer_B,
//             bool C_Correct, string Answer_C,
//             bool D_Correct, string Answer_D,
//             bool E_Correct, string Answer_E,
//             bool F_Correct, string Answer_F,
//             string Discussion)
//        {
//            this.TopicID = TopicID;
//            this.QuestionNum = QuestionNum;
//            this.HeaderNum = HeaderNum;
//            this.HeaderName = HeaderName;
//            this.Question = Question;
//            this.A_Correct = A_Correct;
//            this.Answer_A = Answer_A;
//            this.B_Correct = B_Correct;
//            this.Answer_B = Answer_B;
//            this.C_Correct = C_Correct;
//            this.Answer_C = Answer_C;
//            this.D_Correct = D_Correct;
//            this.Answer_D = Answer_D;
//            this.E_Correct = E_Correct;
//            this.Answer_E = Answer_E;
//            this.F_Correct = F_Correct;
//            this.Answer_F = Answer_F;
//            this.Discussion = Discussion;
//        }

//        //public bool Delete()
//        //{
//        //    bool success = TopicDetail.DeleteTopicDetail(this.TopicID);
//        //    if (success)
//        //        this.TopicID = 0;
//        //    return success;
//        //}

//        //public bool Update()
//        //{
//        //    return Topic.UpdateTopic(this.TopicID, this.Type, this.Data,
//        //   this.CorrectAns, this.Order, this.Discussion);
//        //}

//        /***********************************
//        * Static methods
//        ************************************/

//        /// <summary>
//        /// Returns a collection with all Topic Questions
//        /// </summary>
//        public static List<TopicQuestion> GetTopicQuestionsByTopicID(int TopicID)
//        {
//            //            List<TopicQuestion> TopicQuestions = null;
           
//            // set up the TopicQuestions list containing 6
//            // new default TopicQuestion objects
//            //TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, 1));
//            //TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, 2));
//            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 3));
//            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 4));
//            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 5));
//            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 6));

//            // get list of TopicDetail records matching the TopicID
//            //List<TopicDetail> TopicDetails = TopicDetail.GetTopicDetailsByTopicID(TopicID);
//            List<TopicQuestion> TopicQuestions = new List<TopicQuestion>();
//            QTIUtils oQTIUtils = new QTIUtils();
//            TestDefinition TestDef = TestDefinition.GetTestDefinitionByTopicID(TopicID);
//            List<QTIQuestionObject> QTIQuestions = new List<QTIQuestionObject>();
//            if (TestDef == null)
//            {
//                // new item being added
//                QTIQuestionObject QTIQuestion;
//                QTIQuestion = new QTIQuestionObject();
//                //QTIQuestions.Add(QTIQuestion);
//                //QTIQuestions.Add(QTIQuestion);
//                //QTIQuestions.Add(QTIQuestion);
//                //QTIQuestions.Add(QTIQuestion);
//                //QTIQuestions.Add(QTIQuestion);
//                //QTIQuestions.Add(QTIQuestion);
//            }
//            else
//            {
//                QTIQuestions = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(TestDef.XMLTest.ToString());
//            }

//            // check the count to be sure it's 6 question objects
//            if (QTIQuestions.Count > 0)
//            {
//                // 6 objects found, so put the info into the 6
//                // TopicQuestion objects in the TopicQuestions list

//                int J = 1;
//                int K = 1;
//                string HeaderName = "";
//                for (int i = 1; i <= QTIQuestions.Count; i++)
//                {
//                    //TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, i));

//                    //Bsk New
//                    int HeaderNum = Convert.ToInt32(QTIQuestions[i - 1].cHeaderID);

//                    if (HeaderNum < 0)
//                    {
//                        HeaderNum = -J;
//                        HeaderName = "";
//                        J = J + 1;
//                    }
//                    else
//                    {
//                        HeaderNum = K;
//                        HeaderName = "Question # " + K;
//                        K = K + 1;
//                    }
//                    TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, i, HeaderNum, HeaderName));

//                }

//                int iOrder = 0;
//                foreach (QTIQuestionObject QTIQuestion in QTIQuestions)
//                {
//                    TopicQuestions[iOrder].Question = QTIQuestion.cQuestionText;
//                    TopicQuestions[iOrder].Answer_A = QTIQuestion.cAnswer_A_Text;
//                    TopicQuestions[iOrder].Answer_B = QTIQuestion.cAnswer_B_Text;
//                    TopicQuestions[iOrder].Answer_C = QTIQuestion.cAnswer_C_Text;
//                    TopicQuestions[iOrder].Answer_D = QTIQuestion.cAnswer_D_Text;
//                    TopicQuestions[iOrder].Answer_E = QTIQuestion.cAnswer_E_Text;
//                    TopicQuestions[iOrder].Answer_F = QTIQuestion.cAnswer_F_Text;

//                    // 8-13-2008 Workaround to force True and False into first two options
//                    // on questions 3, 4, 5, 6 (zero-based collection = 2,3,4,5)
//                    //if (iOrder > 1)
//                    //{
//                    //    TopicQuestions[iOrder].Answer_A = "True";
//                    //    TopicQuestions[iOrder].Answer_B = "False";
//                    //}
//                    // end workaround

//                    // Correct Answer
//                    if (QTIQuestion.cCorrectAnswer == "A")
//                        TopicQuestions[iOrder].A_Correct = true;
//                    if (QTIQuestion.cCorrectAnswer == "B")
//                        TopicQuestions[iOrder].B_Correct = true;
//                    if (QTIQuestion.cCorrectAnswer == "C")
//                        TopicQuestions[iOrder].C_Correct = true;
//                    if (QTIQuestion.cCorrectAnswer == "D")
//                        TopicQuestions[iOrder].D_Correct = true;
//                    if (QTIQuestion.cCorrectAnswer == "E")
//                        TopicQuestions[iOrder].E_Correct = true;
//                    if (QTIQuestion.cCorrectAnswer == "F")
//                        TopicQuestions[iOrder].F_Correct = true;

//                    // Discussion
//                    TopicQuestions[iOrder].Discussion = QTIQuestion.cCorrectAnswerFeedback;

//                    iOrder++;

//                }
//            }
//            return TopicQuestions;
//        }




//        /// <summary>
//        /// Returns a collection with all Topic Questions
//        /// </summary>
//        public static List<TopicQuestion> GetVIQuestionsByTopicID(int TopicID)
//        {
//            //            List<TopicQuestion> TopicQuestions = null;

//            // set up the TopicQuestions list containing 6
//            // new default TopicQuestion objects
//            //TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, 1));
//            //TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, 2));
//            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 3));
//            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 4));
//            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 5));
//            //TopicQuestions.Add(CreateNewTrueFalseQuestion(TopicID, 6));

//            // get list of TopicDetail records matching the TopicID
//            //List<TopicDetail> TopicDetails = TopicDetail.GetTopicDetailsByTopicID(TopicID);
//            List<TopicQuestion> TopicQuestions = new List<TopicQuestion>();
//            QTIUtils oQTIUtils = new QTIUtils();
//            ViDefinition TestDef = ViDefinition.GetViDefinitionByTopicID(TopicID);
//            List<QTIQuestionObject> QTIQuestions = new List<QTIQuestionObject>();
//            if (TestDef == null)
//            {
//                // new item being added
//                QTIQuestionObject QTIQuestion;
//                QTIQuestion = new QTIQuestionObject();
//                //QTIQuestions.Add(QTIQuestion);
//                //QTIQuestions.Add(QTIQuestion);
//                //QTIQuestions.Add(QTIQuestion);
//                //QTIQuestions.Add(QTIQuestion);
//                //QTIQuestions.Add(QTIQuestion);
//                //QTIQuestions.Add(QTIQuestion);
//            }
//            else
//            {
//                QTIQuestions = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(TestDef.XMLTest.ToString());
//            }

//            // check the count to be sure it's 6 question objects
//            if (QTIQuestions.Count > 0)
//            {
//                // 6 objects found, so put the info into the 6
//                // TopicQuestion objects in the TopicQuestions list


//                for (int i = 1; i <= QTIQuestions.Count; i++)
//                {
//                    TopicQuestions.Add(CreateNewMultipleChoiceQuestion(TopicID, i,0,""));
//                }

//                int iOrder = 0;
//                foreach (QTIQuestionObject QTIQuestion in QTIQuestions)
//                {
//                    TopicQuestions[iOrder].Question = QTIQuestion.cQuestionText;
//                    TopicQuestions[iOrder].Answer_A = QTIQuestion.cAnswer_A_Text;
//                    TopicQuestions[iOrder].Answer_B = QTIQuestion.cAnswer_B_Text;
//                    TopicQuestions[iOrder].Answer_C = QTIQuestion.cAnswer_C_Text;
//                    TopicQuestions[iOrder].Answer_D = QTIQuestion.cAnswer_D_Text;
//                    TopicQuestions[iOrder].Answer_E = QTIQuestion.cAnswer_E_Text;
//                    TopicQuestions[iOrder].Answer_F = QTIQuestion.cAnswer_F_Text;

//                    // 8-13-2008 Workaround to force True and False into first two options
//                    // on questions 3, 4, 5, 6 (zero-based collection = 2,3,4,5)
//                    //if (iOrder > 1)
//                    //{
//                    //    TopicQuestions[iOrder].Answer_A = "True";
//                    //    TopicQuestions[iOrder].Answer_B = "False";
//                    //}
//                    // end workaround

//                    // Correct Answer
//                    if (QTIQuestion.cCorrectAnswer == "A")
//                        TopicQuestions[iOrder].A_Correct = true;
//                    if (QTIQuestion.cCorrectAnswer == "B")
//                        TopicQuestions[iOrder].B_Correct = true;
//                    if (QTIQuestion.cCorrectAnswer == "C")
//                        TopicQuestions[iOrder].C_Correct = true;
//                    if (QTIQuestion.cCorrectAnswer == "D")
//                        TopicQuestions[iOrder].D_Correct = true;
//                    if (QTIQuestion.cCorrectAnswer == "E")
//                        TopicQuestions[iOrder].E_Correct = true;
//                    if (QTIQuestion.cCorrectAnswer == "F")
//                        TopicQuestions[iOrder].F_Correct = true;

//                    // Discussion
//                    TopicQuestions[iOrder].Discussion = QTIQuestion.cCorrectAnswerFeedback;

//                    iOrder++;

//                }
//            }
//            return TopicQuestions;
//        }



//        /// <summary>
//        /// Returns a new multiple choice TopicQuestion object
//        /// </summary>
//        private static TopicQuestion CreateNewMultipleChoiceQuestion(int TopicID, int QuestionNum, int HeaderNum, string HeaderName)
//        {
//            TopicQuestion objTopicQuestion =
//              new TopicQuestion(TopicID,
//                QuestionNum, HeaderNum, HeaderName, "",
//                false, "",
//                false, "",
//                false, "",
//                false, "",
//                false, "",
//                false, "",
//                "");
//            return objTopicQuestion;
//        }

//        /// <summary>
//        /// Returns a new true/false TopicQuestion object
//        /// </summary>
//        private static TopicQuestion CreateNewTrueFalseQuestion(int TopicID, int QuestionNum)
//        {
//            TopicQuestion objTopicQuestion =
//              new TopicQuestion(TopicID,
//                QuestionNum, 0, "", "",
//                false, "True",
//                false, "False",
//                false, "",
//                false, "",
//                false, "",
//                false, "",
//                "");
//            return objTopicQuestion;
//        }



//        public static bool DeleteTopicQuestion(int TopicID,int QuestionNum)
//        {

//            List<TopicQuestion> TopicQuestions = GetTopicQuestionsByTopicID(TopicID);
//                        TopicQuestions.RemoveAt(QuestionNum - 1);

//                int i;
//                for (i = QuestionNum; i < TopicQuestions.Count; i++)
//                {
//                    TopicQuestions[i].QuestionNum = i-1;
//                }
//                bool ret;

//                if (TopicQuestions.Count <= 0)
//                {

//                  TestDefinition objtest= TestDefinition.GetTestDefinitionByTopicID(TopicID);
//                    if(objtest is Nullable)
//                    {
//                        ret = false;
                        
//                    }
//                    else
//                    {
//                        ret = TestDefinition.DeleteTestDefinition(objtest.TestDefID);
//                    }
//                }
//                else
//                {

//                  ret = InsertTopicQuestionsFromList(TopicQuestions);
//                }

//            return ret;
            
//        }


//        public static bool DeleteVIQuestion(int TopicID, int QuestionNum)
//        {

//            List<TopicQuestion> TopicQuestions = GetVIQuestionsByTopicID(TopicID);
//            TopicQuestions.RemoveAt(QuestionNum - 1);

//            int i;
//            for (i = QuestionNum; i < TopicQuestions.Count; i++)
//            {
//                TopicQuestions[i].QuestionNum = i - 1;
//            }
//            bool ret;

//            if (TopicQuestions.Count <= 0)
//            {

//                ViDefinition objtest = ViDefinition.GetViDefinitionByTopicID(TopicID);
//                if (objtest is Nullable)
//                {
//                    ret = false;

//                }
//                else
//                {
//                    ret = ViDefinition.DeleteViDefinition(objtest.ViDefID);
//                }
//            }
//            else
//            {

//                ret = InsertVIQuestionsFromList(TopicQuestions);
//            }

//            return ret;

//        }

//        public static bool UpdateTopicQuestion(int TopicID,
//             int QuestionNum, string Question,
//             bool A_Correct, string Answer_A,
//             bool B_Correct, string Answer_B,
//             bool C_Correct, string Answer_C,
//             bool D_Correct, string Answer_D,
//             bool E_Correct, string Answer_E,
//             string Discussion)

// //            bool E_Correct, string Answer_E,
//        //            bool F_Correct, string Answer_F,
//        {
//            // get the complete set of 6 questions from the database
//            List<TopicQuestion> TopicQuestions = GetTopicQuestionsByTopicID(TopicID);

//            int iQuestionPointer;

//            if (QuestionNum > TopicQuestions.Count)
//            {

//                TopicQuestion objTopicQuestion = new TopicQuestion(TopicID, QuestionNum, 0, "", Question, A_Correct, Answer_A,
//                   B_Correct, Answer_B, C_Correct, Answer_C, D_Correct, Answer_D, E_Correct, Answer_E, false, "", Discussion);
//                TopicQuestions.Add(objTopicQuestion);
//            }
//            else
//            {
//                iQuestionPointer = QuestionNum - 1;
//                // update the record for the question number passed in
//                TopicQuestions[iQuestionPointer].Question = Question;
//                TopicQuestions[iQuestionPointer].A_Correct = A_Correct;
//                TopicQuestions[iQuestionPointer].Answer_A = Answer_A;
//                TopicQuestions[iQuestionPointer].B_Correct = B_Correct;
//                TopicQuestions[iQuestionPointer].Answer_B = Answer_B;
//                TopicQuestions[iQuestionPointer].C_Correct = C_Correct;
//                TopicQuestions[iQuestionPointer].Answer_C = Answer_C;
//                TopicQuestions[iQuestionPointer].D_Correct = D_Correct;
//                TopicQuestions[iQuestionPointer].Answer_D = Answer_D;
//                TopicQuestions[iQuestionPointer].E_Correct = E_Correct;
//                TopicQuestions[iQuestionPointer].Answer_E = Answer_E;
//                //TopicQuestions[iQuestionPointer].F_Correct = F_Correct;
//                //TopicQuestions[iQuestionPointer].Answer_F = Answer_F;
//                //TopicQuestions[iQuestionPointer].E_Correct = false;
//                //TopicQuestions[iQuestionPointer].Answer_E = "";
//                TopicQuestions[iQuestionPointer].F_Correct = false;
//                TopicQuestions[iQuestionPointer].Answer_F = "";

//                TopicQuestions[iQuestionPointer].Discussion = Discussion;
//            }

//            // insert the entire TopicQuestions list (it first deletes
//            // the existing topicdetail records)
//            bool ret = InsertTopicQuestionsFromList(TopicQuestions);

//            return ret;

//        }

//        //Bsk New 
//        public static bool InsertTopicQuestion(int TopicID,
//           int QuestionNum, int HeaderNum, string Question,
//           bool A_Correct, string Answer_A,
//           bool B_Correct, string Answer_B,
//           bool C_Correct, string Answer_C,
//           bool D_Correct, string Answer_D,
//           bool E_Correct, string Answer_E,
//           string Discussion)
//        {
//            // get the complete set of 6 questions from the database
//            List<TopicQuestion> TopicQuestions = GetTopicQuestionsByTopicID(TopicID);

//            TopicQuestion objTopicQuestion = new TopicQuestion(TopicID, QuestionNum, HeaderNum, "", Question, A_Correct, Answer_A,
//               B_Correct, Answer_B, C_Correct, Answer_C, D_Correct, Answer_D, E_Correct, Answer_E, false, "", Discussion);
//            TopicQuestions.Add(objTopicQuestion);

//            // insert the entire TopicQuestions list (it first deletes
//            // the existing topicdetail records)
//            bool ret = InsertTopicQuestionsFromList(TopicQuestions);

//            return ret;

//        }



//        public static bool UpdateVIQuestion(int TopicID,
//           int QuestionNum, string Question,
//           bool A_Correct, string Answer_A,
//           bool B_Correct, string Answer_B,
//           bool C_Correct, string Answer_C,
//           bool D_Correct, string Answer_D,
//           bool E_Correct, string Answer_E,
//           string Discussion)

////            bool E_Correct, string Answer_E,
//        //            bool F_Correct, string Answer_F,
//        {
//            // get the complete set of 6 questions from the database
//            List<TopicQuestion> TopicQuestions = GetVIQuestionsByTopicID(TopicID);

//            int iQuestionPointer;

//            if (QuestionNum > TopicQuestions.Count)
//            {
//                TopicQuestion objTopicQuestion = new TopicQuestion(TopicID, QuestionNum,0,"", Question, A_Correct, Answer_A,
//                   B_Correct, Answer_B, C_Correct, Answer_C, D_Correct, Answer_D, E_Correct, Answer_E, false, "", Discussion);
//                TopicQuestions.Add(objTopicQuestion);
//            }
//            else
//            {
//                iQuestionPointer = QuestionNum - 1;
//                // update the record for the question number passed in
//                TopicQuestions[iQuestionPointer].Question = Question;
//                TopicQuestions[iQuestionPointer].A_Correct = A_Correct;
//                TopicQuestions[iQuestionPointer].Answer_A = Answer_A;
//                TopicQuestions[iQuestionPointer].B_Correct = B_Correct;
//                TopicQuestions[iQuestionPointer].Answer_B = Answer_B;
//                TopicQuestions[iQuestionPointer].C_Correct = C_Correct;
//                TopicQuestions[iQuestionPointer].Answer_C = Answer_C;
//                TopicQuestions[iQuestionPointer].D_Correct = D_Correct;
//                TopicQuestions[iQuestionPointer].Answer_D = Answer_D;
//                TopicQuestions[iQuestionPointer].E_Correct = E_Correct;
//                TopicQuestions[iQuestionPointer].Answer_E = Answer_E;
//                //TopicQuestions[iQuestionPointer].F_Correct = F_Correct;
//                //TopicQuestions[iQuestionPointer].Answer_F = Answer_F;
//                //TopicQuestions[iQuestionPointer].E_Correct = false;
//                //TopicQuestions[iQuestionPointer].Answer_E = "";
//                TopicQuestions[iQuestionPointer].F_Correct = false;
//                TopicQuestions[iQuestionPointer].Answer_F = "";

//                TopicQuestions[iQuestionPointer].Discussion = Discussion;
//            }

//            // insert the entire TopicQuestions list (it first deletes
//            // the existing topicdetail records)
//            bool ret = InsertVIQuestionsFromList(TopicQuestions);

//            return ret;

//        }



//        public static bool InsertVIQuestionsFromList(
//            List<TopicQuestion> TopicQuestions)
//        {

//            List<QTIQuestionObject> QTIQuestions = new List<QTIQuestionObject>();

//            int iTopicID = 0;

//            // check the count to be sure it's 6 records
//            if (TopicQuestions.Count > 0)
//            {
//                // 6 recs found, so put the info into 6
//                // QTIQuestionObject objects in the QTIQuestions list
//                int iOrder = 0;
//                string cCorrectAns = "";
//                foreach (TopicQuestion record in TopicQuestions)
//                {

//                    iOrder++;
//                    if (iOrder == 1)
//                        iTopicID = record.TopicID;

//                    QTIQuestionObject QTIQuestion = new QTIQuestionObject();

//                    QTIQuestion.cQuestionID = "Topic_" + iTopicID.ToString() + "_Question_" + iOrder.ToString();
//                    QTIQuestion.cQuestionTitle = "PearlsReview.com Question # " + iOrder.ToString() +
//                        " for Topic # " + iTopicID.ToString();


//                    // TODO: Make this dynamic instead of 2 MC and 4 TF
//                    if (record.Answer_C == "")
//                    {
//                        QTIQuestion.cResponseID = "TF_0" + iOrder.ToString();

//                    }
//                    else
//                    {
//                        QTIQuestion.cResponseID = "MC_0" + iOrder.ToString();
//                    }

//                    QTIQuestion.cQuestionText = record.Question;
//                    QTIQuestion.cAnswer_A_Text = record.Answer_A;
//                    QTIQuestion.cAnswer_B_Text = record.Answer_B;
//                    QTIQuestion.cAnswer_C_Text = record.Answer_C;
//                    QTIQuestion.cAnswer_D_Text = record.Answer_D;
//                    QTIQuestion.cAnswer_E_Text = record.Answer_E;
//                    QTIQuestion.cAnswer_F_Text = record.Answer_F;
//                    if (record.A_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "A";
//                    if (record.B_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "B";
//                    if (record.C_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "C";
//                    if (record.D_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "D";
//                    if (record.E_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "E";
//                    if (record.F_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "F";
//                    QTIQuestion.cCorrectAnswerFeedback = record.Discussion;
//                    QTIQuestion.cUserAnswer = "";

//                    QTIQuestions.Add(QTIQuestion);

//                }


//            }

//            string cXML = "";
//            bool ret = false;

//            if (QTIQuestions.Count > 0 && iTopicID > 0)
//            {
//                QTIUtils oQTIUtils = new QTIUtils();
//                cXML = oQTIUtils.ConvertQTIQuestionObjectListToQTITestXMLString(QTIQuestions);
//                ViDefinition oVIDefinition = ViDefinition.GetViDefinitionByTopicID(iTopicID);
//                if (oVIDefinition == null)
//                {
//                    // inserting new record
//                    int newID = ViDefinition.InsertViDefinition(iTopicID,
//                        cXML,"", 1);
//                    if (newID > 0)
//                        ret = true;
//                }
//                else
//                {
//                    // updating existing record
//                    ret = ViDefinition.UpdateViDefinition(oVIDefinition.ViDefID, iTopicID,
//                        cXML, oVIDefinition.Vignette_Desc, oVIDefinition.Version + 1);
//                }
//            }

//            return ret;
//        }



//        public static bool InsertTopicQuestionsFromList(
//            List<TopicQuestion> TopicQuestions)
//        {

//            List<QTIQuestionObject> QTIQuestions = new List<QTIQuestionObject>();

//            int iTopicID = 0;

//            // check the count to be sure it's 6 records
//            if (TopicQuestions.Count > 0)
//            {
//                // 6 recs found, so put the info into 6
//                // QTIQuestionObject objects in the QTIQuestions list
//                int iOrder = 0;
//                string cCorrectAns = "";
//                foreach (TopicQuestion record in TopicQuestions)
//                {

//                    iOrder++;
//                    //Bsk New
//                    //iOrder = record.QuestionNum;
//                    if (iOrder == 1)
//                        iTopicID = record.TopicID;

//                    QTIQuestionObject QTIQuestion = new QTIQuestionObject();

//                    QTIQuestion.cQuestionID = "Topic_" + iTopicID.ToString() + "_Question_" + iOrder.ToString();
//                    QTIQuestion.cQuestionTitle = "PearlsReview.com Question # " + iOrder.ToString() +
//                        " for Topic # " + iTopicID.ToString();

//                    //Bsk New 
//                    QTIQuestion.cHeaderID = record.HeaderNum.ToString();

//                    // TODO: Make this dynamic instead of 2 MC and 4 TF
//                    if (record.Answer_C == "")
//                    {
//                        QTIQuestion.cResponseID = "TF_0" + iOrder.ToString();

//                    }
//                    else
//                    {
//                        QTIQuestion.cResponseID = "MC_0" + iOrder.ToString();
//                    }

//                    QTIQuestion.cQuestionText = record.Question;
//                    QTIQuestion.cAnswer_A_Text = record.Answer_A;
//                    QTIQuestion.cAnswer_B_Text = record.Answer_B;
//                    QTIQuestion.cAnswer_C_Text = record.Answer_C;
//                    QTIQuestion.cAnswer_D_Text = record.Answer_D;
//                    QTIQuestion.cAnswer_E_Text = record.Answer_E;
//                    QTIQuestion.cAnswer_F_Text = record.Answer_F;
//                    if (record.A_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "A";
//                    if (record.B_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "B";
//                    if (record.C_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "C";
//                    if (record.D_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "D";
//                    if (record.E_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "E";
//                    if (record.F_Correct == true)
//                        QTIQuestion.cCorrectAnswer = "F";
//                    QTIQuestion.cCorrectAnswerFeedback = record.Discussion;
//                    QTIQuestion.cUserAnswer = "";

//                    QTIQuestions.Add(QTIQuestion);

//                }


//            }

//            string cXML = "";
//            bool ret = false;

//            if (QTIQuestions.Count > 0 && iTopicID > 0)
//            {
//                QTIUtils oQTIUtils = new QTIUtils();
//                cXML = oQTIUtils.ConvertQTIQuestionObjectListToQTITestXMLString(QTIQuestions);
//                TestDefinition oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(iTopicID);
//                if (oTestDefinition == null)
//                {
//                    // inserting new record
//                    int newID = TestDefinition.InsertTestDefinition(iTopicID,
//                        cXML, 1);
//                    if (newID > 0)
//                        ret = true;
//                }
//                else
//                {
//                    // updating existing record
//                    ret = TestDefinition.UpdateTestDefinition(oTestDefinition.TestDefID, iTopicID,
//                        cXML, oTestDefinition.Version + 1);
//                }
//            }

//            return ret;
//        }

//        ///// <summary>
//        ///// Returns a new TopicDetail object
//        ///// </summary>
//        //private static TopicDetailInfo CreateNewTopicDetailObject(
//        //  int TopicID, string Type, string Data, string CorrectAns,
//        //    int Order, string Discussion)
//        //{
//        //    TopicDetailInfo objTopicDetail =
//        //      new TopicDetailInfo(TopicID, Type, Data,
//        //      CorrectAns, Order, Discussion);

//        //    return objTopicDetail;
//        //}

//    }


    //////////////////////////////////////////
    /////<Summary>
    /////New GiftCard Business Object class
    /////</Summary>
    /////
    // public class NewGiftcard : BasePR
    // {
    //     private int  _GcID = 0;
    //     public int GcID
    //     {
    //         get{ return _GcID;}
    //         protected set {_GcID= value;}
    //     }

    //     private string _GcNumber = "";

    //     public string GcNumber
    //     {
    //         get { return _GcNumber; }
    //         set { _GcNumber = value; }
    //     }

    //     private int _GiftCardCHour = 0;

    //     public int GiftCardCHour
    //     {
    //         get { return _GiftCardCHour; }
    //         set { _GiftCardCHour = value; }
    //     }

    //     private int _FacilityID = 0;

    //     public int FacilityID
    //     {
    //         get { return _FacilityID; }
    //         set { _FacilityID = value; }
    //     }

    //     private string _FacilityName = "";

    //     public string FacilityName
    //     {
    //         get { return _FacilityName; }
    //         set { _FacilityName = value; }
    //     }

    //     public NewGiftcard(int GcID, string GcNumber, int GiftCardCHour,
    //       int FacilityID, string FacilityName)
    //  {
    //      this.GcID = GcID;
    //      this.GcNumber = GcNumber;
    //      this.GiftCardCHour = GiftCardCHour;
    //      this.FacilityID = FacilityID;
    //      this.FacilityName = FacilityName;
          
    //  }


    //     /// <summary>
    //     /// Insert a batch of TopicDetail records from a List
    //     /// </summary>
    //     /// <summary>
    //     /// Creates a new LicenseType
    //     /// </summary>
    //     /// <summary>
    //     /// Creates a new Test
    //     /// </summary>
    //     public static int InsertNewGiftcard(int GcID, string GcNumber, int GiftCardCHour,
    //       int FacilityID, string FacilityName)
      
    //     {
    //         //GcID = BizObject.ConvertNullToEmptyString(GcID);
    //         GcNumber = BizObject.ConvertNullToEmptyString(GcNumber);
    //         //GiftCardCHour = BizObject.ConvertNullToEmptyString(GiftCardCHour);
    //         //FacilityID = BizObject.ConvertNullToEmptyString(FacilityID);
    //         FacilityName = BizObject.ConvertNullToEmptyString(FacilityName);
             
    //         NewGiftcardInfo record = new NewGiftcardInfo(0,GcNumber,GiftCardCHour,FacilityID,FacilityName);
    //         int ret = SiteProvider.PR.InsertNewGiftcard(record);

    //         BizObject.PurgeCacheItems("NewGiftcards_NewGiftcard");
    //         return ret;
    //     }

    //     public static NewGiftcard GetNewGiftcardInfoByGcNumber(string GcNumber, string cSortExpression)
    //     {
    //         if (cSortExpression == null)
    //             cSortExpression = "";

    //         // provide default sort
    //         if (cSortExpression.Length == 0)
    //             cSortExpression = "gcnumber DESC";

    //         List<NewGiftcard> NewGiftcards = null;
    //         //string key = "Tests_Tests_" + UserID.ToString() + cSortExpression.ToString();

    //         //if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //         //{
    //         //    Tests = (List<Test>)BizObject.Cache[key];
    //         //}
    //         //else
    //         //{
    //         List<NewGiftcardInfo> recordset = SiteProvider.PR.GetNewGiftcardByGcNumber(GcNumber);
    //         NewGiftcards = GetNewGiftcardInfoListFromGetNewGiftcardInfoList(recordset);
    //         //BasePR.CacheData(key, Tests);

    //         //}
    //         if (NewGiftcards.Count > 0)
    //         {
    //             return NewGiftcards[0];
    //         }
    //         else
    //         {
    //             return null;
    //         }
    //     }

    //     /// <summary>
    //     /// Returns a list of Test objects filled with the data taken from the input list of TestInfo
    //     /// </summary>
    //     private static List<NewGiftcard> GetNewGiftcardInfoListFromGetNewGiftcardInfoList(List<NewGiftcardInfo> recordset)
    //     {
    //         List<NewGiftcard> NewGiftcards = new List<NewGiftcard>();
    //         foreach (NewGiftcardInfo record in recordset)
    //             NewGiftcards.Add(GetNewGiftcardFromNewGiftcardInfo(record));
    //         return NewGiftcards;
    //     }


    //     /// <summary>
    //     /// Returns a Test object filled with the data taken from the input TestInfo
    //     /// </summary>
    //     private static NewGiftcard GetNewGiftcardFromNewGiftcardInfo(NewGiftcardInfo record)
    //     {
    //         if (record == null)
    //             return null;
    //         else
    //         {
    //             return new NewGiftcard(record.GcID, record.GcNumber, record.GiftCardCHour, record.FacilityID, record.FacilityName);                 
    //         }
    //     }

    // }

     ///////////////////////////////////////////////////////////////
     ///// <summary>
     ///// FacilityAreaLink business object class
     ///// </summary>

     //public class FacilityAreaLink : BasePR
     //{
     //    private int _facilityid = 0;
     //    public int facilityid
     //    {
     //        get { return _facilityid; }
     //        protected set { _facilityid = value; }
     //    }

     //    private int _areaid = 0;
     //    public int areaid
     //    {
     //        get { return _areaid; }
     //        set { _areaid = value; }
     //    }

     //    public FacilityAreaLink(int facilityid, int areaid)
     //    {
     //        this.facilityid = facilityid;
     //        this.areaid = areaid;
     //    }

     //    public static List<FacilityAreaLink> GetFacilityAreaLinkByFacilityId(int facilityid)
     //    {
     //        List<FacilityAreaLink> FacilityAreaLinks = null;

     //        List<FacilityAreaLinkInfo> recordset = SiteProvider.PR.GetFacilityAreaLinkByFacilityId(facilityid);
     //        FacilityAreaLinks = GetFacilityAreaLinkListFromGetFacilityAreaLinkInfoList(recordset);
     //        return FacilityAreaLinks;

     //    }

     //    private static List<FacilityAreaLink> GetFacilityAreaLinkListFromGetFacilityAreaLinkInfoList(List<FacilityAreaLinkInfo> recordset)
     //    {
     //        List<FacilityAreaLink> FacilityAreaLinks = new List<FacilityAreaLink>();
     //        foreach (FacilityAreaLinkInfo record in recordset)
     //            FacilityAreaLinks.Add(GetFacilityAreaLinkFromFacilityAreaLink(record));
     //        return FacilityAreaLinks;
     //    }

     //    private static FacilityAreaLink GetFacilityAreaLinkFromFacilityAreaLink(FacilityAreaLinkInfo record)
     //    {
     //        if (record == null)
     //            return null;
     //        else
     //        {
     //            return new FacilityAreaLink(record.facilityid,record.areaid);
     //        }
     //    }

     //    public static int UpdateFacilityAreaLink(int facilityid, int areaid)
     //    {
     //        FacilityAreaLinkInfo record = new FacilityAreaLinkInfo(facilityid, areaid);
     //        int ret = SiteProvider.PR.UpdateFacilityAreaLink(record);
     //        BizObject.PurgeCacheItems("FacilityAreaLinks_FacilityAreaLink_" + facilityid.ToString());
     //        BizObject.PurgeCacheItems("FacilityAreaLinks_FacilityAreaLinks");
     //        return ret;
     //    }


     //    public static bool DeleteFacilityAreaLink(int facilityid, int areaid)
     //    {

     //        bool ret = SiteProvider.PR.DeleteFacilityAreaLink(facilityid, areaid);
     //        BizObject.PurgeCacheItems("FacilityAreaLinks_FacilityAreaLink");
     //        return ret;
     //                 }


     //}




    ///////////////////////////////////////////////////////////////
    ///// <summary>
    ///// TopicDetail business object class
    ///// </summary>
    //public class TopicDetail : BasePR
    //{
    //  private int _TopicID = 0;
    //  public int TopicID
    //  {
    //      get { return _TopicID; }
    //      protected set { _TopicID = value; }
    //  }

    //   private string _Type = "";
    //   public string Type
    //  {
    //      get { return _Type; }
    //      set { _Type = value; }
    //  }

    //  private string _Data = "";
    //   public string Data
    //  {
    //      get { return _Data; }
    //      set { _Data = value; }
    //  }

    //  private string _CorrectAns = "";
    //   public string CorrectAns
    //  {
    //      get { return _CorrectAns; }
    //      set { _CorrectAns = value; }
    //  }

    //  private int _Order = 0;
    //   public int Order
    //  {
    //      get { return _Order; }
    //      set { _Order = value; }
    //  }

    //  private string _Discussion = "";
    //   public string Discussion
    //  {
    //      get { return _Discussion; }
    //      set { _Discussion = value; }
    //  }

    //   public TopicDetail(int TopicID, string Type, string Data,
    //       string CorrectAns, int Order, string Discussion)
    //  {
    //      this.TopicID = TopicID;
    //      this.Type = Type;
    //      this.Data = Data;
    //      this.CorrectAns = CorrectAns;
    //      this.Order = Order;
    //      this.Discussion = Discussion;
    //  }

    //    //public bool Delete()
    //    //{
    //    //    bool success = TopicDetail.DeleteTopicDetail(this.TopicID);
    //    //    if (success)
    //    //        this.TopicID = 0;
    //    //    return success;
    //    //}

    //    //public bool Update()
    //    //{
    //    //    return Topic.UpdateTopic(this.TopicID, this.Type, this.Data,
    //    //   this.CorrectAns, this.Order, this.Discussion);
    //    //}

    //    /***********************************
    //    * Static methods
    //    ************************************/

    //    /// <summary>
    //    /// Returns a collection with all Topics
    //    /// </summary>
    //    public static List<TopicDetail> GetTopicDetailsByTopicID(int TopicID)
    //    {
    //        List<TopicDetail> TopicDetails = null;
    //        string key = "TopicDetails_TopicDetails_" + TopicID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicDetails = (List<TopicDetail>)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            List<TopicDetailInfo> recordset = SiteProvider.PR.GetTopicDetailsByTopicID(TopicID);
    //            TopicDetails = GetTopicDetailListFromTopicDetailInfoList(recordset);
    //            BasePR.CacheData(key, TopicDetails);
    //        }
    //        return TopicDetails;
    //    }


    //    /// <summary>
    //    /// Returns the number of total Topics
    //    /// </summary>
    //    public static int GetTopicDetailCount()
    //    {
    //        int TopicDetailCount = 0;
    //        string key = "TopicDetails_TopicDetailCount";

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicDetailCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicDetailCount = SiteProvider.PR.GetTopicDetailCount();
    //            BasePR.CacheData(key, TopicDetailCount);
    //        }
    //        return TopicDetailCount;
    //    }

    //    /// <summary>
    //    /// Returns the number of total TopicDetails for a TopicID
    //    /// </summary>
    //    public static int GetTopicDetailCount(int TopicID)
    //    {
    //        int TopicDetailCount = 0;
    //        string key = "TopicDetails_TopicDetailCount_" + TopicID.ToString();

    //        if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //        {
    //            TopicDetailCount = (int)BizObject.Cache[key];
    //        }
    //        else
    //        {
    //            TopicDetailCount = SiteProvider.PR.GetTopicDetailCountByTopicID(TopicID);
    //            BasePR.CacheData(key, TopicDetailCount);
    //        }
    //        return TopicDetailCount;
    //    }


    //    ///// <summary>
    //    ///// Updates an existing Topic
    //    ///// </summary>
    //    //public static bool UpdateTopic(int TopicID, string TopicName, string HTML,
    //    //   string Questions, string Answers, int Version, bool CorLecture,
    //    //   string Fla_CEType, int Fla_IDNo)
    //    //{
    //    //    TopicName = BizObject.ConvertNullToEmptyString(TopicName);
    //    //    HTML = BizObject.ConvertNullToEmptyString(HTML);
    //    //    Questions = BizObject.ConvertNullToEmptyString(Questions);
    //    //    Answers = BizObject.ConvertNullToEmptyString(Answers);
    //    //    Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);

    //    //    TopicInfo record = new TopicInfo(TopicID, TopicName, HTML,
    //    //   Questions, Answers, Version, CorLecture,
    //    //   Fla_CEType, Fla_IDNo);
    //    //    bool ret = SiteProvider.PR.UpdateTopic(record);

    //    //    BizObject.PurgeCacheItems("Topics_Topic_" + iID.ToString());
    //    //    BizObject.PurgeCacheItems("Topics_Topics");
    //    //    return ret;
    //    //}

    //    ///// <summary>
    //    ///// Creates a new Topic
    //    ///// </summary>
    //    //public static int InsertTopic(string TopicName, string HTML,
    //    //   string Questions, string Answers, int Version, bool CorLecture,
    //    //   string Fla_CEType, int Fla_IDNo)
    //    //{
    //    //    TopicName = BizObject.ConvertNullToEmptyString(TopicName);
    //    //    HTML = BizObject.ConvertNullToEmptyString(HTML);
    //    //    Questions = BizObject.ConvertNullToEmptyString(Questions);
    //    //    Answers = BizObject.ConvertNullToEmptyString(Answers);
    //    //    Fla_CEType = BizObject.ConvertNullToEmptyString(Fla_CEType);

    //    //    TopicInfo record = new TopicInfo(0, TopicName, HTML,
    //    //   Questions, Answers, Version, CorLecture,
    //    //   Fla_CEType, Fla_IDNo);
    //    //    int ret = SiteProvider.PR.InsertTopic(record);

    //    //    BizObject.PurgeCacheItems("Topics_Topic");
    //    //    return ret;
    //    //}

    //    /// <summary>
    //    /// Deletes all existing TopicDetail records for a TopicID
    //    /// </summary>
    //    public static bool DeleteTopicDetailsByTopicID(int TopicID)
    //    {
    //        bool ret = SiteProvider.PR.DeleteTopicDetailsByTopicID(TopicID);
    //        //         new RecordDeletedEvent("Topic", iID, null).Raise();
    //        BizObject.PurgeCacheItems("TopicDetails_TopicDetail");
    //        return ret;
    //    }

    //    /// <summary>
    //    /// Insert a batch of TopicDetail records from a List
    //    /// </summary>
    //    public static bool InsertTopicDetailsFromTopicDetailList(
    //      List<TopicDetailInfo> TopicDetails)
    //    {

    //        bool ret = false;

    //        // check the count to be sure it's 34 records
    //        if (TopicDetails.Count == 34)
    //        {

    //            int TopicID = TopicDetails[0].TopicID;

    //            // delete all existing records for this TopicID
    //            bool delsuccess = DeleteTopicDetailsByTopicID(TopicID);

    //            // insert all new records
    //            ret = SiteProvider.PR.InsertTopicDetailsFromTopicDetailList(
    //              TopicDetails);

    //            ////////////////////////
    //            // not sure about this purge -- need to check it out
    //            BizObject.PurgeCacheItems("TopicDetails_TopicDetail");
    //            ////////////////////////
    //        }

    //        return ret;

    //    }

    //    /// <summary>
    //    /// Returns a TopicDetail object filled with the data taken from the input TopicDetailInfo
    //    /// </summary>
    //    private static TopicDetail GetTopicDetailFromTopicDetailInfo(TopicDetailInfo record)
    //    {
    //        if (record == null)
    //            return null;
    //        else
    //        {
    //            return new TopicDetail(record.TopicID, record.Type, record.Data,
    //       record.CorrectAns, record.Order, record.Discussion);
    //        }
    //    }

    //    /// <summary>
    //    /// Returns a list of TopicDetail objects filled with the data taken from the input list of TopicDetailInfo
    //    /// </summary>
    //    private static List<TopicDetail> GetTopicDetailListFromTopicDetailInfoList(List<TopicDetailInfo> recordset)
    //    {
    //        List<TopicDetail> TopicDetails = new List<TopicDetail>();
    //        foreach (TopicDetailInfo record in recordset)
    //            TopicDetails.Add(GetTopicDetailFromTopicDetailInfo(record));
    //        return TopicDetails;
    //    }
    //}

//    public class Nurse : BasePR
//    {
//        private int _rnID = 0;
//        public int rnID
//        {
//            get { return _rnID; }
//            protected set { _rnID = value; }
//        }

//        private string _UserName = "";
//        public string UserName
//        {
//            get { return _UserName; }
//             set { _UserName = value; }
//        }

//        private string _Password = "";
//        public string Password
//        {
//            get { return _Password; }
//             set { _Password = value; }
//        }

//        private bool _Registered = false;
//        public bool Registered
//        {
//            get { return _Registered; }
//             set { _Registered = value; }
//        }

//        private string _FirstName = "";
//        public string FirstName
//        {
//            get { return _FirstName; }
//            set { _FirstName = value; }
//        }
//        private string _LastName = "";
//        public string LastName
//        {
//            get { return _LastName; }
//            set { _LastName = value; }
//        }
//        private string _Address1 = "";
//        public string Address1
//        {
//            get { return _Address1; }
//             set { _Address1 = value; }
//        }
//        private string _Address2 = "";
//        public string Address2
//        {
//            get { return _Address2; }
//             set { _Address2 = value; }
//        }
//        private string _City = "";
//        public string City
//        {
//            get { return _City; }
//             set { _City = value; }
//        }
//        private string _State = "";
//        public string State
//        {
//            get { return _State; }
//             set { _State = value; }
//        }
//        private string _Zip = "";
//        public string Zip
//        {
//            get { return _Zip; }
//             set { _Zip = value; }
//        }
//        private string _Country = "";
//        public string Country
//        {
//            get { return _Country; }
//             set { _Country = value; }
//        }
//        private string _SAddress1 = "";
//        public string SAddress1
//        {
//            get { return _SAddress1; }
//             set { _SAddress1 = value; }
//        }
//        private string _SAddress2 = "";
//        public string SAddress2
//        {
//            get { return _SAddress2; }
//             set { _SAddress2 = value; }
//        }
//        private string _SCity = "";
//        public string SCity
//        {
//            get { return _SCity; }
//             set { _SCity = value; }
//        }
//        private string _SState = "";
//        public string SState
//        {
//            get { return _SState; }
//             set { _SState = value; }
//        }
//        private string _SZip = "";
//        public string SZip
//        {
//            get { return _SZip; }
//            set { _SZip = value; }
//        }
//        private string _SCountry = "";
//        public string SCountry
//        {
//            get { return _SCountry; }
//             set { _SCountry = value; }
//        }
//        private string _Email = "";
//        public string Email
//        {
//            get { return _Email; }
//             set { _Email = value; }
//        }
//        private string _EmailPref = "";
//        public string EmailPref
//        {
//            get { return _EmailPref; }
//             set { _EmailPref = value; }
//        }
//        private bool _OptIn = false;
//        public bool OptIn
//        {
//            get { return _OptIn; }
//             set { _OptIn = value; }
//        }
//        private bool _Offers = false;
//        public bool Offers
//        {
//            get { return _Offers; }
//            protected set { _Offers = value; }
//        }
//        private string _RNstate = "";
//        public string RNstate
//        {
//            get { return _RNstate; }
//            protected set { _RNstate = value; }
//        }
//        private string _RNnum = "";
//        public string RNnum
//        {
//            get { return _RNnum; }
//            protected set { _RNnum = value; }
//        }
//        private string _RNCode = "";
//        public string RNCode
//        {
//            get { return _RNCode; }
//            protected set { _RNCode = value; }
//        }
//        private string _RNnumX = "";
//        public string RNnumX
//        {
//            get { return _RNnumX; }
//            protected set { _RNnumX = value; }
//        }
//        private string _RNstate2 = "";
//        public string RNstate2
//        {
//            get { return _RNstate2; }
//            protected set { _RNstate2 = value; }
//        }
//        private string _RNnum2 = "";
//        public string RNnum2
//        {
//            get { return _RNnum2; }
//            protected set { _RNnum2 = value; }
//        }
//        private string _RNCode2 = "";
//        public string RNCode2
//        {
//            get { return _RNCode2; }
//            protected set { _RNCode2 = value; }
//        }
//        private string _RNnumX2 = "";
//        public string RNnumX2
//        {
//            get { return _RNnumX2; }
//            protected set { _RNnumX2 = value; }
//        }
//        private int _SpecialtyID = 0;
//        public int SpecialtyID
//        {
//            get { return _SpecialtyID; }
//            protected set { _SpecialtyID = value; }
//        }
//        private string _SpecialtyIDlist = "";
//        public string SpecialtyIDlist
//        {
//            get { return _SpecialtyIDlist; }
//            protected set { _SpecialtyIDlist = value; }
//        }
//        private string _ImportedFrom = "";
//        public string ImportedFrom
//        {
//            get { return _ImportedFrom; }
//            protected set { _ImportedFrom = value; }
//        }
//        private int _FieldID = 0;
//        public int FieldID
//        {
//            get { return _FieldID; }
//            protected set { _FieldID = value; }
//        }
//        private int _AgeGroup = 0;
//        public int AgeGroup
//        {
//            get { return _AgeGroup; }
//            protected set { _AgeGroup = value; }
//        }
//        private string _BrowserAgent = "";
//        public string BrowserAgent
//        {
//            get { return _BrowserAgent; }
//            protected set { _BrowserAgent = value; }
//        }
//        private string _Phone = "";
//        public string Phone
//        {
//            get { return _Phone; }
//            protected set { _Phone = value; }
//        }
//        private string _PhoneType = "";
//        public string PhoneType
//        {
//            get { return _PhoneType; }
//            protected set { _PhoneType = value; }
//        }
//        private string _Specialty = "";
//        public string Specialty
//        {
//            get { return _Specialty; }
//            protected set { _Specialty = value; }
//        }
//        private string _Education = "";
//        public string Education
//        {
//            get { return _Education; }
//            protected set { _Education = value; }
//        }
//        private string _Position = "";
//        public string Position
//        {
//            get { return _Position; }
//            protected set { _Position = value; }
//        }
//        private DateTime _DateEntered = System.DateTime.Now;
//        public DateTime DateEntered
//        {
//            get { return _DateEntered; }
//            protected set { _DateEntered = value; }
//        }
//        private DateTime _UCEend = System.DateTime.Now;
//        public DateTime UCEend
//        {
//            get { return _UCEend; }
//             set { _UCEend = value; }
//        }
//        private DateTime _Updated = System.DateTime.Now;
//        public DateTime Updated
//        {
//            get { return _Updated; }
//            protected set { _Updated = value; }
//        }
//        private DateTime _LastLogin = System.DateTime.Now;
//        public DateTime LastLogin
//        {
//            get { return _LastLogin; }
//            protected set { _LastLogin = value; }
//        }
//        private string _IP = "";
//        public string IP
//        {
//            get { return _IP; }
//            protected set { _IP = value; }
//        }
//        private bool _Disabled = false;
//        public bool Disabled
//        {
//            get { return _Disabled; }
//            protected set { _Disabled = value; }
//        }

//         public Nurse(int rnID, string UserName, string Password, bool Registered, string FirstName, string LastName,
//          string Address1, string Address2, string City, string State, string Zip,
//          string Country, string SAddress1, string SAddress2, string SCity, string SState,
//          string SZip, string SCountry, string Email, string EmailPref, bool OptIn,
//          bool Offers, string RNstate, string RNnum, string RNCode, string RNnumX,
//          string RNstate2, string RNnum2, string RNCode2, string RNnumX2,
//          int SpecialtyID, string SpecialtyIDlist, string ImportedFrom, int FieldID, int AgeGroup,
//         string BrowserAgent, string Phone, string PhoneType, string Specialty, string Education,
//          string Position, DateTime DateEntered, DateTime UCEend, DateTime Updated, DateTime LastLogin,
//          string IP, bool Disabled)
//      {
           
//          this.rnID = rnID;
//          this.UserName = UserName;
//          this.Password = Password;
//          this.Registered = Registered;
//          this.FirstName = FirstName;
//          this.LastName = LastName;
//          this.Address1 = Address1;
//          this.Address2 = Address2;
//          this.City = City;
//          this.State = State;
//          this.Zip = Zip;
//          this.Country = Country;
//          this.SAddress1 = SAddress1;
//          this.SAddress2 = SAddress2;
//          this.SCity = SCity;
//          this.SState = SState;
//          this.SZip = SZip;
//          this.SCountry = SCountry;
//          this.Email = Email;
//          this.EmailPref = EmailPref;
//          this.OptIn = OptIn;
//          this.Offers = Offers;
//          this.RNstate = RNstate;
//          this.RNnum = RNnum;
//          this.RNCode = RNCode;
//          this.RNnumX = RNnumX;
//          this.RNstate2 = RNstate2;
//          this.RNnum2 = RNnum2;
//          this.RNCode2 = RNCode2;
//          this.RNnumX2 = RNnumX2;
//          this.SpecialtyID = SpecialtyID;
//          this.SpecialtyIDlist = SpecialtyIDlist;
//          this.ImportedFrom = ImportedFrom;
//          this.FieldID = FieldID;
//          this.AgeGroup = AgeGroup;
//          this.BrowserAgent = BrowserAgent;
//          this.Phone = Phone;
//          this.PhoneType = PhoneType;
//          this.Specialty = Specialty;
//          this.Education = Education;
//          this.Position = Position;
//          this.DateEntered = DateEntered;
//          this.UCEend = UCEend;
//          this.Updated = Updated;
//          this.LastLogin = LastLogin;
//          this.IP = IP;
//          this.Disabled = Disabled;
			
//}
//         public static int InsertNurse(string FirstName,string LastName,string UserName,string Email,string Password)
//         {
//             FirstName = BizObject.ConvertNullToEmptyString(FirstName);
//             LastName = BizObject.ConvertNullToEmptyString(LastName);
//             UserName = BizObject.ConvertNullToEmptyString(UserName);
//             Email = BizObject.ConvertNullToEmptyString(Email);
//             Password = BizObject.ConvertNullToEmptyString(Password);
//              // NOTE: Always send FirstLogin_Ind as true when inserting a new user record
//             int ret = SiteProvider.PR.InsertNurse(FirstName, LastName, UserName, Email, Password);
//             BizObject.PurgeCacheItems("Nurses_Nurse");
//             return ret;
//         }

//         public static bool UpdateNurse(int rnID, string UserName, string Password, bool Registered, string FirstName, string LastName,
//           string Address1, string Address2, string City, string State, string Zip,
//           string Country, string SAddress1, string SAddress2, string SCity, string SState,
//           string SZip, string SCountry, string Email, string EmailPref, bool OptIn,
//           bool Offers, string RNstate, string RNnum, string RNCode, string RNnumX,
//           string RNstate2, string RNnum2, string RNCode2, string RNnumX2,
//           int SpecialtyID, string SpecialtyIDlist, string ImportedFrom, int FieldID, int AgeGroup,
//          string BrowserAgent, string Phone, string PhoneType, string Specialty, string Education,
//           string Position, DateTime DateEntered, DateTime UCEend, DateTime Updated, DateTime LastLogin,
//           string IP, bool Disabled)
//         {
//             NurseInfo record = new NurseInfo(rnID, UserName, Password, Registered, FirstName, LastName, Address1, Address2,
//                 City, State, Zip, Country, SAddress1, SAddress2, SCity, SState, SZip, SCountry, Email, EmailPref, OptIn,
//                 Offers, RNstate, RNnum, RNCode, RNnumX, RNstate2, RNnum2, RNCode2, RNnumX2, SpecialtyID, SpecialtyIDlist,
//                 ImportedFrom, FieldID, AgeGroup, BrowserAgent, Phone, PhoneType, Specialty, Education, Position, DateEntered,
//                 UCEend, Updated, LastLogin, IP, Disabled);
//             PearlsReview.DAL.SQLClient.SQLPRProvider obj = new PearlsReview.DAL.SQLClient.SQLPRProvider();
//             bool ret=obj.UpdateNurse(record);
//             return ret;
//         }


//        public bool Update()
//        {
//            return Nurse.UpdateNurse(this.rnID, this.UserName, this.Password, this.Registered,
//                this.FirstName, this.LastName, this.Address1, this.Address2, this.City,
//                this.State, this.Zip, this.Country, this.SAddress1, this.SAddress2, SCity, SState, SZip, SCountry, Email, EmailPref, OptIn,
//                 Offers, RNstate, RNnum, RNCode, RNnumX, RNstate2, RNnum2, RNCode2, RNnumX2, SpecialtyID, SpecialtyIDlist,
//                 ImportedFrom, FieldID, AgeGroup, BrowserAgent, Phone, PhoneType, Specialty, Education, Position, DateEntered,
//                 UCEend, Updated, LastLogin, IP, Disabled); 
//        }


//         public static Nurse GetNurseByID(int rnID)
//         {
//             Nurse Nurse = null;
//             string key = "Nurses_Nurse_" + rnID.ToString();

//             if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
//             {
//                 Nurse = (Nurse)BizObject.Cache[key];
//             }
//             else
//             {
//                 Nurse = GetNurseFromNurseInfo(SiteProvider.PR.GetNurseByID(rnID));
//                 BasePR.CacheData(key, Nurse);
//             }
//             return Nurse;
//         }

//         ///// <summary>
//         ///// Returns a list of Nurse objects filled with the data taken from the input list of NrseuInfo
//         ///// </summary>
//         //private static List<Nurse> GetNurseListFromUNurseInfoList(List<NurseInfo> recordset)
//         //{
//         //    List<Nurse> Nurses = new List<Nurse>();
//         //    foreach (NurseInfo record in recordset)
//         //        Nurses.Add(GetNurseFromNurseInfo(record));
//         //    return Nurses;
//         //}
//         /// <summary>
//         /// Returns a UserAccount object filled with the data taken from the input UserAccountInfo
//         /// </summary>
//         private static Nurse GetNurseFromNurseInfo(NurseInfo record)
//         {
//             if (record == null)
//                 return null;
//             else
//             {
//                 return new Nurse(record.rnID, record.UserName, record.Password, record.Registered,
//                 record.FirstName, record.LastName, record.Address1, record.Address2, record.City,
//           record.State, record.Zip, record.Country, record.SAddress1, record.SAddress2,
//           record.SCity, record.SState, record.SZip, record.SCountry,record.Email,
//           record.EmailPref, record.OptIn, record.Offers, record.RNstate, record.RNnum,
//           record.RNCode, record.RNnumX, record.RNstate2, record.RNnum2,
//           record.RNCode2, record.RNnumX2, record.SpecialtyID, record.SpecialtyIDlist, record.ImportedFrom,
//           record.FieldID, record.AgeGroup, record.BrowserAgent, record.Phone, record.PhoneType,
//           record.Specialty, record.Education, record.Position, record.DateEntered, record.UCEend,
//           record.Updated, record.LastLogin, record.IP, record.Disabled);
//             }
//         }


//    }


    //////////////////////////////////////////////////////////////
    ///// <summary>
    ///// HistTranscript business object class
    ///// </summary>
    // public class HistTranscript :BasePR
    // {
    //     private int _TranscriptID = 0;
    //     public int TranscriptID
    //     {
    //         get { return _TranscriptID; }
    //         set { _TranscriptID = value; }
    //     }

    //     private int _ItemID = 0;
    //     public int ItemID
    //     {
    //         get { return _ItemID; }
    //         set { _ItemID = value; }
    //     }

    //     private int _Score = 0;
    //     public int Score
    //     {
    //         get { return _Score; }
    //         set { _Score = value; }
    //     }

    //     private DateTime _DateComplete = System.DateTime.MinValue;
    //     public DateTime DateComplete
    //     {
    //         get { return _DateComplete; }
    //         set { _DateComplete = value; }
    //     }

    //     private string _FirstName = "";
    //     public string FirstName
    //     {
    //         get { return _FirstName; }
    //         set { _FirstName = value; }
    //     }

    //     private string _LastName = "";
    //     public string LastName
    //     {
    //         get { return _LastName; }
    //         set { _LastName = value; }
    //     }

    //     private string _RnState = "";
    //     public string RnState
    //     {
    //         get { return _RnState; }
    //         set { _RnState = value; }
    //     }

    //     private string _RnNum = "";
    //     public string RnNum
    //     {
    //         get { return _RnNum; }
    //         set { _RnNum = value; }
    //     }

    //     private string _RnCode = "";
    //     public string RnCode
    //     {
    //         get { return _RnCode; }
    //         set { _RnCode = value; }
    //     }

    //     private string _RnState2 = "";
    //     public string RnState2
    //     {
    //         get { return _RnState2; }
    //         set { _RnState2 = value; }
    //     }

    //     private string _RnNum2 = "";
    //     public string RnNum2
    //     {
    //         get { return _RnNum2; }
    //         set { _RnNum2 = value; }
    //     }

    //     private string _RnCode2 = "";
    //     public string RnCode2
    //     {
    //         get { return _RnCode2; }
    //         set { _RnCode2 = value; }
    //     }

    //     private string _CourseName = "";
    //     public string CourseName
    //     {
    //         get { return _CourseName; }
    //         set { _CourseName = value; }
    //     }

    //     private string _CourseNum = "";
    //     public string CourseNum
    //     {
    //         get { return _CourseNum; }
    //         set { _CourseNum = value; }

    //     }

    //     private decimal _Credits = 0;
    //     public decimal Credits
    //     {
    //         get { return _Credits; }
    //         set { _Credits = value; }
    //     }

    //      public HistTranscript(int TranscriptID, int ItemID, int Score, DateTime DateComplete, string FirstName,
    //        string LastName, string RnState, string RnNum, string RnCode, string RnState2, string RnNum2, string RnCode2,
    //        string CourseName, string CourseNum, Decimal Credits)
    //    {
    //        this.TranscriptID = TranscriptID;
    //        this.ItemID = ItemID;
    //        this.Score = Score;
    //        this.DateComplete = DateComplete;
    //        this.FirstName = FirstName;
    //        this.LastName = LastName;
    //        this.RnState = RnState;
    //        this.RnNum = RnNum;
    //        this.RnCode = RnCode;
    //        this.RnState2 = RnState2;
    //        this.RnNum2 = RnNum2;
    //        this.RnCode2 = RnCode2;
    //        this.CourseName = CourseName;
    //        this.CourseNum = CourseNum;
    //        this.Credits = Credits;

    //    }



    //     public static List<HistTranscript> GetHistTranscriptRecords(string cSortExpression, int TranscriptID, int ItemID, int Score, DateTime DateComplete,
    //         string FirstName, string LastName, string RNState, string RNNum, string RNCode, string RNState2, string RNNum2, string RNCode2, string CourseName,
    //         string CourseNum, decimal Credits)

    //     {
    //         if(cSortExpression == null)
    //             cSortExpression ="";
    //         if(cSortExpression.Length == 0)
    //             cSortExpression = "Lastname";
    //         List<HistTranscript> HistTranscripts = null;
    //         string key = "HistTranscripts_HistTranscriptsBySearch" + "_" + FirstName + "_" + LastName + "_" + RNState + "_" + RNNum +
    //             "_" + RNCode + "_" + RNState2 + "_" + RNNum2 + "_" + RNCode2 + "_" + CourseName + "_" + Credits;
    //         if(BasePR.Settings.EnableCaching && BizObject.Cache[key]!= null)
    //         {
    //             HistTranscripts = (List<HistTranscript>)BizObject.Cache[key];
    //         }
    //         else
    //         {
    //             PearlsReview.DAL.SQLClient.SQLPRProvider obj =  new PearlsReview.DAL.SQLClient.SQLPRProvider();
    //             List<HistTranscriptInfo> Histtrans = obj.GetHistTranscriptRecords(cSortExpression, TranscriptID, ItemID, Score, DateComplete,
    //             FirstName, LastName, RNState, RNNum, RNCode, RNState2, RNNum2, RNCode2, CourseName, CourseNum, Credits);
    //             HistTranscripts = GetHistTranscriptListFromHistTranscriptInfoList(Histtrans);
    //             BasePR.CacheData(key, HistTranscripts);
    //         }
    //         return HistTranscripts;
    //     }


    //     private static List<HistTranscript> GetHistTranscriptListFromHistTranscriptInfoList(List<HistTranscriptInfo> recordset)
    //     {
    //         List<HistTranscript> HistTranscripts = new List<HistTranscript>();
    //         foreach (HistTranscriptInfo record in recordset)
    //             HistTranscripts.Add(GetHistTranscriptFromHistTranscriptInfo(record));
    //         return HistTranscripts;
    //     }

    //     private static HistTranscript GetHistTranscriptFromHistTranscriptInfo(HistTranscriptInfo record)
    //     {
    //         if (record == null)
    //             return null;
    //         else
    //         {

    //             return new HistTranscript(record.TranscriptID, record.ItemID, record.Score, record.DateComplete, record.FirstName,
    //                 record.LastName, record.RnState, record.RnNum, record.RnCode, record.RnState2, record.RnNum2, record.RnCode2,
    //                 record.CourseName, record.CourseNum, record.Credits);

    //         }
    //     }

    // }
    // public class Meta : BasePR
    // {

    //    public Meta(int metaID, string path, string title, string description, string keyword, string h1)
    //{
    //    this.metaID = metaID;
    //    this.path = path;
    //    this.title = title;
    //    this.description = description;
    //    this.keyword = keyword;
    //    this.h1 = h1;
    //}

    //public int metaID
    //{
    //    get;
    //    set;
    //}
    //public string path
    //{
    //    get;
    //    set;
    //}
    //public string title
    //{
    //    get;
    //    set;
    //}
    //public string description
    //{
    //    get;
    //    set;
    //}
    //public string keyword
    //{
    //    get;
    //    set;
    //}
    //public string h1
    //{
    //    get;
    //    set;
    //}


    //private static List<Meta> GetMetaListFromMetaInfoList(List<MetaInfo> recordset)
    //{
    //    List<Meta> Metas = new List<Meta>();
    //    foreach (MetaInfo record in recordset)
    //        Metas.Add(GetMetaFromMetaInfo(record));
    //    return Metas;
    //}

    //private static Meta GetMetaFromMetaInfo(MetaInfo record)
    //{
    //    if (record == null)
    //        return null;
    //    else
    //    {

    //        return new Meta(record.metaID, record.path, record.title, record.description, record.keyword,
    //            record.h1);

    //    }
    //}


    ///// <summary>
    ///// Returns a Topic object with the specified ID
    ///// </summary>
    //public static Meta GetMetaByPath(String Path)
    //{
    //    Meta Meta = null;
    //    string key = "Metas_Meta_" + Path.ToString();

    //    if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
    //    {
    //        Meta = (Meta)BizObject.Cache[key];
    //    }
    //    else
    //    {
    //        Meta = GetMetaFromMetaInfo(SiteProvider.PR.GetMetaByPath(Path));
    //        BasePR.CacheData(key, Meta);
    //    }
    //    return Meta;
    //}


    // }


     //#region Class CategoryAreaLink
     ///////////////////////////////////////////////////////////////
     ///// <summary>
     ///// CategoryAreaLink business object class 
     ///// </summary>

     //public class CategoryAreaLink : BasePR
     //{
     //    private int _areaid = 0;
     //    public int areaid
     //    {
     //        get { return _areaid; }
     //        protected set { _areaid = value; }
     //    }

     //    private int _categoryid = 0;
     //    public int categoryid
     //    {
     //        get { return _categoryid; }
     //        protected set { _categoryid = value; }
     //    }

     //    public CategoryAreaLink(int categoryid, int areaid)
     //    {
     //        this.areaid = areaid;
     //        this.categoryid = categoryid;
     //    }

     //    public static List<CategoryAreaLink> GetCategoryAreaLinkByCategoryId(int Categoryid)
     //    {
     //        List<CategoryAreaLink> CategoryAreaLinks = null;

     //        List<CategoryAreaLinkInfo> recordset = SiteProvider.PR.GetCategoryAreaLinkByCategoryId(Categoryid);
     //        CategoryAreaLinks = GetCategoryAreaLinkListFromGetCategoryAreaLinkInfoList(recordset);
     //        return CategoryAreaLinks;

     //    }


     //    private static List<CategoryAreaLink> GetCategoryAreaLinkListFromGetCategoryAreaLinkInfoList(List<CategoryAreaLinkInfo> recordset)
     //    {
     //        List<CategoryAreaLink> CategoryAreaLinks = new List<CategoryAreaLink>();
     //        foreach (CategoryAreaLinkInfo record in recordset)
     //            CategoryAreaLinks.Add(GetCategoryAreaLinkFromCategoryAreaLink(record));
     //        return CategoryAreaLinks;
     //    }

     //    private static CategoryAreaLink GetCategoryAreaLinkFromCategoryAreaLink(CategoryAreaLinkInfo record)
     //    {
     //        if (record == null)
     //            return null;
     //        else
     //        {
     //            return new CategoryAreaLink(record.categoryid, record.areaid);
     //        }
     //    }

     //    public static int UpdateCategoryAreaLink(int Categoryid, int areaid)
     //    {
     //        CategoryAreaLinkInfo record = new CategoryAreaLinkInfo(Categoryid, areaid);
     //        int ret = SiteProvider.PR.UpdateCategoryAreaLink(record);
     //        BizObject.PurgeCacheItems("CategoryAreaLinks_CategoryAreaLink_" + Categoryid.ToString());
     //        BizObject.PurgeCacheItems("CategoryAreaLinks_CategoryAreaLinks");
     //        return ret;
     //    }


     //    public static bool DeleteCategoryAreaLink(int Categoryid, int areaid)
     //    {

     //        bool ret = SiteProvider.PR.DeleteCategoryAreaLink(Categoryid, areaid);
     //        BizObject.PurgeCacheItems("CategoryAreaLinks_CategoryAreaLink");
     //        return ret;
     //    }


     //}
     //#endregion



}