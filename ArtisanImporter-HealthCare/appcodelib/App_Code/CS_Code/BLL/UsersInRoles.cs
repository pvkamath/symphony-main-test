﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

    /// <summary>
    /// Summary description for UsersInRoles
    /// </summary>
    public class UsersInRoles: BasePR
    {
        #region Variables and Properties

        private int _UserRoleID = 0;
        public int UserRoleID
        {
            get { return _UserRoleID; }
            protected set { _UserRoleID = value; }
        }

        private string _RoleName = "";
        public string RoleName
        {
            get { return _RoleName; }
            set { _RoleName = value; }
        }

        private string _UserName = "";
        public string UserName
        {
            get { return _UserName.ToLower(); }
            set { _UserName = value.ToLower(); }
        }
        private int _FacilityId = 0;
        public int FacilityId
        {
            get { return _FacilityId; }
            private set { _FacilityId = value; }
        }


        public UsersInRoles(int UserRoleID, string RoleName, string UserName)
        {
            this.UserRoleID = UserRoleID;
            this.RoleName = RoleName;
            this.UserName = UserName.ToLower();
        }

        public bool Delete()
        {
            bool success = UsersInRoles.DeleteUsersInRoles(this.UserRoleID);
            if (success)
                this.UserRoleID = 0;
            return success;
        }

        public bool Update()
        {
            return UsersInRoles.UpdateUsersInRoles(this.UserRoleID, this.RoleName, this.UserName,this.FacilityId);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all UsersInRoles
        /// </summary>
        public static List<UsersInRoles> GetUsersInRoles(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<UsersInRoles> UsersInRoles = null;
            string key = "UsersInRoles_UsersInRoles_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UsersInRoles = (List<UsersInRoles>)BizObject.Cache[key];
            }
            else
            {
                List<UsersInRolesInfo> recordset = SiteProvider.PR2.GetUsersInRoles(cSortExpression);
                UsersInRoles = GetUsersInRolesListFromUsersInRolesInfoList(recordset);
                BasePR.CacheData(key, UsersInRoles);
            }
            return UsersInRoles;
        }


        /// <summary>
        /// Returns the number of total UsersInRoles
        /// </summary>
        public static int GetUsersInRolesCount()
        {
            int UsersInRolesCount = 0;
            string key = "UsersInRoles_UsersInRolesCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UsersInRolesCount = (int)BizObject.Cache[key];
            }
            else
            {
                UsersInRolesCount = SiteProvider.PR2.GetUsersInRolesCount();
                BasePR.CacheData(key, UsersInRolesCount);
            }
            return UsersInRolesCount;
        }

        /// <summary>
        /// Returns a UsersInRoles object with the specified ID
        /// </summary>
        public static UsersInRoles GetUsersInRolesByID(int UserRoleID)
        {
            UsersInRoles UsersInRoles = null;
            string key = "UsersInRoles_UsersInRoles_" + UserRoleID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UsersInRoles = (UsersInRoles)BizObject.Cache[key];
            }
            else
            {
                UsersInRoles = GetUsersInRolesFromUsersInRolesInfo(SiteProvider.PR2.GetUsersInRolesByID(UserRoleID));
                BasePR.CacheData(key, UsersInRoles);
            }
            return UsersInRoles;
        }

        /// <summary>
        /// Returns a UsersInRoles object with the specified ID
        /// </summary>
        public static UsersInRoles GetUsersInRolesByUserName(string UserName)
        {
            UsersInRoles UsersInRoles = null;
            string key = "UsersInRoles_UsersInRoles_" + UserName.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UsersInRoles = (UsersInRoles)BizObject.Cache[key];
            }
            else
            {
                UsersInRoles = GetUsersInRolesFromUsersInRolesInfo(SiteProvider.PR2.GetUsersInRolesByUserName(UserName));
                BasePR.CacheData(key, UsersInRoles);
            }
            return UsersInRoles;
        }

        /// <summary>
        /// Updates an existing UsersInRoles
        /// </summary>
        public static bool UpdateUsersInRoles(int UserRoleID, string RoleName, string UserName,int FacilityID)
        {
            RoleName = BizObject.ConvertNullToEmptyString(RoleName);
            UserName = BizObject.ConvertNullToEmptyString(UserName);


            UsersInRolesInfo record = new UsersInRolesInfo(UserRoleID, RoleName, UserName,FacilityID);
            bool ret = SiteProvider.PR2.UpdateUsersInRoles(record);

            BizObject.PurgeCacheItems("UsersInRoles_UsersInRoles_" + UserRoleID.ToString());
            BizObject.PurgeCacheItems("UsersInRoles_UsersInRoles");
            return ret;
        }

        /// <summary>
        /// Creates a new UsersInRoles
        /// </summary>
        public static int InsertUsersInRoles(string RoleName, string UserName,int FacilityID)
        {
            RoleName = BizObject.ConvertNullToEmptyString(RoleName);
            UserName = BizObject.ConvertNullToEmptyString(UserName);


            UsersInRolesInfo record = new UsersInRolesInfo(0, RoleName, UserName,FacilityID);
            int ret = SiteProvider.PR2.InsertUsersInRoles(record);

            BizObject.PurgeCacheItems("UsersInRoles_UsersInRoles");
            return ret;
        }

        /// <summary>
        /// Deletes an existing UsersInRoles, but first checks if OK to delete
        /// </summary>
        public static bool DeleteUsersInRoles(int UserRoleID)
        {
            bool IsOKToDelete = OKToDelete(UserRoleID);
            if (IsOKToDelete)
            {
                return (bool)DeleteUsersInRoles(UserRoleID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing UsersInRoles - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteUsersInRoles(int UserRoleID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteUsersInRoles(UserRoleID);
            //         new RecordDeletedEvent("UsersInRoles", UserRoleID, null).Raise();
            BizObject.PurgeCacheItems("UsersInRoles_UsersInRoles");
            return ret;
        }



        /// <summary>
        /// Checks to see if a UsersInRoles can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int UserRoleID)
        {
            return true;
        }

        /// Deletes all existing UsersInRoles for username
        /// </summary>
        public static bool DeleteUsersInRolesByUserName(string UserName)
        {
            bool ret = SiteProvider.PR2.DeleteUsersInRolesByUserName(UserName);
            BizObject.PurgeCacheItems("UsersInRoles_UsersInRoles");
            return ret;
        }


        /// <summary>
        /// Returns a UsersInRoles object filled with the data taken from the input UsersInRolesInfo
        /// </summary>
        private static UsersInRoles GetUsersInRolesFromUsersInRolesInfo(UsersInRolesInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new UsersInRoles(record.UserRoleID, record.RoleName, record.UserName.ToLower());
            }
        }

        /// <summary>
        /// Returns a list of UsersInRoles objects filled with the data taken from the input list of UsersInRolesInfo
        /// </summary>
        private static List<UsersInRoles> GetUsersInRolesListFromUsersInRolesInfoList(List<UsersInRolesInfo> recordset)
        {
            List<UsersInRoles> UsersInRoles = new List<UsersInRoles>();
            foreach (UsersInRolesInfo record in recordset)
                UsersInRoles.Add(GetUsersInRolesFromUsersInRolesInfo(record));
            return UsersInRoles;
        }
        #endregion
    }
}
