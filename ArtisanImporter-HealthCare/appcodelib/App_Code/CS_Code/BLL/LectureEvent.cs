﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
/// <summary>
/// Summary description for LectureEvent
/// </summary>
public class LectureEvent: BasePR
    {
        #region Variables and Properties

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            protected set { _LectEvtID = value; }
        }

        private int _LectureID = 0;
        public int LectureID
        {
            get { return _LectureID; }
            set { _LectureID = value; }
        }

        private DateTime _StartDate = System.DateTime.Now;
        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
       
        private int _Capacity = 0;
        public int Capacity
        {
            get { return _Capacity; }
            set { _Capacity = value; }
        }

        private int _Attendees = 0;
        public int Attendees
        {
            get { return _Attendees; }
            set { _Attendees = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        private string _Facility = "";
        public string Facility
        {
            get { return _Facility; }
            set { _Facility = value; }
        }

        private string _City = "";
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }

        private string _State = "";
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        private bool _Finalized = false;
        public bool Finalized
        {
            get { return _Finalized; }
            set { _Finalized = value; }
        }

        private decimal _Hours = 0;
        public decimal Hours
        {
            get { return _Hours; }
            set { _Hours = value; }
        }
        
        private string _EventType = "";
        public string EventType 
        {
            get { return _EventType ; }
            set { _EventType  = value; }
        }

        private DateTime _EndDate = System.DateTime.Now;
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        private bool _IsActive = false;
        public bool IsActive
        {
            get { return _IsActive; }
            private set { _IsActive = value; }
        }

        public LectureEvent(int LectEvtID, int LectureID, DateTime StartDate, int Capacity, int Attendees, string Comment,
            string Facility, string City, string State, bool Finalized, decimal Hours, string EventType, DateTime EndDate, bool IsActive)
        {
            this.LectEvtID = LectEvtID;
            this.LectureID = LectureID;
            this.StartDate = StartDate;
            this.Capacity = Capacity;
            this.Attendees = Attendees;
            this.Comment = Comment;
            this.Facility = Facility;
            this.City = City;
            this.State = State;
            this.Finalized = Finalized;
            this.Hours = Hours;
            this.EventType = EventType;
            this.EndDate = EndDate;
            this.IsActive = IsActive;
        }

        public bool Delete()
        {
            bool success = LectureEvent.DeleteLectureEvent(this.LectEvtID);
            if (success)
                this.LectEvtID = 0;
            return success;
        }

        public bool Update()
        {
            return LectureEvent.UpdateLectureEvent(this.LectEvtID, this.LectureID, this.StartDate, this.Capacity, this.Attendees, this.Comment,
                this.Facility, this.City, this.State, this.Finalized, this.Hours, this.EventType, this.EndDate,this.IsActive);
        }
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all LectureEvents
        /// </summary>
        public static List<LectureEvent> GetLectureEvents(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "StartDate";

            List<LectureEvent> LectureEvents = null;
            string key = "LectureEvents_LectureEvents_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEvents = (List<LectureEvent>)BizObject.Cache[key];
            }
            else
            {
                List<LectureEventInfo> recordset = SiteProvider.PR2.GetLectureEvents(cSortExpression);
                LectureEvents = GetLectureEventListFromLectureEventInfoList(recordset);
                BasePR.CacheData(key, LectureEvents);
            }
            return LectureEvents;
        }

        public static List<LectureEvent> GetLectureEventsByLectureID(int LectureID)
        {

            List<LectureEvent> LectureEvents = null;
            string key = "LectureEvents_LectureEvents_" + LectureID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEvents = (List<LectureEvent>)BizObject.Cache[key];
            }
            else
            {
                List<LectureEventInfo> recordset = SiteProvider.PR2.GetLectureEventsByLectureID(LectureID);
                LectureEvents = GetLectureEventListFromLectureEventInfoList(recordset);
                BasePR.CacheData(key, LectureEvents);
            }
            return LectureEvents;
        }



        public static DataSet GetAllLectureEvents(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
          //  if (cSortExpression.Length == 0)
           //     cSortExpression = "StartDate";

             DataSet LectureEvents = null;
            string key = "LectureEvents_LectureEvents_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
               // LectureEvents = (DataSet>)BizObject.Cache[key];
            }
            else
            {
                LectureEvents = SiteProvider.PR2.GetAllLectureEvents(cSortExpression);
               // LectureEvents = GetLectureEventListFromLectureEventInfoList(recordset);
                BasePR.CacheData(key, LectureEvents);
            }
            return LectureEvents;
        }

        public static DataSet GetInactiveLectureEvents(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            //  if (cSortExpression.Length == 0)
            //     cSortExpression = "StartDate";

            DataSet LectureEvents = null;
            string key = "LectureEvents_LectureEvents_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                // LectureEvents = (DataSet>)BizObject.Cache[key];
            }
            else
            {
                LectureEvents = SiteProvider.PR2.GetInactiveLectureEvents(cSortExpression);
                // LectureEvents = GetLectureEventListFromLectureEventInfoList(recordset);
                BasePR.CacheData(key, LectureEvents);
            }
            return LectureEvents;
        }

        /// <summary>
        /// Returns the number of total LectureEvents
        /// </summary>
        public static int GetLectureEventCount()
        {
            int LectureEventCount = 0;
            string key = "LectureEvents_LectureEventCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEventCount = (int)BizObject.Cache[key];
            }
            else
            {
                LectureEventCount = SiteProvider.PR2.GetLectureEventCount();
                BasePR.CacheData(key, LectureEventCount);
            }
            return LectureEventCount;
        }

        /// <summary>
        /// Returns a LectureEvent object with the specified ID
        /// </summary>
        public static LectureEvent GetLectureEventByID(int LectEvtID)
        {
            LectureEvent LectureEvent = null;
            string key = "LectureEvents_LectureEvent_" + LectEvtID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEvent = (LectureEvent)BizObject.Cache[key];
            }
            else
            {
                LectureEvent = GetLectureEventFromLectureEventInfo(SiteProvider.PR2.GetLectureEventByID(LectEvtID));
                BasePR.CacheData(key, LectureEvent);
            }
            return LectureEvent;
        }

        /// <summary>
        /// Updates an existing LectureEvent
        /// </summary>
        public static bool UpdateLectureEvent(int LectEvtID, int LectureID, DateTime StartDate, int Capacity, int Attendees, string Comment,
            string Facility, string City, string State, bool Finalized, decimal Hours, string EventType, DateTime EndDate,bool IsActive)
        {
            Comment = BizObject.ConvertNullToEmptyString(Comment);


            LectureEventInfo record = new LectureEventInfo(LectEvtID, LectureID, StartDate, Capacity, Attendees, Comment,
                Facility, City, State, Finalized, Hours, EventType, EndDate,IsActive);
            bool ret = SiteProvider.PR2.UpdateLectureEvent(record);

            BizObject.PurgeCacheItems("LectureEvents_LectureEvent_" + LectEvtID.ToString());
            BizObject.PurgeCacheItems("LectureEvents_LectureEvents");
            return ret;
        }

        /// <summary>
        /// Creates a new LectureEvent
        /// </summary>
        public static int InsertLectureEvent(int LectureID, DateTime StartDate, int Capacity, int Attendees, string Comment,
            string Facility, string City, string State, bool Finalized, decimal Hours, string EventType, DateTime EndDate,bool IsActive)
        {
            Comment = BizObject.ConvertNullToEmptyString(Comment);


            LectureEventInfo record = new LectureEventInfo(0, LectureID, StartDate, Capacity, Attendees, Comment, Facility, City, State, Finalized, Hours, EventType, EndDate,IsActive);
            int ret = SiteProvider.PR2.InsertLectureEvent(record);

            if (ret > 0)
            {
                List<LectureTopicLink> TopicLinks = LectureTopicLink.GetLectureTopicLinksByLectureID(LectureID, "");
                foreach (var model in TopicLinks)
                {
                    LectureEventSequence.InsertLectureEventSequence(0, ret, model.Sequence, model.TopicID, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                }
            }
            BizObject.PurgeCacheItems("LectureEvents_LectureEvent");
            return ret;
        }

        /// <summary>
        /// Deletes an existing LectureEvent, but first checks if OK to delete
        /// </summary>
        public static bool DeleteLectureEvent(int LectEvtID)
        {
            bool IsOKToDelete = OKToDelete(LectEvtID);
            if (IsOKToDelete)
            {
                return (bool)DeleteLectureEvent(LectEvtID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing LectureEvent - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteLectureEvent(int LectEvtID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteLectureEvent(LectEvtID);
            //         new RecordDeletedEvent("LectureEvent", LectEvtID, null).Raise();
            BizObject.PurgeCacheItems("LectureEvents_LectureEvent");
            return ret;
        }



        /// <summary>
        /// Checks to see if a LectureEvent can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int LectEvtID)
        {
            return true;
        }



        /// <summary>
        /// Returns a LectureEvent object filled with the data taken from the input LectureEventInfo
        /// </summary>
        private static LectureEvent GetLectureEventFromLectureEventInfo(LectureEventInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new LectureEvent(record.LectEvtID, record.LectureID, record.StartDate, record.Capacity, record.Attendees, record.Comment,
                    record.Facility, record.City, record.State, record.Finalized, record.Hours, record.EventType, record.EndDate,record.IsActive);
            }
        }

        /// <summary>
        /// Returns a list of LectureEvent objects filled with the data taken from the input list of LectureEventInfo
        /// </summary>
        private static List<LectureEvent> GetLectureEventListFromLectureEventInfoList(List<LectureEventInfo> recordset)
        {
            List<LectureEvent> LectureEvents = new List<LectureEvent>();
            foreach (LectureEventInfo record in recordset)
                LectureEvents.Add(GetLectureEventFromLectureEventInfo(record));
            return LectureEvents;
        }

        public static int CheckForLectureEventTopic(string LectEvtID)
        {
            int ret = SiteProvider.PR2.CheckForLectureEventTopic(LectEvtID);
            return ret;
        }

        #endregion
    }
}
