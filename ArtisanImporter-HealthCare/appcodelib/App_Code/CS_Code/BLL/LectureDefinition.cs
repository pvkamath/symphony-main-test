﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for LectureDefinition
/// </summary>
    public class LectureDefinition : BasePR
    {
        #region Variables and Properties
        private int _LectureID = 0;
        public int LectureID
        {
            get { return _LectureID; }
            protected set { _LectureID = value; }
        }

        private string _LectTitle = "";
        public string LectTitle
        {
            get { return _LectTitle; }
            set { _LectTitle = value; }
        }

        private int _CertID = 0;
        public int CertID
        {
            get { return _CertID; }
            set { _CertID = value; }
        }

        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }


        public LectureDefinition(int LectureID, string LectTitle, int CertID, string Comment)
        {
            this.LectureID = LectureID;
            this.LectTitle = LectTitle;
            this.CertID = CertID;
            this.Comment = Comment;
        }

        public bool Delete()
        {
            bool success = LectureDefinition.DeleteLectureDefinition(this.LectureID);
            if (success)
                this.LectureID = 0;
            return success;
        }

        public bool Update()
        {
            return LectureDefinition.UpdateLectureDefinition(this.LectureID, this.LectTitle, this.CertID, this.Comment);
        }

        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all LectureDefinitions
        /// </summary>
        public static List<LectureDefinition> GetLectureDefinitions(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LectTitle";

            List<LectureDefinition> LectureDefinitions = null;
            string key = "LectureDefinitions_LectureDefinitions_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureDefinitions = (List<LectureDefinition>)BizObject.Cache[key];
            }
            else
            {
                List<LectureDefinitionInfo> recordset = SiteProvider.PR2.GetLectureDefinitions(cSortExpression);
                LectureDefinitions = GetLectureDefinitionListFromLectureDefinitionInfoList(recordset);
                BasePR.CacheData(key, LectureDefinitions);
            }
            return LectureDefinitions;
        }


        /// <summary>
        /// Returns the number of total LectureDefinitions
        /// </summary>
        public static int GetLectureDefinitionCount()
        {
            int LectureDefinitionCount = 0;
            string key = "LectureDefinitions_LectureDefinitionCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureDefinitionCount = (int)BizObject.Cache[key];
            }
            else
            {
                LectureDefinitionCount = SiteProvider.PR2.GetLectureDefinitionCount();
                BasePR.CacheData(key, LectureDefinitionCount);
            }
            return LectureDefinitionCount;
        }

        /// <summary>
        /// Returns a LectureDefinition object with the specified ID
        /// </summary>
        public static LectureDefinition GetLectureDefinitionByID(int LectureID)
        {
            LectureDefinition LectureDefinition = null;
            string key = "LectureDefinitions_LectureDefinition_" + LectureID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureDefinition = (LectureDefinition)BizObject.Cache[key];
            }
            else
            {
                LectureDefinition = GetLectureDefinitionFromLectureDefinitionInfo(SiteProvider.PR2.GetLectureDefinitionByID(LectureID));
                BasePR.CacheData(key, LectureDefinition);
            }
            return LectureDefinition;
        }

        /// <summary>
        /// Updates an existing LectureDefinition
        /// </summary>
        public static bool UpdateLectureDefinition(int LectureID, string LectTitle, int CertID, string Comment)
        {
            LectTitle = BizObject.ConvertNullToEmptyString(LectTitle);
            Comment = BizObject.ConvertNullToEmptyString(Comment);


            LectureDefinitionInfo record = new LectureDefinitionInfo(LectureID, LectTitle, CertID, Comment);
            bool ret = SiteProvider.PR2.UpdateLectureDefinition(record);

            BizObject.PurgeCacheItems("LectureDefinitions_LectureDefinition_" + LectureID.ToString());
            BizObject.PurgeCacheItems("LectureDefinitions_LectureDefinitions");
            return ret;
        }

        /// <summary>
        /// Creates a new LectureDefinition
        /// </summary>
        public static int InsertLectureDefinition(string LectTitle, int CertID, string Comment)
        {
            LectTitle = BizObject.ConvertNullToEmptyString(LectTitle);
            Comment = BizObject.ConvertNullToEmptyString(Comment);


            LectureDefinitionInfo record = new LectureDefinitionInfo(0, LectTitle, CertID, Comment);
            int ret = SiteProvider.PR2.InsertLectureDefinition(record);

            BizObject.PurgeCacheItems("LectureDefinitions_LectureDefinition");
            return ret;
        }

        /// <summary>
        /// Deletes an existing LectureDefinition, but first checks if OK to delete
        /// </summary>
        public static bool DeleteLectureDefinition(int LectureID)
        {
            bool IsOKToDelete = OKToDelete(LectureID);
            if (IsOKToDelete)
            {
                return (bool)DeleteLectureDefinition(LectureID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing LectureDefinition - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteLectureDefinition(int LectureID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteLectureDefinition(LectureID);
            //         new RecordDeletedEvent("LectureDefinition", LectureID, null).Raise();
            BizObject.PurgeCacheItems("LectureDefinitions_LectureDefinition");
            return ret;
        }



        /// <summary>
        /// Checks to see if a LectureDefinition can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int LectureID)
        {
            return true;
        }



        /// <summary>
        /// Returns a LectureDefinition object filled with the data taken from the input LectureDefinitionInfo
        /// </summary>
        private static LectureDefinition GetLectureDefinitionFromLectureDefinitionInfo(LectureDefinitionInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new LectureDefinition(record.LectureID, record.LectTitle, record.CertID, record.Comment);
            }
        }

        /// <summary>
        /// Returns a list of LectureDefinition objects filled with the data taken from the input list of LectureDefinitionInfo
        /// </summary>
        private static List<LectureDefinition> GetLectureDefinitionListFromLectureDefinitionInfoList(List<LectureDefinitionInfo> recordset)
        {
            List<LectureDefinition> LectureDefinitions = new List<LectureDefinition>();
            foreach (LectureDefinitionInfo record in recordset)
                LectureDefinitions.Add(GetLectureDefinitionFromLectureDefinitionInfo(record));
            return LectureDefinitions;
        }

        public static int CheckForLectureTopic(string LectureID)
        {
            int ret = SiteProvider.PR2.CheckForLectureTopic(LectureID);
            return ret;
        }

        #endregion
    }
}
