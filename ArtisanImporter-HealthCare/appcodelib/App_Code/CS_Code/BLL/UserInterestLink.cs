﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 
    /// <summary>
    /// Summary description for UserInterestLink
    /// </summary>
    public class UserInterestLink: BasePR
    {
        #region Variables and Properties

        private int _UserIntID = 0;
        public int UserIntID
        {
            get { return _UserIntID; }
            protected set { _UserIntID = value; }
        }

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private int _InterestID = 0;
        public int InterestID
        {
            get { return _InterestID; }
            set { _InterestID = value; }
        }


        public UserInterestLink(int UserIntID, int UserID, int InterestID)
        {
            this.UserIntID = UserIntID;
            this.UserID = UserID;
            this.InterestID = InterestID;
        }

        public bool Delete()
        {
            bool success = UserInterestLink.DeleteUserInterestLink(this.UserIntID);
            if (success)
                this.UserIntID = 0;
            return success;
        }

        public bool Update()
        {
            return UserInterestLink.UpdateUserInterestLink(this.UserIntID, this.UserID, this.InterestID);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all UserInterestLinks
        /// </summary>
        public static List<UserInterestLink> GetUserInterestLinks(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<UserInterestLink> UserInterestLinks = null;
            string key = "UserInterestLinks_UserInterestLinks_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserInterestLinks = (List<UserInterestLink>)BizObject.Cache[key];
            }
            else
            {
                List<UserInterestLinkInfo> recordset = SiteProvider.PR2.GetUserInterestLinks(cSortExpression);
                UserInterestLinks = GetUserInterestLinkListFromUserInterestLinkInfoList(recordset);
                BasePR.CacheData(key, UserInterestLinks);
            }
            return UserInterestLinks;
        }
        /// <summary>
        /// Returns a collection with all FieldsOfInterest assigned to a User
        /// </summary>
        public static List<UserInterestLink> GetUserInterestLinksByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "UserID";

            List<UserInterestLink> UserInterestLink = null;
            string key = "UserInterestLink_UserInterestLink_UserID_" + UserID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserInterestLink = (List<UserInterestLink>)BizObject.Cache[key];
            }
            else
            {
                List<UserInterestLinkInfo> recordset = SiteProvider.PR2.GetUserInterestLinksByUserID(UserID, cSortExpression);
                UserInterestLink = GetUserInterestLinkListFromUserInterestLinkInfoList(recordset);
                BasePR.CacheData(key, UserInterestLink);
            }
            return UserInterestLink;
        }


        /// <summary>
        /// Returns the number of total UserInterestLinks
        /// </summary>
        public static int GetUserInterestLinkCount()
        {
            int UserInterestLinkCount = 0;
            string key = "UserInterestLinks_UserInterestLinkCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserInterestLinkCount = (int)BizObject.Cache[key];
            }
            else
            {
                UserInterestLinkCount = SiteProvider.PR2.GetUserInterestLinkCount();
                BasePR.CacheData(key, UserInterestLinkCount);
            }
            return UserInterestLinkCount;
        }

        /// <summary>
        /// Returns a UserInterestLink object with the specified ID
        /// </summary>
        public static UserInterestLink GetUserInterestLinkByID(int UserIntID)
        {
            UserInterestLink UserInterestLink = null;
            string key = "UserInterestLinks_UserInterestLink_" + UserIntID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserInterestLink = (UserInterestLink)BizObject.Cache[key];
            }
            else
            {
                UserInterestLink = GetUserInterestLinkFromUserInterestLinkInfo(SiteProvider.PR2.GetUserInterestLinkByID(UserIntID));
                BasePR.CacheData(key, UserInterestLink);
            }
            return UserInterestLink;
        }

             /// <summary>
        /// Updates an existing UserInterestLink
        /// </summary>
        public static bool UpdateUserInterestLink(int UserIntID, int UserID, int InterestID)
        {


            UserInterestLinkInfo record = new UserInterestLinkInfo(UserIntID, UserID, InterestID);
            bool ret = SiteProvider.PR2.UpdateUserInterestLink(record);

            BizObject.PurgeCacheItems("UserInterestLinks_UserInterestLink_" + UserIntID.ToString());
            BizObject.PurgeCacheItems("UserInterestLinks_UserInterestLinks");
            return ret;
        }

        /// <summary>
        /// Creates a new UserInterestLink
        /// </summary>
        public static int InsertUserInterestLink(int UserID, int InterestID)
        {


            UserInterestLinkInfo record = new UserInterestLinkInfo(0, UserID, InterestID);
            int ret = SiteProvider.PR2.InsertUserInterestLink(record);

            BizObject.PurgeCacheItems("UserInterestLinks_UserInterestLink");
            return ret;
        }

        /// <summary>
        /// Deletes an existing UserInterestLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteUserInterestLink(int UserIntID)
        {
            bool IsOKToDelete = OKToDelete(UserIntID);
            if (IsOKToDelete)
            {
                return (bool)DeleteUserInterestLink(UserIntID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing UserInterestLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteAllUserInterestLinksByIID(int iiD)
        {
            bool IsOKToDelete = OKToDelete(iiD);
            if (IsOKToDelete)
            {
                return (bool)DeleteAllUserInterestLinksByiID(iiD, true);
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Deletes an existing UserInterestLink - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteUserInterestLink(int UserIntID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteUserInterestLink(UserIntID);
            //         new RecordDeletedEvent("UserInterestLink", UserIntID, null).Raise();
            BizObject.PurgeCacheItems("UserInterestLinks_UserInterestLink");
            return ret;
        }


        /// <summary>
        /// Deletes an existing UserInterestLink - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteAllUserInterestLinksByiID(int iiD, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteUserInterestLinksByiID(iiD);
            //         new RecordDeletedEvent("UserInterestLink", UserIntID, null).Raise();
            BizObject.PurgeCacheItems("UserInterestLinks_UserInterestLink");
            return ret;
        }


        /// <summary>
        /// Checks to see if a UserInterestLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int UserIntID)
        {
            return true;
        }



        /// <summary>
        /// Returns a UserInterestLink object filled with the data taken from the input UserInterestLinkInfo
        /// </summary>
        private static UserInterestLink GetUserInterestLinkFromUserInterestLinkInfo(UserInterestLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new UserInterestLink(record.UserIntID, record.UserID, record.InterestID);
            }
        }

        /// <summary>
        /// Returns a list of UserInterestLink objects filled with the data taken from the input list of UserInterestLinkInfo
        /// </summary>
        private static List<UserInterestLink> GetUserInterestLinkListFromUserInterestLinkInfoList(List<UserInterestLinkInfo> recordset)
        {
            List<UserInterestLink> UserInterestLinks = new List<UserInterestLink>();
            foreach (UserInterestLinkInfo record in recordset)
                UserInterestLinks.Add(GetUserInterestLinkFromUserInterestLinkInfo(record));
            return UserInterestLinks;
        }

        public static bool UpdateUserInterestAssignments(int UserID, string InterestIDAssignments)
        {
            bool ret = SiteProvider.PR2.UpdateUserInterestAssignments(UserID, InterestIDAssignments);
            // TODO: release cache?
            return ret;
        }


        #endregion
    }
}
