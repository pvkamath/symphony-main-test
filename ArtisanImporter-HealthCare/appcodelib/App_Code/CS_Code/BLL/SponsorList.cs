﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for SponsorList
    /// </summary>
    public class SponsorList : BasePR
    {
        #region Variables and Properties

        private int _SponsorID = 0;
        public int SponsorID
        {
            get { return _SponsorID; }
            protected set { _SponsorID = value; }
        }

        private int _InitiativeID = 0;
        public int InitiativeID
        {
            get { return _InitiativeID; }
            set { _InitiativeID = value; }
        }

        public SponsorList(int SponsorID, int InitiativeID)
        {
            this.SponsorID = SponsorID;
            this._InitiativeID = InitiativeID;
        }
        #endregion

        #region Methods

        public static List<SponsorList> GetSponsorListBySponsorID(int SponsorID)
        {
            List<SponsorList> SponsorLists = null;

            List<SponsorListInfo> recordset = SiteProvider.PR2.GetSponsorListBySponsorID(SponsorID);
            SponsorLists = GetSponsorListListFromGetSponsorListInfoList(recordset);
            return SponsorLists;

        }

        private static List<SponsorList> GetSponsorListListFromGetSponsorListInfoList(List<SponsorListInfo> recordset)
        {
            List<SponsorList> SponsorLists = new List<SponsorList>();
            foreach (SponsorListInfo record in recordset)
                SponsorLists.Add(GetSponsorListFromSponsorList(record));
            return SponsorLists;
        }

        private static SponsorList GetSponsorListFromSponsorList(SponsorListInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new SponsorList(record.SponsorID, record.InitiativeID);
            }
        }

        public static int InsertSponsorList(int SponsorID, int InitiativeID)
        {
            SponsorListInfo record = new SponsorListInfo(InitiativeID, SponsorID);
            int ret = SiteProvider.PR2.InsertSponsorList(record);
            BizObject.PurgeCacheItems("SponsorLists_SponsorList_" + SponsorID.ToString());
            BizObject.PurgeCacheItems("SponsorLists_SponsorLists");
            return ret;
        }


        public static bool DeleteSponsorList(int InitiativeID, int SponsorID)
        {

            bool ret = SiteProvider.PR2.DeleteSponsorList(InitiativeID, SponsorID);
            BizObject.PurgeCacheItems("SponsorLists_SponsorList");
            return ret;
        }
       
        #endregion
    }
}
