﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr1
    /// </summary>

    public class Coupons : BasePR
    {
        public Coupons(int cid, string couponCode, string couponDesc, decimal cAmount, DateTime expireDate, 
            int? facilityID, string couponType, int? topicID, string mediaType, decimal? hours, int? uLimit, int? userID,
            bool oneTimePerUserInd)
        {
            this.CID = cid;
            this.CouponCode = couponCode;
            this.CouponDesc = couponDesc;
            this.CAmount = cAmount;
            this.ExpireDate = expireDate;
            this.FacilityID = facilityID;
            this.CouponType = couponType;
            this.TopicID = topicID;
            this.MediaType = mediaType;
            this.Hours = hours;
            this.ULimit = uLimit;
            this.UserID = userID;

            this.OneTimePerUserInd = oneTimePerUserInd;
        }


        #region Variables and Properties
        private int _CID = 0;
        public int CID
        {
            get { return _CID; }
            set { _CID = value; }
        }

        private string _CouponCode = "";
        public string CouponCode
        {
            get { return _CouponCode; }
            set { _CouponCode = value; }
        }

        private string _CouponDesc = "";
        public string CouponDesc
        {
            get { return _CouponDesc; }
            set { _CouponDesc = value; }
        }

        private decimal _CAmount = 0;
        public decimal CAmount
        {
            get { return _CAmount; }
            set { _CAmount = value; }
        }

        private DateTime _ExpireDate = System.DateTime.MinValue;
        public DateTime ExpireDate
        {
            get { return _ExpireDate; }
            set { _ExpireDate = value; }
        }

        private int? _FacilityID = null;
        public int? FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }

        private string _CouponType = "";
        public string CouponType
        {
            get { return _CouponType; }
            set { _CouponType = value; }
        }

        private int? _TopicID = null;
        public int? TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }


        private string _MediaType = null;
        public string MediaType
        {
            get { return _MediaType; }
            set { _MediaType = value; }
        }

        private decimal? _hours = null;
        public decimal? Hours
        {
            get { return _hours; }
            set { _hours = value; }
        }

        private int? _ulimit = null;
        public int? ULimit
        {
            get { return _ulimit; }
            set { _ulimit = value; }
        }

        private int? _userid = null;
        public int? UserID
        {
            get { return _userid; }
            set { _userid = value; }
        }


        private Boolean _oneTimePerUserInd = false;
        public Boolean OneTimePerUserInd
        {
            get { return _oneTimePerUserInd; }
            set { _oneTimePerUserInd = value; }
        }
        public String UserDesc { get; set; }

        #endregion Variables and Properties

        #region Methods
        public static int insertCoupon(string couponCode, string couponDesc, decimal cAmount, DateTime expireDate,
            int? facilityID, string couponType, int? topicID, string mediaType, decimal? hours, int? uLimit, int? userID, string userDesc,
            Boolean oneTimePerUserInd)
        {
            couponCode = BizObject.ConvertNullToEmptyString(couponCode);
            couponDesc = BizObject.ConvertNullToEmptyString(couponDesc);
            couponType = BizObject.ConvertNullToEmptyString(couponType);
            //mediaType = BizObject.ConvertNullToEmptyString(mediaType);

            CouponsInfo record = new CouponsInfo(0, couponCode, couponDesc, cAmount, expireDate, facilityID, couponType,
                topicID, mediaType, hours, uLimit, userID, oneTimePerUserInd);
            record.UserDesc = userDesc;
            int ret = SiteProvider.PR2.insertCoupon(record);
            BizObject.PurgeCacheItems("Coupons_Coupon_Insert");
            return ret;
        }

        public int insert()
        {
            return insertCoupon(this.CouponCode, this.CouponDesc, this.CAmount, this.ExpireDate, this.FacilityID,
                this.CouponType, this.TopicID, this.MediaType, this.Hours, this.ULimit, this.UserID,this.UserDesc, this.OneTimePerUserInd);
        }

        public static bool updateCoupon(int cid, string couponCode, string couponDesc, decimal cAmount, DateTime expireDate,
            int? facilityID, string couponType, int? topicID, string mediaType, decimal? hours, int? uLimit, int? userID, string userDesc,
            Boolean oneTimePerUserInd)
        {
            couponCode = BizObject.ConvertNullToEmptyString(couponCode);
            couponDesc = BizObject.ConvertNullToEmptyString(couponDesc);
            couponType = BizObject.ConvertNullToEmptyString(couponType);
            //mediaType = BizObject.ConvertNullToEmptyString(mediaType);

            CouponsInfo record = new CouponsInfo(cid, couponCode, couponDesc, cAmount, expireDate, facilityID, couponType,
                topicID, mediaType, hours, uLimit, userID, oneTimePerUserInd);
            record.UserDesc = userDesc;
            bool ret = SiteProvider.PR2.updateCoupon(record);
            BizObject.PurgeCacheItems("Coupons_Coupon_Update" + cid.ToString());
            return ret;
        }

        public bool update()
        {
            return updateCoupon(this.CID, this.CouponCode, this.CouponDesc, this.CAmount, this.ExpireDate, this.FacilityID,
                this.CouponType, this.TopicID, this.MediaType, this.Hours, this.ULimit, this.UserID, this.UserDesc, this.OneTimePerUserInd);
        }

        public bool deleteCoupon(int cid)
        {
            bool ret = SiteProvider.PR2.deleteCoupon(cid);
            BizObject.PurgeCacheItems("Coupons_Coupon_Delete" + cid.ToString());
            return ret;
        }

        public static List<Coupons> GetCouponsBySearchCriteria(string cid, string couponCode, string couponDesc, string facilityID,
            string topicID, string userID, string expireAfter, string orderBy) 
        {
            cid = BizObject.ConvertNullToEmptyString(cid);
            couponCode = BizObject.ConvertNullToEmptyString(couponCode);
            couponDesc = BizObject.ConvertNullToEmptyString(couponDesc);    
            facilityID = BizObject.ConvertNullToEmptyString(facilityID);
            topicID = BizObject.ConvertNullToEmptyString(topicID);
            userID = BizObject.ConvertNullToEmptyString(userID);
            expireAfter = BizObject.ConvertNullToEmptyString(expireAfter);
            orderBy = BizObject.ConvertNullToEmptyString(orderBy);

            List<Coupons> couponList = null;
            string key = String.Format("Coupons_GetCouponsBySearchCriteria_{0}_{1}_{2}_{3}_{4}_{5}_{6}_{7}", cid, couponCode, 
                couponDesc, facilityID, topicID, userID, expireAfter, orderBy);

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                couponList = (List<Coupons>)BizObject.Cache[key];
            }
            else
            {
                List<CouponsInfo> couponInfoList = SiteProvider.PR2.GetCouponsBySearchCriteria(cid, couponCode, couponDesc,
                    facilityID, topicID, userID, expireAfter, orderBy);
                couponList = GetCouponsListfromCouponsInfo(couponInfoList);
                BasePR.CacheData(key, couponList);
            }
            return couponList;
        }

        public static Coupons GetCouponByID(int cid)
        {
            Coupons coupon = null;
            string key = "Coupons_GetCouponByID_" + cid.ToString();
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                coupon = (Coupons)BizObject.Cache[key];
            }
            else
            {
                coupon = GetCouponsFromCouponsInfo(SiteProvider.PR2.GetCouponByID(cid));
                BasePR.CacheData(key, coupon);
            }
            return coupon;
        }

        public static List<Coupons> GetCoupons(string facilityid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";
            List<Coupons> Coup = null;
            string key = "Coup_couponcode" + cSortExpression;
            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Coup = (List<Coupons>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<CouponsInfo> recordset = SiteProvider.PR2.GetCoupons(facilityid, cSortExpression);
                Coup = GetCouponsListfromCouponsInfo(recordset);
                BasePR.CacheData(key, Coup);
            }
            return Coup;
        }

        public static List<Coupons> GetCouponsListfromCouponsInfo(List<CouponsInfo> recordset)
        {

            List<Coupons> coupns = new List<Coupons>();
            foreach (CouponsInfo coupinfo in recordset)
            {
                Coupons coup = GetCouponsFromCouponsInfo(coupinfo);

                coupns.Add(coup);
            }
            return coupns;
        }


        private static Coupons GetCouponsFromCouponsInfo(CouponsInfo coupinfo)
        {
            if (coupinfo == null)
                return null;

            Coupons coup = new Coupons(coupinfo.CID, coupinfo.CouponCode, coupinfo.CouponDesc, 
                coupinfo.CAmount, coupinfo.ExpireDate, coupinfo.FacilityID, coupinfo.CouponType, 
                coupinfo.TopicID, coupinfo.MediaType, coupinfo.Hours, coupinfo.ULimit,
                coupinfo.UserID, coupinfo.OneTimePerUserInd);
            coup.UserDesc = coupinfo.UserDesc;

            return coup;
        }

        public static Coupons GetCouponByFacID(int facilityid)
        {
            List<Coupons> Coupons = null;
            string key = "Coupons_Coupons" + facilityid.ToString();


            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Coupons = (List<Coupons>)BizObject.Cache[key];
            }
            else
            {
                List<CouponsInfo> recordset = new List<CouponsInfo>();
               // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                CouponsInfo recordsetinfo = (SiteProvider.PR2.GetCouponByFacID(facilityid));
                if (recordsetinfo != null)
                {
                    recordset.Add(recordsetinfo);
                    Coupons = GetCouponsListFromCouponsInfoList(recordset);
                    //Orders = obj.GetOrdersByID(OrderID);
                    BasePR.CacheData(key, Coupons);
                }
                else
                {
                    return null;
                }
            }
            return Coupons[0];
        }

        private static List<Coupons> GetCouponsListFromCouponsInfoList(List<CouponsInfo> recordset)
        {
            List<Coupons> Coupons = new List<Coupons>();
            foreach (CouponsInfo record in recordset)
                Coupons.Add(GetCouponsFromCouponsInfo(record));
            return Coupons;
        }

        public static int GetCouponIdByCode(string Code)
        {
            int iCouponid;
          
            iCouponid = SiteProvider.PR2.GetCouponIdByCode(Code);
            return iCouponid;
        }

        public static int GetCouponIdByCode(string Code, int FacilityId)
        {
            return SiteProvider.PR2.GetCouponIdByCode(Code, FacilityId);

        }

        public static Coupons GetCouponByCode(string Code)
        {

            List<Coupons> Coupons = null;
            string key = "Coupons_Coupons" + Code.ToString();


            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Coupons = (List<Coupons>)BizObject.Cache[key];
            }
            else
            {
                List<CouponsInfo> recordset = new List<CouponsInfo>();
                // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                CouponsInfo recordsetinfo = (SiteProvider.PR2.GetCouponByCode(Code));
                if (recordsetinfo != null)
                {
                    recordset.Add(recordsetinfo);
                    Coupons = GetCouponsListFromCouponsInfoList(recordset);
                    //Orders = obj.GetOrdersByID(OrderID);
                    BasePR.CacheData(key, Coupons);
                }
                else
                {
                    return null;
                }
            }
            return Coupons[0];

        }

        public static Coupons GetUCECouponByCode(string Code)
        {

            List<Coupons> Coupons = null;
            string key = "Coupons_Coupons" + Code.ToString();


            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Coupons = (List<Coupons>)BizObject.Cache[key];
            }
            else
            {
                List<CouponsInfo> recordset = new List<CouponsInfo>();
                CouponsInfo recordsetinfo = (SiteProvider.PR2.GetUCECouponByCode(Code));
                if (recordsetinfo != null)
                {
                    recordset.Add(recordsetinfo);
                    Coupons = GetCouponsListFromCouponsInfoList(recordset);                    
                    BasePR.CacheData(key, Coupons);
                }
                else
                {
                    return null;
                }
            }
            return Coupons[0];

        }
        #endregion Methods

    }

}
