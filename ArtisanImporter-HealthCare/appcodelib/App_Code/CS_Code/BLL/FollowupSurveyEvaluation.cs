﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    public class FollowupSurveyEvaluation : BasePR
    {
        #region Variables and Properties

        private int _FSID = 0;
        public int FSID
        {
            get { return _FSID; }
            set { _FSID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private int _TestID = 0;
        public int TestID
        {
            get { return _TestID; }
            set { _TestID = value; }
        }

        private int _QNumber = 0;
        public int QNumber
        {
            get { return _QNumber; }
            set { _QNumber = value; }
        }       

        private DateTime _SurveyDate = DateTime.Now;
        public DateTime SurveyDate
        {
            get { return _SurveyDate; }
            set { _SurveyDate = value; }
        }

        private int _QAnswer = 0;
        public int QAnswer
        {
            get { return _QAnswer; }
            set { _QAnswer = value; }
        }

        #endregion

        #region Methods

        public static int InsertFollowupSurveyEvaluation(int topicID, int testID, DateTime surveyDate, int qNumber, int qAnswer)
        {
            
            int ret = SiteProvider.PR2.InsertFollowupSurveyEvaluation(topicID, testID, surveyDate, qNumber, qAnswer);
            BizObject.PurgeCacheItems("FollowupSurveyEvaluations_FollowupSurveyEvaluation");
            return ret;
        }

        /// <summary>
        /// Delete Followup Survey Evaluation By TestId
        /// </summary>
        public static bool DeleteFollowupSurveyEvaluationByTestId(int TestId)
        {
            bool ret = SiteProvider.PR2.DeleteFollowupSurveyEvaluationByTestId(TestId);
            return ret;
        }

        #endregion Methods
    }
}