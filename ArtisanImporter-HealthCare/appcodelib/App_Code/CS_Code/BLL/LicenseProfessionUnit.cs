﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using PearlsReview.DAL.SQLClient;
using PearlsReview.DAL;
using PearlsReview.BLL.PRBase;
using PearlsReview.BLL;

/// <summary>
/// Summary description for LicenseProfessionUnit
/// </summary>
/// 
namespace PearlsReview.BLL
{
    public class LicenseProfessionUnit : BasePR
    {
        #region Properties

        private int _lpuid = 0;
        private string _lpuDesc = string.Empty;
        private int _license_type_id = 0;
        private int _lpu_msid = 0;
        private string _unitName = string.Empty;
        private string _profTitle = string.Empty;
        private string _license_desc = string.Empty;        

        public int LicenseProfUnitID
        {
            get { return _lpuid; }
            set { _lpuid = value; }
        }

        public int License_Type_ID
        {
            get { return _license_type_id; }
            set { _license_type_id = value; }
        }

        public int License_Profession_MSID
        {
            get { return _lpu_msid; }
            set { _lpu_msid = value; }
        }

        public string UnitName
        {
            get { return _unitName; }
            set { _unitName = value; }
        }

        public string Prof_Title
        {
            get { return _profTitle; }
            set { _profTitle = value; }
        }

        public string License_Desc
        {
            get { return _license_desc; }
            set { _license_desc = value; }
        }

        public string LpuDesc
        {
            get { return _lpuDesc; }
            set { _lpuDesc = value; }
        }

        public LicenseProfessionUnit() { }
        public LicenseProfessionUnit(int pLicenseProfUnitID, int pLicense_Type_ID, int pLicense_Profession_MSID, string pUnitName, string pProf_Title, string pLicense_Desc, string pLpuDesc)
        {
            this.LicenseProfUnitID = pLicenseProfUnitID;
            this.License_Type_ID = pLicense_Type_ID;
            this.License_Profession_MSID = pLicense_Profession_MSID;
            this.UnitName = pUnitName;
            this.Prof_Title = pProf_Title;
            this.License_Desc = pLicense_Desc;
            this.LpuDesc = pLpuDesc;
        }

        public bool Delete()
        {
            bool success = LicenseProfessionUnit.DeleteLicenseProfessionUnit(this.LicenseProfUnitID);
            if (success)
                this.License_Type_ID = 0;
            return success;
        }

        public bool Update()
        {
            return LicenseProfessionUnit.UpdateLicenseProfUnit(this.LicenseProfUnitID, this.License_Type_ID, this.License_Profession_MSID, this.UnitName);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all LicenseTypes
        /// </summary>
        public static List<LicenseProfessionUnit> GetLicenseProfessionUnits(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "UnitName";

            List<LicenseProfessionUnit> LicenseProfessionUnits = null;
            string key = "LicenseProfessionUnits_LicenseProfessionUnits_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LicenseProfessionUnits = (List<LicenseProfessionUnit>)BizObject.Cache[key];
            }
            else
            {
                List<LicenseProfessionUnitInfo> recordset = SiteProvider.PR2.GetLicenseProfUnits(cSortExpression);
                LicenseProfessionUnits = GetLicenseProfUnitListFromLicenseProfUnitInfoList(recordset);
                BasePR.CacheData(key, LicenseProfessionUnits);
            }
            return LicenseProfessionUnits;
        }

        public static List<LicenseProfessionUnit> GetLicenseProfessionUnitsBySearchCriteria(string cSortExpression, int pLicenseTypeID, int pProfessionMsID, string pUnitName)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "UnitName";

            List<LicenseProfessionUnit> LicenseProfessionUnits = null;
            string key = "LicenseProfessionUnits_LicenseProfessionUnits_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LicenseProfessionUnits = (List<LicenseProfessionUnit>)BizObject.Cache[key];
            }
            else
            {
                List<LicenseProfessionUnitInfo> recordset = SiteProvider.PR2.GetLicenseProfUnits(cSortExpression, pLicenseTypeID, pProfessionMsID, pUnitName);
                LicenseProfessionUnits = GetLicenseProfUnitListFromLicenseProfUnitInfoList(recordset);
                BasePR.CacheData(key, LicenseProfessionUnits);
            }
            return LicenseProfessionUnits;
        }


        /// <summary>
        /// Returns the number of total LicenseProfessionUnit
        /// </summary>
        public static int GetLicenseProfessionUnitCount()
        {
            int Count = 0;
            string key = "LicenseProfUnits_LicenseProfUnits";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Count = (int)BizObject.Cache[key];
            }
            else
            {
                Count = SiteProvider.PR2.GetLicenseProfUnitCount();
                BasePR.CacheData(key, Count);
            }
            return Count;
        }

        /// <summary>
        /// Returns a LicenseProfessionUnit object with the specified ID
        /// </summary>
        public static LicenseProfessionUnit GetLicenseProfessionUnitByID(int pLicenseProfUnitID)
        {
            LicenseProfessionUnit licenseProfessionUnit = null;
            string key = "LicenseProfUnits_LicenseProfUnits" + pLicenseProfUnitID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                licenseProfessionUnit = (LicenseProfessionUnit)BizObject.Cache[key];
            }
            else
            {
                licenseProfessionUnit = GetLicenseProfUnitFromLicenseProfUnitInfo(SiteProvider.PR2.GetLicenseProfUnitByID(pLicenseProfUnitID));
                BasePR.CacheData(key, licenseProfessionUnit);
            }
            return licenseProfessionUnit;
        }

        /// <summary>
        /// Updates an existing LicenseProfessionUnit
        /// </summary>
        public static bool UpdateLicenseProfUnit(int original_LicenseProfUnitID, int License_Type_ID, int License_Profession_MSID, string UnitName)
        {
            UnitName = BizObject.ConvertNullToEmptyString(UnitName);

            LicenseProfessionUnitInfo record = new LicenseProfessionUnitInfo(original_LicenseProfUnitID, License_Type_ID, License_Profession_MSID, UnitName);
            bool ret = SiteProvider.PR2.UpdateLicenseProfessionUnit(record);

            BizObject.PurgeCacheItems("LicenseProfUnits_LicenseProfUnits" + original_LicenseProfUnitID.ToString());
            BizObject.PurgeCacheItems("LicenseProfUnits_LicenseProfUnits");
            return ret;
        }

        /// <summary>
        /// Creates a new LicenseProfessionUnit
        /// </summary>
        public static int InsertLicenseProfessionUnit(int pLicense_Type_ID, int pLicense_Profession_MSID, string pUnitName)
        {
            pUnitName = BizObject.ConvertNullToEmptyString(pUnitName);

            LicenseProfessionUnitInfo record = new LicenseProfessionUnitInfo(0, pLicense_Type_ID, pLicense_Profession_MSID, pUnitName);
            int ret = SiteProvider.PR2.InsertLicenseProfessionUnit(record);

            BizObject.PurgeCacheItems("LicenseProfUnits_LicenseProfUnits");
            return ret;
        }

        /// <summary>
        /// Deletes an existing LicenseProfessionUnit, but first checks if OK to delete
        /// </summary>
        public static bool DeleteLicenseProfessionUnit(int original_LicenseProfUnitID)
        {
            bool IsOKToDelete = OKToDelete(original_LicenseProfUnitID);
            if (IsOKToDelete)
            {
                return (bool)DeleteLicenseProfessionUnit(original_LicenseProfUnitID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing LicenseProfessionUnit - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteLicenseProfessionUnit(int pLicenseProfUnitID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteLicenseProfessionUnit(pLicenseProfUnitID);            
            BizObject.PurgeCacheItems("LicenseProfUnits_LicenseProfUnits");
            return ret;
        }



        /// <summary>
        /// Checks to see if a LicenseProfessionUnit can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int pLicenseProfUnitID)
        {
            return true;
        }



        /// <summary>
        /// Returns a LicenseProfessionUnit object filled with the data taken from the input LicenseProfessionUnitInfo
        /// </summary>
        private static LicenseProfessionUnit GetLicenseProfUnitFromLicenseProfUnitInfo(LicenseProfessionUnitInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new LicenseProfessionUnit(record.LicenseProfUnitID, record.License_Type_ID, record.License_Profession_MSID, record.UnitName, record.Prof_Title, record.License_Desc, record.LpuDesc);
            }
        }

        /// <summary>
        /// Returns a list of LicenseProfessionUnit objects filled with the data taken from the input list of LicenseProfessionUnitInfo
        /// </summary>
        private static List<LicenseProfessionUnit> GetLicenseProfUnitListFromLicenseProfUnitInfoList(List<LicenseProfessionUnitInfo> recordset)
        {
            List<LicenseProfessionUnit> LicenseProfessionUnits = new List<LicenseProfessionUnit>();
            foreach (LicenseProfessionUnitInfo record in recordset)
                LicenseProfessionUnits.Add(GetLicenseProfUnitFromLicenseProfUnitInfo(record));
            return LicenseProfessionUnits;
        }

        #endregion
    }
}