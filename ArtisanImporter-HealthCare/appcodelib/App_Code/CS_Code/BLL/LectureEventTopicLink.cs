﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;


namespace PearlsReview.BLL
{

    ////////////////////////////////////////////////////////////
    /// <summary>
    /// LectureEventTopicLink business object class
    /// </summary>
    public class LectureEventTopicLink : BasePR
    {

        #region variables and properties

        private int _LectEvtTopID = 0;
        public int LectEvtTopID
        {
            get { return _LectEvtTopID; }
            protected set { _LectEvtTopID = value; }
        }

        private int _LectEvtID = 0;
        public int LectEvtID
        {
            get { return _LectEvtID; }
            set { _LectEvtID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        #endregion

        #region Methods

        public LectureEventTopicLink(int LectEvtTopID, int LectEvtID, int TopicID)
        {
            this.LectEvtTopID = LectEvtTopID;
            this.LectEvtID = LectEvtID;
            this.TopicID = TopicID;
            
        }

        public bool Delete()
        {
            bool success = LectureEventTopicLink.DeleteLectureEventTopicLink(this.LectEvtTopID);
            if (success)
                this.LectEvtTopID = 0;
            return success;
        }

        public bool Update()
        {
            return LectureEventTopicLink.UpdateLectureEventTopicLink(this.LectEvtTopID, this.LectEvtID, this.TopicID);
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all LectureEventTopicLinks
        /// </summary>
        public static List<LectureEventTopicLink> GetLectureEventTopicLinks(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<LectureEventTopicLink> LectureEventTopicLinks = null;
            string key = "LectureEventTopicLinks_LectureEventTopicLinks_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEventTopicLinks = (List<LectureEventTopicLink>)BizObject.Cache[key];
            }
            else
            {
                List<LectureEventTopicLinkInfo> recordset = SiteProvider.PR2.GetLectureEventTopicLinks(cSortExpression);
                LectureEventTopicLinks = GetLectureEventTopicLinkListFromLectureEventTopicLinkInfoList(recordset);
                BasePR.CacheData(key, LectureEventTopicLinks);
            }
            return LectureEventTopicLinks;
        }


        /// <summary>
        /// Returns the number of total LectureEventTopicLinks
        /// </summary>
        public static int GetLectureEventTopicLinkCount()
        {
            int LectureEventTopicLinkCount = 0;
            string key = "LectureEventTopicLinks_LectureEventTopicLinkCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEventTopicLinkCount = (int)BizObject.Cache[key];
            }
            else
            {
                LectureEventTopicLinkCount = SiteProvider.PR2.GetLectureEventTopicLinkCount();
                BasePR.CacheData(key, LectureEventTopicLinkCount);
            }
            return LectureEventTopicLinkCount;
        }

        /// <summary>
        /// Returns a LectureEventTopicLink object with the specified ID
        /// </summary>
        public static LectureEventTopicLink GetLectureEventTopicLinkByID(int LectEvtTopID)
        {
            LectureEventTopicLink LectureEventTopicLink = null;
            string key = "LectureEventTopicLinks_LectureEventTopicLink_" + LectEvtTopID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureEventTopicLink = (LectureEventTopicLink)BizObject.Cache[key];
            }
            else
            {
                LectureEventTopicLink = GetLectureEventTopicLinkFromLectureEventTopicLinkInfo(SiteProvider.PR2.GetLectureEventTopicLinkByID(LectEvtTopID));
                BasePR.CacheData(key, LectureEventTopicLink);
            }
            return LectureEventTopicLink;
        }

        /// <summary>
        /// Updates an existing LectureEventTopicLink
        /// </summary>
        public static bool UpdateLectureEventTopicLink(int LectEvtTopID, int LectEvtID, int TopicID)
        {


            LectureEventTopicLinkInfo record = new LectureEventTopicLinkInfo(LectEvtTopID, LectEvtID, TopicID);
            bool ret = SiteProvider.PR2.UpdateLectureEventTopicLink(record);

            BizObject.PurgeCacheItems("LectureEventTopicLinks_LectureEventTopicLink_" + LectEvtTopID.ToString());
            BizObject.PurgeCacheItems("LectureEventTopicLinks_LectureEventTopicLinks");
            return ret;
        }

        /// <summary>
        /// Creates a new LectureEventTopicLink
        /// </summary>
        public static int InsertLectureEventTopicLink(int LectEvtID, int TopicID)
        {


            LectureEventTopicLinkInfo record = new LectureEventTopicLinkInfo(0, LectEvtID, TopicID);
            int ret = SiteProvider.PR2.InsertLectureEventTopicLink(record);

            BizObject.PurgeCacheItems("LectureEventTopicLinks_LectureEventTopicLink");
            return ret;
        }

        /// <summary>
        /// Deletes an existing LectureEventTopicLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteLectureEventTopicLink(int LectEvtTopID)
        {
            bool IsOKToDelete = OKToDelete(LectEvtTopID);
            if (IsOKToDelete)
            {
                return (bool)DeleteLectureEventTopicLink(LectEvtTopID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing LectureEventTopicLink - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteLectureEventTopicLink(int LectEvtTopID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteLectureEventTopicLink(LectEvtTopID);
            //         new RecordDeletedEvent("LectureEventTopicLink", LectEvtTopID, null).Raise();
            BizObject.PurgeCacheItems("LectureEventTopicLinks_LectureEventTopicLink");
            return ret;
        }

        public static bool DeleteLectureEventTopicLinkByTopicID(int topicID)
        {
            bool ret = SiteProvider.PR2.DeleteLectureEventTopicLinkByTopicID(topicID);
            //         new RecordDeletedEvent("LectureEventTopicLink", LectEvtTopID, null).Raise();
            BizObject.PurgeCacheItems("LectureEventTopicLinks_LectureEventTopicLink");
            return ret;
        }
     
        /// <summary>
        /// Checks to see if a LectureEventTopicLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int LectEvtTopID)
        {
            return true;
        }



        /// <summary>
        /// Returns a LectureEventTopicLink object filled with the data taken from the input LectureEventTopicLinkInfo
        /// </summary>
        private static LectureEventTopicLink GetLectureEventTopicLinkFromLectureEventTopicLinkInfo(LectureEventTopicLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new LectureEventTopicLink(record.LectEvtTopID, record.LectEvtID, record.TopicID);
            }
        }

        /// <summary>
        /// Returns a list of LectureEventTopicLink objects filled with the data taken from the input list of LectureEventTopicLinkInfo
        /// </summary>
        private static List<LectureEventTopicLink> GetLectureEventTopicLinkListFromLectureEventTopicLinkInfoList(List<LectureEventTopicLinkInfo> recordset)
        {
            List<LectureEventTopicLink> LectureEventTopicLinks = new List<LectureEventTopicLink>();
            foreach (LectureEventTopicLinkInfo record in recordset)
                LectureEventTopicLinks.Add(GetLectureEventTopicLinkFromLectureEventTopicLinkInfo(record));
            return LectureEventTopicLinks;
        }

        #endregion

    }
}