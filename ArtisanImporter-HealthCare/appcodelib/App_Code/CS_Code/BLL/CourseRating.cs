﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for CourseRating
    /// </summary>
    public class CourseRating : BasePR
    {
        #region Variables and Properties

        private int _crid = 0;
        public int crid
        {
            get { return _crid; }
            protected set { _crid = value; }
        }

        private int _topicid = 0;
        public int topicid
        {
            get { return _topicid; }
            set { _topicid = value; }
        }
        private int _userid = 0;
        public int userid
        {
            get { return _userid; }
            set { _userid = value; }
        }

        private DateTime _ratingdate = System.DateTime.Now;
        public DateTime ratingdate
        {
            get { return _ratingdate; }
            set { _ratingdate = value; }
        }

        private int _rating = 5;
        public int rating
        {
            get { return _rating; }
            set { _rating = value; }
        }

        private bool _active_ind = true;
        public bool active_ind
        {
            get { return _active_ind; }
            set { _active_ind = value; }
        }

        private string _comment = "";
        public string comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        private bool _recommend_ind = true;
        public bool recommend_ind
        {
            get { return _recommend_ind; }
            set { _recommend_ind = value; }
        }

        private int _helpful_yes = 0;
        public int helpful_yes
        {
            get { return _helpful_yes; }
            set { _helpful_yes = value; }
        }

        private int _helpful_no = 0;
        public int helpful_no
        {
            get { return _helpful_no; }
            set { _helpful_no = value; }
        }

        private string _location = "";
        public string location
        {
            get { return _location; }
            set { _location = value; }
        }

        private string _flag_status = "I";
        public string flag_status
        {
            get { return _flag_status; }
            set { _flag_status = value; }
        }

        private string _editor_response = "";
        public string editor_response
        {
            get { return _editor_response; }
            set { _editor_response = value; }
        }


        private int _editorid = 0;
        public int editorid
        {
            get { return _editorid; }
            set { _editorid = value; }
        }
        private DateTime _editor_cdate;
        public DateTime editor_cdate
        {
            get { return _editor_cdate; }
            set { _editor_cdate = value; }
        }

        private int _flagid = 0;
        public int flagid
        {
            get { return _flagid; }
            set { _flagid = value; }
        }
        #endregion Variables and Properties

        #region Methods

        public CourseRating() { }
        public CourseRating(int crid, int topicid, int userid, DateTime ratingdate, int rating, bool active_ind, string comment,
            bool recommend_ind, int helpful_yes, int helpful_no, string location, string flag_status, string editor_response, 
            int editorid, DateTime editor_cdate, int flagid)
        {
            this.crid = crid;
            this.topicid = topicid;
            this.userid = userid;
            this.ratingdate = ratingdate;
            this.rating = rating;
            this.active_ind = active_ind;
            this.comment = comment;
            this.recommend_ind = recommend_ind;
            this.helpful_yes = helpful_yes;
            this.helpful_no = helpful_no;
            this.location = location;
            this.flag_status = flag_status;
            this.editor_response = editor_response;
            this.editorid = editorid;
            this.editor_cdate = editor_cdate;
            this.flagid = flagid;
        }
        public bool Insert()
        {
            return CourseRating.InsertCourseRating(this.topicid, this.userid, this.ratingdate, this.rating, this.active_ind, this.comment,
             this.recommend_ind, this.helpful_yes, this.helpful_no, this.location, this.flag_status, this.editor_response, this.editorid, this.editor_cdate, this.flagid) > 0;
        }
        public bool Delete()
        {
            bool success = CourseRating.DeleteCourseRating(this.crid);
            if (success)
                this.crid = 0;
            return success;
        }

        public bool Update()
        {
            return CourseRating.UpdateCourseRating(this.crid, this.topicid, this.userid, this.ratingdate, this.rating, this.active_ind, this.comment,
             this.recommend_ind, this.helpful_yes, this.helpful_no, this.location, this.flag_status, this.editor_response, this.editorid, this.editor_cdate, this.flagid);
        }

        /***********************************
        * Static methods
        ************************************/


        /// <summary>
        /// Returns a collection with all CourseRating
        /// </summary>
        public static List<CourseRating> GetCourseRating(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "topicid";

            List<CourseRating> CourseRating = null;
            string key = "CourseRating_CourseRating_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CourseRating = (List<CourseRating>)BizObject.Cache[key];
            }
            else
            {
                List<CourseRatingInfo> recordset = SiteProvider.PR2.GetCourseRating(cSortExpression);
                CourseRating = GetCourseRatingListFromCourseRatingInfoList(recordset);
                BasePR.CacheData(key, CourseRating);
            }
            return CourseRating;
        }
        public static DataTable GetCourseRatingSearchList(string coursenumber, string topicname, string flag_status, string active_ind, string startdate, string enddate, string cSortExpression)
        {
            return SiteProvider.PR2.GetCourseRatingSearchList(coursenumber, topicname, flag_status, active_ind, startdate, enddate, cSortExpression);
        }
        /// <summary>
        /// Returns a CourseRating object with the specified ID
        /// </summary>
        public static CourseRating GetCourseRatingByID(int crid)
        {
            CourseRating CourseRating = null;
            string key = "CourseRating_CourseRating_" + crid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CourseRating = (CourseRating)BizObject.Cache[key];
            }
            else
            {
                CourseRating = GetCourseRatingFromCourseRatingInfo(SiteProvider.PR2.GetCourseRatingByID(crid));
                BasePR.CacheData(key, CourseRating);
            }
            return CourseRating;
        }
        public static DataTable GetCourseRatingDTByID(int crid)
        {
            return SiteProvider.PR2.GetCourseRatingDTByID(crid);
        }

        /// <summary>
        /// Get Discount Id based On Topic Id
        /// </summary>
        public static int GetSumCourseRatingByTopicId(int TopicId)
        {
            int ret = SiteProvider.PR2.GetSumCourseRatingByTopicId(TopicId);
            return ret;
        }

        /// <summary>
        /// Returns a collection with all CourseRating related to the topicId
        /// </summary>
        public static List<CourseRating> GetCourseRatingByTopicId(int topicid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "rating";

            List<CourseRating> CourseRating = null;
            string key = "CourseRating_GetCourseRatingByTopicId_" + topicid.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CourseRating = (List<CourseRating>)BizObject.Cache[key];
            }
            else
            {
                List<CourseRatingInfo> recordset = SiteProvider.PR2.GetCourseRatingByTopicID(topicid, cSortExpression);
                CourseRating = GetCourseRatingListFromCourseRatingInfoList(recordset);
                BasePR.CacheData(key, CourseRating);
            }
            return CourseRating;
        }

        /// <summary>
        /// Returns a collection with all CourseRating related to the topicId and comment
        /// </summary>
        public static List<CourseRating> GetCourseRatingByTopicIDAndComment(int topicid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "rating";

            List<CourseRating> CourseRating = null;
            string key = "CourseRating_GetCourseRatingByTopicIdAndComment_" + topicid.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CourseRating = (List<CourseRating>)BizObject.Cache[key];
            }
            else
            {
                List<CourseRatingInfo> recordset = SiteProvider.PR2.GetCourseRatingByTopicIDAndComment(topicid, cSortExpression);
                CourseRating = GetCourseRatingListFromCourseRatingInfoList(recordset);
                BasePR.CacheData(key, CourseRating);
            }
            return CourseRating;
        }

        public static List<CourseRating> GetCourseRatingByTopicIDAndUserID(int topicid, int userid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "rating";

            List<CourseRating> CourseRating = null;
            string key = "CourseRating_GetCourseRatingByTopicIDAndUserID_" + topicid.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CourseRating = (List<CourseRating>)BizObject.Cache[key];
            }
            else
            {
                List<CourseRatingInfo> recordset = SiteProvider.PR2.GetCourseRatingByTopicIDAndUserID(topicid, userid, cSortExpression);
                CourseRating = GetCourseRatingListFromCourseRatingInfoList(recordset);
                BasePR.CacheData(key, CourseRating);
            }
            return CourseRating;
        }
        /// <summary>
        /// Creates a new CourseRating
        /// </summary>
        public static int InsertCourseRating(int topicid, int userid, DateTime ratingdate, int rating, bool active_ind, string comment,
            bool recommend_ind, int helpful_yes, int helpful_no, string location, string flag_status, string editor_response, int editorid, DateTime editor_cdate, int flagid)
        {


            CourseRatingInfo record = new CourseRatingInfo(0, topicid, userid, ratingdate, rating, active_ind, comment,
             recommend_ind, helpful_yes, helpful_no, location, flag_status, editor_response, editorid, editor_cdate, flagid);
            int ret = SiteProvider.PR2.InsertCourseRating(record);

            BizObject.PurgeCacheItems("CourseRating_CourseRating");
            return ret;
        }


        /// <summary>
        /// Updates an existing CourseRating
        /// </summary>
        public static bool UpdateCourseRating(int crid, int topicid, int userid, DateTime ratingdate, int rating, bool active_ind, string comment,
            bool recommend_ind, int helpful_yes, int helpful_no, string location, string flag_status, string editor_response, int editorid, DateTime editor_cdate, int flagid)
        {

            bool ret = false;
            CourseRatingInfo record = new CourseRatingInfo(crid, topicid, userid, ratingdate, rating, active_ind, comment,
             recommend_ind, helpful_yes, helpful_no, location, flag_status, editor_response, editorid, editor_cdate, flagid);
            ret = SiteProvider.PR2.UpdateCourseRating(record);

            BizObject.PurgeCacheItems("CourseRating_CourseRating_" + crid.ToString());
            BizObject.PurgeCacheItems("CourseRating_CourseRating");
            return ret;
        }

        /// <summary>
        /// Deletes an existing CourseRating, but first checks if OK to delete
        /// </summary>
        public static bool DeleteCourseRating(int crid)
        {
            bool IsOKToDelete = OKToDelete(crid);
            if (IsOKToDelete)
            {
                return (bool)DeleteCourseRating(crid, true);
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Deletes an existing CourseRating - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteCourseRating(int FacID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteCourseRating(FacID);
            //         new RecordDeletedEvent("CourseRating", FacID, null).Raise();
            BizObject.PurgeCacheItems("CourseRating_CourseRating");
            return ret;
        }



        /// <summary>
        /// Checks to see if a CourseRating can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int FacID)
        {
            return true;
        }



        /// <summary>
        /// Returns a CourseRating object filled with the data taken from the input CourseRatingInfo
        /// </summary>
        private static CourseRating GetCourseRatingFromCourseRatingInfo(CourseRatingInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new CourseRating(record.crid, record.topicid, record.userid, record.ratingdate, record.rating, record.active_ind, record.comment,
             record.recommend_ind, record.helpful_yes, record.helpful_no, record.location, record.flag_status, record.editor_response, record.editorid, record.editor_cdate, record.flagid);
            }
        }

        /// <summary>
        /// Returns a list of CourseRating objects filled with the data taken from the input list of CourseRatingInfo
        /// </summary>
        private static List<CourseRating> GetCourseRatingListFromCourseRatingInfoList(List<CourseRatingInfo> recordset)
        {
            List<CourseRating> CourseRating = new List<CourseRating>();
            foreach (CourseRatingInfo record in recordset)
                CourseRating.Add(GetCourseRatingFromCourseRatingInfo(record));
            return CourseRating;
        }
        #endregion Methods
    }
}
