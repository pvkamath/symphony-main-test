﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 
/// <summary>
/// Summary description for FieldOfInterest
/// </summary>
    public class FieldOfInterest : BasePR
    {
        #region Variables and Properties
        private int _InterestID = 0;
        public int InterestID
        {
            get { return _InterestID; }
            protected set { _InterestID = value; }
        }

        private string _IntDescr = "";
        public string IntDescr
        {
            get { return _IntDescr; }
            set { _IntDescr = value; }
        }



        public FieldOfInterest(int InterestID, string IntDescr)
        {
            this.InterestID = InterestID;
            this.IntDescr = IntDescr;
        }

        public bool Delete()
        {
            bool success = FieldOfInterest.DeleteFieldOfInterest(this.InterestID);
            if (success)
                this.InterestID = 0;
            return success;
        }

        public bool Update()
        {
            return FieldOfInterest.UpdateFieldOfInterest(this.InterestID, this.IntDescr);
        }
        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all FieldsOfInterest
        /// </summary>
        public static List<FieldOfInterest> GetFieldsOfInterest(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "IntDescr";

            List<FieldOfInterest> FieldsOfInterest = null;
            string key = "FieldsOfInterest_FieldsOfInterest_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FieldsOfInterest = (List<FieldOfInterest>)BizObject.Cache[key];
            }
            else
            {
                List<FieldOfInterestInfo> recordset = SiteProvider.PR2.GetFieldsOfInterest(cSortExpression);
                FieldsOfInterest = GetFieldOfInterestListFromFieldOfInterestInfoList(recordset);
                BasePR.CacheData(key, FieldsOfInterest);
            }
            return FieldsOfInterest;
        }


        /// <summary>
        /// Returns the number of total FieldsOfInterest
        /// </summary>
        public static int GetFieldOfInterestCount()
        {
            int FieldOfInterestCount = 0;
            string key = "FieldsOfInterest_FieldOfInterestCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FieldOfInterestCount = (int)BizObject.Cache[key];
            }
            else
            {
                FieldOfInterestCount = SiteProvider.PR2.GetFieldOfInterestCount();
                BasePR.CacheData(key, FieldOfInterestCount);
            }
            return FieldOfInterestCount;
        }

        /// <summary>
        /// Returns a FieldOfInterest object with the specified ID
        /// </summary>
        public static FieldOfInterest GetFieldOfInterestByID(int InterestID)
        {
            FieldOfInterest FieldOfInterest = null;
            string key = "FieldsOfInterest_FieldOfInterest_" + InterestID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FieldOfInterest = (FieldOfInterest)BizObject.Cache[key];
            }
            else
            {
                FieldOfInterest = GetFieldOfInterestFromFieldOfInterestInfo(SiteProvider.PR2.GetFieldOfInterestByID(InterestID));
                BasePR.CacheData(key, FieldOfInterest);
            }
            return FieldOfInterest;
        }

        /// <summary>
        /// Updates an existing FieldOfInterest
        /// </summary>
        public static bool UpdateFieldOfInterest(int InterestID, string IntDescr)
        {
            IntDescr = BizObject.ConvertNullToEmptyString(IntDescr);


            FieldOfInterestInfo record = new FieldOfInterestInfo(InterestID, IntDescr);
            bool ret = SiteProvider.PR2.UpdateFieldOfInterest(record);

            BizObject.PurgeCacheItems("FieldsOfInterest_FieldOfInterest_" + InterestID.ToString());
            BizObject.PurgeCacheItems("FieldsOfInterest_FieldsOfInterest");
            return ret;
        }

        /// <summary>
        /// Creates a new FieldOfInterest
        /// </summary>
        public static int InsertFieldOfInterest(string IntDescr)
        {
            IntDescr = BizObject.ConvertNullToEmptyString(IntDescr);


            FieldOfInterestInfo record = new FieldOfInterestInfo(0, IntDescr);
            int ret = SiteProvider.PR2.InsertFieldOfInterest(record);

            BizObject.PurgeCacheItems("FieldsOfInterest_FieldOfInterest");
            return ret;
        }

        /// <summary>
        /// Deletes an existing FieldOfInterest, but first checks if OK to delete
        /// </summary>
        public static bool DeleteFieldOfInterest(int InterestID)
        {
            bool IsOKToDelete = OKToDelete(InterestID);
            if (IsOKToDelete)
            {
                return (bool)DeleteFieldOfInterest(InterestID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing FieldOfInterest - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteFieldOfInterest(int InterestID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteFieldOfInterest(InterestID);
            //         new RecordDeletedEvent("FieldOfInterest", InterestID, null).Raise();
            BizObject.PurgeCacheItems("FieldsOfInterest_FieldOfInterest");
            return ret;
        }



        /// <summary>
        /// Checks to see if a FieldOfInterest can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int InterestID)
        {
            return true;
        }

        /// <summary>
        /// Returns a collection with all FieldsOfInterest assigned to a User
        /// </summary>
        public static List<FieldOfInterest> GetFieldsOfInterestByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "IntDescr";

            List<FieldOfInterest> FieldsOfInterest = null;
            string key = "FieldsOfInterest_FieldsOfInterest_UserID_" + UserID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                FieldsOfInterest = (List<FieldOfInterest>)BizObject.Cache[key];
            }
            else
            {
                List<FieldOfInterestInfo> recordset = SiteProvider.PR2.GetFieldsOfInterestByUserID(UserID, cSortExpression);
                FieldsOfInterest = GetFieldOfInterestListFromFieldOfInterestInfoList(recordset);
                BasePR.CacheData(key, FieldsOfInterest);
            }
            return FieldsOfInterest;
        }



        /// <summary>
        /// Returns a FieldOfInterest object filled with the data taken from the input FieldOfInterestInfo
        /// </summary>
        private static FieldOfInterest GetFieldOfInterestFromFieldOfInterestInfo(FieldOfInterestInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new FieldOfInterest(record.InterestID, record.IntDescr);
            }
        }

        /// <summary>
        /// Returns a list of FieldOfInterest objects filled with the data taken from the input list of FieldOfInterestInfo
        /// </summary>
        private static List<FieldOfInterest> GetFieldOfInterestListFromFieldOfInterestInfoList(List<FieldOfInterestInfo> recordset)
        {
            List<FieldOfInterest> FieldsOfInterest = new List<FieldOfInterest>();
            foreach (FieldOfInterestInfo record in recordset)
                FieldsOfInterest.Add(GetFieldOfInterestFromFieldOfInterestInfo(record));
            return FieldsOfInterest;
        }

        //public static bool UpdateUserInterestAssignments(int UserID, string InterestIDAssignments)
        //{
        //    bool ret = SiteProvider.PR2.UpdateUserInterestAssignments(UserID, InterestIDAssignments);
        //    // TODO: release cache?
        //    return ret;
        //}
        #endregion
    }
}

