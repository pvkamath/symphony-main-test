﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for Advertisement
/// </summary>
    public class Advertisement : BasePR
    {
        #region Variables and Properties
        private int _AdID = 0;
        public int AdID
        {
            get { return _AdID; }
            protected set { _AdID = value; }
        }

        private string _AdName = "";
        public string AdName
        {
            get { return _AdName; }
            set { _AdName = value; }
        }

        private string _AdType = "";
        public string AdType
        {
            get { return _AdType; }
            set { _AdType = value; }
        }

        private string _AdDest = "";
        public string AdDest
        {
            get { return _AdDest; }
            set { _AdDest = value; }
        }

        private string _AdText = "";
        public string AdText
        {
            get { return _AdText; }
            set { _AdText = value; }
        }

        private string _AdFilename = "";
        public string AdFilename
        {
            get { return _AdFilename; }
            set { _AdFilename = value; }
        }

        private string _AdUnitType = "";
        public string AdUnitType
        {
            get { return _AdUnitType; }
            set { _AdUnitType = value; }
        }


        public Advertisement(int AdID, string AdName, string AdType, string AdDest, string AdText, string AdFilename, string AdUnitType)
        {
            this.AdID = AdID;
            this.AdName = AdName;
            this.AdType = AdType;
            this.AdDest = AdDest;
            this.AdText = AdText;
            this.AdFilename = AdFilename;
            this.AdUnitType = AdUnitType;
        }

        public bool Delete()
        {
            bool success = Advertisement.DeleteAdvertisement(this.AdID);
            if (success)
                this.AdID = 0;
            return success;
        }

        public bool Update()
        {
            return Advertisement.UpdateAdvertisement(this.AdID, this.AdName, this.AdType, this.AdDest, this.AdText, this.AdFilename, this.AdUnitType);
        }

        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Advertisements
        /// </summary>
        public static List<Advertisement> GetAdvertisements(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "AdName";

            List<Advertisement> Advertisements = null;
            string key = "Advertisements_Advertisements_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Advertisements = (List<Advertisement>)BizObject.Cache[key];
            }
            else
            {
                List<AdvertisementInfo> recordset = SiteProvider.PR2.GetAdvertisements(cSortExpression);
                Advertisements = GetAdvertisementListFromAdvertisementInfoList(recordset);
                BasePR.CacheData(key, Advertisements);
            }
            return Advertisements;
        }


        /// <summary>
        /// Returns the number of total Advertisements
        /// </summary>
        public static int GetAdvertisementCount()
        {
            int AdvertisementCount = 0;
            string key = "Advertisements_AdvertisementCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AdvertisementCount = (int)BizObject.Cache[key];
            }
            else
            {
                AdvertisementCount = SiteProvider.PR2.GetAdvertisementCount();
                BasePR.CacheData(key, AdvertisementCount);
            }
            return AdvertisementCount;
        }

        /// <summary>
        /// Returns a Advertisement object with the specified ID
        /// </summary>
        public static Advertisement GetAdvertisementByID(int AdID)
        {
            Advertisement Advertisement = null;
            string key = "Advertisements_Advertisement_" + AdID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Advertisement = (Advertisement)BizObject.Cache[key];
            }
            else
            {
                Advertisement = GetAdvertisementFromAdvertisementInfo(SiteProvider.PR2.GetAdvertisementByID(AdID));
                BasePR.CacheData(key, Advertisement);
            }
            return Advertisement;
        }

        /// <summary>
        /// Updates an existing Advertisement
        /// </summary>
        public static bool UpdateAdvertisement(int AdID, string AdName, string AdType, string AdDest, string AdText, string AdFilename, string AdUnitType)
        {
            AdName = BizObject.ConvertNullToEmptyString(AdName);
            AdType = BizObject.ConvertNullToEmptyString(AdType);
            AdDest = BizObject.ConvertNullToEmptyString(AdDest);
            AdText = BizObject.ConvertNullToEmptyString(AdText);
            AdFilename = BizObject.ConvertNullToEmptyString(AdFilename);
            AdUnitType = BizObject.ConvertNullToEmptyString(AdUnitType);


            AdvertisementInfo record = new AdvertisementInfo(AdID, AdName, AdType, AdDest, AdText, AdFilename, AdUnitType);
            bool ret = SiteProvider.PR2.UpdateAdvertisement(record);

            BizObject.PurgeCacheItems("Advertisements_Advertisement_" + AdID.ToString());
            BizObject.PurgeCacheItems("Advertisements_Advertisements");
            return ret;
        }

        /// <summary>
        /// Creates a new Advertisement
        /// </summary>
        public static int InsertAdvertisement(string AdName, string AdType, string AdDest, string AdText, string AdFilename, string AdUnitType)
        {
            AdName = BizObject.ConvertNullToEmptyString(AdName);
            AdType = BizObject.ConvertNullToEmptyString(AdType);
            AdDest = BizObject.ConvertNullToEmptyString(AdDest);
            AdText = BizObject.ConvertNullToEmptyString(AdText);
            AdFilename = BizObject.ConvertNullToEmptyString(AdFilename);
            AdUnitType = BizObject.ConvertNullToEmptyString(AdUnitType);


            AdvertisementInfo record = new AdvertisementInfo(0, AdName, AdType, AdDest, AdText, AdFilename, AdUnitType);
            int ret = SiteProvider.PR2.InsertAdvertisement(record);

            BizObject.PurgeCacheItems("Advertisements_Advertisement");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Advertisement, but first checks if OK to delete
        /// </summary>
        public static bool DeleteAdvertisement(int AdID)
        {
            bool IsOKToDelete = OKToDelete(AdID);
            if (IsOKToDelete)
            {
                return (bool)DeleteAdvertisement(AdID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Advertisement - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteAdvertisement(int AdID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteAdvertisement(AdID);
            //         new RecordDeletedEvent("Advertisement", AdID, null).Raise();
            BizObject.PurgeCacheItems("Advertisements_Advertisement");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Advertisement can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int AdID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Advertisement object filled with the data taken from the input AdvertisementInfo
        /// </summary>
        private static Advertisement GetAdvertisementFromAdvertisementInfo(AdvertisementInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Advertisement(record.AdID, record.AdName, record.AdType, record.AdDest, record.AdText, record.AdFilename, record.AdUnitType);
            }
        }

        /// <summary>
        /// Returns a list of Advertisement objects filled with the data taken from the input list of AdvertisementInfo
        /// </summary>
        private static List<Advertisement> GetAdvertisementListFromAdvertisementInfoList(List<AdvertisementInfo> recordset)
        {
            List<Advertisement> Advertisements = new List<Advertisement>();
            foreach (AdvertisementInfo record in recordset)
                Advertisements.Add(GetAdvertisementFromAdvertisementInfo(record));
            return Advertisements;
        }

        #endregion
    }
}
