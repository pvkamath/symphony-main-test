﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using System.Data.SqlClient;



namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for MockTestDefinition
/// </summary>
public class MockTestDefinition :BasePR
{
	#region Variables and Properties

        private int _ID = 0;
        public int ID
        {
            get { return _ID; }
            protected set { _ID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }

        private string _XMLTest = "";
        public string XMLTest
        {
            get { return _XMLTest; }
            set { _XMLTest = value; }
        }

  


        public MockTestDefinition(int ID, int TopicID, string XMLTest)
        {
            this.ID = ID;
            this.TopicID = TopicID;
            this.XMLTest = XMLTest;
        }
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all TestDefinitions
        /// </summary>
        //public static List<TestDefinition> GetTestDefinitions(string cSortExpression)
        //{
        //    if (cSortExpression == null)
        //        cSortExpression = "";

        //    // provide default sort
        //    if (cSortExpression.Length == 0)
        //        cSortExpression = "Version";

        //    List<TestDefinition> TestDefinitions = null;
        //    string key = "TestDefinitions_TestDefinitions_" + cSortExpression.ToString();

        //    if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
        //    {
        //        TestDefinitions = (List<TestDefinition>)BizObject.Cache[key];
        //    }
        //    else
        //    {
        //        List<TestDefinitionInfo> recordset = SiteProvider.PR2.GetTestDefinitions(cSortExpression);
        //        TestDefinitions = GetTestDefinitionListFromTestDefinitionInfoList(recordset);
        //        BasePR.CacheData(key, TestDefinitions);
        //    }
        //    return TestDefinitions;
        //}


        /// <summary>
        /// Returns a TestDefinition object with the specified ID
        /// </summary>
        //public static TestDefinition GetTestDefinitionByID(int TestDefID)
        //{
        //    TestDefinition TestDefinition = null;
        //    string key = "TestDefinitions_TestDefinition_" + TestDefID.ToString();

        //    if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
        //    {
        //        TestDefinition = (TestDefinition)BizObject.Cache[key];
        //    }
        //    else
        //    {
        //        TestDefinition = GetTestDefinitionFromTestDefinitionInfo(SiteProvider.PR2.GetTestDefinitionByID(TestDefID));
        //        BasePR.CacheData(key, TestDefinition);
        //    }
        //    return TestDefinition;
        //}

        /// <summary>
        /// Returns a TestDefinition object associated with the specified TopicID
        /// </summary>
        //public static TestDefinition GetTestDefinitionByTopicID(int TopicID)
        //{
        //    TestDefinition TestDefinition = null;
        //    string key = "TestDefinitions_TestDefinition_TopicID_" + TopicID.ToString();

        //    if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
        //    {
        //        TestDefinition = (TestDefinition)BizObject.Cache[key];
        //    }
        //    else
        //    {
        //        TestDefinition = GetTestDefinitionFromTestDefinitionInfo(SiteProvider.PR2.GetTestDefinitionByTopicID(TopicID));
        //        BasePR.CacheData(key, TestDefinition);
        //    }
        //    return TestDefinition;
        //}

        /// <summary>
        /// Updates an existing TestDefinition
        /// </summary>
        public static bool UpdateTestDefinition(int MockTestDefnID, int TopicID, string XMLTest)
        {
            XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);
            MockTestDefinitionInfo record = new MockTestDefinitionInfo(MockTestDefnID, TopicID, XMLTest);
            bool ret = SiteProvider.PR2.UpdateMockTestDefinition(record);
            return ret;
        }
    

        /// <summary>
        /// Creates a new TestDefinition
        /// </summary>
        public static int InsertTestDefinition(int TopicID, string XMLTest)
        {
            XMLTest = BizObject.ConvertNullToEmptyString(XMLTest);

            int ret = SiteProvider.PR2.InsertMockTestDefn(TopicID,XMLTest);

            return ret;
        }
        public static MockTestDefinition GetMockTestDefinitionIDByTopicID(int TopicID)
        {
            MockTestDefinitionInfo info = SiteProvider.PR2.GetMockTestDefinitionIDByTopicID(TopicID);
            if (info == null)
                return null;
            else
            {
                return new MockTestDefinition(info.ID, info.TopicID, info.XMLTest);
            }
        }


    
  
        #endregion
   
}
}


namespace PearlsReview.DAL.SQLClient
{
    public class MockTestDefinitionInfo
    {
        public MockTestDefinitionInfo() { }

        public MockTestDefinitionInfo(int ID, int TopicID, string XMLTest)
        {
            this.ID = ID;
            this.TopicID = TopicID;
            this.XMLTest = XMLTest;
        }

        private int _ID = 0;
        public int ID
        {
            get { return _ID; }
            protected set { _ID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            private set { _TopicID = value; }
        }

        private string _XMLTest = "";
        public string XMLTest
        {
            get { return _XMLTest; }
            private set { _XMLTest = value; }
        }
    }

    public partial class SQL2PRProvider : DataAccess
    {
         public int InsertMockTestDefn(int TopicID, string XML)
    {
         using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("delete FROM KangXMLTest WHERE [TopicID] = @TopicID "+             
             
             "insert into KangXMLTest " +
              "(TopicID, " +
              "XMLTest) " +
              "VALUES (" +
              "@TopicID, " +
              "@XMLTest) SET @ID = SCOPE_IDENTITY()", cn);

                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cmd.Parameters.Add("@XMLTest", SqlDbType.VarChar).Value = XML;

                SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
                IDParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(IDParameter);

                cn.Open();
                cmd.ExecuteNonQuery();

                int NewID = (int)IDParameter.Value;
                return NewID;
            }    
    }
         public MockTestDefinitionInfo GetMockTestDefinitionIDByTopicID(int TopicID)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {

                SqlCommand cmd = new SqlCommand("select * " +
                        "from KangXMLTest where TopicID=@TopicID", cn);
                cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TopicID;
                cn.Open();
                IDataReader reader = ExecuteReader(cmd, CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    MockTestDefinitionInfo TDI = new MockTestDefinitionInfo(
              (int)reader["ID"],
              (int)reader["TopicID"],
              reader["XMLTest"].ToString());
                    return TDI;
                }
                else
                    return null;
            }
        }
         public bool UpdateMockTestDefinition(MockTestDefinitionInfo TestDefinition)
         {
             using (SqlConnection cn = new SqlConnection(this.ConnectionString))
             {
                 SqlCommand cmd = new SqlCommand("update KangXMLTest set " +
               "TopicID = @TopicID, " +
               "XMLTest = @XMLTest " +
               "where ID = @ID ", cn);
                 //            cmd.CommandType = CommandType.StoredProcedure;
                 cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = TestDefinition.TopicID;
                 cmd.Parameters.Add("@XMLTest", SqlDbType.VarChar).Value = TestDefinition.XMLTest;
                 cmd.Parameters.Add("@ID", SqlDbType.Int).Value = TestDefinition.ID;

                 cn.Open();
                 int ret = ExecuteNonQuery(cmd);
                 return (ret == 1);
             }
         }        


         public DataTable LoadMigrationData()
         {
             
             using (SqlConnection conn = new SqlConnection("Server=ghg-va-lmssql;Database=Nurse;User=lmsuser;Password=lmsuser;"))
             {
                 string query = "SELECT" +
                     " item as TopicID" +
                     ",Answer as CorrectAns" +
                     ",Questions as Answers" +
                     ",QNum as QNumber" +
                     ",DisplayQuestion as Question" +
                     ",TestName as TestName" +
                     " FROM" +
                     " dbo.nd_questions";
                 SqlCommand cmd = new SqlCommand(query, conn);
                 DataTable table = new DataTable();
                 conn.Open();

                 SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                 adapter.Fill(table);                 
                 return table;
             }
         }
         public DataTable GetTopicIDsByCourseNos(List<string> coursenumbers)
         {
             using (SqlConnection conn = new SqlConnection(this.ConnectionString))
             {
                 string query = "SELECT" +
                     " topicid,item as course_number" +
                     " FROM" +
                     " dbo.fakecoursemap" +
                     " where item in ("; 
                 foreach (string item in coursenumbers)
                 {
                     query = query + "'" + item + "',";
                 }
                 query = query.Remove(query.Length - 1)+")";
                 
                 SqlCommand cmd = new SqlCommand(query, conn);
                
                 conn.Open();
                 DataTable table = new DataTable();
                 SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                 adapter.Fill(table);
                 return table;
             }
         }
    }
}



