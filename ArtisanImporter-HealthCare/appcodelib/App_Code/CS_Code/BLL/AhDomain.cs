﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

/// <summary>
/// Summary description for AhDomain
/// </summary>
namespace PearlsReview.BLL
{
    public class AhDomain : BasePR
    {
        #region  Variables and Properties

        private int _ad_id = 0;
        public int Ad_id
        {
            get { return _ad_id; }
            protected set { _ad_id = value; }
        }

        private string _ad_name = "";
        public string Ad_name
        {
            get { return _ad_name; }
            protected set { _ad_name = value; }
        }

        private string _ad_domainname = "";
        public string Ad_domainname
        {
            get { return _ad_domainname; }
            protected set { _ad_domainname = value; }
        }

        private string _ad_fullurl = "";
        public string Ad_fullurl
        {
            get { return _ad_fullurl; }
            protected set { _ad_fullurl = value; }
        }

        private string _ad_defaultpage = "";
        public string Ad_defaultpage
        {
            get { return _ad_defaultpage; }
            protected set { _ad_defaultpage = value; }
        }

        private string _ad_logo = "";
        public string Ad_logo
        {
            get { return _ad_logo; }
            protected set { _ad_logo = value; }
        }

        private string _ad_largeimage = "";
        public string Ad_largeimage
        {
            get { return _ad_largeimage; }
            protected set { _ad_largeimage = value; }
        }

        private int _ad_default_licensetype = 0;
        public int Ad_default_licensetype
        {
            get { return _ad_default_licensetype; }
            protected set { _ad_default_licensetype = value; }
        }

        private bool _single_prof_ind;
        public bool Single_prof_ind
        {
            get { return _single_prof_ind; }
            protected set { _single_prof_ind = value; }
        }

        private string _primary_prof_url = "";
        public string Primary_prof_url
        {
            get { return _primary_prof_url; }
            protected set { _primary_prof_url = value; }
        }

        private string _contact_phone = "";
        public string Contact_phone
        {
            get { return _contact_phone; }
            protected set { _contact_phone = value; }
        }

        private string _contact_email = "";
        public string Contact_email
        {
            get { return _contact_email; }
            protected set { _contact_email = value; }
        }

        private string _contact_helptext = "";
        public string Contact_helptext
        {
            get { return _contact_helptext; }
            protected set { _contact_helptext = value; }
        }

        private string _contact_helppage = "";
        public string Contact_helppage
        {
            get { return _contact_helppage; }
            protected set { _contact_helppage = value; }
        }

        private string _ad_header = "";
        public string Ad_header
        {
            get { return _ad_header; }
            protected set { _ad_header = value; }
        }

        private string _ad_footer = "";
        public string Ad_footer
        {
            get { return _ad_footer; }
            protected set { _ad_footer = value; }
        }

      

        public AhDomain()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public AhDomain(int ad_id,string ad_name,string ad_domainname,string ad_fullurl,string ad_defaultpage,string ad_logo,
            string ad_largeimage,int ad_default_licensetype,bool single_prof_ind,string primary_prof_url,string contact_phone,
            string contact_email,string contact_helptext,string contact_helppage,string ad_header,string ad_footer)
        {
            this.Ad_id = ad_id;
            this.Ad_name = ad_name;
            this.Ad_domainname = ad_domainname;
            this.Ad_fullurl = ad_fullurl;
            this.Ad_defaultpage = ad_defaultpage;
            this.Ad_logo = ad_logo;
            this.Ad_largeimage = ad_largeimage;
            this.Ad_default_licensetype = ad_default_licensetype;
            this.Single_prof_ind = single_prof_ind;
            this.Primary_prof_url = primary_prof_url;
            this.Contact_phone = contact_phone;
            this.Contact_email = contact_email;
            this.Contact_helptext = contact_helptext;
            this.Contact_helppage = contact_helppage;
            this.Ad_header = ad_header;
            this.Ad_footer = ad_footer;
        }
        public bool Delete()
        {
            bool success = AhDomain.DeleteAhDomain(this.Ad_id);
            if (success)
                this.Ad_id = 0;
            return success;
        }

        public bool Update()
        {
            return AhDomain.UpdateAhDomain(this.Ad_id,this.Ad_name,this.Ad_domainname,this.Ad_fullurl,this.Ad_defaultpage,this.Ad_logo,
                this.Ad_largeimage,this.Ad_default_licensetype,this.Single_prof_ind,this.Primary_prof_url,this.Contact_phone,
                this.Contact_email,this.Contact_helptext,this.Contact_helppage,this.Ad_header,this.Ad_footer);
        }
        #endregion  Variables and Properties
        #region Methods
        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all AhDomains
        //</summary>
        public static List<AhDomain> GetAhDomain(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "ad_name";

            List<AhDomain> AhDomains = null;
            string key = "AhDomain_AhDomain_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AhDomains = (List<AhDomain>)BizObject.Cache[key];
            }
            else
            {
                List<AhDomainInfo> recordset = SiteProvider.PR2.GetAhDomain(cSortExpression);
                AhDomains = GetAhDomainListFromAhDomainInfoList(recordset);
                BasePR.CacheData(key, AhDomains);
            }
            return AhDomains;
        }


        /// <summary>
        /// Returns a AhDomains object with the specified ID
        /// </summary>
        public static List<AhDomain> GetAhDomainByID(int Ad_id)
        {
            List<AhDomain> AhDomains = null;
           
                       
            AhDomains = GetAhDomainListFromAhDomainInfoList(SiteProvider.PR2.GetAhDomainByID(Ad_id));
                
           
            return AhDomains;
        }

        public static AhDomain GetAhDomainByAdID(string  Ad_id)
        {
            AhDomain AhDomains = null;


            AhDomains = GetAhDomainFromAhDomainInfo(SiteProvider.PR2.GetAhDomainByAdID(Ad_id));


            return AhDomains;
        }


        public static AhDomain GetAhDomainIDByName(string DomainName)
        {
            AhDomain AhDomains = null;
            string key = "AhDomains_AhDomains_" + DomainName.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AhDomains = (AhDomain)BizObject.Cache[key];
            }
            else
            {
                AhDomains = GetAhDomainFromAhDomainInfo(SiteProvider.PR2.GetAhDomainIDByName(DomainName));
                BasePR.CacheData(key, AhDomains);
            }
            return AhDomains;
        }
      



        #endregion




        /// <summary>
        /// Updates an existing AhDomain
        /// </summary>
        public static bool UpdateAhDomain(int ad_id, string ad_name, string ad_domainname, string ad_fullurl, string ad_defaultpage, string ad_logo,
            string ad_largeimage, int ad_default_licensetype, bool single_prof_ind, string primary_prof_url, string contact_phone,
            string contact_email, string contact_helptext, string contact_helppage, string ad_header, string ad_footer)
        {
            ad_name = BizObject.ConvertNullToEmptyString(ad_name);
            ad_fullurl = BizObject.ConvertNullToEmptyString(ad_fullurl);
            ad_fullurl = BizObject.ConvertNullToEmptyString(ad_fullurl);
            ad_defaultpage = BizObject.ConvertNullToEmptyString(ad_defaultpage);
            ad_logo = BizObject.ConvertNullToEmptyString(ad_logo);
            ad_largeimage = BizObject.ConvertNullToEmptyString(ad_largeimage);
            primary_prof_url = BizObject.ConvertNullToEmptyString(primary_prof_url);
            contact_phone = BizObject.ConvertNullToEmptyString(contact_phone);
            contact_email = BizObject.ConvertNullToEmptyString(contact_email);
            contact_helptext = BizObject.ConvertNullToEmptyString(contact_helptext);
            contact_helppage = BizObject.ConvertNullToEmptyString(contact_helppage);
            ad_header = BizObject.ConvertNullToEmptyString(ad_header);
            ad_footer = BizObject.ConvertNullToEmptyString(ad_footer);

            AhDomainInfo record = new AhDomainInfo(ad_id, ad_name, ad_domainname, ad_fullurl,ad_defaultpage, ad_logo,
                ad_largeimage, ad_default_licensetype, single_prof_ind, primary_prof_url, contact_phone,
                contact_email, contact_helptext, contact_helppage, ad_header, ad_footer);
            bool ret = SiteProvider.PR2.UpdateAhDomain(record);

            BizObject.PurgeCacheItems("AhDomains_AhDomains_" + ad_id.ToString());
            BizObject.PurgeCacheItems("AhDomain_AhDomains");
            return ret;
        }


        /// <summary>
        /// Deletes an existing AhDomain, but first checks if OK to delete
        /// </summary>
        public static bool DeleteAhDomain(int Ad_id)
        {
            bool IsOKToDelete = OKToDelete(Ad_id);
            if (IsOKToDelete)
            {
                return (bool)DeleteAhDomain(Ad_id, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing ahDomain - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteAhDomain(int Ad_id, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteAhDomain(Ad_id);
            BizObject.PurgeCacheItems("AhDomain_AhDomain");
            return ret;
        }
        /// <summary>
        /// Checks to see if a AhDomain can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }



        private static List<AhDomain> GetAhDomainListFromAhDomainInfoList(List<AhDomainInfo> recordset)
        {
            List<AhDomain> AhDomains = new List<AhDomain>();
            foreach (AhDomainInfo record in recordset)
                AhDomains.Add(GetAhDomainFromAhDomainInfo(record));
            return AhDomains;
        }
        private static AhDomain GetAhDomainFromAhDomainInfo(AhDomainInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new AhDomain(record.Ad_id, record.Ad_name, record.Ad_domainname, record.Ad_fullurl, record.Ad_defaultpage,
                    record.Ad_logo, record.Ad_largeimage, record.Ad_default_licensetype, record.Single_prof_ind, record.Primary_prof_url, record.Contact_phone, record.Contact_email,
                    record.Contact_helptext, record.Contact_helppage, record.Ad_header, record.Ad_footer);
            }
        }

    }


 

}


