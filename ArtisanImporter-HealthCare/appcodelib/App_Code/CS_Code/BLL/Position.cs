﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{  
    /// <summary>
    /// Summary description for pr2
    /// </summary>
    public class Position : BasePR
    {
        #region Variables and Properties
        
        private int _PositionID = 0;
		public int PositionID
		{
			get { return _PositionID; }
			set { _PositionID = value; }
		}

        private int _FacID = 0;
        public int FacID
        {
            get { return _FacID; }
            set { _FacID = value; }
        }
        
        private string _Position_name = "";
		public string Position_name
		{
			get { return _Position_name; }
			set { _Position_name = value; }
		}

        private bool _Position_active_ind = true;
        public bool  Position_active_ind
        {
            get { return _Position_active_ind; }
            set { _Position_active_ind = value; }
        }

        #endregion

        #region Methods
        public Position(int PositionID, int FacID, string Position_name, bool Position_active_ind)
      {
			this.PositionID = PositionID;
            this.FacID = FacID;
			this.Position_name = Position_name;
            this.Position_active_ind = Position_active_ind;
      }

        public bool Delete()
        {
            bool success = Position.DeletePosition(this.PositionID);
            if (success)
                this.PositionID = 0;
            return success;
        }

        public bool Update()
        {
            return Position.UpdatePosition(this.PositionID, this.FacID,this.Position_name,this.Position_active_ind);
        }

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Positions
        /// </summary>
        public static List<Position> GetPositions(string fac_id,string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "position_name";

            List<Position> Positions = null;
            string key = "Position_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Positions = (List<Position>)BizObject.Cache[key];
            }
            else
            {            
            List<PositionInfo> recordset = SiteProvider.PR2.GetPositions(fac_id, cSortExpression);
                            Positions = GetPositionListFromPositionInfoList(recordset);
                            BasePR.CacheData(key, Positions);
            }
            return Positions;
        }  


        /// <summary>
        /// Returns the number of total Positions
        /// </summary>
        public static int GetPositionCount(int FacID)
        {
            int PositionCount = 0;
            string key = "Position_Count";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                PositionCount = (int)BizObject.Cache[key];
            }
            else
            {            
            PositionCount = SiteProvider.PR2.GetPositionCount(FacID);
            BasePR.CacheData(key, PositionCount);
            }
            return PositionCount;
        }

        /// <summary>
        /// Returns a Position object with the specified ID
        /// </summary>
        public static Position GetPositionByID(int PositionID)
        {
            Position Position = null;
            string key = "Positions_" + PositionID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Position = (Position)BizObject.Cache[key];
            }
            else
            {            
            Position = GetPositionFromPositionInfo(SiteProvider.PR2.GetPositionByID(PositionID));
            BasePR.CacheData(key, Position);
            }
            return Position;
        }

        /// <summary>
        /// Updates an existing Position
        /// </summary>
        public static bool UpdatePosition(int PositionID,int FacID, string Position_name,bool Position_active_ind)
        {
			Position_name = BizObject.ConvertNullToEmptyString(Position_name);


            PositionInfo record = new PositionInfo(PositionID, FacID, Position_name,Position_active_ind);            
            bool ret = SiteProvider.PR2.UpdatePosition(record);

            BizObject.PurgeCacheItems("Positions_" + PositionID.ToString());
            BizObject.PurgeCacheItems("Positions_Positions");
            return ret;
        }

        /// <summary>
        /// Creates a new Position
        /// </summary>
        public static int InsertPosition(int FacID,string Position_name,bool Position_active_ind)
        {
			Position_name = BizObject.ConvertNullToEmptyString(Position_name);


            PositionInfo record = new PositionInfo(0, FacID,Position_name,Position_active_ind);            
            int ret = SiteProvider.PR2.InsertPosition(record);

            BizObject.PurgeCacheItems("Positions_Position");
            return ret;
        }

        /// <summary>
        /// Deletes an existing Position, but first checks if OK to delete
        /// </summary>
        public static bool DeletePosition(int PositionID)
        {
            bool IsOKToDelete = OKToDelete(PositionID);
            if (IsOKToDelete)
            {
                return (bool)DeletePosition(PositionID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing Position - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeletePosition(int PositionID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;
            
            bool ret = SiteProvider.PR2.DeletePosition(PositionID);
            //new RecordDeletedEvent("Position", PositionID, null).Raise();
            BizObject.PurgeCacheItems("Positions_Position");
            return ret;
        }



        /// <summary>
        /// Checks to see if a Position can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int PositionID)
        {
            return true;
        }


        private static Position GetPositionFromPositionInfo(PositionInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Position(record.PositionID, record.FacID,record.Position_name,record.Position_active_ind);
            }
        }

        /// <summary>
        /// Returns a list of Position objects filled with the data taken from the input list of PositionInfo
        /// </summary>
        private static List<Position> GetPositionListFromPositionInfoList(List<PositionInfo> recordset)
        {
            List<Position> Positions = new List<Position>();
            foreach (PositionInfo record in recordset)
                Positions.Add(GetPositionFromPositionInfo(record));
            return Positions;
        }
     #endregion

    }

    
}
