﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using System.Collections.Generic;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for UserProfessionInterest
    /// </summary>
    /// 

    public class UserProfessionInterest : BasePR
    {
        #region Variables and Properties

        private int _userpiid = 0;
        public int userpiid
        {
            get { return _userpiid; }
            protected set { _userpiid = value; }
        }
        public int _userdspid = 0;
        public int userdspid
        {
            get { return _userdspid; }
            set { _userdspid = value; }
        }
        public int _miid = 0;
        public int miid
        {
            get { return _miid; }
            set { _miid = value; }
        }
        
        public UserProfessionInterest(int userpiid, int userdspid, int miid)
        {
            this.userpiid = userpiid;
            this.userdspid = userdspid;
            this.miid = miid;          
        }

        # endregion

        #region Methods


        /// <summary>
        /// Creates a new UserProfessionInterest
        /// </summary>
        public static int InsertUserProfessionInterest(int userdspid, int miid)
        {

            UserProfessionInterestInfo record = new UserProfessionInterestInfo(0, userdspid, miid);
            int ret = SiteProvider.PR2.InsertUserProfessionInterest(record);
            BizObject.PurgeCacheItems("UserProfessionInterests_UserProfessionInterest");
            return ret;
        }


        /// <summary>
        /// Updates an existing UserProfessionInterest
        /// </summary>
        public static bool UpdateUserProfessionInterest(int userpiid, int userdspid, int miid)
        {
            UserProfessionInterestInfo record = new UserProfessionInterestInfo(userpiid, userdspid, miid);
            bool ret = SiteProvider.PR2.UpdateUserProfessionInterest(record);
            BizObject.PurgeCacheItems("UserProfessionInterest" + userpiid.ToString());
            return ret;
        }


        /// <summary>
        /// Returns a collection with all UserProfessionInterests for the specified userdspid
        /// </summary>
        public static List<UserProfessionInterest> GetUserProfessionInterestsByUserdspid(int userdspid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

           

            List<UserProfessionInterest> UserProfessionInterests = null;
            string key = "UserProfessionInterests_UserProfessionInterestInfo_" + userdspid.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserProfessionInterests = (List<UserProfessionInterest>)BizObject.Cache[key];
            }
            else
            {
                List<UserProfessionInterestInfo> recordset = SiteProvider.PR2.GetUserProfessionInterestsByUserdspid(userdspid, cSortExpression);
                UserProfessionInterests = GetUserProfessionInterestListFromUserProfessionInterestInfoList(recordset);
                BasePR.CacheData(key, UserProfessionInterests);
            }
            return UserProfessionInterests;
        }


        /// <summary>
        /// Returns a UserProfessionInterest object filled with the data taken from the input UserProfessionInterestInfo
        /// </summary>
        private static UserProfessionInterest GetUserProfessionInterestFromUserProfessionInterestInfo(UserProfessionInterestInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new UserProfessionInterest(record.userpiid, record.userdspid, record.miid);
            }
        }

        /// <summary>
        /// Returns a list of UserProfessionInterest objects filled with the data taken from the input list of UserProfessionInterestInfo
        /// </summary>
        private static List<UserProfessionInterest> GetUserProfessionInterestListFromUserProfessionInterestInfoList(List<UserProfessionInterestInfo> recordset)
        {
            List<UserProfessionInterest> UserProfessionInterests = new List<UserProfessionInterest>();
            foreach (UserProfessionInterestInfo record in recordset)
                UserProfessionInterests.Add(GetUserProfessionInterestFromUserProfessionInterestInfo(record));
            return UserProfessionInterests;
        }


        /// <summary>
        /// Deletes an existing UserProfessionInterestId
        /// </summary>
        public static bool DeleteAllUserProfessionInterestsByuserdspid(int userdspid)
        {
            bool ret = SiteProvider.PR2.DeleteAllUserProfessionInterestsByUserdspid(userdspid);
            return ret;
        }

        # endregion

    }
}
