﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for CategoryMicrositeLink
    /// </summary>
    public class CategoryMicrositeLink : BasePR
    {
        #region Variables and Properties

        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            protected set { _msid = value; }
        }
        private int _categoryid = 0;
        public int Categoryid
        {
            get { return _categoryid; }
            private set { _categoryid = value; }
        }
        
        public CategoryMicrositeLink()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public CategoryMicrositeLink(int msid, int categoryid)
        {
            this.Msid = msid;
            this.Categoryid = categoryid;
        }
        public bool Delete()
        {
            bool success = CategoryMicrositeLink.DeleteCategoryMicrositeLink(this.Msid, this.Categoryid);
            if (success)
                this.Msid = 0;
            return success;
        }

        public bool Update()
        {
            return CategoryMicrositeLink.UpdateCategoryMicrositeLink(this.Msid, this.Categoryid);
        }

        #endregion
        #region Methods
        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all MicrositeDomains
        //</summary>
        public static List<CategoryMicrositeLink> GetCategoryMicrositeLink(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "categoryid";

            List<CategoryMicrositeLink> CategoryMicrositeLinks = null;
            string key = "CategoryMicrositeLinks_CategoryMicrositeLinks_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CategoryMicrositeLinks = (List<CategoryMicrositeLink>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryMicrositeLinkInfo> recordset = SiteProvider.PR2.GetCategoryMicrositeLink(cSortExpression);
                CategoryMicrositeLinks = GetCategoryMicrositeLinkListFromCategoryMicrositeLinkInfoList(recordset);
                BasePR.CacheData(key, CategoryMicrositeLinks);
            }
            return CategoryMicrositeLinks;
        }

        /// <summary>
        /// Returns a MicrositeDomains object with the specified ID
        /// </summary>
        public static CategoryMicrositeLink GetCategoryMicrositeLinkByID(int Msid, int categoryid)
        {
            CategoryMicrositeLink CategoryMicrositeLinks = null;
            string key = "CategoryMicrositeLinks_CategoryMicrositeLinks_" + Msid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CategoryMicrositeLinks = (CategoryMicrositeLink)BizObject.Cache[key];
            }
            else
            {
                CategoryMicrositeLinks = GetCategoryMicrositeLinkFromCategoryMicrositeLinkInfo(SiteProvider.PR2.GetCategoryMicrositeLinkByID(Msid, categoryid));
                BasePR.CacheData(key, CategoryMicrositeLinks);
            }
            return CategoryMicrositeLinks;
        }
        public static List<CategoryMicrositeLink> GetCategoryMicrositeLinkByMsidID(int Msid)
        {
            List<CategoryMicrositeLink> CategoryMicrositeLinks = null;
            string key = "CategoryMicrositeLinks_CategoryMicrositeLinks_" + Msid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CategoryMicrositeLinks = (List<CategoryMicrositeLink>)BizObject.Cache[key];
            }
            else
            {
                List<CategoryMicrositeLinkInfo> recordset = SiteProvider.PR2.GetCategoryMicrositeLinkByMsidID(Msid);
                CategoryMicrositeLinks = GetCategoryMicrositeLinkListFromCategoryMicrositeLinkInfoList(recordset);
                BasePR.CacheData(key, CategoryMicrositeLinks);
            }
            return CategoryMicrositeLinks;
        }
        public static DataTable GetCategoryMicrositeLinkDTByMsidID(int msid)
        {
            return SiteProvider.PR2.GetCategoryMicrositeLinkDTByMsidID(msid);
        }
        public static int InsertCategoryMicrositeLinks(int msid, int categoryid)
        {
            CategoryMicrositeLinkInfo record = new CategoryMicrositeLinkInfo(msid, categoryid);
            int ret = SiteProvider.PR2.InsertCategoryMicrositeLink(record);

            BizObject.PurgeCacheItems("CategoryMicrositeLinks_CategoryMicrositeLinks");
            return ret;
        }
        public static bool UpdateCategoryMicrositeLinkLists(List<string> CategoryIds, int Msid)
        {
            return SiteProvider.PR2.UpdateCategoryMicrositeLinkLists(CategoryIds, Msid);
        }
        /// <summary>
        /// Updates an existing MicrositeDomain
        /// </summary>
        public static bool UpdateCategoryMicrositeLink(int Msid, int categoryid)
        {
            CategoryMicrositeLinkInfo record = new CategoryMicrositeLinkInfo(Msid, categoryid);
            bool ret = SiteProvider.PR2.UpdateCategoryMicrositeLink(record);

            BizObject.PurgeCacheItems("CategoryMicrositeLinks_CategoryMicrositeLinks_" + Msid.ToString());
            BizObject.PurgeCacheItems("CategoryMicrositeLinks_CategoryMicrositeLinks");
            return ret;
        }

        /// <summary>
        /// Deletes an existing MicrositeDomain, but first checks if OK to delete
        /// </summary>
        public static bool DeleteCategoryMicrositeLink(int Msid, int categoryid)
        {
            bool IsOKToDelete = OKToDelete(Msid);
            if (IsOKToDelete)
            {
                return (bool)DeleteCategoryMicrositeLink(Msid, categoryid, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing MicrositeDomain - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteCategoryMicrositeLink(int Msid, int categoryid, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteCategoryMicrositeLink(Msid, categoryid);
            BizObject.PurgeCacheItems("CategoryMicrositeLinks_CategoryMicrositeLinks");
            return ret;
        }

        /// <summary>
        /// Checks to see if a MicrositeDomain can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }


        /// <summary>
        /// Returns a MicrositeDomain object filled with the data taken from the input MicrositeDomainInfo
        /// </summary>
        private static CategoryMicrositeLink GetCategoryMicrositeLinkFromCategoryMicrositeLinkInfo(CategoryMicrositeLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new CategoryMicrositeLink(record.Msid, record.Categoryid);
            }
        }

        /// <summary>
        /// Returns a list of MicrositeDomain objects filled with the data taken from the input list of MicrositeDomainInfo
        /// </summary>
        private static List<CategoryMicrositeLink> GetCategoryMicrositeLinkListFromCategoryMicrositeLinkInfoList(List<CategoryMicrositeLinkInfo> recordset)
        {
            List<CategoryMicrositeLink> CategoryMicrositeLinks = new List<CategoryMicrositeLink>();
            foreach (CategoryMicrositeLinkInfo record in recordset)
                CategoryMicrositeLinks.Add(GetCategoryMicrositeLinkFromCategoryMicrositeLinkInfo(record));
            return CategoryMicrositeLinks;
        }
        #endregion
    }
}