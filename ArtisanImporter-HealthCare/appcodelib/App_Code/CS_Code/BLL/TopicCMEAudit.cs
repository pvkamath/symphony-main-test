﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Alex Chen
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using System.Collections.Generic;
using PearlsReview.DAL;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for TopicCMEAudit
    /// </summary>
    public class TopicCMEAudit : BasePR
    {
        #region Variables and Properties
        private int _topicID = 0;
        public int TopicID
        {
            get { return _topicID; }
            set { _topicID = value; }
        }

        private bool _cmecdEvidenceInd = false;
        public bool CMEcdEvidenceInd
        {
            get { return _cmecdEvidenceInd; }
            set { _cmecdEvidenceInd = value; }
        }

        private bool _cmecdPlannerInd = false;
        public bool CMEcdPlannerInd
        {
            get { return _cmecdPlannerInd; }
            set { _cmecdPlannerInd = value; }
        }

        private bool _cmecdActivityInd = false;
        public bool CMEcdActivityInd
        {
            get { return _cmecdActivityInd; }
            set { _cmecdActivityInd = value; }
        }

        private bool _cmecdRecommendationInd = false;
        public bool CMEcdRecommendationInd
        {
            get { return _cmecdRecommendationInd; }
            set { _cmecdRecommendationInd = value; }
        }

        private bool _cmecdResearchInd = false;
        public bool CMEcdResearchInd
        {
            get { return _cmecdResearchInd; }
            set { _cmecdResearchInd = value; }
        }

        private string _cmecdTargetAudience = "";
        public string CMEcdTargetAudience
        {
            get { return _cmecdTargetAudience; }
            set { _cmecdTargetAudience = value; }
        }

        private string _cmecdContent = "";
        public string CMEcdContent
        {
            get { return _cmecdContent; }
            set { _cmecdContent = value; }
        }

        private string _cmecdPeer1 = "";
        public string CMEcdPeer1
        {
            get { return _cmecdPeer1; }
            set { _cmecdPeer1 = value; }
        }

        private string _cmecdPeer2 = "";
        public string CMEcdPeer2
        {
            get { return _cmecdPeer2; }
            set { _cmecdPeer2 = value; }
        }

        private string _cmecdPeer3 = "";
        public string CMEcdPeer3
        {
            get { return _cmecdPeer3; }
            set { _cmecdPeer3 = value; }
        }

        private bool _cmeprClinicalInd = false;
        public bool CMEprClinicalInd
        {
            get { return _cmeprClinicalInd; }
            set { _cmeprClinicalInd = value; }
        }

        private bool _cmeprQualityInd = false;
        public bool CMEprQualityInd
        {
            get { return _cmeprQualityInd; }
            set { _cmeprQualityInd = value; }
        }

        private bool _cmeprRequirementInd = false;
        public bool CMEprRequirementInd
        {
            get { return _cmeprRequirementInd; }
            set { _cmeprRequirementInd = value; }
        }

        private bool _cmeprEvaluationInd = false;
        public bool CMEprEvaluationInd
        {
            get { return _cmeprEvaluationInd; }
            set { _cmeprEvaluationInd = value; }
        }

        private bool _cmeprOutcomeInd = false;
        public bool CMEprOutcomeInd
        {
            get { return _cmeprOutcomeInd; }
            set { _cmeprOutcomeInd = value; }
        }

        private bool _cmeprSearchInd = false;
        public bool CMEprSearchInd
        {
            get { return _cmeprSearchInd; }
            set { _cmeprSearchInd = value; }
        }

        private bool _cmeprCaseInd = false;
        public bool CMEprCaseInd
        {
            get { return _cmeprCaseInd; }
            set { _cmeprCaseInd = value; }
        }

        private bool _cmeprOtherInd = false;
        public bool CMEprOtherInd
        {
            get { return _cmeprOtherInd; }
            set { _cmeprOtherInd = value; }
        }

        private string _cmeprDescribe = "";
        public string CMEprDescribe
        {
            get { return _cmeprDescribe; }
            set { _cmeprDescribe = value; }
        }

        private string _cmeusdLearner = "";
        public string CMEusdLearner
        {
            get { return _cmeusdLearner; }
            set { _cmeusdLearner = value; }
        }

        private string _cmeusdProcedure = "";
        public string CMEusdProcedure
        {
            get { return _cmeusdProcedure; }
            set { _cmeusdProcedure = value; }
        }

        private string _cmeusdResult = "";
        public string CMEusdResult
        {
            get { return _cmeusdResult; }
            set { _cmeusdResult = value; }
        }

        private string _cmeusdCause = "";
        public string CMEusdCause
        {
            get { return _cmeusdCause; }
            set { _cmeusdCause = value; }
        }

        private string _cmeusdExplain = "";
        public string CMEusdExplain
        {
            get { return _cmeusdExplain; }
            set { _cmeusdExplain = value; }
        }

        private bool _cmecpPatientInd = false;
        public bool CMEcpPatientInd
        {
            get { return _cmecpPatientInd; }
            set { _cmecpPatientInd = value; }
        }

        private string _cmecpPatient = "";
        public string CMEcpPatient
        {
            get { return _cmecpPatient; }
            set { _cmecpPatient = value; }
        }

        private bool _cmecpKnowledgeInd = false;
        public bool CMEcpKnowledgeInd
        {
            get { return _cmecpKnowledgeInd; }
            set { _cmecpKnowledgeInd = value; }
        }

        private string _cmecpKnowledge = "";
        public string CMEcpKnowledge
        {
            get { return _cmecpKnowledge; }
            set { _cmecpKnowledge = value; }
        }

        private bool _cmecpPracticeInd = false;
        public bool CMEcpPracticeInd
        {
            get { return _cmecpPracticeInd; }
            set { _cmecpPracticeInd = value; }
        }

        private bool _cmecpSystemPracticeInd = false;
        public bool CMEcpSystemPracticeInd
        {
            get { return _cmecpSystemPracticeInd; }
            set { _cmecpSystemPracticeInd = value; }
        }

        private string _cmecpPractice = "";
        public string CMEcpPractice
        {
            get { return _cmecpPractice; }
            set { _cmecpPractice = value; }
        }

        private bool _cmecpProfessionInd = false;
        public bool CMEcpProfessionInd
        {
            get { return _cmecpProfessionInd; }
            set { _cmecpProfessionInd = value; }
        }

        private string _cmecpProfession = "";
        public string CMEcpProfession
        {
            get { return _cmecpProfession; }
            set { _cmecpProfession = value; }
        }

        private bool _cmecpSkillInd = false;
        public bool CMEcpSkillInd
        {
            get { return _cmecpSkillInd; }
            set { _cmecpSkillInd = value; }
        }

        private string _cmecpSkill = "";
        public string CMEcpSkill
        {
            get { return _cmecpSkill; }
            set { _cmecpSkill = value; }
        }

        private bool _cmecpCareInd = false;
        public bool CMEcpCareInd
        {
            get { return _cmecpCareInd; }
            set { _cmecpCareInd = value; }
        }

        private string _cmecpCare = "";
        public string CMEcpCare
        {
            get { return _cmecpCare; }
            set { _cmecpCare = value; }
        }

        private bool _cmecpTeamInd = false;
        public bool CMEcpTeamInd
        {
            get { return _cmecpTeamInd; }
            set { _cmecpTeamInd = value; }
        }

        private string _cmecpTeam = "";
        public string CMEcpTeam
        {
            get { return _cmecpTeam; }
            set { _cmecpTeam = value; }
        }

        private bool _cmecpEvidenceInd = false;
        public bool CMEcpEvidenceInd
        {
            get { return _cmecpEvidenceInd; }
            set { _cmecpEvidenceInd = value; }
        }

        private string _cmecpEvidence = "";
        public string CMEcpEvidence
        {
            get { return _cmecpEvidence; }
            set { _cmecpEvidence = value; }
        }

        private bool _cmecpQualityInd = false;
        public bool CMEcpQualityInd
        {
            get { return _cmecpQualityInd; }
            set { _cmecpQualityInd = value; }
        }

        private string _cmecpQuality = "";
        public string CMEcpQuality
        {
            get { return _cmecpQuality; }
            set { _cmecpQuality = value; }
        }

        private bool _cmecpUtilizeInd = false;
        public bool CMEcpUtilizeInd
        {
            get { return _cmecpUtilizeInd; }
            set { _cmecpUtilizeInd = value; }
        }

        private string _cmecpUtilize = "";
        public string CMEcpUtilize
        {
            get { return _cmecpUtilize; }
            set { _cmecpUtilize = value; }
        }

        private string _cmecpContent = "";
        public string CMEcpContent
        {
            get { return _cmecpContent; }
            set { _cmecpContent = value; }
        }

        private string _cmecpBarrier = "";
        public string CMEcpBarrier
        {
            get { return _cmecpBarrier; }
            set { _cmecpBarrier = value; }
        }

        private string _cmecpStrategy = "";
        public string CMEcpStrategy
        {
            get { return _cmecpStrategy; }
            set { _cmecpStrategy = value; }
        }

        private string _cmecpNonStrategy = "";
        public string CMEcpNonStrategy
        {
            get { return _cmecpNonStrategy; }
            set { _cmecpNonStrategy = value; }
        }

        private string _cmecpPurpose = "";
        public string CMEcpPurpose
        {
            get { return _cmecpPurpose; }
            set { _cmecpPurpose = value; }
        }

        private string _cmecpFactors = "";
        public string CMEcpFactors
        {
            get { return _cmecpFactors; }
            set { _cmecpFactors = value; }
        }

        private string _cmepgPg1 = "";
        public string CMEpgPg1
        {
            get { return _cmepgPg1; }
            set { _cmepgPg1 = value; }
        }

        private string _cmepgPg2 = "";
        public string CMEpgPg2
        {
            get { return _cmepgPg2; }
            set { _cmepgPg2 = value; }
        }

        private string _cmepgPg3 = "";
        public string CMEpgPg3
        {
            get { return _cmepgPg3; }
            set { _cmepgPg3 = value; }
        }

        #endregion

        #region Methods
        public TopicCMEAudit(int topicID, bool cmecdEvidenceInd, bool cmecdPlannerInd, bool cmecdActivityInd,
            bool cmecdRecommendationInd, bool cmecdResearchInd, string cmecdTargetAudience, string cmecdContent,
            string cmecdPeer1, string cmecdPeer2, string cmecdPeer3, bool cmeprClinicalInd, bool cmeprQualityInd, 
            bool cmeprRequirementInd, bool cmeprEvaluationInd, bool cmeprOutcomeInd, bool cmeprSearchInd, 
            bool cmeprCaseInd, bool cmeprOtherInd, string cmeprDescribe, string cmeusdLearner, string cmeusdProcedure, 
            string cmeusdResult, string cmeusdCause, string cmeusdExplain, bool cmecpPatientInd, string cmecpPatient, 
            bool cmecpKnowledgeInd, string cmecpKnowledge,  bool cmecpPracticeInd, bool cmecpSystemPracticeInd, 
            string cmecpPractice, bool cmecpProfessionInd, string cmecpProfession, bool cmecpSkillInd, string cmecpSkill, 
            bool cmecpCareInd, string cmecpCare, bool cmecpTeamInd, string cmecpTeam, bool cmecpEvidenceInd, 
            string cmecpEvidence, bool cmecpQualityInd, string cmecpQuality, bool cmecpUtilizeInd, string cmecpUtilize, 
            string cmecpContent, string cmecpBarrier, string cmecpStrategy, string cmecpNonStrategy, 
            string cmecpPurpose, string cmecpFactors, string cmepgPg1, string cmepgPg2, string cmepgPg3
            )
        {
            this.TopicID = topicID;
            this.CMEcdEvidenceInd = cmecdEvidenceInd;
            this.CMEcdPlannerInd = cmecdPlannerInd;
            this.CMEcdActivityInd = cmecdActivityInd;
            this.CMEcdRecommendationInd = cmecdRecommendationInd;
            this.CMEcdResearchInd = cmecdResearchInd;
            this.CMEcdTargetAudience = cmecdTargetAudience;
            this.CMEcdContent = cmecdContent;
            this.CMEcdPeer1 = cmecdPeer1;
            this.CMEcdPeer2 = cmecdPeer2;
            this.CMEcdPeer3 = cmecdPeer3;
            this.CMEprClinicalInd = cmeprClinicalInd;
            this.CMEprQualityInd = cmeprQualityInd;
            this.CMEprRequirementInd = cmeprRequirementInd;
            this.CMEprEvaluationInd = cmeprEvaluationInd;
            this.CMEprOutcomeInd = cmeprOutcomeInd;
            this.CMEprSearchInd = cmeprSearchInd;
            this.CMEprCaseInd = cmeprCaseInd;
            this.CMEprOtherInd = cmeprOtherInd;
            this.CMEprDescribe = cmeprDescribe;
            this.CMEusdLearner = cmeusdLearner;
            this.CMEusdProcedure = cmeusdProcedure;
            this.CMEusdResult = cmeusdResult;
            this.CMEusdCause = cmeusdCause;
            this.CMEusdExplain = cmeusdExplain;
            this.CMEcpPatientInd = cmecpPatientInd;
            this.CMEcpPatient = cmecpPatient;
            this.CMEcpKnowledgeInd = cmecpKnowledgeInd;
            this.CMEcpKnowledge = cmecpKnowledge;
            this.CMEcpPracticeInd = cmecpPracticeInd;
            this.CMEcpSystemPracticeInd = cmecpSystemPracticeInd;
            this.CMEcpPractice = cmecpPractice;
            this.CMEcpProfessionInd = cmecpPracticeInd;
            this.CMEcpProfession = cmecpProfession;
            this.CMEcpSkillInd = cmecpSkillInd;
            this.CMEcpSkill = cmecpSkill;
            this.CMEcpCareInd = cmecpCareInd;
            this.CMEcpCare = cmecpCare;
            this.CMEcpTeamInd = cmecpTeamInd;
            this.CMEcpTeam = cmecpTeam;
            this.CMEcpEvidenceInd = cmecpEvidenceInd;
            this.CMEcpEvidence = cmecpEvidence;
            this.CMEcpQualityInd = cmecpQualityInd;
            this.CMEcpQuality = cmecpQuality;
            this.CMEcpUtilizeInd = cmecpUtilizeInd;
            this.CMEcpUtilize = cmecpUtilize;
            this.CMEcpContent = cmecpContent;
            this.CMEcpBarrier = cmecpBarrier;
            this.CMEcpStrategy = cmecpStrategy;
            this.CMEcpNonStrategy = cmecpNonStrategy;
            this.CMEcpPurpose = cmecpPurpose;
            this.CMEcpFactors = cmecpFactors;
            this.CMEpgPg1 = cmepgPg1;
            this.CMEpgPg2 = cmepgPg2;
            this.CMEpgPg3 = cmepgPg3;
        }

        public bool Update()
        {
            return TopicCMEAudit.UpdateTopicCMEAudit(this.TopicID, this.CMEcdEvidenceInd, this.CMEcdPlannerInd,
                    this.CMEcdActivityInd, this.CMEcdRecommendationInd, this.CMEcdResearchInd,
                    this.CMEcdTargetAudience, this.CMEcdContent,
                    this.CMEcdPeer1, this.CMEcdPeer2, this.CMEcdPeer3,
                    this.CMEprClinicalInd, this.CMEprQualityInd,
                    this.CMEprRequirementInd, this.CMEprEvaluationInd, this.CMEprOutcomeInd,
                    this.CMEprSearchInd, this.CMEprCaseInd, this.CMEprOtherInd, this.CMEprDescribe,
                    this.CMEusdLearner, this.CMEusdProcedure, this.CMEusdResult, this.CMEusdCause,
                    this.CMEusdExplain, this.CMEcpPatientInd, this.CMEcpPatient, this.CMEcpKnowledgeInd,
                    this.CMEcpKnowledge, this.CMEcpPracticeInd, this.CMEcpSystemPracticeInd,
                    this.CMEcpPractice, this.CMEcpProfessionInd,
                    this.CMEcpProfession, this.CMEcpSkillInd, this.CMEcpSkill,
                    this.CMEcpCareInd, this.CMEcpCare, this.CMEcpTeamInd, this.CMEcpTeam,
                    this.CMEcpEvidenceInd, this.CMEcpEvidence, this.CMEcpQualityInd, this.CMEcpQuality,
                    this.CMEcpUtilizeInd, this.CMEcpUtilize, this.CMEcpContent, this.CMEcpBarrier,
                    this.CMEcpStrategy, this.CMEcpNonStrategy, this.CMEcpPurpose, this.CMEcpFactors, this.CMEpgPg1,
                    this.CMEpgPg2, this.CMEpgPg3);
        }

        public bool Delete()
        {
            return TopicCMEAudit.DeleteTopicCMEAudit(this.TopicID);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <returns>All Topic CME Audits</returns>
        public static List<TopicCMEAudit> GetTopicCMEAudits(string sortExpression)
        {
            if (String.IsNullOrEmpty(sortExpression))
                sortExpression = "topicid";

            List<TopicCMEAudit> topicCMEAudits = null;
            string key = "TopicCMEAudits_TopicCMEAudits_" + sortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                topicCMEAudits = (List<TopicCMEAudit>)BizObject.Cache[key];
            }
            else
            {
                List<TopicCMEAuditInfo> recordset = SiteProvider.PR2.GetTopicCMEAudits(sortExpression);
                topicCMEAudits = GetTopicCMEAuditListFromTopicCMEAuditInfoList(recordset);
                BasePR.CacheData(key, topicCMEAudits);
            }
            return topicCMEAudits;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="topicID"></param>
        /// <returns>Topic CME Audit</returns>
        public static TopicCMEAudit GetTopicCMEAuditByID(int topicID)
        {
            TopicCMEAudit topicCMEAudit = null;
            string key = "TopicCMEAudits_TopicCMEAudit_" + topicID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                topicCMEAudit = (TopicCMEAudit)BizObject.Cache[key];
            }
            else
            {
                topicCMEAudit = GetTopicCMEAuditFromTopicCMEAuditInfo(SiteProvider.PR2.GetTopicCMEAuditByID(topicID));
                BasePR.CacheData(key, topicCMEAudit);
            }
            return topicCMEAudit;
        }

        public static int InsertTopicCMEAudit(int topicID, bool cmecdEvidenceInd, bool cmecdPlannerInd, bool cmecdActivityInd,
            bool cmecdRecommendationInd, bool cmecdResearchInd, string cmecdTargetAudience, string cmecdContent,
            string cmecdPeer1, string cmecdPeer2, string cmecdPeer3, bool cmeprClinicalInd, bool cmeprQualityInd,
            bool cmeprRequirementInd, bool cmeprEvaluationInd, bool cmeprOutcomeInd, bool cmeprSearchInd,
            bool cmeprCaseInd, bool cmeprOtherInd, string cmeprDescribe, string cmeusdLearner, string cmeusdProcedure,
            string cmeusdResult, string cmeusdCause, string cmeusdExplain, bool cmecpPatientInd, string cmecpPatient,
            bool cmecpKnowledgeInd, string cmecpKnowledge, bool cmecpPracticeInd, bool cmecpSystemPracticeInd,
            string cmecpPractice, bool cmecpProfessionInd, string cmecpProfession, bool cmecpSkillInd, string cmecpSkill,
            bool cmecpCareInd, string cmecpCare, bool cmecpTeamInd, string cmecpTeam, bool cmecpEvidenceInd,
            string cmecpEvidence, bool cmecpQualityInd, string cmecpQuality, bool cmecpUtilizeInd, string cmecpUtilize,
            string cmecpContent, string cmecpBarrier, string cmecpStrategy, string cmecpNonStrategy,
            string cmecpPurpose, string cmecpFactors, string cmepgPg1, string cmepgPg2, string cmepgPg3
            )
        {
            cmecdTargetAudience = BizObject.ConvertNullToEmptyString(cmecdTargetAudience);
            cmecdContent = BizObject.ConvertNullToEmptyString(cmecdContent);
            cmecdPeer1 = BizObject.ConvertNullToEmptyString(cmecdPeer1);
            cmecdPeer2 = BizObject.ConvertNullToEmptyString(cmecdPeer2);
            cmecdPeer3 = BizObject.ConvertNullToEmptyString(cmecdPeer3);
            cmeprDescribe = BizObject.ConvertNullToEmptyString(cmeprDescribe);
            cmeusdLearner = BizObject.ConvertNullToEmptyString(cmeusdLearner);
            cmeusdProcedure = BizObject.ConvertNullToEmptyString(cmeusdProcedure);
            cmeusdResult = BizObject.ConvertNullToEmptyString(cmeusdResult);
            cmeusdCause = BizObject.ConvertNullToEmptyString(cmeusdCause);
            cmeusdExplain = BizObject.ConvertNullToEmptyString(cmeusdExplain);
            cmecpPatient = BizObject.ConvertNullToEmptyString(cmecpPatient);
            cmecpKnowledge = BizObject.ConvertNullToEmptyString(cmecpKnowledge);
            cmecpPractice = BizObject.ConvertNullToEmptyString(cmecpPractice);
            cmecpProfession = BizObject.ConvertNullToEmptyString(cmecpProfession);
            cmecpSkill = BizObject.ConvertNullToEmptyString(cmecpSkill);
            cmecpCare = BizObject.ConvertNullToEmptyString(cmecpCare);
            cmecpTeam = BizObject.ConvertNullToEmptyString(cmecpTeam);
            cmecpEvidence = BizObject.ConvertNullToEmptyString(cmecpEvidence);
            cmecpQuality = BizObject.ConvertNullToEmptyString(cmecpQuality);
            cmecpUtilize = BizObject.ConvertNullToEmptyString(cmecpUtilize);
            cmecpContent = BizObject.ConvertNullToEmptyString(cmecpContent);
            cmecpBarrier = BizObject.ConvertNullToEmptyString(cmecpBarrier);
            cmecpStrategy = BizObject.ConvertNullToEmptyString(cmecpStrategy);
            cmecpNonStrategy = BizObject.ConvertNullToEmptyString(cmecpNonStrategy);
            cmecpPurpose = BizObject.ConvertNullToEmptyString(cmecpPurpose);
            cmecpFactors = BizObject.ConvertNullToEmptyString(cmecpFactors);
            cmepgPg1 = BizObject.ConvertNullToEmptyString(cmepgPg1);
            cmepgPg2 = BizObject.ConvertNullToEmptyString(cmepgPg2);
            cmepgPg3 = BizObject.ConvertNullToEmptyString(cmepgPg3);

            TopicCMEAuditInfo record = new TopicCMEAuditInfo(topicID, cmecdEvidenceInd, cmecdPlannerInd, 
                cmecdActivityInd, cmecdRecommendationInd, cmecdResearchInd, cmecdTargetAudience, cmecdContent,
                cmecdPeer1, cmecdPeer2, cmecdPeer3, cmeprClinicalInd, cmeprQualityInd,
                cmeprRequirementInd, cmeprEvaluationInd, cmeprOutcomeInd, cmeprSearchInd,
                cmeprCaseInd, cmeprOtherInd, cmeprDescribe, cmeusdLearner, cmeusdProcedure,
                cmeusdResult, cmeusdCause, cmeusdExplain, cmecpPatientInd, cmecpPatient,
                cmecpKnowledgeInd, cmecpKnowledge, cmecpPracticeInd, cmecpSystemPracticeInd,
                cmecpPractice, cmecpProfessionInd, cmecpProfession, cmecpSkillInd, cmecpSkill,
                cmecpCareInd, cmecpCare, cmecpTeamInd, cmecpTeam, cmecpEvidenceInd,
                cmecpEvidence, cmecpQualityInd, cmecpQuality, cmecpUtilizeInd, cmecpUtilize,
                cmecpContent, cmecpBarrier, cmecpStrategy, cmecpNonStrategy,
                cmecpPurpose, cmecpFactors, cmepgPg1, cmepgPg2, cmepgPg3);

            int ret = SiteProvider.PR2.InsertTopicCMEAudit(record);
            BizObject.PurgeCacheItems("TopicCMEAudits_TopicCMEAudit");
            return ret;
        }

        public static bool UpdateTopicCMEAudit(int topicID, bool cmecdEvidenceInd, bool cmecdPlannerInd, 
            bool cmecdActivityInd, bool cmecdRecommendationInd, bool cmecdResearchInd, string cmecdTargetAudience, 
            string cmecdContent, string cmecdPeer1, string cmecdPeer2, string cmecdPeer3, bool cmeprClinicalInd, 
            bool cmeprQualityInd, bool cmeprRequirementInd, bool cmeprEvaluationInd, bool cmeprOutcomeInd, 
            bool cmeprSearchInd, bool cmeprCaseInd, bool cmeprOtherInd, string cmeprDescribe, 
            string cmeusdLearner, string cmeusdProcedure, string cmeusdResult, string cmeusdCause, 
            string cmeusdExplain, bool cmecpPatientInd, string cmecpPatient, bool cmecpKnowledgeInd, 
            string cmecpKnowledge, bool cmecpPracticeInd, bool cmecpSystemPracticeInd, string cmecpPractice, 
            bool cmecpProfessionInd, string cmecpProfession, bool cmecpSkillInd, string cmecpSkill,
            bool cmecpCareInd, string cmecpCare, bool cmecpTeamInd, string cmecpTeam, bool cmecpEvidenceInd,
            string cmecpEvidence, bool cmecpQualityInd, string cmecpQuality, bool cmecpUtilizeInd, string cmecpUtilize,
            string cmecpContent, string cmecpBarrier, string cmecpStrategy, string cmecpNonStrategy,
            string cmecpPurpose, string cmecpFactors, string cmepgPg1, string cmepgPg2, string cmepgPg3
            )
        {
            cmecdTargetAudience = BizObject.ConvertNullToEmptyString(cmecdTargetAudience);
            cmecdContent = BizObject.ConvertNullToEmptyString(cmecdContent);
            cmecdPeer1 = BizObject.ConvertNullToEmptyString(cmecdPeer1);
            cmecdPeer2 = BizObject.ConvertNullToEmptyString(cmecdPeer2);
            cmecdPeer3 = BizObject.ConvertNullToEmptyString(cmecdPeer3);
            cmeprDescribe = BizObject.ConvertNullToEmptyString(cmeprDescribe);
            cmeusdLearner = BizObject.ConvertNullToEmptyString(cmeusdLearner);
            cmeusdProcedure = BizObject.ConvertNullToEmptyString(cmeusdProcedure);
            cmeusdResult = BizObject.ConvertNullToEmptyString(cmeusdResult);
            cmeusdCause = BizObject.ConvertNullToEmptyString(cmeusdCause);
            cmeusdExplain = BizObject.ConvertNullToEmptyString(cmeusdExplain);
            cmecpPatient = BizObject.ConvertNullToEmptyString(cmecpPatient);
            cmecpKnowledge = BizObject.ConvertNullToEmptyString(cmecpKnowledge);
            cmecpPractice = BizObject.ConvertNullToEmptyString(cmecpPractice);
            cmecpProfession = BizObject.ConvertNullToEmptyString(cmecpProfession);
            cmecpSkill = BizObject.ConvertNullToEmptyString(cmecpSkill);
            cmecpCare = BizObject.ConvertNullToEmptyString(cmecpCare);
            cmecpTeam = BizObject.ConvertNullToEmptyString(cmecpTeam);
            cmecpEvidence = BizObject.ConvertNullToEmptyString(cmecpEvidence);
            cmecpQuality = BizObject.ConvertNullToEmptyString(cmecpQuality);
            cmecpUtilize = BizObject.ConvertNullToEmptyString(cmecpUtilize);
            cmecpContent = BizObject.ConvertNullToEmptyString(cmecpContent);
            cmecpBarrier = BizObject.ConvertNullToEmptyString(cmecpBarrier);
            cmecpStrategy = BizObject.ConvertNullToEmptyString(cmecpStrategy);
            cmecpNonStrategy = BizObject.ConvertNullToEmptyString(cmecpNonStrategy);
            cmecpPurpose = BizObject.ConvertNullToEmptyString(cmecpPurpose);
            cmecpFactors = BizObject.ConvertNullToEmptyString(cmecpFactors);
            cmepgPg1 = BizObject.ConvertNullToEmptyString(cmepgPg1);
            cmepgPg2 = BizObject.ConvertNullToEmptyString(cmepgPg2);
            cmepgPg3 = BizObject.ConvertNullToEmptyString(cmepgPg3);

            TopicCMEAuditInfo record = new TopicCMEAuditInfo(topicID, cmecdEvidenceInd, cmecdPlannerInd,
                cmecdActivityInd, cmecdRecommendationInd, cmecdResearchInd, cmecdTargetAudience, cmecdContent,
                cmecdPeer1, cmecdPeer2, cmecdPeer3, cmeprClinicalInd, cmeprQualityInd,
                cmeprRequirementInd, cmeprEvaluationInd, cmeprOutcomeInd, cmeprSearchInd,
                cmeprCaseInd, cmeprOtherInd, cmeprDescribe, cmeusdLearner, cmeusdProcedure,
                cmeusdResult, cmeusdCause, cmeusdExplain, cmecpPatientInd, cmecpPatient,
                cmecpKnowledgeInd, cmecpKnowledge, cmecpPracticeInd, cmecpSystemPracticeInd,
                cmecpPractice, cmecpProfessionInd, cmecpProfession, cmecpSkillInd, cmecpSkill,
                cmecpCareInd, cmecpCare, cmecpTeamInd, cmecpTeam, cmecpEvidenceInd,
                cmecpEvidence, cmecpQualityInd, cmecpQuality, cmecpUtilizeInd, cmecpUtilize,
                cmecpContent, cmecpBarrier, cmecpStrategy, cmecpNonStrategy,
                cmecpPurpose, cmecpFactors, cmepgPg1, cmepgPg2, cmepgPg3);

            bool ret = SiteProvider.PR2.UpdateTopicCMEAudit(record);

            BizObject.PurgeCacheItems("TopicCMEAudits_TopicCMEAudit_" + topicID.ToString());
            BizObject.PurgeCacheItems("TopicCMEAudits_TopicCMEAudits");
            return ret;
        }

        public static bool DeleteTopicCMEAudit(int topicID)
        {
            bool ret = SiteProvider.PR2.DeleteTopicCMEAudit(topicID);
            BizObject.PurgeCacheItems("TopicCMEAudits_TopicCMEAudit");
            return ret;
        }

        private static List<TopicCMEAudit> GetTopicCMEAuditListFromTopicCMEAuditInfoList(List<TopicCMEAuditInfo> recordset)
        {
            List<TopicCMEAudit> topicCMEAudits = new List<TopicCMEAudit>();
            foreach (TopicCMEAuditInfo record in recordset)
                topicCMEAudits.Add(GetTopicCMEAuditFromTopicCMEAuditInfo(record));
            return topicCMEAudits;
        }

        private static TopicCMEAudit GetTopicCMEAuditFromTopicCMEAuditInfo(TopicCMEAuditInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new TopicCMEAudit(record.TopicID, record.CMEcdEvidenceInd, record.CMEcdPlannerInd,
                    record.CMEcdActivityInd, record.CMEcdRecommendationInd, record.CMEcdResearchInd,
                    record.CMEcdTargetAudience, record.CMEcdContent,
                    record.CMEcdPeer1, record.CMEcdPeer2, record.CMEcdPeer3,
                    record.CMEprClinicalInd, record.CMEprQualityInd,
                    record.CMEprRequirementInd, record.CMEprEvaluationInd, record.CMEprOutcomeInd,
                    record.CMEprSearchInd, record.CMEprCaseInd, record.CMEprOtherInd, record.CMEprDescribe,
                    record.CMEusdLearner, record.CMEusdProcedure, record.CMEusdResult, record.CMEusdCause, 
                    record.CMEusdExplain, record.CMEcpPatientInd, record.CMEcpPatient, record.CMEcpKnowledgeInd, 
                    record.CMEcpKnowledge, record.CMEcpPracticeInd, record.CMEcpSystemPracticeInd, 
                    record.CMEcpPractice, record.CMEcpProfessionInd, 
                    record.CMEcpProfession, record.CMEcpSkillInd, record.CMEcpSkill,
                    record.CMEcpCareInd, record.CMEcpCare, record.CMEcpTeamInd, record.CMEcpTeam, 
                    record.CMEcpEvidenceInd, record.CMEcpEvidence, record.CMEcpQualityInd, record.CMEcpQuality, 
                    record.CMEcpUtilizeInd, record.CMEcpUtilize, record.CMEcpContent, record.CMEcpBarrier, 
                    record.CMEcpStrategy, record.CMEcpNonStrategy, record.CMEcpPurpose, record.CMEcpFactors, record.CMEpgPg1, 
                    record.CMEpgPg2, record.CMEpgPg3);
            }
        }

        #endregion
    }
}
