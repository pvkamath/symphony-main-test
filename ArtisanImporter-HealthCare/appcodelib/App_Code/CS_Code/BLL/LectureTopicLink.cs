﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
/// <summary>
/// Summary description for LectureTopicLink
/// </summary>
    public class LectureTopicLink : BasePR
    {
        #region Variables and Properties

        private int _LectTopID = 0;
        public int LectTopID
        {
            get { return _LectTopID; }
            protected set { _LectTopID = value; }
        }

        private int _LectureID = 0;
        public int LectureID
        {
            get { return _LectureID; }
            set { _LectureID = value; }
        }

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }


        private int _Sequence = 0;
        public int Sequence
        {
            get { return _Sequence; }
            private set { _Sequence = value; }
        }



        public LectureTopicLink(int LectTopID, int LectureID, int TopicID, int Sequence)
        {
            this.LectTopID = LectTopID;
            this.LectureID = LectureID;
            this.TopicID = TopicID;
            this.Sequence = Sequence;
        }

        public bool Delete()
        {
            bool success = LectureTopicLink.DeleteLectureTopicLink(this.LectTopID);
            if (success)
                this.LectTopID = 0;
            return success;
        }

        public bool Update()
        {
            return LectureTopicLink.UpdateLectureTopicLink(this.LectTopID, this.LectureID, this.TopicID, this.Sequence);
        }
        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all LectureTopicLinks
        /// </summary>
        public static List<LectureTopicLink> GetLectureTopicLinks(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LectureID";

            List<LectureTopicLink> LectureTopicLinks = null;
            string key = "LectureTopicLinks_LectureTopicLinks_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureTopicLinks = (List<LectureTopicLink>)BizObject.Cache[key];
            }
            else
            {
                List<LectureTopicLinkInfo> recordset = SiteProvider.PR2.GetLectureTopicLinks(cSortExpression);
                LectureTopicLinks = GetLectureTopicLinkListFromLectureTopicLinkInfoList(recordset);
                BasePR.CacheData(key, LectureTopicLinks);
            }
            return LectureTopicLinks;
        }

        /// <summary>
        /// Returns a collection with all LectureTopicLinks for a LectureID
        /// </summary>
        public static List<LectureTopicLink> GetLectureTopicLinksByLectureID(int LectureID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "LectureID";

            List<LectureTopicLink> LectureTopicLinks = null;
            string key = "LectureTopicLinks_LectureTopicLinks_" + LectureID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureTopicLinks = (List<LectureTopicLink>)BizObject.Cache[key];
            }
            else
            {
                List<LectureTopicLinkInfo> recordset = SiteProvider.PR2.GetLectureTopicLinksByLectureID(LectureID, cSortExpression);
                LectureTopicLinks = GetLectureTopicLinkListFromLectureTopicLinkInfoList(recordset);
                BasePR.CacheData(key, LectureTopicLinks);
            }
            return LectureTopicLinks;
        }

        /// <summary>
        /// Returns the number of total LectureTopicLinks
        /// </summary>
        public static int GetLectureTopicLinkCount()
        {
            int LectureTopicLinkCount = 0;
            string key = "LectureTopicLinks_LectureTopicLinkCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureTopicLinkCount = (int)BizObject.Cache[key];
            }
            else
            {
                LectureTopicLinkCount = SiteProvider.PR2.GetLectureTopicLinkCount();
                BasePR.CacheData(key, LectureTopicLinkCount);
            }
            return LectureTopicLinkCount;
        }

        /// <summary>
        /// Returns a LectureTopicLink object with the specified ID
        /// </summary>
        public static LectureTopicLink GetLectureTopicLinkByID(int LectTopID)
        {
            LectureTopicLink LectureTopicLink = null;
            string key = "LectureTopicLinks_LectureTopicLink_" + LectTopID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                LectureTopicLink = (LectureTopicLink)BizObject.Cache[key];
            }
            else
            {
                LectureTopicLink = GetLectureTopicLinkFromLectureTopicLinkInfo(SiteProvider.PR2.GetLectureTopicLinkByID(LectTopID));
                BasePR.CacheData(key, LectureTopicLink);
            }
            return LectureTopicLink;
        }

        /// <summary>
        /// Updates an existing LectureTopicLink
        /// </summary>
        public static bool UpdateLectureTopicLink(int LectTopID, int LectureID, int TopicID, int Sequence)
        {


            LectureTopicLinkInfo record = new LectureTopicLinkInfo(LectTopID, LectureID, TopicID, Sequence);
            bool ret = SiteProvider.PR2.UpdateLectureTopicLink(record);

            BizObject.PurgeCacheItems("LectureTopicLinks_LectureTopicLink_" + LectTopID.ToString());
            BizObject.PurgeCacheItems("LectureTopicLinks_LectureTopicLinks");
            return ret;
        }

        /// <summary>
        /// Creates a new LectureTopicLink
        /// </summary>
        public static int InsertLectureTopicLink(int LectureID, int TopicID, int Sequence)
        {


            LectureTopicLinkInfo record = new LectureTopicLinkInfo(0, LectureID, TopicID, Sequence);
            int ret = SiteProvider.PR2.InsertLectureTopicLink(record);

            BizObject.PurgeCacheItems("LectureTopicLinks_LectureTopicLink");
            return ret;
        }

        public static bool UpdateLectureTopicLinkAssignments(int LectureID, string TopicIDAssignments, int Sequence)
        {
            bool ret = SiteProvider.PR2.UpdateLectureTopicLinkAssignments(LectureID, TopicIDAssignments, Sequence);
            // TODO: release cache?
            return ret;
        }

        /// <summary>
        /// Deletes an existing LectureTopicLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteLectureTopicLink(int LectTopID)
        {
            bool IsOKToDelete = OKToDelete(LectTopID);
            if (IsOKToDelete)
            {
                return (bool)DeleteLectureTopicLink(LectTopID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing LectureTopicLink - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteLectureTopicLink(int LectTopID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteLectureTopicLink(LectTopID);
            //         new RecordDeletedEvent("LectureTopicLink", LectTopID, null).Raise();
            BizObject.PurgeCacheItems("LectureTopicLinks_LectureTopicLink");
            return ret;
        }


        /// <summary>
        /// Deletes an existing LectureTopicLink - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteLectureTopicLinkByTopicAndLecID(int TopicID, int LectureID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteLectureTopicLinkByTopicAndLecID(TopicID,LectureID);
            //         new RecordDeletedEvent("LectureTopicLink", LectTopID, null).Raise();
            BizObject.PurgeCacheItems("LectureTopicLinks_LectureTopicLink");
            return ret;
        }

        /// <summary>
        /// Checks to see if a LectureTopicLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int LectTopID)
        {
            return true;
        }



        /// <summary>
        /// Returns a LectureTopicLink object filled with the data taken from the input LectureTopicLinkInfo
        /// </summary>
        private static LectureTopicLink GetLectureTopicLinkFromLectureTopicLinkInfo(LectureTopicLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new LectureTopicLink(record.LectTopID, record.LectureID, record.TopicID, record.Sequence);
            }
        }

        /// <summary>
        /// Returns a list of LectureTopicLink objects filled with the data taken from the input list of LectureTopicLinkInfo
        /// </summary>
        private static List<LectureTopicLink> GetLectureTopicLinkListFromLectureTopicLinkInfoList(List<LectureTopicLinkInfo> recordset)
        {
            List<LectureTopicLink> LectureTopicLinks = new List<LectureTopicLink>();
            foreach (LectureTopicLinkInfo record in recordset)
                LectureTopicLinks.Add(GetLectureTopicLinkFromLectureTopicLinkInfo(record));
            return LectureTopicLinks;
        }


       #endregion
    }
}
