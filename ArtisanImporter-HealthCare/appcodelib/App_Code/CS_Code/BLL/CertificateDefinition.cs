﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for CertificateDefinition
/// </summary>
    public class CertificateDefinition : BasePR
    {
        #region Variables and Properties

        private int _CertID = 0;
        public int CertID
        {
            get { return _CertID; }
            protected set { _CertID = value; }
        }

        private string _CertName = "";
        public string CertName
        {
            get { return _CertName; }
            set { _CertName = value; }
        }

        private string _CertBody = "";
        public string CertBody
        {
            get { return _CertBody; }
            set { _CertBody = value; }
        }

        private string _CertType = "";
        public string CertType
        {
            get { return _CertType; }
            set { _CertType = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }


        public CertificateDefinition(int CertID, string CertName, string CertBody, string CertType, int FacilityID)
        {
            this.CertID = CertID;
            this.CertName = CertName;
            this.CertBody = CertBody;
            this.CertType = CertType;
            this.FacilityID = FacilityID;
        }

        public bool Delete()
        {
            bool success = CertificateDefinition.DeleteCertificateDefinition(this.CertID);
            if (success)
                this.CertID = 0;
            return success;
        }

        public bool Update()
        {
            return CertificateDefinition.UpdateCertificateDefinition(this.CertID, this.CertName,
                this.CertBody, this.CertType, this.FacilityID);
        }

        #endregion

        #region Methods
        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all CertificateDefinitions
        /// </summary>
        public static List<CertificateDefinition> GetCertificateDefinitions(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "CertName";

            List<CertificateDefinition> CertificateDefinitions = null;
            string key = "CertificateDefinitions_CertificateDefinitions_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CertificateDefinitions = (List<CertificateDefinition>)BizObject.Cache[key];
            }
            else
            {
                List<CertificateDefinitionInfo> recordset = SiteProvider.PR2.GetCertificateDefinitions(cSortExpression);
                CertificateDefinitions = GetCertificateDefinitionListFromCertificateDefinitionInfoList(recordset);
                BasePR.CacheData(key, CertificateDefinitions);
            }
            return CertificateDefinitions;
        }

        /// <summary>
        /// Returns a collection with all CertificateDefinitions by CertType
        /// CertType: L=Lecture, C=CE Credit
        /// </summary>
        public static List<CertificateDefinition> GetCertificateDefinitionsByCertType(string cCertType, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "CertName";

            List<CertificateDefinition> CertificateDefinitions = null;
            string key = "CertificateDefinitions_CertificateDefinitionsByCertType_" +
                cCertType.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CertificateDefinitions = (List<CertificateDefinition>)BizObject.Cache[key];
            }
            else
            {
                List<CertificateDefinitionInfo> recordset =
                    SiteProvider.PR2.GetCertificateDefinitionsByCertType(cCertType, cSortExpression);
                CertificateDefinitions = GetCertificateDefinitionListFromCertificateDefinitionInfoList(recordset);
                BasePR.CacheData(key, CertificateDefinitions);
            }
            return CertificateDefinitions;
        }



        /// <summary>
        /// Returns the number of total CertificateDefinitions
        /// </summary>
        public static int GetCertificateDefinitionCount()
        {
            int CertificateDefinitionCount = 0;
            string key = "CertificateDefinitions_CertificateDefinitionCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CertificateDefinitionCount = (int)BizObject.Cache[key];
            }
            else
            {
                CertificateDefinitionCount = SiteProvider.PR2.GetCertificateDefinitionCount();
                BasePR.CacheData(key, CertificateDefinitionCount);
            }
            return CertificateDefinitionCount;
        }

        /// <summary>
        /// Returns a CertificateDefinition object with the specified ID
        /// </summary>
        public static CertificateDefinition GetCertificateDefinitionByID(int CertID)
        {
            CertificateDefinition CertificateDefinition = null;
            string key = "CertificateDefinitions_CertificateDefinition_" + CertID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                CertificateDefinition = (CertificateDefinition)BizObject.Cache[key];
            }
            else
            {
                CertificateDefinition = GetCertificateDefinitionFromCertificateDefinitionInfo(SiteProvider.PR2.GetCertificateDefinitionByID(CertID));
                BasePR.CacheData(key, CertificateDefinition);
            }
            return CertificateDefinition;
        }

        /// <summary>
        /// Updates an existing CertificateDefinition
        /// </summary>
        public static bool UpdateCertificateDefinition(int CertID, string CertName, string CertBody,
            string CertType, int FacilityID)
        {
            CertName = BizObject.ConvertNullToEmptyString(CertName);
            CertBody = BizObject.ConvertNullToEmptyString(CertBody);
            CertType = BizObject.ConvertNullToEmptyString(CertType);


            CertificateDefinitionInfo record = new CertificateDefinitionInfo(CertID, CertName, CertBody,
                CertType, FacilityID);
            bool ret = SiteProvider.PR2.UpdateCertificateDefinition(record);

            BizObject.PurgeCacheItems("CertificateDefinitions_CertificateDefinition_" + CertID.ToString());
            BizObject.PurgeCacheItems("CertificateDefinitions_CertificateDefinitions");
            return ret;
        }

        /// <summary>
        /// Creates a new CertificateDefinition
        /// </summary>
        public static int InsertCertificateDefinition(string CertName, string CertBody, string CertType,
            int FacilityID)
        {
            CertName = BizObject.ConvertNullToEmptyString(CertName);
            CertBody = BizObject.ConvertNullToEmptyString(CertBody);
            CertType = BizObject.ConvertNullToEmptyString(CertType);

            CertificateDefinitionInfo record = new CertificateDefinitionInfo(0, CertName, CertBody, CertType, 0);
            int ret = SiteProvider.PR2.InsertCertificateDefinition(record);

            BizObject.PurgeCacheItems("CertificateDefinitions_CertificateDefinition");
            return ret;
        }

        /// <summary>
        /// Deletes an existing CertificateDefinition, but first checks if OK to delete
        /// </summary>
        public static bool DeleteCertificateDefinition(int CertID)
        {
            bool IsOKToDelete = OKToDelete(CertID);
            if (IsOKToDelete)
            {
                return (bool)DeleteCertificateDefinition(CertID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing CertificateDefinition - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteCertificateDefinition(int CertID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteCertificateDefinition(CertID);
            //         new RecordDeletedEvent("CertificateDefinition", CertID, null).Raise();
            BizObject.PurgeCacheItems("CertificateDefinitions_CertificateDefinition");
            return ret;
        }



        /// <summary>
        /// Checks to see if a CertificateDefinition can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int CertID)
        {
            return true;
        }



        /// <summary>
        /// Returns a CertificateDefinition object filled with the data taken from the input CertificateDefinitionInfo
        /// </summary>
        private static CertificateDefinition GetCertificateDefinitionFromCertificateDefinitionInfo(CertificateDefinitionInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new CertificateDefinition(record.CertID, record.CertName, record.CertBody,
                    record.CertType, record.FacilityID);
            }
        }

        /// <summary>
        /// Returns a list of CertificateDefinition objects filled with the data taken from the input list of CertificateDefinitionInfo
        /// </summary>
        private static List<CertificateDefinition> GetCertificateDefinitionListFromCertificateDefinitionInfoList(List<CertificateDefinitionInfo> recordset)
        {
            List<CertificateDefinition> CertificateDefinitions = new List<CertificateDefinition>();
            foreach (CertificateDefinitionInfo record in recordset)
                CertificateDefinitions.Add(GetCertificateDefinitionFromCertificateDefinitionInfo(record));
            return CertificateDefinitions;
        }

        #endregion
    }
}
