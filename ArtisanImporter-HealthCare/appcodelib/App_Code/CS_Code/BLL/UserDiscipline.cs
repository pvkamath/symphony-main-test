﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using System.Collections.Generic;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for UserDiscipline
    /// </summary>
    /// 

    public class UserDiscipline:BasePR
    {
        #region Variables and Properties

        private int _userdspid = 0;
        public int userdspid
        {
            get { return _userdspid; }
            protected set { _userdspid = value; }
        }
        public int _userid = 0;
        public int userid
        {
            get { return _userid; }
            set { _userid = value; }
        }
        public int _disciplineid = 0;
        public int disciplineid
        {
            get { return _disciplineid; }
            set { _disciplineid = value; }
        }
        public bool _primary_ind = false;
        public bool primary_ind
        {
            get { return _primary_ind; }
            set { _primary_ind = value; }
        }
        public UserDiscipline(int userdspid, int userid, int disciplineid, bool primary_ind)
        {
            this.userdspid = userdspid;
            this.userid = userid;
            this.disciplineid = disciplineid;
            this.primary_ind = primary_ind;
        }
       
        # endregion

        #region Methods


        /// <summary>
        /// Creates a new UserDiscipline
        /// </summary>
        public static int InsertUserDiscipline(int userdspid, int userid, int disciplineid, bool primary_ind)
        {

            UserDisciplineInfo record = new UserDisciplineInfo(0, userid, disciplineid, primary_ind);
            int ret = SiteProvider.PR2.InsertUserDiscipline(record);
            BizObject.PurgeCacheItems("UserDisciplines_UserDiscipline");
            return ret;
        }


        /// <summary>
        /// Updates an existing UserDiscipline
        /// </summary>
        public static bool UpdateUserDiscipline(int userdspid, int userid, int disciplineid, bool primary_ind)
        {
            UserDisciplineInfo record = new UserDisciplineInfo(userdspid, userid, disciplineid, primary_ind);
            bool ret = SiteProvider.PR2.UpdateUserDiscipline(record);
            BizObject.PurgeCacheItems("UserDiscipline" + userdspid.ToString());
            return ret;
        }


        /// <summary>
        /// Returns a collection with all UserDisciplines for the specified UserID
        /// </summary>
        public static List<UserDiscipline> GetUserDisciplinesByUserID(int UserID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "primary_ind";

            List<UserDiscipline> UserDisciplines = null;
            string key = "UserDisciplines_UserDisciplineInfo_" + UserID.ToString() + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserDisciplines = (List<UserDiscipline>)BizObject.Cache[key];
            }
            else
            {
                List<UserDisciplineInfo> recordset = SiteProvider.PR2.GetUserDisciplinesByUserID(UserID, cSortExpression);
                UserDisciplines = GetUserDisciplineListFromUserDisciplineInfoList(recordset);
                BasePR.CacheData(key, UserDisciplines);
            }
            return UserDisciplines;
        }

        public static UserDiscipline GetUserDisciplinesByUserIDAndDisciplineID(int UserID, int Disciplineid)
        {
            UserDiscipline UserDiscipline = null;
            string key = "UserDisciplines_UserDiscipline_" + Disciplineid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserDiscipline = (UserDiscipline)BizObject.Cache[key];
            }
            else
            {
                UserDiscipline = GetUserDisciplineFromUserDisciplineInfo(SiteProvider.PR2.GetUserDisciplinesByUserIDAndDisciplineID(UserID, Disciplineid));
                BasePR.CacheData(key, UserDiscipline);
            }
            return UserDiscipline;
        }

        public static UserDiscipline GetUserDisciplinesByUserDsspID(int UserDsspID)
        {
            UserDiscipline UserDiscipline = null;
            string key = "UserDisciplines_UserDiscipline_" + UserDsspID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                UserDiscipline = (UserDiscipline)BizObject.Cache[key];
            }
            else
            {
                UserDiscipline = GetUserDisciplineFromUserDisciplineInfo(SiteProvider.PR2.GetUserDisciplinesByUserDsspID(UserDsspID));
                BasePR.CacheData(key, UserDiscipline);
            }
            return UserDiscipline;
        } 


        /// <summary>
        /// Returns a AreaType object filled with the data taken from the input AreaTypeInfo
        /// </summary>
        private static UserDiscipline GetUserDisciplineFromUserDisciplineInfo(UserDisciplineInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new UserDiscipline(record.userdspid, record.userid, record.disciplineid, record.primary_ind);
            }
        }

        /// <summary>
        /// Returns a list of AreaType objects filled with the data taken from the input list of AreaTypeInfo
        /// </summary>
        private static List<UserDiscipline> GetUserDisciplineListFromUserDisciplineInfoList(List<UserDisciplineInfo> recordset)
        {
            List<UserDiscipline> UserDisciplines = new List<UserDiscipline>();
            foreach (UserDisciplineInfo record in recordset)
                UserDisciplines.Add(GetUserDisciplineFromUserDisciplineInfo(record));
            return UserDisciplines;
        }


         /// <summary>
        /// Deletes an existing UserDisciplineId
        /// </summary>
        public static bool DeleteAllUserDisciplinesByUserId(int UserId)
        {
            bool ret = SiteProvider.PR2.DeleteAllUserDisciplinesByUserId(UserId);
                       return ret;
        }

        /// <summary>
        /// Deletes an existing UserId and Disciplineid
        /// </summary>
        public static bool DeleteUserDisciplineByUserdspid(int userdspid)
        {
            bool ret = SiteProvider.PR2.DeleteUserDisciplineByUserdspid(userdspid);
            return ret;
        }

        # endregion

    }
}
