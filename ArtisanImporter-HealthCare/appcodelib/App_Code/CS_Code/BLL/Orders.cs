﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr1
    /// </summary>
    public class Orders : BasePR
    {
        public Orders()
        { }


        #region Fields and Properties
        private int _OrderID = 0;
        public int OrderID
        {
            get { return _OrderID; }
            private set { _OrderID = value; }
        }

        private int _UserID = 0;
        public int UserID
        {
            get { return _UserID; }
            private set { _UserID = value; }
        }

        private string _FirstName = "";
        public string FirstName
        {
            get { return _FirstName; }
            private set { _FirstName = value; }
        }

        private string _LastName = "";
        public string LastName
        {
            get { return _LastName; }
            private set { _LastName = value; }
        }

        private string _Address1 = "";
        public string Address1
        {
            get { return _Address1; }
            private set { _Address1 = value; }
        }


        private string _Address2 = "";
        public string Address2
        {
            get { return _Address2; }
            private set { _Address2 = value; }
        }

        private string _City = "";
        public string City
        {
            get { return _City; }
            private set { _City = value; }
        }

        private string _State = "";
        public string State
        {
            get { return _State; }
            private set { _State = value; }
        }

        private string _Zip = "";
        public string Zip
        {
            get { return _Zip; }
            private set { _Zip = value; }
        }


        private string _Country = "";
        public string Country
        {
            get { return _Country; }
            private set { _Country = value; }
        }


        private string _Email = "";
        public string Email
        {
            get { return _Email; }
            private set { _Email = value; }
        }

        private decimal _SubTotal = 0;
        public decimal SubTotal
        {
            get { return _SubTotal; }
            set { _SubTotal = value; }
        }

        private decimal _ShipCost = 0;
        public decimal ShipCost
        {
            get { return _ShipCost; }
            set { _ShipCost = value; }
        }

        private decimal _Tax = 0;
        public decimal Tax
        {
            get { return _Tax; }
            set { _Tax = value; }
        }

        private decimal _TotalCost = 0;
        public decimal TotalCost
        {
            get { return _TotalCost; }
            set { _TotalCost = value; }
        }

        private bool _Validate_Ind = false;
        public bool Validate_Ind
        {
            get { return _Validate_Ind; }
            set { _Validate_Ind = value; }
        }

        private string _Verification = "";
        public string Verification
        {
            get { return _Verification; }
            private set { _Verification = value; }
        }

        private bool _Ship_Ind = false;
        public bool Ship_Ind
        {
            get { return _Ship_Ind; }
            set { _Ship_Ind = value; }
        }

        private DateTime _OrderDate = System.DateTime.MinValue;
        public DateTime OrderDate
        {
            get { return _OrderDate; }
            set { _OrderDate = value; }
        }


        private string _Comment = "";
        public string Comment
        {
            get { return _Comment; }
            private set { _Comment = value; }
        }

        private int _CouponId = 0;
        public int CouponId
        {
            get { return _CouponId; }
            private set { _CouponId = value; }
        }

        private decimal _Couponamount = 0;
        public decimal Couponamount
        {
            get { return _Couponamount; }
            set { _Couponamount = value; }
        }

        private int _facilityId = 0;
        public int facilityId
        {
            get { return _facilityId; }
            set { _facilityId = value; }

        }
        #endregion

        public Orders(int OrderID, int UserID, string FirstName, string LastName, string Address1, string Address2,
        string City, string State, string Zip, string Country, string Email, decimal SubTotal, decimal ShipCost, decimal Tax, decimal TotalCost, bool Validate_Ind,
        string Verification, bool Ship_Ind, DateTime OrderDate, string Comment, int CouponId, decimal Couponamount, int facilityId)
        {

            this.OrderID = OrderID;
            this.UserID = UserID;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Address1 = Address1.ToString();
            this.Address2 = Address2;
            this.City = City;
            this.State = State;
            this.Zip = Zip;
            this.Country = Country;
            this.Email = Email;
            this.SubTotal = SubTotal;
            this.ShipCost = ShipCost;
            this.Tax = Tax;
            this.TotalCost = TotalCost;
            this.Validate_Ind = Validate_Ind;
            this.Verification = Verification;
            this.Ship_Ind = Ship_Ind;
            this.OrderDate = OrderDate;
            this.Comment = Comment;
            this.CouponId = CouponId;
            this.Couponamount = Couponamount;
            this.facilityId = facilityId;

        }

        public bool Update()
        {
            return Orders.UpdateOrders(this.OrderID, this.UserID, this.FirstName, this.LastName, this.Address1, this.Address2,
        this.City, this.State, this.Zip, this.Country, this.Email, this.SubTotal, this.ShipCost, this.Tax, this.TotalCost, this.Validate_Ind,
        this.Verification, this.Ship_Ind, this.OrderDate, this.Comment, this.CouponId, this.Couponamount,this.facilityId);
        }

        public bool Delete()
        {
            bool success = Orders.DeleteOrders(this.OrderID);
            if (success)
                this.OrderID = 0;
            return success;

        }


        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all Orders
        /// </summary>
        public static List<Orders> GetOrderbyDate(string cSortExpression, string selDate, bool IsPending, bool isShipped)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "orderid";

            List<Orders> Orders = null;
            string key = "Orders_Orders" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Orders = (List<Orders>)BizObject.Cache[key];
            }
            else
            {
                // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<OrdersInfo> recordset = SiteProvider.PR2.GetOrdersbyDate(cSortExpression, selDate, IsPending, isShipped);
                Orders = GetOrdersListFromOrdersInfoList(recordset);
                BasePR.CacheData(key, Orders);
            }
            return Orders;
        }



        /// <summary>
        /// Returns a collection with all Orders
        /// </summary>
        public static List<Orders> GetOrders(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "orderid";

            List<Orders> Orders = null;
            string key = "Orders_Orders" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Orders = (List<Orders>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<OrdersInfo> recordset = SiteProvider.PR2.GetOrders(cSortExpression);
                Orders = GetOrdersListFromOrdersInfoList(recordset);
                BasePR.CacheData(key, Orders);
            }
            return Orders;
        }

        /// <summary>
        /// Returns a collection with all Orders
        /// </summary>
        public static List<Orders> GetPendingOrders(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "orderid";

            List<Orders> Orders = null;
            string key = "Orders_Orders" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Orders = (List<Orders>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<OrdersInfo> recordset = SiteProvider.PR2.GetPendingOrders(cSortExpression);
                Orders = GetOrdersListFromOrdersInfoList(recordset);
                BasePR.CacheData(key, Orders);
            }
            return Orders;
        }

        /// <summary>
        /// Returns a collection with all Orders
        /// </summary>
        public static List<Orders> GetShippedOrders(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "orderid";

            List<Orders> Orders = null;
            string key = "Orders_Orders" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Orders = (List<Orders>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<OrdersInfo> recordset = SiteProvider.PR2.GetShippedOrders(cSortExpression);
                Orders = GetOrdersListFromOrdersInfoList(recordset);
                BasePR.CacheData(key, Orders);
            }
            return Orders;
        }

        /// <summary>
        /// Returns Orders object with the specified USERID
        /// </summary>
        public static Orders GetOrdersbyUserID(int userid)
        {
            List<Orders> Orders = null;
            string key = "Orders_Orders" + userid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Orders = (List<Orders>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<OrdersInfo> recordset = (SiteProvider.PR2.GetOrdersbyUserID(userid));

                Orders = GetOrdersListFromOrdersInfoList(recordset);
                BasePR.CacheData(key, Orders);
            }
            return Orders[0];

        }

        public static List<Orders> GetAllOrdersbyUserID(int userid)
        {
            List<Orders> Orders = null;
            string key = "Orders_Orders" + userid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Orders = (List<Orders>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<OrdersInfo> recordset = (SiteProvider.PR2.GetOrdersbyUserID(userid));

                Orders = GetOrdersListFromOrdersInfoList(recordset);
                BasePR.CacheData(key, Orders);
            }
            return Orders;

        }

        public static List<Orders> GetAllMSOrdersbyUserID(int userid)
        {
            List<Orders> Orders = null;
            string key = "Orders_MSOrders" + userid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Orders = (List<Orders>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<OrdersInfo> recordset = (SiteProvider.PR2.GetMSOrdersbyUserID(userid));

                Orders = GetOrdersListFromOrdersInfoList(recordset);
                BasePR.CacheData(key, Orders);
            }
            return Orders;

        }

        //added for ahDomain

        public static List<Orders> GetAllMSOrdersbyUserIDAndAhDomainId(int userid, int ahDomainId)
        {
            List<Orders> Orders = null;
            string key = "Orders_MSOrders" + userid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Orders = (List<Orders>)BizObject.Cache[key];
            }
            else
            {    
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<OrdersInfo> recordset = (SiteProvider.PR2.GetMSOrdersbyUserIDAndAhDomainId(userid, ahDomainId));

                Orders = GetOrdersListFromOrdersInfoList(recordset);
                BasePR.CacheData(key, Orders);
            }
            return Orders;

        }


        public static List<Orders> GetAllRetailOrdersbyUserID(int userid)
        {
            List<Orders> Orders = null;
            string key = "Orders_RetailOrders" + userid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Orders = (List<Orders>)BizObject.Cache[key];
            }
            else
            {
                //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                List<OrdersInfo> recordset = (SiteProvider.PR2.GetRetailOrdersbyUserID(userid));

                Orders = GetOrdersListFromOrdersInfoList(recordset);
                BasePR.CacheData(key, Orders);
            }
            return Orders;

        }

        /// <summary>
        /// Returns Orders object with the specified ID
        /// </summary>
        public static Orders GetOrdersByID(int OrderID)
        {
            List<Orders> Orders = null;
            string key = "Orders_Orders" + OrderID.ToString();


            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                Orders = (List<Orders>)BizObject.Cache[key];
            }
            else
            {
                List<OrdersInfo> recordset = new List<OrdersInfo>();
                // PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
                OrdersInfo recordsetinfo = (SiteProvider.PR2.GetOrdersByID(OrderID));

                recordset.Add(recordsetinfo);
                Orders = GetOrdersListFromOrdersInfoList(recordset);
                //Orders = obj.GetOrdersByID(OrderID);
                BasePR.CacheData(key, Orders);
            }
            return Orders[0];
        }



        /// <summary>
        /// Creates new orders
        /// </summary>
        public static int InsertOrders(int OrderID, int UserID, string FirstName, string LastName, string Address1, string Address2,
        string City, string State, string Zip, string Country, string Email, decimal SubTotal, decimal ShipCost, decimal Tax, decimal TotalCost, bool Validate_Ind,
        string Verification, bool Ship_Ind, DateTime OrderDate, string Comment, int CouponId, decimal Couponamount, int facilityId)
        {
            OrdersInfo record = new OrdersInfo(0, UserID, FirstName, LastName, Address1, Address2,
            City, State, Zip, Country, Email, SubTotal, ShipCost, Tax, TotalCost, Validate_Ind,
            Verification, Ship_Ind, OrderDate, Comment, CouponId, Couponamount,facilityId);

            int ret = SiteProvider.PR2.InsertOrders(record);

            BizObject.PurgeCacheItems("Orders_Orders");
            return ret;
        }


        /// <summary>
        /// Updates an existing Order
        /// </summary>
        public static bool UpdateOrders(int OrderID, int UserID, string FirstName, string LastName, string Address1, string Address2,
        string City, string State, string Zip, string Country, string Email, decimal SubTotal, decimal ShipCost, decimal Tax, decimal TotalCost, bool Validate_Ind,
        string Verification, bool Ship_Ind, DateTime OrderDate, string Comment, int CouponId, decimal Couponamount,int facilityId)
        {
            OrdersInfo record = new OrdersInfo(OrderID, UserID, FirstName, LastName, Address1, Address2,
           City, State, Zip, Country, Email, SubTotal, ShipCost, Tax, TotalCost, Validate_Ind,
           Verification, Ship_Ind, OrderDate, Comment, CouponId, Couponamount,facilityId);

            //PearlsReview.DAL.SQLClient.SQLPRProvider1 obj = new PearlsReview.DAL.SQLClient.SQLPRProvider1();
            SiteProvider.PR2.UpdateOrders(record);
            return false;

            //bool ret = SiteProvider1.PR.UpdateOrders(record);

            //BizObject.PurgeCacheItems("Orders_Orders" + OrderID.ToString());
            //BizObject.PurgeCacheItems("Orders_Orders");
            //return ret;
        }


        /// <summary>
        /// Deletes an existing Order, but first checks if OK to delete
        /// </summary>
        public static bool DeleteOrders(int OrdersID)
        {
            bool IsOKToDelete = OKToDelete(OrdersID);
            if (IsOKToDelete)
            {
                return (bool)DeleteOrders(OrdersID, true);
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Deletes an existing Orders - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteOrders(int OrdersID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteOrders(OrdersID);
            BizObject.PurgeCacheItems("Orders_Orders");
            return ret;
        }

        /// <summary>
        /// Checks to see if a Orders can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }



        /// <summary>
        /// Returns a Discount object filled with the data taken from the input DiscountInfo
        /// </summary>
        private static Orders GetOrdersFromOrdersInfo(OrdersInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new Orders(record.OrderID, record.UserID, record.FirstName, record.LastName, record.Address1, record.Address2,
           record.City, record.State, record.Zip, record.Country, record.Email, record.SubTotal, record.ShipCost, record.Tax, record.TotalCost, record.Validate_Ind,
           record.Verification, record.Ship_Ind, record.OrderDate, record.Comment, record.CouponID, record.CouponAmount,record.facilityId);
            }
        }

        /// <summary>
        /// Returns a list of Orders objects filled with the data taken from the input list of DiscountInfo
        /// </summary>
        private static List<Orders> GetOrdersListFromOrdersInfoList(List<OrdersInfo> recordset)
        {
            List<Orders> Orders = new List<Orders>();
            foreach (OrdersInfo record in recordset)
                Orders.Add(GetOrdersFromOrdersInfo(record));
            return Orders;
        }

        public static void UpdateOrdersForOrderID(OrdersInfo Orders, int OrderID)
        {
            SiteProvider.PR2.UpdateOrdersForOrderID(Orders, OrderID);
        }


    }
}
