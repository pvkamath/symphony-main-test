﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

    /// <summary>
    /// Summary description for FacilityAreaLink
    /// </summary>
    public class FacilityAreaLink : BasePR
    {
        #region Variables and Properties

        private int _facilityid = 0;
        public int facilityid
        {
            get { return _facilityid; }
            protected set { _facilityid = value; }
        }

        private int _areaid = 0;
        public int areaid
        {
            get { return _areaid; }
            set { _areaid = value; }
        }

        public FacilityAreaLink(int facilityid, int areaid)
        {
            this.facilityid = facilityid;
            this.areaid = areaid;
        }
        #endregion

        #region Methods

        public static List<FacilityAreaLink> GetFacilityAreaLinkByFacilityId(int facilityid)
        {
            List<FacilityAreaLink> FacilityAreaLinks = null;

            List<FacilityAreaLinkInfo> recordset = SiteProvider.PR2.GetFacilityAreaLinkByFacilityId(facilityid);
            FacilityAreaLinks = GetFacilityAreaLinkListFromGetFacilityAreaLinkInfoList(recordset);
            return FacilityAreaLinks;

        }

        private static List<FacilityAreaLink> GetFacilityAreaLinkListFromGetFacilityAreaLinkInfoList(List<FacilityAreaLinkInfo> recordset)
        {
            List<FacilityAreaLink> FacilityAreaLinks = new List<FacilityAreaLink>();
            foreach (FacilityAreaLinkInfo record in recordset)
                FacilityAreaLinks.Add(GetFacilityAreaLinkFromFacilityAreaLink(record));
            return FacilityAreaLinks;
        }

        private static FacilityAreaLink GetFacilityAreaLinkFromFacilityAreaLink(FacilityAreaLinkInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new FacilityAreaLink(record.facilityid, record.areaid);
            }
        }

        public static int UpdateFacilityAreaLink(int facilityid, int areaid)
        {
            FacilityAreaLinkInfo record = new FacilityAreaLinkInfo(facilityid, areaid);
            int ret = SiteProvider.PR2.UpdateFacilityAreaLink(record);
            BizObject.PurgeCacheItems("FacilityAreaLinks_FacilityAreaLink_" + facilityid.ToString());
            BizObject.PurgeCacheItems("FacilityAreaLinks_FacilityAreaLinks");
            return ret;
        }


        public static bool DeleteFacilityAreaLink(int facilityid, int areaid)
        {

            bool ret = SiteProvider.PR2.DeleteFacilityAreaLink(facilityid, areaid);
            BizObject.PurgeCacheItems("FacilityAreaLinks_FacilityAreaLink");
            return ret;
        }
        #endregion
    }
}
