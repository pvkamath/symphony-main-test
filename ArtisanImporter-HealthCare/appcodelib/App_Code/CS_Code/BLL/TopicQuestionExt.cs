﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;
using PearlsReview.QTI;
using PearlsReview.Constants;
using System.Reflection;
using System.Data.SqlClient;


namespace PearlsReview.BLL
{
    public class MigrationResults
    {
        public MigrationResults()
        {
            Log = new List<LogItem>();
        }

        public int TotalCount { get; set; }
        public int TotalSucceeded { get; set; }
        public int TotalFailed { get; set; }
        public List<LogItem> Log { get; set; }
    }
    public class LogItem
    {
        public string Course { get; set; }
        public int QuestionNumber { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }

    public partial class TopicQuestion : BasePR
    {
        public static bool InsertNewTopicQuestionsFromList(IEnumerable<TopicQuestion> TopicQuestions)
        {

            List<QTIQuestionObject> QTIQuestions = new List<QTIQuestionObject>();

            int iTopicID = 0;

            // check the count to be sure it's 6 records
            if (TopicQuestions.Count() > 0)
            {
                // 6 recs found, so put the info into 6
                // QTIQuestionObject objects in the QTIQuestions list
                int iOrder = 0;
                //string cCorrectAns = "";
                foreach (TopicQuestion record in TopicQuestions)
                {

                    iOrder++;
                    //Bsk New
                    //iOrder = record.QuestionNum;
                    if (iOrder == 1)
                        iTopicID = record.TopicID;

                    QTIQuestionObject QTIQuestion = new QTIQuestionObject();

                    QTIQuestion.cQuestionID = "Topic_" + iTopicID.ToString() + "_Question_" + iOrder.ToString();
                    QTIQuestion.cQuestionTitle = "PearlsReview.com Question # " + iOrder.ToString() +
                        " for Topic # " + iTopicID.ToString();

                    //Bsk New 
                    QTIQuestion.cHeaderID = record.HeaderNum.ToString();

                    // TODO: Make this dynamic instead of 2 MC and 4 TF
                    if (record.Answer_C == "")
                    {
                        QTIQuestion.cResponseID = "TF_0" + iOrder.ToString();

                    }
                    else
                    {
                        QTIQuestion.cResponseID = "MC_0" + iOrder.ToString();
                    }

                    QTIQuestion.cQuestionText = record.Question;
                    QTIQuestion.cAnswer_A_Text = record.Answer_A;
                    QTIQuestion.cAnswer_B_Text = record.Answer_B;
                    QTIQuestion.cAnswer_C_Text = record.Answer_C;
                    QTIQuestion.cAnswer_D_Text = record.Answer_D;
                    QTIQuestion.cAnswer_E_Text = record.Answer_E;
                    QTIQuestion.cAnswer_F_Text = record.Answer_F;
                    if (record.A_Correct == true)
                        QTIQuestion.cCorrectAnswer = "A";
                    if (record.B_Correct == true)
                        QTIQuestion.cCorrectAnswer = "B";
                    if (record.C_Correct == true)
                        QTIQuestion.cCorrectAnswer = "C";
                    if (record.D_Correct == true)
                        QTIQuestion.cCorrectAnswer = "D";
                    if (record.E_Correct == true)
                        QTIQuestion.cCorrectAnswer = "E";
                    if (record.F_Correct == true)
                        QTIQuestion.cCorrectAnswer = "F";
                    QTIQuestion.cCorrectAnswerFeedback = record.Discussion;
                    QTIQuestion.cUserAnswer = "";

                    QTIQuestions.Add(QTIQuestion);

                }


            }

            string cXML = "";
            bool ret = false;
            QTIUtils oQTIUtils = new QTIUtils();
            MockTestDefinition oTestDefinitionID = MockTestDefinition.GetMockTestDefinitionIDByTopicID(iTopicID);
            //List<QTIQuestionObject> existingQs;
            //if (oTestDefinitionID != null)
            //{
            //    existingQs = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinitionID.XMLTest);                
            //}
            //else
            //{
            //    existingQs = new List<QTIQuestionObject>();
            //}

            //existingQs.AddRange(QTIQuestions);

            if (QTIQuestions.Count > 0 && iTopicID > 0)
            {

                cXML = oQTIUtils.ConvertQTIQuestionObjectListToQTITestXMLString(QTIQuestions);
                if (oTestDefinitionID == null)
                {
                    // inserting new record
                    int newID = MockTestDefinition.InsertTestDefinition(iTopicID, cXML);
                    if (newID > 0)
                        ret = true;
                }
                else
                {
                    // updating existing record
                    ret = MockTestDefinition.UpdateTestDefinition(oTestDefinitionID.ID, iTopicID, cXML);
                }
            }

            return ret;
        }

        public static MigrationResults MigrateData(DataTable ExternalTopicQuestionData, ITestMigrationStrategy strategy)
        {
            MigrationResults results;
            List<TopicQuestion> AllTopicsQuestions = strategy.MapTo<TopicQuestion>(ExternalTopicQuestionData, out results).ToList<TopicQuestion>();
            WriteMigrationData(AllTopicsQuestions);
            results.TotalCount = ExternalTopicQuestionData.Rows.Count;
            var distinctTopics = AllTopicsQuestions.Select(x => x.TopicID).Distinct();

            foreach (var item in distinctTopics)
            {
                InsertNewTopicQuestionsFromList(AllTopicsQuestions.Where(x => x.TopicID == item));
            }
            return results;
        }
        private static void WriteMigrationData(List<TopicQuestion> migrationdata)
        {
            using (SqlConnection cn = new SqlConnection("Server=ghg-va-lmssql;Database=ECET;User=lmsuser;Password=lmsuser;"))
            {
                cn.Open();
                SqlCommand delcmd = new SqlCommand("delete FROM KangQuestionAnswer", cn);
                delcmd.ExecuteNonQuery();

                foreach (TopicQuestion item in migrationdata)
                {
                    SqlCommand cmd = new SqlCommand(
                        "insert into KangQuestionAnswer " +
                        "(TopicID,QuestionID, " +
                        "AnswerA,AnswerB,AnswerC,AnswerD,AnswerE,AnswerF,CorrectAnswer,Question) " +
                        "VALUES (" +
                        "@TopicID,@QuestionID," +
                        "@AnswerA,@AnswerB,@AnswerC,@AnswerD,@AnswerE,@AnswerF,@CorrectAnswer,@Question)", cn);

                    cmd.Parameters.Add("@TopicID", SqlDbType.Int).Value = item.TopicID;
                    cmd.Parameters.Add("@QuestionID", SqlDbType.Int).Value = item.QuestionNum;
                    cmd.Parameters.Add("@AnswerA", SqlDbType.VarChar).Value = item.Answer_A;
                    cmd.Parameters.Add("@AnswerB", SqlDbType.VarChar).Value = item.Answer_B;
                    cmd.Parameters.Add("@AnswerC", SqlDbType.VarChar).Value = item.Answer_C;
                    cmd.Parameters.Add("@AnswerD", SqlDbType.VarChar).Value = item.Answer_D;
                    cmd.Parameters.Add("@AnswerE", SqlDbType.VarChar).Value = item.Answer_E;
                    cmd.Parameters.Add("@AnswerF", SqlDbType.VarChar).Value = item.Answer_F;
                    cmd.Parameters.Add("@CorrectAnswer", SqlDbType.VarChar).Value =
                        item.GetType().GetProperties()
                        .Where(x => x.Name.EndsWith("_Correct"))
                        .Where(x => (Boolean)x.GetValue(item, null) == true)
                        .First().Name.Remove(1);
                    cmd.Parameters.Add("@Question", SqlDbType.VarChar).Value = item.Question;
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public partial class NurseTestDataMigrationStrategy : ITestMigrationStrategy
    {
        private bool ResolveCorrectAns(string TQProp, object value)
        {
            return TQProp.Replace("_Correct", "").ToLower().Equals(value.ToString().ToLower());
        }
        private string ResolveAns(string ans_prop, string Answers)
        {
            Answers = Answers.Replace("", "");
            string[] QAnswers = Answers.ToString().Split(new string[] { "~", "~", "" }, StringSplitOptions.RemoveEmptyEntries);
            string selector = ans_prop.Replace("Answer_", "").ToLower();
            var match = QAnswers.Select(x => x.Trim()).Where(x => x.ToLower().StartsWith(selector + "."))
                .Union(QAnswers.Select(x => x.Trim()).Where(x => x.ToLower().StartsWith(selector + ")")));
            return match.Count() > 0 ? FormatChecker(match.First()).TrimStart().Remove(0, 2).Trim().CleanupString()
                                       : "";
        }

        private string FormatChecker(string answer)
        {
            if (answer.TrimStart().Remove(0, 2).StartsWith("."))
                return "";
            else if (answer.TrimStart().Remove(0, 2).StartsWith(")"))
                return "";
            else
                return answer;
        }
        #region IMigrationStrategy<T> Members

        public IList<T> MapTo<T>(DataTable ExternalData, out MigrationResults results) where T : class, new()
        {
            results = new MigrationResults();
            results.TotalCount = 0;
            results.TotalFailed = 0;
            results.TotalSucceeded = 0;
            IList<T> questiondata = new List<T>();
            PropertyInfo[] info = (typeof(T)).GetProperties();
            var prop_correctans = info.Where(x => x.Name.Contains("_Correct"));
            var prop_ans = info.Where(x => x.Name.Contains("Answer_"));
            DataTable mapper = SQL2PRProvider.Instance
                .GetTopicIDsByCourseNos(ExternalData.AsEnumerable()
                .Select(x => x[TopicConstants.TopicID].ToString())
                .Distinct().ToList());

            foreach (DataRow x in ExternalData.Rows)
            {
                try
                {
                    int answercount = 0;
                    TopicQuestion ques = (new T() as TopicQuestion);

                    var matches = mapper.AsEnumerable().Where(p => p["course_number"].ToString() == x[TopicConstants.TopicID].ToString());
                    if (matches.Count() > 0)
                        ques.TopicID = int.Parse(matches.First()["topicid"].ToString());
                    else
                        throw new Exception("Course Number does not exist in the Destination Database");

                    ques.HeaderNum = Int32.Parse(x[TopicConstants.QuestionNumber].ToString());
                    ques.Question = x[TopicConstants.Question].ToString().CleanupString();
                    ques.QuestionNum = Int32.Parse(x[TopicConstants.QuestionNumber].ToString());
                   
                    foreach (PropertyInfo item in prop_ans)
                    {
                        int tempcount = answercount;
                        item.SetValue(ques, ResolveAns(item.Name, x[TopicConstants.Answers].ToString()), null);
                        if (item.GetValue(ques, null).ToString() != "")
                            answercount++;
                        if (tempcount == answercount)
                        {
                            break;// Answer options cannot be
                        }
                        
                    }
                    foreach (PropertyInfo item in prop_correctans)
                    {
                        item.SetValue(ques, ResolveCorrectAns(item.Name, x[TopicConstants.CorrectAns]), null);
                    }
                    if (answercount < 2)
                    {
                        throw new Exception("Answers are Empty.");
                    }

                    questiondata.Add(ques as T);
                    results.TotalSucceeded++;
                    var logitem = new LogItem
                    {
                        Course = x[TopicConstants.TopicID].ToString(),
                        QuestionNumber = (int)x[TopicConstants.QuestionNumber],
                        Success = true,
                        ErrorMessage = String.Empty
                    };
                    results.Log.Add(logitem);
                }
                catch (Exception e)
                {
                    results.TotalFailed++;
                    var logitem = new LogItem
                    {
                        Course = x[TopicConstants.TopicID].ToString(),
                        QuestionNumber = (int)x[TopicConstants.QuestionNumber],
                        Success = false,
                        ErrorMessage = e.Message
                    };
                    results.Log.Add(logitem);
                }
                questiondata = questiondata
                                .OrderBy(q => (q as TopicQuestion).TopicID)
                                .ThenBy(q => (q as TopicQuestion).QuestionNum).ToList<T>();
            }
            return questiondata;
        }
        #endregion


    }
    public interface ITestMigrationStrategy
    {
        IList<T> MapTo<T>(DataTable ExternalData, out MigrationResults results) where T : class, new();
    }
}
