﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/
using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for TopicCollection
    /// </summary>
    public class TopicCollection: BasePR
    {
        #region Variables and Properties

        private int _TopicID = 0;
        public int TopicID
        {
            get { return _TopicID; }
            set { _TopicID = value; }
        }
        private int _ChapterID = 0;
        public int ChapterID
        {
            get { return _ChapterID; }
            set { _ChapterID = value; }
        }
        private int _ChapterNumber = 0;
        public int ChapterNumber
        {
            get { return _ChapterNumber; }
            set { _ChapterNumber = value; }
        }
        private int _TCID = 0;
        public int TCID
        {
            get { return _TCID; }
            set { _TCID = value; }
        }
        public TopicCollection(int TopicID, int ChapterID, int ChapterNumber, int TCID)
        {
            this.TopicID = TopicID;
            this.ChapterID = ChapterID;
            this.ChapterNumber = ChapterNumber;
            this.TCID = TCID;

        }
        #endregion

        #region Methods

        /// <summary>
        /// Returns a collection with all TopicComplianceLinks for a FacilityID
        /// </summary>
        public static List<TopicCollection> GetTopicCollectionsByTopicID(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<TopicCollection> TopicCollections = null;
            string key = "TopicCollections_TopicCollectionsByTopicID_" +
                TopicID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicCollections = (List<TopicCollection>)BizObject.Cache[key];
            }
            else
            {
                List<TopicCollectionInfo> recordset =
                    SiteProvider.PR2.GetTopicCollectionsByTopicID(TopicID, cSortExpression);
                TopicCollections = GetTopicCollectionListFromTopicCollectionInfoList(recordset);
                BasePR.CacheData(key, TopicCollections);
            }
            return TopicCollections;
        }

        /// <summary>
        /// Returns a TopicComplianceLink object filled with the data taken from the input TopicComplianceLinkInfo
        /// </summary>
        private static TopicCollection GetTopicCollectionFromTopicCollectionInfo(TopicCollectionInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new TopicCollection(record.topicid, record.chapterid, record.chapternumber, record.tcid);
            }
        }

        /// <summary>
        /// Returns a list of TopicComplianceLink objects filled with the data taken from the input list of TopicComplianceLinkInfo
        /// </summary>
        private static List<TopicCollection> GetTopicCollectionListFromTopicCollectionInfoList(List<TopicCollectionInfo> recordset)
        {
            List<TopicCollection> TopicCollections = new List<TopicCollection>();
            foreach (TopicCollectionInfo record in recordset)
                TopicCollections.Add(GetTopicCollectionFromTopicCollectionInfo(record));
            return TopicCollections;
        }


        public static DataSet GetTopicCollectionsDataSetByTopicID(int TopicID, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            DataSet TopicCollections = null;
            string key = "TopicCollectionsds_TopicCollectionsdsByTopicID_" +
                TopicID.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                TopicCollections = (DataSet)BizObject.Cache[key];
            }
            else
            {
                TopicCollections =
                    SiteProvider.PR2.GetTopicCollectionsDataSetByTopicID(TopicID, cSortExpression);
            }
            return TopicCollections;
        }


        public static bool UpdateTopicCollections(int chapterid, int chapternumber, int tcid)
        {
            bool ret = SiteProvider.PR2.UpdateTopicCollections(chapterid, chapternumber, tcid);
            // TODO: release cache?
            return ret;
        }

        /// <summary>
        /// Creates a new TopicComplianceLink
        /// </summary>
        public static int InsertTopicCollection(int topicid, int chapterid, int chapternumber, int tcid)
        {
            TopicCollectionInfo record = new TopicCollectionInfo(topicid, chapterid, chapternumber, tcid);
            int ret = SiteProvider.PR2.InsertTopicCollection(record);

            BizObject.PurgeCacheItems("TopicCollections_TopicCollection");
            return ret;
        }



        /// <summary>
        /// Deletes an existing TopicComplianceLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteTopicColection(int Topicid)
        {
            bool IsOKToDelete = OKToDelete(Topicid);
            if (IsOKToDelete)
            {
                return (bool)DeleteTopicCollection(Topicid, true);
            }
            else
            {
                return false;
            }
        }


        public static bool DeleteTopicCollection(int Topicid, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteTopicCollection(Topicid);
            //         new RecordDeletedEvent("TopicComplianceLink", TopCompID, null).Raise();
            BizObject.PurgeCacheItems("DeleteTopicColections_DeleteTopicColection");
            return ret;
        }



        /// <summary>
        /// Checks to see if a TopicComplianceLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int Topicid)
        {
            return true;

        }
        #endregion
    }
}
