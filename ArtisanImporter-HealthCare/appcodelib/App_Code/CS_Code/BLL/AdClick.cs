﻿/*************************************************************************
 * Gannett Health Group,Inc
 * 
 * File Description:  
 * 
 * Author   :   Bhaskar N
 * 
 * Revision Information:
 * 
 * $NoKeywords: $
 * **********************************************************************/

using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{ 

/// <summary>
/// Summary description for AdClick
/// </summary>
    public class AdClick : BasePR
    {
        #region Variables and Properties
        private int _AdClickID = 0;
        public int AdClickID
        {
            get { return _AdClickID; }
            protected set { _AdClickID = value; }
        }

        private int _AdID = 0;
        public int AdID
        {
            get { return _AdID; }
            set { _AdID = value; }
        }

        private DateTime _Date = System.DateTime.Now;
        public DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }

        private string _PageName = "";
        public string PageName
        {
            get { return _PageName; }
            set { _PageName = value; }
        }


        public AdClick(int AdClickID, int AdID, DateTime Date, string PageName)
        {
            this.AdClickID = AdClickID;
            this.AdID = AdID;
            this.Date = Date;
            this.PageName = PageName;
        }

        public bool Delete()
        {
            bool success = AdClick.DeleteAdClick(this.AdClickID);
            if (success)
                this.AdClickID = 0;
            return success;
        }

        public bool Update()
        {
            return AdClick.UpdateAdClick(this.AdClickID, this.AdID, this.Date, this.PageName);
        }

        #endregion

        #region Methods

        /***********************************
        * Static methods
        ************************************/

        /// <summary>
        /// Returns a collection with all AdClicks
        /// </summary>
        public static List<AdClick> GetAdClicks(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "PageName";

            List<AdClick> AdClicks = null;
            string key = "AdClicks_AdClicks_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AdClicks = (List<AdClick>)BizObject.Cache[key];
            }
            else
            {
                List<AdClickInfo> recordset = SiteProvider.PR2.GetAdClicks(cSortExpression);
                AdClicks = GetAdClickListFromAdClickInfoList(recordset);
                BasePR.CacheData(key, AdClicks);
            }
            return AdClicks;
        }


        /// <summary>
        /// Returns the number of total AdClicks
        /// </summary>
        public static int GetAdClickCount()
        {
            int AdClickCount = 0;
            string key = "AdClicks_AdClickCount";

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AdClickCount = (int)BizObject.Cache[key];
            }
            else
            {
                AdClickCount = SiteProvider.PR2.GetAdClickCount();
                BasePR.CacheData(key, AdClickCount);
            }
            return AdClickCount;
        }

        /// <summary>
        /// Returns a AdClick object with the specified ID
        /// </summary>
        public static AdClick GetAdClickByID(int AdClickID)
        {
            AdClick AdClick = null;
            string key = "AdClicks_AdClick_" + AdClickID.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                AdClick = (AdClick)BizObject.Cache[key];
            }
            else
            {
                AdClick = GetAdClickFromAdClickInfo(SiteProvider.PR2.GetAdClickByID(AdClickID));
                BasePR.CacheData(key, AdClick);
            }
            return AdClick;
        }

        /// <summary>
        /// Updates an existing AdClick
        /// </summary>
        public static bool UpdateAdClick(int AdClickID, int AdID, DateTime Date, string PageName)
        {
            PageName = BizObject.ConvertNullToEmptyString(PageName);


            AdClickInfo record = new AdClickInfo(AdClickID, AdID, Date, PageName);
            bool ret = SiteProvider.PR2.UpdateAdClick(record);

            BizObject.PurgeCacheItems("AdClicks_AdClick_" + AdClickID.ToString());
            BizObject.PurgeCacheItems("AdClicks_AdClicks");
            return ret;
        }

        /// <summary>
        /// Creates a new AdClick
        /// </summary>
        public static int InsertAdClick(int AdID, DateTime Date, string PageName)
        {
            PageName = BizObject.ConvertNullToEmptyString(PageName);


            AdClickInfo record = new AdClickInfo(0, AdID, Date, PageName);
            int ret = SiteProvider.PR2.InsertAdClick(record);

            BizObject.PurgeCacheItems("AdClicks_AdClick");
            return ret;
        }

        /// <summary>
        /// Deletes an existing AdClick, but first checks if OK to delete
        /// </summary>
        public static bool DeleteAdClick(int AdClickID)
        {
            bool IsOKToDelete = OKToDelete(AdClickID);
            if (IsOKToDelete)
            {
                return (bool)DeleteAdClick(AdClickID, true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing AdClick - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteAdClick(int AdClickID, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteAdClick(AdClickID);
            //         new RecordDeletedEvent("AdClick", AdClickID, null).Raise();
            BizObject.PurgeCacheItems("AdClicks_AdClick");
            return ret;
        }



        /// <summary>
        /// Checks to see if a AdClick can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int AdClickID)
        {
            return true;
        }



        /// <summary>
        /// Returns a AdClick object filled with the data taken from the input AdClickInfo
        /// </summary>
        private static AdClick GetAdClickFromAdClickInfo(AdClickInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new AdClick(record.AdClickID, record.AdID, record.Date, record.PageName);
            }
        }

        /// <summary>
        /// Returns a list of AdClick objects filled with the data taken from the input list of AdClickInfo
        /// </summary>
        private static List<AdClick> GetAdClickListFromAdClickInfoList(List<AdClickInfo> recordset)
        {
            List<AdClick> AdClicks = new List<AdClick>();
            foreach (AdClickInfo record in recordset)
                AdClicks.Add(GetAdClickFromAdClickInfo(record));
            return AdClicks;
        }

        #endregion
    }
}
