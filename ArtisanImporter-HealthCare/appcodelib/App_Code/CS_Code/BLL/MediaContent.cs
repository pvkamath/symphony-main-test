﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;


namespace PearlsReview.BLL
{
    /// <summary>
    /// Summary description for pr2
    /// </summary>

    /// <summary>
    /// /////////Media Content
    /// </summary>
    public class MediaContent : BasePR
    {

        public MediaContent(int mcid, int topicid, string mediatype, string asseturl, string description)
        {
            this.mcid = mcid;
            this.topicid = topicid;
            this.mediatype = mediatype;
            this.asseturl = asseturl;
            this.description = description;
        }

        private int _mcid = 0;
        public int mcid
        {
            get { return _mcid; }
            protected set { _mcid = value; }
        }
        private int _topicid = 0;
        public int topicid
        {
            get { return _topicid; }
            protected set { _topicid = value; }
        }
        private string _mediatype;
        public string mediatype
        {
            get { return _mediatype; }
            protected set { _mediatype = value; }
        }
        private string _asseturl = "";
        public string asseturl
        {
            get { return _asseturl; }
            protected set { _asseturl = value; }
        }
        private string _description = "";
        public string description
        {
            get { return _description; }
            protected set { _description = value; }
        }



        public static bool UpdateMediaContent(int mcid, int topicid, string mediatype, string asseturl, string description)
        {
            MediaContentInfo record = new MediaContentInfo(mcid, topicid, mediatype, asseturl, description);
            bool ret = SiteProvider.PR2.UpdateMediaContent(record);

            BizObject.PurgeCacheItems("MediaContents_MediaContent_" + mcid.ToString());
            BizObject.PurgeCacheItems("MediaContents_MediaContents");
            return ret;
        }

        /// <summary>
        /// Creates a new TopicComplianceLink
        /// </summary>
        public static int InsertMediaContent(int topicid, string mediatype, string asseturl, string description)
        {
            MediaContentInfo record = new MediaContentInfo(0, topicid, mediatype, asseturl, description);
            int ret = SiteProvider.PR2.InsertMediaContent(record);

            BizObject.PurgeCacheItems("MediaContents_MediaContent");
            return ret;
        }

        /// <summary>
        /// Deletes an existing TopicComplianceLink, but first checks if OK to delete
        /// </summary>
        public static bool DeleteMediaContent(int mcid)
        {
            bool IsOKToDelete = OKToDelete(mcid);
            if (IsOKToDelete)
            {
                return (bool)DeleteMediaContent(mcid, true);
            }
            else
            {
                return false;
            }
        }


        public static bool DeleteMediaContent(int mcid, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteMediaContent(mcid);
            //         new RecordDeletedEvent("TopicComplianceLink", TopCompID, null).Raise();
            BizObject.PurgeCacheItems("MediaContents_MediaContent");
            return ret;
        }



        /// <summary>
        /// Checks to see if a TopicComplianceLink can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int mcid)
        {
            return true;

        }


        /// <summary>
        /// Returns a collection with all TopicComplianceLinks for a FacilityID
        /// </summary>
        public static List<MediaContent> GetMediaContentByTopicID(int TopicId, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "";

            List<MediaContent> MediaContents = null;
            string key = "MediaContents_MediaContentByTopicID_" +
                TopicId.ToString() + "_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MediaContents = (List<MediaContent>)BizObject.Cache[key];
            }
            else
            {
                List<MediaContentInfo> recordset =
                    SiteProvider.PR2.GetMediaContentByTopicID(TopicId, cSortExpression);
                MediaContents = GetMediaContentListFromMediaContentInfoList(recordset);
                BasePR.CacheData(key, MediaContents);
            }
            return MediaContents;
        }


        /// <summary>
        /// Returns a TopicComplianceLink object filled with the data taken from the input TopicComplianceLinkInfo
        /// </summary>
        private static MediaContent GetMediaContentFromMediaContentInfo(MediaContentInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new MediaContent(record.mcid, record.topicid, record.mediatype, record.asseturl, record.description);
            }
        }

        /// <summary>
        /// Returns a list of TopicComplianceLink objects filled with the data taken from the input list of TopicComplianceLinkInfo
        /// </summary>
        private static List<MediaContent> GetMediaContentListFromMediaContentInfoList(List<MediaContentInfo> recordset)
        {
            List<MediaContent> MediaContents = new List<MediaContent>();
            foreach (MediaContentInfo record in recordset)
                MediaContents.Add(GetMediaContentFromMediaContentInfo(record));
            return MediaContents;
        }
    }

}