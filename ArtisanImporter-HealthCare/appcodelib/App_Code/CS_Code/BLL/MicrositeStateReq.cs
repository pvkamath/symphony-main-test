﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using PearlsReview.BLL.PRBase;
using PearlsReview.DAL;
using PearlsReview.DAL.SQLClient;

namespace PearlsReview.BLL
{

    /// <summary>
    /// Summary description for MicrositeStateReq
    /// </summary>
    public class MicrositeStateReq : BasePR
    {
        #region Variables and Properties
        private int _msrID = 0;
        public int MsrID
        {
            get { return _msrID; }
            set { _msrID = value; }
        }

        private int _msid = 0;
        public int Msid
        {
            get { return _msid; }
            set { _msid = value; }
        }
        private string _stateabr = "";
        public string Stateabr
        {
            get { return _stateabr; }
            set { _stateabr = value; }
        }
        private string _req_text = "";
        public string Req_text
        {
            get { return _req_text; }
            set { _req_text = value; }
        }
        private string _req_header = "";
        public string Req_header
        {
            get { return _req_header; }
            set { _req_header = value; }
        }
        public MicrositeStateReq()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public MicrositeStateReq(int msrID, int msid, string stateabr, string req_text, string req_header)
        {
            this.MsrID = msrID;
            this.Msid = msid;
            this.Stateabr = stateabr;
            this.Req_text = req_text;
            this.Req_header = req_header;
        }
        public bool Delete()
        {
            bool success = MicrositeStateReq.DeleteMicrositeStateReq(this.Msid, this.Stateabr);
            if (success)
                this.Msid = 0;
            return success;
        }

        public bool Update()
        {
            return MicrositeStateReq.UpdateMicrositeStateReq(this.Msid, this.Stateabr, this.Req_text, this.Req_header);
        }

        #endregion
        #region Methods
        /***********************************
        * Static methods
        ************************************/

        //<summary>
        //Returns a collection with all MicrositeDomains
        //</summary>
        public static List<MicrositeStateReq> GetMicrositeStateReq(string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "req_text";

            List<MicrositeStateReq> MicrositeStateReqs = null;
            string key = "MicrositeStateReqs_MicrositeStateReqs_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeStateReqs = (List<MicrositeStateReq>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeStateReqInfo> recordset = SiteProvider.PR2.GetMicrositeStateReq(cSortExpression);
                MicrositeStateReqs = GetMicrositeStateReqListFromMicrositeStateReqInfoList(recordset);
                BasePR.CacheData(key, MicrositeStateReqs);
            }
            return MicrositeStateReqs;
        }

        /// <summary>
        /// Returns a MicrositeDomains object with the specified ID
        /// </summary>
        public static MicrositeStateReq GetMicrositeStateReqByID(int Msid, string stateabr)
        {
            MicrositeStateReq MicrositeStateReqs = null;
            string key = "MicrositeStateReqs_MicrositeStateReqs_" + Msid.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeStateReqs = (MicrositeStateReq)BizObject.Cache[key];
            }
            else
            {
                MicrositeStateReqs = GetMicrositeStateReqFromMicrositeStateReqInfo(SiteProvider.PR2.GetMicrositeStateReqByID(Msid, stateabr));
                BasePR.CacheData(key, MicrositeStateReqs);
            }
            return MicrositeStateReqs;
        }

        public static List<MicrositeStateReq> GetMicrositeStateReqsByMsid(int Msid, string stateabr)
        {
            return GetMicrositeStateReqListFromMicrositeStateReqInfoList(SiteProvider.PR2.GetMicrositeStateReqsByMsid(Msid, stateabr));
        }
        public static List<MicrositeStateReq> GetMicrositeStateReqByMisd(int Msid, string cSortExpression)
        {
            if (cSortExpression == null)
                cSortExpression = "";

            // provide default sort
            if (cSortExpression.Length == 0)
                cSortExpression = "Stateabr";

            List<MicrositeStateReq> MicrositeStateReqs = null;
            string key = "MicrositeStateReqs_MicrositeStateReqs_" + cSortExpression.ToString();

            if (BasePR.Settings.EnableCaching && BizObject.Cache[key] != null)
            {
                MicrositeStateReqs = (List<MicrositeStateReq>)BizObject.Cache[key];
            }
            else
            {
                List<MicrositeStateReqInfo> recordset = SiteProvider.PR2.GetMicrositeStateReqByMsid(Msid, cSortExpression);
                MicrositeStateReqs = GetMicrositeStateReqListFromMicrositeStateReqInfoList(recordset);
                BasePR.CacheData(key, MicrositeStateReqs);
            }
            return MicrositeStateReqs;
        }
        public static DataTable GetStateReqTexts(int msid, string cSortExpression)
        {
            return SiteProvider.PR2.GetStateReqTexts(msid, cSortExpression);
        }
        public static DataTable GetMicrositeStateReqDTByMsid(int msid, string cSortExpression)
        {
            return SiteProvider.PR2.GetMicrositeStateReqDTByMsid(msid, cSortExpression);
        }
        public static int InsertMicrositeStateReqs(int msid, string stateabr, string req_text, string req_header)
        {
            stateabr = BizObject.ConvertNullToEmptyString(stateabr);
            req_text = BizObject.ConvertNullToEmptyString(req_text);
            req_header = BizObject.ConvertNullToEmptyString(req_header);

            MicrositeStateReqInfo record = new MicrositeStateReqInfo(0, msid, stateabr, req_text, req_header);
            int ret = SiteProvider.PR2.InsertMicrositeStateReq(record);

            BizObject.PurgeCacheItems("MicrositeStateReqs_MicrositeStateReqs");
            return ret;
        }
        public static bool UpdateMicrositeStateReqLists(List<MicrositeStateReq> StateReq, int Msid)
        {
            return SiteProvider.PR2.UpdateMicrositeStateReqLists(GetMicrositeStateReqInfoListFromMicrositeStateReqList(StateReq), Msid);
        }
        /// <summary>
        /// Updates an existing MicrositeDomain
        /// </summary>
        public static bool UpdateMicrositeStateReq(int Msid, string stateabr, string req_text, string req_header)
        {
            stateabr = BizObject.ConvertNullToEmptyString(stateabr);
            req_text = BizObject.ConvertNullToEmptyString(req_text);
            req_header = BizObject.ConvertNullToEmptyString(req_header);

            MicrositeStateReqInfo record = new MicrositeStateReqInfo(0, Msid, stateabr, req_text, req_header);
            bool ret = SiteProvider.PR2.UpdateMicrositeStateReq(record);

            BizObject.PurgeCacheItems("MicrositeStateReqs_MicrositeStateReqs_" + Msid.ToString());
            BizObject.PurgeCacheItems("MicrositeStateReq_MicrositeStateReqs");
            return ret;
        }

        /// <summary>
        /// Deletes an existing MicrositeDomain, but first checks if OK to delete
        /// </summary>
        public static bool DeleteMicrositeStateReq(int Msid, string stateabr)
        {
            bool IsOKToDelete = OKToDelete(Msid);
            if (IsOKToDelete)
            {
                return (bool)DeleteMicrositeStateReq(Msid, stateabr,  true);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes an existing MicrositeDomain - second param forces skip of OKToDelete
        /// (assuming that the calling program has already called that if it's
        /// passing the second param as true)
        /// </summary>
        public static bool DeleteMicrositeStateReq(int Msid, string stateabr, bool SkipOKToDelete)
        {
            if (!SkipOKToDelete)
                return false;

            bool ret = SiteProvider.PR2.DeleteMicrositeStateReq(Msid, stateabr);
            BizObject.PurgeCacheItems("MicrositeResources_MicrositeResources");
            return ret;
        }

        /// <summary>
        /// Checks to see if a MicrositeDomain can be deleted safely
        /// (This default method just returns true. Certain entities
        /// might set datadict_tables.lNoOKDel to eliminate this
        /// default method and provide a custom version in
        /// datadict_tables.boExtra)
        /// </summary>
        public static bool OKToDelete(int EDefID)
        {
            return true;
        }


        /// <summary>
        /// Returns a MicrositeDomain object filled with the data taken from the input MicrositeDomainInfo
        /// </summary>
        private static MicrositeStateReq GetMicrositeStateReqFromMicrositeStateReqInfo(MicrositeStateReqInfo record)
        {
            if (record == null)
                return null;
            else
            {
                return new MicrositeStateReq(record.MsrID, record.Msid, record.Stateabr, record.Req_text, record.Req_header);
            }
        }
        private static MicrositeStateReqInfo GetMicrositeStateReqInfoFromMicrositeStateReq(MicrositeStateReq record)
        {
            if (record == null)
                return null;
            else
            {
                return new MicrositeStateReqInfo(record.MsrID, record.Msid, record.Stateabr, record.Req_text, record.Req_header);
            }
        }
        /// <summary>
        /// Returns a list of MicrositeDomain objects filled with the data taken from the input list of MicrositeDomainInfo
        /// </summary>
        private static List<MicrositeStateReq> GetMicrositeStateReqListFromMicrositeStateReqInfoList(List<MicrositeStateReqInfo> recordset)
        {
            List<MicrositeStateReq> MicrositeStateReqs = new List<MicrositeStateReq>();
            foreach (MicrositeStateReqInfo record in recordset)
                MicrositeStateReqs.Add(GetMicrositeStateReqFromMicrositeStateReqInfo(record));
            return MicrositeStateReqs;
        }
        private static List<MicrositeStateReqInfo> GetMicrositeStateReqInfoListFromMicrositeStateReqList(List<MicrositeStateReq> recordset)
        {
            List<MicrositeStateReqInfo> MicrositeStateReqs = new List<MicrositeStateReqInfo>();
            foreach (MicrositeStateReq record in recordset)
                MicrositeStateReqs.Add(GetMicrositeStateReqInfoFromMicrositeStateReq(record));
            return MicrositeStateReqs;
        }
        #endregion
    }
}