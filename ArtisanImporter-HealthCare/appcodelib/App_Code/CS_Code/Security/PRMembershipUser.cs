using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PearlsReview.Security.Membership
{
    /// <summary>
    /// Summary description for PRMembershipUser
    /// </summary>
    public class PRMembershipUser : MembershipUser
    {
		//
		// TODO: Add constructor logic here
		//

        //private int _iid = 0;
        //public int iID
        //{
        //    get { return _iid; }
        //    protected set { _iid = value; }
        //}

        // extras we added in to store lower-case versions for searching
        //private string _LCUserName = "";
        //public string LCUserName
        //{
        //    get { return _LCUserName.ToLower(); }
        //    set { _LCUserName = value.ToLower(); }
        //}

        private string _LCEmail = "";
        public string LCEmail
        {
            get { return _LCEmail.ToLower(); }
            set { _LCEmail = value.ToLower(); }
        }
        //

        // extra fields that go beyond the usual .NET MembershipUser properties
        // These will be accessed by casting membershipuser instances as PRMembershipUser
        private string _cFirstName = "";
        public string cFirstName
        {
            get { return _cFirstName; }
            set { _cFirstName = value; }
        }

        private string _cLastName = "";
        public string cLastName
        {
            get { return _cLastName; }
            set { _cLastName = value; }
        }

        private string _cMiddle = "";
        public string cMiddle
        {
            get { return _cMiddle; }
            set { _cMiddle = value; }
        }

        private string _cAddress1 = "";
        public string cAddress1
        {
            get { return _cAddress1; }
            set { _cAddress1 = value; }
        }

        private string _cAddress2 = "";
        public string cAddress2
        {
            get { return _cAddress2; }
            set { _cAddress2 = value; }
        }

        private string _cCity = "";
        public string cCity
        {
            get { return _cCity; }
            set { _cCity = value; }
        }

        private string _cState = "";
        public string cState
        {
            get { return _cState; }
            set { _cState = value; }
        }

        private string _cZipCode = "";
        public string cZipCode
        {
            get { return _cZipCode; }
            set { _cZipCode = value; }
        }

        private string _cPhone = "";
        public string cPhone
        {
            get { return _cPhone; }
            set { _cPhone = value; }
        }

         //private string _cPW = "";
        //public string cPW
        //{
        //    get { return _cPW; }
        //    set { _cPW = value; }
        //}

        private string _cSocial = "";
        public string cSocial
        {
            get { return _cSocial; }
            set { _cSocial = value; }
        }

        private string _cBirthDate = "";
        public string cBirthDate
        {
            get { return _cBirthDate; }
            set { _cBirthDate = value; }
        }

        private string _cPromoCode = "";
        public string cPromoCode
        {
            get { return _cPromoCode; }
            set { _cPromoCode = value; }
        }

        private int _PromoID = 0;
        public int PromoID
        {
            get { return _PromoID; }
            set { _PromoID = value; }
        }

        private int _FacilityID = 0;
        public int FacilityID
        {
            get { return _FacilityID; }
            set { _FacilityID = value; }
        }

        private string _cRefCode = "";
        public string cRefCode
        {
            get { return _cRefCode; }
            set { _cRefCode = value; }
        }

        private string _cFloridaNo = "";
        public string cFloridaNo
        {
            get { return _cFloridaNo; }
            set { _cFloridaNo = value; }
        }

        private string _cLectDate = "";
        public string cLectDate
        {
            get { return _cLectDate; }
            set { _cLectDate = value; }
        }

        private string _cExpires = "";
        public string cExpires
        {
            get { return _cExpires; }
            set { _cExpires = value; }
        }

        private bool _lAdmin = false;
        public bool lAdmin
        {
            get { return _lAdmin; }
            set { _lAdmin = value; }
        }

        private bool _lPrimary = false;
        public bool lPrimary
        {
            get { return _lPrimary; }
            set { _lPrimary = value; }
        }

        private string _cRegType = "";
        public string cRegType
        {
            get { return _cRegType; }
            set { _cRegType = value; }
        }

        private string _cInstitute = "";
        public string cInstitute
        {
            get { return _cInstitute; }
            set { _cInstitute = value; }
        }

        private int _SpecID = 0;
        public int SpecID
        {
            get { return _SpecID; }
            set { _SpecID = value; }
        }

        private int _AgeGroup = 0;
        public int AgeGroup
        {
            get { return _AgeGroup; }
            set { _AgeGroup = value; }
        }

        private int _RegTypeID = 0;
        public int RegTypeID
        {
            get { return _RegTypeID; }
            set { _RegTypeID = value; }
        }

        private int _RepID = 0;
        public int RepID
        {
            get { return _RepID; }
            set { _RepID = value; }
        }

        private string _cUserName = "";
        public string cUserName
        {
            get { return _cUserName.ToLower(); }
            set { _cUserName = value.ToLower(); }
        }

        public PRMembershipUser(string providername,
                                  string username,
                                  object providerUserKey,
                                  string email,
                                  string passwordQuestion,
                                  string comment,
                                  bool isApproved,
                                  bool isLockedOut,
                                  DateTime creationDate,
                                  DateTime lastLoginDate,
                                  DateTime lastActivityDate,
                                  DateTime lastPasswordChangedDate,
                                  DateTime lastLockedOutDate,
                                  string cFirstName,
                                  string cLastName,
                                  string cMiddle,
                                  string cAddress1,
                                  string cAddress2,
                                  string cCity,
                                  string cState,
                                  string cZipCode,
                                  string cPhone,
                                  string cSocial,
                                  string cBirthDate,
                                  string cPromoCode,
                                  int PromoID,
                                  int FacilityID,
                                  string cRefCode,
                                  string cFloridaNo,
                                  string cLectDate,
                                  string cExpires,
                                  bool lAdmin,
                                  bool lPrimary,
                                  string cRegType,
                                  string cInstitute,
                                  int SpecID,
                                  int AgeGroup,
                                  int RegTypeID, 
                                  int RepID,
                                  string cUserName) :
                                  base(providername,
                                       username,
                                       providerUserKey,
                                       email,
                                       passwordQuestion,
                                       comment,
                                       isApproved,
                                       isLockedOut,
                                       creationDate,
                                       lastLoginDate,
                                       lastActivityDate,
                                       lastPasswordChangedDate,
                                       lastLockedOutDate)
        {
//            this.iID = iID;
//            this.LCUserName = username.ToLower();
            this.LCEmail = email.ToLower();
            
            this.cFirstName = cFirstName;
            this.cLastName = cLastName;
            this.cMiddle = cMiddle;
            this.cAddress1 = cAddress1;
            this.cAddress2 = cAddress2;
            this.cCity = cCity;
            this.cState = cState;
            this.cZipCode = cZipCode;
            this.cPhone = cPhone;
//            this.cEmail = cEmail;
//            this.cPW = cPW;
            this.cSocial = cSocial;
            this.cBirthDate = cBirthDate;
            this.cPromoCode = cPromoCode;
            this.PromoID = PromoID;
            this.FacilityID = FacilityID;
            this.cRefCode = cRefCode;
            this.cFloridaNo = cFloridaNo;
            this.cLectDate = cLectDate;
            this.cExpires = cExpires;
            this.lAdmin = lAdmin;
            this.lPrimary = lPrimary;
            this.cRegType = cRegType;
            this.cInstitute = cInstitute;
            this.SpecID = SpecID;
            this.AgeGroup = AgeGroup;
            this.RegTypeID = RegTypeID;
            this.RepID = RepID;
            this.cUserName = cUserName;
        }

    }
    ///////// end of PRMembershipUser class

}


