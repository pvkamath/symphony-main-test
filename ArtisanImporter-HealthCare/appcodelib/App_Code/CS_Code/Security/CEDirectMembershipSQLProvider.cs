using System.Web.Security;
using System.Configuration.Provider;
using System.Collections.Specialized;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using PearlsReview.BLL;

/*
This provider works with a expanded schema for the table of user data,
including fields that are not usually in the .NET Security User table.

In addition, the following property names on the left are mapped over to
fields in the UserAccount table with the names on the right.

*** TODO: Revise this documentation to reflect the actual schema being used
  PKID Guid NOT NULL PRIMARY KEY,       iID
  Username Text (255) NOT NULL,         lcUserName (combo of cUserName preceded by 5-digit FacilityID (padded left with zeroes)
  ApplicationName Text (255) NOT NULL,  (not used)
  Email Text (128) NOT NULL,            cEmail
  Comment Text (255),                   Comment (OK)
  Password Text (128) NOT NULL,         cPW
  PasswordQuestion Text (255),          PWQuest
  PasswordAnswer Text (255),            PWAns
  IsApproved YesNo,                     IsApproved (OK)
  LastActivityDate DateTime,            LastAct
  LastLoginDate DateTime,               LastLogin
  LastPasswordChangedDate DateTime,     LastPWChg
  CreationDate DateTime,                Created
  IsOnLine YesNo,                       IsOnline (OK)
  IsLockedOut YesNo,                    IsLocked
  LastLockedOutDate DateTime,           LastLock
  FailedPasswordAttemptCount Integer,   XPWAtt
  FailedPasswordAttemptWindowStart DateTime,    XPWAttSt
  FailedPasswordAnswerAttemptCount Integer,     XPWAnsAtt
  FailedPasswordAnswerAttemptWindowStart DateTime   XPWAnsSt

*/


namespace PearlsReview.Security.Membership
{

  public class CEDirectMembershipSQLProvider: MembershipProvider
  {

    //
    // Global connection string, generated password length, generic exception message, event log info.
    //

    private int    newPasswordLength = 8;
    private string eventSource = "CEDirectMembershipSQLProvider";
    private string eventLog = "Application";
    private string exceptionMessage = "An exception occurred. Please check the Event Log.";
    private string connectionString;
    private string sconnectionString;
    //private string connectionStringX;

    //
    // Used when determining encryption key values.
    //

    private MachineKeySection machineKey;

    //
    // If false, exceptions are thrown to the caller. If true,
    // exceptions are written to the event log.
    //

    private bool pWriteExceptionsToEventLog;

    public bool WriteExceptionsToEventLog
    {
      get { return pWriteExceptionsToEventLog; }
      set { pWriteExceptionsToEventLog = value; }
    }


    //
    // System.Configuration.Provider.ProviderBase.Initialize Method
    //

    public override void Initialize(string name, NameValueCollection config)
    {
      //
      // Initialize values from web.config.
      //

      if (config == null)
        throw new ArgumentNullException("config");

      if (name == null || name.Length == 0)
        name = "CEDirectMembershipSQLProvider";

      if (String.IsNullOrEmpty(config["description"]))
      {
        config.Remove("description");
        config.Add("description", "CEDirect SQL Server Membership provider");
      }

      // Initialize the abstract base class.
      base.Initialize(name, config);

      pApplicationName            = GetConfigValue(config["applicationName"], 
                                      System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
      pMaxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
      pPasswordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
      pMinRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1"));
      pMinRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
      pPasswordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], ""));
      pEnablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
      pEnablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
      pRequiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
      pRequiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));
      pWriteExceptionsToEventLog = Convert.ToBoolean(GetConfigValue(config["writeExceptionsToEventLog"], "true"));

      string temp_format = config["passwordFormat"];
      if (temp_format == null)
      {
        temp_format = "Hashed";
      }

      switch (temp_format)
      {
        case "Hashed":
          pPasswordFormat = MembershipPasswordFormat.Hashed;
          break;
        case "Encrypted":
          pPasswordFormat = MembershipPasswordFormat.Encrypted;
          break;
        case "Clear":
          pPasswordFormat = MembershipPasswordFormat.Clear;
          break;
        default:
          throw new ProviderException("Password format not supported.");
      }

      //
      // Initialize SQLConnection.
      //

      ConnectionStringSettings ConnectionStringSettings =
        ConfigurationManager.ConnectionStrings[config["connectionStringName"]];

      if (ConnectionStringSettings == null || ConnectionStringSettings.ConnectionString.Trim() == "")
      {
        throw new ProviderException("Connection string cannot be blank.");
      }

      connectionString = ConnectionStringSettings.ConnectionString;


      ConnectionStringSettings SConnectionStringSettings =
             ConfigurationManager.ConnectionStrings[config["SconnectionStringName"]];

      if (SConnectionStringSettings == null || SConnectionStringSettings.ConnectionString.Trim() == "")
      {
          throw new ProviderException("Site Connection string cannot be blank.");
      }

      sconnectionString = SConnectionStringSettings.ConnectionString;

      // Get encryption and decryption key information from the configuration.
      Configuration cfg =
        WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
      machineKey = (MachineKeySection)cfg.GetSection("system.web/machineKey");

      if (machineKey.ValidationKey.Contains("AutoGenerate"))
        if (PasswordFormat != MembershipPasswordFormat.Clear)
          throw new ProviderException("Hashed or Encrypted passwords " +
                                      "are not supported with auto-generated keys.");
    }


    //
    // A helper function to retrieve config values from the configuration file.
    //

    private string GetConfigValue(string configValue, string defaultValue)
    {
      if (String.IsNullOrEmpty(configValue))
        return defaultValue;

      return configValue;
    }


    //
    // System.Web.Security.MembershipProvider properties.
    //
    

    private string pApplicationName;
    private bool   pEnablePasswordReset;
    private bool   pEnablePasswordRetrieval;
    private bool   pRequiresQuestionAndAnswer;
    private bool   pRequiresUniqueEmail;
    private int    pMaxInvalidPasswordAttempts;
    private int    pPasswordAttemptWindow;
    private MembershipPasswordFormat pPasswordFormat;

    public override string ApplicationName
    {
      get { return pApplicationName; }
      set { pApplicationName = value; }
    } 

    public override bool EnablePasswordReset
    {
      get { return pEnablePasswordReset; }
    }


    public override bool EnablePasswordRetrieval
    {
      get { return pEnablePasswordRetrieval; }
    }


    public override bool RequiresQuestionAndAnswer
    {
      get { return pRequiresQuestionAndAnswer; }
    }


    public override bool RequiresUniqueEmail
    {
      get { return pRequiresUniqueEmail; }
    }


    public override int MaxInvalidPasswordAttempts
    {
      get { return pMaxInvalidPasswordAttempts; }
    }


    public override int PasswordAttemptWindow
    {
      get { return pPasswordAttemptWindow; }
    }


    public override MembershipPasswordFormat PasswordFormat
    {
      get { return pPasswordFormat; }
    }

    private int pMinRequiredNonAlphanumericCharacters;

    public override int MinRequiredNonAlphanumericCharacters
    {
      get { return pMinRequiredNonAlphanumericCharacters; }
    }

    private int pMinRequiredPasswordLength;

    public override int MinRequiredPasswordLength
    {
      get { return pMinRequiredPasswordLength; }
    }

    private string pPasswordStrengthRegularExpression;

    public override string PasswordStrengthRegularExpression
    {
      get { return pPasswordStrengthRegularExpression; }
    }

    //
    // System.Web.Security.MembershipProvider methods.
    //

    //
    // MembershipProvider.ChangePassword
    //

    //public bool ChangePasswordX(int userid, string oldPwd, string newPwd, int FacilityID)
    //{
    //    string username = "";
    //    SqlConnection conn = new SqlConnection(connectionString);
    //    SqlCommand cmd = new SqlCommand("SELECT cUsername FROM UserAccount WHERE iID=" + userid, conn);
    //    conn.Open();
    //    try
    //    {            
    //        username = Convert.ToString(cmd.ExecuteScalar());
    //        conn.Close();
    //    }
    //    catch (SqlException e)
    //    {
    //        if (WriteExceptionsToEventLog)
    //        {
    //            WriteToEventLog(e, "ChangePassword");

    //            throw new ProviderException(exceptionMessage);
    //        }
    //        else
    //        {
    //            throw e;
    //        }
    //    }
    //    finally
    //    {
    //        conn.Close();
    //    }

    //  if (!ValidateUserX(username, oldPwd, FacilityID))
    //    return false;


    //  ValidatePasswordEventArgs args = 
    //    new ValidatePasswordEventArgs(username, newPwd, true);

    //  OnValidatingPassword(args);
  
    //  if (args.Cancel)
    //    if (args.FailureInformation != null)
    //      throw args.FailureInformation;
    //    else
    //      throw new MembershipPasswordException("Change password canceled due to new password validation failure.");

    //  cmd = new SqlCommand("UPDATE UserAccount "  +
    //          " SET cPW = @Password, LastPWChg = @LastPasswordChangedDate " +
    //          " WHERE cUsername = @username AND FacilityID = @FacilityID", conn);

    //  cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = EncodePassword(newPwd);
    //  cmd.Parameters.Add("@LastPasswordChangedDate", SqlDbType.DateTime).Value = DateTime.Now;
    //  cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = username;
    //  cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //  //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;


    //  int rowsAffected = 0;

    //  try
    //  {
    //    conn.Open();

    //    rowsAffected = cmd.ExecuteNonQuery();
    //  }
    //  catch (SqlException e)
    //  {
    //    if (WriteExceptionsToEventLog)
    //    {
    //      WriteToEventLog(e, "ChangePassword");

    //      throw new ProviderException(exceptionMessage);
    //    }
    //    else
    //    {
    //      throw e;
    //    }
    //  }
    //  finally
    //  {
    //    conn.Close();
    //  }

    //  if (rowsAffected > 0)
    //  {
    //    return true;
    //  }

    //  return false;
    //}


    public override bool ChangePassword(string username, string oldPwd, string newPwd)
    {
        if (!ValidateUser(username.ToLower(), oldPwd.ToLower()))
            return false;


        ValidatePasswordEventArgs args =
          new ValidatePasswordEventArgs(username.ToLower(), newPwd.ToLower(), true);

        OnValidatingPassword(args);

        if (args.Cancel)
            if (args.FailureInformation != null)
                throw args.FailureInformation;
            else
                throw new MembershipPasswordException("Change password canceled due to new password validation failure.");

        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("UPDATE UserAccount " +
                " SET cPW = @Password, LastPWChg = @LastPasswordChangedDate " +
                " WHERE lcUserName = @Username", conn);

        cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = EncodePassword(newPwd);
        cmd.Parameters.Add("@LastPasswordChangedDate", SqlDbType.DateTime).Value = DateTime.Now;
        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
        //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;


        int rowsAffected = 0;

        try
        {
            conn.Open();

            rowsAffected = cmd.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            if (WriteExceptionsToEventLog)
            {
                WriteToEventLog(e, "ChangePassword");

                throw new ProviderException(exceptionMessage);
            }
            else
            {
                throw e;
            }
        }
        finally
        {
            conn.Close();
        }

        if (rowsAffected > 0)
        {
            return true;
        }

        return false;
    }


    //
    // MembershipProvider.ChangePasswordQuestionAndAnswer
    //

    //public bool ChangePasswordQuestionAndAnswerX(string username,
    //              string password,
    //              string newPwdQuestion,
    //              string newPwdAnswer, int FacilityID)
    //{
    //  if (!ValidateUserX(username, password, FacilityID))
    //    return false;

    //  SqlConnection conn = new SqlConnection(connectionString);
    //  SqlCommand cmd = new SqlCommand("UPDATE UserAccount " +
    //          " SET PWQuest = @Question, PWAns = @Answer" +
    //          " WHERE cUserName = @Username AND FacilityID = @FacilityID", conn);

    //  cmd.Parameters.Add("@Question", SqlDbType.VarChar).Value = newPwdQuestion;
    //  cmd.Parameters.Add("@Answer", SqlDbType.VarChar).Value = EncodePassword(newPwdAnswer);
    //  cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //  cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //  //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;


    //  int rowsAffected = 0;

    //  try
    //  {
    //    conn.Open();

    //    rowsAffected = cmd.ExecuteNonQuery();
    //  }
    //  catch (SqlException e)
    //  {
    //    if (WriteExceptionsToEventLog)
    //    {
    //      WriteToEventLog(e, "ChangePasswordQuestionAndAnswer");

    //      throw new ProviderException(exceptionMessage);
    //    }
    //    else
    //    {
    //      throw e;
    //    }
    //  }
    //  finally
    //  {
    //    conn.Close();
    //  }

    //  if (rowsAffected > 0)
    //  {
    //    return true;
    //  }

    //  return false;
    //}


    public override bool ChangePasswordQuestionAndAnswer(string username,
                  string password,
                  string newPwdQuestion,
                  string newPwdAnswer)
    {
        if (!ValidateUser(username.ToLower(), password.ToLower()))
            return false;

        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("UPDATE UserAccount " +
                " SET PWQuest = @Question, PWAns = @Answer" +
                " WHERE lcUserName = @Username", conn);

        cmd.Parameters.Add("@Question", SqlDbType.VarChar).Value = newPwdQuestion;
        cmd.Parameters.Add("@Answer", SqlDbType.VarChar).Value = EncodePassword(newPwdAnswer.ToLower());
        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
        //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;


        int rowsAffected = 0;

        try
        {
            conn.Open();

            rowsAffected = cmd.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            if (WriteExceptionsToEventLog)
            {
                WriteToEventLog(e, "ChangePasswordQuestionAndAnswer");

                throw new ProviderException(exceptionMessage);
            }
            else
            {
                throw e;
            }
        }
        finally
        {
            conn.Close();
        }

        if (rowsAffected > 0)
        {
            return true;
        }

        return false;
    }


    //
    // MembershipProvider.CreateUser
    //

    //public MembershipUser CreateUserX(string username,
    //         string password,
    //         string email,
    //         string passwordQuestion,
    //         string passwordAnswer,
    //         bool isApproved,
    //         object providerUserKey, int FacilityID,
    //         out MembershipCreateStatus status)
    //{
    //    return this.CreateUserX(username, password, email,
    //                          passwordQuestion, passwordAnswer,
    //                          isApproved, providerUserKey, 
    //                          "", "", false, false, "", "", "", 
    //                          "", "", "", "", "",
    //                          "", "", "", 0, FacilityID, 
    //                          "", "", "", "", "",
    //                          0, 0, 0, 0,
    //                          out status);
        
    //}
    //// PRMembershipProvider.CreateUser -- returns PRMembershipUser
    //        // NOTE: We are not exposing these control fields in the
    //        // PRMembershipUser object
    //        //string LCUserName, 
    //        //DateTime LastAct, 
    //        //string PassFmt, 
    //        //string PassSalt, 
    //        //string MobilePIN, 
    //        //string LCEmail, 
    //        //bool IsOnline, 
    //        //bool IsLocked, 
    //        //DateTime LastLogin, 
    //        //DateTime LastPWChg, 
    //        //DateTime LastLock, 
    //        //int XPWAtt, 
    //        //DateTime XPWAttSt, 
    //        //int XPWAnsAtt, 
    //        //DateTime XPWAnsSt, 
    //        //string Comment, 
    //        //DateTime Created, 

    //public PRMembershipUser CreateUserX(
    //         string username,
    //         string password,
    //         string email,
    //         string passwordQuestion,
    //         string passwordAnswer,
    //         bool isApproved,
    //         object providerUserKey,
    //        string cAddress1, 
    //        string cAddress2, 
    //        bool lAdmin,
    //        bool lPrimary,
    //        string cBirthDate, 
    //        string cCity, 
    //        string cExpires, 
    //        string cFirstName, 
    //        string cFloridaNo, 
    //        string cInstitute, 
    //        string cLastName, 
    //        string cLectDate, 
    //        string cMiddle, 
    //        string cPhone, 
    //        string cPromoCode, 
    //        int PromoID,
    //        int FacilityID, 
    //        string cRefCode, 
    //        string cRegType, 
    //        string cSocial, 
    //        string cState, 
    //        string cZipCode, 
    //        int SpecID, 
    //        int AgeGroup, 
    //        int RegTypeID, 
    //        int RepID,
    //        out MembershipCreateStatus status)
    //{
    //  ValidatePasswordEventArgs args = 
    //    new ValidatePasswordEventArgs(username, password, true);

    //  OnValidatingPassword(args);
  
    //  if (args.Cancel)
    //  {
    //    status = MembershipCreateStatus.InvalidPassword;
    //    return null;
    //  }



    //  if (RequiresUniqueEmail && GetUserNameByEmail(email) != "")
    //  {
    //    status = MembershipCreateStatus.DuplicateEmail;
    //    return null;
    //  }

    //    MembershipUser u = GetUserX(username, false, FacilityID);

    //  if (u == null)
    //  {
    //    DateTime createDate = System.DateTime.Now;

    //    /////////////////////////////////
    //    // TODO: This section needs to be reworked for Int instead of guid
    //    //if ( providerUserKey.ToString() == "0")
    //    //{
    //    //  providerUserKey = ++UserAccount.GetHighestUserAccountID();
    //    //}
    //    //else
    //    //{
    //    //  if ( !(providerUserKey is int) )
    //    //  {
    //    //    status = MembershipCreateStatus.InvalidProviderUserKey;
    //    //    return null;
    //    //  }
    //    //}
    //    //////////////////////////

    //    SqlConnection conn = new SqlConnection(connectionString);
    //    SqlCommand cmd = new SqlCommand("INSERT INTO UserAccount " +
    //          " (cUserName, cPW, cEmail, PWQuest, " +
    //          " PWAns, IsApproved," +
    //          " Comment, Created, LastPWChg, LastAct," +
    //          " IsLocked, IsOnline, LastLock, LastLogin, " +
    //          " XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, passsalt, passfmt, MobilePIN, " +
    //          " LCUserName, LCEmail, " +
    //          " cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cExpires, " +
    //          " cFirstName, cFloridaNo, cInstitute, cLastName, cLectDate, " +
    //          " cMiddle, cPhone, cPromoCode, PromoID, FacilityID, cRefCode, cRegType, " +
    //          " cSocial, cState, cZipCode, SpecID, AgeGroup, " +
    //          " RegTypeID, RepID )" +
    //          " Values(@Username, @Password, @Email, @PasswordQuestion, " +
    //          " @PasswordAnswer, @IsApproved, " +
    //          " @Comment, @CreationDate, @LastPasswordChangedDate, @LastActivityDate, "+
    //          " @IsLocked, @IsOnline, @LastLockedOutDate, @LastLogin, " +
    //          " @FailedPasswordAttemptCount, @FailedPasswordAttemptWindowStart, @FailedPasswordAnswerAttemptCount, "+
    //          " @FailedPasswordAnswerAttemptWindowStart, @Passsalt, @Passfmt, @MobilePIN, " +
    //          " @LCUserName, @LCEmail, " +
    //          " @cAddress1, @cAddress2, @lAdmin, @lPrimary, @cBirthDate, @cCity, @cExpires, "+
    //          " @cFirstName, @cFloridaNo, @cInstitute, @cLastName, @cLectDate, " +
    //          " @cMiddle, @cPhone, @cPromoCode, @PromoID, @FacilityID, @cRefCode, @cRegType, " +
    //          " @cSocial, @cState, @cZipCode, @SpecID, @AgeGroup, @RegTypeID, @RepID )", conn);

    //    cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //    cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = EncodePassword(password);
    //    cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = email;
    //    cmd.Parameters.Add("@PasswordQuestion", SqlDbType.VarChar).Value = passwordQuestion;
        
    //    // temporary workaround for not passing in this value
    //    passwordAnswer = "";
    //      cmd.Parameters.Add("@PasswordAnswer", SqlDbType.VarChar).Value = EncodePassword(passwordAnswer);
        
    //      cmd.Parameters.Add("@IsApproved", SqlDbType.Bit).Value = isApproved;

    //      cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = "";
    //    cmd.Parameters.Add("@CreationDate", SqlDbType.DateTime).Value = createDate;
    //    cmd.Parameters.Add("@LastPasswordChangedDate", SqlDbType.DateTime).Value = DBNull.Value;
    //    cmd.Parameters.Add("@LastActivityDate", SqlDbType.DateTime).Value = DBNull.Value;

    //      cmd.Parameters.Add("@IsLocked", SqlDbType.Bit).Value = false;
    //    cmd.Parameters.Add("@IsOnline", SqlDbType.Bit).Value = false;
    //    cmd.Parameters.Add("@LastLockedOutDate", SqlDbType.DateTime).Value = DBNull.Value;
    //    cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = DBNull.Value;

    //      cmd.Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int).Value = 0;
    //    cmd.Parameters.Add("@FailedPasswordAttemptWindowStart", SqlDbType.DateTime).Value = createDate;
    //    cmd.Parameters.Add("@FailedPasswordAnswerAttemptCount", SqlDbType.Int).Value = 0;
    //    cmd.Parameters.Add("@FailedPasswordAnswerAttemptWindowStart", SqlDbType.DateTime).Value = createDate;
    //    cmd.Parameters.Add("@Passsalt", SqlDbType.VarChar).Value = "";
    //    cmd.Parameters.Add("@Passfmt", SqlDbType.VarChar).Value = "";
    //    cmd.Parameters.Add("@MobilePIN", SqlDbType.VarChar).Value = "";

    //    // now our custom properties - these might be useful for queries
    //    cmd.Parameters.Add("@LCUserName", SqlDbType.VarChar).Value = username.ToLower();
    //    cmd.Parameters.Add("@LCEmail", SqlDbType.VarChar).Value = email.ToLower();

    //    // and the normal UserAccount fields that aren't already above
    //    cmd.Parameters.Add("@cAddress1", SqlDbType.VarChar).Value = cAddress1;
    //    cmd.Parameters.Add("@cAddress2", SqlDbType.VarChar).Value = cAddress2;
    //    cmd.Parameters.Add("@lAdmin", SqlDbType.Bit).Value = lAdmin;
    //    cmd.Parameters.Add("@lPrimary", SqlDbType.Bit).Value = lPrimary;
    //    cmd.Parameters.Add("@cBirthDate", SqlDbType.VarChar).Value = cBirthDate;
    //    cmd.Parameters.Add("@cCity", SqlDbType.VarChar).Value = cCity;
    //    cmd.Parameters.Add("@cExpires", SqlDbType.VarChar).Value = cExpires;
    //    cmd.Parameters.Add("@cFirstName", SqlDbType.VarChar).Value = cFirstName;
    //    cmd.Parameters.Add("@cFloridaNo", SqlDbType.VarChar).Value = cFloridaNo;
    //    cmd.Parameters.Add("@cInstitute", SqlDbType.VarChar).Value = cInstitute;
    //    cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = cLastName;
    //    cmd.Parameters.Add("@cLectDate", SqlDbType.VarChar).Value = cLectDate;
    //    cmd.Parameters.Add("@cMiddle", SqlDbType.VarChar).Value = cMiddle;
    //    cmd.Parameters.Add("@cPhone", SqlDbType.VarChar).Value = cPhone;
    //    cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = cPromoCode;

    //      // pass null to database if value is zero (nothing selected by user)
    //    // this allows the database to have a NULL foreign key and still enforce relational integrity
    //    if (PromoID == 0)
    //    {
    //        cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = System.DBNull.Value;
    //    }
    //    else
    //    {
    //        cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = PromoID;
    //    }

    //    // pass null to database if value is zero (nothing selected by user)
    //    // this allows the database to have a NULL foreign key and still enforce relational integrity
    //    if (FacilityID == 0)
    //    {
    //        cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = System.DBNull.Value;
    //    }
    //    else
    //    {
    //        cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //    }

    //    cmd.Parameters.Add("@cRefCode", SqlDbType.VarChar).Value = cRefCode;
    //    cmd.Parameters.Add("@cRegType", SqlDbType.VarChar).Value = cRegType;
    //    cmd.Parameters.Add("@cSocial", SqlDbType.VarChar).Value = cSocial;
    //    cmd.Parameters.Add("@cState", SqlDbType.VarChar).Value = cState;
    //    cmd.Parameters.Add("@cZipCode", SqlDbType.VarChar).Value = cZipCode;

    //    // pass null to database if value is zero (nothing selected by user)
    //    // this allows the database to have a NULL foreign key and still enforce relational integrity
    //    if (SpecID == 0)
    //    {
    //        cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = System.DBNull.Value;
    //    }
    //    else
    //    {
    //        cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = SpecID;
    //    }

    //    // pass null to database if value is zero (nothing selected by user)
    //    // this allows the database to have a NULL foreign key and still enforce relational integrity
    //    if (AgeGroup == 0)
    //    {
    //        cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = System.DBNull.Value;
    //    }
    //    else
    //    {
    //        cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = AgeGroup;
    //    }

    //    // pass null to database if value is zero (nothing selected by user)
    //    // this allows the database to have a NULL foreign key and still enforce relational integrity
    //    if (RegTypeID == 0)
    //    {
    //        cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = System.DBNull.Value;
    //    }
    //    else
    //    {
    //        cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = RegTypeID;
    //    }

    //    // pass null to database if value is zero (nothing selected by user)
    //    // this allows the database to have a NULL foreign key and still enforce relational integrity
    //    if (RepID == 0)
    //    {
    //        cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
    //    }
    //    else
    //    {
    //        cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = RepID;
    //    }


    //    try
    //    {
    //      conn.Open();

    //      int recAdded = cmd.ExecuteNonQuery();

    //      if (recAdded > 0)
    //      {
    //        status = MembershipCreateStatus.Success;
    //      }
    //      else
    //      {
    //        status = MembershipCreateStatus.UserRejected;
    //      }
    //    }
    //    catch (SqlException e)
    //    {
    //      if (WriteExceptionsToEventLog)
    //      {
    //        WriteToEventLog(e, "CreateUser");
    //      }

    //      status = MembershipCreateStatus.ProviderError;
    //    }
    //    finally
    //    {
    //      conn.Close();
    //    }

    //    // cast the return value as the custom PRMembershipUser class
    //    // so we'll have all of the custom properties
    //      return (PRMembershipUser)GetUserX(username, false, FacilityID);

    //  }        
    //  else
    //  {
    //    status = MembershipCreateStatus.DuplicateUserName;
    //  }
      

    //  return null;
    //}



    public override MembershipUser CreateUser(string username,
             string password,
             string email,
             string passwordQuestion,
             string passwordAnswer,
             bool isApproved,
             object providerUserKey,
             out MembershipCreateStatus status)
    {
        return this.CreateUser(username.ToLower(), password.ToLower(), email,
                              passwordQuestion, passwordAnswer.ToLower(),
                              isApproved, providerUserKey,
                              "", "", false, false, "", "", "",
                              "", "", "", "", "",
                              "", "", "", 0, 0,
                              "", "", "", "", "",
                              0, 0, 0, 0, "",
                              out status);

    }
    // PRMembershipProvider.CreateUser -- returns PRMembershipUser
    // NOTE: We are not exposing these control fields in the
    // PRMembershipUser object
    //string LCUserName, 
    //DateTime LastAct, 
    //string PassFmt, 
    //string PassSalt, 
    //string MobilePIN, 
    //string LCEmail, 
    //bool IsOnline, 
    //bool IsLocked, 
    //DateTime LastLogin, 
    //DateTime LastPWChg, 
    //DateTime LastLock, 
    //int XPWAtt, 
    //DateTime XPWAttSt, 
    //int XPWAnsAtt, 
    //DateTime XPWAnsSt, 
    //string Comment, 
    //DateTime Created, 

      // NOTE: This method assumes that the first parameter username already contains the expanded
      // 5-digit FacilityID (padded left with zeroes) in front of the entered username
      //
      // TODO: Revisit this if we ever turn on CreateUserWizard for CEDirect system
      //
      public PRMembershipUser CreateUser(
             string username,
             string password,
             string email,
             string passwordQuestion,
             string passwordAnswer,
             bool isApproved,
             object providerUserKey,
            string cAddress1,
            string cAddress2,
            bool lAdmin,
            bool lPrimary,
            string cBirthDate,
            string cCity,
            string cExpires,
            string cFirstName,
            string cFloridaNo,
            string cInstitute,
            string cLastName,
            string cLectDate,
            string cMiddle,
            string cPhone,
            string cPromoCode,
            int PromoID,
            int FacilityID,
            string cRefCode,
            string cRegType,
            string cSocial,
            string cState,
            string cZipCode,
            int SpecID,
            int AgeGroup,
            int RegTypeID,
            int RepID,
            string cUserName,
            out MembershipCreateStatus status)
    {
        ValidatePasswordEventArgs args =
          new ValidatePasswordEventArgs(username.ToLower(), password.ToLower(), true);

        OnValidatingPassword(args);

        if (args.Cancel)
        {
            status = MembershipCreateStatus.InvalidPassword;
            return null;
        }



        //if (RequiresUniqueEmail && GetUserNameByEmail(email) != "")
        //{
        //    status = MembershipCreateStatus.DuplicateEmail;
        //    return null;
        //}

        //MembershipUser u = GetUser(username.ToLower(), false);
        MembershipUser u = null;

        if (u == null)
        {
            DateTime createDate = System.DateTime.Now;

            /////////////////////////////////
            // TODO: This section needs to be reworked for Int instead of guid
            //if ( providerUserKey.ToString() == "0")
            //{
            //  providerUserKey = ++UserAccount.GetHighestUserAccountID();
            //}
            //else
            //{
            //  if ( !(providerUserKey is int) )
            //  {
            //    status = MembershipCreateStatus.InvalidProviderUserKey;
            //    return null;
            //  }
            //}
            //////////////////////////

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("INSERT INTO UserAccount " +
                  " (cUserName, cPW, cEmail, PWQuest, " +
                  " PWAns, IsApproved," +
                  " Comment, Created, LastPWChg, LastAct," +
                  " IsLocked, IsOnline, LastLock, LastLogin, " +
                  " XPWAtt, XPWAttSt, XPWAnsAtt, XPWAnsSt, passsalt, passfmt, MobilePIN, " +
                  " LCUserName, LCEmail, " +
                  " cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cExpires, " +
                  " cFirstName, cFloridaNo, cInstitute, cLastName, cLectDate, " +
                  " cMiddle, cPhone, cPromoCode, PromoID, FacilityID, cRefCode, cRegType, " +
                  " cSocial, cState, cZipCode, SpecID, AgeGroup, " +
                  " RegTypeID, RepID )" +
                  " Values(@Username, @Password, @Email, @PasswordQuestion, " +
                  " @PasswordAnswer, @IsApproved, " +
                  " @Comment, @CreationDate, @LastPasswordChangedDate, @LastActivityDate, " +
                  " @IsLocked, @IsOnline, @LastLockedOutDate, @LastLogin, " +
                  " @FailedPasswordAttemptCount, @FailedPasswordAttemptWindowStart, @FailedPasswordAnswerAttemptCount, " +
                  " @FailedPasswordAnswerAttemptWindowStart, @Passsalt, @Passfmt, @MobilePIN, " +
                  " @LCUserName, @LCEmail, " +
                  " @cAddress1, @cAddress2, @lAdmin, @lPrimary, @cBirthDate, @cCity, @cExpires, " +
                  " @cFirstName, @cFloridaNo, @cInstitute, @cLastName, @cLectDate, " +
                  " @cMiddle, @cPhone, @cPromoCode, @PromoID, @FacilityID, @cRefCode, @cRegType, " +
                  " @cSocial, @cState, @cZipCode, @SpecID, @AgeGroup, @RegTypeID, @RepID )", conn);

            cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
            cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = password;
            cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = email;
            cmd.Parameters.Add("@PasswordQuestion", SqlDbType.VarChar).Value = passwordQuestion;

            // temporary workaround for not passing in this value
            passwordAnswer = "";
            cmd.Parameters.Add("@PasswordAnswer", SqlDbType.VarChar).Value = passwordAnswer;

            cmd.Parameters.Add("@IsApproved", SqlDbType.Bit).Value = isApproved;

            cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@CreationDate", SqlDbType.DateTime).Value = createDate;
            cmd.Parameters.Add("@LastPasswordChangedDate", SqlDbType.DateTime).Value = DBNull.Value;
            cmd.Parameters.Add("@LastActivityDate", SqlDbType.DateTime).Value = DBNull.Value;

            cmd.Parameters.Add("@IsLocked", SqlDbType.Bit).Value = false;
            cmd.Parameters.Add("@IsOnline", SqlDbType.Bit).Value = false;
            cmd.Parameters.Add("@LastLockedOutDate", SqlDbType.DateTime).Value = DBNull.Value;
            cmd.Parameters.Add("@LastLogin", SqlDbType.DateTime).Value = DBNull.Value;

            cmd.Parameters.Add("@FailedPasswordAttemptCount", SqlDbType.Int).Value = 0;
            cmd.Parameters.Add("@FailedPasswordAttemptWindowStart", SqlDbType.DateTime).Value = createDate;
            cmd.Parameters.Add("@FailedPasswordAnswerAttemptCount", SqlDbType.Int).Value = 0;
            cmd.Parameters.Add("@FailedPasswordAnswerAttemptWindowStart", SqlDbType.DateTime).Value = createDate;
            cmd.Parameters.Add("@Passsalt", SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@Passfmt", SqlDbType.VarChar).Value = "";
            cmd.Parameters.Add("@MobilePIN", SqlDbType.VarChar).Value = "";

            // now our custom properties - these might be useful for queries
            cmd.Parameters.Add("@LCUserName", SqlDbType.VarChar).Value = (100000 + 3).ToString().Substring(1) + username.ToLower();
            cmd.Parameters.Add("@LCEmail", SqlDbType.VarChar).Value = email.ToLower();

            // and the normal UserAccount fields that aren't already above
            cmd.Parameters.Add("@cAddress1", SqlDbType.VarChar).Value = cAddress1;
            cmd.Parameters.Add("@cAddress2", SqlDbType.VarChar).Value = cAddress2;
            cmd.Parameters.Add("@lAdmin", SqlDbType.Bit).Value = lAdmin;
            cmd.Parameters.Add("@lPrimary", SqlDbType.Bit).Value = lPrimary;
            cmd.Parameters.Add("@cBirthDate", SqlDbType.VarChar).Value = cBirthDate;
            cmd.Parameters.Add("@cCity", SqlDbType.VarChar).Value = cCity;
            cmd.Parameters.Add("@cExpires", SqlDbType.VarChar).Value = cExpires;
            cmd.Parameters.Add("@cFirstName", SqlDbType.VarChar).Value = cFirstName;
            cmd.Parameters.Add("@cFloridaNo", SqlDbType.VarChar).Value = cFloridaNo;
            cmd.Parameters.Add("@cInstitute", SqlDbType.VarChar).Value = cInstitute;
            cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = cLastName;
            cmd.Parameters.Add("@cLectDate", SqlDbType.VarChar).Value = cLectDate;
            cmd.Parameters.Add("@cMiddle", SqlDbType.VarChar).Value = cMiddle;
            cmd.Parameters.Add("@cPhone", SqlDbType.VarChar).Value = cPhone;
            cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = cPromoCode;

            // pass null to database if value is zero (nothing selected by user)
            // this allows the database to have a NULL foreign key and still enforce relational integrity
            if (PromoID == 0)
            {
                cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = System.DBNull.Value;
            }
            else
            {
                cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = PromoID;
            }

            // pass null to database if value is zero (nothing selected by user)
            // this allows the database to have a NULL foreign key and still enforce relational integrity
            if (FacilityID == 0)
            {
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = 3;
            }
            else
            {
                cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
            }

            cmd.Parameters.Add("@cRefCode", SqlDbType.VarChar).Value = cRefCode;
            cmd.Parameters.Add("@cRegType", SqlDbType.VarChar).Value = cRegType;
            cmd.Parameters.Add("@cSocial", SqlDbType.VarChar).Value = cSocial;
            cmd.Parameters.Add("@cState", SqlDbType.VarChar).Value = cState;
            cmd.Parameters.Add("@cZipCode", SqlDbType.VarChar).Value = cZipCode;

            // pass null to database if value is zero (nothing selected by user)
            // this allows the database to have a NULL foreign key and still enforce relational integrity
            if (SpecID == 0)
            {
                cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = System.DBNull.Value;
            }
            else
            {
                cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = SpecID;
            }

            // pass null to database if value is zero (nothing selected by user)
            // this allows the database to have a NULL foreign key and still enforce relational integrity
            if (AgeGroup == 0)
            {
                cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = System.DBNull.Value;
            }
            else
            {
                cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = AgeGroup;
            }

            // pass null to database if value is zero (nothing selected by user)
            // this allows the database to have a NULL foreign key and still enforce relational integrity
            if (RegTypeID == 0)
            {
                cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = System.DBNull.Value;
            }
            else
            {
                cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = RegTypeID;
            }

            // pass null to database if value is zero (nothing selected by user)
            // this allows the database to have a NULL foreign key and still enforce relational integrity
            if (RepID == 0)
            {
                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
            }
            else
            {
                cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = RepID;
            }


            try
            {
                conn.Open();

                int recAdded = cmd.ExecuteNonQuery();

                if (recAdded > 0)
                {
                    status = MembershipCreateStatus.Success;
                }
                else
                {
                    status = MembershipCreateStatus.UserRejected;
                }
            }
            catch 
            {
                
                status = MembershipCreateStatus.ProviderError;
            }
            finally
            {
                conn.Close();
            }

            // cast the return value as the custom PRMembershipUser class
            // so we'll have all of the custom properties
            return (PRMembershipUser)GetUser((100000 + 3).ToString().Substring(1) + username.ToLower(), false);

        }
        else
        {
            status = MembershipCreateStatus.DuplicateUserName;
        }


        return null;
    }




    //
    // MembershipProvider.DeleteUser
    //

    //public bool DeleteUserX(string username, bool deleteAllRelatedData, int FacilityID)
    //{
    //  SqlConnection conn = new SqlConnection(connectionString);
    //  SqlCommand cmd = new SqlCommand("DELETE FROM UserAccount " +
    //          " WHERE cUserName = @Username AND FacilityID = @FacilityID", conn);

    //  cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //  cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //  //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

    //  int rowsAffected = 0;

    //  try
    //  {
    //    conn.Open();

    //    rowsAffected = cmd.ExecuteNonQuery();

    //    if (deleteAllRelatedData)
    //    {
    //      // Process commands to delete all data for the user in the database.
    //    }
    //  }
    //  catch (SqlException e)
    //  {
    //    if (WriteExceptionsToEventLog)
    //    {
    //      WriteToEventLog(e, "DeleteUser");

    //      throw new ProviderException(exceptionMessage);
    //    }
    //    else
    //    {
    //      throw e;
    //    }
    //  }
    //  finally
    //  {
    //    conn.Close();
    //  }

    //  if (rowsAffected > 0)
    //    return true;

    //  return false;
    //}


    public override bool DeleteUser(string username, bool deleteAllRelatedData)
    {
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("DELETE FROM UserAccount " +
                " WHERE lcUserName = @Username", conn);

        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
        //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

        int rowsAffected = 0;

        try
        {
            conn.Open();

            rowsAffected = cmd.ExecuteNonQuery();

            if (deleteAllRelatedData)
            {
                // Process commands to delete all data for the user in the database.
            }
        }
        catch (SqlException e)
        {
            if (WriteExceptionsToEventLog)
            {
                WriteToEventLog(e, "DeleteUser");

                throw new ProviderException(exceptionMessage);
            }
            else
            {
                throw e;
            }
        }
        finally
        {
            conn.Close();
        }

        if (rowsAffected > 0)
            return true;

        return false;
    }


    //
    // MembershipProvider.GetAllUsers
    //

    public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
    {
      SqlConnection conn = new SqlConnection(connectionString);
      SqlCommand cmd = new SqlCommand("SELECT Count(*) FROM UserAccount ", conn);
//      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ApplicationName;

      MembershipUserCollection users = new MembershipUserCollection();

      SqlDataReader reader = null;
      totalRecords = 0;

      try
      {
        conn.Open();
        totalRecords = (int)cmd.ExecuteScalar();

        if (totalRecords <= 0) { return users; }

        //cmd.CommandText = "SELECT iID, cUserName, cEmail, PWQuest," +
        //         " Comment, IsApproved, IsLocked, Created, LastLogin," +
        //         " LastAct, LastPWChg, LastLock " +
        //         " FROM UserAccount " + 
        //         " ORDER BY cUserName Asc";

        // pull all fields so the custom user object will have what it needs
          cmd.CommandText = "SELECT * " +
                 " FROM UserAccount " +
                 " ORDER BY cUserName Asc";

        reader = cmd.ExecuteReader();

          //TODO: Need a more efficient method of paging, in case this feature is used
          // at some point
          
          int counter = 0;
        int startIndex = pageSize * pageIndex;
        int endIndex = startIndex + pageSize - 1;

        while (reader.Read())
        {
          if (counter >= startIndex)
          {
            PRMembershipUser u = GetUserFromReader(reader);
            users.Add(u);
          }

          if (counter >= endIndex) { cmd.Cancel(); }

          counter++;
        }
      }
      catch (SqlException e)
      {
        if (WriteExceptionsToEventLog)
        {
          WriteToEventLog(e, "GetAllUsers ");

          throw new ProviderException(exceptionMessage);
        }
        else
        {
          throw e;
        }
      }
      finally
      {
        if (reader != null) { reader.Close(); }
         conn.Close();
      }

      return users;
    }


    //
    // MembershipProvider.GetNumberOfUsersOnline
    //

    public override int GetNumberOfUsersOnline()
    {

      TimeSpan onlineSpan = new TimeSpan(0, System.Web.Security.Membership.UserIsOnlineTimeWindow, 0);
      DateTime compareTime = DateTime.Now.Subtract(onlineSpan);

      SqlConnection conn = new SqlConnection(connectionString);
      //SqlCommand cmd = new SqlCommand("SELECT Count(*) FROM UserAccount " +
      //        " WHERE LastAct > @CompareDate ", conn);

        // account for possible null date
        SqlCommand cmd = new SqlCommand("SELECT Count(*) FROM UserAccount " +
              " WHERE LastAct IS NOT NULL AND LastAct > @CompareDate ", conn);

      cmd.Parameters.Add("@CompareDate", SqlDbType.DateTime).Value = compareTime;
//      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

      int numOnline = 0;

      try
      {
        conn.Open();

        numOnline = (int)cmd.ExecuteScalar();
      }
      catch (SqlException e)
      {
        if (WriteExceptionsToEventLog)
        {
          WriteToEventLog(e, "GetNumberOfUsersOnline");

          throw new ProviderException(exceptionMessage);
        }
        else
        {
          throw e;
        }
      }
      finally
      {
        conn.Close();
      }

      return numOnline;
    }



    //
    // MembershipProvider.GetPassword
    //

    //public string GetPasswordX(string username, string answer, int FacilityID)
    //{
    //  if (!EnablePasswordRetrieval)
    //  {
    //    throw new ProviderException("Password Retrieval Not Enabled.");
    //  }

    //  if (PasswordFormat == MembershipPasswordFormat.Hashed)
    //  {
    //    throw new ProviderException("Cannot retrieve Hashed passwords.");
    //  }

    //  SqlConnection conn = new SqlConnection(connectionString);
    //  SqlCommand cmd = new SqlCommand("SELECT cPW, PWAns, IsLocked FROM UserAccount " +
    //        " WHERE cUserName = @Username  AND FacilityID = @FacilityID", conn);

    //  cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //  cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //  //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

    //  string password = "";
    //  string passwordAnswer = "";
    //  SqlDataReader reader = null;

    //  try
    //  {
    //    conn.Open();

    //    reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

    //    if (reader.HasRows)
    //    {
    //      reader.Read();

    //      if (reader.GetBoolean(2))
    //        throw new MembershipPasswordException("The supplied user is locked out.");

    //    password = reader.GetString(0).Trim();
    //    passwordAnswer = reader.GetString(1).Trim();
    //    }
    //    else
    //    {
    //      throw new MembershipPasswordException("The supplied user name is not found.");
    //    }
    //  }
    //  catch (SqlException e)
    //  {
    //    if (WriteExceptionsToEventLog)
    //    {
    //      WriteToEventLog(e, "GetPassword");

    //      throw new ProviderException(exceptionMessage);
    //    }
    //    else
    //    {
    //      throw e;
    //    }
    //  }
    //  finally
    //  {
    //    if (reader != null) { reader.Close(); }
    //    conn.Close();
    //  }


    //  if (RequiresQuestionAndAnswer && !CheckPassword(answer, passwordAnswer))
    //  {
    //    UpdateFailureCountX(username, "passwordAnswer", FacilityID);

    //    throw new MembershipPasswordException("Incorrect password answer.");
    //  }


    //  if (PasswordFormat == MembershipPasswordFormat.Encrypted)
    //  {
    //    password = UnEncodePassword(password);
    //  }

    //  return password;
    //}


    public override string GetPassword(string username, string answer)
    {
        if (!EnablePasswordRetrieval)
        {
            throw new ProviderException("Password Retrieval Not Enabled.");
        }

        if (PasswordFormat == MembershipPasswordFormat.Hashed)
        {
            throw new ProviderException("Cannot retrieve Hashed passwords.");
        }

        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("SELECT cPW, PWAns, IsLocked FROM UserAccount " +
              " WHERE lcUserName = @Username", conn);

        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
        //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

        string password = "";
        string passwordAnswer = "";
        SqlDataReader reader = null;

        try
        {
            conn.Open();

            reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

            if (reader.HasRows)
            {
                reader.Read();

                if (reader.GetBoolean(2))
                    throw new MembershipPasswordException("The supplied user is locked out.");

                password = reader.GetString(0).Trim().ToLower();
                passwordAnswer = reader.GetString(1).Trim().ToLower();
            }
            else
            {
                throw new MembershipPasswordException("The supplied user name is not found.");
            }
        }
        catch (SqlException e)
        {
            if (WriteExceptionsToEventLog)
            {
                WriteToEventLog(e, "GetPassword");

                throw new ProviderException(exceptionMessage);
            }
            else
            {
                throw e;
            }
        }
        finally
        {
            if (reader != null) { reader.Close(); }
            conn.Close();
        }


        if (RequiresQuestionAndAnswer && !CheckPassword(answer.ToLower(), passwordAnswer.ToLower()))
        {
            UpdateFailureCount(username.ToLower(), "passwordAnswer");

            throw new MembershipPasswordException("Incorrect password answer.");
        }


        if (PasswordFormat == MembershipPasswordFormat.Encrypted)
        {
            password = UnEncodePassword(password);
        }

        return password.ToLower();
    }


    //
    // MembershipProvider.GetUser(string, bool)
    //

    //public MembershipUser GetUserX(string username, bool userIsOnline, int FacilityID)
    //{
    //   SqlConnection conn = new SqlConnection(connectionString);
    //   //SqlCommand cmd = new SqlCommand("SELECT iID, cUserName, cEmail, PWQuest," +
    //   //     " Comment, IsApproved, IsLocked, Created, LastLogin," +
    //   //     " LastAct, LastPWChg, LastLock," +
    //   //     " cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cExpires, " +
    //   //     " cFirstName, cFloridaNo, cInstitute, cLastName, cLectDate, " +
    //   //     " cMiddle, cPhone, cPromoCode, PromoID, FacilityID, cRefCode, cRegType, " +
    //   //     " cSocial, cState, cZipCode, SpecID, AgeGroup, " +
    //   //     " RegTypeID, RepID " +
    //   //     " FROM UserAccount WHERE cUserName = @Username", conn);

    //   // pull all fields so our custom user object won't fail
    //    SqlCommand cmd = new SqlCommand("SELECT * " +
    //" FROM UserAccount WHERE cUserName = @Username AND FacilityID = @FacilityID", conn);

    //  cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //  cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //  //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

    //  PRMembershipUser u = null;
    //  SqlDataReader reader = null;

    //  try
    //  {
    //    conn.Open();

    //    reader = cmd.ExecuteReader();

    //    if (reader.HasRows)
    //    {
    //      reader.Read();
    //      u = GetUserFromReader(reader);
          
    //      if (userIsOnline)
    //      {
    //        SqlCommand updateCmd = new SqlCommand("UPDATE UserAccount " +
    //                  "SET LastAct = @LastActivityDate " +
    //                  "WHERE cUserName = @Username AND FacilityID = @FacilityID", conn);

    //        updateCmd.Parameters.Add("@LastActivityDate", SqlDbType.DateTime).Value = DateTime.Now;
    //        updateCmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //        updateCmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //        //            updateCmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

    //        updateCmd.ExecuteNonQuery();
    //      }
    //    }

    //  }
    //  catch (SqlException e)
    //  {
    //    if (WriteExceptionsToEventLog)
    //    {
    //      WriteToEventLog(e, "GetUser(String, Boolean)");

    //      throw new ProviderException(exceptionMessage);
    //    }
    //    else
    //    {
    //      throw e;
    //    }
    //  }
    //  finally
    //  {
    //    if (reader != null) { reader.Close(); }

    //    conn.Close();
    //  }

    //  return u;      
    //}

    public override MembershipUser GetUser(string username, bool userIsOnline)
    {
        SqlConnection conn = new SqlConnection(connectionString);
        //SqlCommand cmd = new SqlCommand("SELECT iID, cUserName, cEmail, PWQuest," +
        //     " Comment, IsApproved, IsLocked, Created, LastLogin," +
        //     " LastAct, LastPWChg, LastLock," +
        //     " cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cExpires, " +
        //     " cFirstName, cFloridaNo, cInstitute, cLastName, cLectDate, " +
        //     " cMiddle, cPhone, cPromoCode, PromoID, FacilityID, cRefCode, cRegType, " +
        //     " cSocial, cState, cZipCode, SpecID, AgeGroup, " +
        //     " RegTypeID, RepID " +
        //     " FROM UserAccount WHERE cUserName = @Username", conn);

        // pull all fields so our custom user object won't fail
        SqlCommand cmd = new SqlCommand("SELECT * " +
    " FROM UserAccount WHERE lcUserName = @Username", conn);

        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
        //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

        PRMembershipUser u = null;
        SqlDataReader reader = null;

        try
        {
            conn.Open();

            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Read();
                u = GetUserFromReader(reader);

                if (userIsOnline)
                {
                    SqlCommand updateCmd = new SqlCommand("UPDATE UserAccount " +
                              "SET LastAct = @LastActivityDate " +
                              "WHERE lcUserName = @Username", conn);

                    updateCmd.Parameters.Add("@LastActivityDate", SqlDbType.DateTime).Value = DateTime.Now;
                    updateCmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
                    //            updateCmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

                    updateCmd.ExecuteNonQuery();
                }
            }

        }
        catch (SqlException e)
        {
            if (WriteExceptionsToEventLog)
            {
                WriteToEventLog(e, "GetUser(String, Boolean)");

                throw new ProviderException(exceptionMessage);
            }
            else
            {
                throw e;
            }
        }
        finally
        {
            if (reader != null) { reader.Close(); }

            conn.Close();
        }

        return u;
    }


    //
    // MembershipProvider.GetUser(object, bool)
    //

    public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
    {
      SqlConnection conn = new SqlConnection(connectionString);
      //SqlCommand cmd = new SqlCommand("SELECT iID, cUserName, cEmail, PWQuest," +
      //      " Comment, IsApproved, IsLocked, Created, LastLogin," +
      //      " LastAct, LastPWChg, LastLock," +
      //      " cAddress1, cAddress2, lAdmin, lPrimary, cBirthDate, cCity, cExpires, " +
      //      " cFirstName, cFloridaNo, cInstitute, cLastName, cLectDate, " +
      //      " cMiddle, cPhone, cPromoCode, PromoID, FacilityID, cRefCode, cRegType, " +
      //      " cSocial, cState, cZipCode, SpecID, AgeGroup, " +
      //      " RegTypeID, RepID " +
      //      " FROM UserAccount WHERE iID = @iID", conn);

      // pull all fields so our custom user object won't fail
      SqlCommand cmd = new SqlCommand("SELECT * " +
  " FROM UserAccount WHERE iID = @iID", conn);


      cmd.Parameters.Add("@iID", SqlDbType.Int).Value = providerUserKey;

      // Using custom PRMembershipUser class
      PRMembershipUser u = null;
      SqlDataReader reader = null;

      try
      {
        conn.Open();

        reader = cmd.ExecuteReader();

        if (reader.HasRows)
        {
          reader.Read();
          u = GetUserFromReader(reader);
          
          if (userIsOnline)
          {
            SqlCommand updateCmd = new SqlCommand("UPDATE UserAccount " +
                      "SET LastAct = @LastActivityDate " +
                      "WHERE iID = @iID", conn);

            updateCmd.Parameters.Add("@LastActivityDate", SqlDbType.DateTime).Value = DateTime.Now;
            updateCmd.Parameters.Add("@iID", SqlDbType.Int).Value = providerUserKey;

            updateCmd.ExecuteNonQuery();
          }
        }

      }
      catch (SqlException e)
      {
        if (WriteExceptionsToEventLog)
        {
          WriteToEventLog(e, "GetUser(Object, Boolean)");

          throw new ProviderException(exceptionMessage);
        }
        else
        {
          throw e;
        }
      }
      finally
      {
        if (reader != null) { reader.Close(); }

        conn.Close();
      }

      return u;      
    }


    //
    // GetUserFromReader
    //    A helper function that takes the current row from the SqlDataReader
    // and hydrates a MembershiUser from the values. Called by the 
    // MembershipUser.GetUser implementation.
    //

    private PRMembershipUser GetUserFromReader(SqlDataReader reader)
    {
      //object providerUserKey = reader.GetValue(0);
      //string username = reader.GetString(1);
      //string email = reader.GetString(2);

      //string passwordQuestion = "";
      //if (reader.GetValue(3) != DBNull.Value)
      //  passwordQuestion = reader.GetString(3);

      //string comment = "";
      //if (reader.GetValue(4) != DBNull.Value)
      //  comment = reader.GetString(4);

      //bool isApproved = reader.GetBoolean(5);
      //bool isLockedOut = reader.GetBoolean(6);
      //DateTime creationDate = reader.GetDateTime(7);

      //DateTime lastLoginDate = new DateTime();
      //if (reader.GetValue(8) != DBNull.Value)
      //  lastLoginDate = reader.GetDateTime(8);

      //DateTime lastActivityDate = reader.GetDateTime(9);
      //DateTime lastPasswordChangedDate = reader.GetDateTime(10);

      //DateTime lastLockedOutDate = new DateTime();
      //if (reader.GetValue(11) != DBNull.Value)
      //  lastLockedOutDate = reader.GetDateTime(11);

      PRMembershipUser u = new PRMembershipUser(this.Name,
                                            reader["lcUsername"].ToString().ToLower(),
                                            (int)reader["iID"],
                                            reader["cEmail"].ToString().Trim(),
                                            reader["PWQuest"].ToString().Trim(),
                                            reader["comment"].ToString().Trim(),
                                            (bool)reader["isApproved"],
                                            (bool)reader["isLocked"],
                                            (DateTime)(Convert.IsDBNull(reader["Created"]) ? DateTime.MinValue : (DateTime)reader["Created"]),
                                            (DateTime)(Convert.IsDBNull(reader["LastLogin"]) ? DateTime.MinValue : (DateTime)reader["LastLogin"]),
                                            (DateTime)(Convert.IsDBNull(reader["LastAct"]) ? DateTime.MinValue : (DateTime)reader["LastAct"]),
                                            (DateTime)(Convert.IsDBNull(reader["LastPWChg"]) ? DateTime.MinValue : (DateTime)reader["LastPWChg"]),
                                            (DateTime)(Convert.IsDBNull(reader["LastLock"]) ? DateTime.MinValue : (DateTime)reader["LastLock"]),
                                            reader["cFirstName"].ToString().Trim(),
                                            reader["cLastName"].ToString().Trim(),
                                            reader["cMiddle"].ToString().Trim(),
                                            reader["cAddress1"].ToString().Trim(),
                                            reader["cAddress2"].ToString().Trim(),
                                            reader["cCity"].ToString().Trim(),
                                            reader["cState"].ToString().Trim(),
                                            reader["cZipCode"].ToString().Trim(),
                                            reader["cPhone"].ToString().Trim(),
                                            reader["cSocial"].ToString().Trim(),
                                            reader["cBirthDate"].ToString().Trim(),
                                            reader["cPromoCode"].ToString().Trim(),
                                            (int)(Convert.IsDBNull(reader["PromoID"]) ? (int)0 : (int)reader["PromoID"]),
                                            (int)(Convert.IsDBNull(reader["FacilityID"]) ? (int)0 : (int)reader["FacilityID"]),
                                            reader["cRefCode"].ToString().Trim(),
                                            reader["cFloridaNo"].ToString().Trim(),
                                            reader["cLectDate"].ToString().Trim(),
                                            reader["cExpires"].ToString().Trim(),
                                            (bool)reader["lAdmin"],
                                            (bool)reader["lPrimary"],
                                            reader["cRegType"].ToString().Trim(),
                                            reader["cInstitute"].ToString().Trim(),
                                            (int)(Convert.IsDBNull(reader["SpecID"]) ? (int)0 : (int)reader["SpecID"]),
                                            (int)(Convert.IsDBNull(reader["AgeGroup"]) ? (int)0 : (int)reader["AgeGroup"]),
                                            (int)(Convert.IsDBNull(reader["RegTypeID"]) ? (int)0 : (int)reader["RegTypeID"]),
                                            (int)(Convert.IsDBNull(reader["RepID"]) ? (int)0 : (int)reader["RepID"]),
                                            reader["cUsername"].ToString().ToLower() );

      return u;
    }


    //
    // MembershipProvider.UnlockUser
    //

    //public bool UnlockUserX(string username, int FacilityID)
    //{
    //  SqlConnection conn = new SqlConnection(connectionString);
    //  SqlCommand cmd = new SqlCommand("UPDATE UserAccount " +
    //                                    " SET IsLocked = '0', LastLock = @LastLockedOutDate " +
    //                                    " WHERE cUserName = @Username AND FacilityID = @FacilityID", conn);

    //  cmd.Parameters.Add("@LastLockedOutDate", SqlDbType.DateTime).Value = DateTime.Now;
    //  cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //  cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //  //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

    //  int rowsAffected = 0;

    //  try
    //  {
    //    conn.Open();

    //    rowsAffected = cmd.ExecuteNonQuery();
    //  }
    //  catch (SqlException e)
    //  {
    //    if (WriteExceptionsToEventLog)
    //    {
    //      WriteToEventLog(e, "UnlockUser");

    //      throw new ProviderException(exceptionMessage);
    //    }
    //    else
    //    {
    //      throw e;
    //    }
    //  }
    //  finally
    //  {
    //    conn.Close();
    //  }

    //  if (rowsAffected > 0)
    //    return true;

    //  return false;      
    //}

    public override bool UnlockUser(string username)
    {
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("UPDATE UserAccount " +
                                          " SET IsLocked = '0', LastLock = @LastLockedOutDate " +
                                          " WHERE lcUserName = @Username", conn);

        cmd.Parameters.Add("@LastLockedOutDate", SqlDbType.DateTime).Value = DateTime.Now;
        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
        //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

        int rowsAffected = 0;

        try
        {
            conn.Open();

            rowsAffected = cmd.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            if (WriteExceptionsToEventLog)
            {
                WriteToEventLog(e, "UnlockUser");

                throw new ProviderException(exceptionMessage);
            }
            else
            {
                throw e;
            }
        }
        finally
        {
            conn.Close();
        }

        if (rowsAffected > 0)
            return true;

        return false;
    }

    //
    // MembershipProvider.GetUserNameByEmail
    //

    public override string GetUserNameByEmail(string email)
    {
      SqlConnection conn = new SqlConnection(connectionString);
      SqlCommand cmd = new SqlCommand("SELECT cUserName" +
            " FROM UserAccount WHERE cEmail = @Email", conn);

      cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = email;
//      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

      string username = "";

      try
      {
        conn.Open();

        username = (string)cmd.ExecuteScalar();
      }
      catch (SqlException e)
      {
        if (WriteExceptionsToEventLog)
        {
          WriteToEventLog(e, "GetUserNameByEmail");

          throw new ProviderException(exceptionMessage);
        }
        else
        {
          throw e;
        }
      }
      finally
      {
        conn.Close();
      }

      if (username == null)
        username = "";

      return username;
    }




    //
    // MembershipProvider.ResetPassword
    //

    //public string ResetPasswordX(string username, string answer, int FacilityID)
    //{
    //  if (!EnablePasswordReset)
    //  {
    //    throw new NotSupportedException("Password reset is not enabled.");
    //  }

    //  if (answer == null && RequiresQuestionAndAnswer)
    //  {
    //    UpdateFailureCountX(username, "passwordAnswer", FacilityID);

    //    throw new ProviderException("Password answer required for password reset.");
    //  }

    //  string newPassword = 
    //    System.Web.Security.Membership.GeneratePassword(newPasswordLength,MinRequiredNonAlphanumericCharacters);


    //  ValidatePasswordEventArgs args = 
    //    new ValidatePasswordEventArgs(username, newPassword, true);

    //  OnValidatingPassword(args);
  
    //  if (args.Cancel)
    //    if (args.FailureInformation != null)
    //      throw args.FailureInformation;
    //    else
    //      throw new MembershipPasswordException("Reset password canceled due to password validation failure.");


    //  SqlConnection conn = new SqlConnection(connectionString);
    //  SqlCommand cmd = new SqlCommand("SELECT PWAns, IsLocked FROM UserAccount " +
    //        " WHERE cUserName = @Username AND FacilityID = @FacilityID", conn);

    //  cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //  cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //  //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

    //  int rowsAffected = 0;
    //  string passwordAnswer = "";
    //  SqlDataReader reader = null;

    //  try
    //  {
    //    conn.Open();

    //    reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

    //    if (reader.HasRows)
    //    {
    //      reader.Read();

    //      if (reader.GetBoolean(1))
    //        throw new MembershipPasswordException("The supplied user is locked out.");

    //    passwordAnswer = reader.GetString(0).Trim();
    //    }
    //    else
    //    {
    //      throw new MembershipPasswordException("The supplied user name is not found.");
    //    }

    //    if (RequiresQuestionAndAnswer && !CheckPassword(answer, passwordAnswer))
    //    {
    //      UpdateFailureCountX(username, "passwordAnswer", FacilityID);

    //      throw new MembershipPasswordException("Incorrect password answer.");
    //    }

    //    SqlCommand updateCmd = new SqlCommand("UPDATE UserAccount " +
    //        " SET cPW = @Password, LastPWChg = @LastPasswordChangedDate" +
    //        " WHERE cUserName = @Username AND IsLocked = '0' AND FacilityID = @FacilityID", conn);

    //    updateCmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = EncodePassword(newPassword);
    //    updateCmd.Parameters.Add("@LastPasswordChangedDate", SqlDbType.DateTime).Value = DateTime.Now;
    //    updateCmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //    updateCmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //    //        updateCmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

    //    rowsAffected = updateCmd.ExecuteNonQuery();
    //  }
    //  catch (SqlException e)
    //  {
    //    if (WriteExceptionsToEventLog)
    //    {
    //      WriteToEventLog(e, "ResetPassword");

    //      throw new ProviderException(exceptionMessage);
    //    }
    //    else
    //    {
    //      throw e;
    //    }
    //  }
    //  finally
    //  {
    //    if (reader != null) { reader.Close(); }
    //    conn.Close();
    //  }

    //  if (rowsAffected > 0)
    //  {
    //    return newPassword;
    //  }
    //  else
    //  {
    //    throw new MembershipPasswordException("User not found, or user is locked out. Password not Reset.");
    //  }
    //}


    public override string ResetPassword(string username, string answer)
    {
        if (!EnablePasswordReset)
        {
            throw new NotSupportedException("Password reset is not enabled.");
        }

        if (answer == null && RequiresQuestionAndAnswer)
        {
            UpdateFailureCount(username.ToLower(), "passwordAnswer");

            throw new ProviderException("Password answer required for password reset.");
        }

        string newPassword =
          System.Web.Security.Membership.GeneratePassword(newPasswordLength, MinRequiredNonAlphanumericCharacters).ToLower();


        ValidatePasswordEventArgs args =
          new ValidatePasswordEventArgs(username.ToLower(), newPassword.ToLower(), true);

        OnValidatingPassword(args);

        if (args.Cancel)
            if (args.FailureInformation != null)
                throw args.FailureInformation;
            else
                throw new MembershipPasswordException("Reset password canceled due to password validation failure.");


        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("SELECT PWAns, IsLocked FROM UserAccount " +
              " WHERE lcUserName = @Username ", conn);

        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
        //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

        int rowsAffected = 0;
        string passwordAnswer = "";
        SqlDataReader reader = null;

        try
        {
            conn.Open();

            reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

            if (reader.HasRows)
            {
                reader.Read();

                if (reader.GetBoolean(1))
                    throw new MembershipPasswordException("The supplied user is locked out.");

                passwordAnswer = reader.GetString(0).Trim().ToLower();
            }
            else
            {
                throw new MembershipPasswordException("The supplied user name is not found.");
            }

            if (RequiresQuestionAndAnswer && !CheckPassword(answer.ToLower(), passwordAnswer.ToLower()))
            {
                UpdateFailureCount(username.ToLower(), "passwordAnswer");

                throw new MembershipPasswordException("Incorrect password answer.");
            }

            SqlCommand updateCmd = new SqlCommand("UPDATE UserAccount " +
                " SET cPW = @Password, LastPWChg = @LastPasswordChangedDate" +
                " WHERE lcUserName = @Username AND IsLocked = '0'", conn);

            updateCmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = EncodePassword(newPassword);
            updateCmd.Parameters.Add("@LastPasswordChangedDate", SqlDbType.DateTime).Value = DateTime.Now;
            updateCmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
            //        updateCmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

            rowsAffected = updateCmd.ExecuteNonQuery();
        }
        catch (SqlException e)
        {
            if (WriteExceptionsToEventLog)
            {
                WriteToEventLog(e, "ResetPassword");

                throw new ProviderException(exceptionMessage);
            }
            else
            {
                throw e;
            }
        }
        finally
        {
            if (reader != null) { reader.Close(); }
            conn.Close();
        }

        if (rowsAffected > 0)
        {
            return newPassword.ToLower();
        }
        else
        {
            throw new MembershipPasswordException("User not found, or user is locked out. Password not Reset.");
        }
    }

    //
    // MembershipProvider.UpdateUser
    //

    public override void UpdateUser(MembershipUser user)
    {
      SqlConnection conn = new SqlConnection(connectionString);

        // NOTE: This update command builds on the normal .NET standard
        // for UpdateUser, which is to update only three fields:
        // email, comment and isapproved.
        // We take it further and update all of our other fields that go
        // beyond the standard MembershipUser properties.
        //
        // NOTE: The standard .NET UpdateUser method does not touch the
        // other important user fields like lastlogin, etc because those
        // are all updated by other methods in the MembershipProvider class.
        //
        // We also use the iID field as the PK to lookup the user
        // instead of the standard username used by the default
        // MembershipProvider class -- this does not affect the way this
        // method is called because we pull the iID right off
        // the PRMembershipUser instance being passed in
        SqlCommand cmd = new SqlCommand("UPDATE UserAccount " +
              " SET cEmail = @Email, " +
              "Comment = @Comment, " +
              "IsApproved = @IsApproved, " +
            "LCUserName = @UserName, " +
            "LCEmail = @LCEmail, " +
            "cAddress1 = @cAddress1, " +
            "cAddress2 = @cAddress2, " +
            "lAdmin = @lAdmin, " +
            "lPrimary = @lPrimary, " +
            "cBirthDate = @cBirthDate, " +
            "cCity = @cCity, " +
            "cExpires = @cExpires, " +
            "cFirstName = @cFirstName, " +
            "cFloridaNo = @cFloridaNo, " +
            "cInstitute = @cInstitute, " +
            "cLastName = @cLastName, " +
            "cLectDate = @cLectDate, " +
            "cMiddle = @cMiddle, " +
            "cPhone = @cPhone, " +
            "cPromoCode = @cPromoCode, " +
            "PromoID = @PromoID, " +
            "FacilityID = @FacilityID, " +
            "cRefCode = @cRefCode, " +
            "cRegType = @cRegType, " +
            "cSocial = @cSocial, " +
            "cState = @cState, " +
            "cZipCode = @cZipCode, " +
            "SpecID = @SpecID, " +
            "AgeGroup = @AgeGroup, " +
            "RegTypeID = @RegTypeID, " +
            "RepID = @RepID, " +
            "cUserName = @cUserName " +
              " WHERE iID = @iID", conn);


      // using custom PRMembershipUser class to cast user
      // so we can access the extended properties  
      PRMembershipUser u = (PRMembershipUser)user;

      cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = u.Email;
      cmd.Parameters.Add("@Comment", SqlDbType.VarChar).Value = u.Comment;
      cmd.Parameters.Add("@IsApproved", SqlDbType.Bit).Value = u.IsApproved;

        // TODO: Add more here and to the SQL statement above
      // now our custom properties - these might be useful for queries
      cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = u.UserName.ToLower();
      cmd.Parameters.Add("@LCEmail", SqlDbType.VarChar).Value = u.Email.ToLower();

      // and the normal UserAccount fields that aren't already above
      cmd.Parameters.Add("@cAddress1", SqlDbType.VarChar).Value = u.cAddress1;
      cmd.Parameters.Add("@cAddress2", SqlDbType.VarChar).Value = u.cAddress2;
      cmd.Parameters.Add("@lAdmin", SqlDbType.Bit).Value = u.lAdmin;
      cmd.Parameters.Add("@lPrimary", SqlDbType.Bit).Value = u.lPrimary;
      cmd.Parameters.Add("@cBirthDate", SqlDbType.VarChar).Value = u.cBirthDate;
      cmd.Parameters.Add("@cCity", SqlDbType.VarChar).Value = u.cCity;
      cmd.Parameters.Add("@cExpires", SqlDbType.VarChar).Value = u.cExpires;
      cmd.Parameters.Add("@cFirstName", SqlDbType.VarChar).Value = u.cFirstName;
      cmd.Parameters.Add("@cFloridaNo", SqlDbType.VarChar).Value = u.cFloridaNo;
      cmd.Parameters.Add("@cInstitute", SqlDbType.VarChar).Value = u.cInstitute;
      cmd.Parameters.Add("@cLastName", SqlDbType.VarChar).Value = u.cLastName;
      cmd.Parameters.Add("@cLectDate", SqlDbType.VarChar).Value = u.cLectDate;
      cmd.Parameters.Add("@cMiddle", SqlDbType.VarChar).Value = u.cMiddle;
      cmd.Parameters.Add("@cPhone", SqlDbType.VarChar).Value = u.cPhone;
      cmd.Parameters.Add("@cPromoCode", SqlDbType.VarChar).Value = u.cPromoCode;

      // pass null to database if value is zero (nothing selected by user)
      // this allows the database to have a NULL foreign key and still enforce relational integrity
      if (u.PromoID == 0)
      {
          cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = System.DBNull.Value;
      }
      else
      {
          cmd.Parameters.Add("@PromoID", SqlDbType.Int).Value = u.PromoID;
      }

      // pass null to database if value is zero (nothing selected by user)
      // this allows the database to have a NULL foreign key and still enforce relational integrity
      if (u.FacilityID == 0)
      {
          cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = System.DBNull.Value;
      }
      else
      {
          cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = u.FacilityID;
      }

      cmd.Parameters.Add("@cRefCode", SqlDbType.VarChar).Value = u.cRefCode;
      cmd.Parameters.Add("@cRegType", SqlDbType.VarChar).Value = u.cRegType;
      cmd.Parameters.Add("@cSocial", SqlDbType.VarChar).Value = u.cSocial;
      cmd.Parameters.Add("@cState", SqlDbType.VarChar).Value = u.cState;
      cmd.Parameters.Add("@cZipCode", SqlDbType.VarChar).Value = u.cZipCode;

      // pass null to database if value is zero (nothing selected by user)
      // this allows the database to have a NULL foreign key and still enforce relational integrity
      if (u.SpecID == 0)
      {
          cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = System.DBNull.Value;
      }
      else
      {
          cmd.Parameters.Add("@SpecID", SqlDbType.Int).Value = u.SpecID;
      }

      // pass null to database if value is zero (nothing selected by user)
      // this allows the database to have a NULL foreign key and still enforce relational integrity
      if (u.AgeGroup == 0)
      {
          cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = System.DBNull.Value;
      }
      else
      {
          cmd.Parameters.Add("@AgeGroup", SqlDbType.Int).Value = u.AgeGroup;
      }

      // pass null to database if value is zero (nothing selected by user)
      // this allows the database to have a NULL foreign key and still enforce relational integrity
      if (u.RegTypeID == 0)
      {
          cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = System.DBNull.Value;
      }
      else
      {
          cmd.Parameters.Add("@RegTypeID", SqlDbType.Int).Value = u.RegTypeID;
      }

      // pass null to database if value is zero (nothing selected by user)
      // this allows the database to have a NULL foreign key and still enforce relational integrity
      if (u.RepID == 0)
      {
          cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = System.DBNull.Value;
      }
      else
      {
          cmd.Parameters.Add("@RepID", SqlDbType.Int).Value = u.RepID;
      }

      cmd.Parameters.Add("@cUserName", SqlDbType.VarChar).Value = u.cUserName.ToLower();

        // for the where clause
      cmd.Parameters.Add("@iID", SqlDbType.Int).Value = (int)u.ProviderUserKey;
      //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

      try
      {
        conn.Open();

        cmd.ExecuteNonQuery();
      }
      catch (SqlException e)
      {
        if (WriteExceptionsToEventLog)
        {
          WriteToEventLog(e, "UpdateUser");

          throw new ProviderException(exceptionMessage);
        }
        else
        {
          throw e;
        }
      }
      finally
      {
        conn.Close();
      }
    }


    //
    // MembershipProvider.ValidateUser
    //

    public bool ValidatePRUser(string username, string password)
    {
        bool isValid = false;
        bool IsPRRetail = false;
        String lcusername = string.Empty;
        
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("SELECT cPW, IsApproved,lcusername FROM UserAccount " +
                " WHERE cUserName = @Username AND IsLocked = '0' AND FacilityID = 3", conn);
        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
        SqlDataReader reader = null;
        SqlDataReader rdr = null;
        bool isApproved = false;
        string pwd = "";

        try
        {
            conn.Open();

            reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

            if (reader.HasRows)
            {
                reader.Read();
                pwd = reader.GetString(0).Trim();
                isApproved = reader.GetBoolean(1);
                lcusername = reader.GetString(2).Trim();
                reader.Close();
            }
            else
            {
                reader.Close();
                SqlCommand command = new SqlCommand("select cpw,IsApproved,lcusername from useraccount " +
                    " WHERE cUsername = @Username AND FacilityID in ( select facid from facilitygroup where alterid>0 and active=0)", conn);
                command.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
               
                 try
                {
                    
                    rdr = command.ExecuteReader(CommandBehavior.SingleRow);
                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        pwd = rdr.GetString(0).Trim();
                        isApproved = rdr.GetBoolean(1);
                        lcusername = rdr.GetString(2).Trim();
                        IsPRRetail = true;
                        rdr.Close();
                    }
                    else
                    {
                        return false;
                        rdr.Close();
                    }

                }
                catch (SqlException e)
                {
                    throw e;

                }
               
            }
                   
            if (CheckPassword(password, pwd))
            {
                if (isApproved)
                {
                    isValid = true;
                    SqlCommand updateCmd;
                    if (IsPRRetail)
                    {
                        updateCmd = new SqlCommand("UPDATE UserAccount SET LastLogin = @LastLoginDate, facilityid=3 , comment = 'PR User Converted' , cexpires = ' " + System.DateTime.Now.AddDays(-1).ToShortDateString() +
                                                               "' , lcusername= '" + (100003).ToString().Substring(1) + username + " ' WHERE lcUserName = @LUsername", conn);

                        SqlCommand updateRoleCmd;

                        updateRoleCmd = new SqlCommand("update usersinroles SET username = '" + (100003).ToString().Substring(1) + username + " ' , facilityid=3  WHERE username = @LUsername", conn);
                        updateRoleCmd.Parameters.Add("@LUsername", SqlDbType.VarChar).Value = lcusername;

                        updateRoleCmd.ExecuteNonQuery();
                    }
                    else
                    {
                       updateCmd = new SqlCommand("UPDATE UserAccount SET LastLogin = @LastLoginDate" +
                                                                " WHERE lcUserName = @LUsername AND FacilityID = 3", conn);
                    }

                    updateCmd.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = DateTime.Now;
                    updateCmd.Parameters.Add("@LUsername", SqlDbType.VarChar).Value = lcusername;
                                       //            updateCmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

                    updateCmd.ExecuteNonQuery();
                }
            }
            else
            {
                conn.Close();

                //UpdateFailureCountX(username, "password", FacilityID);
            }
        }
        catch (SqlException e)
        {
            if (WriteExceptionsToEventLog)
            {
                WriteToEventLog(e, "ValidateUser");

                throw new ProviderException(exceptionMessage);
            }
            else
            {
                throw e;
            }
        }
        finally
        {
            if (reader != null) { reader.Close(); }
            if (rdr != null) { rdr.Close(); }
            conn.Close();
        }

        return isValid;
    }
    int _loginUserID = 0;
    int _loginNurseID = 0;
    string _loginHash = string.Empty;
    string _loginGUID = string.Empty;

    public string loginHash { get { return _loginHash; } }
    public string loginGUID { get { return _loginGUID; } }
    public int loginUserID { get { return _loginUserID; } }
    public int loginNurseID { get { return _loginNurseID; } }

    public bool MSLoginUser(string emailAddress, int facilityId, string password, bool persistlogin, string browserAgent, string loginIP)
    {
        bool isValid = false;

        SqlConnection conn = new SqlConnection(sconnectionString);
        SqlConnection uconn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("SP_LOGIN_USER", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = emailAddress.ToLower();
        cmd.Parameters.Add("@siteID", SqlDbType.Int).Value = facilityId;
        cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = password;
        cmd.Parameters.Add("@persistLogin", SqlDbType.Bit).Value = persistlogin ? 1 : 0;
        cmd.Parameters.Add("@browserAgent", SqlDbType.VarChar).Value = browserAgent;
        cmd.Parameters.Add("@ip", SqlDbType.VarChar).Value = loginIP;

        SqlDataReader reader = null;
        
        try
        {
            conn.Open();

            reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

            if (reader.HasRows)
            {
                reader.Read();
                _loginUserID = reader.GetInt32(0);
                _loginHash = reader.GetString(2).Trim();
                _loginGUID = reader.GetGuid(3).ToString().Trim();
                _passwordExpires = reader.GetDateTime(4);
                isValid = true;
                uconn.Open();
                SqlCommand updateCmd = new SqlCommand("UPDATE UserAccount SET LastLogin = @LastLoginDate" +
                                                        " WHERE cemail = @EmailAddress  and facilityId=2", uconn);

                updateCmd.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = DateTime.Now;
                updateCmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar).Value = emailAddress.ToLower();                

                updateCmd.ExecuteNonQuery();
            }
            else
            {
                UpdateFailureCount(emailAddress.ToLower(), "password");                            
            }

            reader.Close();
            conn.Close();
            uconn.Close();
        }
        catch (SqlException e)
        {
            if (WriteExceptionsToEventLog)
            {
                WriteToEventLog(e, "ValidateUser");

                throw new ProviderException(exceptionMessage);
            }
            else
            {
                throw e;
            }
        }
        finally
        {
            if (reader != null) { reader.Close(); }
            conn.Close();
        }

        return isValid;


        //SqlConnection conn = new SqlConnection(connectionString);        
        //SqlCommand cmd = new SqlCommand("SELECT cPW, IsApproved FROM UserAccount " +
        //       " WHERE cemail = @EmailAddress  and facilityId=@FacilityId", conn);

        //cmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar).Value = emailAddress.ToLower();
        //cmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = facilityId;

        //SqlDataReader reader = null;
        //bool isApproved = false;
        //string pwd = "";

        //try
        //{
        //    conn.Open();

        //    reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

        //    if (reader.HasRows)
        //    {
        //        reader.Read();
        //        pwd = reader.GetString(0).Trim().ToLower();
        //        isApproved = reader.GetBoolean(1);
        //    }
        //    else
        //    {
        //        return false;
        //    }

        //    reader.Close();

        //    if (CheckPassword(password.ToLower(), pwd.ToLower()))
        //    {
        //        //if (isApproved)
        //        //{
        //        isValid = true;

        //        SqlCommand updateCmd = new SqlCommand("UPDATE UserAccount SET LastLogin = @LastLoginDate" +
        //                                                " WHERE cemail = @EmailAddress  and facilityId=@FacilityId", conn);

        //        updateCmd.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = DateTime.Now;
        //        updateCmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar).Value = emailAddress.ToLower();
        //        updateCmd.Parameters.Add("@FacilityId", SqlDbType.Int).Value = facilityId;
                
        //        updateCmd.ExecuteNonQuery();
        //        //}
        //    }
        //    else
        //    {
        //        conn.Close();

        //        UpdateFailureCount(emailAddress.ToLower(), "password");
        //    }
        //}
        //catch (SqlException e)
        //{
        //    if (WriteExceptionsToEventLog)
        //    {
        //        WriteToEventLog(e, "ValidateUser");

        //        throw new ProviderException(exceptionMessage);
        //    }
        //    else
        //    {
        //        throw e;
        //    }
        //}
        //finally
        //{
        //    if (reader != null) { reader.Close(); }
        //    conn.Close();
        //}

        //return isValid;
    }

    DateTime _passwordExpires;

    public DateTime PasswordExpires { get { return _passwordExpires; } }

    
    public int ValidateUser(HttpCookie loginHash, HttpCookie loginGUID, int facilityid, string browserAgent, string loginIP)
    {
        int iuserid = 0;
        try
        {
            if (loginHash != null & loginGUID != null)
            {
                if (loginHash.Value != null & loginGUID.Value != null)
                {
                    SqlConnection conn = new SqlConnection(sconnectionString);
                    SqlCommand cmd = new SqlCommand("SP_VALIDATE_USER", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@loginHash", SqlDbType.VarChar).Value = loginHash.Value.ToString();
                    cmd.Parameters.Add("@loginGUID", SqlDbType.UniqueIdentifier).Value = new Guid(loginGUID.Value.ToString());
                    cmd.Parameters.Add("@siteID", SqlDbType.TinyInt).Value = facilityid;
                    cmd.Parameters.Add("@browserAgent", SqlDbType.VarChar).Value = browserAgent;
                    cmd.Parameters.Add("@ip", SqlDbType.VarChar).Value = loginIP;
                    SqlDataReader reader = null;

                    conn.Open();
                    reader = cmd.ExecuteReader(CommandBehavior.SingleRow);
                    if (reader.HasRows)
                    {
                        reader.Read();
                        iuserid = reader.GetInt32(0);
                        _loginUserID = iuserid;
                        _loginNurseID = reader.GetInt32(1);
                        _passwordExpires = reader.GetDateTime(2);
                        reader.Close();
                    }
                    conn.Close();
                }  
            }
            return iuserid;
        }
        catch (Exception ex) { return 0; }     
    }

    public void MSLogoutUser(int iuserid, int facilityid) 
    {        
        try
        {
            if (loginHash != null & loginGUID != null)
            {
                SqlConnection conn = new SqlConnection(sconnectionString);
                SqlCommand cmd = new SqlCommand("SP_LOGOUT_USER", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@userID", SqlDbType.Int).Value = iuserid;                
                cmd.Parameters.Add("@siteID", SqlDbType.TinyInt).Value = facilityid;
                conn.Open();
                cmd.ExecuteNonQuery();
               
                conn.Close();
            }            
        }
        catch (Exception ex) {  }   
    }
    public override bool ValidateUser(string username, string password)
    {
        bool isValid = false;

        SqlConnection conn = new SqlConnection(connectionString);
        //SqlCommand cmd = new SqlCommand("SELECT cPW, IsApproved FROM UserAccount " +
        //        " WHERE lcUserName = @Username AND IsLocked = '0'", conn);
        SqlCommand cmd = new SqlCommand("SELECT cPW, IsApproved FROM UserAccount " +
               " WHERE lcUserName = @Username ", conn);

        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();

        SqlDataReader reader = null;
        bool isApproved = false;
        string pwd = "";

        try
        {
            conn.Open();

            reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

            if (reader.HasRows)
            {
                reader.Read();
                pwd = reader.GetString(0).Trim().ToLower();
                isApproved = reader.GetBoolean(1);
            }
            else
            {
                return false;
            }

            reader.Close();

            if (CheckPassword(password.ToLower(), pwd.ToLower()))
            {
                //if (isApproved)
                //{
                    isValid = true;

                    SqlCommand updateCmd = new SqlCommand("UPDATE UserAccount SET LastLogin = @LastLoginDate" +
                                                            " WHERE lcUserName = @Username", conn);

                    updateCmd.Parameters.Add("@LastLoginDate", SqlDbType.DateTime).Value = DateTime.Now;
                    updateCmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
                    //            updateCmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

                    updateCmd.ExecuteNonQuery();
                //}
            }
            else
            {
                conn.Close();

                UpdateFailureCount(username.ToLower(), "password");
            }
        }
        catch (SqlException e)
        {
            if (WriteExceptionsToEventLog)
            {
                WriteToEventLog(e, "ValidateUser");

                throw new ProviderException(exceptionMessage);
            }
            else
            {
                throw e;
            }
        }
        finally
        {
            if (reader != null) { reader.Close(); }
            conn.Close();
        }

        return isValid;
    }


    //
    // UpdateFailureCount
    //   A helper method that performs the checks and updates associated with
    // password failure tracking.
    //

    //private void UpdateFailureCountX(string username, string failureType, int FacilityID)
    //{
    //  SqlConnection conn = new SqlConnection(connectionString);
    //  SqlCommand cmd = new SqlCommand("SELECT XPWAtt, " +
    //                                    "  XPWAttSt, " +
    //                                    "  XPWAnsAtt, " +
    //                                    "  XPWAnsSt " + 
    //                                    "  FROM UserAccount " +
    //                                    "  WHERE cUserName = @Username AND FacilityID = @FacilityID", conn);

    //  cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //  cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //  //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

    //  SqlDataReader reader = null;
    //  DateTime windowStart = new DateTime();
    //  int failureCount = 0;

    //  try
    //  {
    //    conn.Open();

    //    reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

    //    if (reader.HasRows)
    //    {
    //      reader.Read();

    //      if (failureType == "password")
    //      {
    //        failureCount = reader.GetInt32(0);
    //        //windowStart = reader.GetDateTime(1);
    //        // handle null in database with DateTime.MinValue
    //        windowStart = (DateTime)(Convert.IsDBNull(reader["XPWAttSt"]) ? DateTime.MinValue : (DateTime)reader["XPWAttSt"]);
    //      }

    //      if (failureType == "passwordAnswer")
    //      {
    //        failureCount = reader.GetInt32(2);
    //        //windowStart = reader.GetDateTime(3);
    //        // handle null in database with DateTime.MinValue
    //        windowStart = (DateTime)(Convert.IsDBNull(reader["XPWAnsSt"]) ? DateTime.MinValue : (DateTime)reader["XPWAnsSt"]);
    //      }
    //    }

    //    reader.Close();

    //    DateTime windowEnd = windowStart.AddMinutes(PasswordAttemptWindow);

    //    if (failureCount == 0 || DateTime.Now > windowEnd)
    //    {
    //      // First password failure or outside of PasswordAttemptWindow. 
    //      // Start a new password failure count from 1 and a new window starting now.

    //      if (failureType == "password")
    //        cmd.CommandText = "UPDATE UserAccount " +
    //                          "  SET XPWAtt = @Count, " +
    //                          "      XPWAttSt = @WindowStart " +
    //                          "  WHERE cUserName = @Username AND FacilityID = @FacilityID";

    //      if (failureType == "passwordAnswer")
    //        cmd.CommandText = "UPDATE UserAccount " +
    //                          "  SET XPWAnsAtt = @Count, " +
    //                          "      XPWAnsSt = @WindowStart " +
    //                          "  WHERE cUserName = @Username AND FacilityID = @FacilityID";

    //      cmd.Parameters.Clear();

    //      cmd.Parameters.Add("@Count", SqlDbType.Int).Value = 1;
    //      cmd.Parameters.Add("@WindowStart", SqlDbType.DateTime).Value = DateTime.Now;
    //      cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //      cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //      //          cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

    //      if (cmd.ExecuteNonQuery() < 0)
    //        throw new ProviderException("Unable to update failure count and window start.");
    //    }
    //    else
    //    {
    //      if (failureCount++ >= MaxInvalidPasswordAttempts)
    //      {
    //        // Password attempts have exceeded the failure threshold. Lock out
    //        // the user.

    //        cmd.CommandText = "UPDATE UserAccount " +
    //                          "  SET IsLocked = @IsLockedOut, LastLock = @LastLockedOutDate " +
    //                          "  WHERE cUserName = @Username AND FacilityID = @FacilityID";

    //        cmd.Parameters.Clear();

    //        cmd.Parameters.Add("@IsLockedOut", SqlDbType.Bit).Value = true;
    //        cmd.Parameters.Add("@LastLockedOutDate", SqlDbType.DateTime).Value = DateTime.Now;
    //        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //        cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //        //            cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

    //        if (cmd.ExecuteNonQuery() < 0)
    //          throw new ProviderException("Unable to lock out user.");
    //      }
    //      else
    //      {
    //        // Password attempts have not exceeded the failure threshold. Update
    //        // the failure counts. Leave the window the same.

    //        if (failureType == "password")
    //          cmd.CommandText = "UPDATE UserAccount " +
    //                            "  SET XPWAtt = @Count" +
    //                            "  WHERE cUserName = @Username AND FacilityID = @FacilityID";

    //        if (failureType == "passwordAnswer")
    //          cmd.CommandText = "UPDATE UserAccount " +
    //                            "  SET XPWAnsAtt = @Count" +
    //                            "  WHERE cUserName = @Username AND FacilityID = @FacilityID";

    //         cmd.Parameters.Clear();

    //         cmd.Parameters.Add("@Count", SqlDbType.Int).Value = failureCount;
    //         cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username;
    //         cmd.Parameters.Add("@FacilityID", SqlDbType.Int).Value = FacilityID;
    //         //             cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

    //         if (cmd.ExecuteNonQuery() < 0)
    //           throw new ProviderException("Unable to update failure count.");
    //      }
    //    }
    //  }
    //  catch (SqlException e)
    //  {
    //    if (WriteExceptionsToEventLog)
    //    {
    //      WriteToEventLog(e, "UpdateFailureCount");

    //      throw new ProviderException(exceptionMessage);
    //    }
    //    else
    //    {
    //      throw e;
    //    }
    //  }
    //  finally
    //  {
    //    if (reader != null) { reader.Close(); }
    //    conn.Close();
    //  }       
    //}


    private void UpdateFailureCount(string username, string failureType)
    {
        SqlConnection conn = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("SELECT XPWAtt, " +
                                          "  XPWAttSt, " +
                                          "  XPWAnsAtt, " +
                                          "  XPWAnsSt " +
                                          "  FROM UserAccount " +
                                          "  WHERE lcUserName = @Username", conn);

        cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
        //      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

        SqlDataReader reader = null;
        DateTime windowStart = new DateTime();
        int failureCount = 0;

        try
        {
            conn.Open();

            reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

            if (reader.HasRows)
            {
                reader.Read();

                if (failureType == "password")
                {
                    failureCount = reader.GetInt32(0);
                    //windowStart = reader.GetDateTime(1);
                    // handle null in database with DateTime.MinValue
                    windowStart = (DateTime)(Convert.IsDBNull(reader["XPWAttSt"]) ? DateTime.MinValue : (DateTime)reader["XPWAttSt"]);
                }

                if (failureType == "passwordAnswer")
                {
                    failureCount = reader.GetInt32(2);
                    //windowStart = reader.GetDateTime(3);
                    // handle null in database with DateTime.MinValue
                    windowStart = (DateTime)(Convert.IsDBNull(reader["XPWAnsSt"]) ? DateTime.MinValue : (DateTime)reader["XPWAnsSt"]);
                }
            }

            reader.Close();

            DateTime windowEnd = windowStart.AddMinutes(PasswordAttemptWindow);

            if (failureCount == 0 || DateTime.Now > windowEnd)
            {
                // First password failure or outside of PasswordAttemptWindow. 
                // Start a new password failure count from 1 and a new window starting now.

                if (failureType == "password")
                    cmd.CommandText = "UPDATE UserAccount " +
                                      "  SET XPWAtt = @Count, " +
                                      "      XPWAttSt = @WindowStart " +
                                      "  WHERE lcUserName = @Username";

                if (failureType == "passwordAnswer")
                    cmd.CommandText = "UPDATE UserAccount " +
                                      "  SET XPWAnsAtt = @Count, " +
                                      "      XPWAnsSt = @WindowStart " +
                                      "  WHERE lcUserName = @Username";

                cmd.Parameters.Clear();

                cmd.Parameters.Add("@Count", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@WindowStart", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
                //          cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

                if (cmd.ExecuteNonQuery() < 0)
                    throw new ProviderException("Unable to update failure count and window start.");
            }
            else
            {
                if (failureCount++ >= MaxInvalidPasswordAttempts)
                {
                    // Password attempts have exceeded the failure threshold. Lock out
                    // the user.

                    cmd.CommandText = "UPDATE UserAccount " +
                                      "  SET IsLocked = @IsLockedOut, LastLock = @LastLockedOutDate " +
                                      "  WHERE lcUserName = @Username";

                    cmd.Parameters.Clear();

                    cmd.Parameters.Add("@IsLockedOut", SqlDbType.Bit).Value = true;
                    cmd.Parameters.Add("@LastLockedOutDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
                    //            cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

                    if (cmd.ExecuteNonQuery() < 0)
                        throw new ProviderException("Unable to lock out user.");
                }
                else
                {
                    // Password attempts have not exceeded the failure threshold. Update
                    // the failure counts. Leave the window the same.

                    if (failureType == "password")
                        cmd.CommandText = "UPDATE UserAccount " +
                                          "  SET XPWAtt = @Count" +
                                          "  WHERE lcUserName = @Username";

                    if (failureType == "passwordAnswer")
                        cmd.CommandText = "UPDATE UserAccount " +
                                          "  SET XPWAnsAtt = @Count" +
                                          "  WHERE lcUserName = @Username";

                    cmd.Parameters.Clear();

                    cmd.Parameters.Add("@Count", SqlDbType.Int).Value = failureCount;
                    cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = username.ToLower();
                    //             cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

                    if (cmd.ExecuteNonQuery() < 0)
                        throw new ProviderException("Unable to update failure count.");
                }
            }
        }
        catch (SqlException e)
        {
            if (WriteExceptionsToEventLog)
            {
                WriteToEventLog(e, "UpdateFailureCount");

                throw new ProviderException(exceptionMessage);
            }
            else
            {
                throw e;
            }
        }
        finally
        {
            if (reader != null) { reader.Close(); }
            conn.Close();
        }
    }

    //
    // CheckPassword
    //   Compares password values based on the MembershipPasswordFormat.
    //

    private bool CheckPassword(string password, string dbpassword)
    {
        string pass1 = password.Trim().ToLower();
        string pass2 = dbpassword.Trim().ToLower();

      switch (PasswordFormat)
      {
        case MembershipPasswordFormat.Encrypted:
              pass2 = UnEncodePassword(dbpassword).ToLower();
          break;
        case MembershipPasswordFormat.Hashed:
          pass1 = EncodePassword(password.ToLower());
          break;
        default:
          break;
      }

      if (pass1 == pass2)
      {
        return true;
      }

      return false;
    }


    //
    // EncodePassword
    //   Encrypts, Hashes, or leaves the password clear based on the PasswordFormat.
    //

    private string EncodePassword(string password)
    {
        string encodedPassword = password.Trim();

      switch (PasswordFormat)
      {
        case MembershipPasswordFormat.Clear:
          break;
        case MembershipPasswordFormat.Encrypted:
          encodedPassword = 
            //Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));
            Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password.Replace(" ", "+")))); //bsk
            
          break;
        case MembershipPasswordFormat.Hashed:
          HMACSHA1 hash = new HMACSHA1();
          hash.Key = HexToByte(machineKey.ValidationKey);
          encodedPassword = 
            //Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
            Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password.Replace(" ", "+")))); //bsk
          break;
        default:
          throw new ProviderException("Unsupported password format.");
      }

      return encodedPassword;
    }


    //
    // UnEncodePassword
    //   Decrypts or leaves the password clear based on the PasswordFormat.
    //

    private string UnEncodePassword(string encodedPassword)
    {
      string password = encodedPassword;

      switch (PasswordFormat)
      {
        case MembershipPasswordFormat.Clear:
          break;
        case MembershipPasswordFormat.Encrypted:
          password =
              Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password.Replace(" ", "+"))));    //Bhaskar           
            //Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password))); //bhaskar 
          break;
        case MembershipPasswordFormat.Hashed:
          throw new ProviderException("Cannot unencode a hashed password.");
        default:
          throw new ProviderException("Unsupported password format.");
      }

      return password.Trim().ToLower();
    }

    //
    // HexToByte
    //   Converts a hexadecimal string to a byte array. Used to convert encryption
    // key values from the configuration.
    //

    private byte[] HexToByte(string hexString)
    {
      byte[] returnBytes = new byte[hexString.Length / 2];
      for (int i = 0; i < returnBytes.Length; i++)
        returnBytes[i] = Convert.ToByte(hexString.Substring(i*2, 2), 16);
      return returnBytes;
    }


    //
    // MembershipProvider.FindUsersByName
    //

    public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
    {

      SqlConnection conn = new SqlConnection(connectionString);
      SqlCommand cmd = new SqlCommand("SELECT Count(*) FROM UserAccount " +
                "WHERE cUserName LIKE @UsernameSearch", conn);
      cmd.Parameters.Add("@UsernameSearch", SqlDbType.VarChar).Value = usernameToMatch;
//      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = pApplicationName;

      MembershipUserCollection users = new MembershipUserCollection();

      SqlDataReader reader = null;

      try
      {
        conn.Open();
        totalRecords = (int)cmd.ExecuteScalar();

        if (totalRecords <= 0) { return users; }

        //cmd.CommandText = "SELECT iID, cUserName, cEmail, PWQuest," +
        //  " Comment, IsApproved, IsLocked, Created, LastLogin," +
        //  " LastAct, LastPWChg, LastLock " +
        //  " FROM UserAccount " +
        //  " WHERE cUserName LIKE @UsernameSearch " +
        //  " ORDER BY cUserName Asc";

        // select all fields so the custom user object can have all the data we might need
          cmd.CommandText = "SELECT * " +
        " FROM UserAccount " +
        " WHERE cUserName LIKE @UsernameSearch " +
        " ORDER BY cUserName Asc";

        reader = cmd.ExecuteReader();

        int counter = 0;
        int startIndex = pageSize * pageIndex;
        int endIndex = startIndex + pageSize - 1;

        while (reader.Read())
        {
          if (counter >= startIndex)
          {
            PRMembershipUser u = GetUserFromReader(reader);
            users.Add(u);
          }

          if (counter >= endIndex) { cmd.Cancel(); }

          counter++;
        }
      }
      catch (SqlException e)
      {
        if (WriteExceptionsToEventLog)
        {
          WriteToEventLog(e, "FindUsersByName");

          throw new ProviderException(exceptionMessage);
        }
        else
        {
          throw e;
        }
      }
      finally
      {
        if (reader != null) { reader.Close(); }

        conn.Close();
      }

      return users;
    }

    //
    // MembershipProvider.FindUsersByEmail
    //

    public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
    {
      SqlConnection conn = new SqlConnection(connectionString);
      SqlCommand cmd = new SqlCommand("SELECT Count(*) FROM UserAccount " +
                                        "WHERE cEmail LIKE @EmailSearch ", conn);
      cmd.Parameters.Add("@EmailSearch", SqlDbType.VarChar).Value = emailToMatch;
//      cmd.Parameters.Add("@ApplicationName", SqlDbType.VarChar).Value = ApplicationName;

      MembershipUserCollection users = new MembershipUserCollection();

      SqlDataReader reader = null;
      totalRecords = 0;

      try
      {
        conn.Open();
        totalRecords = (int)cmd.ExecuteScalar();

        if (totalRecords <= 0) { return users; }

        //cmd.CommandText = "SELECT iID, cUserName, cEmail, PWQuest," +
        //         " Comment, IsApproved, IsLocked, Created, LastLogin," +
        //         " LastAct, LastPWChg, LastLock " +
        //         " FROM UserAccount " +
        //         " WHERE cEmail LIKE @EmailSearch " +
        //         " ORDER BY cUserName Asc";

        // select all fields so the custom user object can have all the data we might need
        cmd.CommandText = "SELECT * " +
              " FROM UserAccount " +
              " WHERE cEmail LIKE @EmailSearch " +
              " ORDER BY cUserName Asc";
        
          reader = cmd.ExecuteReader();

        int counter = 0;
        int startIndex = pageSize * pageIndex;
        int endIndex = startIndex + pageSize - 1;

        while (reader.Read())
        {
          if (counter >= startIndex)
          {
            PRMembershipUser u = GetUserFromReader(reader);
            users.Add(u);
          }
    
          if (counter >= endIndex) { cmd.Cancel(); }
    
          counter++;
        }
      }
      catch (SqlException e)
      {
        if (WriteExceptionsToEventLog)
        {
          WriteToEventLog(e, "FindUsersByEmail");

          throw new ProviderException(exceptionMessage);
        }
        else
        {
          throw e;
        }
      }
      finally
      {
        if (reader != null) { reader.Close(); }
    
        conn.Close();
      }
    
      return users;
    }


    //
    // WriteToEventLog
    //   A helper function that writes exception detail to the event log. Exceptions
    // are written to the event log as a security measure to avoid private database
    // details from being returned to the browser. If a method does not return a status
    // or boolean indicating the action succeeded or failed, a generic exception is also 
    // thrown by the caller.
    //

    private void WriteToEventLog(Exception e, string action)
    {
      EventLog log = new EventLog();
      log.Source = eventSource;
      log.Log = eventLog;

      string message = "An exception occurred communicating with the data source.\n\n";
      message += "Action: " + action + "\n\n";
      message += "Exception: " + e.ToString();

      log.WriteEntry(message);
    }

  }
}    
    
    
    
    
    
    
    
    
