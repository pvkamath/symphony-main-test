﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PearlsReview.Constants;
using PayPal;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;
using PayPal.Payments.Common.Utility;

/// <summary>
/// Summary description for PCI
/// </summary>
public class ccPCI
{   
    
    public Dictionary<string, string> Params;
	public ccPCI()
	{
        Params = new Dictionary<string, string>();
        Params.Add("CREATESECURETOKEN","Y");
        Params.Add("CURRENCY", "USD");
        Params.Add("TENDER", "C");
        Params.Add("SILENTTRAN","TRUE");		
	}

    public Response GetSecuretoken()
    {
        Params.Add("SECURETOKENID", Guid.NewGuid().ToString());
        PayflowConnectionData connection = new PayflowConnectionData("pilot-payflowpro.paypal.com");
        UserInfo info = new UserInfo(PaypalAccConstants.User,PaypalAccConstants.Vendor,PaypalAccConstants.Partner,PaypalAccConstants.Pwd);
        BaseTransaction trans = new BaseTransaction("S", info,connection,PayflowUtility.RequestId);
        foreach(var param in Params)
        {
            trans.AddToExtendData(new ExtendData(param.Key, param.Value));
        }
        try
        {
            return trans.SubmitTransaction();
        }
        catch (Exception ex)
        {
            throw;
        }
    }
}
