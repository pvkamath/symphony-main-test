/*
* credit card processing code provided for free use in the .NET community by
* Rick Strahl, www.west-wind.com
*/
using System;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Globalization;

using Westwind.InternetTools;
using Westwind.Tools;

using PayPal.Payments.Common;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;
using System.Collections.Generic;
using System.Configuration;

namespace Westwind.WebStore
{
    /// <summary>
    ///
    /// </summary>
    public class ccPayFlowPro_Net_v4 : ccProcessing
    {
        /// <summary>
        /// Port on the server (Verisign)
        /// </summary>
        public int HostPort
        {
            get { return _HostPort; }
            set { _HostPort = value; }
        }
        private int _HostPort = 443;


        public string ProxyAddress
        {
            get { return _ProxyAddress; }
            set { _ProxyAddress = value; }
        }
        private string _ProxyAddress = "";


        public int ProxyPort
        {
            get { return _ProxyPort; }
            set { _ProxyPort = value; }
        }
        private int _ProxyPort = 0;


        public string ProxyUsername
        {
            get { return _ProxyUsername; }
            set { _ProxyUsername = value; }
        }
        private string _ProxyUsername = "";

        public string ProxyPassword
        {
            get { return _ProxyPassword; }
            set { _ProxyPassword = value; }
        }
        private string _ProxyPassword = "";

        /// <summary>
        /// Sign up partner ID. Required only if you signed up through
        /// a third party.
        /// </summary>
        public string SignupPartner
        {
            get { return _SignupPartner; }
            set { _SignupPartner = value; }
        }
        private string _SignupPartner = "Verisign";

        /// <summary>
        /// Overridden consistency with API names.
        /// maps to MerchantId
        /// </summary>
        public string UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }
        private string _UserId = "";


        public int UniqueId
        {
            get { return _UniqueId; }
            set { _UniqueId = value; }
        }
        private int _UniqueId = 0;

        public string ProfileId
        {
            get { return _ProfileId; }
            set { _ProfileId = value; }
        }
        private string _ProfileId = "";

        /// <summary>
        /// Internal string value used to hold the values sent to the server
        /// </summary>
        string Parameters = "";

        /// <summary>
        /// Validates the credit card. Supported transactions include only Sale or Credit.
        /// Credits should have a negative sales amount.
        /// </summary>
        /// <returns></returns>
        public override bool ValidateCard()
        {
            if (!base.ValidateCard())
                return false;

            this.Error = false;
            this.ErrorMessage = "";

            // *** Counter that holds our parameter string to send
            this.Parameters = "";

            // *** Sale and Credit Supported
            decimal TOrderAmount = this.OrderAmount;
            if (this.OrderAmount < 0.00M)
            {
                this.Parameters = "TRXTYPE=C";
                TOrderAmount = this.OrderAmount * -1.0M;
            }
            else
                this.Parameters = "TRXTYPE=S";

            string CardNo = Regex.Replace(this.CreditCardNumber, @"[ -/._#]", "");

            // don't allow Discover cards to pass this point
            bool bIsDiscover = false;
            string cCardNoTrimmed = CardNo.Trim();

            if (cCardNoTrimmed.Length > 5 && cCardNoTrimmed.StartsWith("622"))
            {
                // could be Discover, so see if it's between 622126 and 622925
                int iValue = Int32.Parse(cCardNoTrimmed.Substring(0, 6));
                if ((iValue > 622125) && (iValue < 622926))
                {
                    bIsDiscover = true;
                }
            }
            else if (cCardNoTrimmed.Length > 3 && cCardNoTrimmed.StartsWith("6011"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("644"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("645"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("646"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("647"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("648"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("649"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 1 && cCardNoTrimmed.StartsWith("65"))
            {
                bIsDiscover = true;
            }

            if (bIsDiscover == true)
            {
                this.ValidatedResult = "FAILED";
                this.ValidatedMessage = "We are sorry that we cannot accept Discover cards.";
                return false;
            }


            string TUserId = this.UserId;

            if (TUserId == "")
                TUserId = this.MerchantId;

            string ExpDate = string.Format("{0:##}", this.CreditCardExpirationMonth);
            ExpDate += this.CreditCardExpirationYear;

            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            //UserInfo User = new UserInfo("OnCourseCE", "OnCourseCE", "Verisign", "okm1qaz");
            //UserInfo User = new UserInfo(this.MerchantId, this.MerchantId, this.SignupPartner, this.MerchantPassword);



            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;
            if (CardNo == "4111111111111111")
            {
                Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com/transaction/");
            }
            else
            {
                Connection = new PayflowConnectionData("payflowpro.verisign.com/transaction/");
            }

            // Create a new Invoice data object with the Amount, Billing Address etc. details.
            Invoice Inv = new Invoice();

            // Set Amount.
            Currency Amt = new Currency(TOrderAmount);
            Inv.Amt = Amt;
            Inv.PoNum = "";
            Inv.InvNum = "";
            Inv.Comment1 = this.Comment;
            Inv.Comment2 = "UserName: " + this.OrderId;

            if (this.TaxAmount > 0.00M)
            {
                Currency TaxAmount = new Currency(this.TaxAmount);
                Inv.TaxAmt = TaxAmount;
            }

            // Set the Billing Address details.
            BillTo Bill = new BillTo();
            Bill.FirstName = this.Firstname;
            Bill.LastName = this.Lastname;
            Bill.Street = this.Address;
            Bill.City = this.City;
            Bill.State = this.State;
            Bill.Zip = this.Zip;
            Bill.Email = this.Email;


            Inv.BillTo = Bill;

            // Create a new Payment Device - Credit Card data object.
            // The input parameters are Credit Card Number and Expiration Date of the Credit Card.
            CreditCard CC = new CreditCard(CardNo, ExpDate);

            if (this.SecurityCode != "")
                CC.Cvv2 = this.SecurityCode;

            // Create a new Tender - Card Tender data object.
            CardTender Card = new CardTender(CC);


            // Create a new Sale Transaction.
            SaleTransaction Trans = new SaleTransaction(
                User, Connection, Inv, Card, PayflowUtility.RequestId);

            this.RawProcessorResult = "";

            Response Resp = null;
            try
            {
                // Submit the Transaction
                Resp = Trans.SubmitTransaction();
            }
            catch (Exception ex)
            {
                this.ValidatedResult = "FAILED";
                this.ValidatedMessage = ex.Message;
                this.LogTransaction();
                return false;
            }

            // Display the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                if (TrxnResponse != null)
                {

                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();
                    this.TransactionId = TrxnResponse.Pnref;

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        this.ValidatedResult = "APPROVED";
                        this.AuthorizationCode = TrxnResponse.AuthCode;
                        this.LogTransaction();
                        return true;
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = " DECLINED: Credit card authorization was declined.";
                    }

                }

            }


            this.Error = true;
            this.LogTransaction();
            return false;
        }

        public static string GetSecureToken(Invoice Inv)
        {
            PayflowConnectionData Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com", 443);
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            SaleTransaction Trans = new SaleTransaction(User, Connection, Inv, null, PayflowUtility.RequestId);

            // Set VERBOSITY to High
            Trans.Verbosity = "High";
            List<ExtendData> extenddata = new List<ExtendData>();
            extenddata.Add(new ExtendData("CREATESECURETOKEN", "Y"));
            extenddata.Add(new ExtendData("SECURETOKENID", PayflowUtility.RequestId));
            extenddata.Add(new ExtendData("SILENTTRAN", "TRUE"));
            extenddata.Add(new ExtendData("RETURNURL", "https://www.continuingeducation.com/aboutus.aspx"));

            foreach (var item in extenddata)
            {
                Trans.AddToExtendData(item);
            }

            Trans.SubmitTransaction();

            return Trans.Response.ResponseString;
            //"&ORDERID="+ oUserAccount.LCEmail +
            //        "&CREATESECURETOKEN=Y&SECURETOKENID="+ securetoken +
            //        "&VENDOR=OnCourseCE&PARTNER=VeriSign&USER=CEOnline&PWD=xaw6udRE" +
            //        "&RETURNURL=https://www.continuingeducation.com/aboutus.aspx"+
            //        "&CANCELURL=https://www.continuingeducation.com/aboutus.aspx" +
            //        //"&CANCELURL=" + Request.Url.AbsolutePath +
            //        "&ERRORURL=https://www.continuingeducation.com/aboutus.aspx" +
            //        //"&AMT=1.00"+
            //        "&AMT=" + TotalAmount.ToString() +
            //        "&TRXTYPE=S&CURRENCY=USD&TENDER=C&SILENTTRAN=TRUE"
        }

        public bool AddUpdateProfileWithTransaction(string AuthCode, Decimal Amount, DateTime StartDate)
        {
            string CardNo = Regex.Replace(this.CreditCardNumber, @"[ -/._#]", "");

            string TUserId = this.UserId;

            if (TUserId == "")
                TUserId = this.MerchantId;

            string ExpDate = string.Format("{0:##}", this.CreditCardExpirationMonth);
            ExpDate += this.CreditCardExpirationYear;
            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            //UserInfo User = new UserInfo("OnCourseCE", "OnCourseCE", "Verisign", "okm1qaz");
            //UserInfo User = new UserInfo(this.MerchantId, this.MerchantId, this.SignupPartner, this.MerchantPassword);



            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;
            if (CardNo == "4111111111111111")
            {
                Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com/transaction/");
            }
            else
            {
                Connection = new PayflowConnectionData("payflowpro.verisign.com/transaction/");
            }

            // Create a new Invoice data object with the Amount, Billing Address etc. details.
            Invoice Inv = new Invoice();

            // Set Amount.
            Currency Amt = new Currency((decimal)Amount);
            Inv.Amt = Amt;
            Inv.PoNum = "";
            Inv.InvNum = "";
            Inv.Comment1 = this.Comment;
            Inv.Comment2 = "UserName: " + this.OrderId;

            if (this.TaxAmount > 0.00M)
            {
                Currency TaxAmount = new Currency(this.TaxAmount);
                Inv.TaxAmt = TaxAmount;
            }

            // Set the Billing Address details.
            BillTo Bill = new BillTo();
            Bill.FirstName = this.Firstname;
            Bill.LastName = this.Lastname;
            Bill.Street = this.Address;
            Bill.City = this.City;
            Bill.State = this.State;
            Bill.Zip = this.Zip;
            Bill.Email = this.Email;


            Inv.BillTo = Bill;

            // Create a new Payment Device - Credit Card data object.
            // The input parameters are Credit Card Number and Expiration Date of the Credit Card.
            CreditCard CC = new CreditCard(CardNo, ExpDate);

            if (this.SecurityCode != "")
                CC.Cvv2 = this.SecurityCode;

            // Create a new Tender - Card Tender data object.
            CardTender Card = new CardTender(CC);

            RecurringInfo RecurInfo = new RecurringInfo();

            // The date that the first payment will be processed.
            // Note - this value must be greater
            // than the current date! We will charge the customer
            // for the first month in a sep. Transaction
            // Format must be mmddyyyyDateTime StartBilling = DateTime.Now.AddMonths(1);

            //RecurInfo.Start = System.DateTime.Today.AddYears(1).ToString("MMddyyyy");
            //Fix the Next Payment Date
            RecurInfo.Start = StartDate.ToString("MMddyyyy");
            RecurInfo.ProfileName = "UCErenewal" + UniqueId;
            // Specifies how often the payment occurs. All PAYPERIOD values must use
            // capital letters and can be any of WEEK / BIWK / SMMO / FRWK / MONT /
            // QTER / SMYR / YEAR
            RecurInfo.PayPeriod = "YEAR";
            RecurInfo.Term = 0; // Number of payments 0 = indef.
            RecurInfo.RetryNumDays = 2;
            RecurInfo.MaxFailPayments = 1;
            // Perform an Optional Transaction. Verify that we need to do this!!
            //RecurInfo.OptionalTrx = "S"; // S = Sale, A = Authorization

            RecurInfo.OrigProfileId = AuthCode;

            // Set the amount if doing a "Sale" for the Optional Transaction.
            //Currency oTrxAmt = new Currency((decimal)(0.0), "USD");
            //RecurInfo.OptionalTrxAmt = oTrxAmt;

            // Create a new Recurring Add Transaction.
            RecurringAddTransaction Trans = new RecurringAddTransaction(
            User, Connection, Inv, Card, RecurInfo, PayflowUtility.RequestId);
            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if ((TrxnResponse != null) && (RecurResponse != null))
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        this.ValidatedResult = "APPROVED";
                        ProfileId = RecurResponse.ProfileId;
                        this.LogTransaction();
                        return true;
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                        return false;
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                        return false;
                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED: Credit card authorization was declined.";
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public bool CheckRecurringProfile(string AuthCode)
        {

            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;
            if (AuthCode.Contains("RT"))
            {
                Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com/transaction/");
            }
            else
            {
                Connection = new PayflowConnectionData("payflowpro.verisign.com/transaction/");
            }
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            //UserInfo User = new UserInfo("OnCourseCE", "OnCourseCE", "Verisign", "okm1qaz");
            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.

            RecurringInfo RecurInfo = new RecurringInfo();
            RecurInfo.OrigProfileId = AuthCode;
            PayPal.Payments.Transactions.RecurringInquiryTransaction Transaction = new RecurringInquiryTransaction(User, Connection, RecurInfo, PayflowUtility.RequestId);

            Response Resp = Transaction.SubmitTransaction();
            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if ((TrxnResponse != null) && (RecurResponse != null))
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        if (Convert.ToString(RecurResponse.Status).ToUpper() == "ACTIVE")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                        return false;
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                        return false;
                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED: Credit card authorization was declined.";
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        //check if the profile is  DEACTIVATED BY MERCHANT
        public bool IsProfileDeactive(string Profileid)
        {

            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;
            if (Profileid.Contains("RT"))
            {
                Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com/transaction/");
            }
            else
            {
                Connection = new PayflowConnectionData("payflowpro.verisign.com/transaction/");
            }
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            //UserInfo User = new UserInfo("OnCourseCE", "OnCourseCE", "Verisign", "okm1qaz");
            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.

            RecurringInfo RecurInfo = new RecurringInfo();
            RecurInfo.OrigProfileId = Profileid;
            PayPal.Payments.Transactions.RecurringInquiryTransaction Transaction = new RecurringInquiryTransaction(User, Connection, RecurInfo, PayflowUtility.RequestId);

            Response Resp = Transaction.SubmitTransaction();
            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if ((TrxnResponse != null) && (RecurResponse != null))
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        if (Convert.ToString(RecurResponse.Status).ToUpper() != "ACTIVE")
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                        return false;
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                        return false;
                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED: Credit card authorization was declined.";
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }


        public bool CancelProfileWithTransaction(string AuthCode)
        {

            string CardNo = Regex.Replace(this.CreditCardNumber, @"[ -/._#]", "");

            string TUserId = this.UserId;

            if (TUserId == "")
                TUserId = this.MerchantId;

            string ExpDate = string.Format("{0:##}", this.CreditCardExpirationMonth);
            ExpDate += this.CreditCardExpirationYear;
            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            //UserInfo User = new UserInfo("OnCourseCE", "OnCourseCE", "Verisign", "okm1qaz");

            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;
            if (AuthCode.Contains("RT"))
            {
                Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com/transaction/");
            }
            else
            {
                Connection = new PayflowConnectionData("payflowpro.verisign.com/transaction/");
            }


            // Create a new Payment Device - Credit Card data object.
            // The input parameters are Credit Card Number and Expiration Date of the Credit Card.
            CreditCard CC = new CreditCard(CardNo, ExpDate);

            if (this.SecurityCode != "")
                CC.Cvv2 = this.SecurityCode;

            // Create a new Tender - Card Tender data object.
            CardTender Card = new CardTender(CC);

            RecurringInfo RecurInfo = new RecurringInfo();

            // The date that the first payment will be processed.
            // Note - this value must be greater
            // than the current date! We will charge the customer
            // for the first month in a sep. Transaction
            // Format must be mmddyyyyDateTime StartBilling = DateTime.Now.AddMonths(1);

            //RecurInfo.Start = System.DateTime.Today.AddYears(1).ToString("MMddyyyy");
            RecurInfo.ProfileName = "UCErenewal" + UniqueId;
            // Specifies how often the payment occurs. All PAYPERIOD values must use
            // capital letters and can be any of WEEK / BIWK / SMMO / FRWK / MONT /
            // QTER / SMYR / YEAR
            RecurInfo.PayPeriod = "YEAR";
            RecurInfo.Term = 0; // Number of payments 0 = indef.
            RecurInfo.RetryNumDays = 2;

            // Perform an Optional Transaction. Verify that we need to do this!!
            //RecurInfo.OptionalTrx = "S"; // S = Sale, A = Authorization

            RecurInfo.OrigProfileId = AuthCode;

            // Set the amount if doing a "Sale" for the Optional Transaction.
            //Currency oTrxAmt = new Currency((decimal)(0.0), "USD");
            //RecurInfo.OptionalTrxAmt = oTrxAmt;

            // Create a new Recurring Add Transaction.
            RecurringCancelTransaction Trans = new RecurringCancelTransaction(
            User, Connection, RecurInfo, PayflowUtility.RequestId);

            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if ((TrxnResponse != null) && (RecurResponse != null))
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        this.ValidatedResult = "APPROVED";
                        ProfileId = RecurResponse.ProfileId;
                        this.LogTransaction();
                        return true;
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                        return false;
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                        return false;
                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED: Credit card authorization was declined.";
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public bool CancelProfilePCI(string ProfileID)
        {
            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");

            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;
            if (ProfileID.Contains("RT"))
            {
                Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com/transaction/");
            }
            else
            {
                Connection = new PayflowConnectionData("payflowpro.verisign.com/transaction/");
            }
            RecurringInfo RecurInfo = new RecurringInfo();

            RecurInfo.OrigProfileId = ProfileID;

            // Create a new Recurring Add Transaction.
            RecurringCancelTransaction Trans = new RecurringCancelTransaction(
            User, Connection, RecurInfo, PayflowUtility.RequestId);

            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if ((TrxnResponse != null) && (RecurResponse != null))
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        this.ValidatedResult = "APPROVED";
                        ProfileId = RecurResponse.ProfileId;
                        this.LogTransaction();
                        return true;
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                        return false;
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                        return false;
                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED:";
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool IsCardValid()
        {
            if (!base.ValidateCard())
                return false;
            return true;
        }

        public bool ModifyProfileWithTransaction(string AuthCode)
        {
            if (!base.ValidateCard())
                return false;

            this.Error = false;
            this.ErrorMessage = "";

            // *** Counter that holds our parameter string to send
            this.Parameters = "";

            string CardNo = Regex.Replace(this.CreditCardNumber, @"[ -/._#]", "");

            // don't allow Discover cards to pass this point
            bool bIsDiscover = false;
            string cCardNoTrimmed = CardNo.Trim();

            if (cCardNoTrimmed.Length > 5 && cCardNoTrimmed.StartsWith("622"))
            {
                // could be Discover, so see if it's between 622126 and 622925
                int iValue = Int32.Parse(cCardNoTrimmed.Substring(0, 6));
                if ((iValue > 622125) && (iValue < 622926))
                {
                    bIsDiscover = true;
                }
            }
            else if (cCardNoTrimmed.Length > 3 && cCardNoTrimmed.StartsWith("6011"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("644"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("645"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("646"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("647"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("648"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 2 && cCardNoTrimmed.StartsWith("649"))
            {
                bIsDiscover = true;
            }
            else if (cCardNoTrimmed.Length > 1 && cCardNoTrimmed.StartsWith("65"))
            {
                bIsDiscover = true;
            }

            if (bIsDiscover == true)
            {
                this.ValidatedResult = "FAILED";
                this.ValidatedMessage = "We are sorry that we cannot accept Discover cards.";
                return false;
            }


            string TUserId = this.UserId;

            if (TUserId == "")
                TUserId = this.MerchantId;

            string ExpDate = string.Format("{0:##}", this.CreditCardExpirationMonth);
            ExpDate += this.CreditCardExpirationYear;

            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection = new PayflowConnectionData();
            RecurringInfo RecurInfo = new RecurringInfo();
            RecurInfo.OrigProfileId = AuthCode;
            RecurInfo.MaxFailPayments = 1;

            if (CardNo == "4111111111111111")
            {
                Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com/transaction/");
            }
            else
            {
                Connection = new PayflowConnectionData("payflowpro.verisign.com/transaction/");
            }

            // Create a new Invoice data object with the Amount, Billing Address etc. details.
            Invoice Inv = new Invoice();

            //// Set Amount.
            Currency Amt = new Currency(this.OrderAmount);
            Inv.Amt = Amt;
            Inv.PoNum = "";
            Inv.InvNum = "";
            Inv.Comment1 = this.Comment;
            Inv.Comment2 = "UserName: " + this.OrderId;

            if (this.TaxAmount > 0.00M)
            {
                Currency TaxAmount = new Currency(this.TaxAmount);
                Inv.TaxAmt = TaxAmount;
            }

            // Set the Billing Address details.
            BillTo Bill = new BillTo();
            Bill.FirstName = this.Firstname;
            Bill.LastName = this.Lastname;
            Bill.Street = this.Address;
            Bill.City = this.City;
            Bill.State = this.State;
            Bill.Zip = this.Zip;
            Bill.Email = this.Email;
            Inv.BillTo = Bill;

            // Create a new Payment Device - Credit Card data object.
            // The input parameters are Credit Card Number and Expiration Date of the Credit Card.
            CreditCard CC = new CreditCard(CardNo, ExpDate);

            if (this.SecurityCode != "")
                CC.Cvv2 = this.SecurityCode;

            // Create a new Tender - Card Tender data object.
            CardTender Card = new CardTender(CC);


            // Create a new Sale Transaction.
            PayPal.Payments.Transactions.RecurringModifyTransaction Trans = new RecurringModifyTransaction(User, Connection, RecurInfo, Inv, Card, PayflowUtility.RequestId);

            this.RawProcessorResult = "";

            Response Resp = null;
            try
            {
                // Submit the Transaction
                Resp = Trans.SubmitTransaction();
            }
            catch (Exception ex)
            {
                this.ValidatedResult = "FAILED";
                this.ValidatedMessage = ex.Message;
                this.LogTransaction();
                return false;
            }

            // Display the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                if (TrxnResponse != null)
                {

                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();
                    this.TransactionId = TrxnResponse.Pnref;

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        this.ValidatedResult = "APPROVED";
                        this.AuthorizationCode = TrxnResponse.AuthCode;
                        this.LogTransaction();
                        return true;
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED: Credit card authorization was declined.";
                    }

                }

            }


            this.Error = true;
            this.LogTransaction();
            return false;
        }


        public string GetProfileByAuthode(string AuthCode)
        {
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            //UserInfo User = new UserInfo("OnCourseCE", "OnCourseCE", "Verisign", "okm1qaz");
            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection = new PayflowConnectionData();

            RecurringInfo RecurInfo = new RecurringInfo();
            RecurInfo.OrigProfileId = AuthCode;
            PayPal.Payments.Transactions.RecurringInquiryTransaction Transaction = new RecurringInquiryTransaction(User, RecurInfo, PayflowUtility.RequestId);

            Response Resp = Transaction.SubmitTransaction();
            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if ((TrxnResponse != null) && (RecurResponse != null))
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        if (Convert.ToString(RecurResponse.Status).ToUpper() == "ACTIVE")
                        {
                            return RecurResponse.ExpDate;
                        }
                        else
                        {
                            return "";
                        }
                    }

                    else
                    {
                        return "";
                    }

                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }
        }


        public bool ActivateProfileWithTransaction(string AuthCode)
        {
            this.Error = false;
            this.ErrorMessage = "";

            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            //Cretae RecurInfo
            RecurringInfo RecurInfo = new RecurringInfo();
            RecurInfo.OrigProfileId = AuthCode;
            RecurInfo.ProfileName = "UCErenewal" + UniqueId;
            RecurInfo.PayPeriod = "YEAR";
            RecurInfo.Term = 0; // Number of payments 0 = indef.
            RecurInfo.RetryNumDays = 2;
            RecurInfo.MaxFailPayments = 1;
            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;
            if (AuthCode.Contains("RT"))
            {
                Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com/transaction/");
            }
            else
            {
                Connection = new PayflowConnectionData("payflowpro.verisign.com/transaction/");
            }

            // Activate a Recur Transaction.
            PayPal.Payments.Transactions.RecurringReActivateTransaction Trans = new RecurringReActivateTransaction(User, Connection, RecurInfo, PayflowUtility.RequestId);

            this.RawProcessorResult = "";

            Response Resp = null;
            try
            {
                // Submit the Transaction
                Resp = Trans.SubmitTransaction();
            }
            catch (Exception ex)
            {
                this.ValidatedResult = "FAILED";
                this.ValidatedMessage = ex.Message;
                this.LogTransaction();
                return false;
            }

            // Display the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                if (TrxnResponse != null)
                {

                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();
                    this.TransactionId = TrxnResponse.Pnref;

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        this.ValidatedResult = "APPROVED";
                        this.AuthorizationCode = TrxnResponse.AuthCode;
                        this.LogTransaction();
                        return true;
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED: Credit card authorization was declined.";
                    }

                }

            }
            this.Error = true;
            this.LogTransaction();
            return false;
        }

        public bool ReActivateProfilePCI(string ProfileID)
        {
            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");

            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;
            if (ProfileID.Contains("RT"))
            {
                Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com/transaction/");
            }
            else
            {
                Connection = new PayflowConnectionData("payflowpro.verisign.com/transaction/");
            }
            RecurringInfo RecurInfo = new RecurringInfo();

            RecurInfo.OrigProfileId = ProfileID;

            // Create a new Recurring Add Transaction.      

            RecurringReActivateTransaction Trans = new RecurringReActivateTransaction(
            User, Connection, RecurInfo, PayflowUtility.RequestId);

            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if ((TrxnResponse != null) && (RecurResponse != null))
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        this.ValidatedResult = "APPROVED";
                        ProfileId = RecurResponse.ProfileId;
                        this.LogTransaction();
                        return true;
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                        return false;
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                        return false;
                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED:";
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public bool ModifyProfilePCI(string ProfileID, decimal Amount)
        {
            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");

            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;
            if (ProfileID.Contains("RT"))
            {
                Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com/transaction/");
            }
            else
            {
                Connection = new PayflowConnectionData("payflowpro.verisign.com/transaction/");
            }
            RecurringInfo RecurInfo = new RecurringInfo();

            RecurInfo.OrigProfileId = ProfileID;

            Invoice Inv = new Invoice();

            // Set Amount.
            Currency Amt = new Currency((decimal)Amount);
            Inv.Amt = Amt;
            Inv.PoNum = "";
            Inv.InvNum = "";
            Inv.Comment1 = this.Comment;
            Inv.Comment2 = "UserName: " + this.OrderId;

            if (this.TaxAmount > 0.00M)
            {
                Currency TaxAmount = new Currency(this.TaxAmount);
                Inv.TaxAmt = TaxAmount;
            }

            // Set the Billing Address details.
            BillTo Bill = new BillTo();
            Bill.FirstName = this.Firstname;
            Bill.LastName = this.Lastname;
            Bill.Street = this.Address;
            Bill.City = this.City;
            Bill.State = this.State;
            Bill.Zip = this.Zip;
            Bill.Email = this.Email;

            Inv.BillTo = Bill;

            // Create a new Recurring Modify Transaction.      
            RecurringTransaction Trans = new RecurringTransaction("M", RecurInfo, User, Inv, PayflowUtility.RequestId);

            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if ((TrxnResponse != null) && (RecurResponse != null))
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        this.ValidatedResult = "APPROVED";
                        ProfileId = RecurResponse.ProfileId;
                        this.TransactionId = RecurResponse.TrxPNRef;
                        this.LogTransaction();
                        return true;
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                        return false;
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                        return false;
                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED:";
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public bool AddUpdateProfileWithTransactionPCI(string AuthCode, Decimal Amount, DateTime StartDate)
        {


            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            //UserInfo User = new UserInfo("OnCourseCE", "OnCourseCE", "Verisign", "okm1qaz");
            //UserInfo User = new UserInfo(this.MerchantId, this.MerchantId, this.SignupPartner, this.MerchantPassword);

            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;

            Connection = new PayflowConnectionData(ConfigurationManager.AppSettings["PAYFLOW_HOST"].ToString());


            // Create a new Invoice data object with the Amount, Billing Address etc. details.
            Invoice Inv = new Invoice();

            // Set Amount.
            Currency Amt = new Currency((decimal)Amount);
            Inv.Amt = Amt;
            Inv.PoNum = "";
            Inv.InvNum = "";
            Inv.Comment1 = this.Comment;
            Inv.Comment2 = "UserName: " + this.OrderId;

            if (this.TaxAmount > 0.00M)
            {
                Currency TaxAmount = new Currency(this.TaxAmount);
                Inv.TaxAmt = TaxAmount;
            }

            // Set the Billing Address details.
            BillTo Bill = new BillTo();
            Bill.FirstName = this.Firstname;
            Bill.LastName = this.Lastname;
            Bill.Street = this.Address;
            Bill.City = this.City;
            Bill.State = this.State;
            Bill.Zip = this.Zip;
            Bill.Email = this.Email;


            Inv.BillTo = Bill;

            RecurringInfo RecurInfo = new RecurringInfo();

            // The date that the first payment will be processed.
            // Note - this value must be greater
            // than the current date! We will charge the customer
            // for the first month in a sep. Transaction
            // Format must be mmddyyyyDateTime StartBilling = DateTime.Now.AddMonths(1);

            //RecurInfo.Start = System.DateTime.Today.AddYears(1).ToString("MMddyyyy");
            //Fix the Next Payment Date
            RecurInfo.Start = StartDate.ToString("MMddyyyy");
            RecurInfo.ProfileName = "UCErenewal" + UniqueId;
            // Specifies how often the payment occurs. All PAYPERIOD values must use
            // capital letters and can be any of WEEK / BIWK / SMMO / FRWK / MONT /
            // QTER / SMYR / YEAR
            RecurInfo.PayPeriod = "YEAR";
            RecurInfo.Term = 0; // Number of payments 0 = indef.
            RecurInfo.RetryNumDays = 2;
            RecurInfo.MaxFailPayments = 1;
            // Perform an Optional Transaction. Verify that we need to do this!!
            //RecurInfo.OptionalTrx = "S"; // S = Sale, A = Authorization
            RecurInfo.OrigProfileId = AuthCode;
            List<ExtendData> data = new List<ExtendData>();
            data.Add(new ExtendData("ORIGID", AuthCode));
            data.Add(new ExtendData("TENDER", "C"));


            RecurringTransaction Trans = new RecurringTransaction("Add", RecurInfo, User, Connection, Inv, PayflowUtility.RequestId);
            foreach (var item in data)
            {
                Trans.AddToExtendData(item);
            }


            // Create a new Recurring Add Transaction.

            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if ((TrxnResponse != null) && (RecurResponse != null))
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        this.ValidatedResult = "APPROVED";
                        ProfileId = RecurResponse.ProfileId;
                        this.LogTransaction();
                        return true;
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                        return false;
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                        return false;
                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED: Credit card authorization was declined.";
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public bool AddUpdateProfileWithTransactionPCI(string AuthCode, Decimal Amount, DateTime StartDate, string Period)
        {


            // Create the Data Objects.
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            //UserInfo User = new UserInfo("OnCourseCE", "OnCourseCE", "Verisign", "okm1qaz");
            //UserInfo User = new UserInfo(this.MerchantId, this.MerchantId, this.SignupPartner, this.MerchantPassword);

            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;

            Connection = new PayflowConnectionData(ConfigurationManager.AppSettings["PAYFLOW_HOST"].ToString());


            // Create a new Invoice data object with the Amount, Billing Address etc. details.
            Invoice Inv = new Invoice();

            // Set Amount.
            Currency Amt = new Currency((decimal)Amount);
            Inv.Amt = Amt;
            Inv.PoNum = "";
            Inv.InvNum = "";
            Inv.Comment1 = this.Comment;
            Inv.Comment2 = "UserName: " + this.OrderId;

            if (this.TaxAmount > 0.00M)
            {
                Currency TaxAmount = new Currency(this.TaxAmount);
                Inv.TaxAmt = TaxAmount;
            }

            // Set the Billing Address details.
            BillTo Bill = new BillTo();
            Bill.FirstName = this.Firstname;
            Bill.LastName = this.Lastname;
            Bill.Street = this.Address;
            Bill.City = this.City;
            Bill.State = this.State;
            Bill.Zip = this.Zip;
            Bill.Email = this.Email;


            Inv.BillTo = Bill;

            RecurringInfo RecurInfo = new RecurringInfo();

            // The date that the first payment will be processed.
            // Note - this value must be greater
            // than the current date! We will charge the customer
            // for the first month in a sep. Transaction
            // Format must be mmddyyyyDateTime StartBilling = DateTime.Now.AddMonths(1);

            //RecurInfo.Start = System.DateTime.Today.AddYears(1).ToString("MMddyyyy");
            //Fix the Next Payment Date
            RecurInfo.Start = StartDate.ToString("MMddyyyy");
            RecurInfo.ProfileName = "CEDRetail" + UniqueId;
            // Specifies how often the payment occurs. All PAYPERIOD values must use
            // capital letters and can be any of WEEK / BIWK / SMMO / FRWK / MONT /
            // QTER / SMYR / YEAR
            RecurInfo.PayPeriod = Period.ToString().Trim().ToUpper();
            RecurInfo.Term = 0; // Number of payments 0 = indef.
            RecurInfo.RetryNumDays = 2;
            RecurInfo.MaxFailPayments = 1;
            // Perform an Optional Transaction. Verify that we need to do this!!
            //RecurInfo.OptionalTrx = "S"; // S = Sale, A = Authorization
            RecurInfo.OrigProfileId = AuthCode;
            List<ExtendData> data = new List<ExtendData>();
            data.Add(new ExtendData("ORIGID", AuthCode));
            data.Add(new ExtendData("TENDER", "C"));


            RecurringTransaction Trans = new RecurringTransaction("Add", RecurInfo, User, Connection, Inv, PayflowUtility.RequestId);
            foreach (var item in data)
            {
                Trans.AddToExtendData(item);
            }


            // Create a new Recurring Add Transaction.

            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if ((TrxnResponse != null) && (RecurResponse != null))
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        this.ValidatedResult = "APPROVED";
                        ProfileId = RecurResponse.ProfileId;
                        this.LogTransaction();
                        return true;
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                        return false;
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                        return false;
                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED: Credit card authorization was declined.";
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public bool SaleTransactionPCI(string AuthCode, Decimal Amount)
        {
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;
            Connection = new PayflowConnectionData(ConfigurationManager.AppSettings["PAYFLOW_HOST"].ToString());
            // Create a new Invoice data object with the Amount, Billing Address etc. details.
            Invoice Inv = new Invoice();

            // Set Amount.
            Currency Amt = new Currency((decimal)Amount);
            Inv.Amt = Amt;
            Inv.PoNum = "";
            Inv.InvNum = "";
            Inv.Comment1 = this.Comment;
            Inv.Comment2 = "UserName: " + this.OrderId;

            if (this.TaxAmount > 0.00M)
            {
                Currency TaxAmount = new Currency(this.TaxAmount);
                Inv.TaxAmt = TaxAmount;
            }

            // Set the Billing Address details.
            BillTo Bill = new BillTo();
            Bill.FirstName = this.Firstname;
            Bill.LastName = this.Lastname;
            Bill.Street = this.Address;
            Bill.City = this.City;
            Bill.State = this.State;
            Bill.Zip = this.Zip;
            Bill.Email = this.Email;

            Inv.BillTo = Bill;

            List<ExtendData> data = new List<ExtendData>();
            data.Add(new ExtendData("ORIGID", AuthCode));
            data.Add(new ExtendData("TENDER", "C"));


            BaseTransaction Trans = new BaseTransaction("S", User, Connection, Inv, PayflowUtility.RequestId);
            foreach (var item in data)
            {
                Trans.AddToExtendData(item);
            }


            // Create a new Recurring Add Transaction.

            // Submit the Transaction
            Response Resp = Trans.SubmitTransaction();

            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if (TrxnResponse != null)
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        this.ValidatedResult = "APPROVED";
                      //  ProfileId = RecurResponse.ProfileId;
                        this.LogTransaction();
                        return true;
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                        return false;
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                        return false;
                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED: Credit card authorization was declined.";
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        public string GetExpDateFromRecurringProfile(string AuthCode)
        {

            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.
            PayflowConnectionData Connection;
            if (AuthCode.Contains("RT"))
            {
                Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com/transaction/");
            }
            else
            {
                Connection = new PayflowConnectionData("payflowpro.verisign.com/transaction/");
            }
            // Create the User data object with the required user details.
            UserInfo User = new UserInfo("Website", "OnCourseCE", "Verisign", "zcTxLpF7EKx7ZBX");
            //UserInfo User = new UserInfo("OnCourseCE", "OnCourseCE", "Verisign", "okm1qaz");
            // Create the Payflow  Connection data object with the required connection details.
            // The PAYFLOW_HOST property is defined in the App config file.

            RecurringInfo RecurInfo = new RecurringInfo();
            RecurInfo.OrigProfileId = AuthCode;
            PayPal.Payments.Transactions.RecurringInquiryTransaction Transaction = new RecurringInquiryTransaction(User, Connection, RecurInfo, PayflowUtility.RequestId);

            Response Resp = Transaction.SubmitTransaction();
            // Return the transaction response parameters.
            if (Resp != null)
            {
                // Get the Transaction Response parameters.
                TransactionResponse TrxnResponse = Resp.TransactionResponse;

                // Get the Recurring Response parameters.
                RecurringResponse RecurResponse = Resp.RecurringResponse;

                if ((TrxnResponse != null) && (RecurResponse != null))
                {
                    int iResultValue = TrxnResponse.Result;
                    string ResultValue = iResultValue.ToString();

                    // *** 0 means success
                    if (ResultValue == "0")
                    {
                        if (Convert.ToString(RecurResponse.Status).ToUpper() == "ACTIVE")
                        {
                            return RecurResponse.ExpDate;
                        }
                        else
                        {
                            return "";
                        }
                    }
                    // *** Empty response means we have an unknown failure
                    else if (ResultValue == "")
                    {
                        this.ValidatedMessage = "ERROR: Unknown problem with Credit card authorization.";
                        this.ValidatedResult = "FAILED";
                        return "";
                    }
                    // *** Negative number means communication failure
                    else if (ResultValue.StartsWith("-"))
                    {
                        this.ValidatedResult = "FAILED";
                        this.ValidatedMessage = "FAILED: Communication problems with Credit card authorization.";

                        return "";
                    }
                    // *** POSITIVE number means we have a DECLINE
                    else
                    {
                        this.ValidatedResult = "DECLINED";
                        this.ValidatedMessage = "DECLINED: Credit card authorization was declined.";
                        return "";
                    }
                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }
        }

    }
}