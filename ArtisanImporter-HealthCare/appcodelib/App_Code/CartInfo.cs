﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace PearlsReview { 
/// <summary>
/// Summary description for CartInfo
/// </summary>
/// 
[Serializable]
public class MicroSiteCartInfo  
{
    //
    public int topicid { get; set; }
    public int cartid { get; set; }
    public decimal online_cost { get; set; }
    public string topicfullname { get; set; }
    public string topic_type { get; set; }
    public string course_number { get; set; }
    public string media_type { get; set; }
    public string mediatype { get; set; }
    public string unitprice { get; set; }
    public string quantitydisplay { get; set; }
    public string itemprice { get; set; }
    public string itempricewithoutdiscount { get; set; }
    public bool cartprepaid { get; set; }

    public MicroSiteCartInfo()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
}