﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PearlsReview.BLL;
using System.Data;
using Westwind.WebStore;
using System.Net.Mail;
using System.IO;


/// <summary>
/// Summary description for FollowupSurvey
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class FollowupSurvey : System.Web.Services.WebService {

    MailAddress toAddress, bccAddress;
    string strException = string.Empty;

    public FollowupSurvey () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public void Followup()
    {
        string toAddress = "";
        string userName = "";
        string testId = "";
        string testDate = "";
        string testName = "";
        string testCompletion = "";
        string subject = "";        
        try
        {
            System.Data.DataSet ds = UserAccount1.GetFollowupUserAccount();
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    //Sending a recurring email
                    toAddress= Convert.ToString(ds.Tables[0].Rows[i]["cemail"]);
                    userName = Convert.ToString(ds.Tables[0].Rows[i]["cfirstname"]);
                    testId = Convert.ToString(ds.Tables[0].Rows[i]["testid"]);
                    testDate = Convert.ToString(ds.Tables[0].Rows[i]["lastmod"]);
                    testName = Convert.ToString(ds.Tables[0].Rows[i]["name"]);
                    testCompletion = Convert.ToString(ds.Tables[0].Rows[i]["DateCompletion"]);
                    subject = "CE Evaluation Survey from ContinuingEducation.com";
                    SendEmail(toAddress, userName, testId, testDate, testName, testCompletion, subject);
                }
            }


            System.Data.DataSet ds1 = UserAccount1.GetFollowupReminderUserAccount();
            if (ds1.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= ds1.Tables[0].Rows.Count - 1; i++)
                {
                    //Sending a recurring email
                    toAddress = Convert.ToString(ds1.Tables[0].Rows[i]["cemail"]);
                    userName = Convert.ToString(ds1.Tables[0].Rows[i]["cfirstname"]);
                    testId = Convert.ToString(ds1.Tables[0].Rows[i]["testid"]);
                    testDate = Convert.ToString(ds1.Tables[0].Rows[i]["lastmod"]);
                    testName = Convert.ToString(ds1.Tables[0].Rows[i]["name"]);
                    testCompletion = Convert.ToString(ds1.Tables[0].Rows[i]["DateCompletion"]);
                    subject = "CE Evaluation Survey from ContinuingEducation.com Reminder";
                    SendEmail(toAddress, userName, testId, testDate, testName, testCompletion, subject);                    
                }
            }  
           
        }
        catch (Exception ex)
        {
            strException = strException + " - " + ex.Message;
        }
    }

    public bool SendEmail(string toAddress, string userName, string testId, string testDate, string testName,string testCompletion, string subject)
    {
        string GetLmsHostName = Convert.ToString(HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString());
        MailMessage objMM = new MailMessage();
        string cServerLocation = System.Configuration.ConfigurationManager.AppSettings["serverLocation"];
        string cMailServerHost = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServer"];
        string cMailServerUserName = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServerUserName"];
        string cMailServerPassword = System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateMailServerPassword"];
        //MailAddress fromAddress = new MailAddress(System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCertificateFromAddress"]);
        MailAddress fromAddress = new MailAddress(System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailFollowupSurveyFromAddress"]);        
        bccAddress = new MailAddress(System.Configuration.ConfigurationManager.AppSettings[cServerLocation + "MailCEBrokerCCAddress"]);
      
        objMM.From = fromAddress;       
        objMM.IsBodyHtml = true;
        objMM.Priority = MailPriority.Normal;
        objMM.To.Add(toAddress);
        //objMM.Bcc.Add("bnachanally@gannetthg.com");

        if (subject == "CE Evaluation Survey from ContinuingEducation.com")
        {
            //lms.nurse.com
            //cestage.nurse.com:8088           

            objMM.Body = "<html> <p> Dear " + userName + ", </p>" +
                       " <p> On " + testDate + ", you completed \"" + testName + ",\" an online continuing education course provided by ContinuingEducation.com. To determine the effect of our courses on your practice, please complete this " +
                        " <a href=" + "http://" + GetLmsHostName + "/Aspx/FollowUpSurvey.aspx?testid=" + testId + ">three-month follow-up evaluation</a> by " + testCompletion + ".</p> " +
                        " <p> We are spearheading a new trend to evaluate outcomes and effectiveness in CE that will become common in the near future and is already required by some CE accreditation organizations. Completion of the survey does not affect the status of your contact hours, which you have already been awarded.</p>" +
                        " <p> Please feel free to contact ContinuingEducation.com with any questions at ce@gannetthg.com or 800-866-0919. Thank you for your interest in ContinuingEducation.com’s programs. </p> </html> ";
        }
        else
        {            
            objMM.Body = "<html> <p> Dear " + userName + ", </p>" +
                            " <p><B><u>Reminder</u></B></p>" +
                            " <p> On " + testDate + ", you completed \"" + testName + ",\" an online continuing education course provided by ContinuingEducation.com. To determine the effect of our courses on your practice, please complete this " +
                            " <a href=" + "http://" + GetLmsHostName + "/Aspx/FollowUpSurvey.aspx?testid=" + testId + ">three-month follow-up evaluation</a> by " + testCompletion + ".</p> " +
                            " <p> We are spearheading a new trend to evaluate outcomes and effectiveness in CE that will become common in the near future and is already required by some CE accreditation organizations. Completion of the survey does not affect the status of your contact hours, which you have already been awarded.</p>" +
                            " <p> Please feel free to contact ContinuingEducation.com with any questions at ce@gannetthg.com or 800-866-0919. Thank you for your interest in ContinuingEducation.com’s programs. </p> </html> ";
        }
        objMM.Subject = subject;
        //objMM.Subject = "3-Month Follow-up Survey";

        if (cMailServerHost == null)
        {
            cMailServerHost = "smtp";
        }
        if (cMailServerUserName == null)
        {
            cMailServerUserName = "";
        }
        if (cMailServerPassword == null)
        {
            cMailServerPassword = "";
        }
        SmtpClient objSMTP = new SmtpClient();
        objSMTP.Host = cMailServerHost;
        if (!string.IsNullOrEmpty(cMailServerUserName) & !string.IsNullOrEmpty(cMailServerPassword))
        {
            System.Net.NetworkCredential objSMTPCredentials = new System.Net.NetworkCredential(cMailServerUserName, cMailServerPassword);
            objSMTP.UseDefaultCredentials = false;
            objSMTP.Credentials = objSMTPCredentials;
        }
        else
        {
            objSMTP.UseDefaultCredentials = true;
        }
        try
        {
            objSMTP.Send(objMM);
            return true;
        }
        catch (Exception ex)
        {
           return false;
        }
    }    
}

