﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using PearlsReview;
using PearlsReview.DAL;
using System.Data.SqlClient;

/// <summary>
/// Summary description for PRSubscription
/// </summary>
public class PRSubscription
{
	public PRSubscription()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static decimal SubValue
    {
        get
        {
            return (decimal)69.00;
        }
    }
    public static int PRUserPaymentMade(int userid)
    {
        int result;
        using (SqlConnection conn = new SqlConnection(Globals.Settings.PR.ConnectionString))
        {
            SqlCommand command = new SqlCommand("SELECT PaymentId FROM [dbo].[Payment] WHERE [paystatus]='S' "
            +"AND [Active]='1' AND [facilityid]=3 AND [paydate]>DATEADD(mm, -1,GETDATE()) AND "
            +"userid = @userid",conn);
            command.Parameters.Add("@userid", SqlDbType.Int).Value = userid;
            conn.Open();
            var res = command.ExecuteScalar();
            result = res == null?0:(int)res;
        }
        return result;
    }
    public static bool PRUserPaymentMade(string username)
    {
        int result;
        using (SqlConnection conn = new SqlConnection(Globals.Settings.PR.ConnectionString))
        {
            SqlCommand command = new SqlCommand("SELECT Count(*) FROM [dbo].[Payment] p " +
                "JOIN [dbo].[UserAccount] u ON u.[iid] = [p].[userid]" +
                "WHERE [paystatus]='S' AND [Active]='1' AND [p].[facilityid]=3"
            + " AND [paydate]<DATEADD(mm, -1,GETDATE()) AND "
            + "cusername = @ShortUserName", conn);
            command.Parameters.Add("@ShortUserName", SqlDbType.VarChar).Value = username.ToLower();
            conn.Open();
            result = (int)command.ExecuteScalar();
        }
        return result > 0 ? true : false;
    }
}
