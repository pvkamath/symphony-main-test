﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Common
/// </summary>
public class Common : System.Web.UI.Page
{
	public Common()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static int RetailId
    {
        get
        {
            return 2;
        }
    }

    public static string Formaturl(string url)
    {
        return Convert.ToString(url).ToLower();
    }

    public static int GetRetailId()
    {
        return 2;
    }

    protected void GET404Error()
    {
        
        Response.Redirect("~/404.aspx");
       
    }

}
