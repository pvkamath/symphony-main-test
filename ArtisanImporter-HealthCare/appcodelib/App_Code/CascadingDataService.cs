﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;

/// <summary>
/// Summary description for CascadingDataService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService()]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class CascadingDataService : System.Web.Services.WebService {
    string conString = System.Configuration.ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
    public CascadingDataService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetDropdownOrgs(string knownCategoryValues, string category)
    {
        SqlConnection sqlConn = new SqlConnection(conString);
        sqlConn.Open();
        SqlCommand sqlSelect = new SqlCommand("SELECT parent_id,parent_name FROM ParentOrganization", sqlConn);
        sqlSelect.CommandType = System.Data.CommandType.Text;
        SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlSelect);
        DataSet myDataset = new DataSet();
        sqlAdapter.Fill(myDataset);
        sqlConn.Close();

        List<AjaxControlToolkit.CascadingDropDownNameValue> cascadingValues = new List<AjaxControlToolkit.CascadingDropDownNameValue>();

        foreach (DataRow dRow in myDataset.Tables[0].Rows)
        {
            string ParentID = dRow["parent_id"].ToString();
            string ParentName = dRow["parent_name"].ToString();
            cascadingValues.Add(new AjaxControlToolkit.CascadingDropDownNameValue(ParentName, ParentID));
        }

        return cascadingValues.ToArray();
    }


    [WebMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] GetDropDownFacs(string knownCategoryValues, string category)
    {
        int parentID;

        StringDictionary categoryValues = AjaxControlToolkit.CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);

        parentID = Convert.ToInt32(categoryValues["category"]);

        SqlConnection sqlConn = new SqlConnection(conString);
        sqlConn.Open();
        SqlCommand sqlSelect = new SqlCommand("select facid,facname from facilitygroup where parent_id=@parentID", sqlConn);
        
        sqlSelect.CommandType = System.Data.CommandType.Text;
        sqlSelect.Parameters.Add("@parentID", SqlDbType.Int).Value = parentID;
        SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlSelect);
        DataSet myDataset = new DataSet();
        sqlAdapter.Fill(myDataset);
        sqlConn.Close();

        List<AjaxControlToolkit.CascadingDropDownNameValue> cascadingValues = new List<AjaxControlToolkit.CascadingDropDownNameValue>();

        foreach (DataRow dRow in myDataset.Tables[0].Rows)
        {
            string FacID = dRow["facid"].ToString();
            string FacName = dRow["facname"].ToString();
            cascadingValues.Add(new AjaxControlToolkit.CascadingDropDownNameValue(FacName, FacID));
        }

        return cascadingValues.ToArray();
    }
    
}

