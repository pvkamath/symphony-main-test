﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using PearlsReview.BLL;
using PearlsReview.Security;
using System.Net;

/// <summary>
/// Summary description for CEWebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class CEWebService : System.Web.Services.WebService {

    public CEWebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string GetTokenWithIP()
    {
        string token = "";
        //string ip = HttpContext.Current.Request.UserHostAddress;
        string ip = "";

        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();

        FacilityGroup facility = null;
        if (!String.IsNullOrEmpty(ip))
            facility = FacilityGroup.GetFacilityGroupByIP(ip);

        if (facility != null)
        {
            token = Encryption.Encrypt(facility.FacID.ToString() + "::" + DateTime.Now.AddMinutes(10).ToString());
        }
        else
            token = "Unauthorized Access";

        return token;
    }

    [WebMethod]
    public string GetTokenWithDomain()
    {
        string token = "";
        //string domain = HttpContext.Current.Request.UserHostName;
        string domain = "";
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            domain = Dns.GetHostByAddress(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString()).HostName;

        FacilityGroup facility = null;
        if (!String.IsNullOrEmpty(domain))
            facility = FacilityGroup.GetFacilityGroupByDomain(domain);

        if (facility != null)
        {
            token = Encryption.Encrypt(facility.FacID.ToString() + "::" + DateTime.Now.AddMinutes(10).ToString());
        }
        else
            token = "Unauthorized Access";

        return token;
    }

    //Authorize by IP
    [WebMethod]
    public string InsertUserWithIP(string strFirstName, string strLastName, string strUserName, string strEmail)
    {
        //string ip = HttpContext.Current.Request.UserHostAddress;
        string ip = "";
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();

        FacilityGroup facility = null;
        if (!String.IsNullOrEmpty(ip))
            facility = FacilityGroup.GetFacilityGroupByIP(ip);

        if (facility != null)
            return InsertUser(strFirstName, strLastName, strUserName, strEmail, facility.FacID);
        else
            return "Unauthorized Access";  //unauthorized IP address
    }

    //Authorize by Domain
    [WebMethod]
    public string InsertUserWithDomain(string strFirstName, string strLastName, string strUserName, string strEmail)
    {
        //string domain = HttpContext.Current.Request.UserHostName;
        string domain = "";
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            domain = Dns.GetHostByAddress(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString()).HostName;

        FacilityGroup facility = null;
        if (!String.IsNullOrEmpty(domain))
            facility = FacilityGroup.GetFacilityGroupByDomain(domain);

        if (facility != null)
            return InsertUser(strFirstName, strLastName, strUserName, strEmail, facility.FacID);
        else
            return "Unauthorized Access";  //unauthorized Domain
    }

    //Authorize by Token
    [WebMethod]
    public string InsertUserWithToken(string strFirstName, string strLastName, string strUserName, string strEmail, string strToken)
    {
        int facilityID = 0;
        DateTime time = DateTime.Now;

        try
        {
            strToken = Encryption.Decrypt(strToken);
            facilityID = Convert.ToInt32(strToken.Substring(0, strToken.IndexOf("::")));
            time = Convert.ToDateTime(strToken.Substring(strToken.IndexOf("::") + 2));
        }
        catch (Exception ex)
        {
            return "Invalid Token";  //Invalid token
        }

        if (time > DateTime.Now)
            return InsertUser(strFirstName, strLastName, strUserName, strEmail, facilityID);
        else
            return "Token Expired";   //Token expired
    }

    private string InsertUser(string strFirstName, string strLastName, string strUserName, string strEmail, int facilityID)
    {
        UserAccount account = UserAccount.GetUserAccountByShortUserNameAndFacilityID(strUserName, facilityID);

        int iUserId;

        if (account != null)
            return "User Already Exist";   //User already exist
        else
        {
            iUserId=UserAccount.InsertUserAccount(string.Empty, string.Empty, false, false, string.Empty, string.Empty,
                        strEmail, System.DateTime.MinValue.ToShortDateString(), strFirstName, string.Empty,
                        string.Empty, strLastName, string.Empty, string.Empty, string.Empty, string.Empty, 0, facilityID,
                        string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, strUserName,
                        string.Empty, 0, (100000 + facilityID).ToString().Substring(1, 5) + strUserName,
                        System.DateTime.MinValue, string.Empty, string.Empty,
                        string.Empty, string.Empty, string.Empty, string.Empty, true, true,
                        false, System.DateTime.MinValue, System.DateTime.MinValue, System.DateTime.MinValue, 0,
                        System.DateTime.MinValue, 0, System.DateTime.MinValue, string.Empty, System.DateTime.Now.Date,
                        0, 0, 0, string.Empty, string.Empty, System.DateTime.MinValue,
                        System.DateTime.MinValue, 0, 0, 0, string.Empty, false,
                        0, 0, UserAccount.GetUserAccountCount() + 1, false);
            if (iUserId > 0)
            {
                UserAccount Userinfo = UserAccount.GetUserAccountByID(iUserId);
                UsersInRoles.InsertUsersInRoles("ActiveFacilityMember", (100000 + facilityID).ToString().Substring(1, 5) + strUserName, facilityID);
            }

            return(iUserId> 0 ? "Success" : "General Error");
        }
    }

    //Authorize by IP
    [WebMethod]
    public string UpdateUserWithIP(string strFirstName, string strLastName, string strUserName, string strEmail)
    {
        //string ip = HttpContext.Current.Request.UserHostAddress;
        string ip = "";
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();

        FacilityGroup facility = null;
        if (!String.IsNullOrEmpty(ip))
            facility = FacilityGroup.GetFacilityGroupByIP(ip);

        if (facility != null)
            return UpdateUser(strFirstName, strLastName, strUserName, strEmail, facility.FacID);
        else
            return "Unauthorized Access";  //unauthorized IP address
    }

    //Authorize by Domain
    [WebMethod]
    public string UpdateUserWithDomain(string strFirstName, string strLastName, string strUserName, string strEmail)
    {
        //string domain = HttpContext.Current.Request.UserHostName;
        string domain = "";
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            domain = Dns.GetHostByAddress(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString()).HostName;

        FacilityGroup facility = null;
        if (!String.IsNullOrEmpty(domain))
            facility = FacilityGroup.GetFacilityGroupByDomain(domain);

        if (facility != null)
            return UpdateUser(strFirstName, strLastName, strUserName, strEmail, facility.FacID);
        else
            return "Unauthorized Access";  //unauthorized Domain
    }

    //Authorize by Token
    [WebMethod]
    public string UpdateUserWithToken(string strFirstName, string strLastName, string strUserName, string strEmail, string strToken)
    {
        int facilityID = 0;
        DateTime time = DateTime.Now;

        try
        {
            strToken = Encryption.Decrypt(strToken);
            facilityID = Convert.ToInt32(strToken.Substring(0, strToken.IndexOf("::")));
            time = Convert.ToDateTime(strToken.Substring(strToken.IndexOf("::") + 2));
        }
        catch (Exception ex)
        {
            return "Invalid Token";  //Invalid token
        }

        if (time > DateTime.Now)
            return UpdateUser(strFirstName, strLastName, strUserName, strEmail, facilityID);
        else
            return "Token Expired";   //Token expired
    }


    private string UpdateUser(string strFirstName, string strLastName, string strUserName, string strEmail, int facilityID)
    {
        UserAccount account = UserAccount.GetUserAccountByShortUserNameAndFacilityID(strUserName, facilityID);
        if (account != null)
        {
            account.cFirstName = strFirstName;
            account.cLastName = strLastName;
            account.cEmail = strEmail;
            return (account.Update() ? "Success" : "General Error");
        }
        else
            return "User Dose Not Exist";   //User does not exist
    }

    //Authorize with IP
    [WebMethod]
    public string DeleteUserWithIP(string strUserName)
    {
        //string ip = HttpContext.Current.Request.UserHostAddress;
        string ip = "";

        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();

        FacilityGroup facility = null;
        if (!String.IsNullOrEmpty(ip))
            facility = FacilityGroup.GetFacilityGroupByIP(ip);

        if (facility != null)
            return DeleteUser(strUserName, facility.FacID);
        else
            return "Unauthorized Access";  //unauthorized IP address
    }

    //Authorize with Domain
    [WebMethod]
    public string DeleteUserWithDomain(string strUserName)
    {
        //string domain = HttpContext.Current.Request.UserHostName;
        string domain = "";
        if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            domain = Dns.GetHostByAddress(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString()).HostName;

        FacilityGroup facility = null;
        if (!String.IsNullOrEmpty(domain))
            facility = FacilityGroup.GetFacilityGroupByDomain(domain);

        if (facility != null)
            return DeleteUser(strUserName, facility.FacID);
        else
            return "Unauthorized Access";  //unauthorized Domain
    }

    //Authorize with Token
    [WebMethod]
    public string DeleteUserWithToken(string strUserName, string strToken)
    {
        int facilityID = 0;
        DateTime time = DateTime.Now;

        try
        {
            strToken = Encryption.Decrypt(strToken);
            facilityID = Convert.ToInt32(strToken.Substring(0, strToken.IndexOf("::")));
            time = Convert.ToDateTime(strToken.Substring(strToken.IndexOf("::") + 2));
        }
        catch (Exception ex)
        {
            return "Invalid Token";  //Invalid token
        }

        if (time > DateTime.Now)
            return DeleteUser(strUserName, facilityID);
        else
            return "Token Expired";   //Token expired
    }

    private string DeleteUser(string strUserName, int facilityID)
    {
        UserAccount account = UserAccount.GetUserAccountByShortUserNameAndFacilityID(strUserName, facilityID);
        if (account != null)
        {
            return (account.Delete() ? "Success" : "General Error");
        }
        else
            return "User Dose Not Exist";   //User does not exist
    }
}

