﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FredCK.FCKeditorV2;

/// <summary>
/// Summary description for MicrositeRootPage
/// </summary>
public class MicrositeRootPage : System.Web.UI.Page
{
	public MicrositeRootPage()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public void ResetControls(Control cl)
    {
        foreach (Control c in cl.Controls)
        {
            if (c is TextBox)
            {
                ((TextBox)c).Text = string.Empty;
            }
            if (c is FCKeditor)
            {
                ((FCKeditor)c).Value = string.Empty;
            }
            if (c is DropDownList)
            {
                ((DropDownList)c).SelectedValue = "0";
            }
        }
    }
}
