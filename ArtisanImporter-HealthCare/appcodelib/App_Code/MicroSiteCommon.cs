﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net.Mail;
using System.Configuration;
using System.Globalization;
using System.Net;
using PearlsReview.BLL;
using System.Text.RegularExpressions;
using System.Text;
using PearlsReview;
/// <summary>
/// Summary description for Common
/// </summary>
public class MicroSiteCommon : System.Web.UI.Page
{

    private static readonly Dictionary<String, int> AhDomainIds;
    private static readonly Dictionary<String, String> AhDomainNames;
    private static string AhDomainFullName = string.Empty;
    private static string _fromEmailAddress = string.Empty;
    private static string _mailOrderFromAddress = string.Empty;
    static MicroSiteCommon()
    {
        //if (ConfigurationManager.AppSettings["serverLocation"] == "test")
        //{
        //    AhDomainIds = new Dictionary<string, int>()
        //    {
        //         {"ptstage",6},
        //        {"cestage",5},
        //        {"otstage",8},
        //         {"localhost",5}
        //    };
        //    AhDomainNames = new Dictionary<string, string>()
        //    {
        //        {"ptstage","TodayinPT.com"},
        //        {"cestage","nurse.com"},
        //         {"otstage","gannetthg.com"}
        //    };
        //}
        //else
        //{
            AhDomainIds = new Dictionary<string, int>()
             {
                {"todayinpt",6},
                {"continuingeducation",5},
                {"todayinot",7},
                {"nutritiondimension",8},
                {"localhost",5}
            };
            AhDomainNames = new Dictionary<string, string>()
            {
                {"todayinpt","TodayinPT.com"},
                {"continuingeducation","ContinuingEducation.com"},
                 {"todayinot","TodayinOT.com"},
                {"nutritiondimension","NutritionDimension.com"},
            };
        //}
        SetFromEmailAddress();
    }

    public MicroSiteCommon()
    {

        //
        // TODO: Add constructor logic here
        //
    }


    public static void SetFromEmailAddress()
    {
        _fromEmailAddress = System.Configuration.ConfigurationManager.AppSettings["liveMailFromAddress"];
        _mailOrderFromAddress = _fromEmailAddress;
        AhDomain _aHDomain = AhDomain.GetAhDomainByAdID(GetAhDomainId().ToString());
        if (_aHDomain != null)
            if (_aHDomain.Single_prof_ind)
            {
                _fromEmailAddress = "no-reply@" + _aHDomain.Ad_domainname;
                _mailOrderFromAddress = _fromEmailAddress;
            }
    }


    public static string FromEmailAddress
    {
        get
        {
            _fromEmailAddress = "no-reply@" + GetAhDomainName().ToLower();
            return _fromEmailAddress;
        }
    }

    public static string MailOrderFromAddress
    {
        get
        {
            _mailOrderFromAddress = "no-reply@" + GetAhDomainName().ToLower();
            return _mailOrderFromAddress;
        }
    }

    public static int GetDomainID
    {
        get
        {
            return GetStaticMicroSiteDomainID();
        }
    }

    public static string GetMicroHostName
    {
        get
        {
            return GetStaticHostName();
        }
    }

    public static int LoginId
    {
        get
        {
            return GetLoginId();
        }
    }

    public static bool LoginStatus
    {
        get
        {
            return GetLoginStatus();
        }
    }

    // Whether the system has a free course for first users or not; if we don't have a promotion, set it to false. if we do have a promotion, set it to true.
    public const bool FirstCoursePromotion = true;

    #region CEPRO Indicators
    public static bool IsCEPROInCart(List<Cart> cart)
    {
        foreach (Cart oCart in cart)
        {
            if (isCartCEPRO(oCart))
            {
                return true;
            }
        }
        return false;
    }
    //Check whether this item is CEPRO
    public static bool isCartCEPRO(Cart c)
    {
        return c.MediaType.Equals("C");
    }

    //Remove CEPRO in cart if exist
    public static void removeCEPROFromCart(List<Cart> carts)
    {
        foreach (Cart c in carts)
        {
            if (isCartCEPRO(c))
            {
                Cart.DeleteCart(c.CartID);
                break;
            }
        }
    }

    public static decimal CEPROPrice
    {
        get
        {
            return GetCEPROPrice();
        }
    }

    public static decimal GetCEPROPrice()
    {
        Topic ObjTopic = Topic.GetTopicByCourseNumberAndDomainId("CEPRO", 5);
        if (ObjTopic != null)
        {
            return ObjTopic.offline_Cost;
        }
        else
        {
            return (decimal)0.0;

        }
    }

    public static void CheckCEPRO(int UserID)
    {
        ProMembership proMembership = ProMembership.GetActiveProMemeberShipByUserID(UserID);
        if (proMembership != null)
        {
            HttpContext.Current.Session["CEPRO_MemberID"] = proMembership.Memberid;
        }
    }
    public static int GetCEPROMemberID()
    {
        if (HttpContext.Current.Session["CEPRO_MemberID"] != null)
        {
            return int.Parse(HttpContext.Current.Session["CEPRO_MemberID"].ToString());
        }
        return 0;
    }
    #endregion

    public static string GetAhStaticMicrositeDomain()
    {

        AhDomain _ahDomain = AhDomain.GetAhDomainByAdID(GetAhDomainId().ToString());
        if (_ahDomain.Single_prof_ind)
        {

            return "";
        }


        else
            return "/" + GetStaticMicroSiteDomain();

    }

    public static int GetAhDomainId()
    {
        foreach (var item in AhDomainIds)
        {
            if (HttpContext.Current.Request.Url.Host.Contains(item.Key))
                return item.Value;
            else
                continue;
        }
        return 5;
    }
    public static string GetAhDomainName()
    {
        foreach (var item in AhDomainNames)
        {
            if (HttpContext.Current.Request.Url.Host.Contains(item.Key))
                return item.Value;
            else
                continue;
        }
        return "localhost";
    }
    public static string GetAhFullDomainName()
    {
        if (AhDomainFullName == string.Empty)
        {
            if (AhDomain.GetAhDomainByAdID(GetAhDomainId().ToString()) != null)
                AhDomainFullName = AhDomain.GetAhDomainByAdID(GetAhDomainId().ToString()).Ad_fullurl;
        }
        return AhDomainFullName;
    }
    public string GetMicroSiteDomain()
    {
        if (HttpContext.Current.Items["profession"] != null && !string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Items["profession"])))
        {
            return Convert.ToString(HttpContext.Current.Items["profession"]).ToLower();
        }
        else
        {
            //return "ContinuingEducation".ToLower();
            return GetAhDomainName().Replace(".com", "").ToLower();
        }
    }
    //changing GetMicroSiteDomain() to GetMicrositeProfession
    public string GetMicrositeProfession()
    {
        if (HttpContext.Current.Items["profession"] != null && !string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Items["profession"])))
        {
            return Convert.ToString(HttpContext.Current.Items["profession"]).ToLower();
        }
        else return "";
    }
    public string GetMicroSiteDomainName()
    {
        MicrositeDomain Domain = MicrositeDomain.GetMicrositeDomainIDByName(GetMicroSiteDomain());

        if (Domain != null)
        {
            return Domain.Index_title;
        }
        return "";

    }
    public int GetMicroSiteDomainID()
    {
        //Get Ah Domain ID
        AhDomain _aHDomain = AhDomain.GetAhDomainByAdID(GetAhDomainId().ToString());

        if (_aHDomain != null)
        {
            List<AlliedProfLink> APList = AlliedProfLink.GetAlliedProfLinksByAdId(_aHDomain.Ad_id);

            if (APList != null)
            {
                //If it's a single profession, return MSID from alliedprolink
                if (_aHDomain.Single_prof_ind)
                    return APList[0].Msid;
                else
                {
                    //If it's mutiple professions, look into the profession parameter to get the MSID
                    MicrositeDomain Domain = MicrositeDomain.GetMicrositeDomainIDByName(GetMicroSiteDomain());
                    if (Domain != null)
                        return Domain.Msid;
                }
            }
        }
        //If nothing found, redirect to 404 page.
        //HttpContext.Current.Response.Redirect("~/Aspx/404.aspx");
        return GetAhDomainId();
        //foreach (MicrositeValues.DomainNames value in Enum.GetValues(typeof(MicrositeValues.DomainNames)))
        //{
        //    if (GetMicroSiteDomain().Equals(value.ToString().ToLower()))
        //        return (int)value;
        //}

        //return Domain.Msid;
    }
    //changing GetMicroSiteDomainID() to GetMicroSiteProfessionID
    public int GetMicroSiteProfessionID()
    {
        //Get Ah Domain ID
        AhDomain _aHDomain = AhDomain.GetAhDomainByAdID(GetAhDomainId().ToString());

        if (_aHDomain != null)
        {
            List<AlliedProfLink> APList = AlliedProfLink.GetAlliedProfLinksByAdId(_aHDomain.Ad_id);

            if (APList != null)
            {
                //If it's a single profession, return MSID from alliedproflink
                if (_aHDomain.Single_prof_ind)
                    return APList[0].Msid;
                else
                {
                    //If it's mutiple professions, look into the profession parameter to get the MSID
                    MicrositeDomain profession = MicrositeDomain.GetMicrositeDomainIDByName(GetMicrositeProfession());
                    if (profession != null)
                        return profession.Msid;
                }
            }
        }
        return 0;
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    public static string GetStaticMicroSiteDomain()
    {
        MicroSiteCommon objMicroSiteDomain = new MicroSiteCommon();
        return objMicroSiteDomain.GetMicroSiteDomain();
    }

    public static int GetStaticMicroSiteDomainID()
    {
        MicroSiteCommon objMicroSiteDomain = new MicroSiteCommon();
        return objMicroSiteDomain.GetMicroSiteDomainID();
    }
    public static string GetStaticMicroSiteProfession()
    {
        MicroSiteCommon objMicroSiteDomain = new MicroSiteCommon();
        return objMicroSiteDomain.GetMicrositeProfession();
    }
    public static int GetStaticMicroSiteProfessionID()
    {
        MicroSiteCommon objMicroSiteDomain = new MicroSiteCommon();
        return objMicroSiteDomain.GetMicroSiteProfessionID();
    }
    public static string GetStaticHostName()
    {
        MicroSiteCommon objMicrositeDomain = new MicroSiteCommon();
        return objMicrositeDomain.GetHostName();
    }

    public string GetHostName()
    {
        return Convert.ToString(HttpContext.Current.Request.ServerVariables["HTTP_HOST"].ToString());
    }

    #region Session Shopping Cart
    public List<MicroSiteCartInfo> UpdateSessionShoppingCart(int id, int quantity)
    {
        List<MicroSiteCartInfo> cart = GetSessionShoppingCart();

        if (cart.Count > 0)
        {
            MicroSiteCartInfo info = cart.SingleOrDefault(c => c.cartid == id);

            if (info != null)
            {
                info.quantitydisplay = quantity.ToString();
                Decimal value;
                if (decimal.TryParse(info.unitprice, NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out value))
                    info.itemprice = (quantity * value).ToString();
            }
            Session["shoppingcart"] = cart;
        }
        return cart;
    }
    public List<MicroSiteCartInfo> DeleteSessionShoppingCart(int id)
    {
        List<MicroSiteCartInfo> cart = GetSessionShoppingCart();
        if (cart.Count > 0)
        {
            cart.RemoveAll(c => c.cartid == id);
            Session["shoppingcart"] = cart;
        }
        return cart;
    }
    public List<MicroSiteCartInfo> InsertSessionShoppingCart(int TopicID, string MediaType)
    {
        List<MicroSiteCartInfo> cart = GetSessionShoppingCart();
        MicroSiteCartInfo checkCart = cart.Where(r => r.topicid == TopicID && r.mediatype == MediaType).SingleOrDefault();
        if (checkCart == null)
        {
            Topic ObjTopic = Topic.GetTopicByID(TopicID);
            if (ObjTopic != null)
            {
                MicroSiteCartInfo c = new MicroSiteCartInfo();
                c.topicid = TopicID;
                c.course_number = ObjTopic.Course_Number;
                c.online_cost = ObjTopic.online_Cost;
                c.topic_type = ObjTopic.Topic_Type;
                c.topicfullname = ObjTopic.TopicName + " (" + ObjTopic.Course_Number + ") " + (String.IsNullOrEmpty(ObjTopic.Subtitle) ? "<br/>" + ObjTopic.Subtitle : "");
                c.media_type = (ObjTopic.prepaid_ind && MediaType == "O") ? "Prepaid" : ((MediaType == "O") ? "Online" : ((MediaType == "V") ? "Video" : ((MediaType == "B") ? "Book" : "Online")));
                c.cartprepaid = (ObjTopic.prepaid_ind && MediaType == "O") ? true : false;
                c.mediatype = MediaType;
                c.unitprice = ObjTopic.online_Cost.ToString();
                if (Session["CEDSeat"] == null)
                    c.quantitydisplay = (MediaType == "O") ? "Online" : "1";
                else
                {
                    int seat = 1;
                    seat = Convert.ToInt32(Session["CEDSeat"].ToString());
                    c.quantitydisplay = seat.ToString();
                    c.media_type = ObjTopic.MediaType;
                    Session["CEDSeat"] = null;
                }
                c.itemprice = ObjTopic.online_Cost.ToString();
                c.itempricewithoutdiscount = c.itemprice;

                if (cart.Count > 0)
                    c.cartid = cart.OrderByDescending(x => x.cartid).First().cartid + 1;
                else
                    c.cartid = 1;
                cart.Add(c);
                Session["shoppingcart"] = cart;
            }
        }
        else
        {
            if (checkCart.mediatype != "O")
            {
                UpdateSessionShoppingCart(checkCart.cartid, int.Parse(checkCart.quantitydisplay) + 1);
            }
        }
        return cart;
    }
    public List<MicroSiteCartInfo> GetSessionShoppingCart()
    {
        List<MicroSiteCartInfo> cart = new List<MicroSiteCartInfo>();
        if (Session["shoppingcart"] != null)
        {
            try
            {
                cart = (List<MicroSiteCartInfo>)Session["shoppingcart"];
            }
            catch
            {
                return cart;
            }
        }
        return cart;
    }

    public MicroSiteCartInfo GetSessionShoppingCartByTopicIDAndMediaType(int topicID, string mediaType)
    {
        List<MicroSiteCartInfo> cart = GetSessionShoppingCart();
        return cart.Where(r => r.topicid == topicID && r.mediatype == mediaType).SingleOrDefault();
    }

    public void CheckSessionCartAfterLogin(bool CheckCredits, bool CheckFirstCourseFree)
    {
        List<MicroSiteCartInfo> cart = GetSessionShoppingCart();

        if (cart.Count > 0)
        {
            List<Cart> objCarts = Cart.GetInCompleteCartsByUserIdAndDomainId(LoginId, GetAhDomainId());
            if (objCarts != null && objCarts.Count > 0)
            {
                foreach (Cart c in objCarts)
                {
                    for (int i = cart.Count - 1; i >= 0; i--)
                    {
                        MicroSiteCartInfo SessionCart = cart[i];
                        if (c.TopicID == SessionCart.topicid && c.MediaType == SessionCart.mediatype)
                        {
                            if (SessionCart.mediatype != "O")
                            {
                                UpdateSessionShoppingCart(SessionCart.cartid, int.Parse(SessionCart.quantitydisplay) + c.Quantity);
                            }
                            else
                                cart.RemoveAt(i);
                        }
                    }
                }
            }
            if (cart.Count > 0)
            {
                foreach (MicroSiteCartInfo info in cart)
                {
                    if (info.mediatype != "O")
                    {
                        Cart CheckCart = objCarts.Where(r => r.TopicID == info.topicid && r.MediaType == info.mediatype && r.MediaType != "O").SingleOrDefault();
                        if (CheckCart != null)
                        {
                            CheckCart.Quantity = int.Parse(info.quantitydisplay);
                            CheckCart.Update();
                        }
                        else
                        {
                            Cart.InsertCart(0, info.topicid, LoginId, info.mediatype, 0,
                            System.DateTime.Now, string.Empty, string.Empty,
                             (info.quantitydisplay == "Online") ? 1 : int.Parse(info.quantitydisplay),
                            0, false, false, GetAhDomainId(), info.cartprepaid);
                        }
                    }
                    else
                    {
                        if (info.media_type == "C")
                        {
                            if (GetCEPROMemberID() == 0)
                                Cart.InsertCart(0, info.topicid, LoginId, info.mediatype, 0,
                                      System.DateTime.Now, string.Empty, string.Empty,
                                      (info.quantitydisplay == "Online") ? 1 : int.Parse(info.quantitydisplay),
                                       0, false, false, GetAhDomainId(), info.cartprepaid);
                        }
                        else
                            Cart.InsertCart(0, info.topicid, LoginId, info.mediatype, 0,
                                        System.DateTime.Now, string.Empty, string.Empty,
                                       (info.quantitydisplay == "Online") ? 1 : int.Parse(info.quantitydisplay),
                                       0, false, false, GetAhDomainId(), info.cartprepaid);
                    }
                }
            }
        }
        ClearSessionShoppingCart();

        if (CheckFirstCourseFree)
            RedirectFirstTimeUserDiscount();
        //For discount membership, do not redirect to credit page
        if (HttpContext.Current.Request["redirectdiscount"] != null)
        {
            return;
        }
        if (CheckCredits)
            RedirectCredits();
    }

    public bool IsCartStillInSession()
    {
        List<MicroSiteCartInfo> cart = GetSessionShoppingCart();
        if (cart.Count > 0 && LoginStatus)
        {
            return true;
        }
        return false;
    }
    public void ClearSessionShoppingCart()
    {
        Session["shoppingcart"] = null;
    }
    public int GetShoppingCartCount()
    {
        if (!LoginStatus)
        {
            return GetSessionShoppingCart().Count;
        }
        else
        {
            return Cart.GetInCompleteCartsByUserIdAndDomainId(MicroSiteCommon.LoginId, GetAhDomainId()).Count;
        }
    }
    public bool InsertCEPROToCart()
    {
        if (LoginStatus)
        {
            if (GetCEPROMemberID() > 0)
                return false;
            List<Cart> objCarts = Cart.GetCartsByUserIDAndDomainId(LoginId, GetAhDomainId(), "cartid");
            if (IsCEPROInCart(objCarts))
                return true;
            Topic ObjTopic = Topic.GetTopicByCourseNumberAndDomainId("CEPRO", 5);
            if (ObjTopic != null)
            {
                Cart.InsertCart(0, ObjTopic.TopicID, LoginId, "C", 0, System.DateTime.Now, string.Empty, string.Empty, 1, 0, false, false, GetAhDomainId(), false);
                return true;
            }
        }
        return false;
    }
    #endregion

    //For multiple to addresses with cc and bcc
    public static bool sendEmail(String from, List<String> to, List<String> cc, List<String> bcc, String subject, String body,
         List<Attachment> attachmentList)
    {
        MailMessage mail = new MailMessage();
        mail.From = new MailAddress(from);

        if (to != null)
        {
            foreach (String address in to)
            {
                if (!String.IsNullOrEmpty(address))
                    mail.To.Add(new MailAddress(address));
            }
        }

        if (cc != null)
        {
            foreach (String address in cc)
            {
                if (!String.IsNullOrEmpty(address))
                    mail.CC.Add(new MailAddress(address));
            }
        }

        if (bcc != null)
        {
            foreach (String address in bcc)
            {
                if (!String.IsNullOrEmpty(address))
                    mail.Bcc.Add(new MailAddress(address));
            }
        }

        if (attachmentList != null)
        {
            foreach (Attachment attachment in attachmentList)
            {
                if (attachment != null)
                    mail.Attachments.Add(attachment);
            }
        }

        mail.Subject = subject;
        mail.Body = body;
        mail.IsBodyHtml = true;

        string strServerLocation = ConfigurationManager.AppSettings["serverLocation"];
        if (strServerLocation == null)
            strServerLocation = "";

        string strHost = ConfigurationManager.AppSettings[strServerLocation + "MailCertificateMailServer"];
        string strUserName = ConfigurationManager.AppSettings[strServerLocation + "MailCertificateMailServerUserName"];
        string strPassword = ConfigurationManager.AppSettings[strServerLocation + "MailCertificateMailServerPassword"];

        if (String.IsNullOrEmpty(strHost))
            strHost = "smtp";

        SmtpClient client = new SmtpClient(strHost);
        if (String.IsNullOrEmpty(strUserName) || String.IsNullOrEmpty(strPassword))
            client.UseDefaultCredentials = true;
        else
        {
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(strUserName, strPassword);
        }

        try
        {
            client.Send(mail);
        }
        catch (Exception ex)
        {
            //   throw ex;
        }

        return true;
    }

    public static bool sendEmail(String from, List<String> to, List<String> cc, List<String> bcc, String subject, String body)
    {
        return sendEmail(from, to, cc, bcc, subject, body, null);
    }

    //For multiple to address with cc and bcc, split address with ,
    public static bool sendEmail(String from, String to, String cc, String bcc, String subject, String body, Attachment attachment)
    {
        List<String> lstTo = null;
        List<String> lstCC = null;
        List<String> lstbcc = null;
        List<Attachment> lstAttach = null;

        if (!String.IsNullOrEmpty(to))
            lstTo = new List<string>(to.Trim().Split(','));

        if (!String.IsNullOrEmpty(cc))
            lstCC = new List<string>(cc.Trim().Split(','));

        if (!String.IsNullOrEmpty(bcc))
            lstbcc = new List<string>(bcc.Trim().Split(','));

        if (attachment != null)
        {
            lstAttach = new List<Attachment>();
            lstAttach.Add(attachment);
        }

        return sendEmail(from, lstTo, lstCC, lstbcc, subject, body, lstAttach);
    }

    public static bool sendEmail(String from, String to, String cc, String bcc, String subject, String body)
    {
        return sendEmail(from, to, cc, bcc, subject, body, null);
    }

    //For multiple to address without cc and bcc
    public static bool sendEmail(String from, String to, String subject, String body)
    {
        return sendEmail(from, to, "", "", subject, body);
    }

    //For multiple to addresses without cc and bcc
    public static bool sendEmail(String from, List<String> to, String subject, String body)
    {
        return sendEmail(from, to, null, null, subject, body);
    }

    //Login Check

    public static int GetLoginId()
    {
        if (HttpContext.Current.Session["UserID"] != null)
        {
            if (int.Parse(HttpContext.Current.Session["UserID"].ToString()) > 0)
            {
                return int.Parse(HttpContext.Current.Session["UserID"].ToString());
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }

    public static bool GetLoginStatus()
    {
        if (GetLoginId() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public static bool IsUnlimited(int iid)
    {
        Payment payment = Payment.GetActivePaymentByUserIdandDomainId(iid, GetDomainID);
        return (payment != null);
    }

    public static void Redirect(string CurrentUrl)
    {
        if (!LoginStatus)
        {
            HttpContext.Current.Response.Redirect("http://" + GetMicroHostName + "/aspx/LoginPage.aspx?goto=" + CurrentUrl);
        }

    }
    public static void RedirectBuyNow(string CurrentUrl)
    {
        if (!LoginStatus)
        {
            //first step
            HttpContext.Current.Response.Redirect("http://" + GetMicroHostName + "/aspx/LoginBuyNow.aspx");
        }
        else
        {
            //second step
            HttpContext.Current.Response.Redirect("http://" + GetMicroHostName + "/aspx/ProcessBuyNow.aspx");
        }
    }
    public static void RedirectCredits()
    {
        System.Data.DataSet dsCreditsCart = Credits.GetCreditsCartByUserId(LoginId, GetAhDomainId());
        if (dsCreditsCart.Tables[0].Rows.Count > 0)
        {
            HttpContext.Current.Response.Redirect("http://" + GetMicroHostName + "/aspx/Credits.aspx");
        }
    }

    public static bool CheckFirstTimeUser()
    {
        UserAccount loggedUser = UserAccount.GetUserAccountByID(LoginId);
        if (loggedUser == null) return false;
        return loggedUser.FirstLogin_Ind;
    }


    public static bool CheckFirstTime()
    {
        UserAccount loggedUser = UserAccount.GetUserAccountByID(LoginId);
        if (loggedUser != null)
        {
            if (HttpContext.Current.Session["firsttimeuser"] != null)
                return HttpContext.Current.Session["firsttimeuser"].ToString() == "1";
            return loggedUser.FirstLogin_Ind;
        }
        else
        {
            if (HttpContext.Current.Request.Cookies["firsttimeuser"] != null)
                return HttpContext.Current.Request.Cookies["firsttimeuser"].ToString() == "1";
            else return true;
        }
    }

    public static void RedirectFirstTimeUserDiscount()
    {
        //Disable for new release on 10/2/2012
        if (!FirstCoursePromotion)
            return;
        if (!CheckFirstTimeUser())
            return;
        if (Cart.GetInCompleteCartsByUserIdAndDomainId(LoginId, GetAhDomainId()).Count > 0)
        {
            HttpContext.Current.Response.Redirect("http://" + GetMicroHostName + "/aspx/FirstTimeUserDiscount.aspx");
        }
    }

    public string ClearHTMLTags(string strHTML)
    {
        //Variables used in the function
        Regex regEx = new Regex("<[^>]*>", RegexOptions.IgnoreCase);
        string strTagLess = null;
        //---------------------------------------
        strTagLess = strHTML;
        //---------------------------------------
        //regEx.Pattern = "<[^>]*>"
        //this pattern mathces any html tag
        strTagLess = regEx.Replace(strTagLess, "");
        //all html tags are stripped
        //---------------------------------------
        regEx = null;
        //Destroys the regExp object
        //---------------------------------------    
        //---------------------------------------
        //The results are passed back
        //---------------------------------------
        return strTagLess;

    }

    public static void RemoveCookie(string key)
    {
        //get cookies value
        HttpCookie cookie = null;
        if (HttpContext.Current.Request.Cookies[key] != null)
        {
            cookie = HttpContext.Current.Request.Cookies[key];
            cookie.Value = null;
            cookie.Expires = DateTime.Now.AddDays(-1);
            cookie.Domain = "." + MicroSiteCommon.GetAhDomainName().ToLower();
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }

    public static void CreateCookie(string key, string value)
    {
        //get cookies value
        HttpCookie cookie = null;
        if (HttpContext.Current.Request.Cookies[key] != null)
        {
            cookie = HttpContext.Current.Request.Cookies[key];
        }
        else
        {
            cookie = new HttpCookie(key);
        }
        cookie.Domain = "." + MicroSiteCommon.GetAhDomainName().ToLower();
        cookie.Expires = DateTime.Now.AddDays(10);
        cookie.Value = value;
        HttpContext.Current.Response.Cookies.Add(cookie);
    }

    public static void CreateCookie(string key, string value, DateTime expiredate)
    {
        //get cookies value
        HttpCookie cookie = null;
        if (HttpContext.Current.Request.Cookies[key] != null)
        {
            cookie = HttpContext.Current.Request.Cookies[key];
        }
        else
        {
            cookie = new HttpCookie(key);
        }
        cookie.Expires = expiredate;
        cookie.Value = value;
        HttpContext.Current.Response.Cookies.Add(cookie);
    }

    public static void CreateLoginCookie(string key, string value, DateTime expiredate)
    {
        //get cookies value
        HttpCookie cookie = null;
        if (HttpContext.Current.Request.Cookies[key] != null)
        {
            cookie = HttpContext.Current.Request.Cookies[key];
        }
        else
        {
            cookie = new HttpCookie(key);
        }
        cookie.Domain = "." + MicroSiteCommon.GetAhDomainName().ToLower();
        if(expiredate > DateTime.Now)
            cookie.Expires = expiredate;
        cookie.Value = value;
        HttpContext.Current.Response.Cookies.Add(cookie);
    }

    protected void GET404Error()
    {
        //StringBuilder sb = new StringBuilder();
        //sb.Append(@"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""><html xmlns=""http://www.w3.org/1999/xhtml"">");
        //sb.Append("<body>");
        //sb.Append("<br>The document you requested can not be found.");
        //sb.Append("<a href=\"/index.aspx\" \"> <FONT COLOR=\"#0000FF\">  Go to Home page </FONT></a>");
        //sb.Append("</br><div style='height:200px'></div>");
        //Response.Clear();
        Response.Redirect("~/Aspx/404.aspx");
        //Response.Status = "404 page not found!";
        //Response.Write(sb.ToString());
        //Response.End();
        //throw new HttpException(404, "not found");
    }

    public static bool IsValidDateTime(string dateTime)
    {
        DateTime value;

        return (DateTime.TryParse(dateTime, out value) &&
            (value >= new DateTime(1953, 1, 1) && value <= new DateTime(9999, 12, 31))
            );
    }
}