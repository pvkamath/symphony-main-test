﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Globalization;

/// <summary>
/// Summary description for RootPage
/// </summary>
public class RptRootPage : Page
{
    public RptRootPage()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public void ExportExcel(Control col, string fileName, string title)
    {
        try
        {           
            WriteHeader(fileName, title);
            WriteGridView((GridView)col);            
        }
        catch (Exception ex)
        {
            string s = ex.Message;
            throw ex;
        }
    }

    protected void WriteHeader(string fileName, string title)
    {
        Prepare(fileName, "excel");
        Response.Write("<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>");
        Response.Write("<b> <span style='font-size:large;'>" + title + "</span></b><br/><br/>");
    }

    protected void WriteGridView(GridView gv)
    {
          Response.Write("<div><table cellspacing=\"0\" rules=\"all\" border=\"1\" style=\"border-collapse:collapse;\">");   
          using (System.IO.StringWriter stringWrite = new StringWriter(CultureInfo.CurrentCulture))
          {
              using (HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite))
              {               
                  foreach (GridViewRow row in gv.Rows)
                  {
                      row.Attributes.Add("class", "text");
                  }
                  gv.RenderControl(htmlWrite);                  
                  string style = @"<style> .text { mso-number-format:\@; } </style> ";
                  Response.Write(style);
                  Response.Write(stringWrite.ToString());
                  Response.Write("</table></div>");
                  Response.Flush();
                  Response.End();
              }
          }
    }

    public void ExportExcelFromDataTable(DataTable dt, string fileName, string title)
    {
        try
        {            
            WriteHeader(fileName, title);
            WriteTable(dt);
            Response.End();            
        }
        catch (Exception ex)
        {
            string s = ex.ToString();
            throw ex;
        }
    }

    protected void WriteTable(DataTable dt)
    {   
        GridView gridview = new GridView();
        gridview.DataSource = dt;
        gridview.DataBind();
        WriteGridView(gridview);
    }

    public void ExportExcel(List<Control> cols, string fileName, string Title)
    {
        try
        {
            if (cols.Count > 0)
            {
                Prepare(fileName, "excel");
                System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                foreach (Control c in cols)
                    c.RenderControl(htmlWrite);
                Response.Write("<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>");
                Response.Write("<b> <span style='font-size:large;'>" + Title + "</span></b><br/><br/>");
                Response.Write(stringWrite.ToString());
                Response.End();
            }
        }
        catch (Exception ex)
        {
            string s = ex.ToString();
            throw ex;
        }
    }
    public void ExportWord(Control col, string fileName, string Title)
    {

        try
        {
            Prepare(fileName, "word");
            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            col.RenderControl(htmlWrite);
            Response.Write("<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>");
            Response.Write("<b> <span style='font-size:large;'> " + Title + " </span></b><br/><br/>");
            Response.Write(stringWrite.ToString());
            Response.End();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void ExportWord(List<Control> cols, string fileName, string Title)
    {
        try
        {
            if (cols.Count > 0)
            {
                Prepare(fileName, "word");
                System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                foreach (Control c in cols)
                    c.RenderControl(htmlWrite);
                Response.Write("<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body>");
                Response.Write("<b> <span style='font-size:large;'> " + Title + "  </span></b><br/><br/>");
                Response.Write(stringWrite.ToString());
                Response.End();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void WriteCSV(GridView gv, string FileName)
    {
        if (gv.Rows.Count > 0)
        {
            try
            {
                Prepare(FileName, "csv");
                System.IO.StreamWriter stringWrite = new System.IO.StreamWriter(Response.OutputStream);
                
                //Cannot get headertext from class file, it's only accessible from aspx page
                foreach (DataControlField d in gv.Columns)
                {
                    gv.HeaderRow.Cells[gv.Columns.IndexOf(d)].Text = d.HeaderText;
                }
                stringWrite.WriteLine(GetCSVLine(gv.HeaderRow.Cells));

                // Add all the data rows
                foreach (GridViewRow row in gv.Rows)
                {
                    stringWrite.WriteLine(GetCSVLine(row.Cells));
                }
                stringWrite.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
    public static string GetCSVLine(TableCellCollection cellsToAdd)
    {
        string line = String.Empty;
        string celltext = "";

        bool isFirst = true;
        foreach (TableCell cell in cellsToAdd)
        {
            if (!isFirst)
            {
                line += ",";
            }
            isFirst = false;
            celltext = HttpUtility.HtmlDecode(cell.Text);
            if (celltext == " ")
            {
                line += "\"\"";
            }
            else
            {
                line += "\"" + celltext + "\"";
            }
        }
        return line;
    }
    void Prepare(string pFileName, string pType)
    {
        Response.Clear();
        Response.ClearHeaders();
        Response.Buffer = true;        
        Response.ContentEncoding = System.Text.Encoding.UTF8;
        Response.Charset = "utf-8";
        switch (pType)
        {
            default:
                Response.AddHeader("content-disposition", "attachment;filename=" + pFileName + ".xls");
                Response.ContentType = "application/vnd.xls"; 
                break;
            case "word":
                Response.AddHeader("content-disposition", "attachment;filename=" + pFileName + ".doc");
                Response.ContentType = "application/vnd.word"; 
                break;
            case "csv":
                Response.AddHeader("content-disposition", "attachment;filename=" + pFileName + ".csv");
                Response.ContentType = "text/plain"; 
                break;
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        // Nothing needed here...
    }
}
