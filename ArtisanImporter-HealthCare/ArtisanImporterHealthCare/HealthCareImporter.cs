﻿using PearlsReview.BLL;
using PearlsReview.QTI;
using Symphony.Core;
using Symphony.Core.Controllers;
using Symphony.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ArtisanImporterHealthCare
{

    public class HealthcareImporter
    {
        //template is added to all pages
        private string template = @"
            <div id=""Main"" class=""Main"">
              <div class=""MainWrapper"">
                <div id=""Content"" class=""Content"">
                  <div class=""ContentWrapper"">
                    {0}
                  </div>
                </div>
              </div>
            </div>
<script type='text/javascript'>
var popupWindow = null;
function centeredPopup(url,winName,w,h,scroll){{
  var leftPosition = (screen.width) ? (screen.width-w)/2 : 0;
  var topPosition = (screen.height) ? (screen.height-h)/4 : 0;
  var settings = 'height='+h+',width='+w+',top='+topPosition+',left='+leftPosition+',scrollbars='+scroll+',resizable,location=no';
  window.open(url,winName,settings)
}}
</script>
";


        private int PageCounter = -1;
        private ArtisanController Controller;
        private string Root = string.Empty;
        private Random Random;
        private int CustomerID = 1;
        private int UserID = 147;


        public string ww(string a)
        {
            return a;
        }

        public Boolean ImportCourse(int cetopicid, out int artisanCourseId)
        {

            //  AccessCETopic ce = new AccessCETopic();
            Topic cetopic = Topic.GetTopicByID(cetopicid);

            Controller = new ArtisanController();


            ArtisanCourse ac = new ArtisanCourse();
            //ac.Description = "description";
            // ac.Objectives = "objectives";
            //ac.Name = "gggcourseName";
            //ac.Keywords = "id";

            ac.Description = HtmlRemoval.StripTagsCharArray(cetopic.topic_summary);
            string courseObj = HtmlRemoval.StripTagsCharArray(cetopic.Objectives);
            if (courseObj.Length > 1024)
                courseObj = courseObj.Substring(1023);

            ac.Objectives = courseObj;
            ac.Name = "ImportTestRound3 " + cetopic.Course_Number + " " + cetopic.TopicName;
            ac.Keywords = cetopic.MetaKW;

            ac.PassingScore = cetopic.Pass_Score;
            ac.Sections = new List<ArtisanSection>();
            ac.NavigationMethod = ArtisanCourseNavigationMethod.FreeForm;
            ac.CompletionMethod = ArtisanCourseCompletionMethod.Test;
            ac.Parameters = "[]";
            //add categoryid=136
            ac.CategoryId = 136;

            Symphony.Core.Data.ArtisanTheme theme = new Symphony.Core.Data.ArtisanTheme("Name", "BE Smooth");
            ac.ThemeId = theme.Id;

            //add Objective section
            var des = new ArtisanSection()
            {
                Name = "Objectives",
                SectionType = (int)ArtisanSectionType.Objectives
            };
            ac.Sections.Add(des);

            // add section
            var sco = new ArtisanSection()
            {
                Name = ac.Name,
                SectionType = (int)ArtisanSectionType.Sco
            };
            ac.Sections.Add(sco);
            sco.Sections = new List<ArtisanSection>();



            // get vignette desc 
            ViDefinition vinfo = ViDefinition.GetViDefinitionByTopicID(cetopicid);

            if (vinfo != null)
            {
                // vignett root section
                var vig = new ArtisanSection()
                {
                    SectionType = (int)ArtisanSectionType.Sco,
                    //TestType = ArtisanTestType.Random,
                    Name = "Vig"
                };
                ac.Sections.Add(vig);
                vig.Sections = new List<ArtisanSection>();

                var vigQuestion = new ArtisanSection()
                {
                    Name = "vig question page",
                    SectionType = (int)ArtisanSectionType.LearningObject,
                    PassingScore = 100,
                    Description = vinfo.Vignette_Desc,
                    IsQuiz = true
                };

                vig.Sections.Add(vigQuestion);
                vigQuestion.Pages = new List<ArtisanPage>();

                GetQuestionPage(cetopicid, vigQuestion);

            }



            //test root section

            var test = new ArtisanSection()
            {
                SectionType = (int)ArtisanSectionType.Sco,
                //TestType = ArtisanTestType.Random,
                Name = "Test"
            };
            ac.Sections.Add(test);
            test.Sections = new List<ArtisanSection>();



            //var postTest = new ArtisanSection()
            //{
            //    SectionType = (int)ArtisanSectionType.Posttest,
            //    TestType = ArtisanTestType.Random,
            //    Name = "Post-Test"
            //};
            //ac.Sections.Add(postTest);
            //postTest.Pages = new List<ArtisanPage>();


            //add course content
            var learningObject = new ArtisanSection()
            {
                Name = "Course Text",
                SectionType = (int)ArtisanSectionType.LearningObject
            };
            sco.Sections.Add(learningObject);
            learningObject.Pages = new List<ArtisanPage>();
            List<TopicPage> pages = TopicPage.GetTopicPagesByTopicID(cetopicid, "");
            foreach (var page in pages)
            {


                string cepage = page.Page_Content;
                AddContentPage(cepage, learningObject);
                // string s1 = page.Page_Content;
                // int s2 = page.Page_Num;
            }


            //learningObject.Pages = new List<ArtisanPage>();
            //string cepage = "<div><p>ce page test 1</p></div>";
            //AddContentPage(cepage, learningObject);
            // string cepage2 = "<div><p>ce page test 2</p></div>";
            //AddContentPage(cepage2, learningObject);



            //add vignette content



            //if (vinfo != null)
            //{
            //    var vigQuestion = new ArtisanSection()
            //    {
            //        Name = "vig question page",
            //        SectionType = (int)ArtisanSectionType.LearningObject,
            //        PassingScore = 100,
            //        Description = vinfo.Vignette_Desc,
            //        IsQuiz = true
            //    };

            //    vig.Sections.Add(vigQuestion);
            //    vigQuestion.Pages = new List<ArtisanPage>();

            //    GetQuestionPage(cetopicid, vigQuestion);

            //}


            //add test qestions

            //add test

            TestDefinition testInfo = TestDefinition.GetTestDefinitionByTopicID(cetopicid);
            if (testInfo != null)
            {
                var testQuestion = new ArtisanSection()
                {
                    Name = "Test question page",
                    SectionType = (int)ArtisanSectionType.LearningObject,
                    PassingScore = 100,
                    Description = testInfo.TopicID.ToString(),
                    IsQuiz = true
                };

                test.Sections.Add(testQuestion);
                testQuestion.Pages = new List<ArtisanPage>();

                GetTestQuestionPage(cetopicid, testQuestion);

            }





            // save the course            
            var result = Controller.SaveCourse(CustomerID, UserID, 10000 + cetopicid, ac);
            artisanCourseId = result.Data.Id;
            return result.Success;




        }



        //methdo to add course pages here
        private void AddContentPage(string cepage, ArtisanSection learningObject)
        {
            // List<ArtisanAsset> assets = new List<ArtisanAsset>();
            ArtisanPage ap = new ArtisanPage()
            {
                Name = "page name",
                Html = string.Format(template, cepage),
                PageType = (int)ArtisanPageType.Content,
                Id = PageCounter--
            };


            // change ap name 
            //if (ap.Name.EndsWith("-S"))
            //{
            //    ap.Name = "Summary";
            //}
            //else if (ap.Name.EndsWith("-I"))
            //{
            //    ap.Name = "Introduction";
            //}

            //add page
            learningObject.Pages.Add(ap);


        }

        //to add question page
        private void GetQuestionPage(int CETopicid, ArtisanSection vigSection)
        {
            ArtisanQuestionType questionType = ArtisanQuestionType.MultipleChoice;


            // AccessCETopic ac = new AccessCETopic();
            List<QTIQuestionObject> questions = Test.GetVignetteQuestionObjectListByTopicID(CETopicid, "");
            int questionCount = 0;
            foreach (var item in questions)
            {

                //add answers
                var answers = new List<ArtisanAnswer>();
                var j = 0;
                var a = new List<string>();
                a.Add(item.cAnswer_A_Text);
                a.Add(item.cAnswer_B_Text);
                a.Add(item.cAnswer_C_Text);
                a.Add(item.cAnswer_D_Text);
                a.Add(item.cAnswer_E_Text);
                int correctAnswer = item.cCorrectAnswer[0] - 65;
                for (int i = 0; i < 5; i++)
                {
                    if (a[i] != "")
                    {


                        string b = a[i];
                        answers.Add(new ArtisanAnswer()
                        {
                            Text = a[i],
                            IsCorrect = (i == correctAnswer),
                            Sort = j++
                        });
                    }
                }




                ArtisanPage ap = new ArtisanPage()
                {
                    Name = "Question " + ++questionCount,
                    Html = item.cQuestionText,
                    CorrectResponse = "Correct!" + item.cCorrectAnswerFeedback,
                    IncorrectResponse = "Incorrect. ",
                    PageType = (int)ArtisanPageType.Question,

                    QuestionType = questionType,
                    Answers = answers,
                    Id = PageCounter--
                };



                vigSection.Pages.Add(ap);

                //Console.WriteLine(item.cCorrectAnswer);
            }


        }

        public static List<QTIQuestionObject> GetTopicQuestionObjectListByTopicID(int TopicID, string sUserAnswers)
        {
            //  ViDefinitionInfo oViDefinition = null;
            TestDefinition oTestDefinition = null;
            QTIUtils oQTIUtils = new QTIUtils();

            // NOTE: No caching of these items, because they contain user responses
            // along with test questions
            List<QTIQuestionObject> QTIQuestionObjects = null;

            // get the vignette definition record for this topic
            //  oViDefinition = GetViDefinitionByTopicID(TopicID);

            //get topic Definition

            oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(TopicID);


            if (oTestDefinition != null)
            {
                // get the QTIQuestionObjectList from the ViDefinition XML
                QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
            }


            if (QTIQuestionObjects != null)
            {

                if (string.IsNullOrEmpty(sUserAnswers))
                {
                    // incomplete test that does not match the current vignette definition version number
                    // set to default X and blank answers
                    // NOTE: This is also the case with new test record that has not had the vignette
                    // shown yet -- it will start out with vignetteversion field = 0 to this
                    // comparison will fail and we will insert all empty answers
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                        {
                            QTIQuestion.cUserAnswer = "X";
                        }
                        else
                        {
                            QTIQuestion.cUserAnswer = "";
                        }
                    }
                }
                // * chnaged By navya
                else
                {
                    // incomplete test that matches the current test definition version number
                    // so it may have some user responses stored. Grab them from the test XMLVignette field
                    // and populate them here.
                    List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(sUserAnswers);
                    String[] Answers = UserAnswers.ToArray();

                    int iLoop = 0;
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        QTIQuestion.cUserAnswer = Answers[iLoop];
                        // be sure we're not empty -- if so, set to X
                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                            QTIQuestion.cUserAnswer = "X";
                        iLoop++;
                    }
                }
            }

            return QTIQuestionObjects;
        }

        private void GetTestQuestionPage(int CETopicid, ArtisanSection testSection)
        {
            ArtisanQuestionType questionType = ArtisanQuestionType.MultipleChoice;


            //  AccessCETopic ac = new AccessCETopic();
            List<QTIQuestionObject> questions = HealthcareImporter.GetTopicQuestionObjectListByTopicID(CETopicid, "");
            int questionCount = 0;
            foreach (var item in questions)
            {

                //add answers
                var answers = new List<ArtisanAnswer>();
                var j = 0;
                var a = new List<string>();
                a.Add(item.cAnswer_A_Text);
                a.Add(item.cAnswer_B_Text);
                a.Add(item.cAnswer_C_Text);
                a.Add(item.cAnswer_D_Text);
                a.Add(item.cAnswer_E_Text);
                int correctAnswer = item.cCorrectAnswer[0] - 65;
                for (int i = 0; i < 5; i++)
                {
                    if (a[i] != "")
                    {


                        string b = a[i];
                        answers.Add(new ArtisanAnswer()
                        {
                            Text = a[i],
                            IsCorrect = (i == correctAnswer),
                            Sort = j++
                        });
                    }
                }




                ArtisanPage ap = new ArtisanPage()
                {
                    Name = "Question " + ++questionCount,
                    Html = item.cQuestionText,
                    CorrectResponse = "Correct!" + item.cCorrectAnswerFeedback,
                    IncorrectResponse = "Incorrect. ",
                    PageType = (int)ArtisanPageType.Question,

                    QuestionType = questionType,
                    Answers = answers,
                    Id = PageCounter--
                };



                testSection.Pages.Add(ap);

                //Console.WriteLine(item.cCorrectAnswer);
            }


        }
    }

    public static class HtmlRemoval
    {
        /// <summary>
        /// Remove HTML from string with Regex.
        /// </summary>
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        /// <summary>
        /// Remove HTML tags from string using char array.
        /// </summary>
        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }

}
