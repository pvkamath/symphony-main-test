﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtisanImporterHealthCare
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                if (args.Length == 0)
                {
                    WriteUsage();
                }
                else if (args.Length > 4)
                {
                    WriteUsage();
                    Console.WriteLine("NOTE: it appears that you may have used a path with spaces in it; if the path has spaces in it, it *must* be enclosed in quotes!");
                }
                else
                {
                    int customerId = int.Parse(args[0]);
                    int userId = int.Parse(args[1]);
                    if (args[2].Trim() == "-single")
                    {
                        int topicId = Convert.ToInt32(args[3]);
                        Import(customerId, userId, topicId);
                    }
                    else if (args[2].Trim() == "-multiple")
                    {
                        foreach (string directory in Directory.GetDirectories(args[3]))
                        {
                            Import(customerId, userId, directory);
                        }
                    }
                    else
                    {
                        WriteUsage();
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.ToString());
            }
            Console.ReadLine();
        }

        private static void Import(int customerId, int userId, string folder)
        {
            if (!Directory.Exists(folder))
            {
                throw new Exception("The specified path doesn't exist. If there are spaces in the path, be sure to wrap it in quotes.");
            }

            //var importer = HealthcareImporter.ImportCourse(customerId, userId, folder);



            //importer.Process();

        }

        private static void Import(int customerId, int userId, int topicId)
        {
            HealthcareImporter importer = new HealthcareImporter();
            int artisanCourseId = -1;
            importer.ImportCourse(topicId, out artisanCourseId);
 
        }

        private static void WriteUsage()
        {
            Console.WriteLine(@"
Artisan Course Importer
-----------------------

Currently, this importer only supports HealthCare courses. Please ensure the connection string is set correctly in the app.config for the database into which you want the courses imported.
Parameters: 

  {customerId}: the integer id of the customer under which the import will occur

  {userId}: the integer id of the user under which the import will occur

  -single {path}: imports a single course from the specified path. The course contents must be directly under the specified path.

  -multiple {path}: imports all courses in the specified path. The courses must each be in individual folders under the specified path.

You can only use one parameter or the other, not both.

Example: ArtisanImporterHealthCare.exe 1 147 -single ""C:\temp\Course-DU-Teller Customer Service Standards - PT""

");
        }
    }
}
