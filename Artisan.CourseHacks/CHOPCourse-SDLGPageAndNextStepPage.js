﻿
//------------ for Self-Directed Learning Goals Page
function updateReviewPage(page) {
    page.getHtml = function () {
        // Get the questions actually answered first to avoid removing them if by the end 
        // of the course they have been hidden
        if (!page.record._answeredQuestionsCache || page.record._answeredQuestionsCache.length == 0) {
            page.record.buildAnswerCache();
        }

        var answeredQuestionIds = {};

        for (var i = 0; i < page.record._answeredQuestionsCache.length; i++) {
            answeredQuestionIds[page.record._answeredQuestionsCache[i].id] = true;
        }

        // Clears question type on any hidden question pages that were not
        // answered. This will exclude them from the review calculation
        cleanHiddenQuestionPages(Artisan.App.course, answeredQuestionIds);

        var content = this.getRawHtml(); // Review calc is done here

        // Undo all the changes made by cleanHiddenQuestionPages
        restoreHiddenQuestionPages(Artisan.App.course);

        return $('.artisan-page-wrap').html().replace('{' + '!content}', content);
    }
}

Artisan.App.moveNext = function () {
    var state = Artisan.App.getState();
    var nextPage = (state.page != null ? state.learningObject.getNextPage(state.page.getId()) : null);
    var nextLO = Artisan.App.course.getNextLearningObject(state.learningObject.getId());
    var nextSCO = Artisan.App.course.getNextSco(state.sco.getId());

    if (nextPage && nextPage.isReview) {
        updateReviewPage(nextPage);
    }

    if (Artisan.App.nextLOOverride) {
        this.selectLearningObject(Artisan.App.nextLOOverride, false);
        Artisan.App.nextLOOverride = null;
    } else if (nextPage) {
        if (nextPage.isReview && Artisan.App.isReturnOverride) {
            this.selectLearningObject(Artisan.App.returnLOOverride);
            this.selectPage(Artisan.App.returnPageOverride);
            Artisan.App.returnPageOverride = null;
            Artisan.App.returnLOOverride = null;
            Artisan.App.isReturnOverride = false;
            goalSelected = true;
            nextStepSelected = true;

            Artisan.App.moveNext();

            // Hide the section after we complete it so we don't see it again
            var section = findSectionByName(Artisan.App.course, 'Schedule Meeting');
            if (section != null) {
                section.config.hidden = true;
            }
        } else {
            this.selectPage(nextPage);

            if (nextPage.config.hidden) {
                Artisan.App.moveNext();
            }
        }
    }
    else if (nextLO) {
        if (Artisan.App.isReturnOverride) {
            this.selectLearningObject(Artisan.App.returnLOOverride);
            this.selectPage(Artisan.App.returnPageOverride);
            Artisan.App.returnPageOverride = null;
            Artisan.App.returnLOOverride = null;
            Artisan.App.isReturnOverride = false;
            goalSelected = true;
            nextStepSelected = true;

            Artisan.App.moveNext();
            // Hide the section after we complete it so we don't see it again
            var section = findSectionByName(Artisan.App.course, 'Schedule Meeting');
            if (section != null) {
                section.config.hidden = true;
            }
        } else if (nextLO.config.hidden) {
            if (nextLO.getLastPage().isReview) {
                updateReviewPage(nextLO.getLastPage());
            }
            this.selectLearningObject(nextLO, true);
            Artisan.App.moveNext();
        } else {
            this.selectLearningObject(nextLO, false);
        }
    }
    else if (nextSCO) {
        if (Artisan.App.isReturnOverride) {
            this.selectLearningObject(Artisan.App.returnLOOverride);
            this.selectPage(Artisan.App.returnPageOverride);
            Artisan.App.returnPageOverride = null;
            Artisan.App.returnLOOverride = null;
            Artisan.App.isReturnOverride = false;
            goalSelected = true;
            nextStepSelected = true;

            Artisan.App.moveNext();

            // Hide the section after we complete it so we don't see it again
            var section = findSectionByName(Artisan.App.course, 'Schedule Meeting');
            if (section != null) {
                section.config.hidden = true;
            }
        } else {
            this.selectSco(nextSCO, false);
        }
    }
    else if (Artisan.App.course.hasSurvey()) {
        if (Artisan.App.isReturnOverride) {
            this.selectLearningObject(Artisan.App.returnLOOverride);
            this.selectPage(Artisan.App.returnPageOverride);
            Artisan.App.returnPageOverride = null;
            Artisan.App.returnLOOverride = null;
            Artisan.App.isReturnOverride = false;
            goalSelected = true;
            nextStepSelected = true;

            Artisan.App.moveNext();

        } else {
            this.selectSco(Artisan.App.course.getSurveySection());
        }
    }
}
Artisan.App.movePrevious = function () {
    var state = Artisan.App.getState();
    var previousPage = state.learningObject.getPreviousPage(state.page.getId());
    var previousLO = Artisan.App.course.getPreviousLearningObject(state.learningObject.getId());
    var previousSCO = Artisan.App.course.getPreviousSco(state.sco.getId());
    if (previousPage) {
        this.selectPage(previousPage);
        if (previousPage.config.hidden) {
            this.movePrevious();
        }
    } else if (previousLO) {
        if (Artisan.App.isReturnOverride) {
            this.selectLearningObject(Artisan.App.returnLOOverride);
            this.selectPage(Artisan.App.returnPageOverride);
            Artisan.App.returnPageOverride = null;
            Artisan.App.returnLOOverride = null;
            Artisan.App.isReturnOverride = false;
        } else if (previousLO.config.hidden) {
            this.selectLearningObject(previousLO, false);
            this.movePrevious();
        } else {
            this.selectLearningObject(previousLO, true);
        }
    } else if (previousSCO) {
        this.selectSco(previousSCO, true);
    }
}

var hidePages = [
    'Knowledge Check [PM1!]',
    'Knowledge Check [PM2!]',
    'Knowledge Check [PM3!]',
    'Knowledge Check [PM4!]',
    'Knowledge Check [SM1!]',
    'Knowledge Check [SM2!]',
    'Knowledge Check [SM3!]',
    'Knowledge Check [SM4!]',
    'Next Step [SM5!]'
];

function setHidden(section, pages, isHidden) {
    if (section.pages.length) {
        for (var i = 0; i < section.pages.length; i++) {
            if ($.inArray(section.pages[i].config.name, pages) > -1) {
                section.pages[i].config.hidden = isHidden;
                if (!isHidden) {
                    section.pages[i].config.html = section.pages[i].config.html.replace(/\s\[.+!\]/, '');
                }
            }
        }
    }
    if (section.sections.length) {
        for (var x = 0; x < section.sections.length; x++) {
            setHidden(section.sections[x], pages, isHidden);
        }
    }
}

setHidden(Artisan.App.course, hidePages, true);

var goalSelected = false; // For use with disabling next button

$('input[name="selfDirectedType"]').click(function () {
    var selected = $(this).val();

    goalSelected = true; // Next button will enable itself

    switch (selected) {
        case 'communication':
            var showPages = ['Knowledge Check [PM1!]', 'Knowledge Check [SM1!]'];
            setHidden(Artisan.App.course, showPages, false);
            break;
        case 'empathy':
            var showPages = ['Knowledge Check [PM2!]', 'Knowledge Check [SM2!]'];
            setHidden(Artisan.App.course, showPages, false);
            break;
        case 'train':
            var showPages = ['Knowledge Check [PM3!]', 'Knowledge Check [SM3!]'];
            setHidden(Artisan.App.course, showPages, false);
            break;
        case 'respect':
            var showPages = ['Knowledge Check [PM4!]', 'Knowledge Check [SM4!]'];
            setHidden(Artisan.App.course, showPages, false);
            break;
    }
});

// Hacky - because the next button is enabled/disabled based on other methods
// we need to continuously check if the goal has been selected and keep that 
// button disabled. Otherwise it will enable itself after a minimum page
// time out.
function handleNextButton() {
    if (!goalSelected) {
        $('.artisan-next').addClass('disabled');
        setTimeout(handleNextButton, 10);
    } else {
        $('.artisan-next').removeClass('disabled');
        // no need to keep checking after the user has selected
    }
}
handleNextButton();

// Function to clean out the unused question pages for grading
// It will create a temporary array of the questions in order
// to stick them back in once the score has been calculated
var replacedPages = {};
function cleanHiddenQuestionPages(section, answeredQuestionIds) {
    if (section.pages.length) {
        for (var i = 0; i < section.pages.length; i++) {

            // Leave pages alone if they were answered
            if (answeredQuestionIds[section.pages[i].config.id]) {
                continue;
            }

            if (section.pages[i].config.hidden || section.pages[i].parent.config.hidden) {
                replacedPages["p" + i + "s" + section.config.id] = section.pages[i].config.questionType;
                section.pages[i].config.questionType = null;
            }
        }
    }
    if (section.sections.length) {
        for (var x = 0; x < section.sections.length; x++) {
            cleanHiddenQuestionPages(section.sections[x], answeredQuestionIds);
        }
    }
}

function restoreHiddenQuestionPages(section) {
    if (section.pages.length) {
        for (var i = 0; i < section.pages.length; i++) {
            if (section.pages[i].config.questionType == null) {
                if (replacedPages["p" + i + "s" + section.config.id]) {
                    section.pages[i].config.questionType = replacedPages["p" + i + section.config.id];
                }
            }
        }
    }
}


//--------- Script for Next Step Page

// Show the schedule meetings section since this page will decide what to do with it
var section = findSectionByName(Artisan.App.course, 'Schedule Meeting');
if (section != null) {
    section.config.hidden = false;
}
function findSectionByName(section, name) {
    for (var i = 0; i < section.sections.length; i++) {
        if (section.sections[i].config.name == name) {
            return section.sections[i];
        } else {
            var possibleSection = findSectionByName(section.sections[i], name);
            if (possibleSection != null) {
                return possibleSection;
            }
        }
    }
    return null;
}

$('.artisan-next').addClass('disabled');


var section = findSectionByName(Artisan.App.course, 'Schedule Meeting');
if (section != null) {
    section.config.hidden = true;
}


var nextStepSelected = false;

$('input[name="nextStep"]').click(function () {
    var selected = $(this).val();
    $('.artisan-next').removeClass('disabled');
    Artisan.App.selected = selected;
    nextStepSelected = true;
    switch (selected) {
        case 'plan':
            Artisan.App.nextLOOverride = null;
            Artisan.App.returnPageOverride = null;
            Artisan.App.returnLOOverride = null;
            Artisan.App.isReturnOverride = false;
            break;
        case 'schedule':
            if (section != null) {
                Artisan.App.nextLOOverride = section;
                Artisan.App.returnPageOverride = Artisan.App.getState().page;
                Artisan.App.returnLOOverride = Artisan.App.getState().learningObject;
                Artisan.App.isReturnOverride = true;
                setHidden(Artisan.App.course, ['Next Step [SM5!]'], false);
            }
            break;
    }
});

// Hacky - because the next button is enabled/disabled based on other methods
// we need to continuously check if the goal has been selected and keep that 
// button disabled. Otherwise it will enable itself after a minimum page
// time out.
function handleNextButton() {
    if (!nextStepSelected) {
        $('.artisan-next').addClass('disabled');
        setTimeout(handleNextButton, 10);
    } else {
        $('.artisan-next').removeClass('disabled');
        // no need to keep checking after the user has selected
    }
}
handleNextButton();