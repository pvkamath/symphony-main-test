<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   FrozenSso
 * @author    Frozen Mountain <info@FrozenMountain.com>
 * @license   GPL-2.0+
 * @link      http://www.frozenmountain.com
 * @copyright 2013 Frozen Mountain Software
 */
?>
<div class="wrap">
	<?php screen_icon(); ?>
	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
	<p>Post <code>username</code> and <code>password</code> to the Authentication URL. If the login is valid, you will recieve JSON that looks like:</p>
	<pre><code>{
	"success":true,
	"uri":"<?php echo(home_url( '?sso=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234'));?>"
}</code></pre>
	<p>If the username or password are not valid you'll receive a response like:</p>
	<pre><code>{
	"success":false,
	"errors":[
		"empty_username",
		"invalid_username",
		"empty_password",
		"invalid_password"
	]
}</code></pre>
	<p>
		That URL can be retrieved using either an ajax GET request or by simply
		using an image. If the token in the URL is valid, the user will be 
		logged in and the following JSON will be returned:
	</p>
	<pre><code>{
	"success":true,
	"userId":1
}</code></pre>
	<p>
		If the token is not valid, you will get a JSON response like the one above
		but the error code will be "invalid_token" (potentially also 
		invalid_username if the user is deleted before the final step of the login
		takes place).		
	</p>
	<table class="form-table">
		<tbody>
			<tr valign="top">
				<th scope="row"><label for="mailserver_url">Authentication URL</label></th>
				<td><?php echo(home_url( '?sso=!'));?></td>
			</tr>
		</tbody>
	</table>
</div>