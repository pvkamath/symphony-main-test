<?php
/**
 * Represents the view for the public-facing component of the plugin.
 *
 * This typically includes any information, if any, that is rendered to the
 * frontend of the theme when the plugin is activated.
 *
 * @package   FrozenSso
 * @author    Frozen Mountain <info@FrozenMountain.com>
 * @license   GPL-2.0+
 * @link      http://www.frozenmountain.com
 * @copyright 2013 Frozen Mountain Software
 */
?>
<!-- This file is used to markup the public facing aspect of the plugin. -->