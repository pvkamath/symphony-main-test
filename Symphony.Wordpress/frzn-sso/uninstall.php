<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @package   FrozenSso
 * @author    Frozen Mountain <info@FrozenMountain.com>
 * @license   GPL-2.0+
 * @link      http://www.frozenmountain.com
 * @copyright 2013 Frozen Mountain Software
 */

// If uninstall, not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// TODO: Define uninstall functionality here