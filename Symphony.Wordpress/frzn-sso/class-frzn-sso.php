<?php
/**
 * Plugin Name.
 *
 * @package   FrozenSso
 * @author    Frozen Mountain <info@FrozenMountain.com>
 * @license   GPL-2.0+
 * @link      http://www.frozenmountain.com
 * @copyright 2013 Frozen Mountain Software
 */

/**
 * Plugin class.
 *
 * TODO: Rename this class to a proper name for your plugin.
 *
 * @package FrozenSso
 * @author  Frozen Mountain <info@FrozenMountain.com>
 */
class FrozenSso {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since   1.0.0
	 *
	 * @var     string
	 */
	protected $version = '1.0.0';

	/**
	 * Unique identifier for your plugin.
	 *
	 * Use this value (not the variable name) as the text domain when internationalizing strings of text. It should
	 * match the Text Domain file header in the main plugin file.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'frzn-sso';

	/**
	 * an interal secret string used for validating authentications. 
	 * Overwrite this value by defining FRZN_SSO_SECRET
	 * @since    1.0.0
	 * @var  	 string
	 */

	protected $auth_secret = 'jWbR4qEwlCrWEtPnxYRXlGfDnCt34yzPgK8wNu3PJlG0vi21EsB7bSQ7zaOqRf3oVHxXNXiqnPJ';

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Slug of the plugin screen.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_screen_hook_suffix = 'FrozenSso';

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {
		if ( NULL <> $this->auth_secret && !defined( 'FRZN_SSO_SECRET' ) )
			define( 'FRZN_SSO_SECRET', $this->auth_secret );
		add_filter('set_current_user', array($this,  'set_current_user'), 1, 3); //should happen AFTER password and cookie authentication
		
		// Load plugin text domain
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		//setup custom URLs
		add_action('parse_request', array($this, 'parse_request'), $accepted_args = 1 );
		add_action('init', array($this, 'do_rewrite'));
 
		add_action('send_headers', array($this, 'add_cors_headers'));

		// Add the options page and menu item.
		add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );

		// Load admin style sheet and JavaScript.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

		// Load public-facing style sheet and JavaScript.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		// Define custom functionality. Read more about actions and filters: http://codex.wordpress.org/Plugin_API#Hooks.2C_Actions_and_Filters
		add_action( 'TODO', array( $this, 'action_method_name' ) );
		add_filter( 'TODO', array( $this, 'filter_method_name' ) );

	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Fired when the plugin is activated.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
	 */
	public static function activate( $network_wide ) {
		// TODO: Define activation functionality here
		self::add_rewrite_rule();
		flush_rewrite_rules( true );
	}

	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Deactivate" action, false if WPMU is disabled or plugin is deactivated on an individual blog.
	 */
	public static function deactivate( $network_wide ) {
		// TODO: Define deactivation functionality here
		flush_rewrite_rules( true );
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . $locale . '.mo' );
		load_plugin_textdomain( $domain, FALSE, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
	}

	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_styles() {

		if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
			return;
		}

		$screen = get_current_screen();
		if ( $screen->id == $this->plugin_screen_hook_suffix ) {
			wp_enqueue_style( $this->plugin_slug .'-admin-styles', plugins_url( 'css/admin.css', __FILE__ ), array(), $this->version );
		}

	}

	/**
	 * Register and enqueue admin-specific JavaScript.
	 *
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_scripts() {

		if ( ! isset( $this->plugin_screen_hook_suffix ) ) {
			return;
		}

		$screen = get_current_screen();
		if ( $screen->id == $this->plugin_screen_hook_suffix ) {
			wp_enqueue_script( $this->plugin_slug . '-admin-script', plugins_url( 'js/admin.js', __FILE__ ), array( 'jquery' ), $this->version );
		}

	}

	/**
	 * Register and enqueue public-facing style sheet.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_slug . '-plugin-styles', plugins_url( 'css/public.css', __FILE__ ), array(), $this->version );
	}

	/**
	 * Register and enqueues public-facing JavaScript files.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_slug . '-plugin-script', plugins_url( 'js/public.js', __FILE__ ), array( 'jquery' ), $this->version );
	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		/*
		 * TODO:
		 *
		 * Change 'Page Title' to the title of your plugin admin page
		 * Change 'Menu Text' to the text for menu item for the plugin settings page
		 * Change 'frzn-sso' to the name of your plugin
		 */
		$this->plugin_screen_hook_suffix = add_plugins_page(
			__( 'Single Signon', $this->plugin_slug ),
			__( 'Single Signon', $this->plugin_slug ),
			'read',
			$this->plugin_slug,
			array( $this, 'display_plugin_admin_page' )
		);

	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_admin_page() {
		include_once( 'views/admin.php' );
	}

	/**
	 * NOTE:  Actions are points in the execution of a page or process
	 *        lifecycle that WordPress fires.
	 *
	 *        WordPress Actions: http://codex.wordpress.org/Plugin_API#Actions
	 *        Action Reference:  http://codex.wordpress.org/Plugin_API/Action_Reference
	 *
	 * @since    1.0.0
	 */
	public function action_method_name() {
		// TODO: Define your action hook callback here
	}

	/**
	 * NOTE:  Filters are points of execution in which WordPress modifies data
	 *        before saving it or sending it to the browser.
	 *
	 *        WordPress Filters: http://codex.wordpress.org/Plugin_API#Filters
	 *        Filter Reference:  http://codex.wordpress.org/Plugin_API/Filter_Reference
	 *
	 * @since    1.0.0
	 */
	public function filter_method_name() {
		// TODO: Define your filter hook callback here
	}

	public function do_rewrite()
	{
		self::add_rewrite_rule();
	}
	public static function add_rewrite_rule()
	{
		global $wp_rewrite;
		// add_rewrite_rule( '^sso/session/([^/]+)/?$', 'index.php?sso=login&sso-token=$matches[1]', 'top');
		// add_rewrite_rule( '^sso/session/?$', 'index.php?sso=create_session', 'top');
		
		add_rewrite_tag( '%sso%', '!|([a-zA-Z0-9_-]+)' );
	}



	public function parse_request($wp)
	{
		// only process requests with sso query_var
		if (array_key_exists('sso', $wp->query_vars)) {
			$action = $wp->query_vars['sso'];
			switch ($action) {
				case '!':
					$this->get_session_token();
					break;
				// default:
				// 	// do the login
				// 	$this->do_login($action);
				// 	break;
			}			
		}
	}
	
	public function get_session_token() {
		$username = $_POST['username'];
		$password = $_POST['password'];
		$data = new StdClass;
		$this->add_cors_headers();
		header('Content-Type: application/json');
		$user = wp_authenticate_username_password( null, $username, $password );
		if (is_wp_error($user)) {
			$data->success = false;
			$data->errors = $user->get_error_codes();
		} else {
			$usr = new StdClass;
			$usr->username = $username;
			$usr->expiry = strtotime('+ 1 day'); 
			$usr->sig = sha1( json_encode($usr) . FRZN_SSO_SECRET );
			$usr_string = json_encode( $usr );
			$usr_string = $this->base64url_encode( $usr_string );
			// $usr_string = $this->encrypt_string($usr_string);
			$data->success = true;
			$data->token = 'sso='.$usr_string;
		}
		echo(json_encode($data));
		die();
	}
	public function set_current_user()
	{
		$sso = $_REQUEST["sso"];
		if (is_null($sso) || $sso == '!') {
			return;
		}
		global $current_user;
		$user = $this->get_user_from_token($sso);

		if (is_null($user)) {
			// the token is invalid delete from the request
			$_GET['sso'] = '';
			return;
		}

		if($current_user->ID != $user->ID){
			$current_user = $user;
			wp_set_auth_cookie( $user->ID, $remember = false);
		}
	}

	public function get_user_from_token($token)
	{
		$token = $this->base64url_decode($token);
		$data = json_decode($token);
		$token_sig = $data->sig;
		unset($data->sig);
		$calc_sig = sha1( json_encode($data) . FRZN_SSO_SECRET );
		if ($token_sig == $calc_sig && $data->expiry >= time()) {
			$user = get_user_by('login', $data->username);

			//This is taken from the core wp_authenticate_username_password code http://core.trac.wordpress.org/browser/tags/3.5.1/wp-includes/user.php#L72
	        if ( !$user )
	        	return null;
		
	        if ( is_multisite() ) {
	            // Is user marked as spam?
	            if ( 1 == $user->spam)
	                return null;
	            // Is a user's blog marked as spam?
	            if ( !is_super_admin( $user->ID ) && isset($user->primary_blog) ) {
	                $details = get_blog_details( $user->primary_blog );
	                if ( is_object( $details ) && $details->spam == 1 )
	                    return null;
	            }
	        }
		}
		return $user;	
	}

	public function add_cors_headers() {
		//TODO: change the allowed origins to be taken from a setting set in the 
		header("Access-Control-Allow-Origin: *");
		header('Access-Control-Allow-Credentials: true');
	}

	/**
	 * Create a hash based on the FRZN_SSO_SECRET definition
	 * @access private
	 * @return string
	 */
	private function create_hash() {
		return substr( sha1( FRONT_END_COOKIE_SSO_SECRET ), 0, 32 );
	}


	/**
	 * Encrypt the authentication string
	 * 
	 * @access private
	 * @param string $text
	 * @return string
	 */
	private function encrypt_string( $text ) {
				
		$salt = $this->create_hash();
		return trim( $this->base64url_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv( mcrypt_get_iv_size( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND ) ) ) ); 
	}
		
	/**
	 * Decrypt the cookie string
	 * @uses apply_filters() Calls $this->plugin_prefix . 'decrypt_string' to allow user defined decryption algorithms. Parameter (boolean) false, (string) $text. Hooked function needs to return something other than false to kick in
	 * @access private
	 * @param string $text
	 * @return string
	 */
	private function decrypt_string( $text ) {
		$salt = $this->create_hash();
		return trim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, $salt, $this->base64url_decode( $text ), MCRYPT_MODE_ECB, mcrypt_create_iv( mcrypt_get_iv_size( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ), MCRYPT_RAND ) ) ); 
	}


	//base64 encoding does not produce values that work in URLs so we replace the + with - and / with _ and add the = padding on decode
	// take from http://us.php.net/manual/en/function.base64-encode.php#103849
	private function base64url_encode($data) 
	{ 
  		return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
  	} 
  	private function base64url_decode($data) { 
  		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
  	} 

}