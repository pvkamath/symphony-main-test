<?php
/**
 * Single Signon
 *
 * This is a simplified sso plugin that allows you to transfer authentication from one server to 
 * the browser. 
 *
 * @package   FrozenSso
 * @author    Frozen Mountain <info@FrozenMountain.com>
 * @license   GPL-2.0+
 * @link      http://www.frozenmountain.com
 * @copyright 2013 Frozen Mountain Software
 *
 * @wordpress-plugin
 * Plugin Name: Frozen Mountain Single Sign-om
 * Plugin URI:  http://www.forzenmountain.com
 * Description: TODO
 * Version:     1.0.0
 * Author:      Frozen Mountain
 * Author URI:  http://www.frozenmountain.com
 * Text Domain: frzn-sso-locale
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path: /lang
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// TODO: replace `class-frzn-sso.php` with the name of the actual plugin's class file
require_once( plugin_dir_path( __FILE__ ) . 'class-frzn-sso.php' );

// Register hooks that are fired when the plugin is activated, deactivated, and uninstalled, respectively.
register_activation_hook( __FILE__, array( 'FrozenSso', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'FrozenSso', 'deactivate' ) );
FrozenSso::get_instance();