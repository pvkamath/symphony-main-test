<?php
/*
Plugin Name: JSON Data Feed
Plugin URI: http://xiik.com
Description: Creates a JSON data feed for your wordpress site.
Version: 0.1
Author: Jason Cordial
Author URI: http://xiik.com/team/jason_cordial	
License: GPL2
*/
/*  Copyright 2013  Jason Cordial  (email : jasonc@xiik.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
require_once "JSONFeedSettings.inc.php";
require_once "JSONFeeder.inc.php";

new JSONFeedSettings();
new JSONFeeder();


?>