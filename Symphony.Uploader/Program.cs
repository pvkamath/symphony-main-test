﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace Symphony.Uploader
{
    class Program
    {
        public class SymphonyWebClient : WebClient
        {
            private CookieContainer container = new CookieContainer();
            /// <summary>
            /// Time in milliseconds
            /// </summary>
            public int Timeout { get; set; }

            public SymphonyWebClient() : this(60000) { }

            public SymphonyWebClient(int timeout)
            {
                this.Timeout = timeout;
                
            }

            protected override WebRequest GetWebRequest(Uri address)
            {
                var request = base.GetWebRequest(address);
                if (request != null)
                {
                    request.Timeout = this.Timeout;
                    ((HttpWebRequest)request).CookieContainer = container;
                }
                return request;
            }
        }

        static void Main(string[] args)
        {
            SymphonyWebClient client = new SymphonyWebClient();

            string domain = "http://localhost:6397";
            string result = client.UploadString(domain + "/Handlers/LoginHandler.ashx", "customer=be&userName=admin&password=dave123!");
            if (result == "OK")
            {
                //client.UploadData(domain + "/Uploaders/
            }
            else
            {
                Console.Write(result);
            }

            Console.Read();
        }
    }
}
