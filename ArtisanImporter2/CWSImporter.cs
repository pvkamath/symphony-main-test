﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using Symphony.Core;
using Symphony.Core.Models;
using Symphony.Core.Controllers;
using Data = Symphony.Core.Data;
using System.Diagnostics;
using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;
using Symphony.Core.Extensions;

namespace ArtisanImporter2
{
    class CWSImporter : Importer
    {
        private ArtisanController artisan = new ArtisanController();
        private Dictionary<string, ArtisanAsset> assetIds = new Dictionary<string, ArtisanAsset>();
        private Dictionary<string, Data.ArtisanAsset> existingAssets = new Dictionary<string, Data.ArtisanAsset>();
        private List<ArtisanPage> pages = new System.Collections.Generic.List<ArtisanPage>();
        private Dictionary<string, string> existingCourses = new Dictionary<string, string>();
        private Dictionary<string, string> existingCourseAbbrs = new Dictionary<string, string>();
        private Dictionary<string, string> coursesToLoad = new Dictionary<string, string>();
        private Dictionary<string, int> courseVersions = new Dictionary<string, int>();
        private Dictionary<string, string> forcedCoursesDictionary = new Dictionary<string, string>(); // IF this contains course abbreviations, then ONLY these courses will be added EVEN if they exist already
        private bool reprocess = false;
        private bool status = false;
        private int processedFolders = 0;
        private int invalidFolders = 0;
        private int invalidMasterFiles = 0;
        private int totalCourses = 0;


        private int ChaptersPerCourse = 2;

        private enum ListStyleType
        {
            None, Arrow, Circle, Number, Alpha, Roman
        }

        Dictionary<string, ListStyleType> listStyles = new Dictionary<string, ListStyleType>() {
            { "arrow", ListStyleType.Arrow },
            { "dot", ListStyleType.Circle },
            { "num_", ListStyleType.Number },
            { "let_", ListStyleType.Alpha },
            { "rn_", ListStyleType.Roman }
        };

        public string Code
        {
            get { return "CWS"; } 
        }
        public string Name
        {
            get { return "Career Web Schools"; }
        }

        private string GetCourseKey(string fileName) {
            return fileName.Replace("_XML.xml", "").Replace("_EX.xml", "").Replace("_XML_p.xml", "").Replace("_EX_p.xml", "");
        }

        private bool IsCourseFolder(string folderName)
        {
            bool isCourseFolder = folderName.Contains("_XML") || folderName.Contains("_EX") || folderName.Contains("_EXPREP");
            return isCourseFolder;
        }

        private bool IsProcessed(string fileName, string folderName)
        {
            bool isProcesed = fileName.Contains("_XML_p.xml") || fileName.Contains("_EX_p.xml") || fileName.Contains(folderName + "_p.xml");

            return isProcesed;
        }

        private bool IsCourseMaster(string fileName, string folderName)
        {
            bool isProcessed = IsProcessed(fileName, folderName);
            bool isMaster = (fileName.Contains("_XML.xml") || fileName.Contains("_EX.xml") || fileName.Contains(folderName + ".xml")) || (reprocess && isProcessed);

            if (isProcessed && status)
            {
                Log.Info("Processed: " + fileName);
            }

            return isMaster;
        }

        public bool ValidateDirectory()
        {
            try
            {
                if (ConfigurationManager.AppSettings["status"].ToLower() == "true")
                {
                    status = true;
                }
                if (ConfigurationManager.AppSettings["reprocess"].ToLower() == "true")
                {
                    reprocess = true;
                }


                DirectoryInfo dir = new DirectoryInfo(Parameters.ImportPath);

                DirectoryInfo[] courseFolders = dir.GetDirectories();

                List<DirectoryInfo> validCourses = new List<DirectoryInfo>();

                foreach (DirectoryInfo courseFolder in courseFolders)
                {
                    bool validCourse = false;
                    bool validCourseFolder = false;
                    bool processed = false;

                    DirectoryInfo[] cwSubFolders = courseFolder.GetDirectories();
                    foreach (DirectoryInfo cwSubFolder in cwSubFolders)
                    {
                        if (IsCourseFolder(cwSubFolder.Name))
                        {
                            validCourseFolder = true;
                            FileInfo[] files = cwSubFolder.GetFiles();
                            foreach (FileInfo file in files)
                            {
                                if (IsCourseMaster(file.Name, courseFolder.Name))
                                {
                                    validCourses.Add(courseFolder);
                                    validCourse = true;
                                    break;
                                }
                                else if (IsProcessed(file.Name, courseFolder.Name))
                                {
                                    processed = true;
                                }
                            }
                            break;
                        }
                    }

                    if (!validCourse) {
                        // Check if it uses exam format
                        FileInfo[] possibleExamFiles = courseFolder.GetFiles();
                        foreach (FileInfo file in possibleExamFiles)
                        {
                            validCourseFolder = true;
                            if (IsCourseMaster(file.Name, courseFolder.Name))
                            {
                                validCourses.Add(courseFolder);
                                validCourse = true;
                                break;
                            }
                            else if (IsProcessed(file.Name, courseFolder.Name))
                            {
                                processed = true;
                            }
                        }
                    }

                    if (!validCourseFolder && status)
                    {
                        Log.Info("Not valid folder: " + courseFolder.Name);
                        invalidFolders++;
                    }
                    else if (validCourseFolder && !validCourse && status && !processed)
                    {
                        Log.Info("Invalid course master file: " + courseFolder.Name);
                        invalidMasterFiles++;
                    }
                    else
                    {
                        processedFolders++;
                    }
                    totalCourses++;

                }

                if (status)
                {
                    Log.Info("Total Course Folders: " + totalCourses);
                    Log.Info("Total Processed Folders: " + processedFolders);
                    Log.Info("Total Invalid Folders: " + invalidFolders);
                    Log.Info("Total Invalid master files: " + invalidMasterFiles);
                }

                if (validCourses.Count() > 0)
                {
                    Console.WriteLine("Found valid courses:");
                    foreach (DirectoryInfo course in validCourses)
                    {
                        Console.WriteLine(course.FullName);
                    }
                    Console.WriteLine(validCourses.Count() + " course(s) were found. Would you like to continue? (Y/N)");

                    var result = Console.ReadKey().Key == ConsoleKey.Y;
                    Console.WriteLine();
                    return result;
                }
            }
            catch (Exception e)
            {
                // Don't really care what it was probably just invalid path, or when calling this method
                // prior to setting a path - we do this to determine if a path is required.
            }
            Console.WriteLine("No valid courses were found.");
            return false;

        }

        private string GetExternalModuleKey(string k)
        {
            string[] splitKey = k.Split('|');
            if (splitKey.Length > 0)
            {
                k = splitKey[1];    
            }

            if (k.Contains("-"))
            {
                k = k.Split('-')[0];
            }

            return k;
        }

        private string GetAbbr(string k)
        {
            int index = k.LastIndexOf("_");
            if (index > 0)
            {
                return k.Substring(0, index);
            } 
            return k;
        }

        public void Import()
        {
            Log.Info("Import Started");
            Log.Info("Course Path: " + Parameters.ImportPath);
            Log.Info("Mode: " + Parameters.Mode);
            DateTime start = DateTime.Now;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ForcedCourseAbbreviations"]))
            {
                forcedCoursesDictionary = ConfigurationManager.AppSettings["ForcedCourseAbbreviations"].Split(',').ToDictionary(a => a.Trim());
            }

            List<Data.ArtisanCourse> existingCoursesList = new Data.ArtisanCourseCollection()
                .Where(Data.ArtisanCourse.CategoryIDColumn.ColumnName, Parameters.CategoryID)
                .Load()
                .DistinctBy(c => c.ExternalID)
                .Where(c => c.ExternalID != null)
                .ToList();

            existingCourses = existingCoursesList.ToDictionary(c => GetExternalModuleKey(c.ExternalID), e => e.ExternalID);
            existingCourseAbbrs = existingCoursesList.ToDictionary(c =>
            {
                return GetAbbr(GetExternalModuleKey(c.ExternalID));
            }, e => e.ExternalID);
                                
            DirectoryInfo dir = new DirectoryInfo(Parameters.ImportPath);

            DirectoryInfo[] courseFolders = dir.GetDirectories();

            List<DirectoryInfo> validCourses = new List<DirectoryInfo>();

            List<FileInfo> masterCourseList = new List<FileInfo>();
            List<string> coursesImporting = new List<string>();
            foreach (DirectoryInfo courseFolder in courseFolders)
            {
                DirectoryInfo[] cwSubFolders = courseFolder.GetDirectories();
                bool importedCourse = false;
                foreach (DirectoryInfo cwSubFolder in cwSubFolders)
                {
                    if (IsCourseFolder(cwSubFolder.Name))
                    {
                        FileInfo[] files = cwSubFolder.GetFiles();
                        foreach (FileInfo file in files)
                        {
                            if (IsCourseMaster(file.Name, courseFolder.Name))
                            {
                                importedCourse = true;

                                masterCourseList.Add(file);
                                coursesImporting.Add(courseFolder.Name);
                                break;
                            }
                        }
                        break;
                    }
                }

                if (!importedCourse)
                {
                    FileInfo[] files = courseFolder.GetFiles();
                    foreach (FileInfo file in files)
                    {
                        if (IsCourseMaster(file.Name, courseFolder.Name))
                        {
                            importedCourse = true;

                            masterCourseList.Add(file);
                            coursesImporting.Add(courseFolder.Name);
                            break;
                        }
                    }
                }
            }

            coursesToLoad = new Dictionary<string, string>();

            foreach (FileInfo file in masterCourseList)
            {
                BuildCourseKeyDictionary(file);    
            }

            foreach (string key in courseVersions.Keys)
            {
                string courseKey = key;

                if (key.Contains("_rev") && courseVersions[key] > 0)
                {
                    courseKey = key.Replace("_rev", "") + courseVersions[key] + "_rev";
                }
                else if (courseVersions[key] > 0)
                {
                    courseKey = key + courseVersions[key].ToString();
                }

                if (!coursesToLoad.ContainsKey(courseKey))
                {
                    coursesToLoad.Add(courseKey, courseKey);
                }
            }

            Log.Info("Importing the following coursess:");
            foreach (string c in coursesImporting)
            {
                Log.Info(c);
            }
            int count = 0;
            double totalDuration = 0;
            double averageDuration = 0;
            foreach (FileInfo file in masterCourseList)
            {
                DateTime startImport = DateTime.Now;
                ImportCourse(file);
                count++;
                Log.Info("Imported course " + count + " of " + masterCourseList.Count);
                totalDuration += (DateTime.Now - startImport).TotalMilliseconds;
                averageDuration = totalDuration / count;
                Log.Info("Average Duration: " + averageDuration);
                Log.Info("Estimated Completion Time: " + DateTime.Now.AddMilliseconds(averageDuration * (masterCourseList.Count - count)));

                if (!file.Name.Contains("_p.xml"))
                {
                    File.Move(file.FullName, Path.Combine(Path.GetDirectoryName(file.FullName), Path.GetFileNameWithoutExtension(file.Name) + "_p.xml"));
                }
            }

            Log.Info("Completed in: " + (DateTime.Now - start).TotalMilliseconds);
        }

        private void BuildCourseKeyDictionary(FileInfo file)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(file.FullName);
            }
            catch (Exception e)
            {
                Log.Info("Could load course XML: " + file.Name);
                Log.Error(e.ToString());
            }

            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/course/chapter/module");

            foreach (XmlNode n in nodes)
            {
                KeyValuePair<string, int> abbrVerPair = GetCourseAbbrVerPair(n.Attributes["abbr"].InnerText);

                if (courseVersions.ContainsKey(abbrVerPair.Key))
                {
                    if (courseVersions[abbrVerPair.Key] < abbrVerPair.Value)
                    {
                        courseVersions[abbrVerPair.Key] = abbrVerPair.Value;
                    }
                }
                else
                {
                    courseVersions.Add(abbrVerPair.Key, abbrVerPair.Value);
                }
            }
        }

        private KeyValuePair<string, int> GetCourseAbbrVerPair(string abbr) {
            Regex e = new Regex("([0-9]+)_(.+)");

            MatchCollection matches = e.Matches(abbr);

            int version = 0;
            
            if (matches.Count > 0)
            {
                string versionString = matches[0].Groups[1].ToString();
                bool rev = false;
                if (abbr.Contains("_rev_"))
                {
                    rev = true;
                }
                
                abbr = abbr.Substring(0, abbr.IndexOf(versionString + "_"));

                if (rev)
                {
                    abbr += "_rev";
                }

                int.TryParse(versionString, out version);
            }
            else
            {
                abbr = GetAbbr(abbr);
            }

            return new KeyValuePair<string, int>(abbr, version);
        }

        private ArtisanCourse CreateArtisanCourse(int chapterCount, XmlNode chapterNode, XmlNode courseNode)
        {
            string title = courseNode.Attributes["name"].InnerText + " - " + chapterCount + ". " + chapterNode.Attributes["name"].InnerText;

            ArtisanCourse course = new ArtisanCourse()
            {
                Name = title,
                ExternalID = courseNode.Attributes["abbr"].InnerText + "-" + courseNode.Attributes["inst_id"].InnerText,
                Keywords = courseNode.Attributes["abbr"].InnerText + ", " + courseNode.Attributes["inst_id"].InnerText,
                NavigationMethod = ArtisanCourseNavigationMethod.FullSequential,
                MarkingMethod = ArtisanCourseMarkingMethod.ShowAnswers,
                ReviewMethod = ArtisanCourseReviewMethod.HideReview,
                CompletionMethod = ArtisanCourseCompletionMethod.Navigation,
                InactivityTimeout = 900,
                TimeoutMethod = ArtisanCourseTimeoutMethod.ShowAlert,
                CertificateEnabled = false,
                ThemeId = 27,
                Theme = new ArtisanTheme(),
                CategoryId = Parameters.CategoryID,
                Objectives = "",
                Parameters = "{}",
                Sections = new List<ArtisanSection>(),
                PassingScore = 80
            };

            return course;
        }

        private void ImportCourse(FileInfo courseXML)
        {
            string courseAssetKey = "cws_import_" + GetCourseKey(courseXML.Name);

            existingAssets = SubSonic.Select.AllColumnsFrom<Data.ArtisanAsset>()
                .Where(Data.ArtisanAsset.ExternalIDColumn).ContainsString(courseAssetKey)
                .ExecuteTypedList<Data.ArtisanAsset>()
                .DistinctBy(a => a.ExternalID)
                .ToDictionary(a => a.ExternalID);

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(courseXML.FullName);
            }
            catch (Exception e)
            {
                Log.Info("Could not load course XML: " + courseXML.Name);
                Log.Error(e.ToString());
                return;
            }

            List<ArtisanAsset> pdfMenu = new List<ArtisanAsset>();
            assetIds = new Dictionary<string, ArtisanAsset>();

            try
            {
                ImportAssets(courseXML);
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                return;
            }
            XmlNodeList nodes = doc.DocumentElement.SelectNodes("/course");
            
            foreach (XmlNode courseNode in nodes)
            {
                List<ArtisanCourse> courses = new List<ArtisanCourse>();

                pages = new System.Collections.Generic.List<ArtisanPage>();
                int chapterCount = 1;
                List<XmlNode> pdfNodes = courseNode.ChildNodes.Cast<XmlNode>().Where(n => n.Name == "pdf").ToList();

                foreach (XmlNode pdfNode in pdfNodes)
                {
                    ArtisanAsset asset = ImportPDF(pdfNode, courseXML);

                    pdfMenu.Add(asset);

                    
                }

                foreach (XmlNode childNode in courseNode.ChildNodes)
                {
                    switch (childNode.Name) {
                        case "chapter":
                            courses.Add(CreateArtisanCourse(chapterCount, childNode, courseNode));
                            chapterCount++;
                            ImportChapter(courses[courses.Count() - 1], childNode, courseXML);
                            
                            break;
                        case "pdf":
                            continue;
                        default:
                            Log.Error("Unsuported course node type: " + childNode.Name);
                            continue;
                    }
                }

                if (pdfMenu.Count > 0)
                {
                    string pdfLinkList = "";

                    foreach (ArtisanAsset asset in pdfMenu)
                    {
                        pdfLinkList += string.Format("<li>{0}</li>", GetAssetTemplate(asset));
                    }

                    courses.ForEach(c => c.PopupMenu = string.Format("<div class='pdf-menu' data-title='Reference Library'><ul>{0}</ul></div>", pdfLinkList));
                }

                int count = 1;
                foreach (ArtisanCourse course in courses)
                {
                    try
                    {
                        Log.Info("Splitting Course " + courseXML.Name + " " + course.Name + " into multiple courses...");

                        List<ArtisanCourse> subCourses = new List<ArtisanCourse>();

                        foreach (ArtisanSection s in course.Sections)
                        {
                            if (existingCourses.ContainsKey(s.ExternalID.Replace("|","")))
                            {
                                Log.Info("The exact course " + s.Name + "-" + s.ExternalID + " already exists in the current category. Not importing.");
                                continue;
                            }

                            if (existingCourseAbbrs.ContainsKey(GetAbbr(s.ExternalID.Replace("|","")))){
                                Log.Info("A version of course: " + s.Name + " - " + s.ExternalID + " already exists.");
                            }
                            

                            // One section per course
                            ArtisanCourse subCourse = new ArtisanCourse();

                            subCourse.ReviewMethod = ArtisanCourseReviewMethod.HideReview;

                            subCourse.CopyFrom(course);
                            subCourse.Sections = new List<ArtisanSection>() {
                                s
                            };

                            subCourse.ExternalID += "-" + s.ExternalID;
                            subCourse.Keywords += ", " + s.ExternalID.Replace("-", ", ");
                            subCourse.Keywords += ", " + subCourse.Name + ", " + s.Name;
                            subCourse.Name = s.Name;

                            subCourses.Add(subCourse);

                            foreach (ArtisanSection ss in subCourse.Sections) {
                                if (ss.Name.Contains("Post-Course Assessment") ||
                                    ss.Name.Contains("Pre-Course Assessment") ||
                                    ss.Name.Contains("Exam") ||
                                    ss.SectionType == (int)ArtisanSectionType.Posttest ||
                                    ss.SectionType == (int)ArtisanSectionType.Pretest)
                                {
                                    ss.Pages = ss.Sections[0].Pages;
                                    ss.Sections = new List<ArtisanSection>();

                                    if (ss.Name.Contains("Post-Course Assessment") ||
                                        ss.SectionType == (int)ArtisanSectionType.Posttest ||
                                        ss.Name.Contains("Exam"))
                                    {
                                        ss.SectionType = (int)ArtisanSectionType.Posttest;
                                    }
                                    else
                                    {
                                        ss.SectionType = (int)ArtisanSectionType.Pretest;
                                    }

                                    subCourse.CompletionMethod = ArtisanCourseCompletionMethod.Test;
                                    subCourse.ReviewMethod = ArtisanCourseReviewMethod.ShowReview;

                                    if (ss.Pages[0].PageType == (int)ArtisanPageType.Content) {
                                        ss.Description = Util.ConvertHtmlToDescription(ss.Pages[0].Html);
                                        ss.Pages.RemoveAt(0);
                                    }
                                }
                                else
                                {
                                    if (ss.Name.Contains("Quiz"))
                                    {
                                        ss.Sections[0].IsQuiz = true;
                                        ss.Sections[0].Name = ss.Name;
                                    }
                                    else
                                    {
                                        ArtisanSection currentSection = null;
                                        List<ArtisanSection> mergedSections = new List<ArtisanSection>();

                                        foreach (ArtisanSection lo in ss.Sections)
                                        {
                                            if (currentSection == null)
                                            {
                                                currentSection = lo;
                                                mergedSections.Add(currentSection);
                                            }
                                            else
                                            {
                                                if (currentSection.Name == lo.Name)
                                                {
                                                    currentSection.Pages.AddRange(lo.Pages);
                                                }
                                                else
                                                {
                                                    currentSection = lo;
                                                    mergedSections.Add(currentSection);
                                                }
                                            }
                                        }

                                        ss.Sections = mergedSections;
                                    }


                                    foreach (ArtisanSection lo in ss.Sections)
                                    {
                                        if (lo.IsQuiz)
                                        {
                                            if (lo.Pages[0].PageType == (int)ArtisanPageType.Content)
                                            {
                                                lo.Description = Util.ConvertHtmlToDescription(lo.Pages[0].Html);
                                                lo.Pages.RemoveAt(0);
                                            }
                                        }

                                        lo.IsRandomizeAnswers = true;
                                    }
                                }
                            }
                        }

                        foreach (ArtisanCourse sCourse in subCourses)
                        {
                            Log.Info("Saving Course " + courseXML.Name + " " + sCourse.Name);

                            string courseKey = GetExternalModuleKey(sCourse.ExternalID);
                            string courseAbbr = GetAbbr(courseKey);

                            bool isForced = false;

                            if (forcedCoursesDictionary.Keys.Count() > 0)
                            {
                                if (forcedCoursesDictionary.ContainsKey(courseAbbr))
                                {
                                    isForced = true;
                                    forcedCoursesDictionary.Remove(courseAbbr); // So we don't import a forced course more than once in a batch
                                }
                                else
                                {
                                    Log.Info("FORCED LIST ACTIVE - Skipping " + courseAbbr + " in " + courseXML.Name + " since it is not in the forced courses list.");
                                    continue;
                                }
                            }

                            if (!existingCourseAbbrs.ContainsKey(courseAbbr) || isForced)
                            {
                                if (!coursesToLoad.ContainsKey(courseAbbr))
                                {
                                    Log.Info("VERSIONDUPLICATE - newer version exists, ADDING ANYWAY! Course Name: " + courseXML.Name + " Sub Course: " + sCourse.Name + " Abbr: " + courseAbbr);
                                }

                                if (isForced)
                                {
                                    Log.Info("FORCEDSAVE - newer version may exist or course might exist in another category - Abbr: " + courseAbbr);
                                }

                                if (!existingCourseAbbrs.ContainsKey(courseAbbr))
                                {
                                    existingCourseAbbrs.Add(courseAbbr, courseAbbr);
                                }
                                if (!existingCourses.ContainsKey(courseKey))
                                {
                                    existingCourses.Add(courseKey, courseKey);
                                }

                                if (sCourse.Name.ToLower().Contains("exam") ||
                                    (sCourse.Sections.Count() == 1 && sCourse.Sections[0].SectionType == (int)ArtisanSectionType.Posttest))
                                {
                                    Log.Info("The Course: " + sCourse.Name + " (External ID: " + sCourse.ExternalID + " Keywords: " + sCourse.Keywords + ") was detected as an Exam. Setting default exam properties.");

                                    for (var sId = 0; sId < sCourse.Sections.Count(); sId++)
                                    {
                                        if (sCourse.Sections[sId].SectionType == (int)ArtisanSectionType.Posttest)
                                        {
                                            sCourse.Sections[sId].TestType = ArtisanTestType.Random;
                                            sCourse.PassingScore = 70;
                                            sCourse.CompletionMethod = ArtisanCourseCompletionMethod.TestForceRestart;
                                            sCourse.NavigationMethod = ArtisanCourseNavigationMethod.FreeForm;
                                            sCourse.CertificateEnabled = false;
                                            sCourse.InactivityTimeout = 0;
                                            sCourse.TimeoutMethod = ArtisanCourseTimeoutMethod.None;
                                            sCourse.MarkingMethod = ArtisanCourseMarkingMethod.HideAnswersAndFeedback;
                                            sCourse.ReviewMethod = ArtisanCourseReviewMethod.HideReview;
                                        }
                                    }
                                }

                                try
                                {
                                    artisan.SaveCourse(SymphonyLogin.CustomerID, SymphonyLogin.UserID, 0, sCourse);
                                }
                                catch (Exception e)
                                {
                                    Log.Error("SAVE FAILED Abbr: " + courseAbbr);
                                    Log.Error(e.Message);
                                }
                            }
                            else

                            {
                                    Log.Info("COURSEEXISTS: Not saving. Course Name: " + courseXML.Name + " Sub Course: " + sCourse.Name + " Abbr: " + courseAbbr);
                            }

                            
                        }
                        count++;
                    }
                    catch (Exception e)
                    {
                        Log.Info("Could not save course: " + course.Name + " " + course.ExternalID + " (" + courseXML.Name + ")");
                        Log.Error(e);
                    }
                }
            }
        }


        private void ImportChapter(ArtisanCourse course, XmlNode chapter, FileInfo courseXML)
        {
            Log.Info("Importing chapter");
           /* ArtisanSection section = new ArtisanSection()
            {
                Name = Util.ToTitleCase(chapter.Attributes["name"].InnerText),
                ExternalID = chapter.Attributes["abbr"].InnerText + "-" + chapter.Attributes["inst_id"],
                Sections = new List<ArtisanSection>(),
                Pages = new List<ArtisanPage>(),
                Objectives = "",
                Description = "",
                SectionType = (int)ArtisanSectionType.Sco
            };*/

            XmlNodeList moduleNodes = chapter.ChildNodes;

            foreach (XmlNode module in moduleNodes)
            {
                Log.Info("Importing module");
                ArtisanSection section = new ArtisanSection()
                {
                    Name = Util.ToTitleCase(module.Attributes["name"].InnerText),
                    ExternalID = "|" + module.Attributes["abbr"].InnerText + "-" + module.Attributes["inst_id"].InnerText + "|",
                    Pages = new List<ArtisanPage>(),
                    Sections = new List<ArtisanSection>(),
                    Objectives = "",
                    Description = "",
                    SectionType = (int)ArtisanSectionType.Sco
                };

                if (module.Attributes["exam_type"] != null)
                {
                    section.SectionType = (int)ArtisanSectionType.Posttest;
                }

                /*if (learningObject.Name.ToLower().Contains("pre-course") ||
                    learningObject.Name.ToLower().Contains("quiz") ||
                    learningObject.Name.ToLower().Contains("post-course"))
                {
                    if (learningObject.Name.ToLower().Contains("pre-course"))
                    {
                        learningObject.SectionType = (int)ArtisanSectionType.Pretest;
                    }
                    else if (learningObject.Name.ToLower().Contains("post-course"))
                    {
                        learningObject.SectionType = (int)ArtisanSectionType.Posttest;
                    }
                    else
                    {
                        learningObject.IsQuiz = true;
                    }
                    
                    int questions;
                    if (int.TryParse(module.Attributes["questions_override"].InnerText, out questions))
                    {
                        learningObject.MaxQuestions = questions;
                    }
                }*/


                XmlDocument moduleDoc = new XmlDocument();
                string moduleXML = string.Format("{0}_{1}.xml",
                    Path.GetFileNameWithoutExtension(courseXML.Name.Replace("_p.xml", ".xml")),
                    module.Attributes["abbr"].InnerText);

                moduleDoc.Load(Path.Combine(Path.GetDirectoryName(courseXML.FullName), moduleXML));

                XmlNode moduleNode = moduleDoc.DocumentElement.SelectSingleNode("/course/module");
                if (moduleNode.Attributes["exam_type"] != null)
                {
                    section.SectionType = (int)ArtisanSectionType.Posttest;
                }

                XmlNodeList storyboards = moduleDoc.DocumentElement.SelectNodes("/course/module/storyboard");
                
                foreach (XmlNode storyboard in storyboards)
                {
                    List<XmlNode> pageNodes = storyboard.ChildNodes.Cast<XmlNode>().Where(n => n.Name == "page").ToList();

                    ArtisanSection learningObject = new ArtisanSection()
                    {
                        Name = "",
                        ExternalID = storyboard.Attributes["abbr"].InnerText + "-" + module.Attributes["abbr"].InnerText + "-" + module.Attributes["inst_id"].InnerText,
                        Pages = new List<ArtisanPage>(),
                        Sections = new List<ArtisanSection>(),
                        Objectives = "",
                        Description = "",
                        SectionType = (int)ArtisanSectionType.LearningObject
                    };

                    XmlNode firstPageXmlNode = pageNodes.FirstOrDefault();
                    if (firstPageXmlNode != null)
                    {
                        string abbr = firstPageXmlNode.Attributes["abbr"] != null ? firstPageXmlNode.Attributes["abbr"].InnerText : "";
                        string type = firstPageXmlNode.Attributes["pqtype"] != null ? firstPageXmlNode.Attributes["pqtype"].InnerText : "";

                        if (abbr.Contains("fdbk_p") || type.Contains("Feedback")) {
                            
                            //Log.Info("Skipping storyboard: " + storyboard.Attributes["abbr"].InnerText + " of module " + module.Attributes["abbr"].InnerText + " - Was detected as a feedback section");
                            //continue;
                            Log.Info("Adding a feedback section - should probably be removed : " + storyboard.Attributes["abbr"].InnerText + " of module " + module.Attributes["abbr"].InnerText + " - Was detected as a feedback section");
                            
                        }
                    }

                    bool gradablePage = false;

                    foreach (XmlNode page in pageNodes)
                    {
                        string pageXML = page.InnerText;
                        string pageType = page.Attributes["pqtype"].InnerText;
                        string pageAbbr = page.Attributes["abbr"].InnerText;

                        if (pageType.Contains("Feedback"))
                        {
                            pageType = "normal";
                            //Log.Info("Skipping page: " + pageAbbr + " of storyboard " + storyboard.Attributes["abbr"].InnerText + " of module " + module.Attributes["abbr"].InnerText + " - Was detected as feedback page");
                            //continue;
                            Log.Info("Adding feedback page. Should probably be removed: " + pageAbbr + " of storyboard " + storyboard.Attributes["abbr"].InnerText + " of module " + module.Attributes["abbr"].InnerText + " - Was detected as feedback page");
                            
                        }

                        if (pageType == "Test")
                        {
                            pageType = "teaching";
                        }

                        XmlDocument pageDoc = new XmlDocument();
                        string pagePath = Path.Combine(Path.GetDirectoryName(courseXML.FullName), "..", pageXML);

                        if (!File.Exists(pagePath))
                        {
                            
                            Log.Error("Missing Page File: " + pagePath);
                            
                            pagePath = Path.Combine(Path.GetDirectoryName(courseXML.FullName), "../..", "standard/standard_placeholder.xml");

                            if (!File.Exists(pagePath))
                            {
                                pagePath = "C:\\Working\\Error\\error.xml";
                                Log.Error("The following page could not be found, and was not found in the standard pages: " + Path.Combine(Path.GetDirectoryName(courseXML.FullName), "..", pageXML) + " Course Name: " + courseXML.FullName + " Module: " + module.Attributes["name"].InnerText);
                            }
                        }

                        if (!File.Exists(pagePath))
                        {
                            Log.Error("Could not load error page for missing page file: " + Path.Combine(Path.GetDirectoryName(courseXML.FullName), "..", pageXML) + " Course Name: " + courseXML.FullName + " Module: " + module.Attributes["name"].InnerText);
                            continue;
                        }

                        pageDoc.Load(pagePath);

                        ArtisanPage artisanPage = new ArtisanPage()
                        {
                            ExternalID = pageAbbr
                        };
                        bool isExternalPlayer = false;
                        XmlNode pageNode = pageDoc.SelectSingleNode("/page");
                        try
                        {
                            string externalPlayer = pageNode.Attributes["external_player"].InnerText;
                            if (!string.IsNullOrWhiteSpace(externalPlayer))
                            {
                                isExternalPlayer = true;
                                artisanPage.MinimumPageTime = 30;
                            }
                        }
                        catch (Exception e)
                        {
                            // External player attribute didn't exist so leave it as false
                        }

                        XmlNodeList presentationItems = pageDoc.SelectNodes("/page/Presentation_Item");
                        XmlNode header1 = pageDoc.SelectSingleNode("/page/header1");
                        XmlNode header2 = pageDoc.SelectSingleNode("/page/header2");
                        XmlNode header3 = pageDoc.SelectSingleNode("/page/header3");
                        XmlNode header4 = pageDoc.SelectSingleNode("/page/header4");
                        XmlNodeList quickPdfs = pageDoc.SelectNodes("/page/pdf");
                        // In general header1 is the section header, header 2 is the page header.
                        // In some cases, there is no header 2 set, so in that case we will choose 
                        // header1
                        string pageHeader = header2 != null ? Util.ToTitleCase(header2.Attributes["text"].InnerText) : "";

                        if (string.IsNullOrWhiteSpace(pageHeader))
                        {
                            XmlNode header = header1 ?? header3 ?? header4;
                            if (header != null)
                            {
                                pageHeader = Util.ToTitleCase(header.Attributes["text"].InnerText);
                            }
                        }


                        artisanPage.Name = string.IsNullOrWhiteSpace(pageHeader) ? pageAbbr : pageHeader + " (" + pageAbbr + ")";

                        if (string.IsNullOrEmpty(learningObject.Name))
                        {
                            learningObject.Name = string.IsNullOrWhiteSpace(pageHeader) ? pageAbbr : pageHeader;
                        }

                        List<XmlNode> orderedPresentationItems = presentationItems.Cast<XmlNode>().OrderBy(n => int.Parse(n.Attributes["yPos"].InnerText)).ToList();
                        int lastXValue = 0;
                        int level = 0;
                        string presentationContent = "";

                        bool hasText = false;
                        int swfCount = 0;

                        foreach (XmlNode presentationItem in orderedPresentationItems)
                        {
                            List<XmlNode> assetNodes = presentationItem.ChildNodes.Cast<XmlNode>().Where(n =>
                                !string.IsNullOrWhiteSpace(n.Attributes["path"].InnerText) &&
                                !n.Attributes["path"].InnerText.Contains("\\bullets\\")).ToList();

                            if (!string.IsNullOrWhiteSpace(presentationItem.Attributes["text"].InnerText))
                            {
                                int xPos = int.Parse(presentationItem.Attributes["xPos"].InnerText);

                                presentationContent += AppendListHTML(xPos, presentationItem, ref level, ref lastXValue);

                                string tag = level > 1 ? "li" : "p";

                                presentationContent += string.Format("<{0}>{1}</{0}>",
                                                            tag, presentationItem.Attributes["text"].InnerText);

                                hasText = true;
                            }

                            foreach (XmlNode node in assetNodes)
                            {
                                int xPos = int.Parse(node.Attributes["xPos"].InnerText);
                                presentationContent += AppendListHTML(xPos, presentationItem, ref level, ref lastXValue);
                                string tag = level > 1 ? "li" : "p";

                                presentationContent += string.Format("<{0}>{1}</{0}>",
                                                            tag, GetAssetTemplate(node, pagePath));
                                swfCount++;
                            }
                        }

                        if (level > 1)
                        {
                            presentationContent += string.Concat(Enumerable.Repeat("</ul>", level - 1));
                        }

                        string quickLinksList = "";

                        foreach (XmlNode quickPdfNode in quickPdfs)
                        {
                            string fileName = quickPdfNode.Attributes["fname"].InnerText;
                            string pdfName = quickPdfNode.Attributes["name"].InnerText;
                            string[] fileNameParts = fileName.Split('#');
                            string fileNameWithoutAnchor = fileNameParts[0];

                            string path = Path.Combine(Path.GetDirectoryName(courseXML.FullName), "pdf", fileNameWithoutAnchor);
                            if (assetIds.ContainsKey(path)) {
                                ArtisanAsset pdfAsset = assetIds[path];
                                string oldName = pdfAsset.Name;
                                string oldPath = pdfAsset.Path;
                                pdfAsset.Name += " - " + pdfName;
                                pdfAsset.Path += "#" + fileNameParts[1];
                                quickLinksList += "<li>" + GetAssetTemplate(pdfAsset) + "</li>";
                                pdfAsset.Name = oldName;
                                pdfAsset.Path = oldPath;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(quickLinksList))
                        {
                            quickLinksList = "<h5>Quick Links</h5><ul>" + quickLinksList + "</ul>";
                        }

                        switch (pageType.ToLower())
                        {
                            case "normal":

                                if (!isExternalPlayer)
                                {
                                    if (header4 != null && !string.IsNullOrWhiteSpace(header4.Attributes["text"].InnerText))
                                    {
                                        presentationContent = "<h5>" + header4.Attributes["text"].InnerText + "</h5>" + presentationContent;
                                    }
                                    if (header3 != null && !string.IsNullOrWhiteSpace(header3.Attributes["text"].InnerText))
                                    {
                                        presentationContent = "<h4>" + header3.Attributes["text"].InnerText + "</h4>" + presentationContent;
                                    }
                                }

                                if (isExternalPlayer)
                                {
                                    // We need to do something better than this
                                    // This will just indicate how to progress through the
                                    // slide contents but will not stop the student from 
                                    // directly skipping the page.
                                    presentationContent += "<p class='swf-direction'>Click the slide to advance the presentation.</p>";
                                }

                                if (!string.IsNullOrWhiteSpace(quickLinksList))
                                {
                                    presentationContent += quickLinksList;
                                }

                                artisanPage.PageType = (int)ArtisanPageType.Content;
                                artisanPage.Html = Util.GetDefaultHtml(pageHeader, presentationContent);
                                artisanPage.TemplateId = Util.GetDefaultTemplateId();
                                
                                

                                break;
                            case "teaching":
                                artisanPage.PageType = (int)ArtisanPageType.Question;
                                artisanPage.QuestionType = ArtisanQuestionType.MultipleChoice;
                                artisanPage.TemplateId = 34;
                                artisanPage.CorrectResponse = "Correct";
                                artisanPage.IncorrectResponse = "Incorrect";
                                artisanPage.IsGradable = true;

                                gradablePage = true;

                                XmlNodeList questionNodes = pageDoc.SelectNodes("/page/Question_Item");
                                foreach (XmlNode questionNode in questionNodes)
                                {
                                    if (!string.IsNullOrWhiteSpace(questionNode.Attributes["text"].InnerText))
                                    {
                                        artisanPage.Html += !string.IsNullOrWhiteSpace(artisanPage.Html) ? "<br/>" : "";
                                        artisanPage.Html += questionNode.Attributes["text"].InnerText;
                                    }
                                    foreach (XmlNode assetNode in questionNode.ChildNodes.Cast<XmlNode>().Where(n => !string.IsNullOrWhiteSpace(n.Attributes["path"].InnerText)))
                                    {
                                        artisanPage.Html += !string.IsNullOrWhiteSpace(artisanPage.Html) ? "<br/>" : "";
                                        artisanPage.Html += GetAssetTemplate(assetNode, pagePath);
                                    }
                                }


                                if (!string.IsNullOrWhiteSpace(quickLinksList))
                                {
                                    artisanPage.Html += quickLinksList;
                                }

                                XmlNodeList optionNodes = pageDoc.SelectNodes("/page/option");

                                artisanPage.Answers = new List<ArtisanAnswer>();
                                int sort = 0;
                                foreach (XmlNode optionNode in optionNodes)
                                {
                                    ArtisanAnswer answer = new ArtisanAnswer()
                                    {
                                        IsCorrect = Convert.ToBoolean(int.Parse(optionNode.Attributes["correct"].InnerText)),
                                        Text = optionNode.Attributes["text"].InnerText,
                                        Sort = sort,
                                        Feedback = ""
                                    };

                                    artisanPage.Answers.Add(answer);

                                    foreach (XmlNode possibleExplaination in optionNode.ChildNodes)
                                    {
                                        if (possibleExplaination.Name == "explaination")
                                        {
                                            if (!string.IsNullOrWhiteSpace(possibleExplaination.Attributes["text"].InnerText)
                                                && !artisanPage.IncorrectResponse.Contains(possibleExplaination.Attributes["text"].InnerText))
                                            {
                                                answer.Feedback += (!string.IsNullOrWhiteSpace(answer.Feedback) ? "<br/>" : "") + possibleExplaination.Attributes["text"].InnerText;
                                            }

                                            foreach (XmlNode assetNode in possibleExplaination.ChildNodes)
                                            {
                                                if (assetNode.Name == "image_swf" && !string.IsNullOrWhiteSpace(assetNode.Attributes["path"].InnerText))
                                                {
                                                    string assetTemplate = GetAssetTemplate(assetNode, pagePath);
                                                    if (!artisanPage.IncorrectResponse.Contains(assetTemplate))
                                                    {
                                                        answer.Feedback += (!string.IsNullOrWhiteSpace(answer.Feedback) ? "<br/>" : "") + assetTemplate;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (string.IsNullOrWhiteSpace(artisanPage.IncorrectResponse)) artisanPage.IncorrectResponse = "Incorrect";

                                    sort++;
                                }

                                break;
                            default:
                                Log.Error("Error: Unknown page type - " + pageType + " - need to add handling for this case.");
                                Log.Error(pagePath);
                                continue;
                        }
                        pages.Add(artisanPage);

                        learningObject.Pages.Add(artisanPage);
                    }

                    if (learningObject.Pages.Count() > 0 && !string.IsNullOrEmpty(learningObject.Name))
                    {
                        if (learningObject.SectionType == (int)ArtisanSectionType.Pretest ||
                            learningObject.SectionType == (int)ArtisanSectionType.Posttest)
                        {
                            section = learningObject;
                            course.CompletionMethod = ArtisanCourseCompletionMethod.Test;
                        }
                        else
                        {
                            if (learningObject.IsQuiz)
                            {
                                if (learningObject.Pages.Count > 0 && learningObject.Pages[0].PageType == (int)ArtisanPageType.Content)
                                {
                                    ArtisanPage firstPage = learningObject.Pages[0];

                                    learningObject.Objectives = Regex.Replace(
                                                                    Regex.Replace(
                                                                        Regex.Replace(firstPage.Html, "<h3.*/h3>", " "),
                                                                    "<.*?>", " "),
                                                                "\\s{2,}", " ");

                                    learningObject.Description = learningObject.Objectives;

                                    learningObject.Pages.RemoveAt(0);
                                }
                            }
                            else if (gradablePage)
                            {
                                course.CompletionMethod = ArtisanCourseCompletionMethod.InlineQuestions;
                            }

                            section.Sections.Add(learningObject);
                        }
                    }
                }
                course.Sections.Add(section);
            }
        }

        private string AppendListHTML(int xPos, XmlNode node, ref int level, ref int lastXValue)
        {
            string returnTag = "";

            if (xPos > lastXValue)
            {
                level++;
                if (level > 1)
                {
                    List<XmlNode> innerNodes = node.ChildNodes.Cast<XmlNode>().Where(n =>
                        n.Name == "image_swf"
                        && !string.IsNullOrWhiteSpace(n.Attributes["path"].InnerText) &&
                        n.Attributes["path"].InnerText.Contains("\\bullets\\")).ToList();

                    ListStyleType listStyleType = ListStyleType.None;

                    if (innerNodes.Count() > 0)
                    {
                        XmlNode n = innerNodes[0];
                        string imageUrl = n.Attributes["path"].InnerText;
                        
                        foreach (string key in listStyles.Keys)
                        {
                            if (imageUrl.Contains(key))
                            {
                                listStyleType = listStyles[key];
                                break;
                            }
                        }
                    }

                    string listClass = "list-style-" + listStyleType.ToString();
                    
                    returnTag = string.Format("<ul class='{0}'>", listClass);
                }
            }
            else if (xPos < lastXValue)
            {
                level--;
                if (level >= 1)
                {
                    returnTag = "</ul>";
                }
            }

            lastXValue = xPos;

            return returnTag;
        }

        private string GetAssetTemplate(ArtisanAsset asset) {
            
            Regex styleTpl = new Regex("style=\"<.*>\"");
            string style = string.Format("style=\"{0} {1}\"",
                (asset.Width.HasValue ?
                "width: " + asset.Width.Value + "px;" :
                "max-width: 100%"),
                (asset.Height.HasValue ?
                "height: " + asset.Height.Value + "px;" :
                "max-height: 100%"));

            if (asset.AssetTypeId == (int)Symphony.Core.ArtisanAssetType.Flash)
            {
                SetSWFWidthHeight(asset);
            }

            return styleTpl.Replace(asset.Template, style)
                .Replace("{path}", asset.Path)
                .Replace("{description}", asset.Description)
                .Replace("{name}", asset.Name)
                .Replace("{element_id}", asset.Id + asset.Name)
                .Replace("{width}", asset.Width.ToString())
                .Replace("{height}", asset.Height.ToString())
                .Replace("{id}", asset.Id.ToString())
                .Replace("width=\"100%\"", "width=\"" + asset.Width + "\"")
                .Replace("height=\"100%\"", "height=\"" + asset.Height + "\"")
                .Replace("width=\"850px\"", "width=\"" + asset.Width + "\"")
                .Replace("height=\"700px\"", "height=\"" + asset.Height + "\""); ;
        
        }

        private string GetAssetTemplate(XmlNode assetNode, string pagePath)
        {
            string pageFolder = Path.GetDirectoryName(pagePath);
            string filePath = Path.Combine(pageFolder, assetNode.Attributes["path"].InnerText);
            string fileName = Path.GetFileName(assetNode.Attributes["path"].InnerText);

            filePath = Path.GetFullPath((new Uri(filePath).LocalPath));


            if (!assetIds.ContainsKey(filePath))
            {
                Log.Error("Could not generate image tag for " + filePath);
                return "";
            }

            ArtisanAsset asset = assetIds[filePath];

            return GetAssetTemplate(asset);
        }


        private ArtisanAsset ImportPDF(XmlNode pdf, FileInfo courseXML)
        {
            Log.Info("Importing pdf");
            string fileName = pdf.Attributes["fname"].InnerText;
            string path = Path.Combine(Path.GetDirectoryName(courseXML.FullName), "pdf", fileName);
            string pdfName = pdf.Attributes["name"].InnerText;

            string assetKey = "cws_import_" + GetCourseKey(courseXML.Name) + "_" + fileName;

            return UploadAsset(path, pdfName, assetKey);
        }

        private ArtisanAsset UploadAsset(string path, string name, string id)
        {
            path = Path.GetFullPath((new Uri(path).LocalPath));

            if (!File.Exists(path))
            {
                Log.Error("Could not upload file: " + path);
                return null;
            }

            string fileName = Path.GetFileName(path);

            if (assetIds.ContainsKey(path)) {
                Log.Error("File already exists: " + path);
                return assetIds[path];
            }

            if (existingAssets.ContainsKey(id))
            {
                ArtisanAsset existingAsset = new ArtisanAsset();
                existingAsset.CopyFrom(existingAssets[id]);
                
                Data.ArtisanAssetType assetType = new Data.ArtisanAssetType(existingAsset.AssetTypeId);
                artisan.AugmentAsset(existingAsset, assetType);

                assetIds.Add(path, existingAsset);
                return assetIds[path];
            }

            byte[] bytes = File.ReadAllBytes(path);

            ArtisanAsset asset = artisan.UploadArtisanAsset(SymphonyLogin.CustomerID, Path.GetFileName(path), name, "", bytes,  ConfigurationManager.AppSettings["appPath"], id).Data;

            assetIds.Add(path, asset);

            return asset;
        }

        private void ImportAssets(FileInfo courseXML)
        {
            string fileList = Path.GetFileNameWithoutExtension(courseXML.FullName.Replace("_p.xml", ".xml")) + "_filelist.xml";
            string coursePath = Path.GetDirectoryName(courseXML.FullName);
            string fileListPath = Path.Combine(coursePath, fileList);

            if (!File.Exists(fileListPath))
            {
                throw new Exception("Could not load file list. File not found. " + fileListPath);
            }

            XmlDocument fileListDoc = new XmlDocument();
            fileListDoc.Load(fileListPath);

            foreach (XmlNode file in fileListDoc.DocumentElement.SelectNodes("/course/asset/file"))
            {
                if (file.InnerText.Contains("\\bullets\\"))
                {
                    // Ignore bullets directory
                    continue;
                }

                string assetPath = Path.Combine(coursePath, "..", file.InnerText);

                Log.Info("Importing Asset: " + assetPath);

                if (!File.Exists(assetPath))
                {
                    Log.Error("Could not load file: " + assetPath + " file not found");
                    continue;
                }

                //if (Path.GetExtension(assetPath) == ".swf")
                //{
                //    assetPath = convertSWFToPNG(assetPath);
                //    if (!File.Exists(assetPath)) {
                //        Console.WriteLine("Could not convert to swf");
                //    }
                //}

                string[] splitPath = assetPath.Split(new string[]{"\\"}, StringSplitOptions.None);

                string fileName = Path.GetFileName(assetPath);
                string moduleKey = splitPath.Count() >= 2 ? splitPath[1] : "unkn";
                string courseKey = GetCourseKey(courseXML.Name);
                string assetKey = "cws_import_" + courseKey + "_" + moduleKey + "_" + fileName;

                try
                {
                    Log.Info("Uploading Asset");
                    UploadAsset(assetPath, Path.GetFileNameWithoutExtension(assetPath), assetKey);
                }
                catch (Exception e)
                {
                    Log.Error("Could not upload asset." + assetPath + " " + e);
                }
            }
        }

        private void SetSWFWidthHeight(ArtisanAsset asset)
        {
            try
            {
                string appPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string swfDumpPath = Path.Combine(Path.GetDirectoryName(appPath), "SWFTools", "swfdump.exe");
                string swfPath = Path.Combine(ConfigurationManager.AppSettings["appPath"], asset.Path.Substring(1));
                Log.Info("Dump Path: " + swfDumpPath);
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.CreateNoWindow = false;
                startInfo.UseShellExecute = false;
                startInfo.FileName = "\"" + swfDumpPath + "\"";
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.Arguments = "-X -Y \"" + swfPath + "\"";
                startInfo.RedirectStandardOutput = true;

                string output = "";
                
                using (Process exeProcess = Process.Start(startInfo))
                {
                    while (!exeProcess.StandardOutput.EndOfStream)
                    {
                        output += exeProcess.StandardOutput.ReadLine();        
                    }
                }
                Log.Info("SWFDumpOutput Was: " + output);
                string[] outputTokens = output.Split(' ');

                for (var i = 0; i < outputTokens.Length; i++)
                {
                    if (outputTokens[i] == "-X" && (i+1) < outputTokens.Length)
                    {
                        int w;
                        if (int.TryParse(outputTokens[i+1], out w)) {
                            asset.Width = w;
                        }
                    }
                    if (outputTokens[i] == "-Y" && (i+1) < outputTokens.Length)
                    {
                        int h;
                        if (int.TryParse(outputTokens[i+1], out h)) {
                            asset.Height = h;
                        }
                    }
                }
            } catch(Exception e) {
                Log.Error("Could not get dimensions of swf " + asset.Path);
            }
        }

        private string convertSWFToPNG(string assetPath)
        {
            try
            {
                string outputFile = Path.Combine(Path.GetDirectoryName(assetPath), Path.GetFileNameWithoutExtension(assetPath) + ".png");
                string appPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string swfRenderPath = Path.Combine(Path.GetDirectoryName(appPath), "SWFTools", "swfrender.exe");


                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.CreateNoWindow = false;
                startInfo.UseShellExecute = false;
                startInfo.FileName = "\"" + swfRenderPath + "\"";
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                startInfo.Arguments = "-o \"" + outputFile + "\" \"" + assetPath + "\"";

                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                }

                return outputFile;
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not convert SWF to PNG: " + assetPath + " " + e);
            }

            return null;
        }
    }
}
