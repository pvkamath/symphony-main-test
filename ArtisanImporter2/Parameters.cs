﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Symphony.Core.Controllers;
using Symphony.Core;
using Symphony.Core.Models;
using System.IO;

namespace ArtisanImporter2
{
    class Parameters
    {
        public static string ImportPath = "";
        public static string Mode = "";
        public static int CategoryID = 0;

        public static Dictionary<string, Importer> Importers = new Dictionary<string, Importer>();

        public static Importer Importer
        {
            get
            {
                if (Importers.ContainsKey(Mode))
                {
                    return Importers[Mode];
                }
                return new CWSImporter();
            }
        }

        public Parameters()
        {
            foreach (Type t in this.GetType().Assembly.GetTypes()) {
                if (t.GetInterfaces().Contains(typeof(Importer)))
                {
                    Importer importer = (Importer)Activator.CreateInstance(t);
                    int duplicates = 0;
                    string postfix = "";

                    while (Importers.ContainsKey(importer.Code.ToUpper() + postfix))
                    {
                        duplicates++;
                        postfix = duplicates.ToString();
                    }

                    Importers.Add(importer.Code.ToUpper() + postfix, importer);
                }
            }
        }

        public void HandleParameters() {
            Console.WriteLine("Select mode to use: (" + String.Join(", ", Importers.Keys.ToArray<string>()) + ")");
            while (!GetMode())
            {
                Console.WriteLine("Please select a valid mode.");
            }

            TextWriter writer = Console.Out;
            Console.SetOut(TextWriter.Null);
            bool needsDirectory = !Importer.ValidateDirectory();
            Console.SetOut(writer);

            if (needsDirectory)
            {
                Console.WriteLine("Enter directory to import from:");
                while (!GetDirectory())
                {
                    Console.WriteLine("Please enter a valid directory.");
                }
            }

            Console.WriteLine("Enter category to import into:");
            while (!GetCategory())
            {
                Console.WriteLine("Could not create or retrieve that category, try again.");
            }
        }

        private bool GetCategory()
        {
            //string category = "main"; //Console.ReadLine();
            string category = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(category)) {
                return false;
            }

            List<Category> categories = CategoryController.GetCategories(SymphonyLogin.CustomerID, category, CategoryType.Artisan).Data;

            categories = categories.Where(c => c.Name == category).ToList();

            if (categories.Count == 0)
            {
                Category cat = CategoryController.SaveCategory(SymphonyLogin.CustomerID, 0, CategoryType.Artisan, new Category()
                {
                    Name = category,
                    Description = "Auto Import Category"
                }).Data;

                if (cat == null || cat.Id == 0)
                {
                    return false;
                }

                CategoryID = cat.Id;
                return true;
            }

            CategoryID = categories[0].Id;
            return true;
        }

        private bool GetDirectory()
        {

            string dir = Console.ReadLine();
            if (Directory.Exists(dir))
            {
                ImportPath = dir;

                return Importer.ValidateDirectory();
            }

            return false;
        }


        private bool GetMode()
        {
            //string mode = "HC"; 
            string mode = Console.ReadLine().ToUpper();
            if (Importers.ContainsKey(mode))
            {
                Mode = mode;

                Console.WriteLine("Import using " + Importer.Name + "? (Y/N)");
                //var result = true; 
                var result = Console.ReadKey().Key == ConsoleKey.Y;
                Console.WriteLine("");
                return result;
            }

            return false;
        }
    }
}
