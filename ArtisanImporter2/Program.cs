﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Symphony.Core;
using Symphony.Core.Controllers;
using Symphony.Core.Models;
using System.Net;
using System.Web;
using log4net;

namespace ArtisanImporter2
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.Setup();
            try
            {
                SymphonyLogin login = new SymphonyLogin();
                login.HandleLogin();

                Parameters parameters = new Parameters();
                parameters.HandleParameters();
            }
            catch (Exception e)
            {
                Log.Error(e);
                Console.ReadLine();
                return;
            }
            try
            {
                Parameters.Importer.Import();
            }
            catch (Exception e)
            {
                Log.Error("Could not complete import");
                Log.Error(e);
            }
            Log.Info("Complete");
            Console.ReadLine();
        }

    }
}
