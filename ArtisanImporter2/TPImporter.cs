﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using Symphony.Core;
using Symphony.Core.Models;
using Data = Symphony.Core.Data;
using Symphony.Core.Extensions;

using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace ArtisanImporter2
{
    class TPImporter : Importer
    {
        Regex partReg = new Regex("Part ([a-zA-Z0-9])(?!.*Part [a-zA-Z0-9])");
                                        

        // SQL Commands
        string getCourses = @"SELECT C.*, AT.Type, AP.Name  
            FROM Courses as C
            JOIN AccreditationTypes as AT on AT.ID = C.AccreditationType 
            JOIN ApprovalType as AP on AP.ApprovalTypeID = C.ApprovalType 
            WHERE C.CourseID in (@tpCourseIds)";

        string getLessons = @"SELECT L.*, TmT.Type 
            FROM Lessons as L
            JOIN TimeTypes as TmT on TmT.TypeID = L.TimeType 
            WHERE L.CourseID in (@tpCourseIds)";

        string getLessonPDFs = @"SELECT LP.*
            FROM LessonPDFs as LP
            WHERE LP.LessonID in (
	            SELECT L.LessonID  
	            FROM Lessons as L
	            WHERE L.CourseID in (@tpCourseIds)
	            )";

        string getLessonSections = @"SELECT LS.* 
            FROM LessonSections as LS
            WHERE LS.LessonID in (
	            SELECT L.LessonID 
	            FROM Lessons as L
	            WHERE L.CourseID in (@tpCourseIds))";

        string getTests = @"SELECT T.*, TsT.Type, TmT.Type, l.CourseID  
            FROM Tests as T
            JOIN TestTypes as TsT on TsT.TypeID = T.TestType 
            JOIN TimeTypes as TmT on TmT.TypeID = T.TimeType
            JOIN Lessons as L on l.LessonID = T.LessonID
            WHERE T.LessonID in (
	            SELECT L.LessonID 
	            FROM Lessons as L
	            WHERE L.CourseID in (@tpCourseIds))";
        // Loads questions specific to post tests
        string getTestQuestions = @"SELECT DISTINCT TQ.*, TQT.Type, T.LessonID
            FROM TestsQuestions as TQ
            JOIN TestQuestionTypes as TQT on TQT.ID = TQ.QuestionTypeID
            JOIN Tests as T on T.TestID = TQ.TestID
            LEFT JOIN InteractionItems i on i.LessonID = T.LessonID 
            WHERE TQ.TestID in (
	            SELECT T.TestID 
	            FROM Tests as T
	            WHERE T.LessonID in (SELECT L.LessonID 
	            FROM Lessons as L
	            WHERE L.CourseID in (@tpCourseIds)))";

        string getTestAnswers = @"SELECT TA.*, TAT.Type 
            FROM TestsAnswers as TA
            JOIN TestAnswerTypes as TAT on TAT.TypeID = TA.AnswerTypeID 
            WHERE TA.QuestionID IN (
	            SELECT TQ.QuestionID 
	            FROM TestsQuestions as TQ
	            WHERE TQ.TestID in (SELECT T.TestID 
	            FROM Tests as T
	            WHERE T.LessonID in (SELECT L.LessonID 
	            FROM Lessons as L
	            WHERE L.CourseID in (@tpCourseIds))))";

        // Loads questions specific to quizes or other standard learning objects
        string getQuizQuestions = @"select * from (
SELECT IMCA.AnswerID, IMCA.QuestionID, IMCA.ItemID, IMCA.Answer, IMCA.CorrectAnswer, IMCA.Sort, '' as Matches
FROM InteractionMultipleChoiceAnswers as IMCA
WHERE IMCA.ItemID in (
	SELECT II.ItemID 
	FROM InteractionItems as II
	WHERE CourseID in (@tpCourseIds)
	)
UNION
SELECT IFCA.AnswerID, IFCA.QuestionID, IFCA.ItemID, IFCA.CorrectAnswer as Answer, 1 as CorrectAnswer, 0 as Sort, '' as Matches
FROM InteractionFillCompareAnswers as IFCA
WHERE IFCA.ItemID in (
	SELECT II.ItemID 
	FROM InteractionItems as II
	WHERE CourseID in (@tpCourseIds)
	)
UNION
SELECT IMA.AnswerID, IMA.QuestionID, IMA.ItemID, IMA.CorrectAnswer as Answer, 1 as CorrectAnswer, 0 as Sort, '' as Matches
FROM InteractionMatchAnswers as IMA
WHERE IMA.ItemID in (
	SELECT II.ItemID 
	FROM InteractionItems as II
	WHERE CourseID in (@tpCourseIds)
	)
UNION
Select a.AnswerID, a.QuestionID, a.ItemID, q.Question as Answer, 1 as CorrectAnswer, a.Sort, a.CorrectAnswer as Matches from InteractionItems i
join InteractionQuestions q on q.ItemID = i.ItemID
join InteractionMatchAnswers a on a.QuestionID = q.QuestionID and a.ItemID = q.ItemID
where QuestionTypeID = 4
and a.ItemID in (
	SELECt II.ItemID
	From InteractionItems as II
	Where CourseID in (@tpCourseIds)
)
) a 
left join InteractionAnswersFeedback f on f.AnswerID = a.AnswerID	
join (
	SELECT II.*, IQT.Type 
	FROM InteractionItems as II
	JOIN InteractionQuestionTypes as IQT on II.QuestionTypeID = IQT.TypeID 
	WHERE CourseID in (@tpCourseIds)
) q on q.ItemID = a.ItemID
join (
	SELECT * 
	FROM InteractionQuestions as IQ
	WHERE IQ.ItemID in (
		SELECT II.ItemID 
		FROM InteractionItems as II
		WHERE CourseID in (@tpCourseIds)
	)
) iqs on iqs.ItemID = q.ItemID
left join Tests T on T.LessonId = q.LessonId
order by q.ItemID, a.Sort asc";

        ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["TrainingPro"];
        Dictionary<string, ArtisanCourse> existingCourses = new Dictionary<string, ArtisanCourse>();

        string quizDescription = "This quiz is designed to evaluate knowledge following completion of this section of the required reading.  You must pass this quiz with a score of no less than 70% in order to move forward with this course.";
        string testDescription = "This exam is designed to evaluate knowledge following completion of the required reading for this course.  You must pass this exam with a score of at least 70% in order to complete your course.";

        public string Code
        {
            get { return "TP"; }
        }

        public string Name
        {
            get { return "Training Pro"; }
        }

        public bool ValidateDirectory()
        {
            return true;
        }

        public bool NeedsDirectory
        {
            get { return false; }
        }

        public int[] GetCourseIds()
        {
            string courseIdString = "";

            try
            {
                courseIdString = ConfigurationManager.AppSettings["trainingProIds"];
                Log.Info("Using course ids from config: " + courseIdString);
            }
            catch (Exception e)
            {
                Log.Info("Course ids not in config - Please enter ids:");
                courseIdString = Console.ReadLine();
            }
            List<int> ids = new List<int>();

            string[] courseIdTokens = courseIdString.Split(',');
            foreach (string s in courseIdTokens)
            {
                string[] minMax = s.Split('-');
                if (minMax.Length == 2)
                {
                    int min, max;
                    if (int.TryParse(minMax[0], out min) &&
                        int.TryParse(minMax[1], out max))
                    {
                        for (var i = min; i <= max; i++)
                        {
                            ids.Add(i);
                        }
                    }
                }
                else
                {
                    int id;
                    if (int.TryParse(minMax[0], out id))
                    {
                        ids.Add(id);
                    }
                }
            }

            return ids.ToArray();
        }

        public void Import()
        {
            try
            {
                if (settings == null)
                {
                    throw new Exception("No TrainingPro connection string set");
                }

                Log.Info("Starting TrainingPRO import");

                int[] courseIdsArr = new int[0];
                while (courseIdsArr.Length == 0)
                {
                    Console.WriteLine("Enter course IDs to import:");

                    courseIdsArr = GetCourseIds();
                    if (courseIdsArr.Length == 0)
                    {
                        Console.WriteLine("Course ids were invalid");
                    }
                }


                existingCourses = SubSonic.Select.AllColumnsFrom<Data.ArtisanCourse>()
                    .Where(Data.ArtisanCourse.CategoryIDColumn).IsEqualTo(Parameters.CategoryID)
                    .And(Data.ArtisanCourse.ExternalIDColumn).ContainsString("trpr-")
                    .ExecuteTypedList<ArtisanCourse>()
                    .DistinctBy(c => c.ExternalID)
                    .ToDictionary(c => c.ExternalID.Replace("trpr-", ""));

                Double totalTime = 0;
                int count = 0;

                foreach (int i in courseIdsArr)
                {
                    int courseId = i;

                    DateTime start = DateTime.Now;
                    if (existingCourses.ContainsKey(i.ToString()))
                    {
                        ArtisanCourse existing = existingCourses[i.ToString()];

                        Log.Info("Skipping course ID: " + i + " as it already exists in the current category. (Course Name: " + existing.Name + " Ext. ID: " + existing.ExternalID + ")");

                        continue;
                    }

                    Log.Info("Importing course: " + i.ToString());

                    ArtisanCourse currentCourse = new ArtisanCourse();
                    try
                    {

                        List<ArtisanCourse> courses = new List<ArtisanCourse>();

                        // Normal sections with pages
                        List<ArtisanSection> sections = new List<ArtisanSection>();
                        List<ArtisanPage> pages = new List<ArtisanPage>();

                        List<ArtisanAsset> assets = new List<ArtisanAsset>();

                        // Test sections with questions and answers (Some tests may be quizzes)       
                        List<ArtisanSection> tests = new List<ArtisanSection>();
                        List<ArtisanPage> questions = new List<ArtisanPage>();
                        List<ArtisanAnswer> answers = new List<ArtisanAnswer>();

                        // Quizzes
                        List<ArtisanPage> quizQuestions = new List<ArtisanPage>();
                        List<ArtisanPage> inlineQuestions = new List<ArtisanPage>();


                        Dictionary<int, ArtisanSection> sectionDictionary = new Dictionary<int, ArtisanSection>();
                        Log.Info("Connecting to db...");
                        // load up all the data first
                        using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
                        {
                            // Get Courses
                            Log.Info("Retrieving all courses...");
                            ReadTpSql(conn, getCourses, courseId, (reader) =>
                            {
                                while (reader.Read())
                                {
                                    ArtisanCourse course = new ArtisanCourse()
                                    {
                                        Id = reader.GetInt32(0),
                                        ExternalID = "trpr-" + reader.GetInt32(0).ToString() + "-nmls" + reader.GetString(31),
                                        Name = reader.GetString(1),
                                        Description = reader.GetString(2),
                                        Keywords = "TrainingPro, " + reader.GetInt32(0) + ", " + reader.GetString(43) + ", " + reader.GetString(44),
                                        NavigationMethod = ArtisanCourseNavigationMethod.FullSequential,
                                        MarkingMethod = ArtisanCourseMarkingMethod.HideAnswersAndFeedback,
                                        ReviewMethod = ArtisanCourseReviewMethod.ShowReview,
                                        CompletionMethod = ArtisanCourseCompletionMethod.Navigation,
                                        CertificateEnabled = false,
                                        InactivityTimeout = 1200,
                                        ThemeId = 26,
                                        Theme = new ArtisanTheme(),
                                        CategoryId = Parameters.CategoryID,
                                        Objectives = "",
                                        Parameters = "{}",
                                        Sections = new List<ArtisanSection>()
                                    };
                                    currentCourse = course;
                                    Log.Info("Adding course: " + course.Name);
                                    courses.Add(course);
                                }
                                return true;
                            });

                            // Get Sections
                            Log.Info("Retrieving all sections...");
                            ReadTpSql(conn, getLessons, courseId, (reader) =>
                            {
                                while (reader.Read())
                                {
                                    ArtisanSection section = new ArtisanSection()
                                    {
                                        Id = reader.GetInt32(0),
                                        ExternalID = "trpr-" + reader.GetInt32(0).ToString(),
                                        Name = reader.GetString(1),
                                        CourseId = reader.GetInt32(2),
                                        SectionType = (int)ArtisanSectionType.LearningObject,
                                        Sections = new List<ArtisanSection>(),
                                        Pages = new List<ArtisanPage>(),
                                        Sort = reader.GetInt32(3) * 10 // so we can insert sections in between
                                    };
                                    Log.Info("Adding section " + section.Name);
                                    sections.Add(section);
                                }
                                return true;
                            });

                            // Get PDFs -- TODO: Need a way/url to access the files
                            Log.Info("Retrieving all pdfs...");
                            ReadTpSql(conn, getLessonPDFs, courseId, (reader) =>
                            {
                                while (reader.Read())
                                {
                                    ArtisanAsset asset = new ArtisanAsset()
                                    {
                                        Id = reader.GetInt32(0),
                                        Keywords = reader.GetInt32(0).ToString() + "-" + reader.GetInt32(1),
                                        Filename = reader.GetString(2)
                                    };
                                    Log.Info("Adding asset " + asset.Filename);
                                    assets.Add(asset);
                                }

                                return true;
                            });

                            // Get the lesson sections - these are pages
                            Log.Info("Retrieving all pages...");
                            ReadTpSql(conn, getLessonSections, courseId, (reader) =>
                            {
                                while (reader.Read())
                                {
                                    ArtisanPage page = new ArtisanPage()
                                    {
                                        Id = reader.GetInt32(0),
                                        ExternalID = "trpr-" + reader.GetInt32(0).ToString() + "-" + reader.GetInt32(1).ToString(),
                                        Name = reader.GetString(2),
                                        Sort = reader.GetInt32(3) * 100,
                                        Html = Util.GetDefaultHtml(reader.GetString(2), reader.GetString(4)),
                                        Answers = new List<ArtisanAnswer>(),
                                        SectionID = reader.GetInt32(1),
                                        PageType = (int)ArtisanPageType.Content,
                                        TemplateId = Util.GetDefaultTemplateId()
                                    };

                                    page.Html = Util.ProcessHtml(page.Html, (url) =>
                                    {
                                        return "http://www.trainingpro.com/" + url;
                                    });

                                    Log.Info("Adding page " + page.Name);
                                    pages.Add(page);
                                }
                                return true;
                            });

                            // Get the tests - these are sections that will be post tests
                            Log.Info("Retrieving all tests...");
                            ReadTpSql(conn, getTests, courseId, (reader) =>
                            {
                                while (reader.Read())
                                {
                                    bool showFeedback = false;
                                    bool showCorrectAnswer = false;
                                    try
                                    {
                                        showFeedback = reader.GetBoolean(19);
                                    }
                                    catch (Exception e)
                                    {
                                        Log.Info("error parsing feeback boolean - assuming false");
                                    }
                                    try
                                    {
                                        showCorrectAnswer = reader.GetBoolean(20);
                                    }
                                    catch (Exception e)
                                    {
                                        Log.Info("error parsing show answer boolean - assuming false");
                                    }
                                    ArtisanCourseMarkingMethod markingMethod = !showFeedback ? ArtisanCourseMarkingMethod.HideAnswersAndFeedback :
                                        !showCorrectAnswer ? ArtisanCourseMarkingMethod.HideAnswers :
                                        ArtisanCourseMarkingMethod.ShowAnswers;

                                    int sectionId = reader.GetInt32(1);

                                    ArtisanSection test = new ArtisanSection()
                                    {
                                        // The id in this case will be the testID
                                        Id = reader.GetInt32(0),
                                        ExternalID = "trpr-" + reader.GetInt32(0).ToString() + "-" + reader.GetInt32(1),
                                        Name = reader.GetString(2),
                                        PassingScore = reader.GetInt32(5),
                                        IsReRandomizeOrder = reader.GetBoolean(9),
                                        MaxQuestions = reader.GetInt32(10),
                                        MarkingMethod = (int)markingMethod,
                                        Sections = new List<ArtisanSection>(),
                                        TestType = ArtisanTestType.Random,
                                        // Assuming quiz until we find out it has test questions
                                        SectionType = (int)ArtisanSectionType.LearningObject,
                                        IsQuiz = true,
                                        Pages = new List<ArtisanPage>(),
                                        CourseId = reader.GetInt32(30)
                                    };

                                    ArtisanSection relatedSection = sections.FirstOrDefault(s => s.Id == sectionId);
                                    int insertAfter = sections.IndexOf(relatedSection);

                                    if (insertAfter > -1)
                                    {
                                        test.Sort = relatedSection.Sort + (1 + relatedSection.RelatedQuizes);
                                        relatedSection.RelatedQuizes++;
                                        sections.Insert(insertAfter + 1, test);
                                    }
                                    else
                                    {
                                        sections.Add(relatedSection);
                                    }

                                }

                                return true;
                            });

                            // Getting test questions
                            Log.Info("Retrieving all test questions...");
                            ReadTpSql(conn, getTestQuestions, courseId, (reader) =>
                            {
                                while (reader.Read())
                                {
                                    string incorrectResponse = "Incorrect";
                                    string html = "";
                                    try
                                    {
                                        incorrectResponse = reader.GetString(5);
                                    }
                                    catch (Exception e)
                                    {
                                        Log.Info("No incorrect response entered for question - assuming 'Incorrect' QID: " + reader.GetInt32(0));
                                    }

                                    try
                                    {
                                        html = reader.GetString(2);
                                    }
                                    catch (Exception e)
                                    {
                                        Log.Info("No text set for question, assuming blank? QID: " + reader.GetInt32(0));
                                    }

                                    ArtisanPage page = new ArtisanPage()
                                    {
                                        Id = reader.GetInt32(0),
                                        SectionID = reader.GetInt32(1),
                                        ExternalID = "trpr-" + reader.GetInt32(0) + "-" + reader.GetInt32(1),
                                        Name = reader.GetInt32(0).ToString(),
                                        Sort = reader.GetInt32(3),
                                        QuestionType = reader.GetInt32(4) == 1 ? ArtisanQuestionType.MultipleChoice : ArtisanQuestionType.LongAnswer,
                                        IncorrectResponse = incorrectResponse,
                                        Answers = new List<ArtisanAnswer>(),
                                        Html = html,
                                        PageType = (int)ArtisanPageType.Question
                                    };

                                    page.Html = Util.ProcessHtml(page.Html, (url) => { return "http://www.trainingpro.com/" + url; });

                                    Log.Info("Adding question " + page.Name);
                                    questions.Add(page);
                                }
                                return true;
                            });

                            Log.Info("Retrieving all test answers...");
                            ReadTpSql(conn, getTestAnswers, courseId, (reader) =>
                            {
                                while (reader.Read())
                                {
                                    ArtisanAnswer answer = new ArtisanAnswer()
                                    {
                                        Id = reader.GetInt32(0),
                                        IsCorrect = reader.GetBoolean(5),
                                        PageId = reader.GetInt32(2),
                                        Text = reader.GetString(3),
                                        Sort = reader.GetInt32(4)
                                    };
                                    Log.Info("Adding answer " + answer.Id);
                                    answers.Add(answer);
                                }

                                return true;
                            });

                            Log.Info("Retrieving all quiz questions with answers...");
                            ReadTpSql(conn, getQuizQuestions, courseId, (reader) =>
                            {
                                int lastQuetsionId = 0;
                                ArtisanQuestionType questionType;
                                bool lastInline = false;

                                while (reader.Read())
                                {
                                    int questionId = reader.GetInt32(1);

                                    if (questionId != lastQuetsionId)
                                    {

                                        int questionTypeId = reader.GetInt32(14);

                                        switch (questionTypeId)
                                        {
                                            case 1:
                                                questionType = ArtisanQuestionType.FillInTheBlank;
                                                break;
                                            case 2:
                                                questionType = ArtisanQuestionType.LongAnswer;
                                                break;
                                            case 3:
                                                questionType = ArtisanQuestionType.MultipleChoice;
                                                break;
                                            case 4:
                                                questionType = ArtisanQuestionType.Matching;
                                                break;
                                            default:
                                                Log.Error("Unkonwn question type: " + questionTypeId + " defaulting to Multiple Choice");
                                                questionType = ArtisanQuestionType.MultipleChoice;
                                                break;
                                        }

                                        int sectionId = reader.GetInt32(14);
                                        try
                                        {
                                            sectionId = reader.GetInt32(23);
                                        }
                                        catch
                                        {
                                            // Don't care - just means its just an inline question not part of a separate quiz
                                        }

                                        int lessonSectionId = 0;
                                        try
                                        {
                                            lessonSectionId = reader.GetInt32(15);
                                        } catch {

                                        }


                                        

                                        ArtisanPage page = new ArtisanPage()
                                        {
                                            ExternalID = "trpr-" + reader.GetInt32(1).ToString(),
                                            Html = reader.GetString(20),
                                            Id = reader.GetInt32(1),
                                            SectionID = sectionId,
                                            Sort = reader.GetInt32(22),
                                            CourseId = reader.GetInt32(13),
                                            QuestionType = questionType,
                                            Answers = new List<ArtisanAnswer>(),
                                            Name = reader.GetInt32(1).ToString(),
                                            IncorrectResponse = "",
                                            CorrectResponse = "",
                                            PageType = (int)ArtisanPageType.Question
                                        };



                                        page.Html = Util.ProcessHtml(page.Html, (url) => { return "http://www.trainingpro.com/" + url; });

                                        Log.Info("Adding Question " + page.Name);

                                        if (lessonSectionId > 0)
                                        {
                                            page.SectionID = lessonSectionId;
                                            inlineQuestions.Add(page);
                                            lastInline = true;
                                        }
                                        else
                                        {
                                            quizQuestions.Add(page);
                                            lastInline = false;
                                        }
                                    }


                                    lastQuetsionId = questionId;

                                    ArtisanPage question = lastInline ? inlineQuestions[inlineQuestions.Count() - 1] : quizQuestions[quizQuestions.Count() - 1];
                                    
                                    ArtisanAnswer answer;
                                    try
                                    {
                                        answer = new ArtisanAnswer()
                                        {
                                            Id = reader.GetInt32(0),
                                            IsCorrect = reader.GetInt32(4) == 1,
                                            PageId = reader.GetInt32(2),
                                            Sort = reader.GetInt32(5),
                                            Text = reader.GetString(3)
                                        };
                                    }
                                    catch (Exception e)
                                    {
                                        Log.Error("Problem with answer");
                                        throw e;
                                    }
                                    Log.Info("Adding Answer " + answer.Id);
                                    switch (question.QuestionType)
                                    {
                                        case ArtisanQuestionType.Matching:
                                            answer.MatchingAnswer = new ArtisanMatchingAnswer()
                                            {
                                                LeftText = reader.GetString(3),
                                                RightText = reader.GetString(6)
                                            };
                                            break;
                                        case ArtisanQuestionType.FillInTheBlank:
                                            Regex reg = new Regex("_{2,}");
                                            question.Html = reg.Replace(question.Html, "_______________");
                                            if (!question.Html.Contains("_______________"))
                                            {
                                                question.Html += " _______________";
                                            }
                                            break;
                                    }
                                    string feedback = "";
                                    try
                                    {
                                        feedback = reader.GetString(10);
                                        answer.Feedback = feedback;
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                    if (answer.IsCorrect && !string.IsNullOrWhiteSpace(feedback))
                                    {
                                        question.CorrectResponse += (!string.IsNullOrWhiteSpace(question.CorrectResponse) ? "<br/>" : "") + feedback;
                                    }
                                    else if (!answer.IsCorrect && !string.IsNullOrWhiteSpace(feedback))
                                    {
                                        question.IncorrectResponse += (!string.IsNullOrWhiteSpace(question.IncorrectResponse) ? "<br/>" : "") + feedback;
                                    }

                                    question.Answers.Add(answer);
                                }

                                quizQuestions.ForEach((q) =>
                                {
                                    q.IncorrectResponse = string.IsNullOrWhiteSpace(q.IncorrectResponse) ? "Incorrect" : q.IncorrectResponse;
                                    q.CorrectResponse = string.IsNullOrWhiteSpace(q.CorrectResponse) ? "Correct" : q.CorrectResponse;
                                });

                                return true;
                            });
                        }

                        Log.Info("Creating section dictionary");
                        sectionDictionary = sections.ToDictionary(s => s.Id);

                        // Spit test answers into the proper test questions
                        Log.Info("Sticking answers in questions");
                        Dictionary<int, ArtisanPage> questionDictionary = questions.ToDictionary(q => q.Id);
                        foreach (ArtisanAnswer answer in answers)
                        {
                            if (questionDictionary.ContainsKey(answer.PageId))
                            {
                                if (questionDictionary[answer.PageId].Answers == null)
                                {
                                    questionDictionary[answer.PageId].Answers = new List<ArtisanAnswer>();
                                }
                                questionDictionary[answer.PageId].Answers.Add(answer);
                            }
                        }
                        Log.Info("Sticking pages in sections");

                        Dictionary<int, int> pagePositions = new Dictionary<int,int>();

                        for (int pi = 0; pi < pages.Count(); pi++)
                        {
                            pagePositions.Add(pages[pi].Id, pi);
                        }

                        foreach (ArtisanPage iq in inlineQuestions)
                        {
                            if (pagePositions.ContainsKey(iq.SectionID))
                            {
                                int pageIndex = pagePositions[iq.SectionID];
                                iq.SectionID = pageIndex;
                            }
                            else
                            {
                                iq.SectionID = -1;
                            }
                        }

                        inlineQuestions = inlineQuestions.OrderByDescending(iq => iq.SectionID).ToList();
                                       
                        foreach (ArtisanPage iq in inlineQuestions)
                        {
                            if (iq.SectionID > -1)
                            {
                                if (iq.SectionID + 1 > pages.Count())
                                {
                                    continue;
                                }
                                pages.Insert(iq.SectionID + 1, iq);
                                iq.Sort = pages[iq.SectionID].Sort;
                                iq.Name = partReg.Replace(pages[iq.SectionID].Name, "");
                                if (iq.Name[iq.Name.Length - 1] == ' ')
                                {
                                    iq.Name += "Question, Part Z";
                                }
                                else
                                {
                                    iq.Name += ", Question, Part Z";
                                }
                                iq.SectionID = pages[iq.SectionID].SectionID;
                               
                            }
                        }


                        // Spit pages out into the proper sections
                        foreach (ArtisanPage page in pages)
                        {
                            if (sectionDictionary.ContainsKey(page.SectionID))
                            {
                                if (sectionDictionary[page.SectionID].Pages == null)
                                {
                                    sectionDictionary[page.SectionID].Pages = new List<ArtisanPage>();
                                }
                                sectionDictionary[page.SectionID].Pages.Add(page);
                            }
                        }
                        Log.Info("Sticking questions in sections");
                        // Spit the questions into proper sections
                        foreach (ArtisanPage question in questions)
                        {
                            if (sectionDictionary.ContainsKey(question.SectionID))
                            {
                                if (sectionDictionary[question.SectionID].Pages == null)
                                {
                                    sectionDictionary[question.SectionID].Pages = new List<ArtisanPage>();
                                }
                                sectionDictionary[question.SectionID].Pages.Add(question);
                                if (sectionDictionary[question.SectionID].Name.ToLower().Contains("assignment"))
                                {
                                    question.QuestionType = ArtisanQuestionType.LongAnswer;
                                    sectionDictionary[question.SectionID].IsQuiz = false;
                                }
                                else if (sectionDictionary[question.SectionID].Name.ToLower().Contains("quiz"))
                                {
                                    sectionDictionary[question.SectionID].SectionType = (int)ArtisanSectionType.LearningObject;
                                    sectionDictionary[question.SectionID].IsQuiz = true;
                                    sectionDictionary[question.SectionID].PassingScore = 70;
                                    sectionDictionary[question.SectionID].MaxQuestions = 5;
                                    sectionDictionary[question.SectionID].IsRandomizeAnswers = true;
                                    sectionDictionary[question.SectionID].MarkingMethod = (int)ArtisanCourseMarkingMethod.HideAnswersAndFeedback;
                                    
                                    if (string.IsNullOrWhiteSpace(sectionDictionary[question.SectionID].Description))
                                    {
                                        sectionDictionary[question.SectionID].Description = quizDescription;
                                    }
                                }
                                else
                                {
                                    sectionDictionary[question.SectionID].SectionType = (int)ArtisanSectionType.Posttest;
                                    sectionDictionary[question.SectionID].Description = testDescription;
                                    sectionDictionary[question.SectionID].IsQuiz = false;
                                }
                            }
                        }
                        Log.Info("Sticking quiz questions in sections");
                        foreach (ArtisanPage quizQuestion in quizQuestions)
                        {
                            if (sectionDictionary.ContainsKey(quizQuestion.SectionID))
                            {
                                if (sectionDictionary[quizQuestion.SectionID].Pages == null)
                                {
                                    sectionDictionary[quizQuestion.SectionID].Pages = new List<ArtisanPage>();
                                }
                                sectionDictionary[quizQuestion.SectionID].Pages.Add(quizQuestion);
                                sectionDictionary[quizQuestion.SectionID].IsQuiz = sectionDictionary[quizQuestion.SectionID].Pages.Where(p => p.PageType == (int)ArtisanPageType.Content).Count() == 0;
                                sectionDictionary[quizQuestion.SectionID].SectionType = (int)ArtisanSectionType.LearningObject;

                                if (sectionDictionary[quizQuestion.SectionID].IsQuiz)
                                {
                                    sectionDictionary[quizQuestion.SectionID].Description = quizDescription;
                                }
                            }
                        }
                        Log.Info("Ordering Sections");
                        sections = sections.OrderBy(s => s.Sort).ToList();

                        Log.Info("Sticking sections in courses");
                        Dictionary<int, ArtisanCourse> courseDictionary = courses.ToDictionary(c => c.Id);
                        string finalExamInstructions = "";
                        foreach (ArtisanSection s in sections)
                        {
                            if (courseDictionary.ContainsKey(s.CourseId))
                            {
                                ArtisanCourse c = courseDictionary[s.CourseId];
                                if (c.Sections == null || c.Sections.Count == 0)
                                {
                                    c.Sections = new List<ArtisanSection>();
                                }

                                if (s.Name == "Final Exam Instructions" && s.Pages.Count() > 0)
                                {
                                    finalExamInstructions = Util.ConvertHtmlToDescription(s.Pages[0].Html);
                                }
                                else
                                {
                                    c.Sections.Add(s);

                                    if (s.SectionType == (int)ArtisanSectionType.Posttest)
                                    {
                                        s.Description = !String.IsNullOrWhiteSpace(finalExamInstructions) ? finalExamInstructions : testDescription;
                                        c.CompletionMethod = ArtisanCourseCompletionMethod.Test;
                                    }
                                    else
                                    {
                                        s.SectionType = (int)ArtisanSectionType.Sco;
                                    }
                                }
                            }
                        }
                        Log.Info("Starting save");
                        Symphony.Core.Controllers.ArtisanController artisanController = new Symphony.Core.Controllers.ArtisanController();
                        foreach (ArtisanCourse c in courses)
                        {
                            bool hasPretest = false;
                            bool hasPosttest = false;
                            bool hasContent = false;
                            bool hasPreContent = false;
                            bool hasPostContent = false;

                            List<ArtisanCourse> saveableSubCourses = new List<ArtisanCourse>();

                            List<ArtisanSection> processedSectionList = new List<ArtisanSection>();

                            Log.Info("Restructuring sections");

                            foreach (ArtisanSection s in c.Sections)
                            {
                                Log.Info("Hitting a section");

                                Log.Info(s.Name);

                                if (s.SectionType == (int)ArtisanSectionType.Sco)
                                {
                                    Log.Info("IS A SCO");
                                    if (s.IsQuiz)
                                    {
                                        Log.Info("Make it a quiz");
                                        // If it's a quiz all we need to do is stick a new learning object
                                        // in for it. There wont be any sub learning objects.
                                        ArtisanSection quizLearningObject = new ArtisanSection();
                                        quizLearningObject.CopyFrom(s);
                                        quizLearningObject.SectionType = (int)ArtisanSectionType.LearningObject;

                                        s.IsQuiz = false;
                                        s.Pages = new List<ArtisanPage>();
                                        s.Sections = new List<ArtisanSection>() {
                                        quizLearningObject
                                    };
                                    }
                                    else
                                    {
                                        Log.Info("Make it a LO");
                                        // If it isn't a quiz, lets break the sco down into multiple learning
                                        // objects. Doing this based on page titles
                                        ArtisanSection learningObject = new ArtisanSection();
                                        Regex newLO = new Regex("[aA01]");
                                        foreach (ArtisanPage page in s.Pages)
                                        {

                                            Match match = partReg.Match(page.Name);
                                            string partLetter = "";
                                            if (match.Success)
                                            {
                                                Group g = match.Groups[1];
                                                partLetter = g.ToString();
                                            }
                                            Log.Info("Part Letter: " + partLetter + " - " + " Page Name: " + page.Name + " Learning Object Name: " + learningObject.Name + " Match?: " + newLO.Match(partLetter).Success);

                                            if ((string.IsNullOrWhiteSpace(partLetter) && page.Name != learningObject.Name) || newLO.Match(partLetter).Success)
                                            {
                                                string learningObjectName = page.Name.Replace(", Part " + partLetter, "");

                                                learningObject = new ArtisanSection()
                                                {
                                                    ExternalID = s.ExternalID + "-tp-autoLO",
                                                    Name = learningObjectName,
                                                    Sections = new List<ArtisanSection>(),
                                                    Pages = new List<ArtisanPage>(),
                                                    SectionType = (int)ArtisanSectionType.LearningObject
                                                };

                                                s.Sections.Add(learningObject);
                                            }

                                            page.Name = page.Name.Replace(", Question, Part Z", " Question");

                                            if (learningObject.Pages == null)
                                            {
                                                // So there is a new section,
                                                // and the first page is Part B, or something other than
                                                // Part A or Part 1.
                                                // Just carry on, but log the issue so we can review later
                                                //
                                                Log.Error("Learning Object start with a part that is not Part A or Part 1");
                                                Log.Error("Section: " + s.Name + "-" + s.ExternalID);
                                                Log.Error("Page: " + page.Name);

                                                string learningObjectName = page.Name.Replace(", Part " + partLetter, "");

                                                learningObject = new ArtisanSection()
                                                {
                                                    ExternalID = s.ExternalID + "-tp-autoLO",
                                                    Name = learningObjectName,
                                                    Sections = new List<ArtisanSection>(),
                                                    Pages = new List<ArtisanPage>(),
                                                    SectionType = (int)ArtisanSectionType.LearningObject
                                                };

                                                s.Sections.Add(learningObject);
                                            }

                                            string learningObjectTitle = Util.StripLastComma(page.Name);

                                            learningObject.Pages.Add(page);
                                        }

                                        s.Pages = new List<ArtisanPage>();
                                    }
                                }

                                // Now that we've made all these learning objects, lets undo
                                // it in the case that we've created a serries of learning objects
                                // with just one page each
                                if (s.SectionType == (int)ArtisanSectionType.Sco)
                                {
                                    Log.Info("Cleaning SCO");
                                    List<ArtisanSection> sectionsWithOnePage = s.Sections.Where(l => l.Pages.Count() == 1).ToList();
                                    if (s.Sections.Count() > 0)
                                    {
                                        if (sectionsWithOnePage.Count() == s.Sections.Count())
                                        {
                                            Log.Info("Has sections with one page each - combining into one section");

                                            s.Sections[0].Name = s.Name;
                                            List<ArtisanPage> sp = sectionsWithOnePage.Select(ss => ss.Pages[0]).ToList();
                                            List<ArtisanSection> newSections = new List<ArtisanSection>() { s.Sections[0] };
                                            newSections[0].Pages = sp;

                                            s.Sections = newSections;
                                        }
                                    }
                                    processedSectionList.Add(s);
                                }
                                else if (s.SectionType == (int)ArtisanSectionType.Posttest)
                                {
                                    Log.Info("Found post test section");
                                    if (s.Name.Contains("Pre-test") || s.Name.Contains("Pre-Test"))
                                    {
                                        Log.Info("It is actually a pretest");
                                        if (processedSectionList.Count() > 0)
                                        {
                                            Log.Info("We have content already, save that as a new course");
                                            ArtisanCourse overviewCourse = new ArtisanCourse();
                                            overviewCourse.CopyFrom(c);
                                            overviewCourse.Sections = processedSectionList;
                                            overviewCourse.Name += " - Orientation";
                                            overviewCourse.Keywords += ", Orientation";
                                            Log.Info("Adding orientation to saveable courses");
                                            saveableSubCourses.Add(overviewCourse);
                                            hasPreContent = true;
                                        }

                                        Log.Info("Building pretest course");
                                        ArtisanCourse pretestCourse = new ArtisanCourse();
                                        pretestCourse.CopyFrom(c);
                                        pretestCourse.Sections = new List<ArtisanSection>() { s };
                                        s.SectionType = (int)ArtisanSectionType.Pretest;
                                        pretestCourse.Name += " - Pretest";
                                        pretestCourse.Keywords += ", Pretest";

                                        if (saveableSubCourses.Count() > 0)
                                        {
                                            Log.Info("Getting instructions from previous course");
                                            ArtisanCourse lastCourse = saveableSubCourses[saveableSubCourses.Count() - 1];
                                            if (lastCourse.Sections[lastCourse.Sections.Count() - 1].Name.Contains("Pre-test") ||
                                                lastCourse.Sections[lastCourse.Sections.Count() - 1].Name.Contains("Pre-Test"))
                                            {
                                                if (lastCourse.Sections.Count() > 0 &&
                                                    lastCourse.Sections[lastCourse.Sections.Count() - 1].Sections.Count() > 0 &&
                                                    lastCourse.Sections[lastCourse.Sections.Count() - 1].Sections[0].Pages.Count() > 0)
                                                {
                                                    ArtisanPage instructions = lastCourse.Sections[lastCourse.Sections.Count() - 1].Sections[0].Pages[0];
                                                    lastCourse.Sections.RemoveAt(lastCourse.Sections.Count() - 1);
                                                    pretestCourse.Sections[0].Description = Util.ConvertHtmlToDescription(instructions.Html);
                                                }

                                            }
                                        }
                                        Log.Info("Adding pretest to savable courses");
                                        saveableSubCourses.Add(pretestCourse);
                                        hasPretest = true;
                                    }
                                    else
                                    {
                                        Log.Info("It's a post test");
                                        if (processedSectionList.Count > 0)
                                        {
                                            Log.Info("We have content so lets save that as a content course");
                                            ArtisanCourse contentCourse = new ArtisanCourse();
                                            contentCourse.CopyFrom(c);
                                            contentCourse.Sections = processedSectionList;
                                            saveableSubCourses.Add(contentCourse);
                                        }
                                        Log.Info("Build the post test course");
                                        ArtisanCourse posttestCourse = new ArtisanCourse();
                                        posttestCourse.CopyFrom(c);
                                        posttestCourse.Sections = new List<ArtisanSection>() { s };
                                        posttestCourse.Name += " - Exam";
                                        posttestCourse.Keywords += ", Exam";
                                        posttestCourse.PassingScore = 70;

                                        if (saveableSubCourses.Count() > 0)
                                        {
                                            Log.Info("Get the instructions from the previous course");
                                            ArtisanCourse lastCourse = saveableSubCourses[saveableSubCourses.Count() - 1];
                                            if (lastCourse.Sections[lastCourse.Sections.Count() - 1].Name.Contains("Final Examination"))
                                            {
                                                if (lastCourse.Sections.Count() > 0 &&
                                                    lastCourse.Sections[lastCourse.Sections.Count() - 1].Sections.Count() > 0 &&
                                                    lastCourse.Sections[lastCourse.Sections.Count() - 1].Sections[0].Pages.Count() > 0)
                                                {
                                                    ArtisanPage instructions = lastCourse.Sections[lastCourse.Sections.Count() - 1].Sections[0].Pages[0];
                                                    lastCourse.Sections.RemoveAt(lastCourse.Sections.Count() - 1);
                                                    posttestCourse.Sections[0].Description = Util.ConvertHtmlToDescription(instructions.Html);
                                                }

                                            }
                                        }

                                        if (string.IsNullOrWhiteSpace(posttestCourse.Description))
                                        {
                                            posttestCourse.Description = testDescription;

                                        }

                                        SetExamSettings(posttestCourse);

                                        Log.Info("add the post test course");
                                        saveableSubCourses.Add(posttestCourse);
                                        hasPosttest = true;
                                    }


                                    Log.Info("Clear the processsed section list each time we hit a post or pre test so we can start fresh");
                                    processedSectionList = new List<ArtisanSection>();
                                }

                            }

                            if (saveableSubCourses.Count() == 0)
                            {
                                Log.Info("No saveable sub courses have been created");
                                saveableSubCourses.Add(c);
                            }
                            else
                            {
                                Log.Info("We have courses, check if there was any post content created");
                                if (processedSectionList.Count() > 0)
                                {
                                    if (hasPosttest)
                                    {
                                        Log.Info("We have post content");
                                        ArtisanCourse assignmentCourse = new ArtisanCourse();
                                        assignmentCourse.CopyFrom(c);
                                        assignmentCourse.Sections = processedSectionList;
                                        assignmentCourse.Name += " - Discussion";
                                        assignmentCourse.Keywords += ", Discussion";
                                        saveableSubCourses.Add(assignmentCourse);
                                        hasPostContent = true;
                                    }
                                    else
                                    {
                                        Log.Info("Just regular content");
                                        ArtisanCourse contentCourse = new ArtisanCourse();
                                        contentCourse.CopyFrom(c);
                                        contentCourse.Sections = processedSectionList;
                                        contentCourse.Keywords += ", Content";
                                        saveableSubCourses.Add(contentCourse);

                                        hasContent = true;
                                    }
                                }
                            }

                            List<ArtisanCourse> mergedCourses = new List<ArtisanCourse>();

                            if (!hasPreContent && !hasPostContent)
                            {
                                Log.Info("No pre or post content found, saving original");

                                Log.Info("Last Section:");
                                Log.Info(c.Sections[c.Sections.Count() - 1].Name);
                                if (c.Sections[c.Sections.Count() - 1].SectionType == (int)ArtisanSectionType.Posttest)
                                {
                                    ArtisanSection examSection = c.Sections[c.Sections.Count() - 1];
                                    Log.Info("Splitting out exam");
                                    ArtisanCourse exam = new ArtisanCourse();

                                    exam.CopyFrom(c);
                                    exam.CompletionMethod = ArtisanCourseCompletionMethod.Test;
                                    exam.Sections = new List<ArtisanSection>() { examSection };
                                    exam.Name += " - Exam";
                                    saveableSubCourses = new List<ArtisanCourse>();

                                    SetExamSettings(exam);

                                    saveableSubCourses.Add(exam);

                                    c.Sections.RemoveAt(c.Sections.Count() - 1);
                                    saveableSubCourses.Add(c);

                                }
                                else
                                {
                                    saveableSubCourses = new List<ArtisanCourse>() { c };
                                }

                            }
                            else if (hasPreContent && hasPostContent)
                            {
                                Log.Info("Pre and post content found, merging pre/post tests with content");
                                mergedCourses.Add(saveableSubCourses[0]);



                                
                                ArtisanCourse contentCourse = saveableSubCourses[2];
                                ArtisanCourse pretestCourse = saveableSubCourses[1];
                                ArtisanCourse posttestCouse = saveableSubCourses[3];

                                contentCourse.Sections.Insert(0, pretestCourse.Sections[0]);

                                SetExamSettings(posttestCouse);

                                mergedCourses.Add(contentCourse);
                                mergedCourses.Add(posttestCouse);
                                mergedCourses.Add(saveableSubCourses[4]);

                                saveableSubCourses = mergedCourses;

                            }
                            else if (hasPreContent && !hasPostContent)
                            {
                                Log.Info("Has pre content - merging content with pre/post tests");
                                ArtisanCourse preCourse = saveableSubCourses[0];
                                saveableSubCourses.RemoveAt(0);

                                ArtisanCourse contentCourse = saveableSubCourses[hasPretest ? 1 : 0];

                                if (hasPretest)
                                {
                                    contentCourse.Sections.Insert(0, saveableSubCourses[0].Sections[0]);
                                }

                                mergedCourses.Add(preCourse);
                                mergedCourses.Add(contentCourse);

                                if (hasPosttest)
                                {
                                    ArtisanCourse posttest = saveableSubCourses[hasPretest ? 2 : 1];
                                    SetExamSettings(posttest);
                                    mergedCourses.Add(posttest);
                                }

                                saveableSubCourses = mergedCourses;
                            }
                            else if (!hasPreContent && hasPostContent)
                            {
                                Log.Info("Has post content - merging content with pre/post tests");
                                ArtisanCourse postCourse = saveableSubCourses[saveableSubCourses.Count() - 1];
                                saveableSubCourses.RemoveAt(saveableSubCourses.Count() - 1);

                                ArtisanCourse contentCourse = saveableSubCourses[hasPretest ? 1 : 0];

                                if (hasPretest)
                                {
                                    contentCourse.Sections.Insert(0, saveableSubCourses[0].Sections[0]);
                                }

                                mergedCourses.Add(contentCourse);

                                if (hasPosttest)
                                {
                                    ArtisanCourse postest = saveableSubCourses[hasPretest ? 2 : 1];
                                    SetExamSettings(postest);
                                    mergedCourses.Add(postest);
                                }

                                mergedCourses.Add(postCourse);

                                saveableSubCourses = mergedCourses;
                            }




                            foreach (ArtisanCourse subC in saveableSubCourses)
                            {
                                currentCourse = subC;
                                string lastDescription = "";
                                bool isError = false;
                                if (subC.Description.Length > 1024)
                                {
                                    Log.Error("Truncating description - Course ID: " + subC.Id + " was: " + subC.Description);
                                    lastDescription = subC.Description;

                                    subC.Description = subC.Description.Substring(0, 1024);

                                    isError = true;
                                }

                                Log.Info("Saving course: " + subC.Name);
                                ArtisanCourse course = artisanController.SaveCourse(SymphonyLogin.CustomerID, SymphonyLogin.UserID, 0, subC).Data;

                                if (isError)
                                {
                                    Log.Error(String.Format("Error - Description turncated run sql: update ArtisanCourses set Description = '{0}' where ID = {1}",
                                        lastDescription,
                                        course.Id));
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Error("Could not save the course: " + currentCourse.Id + " " + currentCourse.Name);
                        Log.Error(e);
                    }

                    double duration = (DateTime.Now - start).TotalMilliseconds;
                    totalTime += duration;
                    count++;
                    double averageTime = duration / count;
                    Log.Info("Took: " + duration + "ms");
                    Log.Info("Average Time: " + averageTime + "ms");
                    Log.Info("Expected Completion: " + (DateTime.Now.AddMilliseconds(averageTime * (courseIdsArr.Count() - count))));

                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }

            Log.Info("Completed: " + DateTime.Now);
        }


        private void SetExamSettings(ArtisanCourse course)
        {
            course.PassingScore = 70;
            course.NavigationMethod = ArtisanCourseNavigationMethod.FullSequential;
            course.InactivityTimeout = 1200;
            course.TimeoutMethod = ArtisanCourseTimeoutMethod.ShowAlert;
            course.ReviewMethod = ArtisanCourseReviewMethod.ShowReview;

            foreach (ArtisanSection section in course.Sections)
            {
                if (section.SectionType == (int)ArtisanSectionType.Posttest)
                {
                    section.TestType = ArtisanTestType.Random;
                    if (string.IsNullOrWhiteSpace(section.Description))
                    {
                        section.Description = testDescription;
                    }
                    section.IsRandomizeAnswers = true;
                    section.IsReRandomizeOrder = true;
                    section.MarkingMethod = (int)ArtisanCourseMarkingMethod.HideAnswersAndFeedback;

                }
            }
        }

        private bool ReadTpSql(SqlConnection conn, string command, int tpCourseId, Func<SqlDataReader, bool> read)
        {
            bool result;

            command = command.Replace("@tpCourseIds", tpCourseId.ToString());


            using (SqlCommand cmd = new SqlCommand(command))
            {
                cmd.Connection = conn;
                cmd.Connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                result = read(reader);
                cmd.Connection.Close();
            }
            return result;
        }
    }
}
