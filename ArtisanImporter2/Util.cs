﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data = Symphony.Core.Data;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using Symphony.Core.Models;
using SubSonic;
using Symphony.Core.Extensions;
using System.IO;

namespace ArtisanImporter2
{
    class Util
    {
        public const string AssetTagPdf = "[PDF]";
        public const string AssetTagDoc = "[DOC]";
        public const string AssetTagPpt = "[PPT]";

        // default page template
        private static Data.ArtisanTemplate defaultTemplate = new Data.ArtisanTemplate(8);

        public static string GetDefaultHtml(string header, string content)
        {
            return defaultTemplate.Html.Replace("Header", header)
                                       .Replace(">Content<", ">" + content + "<");
        }

        public static int GetDefaultTemplateId()
        {
            return defaultTemplate.Id;
        }

        public static string ToTitleCase(string title)
        {
            return title;
            //return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(title.ToLower());
        }

        public static string StripLastComma(string a)
        {
            int commaIndex = a.LastIndexOf(',');
            if (commaIndex > 0)
            {
                return a.Substring(0, commaIndex);
            }
            return a;
        }

        public static string ConvertHtmlToDescription(string html)
        {
            Regex examInstructions = new Regex("<div.*class=\".*element_text\">(.*)</div>");
            Match m = examInstructions.Match(html.Replace("\r\n", ""));
            if (m.Success)
            {
                html = m.Groups[1].ToString();
            }

            html = html.Replace("Click &lsquo;Complete Lesson&rsquo;", "Click &lsquo;Next&rsquo;");

            return html;
        }

        public static string ProcessHtml(string html, Func<string, string> handleUrl, string[] selectors = null, string[] attributes = null)
        {
            if (selectors == null)
            {
                selectors = new string[] { "//*[@src]", "//*[@href]", "//*[@name='movie' and @value]", "//*[@name='movie' and @src]", "//*[@name='Movie' and @value]", "//*[@name='Movie' and @src]", "//*[@name='Src' and @value]" };
            }
            if (attributes == null)
            {
                attributes = new string[] { "href", "src", "value", "Src", "Value" };
            }
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            foreach (string selector in selectors)
            {
                HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(selector);
                if (nodes != null)
                {
                    foreach (HtmlNode node in nodes)
                    {
                        HtmlAttribute attribute = null;
                        foreach (string attrib in attributes)
                        {
                            attribute = node.Attributes[attrib];
                            if (attribute != null)
                            {
                                break;
                            }
                        }

                        if (attribute != null)
                        {
                            string url = attribute.Value.Replace("\n", "");
                            if (!url.StartsWith("http") && !url.StartsWith("#") && !url.StartsWith("mailto:"))
                            {
                                attribute.Value = handleUrl(url);
                            }
                        }
                    }
                }
            }

            return doc.DocumentNode.WriteTo();
        }
        public static Dictionary<string, List<ArtisanAssetImport>> GetExistingAssets(int customerId, int userId, string assetTag, Dictionary<string, List<ArtisanAssetImport>> existingAssets, DateTime? minCreatedOnDate = null)
        {

            if (assetTag != AssetTagDoc && assetTag != AssetTagPdf && assetTag != AssetTagPpt)
            {
                throw new Exception("Invalid asset tag use [DOC], [PDF], or [PPT]");
            }

            SqlQuery query = new Select()
                .From<Data.ArtisanAsset>()
                .Max(Data.ArtisanAsset.IdColumn, "Id")
                .OrderAsc("max([dbo].[ArtisanAssets].[Id])")
                .Where(Data.ArtisanAsset.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.ArtisanAsset.FromImportColumn).IsEqualTo(1)
                .And(Data.ArtisanAsset.CreatedByUserIdColumn).IsEqualTo(userId)
                .And(Data.ArtisanAsset.NameColumn).ContainsString(assetTag);


            if (minCreatedOnDate.HasValue)
            {
                query.And(Data.ArtisanAsset.CreatedOnColumn).IsGreaterThan(minCreatedOnDate);
            }

            query.Aggregates.Add(new Aggregate("REPLACE(STUFF(Name, PATINDEX('%(% of %)%', Name), 1000, ''), '" + assetTag + " ', '')", "OriginalName", AggregateFunction.GroupBy));

            foreach (TableSchema.TableColumn column in Data.ArtisanAsset.Schema.Columns)
            {
                if (column != Data.ArtisanAsset.IdColumn)
                {
                    query.GroupBy(column);
                }
            }

            List<ArtisanAssetImport> assets = query.ExecuteTypedList<ArtisanAssetImport>();

            foreach (ArtisanAssetImport asset in assets)
            {
                string assetName = asset.OriginalName.Trim();
                if (!existingAssets.Keys.Contains(assetName))
                {
                    existingAssets.Add(assetName, new List<ArtisanAssetImport>());
                }
                if (existingAssets[assetName].Where(a => a.Name == asset.Name).Count() == 0)
                {
                    existingAssets[assetName].Add(asset);
                }
            }

            return existingAssets;
        }
    }
}
