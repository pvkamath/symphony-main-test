﻿using PearlsReview.BLL;
using PearlsReview.QTI;
using SubSonic;
using Symphony.Core;
using Symphony.Core.Controllers;
using Symphony.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Data = Symphony.Core.Data;
using Models = Symphony.Core.Models;
using HC = PearlsReview.BLL;
using System.Configuration;
using System.Web.Security;
using System.Net;
using System.IO;
using System.Transactions;
using System.Web;
using System.Reflection;
using HtmlAgilityPack;
using System.Data.SqlClient;
using System.Collections.Concurrent;
using Symphony.ExternalSystemIntegration.HttpController;

namespace ArtisanImporter2
{
    public class HealthCareTopicFile {
        private List<ArtisanPage> _pages; 
        public int TopicID { get; set; }
        public string FilePath { get; set; }
        public string CourseNumber { get; set; }
        public string FileName { get { return Path.GetFileName(FilePath); } }
        public string Extension { get { return Path.GetExtension(FilePath); } }
        public List<ArtisanPage> Pages { get { return _pages; } set { _pages = value; } }
        public List<ArtisanAssetImport> AssetsForPages { get; set; }
        public override string ToString()
        {
            return "TopicID: " + TopicID + " CourseNumber: " + CourseNumber + " FilePath: " + FilePath;
        }
    }

    public class TopicForImport
    {
        public int ID { get; set; }
        public string CourseNumber { get; set; }
        public string CourseName { get; set; }
        public string Types { get; set; }
    }

    public class HealthCareImporter : Importer
    {

        private string HC_CATEGORY = ConfigurationManager.AppSettings["healthCareCategory"];
        private const string HC_CODE_NAME = "HC";
        private const string HC_FORMAT_DATE = "MM/dd/yyyy";

        private string basePath = ConfigurationManager.AppSettings["healthCareBasePath"];
        private string appPath = ConfigurationManager.AppSettings["appPath"];

        private string baseAssetsSourcePath = Path.Combine(ConfigurationManager.AppSettings["healthCareBasePath"], ConfigurationManager.AppSettings["healthCareFiles.Assets"]);
        private string fullPathArtisanAssetsUploadDirectory = ConfigurationManager.AppSettings["ArtisanAssetsUploadDirFullLocalPath"];
        private string pathArtisanAssetsUploadDirectory = ConfigurationManager.AppSettings["ArtisanAssetsUploadDirectory"];
        private string uriArtisanAssetsUpload = ConfigurationManager.AppSettings["ArtisanAssetsUploadURI"];

        private string healthCareAssetsServer = ConfigurationManager.AppSettings["healthCareAssetsServer"];
        private string healthCareAssetsUserName = ConfigurationManager.AppSettings["healthCareAssetsUserName"];
        private string healthCareAssetsPassword = ConfigurationManager.AppSettings["healthCareAssetsPassword"];
        private string healthCareAssetsBasePath = ConfigurationManager.AppSettings["healthCareAssetsBasePath"];
        private string healthCarePowerPoint = ConfigurationManager.AppSettings["healthCarePowerPoint"];
        private string healthCareDocs = ConfigurationManager.AppSettings["healthCareDocs"];
        private string healthCareSupplement = ConfigurationManager.AppSettings["healthCareSupplement"];
        private string healthCareXML = ConfigurationManager.AppSettings["healthCareXML"];
        private string healthCarePDF = ConfigurationManager.AppSettings["healthCarePDF"];
        private string healthCareImages = ConfigurationManager.AppSettings["healthCareImages"];
        private string healthCareCourseImages = ConfigurationManager.AppSettings["healthCareCourseImages"];
        private bool healthCareIgnoreInactive = bool.Parse(ConfigurationManager.AppSettings["healthCareIgnoreInactive"]);
        private bool healthCareShowCourseNumber = bool.Parse(ConfigurationManager.AppSettings["healthCareShowCourseNumber"]);
        private bool healthCareAutoLoadIds = bool.Parse(ConfigurationManager.AppSettings["healthCareAutoLoadIds"]);
        private bool healthCareAddCourseTypeToKeywords = bool.Parse(ConfigurationManager.AppSettings["healthCareAddCourseTypeToKeywords"]);

        private int PageCounter = -1;
        private int PageContentCounter = 1;
        private ArtisanController Controller;
        private string Root = string.Empty;
        private Random Random;
        private int CustomerID = 1;
        private int UserID = 147;
        ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["HealthCare"];

        Dictionary<int, HealthCareTopicFile> healthCareTopicFiles = new Dictionary<int, HealthCareTopicFile>();
        ConcurrentDictionary<string, List<ArtisanPage>> importedPages = new ConcurrentDictionary<string, List<ArtisanPage>>();
        Dictionary<string, List<ArtisanAssetImport>> existingAssetsDictionary = new Dictionary<string,List<ArtisanAssetImport>>();
        Dictionary<int, bool> hideAnswersCourse = new Dictionary<int, bool>();
        Dictionary<int, TopicForImport> topicsForImport = new Dictionary<int, TopicForImport>();

        #region Template

        private string audioAssetTemplate = @"
            <a href='{path}' target='_blank'>Download Course Audio</a>  
        ";

        private string linkedContentLink = @"
            <li><a href='{0}' target='_blank'>{1}</a></li>
        ";

        private string basePageContent = @"
            <div>
                <h3 calss='element1 element_text' style='overflow: hidden;'>
                    {0}
                </h3>
                <div class='p element2 element_text' style='overflow: audo;'>
                    {1}
                </div>
            </div>
        ";

        private string temporaryLinkedContent = @"
            <div>
                <h3 class='element1 element_text' style='overflow: hidden;'>
                    External Content
                </h3>
                <div class='p element2 element_text' style='overflow: auto;'>
                    This course contains required external content for you to review. Please study this material prior to
                    answering any questions. 

                    The links below will allow you to access the additional content:
                    <ul>
                        {0}
                    </ul> 
                </div>
            </div>
        ";

        private string template = @"
            <div id=""Main"" class=""Main"">
              <div class=""MainWrapper"">
                <div id=""Content"" class=""Content"">
                  <div class=""ContentWrapper"">
                    {0}
                  </div>
                </div>
              </div>
            </div>
<script type='text/javascript'>
var popupWindow = null;
function centeredPopup(url,winName,w,h,scroll){{
  var leftPosition = (screen.width) ? (screen.width-w)/2 : 0;
  var topPosition = (screen.height) ? (screen.height-h)/4 : 0;
  var settings = 'height='+h+',width='+w+',top='+topPosition+',left='+leftPosition+',scrollbars='+scroll+',resizable,location=no';
  window.open(url,winName,settings)
}}
</script>
";

        #endregion

        // This is a very slow query, but will get all the course ids based on the 
        // sql statements they have referenced in the document
        private string getAllCourseIdsQuery = @"
        with 
	        topiclisting
        as
	        (
		        -- 1 Hour DB courses (No facility no media type)
		        select
			        t.topicid,
			        t.course_number,
			        t.topicname,
			        'HTML 1hr' as topictype
		        from 
			        Topic t
		        where 
			        t.fla_cetype='DB' and t.[hours] <= 1 and mediatype is null and facilityid = -1
		        union
		        -- Multi Hour DB Courses  (No facility no media type)
		        select 
			        t.topicid,
			        t.course_number,
			        t.topicname,
			        'HTML Multi-HR' as topictype
		        from
			        Topic t
		        where 
			        t.fla_cetype='DB' and t.[hours] > 1 and t.mediatype is null and facilityid = -1
		        union
		        -- 1 Hour w/Audio courses (Any facility, only audio media type)	
		        select
			        t.topicid,
			        t.course_number,
			        t.topicname,
			        'HTML 1hr w/Audio' as topictype
		        from
			        Topic t
		        join
			        MediaContent c on c.topicid = t.topicid
		        where
			        t.fla_cetype='DB' and t.[hours] = 1 and lower(t.mediatype) = 'audio'
		        union
		        -- DB courses that have links to poweremail.oncourselearning.com - any facility or media type
		        select
			        t.topicid,
			        t.course_number,
			        t.topicname,
			        'LinkedContent' as topictype
		        from
			        Topic t
		        join
			        pages p on p.topicid = t.topicid and p.page_content like '%http://media.oncourselearning.com/%'  
		        where
			        t.active_ind = 1 and t.fla_cetype = 'DB' and t.course_number not like 'WEB%'
		        union	
		        -- DB courses for single facility. No media type
		        select
			        t.topicid,
			        t.course_number,
			        t.topicname,
			        'Facility Course' as topictype
		        from 
			        Topic t
		        where
			        t.fla_cetype='DB' and facilityid > 0 and t.mediatype is null
		        union
		        -- RadTech Courses... This seems to be looking for the same link as the Linked content courses
		        select
			        t.topicid,
			        t.course_number,
			        t.topicname,
			        'Rad Tech' as topictype
		        from
			        Topic t
		        join
			        pages p on p.topicid = t.topicid and p.page_content like '%http://media.oncourselearning.com/%'  
		        where
			        t.fla_cetype='DB'
		        union
		        -- PA Child Abuse - specific category
		        select
			        t.topicid,
			        t.course_number,
			        t.topicname,
			        'PA Child-Abuse' as topictype
		        from
			        Topic t
		        where
			        t.fla_cetype = 'DB' and
			        t.topicid in (
				        select topicid from CategoryLink where categoryid = 547
			        )
		        union
		        -- Webinar courses - no facility webinar only
		        select
			        t.topicid,
			        t.course_number,
			        t.topicname,
			        'Webinar' as topictype
		        from
			        Topic t
		        where
			        t.fla_cetype = 'DB' and facilityid = -1 and lower(t.mediatype) = 'webinar'
		        union
		        -- FCE webinar - no facility fce webinar only
		        select
			        t.topicid,
			        t.course_number,
			        t.topicname,
			        'FCE Webinar' as topictype
		        from
			        Topic t
		        where
			        t.fla_cetype = 'DB' and facilityid = -1 and lower(t.mediatype) = 'fce webinar'
		        union
		        -- Courses imported from file
		        select
			        t.topicid,
			        t.course_number,
			        t.topicname,
			        t.fla_cetype as topictype
		        from
			        Topic t
		        where
			        t.fla_cetype in ('PD', 'PP', 'DO') and facilityid = -1
	        )
        select
	        topicid,
	        course_number,
	        topicname,
	        stuff(
		        (
			        select distinct ',' + topictype
			        from topiclisting tli
			        where tli.topicid = tl.topicid
			        for xml path('')
		        ), 1, 1, '') as topictypes
        from 
	        topiclisting as tl
        group by topicid, course_number, topicname
";

        private string getHideAnswersCourses = @"
            select 
                t.topicid
            from
                topic t
            join 
                pages p on p.page_content like '%http://poweremail.oncourselearning.com/%' and p.topicid = t.topicid
            where
               t.topicid in (@topicids) 
";

        private string getCourseFilesSQL = @"
            select 
	            topicid,
	            course_number,
	            course_file,
	            fla_cetype,
	            foldername
	            from
		            (select
			            case 
				            when
					            t.Fla_CEType = 'PP' and t.course_number like 'PR%' and t.foldername != ''
				            then 
					            'pdf\\' + t.folderName + '.pdf'
				            when
					            t.Fla_CEType = 'PD' and t.docxfile != ''
				            then
					            'pdf\\' + docxfile
				            when 
					            t.Fla_CEType = 'PD' and t.docxfile = ''
				            then
					            'pdf\\' + t.foldername + '.pdf'
				            when
					            t.Fla_CEType = 'PP' and t.course_number not like 'PR%'
				            then
					            'pp\\' + foldername + '\\' + foldername + '_files\\' + foldername + '.ppt'
				            when
					            t.Fla_CEType = 'DO' and docxfile != '' and docxfile is not null
				            then
					            'docx\\' + foldername + '\\' + docxfile	
			            else 
				            null
			            end as course_file,
			            topicid,
			            course_number,
			            foldername,
			            fla_cetype,
			            docxfile,
			            docxhofile
		            from 
			            Topic t
		            where obsolete = 0 and fla_cetype != 'DB'
	            ) t
	            where
		            t.course_file is not null and t.topicid in (@topicids)";


       

        private string GetSharePath(string assetPath)
        {
            return string.Format(@"\\{0}{1}{2}", healthCareAssetsServer, healthCareAssetsBasePath, assetPath);
        }

        public void ImportCourse(int customerId, int userId, int cetopicid, int categoryId)
        {
            
            PageContentCounter = 1;
            int artisanCourseId = 0;

            SingleResult<ArtisanCourse> result = null;

            CustomerID = customerId;

            UserID = userId;

            Topic cetopic = Topic.GetTopicByID(cetopicid);

            if (healthCareIgnoreInactive)
            {
                if (cetopic.Avail_Ind == false || cetopic.Obsolete == true || cetopic.Active_Ind == false)
                {
                    Console.WriteLine("       >>> Course {0} is not active or obsolete. No import action required!", cetopicid);
                    return;
                }
            }


            Controller = new ArtisanController();

            // Want to track the topics in categories, not globally. 
            // Added category id to topic tracking so we can import a course multiple times
            // if needed for comparison. 
            // If the same category is always used, only one version of the course will be imported. 
            Data.ExternalSystemSourceTracking trackTopic = Select.AllColumnsFrom<Data.ExternalSystemSourceTracking>().Where(Data.ExternalSystemSourceTracking.SourceIdColumn).IsEqualTo(cetopicid)
                .And(Data.ExternalSystemSourceTracking.CategoryIDColumn).IsEqualTo(categoryId)
                .ExecuteSingle<Data.ExternalSystemSourceTracking>();

            bool courseExistsInCurrentCategory = false; 

            if (trackTopic != null && trackTopic.Id > 0 && trackTopic.ArtisanCourseId.HasValue) {
                Data.ArtisanCourse course = new Data.ArtisanCourse(trackTopic.ArtisanCourseId.Value);
                if (course.CategoryID == cetopicid) {
                    artisanCourseId = trackTopic.ArtisanCourseId.Value;
                    Console.WriteLine("          This couse has been previously imported: {0}, Updating", cetopicid);
                    courseExistsInCurrentCategory = true;
                }
            }

            ArtisanCourse ac = new ArtisanCourse();
            
            ac.Description = HtmlRemoval.StripQuotes(HtmlRemoval.StripTagsCharArray(cetopic.topic_summary));

            string courseObj = ""; 

            string stopObjectiveIndexString = "accreditation information";
            
            int stopObjIndex = cetopic.Objectives.ToLower().IndexOf(stopObjectiveIndexString);

            if (stopObjIndex > 0)
            {
                courseObj = cetopic.Objectives.Substring(0, stopObjIndex);
            }
            else
            {
                courseObj = cetopic.Objectives;
            }

            ac.Objectives = courseObj;
            ac.Accreditation = HtmlRemoval.StripQuotes(HtmlRemoval.StripTagsCharArray(cetopic.Accreditation));
            //ac.Name = "ImportTestRound3 " + cetopic.Course_Number + " " + cetopic.TopicName;
            ac.Name = (healthCareShowCourseNumber ? "(" + cetopic.Course_Number + ")" : "") + cetopic.TopicName;

            if (!string.IsNullOrWhiteSpace(cetopic.Subtitle))
            {
                ac.Name += ": " + cetopic.Subtitle;
            }

            ac.Keywords = cetopic.MetaKW;

            if (topicsForImport.ContainsKey(cetopic.TopicID) && healthCareAddCourseTypeToKeywords)
            {
                ac.Keywords += ", " + topicsForImport[cetopic.TopicID].Types;
            }

            ac.Keywords = cetopic.Course_Number + ", " + ac.Keywords;

            

            ac.PassingScore = cetopic.Pass_Score;
            ac.Sections = new List<ArtisanSection>();
            ac.NavigationMethod = ArtisanCourseNavigationMethod.FreeForm;
            ac.CompletionMethod = ArtisanCourseCompletionMethod.Test;
            ac.CertificateEnabled = false;
            ac.MarkingMethod = ArtisanCourseMarkingMethod.HideAnswers;
            
            ac.Parameters = "[]";
            //ac.ExternalID = cetopic.TopicID.ToString();

            ac.CategoryId = GetHealthCareDataSourceCategory();

            Symphony.Core.Data.ArtisanTheme defaultTheme = new Symphony.Core.Data.ArtisanTheme("Name", "BE Smooth");
            Symphony.Core.Data.ArtisanTheme solidTheme = new Symphony.Core.Data.ArtisanTheme("Name", "Solid - Red");
            ac.ThemeId = solidTheme != null && solidTheme.Id > 0 ? solidTheme.Id : defaultTheme.Id;

            ac.ReviewMethod = ArtisanCourseReviewMethod.HideReview;

            //add Objective section
            var des = new ArtisanSection()
            {
                Name = "Objectives",
                SectionType = (int)ArtisanSectionType.Objectives
            };
            ac.Sections.Add(des);

            // add section
            var sco = new ArtisanSection()
            {
                Name = "Course Content",
                SectionType = (int)ArtisanSectionType.Sco
            };
            
            sco.Sections = new List<ArtisanSection>();

            //add course content
            var learningObject = new ArtisanSection()
            {
                Name = "Course Content",
                SectionType = (int)ArtisanSectionType.LearningObject
            };
            sco.Sections.Add(learningObject);
            learningObject.Pages = new List<ArtisanPage>();
            List<TopicPage> pages = TopicPage.GetTopicPagesByTopicID(cetopicid, "");
            foreach (var page in pages)
            {
                string cepage = page.Page_Content;
                string cepageWithArtisanAssets = ImportImages(cepage, cetopic);
                AddContentPage(cepageWithArtisanAssets, learningObject);
            }

            if (healthCareTopicFiles.ContainsKey(cetopicid))
            {
                HealthCareTopicFile healthCareTopicFile = healthCareTopicFiles[cetopicid];
                learningObject.Pages.AddRange(GetPagesFromHealthCareTopicFile(healthCareTopicFile));
            }

            if (sco.Sections[0].Pages.Count > 0)
            {
                ac.Sections.Add(sco);
            }

            List<Sidebar> sidebars = Sidebar.GetSidebarsByTopicID(cetopicid, "");
            if (sidebars != null && sidebars.Count > 0)
            {
                PearlsReview.BLL.Category category = PearlsReview.BLL.Category.GetCategoryByID(cetopic.categoryid);
                bool isTextbook = category != null && category.Textbook;

                ArtisanSection sidebarSection = new ArtisanSection
                {
                    Name = isTextbook ? "Patient Handout" : "Sidebar",
                    Pages = new List<ArtisanPage>(),
                    SectionType = (int)ArtisanSectionType.LearningObject
                };
    
                foreach (Sidebar sb in sidebars)
                {
                    if (string.IsNullOrWhiteSpace(sb.Body))
                    {
                        continue; // ignoring blank pages
                    }

                    sidebarSection.Pages.Add(new ArtisanPage
                    {
                        Name = HtmlRemoval.StripQuotes(HtmlRemoval.StripTagsCharArray(sb.Title)),
                        PageType = (int)ArtisanPageType.Content,
                        Html = string.Format(template, string.Format(basePageContent, sb.Title, sb.Body))
                    });
                }

                if (sidebarSection.Pages.Count > 0)
                {
                    sco.Sections.Add(sidebarSection);
                }
            }

            if (!string.IsNullOrWhiteSpace(cetopic.Ref_Html))
            {
                sco.Sections.Add(new ArtisanSection() {
                    Name = "References",
                    SectionType = (int)ArtisanSectionType.LearningObject,
                    Pages = new List<ArtisanPage>() {
                        new ArtisanPage() {
                            Name = "References",
                            Html = string.Format(template, string.Format(basePageContent, "References", cetopic.Ref_Html)),
                            PageType = (int)ArtisanPageType.Content
                        }
                    }
                });
            }

            // get vignette desc 
            ViDefinition vinfo = ViDefinition.GetViDefinitionByTopicID(cetopicid);

            if (vinfo != null)
            {
                ac.NavigationMethod = ArtisanCourseNavigationMethod.FreeFormWithQuiz;

                // vignett root section
                var vig = new ArtisanSection()
                {
                    SectionType = (int)ArtisanSectionType.Sco,
                    Name = "Clinical Vignette"
                };
                ac.Sections.Add(vig);
                vig.Sections = new List<ArtisanSection>();

                var vigQuestion = new ArtisanSection()
                {
                    Name = "Clinical Vignette",
                    SectionType = (int)ArtisanSectionType.LearningObject,
                    PassingScore = 100,
                    Description = vinfo.Vignette_Desc,
                    IsQuiz = true,
                    IsSinglePage = true,
                    IsRandomizeQuizQuestions = false
                };

                vig.Sections.Add(vigQuestion);
                vigQuestion.Pages = new List<ArtisanPage>();

                GetQuestionPage(cetopicid, vigQuestion);

            }


            

            //add test
            TestDefinition testInfo = TestDefinition.GetTestDefinitionByTopicID(cetopicid);
            if (testInfo != null)
            {
                var testQuestion = new ArtisanSection()
                {
                    Name = "Posttest",
                    SectionType = (int)ArtisanSectionType.Posttest,
                    PassingScore = 100,
                    Description = "After reading the course material, carefully answer all of the following questions to earn your CE Credit.",
                    ExternalID = testInfo.TopicID.ToString(),
                    MaxQuestions = 999,
                    TestType = ArtisanTestType.Sequential
                };

                ac.Sections.Add(testQuestion);
                testQuestion.Pages = new List<ArtisanPage>();

                GetTestQuestionPage(cetopicid, testQuestion);


                // https://oncourselearning.atlassian.net/browse/OPP-105
                // As indicated in OPP-105, if a post test only contains 1 question, assume it's a question like:
                // "Did you watch either the live presentation or video recording of the webinar?"
                // Then we hide the feedback since it's not really required. 
                if (testQuestion.Pages.Count == 1)
                {
                    testQuestion.MarkingMethod = (int)ArtisanCourseMarkingMethod.HideAllFeedback;
                }

            }

            // save the course            
            System.Transactions.TransactionOptions trOptions = new System.Transactions.TransactionOptions();
            trOptions.IsolationLevel = System.Transactions.IsolationLevel.Serializable;

            if (cetopic.MediaType.ToLower() == "webinar" || cetopic.MediaType.ToLower() == "fce webinar" || cetopic.Video_Ind) {
                ac.ContentTypeID = (int)ArtisanContentType.Video;
            }
            else if (cetopic.MediaType.ToLower() == "audio" || cetopic.Audio_Ind)
            {
                ac.ContentTypeID = (int)ArtisanContentType.Audio;
            }
            else
            {
                ac.ContentTypeID = (int)ArtisanContentType.Unknown;
            }

            

            ac.ExternalCourseNumber = cetopic.Course_Number;
            ac.ExternalID = cetopic.Course_Number;
            ac.PopupMenu = GeneratePopupMenu(cetopic);


            // Removing this - https://oncourselearning.atlassian.net/browse/OPP-105
            // Previously we were hiding all feedback when a Rad Tech course was detected. 
            // Rad Tech courses were defined as any courses with external links to http://media.oncourselearning.com/
            // This applied the HideAllFeedback to more courses than expected.
            // 
            // The new approach, as defined in OPP-105 will be to hide all feedback when there is only 1 question in
            // the section as this will likely be a question like "Did you watch the video?"

            //if (hideanswerscourse.containskey(cetopic.topicid) ||
            //    (topicsforimport.containskey(cetopicid) && topicsforimport[cetopicid].types.contains("rad tech")))
            //{
            //    ac.markingmethod = artisancoursemarkingmethod.hideallfeedback;
            //}




            if (topicsForImport.ContainsKey(cetopic.TopicID) && topicsForImport[cetopic.TopicID].Types.Contains("LinkedContent"))
            {
                List<ArtisanPage> content = sco.Sections[0].Pages;
                List<ArtisanPage> newPages = new List<ArtisanPage>();
                ArtisanPage tempPage = new ArtisanPage() {
                    Name = "External Content (Requires Review)",
                    PageType = (int)ArtisanPageType.Content
                };
                List<HtmlNode> externalContentLinksWithErrors = new List<HtmlNode>();

                foreach (ArtisanPage p in content)
                {
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(p.Html);

                    HtmlNodeCollection nodeCollection = doc.DocumentNode.SelectNodes("//a[@href]");

                    Dictionary<string, List<string>> linkedContentPathSelection = new Dictionary<string, List<string>>();
                    if (nodeCollection != null)
                    {
                        foreach (HtmlNode node in nodeCollection)
                        {
                            HtmlAttribute attr = node.Attributes["href"];
                            string extension = Path.GetExtension(attr.Value);

                            try
                            {
                                switch (extension)
                                {
                                    case ".doc":
                                    case ".docx":
                                    case ".pdf":
                                    case ".html":
                                    case ".ppt":
                                    case ".pptx":
                                    case ".swf":
                                        using (WebClient wc = new WebClient())
                                        {
                                            Uri uri = new Uri(attr.Value);
                                            if (uri != null)
                                            {
                                                byte[] buffer;
                                                buffer = wc.DownloadData(uri);

                                                using (Stream stream = new MemoryStream(buffer))
                                                {
                                                    List<ArtisanPage> importedPages = new List<ArtisanPage>();

                                                    if (extension == ".pdf")
                                                    {
                                                        importedPages = Controller.ImportPDFDocumentToMultiplePDFPages(CustomerID, Path.GetFileName(attr.Value), null, 0, 0, null, null, "noscale", null, null, cetopic.Course_Number, stream, cetopic.Course_Number, appPath).Data;
                                                    } else {
                                                        importedPages = Controller.ImportDocument(CustomerID, Path.GetFileName(attr.Value), null, 0, 0, null, null, "noscale", null, null, cetopic.Course_Number, stream, cetopic.Course_Number, appPath).Data;
                                                    }
                                                    
                                                    if (importedPages.Count > 0)
                                                    {
                                                        newPages.AddRange(importedPages);
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    default:
                                        throw new Exception("Invalid extension for import: " + extension);
                                }
                            }
                            catch (Exception e)
                            {
                                Log.Error("COULD NOT IMPORT LINKED CONTENT: " + attr.Value + " FOR COURSE: " + cetopic.Course_Number + " " + cetopic.TopicName + " ERROR MSG: " + e);
                                externalContentLinksWithErrors.Add(node);
                            }
                        }
                    }
                    else
                    {
                        Log.Error("NO LINKED CONTENT FOUND FOR COURSE: " + cetopic.Course_Number + " " + cetopic.TopicName);
                    }
                }

                if (newPages.Count > 0)
                {
                    foreach (ArtisanPage p in content)
                    {
                        p.Name += " (OLD - Needs Revision)";
                    }

                    if (externalContentLinksWithErrors.Count > 0) {
                        string linkList = "";
                        foreach (HtmlNode n in externalContentLinksWithErrors) {
                            linkList += string.Format(linkedContentLink, n.Attributes["href"].Value, n.InnerHtml);
                        }

                        if (!string.IsNullOrEmpty(linkList)) {
                            tempPage.Html = string.Format(template, string.Format(temporaryLinkedContent, linkList));
                            sco.Sections[0].Pages.Add(tempPage);
                        }
                    }

                    sco.Sections[0].Pages.AddRange(newPages);
                }
            }


            // Last check on navigation method
            // Specific course where course number matches
            // XX-GANN-XX-XXXX
            Regex reg = new Regex("[a-zA-Z0-9]{2}-GANN-[a-zA-Z0-9]{2}-[a-zA-Z0-9]{4}");
            if (reg.IsMatch(cetopic.Course_Number))
            {
                ac.NavigationMethod = ArtisanCourseNavigationMethod.Sequential;
            }

            // Calculate credit hours
            // As per Cathy:
            //Put the value in CreditHours in ceadmin in Duration in Symphony with the changes noted below:
            // In ceadmin, any course where the Course Number starts with SLPA, AAQL, OT, FIT should be multiplied 
            // by 10 to get a value in hours. The rest of the courses are already in hours (or the equivalent). For all courses
            // that have a whole number of hours, you can then import that number directly into the Duration field in Symphony. 
            // To accommodate any with partial hours (0.5, 1.8, 5.2, etc), please multiply them by 60 to get a value in minutes. 
            // Then, import that number and change the drop down in the duration field to minutes.  Added 7/1.  If time permits.  
            // This item was previously identified in JIRA in Ticket 979
            Regex hoursReg = new Regex("^(SLPA|AAQL|OT|FIT)");
            decimal courseDurationInMinutes = 0;
            if (hoursReg.IsMatch(cetopic.Course_Number))
            {
                courseDurationInMinutes = cetopic.Hours * 10 * 60;
            }
            else
            {
                courseDurationInMinutes = cetopic.Hours * 60;
            }

            if (courseDurationInMinutes > 0)
            {
                ac.Duration = (int)Math.Round(courseDurationInMinutes);
                ac.IsUseAutomaticDuration = false;
            }


            result = Controller.SaveCourse(CustomerID, UserID, artisanCourseId, ac);
            
            if (artisanCourseId == 0)
            {
                artisanCourseId = result.Data.Id;
            }

            this.AddCategories(cetopic, artisanCourseId);
            this.AddAuthors(cetopicid, artisanCourseId);

            SaveTopicMetaData(cetopicid);
            
            ImportCourseImage(cetopic, artisanCourseId);

            if (courseExistsInCurrentCategory) {
                // we've successfully updated the course. Yey!
                Console.WriteLine("        UPDATE SUCCESS: Source course id: {0} :: Aritsan Course: {1} has been updated", cetopicid, artisanCourseId);
            } else {
                // We've created a new artisan course, lets track it. 
                Data.ExternalSystemSourceTracking trackingRecord = new Data.ExternalSystemSourceTracking() { ExternalSystemName = "HealthCare", SourceId = cetopicid, ArtisanCourseId = artisanCourseId, CategoryID = categoryId };
                trackingRecord.Save();
                Console.WriteLine("        IMPORT SUCCESS: Source course id: {0} :: New Aritsan Course: {1}", cetopicid, artisanCourseId);
            }
        }

        public List<ArtisanPage> GetPagesFromHealthCareTopicFile(HealthCareTopicFile topicFile)
        {
            if (topicFile.Pages != null && topicFile.Pages.Count > 0)
            {
                // Pages have already been generated during the current import
                return topicFile.Pages;
            }
            else if (topicFile.AssetsForPages != null && topicFile.AssetsForPages.Count > 0)
            {
                List<ArtisanPage> pages = new List<ArtisanPage>();
                // pages were imported as part of a previous import. Generate pages for the assets.
                foreach (ArtisanAssetImport asset in topicFile.AssetsForPages)
                {
                    Data.ArtisanPage page = new Symphony.Core.Data.ArtisanPage();
                    page.Html = string.Format(template, "<img style='max-width:100%' src='" + Utilities.ResolveUrl(pathArtisanAssetsUploadDirectory + asset.Id + ".png") + "' />");
                    page.PageType = (int)ArtisanPageType.Content;
                    page.Name = asset.Name;
                    page.CourseID = 0;

                    pages.Add(Model.Create<ArtisanPage>(page));
                }
                return pages;
            }
            Log.Error("Error with " + topicFile + " no pages could be generated");
            return new List<ArtisanPage>();
        }

        private void ImportCourseImage(Topic cetopic, int artisanCourseId)
        {

            Log.Info("Saving Course Image...");
            string courseImage = Path.Combine(GetSharePath(healthCareCourseImages), cetopic.Img_Name);
            if (File.Exists(courseImage))
            {
                using (Stream fileStream = File.OpenRead(courseImage))
                {
                    new AssociatedImageController().SaveAssociatedImage(CustomerID, "artisancourse", artisanCourseId, fileStream);
                }
            }
            else
            {
                Log.Error(string.Format("Could not load course image for id: {0}, course number: {1}, Filename: {2}", cetopic.TopicID, cetopic.Course_Number, courseImage));
            }
        }

        private string ImportImages(string cepage, Topic cetopic)
        {
            var resultCepage = cepage;
            HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
            htmlDoc.LoadHtml(cepage);
            var imgTags = htmlDoc.DocumentNode.SelectNodes("//img");

            if (imgTags != null)
            {
                foreach (var imgtag in imgTags)
                {
                    try
                    {
                        string originalImageTag = imgtag.OuterHtml;
                        var imgSource = imgtag.Attributes["src"].Value;
                        var newImgSource = "";
                        var newimageTag = this.CreateNewImage(imgtag, out newImgSource);
                        if (newimageTag != "")
                        {
                            resultCepage = resultCepage.Replace(imgSource, newImgSource);
                        }
                    }
                    catch (Exception ex)
                    {
                        // If a file doesn't exist or can't be uploaded, log it and move on. 
                        
                        Console.WriteLine(string.Format("Could not find image for topic id: {0}, course number: {1} ex: {2}", cetopic.TopicID, cetopic.Course_Number, ex));
                    }
                }
            }

            return resultCepage;
        }

        private string CreateNewImage(HtmlNode sourceImage, out string newImageSource)
        {
            string newArtisanAssetTag = "";
            newImageSource = "";
            // upload the assets
            string assetFile = Path.Combine(GetSharePath(healthCareImages), sourceImage.Attributes["src"].Value.Substring(sourceImage.Attributes["src"].Value.LastIndexOf('/') + 1));
            assetFile = System.Web.HttpUtility.UrlDecode(assetFile);

            
            if (!File.Exists(assetFile)) {
                throw new Exception(string.Format("        The image file doesn't exits: {0}", assetFile));
            }
            string name = Path.GetFileNameWithoutExtension(assetFile);
            string ext = Path.GetExtension(assetFile);
            SingleResult<ArtisanAsset> result = Controller.UploadArtisanAsset(CustomerID, assetFile, name, name, File.ReadAllBytes(assetFile), appPath, "");
            if (result.Success)
            {
                string newimage = string.Format("{0}{1}", name, ext);

                sourceImage.Attributes["src"].Value = uriArtisanAssetsUpload + newimage;
                newImageSource = pathArtisanAssetsUploadDirectory + string.Format("{0}{1}", result.Data.Id, ext);
                sourceImage.Attributes.Add("alt", newimage);
                newArtisanAssetTag = sourceImage.OuterHtml;
            }
            return newArtisanAssetTag;

        }

        /*
         * 
         * Removing this function as the DOC courses (DO) are generated directly from the associated pdf
         * The function below adds them as a link in the popupmenu for the course and leaves the course itself empty.
         * 
         * /
        //private void AddDocWord(Topic cetopic, int artisanCourseId)
        //{
        //    int targetSectionId = 0;
        //    ArtisanSection sectionSco = null;
        //    ArtisanSection sectionObjective = null;

        //    ArtisanCourse artisanCourse = Controller.GetCourse(CustomerID, UserID, artisanCourseId).Data;

        //    if (artisanCourse.Sections.Count(s => s.SectionType == 1) > 0)
        //    {
        //        sectionSco = artisanCourse.Sections.Where(s => s.SectionType == 1).First();
        //        if (sectionSco.Sections.Count(ss => ss.SectionType == 2) > 0)
        //        {
        //            sectionObjective = sectionSco.Sections.Where(ss => ss.SectionType == 2).First();
        //            targetSectionId = sectionObjective.Id;
        //        }
        //    }
        //    else
        //    {
        //        return;
        //    }

        //    string sourceDocFile = Path.Combine(GetSharePath(healthCareDocs), cetopic.DOCXFile);

        //    if (!File.Exists(sourceDocFile))
        //    {
        //        throw new Exception("                  File to Import DOC(X) doestn't exists: " + sourceDocFile);
        //    }
        //    else
        //    {

        //        //string nameOverrideDocxFile = uriArtisanAssetsUpload + strippedCetopicFolderName;

        //        MultipleResult<ArtisanPage> contentPages = Controller.ImportDocument(CustomerID, cetopic.DOCXFile, null, artisanCourseId, targetSectionId, null, null, null, null, null,
        //             cetopic.TopicID.ToString(), File.OpenRead(sourceDocFile), cetopic.DOCXFile, fullPathArtisanAssetsUploadDirectory);

        //        if (sectionObjective.Pages == null)
        //        {
        //            sectionObjective.Pages = new List<ArtisanPage>();
        //        }
        //        foreach (ArtisanPage ap in contentPages.Data)
        //        {
        //            var imageMatcher = Regex.Matches(ap.Html, "[0-9]+.png");
        //            if (imageMatcher.Count > 0)
        //            {
        //                ap.Html = ap.Html.Replace(imageMatcher[0].Value, uriArtisanAssetsUpload + imageMatcher[0].Value);
        //            }
        //            sectionObjective.Pages.Add(ap);
        //        }
        //        Controller.SaveCourse(CustomerID, UserID, artisanCourseId, artisanCourse);
        //    }
        //}

        /*
         * 
         * Not doing a search. If the file wasn't found in one of the 9 different 
         * cases where files appear to end up, log it as an error and get them to fix it. 
         * 
         * /
        //private string TryToFindFile(string baseAssetsSourcePath, string mediaFile)
        //{

        //    string tryFile1 = Path.Combine(baseAssetsSourcePath, mediaFile);

        //    /*try
        //    {
        //            string checkFile1 = @"\\10.65.80.112\webfiles\FileLocker\Education\SupplDocs\" + mediaFile;
        //            checkFile1 = File.Exists(checkFile1) ? checkFile1 : "";

        //            if (checkFile1 == "")
        //            {
        //                checkFile1 = @"\\10.65.80.112\webfiles\FileLocker\Education\PDF\" + mediaFile;
        //                checkFile1 = File.Exists(checkFile1) ? checkFile1 : "";
        //            }

        //            if (checkFile1 == "")
        //            {
        //                checkFile1 = @"\\10.65.80.112\webfiles\FileLocker\Education\DOCX\" + mediaFile;
        //                checkFile1 = File.Exists(checkFile1) ? checkFile1 : "";
        //            }

        //            if (checkFile1 != "")
        //            {
        //                File.Copy(checkFile1, baseAssetsSourcePath + mediaFile);
        //            }
                    
               
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //    }*/
            
        //    string newFilePathLocation = "";

        //    tryFile1 = Path.Combine(baseAssetsSourcePath, mediaFile);

        //    if (File.Exists(tryFile1)) return tryFile1;

        //    string[] listSearchFiles = Directory.GetFiles(baseAssetsSourcePath, mediaFile, SearchOption.AllDirectories);
        //    if (listSearchFiles.Length > 0)
        //    {
        //        foreach (string tryFile2 in listSearchFiles)
        //        {
        //            if (File.Exists(tryFile2)) return tryFile2;
        //        }
        //    }
        //    /*
        //    string baseFolder = ConfigurationManager.AppSettings["healthCareBasePath"].ToString();
        //    string sourceFolder = ConfigurationManager.AppSettings["healthCareFiles.Source"].ToString();
        //    string fullFolderName = Path.Combine(baseFolder, sourceFolder);

        //    string[] listSearchFilesInSupport = Directory.GetFiles(fullFolderName, mediaFile, SearchOption.AllDirectories);
        //    if (listSearchFilesInSupport.Length > 0)
        //    {
        //        foreach (string tryFile3 in listSearchFilesInSupport)
        //        {
        //            if (File.Exists(tryFile3)) return tryFile3;
        //        }
        //    }
        //    */

        //    return newFilePathLocation;
        //}

        /*
         * 
         * Removing this function as the ppt courses (PP) are generated directly from the associated pdf
         * The function below adds them as a link in the popupmenu for the course and leaves the course itself empty.
         * 
         * /
        //private void AddPptPresentation(Topic cetopic, int artisanCourseId)
        //{
        //    int targetSectionId = 0;
        //    ArtisanSection sectionSco = null;
        //    ArtisanSection sectionObjective = null;

        //    ArtisanCourse artisanCourse = Controller.GetCourse(CustomerID, UserID, artisanCourseId).Data;
        //    if (artisanCourse.Sections.Count(s => s.SectionType == 1) > 0)
        //    {
        //        sectionSco = artisanCourse.Sections.Where(s => s.SectionType == 1).First();
        //        if (sectionSco.Sections.Count(ss => ss.SectionType == 2) > 0)
        //        {
        //            sectionObjective = sectionSco.Sections.Where(ss => ss.SectionType == 2).First();
        //            targetSectionId = sectionObjective.Id;
        //        }
        //    }
        //    else
        //    {
        //        return;
        //    }

        //    string cetopicFolderName = cetopic.FolderName;
        //    if (cetopicFolderName.StartsWith("'") && cetopicFolderName.EndsWith("'"))
        //    {
        //        cetopicFolderName = cetopicFolderName.Substring(1, cetopicFolderName.Length - 2);
        //    }
        //    /*
        //     *   Not going to go out on a search for a file if it's not in the expected location. The convention for PPT files appears to be
        //     *   /Education/pp/cetopicFolderName/cetopicFolderName_files/cetopicFolderName.ppt if that doesn't exist, too bad. 
        //     *   Log it and send them a list of the missing files at the end. 
        //     */

        //    /*
        //    string sourcePptFile = TryToFindFile(baseAssetsSourcePath, cetopicFolderName);
        //    if (sourcePptFile == "")
        //    {
        //        sourcePptFile = TryToFindFile(baseAssetsSourcePath, cetopicFolderName.Substring(1, cetopicFolderName.Length - 2));
        //    }

        //    if (sourcePptFile == "")
        //    {
        //        string[] fileSelector = Directory.GetFiles(baseAssetsSourcePath, cetopicFolderName + ".ppt", SearchOption.AllDirectories);
        //        if (fileSelector.Length > 0)
        //        {
        //            sourcePptFile = fileSelector[0];
        //         }
        //    }
        //    */

        //    string sourcePptFile = Path.Combine(GetSharePath(healthCarePowerPoint), cetopicFolderName, cetopicFolderName + "_files", cetopicFolderName + ".ppt");
        //    string sourcePptFileNameOnly = Path.GetFileName(sourcePptFile);

        //    if (!File.Exists(sourcePptFile))
        //    {
        //        throw new Exception(" Power point file not found: " + sourcePptFile);
        //    }
        //    else
        //    {
        //        string strippedCetopicFolderName = cetopicFolderName.Replace("'", "");

        //        string nameOverrideDocxFile = uriArtisanAssetsUpload + strippedCetopicFolderName;

        //        MultipleResult<ArtisanPage> contentPages = Controller.ImportDocument(CustomerID, sourcePptFileNameOnly, null, artisanCourseId, targetSectionId, null, null, null, null, null,
        //             cetopic.TopicID.ToString(), File.OpenRead(sourcePptFile), sourcePptFileNameOnly, fullPathArtisanAssetsUploadDirectory);

        //        if (sectionObjective.Pages == null)
        //        {
        //            sectionObjective.Pages = new List<ArtisanPage>();
        //        }
        //        foreach (ArtisanPage ap in contentPages.Data)
        //        {
        //            var imageMatcher = Regex.Matches(ap.Html, "[0-9]+.png");
        //            if (imageMatcher.Count > 0)
        //            {
        //                ap.Html = ap.Html.Replace(imageMatcher[0].Value, uriArtisanAssetsUpload + imageMatcher[0].Value);
        //            }
        //            sectionObjective.Pages.Add(ap);
        //        }
        //        Controller.SaveCourse(CustomerID, UserID, artisanCourseId, artisanCourse);
        //    }
            
        //}

 
        //}
        /*
         * 
         * Removing this function as the pdf courses (PD) are generated directly from the associated pdf
         * The function below adds them as a link in the popupmenu for the course and leaves the course itself empty.
         * 
         */
        //private void AddPdfBook(Topic cetopic, int artisanCourseId)
        //{
        //    int targetSectionId = 0;
        //    ArtisanSection sectionSco = null;
        //    ArtisanSection sectionObjective = null;

        //    ArtisanCourse artisanCourse = Controller.GetCourse(CustomerID, UserID, artisanCourseId).Data;

        //    if (artisanCourse.Sections.Count(s => s.SectionType == 1) > 0)
        //    {
        //        sectionSco = artisanCourse.Sections.Where(s => s.SectionType == 1).First();
        //        if (sectionSco.Sections.Count(ss => ss.SectionType == 2) > 0)
        //        {
        //            sectionObjective = sectionSco.Sections.Where(ss => ss.SectionType == 2).First();
        //            targetSectionId = sectionObjective.Id;
        //        }
        //    }
        //    else
        //    {
        //        return;
        //    }

        //    List<ArtisanPage> contentPages = GeneratePagesFromPDF(cetopic);
            
        //    if (sectionObjective.Pages == null)
        //    {
        //        sectionObjective.Pages = new List<ArtisanPage>();
        //    }

        //    sectionObjective.Pages.AddRange(contentPages);

        //    Controller.SaveCourse(CustomerID, UserID, artisanCourseId, artisanCourse);

        //}

        //funzione che mi legge lo stream e mi crea un' array di byte
        public static byte[] ReadStream(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        private string GeneratePopupMenu(Topic cetopic)
        {
            bool isAudioMenu = false;

            List<Document> documents = Document.GetDocumentsByRelationIDAndType(cetopic.TopicID, "T", "");
            Dictionary<string, Document> documentDictionary = new Dictionary<string, Document>();
            List<string> documentPaths = new List<string>();
            List<string> assetList = new List<string>();

            List<MediaContent> mediaContent = MediaContent.GetMediaContentByTopicID(cetopic.TopicID, "");

            

            if (!string.IsNullOrWhiteSpace(cetopic.DOCXHoFile))
            {
                string path = Path.Combine(GetSharePath(healthCareDocs), cetopic.DOCXHoFile);

                documentDictionary.Add(path, new Document(0, cetopic.DOCXHoFile, "Handout", "Temp", DateTime.Now, "Temp"));
                
                documentPaths.Add(Path.Combine(GetSharePath(healthCareDocs), cetopic.FolderName, cetopic.DOCXHoFile));
            }
            /*
            if (documents != null && documents.Count > 0)
            {
                foreach (Document doc in documents)
                {
                    string docPath = Path.Combine(GetSharePath(healthCareSupplement), doc.FileName);

                    if (!documentDictionary.ContainsKey(docPath))
                    {
                        documentPaths.Add(docPath);
                        documentDictionary.Add(docPath, doc);
                    }
                    
                }
            }
             */

            foreach (MediaContent c in mediaContent)
            {
                ArtisanAsset asset = new ArtisanAsset()
                {
                    Url = c.asseturl
                };

                if (Path.GetExtension(c.asseturl) == ".mp3")
                {
                    isAudioMenu = true;
                }

                asset = Controller.SaveAsset(0, asset).Data;
                assetList.Add(string.Format("<li>{0}</li>", GetAssetTemplate(asset, true)));
            }
            
            /*
            foreach (string documentPath in documentPaths) {

                if (!File.Exists(documentPath))
                {
                    Log.Error("File is not in Source directory: " + documentPath);
                    return null;
                }

                byte[] bytes = File.ReadAllBytes(documentPath);

                string description = "Import from HealthCare";
                if (documentDictionary.ContainsKey(documentPath))
                {
                    description = documentDictionary[documentPath].Description;
                }


                Log.Info("Uploading supplemenatry document: " + documentPath);
                if (Path.GetExtension(documentPath).Length == 0)
                {
                    Log.Error("File has no extension. Content error: " + documentPath);
                }
                else
                {
                    ArtisanAsset asset = Controller.UploadArtisanAsset(CustomerID, Path.GetFileName(documentPath), Path.GetFileNameWithoutExtension(documentPath), description, bytes, appPath, cetopic.Course_Number).Data;
                    assetList.Add(string.Format("<li>{0}</li>", GetAssetTemplate(asset)));
                }
            }
             */

            if (assetList.Count > 0)
            {
                string title = isAudioMenu ? "Audio" : "Reference Library";

                return string.Format(@"
                    <div class='pdf-menu' data-title='{0}'>
                        <ul>
                            {1}
                        </ul>
                    </div>
                ", title, string.Join("", assetList));
            }

            return null;
        }
        /*
         * 
         * SaveTopicDouments has become GeneratePopupMenu
         * Only saving a popupmenu document if there id a DOCXHoFile which appears to be a "Handout" file for students
         * Not worrying about the document links since these appear to just be a historical record of the documents
         * used to create the course where one version is usually marked as "Current" and the rest are marked as "Old"
         * 
         * 
         */
        //private void SaveTopicDocuments(int cetopicid, int artisanCourseId)
        //{
        //    string pdfLinkList = "";

        //    Data.ArtisanCourse artisanCourseData = new Data.ArtisanCourse(artisanCourseId);
        //    ArtisanCourse artisanCourse = new ArtisanCourse();
        //    artisanCourse.CopyFrom(new Data.ArtisanCourse(artisanCourseId));

        //    string fullPathSourceDocument = "";
        //    //string fullPathDestinationDocument = "";

        //    List<PearlsReview.BLL.DocumentLink> documentLinks = PearlsReview.BLL.DocumentLink.GetDocumentLinksByTopicId(cetopicid);

        //    bool pdfOneNotHasBeenImported = false;
        //    int documentId = 0;

        //    foreach (PearlsReview.BLL.DocumentLink dl in documentLinks)
        //    {
        //        pdfOneNotHasBeenImported = true;

        //        PearlsReview.BLL.Document documentTopic = PearlsReview.BLL.Document.GetDocumentByID(dl.DocumentID);

        //        documentId = dl.DocumentID;

        //        if (documentTopic == null)
        //        {
        //            pdfOneNotHasBeenImported = true;
        //            continue;
        //        }
        //        if (string.IsNullOrEmpty(documentTopic.FileName)) continue;

        //        fullPathSourceDocument = Path.Combine(GetSharePath(healthCareSupplement), documentTopic.FileName.Trim());

        //        if (!File.Exists(fullPathSourceDocument))
        //        {
        //            throw new Exception("File is not in Source directory: " + fullPathSourceDocument);
        //        }
        //        //fullPathDestinationDocument = Path.Combine(ConfigurationManager.AppSettings["healthCareBasePath"].ToString(), ConfigurationManager.AppSettings["healthCareFiles.Destination"].ToString(), documentTopic.FileName.Trim());

        //        //File.Copy(fullPathSourceDocument, fullPathDestinationDocument, true);
        //        byte[] bytes = File.ReadAllBytes(fullPathSourceDocument);

        //        string fileNameImport = Path.GetFileName(fullPathSourceDocument);
        //        var artisanAssetData = new Data.ArtisanAsset(Data.ArtisanAsset.Columns.Filename, fileNameImport);
        //        ArtisanAsset asset = null;
        //        if (artisanAssetData != null || artisanAssetData.Id == 0)
        //        {
        //            asset = Controller.UploadArtisanAsset(CustomerID, fileNameImport, documentTopic.FileName, documentTopic.Description, bytes, fullPathArtisanAssetsUploadDirectory, documentTopic.DocumentID.ToString()).Data;
        //        }
        //        pdfLinkList += string.Format("<li>{0}</li>", GetAssetTemplate(asset));
        //    }

        //    if (pdfOneNotHasBeenImported == false)
        //    {

        //        Topic cetopic = Topic.GetTopicByID(cetopicid);
        //        if (string.IsNullOrEmpty(cetopic.DOCXFile))
        //            return;

        //        var newDocumentFile = cetopic.DOCXFile;
        //        var newDocumentFileName = Path.GetFileName(newDocumentFile);

        //        TryToFindFile(baseAssetsSourcePath, newDocumentFileName.Trim());

        //        fullPathSourceDocument = Path.Combine(baseAssetsSourcePath, newDocumentFileName.Trim());

        //        if (!File.Exists(fullPathSourceDocument))
        //        {
        //            throw new Exception("File is not in Source directory: " + fullPathSourceDocument);
        //        }
        //        //fullPathDestinationDocument = Path.Combine(ConfigurationManager.AppSettings["healthCareBasePath"].ToString(), ConfigurationManager.AppSettings["healthCareFiles.Destination"].ToString(), documentTopic.FileName.Trim());

        //        //File.Copy(fullPathSourceDocument, fullPathDestinationDocument, true);
        //        byte[] bytes = File.ReadAllBytes(fullPathSourceDocument);

        //        string fileNameImport = Path.GetFileName(fullPathSourceDocument);
        //        var artisanAssetData = new Data.ArtisanAsset(Data.ArtisanAsset.Columns.Filename, fileNameImport);
        //        ArtisanAsset asset = null;
        //        if (artisanAssetData != null || artisanAssetData.Id == 0)
        //        {
        //            asset = Controller.UploadArtisanAsset(CustomerID, fileNameImport, newDocumentFileName, newDocumentFileName, bytes, fullPathArtisanAssetsUploadDirectory, "0").Data;
        //        }
        //        pdfLinkList += string.Format("<li>{0}</li>", GetAssetTemplate(asset));
        //    }

        //    artisanCourseData.PopupMenu = string.Format("<div class='pdf-menu' data-title='Reference Library'><ul>{0}</ul></div>", pdfLinkList);
        //    artisanCourseData.Save();
        //}

        private string GetAssetTemplate(ArtisanAsset asset, bool isDocumentLibrary = false)
        {

            Regex styleTpl = new Regex("style=\"<.*>\"");
            string style = string.Format("style=\"{0} {1}\"",
                (asset.Width.HasValue ?
                "width: " + asset.Width.Value + "px;" :
                "max-width: 100%"),
                (asset.Height.HasValue ?
                "height: " + asset.Height.Value + "px;" :
                "max-height: 100%"));

            string template = asset.Template;
            if (isDocumentLibrary && Path.GetExtension(asset.Path) == ".mp3")
            {
                template = audioAssetTemplate;
            }

            return styleTpl.Replace(template, style)
                .Replace("{path}", asset.Path)
                .Replace("{description}", asset.Description)
                .Replace("{name}", asset.Name)
                .Replace("{element_id}", asset.Id + asset.Name)
                .Replace("{width}", asset.Width.ToString())
                .Replace("{height}", asset.Height.ToString())
                .Replace("{id}", asset.Id.ToString())
                .Replace("width=\"100%\"", "width=\"" + asset.Width + "\"")
                .Replace("height=\"100%\"", "height=\"" + asset.Height + "\"")
                .Replace("width=\"850px\"", "width=\"" + asset.Width + "\"")
                .Replace("height=\"700px\"", "height=\"" + asset.Height + "\""); ;

        }

        private void SaveTopicMetaData(int cetopicid)
        {
            List<Data.ExternalSystemMetaDataRecord> mdRecords = Select.AllColumnsFrom<Data.ExternalSystemMetaDataRecord>()
                .Where(Data.ExternalSystemMetaDataRecord.Columns.ExternalSourceCourseId).IsEqualTo(cetopicid)
                .And(Data.ExternalSystemMetaDataRecord.Columns.Name).IsEqualTo("course_number")
                .And(Data.ExternalSystemMetaDataRecord.Columns.ExternalSystemCode).IsEqualTo(HC_CODE_NAME).ExecuteTypedList<Data.ExternalSystemMetaDataRecord>();

            if (mdRecords.Count > 0) return; /** exit if course_number was imported, that means all other metadata has been saved **/

            Topic cetopic = Topic.GetTopicByID(cetopicid);
            SaveTopicMetaDataElement(cetopicid, "course_number", cetopic.Course_Number);
            SaveTopicMetaDataElement(cetopicid, "shortname", cetopic.shortname);
            SaveTopicMetaDataElement(cetopicid, "hours", cetopic.Hours.ToString());
            SaveTopicMetaDataElement(cetopicid, "minutes", cetopic.Minutes.ToString());
            SaveTopicMetaDataElement(cetopicid, "expiredate", cetopic.ExpireDate.HasValue ? cetopic.ExpireDate.Value.ToString(HC_FORMAT_DATE) : "");
            SaveTopicMetaDataElement(cetopicid, "lastupdate", cetopic.LastUpdate);
            SaveTopicMetaDataElement(cetopicid, "nosurvey", cetopic.NoSurvey.ToString());
            SaveTopicMetaDataElement(cetopicid, "surveyid", cetopic.SurveyID.ToString());
            SaveTopicMetaDataElement(cetopicid, "compliance", cetopic.Compliance.ToString());
            SaveTopicMetaDataElement(cetopicid, "metakw", cetopic.MetaKW.ToString());
            SaveTopicMetaDataElement(cetopicid, "metadesc", cetopic.MetaDesc.ToString());
            SaveTopicMetaDataElement(cetopicid, "mediatype", cetopic.MediaType.ToString());
            SaveTopicMetaDataElement(cetopicid, "textbook", cetopic.Textbook.ToString());
            SaveTopicMetaDataElement(cetopicid, "certid", cetopic.CertID.ToString());
            SaveTopicMetaDataElement(cetopicid, "grantby", cetopic.Method.ToString());
            SaveTopicMetaDataElement(cetopicid, "docxfile", cetopic.DOCXFile.ToString());
            SaveTopicMetaDataElement(cetopicid, "docxhofile", cetopic.DOCXHoFile.ToString());
            SaveTopicMetaDataElement(cetopicid, "audio_ind", cetopic.Audio_Ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "cert_cerp", cetopic.Cert_Cerp.ToString());
            SaveTopicMetaDataElement(cetopicid, "apn_ind", cetopic.Apn_Ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "icn_ind", cetopic.Icn_Ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "magnet_ind", cetopic.Magnet_Ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "active_ind", cetopic.Active_Ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "facilityid", cetopic.FacilityID.ToString());
            SaveTopicMetaDataElement(cetopicid, "alterid", cetopic.AlterID.ToString());
            SaveTopicMetaDataElement(cetopicid, "video_ind", cetopic.Video_Ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "online_ind", cetopic.Online_Ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "ebp_ind", cetopic.Ebp_Ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "ccm_ind", cetopic.Ccm_Ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "img_name", cetopic.Img_Name.ToString());
            SaveTopicMetaDataElement(cetopicid, "img_credit", cetopic.Img_Credit.ToString());
            SaveTopicMetaDataElement(cetopicid, "img_caption", cetopic.Img_Caption.ToString());
            SaveTopicMetaDataElement(cetopicid, "views", cetopic.Views.ToString());
            SaveTopicMetaDataElement(cetopicid, "primaryviews", cetopic.PrimaryViews.ToString());
            SaveTopicMetaDataElement(cetopicid, "pagereads", cetopic.PageReads.ToString());
            SaveTopicMetaDataElement(cetopicid, "featured", cetopic.Featured.ToString());
            SaveTopicMetaDataElement(cetopicid, "online_cost", cetopic.online_Cost.ToString());
            SaveTopicMetaDataElement(cetopicid, "offline_cost", cetopic.offline_Cost.ToString());
            SaveTopicMetaDataElement(cetopicid, "notest", cetopic.notest.ToString());
            SaveTopicMetaDataElement(cetopicid, "rev", cetopic.rev.ToString());
            SaveTopicMetaDataElement(cetopicid, "urlmark", cetopic.urlmark.ToString());
            SaveTopicMetaDataElement(cetopicid, "uce_ind", cetopic.uce_Ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "hold_ind", cetopic.hold_ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "last_upt", cetopic.LastUpdate.ToString());
            SaveTopicMetaDataElement(cetopicid, "rating_total", cetopic.rating_total.ToString());
            SaveTopicMetaDataElement(cetopicid, "rating_avg", cetopic.rating_avg.ToString());
            SaveTopicMetaDataElement(cetopicid, "rating_num_comment", cetopic.rating_num_comment.ToString());
            SaveTopicMetaDataElement(cetopicid, "prepaid_ind", cetopic.prepaid_ind.ToString());
            SaveTopicMetaDataElement(cetopicid, "cme_ind", cetopic.CMEInd.ToString());
            SaveTopicMetaDataElement(cetopicid, "cme_sponsor_ind", cetopic.CMESponsorInd.ToString());
            SaveTopicMetaDataElement(cetopicid, "cme_sponsor_name", cetopic.CMESponsorName.ToString());
            SaveTopicMetaDataElement(cetopicid, "topic_hourlong", cetopic.Topic_hourlong.ToString());
            SaveTopicMetaDataElement(cetopicid, "topic_hourshort", cetopic.Topic_hourshort.ToString());
            SaveTopicMetaDataElement(cetopicid, "releasedate", cetopic.ReleaseDate.HasValue ? cetopic.ReleaseDate.Value.ToString(HC_FORMAT_DATE) : "");
            SaveTopicMetaDataElement(cetopicid, "webinar_start", cetopic.Topic_hourshort.ToString());
            SaveTopicMetaDataElement(cetopicid, "webinar_end", cetopic.Topic_hourshort.ToString());

            SavePeopleInfo(cetopicid, "person_EditorReviewer", "E");
            SavePeopleInfo(cetopicid, "person_FormerPlanner", "F");
            SavePeopleInfo(cetopicid, "person_LeadPlanner", "L");
            SavePeopleInfo(cetopicid, "person_AssistantPlanner", "N");
            SavePeopleInfo(cetopicid, "person_PrimaryEditor", "P");
            SavePeopleInfo(cetopicid, "person_TargetAudienceRep", "T");
            SavePeopleInfo(cetopicid, "person_RelevantContentExperts", "X");

            Console.WriteLine("               Saved meta data for this course [source: topic table] : {0}", cetopicid);

            //PearlsReview.BLL.CertificateDefinition certificateDefinition = PearlsReview.BLL.CertificateDefinition.GetCertificateDefinitionByID(cetopic.CertID);
            List<PearlsReview.BLL.MediaContent> mediaContentForTopic = PearlsReview.BLL.MediaContent.GetMediaContentByTopicID(cetopicid, "");
            int i = 0;
            foreach (MediaContent mc in mediaContentForTopic)
            {
                i++;
                if (mc.mediatype == null) {
                    Console.WriteLine(string.Format("            !!! Media Type is NULL? description: {0}", mc.description));
                }
                SaveTopicMetaDataElement(cetopicid, string.Format("{{ \"table\":\"Mediacontent\", \"column\": \"mediatype\", \"recordid\": \"{0}\" }}", i), mc.mediatype);
                SaveTopicMetaDataElement(cetopicid, string.Format("{{ \"table\":\"Mediacontent\", \"column\": \"asseturl\", \"recordid\": \"{0}\" }}", i), mc.asseturl);
                SaveTopicMetaDataElement(cetopicid, string.Format("{{ \"table\":\"Mediacontent\", \"column\": \"description\", \"recordid\": \"{0}\" }}", i), mc.description);
            }

            Console.WriteLine("               Saved meta data for this course [source: Mediacontent table] : {0}", cetopicid);

            //PearlsReview.BLL.Document documentsForTopic = PearlsReview.BLL.Document.ge

        }

        private void SavePeopleInfo(int cetopicid, string personType, string personTypeShortName)
        {
            List<Data.ExternalSystemMetaDataRecord> mdRecords = Select.AllColumnsFrom<Data.ExternalSystemMetaDataRecord>()
                .Where(Data.ExternalSystemMetaDataRecord.Columns.ExternalSourceCourseId).IsEqualTo(cetopicid)
                .And(Data.ExternalSystemMetaDataRecord.Columns.Name).ContainsString(personType)
                .And(Data.ExternalSystemMetaDataRecord.Columns.ExternalSystemCode).IsEqualTo(HC_CODE_NAME).ExecuteTypedList<Data.ExternalSystemMetaDataRecord>();

            if (mdRecords.Count == 0)
            {
                List<ResourcePerson> resourcePeople = ResourcePerson.GetResourcePersonsByTopicIDAndType(cetopicid, personTypeShortName, "");
                var i = 0;
                foreach (ResourcePerson rp in resourcePeople)
                {
                    i++;
                    var mdRecord = new Data.ExternalSystemMetaDataRecord() { ExternalSystemCode = HC_CODE_NAME, ExternalSourceCourseId = cetopicid, Name = personType + i.ToString(), ValueX = rp.Fullname };
                    mdRecord.Save();
                }
            }
            
        }

        private void SaveTopicMetaDataElement(int cetopicid, string elementName, string elementValue)
        {
            List<Data.ExternalSystemMetaDataRecord> mdRecords = Select.AllColumnsFrom<Data.ExternalSystemMetaDataRecord>()
                .Where(Data.ExternalSystemMetaDataRecord.Columns.ExternalSourceCourseId).IsEqualTo(cetopicid)
                .And(Data.ExternalSystemMetaDataRecord.Columns.Name).IsEqualTo(elementName)
                .And(Data.ExternalSystemMetaDataRecord.Columns.ExternalSystemCode).IsEqualTo(HC_CODE_NAME).ExecuteTypedList<Data.ExternalSystemMetaDataRecord>();

            if (mdRecords.Count > 0)
            {
                Data.ExternalSystemMetaDataRecord mdRecord = mdRecords[0];
                mdRecord.ValueX = elementValue;
                mdRecord.Save();
            }
            else 
            {
                var mdRecord = new Data.ExternalSystemMetaDataRecord() { ExternalSystemCode = HC_CODE_NAME, ExternalSourceCourseId = cetopicid, Name = elementName, ValueX = elementValue };
                mdRecord.Save();
            }
 
        }

        private bool CheckIfTopicHasBeenImported(int cetopicid)
        {
            Data.ExternalSystemSourceTracking trackingRecord = new Data.ExternalSystemSourceTracking(Data.ExternalSystemSourceTracking.Columns.SourceId, cetopicid);
            return trackingRecord.ArtisanCourseId > 0;
        }

        private int GetHealthCareDataSourceCategory()
        {
            Data.Category hcDataCategory = new Data.Category(Data.Category.Columns.Name, HC_CATEGORY);
            if (hcDataCategory == null || string.IsNullOrEmpty(hcDataCategory.Name))
            {
                hcDataCategory = new Data.Category();
                hcDataCategory.Name = HC_CATEGORY;
                hcDataCategory.ParentCategoryID = 0;
                hcDataCategory.CategoryTypeID = (int)CategoryType.Artisan;
                hcDataCategory.CustomerID = CustomerID;
                hcDataCategory.Save(UserID);
            }

            return hcDataCategory.Id;
        }

        private void UpdateAuthors(Topic cetopic, int artisanCourseId)
        {
            List<ResourcePerson> resourcePersonAuthors = ResourcePerson.GetResourcePersonsByTopicIDAndType(cetopic.TopicID, "A", "");
            foreach (ResourcePerson resourcePerson in resourcePersonAuthors)
            {
                var userByEmail = new Data.User(Data.User.Columns.Email, resourcePerson.Email);
                List<Data.ArtisanCourseAuthorsLink> allAuthorsForThisTopicLinks = Select.AllColumnsFrom<Data.ArtisanCourseAuthorsLink>()
                    .Where(Data.ArtisanCourseAuthorsLink.Columns.UserId).IsEqualTo(userByEmail.Id)
                    .And(Data.ArtisanCourseAuthorsLink.Columns.ArtisanCourseId).IsEqualTo(0).ExecuteTypedList<Data.ArtisanCourseAuthorsLink>();
                foreach (Data.ArtisanCourseAuthorsLink oneAuthorLink in allAuthorsForThisTopicLinks)
                {
                    oneAuthorLink.ArtisanCourseId = artisanCourseId;
                    oneAuthorLink.Save();
                }
            }
        }

        private void AddCategories(Topic cetop, int artisanCourseId)
        {

            HC.Category secondCategoryByTopic = null;
            Data.SecondaryCategory categoryStored = null;

            if (cetop.categoryid > 0)
            {
                secondCategoryByTopic = HC.Category.GetCategoryByID(cetop.categoryid);

                categoryStored = new Data.SecondaryCategory(Data.SecondaryCategory.Columns.Name, secondCategoryByTopic.Title);
                if (categoryStored.Id == 0)
                {

                    Data.SecondaryCategory secondaryCategory = new Data.SecondaryCategory(Data.SecondaryCategory.Columns.Name, secondCategoryByTopic.Title);
                    if (secondaryCategory.Id == 0)
                    {
                        secondaryCategory = new Data.SecondaryCategory() { Id = 0, Name = secondCategoryByTopic.Title };
                        secondaryCategory.Save();
                    }

                    categoryStored = new Data.SecondaryCategory(Data.SecondaryCategory.Columns.Name, secondCategoryByTopic.Title);
                    int secondaryCategoryId = categoryStored.Id;

                    Data.ArtisanCourseSecondaryCategoryLink aclnk = new Data.ArtisanCourseSecondaryCategoryLink() { ArtisanCourseId = artisanCourseId, SecondaryCategoryId = secondaryCategoryId };
                    aclnk.Save();

                }
            }

            List<HC.Category> listOfAddionalCategories = HC.Category.GetCategoriesByTopicID(cetop.TopicID, "");
            foreach (HC.Category category in listOfAddionalCategories)
            {
                //if (secondCategoryByTopic != null)
                //{
                //    if (category.Title == secondCategoryByTopic.Title) continue;
                //}

                Data.SecondaryCategory additionalCategory = new Data.SecondaryCategory(Data.SecondaryCategory.Columns.Name, category.Title);
                if (additionalCategory.Id == 0)
                {
                    additionalCategory = new Data.SecondaryCategory() { Id = 0, Name = category.Title };
                    additionalCategory.Save();
                }

                categoryStored = new Data.SecondaryCategory(Data.SecondaryCategory.Columns.Name, category.Title);
                int additionalCategoryId = categoryStored.Id;

                Data.ArtisanCourseSecondaryCategoryLink link = Select.AllColumnsFrom<Data.ArtisanCourseSecondaryCategoryLink>().Where(Data.ArtisanCourseSecondaryCategoryLink.ArtisanCourseIdColumn).IsEqualTo(artisanCourseId)
                    .And(Data.ArtisanCourseSecondaryCategoryLink.SecondaryCategoryIdColumn).IsEqualTo(additionalCategoryId)
                    .ExecuteSingle<Data.ArtisanCourseSecondaryCategoryLink>();

                if (link == null || link.Id == 0)
                {
                    Data.ArtisanCourseSecondaryCategoryLink aclnk = new Data.ArtisanCourseSecondaryCategoryLink() { ArtisanCourseId = artisanCourseId, SecondaryCategoryId = additionalCategoryId };
                    aclnk.Save();
                }

            }

        }

        private void AddAuthors(int topicId, int artisanCourseId)
        {
            List<ResourcePerson> resourcePersonAuthors = ResourcePerson.GetResourcePersonsByTopicIDAndType(topicId, "A", "");

            try
            {
                foreach (ResourcePerson resourcePerson in resourcePersonAuthors)
                {
                    if (string.IsNullOrEmpty(resourcePerson.Email)) continue;
                    var trimmedResourceUserEmail = resourcePerson.Email.Trim();

                    Data.User userByEmail = new Data.User(Data.User.Columns.Email, trimmedResourceUserEmail);
                    if (userByEmail.Id > 0)
                    {
                        userByEmail.IsAuthor = true;
                        userByEmail.Save();

                        var listCourseAuthorLinks = Select.AllColumnsFrom<Data.ArtisanCourseAuthorsLink>()
                            .Where(Data.ArtisanCourseAuthorsLink.Columns.UserId).IsEqualTo(userByEmail.Id)
                            .And(Data.ArtisanCourseAuthorsLink.Columns.ArtisanCourseId).IsEqualTo(artisanCourseId).ExecuteTypedList<Data.ArtisanCourseAuthorsLink>();
                        if (listCourseAuthorLinks.Count == 0)
                        {
                            var artisanCourseAuthorLink = new Data.ArtisanCourseAuthorsLink() { UserId = userByEmail.Id, ArtisanCourseId = artisanCourseId };
                            artisanCourseAuthorLink.Save();
                        }
                    }
                    else
                    {
                        userByEmail = new Data.User()
                        {
                            Id = 0,
                            IsAuthor = true,
                            CustomerID = CustomerID,
                            Username = trimmedResourceUserEmail,
                            EmployeeNumber = trimmedResourceUserEmail,
                            ModifiedBy = "admin",
                            CreatedBy = "admin",
                            Password = Guid.NewGuid().ToString(),
                            FirstName = resourcePerson.RFirstName,
                            LastName = resourcePerson.RLastName,
                            Email = trimmedResourceUserEmail
                        };
                        //userByEmail.Save();
                        User userByEmailModel = new User();

                        //userByEmailModel.CopyFrom(userByEmail);

                        UserController userController = new UserController();
                        Data.Customer customer = new Data.Customer(CustomerID);

                        HttpCommunicator communicator = new HttpCommunicator();

                        List<string> ExternalParameters = new List<string>();
                        ExternalParameters.Add(userByEmail.Username);

                        ExternalParameters.Add(trimmedResourceUserEmail);
                        ExternalParameters.Add(userByEmail.FirstName);
                        ExternalParameters.Add(userByEmail.LastName);
                        ExternalParameters.Add(userByEmail.Email);

                        communicator.CommunicateToSystem("HealthCare.Import", ExternalParameters);

                        userByEmail.Save();

                        /*
                         * POST http://localhost:58599/services/customer.svc/users/0/customer/1 HTTP/1.1
                         * {"id":"0","username":"kykyuser","password":"password1","employeeNumber":"392737333","firstName":"kyky","middleName":"","lastName":"user","newHireIndicator":false,
                         * "email":"kykyuser@yahoo.com","nmlsNumber":"","supervisorId":0,"secondarySupervisorId":0,"reportingSupervisorId":0,"statusId":1,
                         * "locationId":0,"jobRoleId":0,"timeZone":"","notes":"","isLockedOut":false,"isAuthor":false,"speciality":"","applicationPermissions":["Customer - User"]}
                         */

                        //HttpRequestToSymphonyToCreateUser(userByEmailModel);

                        //userController.Save(userByEmailModel);
                        //userController.CreateNewASPNetUser(userByEmailModel, customer.SubDomain, false);
                        
                        //Membership.CreateUser(trimmedResourceUserEmail, Guid.NewGuid().ToString(), trimmedResourceUserEmail, "boo", "bobo", false, out status);

                        userByEmail = new Data.User(Data.User.Columns.Email, trimmedResourceUserEmail);

                        Data.AuthorDetail authorDetail = new Data.AuthorDetail();
                        authorDetail.UserId = userByEmail.Id;
                        authorDetail.Speciality = HtmlRemoval.StripTagsRegex(resourcePerson.Specialty);

                        authorDetail.Save();

                        Data.ArtisanCourseAuthorsLink artisanCourseAuthorLink = new Data.ArtisanCourseAuthorsLink() { UserId = userByEmail.Id, ArtisanCourseId = artisanCourseId };
                        artisanCourseAuthorLink.Save();

                        Data.UserDataField userDataFieldSuffix = Select.AllColumnsFrom<Data.UserDataField>()
                            .Where(Data.UserDataField.Columns.CodeName).IsEqualTo("suffix")
                            .ExecuteSingle<Data.UserDataField>();

                        // save any degrees to the "Suffix" UserDataFieldUserMap
                        var degrees = new Data.UserDataFieldUserMap()
                        {
                            UserID = userByEmail.Id,
                            UserDataFieldID = userDataFieldSuffix.Id,
                            UserValue = resourcePerson.Degrees
                        };

                        degrees.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(" !!!! Exception {0}", ex.Message);
            }
        }

        private void HttpRequestToSymphonyToCreateUser(User userByEmailModel)
        {
            return;
        }

        //methdo to add course pages here
        private void AddContentPage(string cepage, ArtisanSection learningObject)
        {
            // List<ArtisanAsset> assets = new List<ArtisanAsset>();
            ArtisanPage ap = new ArtisanPage()
            {
                Name = string.Format("Page {0}", PageContentCounter++),
                Html = string.Format(template, cepage),
                PageType = (int)ArtisanPageType.Content,
                Id = PageCounter--
            };


            // change ap name 
            //if (ap.Name.EndsWith("-S"))
            //{
            //    ap.Name = "Summary";
            //}
            //else if (ap.Name.EndsWith("-I"))
            //{
            //    ap.Name = "Introduction";
            //}

            //add page
            learningObject.Pages.Add(ap);


        }

        //to add question page
        private void GetQuestionPage(int CETopicid, ArtisanSection vigSection)
        {
            ArtisanQuestionType questionType = ArtisanQuestionType.MultipleChoice;


            // AccessCETopic ac = new AccessCETopic();
            List<QTIQuestionObject> questions = Test.GetVignetteQuestionObjectListByTopicID(CETopicid, "");
            Dictionary<string, QTIQuestionObject> questionDictionary = questions.ToDictionary(q => q.cQuestionID);


            int questionCount = 0;
            foreach (var item in questions)
            {

                bool isHeader =
                    string.IsNullOrWhiteSpace(item.cAnswer_A_Text) &&
                    string.IsNullOrWhiteSpace(item.cAnswer_B_Text) &&
                    string.IsNullOrWhiteSpace(item.cAnswer_C_Text) &&
                    string.IsNullOrWhiteSpace(item.cAnswer_D_Text) &&
                    string.IsNullOrWhiteSpace(item.cAnswer_E_Text);

                if (isHeader) {
                    continue;
                }
                
                //add answers
                var answers = new List<ArtisanAnswer>();
                var j = 0;
                var a = new List<string>();
                a.Add(HttpUtility.HtmlEncode(item.cAnswer_A_Text));
                a.Add(HttpUtility.HtmlEncode(item.cAnswer_B_Text));
                a.Add(HttpUtility.HtmlEncode(item.cAnswer_C_Text));
                a.Add(HttpUtility.HtmlEncode(item.cAnswer_D_Text));
                a.Add(HttpUtility.HtmlEncode(item.cAnswer_E_Text));
                int correctAnswer = item.cCorrectAnswer[0] - 65;


                for (int i = 0; i < 5; i++)
                {
                    if (a[i] != "")
                    {


                        string b = a[i];
                        answers.Add(new ArtisanAnswer()
                        {
                            Text = a[i],
                            IsCorrect = (i == correctAnswer),
                            Sort = j++,
                        });
                    }
                }

                string questionText = item.cQuestionText;
                if (!string.IsNullOrWhiteSpace(item.cHeaderID))
                {
                    if (questionDictionary.ContainsKey(item.cHeaderID))
                    {
                        questionText = questionDictionary[item.cHeaderID].cQuestionText + "<br/>" + questionText;
                    }
                }

                ArtisanPage ap = new ArtisanPage()
                {
                    Name = "Question " + ++questionCount,
                    Html = questionText,
                    CorrectResponse = item.cCorrectAnswerFeedback,
                    IncorrectResponse = item.cCorrectAnswerFeedback,
                    PageType = (int)ArtisanPageType.Question,

                    QuestionType = questionType,
                    Answers = answers,
                    Id = PageCounter--
                };

                vigSection.Pages.Add(ap);
                
                //Console.WriteLine(item.cCorrectAnswer);
            }


        }

        public static List<QTIQuestionObject> GetTopicQuestionObjectListByTopicID(int TopicID, string sUserAnswers)
        {
            //  ViDefinitionInfo oViDefinition = null;
            TestDefinition oTestDefinition = null;
            QTIUtils oQTIUtils = new QTIUtils();

            // NOTE: No caching of these items, because they contain user responses
            // along with test questions
            List<QTIQuestionObject> QTIQuestionObjects = null;

            // get the vignette definition record for this topic
            //  oViDefinition = GetViDefinitionByTopicID(TopicID);

            //get topic Definition

            oTestDefinition = TestDefinition.GetTestDefinitionByTopicID(TopicID);


            if (oTestDefinition != null)
            {
                // get the QTIQuestionObjectList from the ViDefinition XML
                QTIQuestionObjects = oQTIUtils.ConvertQTITestXMLToQTIQuestionObjectList(oTestDefinition.XMLTest);
            }


            if (QTIQuestionObjects != null)
            {

                if (string.IsNullOrEmpty(sUserAnswers))
                {
                    // incomplete test that does not match the current vignette definition version number
                    // set to default X and blank answers
                    // NOTE: This is also the case with new test record that has not had the vignette
                    // shown yet -- it will start out with vignetteversion field = 0 to this
                    // comparison will fail and we will insert all empty answers
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        if (QTIQuestion.cQuestionID.Substring(0, 2) == "MC" ||
                            QTIQuestion.cQuestionID.Substring(0, 2) == "TF")
                        {
                            QTIQuestion.cUserAnswer = "X";
                        }
                        else
                        {
                            QTIQuestion.cUserAnswer = "";
                        }
                    }
                }
                // * chnaged By navya
                else
                {
                    // incomplete test that matches the current test definition version number
                    // so it may have some user responses stored. Grab them from the test XMLVignette field
                    // and populate them here.
                    List<String> UserAnswers = oQTIUtils.GetUserAnswersFromXML(sUserAnswers);
                    String[] Answers = UserAnswers.ToArray();

                    int iLoop = 0;
                    foreach (QTIQuestionObject QTIQuestion in QTIQuestionObjects)
                    {
                        QTIQuestion.cUserAnswer = Answers[iLoop];
                        // be sure we're not empty -- if so, set to X
                        if (QTIQuestion.cUserAnswer.Trim().Length == 0)
                            QTIQuestion.cUserAnswer = "X";
                        iLoop++;
                    }
                }
            }

            return QTIQuestionObjects;
        }

        private void GetTestQuestionPage(int CETopicid, ArtisanSection testSection)
        {
            ArtisanQuestionType questionType = ArtisanQuestionType.MultipleChoice;


            //  AccessCETopic ac = new AccessCETopic();
            List<QTIQuestionObject> questions = HealthCareImporter.GetTopicQuestionObjectListByTopicID(CETopicid, "");
            string lastHeaderText = null;
            int questionCount = 0;
            foreach (var item in questions)
            {
                bool isHeader =
                    string.IsNullOrWhiteSpace(item.cAnswer_A_Text) &&
                    string.IsNullOrWhiteSpace(item.cAnswer_B_Text) &&
                    string.IsNullOrWhiteSpace(item.cAnswer_C_Text) &&
                    string.IsNullOrWhiteSpace(item.cAnswer_D_Text) &&
                    string.IsNullOrWhiteSpace(item.cAnswer_E_Text);

                if (isHeader)
                {
                    lastHeaderText = item.cQuestionText;
                    continue;
                }
                //add answers
                var answers = new List<ArtisanAnswer>();
                var j = 0;
                var a = new List<string>();
                a.Add(HttpUtility.HtmlEncode(item.cAnswer_A_Text));
                a.Add(HttpUtility.HtmlEncode(item.cAnswer_B_Text));
                a.Add(HttpUtility.HtmlEncode(item.cAnswer_C_Text));
                a.Add(HttpUtility.HtmlEncode(item.cAnswer_D_Text));
                a.Add(HttpUtility.HtmlEncode(item.cAnswer_E_Text));
                int correctAnswer = -1;
                if (string.IsNullOrEmpty(item.cCorrectAnswer))
                {
                    correctAnswer = 0;
                }
                else
                {
                    correctAnswer = item.cCorrectAnswer[0] - 65;
                }
                for (int i = 0; i < 5; i++)
                {
                    if (a[i] != "")
                    {


                        string b = a[i];
                        answers.Add(new ArtisanAnswer()
                        {
                            Text = a[i],
                            IsCorrect = (i == correctAnswer),
                            Sort = j++
                        });
                    }
                }


                string questionText = item.cQuestionText;
                if (!string.IsNullOrWhiteSpace(lastHeaderText))
                {
                    questionText = lastHeaderText + "<br/>" + questionText;
                    lastHeaderText = null;
                }

                ArtisanPage ap = new ArtisanPage()
                {
                    Name = "Question " + ++questionCount,
                    Html = questionText,
                    CorrectResponse = item.cCorrectAnswerFeedback,
                    IncorrectResponse = item.cCorrectAnswerFeedback,
                    PageType = (int)ArtisanPageType.Question,

                    QuestionType = questionType,
                    Answers = answers,
                    Id = PageCounter--
                };



                testSection.Pages.Add(ap);

                //Console.WriteLine(item.cCorrectAnswer);
            }


        }

        public string Code
        {
            get { return "HC"; }
        }

        public string Name
        {
            get { return "HealthCare"; }
        }

        public bool ValidateDirectory()
        {
            return true;
        }

        public void Import()
        {
            try
            {
                Utilities.LicenseAsposeByName();
            }
            catch (Exception e)
            {
                Log.Error("Cannot import. Aspose could not be licensed.\r\n" + e);
                throw e;
            }


            using(NetworkShareAccesser.Access(healthCareAssetsServer,healthCareAssetsUserName,healthCareAssetsPassword))
            {
                try
                {
                    int countException = 0;
                    if (settings == null)
                    {
                        throw new Exception("No HealthCare connection string set");
                    }

                    Log.Info("Starting HealthCare import");

                    int[] courseIdsArr = new int[0];
                    while (courseIdsArr.Length == 0)
                    {
                        Console.WriteLine("Enter course IDs to import:");

                        courseIdsArr = GetCourseIds();
                        if (courseIdsArr.Length == 0)
                        {
                            Console.WriteLine("Course ids were invalid");
                        }
                    }

                    Log.Info("Checking for RAD tech courses... (Marking Method Hide All Feedback)");
                    try
                    {
                        GetCoursesWithHiddenAnswers(courseIdsArr);
                    }
                    catch (Exception e)
                    {
                        Log.Error("Could not determine courses with marking method hide all feedback...\r\n" + e);
                    }

                    Log.Info("Importing assets...");
                    try
                    {
                        ImportAssets(courseIdsArr);
                        Log.Info("Asset import complete");
                    }
                    catch (Exception e)
                    {
                        Log.Error("Asset import failure: \r\n" + e);
                    }

                    int categoryId = GetHealthCareDataSourceCategory();

                    foreach (int courseId in courseIdsArr)
                    {
                        if (countException > 10) Environment.Exit(0);
                        try
                        {
                            this.ImportCourse(CustomerID, UserID, courseId, categoryId);
                        }
                        catch (Exception ex)
                        {
                            countException++;
                            Console.WriteLine("           !!!! IMPORT ERROR: Source Course id: {0} :: Error message: {1}", courseId, ex);
                            Console.ReadLine();
                        }
                    }

                    Double totalTime = 0;
                    int count = 0;
                }
                catch (Exception e)
                {
                    Console.WriteLine("       !!! IMPORT ERROR :: Error message: {0}", e);
                    Console.ReadLine();
                }
            }
        }

        private void GetCoursesWithHiddenAnswers(int[] courseids)
        {
            using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
            {
                string command = getHideAnswersCourses.Replace("@topicids", string.Join(",", courseids));
                using (SqlCommand cmd = new SqlCommand(command))
                {
                    cmd.Connection = conn;
                    cmd.Connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        int topicid = !reader.IsDBNull(0) ? reader.GetInt32(0) : 0;

                        if (!hideAnswersCourse.ContainsKey(topicid))
                        {
                            hideAnswersCourse.Add(topicid, true);
                        }

                    }
                }
            }
        }
        /// <summary>
        /// Imports all the assets used to generate course pages
        /// (PD, PPT, DO Courses)
        /// These are imported in parallel. If the asset already exists it is not imported.
        /// Stored in existingAssetsDictionary for access later
        /// </summary>
        /// <param name="courseids"></param>
        private void ImportAssets(int[] courseids)
        {
            Log.Info("Loading files to import from HealthCare DB...");
            using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
            {
                string command = getCourseFilesSQL.Replace("@topicids", string.Join(",", courseids));
                using (SqlCommand cmd = new SqlCommand(command))
                {
                    cmd.Connection = conn;
                    cmd.Connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        int topicid = !reader.IsDBNull(0) ? reader.GetInt32(0) : 0;
                        string course_number = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                        string course_file = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                        string fullPath = string.Format(@"\\{0}{1}", healthCareAssetsServer, Path.Combine(healthCareAssetsBasePath, course_file));

                        HealthCareTopicFile file = new HealthCareTopicFile()
                        {
                            TopicID = topicid,
                            CourseNumber = course_number,
                            FilePath = fullPath
                        };

                        if (!healthCareTopicFiles.ContainsKey(topicid))
                        {
                            healthCareTopicFiles.Add(topicid, file);
                        }
                    }
                    cmd.Connection.Close();
                }
            }

            Log.Info("Loading existing course PDF assets...");
            //Util.GetExistingAssets(CustomerID, UserID, Util.AssetTagPdf, existingAssetsDictionary);
            Log.Info("Loading existing course PPT assets...");
            Util.GetExistingAssets(CustomerID, UserID, Util.AssetTagPpt, existingAssetsDictionary);
            Log.Info("Loading existing course DOC assets...");
            Util.GetExistingAssets(CustomerID, UserID, Util.AssetTagDoc, existingAssetsDictionary);

            List<Task> importTasks = new List<Task>();
            Log.Info("Starting import tasks...");
            var _lock = new object();
            int totalFiles = 0;
            var ctx = HttpContext.Current;

            foreach (KeyValuePair<int, HealthCareTopicFile> healthCareTopicPair in healthCareTopicFiles)
            {
                HealthCareTopicFile healthCareTopicFile = healthCareTopicPair.Value;

                if (!existingAssetsDictionary.ContainsKey(healthCareTopicFile.CourseNumber))
                {
                    totalFiles++;
                    importTasks.Add(Task.Factory.StartNew((healthCareTopicFileObj) =>
                    {
                        try
                        {
                            DateTime startDate = DateTime.Now;
                            HealthCareTopicFile topicFile = (HealthCareTopicFile)healthCareTopicFileObj;
                            int uid = 0;
                            int cid = 0;
                            lock (_lock)
                            {
                                HttpContext.Current = ctx;
                                uid = UserID;
                                cid = CustomerID;
                                Log.Info("Opening file stream for " + topicFile.FileName);
                            }
                            using (Stream fileStream = File.OpenRead(topicFile.FilePath))
                            {
                                lock (_lock)
                                {
                                    Log.Info("Importing pdf " + topicFile.FileName);
                                }
                                ArtisanController ac = new ArtisanController();
                                string extension = Path.GetExtension(topicFile.FilePath);

                                List<ArtisanPage> pages = new List<ArtisanPage>();

                                if (extension == ".pdf")
                                {
                                    pages = ac.ImportPDFDocumentToMultiplePDFPages(cid, topicFile.FilePath, null, 0,
                                        0, null, null, "noscale", null, null, topicFile.CourseNumber, fileStream, topicFile.CourseNumber, appPath).Data;
                                }
                                else
                                {
                                    pages = ac.ImportDocument(cid, topicFile.FilePath, null, 0,
                                        0, null, null, "noscale", null, null, topicFile.CourseNumber, fileStream, topicFile.CourseNumber, appPath).Data;
                                }

                                topicFile.Pages = pages;
                            }

                            lock (_lock)
                            {
                                Log.Info(topicFile.FileName + " completed (" + DateTime.Now.Subtract(startDate).TotalSeconds + " seconds)");
                            }
                        }
                        catch (Exception e)
                        {
                            lock (_lock)
                            {
                                Log.Error("File Import Failure - " + healthCareTopicFile + " ---- \r\n Err Msg:" + e);
                            }
                        }
                    }, healthCareTopicFile));
                }
                else
                {
                    healthCareTopicFile.AssetsForPages = existingAssetsDictionary[healthCareTopicFile.CourseNumber];
                }
            }

            lock (_lock)
            {
                Log.Info(totalFiles + " files have been queued for import.");
            }

            if (importTasks.Count > 0)
            {
                Task.WaitAll(importTasks.ToArray());
            }
        }

        private int[] GetCourseIdsFromDB()
        {
            Log.Info("Loading course ids from db. (This is slow and may take > 15s)");
            List<int> ids = new List<int>();
            try
            {
                using (SqlConnection conn = new SqlConnection(settings.ConnectionString))
                {
                    string command = getAllCourseIdsQuery;
                    using (SqlCommand cmd = new SqlCommand(command))
                    {
                        cmd.Connection = conn;
                        cmd.Connection.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            TopicForImport tfImport = new TopicForImport()
                            {
                                ID = !reader.IsDBNull(0) ? reader.GetInt32(0) : 0,
                                CourseNumber = !reader.IsDBNull(1) ? reader.GetString(1) : null,
                                CourseName = !reader.IsDBNull(2) ? reader.GetString(2) : null,
                                Types = !reader.IsDBNull(3) ? reader.GetString(3) : null
                            };
                            Log.Debug("Checking topics for import for key: " + tfImport.ID);
                            if (tfImport.ID > 0 && !topicsForImport.ContainsKey(tfImport.ID))
                            {
                                Log.Debug("ADDING KEY: " + tfImport.ID);
                                topicsForImport.Add(tfImport.ID, tfImport);
                                ids.Add(tfImport.ID);
                            }
                        }
                        cmd.Connection.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("Could not load ids from db. Courses will not have a course type keyword.");
            }

            return ids.ToArray();
        }

        private int[] GetCourseIds()
        {
            string courseIdString = "";


            int[] idsFromDb = GetCourseIdsFromDB();
            if (healthCareAutoLoadIds)
            {
                return idsFromDb;
            }

            try
            {
                courseIdString = ConfigurationManager.AppSettings["healthCareIds"];
                Log.Info("Using course ids from config: " + courseIdString);
            }
            catch (Exception e)
            {
                Log.Info("Course ids not in config - Please enter ids:");
                courseIdString = Console.ReadLine();
            }
            List<int> ids = new List<int>();

            string[] courseIdTokens = courseIdString.Split(',');
            foreach (string s in courseIdTokens)
            {
                string[] minMax = s.Split('-');
                if (minMax.Length == 2)
                {
                    int min, max;
                    if (int.TryParse(minMax[0], out min) &&
                        int.TryParse(minMax[1], out max))
                    {
                        for (var i = min; i <= max; i++)
                        {
                            ids.Add(i);
                        }
                    }
                }
                else
                {
                    int id;
                    if (int.TryParse(minMax[0], out id))
                    {
                        ids.Add(id);
                    }
                }
            }

            return ids.ToArray();
        }
    }

    public static class HtmlRemoval
    {
        /// <summary>
        /// Remove HTML from string with Regex.
        /// </summary>
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }

        public static string StripQuotes(string source)
        {
            return Regex.Replace(source, "&.*?;", string.Empty);
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        /// <summary>
        /// Remove HTML tags from string using char array.
        /// </summary>
        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }

            string res = new string(array, 0, arrayIndex);
            res = res.Replace("&nbsp;", "");
            res.Replace("\r\n\r\n", "\r\n");
            res.Replace("\r\n\r\n", "\r\n");

            return res;
        }


    }


    
}
