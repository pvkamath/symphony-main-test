﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace ArtisanImporter2
{
    class Log
    {
        static ILog log;

        public static void Setup()
        {
            log4net.Config.XmlConfigurator.Configure();
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        public static void Error(object msg)
        {
            log.Error(msg);
        }

        public static void Debug(object msg)
        {
            log.Debug(msg);
        }

        public static void Fatal(object msg) {
            log.Fatal(msg);
        }

        public static void Info(object msg)
        {
            log.Info(msg);
        }

    }
}
