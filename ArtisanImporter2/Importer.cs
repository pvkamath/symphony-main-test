﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtisanImporter2
{
    interface Importer
    {
        string Code { get; }
        string Name { get; }
        bool ValidateDirectory();
        void Import();
    }
}
