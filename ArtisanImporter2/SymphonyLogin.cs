﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Symphony.Core.Controllers;
using Symphony.Core;
using Symphony.Core.Models;
using Data = Symphony.Core.Data;
using System.Web;

namespace ArtisanImporter2
{
    class SymphonyLogin
    {
        public static int CustomerID = 0;
        public static int UserID = 0;
        public static string CustomerDomain = "";
        public static string Username = "";
        
        public void HandleLogin()
        {
            while (!LoginSymphony())
            {
                Console.WriteLine("Login failed, please try again.");
            }

            AuthUserData userData = (AuthUserData)HttpContext.Current.Items["UserData"];
            CustomerID = userData.CustomerID;
            UserID = userData.UserID;

        }

        private bool LoginSymphony()
        {
            Console.WriteLine("Enter Customer:");
            var customer = "be"; //Console.ReadLine();

            Console.WriteLine("Enter User Name:");
            var userName = "admin";  //Console.ReadLine();

            UserController userController = new UserController();

            Username = userName;
            CustomerDomain = customer;

            return userController.LoginBatch(customer, userName);
        }
    }
}
