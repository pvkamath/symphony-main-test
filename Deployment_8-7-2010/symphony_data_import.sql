use Symphony;

-- trainingprogramtocourselink - invalid column name "due date", invalid column name "customerid"
-- scheduleparameters - canfiltertoincomplete, codename, canfiltercomplete
-- templates - filtercomplete
-- onlinecourse - internalcode
-- classroom - course
-- classroom - coursefile
-- classroom - presentationtype

begin tran t1

declare @tableName nvarchar(100)
declare @dbName varchar(50)
declare @map varchar(50)
declare @database varchar(50)

set @database = 'Symphony2';

alter table [onlinecourse] drop column searchtext
alter table [templates] drop column searchtext
drop index [user].ix_search
alter table [user] drop column search
alter table [Course] drop column hassurvey

declare @tableNames table( name nvarchar(100), db varchar(50), map varchar(50) default null )
insert @tableNames (name, db) select 'aspnet_Applications', 'CustomerBlackBox'
insert @tableNames (name, db) select 'aspnet_Membership', 'CustomerBlackBox'
insert @tableNames (name, db) select 'aspnet_Paths', 'CustomerBlackBox'
insert @tableNames (name, db) select 'aspnet_PersonalizationAllUsers', 'CustomerBlackBox'
insert @tableNames (name, db) select 'aspnet_PersonalizationPerUser', 'CustomerBlackBox'
insert @tableNames (name, db) select 'aspnet_Profile', 'CustomerBlackBox'
insert @tableNames (name, db) select 'aspnet_Roles', 'CustomerBlackBox'
insert @tableNames (name, db) select 'aspnet_SchemaVersions', 'CustomerBlackBox'
insert @tableNames (name, db) select 'aspnet_Users', 'CustomerBlackBox'
insert @tableNames (name, db) select 'aspnet_UsersInRoles', 'CustomerBlackBox'
insert @tableNames (name, db) select 'aspnet_WebEvent_Events', 'CustomerBlackBox'
insert @tableNames (name, db) select 'aspnet_Attachments', 'CustomerBlackBox'
insert @tableNames (name, db) select 'Audience', 'CustomerBlackBox'
insert @tableNames (name, db) select 'cnrCompany', 'CustomerBlackBox'
insert @tableNames (name, db) select 'cnrUser', 'CustomerBlackBox'
insert @tableNames (name, db) select 'Customer', 'CustomerBlackBox'
insert @tableNames (name, db) select 'Import', 'CustomerBlackBox'
insert @tableNames (name, db) select 'ImportType', 'CustomerBlackBox'
insert @tableNames (name, db) select 'JobRole', 'CustomerBlackBox'
insert @tableNames (name, db) select 'Location', 'CustomerBlackBox'
insert @tableNames (name, db) select 'SalesChannel', 'CustomerBlackBox'
insert @tableNames (name, db) select 'User', 'CustomerBlackBox'
insert @tableNames (name, db) select 'UserAudience', 'CustomerBlackBox'
insert @tableNames (name, db) select 'UserStatus', 'CustomerBlackBox'

insert @tableNames (name, db) select 'Class', 'Classroom'
insert @tableNames (name, db) select 'ClassDate', 'Classroom'
insert @tableNames (name, db) select 'ClassInstructors', 'Classroom'
insert @tableNames (name, db) select 'ClassResources', 'Classroom'
--insert @tableNames (name, db) select 'Course', 'Classroom'
insert @tableNames (name, db) select 'CourseCompletionType', 'Classroom'
insert @tableNames (name, db) select 'CourseCompletionTypeParameter', 'Classroom'
insert @tableNames (name, db) select 'CourseDurationUnit', 'Classroom'
insert @tableNames (name, db) select 'CourseGrade', 'Classroom'
insert @tableNames (name, db) select 'CoursePrerequisites', 'Classroom'
insert @tableNames (name, db) select 'CourseSurveys', 'Classroom'
--insert @tableNames (name, db, map) select 'CourseType', 'Classroom', 'PresentationType'
--insert @tableNames (name, db, map) select 'CourseFile', 'Classroom'
insert @tableNames (name, db) select 'FileType', 'Classroom'
insert @tableNames (name, db) select 'Keyword', 'Classroom'
insert @tableNames (name, db) select 'KeywordCourse', 'Classroom'
insert @tableNames (name, db) select 'KeywordType', 'Classroom'
insert @tableNames (name, db) select 'PassingScore', 'Classroom'
insert @tableNames (name, db) select 'Status', 'Classroom'
insert @tableNames (name, db) select 'RegistrationType', 'Classroom'
insert @tableNames (name, db) select 'Registration', 'Classroom'
insert @tableNames (name, db) select 'Resource', 'Classroom'
insert @tableNames (name, db) select 'Room', 'Classroom'
insert @tableNames (name, db) select 'States', 'Classroom'
insert @tableNames (name, db) select 'TimeZone', 'Classroom'
insert @tableNames (name, db) select 'Venue', 'Classroom'
insert @tableNames (name, db) select 'VenueResourceManagers', 'Classroom'
insert @tableNames (name, db) select 'VirtualRoom', 'Classroom'

insert @tableNames (name, db) select 'GTMGroup', 'Collaboration'
insert @tableNames (name, db) select 'GTMInvitation', 'Collaboration'
insert @tableNames (name, db) select 'GTMLicense', 'Collaboration'
insert @tableNames (name, db) select 'GTMMeeting', 'Collaboration'
insert @tableNames (name, db) select 'GTMOrganizer', 'Collaboration'

insert @tableNames (name, db) select 'CourseType', 'CourseAssignment'
insert @tableNames (name, db) select 'HierarchyToTrainingProgramLink', 'CourseAssignment'
insert @tableNames (name, db) select 'HierarchyType', 'CourseAssignment'
insert @tableNames (name, db) select 'SyllabusType', 'CourseAssignment'
insert @tableNames (name, db) select 'TrainingProgram', 'CourseAssignment'
insert @tableNames (name, db) select 'TrainingProgramFiles', 'CourseAssignment'
--insert @tableNames (name, db) select 'TrainingProgramToCourseLink', 'CourseAssignment'


insert @tableNames (name, db) select 'Attachments', 'Messaging'
insert @tableNames (name, db) select 'Notifications', 'Messaging'
insert @tableNames (name, db) select 'Attachments', 'Messaging'
insert @tableNames (name, db) select 'RecipientGroups', 'Messaging'
--insert @tableNames (name, db) select 'ScheduleParameters', 'Messaging'
--insert @tableNames (name, db) select 'Templates', 'Messaging'
insert @tableNames (name, db) select 'TemplateScheduleHistory', 'Messaging'
insert @tableNames (name, db) select 'TemplatesToRecipientGroupsMap', 'Messaging'

--insert @tableNames (name, db) select 'OnlineCourse', 'OnlineTraining'
insert @tableNames (name, db) select 'OnlineCourseRollup', 'OnlineTraining'

declare tableNameCursor cursor for
select db, name, map from @tableNames

open tableNameCursor
fetch next from tableNameCursor into @dbName, @tableName, @map

while (@@fetch_status = 0)
begin
	declare @hasIdentity bit
	declare @sql nvarchar(max)
	set @sql = '';
	
	set @hasIdentity = (SELECT OBJECTPROPERTY(OBJECT_ID(@tableName),'TableHasIdentity'))

	if(@hasIdentity > 0)
	begin
		set @sql = 'set identity_insert ' + @database + '.dbo.[' + @tableName + '] on;'
	end
	
	declare @columnList nvarchar(max) 
	declare @sourceColumnList nvarchar(max)
	set @columnList =	(
	select substring(
		(
		SELECT ',[' + COLUMN_NAME + ']'
		FROM   
		INFORMATION_SCHEMA.COLUMNS 
		WHERE   
		TABLE_NAME = @tableName
		ORDER BY 
		ORDINAL_POSITION ASC
		FOR XML Path('')
		),2,50000)
	);

	set @sourceColumnList = @columnList
	if(@dbName = 'Classroom' and @tableName = 'Course')
	begin
		-- for this particular one, we change one column name
		set @sourceColumnList = replace(@columnList,'[PresentationTypeID]','[CourseTypeID]')
	end
  
	

	if(@map is null)
	begin
		set @map = @tableName
	end
	
	set @sql = @sql + 'insert ' + @database + '.dbo.[' + @map + '] (' + @columnList + ') select ' + @sourceColumnList + ' from ' + @dbName + '.dbo.[' + @tableName + '];';
	
	
	if(@hasIdentity = 1)
	begin
		set @sql = @sql + 'set identity_insert ' + @database + '.dbo.[' + @tableName + '] off;'
	end
	
print @sql
	exec(@sql);
	
	fetch next from tableNameCursor into @dbName, @tableName, @map
end

alter table onlinecourse add [SearchText]  AS (((([Name]+' ')+[Description])+' ')+[Keywords])
alter table templates add [SearchText]  AS (([DisplayName]+' ')+[Area])
alter table [user] add [Search]  AS (((((([Username]+' ')+[FirstName])+' ')+[MiddleName])+' ')+[LastName])
alter table Course add [HasSurvey]  AS (CONVERT([bit],case [SurveyID] when (0) then (0) else (1) end,(0)))
CREATE NONCLUSTERED INDEX [IX_Search] ON [dbo].[User] 
(
	[Search] ASC
)


commit tran t1

--TODO: Add additional inserts/updates here