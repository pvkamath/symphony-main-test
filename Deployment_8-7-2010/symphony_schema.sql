USE [Symphony]
GO
/****** Object:  Table [dbo].[cnrUser]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cnrUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[cnrUser](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[UserID] [int] NOT NULL,
	[CNRUserID] [int] NOT NULL,
 CONSTRAINT [PK_cnrUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[cnrCompany]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cnrCompany]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[cnrCompany](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CustomerID] [int] NOT NULL,
	[CNRCompanyID] [int] NOT NULL,
 CONSTRAINT [PK_cnrCompany] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RestorePermissions]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RestorePermissions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
    @name   sysname
AS
BEGIN
    DECLARE @object sysname
    DECLARE @protectType char(10)
    DECLARE @action varchar(20)
    DECLARE @grantee sysname
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT Object, ProtectType, [Action], Grantee FROM #aspnet_Permissions where Object = @name

    OPEN c1

    FETCH c1 INTO @object, @protectType, @action, @grantee
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = @protectType + '' '' + @action + '' on '' + @object + '' TO ['' + @grantee + '']''
        EXEC (@cmd)
        FETCH c1 INTO @object, @protectType, @action, @grantee
    END

    CLOSE c1
    DEALLOCATE c1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RemoveAllRoleMembers]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RemoveAllRoleMembers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
    @name   sysname
AS
BEGIN
    CREATE TABLE #aspnet_RoleMembers
    (
        Group_name      sysname,
        Group_id        smallint,
        Users_in_group  sysname,
        User_id         smallint
    )

    INSERT INTO #aspnet_RoleMembers
    EXEC sp_helpuser @name

    DECLARE @user_id smallint
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT User_id FROM #aspnet_RoleMembers

    OPEN c1

    FETCH c1 INTO @user_id
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = ''EXEC sp_droprolemember '' + '''''''' + @name + '''''', '''''' + USER_NAME(@user_id) + ''''''''
        EXEC (@cmd)
        FETCH c1 INTO @user_id
    END

    CLOSE c1
    DEALLOCATE c1
END
' 
END
GO
/****** Object:  Table [dbo].[aspnet_SchemaVersions]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_SchemaVersions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_SchemaVersions](
	[Feature] [nvarchar](128) NOT NULL,
	[CompatibleSchemaVersion] [nvarchar](128) NOT NULL,
	[IsCurrentVersion] [bit] NOT NULL,
 CONSTRAINT [PK__aspnet_SchemaVer__173876EA] PRIMARY KEY CLUSTERED 
(
	[Feature] ASC,
	[CompatibleSchemaVersion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[aspnet_Profile]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Profile](
	[UserId] [uniqueidentifier] NOT NULL,
	[PropertyNames] [ntext] NOT NULL,
	[PropertyValuesString] [ntext] NOT NULL,
	[PropertyValuesBinary] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_Profile__4CA06362] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[aspnet_Applications]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Applications](
	[ApplicationName] [nvarchar](256) NOT NULL,
	[LoweredApplicationName] [nvarchar](256) NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__aspnet_Ap__Appli__29572725]  DEFAULT (newid()),
	[Description] [nvarchar](256) NULL,
 CONSTRAINT [PK__aspnet_Applicati__286302EC] PRIMARY KEY NONCLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ__aspnet_Applicati__267ABA7A] UNIQUE NONCLUSTERED 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UQ__aspnet_Applicati__276EDEB3] UNIQUE NONCLUSTERED 
(
	[ApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[GetLocationHierarchyBranch]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLocationHierarchyBranch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLocationHierarchyBranch]
	@iLocationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from [fGetLocationHierarchyBranch](@iLocationID)
END
' 
END
GO
/****** Object:  Table [dbo].[PasswordMask]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PasswordMask]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PasswordMask](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[MaskTemplate] [nvarchar](100) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__PasswordM__Modif__09DE7BCC]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__PasswordM__Creat__0AD2A005]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__PasswordM__Modif__0BC6C43E]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__PasswordM__Creat__0CBAE877]  DEFAULT (getdate()),
 CONSTRAINT [PK_PasswordMask] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PassingScore]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PassingScore]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PassingScore](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CustomerID] [int] NOT NULL,
	[PassingScore] [int] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_PassingScores] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PassingScore]') AND name = N'passingscore__customer_index')
CREATE NONCLUSTERED INDEX [passingscore__customer_index] ON [dbo].[PassingScore] 
(
	[CustomerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OnlineCourseRollup]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnlineCourseRollup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OnlineCourseRollup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[CourseID] [int] NOT NULL,
	[TrainingProgramID] [int] NOT NULL,
	[Score] [decimal](10, 2) NULL,
	[TotalSeconds] [bigint] NOT NULL,
	[Success] [bit] NULL,
	[Completion] [bit] NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[AttemptCount] [int] NOT NULL CONSTRAINT [DF_OnlineCourseRollup_AttemptCount]  DEFAULT ((0)),
	[HighScoreDate] [datetime] NULL,
	[OriginalRegistrationID] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_OnlineCourseRollup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Notifications]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Notifications]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Notifications](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SenderID] [int] NOT NULL,
	[RecipientID] [int] NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Body] [nvarchar](2000) NOT NULL,
	[IsRead] [bit] NOT NULL,
	[AttachmentID] [int] NOT NULL CONSTRAINT [DF_Notifications_AttachmentID]  DEFAULT ((0)),
	[Priority] [tinyint] NOT NULL CONSTRAINT [DF__Notificat__Prior__7F60ED59]  DEFAULT ((1)),
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Notifications] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Notifications]') AND name = N'body_index')
CREATE NONCLUSTERED INDEX [body_index] ON [dbo].[Notifications] 
(
	[Subject] ASC
)
INCLUDE ( [Body]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Notifications]') AND name = N'read_index')
CREATE NONCLUSTERED INDEX [read_index] ON [dbo].[Notifications] 
(
	[IsRead] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Notifications]') AND name = N'recipient_index')
CREATE NONCLUSTERED INDEX [recipient_index] ON [dbo].[Notifications] 
(
	[RecipientID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OnlineCourse]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnlineCourse]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OnlineCourse](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[Name] [nvarchar](150) NOT NULL CONSTRAINT [DF__OnlineCour__Name__32AB8735]  DEFAULT (''),
	[Description] [nvarchar](500) NOT NULL CONSTRAINT [DF__OnlineCou__Descr__339FAB6E]  DEFAULT (''),
	[Keywords] [nvarchar](400) NULL CONSTRAINT [DF_OnlineCourse_Keywords]  DEFAULT ((0)),
	[Cost] [decimal](10, 2) NOT NULL CONSTRAINT [DF__OnlineCour__Cost__3587F3E0]  DEFAULT ((0)),
	[Credit] [decimal](10, 2) NOT NULL CONSTRAINT [DF__OnlineCou__Credi__367C1819]  DEFAULT ((0)),
	[PublicIndicator] [bit] NOT NULL CONSTRAINT [DF__OnlineCou__Publi__37703C52]  DEFAULT ((0)),
	[IsSurvey] [bit] NOT NULL CONSTRAINT [DF__OnlineCou__IsSur__3864608B]  DEFAULT ((0)),
	[IsTest] [bit] NOT NULL CONSTRAINT [DF_OnlineCourse_IsTest]  DEFAULT ((0)),
	[ModifiedBy] [nvarchar](50) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[ModifiedOn] [datetime] NULL,
	[CreatedOn] [datetime] NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_OnlineCourse_Active]  DEFAULT ((1)),
	[DuplicateFromID] [int] NOT NULL CONSTRAINT [DF__OnlineCou__Dupli__503BEA1C]  DEFAULT ((0)),
	[Version] [int] NOT NULL CONSTRAINT [DF__OnlineCou__Versi__5D95E53A]  DEFAULT ((0)),
	[SearchText]  AS (((([Name]+' ')+[Description])+' ')+[Keywords]),
	[InternalCode] [nvarchar](25) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_OnlineCourse] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[NBTSynch]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NBTSynch]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NBTSynch](
	[EmployeeNumber] [nvarchar](255) NULL,
	[SupervisorCode] [nvarchar](255) NULL,
	[UserID] [int] NULL,
	[SupervisorID] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[KeywordType]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KeywordType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KeywordType](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TypeName] [varchar](20) NOT NULL CONSTRAINT [DF_KeywordType_TypeName]  DEFAULT (''),
	[TypeDescription] [varchar](100) NOT NULL CONSTRAINT [DF_KeywordType_TypeDescription]  DEFAULT (''),
	[ActiveIndicator] [bit] NOT NULL CONSTRAINT [DF_KeywordType_ActiveIndicator]  DEFAULT ((1)),
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__KeywordTy__Modif__00551192]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__KeywordTy__Creat__014935CB]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__KeywordTy__Modif__023D5A04]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__KeywordTy__Creat__03317E3D]  DEFAULT (getdate()),
 CONSTRAINT [PK_KeywordType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUserInfo]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUserInfo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @IsPasswordCorrect              bit,
    @UpdateLastLoginActivityDate    bit,
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @LastLoginDate                  datetime,
    @LastActivityDate               datetime
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @IsApproved                             bit
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @IsApproved = m.IsApproved,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        GOTO Cleanup
    END

    IF( @IsPasswordCorrect = 0 )
    BEGIN
        IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAttemptWindowStart ) )
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = 1
        END
        ELSE
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = @FailedPasswordAttemptCount + 1
        END

        BEGIN
            IF( @FailedPasswordAttemptCount >= @MaxInvalidPasswordAttempts )
            BEGIN
                SET @IsLockedOut = 1
                SET @LastLockoutDate = @CurrentTimeUtc
            END
        END
    END
    ELSE
    BEGIN
        IF( @FailedPasswordAttemptCount > 0 OR @FailedPasswordAnswerAttemptCount > 0 )
        BEGIN
            SET @FailedPasswordAttemptCount = 0
            SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            SET @FailedPasswordAnswerAttemptCount = 0
            SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            SET @LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )
        END
    END

    IF( @UpdateLastLoginActivityDate = 1 )
    BEGIN
        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @LastActivityDate
        WHERE   @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END

        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @LastLoginDate
        WHERE   UserId = @UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END


    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
        FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
    WHERE @UserId = UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  Table [dbo].[Keyword]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Keyword]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Keyword](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[KeywordTypeID] [int] NOT NULL,
	[KeywordName] [nvarchar](100) NOT NULL,
	[PublicIndicator] [bit] NOT NULL,
	[ActiveIndicator] [bit] NOT NULL,
	[PurgeIndicator] [bit] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CustomerID] [int] NOT NULL,
 CONSTRAINT [PK_Keyword] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUser]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @Email                nvarchar(256),
    @Comment              ntext,
    @IsApproved           bit,
    @LastLoginDate        datetime,
    @LastActivityDate     datetime,
    @UniqueEmail          int,
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId, @ApplicationId = a.ApplicationId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership WITH (UPDLOCK, HOLDLOCK)
                    WHERE ApplicationId = @ApplicationId  AND @UserId <> UserId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            RETURN(7)
        END
    END

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    UPDATE dbo.aspnet_Users WITH (ROWLOCK)
    SET
         LastActivityDate = @LastActivityDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    UPDATE dbo.aspnet_Membership WITH (ROWLOCK)
    SET
         Email            = @Email,
         LoweredEmail     = LOWER(@Email),
         Comment          = @Comment,
         IsApproved       = @IsApproved,
         LastLoginDate    = @LastLoginDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN -1
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UnlockUser]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UnlockUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
    @ApplicationName                         nvarchar(256),
    @UserName                                nvarchar(256)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
        RETURN 1

    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = 0,
        FailedPasswordAttemptCount = 0,
        FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 ),
        FailedPasswordAnswerAttemptCount = 0,
        FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 ),
        LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )
    WHERE @UserId = UserId

    RETURN 0
END
' 
END
GO
/****** Object:  Table [dbo].[HierarchyType]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HierarchyType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HierarchyType](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL CONSTRAINT [DF_HierarchyType_Name]  DEFAULT (''),
	[Description] [nvarchar](50) NOT NULL,
	[CodeName] [nvarchar](50) NOT NULL CONSTRAINT [DF_HierarchyType_CodeName]  DEFAULT (''),
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__Heirarchy__Modif__6EC0713C]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__Heirarchy__Creat__6FB49575]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__Heirarchy__Modif__70A8B9AE]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__Heirarchy__Creat__719CDDE7]  DEFAULT (getdate()),
 CONSTRAINT [PK_HeirarchyType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_SetPassword]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_SetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_SetPassword]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @NewPassword      nvarchar(128),
    @PasswordSalt     nvarchar(128),
    @CurrentTimeUtc   datetime,
    @PasswordFormat   int = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    UPDATE dbo.aspnet_Membership
    SET Password = @NewPassword, PasswordFormat = @PasswordFormat, PasswordSalt = @PasswordSalt,
        LastPasswordChangedDate = @CurrentTimeUtc
    WHERE @UserId = UserId
    RETURN(0)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ResetPassword]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ResetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
    @ApplicationName             nvarchar(256),
    @UserName                    nvarchar(256),
    @NewPassword                 nvarchar(128),
    @MaxInvalidPasswordAttempts  int,
    @PasswordAttemptWindow       int,
    @PasswordSalt                nvarchar(128),
    @CurrentTimeUtc              datetime,
    @PasswordFormat              int = 0,
    @PasswordAnswer              nvarchar(128) = NULL
AS
BEGIN
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @UserId                                 uniqueidentifier
    SET     @UserId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    SELECT @IsLockedOut = IsLockedOut,
           @LastLockoutDate = LastLockoutDate,
           @FailedPasswordAttemptCount = FailedPasswordAttemptCount,
           @FailedPasswordAttemptWindowStart = FailedPasswordAttemptWindowStart,
           @FailedPasswordAnswerAttemptCount = FailedPasswordAnswerAttemptCount,
           @FailedPasswordAnswerAttemptWindowStart = FailedPasswordAnswerAttemptWindowStart
    FROM dbo.aspnet_Membership WITH ( UPDLOCK )
    WHERE @UserId = UserId

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Membership
    SET    Password = @NewPassword,
           LastPasswordChangedDate = @CurrentTimeUtc,
           PasswordFormat = @PasswordFormat,
           PasswordSalt = @PasswordSalt
    WHERE  @UserId = UserId AND
           ( ( @PasswordAnswer IS NULL ) OR ( LOWER( PasswordAnswer ) = LOWER( @PasswordAnswer ) ) )

    IF ( @@ROWCOUNT = 0 )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
    ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            END
        END

    IF( NOT ( @PasswordAnswer IS NULL ) )
    BEGIN
        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  Table [dbo].[GTMOrganizer]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GTMOrganizer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GTMOrganizer](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GTMGroupID] [int] NOT NULL CONSTRAINT [DF_GTMOrganizers_GTMGroupID]  DEFAULT ((0)),
	[OrganizerKey] [bigint] NOT NULL,
	[Email] [nvarchar](75) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[UserID] [int] NOT NULL CONSTRAINT [DF_GTMOrganizers_UserID]  DEFAULT ((0)),
	[Status] [nvarchar](50) NOT NULL CONSTRAINT [DF_Organizers_Disabled]  DEFAULT ((0)),
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Organizers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GTMOrganizer]') AND name = N'group_index')
CREATE NONCLUSTERED INDEX [group_index] ON [dbo].[GTMOrganizer] 
(
	[GTMGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GTMOrganizer]') AND name = N'user_index')
CREATE NONCLUSTERED INDEX [user_index] ON [dbo].[GTMOrganizer] 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByUserId]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByUserId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
    @UserId               uniqueidentifier,
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    IF ( @UpdateLastActivity = 1 )
    BEGIN
        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        FROM     dbo.aspnet_Users
        WHERE    @UserId = UserId

        IF ( @@ROWCOUNT = 0 ) -- User ID not found
            RETURN -1
    END

    SELECT  m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate, m.LastLoginDate, u.LastActivityDate,
            m.LastPasswordChangedDate, u.UserName, m.IsLockedOut,
            m.LastLockoutDate
    FROM    dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   @UserId = u.UserId AND u.UserId = m.UserId

    IF ( @@ROWCOUNT = 0 ) -- User ID not found
       RETURN -1

    RETURN 0
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByName]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier

    IF (@UpdateLastActivity = 1)
    BEGIN
        SELECT TOP 1 m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, @CurrentTimeUtc, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut,m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1

        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        WHERE    @UserId = UserId
    END
    ELSE
    BEGIN
        SELECT TOP 1 m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut,m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1
    END

    RETURN 0
END
' 
END
GO
/****** Object:  Table [dbo].[GTMMeeting]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GTMMeeting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GTMMeeting](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GTMOrganizerID] [int] NOT NULL,
	[MeetingID] [bigint] NOT NULL,
	[UniqueMeetingID] [nvarchar](50) NOT NULL,
	[Subject] [nvarchar](100) NOT NULL,
	[JoinURL] [nvarchar](max) NOT NULL,
	[MaxParticipants] [int] NOT NULL,
	[MeetingType] [nvarchar](50) NOT NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL,
	[TimeZoneKey] [int] NOT NULL,
	[PasswordRequired] [bit] NOT NULL,
	[ConferenceCallInfo] [nvarchar](max) NOT NULL,
	[IsEditable] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Meetings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GTMMeeting]') AND name = N'enddate_index')
CREATE NONCLUSTERED INDEX [enddate_index] ON [dbo].[GTMMeeting] 
(
	[EndDateTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GTMMeeting]') AND name = N'meeting_index')
CREATE NONCLUSTERED INDEX [meeting_index] ON [dbo].[GTMMeeting] 
(
	[MeetingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GTMMeeting]') AND name = N'organizer_index')
CREATE NONCLUSTERED INDEX [organizer_index] ON [dbo].[GTMMeeting] 
(
	[GTMOrganizerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GTMMeeting]') AND name = N'startdate_index')
CREATE NONCLUSTERED INDEX [startdate_index] ON [dbo].[GTMMeeting] 
(
	[StartDateTime] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GTMMeeting]') AND name = N'subject_index')
CREATE NONCLUSTERED INDEX [subject_index] ON [dbo].[GTMMeeting] 
(
	[Subject] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GTMMeeting]') AND name = N'uniquemeeting_index')
CREATE NONCLUSTERED INDEX [uniquemeeting_index] ON [dbo].[GTMMeeting] 
(
	[UniqueMeetingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByEmail]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
    @ApplicationName  nvarchar(256),
    @Email            nvarchar(256)
AS
BEGIN
    IF( @Email IS NULL )
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                m.LoweredEmail IS NULL
    ELSE
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                LOWER(@Email) = m.LoweredEmail

    IF (@@rowcount = 0)
        RETURN(1)
    RETURN(0)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPasswordWithFormat]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPasswordWithFormat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @UpdateLastLoginActivityDate    bit,
    @CurrentTimeUtc                 datetime
AS
BEGIN
    DECLARE @IsLockedOut                        bit
    DECLARE @UserId                             uniqueidentifier
    DECLARE @Password                           nvarchar(128)
    DECLARE @PasswordSalt                       nvarchar(128)
    DECLARE @PasswordFormat                     int
    DECLARE @FailedPasswordAttemptCount         int
    DECLARE @FailedPasswordAnswerAttemptCount   int
    DECLARE @IsApproved                         bit
    DECLARE @LastActivityDate                   datetime
    DECLARE @LastLoginDate                      datetime

    SELECT  @UserId          = NULL

    SELECT  @UserId = u.UserId, @IsLockedOut = m.IsLockedOut, @Password=Password, @PasswordFormat=PasswordFormat,
            @PasswordSalt=PasswordSalt, @FailedPasswordAttemptCount=FailedPasswordAttemptCount,
		    @FailedPasswordAnswerAttemptCount=FailedPasswordAnswerAttemptCount, @IsApproved=IsApproved,
            @LastActivityDate = LastActivityDate, @LastLoginDate = LastLoginDate
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF (@UserId IS NULL)
        RETURN 1

    IF (@IsLockedOut = 1)
        RETURN 99

    SELECT   @Password, @PasswordFormat, @PasswordSalt, @FailedPasswordAttemptCount,
             @FailedPasswordAnswerAttemptCount, @IsApproved, @LastLoginDate, @LastActivityDate

    IF (@UpdateLastLoginActivityDate = 1 AND @IsApproved = 1)
    BEGIN
        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @CurrentTimeUtc
        WHERE   UserId = @UserId

        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @CurrentTimeUtc
        WHERE   @UserId = UserId
    END


    RETURN 0
END
' 
END
GO
/****** Object:  Table [dbo].[GTMLicense]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GTMLicense]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GTMLicense](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CustomerID] [int] NOT NULL,
	[OrganizerCount] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_GTMOrganizerSeats] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPassword]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_GetPassword]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @PasswordAnswer                 nvarchar(128) = NULL
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @PasswordFormat                         int
    DECLARE @Password                               nvarchar(128)
    DECLARE @passAns                                nvarchar(128)
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @Password = m.Password,
            @passAns = m.PasswordAnswer,
            @PasswordFormat = m.PasswordFormat,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    IF ( NOT( @PasswordAnswer IS NULL ) )
    BEGIN
        IF( ( @passAns IS NULL ) OR ( LOWER( @passAns ) <> LOWER( @PasswordAnswer ) ) )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
        ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            END
        END

        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    IF( @ErrorCode = 0 )
        SELECT @Password, @PasswordFormat

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetNumberOfUsersOnline]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetNumberOfUsersOnline]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_GetNumberOfUsersOnline]
    @ApplicationName            nvarchar(256),
    @MinutesSinceLastInActive   int,
    @CurrentTimeUtc             datetime
AS
BEGIN
    DECLARE @DateActive datetime
    SELECT  @DateActive = DATEADD(minute,  -(@MinutesSinceLastInActive), @CurrentTimeUtc)

    DECLARE @NumOnline int
    SELECT  @NumOnline = COUNT(*)
    FROM    dbo.aspnet_Users u(NOLOCK),
            dbo.aspnet_Applications a(NOLOCK),
            dbo.aspnet_Membership m(NOLOCK)
    WHERE   u.ApplicationId = a.ApplicationId                  AND
            LastActivityDate > @DateActive                     AND
            a.LoweredApplicationName = LOWER(@ApplicationName) AND
            u.UserId = m.UserId
    RETURN(@NumOnline)
END
' 
END
GO
/****** Object:  Table [dbo].[GTMInvitation]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GTMInvitation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GTMInvitation](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[UserID] [int] NOT NULL,
	[GTMMeetingID] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[IsExternal] [bit] NOT NULL,
 CONSTRAINT [PK_GTMInvitations] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GTMInvitation]') AND name = N'meeting_index')
CREATE NONCLUSTERED INDEX [meeting_index] ON [dbo].[GTMInvitation] 
(
	[GTMMeetingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[GTMInvitation]') AND name = N'user_index')
CREATE NONCLUSTERED INDEX [user_index] ON [dbo].[GTMInvitation] 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_CreateUser]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_CreateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_CreateUser]
    @ApplicationName                        nvarchar(256),
    @UserName                               nvarchar(256),
    @Password                               nvarchar(128),
    @PasswordSalt                           nvarchar(128),
    @Email                                  nvarchar(256),
    @PasswordQuestion                       nvarchar(256),
    @PasswordAnswer                         nvarchar(128),
    @IsApproved                             bit,
    @CurrentTimeUtc                         datetime,
    @CreateDate                             datetime = NULL,
    @UniqueEmail                            int      = 0,
    @PasswordFormat                         int      = 0,
    @UserId                                 uniqueidentifier OUTPUT
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @NewUserId uniqueidentifier
    SELECT @NewUserId = NULL

    DECLARE @IsLockedOut bit
    SET @IsLockedOut = 0

    DECLARE @LastLockoutDate  datetime
    SET @LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @FailedPasswordAttemptCount int
    SET @FailedPasswordAttemptCount = 0

    DECLARE @FailedPasswordAttemptWindowStart  datetime
    SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @FailedPasswordAnswerAttemptCount int
    SET @FailedPasswordAnswerAttemptCount = 0

    DECLARE @FailedPasswordAnswerAttemptWindowStart  datetime
    SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @NewUserCreated bit
    DECLARE @ReturnValue   int
    SET @ReturnValue = 0

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    SET @CreateDate = @CurrentTimeUtc

    SELECT  @NewUserId = UserId FROM dbo.aspnet_Users WHERE LOWER(@UserName) = LoweredUserName AND @ApplicationId = ApplicationId
    IF ( @NewUserId IS NULL )
    BEGIN
        SET @NewUserId = @UserId
        EXEC @ReturnValue = dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CreateDate, @NewUserId OUTPUT
        SET @NewUserCreated = 1
    END
    ELSE
    BEGIN
        SET @NewUserCreated = 0
        IF( @NewUserId <> @UserId AND @UserId IS NOT NULL )
        BEGIN
            SET @ErrorCode = 6
            GOTO Cleanup
        END
    END

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @ReturnValue = -1 )
    BEGIN
        SET @ErrorCode = 10
        GOTO Cleanup
    END

    IF ( EXISTS ( SELECT UserId
                  FROM   dbo.aspnet_Membership
                  WHERE  @NewUserId = UserId ) )
    BEGIN
        SET @ErrorCode = 6
        GOTO Cleanup
    END

    SET @UserId = @NewUserId

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership m WITH ( UPDLOCK, HOLDLOCK )
                    WHERE ApplicationId = @ApplicationId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            SET @ErrorCode = 7
            GOTO Cleanup
        END
    END

    IF (@NewUserCreated = 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate = @CreateDate
        WHERE  @UserId = UserId
        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    INSERT INTO dbo.aspnet_Membership
                ( ApplicationId,
                  UserId,
                  Password,
                  PasswordSalt,
                  Email,
                  LoweredEmail,
                  PasswordQuestion,
                  PasswordAnswer,
                  PasswordFormat,
                  IsApproved,
                  IsLockedOut,
                  CreateDate,
                  LastLoginDate,
                  LastPasswordChangedDate,
                  LastLockoutDate,
                  FailedPasswordAttemptCount,
                  FailedPasswordAttemptWindowStart,
                  FailedPasswordAnswerAttemptCount,
                  FailedPasswordAnswerAttemptWindowStart )
         VALUES ( @ApplicationId,
                  @UserId,
                  @Password,
                  @PasswordSalt,
                  @Email,
                  LOWER(@Email),
                  @PasswordQuestion,
                  @PasswordAnswer,
                  @PasswordFormat,
                  @IsApproved,
                  @IsLockedOut,
                  @CreateDate,
                  @CreateDate,
                  @CreateDate,
                  @LastLockoutDate,
                  @FailedPasswordAttemptCount,
                  @FailedPasswordAttemptWindowStart,
                  @FailedPasswordAnswerAttemptCount,
                  @FailedPasswordAnswerAttemptWindowStart )

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  Table [dbo].[HierarchyToTrainingProgramLink]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HierarchyToTrainingProgramLink]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HierarchyToTrainingProgramLink](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TrainingProgramID] [int] NOT NULL,
	[HierarchyNodeID] [int] NOT NULL,
	[HierarchyTypeID] [int] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[IsPrimary] [bit] NOT NULL
 CONSTRAINT [PK_HierarchyToTrainingProgramLink_1] PRIMARY KEY CLUSTERED 
(
	[TrainingProgramID] ASC,
	[HierarchyNodeID] ASC,
	[HierarchyTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HierarchyToTrainingProgramLink]') AND name = N'ix_hierarchytypeid')
CREATE NONCLUSTERED INDEX [ix_hierarchytypeid] ON [dbo].[HierarchyToTrainingProgramLink] 
(
	[HierarchyTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HierarchyToTrainingProgramLink]') AND name = N'ix_trainingprogramid')
CREATE NONCLUSTERED INDEX [ix_trainingprogramid] ON [dbo].[HierarchyToTrainingProgramLink] 
(
	[TrainingProgramID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
    @ApplicationName       nvarchar(256),
    @UserName              nvarchar(256),
    @NewPasswordQuestion   nvarchar(256),
    @NewPasswordAnswer     nvarchar(128)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Membership m, dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId
    IF (@UserId IS NULL)
    BEGIN
        RETURN(1)
    END

    UPDATE dbo.aspnet_Membership
    SET    PasswordQuestion = @NewPasswordQuestion, PasswordAnswer = @NewPasswordAnswer
    WHERE  UserId=@UserId
    RETURN(0)
END
' 
END
GO
/****** Object:  Table [dbo].[GTMGroup]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GTMGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[GTMGroup](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GroupKey] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CodeName] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_ApplicationGroups] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SalesChannelID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[ParentCustomerID] [int] NOT NULL,
	[EvaluationIndicator] [bit] NOT NULL,
	[EvaluationEndDate] [datetime] NULL,
	[SuspendedIndicator] [bit] NOT NULL,
	[UserLimit] [int] NOT NULL,
	[SelfRegistrationIndicator] [bit] NOT NULL,
	[SelfRegistrationPassword] [nvarchar](128) NULL,
	[SelfRegistrationPasswordSalt] [nvarchar](128) NULL,
	[StrongPasswordLength] [int] NOT NULL,
	[PasswordMaskID] [int] NOT NULL,
	[LockoutCount] [int] NOT NULL,
	[SubDomain] [varchar](50) NOT NULL,
	[UserNameAlias] [nvarchar](50) NULL,
	[LocationAlias] [varchar](50) NULL,
	[JobRoleAlias] [varchar](50) NULL,
	[AudienceAlias] [varchar](50) NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[QuickQueryIndicator] [bit] NOT NULL,
	[ReportBuilderIndicator] [bit] NOT NULL,
	[NewHireDuration] [int] NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CourseSurveys]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CourseSurveys]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CourseSurveys](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Url] [nvarchar](250) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseSur__Modif__0BC6C43E]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseSur__Creat__0CBAE877]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseSur__Modif__0DAF0CB0]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseSur__Creat__0EA330E9]  DEFAULT (getdate()),
 CONSTRAINT [PK_CourseSurveys] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CourseGrade]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CourseGrade]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CourseGrade](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CourseID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[Grade] [nchar](3) NOT NULL,
	[PassIndicator] [bit] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseGra__Modif__0A338187]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseGra__Creat__0B27A5C0]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseGra__Modif__0C1BC9F9]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseGra__Creat__0D0FEE32]  DEFAULT (getdate()),
 CONSTRAINT [PK_CourseGrade] PRIMARY KEY CLUSTERED 
(
	[CourseID] ASC,
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetUserState]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetUserState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetUserState] (
    @Count                  int                 OUT,
    @ApplicationName        NVARCHAR(256),
    @InactiveSinceDate      DATETIME            = NULL,
    @UserName               NVARCHAR(256)       = NULL,
    @Path                   NVARCHAR(256)       = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser
        WHERE Id IN (SELECT PerUser.Id
                     FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
                     WHERE Paths.ApplicationId = @ApplicationId
                           AND PerUser.UserId = Users.UserId
                           AND PerUser.PathId = Paths.PathId
                           AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
                           AND (@UserName IS NULL OR Users.LoweredUserName = LOWER(@UserName))
                           AND (@Path IS NULL OR Paths.LoweredPath = LOWER(@Path)))

        SELECT @Count = @@ROWCOUNT
    END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetSharedState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetSharedState] (
    @Count int OUT,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationAllUsers
        WHERE PathId IN
            (SELECT AllUsers.PathId
             FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
             WHERE Paths.ApplicationId = @ApplicationId
                   AND AllUsers.PathId = Paths.PathId
                   AND Paths.LoweredPath = LOWER(@Path))

        SELECT @Count = @@ROWCOUNT
    END
END
' 
END
GO
/****** Object:  Table [dbo].[CourseFile]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CourseFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CourseFile](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FileName] [nvarchar](100) NOT NULL,
	[OriginalFileName] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](200) NOT NULL,
	[Title] [nvarchar](200) NOT NULL,
	[Path] [nvarchar](256) NOT NULL,
	[FileTypeID] [int] NOT NULL,
	[FileSize] [bigint] NOT NULL,
	[FileBytes] [varbinary](max) NOT NULL,
	[FileExtension] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CustomerID] [int] NOT NULL,
	[CourseID] [int] NOT NULL,
 CONSTRAINT [PK_CourseFile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_GetCountOfState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_GetCountOfState] (
    @Count int OUT,
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN

    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
        IF (@AllUsersScope = 1)
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND AllUsers.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
        ELSE
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND PerUser.UserId = Users.UserId
                  AND PerUser.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
                  AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
                  AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_FindState]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_FindState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_FindState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @PageIndex              INT,
    @PageSize               INT,
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound INT
    DECLARE @PageUpperBound INT
    DECLARE @TotalRecords   INT
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table to store the selected results
    CREATE TABLE #PageIndex (
        IndexId int IDENTITY (0, 1) NOT NULL,
        ItemId UNIQUEIDENTIFIER
    )

    IF (@AllUsersScope = 1)
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT Paths.PathId
        FROM dbo.aspnet_Paths Paths,
             ((SELECT Paths.PathId
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND AllUsers.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT DISTINCT Paths.PathId
               FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND PerUser.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path,
               SharedDataPerPath.LastUpdatedDate,
               SharedDataPerPath.SharedDataLength,
               UserDataPerPath.UserDataLength,
               UserDataPerPath.UserCount
        FROM dbo.aspnet_Paths Paths,
             ((SELECT PageIndex.ItemId AS PathId,
                      AllUsers.LastUpdatedDate AS LastUpdatedDate,
                      DATALENGTH(AllUsers.PageSettings) AS SharedDataLength
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, #PageIndex PageIndex
               WHERE AllUsers.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT PageIndex.ItemId AS PathId,
                      SUM(DATALENGTH(PerUser.PageSettings)) AS UserDataLength,
                      COUNT(*) AS UserCount
               FROM aspnet_PersonalizationPerUser PerUser, #PageIndex PageIndex
               WHERE PerUser.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
               GROUP BY PageIndex.ItemId
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC
    END
    ELSE
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT PerUser.Id
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
        WHERE Paths.ApplicationId = @ApplicationId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
              AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
        ORDER BY Paths.Path ASC, Users.UserName ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path, PerUser.LastUpdatedDate, DATALENGTH(PerUser.PageSettings), Users.UserName, Users.LastActivityDate
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths, #PageIndex PageIndex
        WHERE PerUser.Id = PageIndex.ItemId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
        ORDER BY Paths.Path ASC, Users.UserName ASC
    END

    RETURN @TotalRecords
END
' 
END
GO
/****** Object:  Table [dbo].[CourseDurationUnit]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CourseDurationUnit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CourseDurationUnit](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Factor] [decimal](10, 2) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseDur__Modif__0BC6C43E]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseDur__Creat__0CBAE877]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseDur__Modif__0DAF0CB0]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseDur__Creat__0EA330E9]  DEFAULT (getdate()),
 CONSTRAINT [PK_CourseDurationUnit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_DeleteAllState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_DeleteAllState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Count int OUT)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        IF (@AllUsersScope = 1)
            DELETE FROM aspnet_PersonalizationAllUsers
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)
        ELSE
            DELETE FROM aspnet_PersonalizationPerUser
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)

        SELECT @Count = @@ROWCOUNT
    END
END
' 
END
GO
/****** Object:  Table [dbo].[CourseType]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CourseType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CourseType](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL CONSTRAINT [DF_CourseType_Name]  DEFAULT (''),
	[Description] [nvarchar](50) NOT NULL CONSTRAINT [DF_CourseType_Description]  DEFAULT (''),
	[CodeName] [nvarchar](10) NOT NULL CONSTRAINT [DF_CourseType_CodeName]  DEFAULT (''),
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseTyp__Modif__2B0A656D]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseTyp__Creat__2BFE89A6]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseTyp__Modif__2CF2ADDF]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseTyp__Creat__2DE6D218]  DEFAULT (getdate()),
 CONSTRAINT [PK_CourseType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CourseCompletionTypeParameter]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CourseCompletionTypeParameter]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CourseCompletionTypeParameter](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CourseCompletionTypeParameter] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Paths_CreatePath]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths_CreatePath]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Paths_CreatePath]
    @ApplicationId UNIQUEIDENTIFIER,
    @Path           NVARCHAR(256),
    @PathId         UNIQUEIDENTIFIER OUTPUT
AS
BEGIN
    BEGIN TRANSACTION
    IF (NOT EXISTS(SELECT * FROM dbo.aspnet_Paths WHERE LoweredPath = LOWER(@Path) AND ApplicationId = @ApplicationId))
    BEGIN
        INSERT dbo.aspnet_Paths (ApplicationId, Path, LoweredPath) VALUES (@ApplicationId, @Path, LOWER(@Path))
    END
    COMMIT TRANSACTION
    SELECT @PathId = PathId FROM dbo.aspnet_Paths WHERE LOWER(@Path) = LoweredPath AND ApplicationId = @ApplicationId
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[CompletedNewHire]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompletedNewHire]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CompletedNewHire]
	-- Add the parameters for the stored procedure here
	@iUserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF ((SELECT COUNT(*) FROM CoursesRequired WHERE UserID = @iUserID AND IsNewHire = 1 AND LOWER(Status) <> ''passed'') > 0)
	BEGIN
		SELECT 0
	END
	ELSE
	BEGIN
		SELECT 1
	END
END
' 
END
GO
/****** Object:  Table [dbo].[CourseCompletionType]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CourseCompletionType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CourseCompletionType](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[CodeName] [nvarchar](50) NOT NULL CONSTRAINT [DF_CourseCompletionType_CodeName]  DEFAULT (''),
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseCom__Modif__0BC6C43E]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseCom__Creat__0CBAE877]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseCom__Modif__0DAF0CB0]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseCom__Creat__0EA330E9]  DEFAULT (getdate()),
 CONSTRAINT [PK_CourseCompletionType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  UserDefinedFunction [dbo].[fInTrainingCycle]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fInTrainingCycle]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fInTrainingCycle]
(
	-- Add the parameters for the function here
	@StartDate datetime,
	@StopDate datetime
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @now datetime
	DECLARE @in bit
	
	SET @now = GETDATE()

	IF (@StartDate <= @now AND @StopDate >= @now)
	BEGIN
		SET @in = 1
	END
	ELSE
	BEGIN
		SET @in = 0
	END

	RETURN @in
END
' 
END
GO
/****** Object:  Table [dbo].[FileType]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FileType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FileType](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__FileType__Modifi__2E1BDC42]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__FileType__Create__2F10007B]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__FileType__Modifi__300424B4]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__FileType__Create__30F848ED]  DEFAULT (getdate()),
 CONSTRAINT [PK_FileType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  UserDefinedFunction [dbo].[fSplit]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fSplit]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

create function [dbo].[fSplit](
    @sInputList varchar(8000) -- List of delimited items
  , @sDelimiter varchar(8000) = '','' -- delimiter that separates items
) returns @List TABLE (item varchar(8000))

begin
declare @sItem varchar(8000)
while charindex(@sDelimiter,@sInputList,0) <> 0
 begin
 select
  @sItem=rtrim(ltrim(substring(@sInputList,1,charindex(@sDelimiter,@sInputList,0)-1))),
  @sInputList=rtrim(ltrim(substring(@sInputList,charindex(@sDelimiter,@sInputList,0)+len(@sDelimiter),len(@sInputList))))
 
 if len(@sItem) > 0
  insert into @List select @sItem
 end

if len(@sInputList) > 0
 insert into @List select @sInputList -- Put the last item in
return
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ChangeLoginName]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChangeLoginName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE procedure [dbo].[ChangeLoginName](@OldName nvarchar(256), @NewName nvarchar(256), @Application nvarchar(256))
as
begin
	update
		aspnet_Users
	set
		Username = @NewName,
		LoweredUserName = lower(@NewName)
	where
		Username = @OldName
	and
		ApplicationID = (select ApplicationID from aspnet_Applications where LoweredApplicationName = lower(@Application))
end
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fGetCertificateURL]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetCertificateURL]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Jerod Venema
-- Create date: 6/19/08
-- Description:	Builds a url for use in showing a certificate
-- =============================================
CREATE FUNCTION [dbo].[fGetCertificateURL]
(
	@Score varchar(15), @CourseTitle nvarchar(100), @CourseType nvarchar(20), @Date datetime
)
RETURNS nvarchar(500)
AS
BEGIN
	--all the select .... for xml path is doing is nicely converting our text to html-compliant text.
	declare @result nvarchar(500) 
	set @result = 
				''score='' + (select @Score as [data()] for xml path('''')) + 
				''&name='' + ''{User.Name}'' + 
				''&course='' + replace((select @CourseTitle as [data()] for xml path('''')),'' '',''%20'') +
				''&type='' + replace((select @CourseType as [data()] for xml path('''')) + '' Course Completed'','' '',''%20'') +
				''&month='' + CAST(MONTH(@Date) AS NVARCHAR(2)) +
				''&day='' + CAST(DAY(@Date) AS NVARCHAR(2)) +
				''&year='' + CAST(YEAR(@Date) AS NVARCHAR(4))
	return ''http://test.service.betraining.com/Scorm/shared/resources/printcert.html?'' + @result;
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[BulkImportSupervisorFix]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BulkImportSupervisorFix]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[BulkImportSupervisorFix]
	@CustomerID				int
AS
BEGIN
	DECLARE @RoleID varchar(100)
	DECLARE @ApplicationID varchar(100)

	SELECT 
		@ApplicationID = aa.ApplicationID,
		@RoleID = ar.RoleID
	FROM
		Customer c 
	INNER JOIN aspnet_Applications aa ON 
		aa.ApplicationName = c.SubDomain
	INNER JOIN aspnet_Roles ar ON
		aa.ApplicationID = ar.ApplicationID
	WHERE
		c.ID = @CustomerID
	AND
		ar.RoleName = ''Classroom - Supervisor''

	PRINT @ApplicationID
	PRINT @RoleID

	INSERT INTO
		aspnet_UsersInRoles
	(
		UserID,
		RoleID
	)
	SELECT
		au.UserID,
		@RoleID
	FROM
		aspnet_Users au
	INNER JOIN [User] u ON
		au.UserName = u.UserName
	WHERE
		au.ApplicationID = @ApplicationID
	AND
		u.ID IN (SELECT DISTINCT(SupervisorID) FROM [User] WHERE CustomerID = @CustomerID AND SupervisorID > 0)
	AND
		au.UserID NOT IN (SELECT UserID FROM aspnet_UsersInRoles WHERE RoleID = @RoleID)
		
END
' 
END
GO
/****** Object:  Table [dbo].[SalesChannel]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SalesChannel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SalesChannel](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ParentSalesChannelID] [int] NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CustomerCareAlias] [nvarchar](50) NULL,
	[AccountExecutiveAlias] [nvarchar](50) NULL,
 CONSTRAINT [PK_SalesChannel] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[BE_InsertScoreForNBT]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BE_InsertScoreForNBT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BE_InsertScoreForNBT] 
	-- Add the parameters for the stored procedure here
	@firstname nvarchar(100),
	@lastname  nvarchar(100),
	@score	 nvarchar(3),
	@courseid int
AS
BEGIN

INSERT INTO [dbo].[Scorm_BE_ExternalReg]
           ([lms_user_name]
           ,[package_id]
           ,[due_date]
           ,[CustomerID]
           ,[for_credit]
           ,[complete]
           ,[score]
           ,[total_time]
           ,[passed]
           ,[attemptMax]
           ,[attemptCount]
           ,[ModifiedBy]
           ,[CreatedBy]
           ,[ModifiedOn]
           ,[CreatedOn]
           ,[FullName]
           ,[launchCount])
SELECT      
           u.username
           ,tpc.CourseID -- CourseID
           ,tp.TrainingProgramDuedate  -- Due Date
           ,u.customerid  -- CustomerID
           ,1  -- For Credit
           ,2  -- complete?
           ,@score -- score
           ,9999999  -- total time
           ,2  -- pass?
           ,1  -- attempt
           ,1	-- count
           ,''Nu Insert Score''
           ,''Nu''
           ,getdate()
           ,getdate()
           ,u.firstname + '' '' + u.lastname as fullname
           ,2 -- Launch Count
from customerBlackbox.dbo.[user] u
inner join courseAssignment.dbo.TrainingProgramUsers tp
	on u.id = tp.userid
inner join courseAssignment.dbo.TrainingProgramCourses tpc
	on tp.trainingProgramId = tpc.trainingProgramId
where
 u.firstname=@firstname and u.lastname = @lastname
and u.customerid = 80
and tpc.courseID = @courseID
and not exists (select * from [dbo].[Scorm_BE_ExternalReg]
	where due_date = tp.TrainingProgramDuedate 
		and Package_id = tpc.courseid
		and lms_user_name = u.username)



select xt.* from [dbo].[Scorm_BE_ExternalReg] xt
inner join customerBlackbox.dbo.[user] u
on xt.lms_user_name = u.username
where 
u.firstname=@firstname and u.lastname = @lastname


end
' 
END
GO
/****** Object:  Table [dbo].[ScheduleParameters]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ScheduleParameters]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ScheduleParameters](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DisplayName] [nvarchar](50) NOT NULL,
	[CodeName] [nvarchar](30) NOT NULL DEFAULT (''),
	[CanFilterComplete] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_ScheduleParameter] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ImportType]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ImportType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ImportType](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Label] [nvarchar](100) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_ImportType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[RegistrationType]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RegistrationType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RegistrationType](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__Registrat__Modif__0F2D40CE]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__Registrat__Creat__10216507]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__Registrat__Modif__11158940]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__Registrat__Creat__1209AD79]  DEFAULT (getdate()),
 CONSTRAINT [PK_RegistrationType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PossibleProblem]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PossibleProblem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PossibleProblem](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](1000) NOT NULL,
	[Subject] [nvarchar](250) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[CourseTypeID] [int] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_PossibleProblem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[RecipientGroups]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RecipientGroups]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RecipientGroups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Selector] [nvarchar](150) NOT NULL,
	[DisplayName] [nvarchar](50) NOT NULL,
	[ValidForType] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Recipients] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PresentationType]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PresentationType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PresentationType](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseTyp__Modif__173876EA]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CourseTyp__Creat__182C9B23]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseTyp__Modif__1920BF5C]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__CourseTyp__Creat__1A14E395]  DEFAULT (getdate()),
 CONSTRAINT [PK_PresentationType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteProfiles]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteProfiles]
    @ApplicationName        nvarchar(256),
    @UserNames              nvarchar(4000)
AS
BEGIN
    DECLARE @UserName     nvarchar(256)
    DECLARE @CurrentPos   int
    DECLARE @NextPos      int
    DECLARE @NumDeleted   int
    DECLARE @DeletedUser  int
    DECLARE @TranStarted  bit
    DECLARE @ErrorCode    int

    SET @ErrorCode = 0
    SET @CurrentPos = 1
    SET @NumDeleted = 0
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    WHILE (@CurrentPos <= LEN(@UserNames))
    BEGIN
        SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @CurrentPos)
        IF (@NextPos = 0 OR @NextPos IS NULL)
            SELECT @NextPos = LEN(@UserNames) + 1

        SELECT @UserName = SUBSTRING(@UserNames, @CurrentPos, @NextPos - @CurrentPos)
        SELECT @CurrentPos = @NextPos+1

        IF (LEN(@UserName) > 0)
        BEGIN
            SELECT @DeletedUser = 0
            EXEC dbo.aspnet_Users_DeleteUser @ApplicationName, @UserName, 4, @DeletedUser OUTPUT
            IF( @@ERROR <> 0 )
            BEGIN
                SET @ErrorCode = -1
                GOTO Cleanup
            END
            IF (@DeletedUser <> 0)
                SELECT @NumDeleted = @NumDeleted + 1
        END
    END
    SELECT @NumDeleted
    IF (@TranStarted = 1)
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END
    SET @TranStarted = 0

    RETURN 0

Cleanup:
    IF (@TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END
    RETURN @ErrorCode
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[permGetTreeRoots]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[permGetTreeRoots]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Jeff L Greenwell
-- Create date: 12/31/2007
-- Description:	get tree roots
-- =============================================
CREATE PROCEDURE [dbo].[permGetTreeRoots] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT *
	FROM Permissions_Tree
	WHERE ParentID = 0
	ORDER BY LeftID ASC
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[TrainingProgramOwners]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainingProgramOwners]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TrainingProgramOwners]
(@TrainingProgramID int )
AS 
BEGIN
	select 1;
END
' 
END
GO
/****** Object:  Table [dbo].[Status]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Status]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Status](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__Status__Modified__12FDD1B2]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__Status__CreatedB__13F1F5EB]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__Status__Modified__14E61A24]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__Status__CreatedO__15DA3E5D]  DEFAULT (getdate()),
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[States]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[States]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[States](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nchar](2) NOT NULL CONSTRAINT [DF_States_Name]  DEFAULT (''),
	[Description] [nvarchar](50) NOT NULL CONSTRAINT [DF_States_Description]  DEFAULT (''),
 CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TrainingProgram]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainingProgram]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrainingProgram](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CustomerID] [int] NOT NULL CONSTRAINT [DF_TrainingProgram_CustomerID]  DEFAULT ((0)),
	[OwnerID] [int] NOT NULL CONSTRAINT [DF_TrainingProgram_OwnerID]  DEFAULT ((0)),
	[Name] [nvarchar](50) NOT NULL,
	[InternalCode] [nvarchar](50) NULL CONSTRAINT [DF_TrainingProgram_InternalCode]  DEFAULT (''),
	[Cost] [decimal](10, 2) NOT NULL CONSTRAINT [DF_TrainingProgram_Cost]  DEFAULT ((0)),
	[Description] [nvarchar](max) NOT NULL CONSTRAINT [DF_TrainingProgram_Description]  DEFAULT (''),
	[IsNewHire] [bit] NOT NULL CONSTRAINT [DF_TrainingProgram_IsNewHire]  DEFAULT ((0)),
	[IsLive] [bit] NOT NULL CONSTRAINT [DF_TrainingProgram_IsLive]  DEFAULT ((0)),
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[DueDate] [datetime] NOT NULL,
	[EnforceRequiredOrder] [bit] NOT NULL CONSTRAINT [DF_TrainingProgram_EnforceRequiredOrder]  DEFAULT ((0)),
	[MinimumElectives] [int] NULL,
	[FinalAssessmentCourseID] [int] NULL,
	[FinalAssessmentCourseTypeID] [int] NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_TrainingProgram_ModifiedBy]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_TrainingProgram_CreatedBy]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF_TrainingProgram_ModifiedOn]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_TrainingProgram_CreatedOn]  DEFAULT (getdate()),
 CONSTRAINT [PK_TrainingProgram] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrainingProgram]') AND name = N'ix_customerid')
CREATE NONCLUSTERED INDEX [ix_customerid] ON [dbo].[TrainingProgram] 
(
	[CustomerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrainingProgram]') AND name = N'ix_islive')
CREATE NONCLUSTERED INDEX [ix_islive] ON [dbo].[TrainingProgram] 
(
	[IsLive] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrainingProgram]') AND name = N'ix_isnewhire')
CREATE NONCLUSTERED INDEX [ix_isnewhire] ON [dbo].[TrainingProgram] 
(
	[IsNewHire] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TimeZone]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TimeZone]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TimeZone](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__TimeZone__Modifi__060DEAE8]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__TimeZone__Create__07020F21]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__TimeZone__Modifi__07F6335A]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__TimeZone__Create__08EA5793]  DEFAULT (getdate()),
 CONSTRAINT [PK_TimeZone] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TemplatesToRecipientGroupsMap]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TemplatesToRecipientGroupsMap]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TemplatesToRecipientGroupsMap](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RecipientGroupID] [int] NOT NULL,
	[TemplateID] [int] NOT NULL,
 CONSTRAINT [PK_NotificationsToRecipientGroupsMap] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TemplateScheduleHistory]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TemplateScheduleHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TemplateScheduleHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateScheduleID] [int] NOT NULL,
	[ObjectID] [int] NOT NULL,
	[ObjectDateTime] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_TemplateScheduleHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Templates]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Templates]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Templates](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](200) NOT NULL,
	[Body] [nvarchar](2000) NOT NULL,
	[CodeName] [nvarchar](50) NOT NULL,
	[DisplayName] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](2000) NOT NULL,
	[Priority] [tinyint] NOT NULL CONSTRAINT [DF__Templates__Prior__0519C6AF]  DEFAULT ((1)),
	[CustomerID] [int] NOT NULL CONSTRAINT [DF__Templates__Custo__060DEAE8]  DEFAULT ((0)),
	[Enabled] [bit] NOT NULL CONSTRAINT [DF__Templates__Enabl__07020F21]  DEFAULT ((0)),
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Area] [nvarchar](20) NOT NULL CONSTRAINT [DF__Templates__Area__108B795B]  DEFAULT (''),
	[IsScheduled] [bit] NOT NULL CONSTRAINT [DF_Templates_IsScheduled]  DEFAULT ((0)),
	[ScheduleParameterID] [int] NOT NULL CONSTRAINT [DF_Templates_SchedulePosition]  DEFAULT ((0)),
	[RelativeScheduledMinutes] [bigint] NOT NULL CONSTRAINT [DF_Templates_ScheduledMinutes]  DEFAULT ((0)),
	[SearchText]  AS (([DisplayName]+' ')+[Area]),
	[FilterComplete] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_Templates] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SyllabusType]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SyllabusType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SyllabusType](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL CONSTRAINT [DF_SyllabusType_Name]  DEFAULT (''),
	[Description] [nvarchar](50) NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_SyllabusType_ModifiedBy]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_SyllabusType_CreatedBy]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF_SyllabusType_ModifiedOn]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_SyllabusType_CreatedOn]  DEFAULT (getdate()),
 CONSTRAINT [PK_SyllabusType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[StudentTranscriptLatestAttemptsCache]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StudentTranscriptLatestAttemptsCache]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StudentTranscriptLatestAttemptsCache](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CustomerID] [int] NULL,
	[UserID] [int] NULL,
	[CourseID] [int] NULL,
	[CourseType] [varchar](9) NULL,
	[CourseTypeID] [int] NULL,
	[ClassID] [int] NULL,
	[RegistrationID] [int] NULL,
	[CourseName] [varchar](50) NULL,
	[Score] [varchar](50) NULL,
	[AttemptDate] [datetime] NULL,
	[AttemptTime] [int] NULL,
	[AttemptCount] [int] NULL,
	[PassOrFail] [varchar](7) NULL,
	[Passed] [int] NULL,
	[CompleteOrIncomplete] [varchar](10) NULL,
	[Completed] [int] NULL,
	[OnlineTestID] [int] NULL,
	[HasStarted] [int] NULL,
	[HasEnded] [int] NULL,
	[MinClassDate] [datetime] NULL,
	[MaxClassDate] [datetime] NULL,
	[concatid] [varchar](max) NULL,
	[maxdate] [datetime] NULL,
 CONSTRAINT [PK_StudentTranscriptLatestAttemptsCache] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrainingProgramFiles]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainingProgramFiles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrainingProgramFiles](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TrainingProgramID] [int] NOT NULL,
	[FileName] [nvarchar](500) NOT NULL,
	[FileBytes] [image] NOT NULL,
	[Description] [nvarchar](max) NOT NULL CONSTRAINT [DF_TrainingProgramFiles_Description]  DEFAULT (''),
	[Title] [nvarchar](500) NOT NULL CONSTRAINT [DF_TrainingProgramFiles_Title]  DEFAULT (''),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_TrainingProgramFiles_CreatedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_TrainingProgramFiles_CreatedBy]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF_TrainingProgramFiles_ModifiedOn]  DEFAULT (getdate()),
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_TrainingProgramFiles_ModifiedBy]  DEFAULT (''),
 CONSTRAINT [PK_TrainingProgramFiles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserStatus]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserStatus](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__UserStatu__Modif__20C1E124]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__UserStatu__Creat__21B6055D]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__UserStatu__Modif__22AA2996]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__UserStatu__Creat__239E4DCF]  DEFAULT (getdate()),
 CONSTRAINT [PK_UserStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[UserStatus]') AND name = N'status_index')
CREATE NONCLUSTERED INDEX [status_index] ON [dbo].[UserStatus] 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[GetClassesByParams]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetClassesByParams]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author: <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetClassesByParams]
-- Add the parameters for the stored procedure here
@iUserID int,
@iCustomerID int,
@bILTManagerUsersRegistered bit,
@bILTManagerUsersWaitListed bit,
@bILTManagerUsersAwaitingApproval bit,
@bILTManagerUsersDenied bit,
@bILTManagerAll bit,
@bInstructor bit,
@bResourceManagerWithRequired bit,
@bResourceManagerWithoutRequired bit,
@bAvailableClassesStudent bit,
@bAwaitingApprovalStudent bit,
@bRegisteredStudent bit,
@bWaitListStudent bit,
@bDeniedStudent bit,
@bSupervisorUsersRegistered bit,
@bSupervisorUsersWaitListed bit,
@bSupervisorUsersAwaitingApproval bit,
@bSupervisorUsersDenied bit,
@sSupervisedUserList nvarchar(max),
@sLikeText nvarchar(max),
@iStart int,
@iLimit int,
@sSortCol varchar(200),
@sSortDir varchar(4)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

select *,
(case when MaxStartDateTime >= dateadd(day,-14,getdate()) then MinStartDateTime else ''1/1/3000'' end) AS OrderByCol
from dbo.fGetClasses(@iUserID, @iCustomerID,
@bILTManagerUsersRegistered, @bILTManagerUsersWaitListed, @bILTManagerUsersAwaitingApproval, @bILTManagerUsersDenied,@bILTManagerAll,
@bInstructor,
@bResourceManagerWithRequired, @bResourceManagerWithoutRequired,
@bAvailableClassesStudent, @bAwaitingApprovalStudent, @bRegisteredStudent, @bWaitListStudent, @bDeniedStudent,
@bSupervisorUsersRegistered, @bSupervisorUsersWaitListed, @bSupervisorUsersAwaitingApproval, @bSupervisorUsersDenied, @sSupervisedUserList,
@sLikeText, @iStart, @iLimit,@sSortCol,@sSortDir)
--order by

--order by OrderByCol, MinStartDateTime DESC
--where
--maxstartdatetime >= dateadd(day,-14,getdate()) --all classes that have at least one start date either in the future or the last 2 weeks
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetClassDatesByParams]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetClassDatesByParams]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author: <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetClassDatesByParams]
-- Add the parameters for the stored procedure here
@dtStart datetime,
@dtEnd datetime,
@iUserID int,
@iCustomerID int,
@bILTManagerUsersRegistered bit,
@bILTManagerUsersWaitListed bit,
@bILTManagerUsersAwaitingApproval bit,
@bILTManagerUsersDenied bit,
@bILTManagerAll bit,
@bInstructor bit,
@bResourceManagerWithRequired bit,
@bResourceManagerWithoutRequired bit,
@bAvailableClassesStudent bit,
@bAwaitingApprovalStudent bit,
@bRegisteredStudent bit,
@bWaitListStudent bit,
@bDeniedStudent bit,
@bSupervisorUsersRegistered bit,
@bSupervisorUsersWaitListed bit,
@bSupervisorUsersAwaitingApproval bit,
@bSupervisorUsersDenied bit,
@sSupervisedUserList nvarchar(max),
@sLikeText nvarchar(max),
@iStart int,
@iLimit int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

select
cd.*,
IsILTManagerUsersRegistered,
IsILTManagerUsersWaitListed,
IsILTManagerUsersAwaitingApproval,
IsILTManagerUsersDenied,
IsILTManagerAll,
IsInstructor,
IsResourceManagerWithRequired,
IsResourceManagerWithoutRequired,
IsRegisteredStudent,
IsAvailableClassesStudent,
IsAwaitingApprovalStudent,
IsWaitListStudent,
IsDeniedStudent,
IsSupervisorUsersRegistered,
IsSupervisorUsersWaitListed,
IsSupervisorUsersAwaitingApproval,
IsSupervisorUsersDenied,
TotalRecordCount
from dbo.fGetClasses(@iUserID, @iCustomerID,
@bILTManagerUsersRegistered, @bILTManagerUsersWaitListed, @bILTManagerUsersAwaitingApproval, @bILTManagerUsersDenied, @bILTManagerAll,
@bInstructor,
@bResourceManagerWithRequired, @bResourceManagerWithoutRequired,
@bAvailableClassesStudent, @bAwaitingApprovalStudent, @bRegisteredStudent, @bWaitListStudent, @bDeniedStudent,
@bSupervisorUsersRegistered, @bSupervisorUsersWaitListed, @bSupervisorUsersAwaitingApproval, @bSupervisorUsersDenied, @sSupervisedUserList,
@sLikeText, @iStart, @iLimit,'''','''') as Classes
left join
[dbo].[ClassDate] as cd
on
Classes.ID = cd.ClassID

where
cd.StartDateTime between @dtStart and @dtEnd
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[showHigh]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[showHigh]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

create PROC [dbo].[showHigh]

(
@Root int
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @EmpID int, @EmpName varchar(30)

SET @EmpName = (SELECT Username FROM dbo.[User] WHERE ID = @Root)
PRINT REPLICATE(''-'', @@NESTLEVEL * 4) + @EmpName

SET @EmpID = (SELECT MIN(ID) FROM dbo.[User] WHERE SupervisorID = @Root)

WHILE @EmpID IS NOT NULL
BEGIN
EXEC dbo.ShowHierarchy @EmpID
SET @EmpID = (SELECT MIN(ID) FROM dbo.[User] WHERE SupervisorID = @Root AND ID > @EmpID)
END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ShowHierarchy]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShowHierarchy]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

create PROC [dbo].[ShowHierarchy]

(
@Root int
)
AS
BEGIN
SET NOCOUNT ON
DECLARE @EmpID int, @EmpName varchar(30)

SET @EmpName = (SELECT Username FROM dbo.[User] WHERE ID = @Root)
PRINT REPLICATE(''-'', @@NESTLEVEL * 4) + @EmpName

SET @EmpID = (SELECT MIN(ID) FROM dbo.[User] WHERE SupervisorID = @Root)

WHILE @EmpID IS NOT NULL
BEGIN
EXEC dbo.ShowHierarchy @EmpID
SET @EmpID = (SELECT MIN(ID) FROM dbo.[User] WHERE SupervisorID = @Root AND ID > @EmpID)
END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ResetAdmins]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ResetAdmins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[ResetAdmins]
	@iSalesChannelID int,
	@iExcludeUserID int,
	@bAccountExec bit,
	@bCustomerCareRep bit
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

-- Insert statements for procedure here
if(@bAccountExec = 1)
begin
	update [user] set IsAccountExec = 0 where id != @iExcludeUserID and SalesChannelID = @iSalesChannelID
end
else if(@bCustomerCareRep = 1)
begin
	update [user] set IsCustomerCare = 0 where id != @iExcludeUserID and SalesChannelID = @iSalesChannelID
end
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[permGetTreeNodeLevel]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[permGetTreeNodeLevel]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Jeff L Greenwell
-- Create date: 12/31/2007
-- Description:	return the level of a given node
-- =============================================
CREATE PROCEDURE [dbo].[permGetTreeNodeLevel] 
	@NodeID NVARCHAR(400) = N''0'',
	@ByTextID BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

	IF @ByTextID = 0
		SELECT COUNT(ParentID)
			FROM (SELECT DISTINCT ParentID
				FROM PermissionsNodes
				WHERE
					(LeftID < (SELECT RightID FROM PermissionsNodes WHERE ID = CASE ISNUMERIC(@NodeID) WHEN 0 THEN 0 ELSE CAST(@NodeID AS INTEGER) END)) AND
					(RightID >= (SELECT RightID FROM PermissionsNodes WHERE ID = CASE ISNUMERIC(@NodeID) WHEN 0 THEN 0 ELSE CAST(@NodeID AS INTEGER) END))
			) AS DT
	ELSE
		SELECT COUNT(ParentID)
			FROM (SELECT DISTINCT ParentID
				FROM PermissionsNodes
				WHERE
					(LeftID < (SELECT RightID FROM PermissionsNodes WHERE TextID = @NodeID)) AND
					(RightID >= (SELECT RightID FROM PermissionsNodes WHERE TextID = @NodeID))
			) AS DT
	RETURN
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[permGetTreeNode]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[permGetTreeNode]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Jeff L Greenwell
-- Create date: 12/31/2007
-- Description:	returns a tree node
-- =============================================
CREATE PROCEDURE [dbo].[permGetTreeNode] 
	@NodeID NVARCHAR(400) = N''0'',
	@ByTextID BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

	IF @ByTextID = 0
		SELECT * FROM PermissionsNodes
			WHERE ID = CASE ISNUMERIC(@NodeID) WHEN 0 THEN 0 ELSE CAST(@NodeID AS INTEGER) END
	ELSE
		SELECT * FROM PermissionsNodes
			WHERE TextID = @NodeID
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[permGetTreeBranchSize]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[permGetTreeBranchSize]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Jeff L Greenwell
-- Create date: 1/2/2007
-- Description:	number of children in a branch
-- =============================================
CREATE PROCEDURE [dbo].[permGetTreeBranchSize] 
	@NodeID NVARCHAR(400) = N''0'', 
	@ByTextID BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Node_ID INT
	DECLARE @Node_LeftID INT
	DECLARE @Node_RightID INT

	SET @Node_ID = 0
	SET @Node_LeftID = 0
	SET @Node_RightID = 0

	DECLARE @Node TABLE (ID INT, LeftID INT, RightID INT)
	IF @ByTextID = 0
		INSERT INTO @Node
			SELECT ID, LeftID, RightID FROM PermissionsNodes
			WHERE ID = CASE ISNUMERIC(@NodeID) WHEN 0 THEN 0 ELSE CAST(@NodeID AS INTEGER) END
	ELSE
		INSERT INTO @Node
			SELECT ID, LeftID, RightID FROM PermissionsNodes
			WHERE TextID = @NodeID

	SELECT @Node_ID = ID FROM @Node
	SELECT @Node_LeftID = LeftID FROM @Node
	SELECT @Node_RightID = RightID FROM @Node
	SELECT ((@Node_RightID - @Node_LeftID - 1) / 2)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[permGetTreeBranch]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[permGetTreeBranch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Jeff L Greenwell
-- Create date: 1/2/2007
-- Description:	get tree branch
-- =============================================
create PROCEDURE [dbo].[permGetTreeBranch] 
	@NodeID NVARCHAR(400) = N''0'', 
	@ByTextID BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Node_LeftID INT
	DECLARE @Node_RightID INT
	DECLARE @offset INT

	SET @Node_LeftID = 0
	SET @Node_LeftID = 0
	SET @offset = 0

	DECLARE @Node TABLE (ID INT, ParentID INT, TextID NVARCHAR(400), LeftID INT, RightID INT, Title NVARCHAR(400), Description NTEXT, CreatedOn DateTime, ModifiedOn DateTime, CreatedBy NVARCHAR(50), ModifiedBy NVARCHAR(50))
	IF @ByTextID = 0
		INSERT INTO @Node
			SELECT * FROM PermissionsNodes
				WHERE ID = CASE ISNUMERIC(@NodeID) WHEN 0 THEN 0 ELSE CAST(@NodeID AS INTEGER) END
	ELSE
		INSERT INTO @Node
			SELECT * FROM PermissionsNodes
				WHERE TextID = @NodeID

	SELECT @offset = LeftID FROM @Node
	SET @offset = @offset - 1
	SELECT @Node_LeftID = LeftID FROM @Node
	SELECT @Node_RightID = RightID FROM @Node

	DECLARE @Branch TABLE (ID INT, ParentID INT, TextID NVARCHAR(400), LeftID INT, RightID INT, Title NVARCHAR(400), Description NTEXT, CreatedOn DateTime, ModifiedOn DateTime, CreatedBy NVARCHAR(50), ModifiedBy NVARCHAR(50))

	INSERT INTO @Branch
		SELECT * FROM PermissionsNodes
			WHERE LeftID BETWEEN @Node_LeftID AND @Node_RightID

	UPDATE @Branch
		SET LeftID = (LeftID - @offset), RightID = (RightID - @offset)

	SELECT * FROM @Branch
		ORDER BY LeftID ASC
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[permAddTreeNode]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[permAddTreeNode]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Jeff L Greenwell
-- Create date: 12/31/2007
-- Description:	add a tree node
-- =============================================
CREATE PROCEDURE [dbo].[permAddTreeNode]
	@NodeID NVARCHAR(400) = N''0'',
	@TextID NVARCHAR(400) = N'''',
	@Title NVARCHAR(400) = N'''',
	@Desc TEXT = N'''',
	@ByTextID BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Node_ID INT
	DECLARE @Node_RightID INT
	DECLARE @Adjacent_RightID INT
	DECLARE @NewNodeID INT

	SET @Node_ID = 0
	SET @Node_RightID = 0
	SET @Adjacent_RightID = 0
	SET @NewNodeID = 0

	DECLARE @Node TABLE (ID INT, LeftID INT, RightID INT)
	IF @ByTextID = 0
		INSERT INTO @Node
			SELECT ID, LeftID, RightID FROM PermissionsNodes
				WHERE ID = CASE ISNUMERIC(@NodeID) WHEN 0 THEN 0 ELSE CAST(@NodeID AS INTEGER) END
	ELSE
		INSERT INTO @Node
			SELECT ID, LeftID, RightID FROM PermissionsNodes
				WHERE TextID = @NodeID

	SELECT @Node_ID = ID FROM @Node
	SELECT @Node_RightID = RightID FROM @Node

	DECLARE @Adjacent_Node TABLE (ID INT, LeftID INT, RightID INT)
	INSERT INTO @Adjacent_Node
		SELECT ID, LeftID, RightID FROM PermissionsNodes
			WHERE Title <= @Title AND ParentID = @Node_ID
			ORDER BY Title DESC, ID DESC

	SELECT @Adjacent_RightID = RightID FROM @Adjacent_Node
	IF @Adjacent_RightID = 0 BEGIN
		INSERT INTO @Adjacent_Node
			SELECT ID, LeftID, RightID FROM PermissionsNodes
				WHERE ID = @Node_ID
		SELECT @Adjacent_RightID = LeftID FROM @Adjacent_Node
	END

	UPDATE PermissionsNodes SET RightID = RightID + 2 WHERE RightID > @Adjacent_RightID
	UPDATE PermissionsNodes SET LeftID = LeftID + 2 WHERE LeftID > @Adjacent_RightID

	INSERT INTO PermissionsNodes
		(ParentID, TextID, LeftID, RightID, Title, Description, CreatedOn, ModifiedOn)
		VALUES
		(@Node_ID, @TextID, (@Adjacent_RightID + 1), (@Adjacent_RightID + 2), @Title, @Desc, GetDate(), GetDate())

	SELECT @NewNodeID = Scope_Identity()
	IF @TextID = N''''
		UPDATE PermissionsNodes
			SET TextID = @NewNodeID
			WHERE ID = @NewNodeID

	SELECT * FROM PermissionsNodes WHERE ID = @NewNodeID

	RETURN
END
' 
END
GO
/****** Object:  Table [dbo].[Audience]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Audience]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Audience](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[InternalCode] [nvarchar](50) NULL,
	[ParentAudienceID] [int] NULL,
	[SortOrder] [int] NOT NULL,
	[CustomerID] [int] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Audience] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Audience]') AND name = N'audiencename_index')
CREATE NONCLUSTERED INDEX [audiencename_index] ON [dbo].[Audience] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Audience]') AND name = N'parent_audience_index')
CREATE NONCLUSTERED INDEX [parent_audience_index] ON [dbo].[Audience] 
(
	[ParentAudienceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Audience]') AND name = N'status_index')
CREATE NONCLUSTERED INDEX [status_index] ON [dbo].[Audience] 
(
	[CustomerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[permUpdateTreeNode]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[permUpdateTreeNode]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Jeff L Greenwell
-- Create date: 1/2/2007
-- Description:	update a TreeNode
-- =============================================
CREATE PROCEDURE [dbo].[permUpdateTreeNode] 
	@NodeID NVARCHAR(400) = N''0'', 
	@TextID NVARCHAR(400) = N'''',
	@Title NVARCHAR(400) = N'''',
	@Desc NTEXT = N'''',
	@ByTextID BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

	IF @ByTextID = 0
		UPDATE PermissionsNodes
			SET TextID = @TextID, Title = @Title, Description = @Desc, ModifiedOn = GetDate()
				WHERE ID = CASE ISNUMERIC(@NodeID) WHEN 0 THEN 0 ELSE CAST(@NodeID AS INTEGER) END
	ELSE
		UPDATE PermissionsNodes
			SET TextID = @TextID, Title = @Title, Description = @Desc, ModifiedOn = GetDate()
				WHERE TextID = @NodeID

	IF @ByTextID = 0
		SELECT * FROM PermissionsNodes WHERE ID = CASE ISNUMERIC(@NodeID) WHEN 0 THEN 0 ELSE CAST(@NodeID AS INTEGER) END
	ELSE
		SELECT * FROM PermissionsNodes WHERE TextID = @NodeID
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[permRemoveTreeNode]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[permRemoveTreeNode]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		Jeff L Greenwell
-- Create date: 1/1/2007
-- Description:	Remove a tree node
-- =============================================
CREATE PROCEDURE [dbo].[permRemoveTreeNode] 
	@NodeID NVARCHAR(400) = N''0'',
	@ByTextID BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Node_ID INT
	DECLARE @Node_LeftID INT
	DECLARE @Node_RightID INT
	DECLARE @Adjacent_RightID INT
	DECLARE @Nodes_Count INT
	DECLARE @subtractor INT

	SET @Node_ID = 0
	SET @Node_LeftID = 0
	SET @Node_RightID = 0
	SET @Adjacent_RightID = 0
	SET @Nodes_Count = 0
	SET @subtractor = 0

	DECLARE @Node TABLE (ID INT, LeftID INT, RightID INT)
	IF @ByTextID = 0
		INSERT INTO @Node
			SELECT ID, LeftID, RightID FROM PermissionsNodes
				WHERE ID = CASE ISNUMERIC(@NodeID) WHEN 0 THEN 0 ELSE CAST(@NodeID AS INTEGER) END
	ELSE
		INSERT INTO @Node
			SELECT ID, LeftID, RightID FROM PermissionsNodes
				WHERE TextID = @NodeID

	SELECT @Node_ID = ID FROM @Node
	SELECT @Node_LeftID = LeftID FROM @Node
	SELECT @Node_RightID = RightID FROM @Node

	IF @Node_ID = 0 GOTO FINISH

	SELECT @Nodes_Count = COUNT(ID) FROM PermissionsNodes WHERE LeftID > @Node_LeftID AND RightID < @Node_RightID
	SET @subtractor = ((@Nodes_Count * 2) + 2)

	DELETE FROM PermissionsNodes
		WHERE (ID = @Node_ID) OR ((LeftID > @Node_LeftID) AND (RightID < @Node_RightID))

	UPDATE PermissionsNodes
		SET RightID = RightID - @subtractor
		WHERE RightID > @Node_RightID

	UPDATE PermissionsNodes
		SET LeftID = LeftID - @subtractor
		WHERE LeftID > @Node_RightID

FINISH:

	RETURN
END
' 
END
GO
/****** Object:  Table [dbo].[Attachments]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Attachments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Attachments](
	[ID] [int] NOT NULL,
	[Content] [varbinary](5000) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Attachments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[aspnet_WebEvent_Events]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_Events]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_WebEvent_Events](
	[EventId] [char](32) NOT NULL,
	[EventTimeUtc] [datetime] NOT NULL,
	[EventTime] [datetime] NOT NULL,
	[EventType] [nvarchar](256) NOT NULL,
	[EventSequence] [decimal](19, 0) NOT NULL,
	[EventOccurrence] [decimal](19, 0) NOT NULL,
	[EventCode] [int] NOT NULL,
	[EventDetailCode] [int] NOT NULL,
	[Message] [nvarchar](1024) NULL,
	[ApplicationPath] [nvarchar](256) NULL,
	[ApplicationVirtualPath] [nvarchar](256) NULL,
	[MachineName] [nvarchar](256) NOT NULL,
	[RequestUrl] [nvarchar](1024) NULL,
	[ExceptionType] [nvarchar](256) NULL,
	[Details] [ntext] NULL,
 CONSTRAINT [PK__aspnet_WebEvent___07020F21] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CoursePrerequisites]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CoursePrerequisites]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CoursePrerequisites](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CourseID] [int] NOT NULL,
	[CoursePrerequisiteID] [int] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CoursePre__Modif__71D1E811]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__CoursePre__Creat__72C60C4A]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__CoursePre__Modif__73BA3083]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__CoursePre__Creat__74AE54BC]  DEFAULT (getdate()),
 CONSTRAINT [PK_CoursePrerequisites] PRIMARY KEY CLUSTERED 
(
	[CourseID] ASC,
	[CoursePrerequisiteID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[KeywordCourse]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KeywordCourse]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KeywordCourse](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[KeywordID] [int] NOT NULL,
	[CourseID] [int] NOT NULL,
	[ActiveIndicator] [bit] NOT NULL,
	[PurgeIndicator] [bit] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__KeywordCo__Modif__33D4B598]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__KeywordCo__Creat__34C8D9D1]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__KeywordCo__Modif__35BCFE0A]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__KeywordCo__Creat__36B12243]  DEFAULT (getdate()),
 CONSTRAINT [PK_KeywordAsset_1] PRIMARY KEY CLUSTERED 
(
	[KeywordID] ASC,
	[CourseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Class]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Class]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Class](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](1000) NOT NULL,
	[InternalCode] [nvarchar](50) NOT NULL,
	[RoomID] [int] NOT NULL,
	[CourseID] [int] NOT NULL,
	[TimeZoneID] [int] NOT NULL,
	[CapacityOverride] [int] NOT NULL,
	[LockedIndicator] [bit] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[AdditionalInstructions] [nvarchar](1000) NOT NULL,
	[Notes] [nvarchar](1000) NOT NULL,
	[Objective] [nvarchar](1000) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[IsVirtual] [bit] NOT NULL,
	[SurveyID] [int] NOT NULL,
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Class]') AND name = N'class_customerindex')
CREATE NONCLUSTERED INDEX [class_customerindex] ON [dbo].[Class] 
(
	[CustomerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Class]') AND name = N'couseid_index')
CREATE NONCLUSTERED INDEX [couseid_index] ON [dbo].[Class] 
(
	[CourseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Class]') AND name = N'name_index')
CREATE NONCLUSTERED INDEX [name_index] ON [dbo].[Class] 
(
	[Name] ASC
)
INCLUDE ( [Description],
[InternalCode]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Class]') AND name = N'roomid_index')
CREATE NONCLUSTERED INDEX [roomid_index] ON [dbo].[Class] 
(
	[RoomID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Class]') AND name = N'surveyid_index')
CREATE NONCLUSTERED INDEX [surveyid_index] ON [dbo].[Class] 
(
	[SurveyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Class]') AND name = N'timezoneid_index')
CREATE NONCLUSTERED INDEX [timezoneid_index] ON [dbo].[Class] 
(
	[TimeZoneID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClassResources]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClassResources]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ClassResources](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ClassID] [int] NOT NULL,
	[ResourceID] [int] NOT NULL,
 CONSTRAINT [PK_ClassResources_1] PRIMARY KEY CLUSTERED 
(
	[ClassID] ASC,
	[ResourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Registration]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Registration]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Registration](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[RegistrantID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[RegistrationTypeID] [int] NOT NULL,
	[RegistrationStatusID] [int] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[AttendedIndicator] [bit] NOT NULL,
	[AttendedInitials] [nvarchar](4) NOT NULL,
	[Score] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_Registration_1] PRIMARY KEY CLUSTERED 
(
	[RegistrantID] ASC,
	[ClassID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ClassDate]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClassDate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ClassDate](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ClassID] [int] NOT NULL CONSTRAINT [DF_ClassDate_ClassID]  DEFAULT ((0)),
	[StartDateTime] [datetime] NOT NULL CONSTRAINT [DF_ClassDate_StartDateTime]  DEFAULT (getdate()),
	[Duration] [decimal](10, 2) NOT NULL CONSTRAINT [DF_ClassDate_Duration]  DEFAULT ((0)),
	[CustomerID] [int] NOT NULL CONSTRAINT [DF_ClassDate_CustomerID]  DEFAULT ((0)),
 CONSTRAINT [PK_ClassDate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ClassInstructors]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClassInstructors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ClassInstructors](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ClassID] [int] NOT NULL,
	[InstructorID] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_ClassInstructors_CreatedOn]  DEFAULT (getdate()),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF_ClassInstructors_ModifiedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_ClassInstructors_CreatedBy]  DEFAULT (''),
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_ClassInstructors_ModifiedBy]  DEFAULT (''),
 CONSTRAINT [PK_ClassInstructors] PRIMARY KEY CLUSTERED 
(
	[ClassID] ASC,
	[InstructorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[aspnet_Roles]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Roles](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__aspnet_Ro__RoleI__66603565]  DEFAULT (newid()),
	[RoleName] [nvarchar](256) NOT NULL,
	[LoweredRoleName] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](256) NULL,
 CONSTRAINT [PK__aspnet_Roles__656C112C] PRIMARY KEY NONCLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[aspnet_Users]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__aspnet_Us__UserI__6C190EBB]  DEFAULT (newid()),
	[UserName] [nvarchar](256) NOT NULL,
	[LoweredUserName] [nvarchar](256) NOT NULL,
	[MobileAlias] [nvarchar](16) NULL CONSTRAINT [DF__aspnet_Us__Mobil__6D0D32F4]  DEFAULT (NULL),
	[IsAnonymous] [bit] NOT NULL CONSTRAINT [DF__aspnet_Us__IsAno__6E01572D]  DEFAULT ((0)),
	[LastActivityDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_Users__6B24EA82] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[aspnet_Paths]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Paths](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__aspnet_Pa__PathI__693CA210]  DEFAULT (newid()),
	[Path] [nvarchar](256) NOT NULL,
	[LoweredPath] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK__aspnet_Paths__68487DD7] PRIMARY KEY NONCLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[aspnet_Membership]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Membership](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128) NOT NULL,
	[MobilePIN] [nvarchar](16) NULL,
	[Email] [nvarchar](256) NULL,
	[LoweredEmail] [nvarchar](256) NULL,
	[PasswordQuestion] [nvarchar](256) NULL,
	[PasswordAnswer] [nvarchar](128) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowStart] [datetime] NOT NULL,
	[Comment] [ntext] NULL,
 CONSTRAINT [PK__aspnet_Membershi__46E78A0C] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[User]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[SalesChannelID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[EmployeeNumber] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[MiddleName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[HireDate] [datetime] NULL,
	[NewHireIndicator] [bit] NOT NULL,
	[Email] [nvarchar](75) NOT NULL,
	[StatusID] [int] NOT NULL,
	[JobRoleID] [int] NOT NULL,
	[Notes] [nvarchar](max) NOT NULL,
	[LoginCounter] [int] NOT NULL,
	[IsAccountExec] [bit] NOT NULL,
	[IsCustomerCare] [bit] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[TelephoneNumber] [varchar](20) NOT NULL,
	[SupervisorID] [int] NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[IsExternal] [bit] NOT NULL,
	[SecondaryLocationID] [int] NOT NULL,
	[Search]  AS (((((([Username]+' ')+[FirstName])+' ')+[MiddleName])+' ')+[LastName]),
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'customer_index')
CREATE NONCLUSTERED INDEX [customer_index] ON [dbo].[User] 
(
	[CustomerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'email_index')
CREATE NONCLUSTERED INDEX [email_index] ON [dbo].[User] 
(
	[Email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'external_index')
CREATE NONCLUSTERED INDEX [external_index] ON [dbo].[User] 
(
	[IsExternal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'firstname_index')
CREATE NONCLUSTERED INDEX [firstname_index] ON [dbo].[User] 
(
	[FirstName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'ix_parent')
CREATE NONCLUSTERED INDEX [ix_parent] ON [dbo].[User] 
(
	[SupervisorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'IX_Search')
CREATE NONCLUSTERED INDEX [IX_Search] ON [dbo].[User] 
(
	[Search] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'IX_UserFirstName')
CREATE NONCLUSTERED INDEX [IX_UserFirstName] ON [dbo].[User] 
(
	[FirstName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'IX_UserLastName')
CREATE NONCLUSTERED INDEX [IX_UserLastName] ON [dbo].[User] 
(
	[LastName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'IX_UserMiddleName')
CREATE NONCLUSTERED INDEX [IX_UserMiddleName] ON [dbo].[User] 
(
	[MiddleName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'jobrole_index')
CREATE NONCLUSTERED INDEX [jobrole_index] ON [dbo].[User] 
(
	[JobRoleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'lastname_index')
CREATE NONCLUSTERED INDEX [lastname_index] ON [dbo].[User] 
(
	[LastName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'location_index')
CREATE NONCLUSTERED INDEX [location_index] ON [dbo].[User] 
(
	[LocationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'saleschannel_index')
CREATE NONCLUSTERED INDEX [saleschannel_index] ON [dbo].[User] 
(
	[SalesChannelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND name = N'status_index')
CREATE NONCLUSTERED INDEX [status_index] ON [dbo].[User] 
(
	[StatusID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Import]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Import]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Import](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CustomerID] [int] NOT NULL,
	[ImportTypeID] [int] NOT NULL,
	[FileName] [nvarchar](100) NOT NULL,
	[FileType] [nvarchar](50) NOT NULL,
	[FileContents] [image] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Import] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Location]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Location](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL CONSTRAINT [DF_Location_Name]  DEFAULT (''),
	[CustomerID] [int] NOT NULL CONSTRAINT [DF_Location_CustomerID]  DEFAULT ((0)),
	[ParentLocationID] [int] NOT NULL CONSTRAINT [DF_Location_ParentLocationID]  DEFAULT ((0)),
	[InternalCode] [nvarchar](50) NOT NULL CONSTRAINT [DF_Location_InternalCode]  DEFAULT (''),
	[Address1] [nvarchar](100) NOT NULL CONSTRAINT [DF_Location_Address1]  DEFAULT (''),
	[Address2] [nvarchar](100) NOT NULL CONSTRAINT [DF_Location_Address2]  DEFAULT (''),
	[City] [nvarchar](100) NOT NULL CONSTRAINT [DF_Location_City]  DEFAULT (''),
	[State] [nvarchar](25) NOT NULL CONSTRAINT [DF_Location_State]  DEFAULT (''),
	[PostalCode] [varchar](50) NOT NULL CONSTRAINT [DF_Location_PostalCode]  DEFAULT (''),
	[TelephoneNbr] [varchar](50) NOT NULL CONSTRAINT [DF_Location_TelephoneNbr]  DEFAULT (''),
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__Location__Modifi__42E1EEFE]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__Location__Create__43D61337]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__Location__Modifi__44CA3770]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__Location__Create__45BE5BA9]  DEFAULT (getdate()),
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND name = N'locationname_index')
CREATE NONCLUSTERED INDEX [locationname_index] ON [dbo].[Location] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND name = N'parent_location_index')
CREATE NONCLUSTERED INDEX [parent_location_index] ON [dbo].[Location] 
(
	[ParentLocationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobRole]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[JobRole]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[JobRole](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[ParentJobRoleID] [int] NULL,
	[InternalCode] [nvarchar](50) NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__JobRole__Modifie__3F115E1A]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__JobRole__Created__40058253]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__JobRole__Modifie__40F9A68C]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__JobRole__Created__41EDCAC5]  DEFAULT (getdate()),
 CONSTRAINT [PK_JobRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[JobRole]') AND name = N'jobrolename_index')
CREATE NONCLUSTERED INDEX [jobrolename_index] ON [dbo].[JobRole] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[JobRole]') AND name = N'parent_jobrole_index')
CREATE NONCLUSTERED INDEX [parent_jobrole_index] ON [dbo].[JobRole] 
(
	[ParentJobRoleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TrainingProgramToCourseLink]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrainingProgramToCourseLink]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrainingProgramToCourseLink](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TrainingProgramID] [int] NOT NULL,
	[CourseID] [int] NOT NULL,
	[CourseTypeID] [int] NOT NULL,
	[SyllabusTypeID] [int] NOT NULL,
	[RequiredSyllabusOrder] [int] NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_TrainingProgramToCourseLink_ModifiedBy]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_TrainingProgramToCourseLink_CreatedBy]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF_TrainingProgramToCourseLink_ModifiedOn]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_TrainingProgramToCourseLink_CreatedOn]  DEFAULT (getdate()),
	[DueDate] [datetime] NULL,
	[CustomerID] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_TrainingProgramToCourseLink] PRIMARY KEY CLUSTERED 
(
	[TrainingProgramID] ASC,
	[CourseID] ASC,
	[CourseTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Course]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Course]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Course](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[InternalCode] [nvarchar](50) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[CourseDurationUnitID] [int] NOT NULL,
	[PublicIndicator] [bit] NOT NULL,
	[Credit] [decimal](10, 2) NOT NULL,
	[PerStudentFee] [decimal](10, 2) NOT NULL,
	[PresentationTypeID] [int] NOT NULL,
	[CourseCompletionTypeID] [int] NOT NULL,
	[SurveyID] [int] NOT NULL,
	[InTrainingCycle] [bit] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CourseCompletionTypeParameterID] [int] NOT NULL,
	[CourseDurationUnitCount] [int] NULL,
	[CourseObjective] [nvarchar](256) NULL,
	[OnlineCourseID] [int] NOT NULL,
	[HasSurvey]  AS (CONVERT([bit],case [SurveyID] when (0) then (0) else (1) end,(0))),
 CONSTRAINT [PK_Course] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Course]') AND name = N'course_completion_type_index')
CREATE NONCLUSTERED INDEX [course_completion_type_index] ON [dbo].[Course] 
(
	[CourseCompletionTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Course]') AND name = N'course_name_index')
CREATE NONCLUSTERED INDEX [course_name_index] ON [dbo].[Course] 
(
	[Name] ASC
)
INCLUDE ( [Description]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Course]') AND name = N'course_online_course_index')
CREATE NONCLUSTERED INDEX [course_online_course_index] ON [dbo].[Course] 
(
	[OnlineCourseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Course]') AND name = N'public_index')
CREATE NONCLUSTERED INDEX [public_index] ON [dbo].[Course] 
(
	[PublicIndicator] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Resource]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Resource]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Resource](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Cost] [decimal](8, 2) NULL,
	[VenueID] [int] NOT NULL,
	[RoomID] [int] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CustomerID] [int] NOT NULL,
 CONSTRAINT [PK_Resource] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[aspnet_UsersInRoles]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_UsersInRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK__aspnet_UsersInRo__4E88ABD4] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationAllUsers]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_PersonalizationAllUsers](
	[PathId] [uniqueidentifier] NOT NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_Personali__5070F446] PRIMARY KEY CLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationPerUser]    Script Date: 08/05/2010 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_PersonalizationPerUser](
	[Id] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NULL,
	[UserId] [uniqueidentifier] NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_Personali__49C3F6B7] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SentProblem]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SentProblem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SentProblem](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[CourseID] [int] NOT NULL,
	[PossibleProblemID] [int] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_SentProblem] PRIMARY KEY CLUSTERED 
(
	[CourseID] ASC,
	[PossibleProblemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Venue]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Venue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Venue](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CustomerID] [int] NOT NULL CONSTRAINT [DF_Venue_CustomerID]  DEFAULT ((0)),
	[InternalCode] [nvarchar](50) NOT NULL,
	[City] [nvarchar](50) NOT NULL CONSTRAINT [DF_Venue_City]  DEFAULT (''),
	[StateID] [int] NOT NULL CONSTRAINT [DF_Venue_State]  DEFAULT ((0)),
	[TimeZoneID] [int] NOT NULL CONSTRAINT [DF_Venue_TimeZoneID]  DEFAULT ((0)),
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__Venue__ModifiedB__3587F3E0]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__Venue__CreatedBy__367C1819]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__Venue__ModifiedO__37703C52]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__Venue__CreatedOn__3864608B]  DEFAULT (getdate()),
 CONSTRAINT [PK_Venue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[VirtualRoom]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VirtualRoom]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VirtualRoom](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ClassDateID] [int] NOT NULL,
	[GTMMeetingID] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_VirtualRoom] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[VenueResourceManagers]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VenueResourceManagers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VenueResourceManagers](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[VenueID] [int] NOT NULL,
	[ResourceManagerID] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF_VenueResourceManagers_CreatedOn]  DEFAULT (getdate()),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF_VenueResourceManagers_ModifiedOn]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_VenueResourceManagers_CreatedBy]  DEFAULT (''),
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF_VenueResourceManagers_ModifiedBy]  DEFAULT (''),
 CONSTRAINT [PK_VenueResourceManagers] PRIMARY KEY CLUSTERED 
(
	[VenueID] ASC,
	[ResourceManagerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Room]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Room]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Room](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[InternalCode] [nvarchar](50) NOT NULL,
	[Capacity] [int] NOT NULL,
	[NetworkAccessIndicator] [bit] NOT NULL,
	[RoomCost] [decimal](8, 2) NULL,
	[VenueID] [int] NULL,
	[LockedIndicator] [bit] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CustomerID] [int] NOT NULL,
 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Room]') AND name = N'customerid_index')
CREATE NONCLUSTERED INDEX [customerid_index] ON [dbo].[Room] 
(
	[CustomerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Room]') AND name = N'name_index')
CREATE NONCLUSTERED INDEX [name_index] ON [dbo].[Room] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Room]') AND name = N'venueid_index')
CREATE NONCLUSTERED INDEX [venueid_index] ON [dbo].[Room] 
(
	[VenueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAudience]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAudience]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserAudience](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[UserID] [int] NOT NULL,
	[AudienceID] [int] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__UserAudie__Modif__534D60F1]  DEFAULT (''),
	[CreatedBy] [nvarchar](50) NOT NULL CONSTRAINT [DF__UserAudie__Creat__5441852A]  DEFAULT (''),
	[ModifiedOn] [datetime] NOT NULL CONSTRAINT [DF__UserAudie__Modif__5535A963]  DEFAULT (getdate()),
	[CreatedOn] [datetime] NOT NULL CONSTRAINT [DF__UserAudie__Creat__5629CD9C]  DEFAULT (getdate()),
 CONSTRAINT [PK_UserAudience_1] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[AudienceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  View [dbo].[CoursesRegisteredClassroom]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CoursesRegisteredClassroom]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[CoursesRegisteredClassroom]
AS
SELECT     
	cReg.ID AS RegistrationID, 
	cReg.RegistrantID AS UserID, 
	cClass.CourseID, 
	cReg.ClassID, 
	cReg.RegistrationStatusID,
	cClass.Name AS ClassName, 
	cCourse.Name AS CourseName, 
	cClass.[Description] AS ClassDescription, 
	cVenue.Name as ClassVenueName,
	CASE 
		WHEN cClass.IsVirtual = 1 THEN ''Virtual'' 
		ELSE cRoom.Name END AS ClassRoomName, 
	cClass.IsVirtual,
	ISNULL(REPLACE(REPLACE((
		SELECT     
			cUser.FirstName + ''+'' + cUser.LastName AS [data()]
		FROM
			dbo.ClassInstructors AS cClassI 
		INNER JOIN						
			dbo.[User] AS cUser ON cUser.ID = cClassI.InstructorID
		WHERE     
			(cClassI.ClassID = cReg.ClassID) 
			FOR XML PATH('''')), '' '', '', ''), ''+'', '' ''), '''') 
	AS ClassInstructors, 
	CASE 
		WHEN cCourseCT.CodeName = ''attendance'' AND cReg.AttendedIndicator = 1 THEN cast(100 AS nvarchar(50)) 
		WHEN r.ID IS NULL THEN cast(cReg.Score AS nvarchar(50)) 
		ELSE cast(isnull(r.score,''0'') AS nvarchar(50)) 
	END AS Score, 
	cReg.AttendedIndicator AS Attended, 
	cStat.[Description] AS [Status], 
	cClass.AdditionalInstructions AS Notes, 
	ISNULL(REPLACE((
		SELECT     
			cClassD.StartDateTime AS [data()]
		FROM         
			dbo.ClassDate AS cClassD
		WHERE     
			cClassD.ClassID = cReg.ClassID
		ORDER BY 
			StartDateTime ASC FOR XML PATH('''')), '' '', '', ''), '''') 
	AS ClassDates, 
	CASE 
		WHEN cCourseCT.CodeName = ''attendance'' AND 
			(cReg.ModifiedOn > vClassDMM.MaxStartDateTime OR cReg.AttendedIndicator = 1) THEN 1 
		WHEN cCourseCT.CodeName = ''attendance'' THEN 0 
		WHEN cCourseCT.CodeName = ''onlinecourse'' AND (r.Completion = 1 or r.Score is not null) THEN 1 
		WHEN cCourseCT.CodeName = ''onlinecourse'' THEN 0 
		WHEN cReg.Score = '''' THEN 0 ELSE 1 
	END AS ScoreReported, 
	cCourseCT.CodeName, 
	CASE 
		WHEN cCourseCT.CodeName = ''attendance'' AND cReg.AttendedIndicator = 1 THEN 1 
		WHEN cCourseCT.CodeName = ''attendance'' THEN 0 
		WHEN cCourseCT.CodeName = ''numeric'' AND cReg.Score >= CAST(ISNULL(cPass.PassingScore, 70) AS VARCHAR(15)) THEN 1 
		WHEN cCourseCT.CodeName = ''numeric'' THEN 0 
		WHEN cCourseCT.CodeName = ''letter'' AND cReg.Score = ''F'' THEN 0 
		WHEN cCourseCT.CodeName = ''letter'' THEN 1 
		WHEN cCourseCT.CodeName = ''onlinecourse'' AND r.Success = 1 THEN 1 
		WHEN cCourseCT.CodeName = ''onlinecourse'' THEN 0 
	END AS Passed, 
	vClassDMM.MaxStartDateTime AS MaxClassDate, 
	cReg.ModifiedOn, 
	cClass.SurveyID, 
	cCourse.OnlineCourseID
FROM         
	dbo.Registration AS cReg 
INNER JOIN
	dbo.Class AS cClass 
ON 
	cClass.ID = cReg.ClassID 
INNER JOIN
    dbo.Course AS cCourse 
ON 
	cCourse.ID = cClass.CourseID 
INNER JOIN
    dbo.CourseCompletionType AS cCourseCT 
ON 
	cCourseCT.ID = cCourse.CourseCompletionTypeID 
INNER JOIN
    dbo.[Status] AS cStat 
ON 
	cStat.ID = cReg.RegistrationStatusID 
INNER JOIN
    ClassDateMinMax AS vClassDMM 
ON 
	vClassDMM.ClassID = cClass.ID 
LEFT JOIN
    dbo.Room AS cRoom 
ON 
	cRoom.ID = cClass.RoomID 
LEFT JOIN
	dbo.Venue AS cVenue 
ON 
	cVenue.ID = cRoom.VenueID 
LEFT JOIN
	dbo.PassingScore AS cPass 
ON 
	cPass.CustomerID = cClass.CustomerID 
LEFT JOIN
	dbo.[User] AS cUser 
ON 
	cUser.ID = cReg.RegistrantID
LEFT JOIN
	dbo.OnlineCourseRollup as r
  	--dbo.Scorm_BE_ExternalReg AS otReg 
ON 
	r.CourseId = cCourse.OnlineCourseID 
	and
	r.UserID = cUser.ID
'
GO
/****** Object:  View [dbo].[ClassroomTranscript]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ClassroomTranscript]'))
EXEC dbo.sp_executesql @statement = N'


/* ExecuteTransaction() */ CREATE view [dbo].[ClassroomTranscript] as
-- classroom courses transcript
select
	cCourse.CustomerID,
	cReg.ID as RegistrationID, 
	cReg.RegistrantID as UserID, 
	stat.[Description] as RegistrationStatus,
	cClass.CourseID, 
	cReg.ClassID, 
	cClass.[Name] as ClassName, 
    cCourse.[Name] as CourseName, 
	cClass.Description as ClassDescription, 
	cVenue.[Name] as ClassVenueName,
	cClass.IsVirtual,
	cReg.AttendedIndicator as Attended, 
    cStat.Description as Status, 
	cClass.AdditionalInstructions as Notes,	
	cCourseCT.CodeName as CompletionType,
	vClassDMM.MaxStartDateTime as MaxClassDate, 
	vClassDMM.MinStartDateTime as MinClassDate, 
    cReg.ModifiedOn, 
	cClass.SurveyID, 
	cCourse.OnlineCourseID,
	-- Virtual classes get a nice ''virtual'' classroom name
	case 
		when cClass.IsVirtual = 1
			then ''Virtual'' 
		else
			cRoom.[Name] 
	end as ClassRoomName, 
	-- Calculate the score
	--    -if the score is based on attendance, they get 100 if they attended

--    -if the score is entered by the instructor, use that. Since this can be a letter or a number, we use nvarchar
	--    -if the score is based on taking an online course, use that score
	case 
		when cCourseCT.CodeName = ''attendance'' and cReg.AttendedIndicator = 1
			then cast(100 as nvarchar(50)) 
		when ocr.ID is null
            then cast(cReg.Score as nvarchar(50))
		else 
			cast(floor(round(ocr.Score,0)) as nvarchar(50)) 
	end as Score,

	-- To determine if a user has attended a course with attendance based passing,

-- we have a bit of a conundrum - there''s no way to know if they DIDN''T attend, since the default value for

-- attended is 0. Therefore, (and per BE''s instructions) we assume that if the registration was modified after the 

-- last day of the class, then whatever indicator is in the db was the "reported" value. So, if the date modified is
	-- after the last day of the class, or the AttendedIndicator is true, the instructor reported a score.
	-- For online courses, the ''Complete'' flag will be set to ''2'' once a score has been sent to the server.
	case 


when cCourseCT.CodeName = ''attendance'' and (cReg.ModifiedOn > vClassDMM.MaxStartDateTime or cReg.AttendedIndicator = 1) 
			then 1 
		when cCourseCT.CodeName = ''attendance'' 
			then 0 
		when cCourseCT.CodeName = ''onlinecourse'' and (ocr.Completion = 1 or ocr.Score > 0)
			then 1 
		when cCourseCT.CodeName = ''onlinecourse''
			then 0 
		when cReg.Score = '''' 
			then 0
		else 1
	end as ScoreReported, 
	-- Whether or not the user has passed changes based on course type and completion type.
	-- This is where minimum passing scores are set for classroom courses.

-- Online courses have passing scores on a per-course basis, and the "passed" flag will be set as appropriate.
    case
		when cCourseCT.CodeName = ''attendance'' and cReg.AttendedIndicator = 1 
			then 1
		when cCourseCT.CodeName = ''attendance''
			then 0


when cCourseCT.CodeName = ''numeric'' and ( len(cReg.Score) = 3 or cReg.Score >= cast(isnull(cPass.PassingScore, 70) as varchar(15)) )
			then 1
		when cCourseCT.CodeName = ''numeric'' 
			then 0
		when cCourseCT.CodeName = ''letter'' and cReg.Score = ''F'' 
			then 0 
		when cCourseCT.CodeName = ''letter'' 
			then 1 
		when cCourseCT.CodeName = ''onlinecourse'' AND ocr.Success = 1
			then 1 
		when cCourseCT.CodeName = ''onlinecourse'' 
			then 0 
	end as Passed,
	-- when our current date is past the start date, we''ve started
	-- when past the end date, we''ve ended
	case when getdate() > vClassDMM.MinStartDateTime then 1 else 0 end as HasStarted,
	case when getdate() > vClassDMM.MaxStartDateTime then 1 else 0 end as HasEnded,
    ''currentattempt'' as CourseSourceType,
    cCourse.PublicIndicator
from         
	dbo.Registration as cReg 
inner join
	dbo.[Status] as stat on cReg.RegistrationStatusID = stat.ID
inner join
     dbo.Class as cClass ON cClass.ID = cReg.ClassID 
inner join
	dbo.Course as cCourse ON cCourse.ID = cClass.CourseID 
inner join
	dbo.CourseCompletionType as cCourseCT ON cCourseCT.ID = cCourse.CourseCompletionTypeID 
inner join
    dbo.Status as cStat ON cStat.ID = cReg.RegistrationStatusID 
inner join
    dbo.ClassDateMinMax as vClassDMM ON vClassDMM.ClassID = cClass.ID 
left join
    dbo.Room as cRoom ON cRoom.ID = cClass.RoomID 
left join
	dbo.Venue as cVenue ON cVenue.ID = cRoom.VenueID 
left join
    dbo.PassingScore as cPass ON cPass.CustomerID = cClass.CustomerID 
left join
    dbo.[User] as cUser ON cUser.ID = cReg.RegistrantID 
-- this join simply grabs the score from the user if they had an online course to take for this class
left join
	dbo.OnlineCourseRollup as ocr ON 
		ocr.CourseID = cCourse.OnlineCourseID AND 
        ocr.UserID = cUser.ID
;
'
GO
/****** Object:  View [dbo].[CalendarList]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CalendarList]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[CalendarList]
AS
SELECT 
	ROW_NUMBER() OVER (ORDER BY c.ID) AS ID, 
	c.ID AS ClassID, 
	c.CourseID,
	CAST(CASE WHEN dbo.Registration.RegistrantID IS NULL THEN 0 ELSE 1 END AS BIT) AS Registered, 
	c.CustomerID, 
	c.Name AS ClassName, 
	r.Name AS RoomName,
	MIN(cd.StartDateTime) AS MinStartDateTime,
	InstructorIDs = isnull(replace(
		(
		select
			ci.InstructorID as [data()]
		from
			ClassInstructors ci
		where
			ci.ClassID = c.ID
		group by
			ci.InstructorID
		for xml path ('''')
		), '' '', '',''
	),'''')
FROM 
	dbo.Class c
INNER JOIN
	dbo.Room r
ON
	c.RoomID = r.ID
INNER JOIN
	dbo.ClassDate cd
ON
	c.ID = cd.ClassID
LEFT OUTER JOIN
	dbo.Registration
ON
	c.ID = dbo.Registration.ClassID
GROUP BY
	c.ID, 
	c.CustomerID, 
	CAST(CASE WHEN dbo.Registration.RegistrantID IS NULL THEN 0 ELSE 1 END AS BIT), 
	c.Name,
	r.Name,
	c.CourseID
'
GO
/****** Object:  View [dbo].[ClassRegisteredUserCount]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ClassRegisteredUserCount]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[ClassRegisteredUserCount]
 	AS
 	SELECT COUNT(cRegistration.RegistrantID) AS UserCount, cClass.ID AS ClassID
 	FROM dbo.Class AS cClass LEFT OUTER JOIN
 	dbo.Registration AS cRegistration ON cRegistration.ClassID = cClass.ID
 	WHERE cRegistration.RegistrationStatusID = 4
 	GROUP BY cClass.ID
'
GO
/****** Object:  View [dbo].[ClassUserCount]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ClassUserCount]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[ClassUserCount]
AS
SELECT     COUNT(cRegistration.RegistrantID) AS UserCount, cClass.ID AS ClassID
FROM         dbo.Class AS cClass LEFT OUTER JOIN
                      dbo.Registration AS cRegistration ON cRegistration.ClassID = cClass.ID
GROUP BY cClass.ID
'
GO
/****** Object:  View [dbo].[UsersClassDates]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[UsersClassDates]'))
EXEC dbo.sp_executesql @statement = N'


/****** Object: View [dbo].[UsersClassDates] ******/
CREATE VIEW [dbo].[UsersClassDates]
AS
SELECT
cd.ID , cd.ClassID, cd.StartDateTime, cd.Duration, r.RegistrantID, c.CourseID, c.CustomerID,
(select count(*) from Registration where ClassID = c.ID AND RegistrationStatusID = 4) as RegisteredUserCount,
(select count(*) from Registration where ClassID = c.ID AND (RegistrationStatusID = 1 OR RegistrationStatusID = 2)) as PendingUserCount
FROM
dbo.ClassDate AS cd
LEFT JOIN
dbo.Class AS c
ON
cd.ClassID = c.ID

LEFT JOIN
dbo.Registration AS r
ON
r.ClassID = c.ID
'
GO
/****** Object:  View [dbo].[UsersPublicClassDates]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[UsersPublicClassDates]'))
EXEC dbo.sp_executesql @statement = N'


/****** Object: View [dbo].[UsersPublicClassDates] ******/
CREATE VIEW [dbo].[UsersPublicClassDates]
AS
SELECT
cd.ID , cd.ClassID, cd.StartDateTime, cd.Duration, c.CourseID, c.CustomerID,
(select count(*) from Registration where ClassID = c.ID AND RegistrationStatusID = 4) as RegisteredUserCount,
(select count(*) from Registration where ClassID = c.ID AND (RegistrationStatusID = 1 OR RegistrationStatusID = 2)) as PendingUserCount
FROM
dbo.ClassDate AS cd

LEFT JOIN dbo.Class AS c
ON cd.ClassID = c.ID

LEFT JOIN dbo.Course AS co
ON co.ID = c.CourseID

WHERE
co.PublicIndicator = 1
'
GO
/****** Object:  View [dbo].[InstructorsClassDates]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[InstructorsClassDates]'))
EXEC dbo.sp_executesql @statement = N'


/****** Object: View [dbo].[InstructorsClassDates] ******/
CREATE VIEW [dbo].[InstructorsClassDates]
AS
SELECT
cd.ID , cd.ClassID, cd.StartDateTime, cd.Duration, ci.InstructorID, c.CourseID, c.CustomerID,
(select count(*) from Registration where ClassID = c.ID AND RegistrationStatusID = 4) as RegisteredUserCount,
(select count(*) from Registration where ClassID = c.ID AND (RegistrationStatusID = 1 OR RegistrationStatusID = 2)) as PendingUserCount
FROM
dbo.ClassDate AS cd
LEFT JOIN
dbo.Class AS c
ON
cd.ClassID = c.ID

LEFT JOIN
dbo.ClassInstructors AS ci
ON
ci.ClassID = c.ID
'
GO
/****** Object:  View [dbo].[ILTClassDates]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ILTClassDates]'))
EXEC dbo.sp_executesql @statement = N'


/****** Object: View [dbo].[ILTClassDates] ******/
CREATE VIEW [dbo].[ILTClassDates]
AS
SELECT
cd.ID , cd.ClassID, cd.StartDateTime, cd.Duration, c.CourseID, c.CustomerID,
(select count(*) from Registration where ClassID = c.ID AND RegistrationStatusID = 4) as RegisteredUserCount,
(select count(*) from Registration where ClassID = c.ID AND (RegistrationStatusID = 1 OR RegistrationStatusID = 2)) as PendingUserCount
FROM
dbo.ClassDate AS cd

LEFT JOIN dbo.Class AS c
ON cd.ClassID = c.ID
'
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UnRegisterSchemaVersion]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UnRegisterSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    DELETE FROM dbo.aspnet_SchemaVersions
        WHERE   Feature = LOWER(@Feature) AND @CompatibleSchemaVersion = CompatibleSchemaVersion
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_RegisterSchemaVersion]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_RegisterSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128),
    @IsCurrentVersion          bit,
    @RemoveIncompatibleSchema  bit
AS
BEGIN
    IF( @RemoveIncompatibleSchema = 1 )
    BEGIN
        DELETE FROM dbo.aspnet_SchemaVersions WHERE Feature = LOWER( @Feature )
    END
    ELSE
    BEGIN
        IF( @IsCurrentVersion = 1 )
        BEGIN
            UPDATE dbo.aspnet_SchemaVersions
            SET IsCurrentVersion = 0
            WHERE Feature = LOWER( @Feature )
        END
    END

    INSERT  dbo.aspnet_SchemaVersions( Feature, CompatibleSchemaVersion, IsCurrentVersion )
    VALUES( LOWER( @Feature ), @CompatibleSchemaVersion, @IsCurrentVersion )
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_CheckSchemaVersion]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_CheckSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    IF (EXISTS( SELECT  *
                FROM    dbo.aspnet_SchemaVersions
                WHERE   Feature = LOWER( @Feature ) AND
                        CompatibleSchemaVersion = @CompatibleSchemaVersion ))
        RETURN 0

    RETURN 1
END
' 
END
GO
/****** Object:  View [dbo].[AllCoursesWithType]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AllCoursesWithType]'))
EXEC dbo.sp_executesql @statement = N'


/* ExecuteTransaction() */ CREATE view [dbo].[AllCoursesWithType] as
	select 
		cast(oc.ID as nvarchar(max)) + ''-'' + cast(ct.ID as nvarchar(max)) as ID,
		oc.ID AS CourseID, 
		CustomerID, 
		oc.[Name] as [Name], 
		oc.Description,
		ct.ID AS CourseTypeID,
		oc.PublicIndicator as PublicIndicator
	from
		dbo.OnlineCourse as oc
	join 
		CourseType as ct 
	on
		ct.CodeName = ''online''
	where
		IsSurvey = 0 and IsTest = 0

	union all

	select
	cast(c.ID as nvarchar(max)) + ''-'' + cast(ct.ID as nvarchar(max)) as ID,
		c.ID AS CourseID, 
		CustomerID, 
		c.Name, 
		c.Description,
		ct.ID AS CourseTypeID,
		c.PublicIndicator as PublicIndicator
	from
		dbo.Course as c
	join
		CourseType as ct 
	on


ct.CodeName = ''classroom'';
'
GO
/****** Object:  View [dbo].[AllCourses]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AllCourses]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[AllCourses]
AS
SELECT 
	ROW_NUMBER() OVER (ORDER BY A.CourseID) AS ID, 
	A.*,
	CourseType.CodeName AS CodeName
FROM (
		SELECT 
			oc.ID AS CourseID, 
			CustomerID, 
			oc.[Name],
			/*CourseCode AS Code, */
			oc.Description, 
			/*GlobalObjective AS Objective, */
			Cost AS Fee, 
			Credit, 
			PublicIndicator, 
			ct.ID AS CourseTypeID,
			/*oc.InTrainingCycle,*/
			oc.ModifiedBy, 
			oc.CreatedBy, 
			oc.ModifiedOn, 
			oc.CreatedOn
		FROM dbo.OnlineCourse as oc JOIN CourseType as ct ON ct.CodeName = ''online''
		WHERE IsSurvey = 0
		UNION ALL
		SELECT 
			c.ID AS CourseID, 
			CustomerID, 
			c.Name, 
			--InternalCode AS Code, 
			c.Description, 
			--CourseObjective AS Objective, 
			PerStudentFee AS Fee, 
			Credit, 
			PublicIndicator,
			ct.ID AS CourseTypeID, 
			--c.InTrainingCycle,
			c.ModifiedBy, 
			c.CreatedBy, 
			c.ModifiedOn, 
			c.CreatedOn
		FROM dbo.Course as c JOIN CourseType as ct ON ct.CodeName = ''classroom''
	) AS A INNER JOIN CourseType ON CourseTypeID = CourseType.ID
'
GO
/****** Object:  View [dbo].[AvailableTrainingProgramCourses]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AvailableTrainingProgramCourses]'))
EXEC dbo.sp_executesql @statement = N'

create view [dbo].[AvailableTrainingProgramCourses]
as
(
	SELECT 
		oc.ID, 
		CustomerID, 
		oc.[Name],
		oc.[Description], 
		Cost AS Fee, 
		Credit, 
		PublicIndicator, 
		ct.ID AS CourseTypeID,
		oc.ModifiedBy, 
		oc.CreatedBy, 
		oc.ModifiedOn, 
		oc.CreatedOn,
		cast(oc.ID as varchar(20)) + ''-'' + cast(ct.ID as varchar(20)) as [Key]
	FROM dbo.OnlineCourse as oc JOIN CourseType as ct ON ct.CodeName = ''online''
	-- don''t allow surveys to be directly assigned
	WHERE IsSurvey = 0
	UNION ALL
	SELECT 
		c.ID, 
		CustomerID, 
		c.Name, 
		c.[Description], 
		PerStudentFee AS Fee, 
		Credit, 
		PublicIndicator,
		ct.ID AS CourseTypeID, 
		c.ModifiedBy, 
		c.CreatedBy, 
		c.ModifiedOn, 
		c.CreatedOn,
		cast(c.ID as varchar(20)) + ''-'' + cast(ct.ID as varchar(20)) as [Key]
	FROM dbo.Course as c JOIN CourseType as ct ON ct.CodeName = ''classroom''
	-- don''t allow public classroom courses to be directly assigned
	WHERE PublicIndicator = 0
)


'
GO
/****** Object:  View [dbo].[ClassCredit]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ClassCredit]'))
EXEC dbo.sp_executesql @statement = N'create VIEW [dbo].[ClassCredit]
as

select 
	c.CustomerID,
	c.CourseID, 
	co.name as CourseName, 
	c.name as Classname, 
	co.credit,
	dur.Duration,
	case co.credit
		when 0.00 then dur.Duration
		else co.credit end as CreditDuration
FROM
dbo.Class AS c 
LEFT OUTER JOIN     dbo.Course AS co 
ON co.ID = c.CourseID 
inner join classDate dur
on c.id = dur.classid
--where c.customerid = 97
group by  c.CustomerID,
	c.courseid, 
	co.name, 
	c.name,
	co.credit,
	dur.duration
'
GO
/****** Object:  View [dbo].[PublicClasses]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[PublicClasses]'))
EXEC dbo.sp_executesql @statement = N'


/****** Object: View [dbo].[PublicClasses] ******/
CREATE VIEW [dbo].[PublicClasses]
AS
SELECT
	c.*
FROM
	dbo.Class AS c

LEFT JOIN dbo.Course AS co
	ON co.ID = c.CourseID

WHERE
co.PublicIndicator = 1
'
GO
/****** Object:  View [dbo].[SearchableClasses]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SearchableClasses]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[SearchableClasses]
AS
select *,ROW_NUMBER() OVER(ORDER BY a.[Name],MinStartDateTime) AS Row from(
SELECT     cl.*,
                      cl.Name + '', '' + cl.Description + '', '' + cl.Name + '', '' + COALESCE (s.Name, '''') + '', '' + COALESCE (v.City, '''') AS SearchText, co.Name AS CourseName, 
                      COALESCE (s.Name, '''') AS State, COALESCE (v.City, '''') AS City,
	(select min(StartDateTime) from ClassDate cd where cd.ClassID = cl.ID) as MinStartDateTime,
	v.City + '', '' + s.Name as Location
FROM         dbo.Class AS cl INNER JOIN
                      dbo.Course AS co ON cl.CourseID = co.ID LEFT OUTER JOIN
                      dbo.Room AS r ON cl.RoomID = r.ID LEFT OUTER JOIN
                      dbo.Venue AS v ON r.VenueID = v.ID LEFT OUTER JOIN
                      dbo.States AS s ON s.ID = v.StateID
) a
'
GO
/****** Object:  View [dbo].[SimplePublicCourses]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SimplePublicCourses]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[SimplePublicCourses]
as
	select
		ID, Name, [Description], Credit, ''0'' as InternalCode, CustomerID, 2 as CourseTypeID, 0 as CourseCompletionTypeID, 0 as HasSurvey, 0 as OnlineCourseID, Keywords
	from
		OnlineCourse
	where
		publicindicator = 1
	
	union
	
	select
		ID, Name, [Description], Credit, InternalCode, CustomerID, 1 as CourseTypeID, CourseCompletionTypeId, HasSurvey, OnlineCourseID, '''' as Keywords
	from
		Course
	where
		publicindicator = 1
'
GO
/****** Object:  View [dbo].[TrainingProgramCoursesAndClasses]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TrainingProgramCoursesAndClasses]'))
EXEC dbo.sp_executesql @statement = N'


-- TODO: exclude null course ids? this means (at least for classroom courses) that the course has no scheduled dates yet

CREATE view [dbo].[TrainingProgramCoursesAndClasses] as
-- This query will give us a list of all the courses that fall within a given training program.
-- Use this in conjunction with a query to give all the users that have the training program assigned to them
-- to determine the total list of courses a user must take.

--the outer select is simply to coalesce the online and classroom courses into a single set of columns
select
	ROW_NUMBER() OVER (ORDER BY TrainingProgramID, ClassStartDate) AS ID, 
	CustomerID,
	TrainingProgramID,
	TrainingProgramName,
	SyllabusTypeID,
	ProgramStartDate,
	ProgramEndDate,
	coalesce(ClassroomCourseID, OnlineCourseID, null) as CourseID,
	CourseTypeID,
	coalesce(ClassroomCourseName, OnlineCourseName, null) as CourseName,
	coalesce(ClassStartDate, null) as ClassStartDate,
	coalesce(ClassEndDate, null) as ClassEndDate,
	ClassID,
	EnforceRequiredSortOrder,
	coalesce(SortOrder,0) as SortOrder
from
	(
	-- first, get the courses for the training program
	select
		link.CourseTypeID,
		link.TrainingProgramID,
		link.SyllabusTypeID, 
		program.CustomerID as CustomerID,
		program.[Name] as TrainingProgramName,
		program.StartDate as ProgramStartDate,
		program.EndDate as ProgramEndDate,
		classroomCourse.CourseID as ClassroomCourseID,
		classroomCourse.[Name] as ClassroomCourseName,
		classroomCourse.StartDate as ClassStartDate,
		classroomCourse.StartDate as ClassEndDate,
		classroomCourse.ClassID as ClassID,
		onlineCourse.ID as OnlineCourseID,
		onlineCourse.[Name] as OnlineCourseName,
		program.EnforceRequiredOrder as EnforceRequiredSortOrder,
		link.RequiredSyllabusOrder as SortOrder
	from
		TrainingProgram program
	join
		TrainingProgramToCourseLink link
	on
		program.ID = link.TrainingProgramID
	-- second, find the details for the courses
	left join
		dbo.OnlineCourse onlineCourse
	on
		onlineCourse.ID = link.CourseID and link.CourseTypeID = 2
	left join
		(
			-- join only to classroom courses that have scheduled dates
			-- this way we can filter them to the training program
			select
				Course.[Name], 
				Course.ID as CourseID,
				Class.ID as ClassID,
				ClassDate.StartDateTime as StartDate
			from
				dbo.Course as Course
			left join
				dbo.Class as Class
			on
				Course.ID = Class.CourseID
			left join
				dbo.ClassDate as ClassDate
			on
				Class.ID = ClassDate.ClassID
			group by
				Course.[Name],
				Course.ID,
				Class.ID,
				ClassDate.StartDateTime
		) as classroomCourse
	on
		classroomCourse.CourseID = link.CourseID and link.CourseTypeID = 1
	-- make sure we exclude training programs that haven''t been entered completely yet
	--where
	--	program.StartDate != ''1900/01/01''
	--and
	--	program.EndDate != ''1900/01/01''
	-- and make sure we only include training programs that are enabled
	and
		program.IsLive = 1

) as Schedule

where
	-- filter classroom courses by the training program date
	(
		CourseTypeID = 1 
	)
	-- online courses always come in
	or
	(
		CourseTypeID = 2
	)
'
GO
/****** Object:  View [dbo].[ClassroomClassDetails]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ClassroomClassDetails]'))
EXEC dbo.sp_executesql @statement = N'


CREATE view [dbo].[ClassroomClassDetails] as
select
	ROW_NUMBER() OVER (order by cClass.ID) as ID, 
	cCourse.ID as CourseID, 
	cClass.ID as ClassID, 
	cClass.[Name] as ClassName, 
	cCourse.[Name] as CourseName, 
	cVenue.[Name] as VenueName, 
	cClass.Description as ClassDescription, 
	cRoom.[Name] as ClassRoomName, 
	vClassDMM.MinStartDateTime as MinClassDate, 	
	isnull(replace(replace((
		select     
			cUser.FirstName + ''+'' + cUser.LastName as [data()]
		from         
			dbo.ClassInstructors as cClassI 
		inner join
            dbo.[User] as cUser 
		on 
			cUser.ID = cClassI.InstructorID
        where     
			cClassI.ClassID = cClass.ID 
		for xml path('''')), '' '', '', ''), ''+'', '' ''), '''')
	as ClassInstructors, 
	isnull(replace((
		select     
			cClassD.StartDateTime as [data()]
        from         
			dbo.ClassDate as cClassD
        where     
			(cClassD.ClassID = cClass.ID)
        order by 
			StartDateTime ASC 
		for xml path('''')), '' '', '', ''), '''') 
	as ClassDates
	from
		dbo.Class as cClass 
	inner join
		dbo.Course as cCourse 
	on 
		cCourse.ID = cClass.CourseID 
	left join
		dbo.Room as cRoom 
	on 
		cRoom.ID = cClass.RoomID 
	left join
		dbo.Venue as cVenue 
	on 
		cVenue.ID = cRoom.VenueID 
	inner join
		dbo.ClassDate as cClassD 
	on 
		cClassD.ClassID = cClass.ID 
	inner join
		dbo.ClassDateMinMax as vClassDMM 
	on 
		vClassDMM.ClassID = cClass.ID 
		and 
		vClassDMM.MinStartDateTime = cClassD.StartDateTime
'
GO
/****** Object:  View [dbo].[CourseClasses]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CourseClasses]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[CourseClasses]
AS
SELECT     ROW_NUMBER() OVER (ORDER BY cClass.ID) AS ID, cCourse.ID AS CourseID, cClass.ID AS ClassID, cClass.Name AS ClassName, 
cCourse.Name AS CourseName, cVenue.Name AS VenueName, cClass.Description AS ClassDescription, cRoom.Name AS ClassRoomName, 
ISNULL(REPLACE(REPLACE
    ((SELECT     cUser.FirstName + ''+'' + cUser.LastName AS [data()]
        FROM         dbo.ClassInstructors AS cClassI INNER JOIN
                              dbo.[User] AS cUser ON cUser.ID = cClassI.InstructorID
        WHERE     (cClassI.ClassID = cClass.ID) FOR XML PATH('''')), '' '', '', ''), ''+'', '' ''), '''') AS ClassInstructors, vClassDMM.MinStartDateTime AS MinClassDate, 
ISNULL(REPLACE
    ((SELECT     cClassD.StartDateTime AS [data()]
        FROM         dbo.ClassDate AS cClassD
        WHERE     (cClassD.ClassID = cClass.ID)
        ORDER BY StartDateTime ASC FOR XML PATH('''')), '' '', '', ''), '''') AS ClassDates
FROM         dbo.Class AS cClass INNER JOIN
                      dbo.Course AS cCourse ON cCourse.ID = cClass.CourseID LEFT JOIN
                      dbo.Room AS cRoom ON cRoom.ID = cClass.RoomID LEFT JOIN
                      dbo.Venue AS cVenue ON cVenue.ID = cRoom.VenueID INNER JOIN
                      dbo.ClassDate AS cClassD ON cClassD.ClassID = cClass.ID INNER JOIN
                      dbo.ClassDateMinMax AS vClassDMM ON vClassDMM.ClassID = cClass.ID AND vClassDMM.MinStartDateTime = cClassD.StartDateTime
--WHERE     vClassDMM.MinStartDateTime > GETDATE()
'
GO
/****** Object:  View [dbo].[ClassResourceManagers]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ClassResourceManagers]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[ClassResourceManagers]
AS

select
    c.ID as ClassID, vrm.ResourceManagerID as ResourceManagerID,
	count(cr.id) as ResourceCount
from
    Class c
join
    ClassResources cr
on
    c.ID = cr.ClassID

join
    Room r
on
    c.RoomID = r.ID

join
    VenueResourceManagers vrm
on
    vrm.VenueID = r.VenueID

group by
    c.ID, vrm.ResourceManagerID
'
GO
/****** Object:  View [dbo].[ClassDatesInfo]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ClassDatesInfo]'))
EXEC dbo.sp_executesql @statement = N'


/****** Object:  View [dbo].[ClassDatesInfo]   ******/
CREATE VIEW [dbo].[ClassDatesInfo]
AS
SELECT     
		cd.*, c.CourseID, c.RoomID, c.TimeZoneID, c.LockedIndicator
FROM
		dbo.ClassDate AS cd
			LEFT JOIN 
		dbo.Class AS c
			ON
		cd.ClassID = c.ID
'
GO
/****** Object:  StoredProcedure [dbo].[GetAvailableResourcesForClass]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAvailableResourcesForClass]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAvailableResourcesForClass](@iClassID int,@bFilterAlreadyAssigned bit)
AS
BEGIN
--i''m sure there''s a better way to do this, but i''m really tired...
select * from Resource where ID in(
	select
		distinct(r.ID)
	from
		Resource r
	join
		Venue
	on
		VenueID = r.VenueID
	where
		r.VenueID = (select
						VenueID
					from
						Class c
					join
						Room room
					on
						c.RoomID = room.ID
					where
						c.ID = @iClassID)
	and
		1 = case when @bFilterAlreadyAssigned = 1 then
				case when ((select count(*) from ClassResources cr where cr.ClassID = @iClassID and cr.ResourceID = r.ID) = 0) then 1 else 0 end
			else 1
		end
)

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_AnyDataInTables]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_AnyDataInTables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_AnyDataInTables]
    @TablesToCheck int
AS
BEGIN
    -- Check Membership table if (@TablesToCheck & 1) is set
    IF ((@TablesToCheck & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_MembershipUsers'') AND (type = ''V''))))
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Membership))
        BEGIN
            SELECT N''aspnet_Membership''
            RETURN
        END
    END

    -- Check aspnet_Roles table if (@TablesToCheck & 2) is set
    IF ((@TablesToCheck & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Roles'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 RoleId FROM dbo.aspnet_Roles))
        BEGIN
            SELECT N''aspnet_Roles''
            RETURN
        END
    END

    -- Check aspnet_Profile table if (@TablesToCheck & 4) is set
    IF ((@TablesToCheck & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Profiles'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Profile))
        BEGIN
            SELECT N''aspnet_Profile''
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 8) is set
    IF ((@TablesToCheck & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_WebPartState_User'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_PersonalizationPerUser))
        BEGIN
            SELECT N''aspnet_PersonalizationPerUser''
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 16) is set
    IF ((@TablesToCheck & 16) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''aspnet_WebEvent_LogEvent'') AND (type = ''P''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 * FROM dbo.aspnet_WebEvent_Events))
        BEGIN
            SELECT N''aspnet_WebEvent_Events''
            RETURN
        END
    END

    -- Check aspnet_Users table if (@TablesToCheck & 1,2,4 & 8) are all set
    IF ((@TablesToCheck & 1) <> 0 AND
        (@TablesToCheck & 2) <> 0 AND
        (@TablesToCheck & 4) <> 0 AND
        (@TablesToCheck & 8) <> 0 AND
        (@TablesToCheck & 32) <> 0 AND
        (@TablesToCheck & 128) <> 0 AND
        (@TablesToCheck & 256) <> 0 AND
        (@TablesToCheck & 512) <> 0 AND
        (@TablesToCheck & 1024) <> 0)
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Users))
        BEGIN
            SELECT N''aspnet_Users''
            RETURN
        END
        IF (EXISTS(SELECT TOP 1 ApplicationId FROM dbo.aspnet_Applications))
        BEGIN
            SELECT N''aspnet_Applications''
            RETURN
        END
    END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProperties]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Profile_GetProperties]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)

    IF (@UserId IS NULL)
        RETURN
    SELECT TOP 1 PropertyNames, PropertyValuesString, PropertyValuesBinary
    FROM         dbo.aspnet_Profile
    WHERE        UserId = @UserId

    IF (@@ROWCOUNT > 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    END
END
' 
END
GO
/****** Object:  View [dbo].[vw_aspnet_Profiles]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Profiles]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[vw_aspnet_Profiles]
  AS SELECT [dbo].[aspnet_Profile].[UserId], [dbo].[aspnet_Profile].[LastUpdatedDate],
      [DataSize]=  DATALENGTH([dbo].[aspnet_Profile].[PropertyNames])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesString])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesBinary])
  FROM [dbo].[aspnet_Profile]
'
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT 0
        RETURN
    END

    SELECT  COUNT(*)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
    WHERE   ApplicationId = @ApplicationId
        AND u.UserId = p.UserId
        AND (LastActivityDate <= @InactiveSinceDate)
        AND (
                (@ProfileAuthOptions = 2)
                OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
            )
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteInactiveProfiles]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteInactiveProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT  0
        RETURN
    END

    DELETE
    FROM    dbo.aspnet_Profile
    WHERE   UserId IN
            (   SELECT  UserId
                FROM    dbo.aspnet_Users u
                WHERE   ApplicationId = @ApplicationId
                        AND (LastActivityDate <= @InactiveSinceDate)
                        AND (
                                (@ProfileAuthOptions = 2)
                             OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                             OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                            )
            )

    SELECT  @@ROWCOUNT
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_DeleteUser]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_DeleteUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Users_DeleteUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @TablesToDeleteFrom int,
    @NumTablesDeletedFrom int OUTPUT
AS
BEGIN
    DECLARE @UserId               uniqueidentifier
    SELECT  @UserId               = NULL
    SELECT  @NumTablesDeletedFrom = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    DECLARE @ErrorCode   int
    DECLARE @RowCount    int

    SET @ErrorCode = 0
    SET @RowCount  = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   u.LoweredUserName       = LOWER(@UserName)
        AND u.ApplicationId         = a.ApplicationId
        AND LOWER(@ApplicationName) = a.LoweredApplicationName

    IF (@UserId IS NULL)
    BEGIN
        GOTO Cleanup
    END

    -- Delete from Membership table if (@TablesToDeleteFrom & 1) is set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_MembershipUsers'') AND (type = ''V''))))
    BEGIN
        DELETE FROM dbo.aspnet_Membership WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
               @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_UsersInRoles table if (@TablesToDeleteFrom & 2) is set
    IF ((@TablesToDeleteFrom & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_UsersInRoles'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_UsersInRoles WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Profile table if (@TablesToDeleteFrom & 4) is set
    IF ((@TablesToDeleteFrom & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Profiles'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_Profile WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_PersonalizationPerUser table if (@TablesToDeleteFrom & 8) is set
    IF ((@TablesToDeleteFrom & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_WebPartState_User'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Users table if (@TablesToDeleteFrom & 1,2,4 & 8) are all set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (@TablesToDeleteFrom & 2) <> 0 AND
        (@TablesToDeleteFrom & 4) <> 0 AND
        (@TablesToDeleteFrom & 8) <> 0 AND
        (EXISTS (SELECT UserId FROM dbo.aspnet_Users WHERE @UserId = UserId)))
    BEGIN
        DELETE FROM dbo.aspnet_Users WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:
    SET @NumTablesDeletedFrom = 0

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
	    ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000)
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)


	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames  table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles  table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers  table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num	  int
	DECLARE @Pos	  int
	DECLARE @NextPos  int
	DECLARE @Name	  nvarchar(256)
	DECLARE @CountAll int
	DECLARE @CountU	  int
	DECLARE @CountR	  int


	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId
	SELECT @CountR = @@ROWCOUNT

	IF (@CountR <> @Num)
	BEGIN
		SELECT TOP 1 N'''', Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END


	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1


	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	SELECT @CountU = @@ROWCOUNT
	IF (@CountU <> @Num)
	BEGIN
		SELECT TOP 1 Name, N''''
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT au.LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE u.UserId = au.UserId)

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(1)
	END

	SELECT  @CountAll = COUNT(*)
	FROM	dbo.aspnet_UsersInRoles ur, @tbUsers u, @tbRoles r
	WHERE   ur.UserId = u.UserId AND ur.RoleId = r.RoleId

	IF (@CountAll <> @CountU * @CountR)
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 @tbUsers tu, @tbRoles tr, dbo.aspnet_Users u, dbo.aspnet_Roles r
		WHERE		 u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND
					 tu.UserId NOT IN (SELECT ur.UserId FROM dbo.aspnet_UsersInRoles ur WHERE ur.RoleId = tr.RoleId) AND
					 tr.RoleId NOT IN (SELECT ur.RoleId FROM dbo.aspnet_UsersInRoles ur WHERE ur.UserId = tu.UserId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	DELETE FROM dbo.aspnet_UsersInRoles
	WHERE UserId IN (SELECT UserId FROM @tbUsers)
	  AND RoleId IN (SELECT RoleId FROM @tbRoles)
	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_IsUserInRole]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_IsUserInRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_IsUserInRole]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(2)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    DECLARE @RoleId uniqueidentifier
    SELECT  @RoleId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(2)

    SELECT  @RoleId = RoleId
    FROM    dbo.aspnet_Roles
    WHERE   LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
        RETURN(3)

    IF (EXISTS( SELECT * FROM dbo.aspnet_UsersInRoles WHERE  UserId = @UserId AND RoleId = @RoleId))
        RETURN(1)
    ELSE
        RETURN(0)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetUsersInRoles]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetUsersInRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetUsersInRoles]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId
    ORDER BY u.UserName
    RETURN(0)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetRolesForUser]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetRolesForUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetRolesForUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(1)

    SELECT r.RoleName
    FROM   dbo.aspnet_Roles r, dbo.aspnet_UsersInRoles ur
    WHERE  r.RoleId = ur.RoleId AND r.ApplicationId = @ApplicationId AND ur.UserId = @UserId
    ORDER BY r.RoleName
    RETURN (0)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_FindUsersInRole]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_FindUsersInRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_FindUsersInRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256),
    @UserNameToMatch  nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId AND LoweredUserName LIKE LOWER(@UserNameToMatch)
    ORDER BY u.UserName
    RETURN(0)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_AddUsersToRoles]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_AddUsersToRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_AddUsersToRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000),
	@CurrentTimeUtc   datetime
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)
	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames	table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles	table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers	table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num		int
	DECLARE @Pos		int
	DECLARE @NextPos	int
	DECLARE @Name		nvarchar(256)

	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		SELECT TOP 1 Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END

	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1

	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		DELETE FROM @tbNames
		WHERE LOWER(Name) IN (SELECT LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE au.UserId = u.UserId)

		INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
		  SELECT @AppId, NEWID(), Name, LOWER(Name), 0, @CurrentTimeUtc
		  FROM   @tbNames

		INSERT INTO @tbUsers
		  SELECT  UserId
		  FROM	dbo.aspnet_Users au, @tbNames t
		  WHERE   LOWER(t.Name) = au.LoweredUserName AND au.ApplicationId = @AppId
	END

	IF (EXISTS (SELECT * FROM dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr WHERE tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId))
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr, aspnet_Users u, aspnet_Roles r
		WHERE		u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	INSERT INTO dbo.aspnet_UsersInRoles (UserId, RoleId)
	SELECT UserId, RoleId
	FROM @tbUsers, @tbRoles

	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END
' 
END
GO
/****** Object:  View [dbo].[vw_aspnet_Applications]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Applications]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[vw_aspnet_Applications]
  AS SELECT [dbo].[aspnet_Applications].[ApplicationName], [dbo].[aspnet_Applications].[LoweredApplicationName], [dbo].[aspnet_Applications].[ApplicationId], [dbo].[aspnet_Applications].[Description]
  FROM [dbo].[aspnet_Applications]
'
GO
/****** Object:  View [dbo].[SearchableUsers]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SearchableUsers]'))
EXEC dbo.sp_executesql @statement = N'


CREATE view [dbo].[SearchableUsers] as
select
	isnull(EmployeeNumber,'''') + '' '' +isnull(JobRole,'''') + '' '' + isnull(Location,'''') + '' '' + isnull(Username,'''') + '' '' + isnull(Audiences,'''') + '' '' + isnull(FirstName,'''') + '' '' + isnull(LastName,'''') + '' '' + isnull(Supervisor,'''') + isnull(Status,'''') as SearchText,
    *
from
(
select
	replace((
		select
			a.Name as [data()]
		from
			Audience a
		inner join
			[UserAudience] ua
		on
			a.ID = ua.AudienceID
		where
			ua.UserID = u.ID
		for xml path ('''')
	),'' '','', '') as Audiences,
	l.Name as Location, 
	j.Name as JobRole,
	u.*,
	app.ApplicationName,
	isnull(us.Description,'''') as Status,
	isnull(su.FirstName + '' '' + su.LastName,'''') as Supervisor,
	cast(
		(select 
			case when count(1) > 0 then 1 else 0 end
		from
			aspnet_UsersInRoles aspUIR
		join
			aspnet_Roles aspR
		on
			aspUIR.RoleID = aspR.RoleID
		where
			UserID = au.UserID and aspR.LoweredRoleName = ''classroom - supervisor''
		) 
	as bit) as HasSupervisorRole,
	cast(
		(select 
			case when count(1) > 0 then 1 else 0 end
		from
			aspnet_UsersInRoles aspUIR
		join
			aspnet_Roles aspR
		on
			aspUIR.RoleID = aspR.RoleID
		where
			UserID = au.UserID 
		and 
			aspR.LoweredRoleName in (''customer - administrator'',''customer - limited administrator'',''customer - manager'',''customer - limited manager'')
		) 
	as bit) as CanManageCustomers,
	cast(
		(select 
			case when count(1) > 0 then 1 else 0 end
		from
			aspnet_UsersInRoles aspUIR
		join
			aspnet_Roles aspR
		on
			aspUIR.RoleID = aspR.RoleID
		where
			UserID = au.UserID 
		and 
			aspR.LoweredRoleName in (''courseassignment - training administrator'',''courseassignment - training manager'')
		) 
	as bit) as CanOwnTrainingPrograms

from
	[user] u
left join
	[user] su
on
	u.SupervisorID = su.ID
left join
	Customer c
on
	c.ID = u.CustomerID
left join
	location l
on
	l.ID = u.LocationID
left join
	jobrole j
on
	j.ID = u.JobRoleID
left join
	UserStatus us
on
	us.ID = u.StatusID
left join
	aspnet_Applications app
on
	app.ApplicationName = c.SubDomain
left join
	aspnet_Users au
on
	au.LoweredUserName = lower(u.Username)
and
	au.ApplicationID = app.ApplicationID
) as Users
'
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProfiles]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Profile_GetProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @PageIndex              int,
    @PageSize               int,
    @UserNameToMatch        nvarchar(256) = NULL,
    @InactiveSinceDate      datetime      = NULL
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT  u.UserId
        FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
        WHERE   ApplicationId = @ApplicationId
            AND u.UserId = p.UserId
            AND (@InactiveSinceDate IS NULL OR LastActivityDate <= @InactiveSinceDate)
            AND (     (@ProfileAuthOptions = 2)
                   OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                   OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                 )
            AND (@UserNameToMatch IS NULL OR LoweredUserName LIKE LOWER(@UserNameToMatch))
        ORDER BY UserName

    SELECT  u.UserName, u.IsAnonymous, u.LastActivityDate, p.LastUpdatedDate,
            DATALENGTH(p.PropertyNames) + DATALENGTH(p.PropertyValuesString) + DATALENGTH(p.PropertyValuesBinary)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p, #PageIndexForUsers i
    WHERE   u.UserId = p.UserId AND p.UserId = i.UserId AND i.IndexId >= @PageLowerBound AND i.IndexId <= @PageUpperBound

    SELECT COUNT(*)
    FROM   #PageIndexForUsers

    DROP TABLE #PageIndexForUsers
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_DeleteRole]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_DeleteRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Roles_DeleteRole]
    @ApplicationName            nvarchar(256),
    @RoleName                   nvarchar(256),
    @DeleteOnlyIfRoleIsEmpty    bit
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    DECLARE @RoleId   uniqueidentifier
    SELECT  @RoleId = NULL
    SELECT  @RoleId = RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
    BEGIN
        SELECT @ErrorCode = 1
        GOTO Cleanup
    END
    IF (@DeleteOnlyIfRoleIsEmpty <> 0)
    BEGIN
        IF (EXISTS (SELECT RoleId FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId))
        BEGIN
            SELECT @ErrorCode = 2
            GOTO Cleanup
        END
    END


    DELETE FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DELETE FROM dbo.aspnet_Roles WHERE @RoleId = RoleId  AND ApplicationId = @ApplicationId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_RoleExists]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_RoleExists]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Roles_RoleExists]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(0)
    IF (EXISTS (SELECT RoleName FROM dbo.aspnet_Roles WHERE LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId ))
        RETURN(1)
    ELSE
        RETURN(0)
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_GetAllRoles]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_GetAllRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Roles_GetAllRoles] (
    @ApplicationName           nvarchar(256))
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN
    SELECT RoleName
    FROM   dbo.aspnet_Roles WHERE ApplicationId = @ApplicationId
    ORDER BY RoleName
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByEmail]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
    @ApplicationName       nvarchar(256),
    @EmailToMatch          nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    IF( @EmailToMatch IS NULL )
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.Email IS NULL
            ORDER BY m.LoweredEmail
    ELSE
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.LoweredEmail LIKE LOWER(@EmailToMatch)
            ORDER BY m.LoweredEmail

    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY m.LoweredEmail

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Applications_CreateApplication]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications_CreateApplication]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
    @ApplicationName      nvarchar(256),
    @ApplicationId        uniqueidentifier OUTPUT
AS
BEGIN
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName

    IF(@ApplicationId IS NULL)
    BEGIN
        DECLARE @TranStarted   bit
        SET @TranStarted = 0

        IF( @@TRANCOUNT = 0 )
        BEGIN
	        BEGIN TRANSACTION
	        SET @TranStarted = 1
        END
        ELSE
    	    SET @TranStarted = 0

        SELECT  @ApplicationId = ApplicationId
        FROM dbo.aspnet_Applications WITH (UPDLOCK, HOLDLOCK)
        WHERE LOWER(@ApplicationName) = LoweredApplicationName

        IF(@ApplicationId IS NULL)
        BEGIN
            SELECT  @ApplicationId = NEWID()
            INSERT  dbo.aspnet_Applications (ApplicationId, ApplicationName, LoweredApplicationName)
            VALUES  (@ApplicationId, @ApplicationName, LOWER(@ApplicationName))
        END


        IF( @TranStarted = 1 )
        BEGIN
            IF(@@ERROR = 0)
            BEGIN
	        SET @TranStarted = 0
	        COMMIT TRANSACTION
            END
            ELSE
            BEGIN
                SET @TranStarted = 0
                ROLLBACK TRANSACTION
            END
        END
    END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Personalization_GetApplicationId]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Personalization_GetApplicationId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Personalization_GetApplicationId] (
    @ApplicationName NVARCHAR(256),
    @ApplicationId UNIQUEIDENTIFIER OUT)
AS
BEGIN
    SELECT @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetAllUsers]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetAllUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
    @ApplicationName       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0


    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
    SELECT u.UserId
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u
    WHERE  u.ApplicationId = @ApplicationId AND u.UserId = m.UserId
    ORDER BY u.UserName

    SELECT @TotalRecords = @@ROWCOUNT

    SELECT u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName
    RETURN @TotalRecords
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByName]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
    @ApplicationName       nvarchar(256),
    @UserNameToMatch       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT u.UserId
        FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND u.LoweredUserName LIKE LOWER(@UserNameToMatch)
        ORDER BY u.UserName


    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END
' 
END
GO
/****** Object:  View [dbo].[CoursesRegisteredOnline]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CoursesRegisteredOnline]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[CoursesRegisteredOnline]
as
	select
		0 as RegistrationID,
		UserID,
		CourseID,
		[Name] as CourseName,
		case when Score > 0 or Completion = 1 then 1 else Completion end as Completed,
		Success as Passed,
		Score,
		rollup.ModifiedOn,
		rollup.ModifiedBy,
		rollup.TrainingProgramID
	from
		dbo.OnlineCourseRollup as [rollup]
	join
		dbo.OnlineCourse as oc
	on
		[rollup].CourseID = oc.ID
		
	/*select     
		reg.scorm_registration_id as RegistrationID, 
		reg.[user_id] as UserID, 
		reg.course_id as CourseID, 
		oc.Name as CourseName, 
		case when result.Score > 0 or result.Completion = 2 then 2 else result.Completion end as Completed,
		result.Success as Passed, 
		result.score as Score, 
		reg.update_dt as ModifiedOn,
		reg.update_by as ModifiedBy
	from         
		dbo.ScormRegistration as reg 
	inner join
		dbo.[User] as u 
			on reg.[user_id] = u.ID
	left join
		dbo.OnlineCourse oc
			on
			oc.ID = reg.course_id
	left join
		dbo.OnlineCourseRollup as result 
			on 
			result.UserID = reg.[user_id]
			and
			result.TrainingProgramID = reg.training_program_id
			and
			result.CourseID = reg.course_id*/
'
GO
/****** Object:  View [dbo].[OnlineTranscript]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[OnlineTranscript]'))
EXEC dbo.sp_executesql @statement = N'


/* ExecuteTransaction() */ CREATE view [dbo].[OnlineTranscript] as


select
	cUser.CustomerID,
	cUser.ID as UserID, 
	oc.[Name] as CourseName,
	oc.PublicIndicator as PublicIndicator,
	cro.CourseID as CourseID,
	floor(round(cro.Score,0)) as Score, 
    cro.Modifiedon as AttemptDate,
	-- completed if the registration is complete or a score is reported
	cro.Completion as Completed,
	-- the passed flag is 0 if unknown, 1 if failed, 2 if passed
	cro.Success as Passed,
	cro.TotalSeconds as AttemptTime,
	cro.AttemptCount as AttemptCount
    
from
	Symphony.dbo.OnlineCourseRollup as cro 
inner join
	dbo.[User] as cUser on 
		cUser.ID = cro.UserID
left outer join                  
	Symphony.dbo.onlineCourse as oc on 


oc.ID = cro.CourseID;
'
GO
/****** Object:  UserDefinedFunction [dbo].[fGetJobRoleHierarchyBranch]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetJobRoleHierarchyBranch]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

CREATE FUNCTION [dbo].[fGetJobRoleHierarchyBranch]
(   
    --the node that''ll be considered the root node of the branch
    @iJobRoleID int
)
RETURNS TABLE
AS
RETURN
(
WITH JobRole_CTE (ID, ParentJobRoleID, Name, CustomerID, Level, LevelIndentText, LevelText, RootJobRoleID)
AS
(
-- Anchor member definition
    SELECT
            l.ID,
            l.ParentJobRoleID,
            l.Name,
            l.CustomerID,
            0 AS Level,
            CAST(l.Name as nvarchar(max)) as LevelIndentText,
            CAST(l.Name as nvarchar(max)) as LevelText,
            l.ID as RootJobRoleID
        FROM
            JobRole l
      WHERE
                  --Note by Jerod
            --Added a case in here...this way, passing "0" will get the full tree, while passing
            --the ID of any node will get that node''s branch.
            --Note that this means if you have 1 root node in the tree, passing that node id or passing 0 will give the same result set
            1 = case when @iJobRoleID = 0 then
                    case --allow 0 or nulls as root nodes
                        when ParentJobRoleID IS NULL then 1
                        when ParentJobRoleID = 0 then 1
                        else 0
                    end
                else
                    case
                        when ID = @iJobRoleID then 1
                        else 0
                    end
                end
    UNION ALL
-- Recursive member definition
    SELECT
            l.ID,
            l.ParentJobRoleID,
            l.Name,
            l.CustomerID,           
            Level + 1,
            CAST(LevelIndentText + '' > '' + l.Name as nvarchar(max)),
            --CAST(replicate(''&nbsp;&nbsp;&nbsp;&nbsp;'',Level+1) + l.Name as nvarchar(max)),
            CAST(l.Name as nvarchar(max)) as LevelText,
            RootJobRoleID
    FROM
            JobRole l
    INNER JOIN JobRole_CTE lc ON
            lc.ID = l.ParentJobRoleID
)
SELECT
      a.Level,
      a.LevelText,
      a.LevelIndentText,
      a.RootJobRoleID,
      b.*
FROM
      JobRole_CTE a
JOIN
      JobRole b
ON
      a.ID = b.ID

)
' 
END
GO
/****** Object:  View [dbo].[TrainingProgramHierarchies]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TrainingProgramHierarchies]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[TrainingProgramHierarchies]
AS
SELECT     h.ID, h.IsPrimary, h.TrainingProgramID, h.HierarchyNodeID, h.HierarchyTypeID, h.ModifiedBy, h.CreatedBy, h.ModifiedOn, h.CreatedOn, l.Name, '''' AS FirstName, 
                      '''' AS LastName
FROM         dbo.HierarchyToTrainingProgramLink AS h INNER JOIN
                      dbo.Location AS l ON h.HierarchyNodeID = l.ID AND h.HierarchyTypeID = 1
UNION ALL
SELECT     h.ID, h.IsPrimary, h.TrainingProgramID, h.HierarchyNodeID, h.HierarchyTypeID, h.ModifiedBy, h.CreatedBy, h.ModifiedOn, h.CreatedOn, j.Name, '''' AS FirstName, 
                      '''' AS LastName
FROM         dbo.HierarchyToTrainingProgramLink AS h INNER JOIN
                      dbo.JobRole AS j ON h.HierarchyNodeID = j.ID AND h.HierarchyTypeID = 2
UNION ALL
SELECT     h.ID, h.IsPrimary, h.TrainingProgramID, h.HierarchyNodeID, h.HierarchyTypeID, h.ModifiedBy, h.CreatedBy, h.ModifiedOn, h.CreatedOn, a.Name, '''' AS FirstName, 
                      '''' AS LastName
FROM         dbo.HierarchyToTrainingProgramLink AS h INNER JOIN
                      dbo.Audience AS a ON h.HierarchyNodeID = a.ID AND h.HierarchyTypeID = 3
UNION ALL
SELECT     h.ID, h.IsPrimary, h.TrainingProgramID, h.HierarchyNodeID, h.HierarchyTypeID, h.ModifiedBy, h.CreatedBy, h.ModifiedOn, h.CreatedOn, '''' AS Name, u.FirstName, 
                      u.LastName
FROM         dbo.HierarchyToTrainingProgramLink AS h INNER JOIN
                      dbo.[User] AS u ON h.HierarchyNodeID = u.ID AND h.HierarchyTypeID = 4
'
GO
/****** Object:  View [dbo].[JobRoleHierarchy]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[JobRoleHierarchy]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[JobRoleHierarchy] AS
WITH JobRole_CTE (ID, ParentJobRoleID, Name, CustomerID, Level, LevelIndentText, LevelText, RootJobRoleID)
AS
(
-- Anchor member definition
    SELECT
            l.ID,
            l.ParentJobRoleID,
            l.Name,
            l.CustomerID,
            0 AS Level,
            CAST(l.Name as nvarchar(max)) as LevelIndentText,
            --CAST(l.Name as nvarchar(max)) as LevelText,
            CAST(l.Name as nvarchar(max)) as LevelText,
            l.ID as RootJobRoleID
    FROM
            JobRole l
      WHERE
            ParentJobRoleID IS NULL OR ParentJobRoleID = 0
    UNION ALL
-- Recursive member definition
    SELECT
            l.ID,
            l.ParentJobRoleID,
            l.Name,
            l.CustomerID,           
            Level + 1,
            CAST(LevelIndentText + '' > '' + l.Name as nvarchar(max)),
            --CAST(replicate(''&nbsp;&nbsp;&nbsp;&nbsp;'',Level+1) + l.Name as nvarchar(max)),
            CAST(l.Name as nvarchar(max)) as LevelText,
            RootJobRoleID
    FROM
            JobRole l
    INNER JOIN JobRole_CTE lc ON
            lc.ID = l.ParentJobRoleID
)
SELECT
      a.Level,
      a.LevelIndentText,
      a.LevelText,
      a.RootJobRoleID,
      b.*
FROM
      JobRole_CTE a
JOIN
      JobRole b
ON
    a.ID = b.ID
'
GO
/****** Object:  View [dbo].[FastSearchableUsers]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[FastSearchableUsers]'))
EXEC dbo.sp_executesql @statement = N'

CREATE view [dbo].[FastSearchableUsers] as
select
	coalesce(substring((
		select
			('', '' + a.Name)
		from
			Audience a
		inner join
			[UserAudience] ua
		on
			a.ID = ua.AudienceID
		where
			ua.UserID = u.ID
		for xml path ('''')
	),3, 1000),'''') as Audiences,
	l.Name as Location, 
	j.Name as JobRole,
	u.*,
	isnull(us.[Description],'''') as Status,
	u.FirstName + '' '' + u.LastName as FullName,
	isnull(su.FirstName + '' '' + su.LastName,'''') as Supervisor
from
	[user] u
left join
	[user] su
on
	u.SupervisorID = su.ID
left join
	Customer c
on
	c.ID = u.CustomerID
left join
	location l
on
	l.ID = u.LocationID
left join
	jobrole j
on
	j.ID = u.JobRoleID
left join
	UserStatus us
on
	us.ID = u.StatusID
'
GO
/****** Object:  View [dbo].[UsersInHierarchy]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[UsersInHierarchy]'))
EXEC dbo.sp_executesql @statement = N'


CREATE View [dbo].[UsersInHierarchy] as
select c.ID as CustomerId,u.Id as UserId,u.LocationId,u.JobRoleId, UserAudience.[AudienceId] as AudienceId,
isnull(replace(replace((select 	replace(a.Name,'' '',''+'') as [data()]	from	Audience a inner join [UserAudience] ua on a.ID = ua.AudienceID where ua.UserID = u.ID for xml path ('''')),'' '','', ''),''+'','' ''),'''') as AudienceName,
isnull(l.Name,'''') as LocationName,	isnull(j.Name,'''') as JobRoleName,
isnull(su.ID,0) as SupervisorID,
isnull(su.FirstName + '' '' + su.LastName,'''') as SupervisorName,

(select CASE WHEN count(SuperVisorId)  > 0 THEN CAST(1 as BIT)	ELSE CAST(0 as BIT) END	from [user] us where us.SuperVisorId = u.ID) as IsSuperVisor


from
	--[User] u
	UserAudience join [user] u on u.Id = UserAudience.UserId
	 left join [user] su on	u.SupervisorID = su.ID
	
	left join Customer c on	c.ID = u.CustomerID
	left join location l on	l.ID = u.LocationID
	left join jobrole j on	j.ID = u.JobRoleID
'
GO
/****** Object:  View [dbo].[MyMessages]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[MyMessages]'))
EXEC dbo.sp_executesql @statement = N'


/* ExecuteTransaction() */ CREATE view [dbo].[MyMessages] as

select

n.ID, n.RecipientID, n.SenderID, n.Subject, n.Body, n.IsRead, n.AttachmentID, n.Priority, n.CreatedOn, n.ModifiedOn,
	n.Subject as SearchText,
	CASE 
		WHEN u.ID IS NOT NULL THEN u.FirstName + '''' + u.LastName ELSE ''(No reply)'' END AS Sender
from
	Notifications as n
left join
	dbo.[User] u
on

u.ID = n.SenderID;
'
GO
/****** Object:  UserDefinedFunction [dbo].[fGetLocationHierarchyBranch]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetLocationHierarchyBranch]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

CREATE FUNCTION [dbo].[fGetLocationHierarchyBranch]
(   
    --the node that''ll be considered the root node of the branch
    @iLocationID int
)
RETURNS TABLE
AS
RETURN
(
WITH Location_CTE (ID, ParentLocationID, Name, CustomerID, Level, LevelIndentText, LevelText, RootLocationID)
AS
(
-- Anchor member definition
    SELECT
            l.ID,
            l.ParentLocationID,
            l.Name,
            l.CustomerID,
            0 AS Level,
            CAST(l.Name as nvarchar(max)) as LevelIndentText,
            CAST(l.Name as nvarchar(max)) as LevelText,
            l.ID as RootLocationID
        FROM
            Location l
      WHERE
                  --Note by Jerod
            --Added a case in here...this way, passing "0" will get the full tree, while passing
            --the ID of any node will get that node''s branch.
            --Note that this means if you have 1 root node in the tree, passing that node id or passing 0 will give the same result set
            1 = case when @iLocationID = 0 then
                    case --allow 0 or nulls as root nodes
                        when ParentLocationID IS NULL then 1
                        when ParentLocationID = 0 then 1
                        else 0
                    end
                else
                    case
                        when ID = @iLocationID then 1
                        else 0
                    end
                end
    UNION ALL
-- Recursive member definition
    SELECT
            l.ID,
            l.ParentLocationID,
            l.Name,
            l.CustomerID,           
            Level + 1,
            CAST(LevelIndentText + '' > '' + l.Name as nvarchar(max)),
            --CAST(replicate(''&nbsp;&nbsp;&nbsp;&nbsp;'',Level+1) + l.Name as nvarchar(max)),
            CAST(l.Name as nvarchar(max)) as LevelText,
            RootLocationID
    FROM
            Location l
    INNER JOIN Location_CTE lc ON
            lc.ID = l.ParentLocationID
)
SELECT
      a.Level,
      a.LevelText,
      a.LevelIndentText,
      a.RootLocationID,
      b.*
FROM
      Location_CTE a
JOIN
      Location b
ON
      a.ID = b.ID

)
/****** Object:  View [dbo].[JobRoleHierarchy]    Script Date: 01/07/2008 09:35:39 ******/
' 
END
GO
/****** Object:  View [dbo].[UserToGTMMeeting]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[UserToGTMMeeting]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[UserToGTMMeeting]
AS
SELECT     gtmM.ID AS GTMMeetingID, gtmO.UserID, 1 AS IsOrganizer
FROM         dbo.GTMMeeting AS gtmM INNER JOIN
                      dbo.GTMOrganizer AS gtmO ON gtmO.ID = gtmM.GTMOrganizerID
UNION
SELECT     gtmM.ID, gtmI.UserID, 0 AS IsOrganizer
FROM         dbo.GTMInvitation AS gtmI INNER JOIN
                      dbo.GTMMeeting AS gtmM ON gtmM.ID = gtmI.GTMMeetingID INNER JOIN
                      dbo.GTMOrganizer AS gtmO ON gtmO.ID = gtmM.GTMOrganizerID AND gtmI.UserID <> gtmO.UserID
'
GO
/****** Object:  View [dbo].[GTMOrganizerCount]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[GTMOrganizerCount]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[GTMOrganizerCount]
AS
SELECT     COUNT(gtmO.UserID) AS OrganizerCount, cUser.CustomerID
FROM         customerblackbox.dbo.[User] AS cUser LEFT OUTER JOIN
                      dbo.GTMOrganizer AS gtmO ON gtmO.UserID = cUser.ID
GROUP BY cUser.CustomerID
'
GO
/****** Object:  View [dbo].[FullGTMInvitations]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[FullGTMInvitations]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[FullGTMInvitations]
AS
SELECT     gtmI.ID, gtmI.ID AS GTMInvitationID, gtmI.UserID, gtmI.GTMMeetingID, cUser.Username, cUser.CustomerID, cUser.FirstName, cUser.MiddleName, 
                      cUser.LastName, cUser.LastName + '', '' + cUser.FirstName + '' '' + cUser.MiddleName AS FullName
FROM         dbo.GTMInvitation AS gtmI INNER JOIN
                      customerblackbox.dbo.[User] AS cUser ON cUser.ID = gtmI.UserID
'
GO
/****** Object:  View [dbo].[vw_aspnet_MembershipUsers]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_MembershipUsers]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[vw_aspnet_MembershipUsers]
  AS SELECT [dbo].[aspnet_Membership].[UserId],
            [dbo].[aspnet_Membership].[PasswordFormat],
            [dbo].[aspnet_Membership].[MobilePIN],
            [dbo].[aspnet_Membership].[Email],
            [dbo].[aspnet_Membership].[LoweredEmail],
            [dbo].[aspnet_Membership].[PasswordQuestion],
            [dbo].[aspnet_Membership].[PasswordAnswer],
            [dbo].[aspnet_Membership].[IsApproved],
            [dbo].[aspnet_Membership].[IsLockedOut],
            [dbo].[aspnet_Membership].[CreateDate],
            [dbo].[aspnet_Membership].[LastLoginDate],
            [dbo].[aspnet_Membership].[LastPasswordChangedDate],
            [dbo].[aspnet_Membership].[LastLockoutDate],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptWindowStart],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptWindowStart],
            [dbo].[aspnet_Membership].[Comment],
            [dbo].[aspnet_Users].[ApplicationId],
            [dbo].[aspnet_Users].[UserName],
            [dbo].[aspnet_Users].[MobileAlias],
            [dbo].[aspnet_Users].[IsAnonymous],
            [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Membership] INNER JOIN [dbo].[aspnet_Users]
      ON [dbo].[aspnet_Membership].[UserId] = [dbo].[aspnet_Users].[UserId]
'
GO
/****** Object:  StoredProcedure [dbo].[GetCustomerSubDomain]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCustomerSubDomain]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCustomerSubDomain]
	-- Add the parameters for the stored procedure here
	@iCustomerID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select
		SubDomain
	from
		Customer
	where
		ID = @iCustomerID
END
' 
END
GO
/****** Object:  View [dbo].[CustomerHierarchy]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CustomerHierarchy]'))
EXEC dbo.sp_executesql @statement = N'


/* ExecuteTransaction() */ CREATE view [dbo].[CustomerHierarchy] as
select
c.*,
IsHoldingCompany = cast(case
when
(select count(*) from dbo.Customer where ParentCustomerID = c.ID) > 0
then
cast(1 as bit)
else
cast(0 as bit)
end as bit)
from
Customer c;
'
GO
/****** Object:  StoredProcedure [dbo].[GetCoursesToUnregister]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCoursesToUnregister]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

create procedure [dbo].[GetCoursesToUnregister](@trainingprogramid int, @userid int, @targetcourseid int)
as
begin

/* types from various tables */
declare @classroomtype int
declare @syllabustypeid int
set @classroomtype = (select id from coursetype where [name] = ''classroom'')
set @syllabustypeid = (select id from syllabustype where [name] = ''required'')


select
	*
from
	StudentTranscript
where
	(
	CourseID in
		(
		select
			CourseID
		from
			trainingprogramtocourselink l
		join
			trainingProgram p
		on
			l.Trainingprogramid = p.id
		-- for this training program, only required courses, only if enforceorder is true
		where
			trainingprogramid = @trainingprogramid
		and
			syllabustypeid = @syllabustypeid	
		and
			p.EnforceRequiredOrder = 1
		and
			--only the courses after the current course
			requiredsyllabusorder > (
				select 
					requiredsyllabusorder 
				from 
					trainingprogramtocourselink a 
				where 
					courseid = @targetcourseid 
				and	
					coursetypeid = @classroomtype
				and 
					trainingprogramid = @trainingprogramid
			)
		and
			coursetypeid = @classroomtype
		)
		and 
			userid = @userid
	)
end
' 
END
GO
/****** Object:  View [dbo].[CourseProblems]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CourseProblems]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[CourseProblems]
AS
SELECT     ROW_NUMBER() OVER (ORDER BY PossibleProblemID, CourseID) AS ID, PossibleProblemID, CourseID, SentProblemID, Name, Description, Subject, 
Body
FROM         (SELECT     caPProblem.ID AS PossibleProblemID, cCourse.ID AS CourseID, ISNULL(caSProblem.ID, 0) AS SentProblemID, caPProblem.Name, 
                                              caPProblem.Description, caPProblem.Subject, caPProblem.Body
                       FROM          dbo.PossibleProblem AS caPProblem INNER JOIN
                                              dbo.CourseType AS caCourseT ON caCourseT.ID = caPProblem.CourseTypeID LEFT OUTER JOIN
                                              classroom.dbo.Course AS cCourse ON cCourse.ID = cCourse.ID LEFT OUTER JOIN
                                              dbo.SentProblem AS caSProblem ON caSProblem.CourseID = cCourse.ID AND caSProblem.PossibleProblemID = caPProblem.ID
                       WHERE      (caCourseT.CodeName = ''classroom'')
                       UNION
                       SELECT     caPProblem.ID AS PossibleProblemID, otOCourse.ID AS CourseID, ISNULL(caSProblem.ID, 0) AS SentProblemID, caPProblem.Name, 
                                             caPProblem.Description, caPProblem.Subject, caPProblem.Body
                       FROM         dbo.PossibleProblem AS caPProblem INNER JOIN
                                             dbo.CourseType AS caCourseT ON caCourseT.ID = caPProblem.CourseTypeID LEFT OUTER JOIN
                                             onlinetraining.dbo.OnlineCourse AS otOCourse ON otOCourse.ID = otOCourse.ID LEFT OUTER JOIN
                                             dbo.SentProblem AS caSProblem ON caSProblem.CourseID = otOCourse.ID AND caSProblem.PossibleProblemID = caPProblem.ID
                       WHERE     (caCourseT.CodeName = ''online'')) AS T
'
GO
/****** Object:  UserDefinedFunction [dbo].[fGetClasses]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetClasses]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

CREATE function [dbo].[fGetClasses](
    @iUserID int,
    @iCustomerID int,
    @bILTManagerUsersRegistered bit,
	@bILTManagerUsersWaitListed bit,
	@bILTManagerUsersAwaitingApproval bit,
    @bILTManagerUsersDenied bit,
	@bILTManagerAll bit,
    @bInstructor bit,
    @bResourceManagerWithRequired bit,
    @bResourceManagerWithoutRequired bit,
    @bAvailableClassesStudent bit,
    @bAwaitingApprovalStudent bit,
    @bRegisteredStudent bit,
    @bWaitListStudent bit,
    @bDeniedStudent bit,
	@bSupervisorUsersRegistered bit,
	@bSupervisorUsersWaitListed bit,
	@bSupervisorUsersAwaitingApproval bit,
    @bSupervisorUsersDenied bit,
    @sSupervisedUserList nvarchar(max),
    @sLikeText nvarchar(max),
    @iStart int,
    @iLimit int,
	@sSortCol varchar(200),
	@sSortDir varchar(4)
) returns @Items TABLE (
    IsILTManagerUsersRegistered bit,
    IsILTManagerUsersWaitListed bit,
	IsILTManagerUsersAwaitingApproval bit,
	IsILTManagerUsersDenied bit,
	IsILTManagerAll bit,
    IsInstructor bit,
    IsResourceManagerWithRequired bit,
    IsResourceManagerWithoutRequired bit,
    IsRegisteredStudent bit,
    IsAvailableClassesStudent bit,
    IsAwaitingApprovalStudent bit,
    IsWaitListStudent bit,
    IsDeniedStudent bit,
	IsSupervisorUsersRegistered bit,
    IsSupervisorUsersWaitListed bit,
	IsSupervisorUsersAwaitingApproval bit,
	IsSupervisorUsersDenied bit,
    RoomName nvarchar(50),
    ID int,
    [Name] nvarchar(50),
    [Description] nvarchar(1000),
    InternalCode nvarchar(50),
    RoomID int,
    CourseID int,
    TimeZoneID int,
    CapacityOverride int,
    LockedIndicator bit,
    ModifiedBy nvarchar(50),
    CreatedBy nvarchar(50),
    ModifiedOn datetime,
    CreatedOn datetime,
    AdditionalInstructions nvarchar(1000),
    Notes nvarchar(1000),
    Objective nvarchar(1000),
    CustomerID int,
    MinStartDateTime datetime,
    MaxStartDateTime datetime,
    InstructorIDs varchar(MAX),
    [Index] int,
    IsVirtual bit,
    SurveyID int,
    TotalRecordCount int
)
begin

--I HATE THIS...stupid orderby made me do it. this table is identical to "items" above, with one change...substitute totalrecord count and index
declare @TempItems TABLE (
    IsILTManagerUsersRegistered bit,
    IsILTManagerUsersWaitListed bit,
	IsILTManagerUsersAwaitingApproval bit,
	IsILTManagerUsersDenied bit,
	IsILTManagerAll bit,
    IsInstructor bit,
    IsResourceManagerWithRequired bit,
    IsResourceManagerWithoutRequired bit,
    IsRegisteredStudent bit,
    IsAvailableClassesStudent bit,
    IsAwaitingApprovalStudent bit,
    IsWaitListStudent bit,
    IsDeniedStudent bit,
	IsSupervisorUsersRegistered bit,
    IsSupervisorUsersWaitListed bit,
	IsSupervisorUsersAwaitingApproval bit,
	IsSupervisorUsersDenied bit,
    RoomName nvarchar(50),
    ID int,
    [Name] nvarchar(50),
    [Description] nvarchar(1000),
    InternalCode nvarchar(50),
    RoomID int,
    CourseID int,
    TimeZoneID int,
    CapacityOverride int,
    LockedIndicator bit,
    ModifiedBy nvarchar(50),
    CreatedBy nvarchar(50),
    ModifiedOn datetime,
    CreatedOn datetime,
    AdditionalInstructions nvarchar(1000),
    Notes nvarchar(1000),
    Objective nvarchar(1000),
    CustomerID int,
    MinStartDateTime datetime,
    MaxStartDateTime datetime,
    InstructorIDs varchar(MAX),
    IsVirtual bit,
    SurveyID int,
    [Index] int
)

declare @s varchar(200)
set @s = ''ClassName''
declare @SupervisedUsers table
(
    UserID int
)

insert @SupervisedUsers
select
    *
from
    dbo.fSplit(@sSupervisedUserList,'','');

insert @TempItems

    select *, ROW_NUMBER() over (order by [Name]) as [Index] from( --outer select so we can use the column aliases and get row numbers
        select --acutal data select
            case when @bILTManagerUsersRegistered = 1 then
                case when (select count(*) from Registration where ClassID = c.ID and RegistrationStatusID = 4) > 0 then 1 else 0 end
            else 0 end as IsILTManagerUsersRegistered,

			case when @bILTManagerUsersDenied = 1 then
                case when (select count(*) from Registration where ClassID = c.ID and RegistrationStatusID = 3) > 0 then 1 else 0 end
            else 0 end as IsILTManagerUsersDenied,

            case when @bILTManagerUsersWaitListed = 1 then
                case when (select count(*) from Registration where ClassID = c.ID and RegistrationStatusID = 2) > 0 then 1 else 0 end
            else 0 end as IsILTManagerUsersWaitListed,
			
			case when @bILTManagerUsersAwaitingApproval = 1 then
                case when (select count(*) from Registration where ClassID = c.ID and RegistrationStatusID = 1) > 0 then 1 else 0 end
            else 0 end as IsILTManagerUsersAwaitingApproval,

			case when @bILTManagerAll = 1 then 1 else 0 end as IsILTManagerAll,

            case when @bInstructor = 1 then
                case when (select count(*) from ClassInstructors where InstructorID = @iUserID and ClassID = c.ID) > 0 then 1 else 0 end
            else 0 end as IsInstructor,

            case when @bResourceManagerWithRequired = 1 then
                case when (select count(*) from ClassResourceManagers where ResourceManagerID = @iUserID and ClassID = c.ID) > 0 then 1 else 0 end
            else 0 end as IsResourceManagerWithRequired,

            case when @bResourceManagerWithoutRequired = 1 then
                case when (select count(*) from ClassResourceManagers where ResourceManagerID = @iUserID and ClassID = c.ID) = 0 then 1 else 0 end
            else 0 end as IsResourceManagerWithoutRequired,

            case when @bRegisteredStudent = 1 then
                case when (select count(*) from Registration where RegistrantID = @iUserID and ClassID = c.ID and RegistrationStatusID = 4) > 0 then 1 else 0 end
            else 0 end as IsRegisteredStudent,

            case when @bAvailableClassesStudent = 1 then
                case when (select count(*) from PublicClasses where ID = c.ID) > 0 then 1 else 0 end
            else 0 end as IsAvailableClassesStudent,

            case when @bAwaitingApprovalStudent = 1 then
                case when (select count(*) from Registration where RegistrantID = @iUserID and ClassID = c.ID and RegistrationStatusID = 1) > 0 then 1 else 0 end
            else 0 end as IsAwaitingApprovalStudent,

            case when @bWaitListStudent = 1 then
                case when (select count(*) from Registration where RegistrantID = @iUserID and ClassID = c.ID and RegistrationStatusID = 2) > 0 then 1 else 0 end
            else 0 end as IsWaitListStudent,

            case when @bDeniedStudent = 1 then
                case when (select count(*) from Registration where RegistrantID = @iUserID and ClassID = c.ID and RegistrationStatusID = 3) > 0 then 1 else 0 end
            else 0 end as IsDeniedStudent,

			case when @bSupervisorUsersRegistered = 1 then
                case when (select count(*) from Registration where RegistrantID in (select * from @SupervisedUsers) and ClassID = c.ID and RegistrationStatusID = 4) > 0 then 1 else 0 end
            else 0 end as IsSupervisorUsersRegistered,

            case when @bSupervisorUsersWaitListed = 1 then
                case when (select count(*) from Registration where RegistrantID in (select * from @SupervisedUsers) and ClassID = c.ID and RegistrationStatusID = 2) > 0 then 1 else 0 end
            else 0 end as IsSupervisorUsersWaitListed,
			
			case when @bSupervisorUsersAwaitingApproval = 1 then
                case when (select count(*) from Registration where RegistrantID in (select * from @SupervisedUsers) and ClassID = c.ID and RegistrationStatusID = 1) > 0 then 1 else 0 end
            else 0 end as IsSupervisorUsersAwaitingApproval,

			case when @bSupervisorUsersDenied = 1 then
                case when (select count(*) from Registration where RegistrantID in (select * from @SupervisedUsers) and ClassID = c.ID and RegistrationStatusID = 3) > 0 then 1 else 0 end
            else 0 end as IsSupervisorUsersDenied,

            --case when @bSupervisor = 1 then
                --case when (select count(*) from Registration where RegistrantID in (select * from @SupervisedUsers) and ClassID = c.ID) > 0 then 1 else 0 end
            --else 0 end as HasSupervisedStudents,

            r.Name as RoomName,
            c.ID, c.Name, c.Description, c.InternalCode, c.RoomID, c.CourseID, c.TimeZoneID, c.CapacityOverride, c.LockedIndicator, c.ModifiedBy, c.CreatedBy, c.ModifiedOn, c.CreatedOn, c.AdditionalInstructions, c.Notes, c.Objective, c.CustomerID,
            isnull(min(cd.StartDateTime),''1/1/1900'') AS MinStartDateTime,
            isnull(max(cd.StartDateTime),''1/1/1900'') AS MaxStartDateTime,
            InstructorIDs = isnull(replace(
                    (
                    select
                        ci.InstructorID as [data()]
                    from
                        ClassInstructors ci
                    where
                        ci.ClassID = c.ID
                    group by
                        ci.InstructorID
                    for xml path ('''')
                    ), '' '', '',''
                ),''''), 
            c.IsVirtual,
			c.SurveyID
        from
            Class c
        left join
            Room r
        on
            c.RoomID = r.ID
        left join
            ClassDate cd
        on
            cd.ClassID = c.ID
        where
            c.CustomerID = @iCustomerID
        group by
            r.Name, c.ID, c.Name, c.Description, c.InternalCode, c.RoomID, c.CourseID, c.TimeZoneID, c.CapacityOverride, c.LockedIndicator, c.ModifiedBy, c.CreatedBy, c.ModifiedOn, c.CreatedOn, c.AdditionalInstructions, c.Notes, c.Objective, c.CustomerID, c.IsVirtual, c.SurveyID
        ) as InnerClass
    where
    (
        1 = case @bILTManagerUsersRegistered when 1 then
                case IsILTManagerUsersRegistered when 1 then 1 else 0 end
            end
        or
        1 = case @bILTManagerUsersWaitListed when 1 then
                case IsILTManagerUsersWaitListed when 1 then 1 else 0 end
            end
        or
		1 = case @bILTManagerUsersAwaitingApproval when 1 then
                case IsILTManagerUsersAwaitingApproval when 1 then 1 else 0 end
            end
        or
		1 = case @bILTManagerUsersDenied when 1 then
                case IsILTManagerUsersDenied when 1 then 1 else 0 end
            end
        or
		1 = case @bILTManagerAll when 1 then
                case IsILTManagerAll when 1 then 1 else 0 end
            end
        or
        1 = case @bInstructor when 1 then
                case IsInstructor when 1 then 1 else 0 end
            end
        or
        1 = case @bResourceManagerWithRequired when 1 then
                case IsResourceManagerWithRequired when 1 then 1 else 0 end
            end
        or
        1 = case @bResourceManagerWithoutRequired when 1 then
                case IsResourceManagerWithoutRequired when 1 then 1 else 0 end
            end
        or
        1 = case @bRegisteredStudent when 1 then
                case IsRegisteredStudent when 1 then 1 else 0 end
            end
        or
        1 = case @bAvailableClassesStudent when 1 then
                case IsAvailableClassesStudent when 1 then 1 else 0 end
            end
        or
        1 = case @bAwaitingApprovalStudent when 1 then
                case IsAwaitingApprovalStudent when 1 then 1 else 0 end
            end
        or
        1 = case @bWaitListStudent when 1 then
                case IsWaitListStudent when 1 then 1 else 0 end
            end
        or
        1 = case @bDeniedStudent when 1 then
                case IsDeniedStudent when 1 then 1 else 0 end
            end
        or
        1 = case @bSupervisorUsersRegistered when 1 then
                case IsSupervisorUsersRegistered when 1 then 1 else 0 end
            end
        or
        1 = case @bSupervisorUsersWaitListed when 1 then
                case IsSupervisorUsersWaitListed when 1 then 1 else 0 end
            end
        or
		1 = case @bSupervisorUsersAwaitingApproval when 1 then
                case IsSupervisorUsersAwaitingApproval when 1 then 1 else 0 end
            end
        or
		1 = case @bSupervisorUsersDenied when 1 then
                case IsSupervisorUsersDenied when 1 then 1 else 0 end
            end
    )
    and
        [Name] like @sLikeText


	if(lower(@sSortDir) = ''asc'' or lower(@sSortDir) = '''')
	begin
		insert
			@Items
		select
			*, (select count(*) from @TempItems) as TotalRecordCount
		from
			@TempItems
		where
			[Index] between @iStart and (@iStart + @iLimit - 1)
		order by
			case lower(@sSortCol)
				when ''classname'' then [Name]
				when ''roomname'' then [RoomName]
				when ''minstartdatetime'' then cast(MinStartDateTime as varchar(100))
				when ''instructor'' then InstructorIDs
				else [Name]
			end
		asc
	end
	else
	begin
		insert
			@Items
		select
			*, (select count(*) from @TempItems) as TotalRecordCount
		from
			@TempItems
		where
			[Index] between @iStart and (@iStart + @iLimit - 1)
		order by
			case lower(@sSortCol)
				when ''classname'' then [Name]
				when ''roomname'' then [RoomName]
				when ''minstartdatetime'' then cast(MinStartDateTime as varchar(100))
				when ''instructor'' then InstructorIDs
				else [Name]
			end
		desc
	end
return
end
' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fGetSalesChannelHierarchyBranch]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetSalesChannelHierarchyBranch]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fGetSalesChannelHierarchyBranch]
(	
	--the node that''ll be considered the root node of the branch
	@iSalesChannelID int
)
RETURNS TABLE 
AS
RETURN 
(
WITH SalesChannel_CTE (ID, ParentSalesChannelID, Name, Level, RootSalesChannelID)
AS
(
-- Anchor member definition
    SELECT
            rl.ID, 
            rl.ParentSalesChannelID,
            rl.Name,
            0 AS Level,
			rl.ID as RootSalesChannelID
    FROM
            SalesChannel rl
      WHERE
			--Note by Jerod
			--Added a case in here...this way, passing "0" will get the full tree, while passing
			--the ID of any node will get that node''s branch.
			--Note that this means if you have 1 root node in the tree, passing that node id or passing 0 will give the same result set
            1 = case when @iSalesChannelID = 0 then
					case --allow 0 or nulls as root nodes
						when ParentSalesChannelID IS NULL then 1
						when ParentSalesChannelID = 0 then 1
						else 0
					end
				else
					case
						when ID = @iSalesChannelID then 1
						else 0
					end
				end
					

    UNION ALL
-- Recursive member definition
    SELECT
            rl.ID,
            rl.ParentSalesChannelID,
            rl.Name,
            Level + 1,
			RootSalesChannelID
    FROM
            SalesChannel rl
    INNER JOIN SalesChannel_CTE sc ON
            sc.ID = rl.ParentSalesChannelID
)
SELECT
      ID,
      ParentSalesChannelID,
      Name,
      Level,
	  RootSalesChannelID
FROM
      SalesChannel_CTE

)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_CreateUser]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_CreateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Users_CreateUser]
    @ApplicationId    uniqueidentifier,
    @UserName         nvarchar(256),
    @IsUserAnonymous  bit,
    @LastActivityDate DATETIME,
    @UserId           uniqueidentifier OUTPUT
AS
BEGIN
    IF( @UserId IS NULL )
        SELECT @UserId = NEWID()
    ELSE
    BEGIN
        IF( EXISTS( SELECT UserId FROM dbo.aspnet_Users
                    WHERE @UserId = UserId ) )
            RETURN -1
    END

    INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
    VALUES (@ApplicationId, @UserId, @UserName, LOWER(@UserName), @IsUserAnonymous, @LastActivityDate)

    RETURN 0
END
' 
END
GO
/****** Object:  View [dbo].[vw_aspnet_Users]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Users]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[vw_aspnet_Users]
  AS SELECT [dbo].[aspnet_Users].[ApplicationId], [dbo].[aspnet_Users].[UserId], [dbo].[aspnet_Users].[UserName], [dbo].[aspnet_Users].[LoweredUserName], [dbo].[aspnet_Users].[MobileAlias], [dbo].[aspnet_Users].[IsAnonymous], [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Users]
'
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Paths]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Paths]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[vw_aspnet_WebPartState_Paths]
  AS SELECT [dbo].[aspnet_Paths].[ApplicationId], [dbo].[aspnet_Paths].[PathId], [dbo].[aspnet_Paths].[Path], [dbo].[aspnet_Paths].[LoweredPath]
  FROM [dbo].[aspnet_Paths]
'
GO
/****** Object:  View [dbo].[vw_aspnet_Roles]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Roles]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[vw_aspnet_Roles]
  AS SELECT [dbo].[aspnet_Roles].[ApplicationId], [dbo].[aspnet_Roles].[RoleId], [dbo].[aspnet_Roles].[RoleName], [dbo].[aspnet_Roles].[LoweredRoleName], [dbo].[aspnet_Roles].[Description]
  FROM [dbo].[aspnet_Roles]
'
GO
/****** Object:  View [dbo].[VenueSearch]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VenueSearch]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[VenueSearch]
AS
SELECT     dbo.Venue.ID, dbo.Venue.Name, dbo.Venue.CustomerID, dbo.Venue.InternalCode, dbo.Venue.City, dbo.Venue.StateID, dbo.Venue.TimeZoneID, dbo.States.Name + '' '' + dbo.Venue.Name + '' '' + dbo.Venue.City AS SearchText, dbo.Venue.ModifiedBy, 
                      dbo.Venue.CreatedBy, dbo.Venue.ModifiedOn, dbo.Venue.CreatedOn
FROM         dbo.Venue INNER JOIN
                      dbo.States ON dbo.Venue.StateID = dbo.States.ID
'
GO
/****** Object:  View [dbo].[ClassMinStartDateTime]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ClassMinStartDateTime]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[ClassMinStartDateTime]
AS
SELECT     MIN(StartDateTime) AS MinStartDateTime, ClassID
FROM         dbo.ClassDate
GROUP BY ClassID
'
GO
/****** Object:  View [dbo].[ClassMaxStartDateTime]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ClassMaxStartDateTime]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[ClassMaxStartDateTime]
AS
SELECT     MAX(StartDateTime) AS MaxStartDateTime, ClassID
FROM         dbo.ClassDate
GROUP BY ClassID
'
GO
/****** Object:  View [dbo].[ClassDateMinMax]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ClassDateMinMax]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[ClassDateMinMax]
AS
SELECT     
	ClassID, 
	CustomerID,
	MIN(StartDateTime) AS MinStartDateTime, 
	MAX(StartDateTime) AS MaxStartDateTime
FROM         
    ClassDate AS cClassD 
GROUP BY 
	ClassID,
	CustomerID
'
GO
/****** Object:  View [dbo].[vw_aspnet_UsersInRoles]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_UsersInRoles]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[vw_aspnet_UsersInRoles]
  AS SELECT [dbo].[aspnet_UsersInRoles].[UserId], [dbo].[aspnet_UsersInRoles].[RoleId]
  FROM [dbo].[aspnet_UsersInRoles]
'
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_User]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_User]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[vw_aspnet_WebPartState_User]
  AS SELECT [dbo].[aspnet_PersonalizationPerUser].[PathId], [dbo].[aspnet_PersonalizationPerUser].[UserId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationPerUser].[PageSettings]), [dbo].[aspnet_PersonalizationPerUser].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationPerUser]
'
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Shared]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Shared]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[vw_aspnet_WebPartState_Shared]
  AS SELECT [dbo].[aspnet_PersonalizationAllUsers].[PathId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationAllUsers].[PageSettings]), [dbo].[aspnet_PersonalizationAllUsers].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationAllUsers]
'
GO
/****** Object:  View [dbo].[UsersSuperVisorHierarchy]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[UsersSuperVisorHierarchy]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[UsersSuperVisorHierarchy] AS
WITH User_CTE (ID, supervisorID, Lastname, CustomerID, Level, LevelIndentText, LevelText, RootUserID)
AS
(
-- Anchor member definition
    SELECT
            l.ID,
            l.supervisorID,
            l.lastname,
            l.CustomerID,
            0 AS Level,
            CAST((l.lastname) as nvarchar(max)) as LevelIndentText,
            --CAST(l.Name as nvarchar(max)) as LevelText,
            CAST((l.lastname + '', '' + l.firstname) as nvarchar(max)) as LevelText,
            l.ID as RootUserID
    FROM
            [user] l
      WHERE
            --SuperVisorID > 0
			SuperVisorID IS NULL OR SuperVisorID = 0
    UNION ALL
-- Recursive member definition
    SELECT
            l.ID,
            l.supervisorID,
            l.lastname,
            l.CustomerID,           
            Level + 1,
            CAST(LevelIndentText + '' > '' + l.lastname as nvarchar(max)),
            --CAST(replicate(''&nbsp;&nbsp;&nbsp;&nbsp;'',Level+1) + l.Name as nvarchar(max)),
            CAST(l.lastName + '', '' + l.firstname  as nvarchar(max)) as LevelText,
            RootUserID
    FROM
            [user] l
    INNER JOIN User_CTE lc ON
            lc.ID = l.supervisorID
	--where l.CustomerID != 80
)
SELECT
     *
      
FROM
      User_CTE --a
--JOIN
--      [user] b
--ON
--    a.ID = b.ID
'
GO
/****** Object:  View [dbo].[UsersSuperVisorHierarchy_1]    Script Date: 08/05/2010 15:57:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[UsersSuperVisorHierarchy_1]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[UsersSuperVisorHierarchy_1] AS
WITH Users_CTE (ID, SupervisorID, Name, CustomerID, Level, LevelIndentText, LevelText, RootSupervisorID)
AS
(
-- Anchor member definition
SELECT
u.ID,
u.SupervisorID,
u.Username,
u.CustomerID,
0 AS Level,
CAST(u.Username as nvarchar(max)) as LevelIndentText,
--CAST(l.Name as nvarchar(max)) as LevelText,
CAST(u.Username as nvarchar(max)) as LevelText,
u.ID as RootUserID
FROM
[User] u
WHERE
SupervisorID IS NULL OR SupervisorID = 0
UNION ALL
-- Recursive member definition
SELECT
u.ID,
u.SupervisorID,
u.Username,
u.CustomerID,
Level+1,
CAST(LevelIndentText + '' > '' + u.Username as nvarchar(max)),
--CAST(l.Name as nvarchar(max)) as LevelText,
CAST(u.Username as nvarchar(max)) as LevelText,
cte.RootSupervisorID
FROM
[User] u
INNER JOIN Users_CTE cte ON
cte.ID = u.SupervisorID
)
select
*
from
Users_CTE
'
GO
/****** Object:  UserDefinedFunction [dbo].[fGetAudienceHierarchyBranch]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetAudienceHierarchyBranch]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

CREATE FUNCTION [dbo].[fGetAudienceHierarchyBranch]
(   
    --the node that''ll be considered the root node of the branch
    @iAudienceID int
)
RETURNS TABLE
AS
RETURN
(
WITH Audience_CTE (ID, ParentAudienceID, Name, CustomerID, Level, LevelIndentText, LevelText, RootAudienceID)
AS
(
-- Anchor member definition
    SELECT
            l.ID,
            l.ParentAudienceID,
            l.Name,
            l.CustomerID,
            0 AS Level,
            CAST(l.Name as nvarchar(max)) as LevelIndentText,
            CAST(l.Name as nvarchar(max)) as LevelText,
            l.ID as RootAudienceID
        FROM
            Audience l
      WHERE
                  --Note by Jerod
            --Added a case in here...this way, passing "0" will get the full tree, while passing
            --the ID of any node will get that node''s branch.
            --Note that this means if you have 1 root node in the tree, passing that node id or passing 0 will give the same result set
            1 = case when @iAudienceID = 0 then
                    case --allow 0 or nulls as root nodes
                        when ParentAudienceID IS NULL then 1
                        when ParentAudienceID = 0 then 1
                        else 0
                    end
                else
                    case
                        when ID = @iAudienceID then 1
                        else 0
                    end
                end
    UNION ALL
-- Recursive member definition
    SELECT
            l.ID,
            l.ParentAudienceID,
            l.Name,
            l.CustomerID,           
            Level + 1,
            CAST(LevelIndentText + '' > '' + l.Name as nvarchar(max)),
            --CAST(replicate(''&nbsp;&nbsp;&nbsp;&nbsp;'',Level+1) + l.Name as nvarchar(max)),
            CAST(l.Name as nvarchar(max)) as LevelText,
            RootAudienceID
    FROM
            Audience l
    INNER JOIN Audience_CTE lc ON
            lc.ID = l.ParentAudienceID
)
SELECT
      a.Level,
      a.LevelText,
      a.LevelIndentText,
      a.RootAudienceID,
      b.*
FROM
      Audience_CTE a
JOIN
      Audience b
ON
      a.ID = b.ID

)
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetAudienceByParent]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAudienceByParent]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAudienceByParent]
	-- Add the parameters for the stored procedure here
	@iParentID int,
	@iBusinessEntityID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select
		*
	from
		Audience
	where
		ParentAudienceID = @iParentID
	and
		CustomerID = @iBusinessEntityID
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_WebEvent_LogEvent]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_LogEvent]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_WebEvent_LogEvent]
        @EventId         char(32),
        @EventTimeUtc    datetime,
        @EventTime       datetime,
        @EventType       nvarchar(256),
        @EventSequence   decimal(19,0),
        @EventOccurrence decimal(19,0),
        @EventCode       int,
        @EventDetailCode int,
        @Message         nvarchar(1024),
        @ApplicationPath nvarchar(256),
        @ApplicationVirtualPath nvarchar(256),
        @MachineName    nvarchar(256),
        @RequestUrl      nvarchar(1024),
        @ExceptionType   nvarchar(256),
        @Details         ntext
AS
BEGIN
    INSERT
        dbo.aspnet_WebEvent_Events
        (
            EventId,
            EventTimeUtc,
            EventTime,
            EventType,
            EventSequence,
            EventOccurrence,
            EventCode,
            EventDetailCode,
            Message,
            ApplicationPath,
            ApplicationVirtualPath,
            MachineName,
            RequestUrl,
            ExceptionType,
            Details
        )
    VALUES
    (
        @EventId,
        @EventTimeUtc,
        @EventTime,
        @EventType,
        @EventSequence,
        @EventOccurrence,
        @EventCode,
        @EventDetailCode,
        @Message,
        @ApplicationPath,
        @ApplicationVirtualPath,
        @MachineName,
        @RequestUrl,
        @ExceptionType,
        @Details
    )
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetTranscript]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTranscript]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'





CREATE procedure [dbo].[GetTranscript](@userId int)
as
begin

	select
		oc.ID as CourseID,
		oc.[Name] as CourseName,
		2 as CourseTypeID,
		0 as ClassID,
		0 as RegistrationID,
		0 as RegistrationStatusID,
		case 
			when Success is null then ''Unknown''
			when Success = 0 then ''Failed''
			when Success = 1 then ''Passed''
		end
		as PassOrFail,
		Success as Passed,
		case 
			when Completion is null then ''Unknown''
			when Completion = 0 then ''Incomplete''
			when Completion = 1 then ''Completed''
		end
		as CompleteOrIncomplete,
		Completion as Completed,
		0 as OnlineTestID,
		0 as SurveyID,
		oc.PublicIndicator as IsPublic,
		cast(floor(round(cro.Score,0)) as nvarchar(50)) as Score, 
		coalesce(HighScoreDate, cro.Modifiedon, null) as AttemptDate,
		cro.TotalSeconds as AttemptTime,
		cro.AttemptCount as AttemptCount,
		null as StartDate,
		coalesce(HighScoreDate, cro.Modifiedon, null) as EndDate,
		cro.TrainingProgramID
	from
		OnlineCourseRollup as cro 
	left outer join                  
		onlineCourse as oc 
	on
		oc.ID = cro.CourseID
	where
		cro.UserID = @userID
	and
		-- there shouldn''t be any rollups without courses, but you never know...
		oc.ID is not null

	UNION

	select
		CourseID,
		CourseName,
		1 as CourseTypeID,
		ClassID,
		RegistrationID,
		RegistrationStatusID,
		case 
			when Passed = 0 and ScoreReported = 1 then ''Failed''
			when Passed = 1 and ScoreReported = 1 then ''Passed''
			else ''Unknown''
		end as PassOrFail,
		Passed,
		case ScoreReported
			when 1 then ''Complete''
			else ''Incomplete''
		end as CompleteOrIncomplete,
		ScoreReported as Completed,
		OnlineTestID,
		SurveyID,
		PublicIndicator,
		Score,
		AttemptDate,
		AttemptTime,
		AttemptCount,
		StartDate,
		EndDate,
		0 as TrainingProgramID
	from
	(
		select
			cCourse.ID as CourseID,
			cCourse.[Name] as CourseName,
			cClass.ID as ClassID,
			cReg.ID as RegistrationID, 
			cReg.RegistrationStatusID as RegistrationStatusID,
			-- Whether or not the user has passed changes based on course type and completion type.
			-- This is where minimum passing scores are set for classroom courses.
			-- Online courses have passing scores on a per-course basis, and the "passed" flag will be set as appropriate.
			case
				when cCourseCT.CodeName = ''attendance'' and cReg.AttendedIndicator = 1 
					then 1
				when cCourseCT.CodeName = ''attendance''
					then 0
				when cCourseCT.CodeName = ''numeric'' and ( len(cReg.Score) = 3 or cReg.Score >= cast(isnull(cPass.PassingScore, 70) as varchar(15)) )
					then 1
				when cCourseCT.CodeName = ''numeric'' 
					then 0
				when cCourseCT.CodeName = ''letter'' and cReg.Score = ''F'' 
					then 0 
				when cCourseCT.CodeName = ''letter'' 
					then 1 
				when cCourseCT.CodeName = ''onlinecourse'' AND ocr.Success = 1
					then 1 
				when cCourseCT.CodeName = ''onlinecourse'' 
					then 0 
			end as Passed,
			-- To determine if a user has attended a course with attendance based passing,
			-- we have a bit of a conundrum - there''s no way to know if they DIDN''T attend, since the default value for
			-- attended is 0. Therefore, (and per BE''s instructions) we assume that if the registration was modified after the 
			-- last day of the class, then whatever indicator is in the db was the "reported" value. So, if the date modified is
			-- after the last day of the class, or the AttendedIndicator is true, the instructor reported a score.
			-- For online courses, the ''Complete'' flag will be set to ''2'' once a score has been sent to the server.
			case 
				when cCourseCT.CodeName = ''attendance'' and (cReg.ModifiedOn > vClassDMM.MaxStartDateTime or cReg.AttendedIndicator = 1) 
					then 1 
				when cCourseCT.CodeName = ''attendance'' 
					then 0 
				when cCourseCT.CodeName = ''onlinecourse'' and (ocr.Completion = 1 or ocr.Score > 0)
					then 1 
				when cCourseCT.CodeName = ''onlinecourse''
					then 0 
				when cReg.Score = '''' 
					then 0
				else 1
			end as ScoreReported,
			cCourse.OnlineCourseID as OnlineTestID,
			cClass.SurveyID as SurveyID,
			cCourse.PublicIndicator as PublicIndicator,
			-- Calculate the score
			--    -if the score is based on attendance, they get 100 if they attended
			--    -if the score is entered by the instructor, use that. Since this can be a letter or a number, we use nvarchar
			--    -if the score is based on taking an online course, use that score
			case 
				when cCourseCT.CodeName = ''attendance'' and cReg.AttendedIndicator = 1
					then ''100''
				when ocr.ID is null
					then cast(cReg.Score as nvarchar(50))
				else 
					cast(floor(round(ocr.Score,0)) as nvarchar(50)) 
			end as Score,
			null as AttemptDate,
			0 as AttemptTime,
			0 as AttemptCount,
			vClassDMM.MinStartDateTime as StartDate,
			vClassDMM.MaxStartDateTime as EndDate
		from         
			dbo.Registration as cReg 
		inner join
			dbo.[Status] as stat on cReg.RegistrationStatusID = stat.ID
		inner join
			 dbo.Class as cClass ON cClass.ID = cReg.ClassID 
		inner join
			dbo.Course as cCourse ON cCourse.ID = cClass.CourseID 
		inner join
			dbo.CourseCompletionType as cCourseCT ON cCourseCT.ID = cCourse.CourseCompletionTypeID 
		inner join
			dbo.ClassDateMinMax as vClassDMM ON vClassDMM.ClassID = cClass.ID 
		left join
			dbo.PassingScore as cPass ON cPass.CustomerID = cClass.CustomerID 
		--left join
		--	dbo.[User] as cUser ON cUser.ID = cReg.RegistrantID 
		-- this join simply grabs the score from the user if they had an online course to take for this class
		left join
			dbo.OnlineCourseRollup as ocr ON 
				ocr.CourseID = cCourse.OnlineCourseID AND 
				ocr.UserID = @userid
		where
			cReg.RegistrantID = @userid
	) as X
end







' 
END
GO
/****** Object:  View [dbo].[ResourceManagersClassDates]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ResourceManagersClassDates]'))
EXEC dbo.sp_executesql @statement = N'


/****** Object: View [dbo].[ResourceManagersClassDates] ******/
CREATE VIEW [dbo].[ResourceManagersClassDates]
AS
SELECT
cd.ID , cd.ClassID, cd.StartDateTime, cd.Duration, crm.ResourceManagerID, c.CourseID, c.CustomerID,
(select count(*) from Registration where ClassID = c.ID AND RegistrationStatusID = 4) as RegisteredUserCount,
(select count(*) from Registration where ClassID = c.ID AND (RegistrationStatusID = 1 OR RegistrationStatusID = 2)) as PendingUserCount
FROM
dbo.ClassDate AS cd
LEFT JOIN
dbo.Class AS c
ON
cd.ClassID = c.ID

LEFT JOIN
dbo.ClassResourceManagers AS crm
ON
crm.ClassID = c.ID
'
GO
/****** Object:  UserDefinedFunction [dbo].[fGetHierarchiesForUser]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetHierarchiesForUser]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'


CREATE function [dbo].[fGetHierarchiesForUser](@userId int)
returns @temp table(
		ID int, 
		ParentID int, 
		Name nvarchar(50),
		CustomerID int, 
		[Level] int,
		LevelIndentText nvarchar(500), 
		LevelText nvarchar(500),
		RootID int,
		TypeID int
	)
as
begin
	declare @jobrole int
	declare @location int
	set @jobrole = (select jobroleid from [user] where id = @userid);
	set @location = (select locationid from [user] where id = @userid);
	
	if(@jobrole != 0)
	begin
		insert @temp
		select
			ID, ParentJobRoleID, Name, CustomerID, [Level], [LevelIndentText],LevelText, RootJobRoleID, 2 as TypeID
		from
			dbo.fGetJobRoleHierarchyBranch(@jobrole)
	end
	
	if(@location != 0)
	begin
		insert @temp
		select
			ID, ParentLocationID, Name, CustomerID, [Level], [LevelIndentText],LevelText, RootLocationID, 1 as TypeID
		from
			dbo.fGetLocationHierarchyBranch(@location)
	end
	declare @audienceId int
	declare audiences_cursor cursor for
	select audienceid from UserAudience where userid = @userid
	
	open audiences_cursor
	fetch next from audiences_cursor into @audienceId
	
	while @@fetch_status = 0
	begin
		if(@audienceId != 0)
		begin
			insert @temp
			select
				ID, ParentAudienceID, Name, CustomerID, [Level], [LevelIndentText],LevelText, RootAudienceID, 3 as TypeID
			from
				dbo.fGetAudienceHierarchyBranch(@audienceId)	
		end
		fetch next from audiences_cursor into @audienceId
	end
	
	close audiences_cursor
	deallocate audiences_cursor
	return
end



' 
END
GO
/****** Object:  View [dbo].[AudienceHierarchy]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AudienceHierarchy]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[AudienceHierarchy] AS
select * from dbo.fGetAudienceHierarchyBranch(0)



/****** Object: View [dbo].[AudienceHierarchy] Script Date: 01/07/2008 09:35:39 ******/
'
GO
/****** Object:  UserDefinedFunction [dbo].[fGetHierarchyBranch]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fGetHierarchyBranch]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fGetHierarchyBranch] 
(	
	-- Add the parameters for the function here
	@HierarchyNodeID int,
	@HierarchyTypeID int
)
RETURNS @HTable TABLE
(ID int not null)
AS
BEGIN
	DECLARE @HierarchyTypeCodeName AS nvarchar(50)

	SELECT @HierarchyTypeCodeName = CodeName FROM HierarchyType WHERE ID = @HierarchyTypeID
	
	IF (@HierarchyTypeCodeName = ''location'')
	BEGIN
		INSERT @HTable SELECT ID FROM dbo.fGetLocationHierarchyBranch(@HierarchyNodeID)
	END
	IF (@HierarchyTypeCodeName = ''jobrole'')
	BEGIN
		INSERT @HTable SELECT ID FROM dbo.fGetJobRoleHierarchyBranch(@HierarchyNodeID)
	END
	IF (@HierarchyTypeCodeName = ''audience'')
	BEGIN
		INSERT @HTable SELECT ID FROM dbo.fGetAudienceHierarchyBranch(@HierarchyNodeID)
	END
	IF (@HierarchyTypeCodeName = ''user'')
	BEGIN
		INSERT @HTable SELECT  @HierarchyNodeID
	END
	RETURN
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetSalesChannelHierarchyBranch]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSalesChannelHierarchyBranch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSalesChannelHierarchyBranch]
	@iSalesChannelID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from [fGetSalesChannelHierarchyBranch](@iSalesChannelID)
END
' 
END
GO
/****** Object:  View [dbo].[SalesChannelHierarchy]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SalesChannelHierarchy]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[SalesChannelHierarchy] AS
	select * from [fGetSalesChannelHierarchyBranch](0)

/****** Object:  View [dbo].[SalesChannelCustomersHierarchy]    Script Date: 01/07/2008 09:34:11 ******/
'
GO
/****** Object:  View [dbo].[SimpleClassDetails]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SimpleClassDetails]'))
EXEC dbo.sp_executesql @statement = N'



create view [dbo].[SimpleClassDetails] as
	select
		cClass.ID as ID, 
		cCourse.ID as CourseID, 
		cClass.[Name] as ClassName, 
		cClass.[Description] as ClassDescription, 
		cCourse.[Name] as CourseName, 
		cVenue.[Name] as VenueName, 
		cRoom.[Name] as ClassRoomName, 
		dt.MinStartDateTime as MinClassDate
	from
		dbo.Class as cClass 
	inner join
		dbo.Course as cCourse 
	on 
		cCourse.ID = cClass.CourseID 
	left join
		dbo.Room as cRoom 
	on 
		cRoom.ID = cClass.RoomID 
	left join
		dbo.Venue as cVenue 
	on 
		cVenue.ID = cRoom.VenueID 
	inner join
		dbo.ClassMinStartDateTime as dt
	on 
		dt.ClassID = cClass.ID 

'
GO
/****** Object:  View [dbo].[TrainingSummary]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TrainingSummary]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[TrainingSummary]
AS
SELECT     c.CustomerID, c.ID AS ClassID, c.CourseID, COALESCE (co.Name, '''') AS CourseName, c.Name AS ClassName, v.City + '', '' + s.Name AS LocationName, 
                      r.Name AS RoomName, v.Name AS VenueName, v.City AS VenueCity, s.Name AS StateName, c.RoomID, r.VenueID, v.StateID, 
                      CASE WHEN vClassMSDT.MinStartDateTime IS NULL THEN ''Unscheduled'' WHEN vClassMSDT.MinStartDateTime > GETDATE() 
                      THEN ''Scheduled'' ELSE ''Held'' END AS Status, ISNULL(REPLACE(REPLACE
                          ((SELECT     CASE WHEN u.ID IS NULL THEN ''?'' ELSE u.LastName + '',+'' + u.FirstName END AS [data()]
                              FROM         dbo.ClassInstructors AS i LEFT JOIN
                                                    dbo.[User] AS u ON u.ID = i.InstructorID
                              WHERE     (i.ClassID = c.ID) FOR XML PATH('''')), '' '', '';''), ''+'', '' ''), '''') AS InstructorNames, ISNULL(REPLACE
                          ((SELECT     ''['' + CONVERT(NVARCHAR, i.InstructorID) + '']'' AS [data()]
                              FROM         dbo.ClassInstructors AS i
                              WHERE     (i.ClassID = c.ID) FOR XML PATH('''')), '' '', '',''), '''') AS InstructorIDs, COALESCE (vClassMSDT.MinStartDateTime, 0) AS StartDateTime, 
                      COALESCE (vClassUC.UserCount, 0) AS UserCount, cCustomer.Name AS CustomerName, 
                      CASE WHEN cCustomer.ParentCustomerID = 0 THEN ''BankersEdge'' ELSE cPCustomer.Name END AS HostCompany, COALESCE (tz.Description, '''') 
                      AS TimeZone
FROM         dbo.Class AS c LEFT OUTER JOIN
                      dbo.Course AS co ON co.ID = c.CourseID LEFT OUTER JOIN
                      dbo.TimeZone AS tz ON tz.ID = c.TimeZoneID LEFT OUTER JOIN
                      dbo.ClassMinStartDateTime AS vClassMSDT ON vClassMSDT.ClassID = c.ID LEFT OUTER JOIN
                      dbo.Room AS r ON r.ID = c.RoomID LEFT OUTER JOIN
                      dbo.Venue AS v ON v.ID = r.VenueID LEFT OUTER JOIN
                      dbo.States AS s ON s.ID = v.StateID LEFT OUTER JOIN
                      ClassUserCount AS vClassUC ON vClassUC.ClassID = c.ID LEFT JOIN
                      dbo.Customer AS cCustomer ON cCustomer.ID = c.CustomerID LEFT JOIN
                      dbo.Customer AS cPCustomer ON cPCustomer.ID = cCustomer.ParentCustomerID
'
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_SetProperties]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_SetProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Profile_SetProperties]
    @ApplicationName        nvarchar(256),
    @PropertyNames          ntext,
    @PropertyValuesString   ntext,
    @PropertyValuesBinary   image,
    @UserName               nvarchar(256),
    @IsUserAnonymous        bit,
    @CurrentTimeUtc         datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
       BEGIN TRANSACTION
       SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DECLARE @UserId uniqueidentifier
    DECLARE @LastActivityDate datetime
    SELECT  @UserId = NULL
    SELECT  @LastActivityDate = @CurrentTimeUtc

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, @IsUserAnonymous, @LastActivityDate, @UserId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Users
    SET    LastActivityDate=@CurrentTimeUtc
    WHERE  UserId = @UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS( SELECT *
               FROM   dbo.aspnet_Profile
               WHERE  UserId = @UserId))
        UPDATE dbo.aspnet_Profile
        SET    PropertyNames=@PropertyNames, PropertyValuesString = @PropertyValuesString,
               PropertyValuesBinary = @PropertyValuesBinary, LastUpdatedDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    ELSE
        INSERT INTO dbo.aspnet_Profile(UserId, PropertyNames, PropertyValuesString, PropertyValuesBinary, LastUpdatedDate)
             VALUES (@UserId, @PropertyNames, @PropertyValuesString, @PropertyValuesBinary, @CurrentTimeUtc)

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  View [dbo].[MyGTMMeetings]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[MyGTMMeetings]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[MyGTMMeetings]
AS
SELECT     
	vUTM.GTMMeetingID as ID, 
	vUTM.UserID, 
	cast(vUTM.IsOrganizer as bit) as IsOrganizer,  
	gtmM.[Subject], 
	gtmM.GTMOrganizerID, 
	gtmM.MeetingID, 
	gtmM.UniqueMeetingID, 
	gtmM.JoinURL, 
	gtmM.MaxParticipants,
	gtmM.MeetingType, 
	CASE WHEN LOWER(gtmM.MeetingType) = ''immediate'' THEN 1 ELSE 0 END AS StartNow, 
	gtmM.StartDateTime, 
	gtmM.EndDateTime, 
	gtmM.TimeZoneKey, 
	gtmM.PasswordRequired, 
	gtmM.ConferenceCallInfo, 
	cast(gtmM.IsEditable as bit) as IsEditable
FROM         
	dbo.UserToGTMMeeting AS vUTM 
INNER JOIN
     dbo.GTMMeeting AS gtmM ON gtmM.ID = vUTM.GTMMeetingID



'
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_CreateRole]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_CreateRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_Roles_CreateRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId))
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    INSERT INTO dbo.aspnet_Roles
                (ApplicationId, RoleName, LoweredRoleName)
         VALUES (@ApplicationId, @RoleName, LOWER(@RoleName))

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_SetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CurrentTimeUtc, @UserId OUTPUT
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationPerUser WHERE UserId = @UserId AND PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationPerUser SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE UserId = @UserId AND PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationPerUser(UserId, PathId, PageSettings, LastUpdatedDate) VALUES (@UserId, @PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationAllUsers SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationAllUsers(PathId, PageSettings, LastUpdatedDate) VALUES (@PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END
' 
END
GO
/****** Object:  View [dbo].[AllCoursesByTrainingProgram]    Script Date: 08/05/2010 15:57:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AllCoursesByTrainingProgram]'))
EXEC dbo.sp_executesql @statement = N'


CREATE view [dbo].[AllCoursesByTrainingProgram] as
select
	ac.[Name], 
	ac.CourseID, 
	ac.CourseTypeID, 
	cast(ac.[CourseID] as nvarchar(max)) + ''-'' + cast(ac.CourseTypeID as nvarchar(max)) as ID,
	tp.SyllabusTypeID,
	tp.TrainingProgramID,
	RequiredSyllabusOrder
from
	TrainingProgramToCourseLink tp
join
	AllCourses ac
on
	tp.CourseID = ac.CourseID
and
	tp.CourseTypeID = ac.CourseTypeID
'
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE PathId = @PathId AND UserId = @UserId
    RETURN 0
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_GetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationPerUser p WHERE p.PathId = @PathId AND p.UserId = @UserId
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    DELETE FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId
    RETURN 0
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]    Script Date: 08/05/2010 15:57:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationAllUsers p WHERE p.PathId = @PathId
END
' 
END
GO
/****** Object:  View [dbo].[TrainingProgramCourses]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TrainingProgramCourses]'))
EXEC dbo.sp_executesql @statement = N'


CREATE view [dbo].[TrainingProgramCourses] as
-- this query simply collapses out the individual classes from the [TrainingProgramCoursesAndClasses] view
-- so we get a list of only the actual courses
select
	ROW_NUMBER() over (order by TrainingProgramID, SortOrder) as ID,
	CustomerID,
	TrainingProgramID,
	TrainingProgramName,
	SyllabusTypeID,
	ProgramStartDate,
	ProgramEndDate,
	CourseID,
	CourseTypeID,
	CourseName,
	EnforceRequiredSortOrder,
	SortOrder
from
	[TrainingProgramCoursesAndClasses]
group by
	CustomerID,
	TrainingProgramID,
	TrainingProgramName,
	SyllabusTypeID,
	ProgramStartDate,
	ProgramEndDate,
	CourseID,
	CourseTypeID,
	CourseName,
	EnforceRequiredSortOrder,
	SortOrder
'
GO
/****** Object:  View [dbo].[LocationHierarchy]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LocationHierarchy]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[LocationHierarchy] AS
select * from [fGetLocationHierarchyBranch](0)

/****** Object:  View [dbo].[JobRoleHierarchy]    Script Date: 01/07/2008 09:35:39 ******/
'
GO
/****** Object:  View [dbo].[StudentTranscript]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[StudentTranscript]'))
EXEC dbo.sp_executesql @statement = N'


CREATE view [dbo].[StudentTranscript] as
select 
	ROW_NUMBER() over (order by AttemptDate) as ID,
	*
from
(

	select
		CustomerID,	
		UserID,		
		''online'' as CourseType,
		2 as CourseTypeID,
		0 as ClassID,
		0 as RegistrationID,
		'''' as RegistrationStatus,
		CourseID,
		CourseName,
		cast(Score as nvarchar(max)) as Score,
		AttemptDate,
		AttemptTime,
		AttemptCount,
		case 
			when Passed is null then ''Unknown''
			when Passed = 0 then ''Failed''
			when Passed = 1 then ''Passed''
		end
		as PassOrFail,
		Passed,
		case 
			when Completed is null then ''Unknown''
			when Completed = 0 then ''Incomplete''
			when Completed = 1 then ''Completed''
		end
		as CompleteOrIncomplete,
		Completed,
		-- online courses don''t have start/end dates or online test ids
		0 as OnlineTestID,
		0 as HasStarted,
		0 as HasEnded,
		null as MinClassDate,
		null as MaxClassDate,
		0 as SurveyID,
		--CourseSourceType,
		PublicIndicator
	from
		OnlineTranscript

	union

	select
		CustomerID,	
		UserID,		
		''classroom'' as CourseType,
		1 as CourseTypeID,
		ClassID,
		RegistrationID,
		RegistrationStatus,
		CourseID,
		CourseName,
		cast(Score as nvarchar(max)) as Score,
		MinClassDate as AttemptDate,
		''-'' as AttemptTime,
		''-'' as AttemptCount,
		case 
			when Passed = 0 and ScoreReported = 1 then ''Failed''
			when Passed = 1 and ScoreReported = 1 then ''Passed''
			else ''Unknown''
		end
		as PassOrFail,
		Passed,
		case ScoreReported
			when 1 then ''Complete''
			else ''Incomplete''
		end
		as CompleteOrIncomplete,
		ScoreReported as Completed,
		OnlineCourseID as OnlineTestID,
		HasStarted,
		HasEnded,
		MinClassDate,
		MaxClassDate,
		SurveyID,
		--CourseSourceType,
		PublicIndicator
	from
		ClassroomTranscript
) as Transcript
'
GO
/****** Object:  View [dbo].[SimpleUpcomingEvents]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SimpleUpcomingEvents]'))
EXEC dbo.sp_executesql @statement = N'


CREATE view [dbo].[SimpleUpcomingEvents] 
as
	-- get the classroom classes
	select
		c.ID as EventID,
		course.ID as CourseID,
		isnull(p.[Description],''Class'') as EventType,
		c.Name as EventName,
		reg.RegistrantID as UserID,
		StartDateTime as StartDate,
		DATEADD(hh, Duration, StartDateTime) as EndDate
	from
		Registration reg
	join
		Class c
	on
		reg.ClassID = c.ID
	join
		Course course
	on
		course.ID = c.CourseID
	left join
		PresentationType p
	on
		course.PresentationTypeID = p.ID
	-- we can show multiple items in the upcoming events because
	-- each class is...well, upcoming
	join
		ClassDate cd
	on
		cd.ClassID = c.ID
	-- only show classes for which they''re actually registered
	where
		RegistrationStatusID = 4

	union
	
	-- get the meetings
	select
		m.ID as EventID, 
		0 as CourseID, 
		''meeting'' as EventType,
		m.[Subject] as EventName, 
		m.UserID, 
		m.StartDateTime as StartDate, 
		m.EndDateTime as EndDate
	from
		MyGTMMeetings m


'
GO
/****** Object:  View [dbo].[TrainingProgramUsers]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[TrainingProgramUsers]'))
EXEC dbo.sp_executesql @statement = N'


/* ExecuteTransaction() */ CREATE view [dbo].[TrainingProgramUsers] as
-- this view is going to get us a list of all the users in a given training program

SELECT     
	ROW_NUMBER() OVER (ORDER BY TrainingProgramID) AS ID, 
	CustomerID, 
	TrainingProgramID, 
	TrainingProgramName, 
	TrainingProgramStartDate, 
	TrainingProgramDueDate, 
	TrainingProgramEndDate, 
	TrainingProgramDescription, 
	EnforceRequiredOrder, 
	IsNewHire, 
	SourceNodeID, 
	HierarchyTypeID, 
	FirstName, 
	LastName, 
	UserID,
    ((SELECT     COUNT(1)
        FROM         TrainingProgramToCourseLink
        WHERE     T .TrainingProgramID = TrainingProgramID AND SyllabusTypeID IN (1, 4)) + MinimumElectives) 
    AS MinimumCourseCount, 
    MinimumElectives
FROM         
	(SELECT     
	program.CustomerID AS CustomerID, 
	program.ID AS TrainingProgramID, 
	program.[Name] AS TrainingProgramName, 
    case 


when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0)
		then
			program.StartDate 
		else 



cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, ''1/1/1900'') as datetime)
		end as TrainingProgramStartDate,	
	case 


when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0) 
		then
			program.EndDate 
		else 



dateadd(d,customer.NewHireDuration, cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, ''1/1/1900'') as datetime))
		end as TrainingProgramEndDate,
	case 


when (program.IsNewHire = 0) or (coalesce(locationUsers.NewHireIndicator, jobroleUsers.NewHireIndicator, audienceUsers.NewHireIndicator, users.NewHireIndicator, 0) = 0) 
		then
			program.DueDate 
		else



dateadd(d,customer.NewHireDuration, cast(coalesce(locationUsers.HireDate, jobroleUsers.HireDate, audienceUsers.HireDate, users.HireDate, ''1/1/1900'') as datetime))
		end as TrainingProgramDueDate, 
		program.Description AS TrainingProgramDescription, 
                                              
		program.MinimumElectives AS MinimumElectives, 
		program.EnforceRequiredOrder AS EnforceRequiredOrder, 
		program.IsNewHire AS IsNewHire,
		FullList.ID AS SourceNodeID, 
		link.HierarchyTypeID, 


COALESCE (locationUsers.FirstName, jobroleUsers.FirstName, audienceUsers.FirstName, users.FirstName, NULL) AS FirstName, 


COALESCE (locationUsers.LastName, jobroleUsers.LastName, audienceUsers.LastName, users.LastName, NULL) AS LastName, 
		COALESCE (locationUsers.ID, jobroleUsers.ID, audienceUsers.ID, users.ID, NULL) AS UserID
FROM
	TrainingProgram program 
JOIN
	dbo.HierarchyToTrainingProgramLink link 
ON 
	link.TrainingProgramID = program.ID 
CROSS apply 
	dbo.fGetHierarchyBranch(link.HierarchyNodeID, link.HierarchyTypeID) AS FullList 
LEFT JOIN
      dbo.[User] locationUsers ON FullList.ID = locationUsers.LocationID AND HierarchyTypeID = 1
LEFT JOIN
      dbo.[User] jobroleUsers ON FullList.ID = jobroleUsers.JobRoleID AND HierarchyTypeID = 2 
LEFT JOIN
      dbo.UserAudience ua ON FullList.ID = ua.AudienceID AND HierarchyTypeID = 3 
LEFT JOIN
      dbo.[User] audienceUsers ON ua.UserID = audienceUsers.ID 
LEFT JOIN
      dbo.[User] users ON users.ID = FullList.ID AND link.HierarchyTypeID = 4
left join
	dbo.Customer customer
on
	program.CustomerID = customer.ID
WHERE      

COALESCE (locationUsers.ID, jobroleUsers.ID, audienceUsers.ID, users.ID, NULL) IS NOT NULL AND program.IsLive = 1) 
AS T
-- where 
--		(	
--			-- all the single-hierarchy TP users
--			(	select 
--				count(httpl.ID) 
--				from HierarchyToTrainingProgramLink httpl 
--				where httpl.TrainingProgramID = T.TrainingProgramID 
--			)  = 1 
--			-- all the HierarchyTypes of User
--			 OR
--			
--				T.HierarchyTypeID = 4
--			
--			-- the first instance of each user in both hierarchies of a TP
--			--OR
--			--(
--			--	select Count(tpui.UserID) as UserIDCount 
--			--						from TrainingProgramUsersInclusive tpui 
--			---						where tpui.TrainingProgramID = T.TrainingProgramID
--			--						and tpui.userID = T.UserID
--			--) = 2
--		)
;
'
GO
/****** Object:  View [dbo].[HierarchyToTrainingProgramLinkExpanded]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[HierarchyToTrainingProgramLinkExpanded]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[HierarchyToTrainingProgramLinkExpanded]
AS
SELECT     HierTTPL.ID, HierTTPL.TrainingProgramID,HierTTPL.IsPrimary, fLocation.ID AS HierarchyNodeID, HierTTPL.HierarchyTypeID, HierTTPL.ModifiedBy, HierTTPL.CreatedBy, 
                      HierTTPL.ModifiedOn, HierTTPL.CreatedOn
FROM         dbo.HierarchyToTrainingProgramLink AS HierTTPL CROSS APPLY fGetHierarchyBranch(HierTTPL.HierarchyNodeID, HierTTPL.HierarchyTypeID) 
                      AS fLocation
'
GO
/****** Object:  StoredProcedure [dbo].[GetTrainingProgramsForUser]    Script Date: 08/05/2010 15:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTrainingProgramsForUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE procedure [dbo].[GetTrainingProgramsForUser](@userId int, @newHire bit, @searchText nvarchar(50), @orderBy nvarchar(50), @orderDir varchar(4), @pageIndex int, @pageSize int)
as
begin
	set @orderDir = lower(@orderDir);
	set @orderBy = lower(@orderBy);
	-- create a date time that represents today without the time
	declare @today datetime
	set @today = (select cast(floor(cast(getdate() as decimal(12, 5))) as datetime));
	declare @hireDate datetime
	set @hireDate = (select HireDate from [user] where id = @userId);
	declare @newHireDuration int
	set @newHireDuration = (select NewHireDuration from Customer where ID = (select CustomerID from [user] where id = @userId));
	
	declare @newHireStartDate datetime
	declare @newHireEndDate datetime
	
	-- only set these if new hire is set, so they remain null and we can coalesce in the where clause below otherwise
	if(@newHire = 1)
	begin
		set @newHireStartDate = @hireDate;
		set @newHireEndDate = dateadd(d, @newHireDuration, @hireDate);
	end;
			
	-- get all the tp details
with temp as(
	select
		tp.*,
		T.RequiredCourseCount,
		Row
	from
		TrainingProgram tp
	join
		(
			-- make sure we only ever see 1 instance of a given tp
			select
				distinct(X.TrainingProgramID) as TrainingProgramID, 
				ROW_NUMBER() over (order by X.TrainingProgramID) as Row, 
				RequiredCourseCount
			from
				(
					-- get the assigned tps
					select
						l.TrainingProgramID, tp.Name, (count(distinct cl.ID) + tp.MinimumElectives) as RequiredCourseCount
					from
						dbo.HierarchyToTrainingProgramLink l
					left join
						dbo.fGetHierarchiesForUser(@userId) h
					on
						(
						l.[HierarchyNodeID] = h.ID
						and
						l.[HierarchyTypeID] = h.TypeID
						)
					join
						dbo.TrainingProgramToCourseLink cl
					on
						l.TrainingProgramID = cl.TrainingProgramID
					join
						dbo.TrainingProgram tp
					on
						l.TrainingProgramID = tp.ID
					where
						(
							(l.[HierarchyNodeID] = @userId and l.[HierarchyTypeID] = 4) 
							or
							(l.[HierarchyNodeID] = h.ID	and	l.[HierarchyTypeID] = h.TypeID)
						)
						and cl.SyllabusTypeID in (1,4)
						and tp.IsLive = 1
					group by
						l.trainingprogramid, tp.name, tp.MinimumElectives
	
						
				) as X
			where
				X.Name like ''%'' + @searchText + ''%''
			group by
				X.TrainingProgramID, RequiredCourseCount
		)
	as T
	on
		tp.ID = T.TrainingProgramID
	
)
select
	ID, CustomerID, OwnerID, Name, InternalCode, Cost, Description, IsNewHire, IsLive, 
	coalesce(@newHireStartDate, StartDate) as StartDate,
	coalesce(@newHireEndDate, EndDate) as EndDate,
	-- new hire end date is the due date
	coalesce(@newHireEndDate, DueDate) as DueDate,
	EnforceRequiredOrder, MinimumElectives, FinalAssessmentCourseID, 
	FinalAssessmentCourseTypeID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn,
	RequiredCourseCount,
	(select max(row) from temp) as TotalSize
from
	temp
where
	row between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize + 1)
and
	IsNewHire = @newHire
and
	-- end date is in the future, start date is in the past
	(coalesce(@newHireEndDate, EndDate) >= @today and coalesce(@newHireStartDate, StartDate) <= @today)
group by
	ID, CustomerID, OwnerID, Name, InternalCode, Cost, Description, IsNewHire, IsLive, StartDate, EndDate, DueDate, EnforceRequiredOrder, MinimumElectives, FinalAssessmentCourseID, FinalAssessmentCourseTypeID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn,
	RequiredCourseCount
order by
	case when @orderBy = ''Name'' and @orderDir = ''asc'' then Name end,
	case when @orderBy = ''Name'' and @orderDir = ''desc'' then Name end desc,
	case when @orderBy = ''Description'' and @orderDir = ''asc'' then [Description] end,
	case when @orderBy = ''Description'' and @orderDir = ''desc'' then [Description] end desc,
	case when @orderBy = ''StartDate'' and @orderDir = ''asc'' then StartDate end,
	case when @orderBy = ''StartDate'' and @orderDir = ''desc'' then StartDate end desc,
	case when @orderBy = ''EndDate'' and @orderDir = ''asc'' then EndDate end,
	case when @orderBy = ''EndDate'' and @orderDir = ''desc'' then EndDate end desc,
	case when @orderBy = ''DueDate'' and @orderDir = ''asc'' then DueDate end,
	case when @orderBy = ''DueDate'' and @orderDir = ''desc'' then DueDate end desc,
	case when 1 = 1 then StartDate end
	
end


' 
END
GO
/****** Object:  View [dbo].[SalesChannelCustomersHierarchy]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SalesChannelCustomersHierarchy]'))
EXEC dbo.sp_executesql @statement = N'


/****** Object:  View [dbo].[LocationHierarchy]    Script Date: 01/07/2008 09:35:02 ******/
CREATE VIEW [dbo].[SalesChannelCustomersHierarchy]
AS
SELECT     sc.RootSalesChannelID, sc.ParentSalesChannelID, c.ID, c.SalesChannelID, c.Name, c.ParentCustomerID, c.EvaluationIndicator, c.EvaluationEndDate, 
                      c.SuspendedIndicator, c.UserLimit, c.SelfRegistrationIndicator, c.StrongPasswordLength, c.PasswordMaskID, c.LockoutCount, 
					  c.QuickQueryIndicator, c.ReportBuilderIndicator, 
					  c.UserNameAlias, c.LocationAlias, c.JobRoleAlias, c.AudienceAlias, 
					  c.ModifiedBy, c.CreatedBy, c.ModifiedOn, c.CreatedOn
FROM         dbo.SalesChannelHierarchy AS sc INNER JOIN
                      dbo.Customer AS c ON sc.ID = c.SalesChannelID
'
GO
/****** Object:  View [dbo].[StudentTranscriptLatestAttempts]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[StudentTranscriptLatestAttempts]'))
EXEC dbo.sp_executesql @statement = N'


CREATE view [dbo].[StudentTranscriptLatestAttempts] AS
select 
	ROW_NUMBER() over (order by AttemptDate) as ID,
	*
from
(
	SELECT 
		st1.CustomerID
		,st1.UserID
		,st1.CourseID
		,st1.CourseType
		,st1.CourseTypeID
		,st1.ClassID
		,st1.RegistrationID
		,st1.RegistrationStatus
		,st1.CourseName
		,st1.Score
		,st1.AttemptDate
		,st1.AttemptTime
		,st1.AttemptCount
		,st1.PassOrFail
		,st1.Passed
		,st1.CompleteOrIncomplete
		,st1.Completed
		,st1.OnlineTestID
		,st1.HasStarted
		,st1.HasEnded
		,st1.MinClassDate
		,st1.MaxClassDate
		, Cast(st1.CourseID AS VARCHAR(MAX)) + ''-'' + 
				CAST(st1.CourseTypeID AS VARCHAR(MAX)) as ConcatID
		
		,st1.AttemptDate AS maxdate
	FROM dbo.StudentTranscript AS st1
	where st1.attemptDate = 
		(select max(attemptdate) from StudentTranscript
			where userid = st1.userid 
			and courseid = st1.courseid
			and CourseType = st1.CourseType)	
)
AS T
'
GO
/****** Object:  View [dbo].[PublicCourses]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[PublicCourses]'))
EXEC dbo.sp_executesql @statement = N'


CREATE VIEW [dbo].[PublicCourses]
AS
SELECT
	*,
	ROW_NUMBER() OVER (ORDER BY CourseName, [Type]) AS ID
from
(
select
	t.CourseID, 
	t.CourseTypeID,
	t.CourseName, 
	t.CourseDescription, 
	t.[Type], 
	t.UserID, 
	t.ClassID,
	t.RegistrationID, 
	t.ClassName, 
	t.ClassRoomName, 
	t.ClassDates, 
	t.ClassInstructors, 
	t.ClassDescription, 
	t.[Status], 
	t.CourseName + '' '' + t.CourseDescription + '' '' + t.ClassName + '' '' + t.ClassRoomName + '' '' + t.ClassInstructors + '' '' + t.ClassDescription + '' '' + t.Keywords AS SearchText,
	t.SurveyID,
	stla.PassOrFail,
	stla.Passed,
	stla.CompleteOrIncomplete,
	stla.Completed,
	stla.Score,
	stla.OnlineTestID,
	stla.HasStarted,
	stla.HasEnded,
	stla.AttemptCount,
	stla.AttemptDate
	
FROM         
	(
		SELECT     
			cc.ID AS CourseID, 
			cc.Name AS CourseName, 
			cc.Description AS CourseDescription, 
			1 AS CourseTypeID,
			''Classroom'' AS Type, 
			u.ID AS UserID, 
			ISNULL(crc.RegistrationID, 0) AS RegistrationID, 
			crc.ClassID,
			ISNULL(crc.ClassName, '''') AS ClassName, 
			ISNULL(crc.ClassRoomName, '''') AS ClassRoomName, 
			ISNULL(crc.ClassDates, '''') AS ClassDates, 
			ISNULL(crc.ClassInstructors, '''') AS ClassInstructors, 
			ISNULL(crc.ClassDescription, '''') AS ClassDescription, 
			'''' as Keywords,
			CASE WHEN crc.RegistrationID IS NULL 
				THEN ''Available'' 
			WHEN crc.ScoreReported = 1 AND crc.Passed = 1 
				THEN ''Passed'' 
			WHEN crc.ScoreReported = 1 AND crc.Passed = 0 
				THEN ''Failed'' 
			WHEN GETDATE() > crc.MaxClassDate 
				THEN ''Pending'' 
			WHEN crc.Attended = 1 
				THEN ''Attended'' 
			ELSE crc.Status END AS Status,
			cc.SurveyID
		FROM  
			dbo.Course AS cc 
			INNER JOIN dbo.[User] AS u ON u.CustomerID = cc.CustomerID 
			LEFT OUTER JOIN dbo.CoursesRegisteredClassroom AS crc ON crc.CourseID = cc.ID AND crc.UserID = u.ID
		WHERE   
			(
				cc.PublicIndicator = 1 AND cc.ID IN
				(
					SELECT CourseID
					FROM   CourseClasses
				)
			)
		UNION
		SELECT     
			oc.ID AS CourseID, 
			oc.Name AS CourseName, 
			oc.Description AS CourseDescription, 
			2 AS CourseTypeID,
			''Online'' AS Type, 
			u.ID AS UserID, 
			ISNULL(cro.RegistrationID, 0) AS RegistrationID, 
			0 as ClassID,
			'''' AS ClassName, 
			'''' AS ClassRoomName, 
			'''' AS ClassDates, 
			'''' AS ClassInstructors, 
			'''' AS ClassDescription, 
			Keywords as Keywords,
			CASE WHEN cro.RegistrationID IS NULL 
				THEN ''Available'' 
			WHEN cro.Completed = 2 AND cro.Passed = 1 
				THEN ''Passed'' 
			WHEN cro.Completed = 2 AND cro.Passed = 0 
				THEN ''Failed'' 
			WHEN cro.Completed = 1
				THEN ''In Progress''
			ELSE ''Registered'' END AS [Status],
			0 as SurveyID
		FROM         
			dbo.OnlineCourse AS oc 
			INNER JOIN dbo.[User] AS u ON u.CustomerID = oc.CustomerID 
			LEFT OUTER JOIN dbo.CoursesRegisteredOnline AS cro ON cro.CourseID = oc.ID AND cro.UserID = u.ID
		WHERE     (oc.PublicIndicator = 1)
	) 
AS T 
LEFT JOIN dbo.StudentTranscriptLatestAttempts AS stla 
	ON (t.CourseID = stla.CourseID AND t.CourseTypeId = stla.CourseTypeID AND t.userid = stla.userid)

) as S
'
GO
/****** Object:  View [dbo].[StudentCourseList]    Script Date: 08/05/2010 15:57:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[StudentCourseList]'))
EXEC dbo.sp_executesql @statement = N'


CREATE view [dbo].[StudentCourseList] as
-- Our two views "TrainingProgramUsers" and "TrainingProgramCourses" figure out which users are assigned a training
-- program, and which courses fit in a training program. This view simply combines those two views to create
-- a master set of which users get which courses.

select
	users.FirstName, 
	users.LastName,
	users.UserID,
	users.TrainingProgramID,
	users.TrainingProgramName,
	courses.CustomerID,
	courses.SyllabusTypeID,
	courses.ProgramStartDate,
	courses.ProgramEndDate,
	courses.CourseID,
	courses.CourseTypeID,
	courses.CourseName,
	courses.EnforceRequiredSortOrder,
	courses.SortOrder
from
	TrainingProgramUsers users
join
	TrainingProgramCourses courses
on
	users.TrainingProgramID = courses.TrainingProgramID
'
GO
/****** Object:  Check [CK_CoursePrerequisites_EnforceUniqueCourses]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_CoursePrerequisites_EnforceUniqueCourses]') AND parent_object_id = OBJECT_ID(N'[dbo].[CoursePrerequisites]'))
ALTER TABLE [dbo].[CoursePrerequisites]  WITH CHECK ADD  CONSTRAINT [CK_CoursePrerequisites_EnforceUniqueCourses] CHECK  (([CourseID]<>[CoursePrerequisiteID]))
GO
ALTER TABLE [dbo].[CoursePrerequisites] CHECK CONSTRAINT [CK_CoursePrerequisites_EnforceUniqueCourses]
GO
/****** Object:  ForeignKey [FK__aspnet_Me__Appli__47A6A41B]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__47A6A41B]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Me__Appli__47A6A41B] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[aspnet_Membership] CHECK CONSTRAINT [FK__aspnet_Me__Appli__47A6A41B]
GO
/****** Object:  ForeignKey [FK__aspnet_Pa__Appli__55F4C372]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pa__Appli__55F4C372]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
ALTER TABLE [dbo].[aspnet_Paths]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pa__Appli__55F4C372] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[aspnet_Paths] CHECK CONSTRAINT [FK__aspnet_Pa__Appli__55F4C372]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__4E53A1AA]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__4E53A1AA]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]'))
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pe__PathI__4E53A1AA] FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers] CHECK CONSTRAINT [FK__aspnet_Pe__PathI__4E53A1AA]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__498EEC8D]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__498EEC8D]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pe__PathI__498EEC8D] FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] CHECK CONSTRAINT [FK__aspnet_Pe__PathI__498EEC8D]
GO
/****** Object:  ForeignKey [FK__aspnet_Ro__Appli__55009F39]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Ro__Appli__55009F39]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
ALTER TABLE [dbo].[aspnet_Roles]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Ro__Appli__55009F39] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[aspnet_Roles] CHECK CONSTRAINT [FK__aspnet_Ro__Appli__55009F39]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__Appli__56E8E7AB]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__56E8E7AB]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Us__Appli__56E8E7AB] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
ALTER TABLE [dbo].[aspnet_Users] CHECK CONSTRAINT [FK__aspnet_Us__Appli__56E8E7AB]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__RoleI__4C6B5938]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__RoleI__4C6B5938]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Us__RoleI__4C6B5938] FOREIGN KEY([RoleId])
REFERENCES [dbo].[aspnet_Roles] ([RoleId])
GO
ALTER TABLE [dbo].[aspnet_UsersInRoles] CHECK CONSTRAINT [FK__aspnet_Us__RoleI__4C6B5938]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__UserI__4D5F7D71]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__UserI__4D5F7D71]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Us__UserI__4D5F7D71] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[aspnet_UsersInRoles] CHECK CONSTRAINT [FK__aspnet_Us__UserI__4D5F7D71]
GO
/****** Object:  ForeignKey [FK_Audience_Audience]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Audience_Audience]') AND parent_object_id = OBJECT_ID(N'[dbo].[Audience]'))
ALTER TABLE [dbo].[Audience]  WITH NOCHECK ADD  CONSTRAINT [FK_Audience_Audience] FOREIGN KEY([ParentAudienceID])
REFERENCES [dbo].[Audience] ([ID])
GO
ALTER TABLE [dbo].[Audience] NOCHECK CONSTRAINT [FK_Audience_Audience]
GO
/****** Object:  ForeignKey [FK_Class_Course]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Class_Course]') AND parent_object_id = OBJECT_ID(N'[dbo].[Class]'))
ALTER TABLE [dbo].[Class]  WITH NOCHECK ADD  CONSTRAINT [FK_Class_Course] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([ID])
GO
ALTER TABLE [dbo].[Class] NOCHECK CONSTRAINT [FK_Class_Course]
GO
/****** Object:  ForeignKey [FK_Class_Room]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Class_Room]') AND parent_object_id = OBJECT_ID(N'[dbo].[Class]'))
ALTER TABLE [dbo].[Class]  WITH NOCHECK ADD  CONSTRAINT [FK_Class_Room] FOREIGN KEY([RoomID])
REFERENCES [dbo].[Room] ([ID])
GO
ALTER TABLE [dbo].[Class] NOCHECK CONSTRAINT [FK_Class_Room]
GO
/****** Object:  ForeignKey [FK_Class_TimeZone]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Class_TimeZone]') AND parent_object_id = OBJECT_ID(N'[dbo].[Class]'))
ALTER TABLE [dbo].[Class]  WITH NOCHECK ADD  CONSTRAINT [FK_Class_TimeZone] FOREIGN KEY([TimeZoneID])
REFERENCES [dbo].[TimeZone] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[Class] NOCHECK CONSTRAINT [FK_Class_TimeZone]
GO
/****** Object:  ForeignKey [FK_ClassDate_Class]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClassDate_Class]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClassDate]'))
ALTER TABLE [dbo].[ClassDate]  WITH NOCHECK ADD  CONSTRAINT [FK_ClassDate_Class] FOREIGN KEY([ClassID])
REFERENCES [dbo].[Class] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[ClassDate] NOCHECK CONSTRAINT [FK_ClassDate_Class]
GO
/****** Object:  ForeignKey [FK_ClassInstructors_Class]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClassInstructors_Class]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClassInstructors]'))
ALTER TABLE [dbo].[ClassInstructors]  WITH NOCHECK ADD  CONSTRAINT [FK_ClassInstructors_Class] FOREIGN KEY([ClassID])
REFERENCES [dbo].[Class] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[ClassInstructors] NOCHECK CONSTRAINT [FK_ClassInstructors_Class]
GO
/****** Object:  ForeignKey [FK_ClassResources_Class]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ClassResources_Class]') AND parent_object_id = OBJECT_ID(N'[dbo].[ClassResources]'))
ALTER TABLE [dbo].[ClassResources]  WITH NOCHECK ADD  CONSTRAINT [FK_ClassResources_Class] FOREIGN KEY([ClassID])
REFERENCES [dbo].[Class] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[ClassResources] NOCHECK CONSTRAINT [FK_ClassResources_Class]
GO
/****** Object:  ForeignKey [FK_Course_CourseCompletionTypeParameter]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Course_CourseCompletionTypeParameter]') AND parent_object_id = OBJECT_ID(N'[dbo].[Course]'))
ALTER TABLE [dbo].[Course]  WITH NOCHECK ADD  CONSTRAINT [FK_Course_CourseCompletionTypeParameter] FOREIGN KEY([CourseCompletionTypeParameterID])
REFERENCES [dbo].[CourseCompletionTypeParameter] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[Course] NOCHECK CONSTRAINT [FK_Course_CourseCompletionTypeParameter]
GO
/****** Object:  ForeignKey [FK_Course_CourseDurationUnit]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Course_CourseDurationUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[Course]'))
ALTER TABLE [dbo].[Course]  WITH NOCHECK ADD  CONSTRAINT [FK_Course_CourseDurationUnit] FOREIGN KEY([CourseDurationUnitID])
REFERENCES [dbo].[CourseDurationUnit] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[Course] NOCHECK CONSTRAINT [FK_Course_CourseDurationUnit]
GO
/****** Object:  ForeignKey [FK_Course_CourseSurveys]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Course_CourseSurveys]') AND parent_object_id = OBJECT_ID(N'[dbo].[Course]'))
ALTER TABLE [dbo].[Course]  WITH NOCHECK ADD  CONSTRAINT [FK_Course_CourseSurveys] FOREIGN KEY([SurveyID])
REFERENCES [dbo].[CourseSurveys] ([ID])
GO
ALTER TABLE [dbo].[Course] NOCHECK CONSTRAINT [FK_Course_CourseSurveys]
GO
/****** Object:  ForeignKey [FK_Course_PresentationType]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Course_PresentationType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Course]'))
ALTER TABLE [dbo].[Course]  WITH NOCHECK ADD  CONSTRAINT [FK_Course_PresentationType] FOREIGN KEY([PresentationTypeID])
REFERENCES [dbo].[PresentationType] ([ID])
GO
ALTER TABLE [dbo].[Course] NOCHECK CONSTRAINT [FK_Course_PresentationType]
GO
/****** Object:  ForeignKey [FK_CoursePrerequisites_Course]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CoursePrerequisites_Course]') AND parent_object_id = OBJECT_ID(N'[dbo].[CoursePrerequisites]'))
ALTER TABLE [dbo].[CoursePrerequisites]  WITH NOCHECK ADD  CONSTRAINT [FK_CoursePrerequisites_Course] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([ID])
GO
ALTER TABLE [dbo].[CoursePrerequisites] NOCHECK CONSTRAINT [FK_CoursePrerequisites_Course]
GO
/****** Object:  ForeignKey [FK_CoursePrerequisites_Course1]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CoursePrerequisites_Course1]') AND parent_object_id = OBJECT_ID(N'[dbo].[CoursePrerequisites]'))
ALTER TABLE [dbo].[CoursePrerequisites]  WITH NOCHECK ADD  CONSTRAINT [FK_CoursePrerequisites_Course1] FOREIGN KEY([CoursePrerequisiteID])
REFERENCES [dbo].[Course] ([ID])
GO
ALTER TABLE [dbo].[CoursePrerequisites] NOCHECK CONSTRAINT [FK_CoursePrerequisites_Course1]
GO
/****** Object:  ForeignKey [FK_Customer_Customer]    Script Date: 08/05/2010 15:57:00 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Customer_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Customer]'))
ALTER TABLE [dbo].[Customer]  WITH NOCHECK ADD  CONSTRAINT [FK_Customer_Customer] FOREIGN KEY([ParentCustomerID])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Customer] NOCHECK CONSTRAINT [FK_Customer_Customer]
GO
/****** Object:  ForeignKey [FK_Import_Customer]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Import_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Import]'))
ALTER TABLE [dbo].[Import]  WITH CHECK ADD  CONSTRAINT [FK_Import_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Import] CHECK CONSTRAINT [FK_Import_Customer]
GO
/****** Object:  ForeignKey [FK_JobRole_Customer]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_JobRole_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[JobRole]'))
ALTER TABLE [dbo].[JobRole]  WITH NOCHECK ADD  CONSTRAINT [FK_JobRole_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[JobRole] NOCHECK CONSTRAINT [FK_JobRole_Customer]
GO
/****** Object:  ForeignKey [FK_JobRole_JobRole]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_JobRole_JobRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[JobRole]'))
ALTER TABLE [dbo].[JobRole]  WITH NOCHECK ADD  CONSTRAINT [FK_JobRole_JobRole] FOREIGN KEY([ParentJobRoleID])
REFERENCES [dbo].[JobRole] ([ID])
GO
ALTER TABLE [dbo].[JobRole] NOCHECK CONSTRAINT [FK_JobRole_JobRole]
GO
/****** Object:  ForeignKey [FK_KeywordCourse_Course]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KeywordCourse_Course]') AND parent_object_id = OBJECT_ID(N'[dbo].[KeywordCourse]'))
ALTER TABLE [dbo].[KeywordCourse]  WITH NOCHECK ADD  CONSTRAINT [FK_KeywordCourse_Course] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Course] ([ID])
GO
ALTER TABLE [dbo].[KeywordCourse] NOCHECK CONSTRAINT [FK_KeywordCourse_Course]
GO
/****** Object:  ForeignKey [FK_KeywordCourse_Keyword]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_KeywordCourse_Keyword]') AND parent_object_id = OBJECT_ID(N'[dbo].[KeywordCourse]'))
ALTER TABLE [dbo].[KeywordCourse]  WITH CHECK ADD  CONSTRAINT [FK_KeywordCourse_Keyword] FOREIGN KEY([KeywordID])
REFERENCES [dbo].[Keyword] ([ID])
GO
ALTER TABLE [dbo].[KeywordCourse] CHECK CONSTRAINT [FK_KeywordCourse_Keyword]
GO
/****** Object:  ForeignKey [FK_Location_Customer]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Location]'))
ALTER TABLE [dbo].[Location]  WITH NOCHECK ADD  CONSTRAINT [FK_Location_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[Location] NOCHECK CONSTRAINT [FK_Location_Customer]
GO
/****** Object:  ForeignKey [FK_Location_Location]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Location_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Location]'))
ALTER TABLE [dbo].[Location]  WITH NOCHECK ADD  CONSTRAINT [FK_Location_Location] FOREIGN KEY([ParentLocationID])
REFERENCES [dbo].[Location] ([ID])
GO
ALTER TABLE [dbo].[Location] NOCHECK CONSTRAINT [FK_Location_Location]
GO
/****** Object:  ForeignKey [FK_Registration_Class]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Registration_Class]') AND parent_object_id = OBJECT_ID(N'[dbo].[Registration]'))
ALTER TABLE [dbo].[Registration]  WITH NOCHECK ADD  CONSTRAINT [FK_Registration_Class] FOREIGN KEY([ClassID])
REFERENCES [dbo].[Class] ([ID])
GO
ALTER TABLE [dbo].[Registration] NOCHECK CONSTRAINT [FK_Registration_Class]
GO
/****** Object:  ForeignKey [FK_Registration_RegistrationType]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Registration_RegistrationType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Registration]'))
ALTER TABLE [dbo].[Registration]  WITH CHECK ADD  CONSTRAINT [FK_Registration_RegistrationType] FOREIGN KEY([RegistrationTypeID])
REFERENCES [dbo].[RegistrationType] ([ID])
GO
ALTER TABLE [dbo].[Registration] CHECK CONSTRAINT [FK_Registration_RegistrationType]
GO
/****** Object:  ForeignKey [FK_Registration_Status]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Registration_Status]') AND parent_object_id = OBJECT_ID(N'[dbo].[Registration]'))
ALTER TABLE [dbo].[Registration]  WITH CHECK ADD  CONSTRAINT [FK_Registration_Status] FOREIGN KEY([RegistrationStatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Registration] CHECK CONSTRAINT [FK_Registration_Status]
GO
/****** Object:  ForeignKey [FK_Resource_Room]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Resource_Room]') AND parent_object_id = OBJECT_ID(N'[dbo].[Resource]'))
ALTER TABLE [dbo].[Resource]  WITH NOCHECK ADD  CONSTRAINT [FK_Resource_Room] FOREIGN KEY([RoomID])
REFERENCES [dbo].[Room] ([ID])
GO
ALTER TABLE [dbo].[Resource] NOCHECK CONSTRAINT [FK_Resource_Room]
GO
/****** Object:  ForeignKey [FK_Resource_Venue]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Resource_Venue]') AND parent_object_id = OBJECT_ID(N'[dbo].[Resource]'))
ALTER TABLE [dbo].[Resource]  WITH NOCHECK ADD  CONSTRAINT [FK_Resource_Venue] FOREIGN KEY([VenueID])
REFERENCES [dbo].[Venue] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[Resource] NOCHECK CONSTRAINT [FK_Resource_Venue]
GO
/****** Object:  ForeignKey [FK_Room_Venue]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Room_Venue]') AND parent_object_id = OBJECT_ID(N'[dbo].[Room]'))
ALTER TABLE [dbo].[Room]  WITH NOCHECK ADD  CONSTRAINT [FK_Room_Venue] FOREIGN KEY([VenueID])
REFERENCES [dbo].[Venue] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[Room] NOCHECK CONSTRAINT [FK_Room_Venue]
GO
/****** Object:  ForeignKey [FK_SalesChannel_SalesChannel]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SalesChannel_SalesChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[SalesChannel]'))
ALTER TABLE [dbo].[SalesChannel]  WITH NOCHECK ADD  CONSTRAINT [FK_SalesChannel_SalesChannel] FOREIGN KEY([ParentSalesChannelID])
REFERENCES [dbo].[SalesChannel] ([ID])
GO
ALTER TABLE [dbo].[SalesChannel] NOCHECK CONSTRAINT [FK_SalesChannel_SalesChannel]
GO
/****** Object:  ForeignKey [FK_SentProblem_PossibleProblem]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SentProblem_PossibleProblem]') AND parent_object_id = OBJECT_ID(N'[dbo].[SentProblem]'))
ALTER TABLE [dbo].[SentProblem]  WITH CHECK ADD  CONSTRAINT [FK_SentProblem_PossibleProblem] FOREIGN KEY([PossibleProblemID])
REFERENCES [dbo].[PossibleProblem] ([ID])
GO
ALTER TABLE [dbo].[SentProblem] CHECK CONSTRAINT [FK_SentProblem_PossibleProblem]
GO
/****** Object:  ForeignKey [FK_TrainingProgramToCourseLink_CourseType]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainingProgramToCourseLink_CourseType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainingProgramToCourseLink]'))
ALTER TABLE [dbo].[TrainingProgramToCourseLink]  WITH NOCHECK ADD  CONSTRAINT [FK_TrainingProgramToCourseLink_CourseType] FOREIGN KEY([CourseTypeID])
REFERENCES [dbo].[CourseType] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[TrainingProgramToCourseLink] NOCHECK CONSTRAINT [FK_TrainingProgramToCourseLink_CourseType]
GO
/****** Object:  ForeignKey [FK_TrainingProgramToCourseLink_SyllabusType]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainingProgramToCourseLink_SyllabusType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainingProgramToCourseLink]'))
ALTER TABLE [dbo].[TrainingProgramToCourseLink]  WITH NOCHECK ADD  CONSTRAINT [FK_TrainingProgramToCourseLink_SyllabusType] FOREIGN KEY([SyllabusTypeID])
REFERENCES [dbo].[SyllabusType] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[TrainingProgramToCourseLink] NOCHECK CONSTRAINT [FK_TrainingProgramToCourseLink_SyllabusType]
GO
/****** Object:  ForeignKey [FK_TrainingProgramToCourseLink_TrainingProgram]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TrainingProgramToCourseLink_TrainingProgram]') AND parent_object_id = OBJECT_ID(N'[dbo].[TrainingProgramToCourseLink]'))
ALTER TABLE [dbo].[TrainingProgramToCourseLink]  WITH NOCHECK ADD  CONSTRAINT [FK_TrainingProgramToCourseLink_TrainingProgram] FOREIGN KEY([TrainingProgramID])
REFERENCES [dbo].[TrainingProgram] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[TrainingProgramToCourseLink] NOCHECK CONSTRAINT [FK_TrainingProgramToCourseLink_TrainingProgram]
GO
/****** Object:  ForeignKey [FK_User_Customer]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_User_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[User]'))
ALTER TABLE [dbo].[User]  WITH NOCHECK ADD  CONSTRAINT [FK_User_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([ID])
GO
ALTER TABLE [dbo].[User] NOCHECK CONSTRAINT [FK_User_Customer]
GO
/****** Object:  ForeignKey [FK_User_JobRole]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_User_JobRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[User]'))
ALTER TABLE [dbo].[User]  WITH NOCHECK ADD  CONSTRAINT [FK_User_JobRole] FOREIGN KEY([JobRoleID])
REFERENCES [dbo].[JobRole] ([ID])
GO
ALTER TABLE [dbo].[User] NOCHECK CONSTRAINT [FK_User_JobRole]
GO
/****** Object:  ForeignKey [FK_User_Location]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_User_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[User]'))
ALTER TABLE [dbo].[User]  WITH NOCHECK ADD  CONSTRAINT [FK_User_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([ID])
GO
ALTER TABLE [dbo].[User] NOCHECK CONSTRAINT [FK_User_Location]
GO
/****** Object:  ForeignKey [FK_User_SalesChannel]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_User_SalesChannel]') AND parent_object_id = OBJECT_ID(N'[dbo].[User]'))
ALTER TABLE [dbo].[User]  WITH NOCHECK ADD  CONSTRAINT [FK_User_SalesChannel] FOREIGN KEY([SalesChannelID])
REFERENCES [dbo].[SalesChannel] ([ID])
GO
ALTER TABLE [dbo].[User] NOCHECK CONSTRAINT [FK_User_SalesChannel]
GO
/****** Object:  ForeignKey [fk_user_supervisor]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_user_supervisor]') AND parent_object_id = OBJECT_ID(N'[dbo].[User]'))
ALTER TABLE [dbo].[User]  WITH NOCHECK ADD  CONSTRAINT [fk_user_supervisor] FOREIGN KEY([SupervisorID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[User] NOCHECK CONSTRAINT [fk_user_supervisor]
GO
/****** Object:  ForeignKey [FK_User_UserStatus]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_User_UserStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[User]'))
ALTER TABLE [dbo].[User]  WITH NOCHECK ADD  CONSTRAINT [FK_User_UserStatus] FOREIGN KEY([StatusID])
REFERENCES [dbo].[UserStatus] ([ID])
GO
ALTER TABLE [dbo].[User] NOCHECK CONSTRAINT [FK_User_UserStatus]
GO
/****** Object:  ForeignKey [FK_UserAudience_Audience]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserAudience_Audience]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserAudience]'))
ALTER TABLE [dbo].[UserAudience]  WITH NOCHECK ADD  CONSTRAINT [FK_UserAudience_Audience] FOREIGN KEY([AudienceID])
REFERENCES [dbo].[Audience] ([ID])
GO
ALTER TABLE [dbo].[UserAudience] NOCHECK CONSTRAINT [FK_UserAudience_Audience]
GO
/****** Object:  ForeignKey [FK_UserAudience_User]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserAudience_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserAudience]'))
ALTER TABLE [dbo].[UserAudience]  WITH NOCHECK ADD  CONSTRAINT [FK_UserAudience_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserAudience] NOCHECK CONSTRAINT [FK_UserAudience_User]
GO
/****** Object:  ForeignKey [FK_Venue_States]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venue_States]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venue]'))
ALTER TABLE [dbo].[Venue]  WITH NOCHECK ADD  CONSTRAINT [FK_Venue_States] FOREIGN KEY([StateID])
REFERENCES [dbo].[States] ([ID])
GO
ALTER TABLE [dbo].[Venue] NOCHECK CONSTRAINT [FK_Venue_States]
GO
/****** Object:  ForeignKey [FK_Venue_TimeZone]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Venue_TimeZone]') AND parent_object_id = OBJECT_ID(N'[dbo].[Venue]'))
ALTER TABLE [dbo].[Venue]  WITH CHECK ADD  CONSTRAINT [FK_Venue_TimeZone] FOREIGN KEY([TimeZoneID])
REFERENCES [dbo].[TimeZone] ([ID])
GO
ALTER TABLE [dbo].[Venue] CHECK CONSTRAINT [FK_Venue_TimeZone]
GO
/****** Object:  ForeignKey [FK_VenueResourceManagers_Venue]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VenueResourceManagers_Venue]') AND parent_object_id = OBJECT_ID(N'[dbo].[VenueResourceManagers]'))
ALTER TABLE [dbo].[VenueResourceManagers]  WITH NOCHECK ADD  CONSTRAINT [FK_VenueResourceManagers_Venue] FOREIGN KEY([VenueID])
REFERENCES [dbo].[Venue] ([ID])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[VenueResourceManagers] NOCHECK CONSTRAINT [FK_VenueResourceManagers_Venue]
GO
/****** Object:  ForeignKey [FK_VirtualRoom_ClassDate]    Script Date: 08/05/2010 15:57:01 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VirtualRoom_ClassDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[VirtualRoom]'))
ALTER TABLE [dbo].[VirtualRoom]  WITH NOCHECK ADD  CONSTRAINT [FK_VirtualRoom_ClassDate] FOREIGN KEY([ClassDateID])
REFERENCES [dbo].[ClassDate] ([ID])
GO
ALTER TABLE [dbo].[VirtualRoom] CHECK CONSTRAINT [FK_VirtualRoom_ClassDate]
GO


/****** Object:  StoredProcedure [dbo].[GetTrainingProgramsForUser]    Script Date: 08/06/2010 14:18:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/* fix for pagination */

ALTER procedure [dbo].[GetTrainingProgramsForUser](@userId int, @newHire bit, @searchText nvarchar(50), @orderBy nvarchar(50), @orderDir varchar(4), @pageIndex int, @pageSize int)
as
begin
	set @orderDir = lower(@orderDir);
	set @orderBy = lower(@orderBy);
	-- create a date time that represents today without the time
	declare @today datetime
	set @today = (select cast(floor(cast(getdate() as decimal(12, 5))) as datetime));
	declare @hireDate datetime
	set @hireDate = (select HireDate from [user] where id = @userId);
	declare @newHireDuration int
	set @newHireDuration = (select NewHireDuration from Customer where ID = (select CustomerID from [user] where id = @userId));
	
	declare @newHireStartDate datetime
	declare @newHireEndDate datetime
	
	-- only set these if new hire is set, so they remain null and we can coalesce in the where clause below otherwise
	if(@newHire = 1)
	begin
		set @newHireStartDate = @hireDate;
		set @newHireEndDate = dateadd(d, @newHireDuration, @hireDate);
	end;
			
	-- get all the tp details
	with temp as(
		select
			tp.*,
			T.RequiredCourseCount,
			ROW_NUMBER() over (order by TrainingProgramID) as MyRow
		from
			TrainingProgram tp
		join
			(
				-- make sure we only ever see 1 instance of a given tp
				select
					distinct(TrainingProgramID) as TrainingProgramID, 
					RequiredCourseCount
				from
					(
						-- get the assigned tps
						select
							l.TrainingProgramID, tp.Name, (count(distinct cl.ID) + tp.MinimumElectives) as RequiredCourseCount
						from
							dbo.HierarchyToTrainingProgramLink l
						left join
							dbo.fGetHierarchiesForUser(@userId) h
						on
							(
							l.[HierarchyNodeID] = h.ID
							and
							l.[HierarchyTypeID] = h.TypeID
							)
						join
							dbo.TrainingProgramToCourseLink cl
						on
							l.TrainingProgramID = cl.TrainingProgramID
						join
							dbo.TrainingProgram tp
						on
							l.TrainingProgramID = tp.ID
						where
							(
								(l.[HierarchyNodeID] = @userId and l.[HierarchyTypeID] = 4) 
								or
								(l.[HierarchyNodeID] = h.ID	and	l.[HierarchyTypeID] = h.TypeID)
							)
							and cl.SyllabusTypeID in (1,4)
						group by
							l.trainingprogramid, tp.name, tp.MinimumElectives
							
					) as X
				
				group by
					TrainingProgramID, RequiredCourseCount
			)
		as T
		on
			tp.ID = T.TrainingProgramID
		where
			Name like '%' + @searchText + '%'
		and
			IsNewHire = @newHire
		and 
			IsLive = 1
		and
			-- end date is in the future, start date is in the past
			(coalesce(@newHireEndDate, EndDate) >= @today and coalesce(@newHireStartDate, StartDate) <= @today)
		
	)
	select
		ID, CustomerID, OwnerID, Name, InternalCode, Cost, Description, IsNewHire, IsLive, 
		coalesce(@newHireStartDate, StartDate) as StartDate,
		coalesce(@newHireEndDate, EndDate) as EndDate,
		-- new hire end date is the due date
		coalesce(@newHireEndDate, DueDate) as DueDate,
		EnforceRequiredOrder, MinimumElectives, FinalAssessmentCourseID, 
		FinalAssessmentCourseTypeID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn,
		RequiredCourseCount,
		(select max(myrow) from temp) as TotalSize
	from
		temp
	where
		myrow between ((@pageIndex * @pageSize) + 1) and ((@pageIndex + 1) * @pageSize + 1)
	group by
		ID, CustomerID, OwnerID, Name, InternalCode, Cost, Description, IsNewHire, IsLive, StartDate, EndDate, DueDate, EnforceRequiredOrder, MinimumElectives, FinalAssessmentCourseID, FinalAssessmentCourseTypeID, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn,
		RequiredCourseCount
	order by
		case when @orderBy = 'Name' and @orderDir = 'asc' then Name end,
		case when @orderBy = 'Name' and @orderDir = 'desc' then Name end desc,
		case when @orderBy = 'Description' and @orderDir = 'asc' then [Description] end,
		case when @orderBy = 'Description' and @orderDir = 'desc' then [Description] end desc,
		case when @orderBy = 'StartDate' and @orderDir = 'asc' then StartDate end,
		case when @orderBy = 'StartDate' and @orderDir = 'desc' then StartDate end desc,
		case when @orderBy = 'EndDate' and @orderDir = 'asc' then EndDate end,
		case when @orderBy = 'EndDate' and @orderDir = 'desc' then EndDate end desc,
		case when @orderBy = 'DueDate' and @orderDir = 'asc' then DueDate end,
		case when @orderBy = 'DueDate' and @orderDir = 'desc' then DueDate end desc,
		case when 1 = 1 then StartDate end
	
end

GO

ALTER procedure [dbo].[GetTranscript](@userId int)
as
begin

	select
		oc.ID as CourseID,
		oc.[Name] as CourseName,
		2 as CourseTypeID,
		0 as ClassID,
		0 as RegistrationID,
		0 as RegistrationStatusID,
		case 
			when Success is null then 'Unknown'
			when Success = 0 then 'Failed'
			when Success = 1 then 'Passed'
		end
		as PassOrFail,
		Success as Passed,
		case 
			when Completion is null then 'Unknown'
			when Completion = 0 then 'Incomplete'
			when Completion = 1 then 'Completed'
		end
		as CompleteOrIncomplete,
		Completion as Completed,
		0 as OnlineTestID,
		0 as SurveyID,
		oc.PublicIndicator as IsPublic,
		cast(floor(round(cro.Score,0)) as nvarchar(50)) as Score, 
		coalesce(HighScoreDate, cro.Modifiedon, null) as AttemptDate,
		cro.TotalSeconds as AttemptTime,
		cro.AttemptCount as AttemptCount,
		null as StartDate,
		coalesce(HighScoreDate, cro.Modifiedon, null) as EndDate,
		cro.TrainingProgramID
	from
		OnlineCourseRollup as cro 
	left outer join                  
		onlineCourse as oc 
	on
		oc.ID = cro.CourseID
	where
		cro.UserID = @userID
	and
		-- there shouldn't be any rollups without courses, but you never know...
		oc.ID is not null

	UNION

	select
		CourseID,
		CourseName,
		1 as CourseTypeID,
		ClassID,
		RegistrationID,
		RegistrationStatusID,
		case 
			when Passed = 0 and ScoreReported = 1 then 'Failed'
			when Passed = 1 and ScoreReported = 1 then 'Passed'
			else 'Unknown'
		end as PassOrFail,
		Passed,
		case ScoreReported
			when 1 then 'Complete'
			else 'Incomplete'
		end as CompleteOrIncomplete,
		ScoreReported as Completed,
		OnlineTestID,
		SurveyID,
		PublicIndicator,
		Score,
		AttemptDate,
		AttemptTime,
		AttemptCount,
		StartDate,
		EndDate,
		0 as TrainingProgramID
	from
	(
		select
			cCourse.ID as CourseID,
			cCourse.[Name] as CourseName,
			cClass.ID as ClassID,
			cReg.ID as RegistrationID, 
			cReg.RegistrationStatusID as RegistrationStatusID,
			-- Whether or not the user has passed changes based on course type and completion type.
			-- This is where minimum passing scores are set for classroom courses.
			-- Online courses have passing scores on a per-course basis, and the "passed" flag will be set as appropriate.
			case
				when cCourseCT.CodeName = 'attendance' and cReg.AttendedIndicator = 1 
					then 1
				when cCourseCT.CodeName = 'attendance'
					then 0
				when cCourseCT.CodeName = 'numeric' and ( len(cReg.Score) = 3 or cReg.Score >= cast(isnull(cPass.PassingScore, 70) as varchar(15)) )
					then 1
				when cCourseCT.CodeName = 'numeric' 
					then 0
				when cCourseCT.CodeName = 'letter' and (cReg.Score not in ('A','B','C','D'))
					then 0 
				when cCourseCT.CodeName = 'letter' 
					then 1 
				when cCourseCT.CodeName = 'onlinecourse' AND ocr.Success = 1
					then 1 
				when cCourseCT.CodeName = 'onlinecourse' 
					then 0 
			end as Passed,
			-- To determine if a user has attended a course with attendance based passing,
			-- we have a bit of a conundrum - there's no way to know if they DIDN'T attend, since the default value for
			-- attended is 0. Therefore, (and per BE's instructions) we assume that if the registration was modified after the 
			-- last day of the class, then whatever indicator is in the db was the "reported" value. So, if the date modified is
			-- after the last day of the class, or the AttendedIndicator is true, the instructor reported a score.
			-- For online courses, the 'Complete' flag will be set to '2' once a score has been sent to the server.
			case 
				when cCourseCT.CodeName = 'attendance' and (cReg.ModifiedOn > vClassDMM.MaxStartDateTime or cReg.AttendedIndicator = 1) 
					then 1 
				when cCourseCT.CodeName = 'attendance' 
					then 0 
				when cCourseCT.CodeName = 'onlinecourse' and (ocr.Completion = 1 or ocr.Score > 0)
					then 1 
				when cCourseCT.CodeName = 'onlinecourse'
					then 0 
				when cReg.Score = '' 
					then 0
				else 1
			end as ScoreReported,
			cCourse.OnlineCourseID as OnlineTestID,
			cClass.SurveyID as SurveyID,
			cCourse.PublicIndicator as PublicIndicator,
			-- Calculate the score
			--    -if the score is based on attendance, they get 100 if they attended
			--    -if the score is entered by the instructor, use that. Since this can be a letter or a number, we use nvarchar
			--    -if the score is based on taking an online course, use that score
			case 
				when cCourseCT.CodeName = 'attendance' and cReg.AttendedIndicator = 1
					then '100'
				when ocr.ID is null
					then cast(cReg.Score as nvarchar(50))
				else 
					cast(floor(round(ocr.Score,0)) as nvarchar(50)) 
			end as Score,
			null as AttemptDate,
			0 as AttemptTime,
			0 as AttemptCount,
			vClassDMM.MinStartDateTime as StartDate,
			vClassDMM.MaxStartDateTime as EndDate
		from         
			dbo.Registration as cReg 
		inner join
			dbo.[Status] as stat on cReg.RegistrationStatusID = stat.ID
		inner join
			 dbo.Class as cClass ON cClass.ID = cReg.ClassID 
		inner join
			dbo.Course as cCourse ON cCourse.ID = cClass.CourseID 
		inner join
			dbo.CourseCompletionType as cCourseCT ON cCourseCT.ID = cCourse.CourseCompletionTypeID 
		inner join
			dbo.ClassDateMinMax as vClassDMM ON vClassDMM.ClassID = cClass.ID 
		left join
			dbo.PassingScore as cPass ON cPass.CustomerID = cClass.CustomerID 
		--left join
		--	dbo.[User] as cUser ON cUser.ID = cReg.RegistrantID 
		-- this join simply grabs the score from the user if they had an online course to take for this class
		left join
			dbo.OnlineCourseRollup as ocr ON 
				ocr.CourseID = cCourse.OnlineCourseID AND 
				ocr.UserID = @userid
		where
			cReg.RegistrantID = @userid
	) as X
end








GO


