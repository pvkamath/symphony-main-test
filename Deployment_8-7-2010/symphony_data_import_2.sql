use Symphony2;

-- trainingprogramtocourselink - invalid column name "due date", invalid column name "customerid"
set identity_insert dbo.TrainingProgramToCourseLink on;

insert dbo.TrainingProgramToCourseLink(
	ID, TrainingProgramID, CourseID, CourseTypeID, SyllabusTypeID, RequiredSyllabusOrder, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, CustomerID
) 
select 
	l.ID, l.TrainingProgramID, l.CourseID, l.CourseTypeID, l.SyllabusTypeID, l.RequiredSyllabusOrder, l.ModifiedBy, l.CreatedBy, l.ModifiedOn, l.CreatedOn, tp.CustomerID
from
	CourseAssignment.dbo.TrainingProgramToCourseLink l
join
	CourseAssignment.dbo.TrainingProgram tp
on
	l.TrainingProgramID = tp.ID;

set identity_insert dbo.TrainingProgramToCourseLink off;

-- scheduleparameters - codename, canfiltercomplete
SET IDENTITY_INSERT [dbo].[ScheduleParameters] ON
INSERT [dbo].[ScheduleParameters] ([ID], [DisplayName], [CodeName], [CanFilterComplete]) VALUES (1, N'Training Program - Start Date', N'trainingprogramstartdate', 0)
INSERT [dbo].[ScheduleParameters] ([ID], [DisplayName],  [CodeName], [CanFilterComplete]) VALUES (2, N'Training Program - End Date', N'trainingprogramenddate', 1)
INSERT [dbo].[ScheduleParameters] ([ID], [DisplayName],  [CodeName], [CanFilterComplete]) VALUES (3, N'Training Program - Due Date', N'trainingprogramduedate', 1)
INSERT [dbo].[ScheduleParameters] ([ID], [DisplayName],  [CodeName], [CanFilterComplete]) VALUES (5, N'Class - Start Date', N'classstartdate', 0)
INSERT [dbo].[ScheduleParameters] ([ID], [DisplayName],  [CodeName], [CanFilterComplete]) VALUES (6, N'Class - End Date', N'classenddate', 1)
INSERT [dbo].[ScheduleParameters] ([ID], [DisplayName], [CodeName], [CanFilterComplete]) VALUES (7, N'Training Program Course - Due Date', N'trainingprogramcourseduedate', 1)
SET IDENTITY_INSERT [dbo].[ScheduleParameters] OFF

-- templates - filtercomplete
SET IDENTITY_INSERT [dbo].Templates ON
insert dbo.Templates(
	ID, Subject, Body, CodeName, DisplayName, Description, Priority, CustomerID, Enabled, 
	ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, Area, IsScheduled, ScheduleParameterID, 
	RelativeScheduledMinutes, FilterComplete
)
select 
	ID, Subject, Body, CodeName, DisplayName, Description, Priority, CustomerID, Enabled, 
	ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, Area, IsScheduled, ScheduleParameterID, 
	RelativeScheduledMinutes, 0
from Messaging.dbo.Templates
SET IDENTITY_INSERT [dbo].Templates OFF

-- this line is needed twice
delete from PresentationType;
dbcc checkident('PresentationType',reseed,0);
delete from PresentationType;
dbcc checkident('PresentationType',reseed,0);
-- classroom - presentationtype
insert into PresentationType
	([Description], [CreatedOn], [ModifiedOn])
	values
	('Class', GETDATE(), GETDATE())
insert into PresentationType
	([Description], [CreatedOn], [ModifiedOn])
	values
	('Lecture', GETDATE(), GETDATE())
insert into PresentationType
	([Description], [CreatedOn], [ModifiedOn])
	values
	('Seminar', GETDATE(), GETDATE())
insert into PresentationType
	([Description], [CreatedOn], [ModifiedOn])
	values
	('Vendor Training', GETDATE(), GETDATE())
insert into PresentationType
	([Description], [CreatedOn], [ModifiedOn])
	values
	('Third Party Training', GETDATE(), GETDATE())
insert into PresentationType
	([Description], [CreatedOn], [ModifiedOn])
	values
	('Conference', GETDATE(), GETDATE())
insert into PresentationType
	([Description], [CreatedOn], [ModifiedOn])
	values
	('Forum', GETDATE(), GETDATE())
insert into PresentationType
	([Description], [CreatedOn], [ModifiedOn])
	values
	('Meeting', GETDATE(), GETDATE())


-- onlinecourse - internalcode
SET IDENTITY_INSERT [dbo].OnlineCourse ON
insert dbo.OnlineCourse(
	ID, CustomerID, Name, Description, Keywords, Cost, Credit, PublicIndicator, IsSurvey, IsTest, ModifiedBy, 
	CreatedBy, ModifiedOn, CreatedOn, Active, DuplicateFromID, Version, InternalCode
)
select
	ID, CustomerID, Name, Description, Keywords, Cost, Credit, PublicIndicator, IsSurvey, IsTest, ModifiedBy, 
	CreatedBy, ModifiedOn, CreatedOn, Active, DuplicateFromID, Version, ''
from
	OnlineTraining.dbo.OnlineCourse
SET IDENTITY_INSERT [dbo].OnlineCourse OFF

-- classroom - course
SET IDENTITY_INSERT [dbo].Course ON
insert dbo.Course(
	ID, Name, InternalCode, CustomerID, Description, CourseDurationUnitID, PublicIndicator, 
	Credit, PerStudentFee, PresentationTypeID, CourseCompletionTypeID, SurveyID, InTrainingCycle, 
	ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, CourseCompletionTypeParameterID, 
	CourseDurationUnitCount, CourseObjective, OnlineCourseID
)
select
	ID, Name, InternalCode, CustomerID, Description, CourseDurationUnitID, PublicIndicator, 
	Credit, PerStudentFee, CourseTypeID, CourseCompletionTypeID, SurveyID, InTrainingCycle, 
	ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, CourseCompletionTypeParameterID, 
	CourseDurationUnitCount, CourseObjective, OnlineCourseID
from
	Classroom.dbo.Course
SET IDENTITY_INSERT [dbo].Course OFF

-- classroom - coursefile
set IDENTITY_INSERT coursefile on
insert coursefile
	(ID, FileName, OriginalFileName, Description, Title, Path, FileTypeID, FileSize, FileBytes, FileExtension, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, CustomerID, CourseID)
select 
	ID, FileName, OriginalFileName, Description, Title, Path, FileTypeID, FileSize, 0, FileExtension, ModifiedBy, CreatedBy, ModifiedOn, CreatedOn, CustomerID, 0
from 
	classroom.dbo.[file]
set IDENTITY_INSERT coursefile off


update
	coursefile
set
	coursefile.courseid = b.courseid
from
	coursefile
join
	classroom.dbo.coursefiles b
on
	coursefile.id = b.fileid

-- a few further updates
update recipientgroups set selector = 'ClassInstructors.InstructorID' where id = 2
update recipientgroups set selector = 'Registrations.RegistrantID' where id = 3
update recipientgroups set selector = 'Room.Venue.VenueResourceManagers.ResourceManagerID' where id = 4
update coursecompletiontype set [description] = 'Online Test' where codename = 'onlinecourse'

update templates set enabled = 0 where customerid = 0