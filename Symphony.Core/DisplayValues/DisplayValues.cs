﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

using System.Linq;
using System.Text;
using System.Reflection;
using System.Configuration;
using System.Runtime.Serialization;
using Symphony.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Symphony.Core.Controllers;

namespace Symphony.Core
{
    [DataContract(Name = "DisplayValue")]
    [Serializable()]
    public class DisplayValue : Model
    {
        private string _defaultValue = "";

        protected DisplayValue(string defaultValue) { _defaultValue = defaultValue; }

        private static ConcurrentDictionary<string, ConcurrentDictionary<DisplayValue, string>> valueDictionary = new ConcurrentDictionary<string, ConcurrentDictionary<DisplayValue, string>>();
        private static ConcurrentDictionary<string, ConcurrentDictionary<DisplayValue, string>> codeDictionary = new ConcurrentDictionary<string, ConcurrentDictionary<DisplayValue, string>>();

        private log4net.ILog log = log4net.LogManager.GetLogger(typeof(DisplayValue));

        public static void BuildLookupDictionaries() {
            List<Type> displayTypes = Assembly.GetAssembly(typeof(DisplayValue)).GetTypes()
                .Where(t => t.IsSubclassOf(typeof(DisplayValue)))
                .ToList();

            valueDictionary = new ConcurrentDictionary<string, ConcurrentDictionary<DisplayValue, string>>();
            codeDictionary = new ConcurrentDictionary<string, ConcurrentDictionary<DisplayValue, string>>();

            foreach (Type t in displayTypes) {
                valueDictionary.AddOrUpdate(t.Name, new ConcurrentDictionary<DisplayValue, string>(), (k, v) => new ConcurrentDictionary<DisplayValue, string>());
                codeDictionary.AddOrUpdate(t.Name, new ConcurrentDictionary<DisplayValue, string>(), (k, v) => new ConcurrentDictionary<DisplayValue, string>());
                FieldInfo[] fields = t.GetFields();

                // Handle fields
                foreach (FieldInfo field in fields)
                {

                    if (field.FieldType.BaseType == typeof(DisplayValue))
                    {
                        DisplayValue val = (DisplayValue)field.GetValue(null);

                        string code = t.Name + "_" + field.Name;
                        string value = ConfigurationManager.AppSettings[code];

                        if (string.IsNullOrEmpty(value))
                        {
                            value = val.Default;
                        }

                        valueDictionary[t.Name].AddOrUpdate(val, value, (k, v) => value);
                        codeDictionary[t.Name].AddOrUpdate(val, code, (k, v) => code);
                    }
                }

                // handle properties
                PropertyInfo[] properties = t.GetProperties();

                foreach (PropertyInfo prop in properties)
                {
                    if (prop.CanRead && prop.PropertyType.BaseType == typeof(DisplayValue))
                    {
                        DisplayValue val = (DisplayValue)prop.GetValue(null, null);

                        string code = t.Name + "_" + prop.Name;
                        string value = ConfigurationManager.AppSettings[code];

                        if (string.IsNullOrEmpty(value))
                        {
                            value = val.Default;
                        }

                        valueDictionary[t.Name].AddOrUpdate(val, value, (k, v) => value);
                        codeDictionary[t.Name].AddOrUpdate(val, code, (k, v) => code);
                    }
                }
            }
        }       

        [DataMember(Name = "value")]
        public string Value
        {
            get
            {
                Type t = this.GetType();

                if (valueDictionary.ContainsKey(t.Name) &&
                    valueDictionary[t.Name].ContainsKey(this) &&
                    IsValid(valueDictionary[t.Name][this])
                    )
                {
                    return valueDictionary[t.Name][this];
                }

                return _defaultValue;
            }
            internal set { }
        }

        [DataMember(Name = "code")]
        public string Code
        {
            get
            {
                Type t = this.GetType();

                if (codeDictionary.ContainsKey(t.Name) &&
                    codeDictionary[t.Name].ContainsKey(this)
                    )
                {
                    return codeDictionary[t.Name][this];
                }

                // Code not found, probably a dynamic value or a missing value
                MethodInfo[] methods = t.GetMethods();
                foreach (MethodInfo method in methods)
                {
                    if (method.Name == "Dynamic")
                    {
                        return t.Name + "_Dynamic";
                    }
                }

                // Couldn't find a code
                return t.Name + "_Unknown";
            }
            internal set { }
        }

        [IgnoreDataMember]
        public string Default
        {
            get { return _defaultValue; }
        }

        protected virtual bool IsValid(string value)
        {
            return true;
        }
    }
    [DataContract(Name = "DateFormat")]
    [Serializable()]	
    public class DateFormat : DisplayValue
    {
        private DateFormat(string value) : base(value) { }

        [IgnoreDataMember]
        public bool HasTimeComponent
        {
            get
            {
                return Default.Contains("g:i");
            }
        }
        /// <summary>
        /// ""
        /// </summary>
        public static DateFormat Empty       = new DateFormat("");
        /// <summary>
        /// m/d/y
        /// </summary>
        public static DateFormat Date        = new DateFormat("m/d/y");
        /// <summary>
        /// m/d/y g:i a T
        /// </summary>
        public static DateFormat DateTimeTz  = new DateFormat("m/d/y g:i a T");
        /// <summary>
        /// m/d/y g:i a
        /// </summary>
        public static DateFormat DateTime    = new DateFormat("m/d/y g:i a");
        /// <summary>
        /// M j, Y
        /// </summary>
        public static DateFormat NaturalDate = new DateFormat("M j, Y");
        /// <summary>
        /// D, F j, Y g:i a
        /// </summary>
        public static DateFormat LongDate    = new DateFormat("D, F j, Y g:i a");
    }
    [DataContract(Name = "IconState")]
    [Serializable()]	
    public class IconState : DisplayValue
    {
        private IconState(string value) : base(value) { }

        /// <summary>
        /// ""
        /// </summary>
        public static IconState Empty = new IconState("");
        /// <summary>
        /// /images/bullet_white.png
        /// </summary>
        public static IconState Inactive    = new IconState("/images/bullet_white.png");
        /// <summary>
        /// /images/bullet_black.png
        /// </summary>
        public static IconState InactiveStrong = new IconState("/images/bullet_black.png");
        /// <summary>
        /// /images/tick.png
        /// </summary>
        public static IconState Active = new IconState("/images/tick.png");
        /// <summary>
        /// /images/medal_gold_3.png
        /// </summary>
        public static IconState Certificate = new IconState("/images/medal_gold_3.png");
        /// <summary>
        /// /images/tick.png
        /// </summary>
        public static IconState Complete    = new IconState("/images/tick.png");
        /// <summary>
        /// /images/bullet_white.png
        /// </summary>
        public static IconState Incomplete = new IconState("/images/bullet_white.png");
        /// <summary>
        /// /images/cross.png
        /// </summary>
        public static IconState Failed      = new IconState("/images/cross.png");
        /// <summary>
        /// /images/new.png
        /// </summary>
        public static IconState New         = new IconState("/images/new.png");
        /// <summary>
        /// /images/error.png
        /// </summary>
        public static IconState Error       = new IconState("/images/error.png");
        /// <summary>
        /// /images/arrow_undo.png
        /// </summary>
        public static IconState Retry = new IconState("/images/arrow_undo.png");
        /// <summary>
        /// /imgages/report.png
        /// </summary>
        public static IconState Assignments = new IconState("/images/report.png");
        /// <summary>
        /// /images/webcam.png
        /// </summary>
        public static IconState Webcam = new IconState("/images/webcam.png");
        /// <summary>
        /// /images/message.png
        /// </summary>
        public static IconState Message = new IconState("/images/message.png");
        /// <summary>
        /// /images/application_cascade.png
        /// </summary>
        public static IconState TrainingProgramLibraryItem = new IconState("/images/application_cascade.png");
        /// <summary>
        /// /images/monitor.png
        /// </summary>
        public static IconState OnlineCourseLibraryItem = new IconState("/images/monitor.png");
        /// <summary>
        /// /images/star.png
        /// </summary>
        public static IconState FavoriteLibraryItem = new IconState("/images/star.png");
        /// <summary>
        /// /images/bullet_white.png
        /// </summary>
        public static IconState UnFavoriteLibraryItem = new IconState("/images/bullet_white.png");
        /// <summary>
        /// /images/tick.png
        /// </summary>
        public static IconState InLibraryItem = new IconState("/images/tick.png");
        /// <summary>
        /// /images/printer.png
        /// </summary>
        public static IconState Print = new IconState("/images/printer.png");
    }
    [DataContract(Name = "Text")]
    [Serializable()]	
    public class Text : DisplayValue
    {
        protected Text(string value) : base(value) { }

        /// <summary>
        /// ""
        /// </summary>
        public static Text Empty = new Text("");
        public static Text Dynamic(string value)
        {
            return new Text(value);
        }

        #region General 
        /// <summary>
        /// View certificate
        /// </summary>
        public static Text ViewCertificate = new Text("View Certificate");
        /// <summary>
        /// -
        /// </summary>
        public static Text Dash = new Text("-");
        /// <summary>
        /// N/A
        /// </summary>
        public static Text NotApplicable = new Text("N/A");
        /// <summary>
        /// Register
        /// </summary>
        public static Text Register = new Text("Register");
        /// <summary>
        /// Wait List
        /// </summary>
        public static Text WaitList = new Text("Wait List");
        /// <summary>
        /// Unregister
        /// </summary>
        public static Text Unregister = new Text("Unregister");
        /// <summary>
        /// Awaiting Approval
        /// </summary>
        public static Text AwaitingApproval = new Text("Awaiting Approval");
        /// <summary>
        /// Launch Webinar
        /// </summary>
        public static Text LaunchWebinar = new Text("Launch Webinar");
        /// <summary>
        /// In Progress
        /// </summary>
        public static Text InProgress = new Text("In Progress");
        /// <summary>
        /// Take Test
        /// </summary>
        public static Text TakeTest = new Text("Take Test");
        /// <summary>
        /// Take Survey
        /// </summary>
        public static Text TakeSurvey = new Text("Take Survey");
        /// <summary>
        /// Completed
        /// </summary>
        public static Text Completed = new Text("Completed");
        /// <summary>
        /// Launch
        /// </summary>
        public static Text Launch = new Text("Launch");
        /// <summary>
        /// Unavailable
        /// </summary>
        public static Text Unavailable = new Text("Unavailable");
        #endregion
        #region Registration
        /// <summary>
        /// Your previous registration has been denied.
        /// </summary>
        public static Text Registration_Denied = new Text("Your previous registration has been denied.");
        #endregion
        #region Session
        /// <summary>
        /// Session registration required.
        /// </summary>
        public static Text Session_RegistrationRequired = new Text("Session registration required.");
        /// <summary>
        /// You are not registered in a session for this training program. Contact your administrator for assistance.
        /// </summary>
        public static Text Session_RegistrationRequiredDetail = new Text("You are not registered in a session for this training program. Contact your administrator for assistance.");
        /// <summary>
        /// The due date for this {!course} has passed. Contact your training program leader for more time if needed.
        /// </summary>
        public static Text Session_DueDatePassed = new Text("The due date for this {!course} has passed.");
        #endregion
        #region Classroom
        /// <summary>
        /// Online
        /// </summary>
        public static Text Class_Location_Online = new Text("Online");
        /// <summary>
        /// Not specified
        /// </summary>
        public static Text Class_Location_Unspecified = new Text("Not specified");
        /// <summary>
        /// Closed
        /// </summary>
        public static Text Class_Registration_Closed = new Text("Closed");
        #endregion
        #region Course
        /// <summary>
        /// You have not successfully passed the assignments for this {!course}.
        /// </summary>
        public static Text Course_AssignmentsComplete = new Text("Your assignments have been successfully completed!");
        /// <summary>
        /// You have not successfully passed the assignments for this {!course}.
        /// </summary>
        public static Text Course_AssignmentsFailed = new Text("You have not successfully passed the assignments for this {!course}.");
        /// <summary>
        /// You have not successfully passed the assignments for this {!course}.
        /// </summary>
        public static Text Course_AssignmentsUnmarked = new Text("Your assignment has not yet been marked by an instructor.");
        /// <summary>
        /// Assignments have not yet been attempted for this {!course}
        /// </summary>
        public static Text Course_AssignmentsUnattempted = new Text("Assignments have not yet been attempted for this {!course}");
        /// <summary>
        /// You have not successfully passed the assignments for this {!course}.
        /// </summary>
        public static Text Course_ScoreRecorded_AssignmentsFailed = new Text("Your score has been recorded and will be available once you retry your assignments.");
        /// <summary>
        /// You have not successfully passed the assignments for this {!course}.
        /// </summary>
        public static Text Course_ScoreRecorded_AssignmentsUnmarked = new Text("Your score has been recorded and will be available once your assignments are marked.");
        /// <summary>
        /// Assignments have not yet been attempted for this {!course}
        /// </summary>
        public static Text Course_ScoreRecorded_AssignmentsUnattempted = new Text("Your score has been recorded and will be available once you complete your assignments.");
        /// <summary>
        /// Assignments have not yet been attempted for this {!course}
        /// </summary>
        public static Text Course_ScoreRecorded_SurveyRequired = new Text("Your score has been recorded and will be available once you complete the required survey.");
        /// <summary>
        /// This {!course} has not been completed.
        /// </summary>
        public static Text Course_Unattempted = new Text("This {!course} has not been completed.");
        /// <summary>
        /// Your instructor has not marked you as attended for this {!course}.
        /// </summary>
        public static Text Course_Unattended = new Text("Your instructor has not marked you as attended for this {!course}.");
        /// <summary>
        /// Certificate will be available after survey is complete.
        /// </summary>
        public static Text Course_SurveyRequired = new Text("Certificate will be available after survey is complete.");
        /// <summary>
        /// You have passed this {!course}, and it doesn\'t allow retries.
        /// </summary>
        public static Text Course_CompletedNoRetry = new Text("You have passed this {!course}, and it doesn\'t allow retries.");
        /// <summary>
        /// You have completed this survey.
        /// </summary>
        public static Text Course_SurveyComplete = new Text("You have completed this survey.");
        /// <summary>
        /// You need instructor approval from a previous {!course} in order to advance.
        /// </summary>
        public static Text Course_ApprovalRequired = new Text("You need instructor approval from a previous {!course} in order to advance.");
        /// <summary>
        /// This {!course} is unavailable until all prior required {!courses} are complete.
        /// </summary>
        public static Text Course_PreviousRequired = new Text("This {!course} is unavailable until all prior required {!courses} are complete.");
        /// <summary>
        /// This {!course} is unavailable until all prior required {!courses} are registered.
        /// </summary>
        public static Text Course_PreviousRegistrationRequired = new Text("This {!course} is unavailable until all prior required {!courses} are registered.");
        /// <summary>
        /// This assignment has been re-submitted.
        /// </summary>
        public static Text Course_Assignments_State_Resubmitted = new Text("This assignment has been re-submitted.");
        /// <summary>
        /// This assignment has not been fully completed.
        /// </summary>
        public static Text Course_Assignments_State_Incomplete = new Text("This assignment has not been fully completed.");
        /// <summary>
        /// This assignment has not yet been marked.
        /// </summary>
        public static Text Course_Assignments_State_Unmarked = new Text("This assignment has not yet been marked.");
        /// <summary>
        /// This assignment has successfully been completed."
        /// </summary>
        public static Text Course_Assignments_State_Passed = new Text("This assignment has successfully been completed.");
        /// <summary>
        /// This assignment has not been completed.
        /// </summary>
        public static Text Course_Assignments_State_Failed = new Text("This assignment has not been completed.");
        /// <summary>
        /// View Assignments
        /// </summary>
        public static Text Course_Assignments_View = new Text("View Assignments");
        /// <summary>
        /// Launch Assignment
        /// </summary>
        public static Text Course_Assignments_Launch = new Text("Launch Assignment");
        /// <summary>
        /// Print This Course
        /// </summary>
        public static Text Course_Print = new Text("Print This Course");
        #endregion
        #region TrainingProgram
        /// <summary>
        /// This training program is incomplete.
        /// </summary>
        public static Text TrainingProgram_Incomplete = new Text("This training program is incomplete.");

        /// <summary>
        /// You have not spent the minimum amount of time in this training program's {!courses} in order to get your certificate.
        /// </summary>
        public static Text TrainingProgram_MinimumTimeIncomplete = new Text("You have not spent the minimum amount of time in this training program's required {!courses} in order to get your certificate.");
        /// <summary>
        /// Your certificate will be available after you complete all required surveys.
        /// </summary>
        public static Text TrainingProgram_SurveysRequired = new Text("Your certificate will be available after you complete all required surveys.");
        /// <summary>
        /// Some {!courses} require assignments yet to be completed or reviewed by an instructor. Please ensure all your assignments are complete.
        /// </summary>
        public static Text TrainingProgram_AssignmentsRequired = new Text("Some {!courses} require assignments yet to be completed or reviewed by an instructor. Please ensure all your assignments are complete.");
        /// <summary>
        /// This training program does not have a survey
        /// </summary>
        public static Text TrainingProgram_NoSurvey = new Text("This training program does not have a survey.");
        /// <summary>
        /// Complete the training program before taking the survey 
        /// </summary>
        public static Text TrainingProgram_SurveyUnavailable = new Text("The survey will be available once the rest of the training program has been completed.");
        /// <summary>
        /// Elective {!courses} are disabled until all required {!courses} are complete.
        /// </summary>
        public static Text TrainingProgram_ElectivesUnavailable = new Text("Elective {!courses} are disabled until all required {!courses} are complete.");
        /// <summary>
        /// Optional {!courses} are disabled until all required {!courses} are complete and minimum elective requirements are met.
        /// </summary>
        public static Text TrainingProgram_OptionalUnavailable = new Text("Optional {!courses} are disabled until all required {!courses} are complete and minimum electives are met.");
        /// <summary>
        /// The final assessment is disabled until:<ul>
        /// </summary>
        public static Text TrainingProgram_FinalDisabled = new Text("The final assessment is disabled until:<ul>");
        /// <summary>
        /// <li>All required {!courses} are complete.</li>
        /// </summary>
        public static Text TrainingProgram_FinalDisabled_RequiredComplete = new Text("<li>All required {!courses} are complete.</li>");
        /// <summary>
        /// <li>You spend the minimum amount of time in your required courses.</li>
        /// </summary>
        public static Text TrainingProgram_FinalDisabled_MinimumTimeIncomplete = new Text("<li>You spend the minimum amount of time in your required courses.</li>");
        
        /// <summary>
        /// <li>Minimum electives are met.</li>
        /// </summary>
        public static Text TrainingProgram_FinalDisabled_ElectivesComplete = new Text("<li>Minimum electives are met.</li>");
        /// <summary>
        /// <li>Your instructor allows you to move forward.</li>
        /// </summary>
        public static Text TrainingProgram_FinalDisabled_RequiredMoveForward = new Text("<li>Your instructor allows you to move forward.</li>");
        /// <summary>
        /// <li>You retry and pass your failed assignments. ({!assignmentsFailed} failed)</li>
        /// </summary>
        public static Text TrainingProgram_FinalDisabled_AssignmentsFailed = new Text("<li>You resubmit your assignments and have them accepted. ({!assignmentsFailed} unaccepted)</li>");
        /// <summary>
        /// <li>Your instructor has marked all of your assignments. ({!assignmentsUnmarked} unmarked}</li>
        /// </summary>
        public static Text TrainingProgram_FinalDisabled_AssignmentsUnmarked = new Text("<li>Your instructor has marked all of your assignments. ({!assignmentsUnmarked} unmarked)</li>");
        /// <summary>
        /// </summary>
        public static Text TrainingProgram_FinalDisabled_Footer = new Text("</ul>");
        /// <summary>
        /// Surveys cannot be taken until<ul>
        /// </summary>
        public static Text TrainingProgram_AccreditationSurveyDisabled_Header = new Text("Surveys cannot be taken until: <ul>");
        /// <summary>
        /// You complete the final assessment.
        /// </summary
        public static Text TrainingProgram_AccreditationSurveyDisabled_FinalComplete = new Text("<li>You complete the final assessment.</li>");
        /// <summary>
        /// </ul>
        /// </summary>
        public static Text TrainingProgram_AccreditationSurveyDisabled_Footer = new Text("</ul>");

        #endregion

        #region LaunchStep
        /// <summary>
        /// {!Course} Unavailable"
        /// </summary>
        public static Text LaunchStep_CourseUnavailable_Title = new Text("{!Course} Unavailable");
        /// <summary>
        /// Cannot launch {!course}. You have spent more than the daily allowed time doing online {!course} work for this {!course}.
        /// </summary>
        public static Text LaunchStep_CourseUnavailable_Content = new Text("Cannot launch {!course}. You have spent more than the daily allowed time doing online {!course} work for this {!course}.");
        /// <summary>
        /// Your certificate will be available after you complete all required surveys.
        /// </summary>
        public static Text LaunchStep_SessionExpired_Title = new Text("Launch in Review Mode?");
        /// <summary>
        /// >Warning! You are about to launch this course in Review Mode. Your progress will not be recorded!
        /// </summary>
        public static Text LaunchStep_SessionExpired_Content = new Text("<img class=\"msg-box-icon\" src=\"/images/error_64x64.png\"/><p class=\"msg-box-text\">Warning! You are about to launch this course in Review Mode. Your progress will not be recorded!</p>");
        /// <summary>
        /// User Validation Required
        /// </summary>
        public static Text LaunchStep_ValidationRequired_Title = new Text("User Validation Required");
        /// <summary>
        /// Please enter your personal details. These will be used to ensure only you are taking online {!courses}. You will only be prompted to enter this information once. While taking courses you will be periodically asked to confirm these details.
        /// </summary>
        public static Text LaunchStep_ValidationRequired_Content = new Text("Please enter your personal details. These will be used to ensure only you are taking online {!courses}. You will only be prompted to enter this information once. While taking courses you will be periodically asked to confirm these details.");
        /// <summary>
        /// Proctor Required
        /// </summary>
        public static Text LaunchStep_ProctorRequired_Title = new Text("Proctor Required");
        /// <summary>
        /// Please have your proctor enter their information
        /// </summary>
        public static Text LaunchStep_ProctorRequired_Content = new Text("Please have your proctor enter their information");
        /// <summary>
        /// Affidavit Required
        /// </summary>
        public static Text LaunchStep_AffidavitRequired_Title = new Text("Affidavit Required");
        /// <summary>
        /// Profession Required
        /// </summary>
        public static Text LaunchStep_ProfessionRequired_Title = new Text("Profession Required");
        /// <summary>
        /// Accreditation Disclaimer
        /// </summary>
        public static Text LaunchStep_Disclaimer_Title = new Text("Course and Accreditation Information");
        /// <summary>
        /// PreValidation Required
        /// </summary>
        public static Text LaunchStep_Prevalidation_Title = new Text("Prevalidation Is Required");

        public static Text LaunchStep_Confirmation_Title = new Text("Your Course Is Ready");
        public static Text LaunchStep_Confirmation_Content = new Text("All Launch Steps have completed. Please click the button to continue.");

        /// <summary>
        /// This course is intended for multiple professions. Please select your profession below:
        /// </summary>
        public static Text LaunchStep_ProfessionRequired_Info = new Text("This {!course} is intended for multiple professions. Please select your profession below:");

        /// <summary>
        /// This {!course} has not been accredited by any accreditation boards for your profession.
        /// </summary>
        public static Text LaunchStep_Disclaimer_None = new Text("This {!course} has not been accredited by any accreditation boards for your profession.");

        #endregion
        #region Event
        /// <summary>
        /// This event is a <b>{eventName}</b>
        /// </summary>
        public static Text Event_Details = new Text("This event is a <b>{eventName}</b>");
        #endregion

        #region Libraries 
        /// <summary>
        /// Training Program
        /// </summary>
        public static Text Library_TrainingProgramItemType = new Text("Training Program");
        /// <summary>
        /// Training Program
        /// </summary>
        public static Text Library_OnlineCourseItemType = new Text("Online Course");
        /// <summary>
        /// Favorite
        /// </summary>
        public static Text Library_FavoriteItem = new Text("Favorite (Click to remove from favorites.)");

        /// <summary>
        /// Favorite
        /// </summary>
        public static Text Library_UnFavoriteItem = new Text("Not a Favorite (Click to add to favorites.)");
        /// <summary>
        /// In Library
        /// </summary>
        public static Text Library_InLibrary = new Text("In Library");
        
        #endregion
        /// <summary>
        /// Print
        /// </summary>
        public static Text Print = new Text("Print");

    }
    /// <summary>
    /// Exetend the Text class in order allow dates to be included with any messages
    /// </summary>
    [DataContract(Name = "DateText")]
    [Serializable()]
    public class DateText : Text
    {
        private DateText(string value) : base(value) { }

        [DataMember(Name = "messageDate")]
        public DisplayDate MessageDate { get; set; }

        public static DateText Dynamic(DynamicDateText message, DisplayDate date)
        {
            DateText displayDateText = new DateText(message.Default);
            displayDateText.MessageDate = date;
            return displayDateText;
        }
    }
    /// <summary>
    /// Text always includes a date token to replace 
    /// Either {!date} or {!relative_date}
    /// </summary>
    [DataContract(Name = "DateText")]
    [Serializable()]
    public class DynamicDateText : DisplayValue
    {
        private DynamicDateText(string value) : base(value) { }
        /// <summary>
        /// Available in {!date_relative}
        /// </summary>
        public static DynamicDateText Course_DateAvailableRelative = new DynamicDateText("Available in {!date_relative}");
        /// <summary>
        /// Available {!date}
        /// </summary>
        public static DynamicDateText Course_DateAvailable = new DynamicDateText("Available {!date}");
    }
    [DataContract(Name = "Url")]
    [Serializable()]	
    public class Url : DisplayValue
    {
        [DataMember(Name = "method")]
        public string Method { get; set; }

        private Url(string value) : base(value) {
            Method = "GET";
        }

        private Url(string value, string method)
            : base(value)
        {
            Method = method;
        }
        /// <summary>
        /// ""
        /// </summary>
        public static Url Empty = new Url("");
        public static Url Dynamic(string value)
        {
            return new Url(value);
        }
        /// <summary>
        /// /Handlers/CertificateHandler.ashx
        /// </summary>
        public static Url ViewCertificate = new Url("/Handlers/CertificateHandler.ashx");
        /// <summary>
        /// /services/portal.svc/classes/
        /// </summary>
        public static Url Register = new Url("/services/portal.svc/classes/");
        /// <summary>
        /// /services/portal.svc/classes/unregister/
        /// </summary>
        public static Url Unregister = new Url("/services/portal.svc/classes/unregister/", "POST");
        /// <summary>
        /// AppSettings - LaunchPageURL
        /// </summary>
        public static Url Launch = new Url(ConfigurationManager.AppSettings["LaunchPageUrl"]);
        /// <summary>
        /// /services/portal.svc/classdetails/
        /// </summary>
        public static Url ClassDetails = new Url("/services/portal.svc/classdetails/");
        /// <summary>
        /// https://www2.gotomeeting.com/en_US/island/organizers/webinar/myWebinars.tmpl
        /// </summary>
        public static Url GTM = new Url("https://www2.gotomeeting.com/en_US/island/organizers/webinar/myWebinars.tmpl");
        /// <summary>
        /// /services/assignment.svc/assignments/
        /// </summary>
        public static Url LaunchAssignments = new Url("/assignments/");
        /// <summary>
        /// /VideoChat.aspx
        /// </summary>
        public static Url LaunchVideoChat = new Url("/VideoChat.aspx");
        /// <summary>
        /// /services/classroom.svc/messages/send/
        /// </summary>
        public static Url SendMessage = new Url("/services/classroom.svc/messages/send/", "POST");
        /// <summary>
        /// /services/library.svc/library/item/
        /// </summary>
        public static Url SaveLibraryFavorite = new Url("/services/library.svc/library/item/", "POST");
        /// <summary>
        /// /services/library.svc/library/item/
        /// </summary>
        public static Url DeleteLibraryFavorite = new Url("/services/library.svc/library/item/", "DELETE");
        /// <summary>
        /// /services/artisan.svc/courses/export/
        /// </summary>
        public static Url PrintCourse = new Url("/services/artisan.svc/courses/export/");
        /// <summary>
        /// /courseassignment.svc/launchstep/profession/
        /// </summary>
        public static Url SaveProfession = new Url("/services/courseassignment.svc/launchstep/profession/", "POST");
        /// <summary>
        /// /courseassignment.svc/launchstep/proctor/
        /// </summary>
        public static Url SaveProctor = new Url("/services/courseassignment.svc/launchstep/proctor/", "POST");
        /// <summary>
        /// /courseassignment.svc/launchstep/affidavit/
        /// </summary>
        public static Url SaveAffidavit = new Url("/services/courseassignment.svc/launchstep/affidavit/", "POST");
        /// <summary>
        /// /courseassignment.svc/launchstep/validation/
        /// </summary>
        public static Url SaveValidation = new Url("/services/courseassignment.svc/launchstep/validation/", "POST");
        /// <summary>
        /// /courseassignment.svc/launchstep/prevalidation/
        /// </summary>
        public static Url SavePreValidation = new Url("/services/courseassignment.svc/launchstep/prevalidation/", "POST");
    }

    [DataContract(Name = "JsonString")]
    [Serializable()]
    public class JsonString : DisplayValue
    {
        private JsonString(string value) : base(value) { }

        protected override bool IsValid(string value)
        {
            try
            {
                JToken.Parse(value);
                return true;
            }
            catch (Exception e)
            {
                // invalid json
            }
            return false;
        }
        /// <summary>
        /// Must be valid json!
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static JsonString Dynamic(string value)
        {
            return new JsonString(value);
        }

        private static JsonString _launchStep_CourseUnavailable;
        private static JsonString _launchStep_SessionExpired;
        private static JsonString _launchStep_ValidationRequired;
        private static JsonString _launchStep_ProctorRequired;
        private static JsonString _launchStep_PrevalidationRequired;

        /// <summary>
        /// Course unavailable window
        /// </summary>
        public static JsonString LaunchStep_CourseUnavailable
        {
            get
            {
                if (_launchStep_CourseUnavailable == null)
                {
                    JObject panel = new JObject();
                    panel["html"] = Text.LaunchStep_CourseUnavailable_Content.Value;
                    panel["htmlCode"] = Text.LaunchStep_CourseUnavailable_Content.Code;
                    
                    panel["iconCls"] = "x-window-error";
                    JObject container = new JObject();
                    container["items"] = new JArray(panel);
                    container["width"] = 250;
                    container["height"] = 120;

                    _launchStep_CourseUnavailable = new JsonString(container.ToString());
                }

                return _launchStep_CourseUnavailable;
            }
        }

        /// <summary>
        /// Session expired window
        /// </summary>
        public static JsonString LaunchStep_SessionExpired
        {
            get
            {
                if (_launchStep_SessionExpired == null)
                {
                    JObject panel = new JObject();
                    panel["html"] = Text.LaunchStep_SessionExpired_Content.Value;
                    panel["htmlCode"] = Text.LaunchStep_SessionExpired_Content.Code;
                    
                    panel["iconCls"] = "x-window-critical";
                    JObject container = new JObject();
                    container["items"] = new JArray(panel);
                    container["width"] = 300;
                    container["height"] = 150;

                    _launchStep_SessionExpired = new JsonString(container.ToString());
                }

                return _launchStep_SessionExpired;
            }
        }

        /// <summary>
        /// Validation required window
        /// </summary>
        public static JsonString LaunchStep_ValidationRequired
        {
            get
            {
                if (_launchStep_ValidationRequired == null)
                {
                    JArray items = new JArray();

                    JObject panel = new JObject();
                    panel["html"] = Text.LaunchStep_ValidationRequired_Content.Value;
                    panel["htmlCode"] = Text.LaunchStep_ValidationRequired_Content.Code;
                    panel["iconCls"] = "x-window-error";
                    panel["border"] = false;
                    

                    JObject ssn = new JObject();
                    ssn["xtype"] = "textfield";
                    ssn["fieldLabel"] = "Social Security Number";
                    ssn["name"] = "ssn";
                    ssn["allowBlank"] = false;

                    JObject date = new JObject();
                    date["xtype"] = "datefield";
                    date["fieldLabel"] = "Date of Birth (MM/DD/YYYY)";
                    date["name"] = "dob";
                    date["allowBlank"] = false;
					date["isUseParseDateLong"] = true;
                    //date["format"] = "d/m/y";

                    items.Add(panel);
                    items.Add(ssn);
                    items.Add(date);

                    JObject container = new JObject();
                    container["items"] = items;
                    container["width"] = 410;
                    container["height"] = 247;

                    _launchStep_ValidationRequired = new JsonString(container.ToString());
                }

                return _launchStep_ValidationRequired;
            }
        }

        /// <summary>
        /// Proctor required window
        /// </summary>
        public static JsonString LaunchStep_ProctorRequired
        {
            get
            {
                if (_launchStep_ProctorRequired == null)
                {
                    JArray items = new JArray();

                    JObject label = new JObject();
                    label["xtype"] = "label";
                    label["text"] = Text.LaunchStep_ProctorRequired_Content.Value;
                    label["textCode"] = Text.LaunchStep_ProctorRequired_Content.Code;
                    label["cls"] = "x-form-item";

                    JObject name = new JObject();
                    name["xtype"] = "textfield";
                    name["fieldLabel"] = "Name";
                    name["name"] = "name";
                    name["allowBlank"] = false;
                    name["itemCls"] = "required";

                    JObject code = new JObject();
                    code["xtype"] = "textfield";
                    code["fieldLabel"] = "Code";
                    code["name"] = "proctorCode";
                    code["allowBlank"] = true;

                    JObject location = new JObject();
                    location["xtype"] = "textfield";
                    location["fieldLabel"] = "Location";
                    location["name"] = "location";
                    location["allowBlank"] = false;
                    location["itemCls"] = "required";

                    JObject authorized = new JObject();
                    authorized["xtype"] = "checkbox";
                    authorized["fieldLabel"] = "";
                    authorized["boxLabel"] = "I am an authorized proctor.";
                    authorized["name"] = "isAuthorized";
                    authorized["allowBlank"] = false;

                    items.Add(label);
                    items.Add(name);
                    items.Add(code);
                    items.Add(location);
                    items.Add(authorized);


                    JObject container = new JObject();
                    container["items"] = items;
                    container["width"] = 330;
                    container["height"] = 205;


                    _launchStep_ProctorRequired = new JsonString(container.ToString());
                }

                return _launchStep_ProctorRequired;
            }
        }

         /// <summary>
        /// Prevalidation required window
        /// </summary>
        public static JsonString LaunchStep_PrevalidationRequired
        {
            get
            {
                if (_launchStep_PrevalidationRequired == null)
                {
                    JArray items = new JArray();

                    JObject container = new JObject();
                    container["items"] = items;
                    container["width"] = 330;
                    container["height"] = 205;

                    _launchStep_PrevalidationRequired = new JsonString(container.ToString());
                }

                return _launchStep_PrevalidationRequired;
            }
        }
    }

    [DataContract(Name = "textFormat")]
    [Serializable()]
    public class TextFormat : DisplayValue
    {
        private TextFormat(string value) : base(value) { }

        
        /// <summary>
        /// {base}&registration=UserId|{UserID}!CourseId|{CourseID}!TrainingProgramId|{TrainingProgramID}
        /// </summary>
        public static TextFormat LaunchLinkUrlFormat = new TextFormat("{base}&registration=UserId|{UserID}!CourseId|{CourseID}!TrainingProgramId|{TrainingProgramID}");
        /// <summary>
        /// launch-link-{CourseID}-{TrainingProgramID}
        /// </summary>
        public static TextFormat LaunchLinkIdFormat = new TextFormat("launch-link-{CourseID}-{TrainingProgramID}");
        /// <summary>
        /// method_launch course-launch-link
        /// </summary>
        public static TextFormat LaunchLinkClassFormat = new TextFormat("method_launch course-launch-link");
        /// <summary>
        /// method_register register-course-link
        /// </summary>
        public static TextFormat RegisterLinkClassFormat = new TextFormat("method_register register-course-link");
        /// <summary>
        /// method_unregister register-course-link
        /// </summary>
        public static TextFormat UnregisterLinkClassFormat = new TextFormat("method_unregister unregister-course-link");
        /// <summary>
        /// method_togglefavorite toggle-favorite-link
        /// </summary>
        public static TextFormat ToggleFavoriteClassFormat = new TextFormat("method_togglefavorite toggle-favorite-link");
        /// <summary>
        ///  {base}{RegistrationID}
        /// </summary>
        public static TextFormat UnregisterLinkUrlFormat = new TextFormat("{base}{RegistrationID}");
        /// <summary>
        ///  {base}{ClassID}
        /// </summary>
        public static TextFormat ClassDetailsLinkUrlFormat = new TextFormat("{base}{ClassID}");  
        /// <summary>
        ///  method_classdetails class-details-link
        /// </summary>
        public static TextFormat ClassDetailsLinkClassFormat = new TextFormat("method_classdetails class-details-link");
        /// <summary>
        ///  method_classdetails class-details-link
        /// </summary>
        public static TextFormat CertificateLinkClassFormat = new TextFormat("method_launchcertificate certificate-link");
        /// <summary>
        /// {base}
        /// </summary>
        public static TextFormat GTMLaunchLinkUrlFormat = new TextFormat("{base}");
        /// <summary>
        ///  method_launchgtm gtm-launch-link
        /// </summary>
        public static TextFormat GTMLaunchLinkClassFormat = new TextFormat("method_launchgtm gtm-launch-link");
        /// <summary>
        /// {base}{TrainingProgramID}/{CourseID}/{UserID}
        /// </summary>
        public static TextFormat ViewAssignmentsUrlFormat = new TextFormat("{base}{TrainingProgramID}/{CourseID}/");
        /// <summary>
        /// method_launchassignments launch-assignments-link
        /// </summary>
        public static TextFormat ViewAssignmentsClassFormat = new TextFormat("method_launchassignments launch-assignments-link");
        /// <summary>
        /// {base}?userId={userId}&invite=true
        /// </summary>
        public static TextFormat LaunchVideoChatUrlFormat = new TextFormat("{base}?userId={userId}&invite=true");
        /// <summary>
        /// {base}
        /// </summary>
        public static TextFormat SendMessageUrlFormat = new TextFormat("{base}");
        /// <summary>
        /// {base}/{itemId}/user/{userId}
        /// </summary>
        public static TextFormat LibraryFavoriteFormat = new TextFormat("{base}{itemId}/user/{userId}");
        /// <summary>
        /// {base}{ArtisanCourseID}
        /// </summary>
        public static TextFormat PrintCourseFormat = new TextFormat("{base}{ArtisanCourseID}?contentOnly=true");
    }

}
