﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;
using Symphony.Core.Controllers;

namespace Symphony.Core.Data
{
    public class ActiveRecordCustom<T> : ActiveRecord<T> where T : ReadOnlyRecord<T>, new()
    {
        protected override void BeforeInsert()
        {
            SymphonyController c = new SymphonyController();

            if (this.GetSchema().Columns.Contains("CreatedByUserId"))
            {
                this.SetColumnValue("CreatedByUserId", c.UserID);
            }

            if (this.GetSchema().Columns.Contains("CreatedByActualUserId"))
            {
                this.SetColumnValue("CreatedByActualUserId", c.ActualUserID);
            }

            base.BeforeInsert();
        }

        protected override void BeforeUpdate()
        {
            SymphonyController c = new SymphonyController();

            if (this.GetSchema().Columns.Contains("ModifiedByUserId"))
            {
                this.SetColumnValue("ModifiedByUserId", c.UserID);
            }

            if (this.GetSchema().Columns.Contains("ModifiedByActualUserId"))
            {
                this.SetColumnValue("ModifiedByActualUserId", c.ActualUserID);
            }

            base.BeforeUpdate();
        }
    }
}
