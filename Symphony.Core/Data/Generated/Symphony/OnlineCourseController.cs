using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
    /// <summary>
    /// Controller class for OnlineCourse
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class OnlineCourseController
    {
        // Preload our schema..
        OnlineCourse thisSchemaLoad = new OnlineCourse();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public OnlineCourseCollection FetchAll()
        {
            OnlineCourseCollection coll = new OnlineCourseCollection();
            Query qry = new Query(OnlineCourse.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public OnlineCourseCollection FetchByID(object Id)
        {
            OnlineCourseCollection coll = new OnlineCourseCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public OnlineCourseCollection FetchByQuery(Query qry)
        {
            OnlineCourseCollection coll = new OnlineCourseCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (OnlineCourse.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (OnlineCourse.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int CustomerID,string Name,string Description,string Keywords,decimal Cost,decimal Credit,bool PublicIndicator,bool IsSurvey,bool IsTest,string ModifiedBy,string CreatedBy,DateTime? ModifiedOn,DateTime? CreatedOn,bool Active,int DuplicateFromID,int Version,string SearchText,string InternalCode,int CategoryID,int? ArtisanCourseID,bool? ShowPretestOverride,bool? ShowPostTestOverride,int? PassingScoreOverride,int? SkinOverride,int? PreTestTypeOverride,int? PostTestTypeOverride,int? NavigationMethodOverride,string DisclaimerOverride,string ContactEmailOverride,bool? ShowObjectivesOverride,bool? PretestTestOutOverride,int RetestMode,int? Retries,int CreatedByUserId,int ModifiedByUserId,int? CreatedByActualUserId,int? ModifiedByActualUserId,int? PublishedArtisanCourseID,bool ProctorRequiredIndicator,bool? RandomizeQuestionsOverride,bool? RandomizeAnswersOverride,int? ReviewMethodOverride,int? MarkingMethodOverride,int? InactivityMethodOverride,int? InactivityTimeoutOverride,int? MinimumPageTimeOverride,int? MaximumQuestionTimeOverride,bool? IsValidationEnabled,int? ValidationInterval,bool? CertificateEnabledOverride,string ExternalID,bool CertificateEnabled,int? MaxTimeOverride,string CertificatePath,int? MinLoTimeOverride,int? MinScoTimeOverride,int? MinTimeOverride,string MetaDataJson,int? Duration,bool IsUseAutomaticDuration,int AutomaticDuration,string AssociatedImageData,bool? IsForceLogoutOverride,int ContentTypeID,int? CertificateTemplateID,int? ThemeFlavorOverrideID,int? BookmarkingMethodOverride)
	    {
		    OnlineCourse item = new OnlineCourse();
		    
            item.CustomerID = CustomerID;
            
            item.Name = Name;
            
            item.Description = Description;
            
            item.Keywords = Keywords;
            
            item.Cost = Cost;
            
            item.Credit = Credit;
            
            item.PublicIndicator = PublicIndicator;
            
            item.IsSurvey = IsSurvey;
            
            item.IsTest = IsTest;
            
            item.ModifiedBy = ModifiedBy;
            
            item.CreatedBy = CreatedBy;
            
            item.ModifiedOn = ModifiedOn;
            
            item.CreatedOn = CreatedOn;
            
            item.Active = Active;
            
            item.DuplicateFromID = DuplicateFromID;
            
            item.Version = Version;
            
            item.SearchText = SearchText;
            
            item.InternalCode = InternalCode;
            
            item.CategoryID = CategoryID;
            
            item.ArtisanCourseID = ArtisanCourseID;
            
            item.ShowPretestOverride = ShowPretestOverride;
            
            item.ShowPostTestOverride = ShowPostTestOverride;
            
            item.PassingScoreOverride = PassingScoreOverride;
            
            item.SkinOverride = SkinOverride;
            
            item.PreTestTypeOverride = PreTestTypeOverride;
            
            item.PostTestTypeOverride = PostTestTypeOverride;
            
            item.NavigationMethodOverride = NavigationMethodOverride;
            
            item.DisclaimerOverride = DisclaimerOverride;
            
            item.ContactEmailOverride = ContactEmailOverride;
            
            item.ShowObjectivesOverride = ShowObjectivesOverride;
            
            item.PretestTestOutOverride = PretestTestOutOverride;
            
            item.RetestMode = RetestMode;
            
            item.Retries = Retries;
            
            item.CreatedByUserId = CreatedByUserId;
            
            item.ModifiedByUserId = ModifiedByUserId;
            
            item.CreatedByActualUserId = CreatedByActualUserId;
            
            item.ModifiedByActualUserId = ModifiedByActualUserId;
            
            item.PublishedArtisanCourseID = PublishedArtisanCourseID;
            
            item.ProctorRequiredIndicator = ProctorRequiredIndicator;
            
            item.RandomizeQuestionsOverride = RandomizeQuestionsOverride;
            
            item.RandomizeAnswersOverride = RandomizeAnswersOverride;
            
            item.ReviewMethodOverride = ReviewMethodOverride;
            
            item.MarkingMethodOverride = MarkingMethodOverride;
            
            item.InactivityMethodOverride = InactivityMethodOverride;
            
            item.InactivityTimeoutOverride = InactivityTimeoutOverride;
            
            item.MinimumPageTimeOverride = MinimumPageTimeOverride;
            
            item.MaximumQuestionTimeOverride = MaximumQuestionTimeOverride;
            
            item.IsValidationEnabled = IsValidationEnabled;
            
            item.ValidationInterval = ValidationInterval;
            
            item.CertificateEnabledOverride = CertificateEnabledOverride;
            
            item.ExternalID = ExternalID;
            
            item.CertificateEnabled = CertificateEnabled;
            
            item.MaxTimeOverride = MaxTimeOverride;
            
            item.CertificatePath = CertificatePath;
            
            item.MinLoTimeOverride = MinLoTimeOverride;
            
            item.MinScoTimeOverride = MinScoTimeOverride;
            
            item.MinTimeOverride = MinTimeOverride;
            
            item.MetaDataJson = MetaDataJson;
            
            item.Duration = Duration;
            
            item.IsUseAutomaticDuration = IsUseAutomaticDuration;
            
            item.AutomaticDuration = AutomaticDuration;
            
            item.AssociatedImageData = AssociatedImageData;
            
            item.IsForceLogoutOverride = IsForceLogoutOverride;
            
            item.ContentTypeID = ContentTypeID;
            
            item.CertificateTemplateID = CertificateTemplateID;
            
            item.ThemeFlavorOverrideID = ThemeFlavorOverrideID;
            
            item.BookmarkingMethodOverride = BookmarkingMethodOverride;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,int CustomerID,string Name,string Description,string Keywords,decimal Cost,decimal Credit,bool PublicIndicator,bool IsSurvey,bool IsTest,string ModifiedBy,string CreatedBy,DateTime? ModifiedOn,DateTime? CreatedOn,bool Active,int DuplicateFromID,int Version,string SearchText,string InternalCode,int CategoryID,int? ArtisanCourseID,bool? ShowPretestOverride,bool? ShowPostTestOverride,int? PassingScoreOverride,int? SkinOverride,int? PreTestTypeOverride,int? PostTestTypeOverride,int? NavigationMethodOverride,string DisclaimerOverride,string ContactEmailOverride,bool? ShowObjectivesOverride,bool? PretestTestOutOverride,int RetestMode,int? Retries,int CreatedByUserId,int ModifiedByUserId,int? CreatedByActualUserId,int? ModifiedByActualUserId,int? PublishedArtisanCourseID,bool ProctorRequiredIndicator,bool? RandomizeQuestionsOverride,bool? RandomizeAnswersOverride,int? ReviewMethodOverride,int? MarkingMethodOverride,int? InactivityMethodOverride,int? InactivityTimeoutOverride,int? MinimumPageTimeOverride,int? MaximumQuestionTimeOverride,bool? IsValidationEnabled,int? ValidationInterval,bool? CertificateEnabledOverride,string ExternalID,bool CertificateEnabled,int? MaxTimeOverride,string CertificatePath,int? MinLoTimeOverride,int? MinScoTimeOverride,int? MinTimeOverride,string MetaDataJson,int? Duration,bool IsUseAutomaticDuration,int AutomaticDuration,string AssociatedImageData,bool? IsForceLogoutOverride,int ContentTypeID,int? CertificateTemplateID,int? ThemeFlavorOverrideID,int? BookmarkingMethodOverride)
	    {
		    OnlineCourse item = new OnlineCourse();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.CustomerID = CustomerID;
				
			item.Name = Name;
				
			item.Description = Description;
				
			item.Keywords = Keywords;
				
			item.Cost = Cost;
				
			item.Credit = Credit;
				
			item.PublicIndicator = PublicIndicator;
				
			item.IsSurvey = IsSurvey;
				
			item.IsTest = IsTest;
				
			item.ModifiedBy = ModifiedBy;
				
			item.CreatedBy = CreatedBy;
				
			item.ModifiedOn = ModifiedOn;
				
			item.CreatedOn = CreatedOn;
				
			item.Active = Active;
				
			item.DuplicateFromID = DuplicateFromID;
				
			item.Version = Version;
				
			item.SearchText = SearchText;
				
			item.InternalCode = InternalCode;
				
			item.CategoryID = CategoryID;
				
			item.ArtisanCourseID = ArtisanCourseID;
				
			item.ShowPretestOverride = ShowPretestOverride;
				
			item.ShowPostTestOverride = ShowPostTestOverride;
				
			item.PassingScoreOverride = PassingScoreOverride;
				
			item.SkinOverride = SkinOverride;
				
			item.PreTestTypeOverride = PreTestTypeOverride;
				
			item.PostTestTypeOverride = PostTestTypeOverride;
				
			item.NavigationMethodOverride = NavigationMethodOverride;
				
			item.DisclaimerOverride = DisclaimerOverride;
				
			item.ContactEmailOverride = ContactEmailOverride;
				
			item.ShowObjectivesOverride = ShowObjectivesOverride;
				
			item.PretestTestOutOverride = PretestTestOutOverride;
				
			item.RetestMode = RetestMode;
				
			item.Retries = Retries;
				
			item.CreatedByUserId = CreatedByUserId;
				
			item.ModifiedByUserId = ModifiedByUserId;
				
			item.CreatedByActualUserId = CreatedByActualUserId;
				
			item.ModifiedByActualUserId = ModifiedByActualUserId;
				
			item.PublishedArtisanCourseID = PublishedArtisanCourseID;
				
			item.ProctorRequiredIndicator = ProctorRequiredIndicator;
				
			item.RandomizeQuestionsOverride = RandomizeQuestionsOverride;
				
			item.RandomizeAnswersOverride = RandomizeAnswersOverride;
				
			item.ReviewMethodOverride = ReviewMethodOverride;
				
			item.MarkingMethodOverride = MarkingMethodOverride;
				
			item.InactivityMethodOverride = InactivityMethodOverride;
				
			item.InactivityTimeoutOverride = InactivityTimeoutOverride;
				
			item.MinimumPageTimeOverride = MinimumPageTimeOverride;
				
			item.MaximumQuestionTimeOverride = MaximumQuestionTimeOverride;
				
			item.IsValidationEnabled = IsValidationEnabled;
				
			item.ValidationInterval = ValidationInterval;
				
			item.CertificateEnabledOverride = CertificateEnabledOverride;
				
			item.ExternalID = ExternalID;
				
			item.CertificateEnabled = CertificateEnabled;
				
			item.MaxTimeOverride = MaxTimeOverride;
				
			item.CertificatePath = CertificatePath;
				
			item.MinLoTimeOverride = MinLoTimeOverride;
				
			item.MinScoTimeOverride = MinScoTimeOverride;
				
			item.MinTimeOverride = MinTimeOverride;
				
			item.MetaDataJson = MetaDataJson;
				
			item.Duration = Duration;
				
			item.IsUseAutomaticDuration = IsUseAutomaticDuration;
				
			item.AutomaticDuration = AutomaticDuration;
				
			item.AssociatedImageData = AssociatedImageData;
				
			item.IsForceLogoutOverride = IsForceLogoutOverride;
				
			item.ContentTypeID = ContentTypeID;
				
			item.CertificateTemplateID = CertificateTemplateID;
				
			item.ThemeFlavorOverrideID = ThemeFlavorOverrideID;
				
			item.BookmarkingMethodOverride = BookmarkingMethodOverride;
				
	        item.Save(UserName);
	    }
    }
}
