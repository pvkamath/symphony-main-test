using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
	/// <summary>
	/// Strongly-typed collection for the ReportTemplatePermCustomer class.
	/// </summary>
    [Serializable]
	public partial class ReportTemplatePermCustomerCollection : ActiveList<ReportTemplatePermCustomer, ReportTemplatePermCustomerCollection>
	{	   
		public ReportTemplatePermCustomerCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ReportTemplatePermCustomerCollection</returns>
		public ReportTemplatePermCustomerCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ReportTemplatePermCustomer o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the ReportTemplatePermCustomers table.
	/// </summary>
	[Serializable]
	public partial class ReportTemplatePermCustomer : Symphony.Core.Data.ActiveRecordCustom<ReportTemplatePermCustomer>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public ReportTemplatePermCustomer()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ReportTemplatePermCustomer(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public ReportTemplatePermCustomer(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public ReportTemplatePermCustomer(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("ReportTemplatePermCustomers", TableType.Table, DataService.GetInstance("Symphony"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarReportTemplateId = new TableSchema.TableColumn(schema);
				colvarReportTemplateId.ColumnName = "ReportTemplateId";
				colvarReportTemplateId.DataType = DbType.Int32;
				colvarReportTemplateId.MaxLength = 0;
				colvarReportTemplateId.AutoIncrement = false;
				colvarReportTemplateId.IsNullable = true;
				colvarReportTemplateId.IsPrimaryKey = false;
				colvarReportTemplateId.IsForeignKey = true;
				colvarReportTemplateId.IsReadOnly = false;
				colvarReportTemplateId.DefaultSetting = @"";
				
					colvarReportTemplateId.ForeignKeyTableName = "ReportTemplate";
				schema.Columns.Add(colvarReportTemplateId);
				
				TableSchema.TableColumn colvarCustomerId = new TableSchema.TableColumn(schema);
				colvarCustomerId.ColumnName = "CustomerId";
				colvarCustomerId.DataType = DbType.Int32;
				colvarCustomerId.MaxLength = 0;
				colvarCustomerId.AutoIncrement = false;
				colvarCustomerId.IsNullable = true;
				colvarCustomerId.IsPrimaryKey = false;
				colvarCustomerId.IsForeignKey = true;
				colvarCustomerId.IsReadOnly = false;
				colvarCustomerId.DefaultSetting = @"";
				
					colvarCustomerId.ForeignKeyTableName = "Customer";
				schema.Columns.Add(colvarCustomerId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["Symphony"].AddSchema("ReportTemplatePermCustomers",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ReportTemplateId")]
		[Bindable(true)]
		public int? ReportTemplateId 
		{
			get { return GetColumnValue<int?>(Columns.ReportTemplateId); }
			set { SetColumnValue(Columns.ReportTemplateId, value); }
		}
		  
		[XmlAttribute("CustomerId")]
		[Bindable(true)]
		public int? CustomerId 
		{
			get { return GetColumnValue<int?>(Columns.CustomerId); }
			set { SetColumnValue(Columns.CustomerId, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a Customer ActiveRecord object related to this ReportTemplatePermCustomer
		/// </summary>
		private Symphony.Core.Data.Customer _Customer;
		public Symphony.Core.Data.Customer Customer
		{
			get { 
			    if(_Customer == null){
			        _Customer = Symphony.Core.Data.Customer.FetchByID(this.CustomerId); 
			    }
			    return _Customer;
			}
			set { 
			    SetColumnValue("CustomerId", value.Id); 
			    _Customer = value;
			}
		}
		
		
		/// <summary>
		/// Returns a ReportTemplate ActiveRecord object related to this ReportTemplatePermCustomer
		/// </summary>
		private Symphony.Core.Data.ReportTemplate _ReportTemplate;
		public Symphony.Core.Data.ReportTemplate ReportTemplate
		{
			get { 
			    if(_ReportTemplate == null){
			        _ReportTemplate = Symphony.Core.Data.ReportTemplate.FetchByID(this.ReportTemplateId); 
			    }
			    return _ReportTemplate;
			}
			set { 
			    SetColumnValue("ReportTemplateId", value.Id); 
			    _ReportTemplate = value;
			}
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int? varReportTemplateId,int? varCustomerId)
		{
			ReportTemplatePermCustomer item = new ReportTemplatePermCustomer();
			
			item.ReportTemplateId = varReportTemplateId;
			
			item.CustomerId = varCustomerId;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,int? varReportTemplateId,int? varCustomerId)
		{
			ReportTemplatePermCustomer item = new ReportTemplatePermCustomer();
			
				item.Id = varId;
			
				item.ReportTemplateId = varReportTemplateId;
			
				item.CustomerId = varCustomerId;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ReportTemplateIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CustomerIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string ReportTemplateId = @"ReportTemplateId";
			 public static string CustomerId = @"CustomerId";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
