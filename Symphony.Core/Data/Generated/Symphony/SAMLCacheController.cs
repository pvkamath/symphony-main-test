using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
    /// <summary>
    /// Controller class for SAMLCache
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class SAMLCacheController
    {
        // Preload our schema..
        SAMLCache thisSchemaLoad = new SAMLCache();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public SAMLCacheCollection FetchAll()
        {
            SAMLCacheCollection coll = new SAMLCacheCollection();
            Query qry = new Query(SAMLCache.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public SAMLCacheCollection FetchByID(object Id)
        {
            SAMLCacheCollection coll = new SAMLCacheCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public SAMLCacheCollection FetchByQuery(Query qry)
        {
            SAMLCacheCollection coll = new SAMLCacheCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (SAMLCache.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (SAMLCache.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Key,string ValueX,long? ValidDurationInMS,DateTime CreatedOn,DateTime ModifiedOn,string CreatedBy,string ModifiedBy,DateTime SQLTime)
	    {
		    SAMLCache item = new SAMLCache();
		    
            item.Key = Key;
            
            item.ValueX = ValueX;
            
            item.ValidDurationInMS = ValidDurationInMS;
            
            item.CreatedOn = CreatedOn;
            
            item.ModifiedOn = ModifiedOn;
            
            item.CreatedBy = CreatedBy;
            
            item.ModifiedBy = ModifiedBy;
            
            item.SQLTime = SQLTime;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,string Key,string ValueX,long? ValidDurationInMS,DateTime CreatedOn,DateTime ModifiedOn,string CreatedBy,string ModifiedBy,DateTime SQLTime)
	    {
		    SAMLCache item = new SAMLCache();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.Key = Key;
				
			item.ValueX = ValueX;
				
			item.ValidDurationInMS = ValidDurationInMS;
				
			item.CreatedOn = CreatedOn;
				
			item.ModifiedOn = ModifiedOn;
				
			item.CreatedBy = CreatedBy;
				
			item.ModifiedBy = ModifiedBy;
				
			item.SQLTime = SQLTime;
				
	        item.Save(UserName);
	    }
    }
}
