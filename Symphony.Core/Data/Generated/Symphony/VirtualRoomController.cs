using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
    /// <summary>
    /// Controller class for VirtualRoom
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class VirtualRoomController
    {
        // Preload our schema..
        VirtualRoom thisSchemaLoad = new VirtualRoom();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public VirtualRoomCollection FetchAll()
        {
            VirtualRoomCollection coll = new VirtualRoomCollection();
            Query qry = new Query(VirtualRoom.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public VirtualRoomCollection FetchByID(object Id)
        {
            VirtualRoomCollection coll = new VirtualRoomCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public VirtualRoomCollection FetchByQuery(Query qry)
        {
            VirtualRoomCollection coll = new VirtualRoomCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (VirtualRoom.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (VirtualRoom.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int ClassDateID,int GTMMeetingID,string CreatedBy,string ModifiedBy,DateTime CreatedOn,DateTime ModifiedOn,int CreatedByUserId,int ModifiedByUserId,int? CreatedByActualUserId,int? ModifiedByActualUserId)
	    {
		    VirtualRoom item = new VirtualRoom();
		    
            item.ClassDateID = ClassDateID;
            
            item.GTMMeetingID = GTMMeetingID;
            
            item.CreatedBy = CreatedBy;
            
            item.ModifiedBy = ModifiedBy;
            
            item.CreatedOn = CreatedOn;
            
            item.ModifiedOn = ModifiedOn;
            
            item.CreatedByUserId = CreatedByUserId;
            
            item.ModifiedByUserId = ModifiedByUserId;
            
            item.CreatedByActualUserId = CreatedByActualUserId;
            
            item.ModifiedByActualUserId = ModifiedByActualUserId;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,int ClassDateID,int GTMMeetingID,string CreatedBy,string ModifiedBy,DateTime CreatedOn,DateTime ModifiedOn,int CreatedByUserId,int ModifiedByUserId,int? CreatedByActualUserId,int? ModifiedByActualUserId)
	    {
		    VirtualRoom item = new VirtualRoom();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.ClassDateID = ClassDateID;
				
			item.GTMMeetingID = GTMMeetingID;
				
			item.CreatedBy = CreatedBy;
				
			item.ModifiedBy = ModifiedBy;
				
			item.CreatedOn = CreatedOn;
				
			item.ModifiedOn = ModifiedOn;
				
			item.CreatedByUserId = CreatedByUserId;
				
			item.ModifiedByUserId = ModifiedByUserId;
				
			item.CreatedByActualUserId = CreatedByActualUserId;
				
			item.ModifiedByActualUserId = ModifiedByActualUserId;
				
	        item.Save(UserName);
	    }
    }
}
