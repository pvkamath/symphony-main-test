using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
	/// <summary>
	/// Strongly-typed collection for the ReportEntity class.
	/// </summary>
    [Serializable]
	public partial class ReportEntityCollection : ActiveList<ReportEntity, ReportEntityCollection>
	{	   
		public ReportEntityCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ReportEntityCollection</returns>
		public ReportEntityCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ReportEntity o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the ReportEntity table.
	/// </summary>
	[Serializable]
	public partial class ReportEntity : Symphony.Core.Data.ActiveRecordCustom<ReportEntity>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public ReportEntity()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ReportEntity(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public ReportEntity(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public ReportEntity(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("ReportEntity", TableType.Table, DataService.GetInstance("Symphony"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "Name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 64;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = false;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);
				
				TableSchema.TableColumn colvarXtype = new TableSchema.TableColumn(schema);
				colvarXtype.ColumnName = "Xtype";
				colvarXtype.DataType = DbType.String;
				colvarXtype.MaxLength = 128;
				colvarXtype.AutoIncrement = false;
				colvarXtype.IsNullable = false;
				colvarXtype.IsPrimaryKey = false;
				colvarXtype.IsForeignKey = false;
				colvarXtype.IsReadOnly = false;
				colvarXtype.DefaultSetting = @"";
				colvarXtype.ForeignKeyTableName = "";
				schema.Columns.Add(colvarXtype);
				
				TableSchema.TableColumn colvarConfig = new TableSchema.TableColumn(schema);
				colvarConfig.ColumnName = "Config";
				colvarConfig.DataType = DbType.String;
				colvarConfig.MaxLength = -1;
				colvarConfig.AutoIncrement = false;
				colvarConfig.IsNullable = true;
				colvarConfig.IsPrimaryKey = false;
				colvarConfig.IsForeignKey = false;
				colvarConfig.IsReadOnly = false;
				colvarConfig.DefaultSetting = @"";
				colvarConfig.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConfig);
				
				TableSchema.TableColumn colvarDefaultOrder = new TableSchema.TableColumn(schema);
				colvarDefaultOrder.ColumnName = "DefaultOrder";
				colvarDefaultOrder.DataType = DbType.Int32;
				colvarDefaultOrder.MaxLength = 0;
				colvarDefaultOrder.AutoIncrement = false;
				colvarDefaultOrder.IsNullable = false;
				colvarDefaultOrder.IsPrimaryKey = false;
				colvarDefaultOrder.IsForeignKey = false;
				colvarDefaultOrder.IsReadOnly = false;
				
						colvarDefaultOrder.DefaultSetting = @"((1000))";
				colvarDefaultOrder.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDefaultOrder);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "Description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = -1;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarIsDeprecated = new TableSchema.TableColumn(schema);
				colvarIsDeprecated.ColumnName = "IsDeprecated";
				colvarIsDeprecated.DataType = DbType.Boolean;
				colvarIsDeprecated.MaxLength = 0;
				colvarIsDeprecated.AutoIncrement = false;
				colvarIsDeprecated.IsNullable = false;
				colvarIsDeprecated.IsPrimaryKey = false;
				colvarIsDeprecated.IsForeignKey = false;
				colvarIsDeprecated.IsReadOnly = false;
				
						colvarIsDeprecated.DefaultSetting = @"((0))";
				colvarIsDeprecated.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDeprecated);
				
				TableSchema.TableColumn colvarReportEntityTypeID = new TableSchema.TableColumn(schema);
				colvarReportEntityTypeID.ColumnName = "ReportEntityTypeID";
				colvarReportEntityTypeID.DataType = DbType.Int32;
				colvarReportEntityTypeID.MaxLength = 0;
				colvarReportEntityTypeID.AutoIncrement = false;
				colvarReportEntityTypeID.IsNullable = true;
				colvarReportEntityTypeID.IsPrimaryKey = false;
				colvarReportEntityTypeID.IsForeignKey = false;
				colvarReportEntityTypeID.IsReadOnly = false;
				colvarReportEntityTypeID.DefaultSetting = @"";
				colvarReportEntityTypeID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReportEntityTypeID);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["Symphony"].AddSchema("ReportEntity",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name 
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}
		  
		[XmlAttribute("Xtype")]
		[Bindable(true)]
		public string Xtype 
		{
			get { return GetColumnValue<string>(Columns.Xtype); }
			set { SetColumnValue(Columns.Xtype, value); }
		}
		  
		[XmlAttribute("Config")]
		[Bindable(true)]
		public string Config 
		{
			get { return GetColumnValue<string>(Columns.Config); }
			set { SetColumnValue(Columns.Config, value); }
		}
		  
		[XmlAttribute("DefaultOrder")]
		[Bindable(true)]
		public int DefaultOrder 
		{
			get { return GetColumnValue<int>(Columns.DefaultOrder); }
			set { SetColumnValue(Columns.DefaultOrder, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("IsDeprecated")]
		[Bindable(true)]
		public bool IsDeprecated 
		{
			get { return GetColumnValue<bool>(Columns.IsDeprecated); }
			set { SetColumnValue(Columns.IsDeprecated, value); }
		}
		  
		[XmlAttribute("ReportEntityTypeID")]
		[Bindable(true)]
		public int? ReportEntityTypeID 
		{
			get { return GetColumnValue<int?>(Columns.ReportEntityTypeID); }
			set { SetColumnValue(Columns.ReportEntityTypeID, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
				
		private Symphony.Core.Data.ReportTemplateEntityCollection colReportTemplateEntities;
		public Symphony.Core.Data.ReportTemplateEntityCollection ReportTemplateEntities
		{
			get
			{
				if(colReportTemplateEntities == null)
				{
					colReportTemplateEntities = new Symphony.Core.Data.ReportTemplateEntityCollection().Where(ReportTemplateEntity.Columns.ReportEntityID, Id).Load();
					colReportTemplateEntities.ListChanged += new ListChangedEventHandler(colReportTemplateEntities_ListChanged);
				}
				return colReportTemplateEntities;			
			}
			set 
			{ 
					colReportTemplateEntities = value; 
					colReportTemplateEntities.ListChanged += new ListChangedEventHandler(colReportTemplateEntities_ListChanged);
			}
		}
		
		void colReportTemplateEntities_ListChanged(object sender, ListChangedEventArgs e)
		{
		    if (e.ListChangedType == ListChangedType.ItemAdded)
		    {
		        // Set foreign key value
		        colReportTemplateEntities[e.NewIndex].ReportEntityID = Id;
		    }
		}
		
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varName,string varXtype,string varConfig,int varDefaultOrder,string varDescription,bool varIsDeprecated,int? varReportEntityTypeID)
		{
			ReportEntity item = new ReportEntity();
			
			item.Name = varName;
			
			item.Xtype = varXtype;
			
			item.Config = varConfig;
			
			item.DefaultOrder = varDefaultOrder;
			
			item.Description = varDescription;
			
			item.IsDeprecated = varIsDeprecated;
			
			item.ReportEntityTypeID = varReportEntityTypeID;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,string varName,string varXtype,string varConfig,int varDefaultOrder,string varDescription,bool varIsDeprecated,int? varReportEntityTypeID)
		{
			ReportEntity item = new ReportEntity();
			
				item.Id = varId;
			
				item.Name = varName;
			
				item.Xtype = varXtype;
			
				item.Config = varConfig;
			
				item.DefaultOrder = varDefaultOrder;
			
				item.Description = varDescription;
			
				item.IsDeprecated = varIsDeprecated;
			
				item.ReportEntityTypeID = varReportEntityTypeID;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn XtypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ConfigColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DefaultOrderColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDeprecatedColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ReportEntityTypeIDColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string Name = @"Name";
			 public static string Xtype = @"Xtype";
			 public static string Config = @"Config";
			 public static string DefaultOrder = @"DefaultOrder";
			 public static string Description = @"Description";
			 public static string IsDeprecated = @"IsDeprecated";
			 public static string ReportEntityTypeID = @"ReportEntityTypeID";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
                if (colReportTemplateEntities != null)
                {
                    foreach (Symphony.Core.Data.ReportTemplateEntity item in colReportTemplateEntities)
                    {
                        if (item.ReportEntityID != Id)
                        {
                            item.ReportEntityID = Id;
                        }
                    }
               }
		}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
                if (colReportTemplateEntities != null)
                {
                    colReportTemplateEntities.SaveAll();
               }
		}
        #endregion
	}
}
