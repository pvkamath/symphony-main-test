using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data{
    /// <summary>
    /// Strongly-typed collection for the GTMOrganizerCount class.
    /// </summary>
    [Serializable]
    public partial class GTMOrganizerCountCollection : ReadOnlyList<GTMOrganizerCount, GTMOrganizerCountCollection>
    {        
        public GTMOrganizerCountCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the GTMOrganizerCount view.
    /// </summary>
    [Serializable]
    public partial class GTMOrganizerCount : ReadOnlyRecord<GTMOrganizerCount>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("GTMOrganizerCount", TableType.View, DataService.GetInstance("Symphony"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrganizerCount = new TableSchema.TableColumn(schema);
                colvarOrganizerCount.ColumnName = "OrganizerCount";
                colvarOrganizerCount.DataType = DbType.Int32;
                colvarOrganizerCount.MaxLength = 0;
                colvarOrganizerCount.AutoIncrement = false;
                colvarOrganizerCount.IsNullable = true;
                colvarOrganizerCount.IsPrimaryKey = false;
                colvarOrganizerCount.IsForeignKey = false;
                colvarOrganizerCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrganizerCount);
                
                TableSchema.TableColumn colvarCustomerID = new TableSchema.TableColumn(schema);
                colvarCustomerID.ColumnName = "CustomerID";
                colvarCustomerID.DataType = DbType.Int32;
                colvarCustomerID.MaxLength = 0;
                colvarCustomerID.AutoIncrement = false;
                colvarCustomerID.IsNullable = false;
                colvarCustomerID.IsPrimaryKey = false;
                colvarCustomerID.IsForeignKey = false;
                colvarCustomerID.IsReadOnly = false;
                
                schema.Columns.Add(colvarCustomerID);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["Symphony"].AddSchema("GTMOrganizerCount",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public GTMOrganizerCount()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public GTMOrganizerCount(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public GTMOrganizerCount(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public GTMOrganizerCount(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrganizerCount")]
        [Bindable(true)]
        public int? OrganizerCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("OrganizerCount");
		    }
            set 
		    {
			    SetColumnValue("OrganizerCount", value);
            }
        }
	      
        [XmlAttribute("CustomerID")]
        [Bindable(true)]
        public int CustomerID 
	    {
		    get
		    {
			    return GetColumnValue<int>("CustomerID");
		    }
            set 
		    {
			    SetColumnValue("CustomerID", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrganizerCount = @"OrganizerCount";
            
            public static string CustomerID = @"CustomerID";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
