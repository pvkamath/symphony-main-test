using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
    /// <summary>
    /// Controller class for ReportJobs
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class ReportJobController
    {
        // Preload our schema..
        ReportJob thisSchemaLoad = new ReportJob();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public ReportJobCollection FetchAll()
        {
            ReportJobCollection coll = new ReportJobCollection();
            Query qry = new Query(ReportJob.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public ReportJobCollection FetchByID(object Id)
        {
            ReportJobCollection coll = new ReportJobCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public ReportJobCollection FetchByQuery(Query qry)
        {
            ReportJobCollection coll = new ReportJobCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (ReportJob.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (ReportJob.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int CustomerID,string UserNotes,string Name,string Group,string Description,string JobType,DateTime CreatedOn,DateTime ModifiedOn,string CreatedBy,string ModifiedBy,int IsScheduled,string ReportCode)
	    {
		    ReportJob item = new ReportJob();
		    
            item.CustomerID = CustomerID;
            
            item.UserNotes = UserNotes;
            
            item.Name = Name;
            
            item.Group = Group;
            
            item.Description = Description;
            
            item.JobType = JobType;
            
            item.CreatedOn = CreatedOn;
            
            item.ModifiedOn = ModifiedOn;
            
            item.CreatedBy = CreatedBy;
            
            item.ModifiedBy = ModifiedBy;
            
            item.IsScheduled = IsScheduled;
            
            item.ReportCode = ReportCode;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,int CustomerID,string UserNotes,string Name,string Group,string Description,string JobType,DateTime CreatedOn,DateTime ModifiedOn,string CreatedBy,string ModifiedBy,int IsScheduled,string ReportCode)
	    {
		    ReportJob item = new ReportJob();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.CustomerID = CustomerID;
				
			item.UserNotes = UserNotes;
				
			item.Name = Name;
				
			item.Group = Group;
				
			item.Description = Description;
				
			item.JobType = JobType;
				
			item.CreatedOn = CreatedOn;
				
			item.ModifiedOn = ModifiedOn;
				
			item.CreatedBy = CreatedBy;
				
			item.ModifiedBy = ModifiedBy;
				
			item.IsScheduled = IsScheduled;
				
			item.ReportCode = ReportCode;
				
	        item.Save(UserName);
	    }
    }
}
