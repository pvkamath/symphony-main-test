using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
	/// <summary>
	/// Strongly-typed collection for the CourseFile class.
	/// </summary>
    [Serializable]
	public partial class CourseFileCollection : ActiveList<CourseFile, CourseFileCollection>
	{	   
		public CourseFileCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CourseFileCollection</returns>
		public CourseFileCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CourseFile o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the CourseFile table.
	/// </summary>
	[Serializable]
	public partial class CourseFile : Symphony.Core.Data.ActiveRecordCustom<CourseFile>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public CourseFile()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CourseFile(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public CourseFile(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public CourseFile(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("CourseFile", TableType.Table, DataService.GetInstance("Symphony"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarFileName = new TableSchema.TableColumn(schema);
				colvarFileName.ColumnName = "FileName";
				colvarFileName.DataType = DbType.String;
				colvarFileName.MaxLength = 100;
				colvarFileName.AutoIncrement = false;
				colvarFileName.IsNullable = false;
				colvarFileName.IsPrimaryKey = false;
				colvarFileName.IsForeignKey = false;
				colvarFileName.IsReadOnly = false;
				colvarFileName.DefaultSetting = @"";
				colvarFileName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFileName);
				
				TableSchema.TableColumn colvarOriginalFileName = new TableSchema.TableColumn(schema);
				colvarOriginalFileName.ColumnName = "OriginalFileName";
				colvarOriginalFileName.DataType = DbType.String;
				colvarOriginalFileName.MaxLength = 100;
				colvarOriginalFileName.AutoIncrement = false;
				colvarOriginalFileName.IsNullable = false;
				colvarOriginalFileName.IsPrimaryKey = false;
				colvarOriginalFileName.IsForeignKey = false;
				colvarOriginalFileName.IsReadOnly = false;
				colvarOriginalFileName.DefaultSetting = @"";
				colvarOriginalFileName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOriginalFileName);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "Description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = 200;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = false;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "Title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 200;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarPath = new TableSchema.TableColumn(schema);
				colvarPath.ColumnName = "Path";
				colvarPath.DataType = DbType.String;
				colvarPath.MaxLength = 256;
				colvarPath.AutoIncrement = false;
				colvarPath.IsNullable = false;
				colvarPath.IsPrimaryKey = false;
				colvarPath.IsForeignKey = false;
				colvarPath.IsReadOnly = false;
				colvarPath.DefaultSetting = @"";
				colvarPath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPath);
				
				TableSchema.TableColumn colvarFileTypeID = new TableSchema.TableColumn(schema);
				colvarFileTypeID.ColumnName = "FileTypeID";
				colvarFileTypeID.DataType = DbType.Int32;
				colvarFileTypeID.MaxLength = 0;
				colvarFileTypeID.AutoIncrement = false;
				colvarFileTypeID.IsNullable = false;
				colvarFileTypeID.IsPrimaryKey = false;
				colvarFileTypeID.IsForeignKey = false;
				colvarFileTypeID.IsReadOnly = false;
				colvarFileTypeID.DefaultSetting = @"";
				colvarFileTypeID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFileTypeID);
				
				TableSchema.TableColumn colvarFileSize = new TableSchema.TableColumn(schema);
				colvarFileSize.ColumnName = "FileSize";
				colvarFileSize.DataType = DbType.Int64;
				colvarFileSize.MaxLength = 0;
				colvarFileSize.AutoIncrement = false;
				colvarFileSize.IsNullable = false;
				colvarFileSize.IsPrimaryKey = false;
				colvarFileSize.IsForeignKey = false;
				colvarFileSize.IsReadOnly = false;
				colvarFileSize.DefaultSetting = @"";
				colvarFileSize.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFileSize);
				
				TableSchema.TableColumn colvarFileBytes = new TableSchema.TableColumn(schema);
				colvarFileBytes.ColumnName = "FileBytes";
				colvarFileBytes.DataType = DbType.Binary;
				colvarFileBytes.MaxLength = -1;
				colvarFileBytes.AutoIncrement = false;
				colvarFileBytes.IsNullable = false;
				colvarFileBytes.IsPrimaryKey = false;
				colvarFileBytes.IsForeignKey = false;
				colvarFileBytes.IsReadOnly = false;
				colvarFileBytes.DefaultSetting = @"";
				colvarFileBytes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFileBytes);
				
				TableSchema.TableColumn colvarFileExtension = new TableSchema.TableColumn(schema);
				colvarFileExtension.ColumnName = "FileExtension";
				colvarFileExtension.DataType = DbType.String;
				colvarFileExtension.MaxLength = 50;
				colvarFileExtension.AutoIncrement = false;
				colvarFileExtension.IsNullable = false;
				colvarFileExtension.IsPrimaryKey = false;
				colvarFileExtension.IsForeignKey = false;
				colvarFileExtension.IsReadOnly = false;
				colvarFileExtension.DefaultSetting = @"";
				colvarFileExtension.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFileExtension);
				
				TableSchema.TableColumn colvarModifiedBy = new TableSchema.TableColumn(schema);
				colvarModifiedBy.ColumnName = "ModifiedBy";
				colvarModifiedBy.DataType = DbType.String;
				colvarModifiedBy.MaxLength = 50;
				colvarModifiedBy.AutoIncrement = false;
				colvarModifiedBy.IsNullable = false;
				colvarModifiedBy.IsPrimaryKey = false;
				colvarModifiedBy.IsForeignKey = false;
				colvarModifiedBy.IsReadOnly = false;
				colvarModifiedBy.DefaultSetting = @"";
				colvarModifiedBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedBy);
				
				TableSchema.TableColumn colvarCreatedBy = new TableSchema.TableColumn(schema);
				colvarCreatedBy.ColumnName = "CreatedBy";
				colvarCreatedBy.DataType = DbType.String;
				colvarCreatedBy.MaxLength = 50;
				colvarCreatedBy.AutoIncrement = false;
				colvarCreatedBy.IsNullable = false;
				colvarCreatedBy.IsPrimaryKey = false;
				colvarCreatedBy.IsForeignKey = false;
				colvarCreatedBy.IsReadOnly = false;
				colvarCreatedBy.DefaultSetting = @"";
				colvarCreatedBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedBy);
				
				TableSchema.TableColumn colvarModifiedOn = new TableSchema.TableColumn(schema);
				colvarModifiedOn.ColumnName = "ModifiedOn";
				colvarModifiedOn.DataType = DbType.DateTime;
				colvarModifiedOn.MaxLength = 0;
				colvarModifiedOn.AutoIncrement = false;
				colvarModifiedOn.IsNullable = false;
				colvarModifiedOn.IsPrimaryKey = false;
				colvarModifiedOn.IsForeignKey = false;
				colvarModifiedOn.IsReadOnly = false;
				colvarModifiedOn.DefaultSetting = @"";
				colvarModifiedOn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedOn);
				
				TableSchema.TableColumn colvarCreatedOn = new TableSchema.TableColumn(schema);
				colvarCreatedOn.ColumnName = "CreatedOn";
				colvarCreatedOn.DataType = DbType.DateTime;
				colvarCreatedOn.MaxLength = 0;
				colvarCreatedOn.AutoIncrement = false;
				colvarCreatedOn.IsNullable = false;
				colvarCreatedOn.IsPrimaryKey = false;
				colvarCreatedOn.IsForeignKey = false;
				colvarCreatedOn.IsReadOnly = false;
				colvarCreatedOn.DefaultSetting = @"";
				colvarCreatedOn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedOn);
				
				TableSchema.TableColumn colvarCustomerID = new TableSchema.TableColumn(schema);
				colvarCustomerID.ColumnName = "CustomerID";
				colvarCustomerID.DataType = DbType.Int32;
				colvarCustomerID.MaxLength = 0;
				colvarCustomerID.AutoIncrement = false;
				colvarCustomerID.IsNullable = false;
				colvarCustomerID.IsPrimaryKey = false;
				colvarCustomerID.IsForeignKey = false;
				colvarCustomerID.IsReadOnly = false;
				colvarCustomerID.DefaultSetting = @"";
				colvarCustomerID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCustomerID);
				
				TableSchema.TableColumn colvarCourseID = new TableSchema.TableColumn(schema);
				colvarCourseID.ColumnName = "CourseID";
				colvarCourseID.DataType = DbType.Int32;
				colvarCourseID.MaxLength = 0;
				colvarCourseID.AutoIncrement = false;
				colvarCourseID.IsNullable = false;
				colvarCourseID.IsPrimaryKey = false;
				colvarCourseID.IsForeignKey = false;
				colvarCourseID.IsReadOnly = false;
				
						colvarCourseID.DefaultSetting = @"((0))";
				colvarCourseID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCourseID);
				
				TableSchema.TableColumn colvarCreatedByUserId = new TableSchema.TableColumn(schema);
				colvarCreatedByUserId.ColumnName = "CreatedByUserId";
				colvarCreatedByUserId.DataType = DbType.Int32;
				colvarCreatedByUserId.MaxLength = 0;
				colvarCreatedByUserId.AutoIncrement = false;
				colvarCreatedByUserId.IsNullable = false;
				colvarCreatedByUserId.IsPrimaryKey = false;
				colvarCreatedByUserId.IsForeignKey = false;
				colvarCreatedByUserId.IsReadOnly = false;
				
						colvarCreatedByUserId.DefaultSetting = @"((0))";
				colvarCreatedByUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedByUserId);
				
				TableSchema.TableColumn colvarModifiedByUserId = new TableSchema.TableColumn(schema);
				colvarModifiedByUserId.ColumnName = "ModifiedByUserId";
				colvarModifiedByUserId.DataType = DbType.Int32;
				colvarModifiedByUserId.MaxLength = 0;
				colvarModifiedByUserId.AutoIncrement = false;
				colvarModifiedByUserId.IsNullable = false;
				colvarModifiedByUserId.IsPrimaryKey = false;
				colvarModifiedByUserId.IsForeignKey = false;
				colvarModifiedByUserId.IsReadOnly = false;
				
						colvarModifiedByUserId.DefaultSetting = @"((0))";
				colvarModifiedByUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedByUserId);
				
				TableSchema.TableColumn colvarCreatedByActualUserId = new TableSchema.TableColumn(schema);
				colvarCreatedByActualUserId.ColumnName = "CreatedByActualUserId";
				colvarCreatedByActualUserId.DataType = DbType.Int32;
				colvarCreatedByActualUserId.MaxLength = 0;
				colvarCreatedByActualUserId.AutoIncrement = false;
				colvarCreatedByActualUserId.IsNullable = true;
				colvarCreatedByActualUserId.IsPrimaryKey = false;
				colvarCreatedByActualUserId.IsForeignKey = false;
				colvarCreatedByActualUserId.IsReadOnly = false;
				colvarCreatedByActualUserId.DefaultSetting = @"";
				colvarCreatedByActualUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedByActualUserId);
				
				TableSchema.TableColumn colvarModifiedByActualUserId = new TableSchema.TableColumn(schema);
				colvarModifiedByActualUserId.ColumnName = "ModifiedByActualUserId";
				colvarModifiedByActualUserId.DataType = DbType.Int32;
				colvarModifiedByActualUserId.MaxLength = 0;
				colvarModifiedByActualUserId.AutoIncrement = false;
				colvarModifiedByActualUserId.IsNullable = true;
				colvarModifiedByActualUserId.IsPrimaryKey = false;
				colvarModifiedByActualUserId.IsForeignKey = false;
				colvarModifiedByActualUserId.IsReadOnly = false;
				colvarModifiedByActualUserId.DefaultSetting = @"";
				colvarModifiedByActualUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedByActualUserId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["Symphony"].AddSchema("CourseFile",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("FileName")]
		[Bindable(true)]
		public string FileName 
		{
			get { return GetColumnValue<string>(Columns.FileName); }
			set { SetColumnValue(Columns.FileName, value); }
		}
		  
		[XmlAttribute("OriginalFileName")]
		[Bindable(true)]
		public string OriginalFileName 
		{
			get { return GetColumnValue<string>(Columns.OriginalFileName); }
			set { SetColumnValue(Columns.OriginalFileName, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		  
		[XmlAttribute("Path")]
		[Bindable(true)]
		public string Path 
		{
			get { return GetColumnValue<string>(Columns.Path); }
			set { SetColumnValue(Columns.Path, value); }
		}
		  
		[XmlAttribute("FileTypeID")]
		[Bindable(true)]
		public int FileTypeID 
		{
			get { return GetColumnValue<int>(Columns.FileTypeID); }
			set { SetColumnValue(Columns.FileTypeID, value); }
		}
		  
		[XmlAttribute("FileSize")]
		[Bindable(true)]
		public long FileSize 
		{
			get { return GetColumnValue<long>(Columns.FileSize); }
			set { SetColumnValue(Columns.FileSize, value); }
		}
		  
		[XmlAttribute("FileBytes")]
		[Bindable(true)]
		public byte[] FileBytes 
		{
			get { return GetColumnValue<byte[]>(Columns.FileBytes); }
			set { SetColumnValue(Columns.FileBytes, value); }
		}
		  
		[XmlAttribute("FileExtension")]
		[Bindable(true)]
		public string FileExtension 
		{
			get { return GetColumnValue<string>(Columns.FileExtension); }
			set { SetColumnValue(Columns.FileExtension, value); }
		}
		  
		[XmlAttribute("ModifiedBy")]
		[Bindable(true)]
		public string ModifiedBy 
		{
			get { return GetColumnValue<string>(Columns.ModifiedBy); }
			set { SetColumnValue(Columns.ModifiedBy, value); }
		}
		  
		[XmlAttribute("CreatedBy")]
		[Bindable(true)]
		public string CreatedBy 
		{
			get { return GetColumnValue<string>(Columns.CreatedBy); }
			set { SetColumnValue(Columns.CreatedBy, value); }
		}
		  
		[XmlAttribute("ModifiedOn")]
		[Bindable(true)]
		public DateTime ModifiedOn 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifiedOn); }
			set { SetColumnValue(Columns.ModifiedOn, value); }
		}
		  
		[XmlAttribute("CreatedOn")]
		[Bindable(true)]
		public DateTime CreatedOn 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedOn); }
			set { SetColumnValue(Columns.CreatedOn, value); }
		}
		  
		[XmlAttribute("CustomerID")]
		[Bindable(true)]
		public int CustomerID 
		{
			get { return GetColumnValue<int>(Columns.CustomerID); }
			set { SetColumnValue(Columns.CustomerID, value); }
		}
		  
		[XmlAttribute("CourseID")]
		[Bindable(true)]
		public int CourseID 
		{
			get { return GetColumnValue<int>(Columns.CourseID); }
			set { SetColumnValue(Columns.CourseID, value); }
		}
		  
		[XmlAttribute("CreatedByUserId")]
		[Bindable(true)]
		public int CreatedByUserId 
		{
			get { return GetColumnValue<int>(Columns.CreatedByUserId); }
			set { SetColumnValue(Columns.CreatedByUserId, value); }
		}
		  
		[XmlAttribute("ModifiedByUserId")]
		[Bindable(true)]
		public int ModifiedByUserId 
		{
			get { return GetColumnValue<int>(Columns.ModifiedByUserId); }
			set { SetColumnValue(Columns.ModifiedByUserId, value); }
		}
		  
		[XmlAttribute("CreatedByActualUserId")]
		[Bindable(true)]
		public int? CreatedByActualUserId 
		{
			get { return GetColumnValue<int?>(Columns.CreatedByActualUserId); }
			set { SetColumnValue(Columns.CreatedByActualUserId, value); }
		}
		  
		[XmlAttribute("ModifiedByActualUserId")]
		[Bindable(true)]
		public int? ModifiedByActualUserId 
		{
			get { return GetColumnValue<int?>(Columns.ModifiedByActualUserId); }
			set { SetColumnValue(Columns.ModifiedByActualUserId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varFileName,string varOriginalFileName,string varDescription,string varTitle,string varPath,int varFileTypeID,long varFileSize,byte[] varFileBytes,string varFileExtension,string varModifiedBy,string varCreatedBy,DateTime varModifiedOn,DateTime varCreatedOn,int varCustomerID,int varCourseID,int varCreatedByUserId,int varModifiedByUserId,int? varCreatedByActualUserId,int? varModifiedByActualUserId)
		{
			CourseFile item = new CourseFile();
			
			item.FileName = varFileName;
			
			item.OriginalFileName = varOriginalFileName;
			
			item.Description = varDescription;
			
			item.Title = varTitle;
			
			item.Path = varPath;
			
			item.FileTypeID = varFileTypeID;
			
			item.FileSize = varFileSize;
			
			item.FileBytes = varFileBytes;
			
			item.FileExtension = varFileExtension;
			
			item.ModifiedBy = varModifiedBy;
			
			item.CreatedBy = varCreatedBy;
			
			item.ModifiedOn = varModifiedOn;
			
			item.CreatedOn = varCreatedOn;
			
			item.CustomerID = varCustomerID;
			
			item.CourseID = varCourseID;
			
			item.CreatedByUserId = varCreatedByUserId;
			
			item.ModifiedByUserId = varModifiedByUserId;
			
			item.CreatedByActualUserId = varCreatedByActualUserId;
			
			item.ModifiedByActualUserId = varModifiedByActualUserId;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,string varFileName,string varOriginalFileName,string varDescription,string varTitle,string varPath,int varFileTypeID,long varFileSize,byte[] varFileBytes,string varFileExtension,string varModifiedBy,string varCreatedBy,DateTime varModifiedOn,DateTime varCreatedOn,int varCustomerID,int varCourseID,int varCreatedByUserId,int varModifiedByUserId,int? varCreatedByActualUserId,int? varModifiedByActualUserId)
		{
			CourseFile item = new CourseFile();
			
				item.Id = varId;
			
				item.FileName = varFileName;
			
				item.OriginalFileName = varOriginalFileName;
			
				item.Description = varDescription;
			
				item.Title = varTitle;
			
				item.Path = varPath;
			
				item.FileTypeID = varFileTypeID;
			
				item.FileSize = varFileSize;
			
				item.FileBytes = varFileBytes;
			
				item.FileExtension = varFileExtension;
			
				item.ModifiedBy = varModifiedBy;
			
				item.CreatedBy = varCreatedBy;
			
				item.ModifiedOn = varModifiedOn;
			
				item.CreatedOn = varCreatedOn;
			
				item.CustomerID = varCustomerID;
			
				item.CourseID = varCourseID;
			
				item.CreatedByUserId = varCreatedByUserId;
			
				item.ModifiedByUserId = varModifiedByUserId;
			
				item.CreatedByActualUserId = varCreatedByActualUserId;
			
				item.ModifiedByActualUserId = varModifiedByActualUserId;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn FileNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OriginalFileNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PathColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn FileTypeIDColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn FileSizeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn FileBytesColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn FileExtensionColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedByColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedByColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedOnColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedOnColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CustomerIDColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CourseIDColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedByUserIdColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedByUserIdColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedByActualUserIdColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedByActualUserIdColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string FileName = @"FileName";
			 public static string OriginalFileName = @"OriginalFileName";
			 public static string Description = @"Description";
			 public static string Title = @"Title";
			 public static string Path = @"Path";
			 public static string FileTypeID = @"FileTypeID";
			 public static string FileSize = @"FileSize";
			 public static string FileBytes = @"FileBytes";
			 public static string FileExtension = @"FileExtension";
			 public static string ModifiedBy = @"ModifiedBy";
			 public static string CreatedBy = @"CreatedBy";
			 public static string ModifiedOn = @"ModifiedOn";
			 public static string CreatedOn = @"CreatedOn";
			 public static string CustomerID = @"CustomerID";
			 public static string CourseID = @"CourseID";
			 public static string CreatedByUserId = @"CreatedByUserId";
			 public static string ModifiedByUserId = @"ModifiedByUserId";
			 public static string CreatedByActualUserId = @"CreatedByActualUserId";
			 public static string ModifiedByActualUserId = @"ModifiedByActualUserId";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
