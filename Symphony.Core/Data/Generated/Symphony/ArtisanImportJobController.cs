using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
    /// <summary>
    /// Controller class for ArtisanImportJob
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class ArtisanImportJobController
    {
        // Preload our schema..
        ArtisanImportJob thisSchemaLoad = new ArtisanImportJob();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public ArtisanImportJobCollection FetchAll()
        {
            ArtisanImportJobCollection coll = new ArtisanImportJobCollection();
            Query qry = new Query(ArtisanImportJob.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public ArtisanImportJobCollection FetchByID(object Id)
        {
            ArtisanImportJobCollection coll = new ArtisanImportJobCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public ArtisanImportJobCollection FetchByQuery(Query qry)
        {
            ArtisanImportJobCollection coll = new ArtisanImportJobCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (ArtisanImportJob.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (ArtisanImportJob.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Course,string Section,int? CoursesProcessed,int? SectionsProcessed,int? PagesProcessed,bool? Running,string CourseID,DateTime CreatedOn,DateTime ModifiedOn,string CreatedBy,string ModifiedBy,string Errors,int? CourseCount,int? SectionCount,int? Lines,int? LinesProcessed,string PathIssues,string DownloadTime,string ProcessTime,string ArtisanTime,string TotalTime,int? TotalFiles,int? FilesDuplicate,int? FilesDownloaded,int? FilesNotFound,int? FilesError,int? HTMLFiles,int? CoursesSkipped,int? LinesSkipped)
	    {
		    ArtisanImportJob item = new ArtisanImportJob();
		    
            item.Course = Course;
            
            item.Section = Section;
            
            item.CoursesProcessed = CoursesProcessed;
            
            item.SectionsProcessed = SectionsProcessed;
            
            item.PagesProcessed = PagesProcessed;
            
            item.Running = Running;
            
            item.CourseID = CourseID;
            
            item.CreatedOn = CreatedOn;
            
            item.ModifiedOn = ModifiedOn;
            
            item.CreatedBy = CreatedBy;
            
            item.ModifiedBy = ModifiedBy;
            
            item.Errors = Errors;
            
            item.CourseCount = CourseCount;
            
            item.SectionCount = SectionCount;
            
            item.Lines = Lines;
            
            item.LinesProcessed = LinesProcessed;
            
            item.PathIssues = PathIssues;
            
            item.DownloadTime = DownloadTime;
            
            item.ProcessTime = ProcessTime;
            
            item.ArtisanTime = ArtisanTime;
            
            item.TotalTime = TotalTime;
            
            item.TotalFiles = TotalFiles;
            
            item.FilesDuplicate = FilesDuplicate;
            
            item.FilesDownloaded = FilesDownloaded;
            
            item.FilesNotFound = FilesNotFound;
            
            item.FilesError = FilesError;
            
            item.HTMLFiles = HTMLFiles;
            
            item.CoursesSkipped = CoursesSkipped;
            
            item.LinesSkipped = LinesSkipped;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,string Course,string Section,int? CoursesProcessed,int? SectionsProcessed,int? PagesProcessed,bool? Running,string CourseID,DateTime CreatedOn,DateTime ModifiedOn,string CreatedBy,string ModifiedBy,string Errors,int? CourseCount,int? SectionCount,int? Lines,int? LinesProcessed,string PathIssues,string DownloadTime,string ProcessTime,string ArtisanTime,string TotalTime,int? TotalFiles,int? FilesDuplicate,int? FilesDownloaded,int? FilesNotFound,int? FilesError,int? HTMLFiles,int? CoursesSkipped,int? LinesSkipped)
	    {
		    ArtisanImportJob item = new ArtisanImportJob();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.Course = Course;
				
			item.Section = Section;
				
			item.CoursesProcessed = CoursesProcessed;
				
			item.SectionsProcessed = SectionsProcessed;
				
			item.PagesProcessed = PagesProcessed;
				
			item.Running = Running;
				
			item.CourseID = CourseID;
				
			item.CreatedOn = CreatedOn;
				
			item.ModifiedOn = ModifiedOn;
				
			item.CreatedBy = CreatedBy;
				
			item.ModifiedBy = ModifiedBy;
				
			item.Errors = Errors;
				
			item.CourseCount = CourseCount;
				
			item.SectionCount = SectionCount;
				
			item.Lines = Lines;
				
			item.LinesProcessed = LinesProcessed;
				
			item.PathIssues = PathIssues;
				
			item.DownloadTime = DownloadTime;
				
			item.ProcessTime = ProcessTime;
				
			item.ArtisanTime = ArtisanTime;
				
			item.TotalTime = TotalTime;
				
			item.TotalFiles = TotalFiles;
				
			item.FilesDuplicate = FilesDuplicate;
				
			item.FilesDownloaded = FilesDownloaded;
				
			item.FilesNotFound = FilesNotFound;
				
			item.FilesError = FilesError;
				
			item.HTMLFiles = HTMLFiles;
				
			item.CoursesSkipped = CoursesSkipped;
				
			item.LinesSkipped = LinesSkipped;
				
	        item.Save(UserName);
	    }
    }
}
