using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data{
    /// <summary>
    /// Strongly-typed collection for the Assignment class.
    /// </summary>
    [Serializable]
    public partial class AssignmentCollection : ReadOnlyList<Assignment, AssignmentCollection>
    {        
        public AssignmentCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the Assignments view.
    /// </summary>
    [Serializable]
    public partial class Assignment : ReadOnlyRecord<Assignment>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("Assignments", TableType.View, DataService.GetInstance("Symphony"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarSectionID = new TableSchema.TableColumn(schema);
                colvarSectionID.ColumnName = "SectionID";
                colvarSectionID.DataType = DbType.Int32;
                colvarSectionID.MaxLength = 0;
                colvarSectionID.AutoIncrement = false;
                colvarSectionID.IsNullable = false;
                colvarSectionID.IsPrimaryKey = false;
                colvarSectionID.IsForeignKey = false;
                colvarSectionID.IsReadOnly = false;
                
                schema.Columns.Add(colvarSectionID);
                
                TableSchema.TableColumn colvarCourseName = new TableSchema.TableColumn(schema);
                colvarCourseName.ColumnName = "CourseName";
                colvarCourseName.DataType = DbType.String;
                colvarCourseName.MaxLength = 150;
                colvarCourseName.AutoIncrement = false;
                colvarCourseName.IsNullable = false;
                colvarCourseName.IsPrimaryKey = false;
                colvarCourseName.IsForeignKey = false;
                colvarCourseName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCourseName);
                
                TableSchema.TableColumn colvarCourseID = new TableSchema.TableColumn(schema);
                colvarCourseID.ColumnName = "CourseID";
                colvarCourseID.DataType = DbType.Int32;
                colvarCourseID.MaxLength = 0;
                colvarCourseID.AutoIncrement = false;
                colvarCourseID.IsNullable = false;
                colvarCourseID.IsPrimaryKey = false;
                colvarCourseID.IsForeignKey = false;
                colvarCourseID.IsReadOnly = false;
                
                schema.Columns.Add(colvarCourseID);
                
                TableSchema.TableColumn colvarTrainingProgramID = new TableSchema.TableColumn(schema);
                colvarTrainingProgramID.ColumnName = "TrainingProgramID";
                colvarTrainingProgramID.DataType = DbType.Int32;
                colvarTrainingProgramID.MaxLength = 0;
                colvarTrainingProgramID.AutoIncrement = false;
                colvarTrainingProgramID.IsNullable = false;
                colvarTrainingProgramID.IsPrimaryKey = false;
                colvarTrainingProgramID.IsForeignKey = false;
                colvarTrainingProgramID.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrainingProgramID);
                
                TableSchema.TableColumn colvarAttempt = new TableSchema.TableColumn(schema);
                colvarAttempt.ColumnName = "Attempt";
                colvarAttempt.DataType = DbType.Int32;
                colvarAttempt.MaxLength = 0;
                colvarAttempt.AutoIncrement = false;
                colvarAttempt.IsNullable = false;
                colvarAttempt.IsPrimaryKey = false;
                colvarAttempt.IsForeignKey = false;
                colvarAttempt.IsReadOnly = false;
                
                schema.Columns.Add(colvarAttempt);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "Name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 512;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarDateX = new TableSchema.TableColumn(schema);
                colvarDateX.ColumnName = "Date";
                colvarDateX.DataType = DbType.DateTime;
                colvarDateX.MaxLength = 0;
                colvarDateX.AutoIncrement = false;
                colvarDateX.IsNullable = true;
                colvarDateX.IsPrimaryKey = false;
                colvarDateX.IsForeignKey = false;
                colvarDateX.IsReadOnly = false;
                
                schema.Columns.Add(colvarDateX);
                
                TableSchema.TableColumn colvarGradedDate = new TableSchema.TableColumn(schema);
                colvarGradedDate.ColumnName = "GradedDate";
                colvarGradedDate.DataType = DbType.DateTime;
                colvarGradedDate.MaxLength = 0;
                colvarGradedDate.AutoIncrement = false;
                colvarGradedDate.IsNullable = true;
                colvarGradedDate.IsPrimaryKey = false;
                colvarGradedDate.IsForeignKey = false;
                colvarGradedDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarGradedDate);
                
                TableSchema.TableColumn colvarUserID = new TableSchema.TableColumn(schema);
                colvarUserID.ColumnName = "UserID";
                colvarUserID.DataType = DbType.Int32;
                colvarUserID.MaxLength = 0;
                colvarUserID.AutoIncrement = false;
                colvarUserID.IsNullable = false;
                colvarUserID.IsPrimaryKey = false;
                colvarUserID.IsForeignKey = false;
                colvarUserID.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserID);
                
                TableSchema.TableColumn colvarNumQuestions = new TableSchema.TableColumn(schema);
                colvarNumQuestions.ColumnName = "NumQuestions";
                colvarNumQuestions.DataType = DbType.Int32;
                colvarNumQuestions.MaxLength = 0;
                colvarNumQuestions.AutoIncrement = false;
                colvarNumQuestions.IsNullable = true;
                colvarNumQuestions.IsPrimaryKey = false;
                colvarNumQuestions.IsForeignKey = false;
                colvarNumQuestions.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumQuestions);
                
                TableSchema.TableColumn colvarTotalQuestions = new TableSchema.TableColumn(schema);
                colvarTotalQuestions.ColumnName = "TotalQuestions";
                colvarTotalQuestions.DataType = DbType.Int32;
                colvarTotalQuestions.MaxLength = 0;
                colvarTotalQuestions.AutoIncrement = false;
                colvarTotalQuestions.IsNullable = true;
                colvarTotalQuestions.IsPrimaryKey = false;
                colvarTotalQuestions.IsForeignKey = false;
                colvarTotalQuestions.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalQuestions);
                
                TableSchema.TableColumn colvarNumCorrect = new TableSchema.TableColumn(schema);
                colvarNumCorrect.ColumnName = "NumCorrect";
                colvarNumCorrect.DataType = DbType.Int32;
                colvarNumCorrect.MaxLength = 0;
                colvarNumCorrect.AutoIncrement = false;
                colvarNumCorrect.IsNullable = true;
                colvarNumCorrect.IsPrimaryKey = false;
                colvarNumCorrect.IsForeignKey = false;
                colvarNumCorrect.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumCorrect);
                
                TableSchema.TableColumn colvarScore = new TableSchema.TableColumn(schema);
                colvarScore.ColumnName = "Score";
                colvarScore.DataType = DbType.Double;
                colvarScore.MaxLength = 0;
                colvarScore.AutoIncrement = false;
                colvarScore.IsNullable = true;
                colvarScore.IsPrimaryKey = false;
                colvarScore.IsForeignKey = false;
                colvarScore.IsReadOnly = false;
                
                schema.Columns.Add(colvarScore);
                
                TableSchema.TableColumn colvarIsMarked = new TableSchema.TableColumn(schema);
                colvarIsMarked.ColumnName = "IsMarked";
                colvarIsMarked.DataType = DbType.Int32;
                colvarIsMarked.MaxLength = 0;
                colvarIsMarked.AutoIncrement = false;
                colvarIsMarked.IsNullable = false;
                colvarIsMarked.IsPrimaryKey = false;
                colvarIsMarked.IsForeignKey = false;
                colvarIsMarked.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsMarked);
                
                TableSchema.TableColumn colvarArtisanCourseID = new TableSchema.TableColumn(schema);
                colvarArtisanCourseID.ColumnName = "ArtisanCourseID";
                colvarArtisanCourseID.DataType = DbType.Int32;
                colvarArtisanCourseID.MaxLength = 0;
                colvarArtisanCourseID.AutoIncrement = false;
                colvarArtisanCourseID.IsNullable = false;
                colvarArtisanCourseID.IsPrimaryKey = false;
                colvarArtisanCourseID.IsForeignKey = false;
                colvarArtisanCourseID.IsReadOnly = false;
                
                schema.Columns.Add(colvarArtisanCourseID);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["Symphony"].AddSchema("Assignments",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public Assignment()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public Assignment(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public Assignment(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public Assignment(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("SectionID")]
        [Bindable(true)]
        public int SectionID 
	    {
		    get
		    {
			    return GetColumnValue<int>("SectionID");
		    }
            set 
		    {
			    SetColumnValue("SectionID", value);
            }
        }
	      
        [XmlAttribute("CourseName")]
        [Bindable(true)]
        public string CourseName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CourseName");
		    }
            set 
		    {
			    SetColumnValue("CourseName", value);
            }
        }
	      
        [XmlAttribute("CourseID")]
        [Bindable(true)]
        public int CourseID 
	    {
		    get
		    {
			    return GetColumnValue<int>("CourseID");
		    }
            set 
		    {
			    SetColumnValue("CourseID", value);
            }
        }
	      
        [XmlAttribute("TrainingProgramID")]
        [Bindable(true)]
        public int TrainingProgramID 
	    {
		    get
		    {
			    return GetColumnValue<int>("TrainingProgramID");
		    }
            set 
		    {
			    SetColumnValue("TrainingProgramID", value);
            }
        }
	      
        [XmlAttribute("Attempt")]
        [Bindable(true)]
        public int Attempt 
	    {
		    get
		    {
			    return GetColumnValue<int>("Attempt");
		    }
            set 
		    {
			    SetColumnValue("Attempt", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("Name");
		    }
            set 
		    {
			    SetColumnValue("Name", value);
            }
        }
	      
        [XmlAttribute("DateX")]
        [Bindable(true)]
        public DateTime? DateX 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("Date");
		    }
            set 
		    {
			    SetColumnValue("Date", value);
            }
        }
	      
        [XmlAttribute("GradedDate")]
        [Bindable(true)]
        public DateTime? GradedDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("GradedDate");
		    }
            set 
		    {
			    SetColumnValue("GradedDate", value);
            }
        }
	      
        [XmlAttribute("UserID")]
        [Bindable(true)]
        public int UserID 
	    {
		    get
		    {
			    return GetColumnValue<int>("UserID");
		    }
            set 
		    {
			    SetColumnValue("UserID", value);
            }
        }
	      
        [XmlAttribute("NumQuestions")]
        [Bindable(true)]
        public int? NumQuestions 
	    {
		    get
		    {
			    return GetColumnValue<int?>("NumQuestions");
		    }
            set 
		    {
			    SetColumnValue("NumQuestions", value);
            }
        }
	      
        [XmlAttribute("TotalQuestions")]
        [Bindable(true)]
        public int? TotalQuestions 
	    {
		    get
		    {
			    return GetColumnValue<int?>("TotalQuestions");
		    }
            set 
		    {
			    SetColumnValue("TotalQuestions", value);
            }
        }
	      
        [XmlAttribute("NumCorrect")]
        [Bindable(true)]
        public int? NumCorrect 
	    {
		    get
		    {
			    return GetColumnValue<int?>("NumCorrect");
		    }
            set 
		    {
			    SetColumnValue("NumCorrect", value);
            }
        }
	      
        [XmlAttribute("Score")]
        [Bindable(true)]
        public double? Score 
	    {
		    get
		    {
			    return GetColumnValue<double?>("Score");
		    }
            set 
		    {
			    SetColumnValue("Score", value);
            }
        }
	      
        [XmlAttribute("IsMarked")]
        [Bindable(true)]
        public int IsMarked 
	    {
		    get
		    {
			    return GetColumnValue<int>("IsMarked");
		    }
            set 
		    {
			    SetColumnValue("IsMarked", value);
            }
        }
	      
        [XmlAttribute("ArtisanCourseID")]
        [Bindable(true)]
        public int ArtisanCourseID 
	    {
		    get
		    {
			    return GetColumnValue<int>("ArtisanCourseID");
		    }
            set 
		    {
			    SetColumnValue("ArtisanCourseID", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string SectionID = @"SectionID";
            
            public static string CourseName = @"CourseName";
            
            public static string CourseID = @"CourseID";
            
            public static string TrainingProgramID = @"TrainingProgramID";
            
            public static string Attempt = @"Attempt";
            
            public static string Name = @"Name";
            
            public static string DateX = @"Date";
            
            public static string GradedDate = @"GradedDate";
            
            public static string UserID = @"UserID";
            
            public static string NumQuestions = @"NumQuestions";
            
            public static string TotalQuestions = @"TotalQuestions";
            
            public static string NumCorrect = @"NumCorrect";
            
            public static string Score = @"Score";
            
            public static string IsMarked = @"IsMarked";
            
            public static string ArtisanCourseID = @"ArtisanCourseID";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
