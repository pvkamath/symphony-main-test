using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
	/// <summary>
	/// Strongly-typed collection for the OnlineCourseAuthorLink class.
	/// </summary>
    [Serializable]
	public partial class OnlineCourseAuthorLinkCollection : ActiveList<OnlineCourseAuthorLink, OnlineCourseAuthorLinkCollection>
	{	   
		public OnlineCourseAuthorLinkCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OnlineCourseAuthorLinkCollection</returns>
		public OnlineCourseAuthorLinkCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OnlineCourseAuthorLink o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the OnlineCourseAuthorLink table.
	/// </summary>
	[Serializable]
	public partial class OnlineCourseAuthorLink : Symphony.Core.Data.ActiveRecordCustom<OnlineCourseAuthorLink>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public OnlineCourseAuthorLink()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OnlineCourseAuthorLink(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public OnlineCourseAuthorLink(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public OnlineCourseAuthorLink(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("OnlineCourseAuthorLink", TableType.Table, DataService.GetInstance("Symphony"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "UserId";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = true;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				
					colvarUserId.ForeignKeyTableName = "User";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarOnlineCourseId = new TableSchema.TableColumn(schema);
				colvarOnlineCourseId.ColumnName = "OnlineCourseId";
				colvarOnlineCourseId.DataType = DbType.Int32;
				colvarOnlineCourseId.MaxLength = 0;
				colvarOnlineCourseId.AutoIncrement = false;
				colvarOnlineCourseId.IsNullable = false;
				colvarOnlineCourseId.IsPrimaryKey = false;
				colvarOnlineCourseId.IsForeignKey = true;
				colvarOnlineCourseId.IsReadOnly = false;
				colvarOnlineCourseId.DefaultSetting = @"";
				
					colvarOnlineCourseId.ForeignKeyTableName = "OnlineCourse";
				schema.Columns.Add(colvarOnlineCourseId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["Symphony"].AddSchema("OnlineCourseAuthorLink",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("OnlineCourseId")]
		[Bindable(true)]
		public int OnlineCourseId 
		{
			get { return GetColumnValue<int>(Columns.OnlineCourseId); }
			set { SetColumnValue(Columns.OnlineCourseId, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a OnlineCourse ActiveRecord object related to this OnlineCourseAuthorLink
		/// </summary>
		private Symphony.Core.Data.OnlineCourse _OnlineCourse;
		public Symphony.Core.Data.OnlineCourse OnlineCourse
		{
			get { 
			    if(_OnlineCourse == null){
			        _OnlineCourse = Symphony.Core.Data.OnlineCourse.FetchByID(this.OnlineCourseId); 
			    }
			    return _OnlineCourse;
			}
			set { 
			    SetColumnValue("OnlineCourseId", value.Id); 
			    _OnlineCourse = value;
			}
		}
		
		
		/// <summary>
		/// Returns a User ActiveRecord object related to this OnlineCourseAuthorLink
		/// </summary>
		private Symphony.Core.Data.User _User;
		public Symphony.Core.Data.User User
		{
			get { 
			    if(_User == null){
			        _User = Symphony.Core.Data.User.FetchByID(this.UserId); 
			    }
			    return _User;
			}
			set { 
			    SetColumnValue("UserId", value.Id); 
			    _User = value;
			}
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varUserId,int varOnlineCourseId)
		{
			OnlineCourseAuthorLink item = new OnlineCourseAuthorLink();
			
			item.UserId = varUserId;
			
			item.OnlineCourseId = varOnlineCourseId;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,int varUserId,int varOnlineCourseId)
		{
			OnlineCourseAuthorLink item = new OnlineCourseAuthorLink();
			
				item.Id = varId;
			
				item.UserId = varUserId;
			
				item.OnlineCourseId = varOnlineCourseId;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OnlineCourseIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string UserId = @"UserId";
			 public static string OnlineCourseId = @"OnlineCourseId";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
