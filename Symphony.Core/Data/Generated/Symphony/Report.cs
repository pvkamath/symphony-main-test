using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
	/// <summary>
	/// Strongly-typed collection for the Report class.
	/// </summary>
    [Serializable]
	public partial class ReportCollection : ActiveList<Report, ReportCollection>
	{	   
		public ReportCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ReportCollection</returns>
		public ReportCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Report o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the Reports table.
	/// </summary>
	[Serializable]
	public partial class Report : Symphony.Core.Data.ActiveRecordCustom<Report>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public Report()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Report(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public Report(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public Report(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("Reports", TableType.Table, DataService.GetInstance("Symphony"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "Name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 64;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = false;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);
				
				TableSchema.TableColumn colvarReportTypeID = new TableSchema.TableColumn(schema);
				colvarReportTypeID.ColumnName = "ReportTypeID";
				colvarReportTypeID.DataType = DbType.Int32;
				colvarReportTypeID.MaxLength = 0;
				colvarReportTypeID.AutoIncrement = false;
				colvarReportTypeID.IsNullable = false;
				colvarReportTypeID.IsPrimaryKey = false;
				colvarReportTypeID.IsForeignKey = false;
				colvarReportTypeID.IsReadOnly = false;
				colvarReportTypeID.DefaultSetting = @"";
				colvarReportTypeID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReportTypeID);
				
				TableSchema.TableColumn colvarNotifyWhenReady = new TableSchema.TableColumn(schema);
				colvarNotifyWhenReady.ColumnName = "NotifyWhenReady";
				colvarNotifyWhenReady.DataType = DbType.Boolean;
				colvarNotifyWhenReady.MaxLength = 0;
				colvarNotifyWhenReady.AutoIncrement = false;
				colvarNotifyWhenReady.IsNullable = false;
				colvarNotifyWhenReady.IsPrimaryKey = false;
				colvarNotifyWhenReady.IsForeignKey = false;
				colvarNotifyWhenReady.IsReadOnly = false;
				
						colvarNotifyWhenReady.DefaultSetting = @"((0))";
				colvarNotifyWhenReady.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNotifyWhenReady);
				
				TableSchema.TableColumn colvarDownloadCSV = new TableSchema.TableColumn(schema);
				colvarDownloadCSV.ColumnName = "DownloadCSV";
				colvarDownloadCSV.DataType = DbType.Boolean;
				colvarDownloadCSV.MaxLength = 0;
				colvarDownloadCSV.AutoIncrement = false;
				colvarDownloadCSV.IsNullable = false;
				colvarDownloadCSV.IsPrimaryKey = false;
				colvarDownloadCSV.IsForeignKey = false;
				colvarDownloadCSV.IsReadOnly = false;
				
						colvarDownloadCSV.DefaultSetting = @"((1))";
				colvarDownloadCSV.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDownloadCSV);
				
				TableSchema.TableColumn colvarDownloadXLS = new TableSchema.TableColumn(schema);
				colvarDownloadXLS.ColumnName = "DownloadXLS";
				colvarDownloadXLS.DataType = DbType.Boolean;
				colvarDownloadXLS.MaxLength = 0;
				colvarDownloadXLS.AutoIncrement = false;
				colvarDownloadXLS.IsNullable = false;
				colvarDownloadXLS.IsPrimaryKey = false;
				colvarDownloadXLS.IsForeignKey = false;
				colvarDownloadXLS.IsReadOnly = false;
				
						colvarDownloadXLS.DefaultSetting = @"((1))";
				colvarDownloadXLS.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDownloadXLS);
				
				TableSchema.TableColumn colvarDownloadPDF = new TableSchema.TableColumn(schema);
				colvarDownloadPDF.ColumnName = "DownloadPDF";
				colvarDownloadPDF.DataType = DbType.Boolean;
				colvarDownloadPDF.MaxLength = 0;
				colvarDownloadPDF.AutoIncrement = false;
				colvarDownloadPDF.IsNullable = false;
				colvarDownloadPDF.IsPrimaryKey = false;
				colvarDownloadPDF.IsForeignKey = false;
				colvarDownloadPDF.IsReadOnly = false;
				
						colvarDownloadPDF.DefaultSetting = @"((1))";
				colvarDownloadPDF.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDownloadPDF);
				
				TableSchema.TableColumn colvarParameters = new TableSchema.TableColumn(schema);
				colvarParameters.ColumnName = "Parameters";
				colvarParameters.DataType = DbType.String;
				colvarParameters.MaxLength = -1;
				colvarParameters.AutoIncrement = false;
				colvarParameters.IsNullable = false;
				colvarParameters.IsPrimaryKey = false;
				colvarParameters.IsForeignKey = false;
				colvarParameters.IsReadOnly = false;
				
						colvarParameters.DefaultSetting = @"('')";
				colvarParameters.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParameters);
				
				TableSchema.TableColumn colvarScheduleType = new TableSchema.TableColumn(schema);
				colvarScheduleType.ColumnName = "ScheduleType";
				colvarScheduleType.DataType = DbType.Int32;
				colvarScheduleType.MaxLength = 0;
				colvarScheduleType.AutoIncrement = false;
				colvarScheduleType.IsNullable = false;
				colvarScheduleType.IsPrimaryKey = false;
				colvarScheduleType.IsForeignKey = false;
				colvarScheduleType.IsReadOnly = false;
				colvarScheduleType.DefaultSetting = @"";
				colvarScheduleType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScheduleType);
				
				TableSchema.TableColumn colvarScheduleHour = new TableSchema.TableColumn(schema);
				colvarScheduleHour.ColumnName = "ScheduleHour";
				colvarScheduleHour.DataType = DbType.Int32;
				colvarScheduleHour.MaxLength = 0;
				colvarScheduleHour.AutoIncrement = false;
				colvarScheduleHour.IsNullable = true;
				colvarScheduleHour.IsPrimaryKey = false;
				colvarScheduleHour.IsForeignKey = false;
				colvarScheduleHour.IsReadOnly = false;
				colvarScheduleHour.DefaultSetting = @"";
				colvarScheduleHour.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScheduleHour);
				
				TableSchema.TableColumn colvarScheduleDaysOfWeek = new TableSchema.TableColumn(schema);
				colvarScheduleDaysOfWeek.ColumnName = "ScheduleDaysOfWeek";
				colvarScheduleDaysOfWeek.DataType = DbType.Int32;
				colvarScheduleDaysOfWeek.MaxLength = 0;
				colvarScheduleDaysOfWeek.AutoIncrement = false;
				colvarScheduleDaysOfWeek.IsNullable = true;
				colvarScheduleDaysOfWeek.IsPrimaryKey = false;
				colvarScheduleDaysOfWeek.IsForeignKey = false;
				colvarScheduleDaysOfWeek.IsReadOnly = false;
				colvarScheduleDaysOfWeek.DefaultSetting = @"";
				colvarScheduleDaysOfWeek.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScheduleDaysOfWeek);
				
				TableSchema.TableColumn colvarScheduleDayOfMonth = new TableSchema.TableColumn(schema);
				colvarScheduleDayOfMonth.ColumnName = "ScheduleDayOfMonth";
				colvarScheduleDayOfMonth.DataType = DbType.Int32;
				colvarScheduleDayOfMonth.MaxLength = 0;
				colvarScheduleDayOfMonth.AutoIncrement = false;
				colvarScheduleDayOfMonth.IsNullable = true;
				colvarScheduleDayOfMonth.IsPrimaryKey = false;
				colvarScheduleDayOfMonth.IsForeignKey = false;
				colvarScheduleDayOfMonth.IsReadOnly = false;
				colvarScheduleDayOfMonth.DefaultSetting = @"";
				colvarScheduleDayOfMonth.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScheduleDayOfMonth);
				
				TableSchema.TableColumn colvarScheduleMonth = new TableSchema.TableColumn(schema);
				colvarScheduleMonth.ColumnName = "ScheduleMonth";
				colvarScheduleMonth.DataType = DbType.Int32;
				colvarScheduleMonth.MaxLength = 0;
				colvarScheduleMonth.AutoIncrement = false;
				colvarScheduleMonth.IsNullable = true;
				colvarScheduleMonth.IsPrimaryKey = false;
				colvarScheduleMonth.IsForeignKey = false;
				colvarScheduleMonth.IsReadOnly = false;
				colvarScheduleMonth.DefaultSetting = @"";
				colvarScheduleMonth.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScheduleMonth);
				
				TableSchema.TableColumn colvarOwnerID = new TableSchema.TableColumn(schema);
				colvarOwnerID.ColumnName = "OwnerID";
				colvarOwnerID.DataType = DbType.Int32;
				colvarOwnerID.MaxLength = 0;
				colvarOwnerID.AutoIncrement = false;
				colvarOwnerID.IsNullable = false;
				colvarOwnerID.IsPrimaryKey = false;
				colvarOwnerID.IsForeignKey = false;
				colvarOwnerID.IsReadOnly = false;
				colvarOwnerID.DefaultSetting = @"";
				colvarOwnerID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOwnerID);
				
				TableSchema.TableColumn colvarCustomerID = new TableSchema.TableColumn(schema);
				colvarCustomerID.ColumnName = "CustomerID";
				colvarCustomerID.DataType = DbType.Int32;
				colvarCustomerID.MaxLength = 0;
				colvarCustomerID.AutoIncrement = false;
				colvarCustomerID.IsNullable = false;
				colvarCustomerID.IsPrimaryKey = false;
				colvarCustomerID.IsForeignKey = false;
				colvarCustomerID.IsReadOnly = false;
				colvarCustomerID.DefaultSetting = @"";
				colvarCustomerID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCustomerID);
				
				TableSchema.TableColumn colvarIsFavorite = new TableSchema.TableColumn(schema);
				colvarIsFavorite.ColumnName = "IsFavorite";
				colvarIsFavorite.DataType = DbType.Boolean;
				colvarIsFavorite.MaxLength = 0;
				colvarIsFavorite.AutoIncrement = false;
				colvarIsFavorite.IsNullable = false;
				colvarIsFavorite.IsPrimaryKey = false;
				colvarIsFavorite.IsForeignKey = false;
				colvarIsFavorite.IsReadOnly = false;
				
						colvarIsFavorite.DefaultSetting = @"((0))";
				colvarIsFavorite.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsFavorite);
				
				TableSchema.TableColumn colvarCreatedOn = new TableSchema.TableColumn(schema);
				colvarCreatedOn.ColumnName = "CreatedOn";
				colvarCreatedOn.DataType = DbType.DateTime;
				colvarCreatedOn.MaxLength = 0;
				colvarCreatedOn.AutoIncrement = false;
				colvarCreatedOn.IsNullable = false;
				colvarCreatedOn.IsPrimaryKey = false;
				colvarCreatedOn.IsForeignKey = false;
				colvarCreatedOn.IsReadOnly = false;
				
						colvarCreatedOn.DefaultSetting = @"(getdate())";
				colvarCreatedOn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedOn);
				
				TableSchema.TableColumn colvarModifiedOn = new TableSchema.TableColumn(schema);
				colvarModifiedOn.ColumnName = "ModifiedOn";
				colvarModifiedOn.DataType = DbType.DateTime;
				colvarModifiedOn.MaxLength = 0;
				colvarModifiedOn.AutoIncrement = false;
				colvarModifiedOn.IsNullable = false;
				colvarModifiedOn.IsPrimaryKey = false;
				colvarModifiedOn.IsForeignKey = false;
				colvarModifiedOn.IsReadOnly = false;
				
						colvarModifiedOn.DefaultSetting = @"(getdate())";
				colvarModifiedOn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedOn);
				
				TableSchema.TableColumn colvarCreatedBy = new TableSchema.TableColumn(schema);
				colvarCreatedBy.ColumnName = "CreatedBy";
				colvarCreatedBy.DataType = DbType.String;
				colvarCreatedBy.MaxLength = 64;
				colvarCreatedBy.AutoIncrement = false;
				colvarCreatedBy.IsNullable = true;
				colvarCreatedBy.IsPrimaryKey = false;
				colvarCreatedBy.IsForeignKey = false;
				colvarCreatedBy.IsReadOnly = false;
				colvarCreatedBy.DefaultSetting = @"";
				colvarCreatedBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedBy);
				
				TableSchema.TableColumn colvarModifiedBy = new TableSchema.TableColumn(schema);
				colvarModifiedBy.ColumnName = "ModifiedBy";
				colvarModifiedBy.DataType = DbType.String;
				colvarModifiedBy.MaxLength = 64;
				colvarModifiedBy.AutoIncrement = false;
				colvarModifiedBy.IsNullable = true;
				colvarModifiedBy.IsPrimaryKey = false;
				colvarModifiedBy.IsForeignKey = false;
				colvarModifiedBy.IsReadOnly = false;
				colvarModifiedBy.DefaultSetting = @"";
				colvarModifiedBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedBy);
				
				TableSchema.TableColumn colvarReportTemplateID = new TableSchema.TableColumn(schema);
				colvarReportTemplateID.ColumnName = "ReportTemplateID";
				colvarReportTemplateID.DataType = DbType.Int32;
				colvarReportTemplateID.MaxLength = 0;
				colvarReportTemplateID.AutoIncrement = false;
				colvarReportTemplateID.IsNullable = false;
				colvarReportTemplateID.IsPrimaryKey = false;
				colvarReportTemplateID.IsForeignKey = true;
				colvarReportTemplateID.IsReadOnly = false;
				
						colvarReportTemplateID.DefaultSetting = @"((0))";
				
					colvarReportTemplateID.ForeignKeyTableName = "ReportTemplate";
				schema.Columns.Add(colvarReportTemplateID);
				
				TableSchema.TableColumn colvarXMLParameters = new TableSchema.TableColumn(schema);
				colvarXMLParameters.ColumnName = "XMLParameters";
				colvarXMLParameters.DataType = DbType.String;
				colvarXMLParameters.MaxLength = -1;
				colvarXMLParameters.AutoIncrement = false;
				colvarXMLParameters.IsNullable = false;
				colvarXMLParameters.IsPrimaryKey = false;
				colvarXMLParameters.IsForeignKey = false;
				colvarXMLParameters.IsReadOnly = false;
				
						colvarXMLParameters.DefaultSetting = @"('')";
				colvarXMLParameters.ForeignKeyTableName = "";
				schema.Columns.Add(colvarXMLParameters);
				
				TableSchema.TableColumn colvarIsSymphonyIdMode = new TableSchema.TableColumn(schema);
				colvarIsSymphonyIdMode.ColumnName = "IsSymphonyIdMode";
				colvarIsSymphonyIdMode.DataType = DbType.Boolean;
				colvarIsSymphonyIdMode.MaxLength = 0;
				colvarIsSymphonyIdMode.AutoIncrement = false;
				colvarIsSymphonyIdMode.IsNullable = true;
				colvarIsSymphonyIdMode.IsPrimaryKey = false;
				colvarIsSymphonyIdMode.IsForeignKey = false;
				colvarIsSymphonyIdMode.IsReadOnly = false;
				colvarIsSymphonyIdMode.DefaultSetting = @"";
				colvarIsSymphonyIdMode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSymphonyIdMode);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["Symphony"].AddSchema("Reports",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name 
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}
		  
		[XmlAttribute("ReportTypeID")]
		[Bindable(true)]
		public int ReportTypeID 
		{
			get { return GetColumnValue<int>(Columns.ReportTypeID); }
			set { SetColumnValue(Columns.ReportTypeID, value); }
		}
		  
		[XmlAttribute("NotifyWhenReady")]
		[Bindable(true)]
		public bool NotifyWhenReady 
		{
			get { return GetColumnValue<bool>(Columns.NotifyWhenReady); }
			set { SetColumnValue(Columns.NotifyWhenReady, value); }
		}
		  
		[XmlAttribute("DownloadCSV")]
		[Bindable(true)]
		public bool DownloadCSV 
		{
			get { return GetColumnValue<bool>(Columns.DownloadCSV); }
			set { SetColumnValue(Columns.DownloadCSV, value); }
		}
		  
		[XmlAttribute("DownloadXLS")]
		[Bindable(true)]
		public bool DownloadXLS 
		{
			get { return GetColumnValue<bool>(Columns.DownloadXLS); }
			set { SetColumnValue(Columns.DownloadXLS, value); }
		}
		  
		[XmlAttribute("DownloadPDF")]
		[Bindable(true)]
		public bool DownloadPDF 
		{
			get { return GetColumnValue<bool>(Columns.DownloadPDF); }
			set { SetColumnValue(Columns.DownloadPDF, value); }
		}
		  
		[XmlAttribute("Parameters")]
		[Bindable(true)]
		public string Parameters 
		{
			get { return GetColumnValue<string>(Columns.Parameters); }
			set { SetColumnValue(Columns.Parameters, value); }
		}
		  
		[XmlAttribute("ScheduleType")]
		[Bindable(true)]
		public int ScheduleType 
		{
			get { return GetColumnValue<int>(Columns.ScheduleType); }
			set { SetColumnValue(Columns.ScheduleType, value); }
		}
		  
		[XmlAttribute("ScheduleHour")]
		[Bindable(true)]
		public int? ScheduleHour 
		{
			get { return GetColumnValue<int?>(Columns.ScheduleHour); }
			set { SetColumnValue(Columns.ScheduleHour, value); }
		}
		  
		[XmlAttribute("ScheduleDaysOfWeek")]
		[Bindable(true)]
		public int? ScheduleDaysOfWeek 
		{
			get { return GetColumnValue<int?>(Columns.ScheduleDaysOfWeek); }
			set { SetColumnValue(Columns.ScheduleDaysOfWeek, value); }
		}
		  
		[XmlAttribute("ScheduleDayOfMonth")]
		[Bindable(true)]
		public int? ScheduleDayOfMonth 
		{
			get { return GetColumnValue<int?>(Columns.ScheduleDayOfMonth); }
			set { SetColumnValue(Columns.ScheduleDayOfMonth, value); }
		}
		  
		[XmlAttribute("ScheduleMonth")]
		[Bindable(true)]
		public int? ScheduleMonth 
		{
			get { return GetColumnValue<int?>(Columns.ScheduleMonth); }
			set { SetColumnValue(Columns.ScheduleMonth, value); }
		}
		  
		[XmlAttribute("OwnerID")]
		[Bindable(true)]
		public int OwnerID 
		{
			get { return GetColumnValue<int>(Columns.OwnerID); }
			set { SetColumnValue(Columns.OwnerID, value); }
		}
		  
		[XmlAttribute("CustomerID")]
		[Bindable(true)]
		public int CustomerID 
		{
			get { return GetColumnValue<int>(Columns.CustomerID); }
			set { SetColumnValue(Columns.CustomerID, value); }
		}
		  
		[XmlAttribute("IsFavorite")]
		[Bindable(true)]
		public bool IsFavorite 
		{
			get { return GetColumnValue<bool>(Columns.IsFavorite); }
			set { SetColumnValue(Columns.IsFavorite, value); }
		}
		  
		[XmlAttribute("CreatedOn")]
		[Bindable(true)]
		public DateTime CreatedOn 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedOn); }
			set { SetColumnValue(Columns.CreatedOn, value); }
		}
		  
		[XmlAttribute("ModifiedOn")]
		[Bindable(true)]
		public DateTime ModifiedOn 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifiedOn); }
			set { SetColumnValue(Columns.ModifiedOn, value); }
		}
		  
		[XmlAttribute("CreatedBy")]
		[Bindable(true)]
		public string CreatedBy 
		{
			get { return GetColumnValue<string>(Columns.CreatedBy); }
			set { SetColumnValue(Columns.CreatedBy, value); }
		}
		  
		[XmlAttribute("ModifiedBy")]
		[Bindable(true)]
		public string ModifiedBy 
		{
			get { return GetColumnValue<string>(Columns.ModifiedBy); }
			set { SetColumnValue(Columns.ModifiedBy, value); }
		}
		  
		[XmlAttribute("ReportTemplateID")]
		[Bindable(true)]
		public int ReportTemplateID 
		{
			get { return GetColumnValue<int>(Columns.ReportTemplateID); }
			set { SetColumnValue(Columns.ReportTemplateID, value); }
		}
		  
		[XmlAttribute("XMLParameters")]
		[Bindable(true)]
		public string XMLParameters 
		{
			get { return GetColumnValue<string>(Columns.XMLParameters); }
			set { SetColumnValue(Columns.XMLParameters, value); }
		}
		  
		[XmlAttribute("IsSymphonyIdMode")]
		[Bindable(true)]
		public bool? IsSymphonyIdMode 
		{
			get { return GetColumnValue<bool?>(Columns.IsSymphonyIdMode); }
			set { SetColumnValue(Columns.IsSymphonyIdMode, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a ReportTemplate ActiveRecord object related to this Report
		/// </summary>
		private Symphony.Core.Data.ReportTemplate _ReportTemplate;
		public Symphony.Core.Data.ReportTemplate ReportTemplate
		{
			get { 
			    if(_ReportTemplate == null){
			        _ReportTemplate = Symphony.Core.Data.ReportTemplate.FetchByID(this.ReportTemplateID); 
			    }
			    return _ReportTemplate;
			}
			set { 
			    SetColumnValue("ReportTemplateID", value.Id); 
			    _ReportTemplate = value;
			}
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varName,int varReportTypeID,bool varNotifyWhenReady,bool varDownloadCSV,bool varDownloadXLS,bool varDownloadPDF,string varParameters,int varScheduleType,int? varScheduleHour,int? varScheduleDaysOfWeek,int? varScheduleDayOfMonth,int? varScheduleMonth,int varOwnerID,int varCustomerID,bool varIsFavorite,DateTime varCreatedOn,DateTime varModifiedOn,string varCreatedBy,string varModifiedBy,int varReportTemplateID,string varXMLParameters,bool? varIsSymphonyIdMode)
		{
			Report item = new Report();
			
			item.Name = varName;
			
			item.ReportTypeID = varReportTypeID;
			
			item.NotifyWhenReady = varNotifyWhenReady;
			
			item.DownloadCSV = varDownloadCSV;
			
			item.DownloadXLS = varDownloadXLS;
			
			item.DownloadPDF = varDownloadPDF;
			
			item.Parameters = varParameters;
			
			item.ScheduleType = varScheduleType;
			
			item.ScheduleHour = varScheduleHour;
			
			item.ScheduleDaysOfWeek = varScheduleDaysOfWeek;
			
			item.ScheduleDayOfMonth = varScheduleDayOfMonth;
			
			item.ScheduleMonth = varScheduleMonth;
			
			item.OwnerID = varOwnerID;
			
			item.CustomerID = varCustomerID;
			
			item.IsFavorite = varIsFavorite;
			
			item.CreatedOn = varCreatedOn;
			
			item.ModifiedOn = varModifiedOn;
			
			item.CreatedBy = varCreatedBy;
			
			item.ModifiedBy = varModifiedBy;
			
			item.ReportTemplateID = varReportTemplateID;
			
			item.XMLParameters = varXMLParameters;
			
			item.IsSymphonyIdMode = varIsSymphonyIdMode;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,string varName,int varReportTypeID,bool varNotifyWhenReady,bool varDownloadCSV,bool varDownloadXLS,bool varDownloadPDF,string varParameters,int varScheduleType,int? varScheduleHour,int? varScheduleDaysOfWeek,int? varScheduleDayOfMonth,int? varScheduleMonth,int varOwnerID,int varCustomerID,bool varIsFavorite,DateTime varCreatedOn,DateTime varModifiedOn,string varCreatedBy,string varModifiedBy,int varReportTemplateID,string varXMLParameters,bool? varIsSymphonyIdMode)
		{
			Report item = new Report();
			
				item.Id = varId;
			
				item.Name = varName;
			
				item.ReportTypeID = varReportTypeID;
			
				item.NotifyWhenReady = varNotifyWhenReady;
			
				item.DownloadCSV = varDownloadCSV;
			
				item.DownloadXLS = varDownloadXLS;
			
				item.DownloadPDF = varDownloadPDF;
			
				item.Parameters = varParameters;
			
				item.ScheduleType = varScheduleType;
			
				item.ScheduleHour = varScheduleHour;
			
				item.ScheduleDaysOfWeek = varScheduleDaysOfWeek;
			
				item.ScheduleDayOfMonth = varScheduleDayOfMonth;
			
				item.ScheduleMonth = varScheduleMonth;
			
				item.OwnerID = varOwnerID;
			
				item.CustomerID = varCustomerID;
			
				item.IsFavorite = varIsFavorite;
			
				item.CreatedOn = varCreatedOn;
			
				item.ModifiedOn = varModifiedOn;
			
				item.CreatedBy = varCreatedBy;
			
				item.ModifiedBy = varModifiedBy;
			
				item.ReportTemplateID = varReportTemplateID;
			
				item.XMLParameters = varXMLParameters;
			
				item.IsSymphonyIdMode = varIsSymphonyIdMode;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ReportTypeIDColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn NotifyWhenReadyColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DownloadCSVColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DownloadXLSColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn DownloadPDFColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ParametersColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ScheduleTypeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ScheduleHourColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ScheduleDaysOfWeekColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ScheduleDayOfMonthColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ScheduleMonthColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn OwnerIDColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CustomerIDColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn IsFavoriteColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedOnColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedOnColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedByColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedByColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn ReportTemplateIDColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn XMLParametersColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn IsSymphonyIdModeColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string Name = @"Name";
			 public static string ReportTypeID = @"ReportTypeID";
			 public static string NotifyWhenReady = @"NotifyWhenReady";
			 public static string DownloadCSV = @"DownloadCSV";
			 public static string DownloadXLS = @"DownloadXLS";
			 public static string DownloadPDF = @"DownloadPDF";
			 public static string Parameters = @"Parameters";
			 public static string ScheduleType = @"ScheduleType";
			 public static string ScheduleHour = @"ScheduleHour";
			 public static string ScheduleDaysOfWeek = @"ScheduleDaysOfWeek";
			 public static string ScheduleDayOfMonth = @"ScheduleDayOfMonth";
			 public static string ScheduleMonth = @"ScheduleMonth";
			 public static string OwnerID = @"OwnerID";
			 public static string CustomerID = @"CustomerID";
			 public static string IsFavorite = @"IsFavorite";
			 public static string CreatedOn = @"CreatedOn";
			 public static string ModifiedOn = @"ModifiedOn";
			 public static string CreatedBy = @"CreatedBy";
			 public static string ModifiedBy = @"ModifiedBy";
			 public static string ReportTemplateID = @"ReportTemplateID";
			 public static string XMLParameters = @"XMLParameters";
			 public static string IsSymphonyIdMode = @"IsSymphonyIdMode";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
