using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data{
    /// <summary>
    /// Strongly-typed collection for the TileApplication class.
    /// </summary>
    [Serializable]
    public partial class TileApplicationCollection : ReadOnlyList<TileApplication, TileApplicationCollection>
    {        
        public TileApplicationCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the TileApplication view.
    /// </summary>
    [Serializable]
    public partial class TileApplication : ReadOnlyRecord<TileApplication>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("TileApplication", TableType.View, DataService.GetInstance("Symphony"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "ID";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "Name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 50;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = false;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarTileUri = new TableSchema.TableColumn(schema);
                colvarTileUri.ColumnName = "TileUri";
                colvarTileUri.DataType = DbType.String;
                colvarTileUri.MaxLength = 255;
                colvarTileUri.AutoIncrement = false;
                colvarTileUri.IsNullable = false;
                colvarTileUri.IsPrimaryKey = false;
                colvarTileUri.IsForeignKey = false;
                colvarTileUri.IsReadOnly = false;
                
                schema.Columns.Add(colvarTileUri);
                
                TableSchema.TableColumn colvarApplicationID = new TableSchema.TableColumn(schema);
                colvarApplicationID.ColumnName = "ApplicationID";
                colvarApplicationID.DataType = DbType.Int32;
                colvarApplicationID.MaxLength = 0;
                colvarApplicationID.AutoIncrement = false;
                colvarApplicationID.IsNullable = false;
                colvarApplicationID.IsPrimaryKey = false;
                colvarApplicationID.IsForeignKey = false;
                colvarApplicationID.IsReadOnly = false;
                
                schema.Columns.Add(colvarApplicationID);
                
                TableSchema.TableColumn colvarApplicationName = new TableSchema.TableColumn(schema);
                colvarApplicationName.ColumnName = "ApplicationName";
                colvarApplicationName.DataType = DbType.String;
                colvarApplicationName.MaxLength = 50;
                colvarApplicationName.AutoIncrement = false;
                colvarApplicationName.IsNullable = false;
                colvarApplicationName.IsPrimaryKey = false;
                colvarApplicationName.IsForeignKey = false;
                colvarApplicationName.IsReadOnly = false;
                
                schema.Columns.Add(colvarApplicationName);
                
                TableSchema.TableColumn colvarAuthenticationUri = new TableSchema.TableColumn(schema);
                colvarAuthenticationUri.ColumnName = "AuthenticationUri";
                colvarAuthenticationUri.DataType = DbType.String;
                colvarAuthenticationUri.MaxLength = 255;
                colvarAuthenticationUri.AutoIncrement = false;
                colvarAuthenticationUri.IsNullable = false;
                colvarAuthenticationUri.IsPrimaryKey = false;
                colvarAuthenticationUri.IsForeignKey = false;
                colvarAuthenticationUri.IsReadOnly = false;
                
                schema.Columns.Add(colvarAuthenticationUri);
                
                TableSchema.TableColumn colvarCreatedOn = new TableSchema.TableColumn(schema);
                colvarCreatedOn.ColumnName = "CreatedOn";
                colvarCreatedOn.DataType = DbType.DateTime;
                colvarCreatedOn.MaxLength = 0;
                colvarCreatedOn.AutoIncrement = false;
                colvarCreatedOn.IsNullable = false;
                colvarCreatedOn.IsPrimaryKey = false;
                colvarCreatedOn.IsForeignKey = false;
                colvarCreatedOn.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreatedOn);
                
                TableSchema.TableColumn colvarCreatedBy = new TableSchema.TableColumn(schema);
                colvarCreatedBy.ColumnName = "CreatedBy";
                colvarCreatedBy.DataType = DbType.String;
                colvarCreatedBy.MaxLength = 64;
                colvarCreatedBy.AutoIncrement = false;
                colvarCreatedBy.IsNullable = true;
                colvarCreatedBy.IsPrimaryKey = false;
                colvarCreatedBy.IsForeignKey = false;
                colvarCreatedBy.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreatedBy);
                
                TableSchema.TableColumn colvarModifiedOn = new TableSchema.TableColumn(schema);
                colvarModifiedOn.ColumnName = "ModifiedOn";
                colvarModifiedOn.DataType = DbType.DateTime;
                colvarModifiedOn.MaxLength = 0;
                colvarModifiedOn.AutoIncrement = false;
                colvarModifiedOn.IsNullable = false;
                colvarModifiedOn.IsPrimaryKey = false;
                colvarModifiedOn.IsForeignKey = false;
                colvarModifiedOn.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifiedOn);
                
                TableSchema.TableColumn colvarModifiedBy = new TableSchema.TableColumn(schema);
                colvarModifiedBy.ColumnName = "ModifiedBy";
                colvarModifiedBy.DataType = DbType.String;
                colvarModifiedBy.MaxLength = 64;
                colvarModifiedBy.AutoIncrement = false;
                colvarModifiedBy.IsNullable = true;
                colvarModifiedBy.IsPrimaryKey = false;
                colvarModifiedBy.IsForeignKey = false;
                colvarModifiedBy.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifiedBy);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["Symphony"].AddSchema("TileApplication",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public TileApplication()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public TileApplication(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public TileApplication(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public TileApplication(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID");
		    }
            set 
		    {
			    SetColumnValue("ID", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("Name");
		    }
            set 
		    {
			    SetColumnValue("Name", value);
            }
        }
	      
        [XmlAttribute("TileUri")]
        [Bindable(true)]
        public string TileUri 
	    {
		    get
		    {
			    return GetColumnValue<string>("TileUri");
		    }
            set 
		    {
			    SetColumnValue("TileUri", value);
            }
        }
	      
        [XmlAttribute("ApplicationID")]
        [Bindable(true)]
        public int ApplicationID 
	    {
		    get
		    {
			    return GetColumnValue<int>("ApplicationID");
		    }
            set 
		    {
			    SetColumnValue("ApplicationID", value);
            }
        }
	      
        [XmlAttribute("ApplicationName")]
        [Bindable(true)]
        public string ApplicationName 
	    {
		    get
		    {
			    return GetColumnValue<string>("ApplicationName");
		    }
            set 
		    {
			    SetColumnValue("ApplicationName", value);
            }
        }
	      
        [XmlAttribute("AuthenticationUri")]
        [Bindable(true)]
        public string AuthenticationUri 
	    {
		    get
		    {
			    return GetColumnValue<string>("AuthenticationUri");
		    }
            set 
		    {
			    SetColumnValue("AuthenticationUri", value);
            }
        }
	      
        [XmlAttribute("CreatedOn")]
        [Bindable(true)]
        public DateTime CreatedOn 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("CreatedOn");
		    }
            set 
		    {
			    SetColumnValue("CreatedOn", value);
            }
        }
	      
        [XmlAttribute("CreatedBy")]
        [Bindable(true)]
        public string CreatedBy 
	    {
		    get
		    {
			    return GetColumnValue<string>("CreatedBy");
		    }
            set 
		    {
			    SetColumnValue("CreatedBy", value);
            }
        }
	      
        [XmlAttribute("ModifiedOn")]
        [Bindable(true)]
        public DateTime ModifiedOn 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("ModifiedOn");
		    }
            set 
		    {
			    SetColumnValue("ModifiedOn", value);
            }
        }
	      
        [XmlAttribute("ModifiedBy")]
        [Bindable(true)]
        public string ModifiedBy 
	    {
		    get
		    {
			    return GetColumnValue<string>("ModifiedBy");
		    }
            set 
		    {
			    SetColumnValue("ModifiedBy", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"ID";
            
            public static string Name = @"Name";
            
            public static string TileUri = @"TileUri";
            
            public static string ApplicationID = @"ApplicationID";
            
            public static string ApplicationName = @"ApplicationName";
            
            public static string AuthenticationUri = @"AuthenticationUri";
            
            public static string CreatedOn = @"CreatedOn";
            
            public static string CreatedBy = @"CreatedBy";
            
            public static string ModifiedOn = @"ModifiedOn";
            
            public static string ModifiedBy = @"ModifiedBy";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
