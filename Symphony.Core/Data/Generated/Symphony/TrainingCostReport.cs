using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data{
    /// <summary>
    /// Strongly-typed collection for the TrainingCostReport class.
    /// </summary>
    [Serializable]
    public partial class TrainingCostReportCollection : ReadOnlyList<TrainingCostReport, TrainingCostReportCollection>
    {        
        public TrainingCostReportCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the TrainingCostReport view.
    /// </summary>
    [Serializable]
    public partial class TrainingCostReport : ReadOnlyRecord<TrainingCostReport>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("TrainingCostReport", TableType.View, DataService.GetInstance("Symphony"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCustomerID = new TableSchema.TableColumn(schema);
                colvarCustomerID.ColumnName = "CustomerID";
                colvarCustomerID.DataType = DbType.Int32;
                colvarCustomerID.MaxLength = 0;
                colvarCustomerID.AutoIncrement = false;
                colvarCustomerID.IsNullable = false;
                colvarCustomerID.IsPrimaryKey = false;
                colvarCustomerID.IsForeignKey = false;
                colvarCustomerID.IsReadOnly = false;
                
                schema.Columns.Add(colvarCustomerID);
                
                TableSchema.TableColumn colvarCourseID = new TableSchema.TableColumn(schema);
                colvarCourseID.ColumnName = "CourseID";
                colvarCourseID.DataType = DbType.Int32;
                colvarCourseID.MaxLength = 0;
                colvarCourseID.AutoIncrement = false;
                colvarCourseID.IsNullable = true;
                colvarCourseID.IsPrimaryKey = false;
                colvarCourseID.IsForeignKey = false;
                colvarCourseID.IsReadOnly = false;
                
                schema.Columns.Add(colvarCourseID);
                
                TableSchema.TableColumn colvarLocationID = new TableSchema.TableColumn(schema);
                colvarLocationID.ColumnName = "LocationID";
                colvarLocationID.DataType = DbType.Int32;
                colvarLocationID.MaxLength = 0;
                colvarLocationID.AutoIncrement = false;
                colvarLocationID.IsNullable = false;
                colvarLocationID.IsPrimaryKey = false;
                colvarLocationID.IsForeignKey = false;
                colvarLocationID.IsReadOnly = false;
                
                schema.Columns.Add(colvarLocationID);
                
                TableSchema.TableColumn colvarJobRoleID = new TableSchema.TableColumn(schema);
                colvarJobRoleID.ColumnName = "JobRoleID";
                colvarJobRoleID.DataType = DbType.Int32;
                colvarJobRoleID.MaxLength = 0;
                colvarJobRoleID.AutoIncrement = false;
                colvarJobRoleID.IsNullable = false;
                colvarJobRoleID.IsPrimaryKey = false;
                colvarJobRoleID.IsForeignKey = false;
                colvarJobRoleID.IsReadOnly = false;
                
                schema.Columns.Add(colvarJobRoleID);
                
                TableSchema.TableColumn colvarAudienceID = new TableSchema.TableColumn(schema);
                colvarAudienceID.ColumnName = "AudienceID";
                colvarAudienceID.DataType = DbType.String;
                colvarAudienceID.MaxLength = -1;
                colvarAudienceID.AutoIncrement = false;
                colvarAudienceID.IsNullable = false;
                colvarAudienceID.IsPrimaryKey = false;
                colvarAudienceID.IsForeignKey = false;
                colvarAudienceID.IsReadOnly = false;
                
                schema.Columns.Add(colvarAudienceID);
                
                TableSchema.TableColumn colvarSupervisorID = new TableSchema.TableColumn(schema);
                colvarSupervisorID.ColumnName = "SupervisorID";
                colvarSupervisorID.DataType = DbType.Int32;
                colvarSupervisorID.MaxLength = 0;
                colvarSupervisorID.AutoIncrement = false;
                colvarSupervisorID.IsNullable = false;
                colvarSupervisorID.IsPrimaryKey = false;
                colvarSupervisorID.IsForeignKey = false;
                colvarSupervisorID.IsReadOnly = false;
                
                schema.Columns.Add(colvarSupervisorID);
                
                TableSchema.TableColumn colvarTrainingCycleID = new TableSchema.TableColumn(schema);
                colvarTrainingCycleID.ColumnName = "TrainingCycleID";
                colvarTrainingCycleID.DataType = DbType.Int32;
                colvarTrainingCycleID.MaxLength = 0;
                colvarTrainingCycleID.AutoIncrement = false;
                colvarTrainingCycleID.IsNullable = true;
                colvarTrainingCycleID.IsPrimaryKey = false;
                colvarTrainingCycleID.IsForeignKey = false;
                colvarTrainingCycleID.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrainingCycleID);
                
                TableSchema.TableColumn colvarCourseName = new TableSchema.TableColumn(schema);
                colvarCourseName.ColumnName = "CourseName";
                colvarCourseName.DataType = DbType.String;
                colvarCourseName.MaxLength = 50;
                colvarCourseName.AutoIncrement = false;
                colvarCourseName.IsNullable = false;
                colvarCourseName.IsPrimaryKey = false;
                colvarCourseName.IsForeignKey = false;
                colvarCourseName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCourseName);
                
                TableSchema.TableColumn colvarLocationName = new TableSchema.TableColumn(schema);
                colvarLocationName.ColumnName = "LocationName";
                colvarLocationName.DataType = DbType.String;
                colvarLocationName.MaxLength = 50;
                colvarLocationName.AutoIncrement = false;
                colvarLocationName.IsNullable = false;
                colvarLocationName.IsPrimaryKey = false;
                colvarLocationName.IsForeignKey = false;
                colvarLocationName.IsReadOnly = false;
                
                schema.Columns.Add(colvarLocationName);
                
                TableSchema.TableColumn colvarJobRoleName = new TableSchema.TableColumn(schema);
                colvarJobRoleName.ColumnName = "JobRoleName";
                colvarJobRoleName.DataType = DbType.String;
                colvarJobRoleName.MaxLength = 50;
                colvarJobRoleName.AutoIncrement = false;
                colvarJobRoleName.IsNullable = false;
                colvarJobRoleName.IsPrimaryKey = false;
                colvarJobRoleName.IsForeignKey = false;
                colvarJobRoleName.IsReadOnly = false;
                
                schema.Columns.Add(colvarJobRoleName);
                
                TableSchema.TableColumn colvarAudienceName = new TableSchema.TableColumn(schema);
                colvarAudienceName.ColumnName = "AudienceName";
                colvarAudienceName.DataType = DbType.String;
                colvarAudienceName.MaxLength = -1;
                colvarAudienceName.AutoIncrement = false;
                colvarAudienceName.IsNullable = false;
                colvarAudienceName.IsPrimaryKey = false;
                colvarAudienceName.IsForeignKey = false;
                colvarAudienceName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAudienceName);
                
                TableSchema.TableColumn colvarSupervisorName = new TableSchema.TableColumn(schema);
                colvarSupervisorName.ColumnName = "SupervisorName";
                colvarSupervisorName.DataType = DbType.String;
                colvarSupervisorName.MaxLength = 102;
                colvarSupervisorName.AutoIncrement = false;
                colvarSupervisorName.IsNullable = false;
                colvarSupervisorName.IsPrimaryKey = false;
                colvarSupervisorName.IsForeignKey = false;
                colvarSupervisorName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSupervisorName);
                
                TableSchema.TableColumn colvarTrainingCycleName = new TableSchema.TableColumn(schema);
                colvarTrainingCycleName.ColumnName = "TrainingCycleName";
                colvarTrainingCycleName.DataType = DbType.String;
                colvarTrainingCycleName.MaxLength = 100;
                colvarTrainingCycleName.AutoIncrement = false;
                colvarTrainingCycleName.IsNullable = true;
                colvarTrainingCycleName.IsPrimaryKey = false;
                colvarTrainingCycleName.IsForeignKey = false;
                colvarTrainingCycleName.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrainingCycleName);
                
                TableSchema.TableColumn colvarStudentCount = new TableSchema.TableColumn(schema);
                colvarStudentCount.ColumnName = "StudentCount";
                colvarStudentCount.DataType = DbType.Int32;
                colvarStudentCount.MaxLength = 0;
                colvarStudentCount.AutoIncrement = false;
                colvarStudentCount.IsNullable = true;
                colvarStudentCount.IsPrimaryKey = false;
                colvarStudentCount.IsForeignKey = false;
                colvarStudentCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarStudentCount);
                
                TableSchema.TableColumn colvarTrainingCost = new TableSchema.TableColumn(schema);
                colvarTrainingCost.ColumnName = "TrainingCost";
                colvarTrainingCost.DataType = DbType.Decimal;
                colvarTrainingCost.MaxLength = 0;
                colvarTrainingCost.AutoIncrement = false;
                colvarTrainingCost.IsNullable = true;
                colvarTrainingCost.IsPrimaryKey = false;
                colvarTrainingCost.IsForeignKey = false;
                colvarTrainingCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrainingCost);
                
                TableSchema.TableColumn colvarUserNameXAlias = new TableSchema.TableColumn(schema);
                colvarUserNameXAlias.ColumnName = "UserNameAlias";
                colvarUserNameXAlias.DataType = DbType.String;
                colvarUserNameXAlias.MaxLength = 50;
                colvarUserNameXAlias.AutoIncrement = false;
                colvarUserNameXAlias.IsNullable = true;
                colvarUserNameXAlias.IsPrimaryKey = false;
                colvarUserNameXAlias.IsForeignKey = false;
                colvarUserNameXAlias.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserNameXAlias);
                
                TableSchema.TableColumn colvarJobRoleAlias = new TableSchema.TableColumn(schema);
                colvarJobRoleAlias.ColumnName = "JobRoleAlias";
                colvarJobRoleAlias.DataType = DbType.AnsiString;
                colvarJobRoleAlias.MaxLength = 50;
                colvarJobRoleAlias.AutoIncrement = false;
                colvarJobRoleAlias.IsNullable = true;
                colvarJobRoleAlias.IsPrimaryKey = false;
                colvarJobRoleAlias.IsForeignKey = false;
                colvarJobRoleAlias.IsReadOnly = false;
                
                schema.Columns.Add(colvarJobRoleAlias);
                
                TableSchema.TableColumn colvarLocationAlias = new TableSchema.TableColumn(schema);
                colvarLocationAlias.ColumnName = "LocationAlias";
                colvarLocationAlias.DataType = DbType.AnsiString;
                colvarLocationAlias.MaxLength = 50;
                colvarLocationAlias.AutoIncrement = false;
                colvarLocationAlias.IsNullable = true;
                colvarLocationAlias.IsPrimaryKey = false;
                colvarLocationAlias.IsForeignKey = false;
                colvarLocationAlias.IsReadOnly = false;
                
                schema.Columns.Add(colvarLocationAlias);
                
                TableSchema.TableColumn colvarAudienceAlias = new TableSchema.TableColumn(schema);
                colvarAudienceAlias.ColumnName = "AudienceAlias";
                colvarAudienceAlias.DataType = DbType.AnsiString;
                colvarAudienceAlias.MaxLength = 50;
                colvarAudienceAlias.AutoIncrement = false;
                colvarAudienceAlias.IsNullable = true;
                colvarAudienceAlias.IsPrimaryKey = false;
                colvarAudienceAlias.IsForeignKey = false;
                colvarAudienceAlias.IsReadOnly = false;
                
                schema.Columns.Add(colvarAudienceAlias);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["Symphony"].AddSchema("TrainingCostReport",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public TrainingCostReport()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public TrainingCostReport(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public TrainingCostReport(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public TrainingCostReport(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CustomerID")]
        [Bindable(true)]
        public int CustomerID 
	    {
		    get
		    {
			    return GetColumnValue<int>("CustomerID");
		    }
            set 
		    {
			    SetColumnValue("CustomerID", value);
            }
        }
	      
        [XmlAttribute("CourseID")]
        [Bindable(true)]
        public int? CourseID 
	    {
		    get
		    {
			    return GetColumnValue<int?>("CourseID");
		    }
            set 
		    {
			    SetColumnValue("CourseID", value);
            }
        }
	      
        [XmlAttribute("LocationID")]
        [Bindable(true)]
        public int LocationID 
	    {
		    get
		    {
			    return GetColumnValue<int>("LocationID");
		    }
            set 
		    {
			    SetColumnValue("LocationID", value);
            }
        }
	      
        [XmlAttribute("JobRoleID")]
        [Bindable(true)]
        public int JobRoleID 
	    {
		    get
		    {
			    return GetColumnValue<int>("JobRoleID");
		    }
            set 
		    {
			    SetColumnValue("JobRoleID", value);
            }
        }
	      
        [XmlAttribute("AudienceID")]
        [Bindable(true)]
        public string AudienceID 
	    {
		    get
		    {
			    return GetColumnValue<string>("AudienceID");
		    }
            set 
		    {
			    SetColumnValue("AudienceID", value);
            }
        }
	      
        [XmlAttribute("SupervisorID")]
        [Bindable(true)]
        public int SupervisorID 
	    {
		    get
		    {
			    return GetColumnValue<int>("SupervisorID");
		    }
            set 
		    {
			    SetColumnValue("SupervisorID", value);
            }
        }
	      
        [XmlAttribute("TrainingCycleID")]
        [Bindable(true)]
        public int? TrainingCycleID 
	    {
		    get
		    {
			    return GetColumnValue<int?>("TrainingCycleID");
		    }
            set 
		    {
			    SetColumnValue("TrainingCycleID", value);
            }
        }
	      
        [XmlAttribute("CourseName")]
        [Bindable(true)]
        public string CourseName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CourseName");
		    }
            set 
		    {
			    SetColumnValue("CourseName", value);
            }
        }
	      
        [XmlAttribute("LocationName")]
        [Bindable(true)]
        public string LocationName 
	    {
		    get
		    {
			    return GetColumnValue<string>("LocationName");
		    }
            set 
		    {
			    SetColumnValue("LocationName", value);
            }
        }
	      
        [XmlAttribute("JobRoleName")]
        [Bindable(true)]
        public string JobRoleName 
	    {
		    get
		    {
			    return GetColumnValue<string>("JobRoleName");
		    }
            set 
		    {
			    SetColumnValue("JobRoleName", value);
            }
        }
	      
        [XmlAttribute("AudienceName")]
        [Bindable(true)]
        public string AudienceName 
	    {
		    get
		    {
			    return GetColumnValue<string>("AudienceName");
		    }
            set 
		    {
			    SetColumnValue("AudienceName", value);
            }
        }
	      
        [XmlAttribute("SupervisorName")]
        [Bindable(true)]
        public string SupervisorName 
	    {
		    get
		    {
			    return GetColumnValue<string>("SupervisorName");
		    }
            set 
		    {
			    SetColumnValue("SupervisorName", value);
            }
        }
	      
        [XmlAttribute("TrainingCycleName")]
        [Bindable(true)]
        public string TrainingCycleName 
	    {
		    get
		    {
			    return GetColumnValue<string>("TrainingCycleName");
		    }
            set 
		    {
			    SetColumnValue("TrainingCycleName", value);
            }
        }
	      
        [XmlAttribute("StudentCount")]
        [Bindable(true)]
        public int? StudentCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("StudentCount");
		    }
            set 
		    {
			    SetColumnValue("StudentCount", value);
            }
        }
	      
        [XmlAttribute("TrainingCost")]
        [Bindable(true)]
        public decimal? TrainingCost 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("TrainingCost");
		    }
            set 
		    {
			    SetColumnValue("TrainingCost", value);
            }
        }
	      
        [XmlAttribute("UserNameXAlias")]
        [Bindable(true)]
        public string UserNameXAlias 
	    {
		    get
		    {
			    return GetColumnValue<string>("UserNameAlias");
		    }
            set 
		    {
			    SetColumnValue("UserNameAlias", value);
            }
        }
	      
        [XmlAttribute("JobRoleAlias")]
        [Bindable(true)]
        public string JobRoleAlias 
	    {
		    get
		    {
			    return GetColumnValue<string>("JobRoleAlias");
		    }
            set 
		    {
			    SetColumnValue("JobRoleAlias", value);
            }
        }
	      
        [XmlAttribute("LocationAlias")]
        [Bindable(true)]
        public string LocationAlias 
	    {
		    get
		    {
			    return GetColumnValue<string>("LocationAlias");
		    }
            set 
		    {
			    SetColumnValue("LocationAlias", value);
            }
        }
	      
        [XmlAttribute("AudienceAlias")]
        [Bindable(true)]
        public string AudienceAlias 
	    {
		    get
		    {
			    return GetColumnValue<string>("AudienceAlias");
		    }
            set 
		    {
			    SetColumnValue("AudienceAlias", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CustomerID = @"CustomerID";
            
            public static string CourseID = @"CourseID";
            
            public static string LocationID = @"LocationID";
            
            public static string JobRoleID = @"JobRoleID";
            
            public static string AudienceID = @"AudienceID";
            
            public static string SupervisorID = @"SupervisorID";
            
            public static string TrainingCycleID = @"TrainingCycleID";
            
            public static string CourseName = @"CourseName";
            
            public static string LocationName = @"LocationName";
            
            public static string JobRoleName = @"JobRoleName";
            
            public static string AudienceName = @"AudienceName";
            
            public static string SupervisorName = @"SupervisorName";
            
            public static string TrainingCycleName = @"TrainingCycleName";
            
            public static string StudentCount = @"StudentCount";
            
            public static string TrainingCost = @"TrainingCost";
            
            public static string UserNameXAlias = @"UserNameAlias";
            
            public static string JobRoleAlias = @"JobRoleAlias";
            
            public static string LocationAlias = @"LocationAlias";
            
            public static string AudienceAlias = @"AudienceAlias";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
