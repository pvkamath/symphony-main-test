using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
	/// <summary>
	/// Strongly-typed collection for the PortalSetting class.
	/// </summary>
    [Serializable]
	public partial class PortalSettingCollection : ActiveList<PortalSetting, PortalSettingCollection>
	{	   
		public PortalSettingCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PortalSettingCollection</returns>
		public PortalSettingCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PortalSetting o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the PortalSettings table.
	/// </summary>
	[Serializable]
	public partial class PortalSetting : Symphony.Core.Data.ActiveRecordCustom<PortalSetting>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public PortalSetting()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PortalSetting(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public PortalSetting(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public PortalSetting(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("PortalSettings", TableType.Table, DataService.GetInstance("Symphony"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCustomerId = new TableSchema.TableColumn(schema);
				colvarCustomerId.ColumnName = "CustomerId";
				colvarCustomerId.DataType = DbType.Int32;
				colvarCustomerId.MaxLength = 0;
				colvarCustomerId.AutoIncrement = false;
				colvarCustomerId.IsNullable = false;
				colvarCustomerId.IsPrimaryKey = false;
				colvarCustomerId.IsForeignKey = true;
				colvarCustomerId.IsReadOnly = false;
				colvarCustomerId.DefaultSetting = @"";
				
					colvarCustomerId.ForeignKeyTableName = "Customer";
				schema.Columns.Add(colvarCustomerId);
				
				TableSchema.TableColumn colvarWidgetId = new TableSchema.TableColumn(schema);
				colvarWidgetId.ColumnName = "WidgetId";
				colvarWidgetId.DataType = DbType.Int32;
				colvarWidgetId.MaxLength = 0;
				colvarWidgetId.AutoIncrement = false;
				colvarWidgetId.IsNullable = true;
				colvarWidgetId.IsPrimaryKey = false;
				colvarWidgetId.IsForeignKey = false;
				colvarWidgetId.IsReadOnly = false;
				colvarWidgetId.DefaultSetting = @"";
				colvarWidgetId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWidgetId);
				
				TableSchema.TableColumn colvarWidgetName = new TableSchema.TableColumn(schema);
				colvarWidgetName.ColumnName = "WidgetName";
				colvarWidgetName.DataType = DbType.AnsiString;
				colvarWidgetName.MaxLength = 100;
				colvarWidgetName.AutoIncrement = false;
				colvarWidgetName.IsNullable = false;
				colvarWidgetName.IsPrimaryKey = false;
				colvarWidgetName.IsForeignKey = false;
				colvarWidgetName.IsReadOnly = false;
				colvarWidgetName.DefaultSetting = @"";
				colvarWidgetName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWidgetName);
				
				TableSchema.TableColumn colvarWidgetOrderNumber = new TableSchema.TableColumn(schema);
				colvarWidgetOrderNumber.ColumnName = "WidgetOrderNumber";
				colvarWidgetOrderNumber.DataType = DbType.Int32;
				colvarWidgetOrderNumber.MaxLength = 0;
				colvarWidgetOrderNumber.AutoIncrement = false;
				colvarWidgetOrderNumber.IsNullable = true;
				colvarWidgetOrderNumber.IsPrimaryKey = false;
				colvarWidgetOrderNumber.IsForeignKey = false;
				colvarWidgetOrderNumber.IsReadOnly = false;
				colvarWidgetOrderNumber.DefaultSetting = @"";
				colvarWidgetOrderNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWidgetOrderNumber);
				
				TableSchema.TableColumn colvarWidgetIsVisible = new TableSchema.TableColumn(schema);
				colvarWidgetIsVisible.ColumnName = "WidgetIsVisible";
				colvarWidgetIsVisible.DataType = DbType.Boolean;
				colvarWidgetIsVisible.MaxLength = 0;
				colvarWidgetIsVisible.AutoIncrement = false;
				colvarWidgetIsVisible.IsNullable = true;
				colvarWidgetIsVisible.IsPrimaryKey = false;
				colvarWidgetIsVisible.IsForeignKey = false;
				colvarWidgetIsVisible.IsReadOnly = false;
				colvarWidgetIsVisible.DefaultSetting = @"";
				colvarWidgetIsVisible.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWidgetIsVisible);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["Symphony"].AddSchema("PortalSettings",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CustomerId")]
		[Bindable(true)]
		public int CustomerId 
		{
			get { return GetColumnValue<int>(Columns.CustomerId); }
			set { SetColumnValue(Columns.CustomerId, value); }
		}
		  
		[XmlAttribute("WidgetId")]
		[Bindable(true)]
		public int? WidgetId 
		{
			get { return GetColumnValue<int?>(Columns.WidgetId); }
			set { SetColumnValue(Columns.WidgetId, value); }
		}
		  
		[XmlAttribute("WidgetName")]
		[Bindable(true)]
		public string WidgetName 
		{
			get { return GetColumnValue<string>(Columns.WidgetName); }
			set { SetColumnValue(Columns.WidgetName, value); }
		}
		  
		[XmlAttribute("WidgetOrderNumber")]
		[Bindable(true)]
		public int? WidgetOrderNumber 
		{
			get { return GetColumnValue<int?>(Columns.WidgetOrderNumber); }
			set { SetColumnValue(Columns.WidgetOrderNumber, value); }
		}
		  
		[XmlAttribute("WidgetIsVisible")]
		[Bindable(true)]
		public bool? WidgetIsVisible 
		{
			get { return GetColumnValue<bool?>(Columns.WidgetIsVisible); }
			set { SetColumnValue(Columns.WidgetIsVisible, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a Customer ActiveRecord object related to this PortalSetting
		/// </summary>
		private Symphony.Core.Data.Customer _Customer;
		public Symphony.Core.Data.Customer Customer
		{
			get { 
			    if(_Customer == null){
			        _Customer = Symphony.Core.Data.Customer.FetchByID(this.CustomerId); 
			    }
			    return _Customer;
			}
			set { 
			    SetColumnValue("CustomerId", value.Id); 
			    _Customer = value;
			}
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varCustomerId,int? varWidgetId,string varWidgetName,int? varWidgetOrderNumber,bool? varWidgetIsVisible)
		{
			PortalSetting item = new PortalSetting();
			
			item.CustomerId = varCustomerId;
			
			item.WidgetId = varWidgetId;
			
			item.WidgetName = varWidgetName;
			
			item.WidgetOrderNumber = varWidgetOrderNumber;
			
			item.WidgetIsVisible = varWidgetIsVisible;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,int varCustomerId,int? varWidgetId,string varWidgetName,int? varWidgetOrderNumber,bool? varWidgetIsVisible)
		{
			PortalSetting item = new PortalSetting();
			
				item.Id = varId;
			
				item.CustomerId = varCustomerId;
			
				item.WidgetId = varWidgetId;
			
				item.WidgetName = varWidgetName;
			
				item.WidgetOrderNumber = varWidgetOrderNumber;
			
				item.WidgetIsVisible = varWidgetIsVisible;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CustomerIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn WidgetIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn WidgetNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn WidgetOrderNumberColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn WidgetIsVisibleColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string CustomerId = @"CustomerId";
			 public static string WidgetId = @"WidgetId";
			 public static string WidgetName = @"WidgetName";
			 public static string WidgetOrderNumber = @"WidgetOrderNumber";
			 public static string WidgetIsVisible = @"WidgetIsVisible";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
