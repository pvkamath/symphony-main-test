using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
	/// <summary>
	/// Strongly-typed collection for the TrainingProgramLicence class.
	/// </summary>
    [Serializable]
	public partial class TrainingProgramLicenceCollection : ActiveList<TrainingProgramLicence, TrainingProgramLicenceCollection>
	{	   
		public TrainingProgramLicenceCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TrainingProgramLicenceCollection</returns>
		public TrainingProgramLicenceCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TrainingProgramLicence o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the TrainingProgramLicence table.
	/// </summary>
	[Serializable]
	public partial class TrainingProgramLicence : Symphony.Core.Data.ActiveRecordCustom<TrainingProgramLicence>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public TrainingProgramLicence()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public TrainingProgramLicence(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public TrainingProgramLicence(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public TrainingProgramLicence(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("TrainingProgramLicence", TableType.Table, DataService.GetInstance("Symphony"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarLicenceID = new TableSchema.TableColumn(schema);
				colvarLicenceID.ColumnName = "LicenceID";
				colvarLicenceID.DataType = DbType.Int32;
				colvarLicenceID.MaxLength = 0;
				colvarLicenceID.AutoIncrement = false;
				colvarLicenceID.IsNullable = false;
				colvarLicenceID.IsPrimaryKey = false;
				colvarLicenceID.IsForeignKey = false;
				colvarLicenceID.IsReadOnly = false;
				colvarLicenceID.DefaultSetting = @"";
				colvarLicenceID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLicenceID);
				
				TableSchema.TableColumn colvarTrainingProgramID = new TableSchema.TableColumn(schema);
				colvarTrainingProgramID.ColumnName = "TrainingProgramID";
				colvarTrainingProgramID.DataType = DbType.Int32;
				colvarTrainingProgramID.MaxLength = 0;
				colvarTrainingProgramID.AutoIncrement = false;
				colvarTrainingProgramID.IsNullable = false;
				colvarTrainingProgramID.IsPrimaryKey = false;
				colvarTrainingProgramID.IsForeignKey = false;
				colvarTrainingProgramID.IsReadOnly = false;
				colvarTrainingProgramID.DefaultSetting = @"";
				colvarTrainingProgramID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrainingProgramID);
				
				TableSchema.TableColumn colvarApprovalCode = new TableSchema.TableColumn(schema);
				colvarApprovalCode.ColumnName = "ApprovalCode";
				colvarApprovalCode.DataType = DbType.String;
				colvarApprovalCode.MaxLength = 64;
				colvarApprovalCode.AutoIncrement = false;
				colvarApprovalCode.IsNullable = true;
				colvarApprovalCode.IsPrimaryKey = false;
				colvarApprovalCode.IsForeignKey = false;
				colvarApprovalCode.IsReadOnly = false;
				
						colvarApprovalCode.DefaultSetting = @"('')";
				colvarApprovalCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApprovalCode);
				
				TableSchema.TableColumn colvarIsPrimary = new TableSchema.TableColumn(schema);
				colvarIsPrimary.ColumnName = "IsPrimary";
				colvarIsPrimary.DataType = DbType.Boolean;
				colvarIsPrimary.MaxLength = 0;
				colvarIsPrimary.AutoIncrement = false;
				colvarIsPrimary.IsNullable = false;
				colvarIsPrimary.IsPrimaryKey = false;
				colvarIsPrimary.IsForeignKey = false;
				colvarIsPrimary.IsReadOnly = false;
				
						colvarIsPrimary.DefaultSetting = @"((0))";
				colvarIsPrimary.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsPrimary);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["Symphony"].AddSchema("TrainingProgramLicence",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("LicenceID")]
		[Bindable(true)]
		public int LicenceID 
		{
			get { return GetColumnValue<int>(Columns.LicenceID); }
			set { SetColumnValue(Columns.LicenceID, value); }
		}
		  
		[XmlAttribute("TrainingProgramID")]
		[Bindable(true)]
		public int TrainingProgramID 
		{
			get { return GetColumnValue<int>(Columns.TrainingProgramID); }
			set { SetColumnValue(Columns.TrainingProgramID, value); }
		}
		  
		[XmlAttribute("ApprovalCode")]
		[Bindable(true)]
		public string ApprovalCode 
		{
			get { return GetColumnValue<string>(Columns.ApprovalCode); }
			set { SetColumnValue(Columns.ApprovalCode, value); }
		}
		  
		[XmlAttribute("IsPrimary")]
		[Bindable(true)]
		public bool IsPrimary 
		{
			get { return GetColumnValue<bool>(Columns.IsPrimary); }
			set { SetColumnValue(Columns.IsPrimary, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varLicenceID,int varTrainingProgramID,string varApprovalCode,bool varIsPrimary)
		{
			TrainingProgramLicence item = new TrainingProgramLicence();
			
			item.LicenceID = varLicenceID;
			
			item.TrainingProgramID = varTrainingProgramID;
			
			item.ApprovalCode = varApprovalCode;
			
			item.IsPrimary = varIsPrimary;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,int varLicenceID,int varTrainingProgramID,string varApprovalCode,bool varIsPrimary)
		{
			TrainingProgramLicence item = new TrainingProgramLicence();
			
				item.Id = varId;
			
				item.LicenceID = varLicenceID;
			
				item.TrainingProgramID = varTrainingProgramID;
			
				item.ApprovalCode = varApprovalCode;
			
				item.IsPrimary = varIsPrimary;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn LicenceIDColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TrainingProgramIDColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ApprovalCodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IsPrimaryColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string LicenceID = @"LicenceID";
			 public static string TrainingProgramID = @"TrainingProgramID";
			 public static string ApprovalCode = @"ApprovalCode";
			 public static string IsPrimary = @"IsPrimary";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
