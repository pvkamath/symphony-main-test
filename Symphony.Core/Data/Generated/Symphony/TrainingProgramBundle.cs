using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
	/// <summary>
	/// Strongly-typed collection for the TrainingProgramBundle class.
	/// </summary>
    [Serializable]
	public partial class TrainingProgramBundleCollection : ActiveList<TrainingProgramBundle, TrainingProgramBundleCollection>
	{	   
		public TrainingProgramBundleCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TrainingProgramBundleCollection</returns>
		public TrainingProgramBundleCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TrainingProgramBundle o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the TrainingProgramBundle table.
	/// </summary>
	[Serializable]
	public partial class TrainingProgramBundle : Symphony.Core.Data.ActiveRecordCustom<TrainingProgramBundle>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public TrainingProgramBundle()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public TrainingProgramBundle(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public TrainingProgramBundle(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public TrainingProgramBundle(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("TrainingProgramBundle", TableType.Table, DataService.GetInstance("Symphony"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSku = new TableSchema.TableColumn(schema);
				colvarSku.ColumnName = "Sku";
				colvarSku.DataType = DbType.String;
				colvarSku.MaxLength = 32;
				colvarSku.AutoIncrement = false;
				colvarSku.IsNullable = false;
				colvarSku.IsPrimaryKey = false;
				colvarSku.IsForeignKey = false;
				colvarSku.IsReadOnly = false;
				colvarSku.DefaultSetting = @"";
				colvarSku.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSku);
				
				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "Name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 512;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = false;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "Description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = -1;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				
						colvarDescription.DefaultSetting = @"('')";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarCustomerID = new TableSchema.TableColumn(schema);
				colvarCustomerID.ColumnName = "CustomerID";
				colvarCustomerID.DataType = DbType.Int32;
				colvarCustomerID.MaxLength = 0;
				colvarCustomerID.AutoIncrement = false;
				colvarCustomerID.IsNullable = false;
				colvarCustomerID.IsPrimaryKey = false;
				colvarCustomerID.IsForeignKey = true;
				colvarCustomerID.IsReadOnly = false;
				colvarCustomerID.DefaultSetting = @"";
				
					colvarCustomerID.ForeignKeyTableName = "Customer";
				schema.Columns.Add(colvarCustomerID);
				
				TableSchema.TableColumn colvarCategoryID = new TableSchema.TableColumn(schema);
				colvarCategoryID.ColumnName = "CategoryID";
				colvarCategoryID.DataType = DbType.Int32;
				colvarCategoryID.MaxLength = 0;
				colvarCategoryID.AutoIncrement = false;
				colvarCategoryID.IsNullable = true;
				colvarCategoryID.IsPrimaryKey = false;
				colvarCategoryID.IsForeignKey = false;
				colvarCategoryID.IsReadOnly = false;
				
						colvarCategoryID.DefaultSetting = @"((0))";
				colvarCategoryID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryID);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "Status";
				colvarStatus.DataType = DbType.String;
				colvarStatus.MaxLength = 32;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"('In-Development')";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarContentType = new TableSchema.TableColumn(schema);
				colvarContentType.ColumnName = "ContentType";
				colvarContentType.DataType = DbType.String;
				colvarContentType.MaxLength = 50;
				colvarContentType.AutoIncrement = false;
				colvarContentType.IsNullable = false;
				colvarContentType.IsPrimaryKey = false;
				colvarContentType.IsForeignKey = false;
				colvarContentType.IsReadOnly = false;
				
						colvarContentType.DefaultSetting = @"('')";
				colvarContentType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContentType);
				
				TableSchema.TableColumn colvarDeliveryMethod = new TableSchema.TableColumn(schema);
				colvarDeliveryMethod.ColumnName = "DeliveryMethod";
				colvarDeliveryMethod.DataType = DbType.String;
				colvarDeliveryMethod.MaxLength = 50;
				colvarDeliveryMethod.AutoIncrement = false;
				colvarDeliveryMethod.IsNullable = false;
				colvarDeliveryMethod.IsPrimaryKey = false;
				colvarDeliveryMethod.IsForeignKey = false;
				colvarDeliveryMethod.IsReadOnly = false;
				
						colvarDeliveryMethod.DefaultSetting = @"('')";
				colvarDeliveryMethod.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryMethod);
				
				TableSchema.TableColumn colvarHours = new TableSchema.TableColumn(schema);
				colvarHours.ColumnName = "Hours";
				colvarHours.DataType = DbType.Int32;
				colvarHours.MaxLength = 0;
				colvarHours.AutoIncrement = false;
				colvarHours.IsNullable = false;
				colvarHours.IsPrimaryKey = false;
				colvarHours.IsForeignKey = false;
				colvarHours.IsReadOnly = false;
				
						colvarHours.DefaultSetting = @"((0))";
				colvarHours.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHours);
				
				TableSchema.TableColumn colvarIsDeleted = new TableSchema.TableColumn(schema);
				colvarIsDeleted.ColumnName = "IsDeleted";
				colvarIsDeleted.DataType = DbType.Boolean;
				colvarIsDeleted.MaxLength = 0;
				colvarIsDeleted.AutoIncrement = false;
				colvarIsDeleted.IsNullable = false;
				colvarIsDeleted.IsPrimaryKey = false;
				colvarIsDeleted.IsForeignKey = false;
				colvarIsDeleted.IsReadOnly = false;
				
						colvarIsDeleted.DefaultSetting = @"((0))";
				colvarIsDeleted.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDeleted);
				
				TableSchema.TableColumn colvarCreatedOn = new TableSchema.TableColumn(schema);
				colvarCreatedOn.ColumnName = "CreatedOn";
				colvarCreatedOn.DataType = DbType.DateTime;
				colvarCreatedOn.MaxLength = 0;
				colvarCreatedOn.AutoIncrement = false;
				colvarCreatedOn.IsNullable = false;
				colvarCreatedOn.IsPrimaryKey = false;
				colvarCreatedOn.IsForeignKey = false;
				colvarCreatedOn.IsReadOnly = false;
				
						colvarCreatedOn.DefaultSetting = @"(getdate())";
				colvarCreatedOn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedOn);
				
				TableSchema.TableColumn colvarModifiedOn = new TableSchema.TableColumn(schema);
				colvarModifiedOn.ColumnName = "ModifiedOn";
				colvarModifiedOn.DataType = DbType.DateTime;
				colvarModifiedOn.MaxLength = 0;
				colvarModifiedOn.AutoIncrement = false;
				colvarModifiedOn.IsNullable = false;
				colvarModifiedOn.IsPrimaryKey = false;
				colvarModifiedOn.IsForeignKey = false;
				colvarModifiedOn.IsReadOnly = false;
				
						colvarModifiedOn.DefaultSetting = @"(getdate())";
				colvarModifiedOn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedOn);
				
				TableSchema.TableColumn colvarCreatedBy = new TableSchema.TableColumn(schema);
				colvarCreatedBy.ColumnName = "CreatedBy";
				colvarCreatedBy.DataType = DbType.String;
				colvarCreatedBy.MaxLength = 64;
				colvarCreatedBy.AutoIncrement = false;
				colvarCreatedBy.IsNullable = true;
				colvarCreatedBy.IsPrimaryKey = false;
				colvarCreatedBy.IsForeignKey = false;
				colvarCreatedBy.IsReadOnly = false;
				colvarCreatedBy.DefaultSetting = @"";
				colvarCreatedBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedBy);
				
				TableSchema.TableColumn colvarModifiedBy = new TableSchema.TableColumn(schema);
				colvarModifiedBy.ColumnName = "ModifiedBy";
				colvarModifiedBy.DataType = DbType.String;
				colvarModifiedBy.MaxLength = 64;
				colvarModifiedBy.AutoIncrement = false;
				colvarModifiedBy.IsNullable = true;
				colvarModifiedBy.IsPrimaryKey = false;
				colvarModifiedBy.IsForeignKey = false;
				colvarModifiedBy.IsReadOnly = false;
				colvarModifiedBy.DefaultSetting = @"";
				colvarModifiedBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedBy);
				
				TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
				colvarCost.ColumnName = "Cost";
				colvarCost.DataType = DbType.Decimal;
				colvarCost.MaxLength = 0;
				colvarCost.AutoIncrement = false;
				colvarCost.IsNullable = false;
				colvarCost.IsPrimaryKey = false;
				colvarCost.IsForeignKey = false;
				colvarCost.IsReadOnly = false;
				colvarCost.DefaultSetting = @"";
				colvarCost.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCost);
				
				TableSchema.TableColumn colvarListPrice = new TableSchema.TableColumn(schema);
				colvarListPrice.ColumnName = "ListPrice";
				colvarListPrice.DataType = DbType.Decimal;
				colvarListPrice.MaxLength = 0;
				colvarListPrice.AutoIncrement = false;
				colvarListPrice.IsNullable = false;
				colvarListPrice.IsPrimaryKey = false;
				colvarListPrice.IsForeignKey = false;
				colvarListPrice.IsReadOnly = false;
				colvarListPrice.DefaultSetting = @"";
				colvarListPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarListPrice);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["Symphony"].AddSchema("TrainingProgramBundle",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Sku")]
		[Bindable(true)]
		public string Sku 
		{
			get { return GetColumnValue<string>(Columns.Sku); }
			set { SetColumnValue(Columns.Sku, value); }
		}
		  
		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name 
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("CustomerID")]
		[Bindable(true)]
		public int CustomerID 
		{
			get { return GetColumnValue<int>(Columns.CustomerID); }
			set { SetColumnValue(Columns.CustomerID, value); }
		}
		  
		[XmlAttribute("CategoryID")]
		[Bindable(true)]
		public int? CategoryID 
		{
			get { return GetColumnValue<int?>(Columns.CategoryID); }
			set { SetColumnValue(Columns.CategoryID, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public string Status 
		{
			get { return GetColumnValue<string>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("ContentType")]
		[Bindable(true)]
		public string ContentType 
		{
			get { return GetColumnValue<string>(Columns.ContentType); }
			set { SetColumnValue(Columns.ContentType, value); }
		}
		  
		[XmlAttribute("DeliveryMethod")]
		[Bindable(true)]
		public string DeliveryMethod 
		{
			get { return GetColumnValue<string>(Columns.DeliveryMethod); }
			set { SetColumnValue(Columns.DeliveryMethod, value); }
		}
		  
		[XmlAttribute("Hours")]
		[Bindable(true)]
		public int Hours 
		{
			get { return GetColumnValue<int>(Columns.Hours); }
			set { SetColumnValue(Columns.Hours, value); }
		}
		  
		[XmlAttribute("IsDeleted")]
		[Bindable(true)]
		public bool IsDeleted 
		{
			get { return GetColumnValue<bool>(Columns.IsDeleted); }
			set { SetColumnValue(Columns.IsDeleted, value); }
		}
		  
		[XmlAttribute("CreatedOn")]
		[Bindable(true)]
		public DateTime CreatedOn 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedOn); }
			set { SetColumnValue(Columns.CreatedOn, value); }
		}
		  
		[XmlAttribute("ModifiedOn")]
		[Bindable(true)]
		public DateTime ModifiedOn 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifiedOn); }
			set { SetColumnValue(Columns.ModifiedOn, value); }
		}
		  
		[XmlAttribute("CreatedBy")]
		[Bindable(true)]
		public string CreatedBy 
		{
			get { return GetColumnValue<string>(Columns.CreatedBy); }
			set { SetColumnValue(Columns.CreatedBy, value); }
		}
		  
		[XmlAttribute("ModifiedBy")]
		[Bindable(true)]
		public string ModifiedBy 
		{
			get { return GetColumnValue<string>(Columns.ModifiedBy); }
			set { SetColumnValue(Columns.ModifiedBy, value); }
		}
		  
		[XmlAttribute("Cost")]
		[Bindable(true)]
		public decimal Cost 
		{
			get { return GetColumnValue<decimal>(Columns.Cost); }
			set { SetColumnValue(Columns.Cost, value); }
		}
		  
		[XmlAttribute("ListPrice")]
		[Bindable(true)]
		public decimal ListPrice 
		{
			get { return GetColumnValue<decimal>(Columns.ListPrice); }
			set { SetColumnValue(Columns.ListPrice, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a Customer ActiveRecord object related to this TrainingProgramBundle
		/// </summary>
		private Symphony.Core.Data.Customer _Customer;
		public Symphony.Core.Data.Customer Customer
		{
			get { 
			    if(_Customer == null){
			        _Customer = Symphony.Core.Data.Customer.FetchByID(this.CustomerID); 
			    }
			    return _Customer;
			}
			set { 
			    SetColumnValue("CustomerID", value.Id); 
			    _Customer = value;
			}
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varSku,string varName,string varDescription,int varCustomerID,int? varCategoryID,string varStatus,string varContentType,string varDeliveryMethod,int varHours,bool varIsDeleted,DateTime varCreatedOn,DateTime varModifiedOn,string varCreatedBy,string varModifiedBy,decimal varCost,decimal varListPrice)
		{
			TrainingProgramBundle item = new TrainingProgramBundle();
			
			item.Sku = varSku;
			
			item.Name = varName;
			
			item.Description = varDescription;
			
			item.CustomerID = varCustomerID;
			
			item.CategoryID = varCategoryID;
			
			item.Status = varStatus;
			
			item.ContentType = varContentType;
			
			item.DeliveryMethod = varDeliveryMethod;
			
			item.Hours = varHours;
			
			item.IsDeleted = varIsDeleted;
			
			item.CreatedOn = varCreatedOn;
			
			item.ModifiedOn = varModifiedOn;
			
			item.CreatedBy = varCreatedBy;
			
			item.ModifiedBy = varModifiedBy;
			
			item.Cost = varCost;
			
			item.ListPrice = varListPrice;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,string varSku,string varName,string varDescription,int varCustomerID,int? varCategoryID,string varStatus,string varContentType,string varDeliveryMethod,int varHours,bool varIsDeleted,DateTime varCreatedOn,DateTime varModifiedOn,string varCreatedBy,string varModifiedBy,decimal varCost,decimal varListPrice)
		{
			TrainingProgramBundle item = new TrainingProgramBundle();
			
				item.Id = varId;
			
				item.Sku = varSku;
			
				item.Name = varName;
			
				item.Description = varDescription;
			
				item.CustomerID = varCustomerID;
			
				item.CategoryID = varCategoryID;
			
				item.Status = varStatus;
			
				item.ContentType = varContentType;
			
				item.DeliveryMethod = varDeliveryMethod;
			
				item.Hours = varHours;
			
				item.IsDeleted = varIsDeleted;
			
				item.CreatedOn = varCreatedOn;
			
				item.ModifiedOn = varModifiedOn;
			
				item.CreatedBy = varCreatedBy;
			
				item.ModifiedBy = varModifiedBy;
			
				item.Cost = varCost;
			
				item.ListPrice = varListPrice;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SkuColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CustomerIDColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryIDColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ContentTypeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryMethodColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn HoursColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDeletedColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedOnColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedOnColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedByColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedByColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CostColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn ListPriceColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string Sku = @"Sku";
			 public static string Name = @"Name";
			 public static string Description = @"Description";
			 public static string CustomerID = @"CustomerID";
			 public static string CategoryID = @"CategoryID";
			 public static string Status = @"Status";
			 public static string ContentType = @"ContentType";
			 public static string DeliveryMethod = @"DeliveryMethod";
			 public static string Hours = @"Hours";
			 public static string IsDeleted = @"IsDeleted";
			 public static string CreatedOn = @"CreatedOn";
			 public static string ModifiedOn = @"ModifiedOn";
			 public static string CreatedBy = @"CreatedBy";
			 public static string ModifiedBy = @"ModifiedBy";
			 public static string Cost = @"Cost";
			 public static string ListPrice = @"ListPrice";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
