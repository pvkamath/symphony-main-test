using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
    /// <summary>
    /// Controller class for File
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class FileController
    {
        // Preload our schema..
        File thisSchemaLoad = new File();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public FileCollection FetchAll()
        {
            FileCollection coll = new FileCollection();
            Query qry = new Query(File.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public FileCollection FetchByID(object Id)
        {
            FileCollection coll = new FileCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public FileCollection FetchByQuery(Query qry)
        {
            FileCollection coll = new FileCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (File.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (File.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string FileName,string OriginalFileName,string Description,string Title,string Path,int FileTypeID,long FileSize,byte[] FileBytes,string FileExtension,string ModifiedBy,string CreatedBy,DateTime ModifiedOn,DateTime CreatedOn,int CustomerID)
	    {
		    File item = new File();
		    
            item.FileName = FileName;
            
            item.OriginalFileName = OriginalFileName;
            
            item.Description = Description;
            
            item.Title = Title;
            
            item.Path = Path;
            
            item.FileTypeID = FileTypeID;
            
            item.FileSize = FileSize;
            
            item.FileBytes = FileBytes;
            
            item.FileExtension = FileExtension;
            
            item.ModifiedBy = ModifiedBy;
            
            item.CreatedBy = CreatedBy;
            
            item.ModifiedOn = ModifiedOn;
            
            item.CreatedOn = CreatedOn;
            
            item.CustomerID = CustomerID;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,string FileName,string OriginalFileName,string Description,string Title,string Path,int FileTypeID,long FileSize,byte[] FileBytes,string FileExtension,string ModifiedBy,string CreatedBy,DateTime ModifiedOn,DateTime CreatedOn,int CustomerID)
	    {
		    File item = new File();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.FileName = FileName;
				
			item.OriginalFileName = OriginalFileName;
				
			item.Description = Description;
				
			item.Title = Title;
				
			item.Path = Path;
				
			item.FileTypeID = FileTypeID;
				
			item.FileSize = FileSize;
				
			item.FileBytes = FileBytes;
				
			item.FileExtension = FileExtension;
				
			item.ModifiedBy = ModifiedBy;
				
			item.CreatedBy = CreatedBy;
				
			item.ModifiedOn = ModifiedOn;
				
			item.CreatedOn = CreatedOn;
				
			item.CustomerID = CustomerID;
				
	        item.Save(UserName);
	    }
    }
}
