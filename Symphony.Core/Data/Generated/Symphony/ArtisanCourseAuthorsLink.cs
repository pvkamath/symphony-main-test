using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
	/// <summary>
	/// Strongly-typed collection for the ArtisanCourseAuthorsLink class.
	/// </summary>
    [Serializable]
	public partial class ArtisanCourseAuthorsLinkCollection : ActiveList<ArtisanCourseAuthorsLink, ArtisanCourseAuthorsLinkCollection>
	{	   
		public ArtisanCourseAuthorsLinkCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ArtisanCourseAuthorsLinkCollection</returns>
		public ArtisanCourseAuthorsLinkCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ArtisanCourseAuthorsLink o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the ArtisanCourseAuthorsLink table.
	/// </summary>
	[Serializable]
	public partial class ArtisanCourseAuthorsLink : Symphony.Core.Data.ActiveRecordCustom<ArtisanCourseAuthorsLink>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public ArtisanCourseAuthorsLink()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ArtisanCourseAuthorsLink(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public ArtisanCourseAuthorsLink(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public ArtisanCourseAuthorsLink(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("ArtisanCourseAuthorsLink", TableType.Table, DataService.GetInstance("Symphony"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "UserId";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = true;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				
					colvarUserId.ForeignKeyTableName = "User";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarArtisanCourseId = new TableSchema.TableColumn(schema);
				colvarArtisanCourseId.ColumnName = "ArtisanCourseId";
				colvarArtisanCourseId.DataType = DbType.Int32;
				colvarArtisanCourseId.MaxLength = 0;
				colvarArtisanCourseId.AutoIncrement = false;
				colvarArtisanCourseId.IsNullable = false;
				colvarArtisanCourseId.IsPrimaryKey = false;
				colvarArtisanCourseId.IsForeignKey = true;
				colvarArtisanCourseId.IsReadOnly = false;
				colvarArtisanCourseId.DefaultSetting = @"";
				
					colvarArtisanCourseId.ForeignKeyTableName = "ArtisanCourses";
				schema.Columns.Add(colvarArtisanCourseId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["Symphony"].AddSchema("ArtisanCourseAuthorsLink",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("ArtisanCourseId")]
		[Bindable(true)]
		public int ArtisanCourseId 
		{
			get { return GetColumnValue<int>(Columns.ArtisanCourseId); }
			set { SetColumnValue(Columns.ArtisanCourseId, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a ArtisanCourse ActiveRecord object related to this ArtisanCourseAuthorsLink
		/// </summary>
		private Symphony.Core.Data.ArtisanCourse _ArtisanCourse;
		public Symphony.Core.Data.ArtisanCourse ArtisanCourse
		{
			get { 
			    if(_ArtisanCourse == null){
			        _ArtisanCourse = Symphony.Core.Data.ArtisanCourse.FetchByID(this.ArtisanCourseId); 
			    }
			    return _ArtisanCourse;
			}
			set { 
			    SetColumnValue("ArtisanCourseId", value.Id); 
			    _ArtisanCourse = value;
			}
		}
		
		
		/// <summary>
		/// Returns a User ActiveRecord object related to this ArtisanCourseAuthorsLink
		/// </summary>
		private Symphony.Core.Data.User _User;
		public Symphony.Core.Data.User User
		{
			get { 
			    if(_User == null){
			        _User = Symphony.Core.Data.User.FetchByID(this.UserId); 
			    }
			    return _User;
			}
			set { 
			    SetColumnValue("UserId", value.Id); 
			    _User = value;
			}
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varUserId,int varArtisanCourseId)
		{
			ArtisanCourseAuthorsLink item = new ArtisanCourseAuthorsLink();
			
			item.UserId = varUserId;
			
			item.ArtisanCourseId = varArtisanCourseId;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,int varUserId,int varArtisanCourseId)
		{
			ArtisanCourseAuthorsLink item = new ArtisanCourseAuthorsLink();
			
				item.Id = varId;
			
				item.UserId = varUserId;
			
				item.ArtisanCourseId = varArtisanCourseId;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ArtisanCourseIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string UserId = @"UserId";
			 public static string ArtisanCourseId = @"ArtisanCourseId";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
