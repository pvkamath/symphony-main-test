using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
    /// <summary>
    /// Controller class for ParameterSetOption
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class ParameterSetOptionController
    {
        // Preload our schema..
        ParameterSetOption thisSchemaLoad = new ParameterSetOption();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public ParameterSetOptionCollection FetchAll()
        {
            ParameterSetOptionCollection coll = new ParameterSetOptionCollection();
            Query qry = new Query(ParameterSetOption.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public ParameterSetOptionCollection FetchByID(object Id)
        {
            ParameterSetOptionCollection coll = new ParameterSetOptionCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public ParameterSetOptionCollection FetchByQuery(Query qry)
        {
            ParameterSetOptionCollection coll = new ParameterSetOptionCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (ParameterSetOption.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (ParameterSetOption.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int ParameterSetID,string ValueX,DateTime CreatedOn,DateTime ModifiedOn,string CreatedBy,string ModifiedBy)
	    {
		    ParameterSetOption item = new ParameterSetOption();
		    
            item.ParameterSetID = ParameterSetID;
            
            item.ValueX = ValueX;
            
            item.CreatedOn = CreatedOn;
            
            item.ModifiedOn = ModifiedOn;
            
            item.CreatedBy = CreatedBy;
            
            item.ModifiedBy = ModifiedBy;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,int ParameterSetID,string ValueX,DateTime CreatedOn,DateTime ModifiedOn,string CreatedBy,string ModifiedBy)
	    {
		    ParameterSetOption item = new ParameterSetOption();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.ParameterSetID = ParameterSetID;
				
			item.ValueX = ValueX;
				
			item.CreatedOn = CreatedOn;
				
			item.ModifiedOn = ModifiedOn;
				
			item.CreatedBy = CreatedBy;
				
			item.ModifiedBy = ModifiedBy;
				
	        item.Save(UserName);
	    }
    }
}
