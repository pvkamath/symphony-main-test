using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data{
    /// <summary>
    /// Strongly-typed collection for the CourseStudentCount class.
    /// </summary>
    [Serializable]
    public partial class CourseStudentCountCollection : ReadOnlyList<CourseStudentCount, CourseStudentCountCollection>
    {        
        public CourseStudentCountCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the CourseStudentCount view.
    /// </summary>
    [Serializable]
    public partial class CourseStudentCount : ReadOnlyRecord<CourseStudentCount>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("CourseStudentCount", TableType.View, DataService.GetInstance("Symphony"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCourseID = new TableSchema.TableColumn(schema);
                colvarCourseID.ColumnName = "CourseID";
                colvarCourseID.DataType = DbType.Int32;
                colvarCourseID.MaxLength = 0;
                colvarCourseID.AutoIncrement = false;
                colvarCourseID.IsNullable = false;
                colvarCourseID.IsPrimaryKey = false;
                colvarCourseID.IsForeignKey = false;
                colvarCourseID.IsReadOnly = false;
                
                schema.Columns.Add(colvarCourseID);
                
                TableSchema.TableColumn colvarTrainingCycleID = new TableSchema.TableColumn(schema);
                colvarTrainingCycleID.ColumnName = "TrainingCycleID";
                colvarTrainingCycleID.DataType = DbType.Int32;
                colvarTrainingCycleID.MaxLength = 0;
                colvarTrainingCycleID.AutoIncrement = false;
                colvarTrainingCycleID.IsNullable = true;
                colvarTrainingCycleID.IsPrimaryKey = false;
                colvarTrainingCycleID.IsForeignKey = false;
                colvarTrainingCycleID.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrainingCycleID);
                
                TableSchema.TableColumn colvarStudentCount = new TableSchema.TableColumn(schema);
                colvarStudentCount.ColumnName = "StudentCount";
                colvarStudentCount.DataType = DbType.Int32;
                colvarStudentCount.MaxLength = 0;
                colvarStudentCount.AutoIncrement = false;
                colvarStudentCount.IsNullable = true;
                colvarStudentCount.IsPrimaryKey = false;
                colvarStudentCount.IsForeignKey = false;
                colvarStudentCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarStudentCount);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["Symphony"].AddSchema("CourseStudentCount",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public CourseStudentCount()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public CourseStudentCount(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public CourseStudentCount(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public CourseStudentCount(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CourseID")]
        [Bindable(true)]
        public int CourseID 
	    {
		    get
		    {
			    return GetColumnValue<int>("CourseID");
		    }
            set 
		    {
			    SetColumnValue("CourseID", value);
            }
        }
	      
        [XmlAttribute("TrainingCycleID")]
        [Bindable(true)]
        public int? TrainingCycleID 
	    {
		    get
		    {
			    return GetColumnValue<int?>("TrainingCycleID");
		    }
            set 
		    {
			    SetColumnValue("TrainingCycleID", value);
            }
        }
	      
        [XmlAttribute("StudentCount")]
        [Bindable(true)]
        public int? StudentCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("StudentCount");
		    }
            set 
		    {
			    SetColumnValue("StudentCount", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CourseID = @"CourseID";
            
            public static string TrainingCycleID = @"TrainingCycleID";
            
            public static string StudentCount = @"StudentCount";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
