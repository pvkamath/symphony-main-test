using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data{
    /// <summary>
    /// Strongly-typed collection for the ReportUser class.
    /// </summary>
    [Serializable]
    public partial class ReportUserCollection : ReadOnlyList<ReportUser, ReportUserCollection>
    {        
        public ReportUserCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the ReportUsers view.
    /// </summary>
    [Serializable]
    public partial class ReportUser : ReadOnlyRecord<ReportUser>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("ReportUsers", TableType.View, DataService.GetInstance("Symphony"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarUserKey = new TableSchema.TableColumn(schema);
                colvarUserKey.ColumnName = "UserKey";
                colvarUserKey.DataType = DbType.Int32;
                colvarUserKey.MaxLength = 0;
                colvarUserKey.AutoIncrement = false;
                colvarUserKey.IsNullable = false;
                colvarUserKey.IsPrimaryKey = false;
                colvarUserKey.IsForeignKey = false;
                colvarUserKey.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserKey);
                
                TableSchema.TableColumn colvarUserID = new TableSchema.TableColumn(schema);
                colvarUserID.ColumnName = "UserID";
                colvarUserID.DataType = DbType.Int32;
                colvarUserID.MaxLength = 0;
                colvarUserID.AutoIncrement = false;
                colvarUserID.IsNullable = false;
                colvarUserID.IsPrimaryKey = false;
                colvarUserID.IsForeignKey = false;
                colvarUserID.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserID);
                
                TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
                colvarFirstName.ColumnName = "FirstName";
                colvarFirstName.DataType = DbType.String;
                colvarFirstName.MaxLength = 250;
                colvarFirstName.AutoIncrement = false;
                colvarFirstName.IsNullable = false;
                colvarFirstName.IsPrimaryKey = false;
                colvarFirstName.IsForeignKey = false;
                colvarFirstName.IsReadOnly = false;
                
                schema.Columns.Add(colvarFirstName);
                
                TableSchema.TableColumn colvarMiddleName = new TableSchema.TableColumn(schema);
                colvarMiddleName.ColumnName = "MiddleName";
                colvarMiddleName.DataType = DbType.String;
                colvarMiddleName.MaxLength = 250;
                colvarMiddleName.AutoIncrement = false;
                colvarMiddleName.IsNullable = false;
                colvarMiddleName.IsPrimaryKey = false;
                colvarMiddleName.IsForeignKey = false;
                colvarMiddleName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMiddleName);
                
                TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
                colvarLastName.ColumnName = "LastName";
                colvarLastName.DataType = DbType.String;
                colvarLastName.MaxLength = 250;
                colvarLastName.AutoIncrement = false;
                colvarLastName.IsNullable = false;
                colvarLastName.IsPrimaryKey = false;
                colvarLastName.IsForeignKey = false;
                colvarLastName.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastName);
                
                TableSchema.TableColumn colvarUserNameX = new TableSchema.TableColumn(schema);
                colvarUserNameX.ColumnName = "UserName";
                colvarUserNameX.DataType = DbType.String;
                colvarUserNameX.MaxLength = 250;
                colvarUserNameX.AutoIncrement = false;
                colvarUserNameX.IsNullable = false;
                colvarUserNameX.IsPrimaryKey = false;
                colvarUserNameX.IsForeignKey = false;
                colvarUserNameX.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserNameX);
                
                TableSchema.TableColumn colvarCustomerKey = new TableSchema.TableColumn(schema);
                colvarCustomerKey.ColumnName = "CustomerKey";
                colvarCustomerKey.DataType = DbType.Int32;
                colvarCustomerKey.MaxLength = 0;
                colvarCustomerKey.AutoIncrement = false;
                colvarCustomerKey.IsNullable = false;
                colvarCustomerKey.IsPrimaryKey = false;
                colvarCustomerKey.IsForeignKey = false;
                colvarCustomerKey.IsReadOnly = false;
                
                schema.Columns.Add(colvarCustomerKey);
                
                TableSchema.TableColumn colvarLocationKey = new TableSchema.TableColumn(schema);
                colvarLocationKey.ColumnName = "LocationKey";
                colvarLocationKey.DataType = DbType.Int32;
                colvarLocationKey.MaxLength = 0;
                colvarLocationKey.AutoIncrement = false;
                colvarLocationKey.IsNullable = true;
                colvarLocationKey.IsPrimaryKey = false;
                colvarLocationKey.IsForeignKey = false;
                colvarLocationKey.IsReadOnly = false;
                
                schema.Columns.Add(colvarLocationKey);
                
                TableSchema.TableColumn colvarJobRoleKey = new TableSchema.TableColumn(schema);
                colvarJobRoleKey.ColumnName = "JobRoleKey";
                colvarJobRoleKey.DataType = DbType.Int32;
                colvarJobRoleKey.MaxLength = 0;
                colvarJobRoleKey.AutoIncrement = false;
                colvarJobRoleKey.IsNullable = false;
                colvarJobRoleKey.IsPrimaryKey = false;
                colvarJobRoleKey.IsForeignKey = false;
                colvarJobRoleKey.IsReadOnly = false;
                
                schema.Columns.Add(colvarJobRoleKey);
                
                TableSchema.TableColumn colvarUserPermission = new TableSchema.TableColumn(schema);
                colvarUserPermission.ColumnName = "UserPermission";
                colvarUserPermission.DataType = DbType.String;
                colvarUserPermission.MaxLength = 100;
                colvarUserPermission.AutoIncrement = false;
                colvarUserPermission.IsNullable = true;
                colvarUserPermission.IsPrimaryKey = false;
                colvarUserPermission.IsForeignKey = false;
                colvarUserPermission.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserPermission);
                
                TableSchema.TableColumn colvarUserPermissionsKey = new TableSchema.TableColumn(schema);
                colvarUserPermissionsKey.ColumnName = "UserPermissionsKey";
                colvarUserPermissionsKey.DataType = DbType.Int32;
                colvarUserPermissionsKey.MaxLength = 0;
                colvarUserPermissionsKey.AutoIncrement = false;
                colvarUserPermissionsKey.IsNullable = true;
                colvarUserPermissionsKey.IsPrimaryKey = false;
                colvarUserPermissionsKey.IsForeignKey = false;
                colvarUserPermissionsKey.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserPermissionsKey);
                
                TableSchema.TableColumn colvarCustomerID = new TableSchema.TableColumn(schema);
                colvarCustomerID.ColumnName = "CustomerID";
                colvarCustomerID.DataType = DbType.Int32;
                colvarCustomerID.MaxLength = 0;
                colvarCustomerID.AutoIncrement = false;
                colvarCustomerID.IsNullable = true;
                colvarCustomerID.IsPrimaryKey = false;
                colvarCustomerID.IsForeignKey = false;
                colvarCustomerID.IsReadOnly = false;
                
                schema.Columns.Add(colvarCustomerID);
                
                TableSchema.TableColumn colvarIsImported = new TableSchema.TableColumn(schema);
                colvarIsImported.ColumnName = "IsImported";
                colvarIsImported.DataType = DbType.Boolean;
                colvarIsImported.MaxLength = 0;
                colvarIsImported.AutoIncrement = false;
                colvarIsImported.IsNullable = false;
                colvarIsImported.IsPrimaryKey = false;
                colvarIsImported.IsForeignKey = false;
                colvarIsImported.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsImported);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["Symphony"].AddSchema("ReportUsers",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ReportUser()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ReportUser(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ReportUser(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ReportUser(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("UserKey")]
        [Bindable(true)]
        public int UserKey 
	    {
		    get
		    {
			    return GetColumnValue<int>("UserKey");
		    }
            set 
		    {
			    SetColumnValue("UserKey", value);
            }
        }
	      
        [XmlAttribute("UserID")]
        [Bindable(true)]
        public int UserID 
	    {
		    get
		    {
			    return GetColumnValue<int>("UserID");
		    }
            set 
		    {
			    SetColumnValue("UserID", value);
            }
        }
	      
        [XmlAttribute("FirstName")]
        [Bindable(true)]
        public string FirstName 
	    {
		    get
		    {
			    return GetColumnValue<string>("FirstName");
		    }
            set 
		    {
			    SetColumnValue("FirstName", value);
            }
        }
	      
        [XmlAttribute("MiddleName")]
        [Bindable(true)]
        public string MiddleName 
	    {
		    get
		    {
			    return GetColumnValue<string>("MiddleName");
		    }
            set 
		    {
			    SetColumnValue("MiddleName", value);
            }
        }
	      
        [XmlAttribute("LastName")]
        [Bindable(true)]
        public string LastName 
	    {
		    get
		    {
			    return GetColumnValue<string>("LastName");
		    }
            set 
		    {
			    SetColumnValue("LastName", value);
            }
        }
	      
        [XmlAttribute("UserNameX")]
        [Bindable(true)]
        public string UserNameX 
	    {
		    get
		    {
			    return GetColumnValue<string>("UserName");
		    }
            set 
		    {
			    SetColumnValue("UserName", value);
            }
        }
	      
        [XmlAttribute("CustomerKey")]
        [Bindable(true)]
        public int CustomerKey 
	    {
		    get
		    {
			    return GetColumnValue<int>("CustomerKey");
		    }
            set 
		    {
			    SetColumnValue("CustomerKey", value);
            }
        }
	      
        [XmlAttribute("LocationKey")]
        [Bindable(true)]
        public int? LocationKey 
	    {
		    get
		    {
			    return GetColumnValue<int?>("LocationKey");
		    }
            set 
		    {
			    SetColumnValue("LocationKey", value);
            }
        }
	      
        [XmlAttribute("JobRoleKey")]
        [Bindable(true)]
        public int JobRoleKey 
	    {
		    get
		    {
			    return GetColumnValue<int>("JobRoleKey");
		    }
            set 
		    {
			    SetColumnValue("JobRoleKey", value);
            }
        }
	      
        [XmlAttribute("UserPermission")]
        [Bindable(true)]
        public string UserPermission 
	    {
		    get
		    {
			    return GetColumnValue<string>("UserPermission");
		    }
            set 
		    {
			    SetColumnValue("UserPermission", value);
            }
        }
	      
        [XmlAttribute("UserPermissionsKey")]
        [Bindable(true)]
        public int? UserPermissionsKey 
	    {
		    get
		    {
			    return GetColumnValue<int?>("UserPermissionsKey");
		    }
            set 
		    {
			    SetColumnValue("UserPermissionsKey", value);
            }
        }
	      
        [XmlAttribute("CustomerID")]
        [Bindable(true)]
        public int? CustomerID 
	    {
		    get
		    {
			    return GetColumnValue<int?>("CustomerID");
		    }
            set 
		    {
			    SetColumnValue("CustomerID", value);
            }
        }
	      
        [XmlAttribute("IsImported")]
        [Bindable(true)]
        public bool IsImported 
	    {
		    get
		    {
			    return GetColumnValue<bool>("IsImported");
		    }
            set 
		    {
			    SetColumnValue("IsImported", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string UserKey = @"UserKey";
            
            public static string UserID = @"UserID";
            
            public static string FirstName = @"FirstName";
            
            public static string MiddleName = @"MiddleName";
            
            public static string LastName = @"LastName";
            
            public static string UserNameX = @"UserName";
            
            public static string CustomerKey = @"CustomerKey";
            
            public static string LocationKey = @"LocationKey";
            
            public static string JobRoleKey = @"JobRoleKey";
            
            public static string UserPermission = @"UserPermission";
            
            public static string UserPermissionsKey = @"UserPermissionsKey";
            
            public static string CustomerID = @"CustomerID";
            
            public static string IsImported = @"IsImported";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
