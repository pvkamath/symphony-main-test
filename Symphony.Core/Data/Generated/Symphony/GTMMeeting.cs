using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
	/// <summary>
	/// Strongly-typed collection for the GTMMeeting class.
	/// </summary>
    [Serializable]
	public partial class GTMMeetingCollection : ActiveList<GTMMeeting, GTMMeetingCollection>
	{	   
		public GTMMeetingCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>GTMMeetingCollection</returns>
		public GTMMeetingCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                GTMMeeting o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the GTMMeeting table.
	/// </summary>
	[Serializable]
	public partial class GTMMeeting : Symphony.Core.Data.ActiveRecordCustom<GTMMeeting>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public GTMMeeting()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public GTMMeeting(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public GTMMeeting(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public GTMMeeting(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("GTMMeeting", TableType.Table, DataService.GetInstance("Symphony"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarGTMOrganizerID = new TableSchema.TableColumn(schema);
				colvarGTMOrganizerID.ColumnName = "GTMOrganizerID";
				colvarGTMOrganizerID.DataType = DbType.Int32;
				colvarGTMOrganizerID.MaxLength = 0;
				colvarGTMOrganizerID.AutoIncrement = false;
				colvarGTMOrganizerID.IsNullable = false;
				colvarGTMOrganizerID.IsPrimaryKey = false;
				colvarGTMOrganizerID.IsForeignKey = false;
				colvarGTMOrganizerID.IsReadOnly = false;
				colvarGTMOrganizerID.DefaultSetting = @"";
				colvarGTMOrganizerID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGTMOrganizerID);
				
				TableSchema.TableColumn colvarMeetingID = new TableSchema.TableColumn(schema);
				colvarMeetingID.ColumnName = "MeetingID";
				colvarMeetingID.DataType = DbType.Int64;
				colvarMeetingID.MaxLength = 0;
				colvarMeetingID.AutoIncrement = false;
				colvarMeetingID.IsNullable = false;
				colvarMeetingID.IsPrimaryKey = false;
				colvarMeetingID.IsForeignKey = false;
				colvarMeetingID.IsReadOnly = false;
				colvarMeetingID.DefaultSetting = @"";
				colvarMeetingID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMeetingID);
				
				TableSchema.TableColumn colvarUniqueMeetingID = new TableSchema.TableColumn(schema);
				colvarUniqueMeetingID.ColumnName = "UniqueMeetingID";
				colvarUniqueMeetingID.DataType = DbType.String;
				colvarUniqueMeetingID.MaxLength = 50;
				colvarUniqueMeetingID.AutoIncrement = false;
				colvarUniqueMeetingID.IsNullable = false;
				colvarUniqueMeetingID.IsPrimaryKey = false;
				colvarUniqueMeetingID.IsForeignKey = false;
				colvarUniqueMeetingID.IsReadOnly = false;
				colvarUniqueMeetingID.DefaultSetting = @"";
				colvarUniqueMeetingID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniqueMeetingID);
				
				TableSchema.TableColumn colvarSubject = new TableSchema.TableColumn(schema);
				colvarSubject.ColumnName = "Subject";
				colvarSubject.DataType = DbType.String;
				colvarSubject.MaxLength = 100;
				colvarSubject.AutoIncrement = false;
				colvarSubject.IsNullable = false;
				colvarSubject.IsPrimaryKey = false;
				colvarSubject.IsForeignKey = false;
				colvarSubject.IsReadOnly = false;
				colvarSubject.DefaultSetting = @"";
				colvarSubject.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubject);
				
				TableSchema.TableColumn colvarJoinURL = new TableSchema.TableColumn(schema);
				colvarJoinURL.ColumnName = "JoinURL";
				colvarJoinURL.DataType = DbType.String;
				colvarJoinURL.MaxLength = -1;
				colvarJoinURL.AutoIncrement = false;
				colvarJoinURL.IsNullable = false;
				colvarJoinURL.IsPrimaryKey = false;
				colvarJoinURL.IsForeignKey = false;
				colvarJoinURL.IsReadOnly = false;
				colvarJoinURL.DefaultSetting = @"";
				colvarJoinURL.ForeignKeyTableName = "";
				schema.Columns.Add(colvarJoinURL);
				
				TableSchema.TableColumn colvarMaxParticipants = new TableSchema.TableColumn(schema);
				colvarMaxParticipants.ColumnName = "MaxParticipants";
				colvarMaxParticipants.DataType = DbType.Int32;
				colvarMaxParticipants.MaxLength = 0;
				colvarMaxParticipants.AutoIncrement = false;
				colvarMaxParticipants.IsNullable = false;
				colvarMaxParticipants.IsPrimaryKey = false;
				colvarMaxParticipants.IsForeignKey = false;
				colvarMaxParticipants.IsReadOnly = false;
				colvarMaxParticipants.DefaultSetting = @"";
				colvarMaxParticipants.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMaxParticipants);
				
				TableSchema.TableColumn colvarMeetingType = new TableSchema.TableColumn(schema);
				colvarMeetingType.ColumnName = "MeetingType";
				colvarMeetingType.DataType = DbType.String;
				colvarMeetingType.MaxLength = 50;
				colvarMeetingType.AutoIncrement = false;
				colvarMeetingType.IsNullable = false;
				colvarMeetingType.IsPrimaryKey = false;
				colvarMeetingType.IsForeignKey = false;
				colvarMeetingType.IsReadOnly = false;
				colvarMeetingType.DefaultSetting = @"";
				colvarMeetingType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMeetingType);
				
				TableSchema.TableColumn colvarStartDateTime = new TableSchema.TableColumn(schema);
				colvarStartDateTime.ColumnName = "StartDateTime";
				colvarStartDateTime.DataType = DbType.DateTime;
				colvarStartDateTime.MaxLength = 0;
				colvarStartDateTime.AutoIncrement = false;
				colvarStartDateTime.IsNullable = false;
				colvarStartDateTime.IsPrimaryKey = false;
				colvarStartDateTime.IsForeignKey = false;
				colvarStartDateTime.IsReadOnly = false;
				colvarStartDateTime.DefaultSetting = @"";
				colvarStartDateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartDateTime);
				
				TableSchema.TableColumn colvarEndDateTime = new TableSchema.TableColumn(schema);
				colvarEndDateTime.ColumnName = "EndDateTime";
				colvarEndDateTime.DataType = DbType.DateTime;
				colvarEndDateTime.MaxLength = 0;
				colvarEndDateTime.AutoIncrement = false;
				colvarEndDateTime.IsNullable = false;
				colvarEndDateTime.IsPrimaryKey = false;
				colvarEndDateTime.IsForeignKey = false;
				colvarEndDateTime.IsReadOnly = false;
				colvarEndDateTime.DefaultSetting = @"";
				colvarEndDateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndDateTime);
				
				TableSchema.TableColumn colvarTimeZoneKey = new TableSchema.TableColumn(schema);
				colvarTimeZoneKey.ColumnName = "TimeZoneKey";
				colvarTimeZoneKey.DataType = DbType.Int32;
				colvarTimeZoneKey.MaxLength = 0;
				colvarTimeZoneKey.AutoIncrement = false;
				colvarTimeZoneKey.IsNullable = false;
				colvarTimeZoneKey.IsPrimaryKey = false;
				colvarTimeZoneKey.IsForeignKey = false;
				colvarTimeZoneKey.IsReadOnly = false;
				colvarTimeZoneKey.DefaultSetting = @"";
				colvarTimeZoneKey.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTimeZoneKey);
				
				TableSchema.TableColumn colvarPasswordRequired = new TableSchema.TableColumn(schema);
				colvarPasswordRequired.ColumnName = "PasswordRequired";
				colvarPasswordRequired.DataType = DbType.Boolean;
				colvarPasswordRequired.MaxLength = 0;
				colvarPasswordRequired.AutoIncrement = false;
				colvarPasswordRequired.IsNullable = false;
				colvarPasswordRequired.IsPrimaryKey = false;
				colvarPasswordRequired.IsForeignKey = false;
				colvarPasswordRequired.IsReadOnly = false;
				colvarPasswordRequired.DefaultSetting = @"";
				colvarPasswordRequired.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPasswordRequired);
				
				TableSchema.TableColumn colvarConferenceCallInfo = new TableSchema.TableColumn(schema);
				colvarConferenceCallInfo.ColumnName = "ConferenceCallInfo";
				colvarConferenceCallInfo.DataType = DbType.String;
				colvarConferenceCallInfo.MaxLength = -1;
				colvarConferenceCallInfo.AutoIncrement = false;
				colvarConferenceCallInfo.IsNullable = false;
				colvarConferenceCallInfo.IsPrimaryKey = false;
				colvarConferenceCallInfo.IsForeignKey = false;
				colvarConferenceCallInfo.IsReadOnly = false;
				colvarConferenceCallInfo.DefaultSetting = @"";
				colvarConferenceCallInfo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConferenceCallInfo);
				
				TableSchema.TableColumn colvarIsEditable = new TableSchema.TableColumn(schema);
				colvarIsEditable.ColumnName = "IsEditable";
				colvarIsEditable.DataType = DbType.Boolean;
				colvarIsEditable.MaxLength = 0;
				colvarIsEditable.AutoIncrement = false;
				colvarIsEditable.IsNullable = false;
				colvarIsEditable.IsPrimaryKey = false;
				colvarIsEditable.IsForeignKey = false;
				colvarIsEditable.IsReadOnly = false;
				colvarIsEditable.DefaultSetting = @"";
				colvarIsEditable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsEditable);
				
				TableSchema.TableColumn colvarCreatedBy = new TableSchema.TableColumn(schema);
				colvarCreatedBy.ColumnName = "CreatedBy";
				colvarCreatedBy.DataType = DbType.String;
				colvarCreatedBy.MaxLength = 50;
				colvarCreatedBy.AutoIncrement = false;
				colvarCreatedBy.IsNullable = false;
				colvarCreatedBy.IsPrimaryKey = false;
				colvarCreatedBy.IsForeignKey = false;
				colvarCreatedBy.IsReadOnly = false;
				colvarCreatedBy.DefaultSetting = @"";
				colvarCreatedBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedBy);
				
				TableSchema.TableColumn colvarModifiedBy = new TableSchema.TableColumn(schema);
				colvarModifiedBy.ColumnName = "ModifiedBy";
				colvarModifiedBy.DataType = DbType.String;
				colvarModifiedBy.MaxLength = 50;
				colvarModifiedBy.AutoIncrement = false;
				colvarModifiedBy.IsNullable = false;
				colvarModifiedBy.IsPrimaryKey = false;
				colvarModifiedBy.IsForeignKey = false;
				colvarModifiedBy.IsReadOnly = false;
				colvarModifiedBy.DefaultSetting = @"";
				colvarModifiedBy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedBy);
				
				TableSchema.TableColumn colvarCreatedOn = new TableSchema.TableColumn(schema);
				colvarCreatedOn.ColumnName = "CreatedOn";
				colvarCreatedOn.DataType = DbType.DateTime;
				colvarCreatedOn.MaxLength = 0;
				colvarCreatedOn.AutoIncrement = false;
				colvarCreatedOn.IsNullable = false;
				colvarCreatedOn.IsPrimaryKey = false;
				colvarCreatedOn.IsForeignKey = false;
				colvarCreatedOn.IsReadOnly = false;
				colvarCreatedOn.DefaultSetting = @"";
				colvarCreatedOn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedOn);
				
				TableSchema.TableColumn colvarModifiedOn = new TableSchema.TableColumn(schema);
				colvarModifiedOn.ColumnName = "ModifiedOn";
				colvarModifiedOn.DataType = DbType.DateTime;
				colvarModifiedOn.MaxLength = 0;
				colvarModifiedOn.AutoIncrement = false;
				colvarModifiedOn.IsNullable = false;
				colvarModifiedOn.IsPrimaryKey = false;
				colvarModifiedOn.IsForeignKey = false;
				colvarModifiedOn.IsReadOnly = false;
				colvarModifiedOn.DefaultSetting = @"";
				colvarModifiedOn.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedOn);
				
				TableSchema.TableColumn colvarCreatedByUserId = new TableSchema.TableColumn(schema);
				colvarCreatedByUserId.ColumnName = "CreatedByUserId";
				colvarCreatedByUserId.DataType = DbType.Int32;
				colvarCreatedByUserId.MaxLength = 0;
				colvarCreatedByUserId.AutoIncrement = false;
				colvarCreatedByUserId.IsNullable = false;
				colvarCreatedByUserId.IsPrimaryKey = false;
				colvarCreatedByUserId.IsForeignKey = false;
				colvarCreatedByUserId.IsReadOnly = false;
				
						colvarCreatedByUserId.DefaultSetting = @"((0))";
				colvarCreatedByUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedByUserId);
				
				TableSchema.TableColumn colvarModifiedByUserId = new TableSchema.TableColumn(schema);
				colvarModifiedByUserId.ColumnName = "ModifiedByUserId";
				colvarModifiedByUserId.DataType = DbType.Int32;
				colvarModifiedByUserId.MaxLength = 0;
				colvarModifiedByUserId.AutoIncrement = false;
				colvarModifiedByUserId.IsNullable = false;
				colvarModifiedByUserId.IsPrimaryKey = false;
				colvarModifiedByUserId.IsForeignKey = false;
				colvarModifiedByUserId.IsReadOnly = false;
				
						colvarModifiedByUserId.DefaultSetting = @"((0))";
				colvarModifiedByUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedByUserId);
				
				TableSchema.TableColumn colvarCreatedByActualUserId = new TableSchema.TableColumn(schema);
				colvarCreatedByActualUserId.ColumnName = "CreatedByActualUserId";
				colvarCreatedByActualUserId.DataType = DbType.Int32;
				colvarCreatedByActualUserId.MaxLength = 0;
				colvarCreatedByActualUserId.AutoIncrement = false;
				colvarCreatedByActualUserId.IsNullable = true;
				colvarCreatedByActualUserId.IsPrimaryKey = false;
				colvarCreatedByActualUserId.IsForeignKey = false;
				colvarCreatedByActualUserId.IsReadOnly = false;
				colvarCreatedByActualUserId.DefaultSetting = @"";
				colvarCreatedByActualUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedByActualUserId);
				
				TableSchema.TableColumn colvarModifiedByActualUserId = new TableSchema.TableColumn(schema);
				colvarModifiedByActualUserId.ColumnName = "ModifiedByActualUserId";
				colvarModifiedByActualUserId.DataType = DbType.Int32;
				colvarModifiedByActualUserId.MaxLength = 0;
				colvarModifiedByActualUserId.AutoIncrement = false;
				colvarModifiedByActualUserId.IsNullable = true;
				colvarModifiedByActualUserId.IsPrimaryKey = false;
				colvarModifiedByActualUserId.IsForeignKey = false;
				colvarModifiedByActualUserId.IsReadOnly = false;
				colvarModifiedByActualUserId.DefaultSetting = @"";
				colvarModifiedByActualUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedByActualUserId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["Symphony"].AddSchema("GTMMeeting",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("GTMOrganizerID")]
		[Bindable(true)]
		public int GTMOrganizerID 
		{
			get { return GetColumnValue<int>(Columns.GTMOrganizerID); }
			set { SetColumnValue(Columns.GTMOrganizerID, value); }
		}
		  
		[XmlAttribute("MeetingID")]
		[Bindable(true)]
		public long MeetingID 
		{
			get { return GetColumnValue<long>(Columns.MeetingID); }
			set { SetColumnValue(Columns.MeetingID, value); }
		}
		  
		[XmlAttribute("UniqueMeetingID")]
		[Bindable(true)]
		public string UniqueMeetingID 
		{
			get { return GetColumnValue<string>(Columns.UniqueMeetingID); }
			set { SetColumnValue(Columns.UniqueMeetingID, value); }
		}
		  
		[XmlAttribute("Subject")]
		[Bindable(true)]
		public string Subject 
		{
			get { return GetColumnValue<string>(Columns.Subject); }
			set { SetColumnValue(Columns.Subject, value); }
		}
		  
		[XmlAttribute("JoinURL")]
		[Bindable(true)]
		public string JoinURL 
		{
			get { return GetColumnValue<string>(Columns.JoinURL); }
			set { SetColumnValue(Columns.JoinURL, value); }
		}
		  
		[XmlAttribute("MaxParticipants")]
		[Bindable(true)]
		public int MaxParticipants 
		{
			get { return GetColumnValue<int>(Columns.MaxParticipants); }
			set { SetColumnValue(Columns.MaxParticipants, value); }
		}
		  
		[XmlAttribute("MeetingType")]
		[Bindable(true)]
		public string MeetingType 
		{
			get { return GetColumnValue<string>(Columns.MeetingType); }
			set { SetColumnValue(Columns.MeetingType, value); }
		}
		  
		[XmlAttribute("StartDateTime")]
		[Bindable(true)]
		public DateTime StartDateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.StartDateTime); }
			set { SetColumnValue(Columns.StartDateTime, value); }
		}
		  
		[XmlAttribute("EndDateTime")]
		[Bindable(true)]
		public DateTime EndDateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.EndDateTime); }
			set { SetColumnValue(Columns.EndDateTime, value); }
		}
		  
		[XmlAttribute("TimeZoneKey")]
		[Bindable(true)]
		public int TimeZoneKey 
		{
			get { return GetColumnValue<int>(Columns.TimeZoneKey); }
			set { SetColumnValue(Columns.TimeZoneKey, value); }
		}
		  
		[XmlAttribute("PasswordRequired")]
		[Bindable(true)]
		public bool PasswordRequired 
		{
			get { return GetColumnValue<bool>(Columns.PasswordRequired); }
			set { SetColumnValue(Columns.PasswordRequired, value); }
		}
		  
		[XmlAttribute("ConferenceCallInfo")]
		[Bindable(true)]
		public string ConferenceCallInfo 
		{
			get { return GetColumnValue<string>(Columns.ConferenceCallInfo); }
			set { SetColumnValue(Columns.ConferenceCallInfo, value); }
		}
		  
		[XmlAttribute("IsEditable")]
		[Bindable(true)]
		public bool IsEditable 
		{
			get { return GetColumnValue<bool>(Columns.IsEditable); }
			set { SetColumnValue(Columns.IsEditable, value); }
		}
		  
		[XmlAttribute("CreatedBy")]
		[Bindable(true)]
		public string CreatedBy 
		{
			get { return GetColumnValue<string>(Columns.CreatedBy); }
			set { SetColumnValue(Columns.CreatedBy, value); }
		}
		  
		[XmlAttribute("ModifiedBy")]
		[Bindable(true)]
		public string ModifiedBy 
		{
			get { return GetColumnValue<string>(Columns.ModifiedBy); }
			set { SetColumnValue(Columns.ModifiedBy, value); }
		}
		  
		[XmlAttribute("CreatedOn")]
		[Bindable(true)]
		public DateTime CreatedOn 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedOn); }
			set { SetColumnValue(Columns.CreatedOn, value); }
		}
		  
		[XmlAttribute("ModifiedOn")]
		[Bindable(true)]
		public DateTime ModifiedOn 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifiedOn); }
			set { SetColumnValue(Columns.ModifiedOn, value); }
		}
		  
		[XmlAttribute("CreatedByUserId")]
		[Bindable(true)]
		public int CreatedByUserId 
		{
			get { return GetColumnValue<int>(Columns.CreatedByUserId); }
			set { SetColumnValue(Columns.CreatedByUserId, value); }
		}
		  
		[XmlAttribute("ModifiedByUserId")]
		[Bindable(true)]
		public int ModifiedByUserId 
		{
			get { return GetColumnValue<int>(Columns.ModifiedByUserId); }
			set { SetColumnValue(Columns.ModifiedByUserId, value); }
		}
		  
		[XmlAttribute("CreatedByActualUserId")]
		[Bindable(true)]
		public int? CreatedByActualUserId 
		{
			get { return GetColumnValue<int?>(Columns.CreatedByActualUserId); }
			set { SetColumnValue(Columns.CreatedByActualUserId, value); }
		}
		  
		[XmlAttribute("ModifiedByActualUserId")]
		[Bindable(true)]
		public int? ModifiedByActualUserId 
		{
			get { return GetColumnValue<int?>(Columns.ModifiedByActualUserId); }
			set { SetColumnValue(Columns.ModifiedByActualUserId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varGTMOrganizerID,long varMeetingID,string varUniqueMeetingID,string varSubject,string varJoinURL,int varMaxParticipants,string varMeetingType,DateTime varStartDateTime,DateTime varEndDateTime,int varTimeZoneKey,bool varPasswordRequired,string varConferenceCallInfo,bool varIsEditable,string varCreatedBy,string varModifiedBy,DateTime varCreatedOn,DateTime varModifiedOn,int varCreatedByUserId,int varModifiedByUserId,int? varCreatedByActualUserId,int? varModifiedByActualUserId)
		{
			GTMMeeting item = new GTMMeeting();
			
			item.GTMOrganizerID = varGTMOrganizerID;
			
			item.MeetingID = varMeetingID;
			
			item.UniqueMeetingID = varUniqueMeetingID;
			
			item.Subject = varSubject;
			
			item.JoinURL = varJoinURL;
			
			item.MaxParticipants = varMaxParticipants;
			
			item.MeetingType = varMeetingType;
			
			item.StartDateTime = varStartDateTime;
			
			item.EndDateTime = varEndDateTime;
			
			item.TimeZoneKey = varTimeZoneKey;
			
			item.PasswordRequired = varPasswordRequired;
			
			item.ConferenceCallInfo = varConferenceCallInfo;
			
			item.IsEditable = varIsEditable;
			
			item.CreatedBy = varCreatedBy;
			
			item.ModifiedBy = varModifiedBy;
			
			item.CreatedOn = varCreatedOn;
			
			item.ModifiedOn = varModifiedOn;
			
			item.CreatedByUserId = varCreatedByUserId;
			
			item.ModifiedByUserId = varModifiedByUserId;
			
			item.CreatedByActualUserId = varCreatedByActualUserId;
			
			item.ModifiedByActualUserId = varModifiedByActualUserId;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,int varGTMOrganizerID,long varMeetingID,string varUniqueMeetingID,string varSubject,string varJoinURL,int varMaxParticipants,string varMeetingType,DateTime varStartDateTime,DateTime varEndDateTime,int varTimeZoneKey,bool varPasswordRequired,string varConferenceCallInfo,bool varIsEditable,string varCreatedBy,string varModifiedBy,DateTime varCreatedOn,DateTime varModifiedOn,int varCreatedByUserId,int varModifiedByUserId,int? varCreatedByActualUserId,int? varModifiedByActualUserId)
		{
			GTMMeeting item = new GTMMeeting();
			
				item.Id = varId;
			
				item.GTMOrganizerID = varGTMOrganizerID;
			
				item.MeetingID = varMeetingID;
			
				item.UniqueMeetingID = varUniqueMeetingID;
			
				item.Subject = varSubject;
			
				item.JoinURL = varJoinURL;
			
				item.MaxParticipants = varMaxParticipants;
			
				item.MeetingType = varMeetingType;
			
				item.StartDateTime = varStartDateTime;
			
				item.EndDateTime = varEndDateTime;
			
				item.TimeZoneKey = varTimeZoneKey;
			
				item.PasswordRequired = varPasswordRequired;
			
				item.ConferenceCallInfo = varConferenceCallInfo;
			
				item.IsEditable = varIsEditable;
			
				item.CreatedBy = varCreatedBy;
			
				item.ModifiedBy = varModifiedBy;
			
				item.CreatedOn = varCreatedOn;
			
				item.ModifiedOn = varModifiedOn;
			
				item.CreatedByUserId = varCreatedByUserId;
			
				item.ModifiedByUserId = varModifiedByUserId;
			
				item.CreatedByActualUserId = varCreatedByActualUserId;
			
				item.ModifiedByActualUserId = varModifiedByActualUserId;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GTMOrganizerIDColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn MeetingIDColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn UniqueMeetingIDColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SubjectColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn JoinURLColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MaxParticipantsColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn MeetingTypeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn StartDateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn EndDateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn TimeZoneKeyColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordRequiredColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ConferenceCallInfoColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn IsEditableColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedByColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedByColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedOnColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedOnColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedByUserIdColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedByUserIdColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedByActualUserIdColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedByActualUserIdColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string GTMOrganizerID = @"GTMOrganizerID";
			 public static string MeetingID = @"MeetingID";
			 public static string UniqueMeetingID = @"UniqueMeetingID";
			 public static string Subject = @"Subject";
			 public static string JoinURL = @"JoinURL";
			 public static string MaxParticipants = @"MaxParticipants";
			 public static string MeetingType = @"MeetingType";
			 public static string StartDateTime = @"StartDateTime";
			 public static string EndDateTime = @"EndDateTime";
			 public static string TimeZoneKey = @"TimeZoneKey";
			 public static string PasswordRequired = @"PasswordRequired";
			 public static string ConferenceCallInfo = @"ConferenceCallInfo";
			 public static string IsEditable = @"IsEditable";
			 public static string CreatedBy = @"CreatedBy";
			 public static string ModifiedBy = @"ModifiedBy";
			 public static string CreatedOn = @"CreatedOn";
			 public static string ModifiedOn = @"ModifiedOn";
			 public static string CreatedByUserId = @"CreatedByUserId";
			 public static string ModifiedByUserId = @"ModifiedByUserId";
			 public static string CreatedByActualUserId = @"CreatedByActualUserId";
			 public static string ModifiedByActualUserId = @"ModifiedByActualUserId";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
