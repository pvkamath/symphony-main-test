using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
    /// <summary>
    /// Controller class for cnrCompany
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class CnrCompanyController
    {
        // Preload our schema..
        CnrCompany thisSchemaLoad = new CnrCompany();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public CnrCompanyCollection FetchAll()
        {
            CnrCompanyCollection coll = new CnrCompanyCollection();
            Query qry = new Query(CnrCompany.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public CnrCompanyCollection FetchByID(object Id)
        {
            CnrCompanyCollection coll = new CnrCompanyCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public CnrCompanyCollection FetchByQuery(Query qry)
        {
            CnrCompanyCollection coll = new CnrCompanyCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (CnrCompany.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (CnrCompany.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int CustomerID,int CNRCompanyID,int CreatedByUserId,int ModifiedByUserId,int? CreatedByActualUserId,int? ModifiedByActualUserId)
	    {
		    CnrCompany item = new CnrCompany();
		    
            item.CustomerID = CustomerID;
            
            item.CNRCompanyID = CNRCompanyID;
            
            item.CreatedByUserId = CreatedByUserId;
            
            item.ModifiedByUserId = ModifiedByUserId;
            
            item.CreatedByActualUserId = CreatedByActualUserId;
            
            item.ModifiedByActualUserId = ModifiedByActualUserId;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,int CustomerID,int CNRCompanyID,int CreatedByUserId,int ModifiedByUserId,int? CreatedByActualUserId,int? ModifiedByActualUserId)
	    {
		    CnrCompany item = new CnrCompany();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.CustomerID = CustomerID;
				
			item.CNRCompanyID = CNRCompanyID;
				
			item.CreatedByUserId = CreatedByUserId;
				
			item.ModifiedByUserId = ModifiedByUserId;
				
			item.CreatedByActualUserId = CreatedByActualUserId;
				
			item.ModifiedByActualUserId = ModifiedByActualUserId;
				
	        item.Save(UserName);
	    }
    }
}
