using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
    /// <summary>
    /// Controller class for CertificateTemplate
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class CertificateTemplateController
    {
        // Preload our schema..
        CertificateTemplate thisSchemaLoad = new CertificateTemplate();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public CertificateTemplateCollection FetchAll()
        {
            CertificateTemplateCollection coll = new CertificateTemplateCollection();
            Query qry = new Query(CertificateTemplate.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public CertificateTemplateCollection FetchByID(object Id)
        {
            CertificateTemplateCollection coll = new CertificateTemplateCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public CertificateTemplateCollection FetchByQuery(Query qry)
        {
            CertificateTemplateCollection coll = new CertificateTemplateCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (CertificateTemplate.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (CertificateTemplate.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int? CustomerID,int CertificateType,string Name,string Description,int Width,int Height,string Content,int ParentID,int? BgImageId,string BgImage,string BgImagePath,DateTime CreatedOn,DateTime ModifiedOn,string CreatedBy,string ModifiedBy,bool IsDeleted)
	    {
		    CertificateTemplate item = new CertificateTemplate();
		    
            item.CustomerID = CustomerID;
            
            item.CertificateType = CertificateType;
            
            item.Name = Name;
            
            item.Description = Description;
            
            item.Width = Width;
            
            item.Height = Height;
            
            item.Content = Content;
            
            item.ParentID = ParentID;
            
            item.BgImageId = BgImageId;
            
            item.BgImage = BgImage;
            
            item.BgImagePath = BgImagePath;
            
            item.CreatedOn = CreatedOn;
            
            item.ModifiedOn = ModifiedOn;
            
            item.CreatedBy = CreatedBy;
            
            item.ModifiedBy = ModifiedBy;
            
            item.IsDeleted = IsDeleted;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,int? CustomerID,int CertificateType,string Name,string Description,int Width,int Height,string Content,int ParentID,int? BgImageId,string BgImage,string BgImagePath,DateTime CreatedOn,DateTime ModifiedOn,string CreatedBy,string ModifiedBy,bool IsDeleted)
	    {
		    CertificateTemplate item = new CertificateTemplate();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.CustomerID = CustomerID;
				
			item.CertificateType = CertificateType;
				
			item.Name = Name;
				
			item.Description = Description;
				
			item.Width = Width;
				
			item.Height = Height;
				
			item.Content = Content;
				
			item.ParentID = ParentID;
				
			item.BgImageId = BgImageId;
				
			item.BgImage = BgImage;
				
			item.BgImagePath = BgImagePath;
				
			item.CreatedOn = CreatedOn;
				
			item.ModifiedOn = ModifiedOn;
				
			item.CreatedBy = CreatedBy;
				
			item.ModifiedBy = ModifiedBy;
				
			item.IsDeleted = IsDeleted;
				
	        item.Save(UserName);
	    }
    }
}
