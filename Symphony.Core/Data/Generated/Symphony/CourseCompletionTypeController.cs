using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
    /// <summary>
    /// Controller class for CourseCompletionType
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class CourseCompletionTypeController
    {
        // Preload our schema..
        CourseCompletionType thisSchemaLoad = new CourseCompletionType();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public CourseCompletionTypeCollection FetchAll()
        {
            CourseCompletionTypeCollection coll = new CourseCompletionTypeCollection();
            Query qry = new Query(CourseCompletionType.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public CourseCompletionTypeCollection FetchByID(object Id)
        {
            CourseCompletionTypeCollection coll = new CourseCompletionTypeCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public CourseCompletionTypeCollection FetchByQuery(Query qry)
        {
            CourseCompletionTypeCollection coll = new CourseCompletionTypeCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (CourseCompletionType.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (CourseCompletionType.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Description,string CodeName,string ModifiedBy,string CreatedBy,DateTime ModifiedOn,DateTime CreatedOn,int CreatedByUserId,int ModifiedByUserId,int? CreatedByActualUserId,int? ModifiedByActualUserId)
	    {
		    CourseCompletionType item = new CourseCompletionType();
		    
            item.Description = Description;
            
            item.CodeName = CodeName;
            
            item.ModifiedBy = ModifiedBy;
            
            item.CreatedBy = CreatedBy;
            
            item.ModifiedOn = ModifiedOn;
            
            item.CreatedOn = CreatedOn;
            
            item.CreatedByUserId = CreatedByUserId;
            
            item.ModifiedByUserId = ModifiedByUserId;
            
            item.CreatedByActualUserId = CreatedByActualUserId;
            
            item.ModifiedByActualUserId = ModifiedByActualUserId;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,string Description,string CodeName,string ModifiedBy,string CreatedBy,DateTime ModifiedOn,DateTime CreatedOn,int CreatedByUserId,int ModifiedByUserId,int? CreatedByActualUserId,int? ModifiedByActualUserId)
	    {
		    CourseCompletionType item = new CourseCompletionType();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.Description = Description;
				
			item.CodeName = CodeName;
				
			item.ModifiedBy = ModifiedBy;
				
			item.CreatedBy = CreatedBy;
				
			item.ModifiedOn = ModifiedOn;
				
			item.CreatedOn = CreatedOn;
				
			item.CreatedByUserId = CreatedByUserId;
				
			item.ModifiedByUserId = ModifiedByUserId;
				
			item.CreatedByActualUserId = CreatedByActualUserId;
				
			item.ModifiedByActualUserId = ModifiedByActualUserId;
				
	        item.Save(UserName);
	    }
    }
}
