using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace Symphony.Core.Data
{
    /// <summary>
    /// Controller class for Audience
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class AudienceController
    {
        // Preload our schema..
        Audience thisSchemaLoad = new Audience();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public AudienceCollection FetchAll()
        {
            AudienceCollection coll = new AudienceCollection();
            Query qry = new Query(Audience.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public AudienceCollection FetchByID(object Id)
        {
            AudienceCollection coll = new AudienceCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public AudienceCollection FetchByQuery(Query qry)
        {
            AudienceCollection coll = new AudienceCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (Audience.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (Audience.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Name,string InternalCode,int? ParentAudienceID,int SortOrder,int CustomerID,string ModifiedBy,string CreatedBy,DateTime ModifiedOn,DateTime CreatedOn,int CreatedByUserId,int ModifiedByUserId,int? CreatedByActualUserId,int? ModifiedByActualUserId,string SalesforcePartnerCode)
	    {
		    Audience item = new Audience();
		    
            item.Name = Name;
            
            item.InternalCode = InternalCode;
            
            item.ParentAudienceID = ParentAudienceID;
            
            item.SortOrder = SortOrder;
            
            item.CustomerID = CustomerID;
            
            item.ModifiedBy = ModifiedBy;
            
            item.CreatedBy = CreatedBy;
            
            item.ModifiedOn = ModifiedOn;
            
            item.CreatedOn = CreatedOn;
            
            item.CreatedByUserId = CreatedByUserId;
            
            item.ModifiedByUserId = ModifiedByUserId;
            
            item.CreatedByActualUserId = CreatedByActualUserId;
            
            item.ModifiedByActualUserId = ModifiedByActualUserId;
            
            item.SalesforcePartnerCode = SalesforcePartnerCode;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,string Name,string InternalCode,int? ParentAudienceID,int SortOrder,int CustomerID,string ModifiedBy,string CreatedBy,DateTime ModifiedOn,DateTime CreatedOn,int CreatedByUserId,int ModifiedByUserId,int? CreatedByActualUserId,int? ModifiedByActualUserId,string SalesforcePartnerCode)
	    {
		    Audience item = new Audience();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.Name = Name;
				
			item.InternalCode = InternalCode;
				
			item.ParentAudienceID = ParentAudienceID;
				
			item.SortOrder = SortOrder;
				
			item.CustomerID = CustomerID;
				
			item.ModifiedBy = ModifiedBy;
				
			item.CreatedBy = CreatedBy;
				
			item.ModifiedOn = ModifiedOn;
				
			item.CreatedOn = CreatedOn;
				
			item.CreatedByUserId = CreatedByUserId;
				
			item.ModifiedByUserId = ModifiedByUserId;
				
			item.CreatedByActualUserId = CreatedByActualUserId;
				
			item.ModifiedByActualUserId = ModifiedByActualUserId;
				
			item.SalesforcePartnerCode = SalesforcePartnerCode;
				
	        item.Save(UserName);
	    }
    }
}
