﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core.Data
{
    public class DataHelper
    {
        /// <summary>
        /// Returns a string with quotes around it
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Quote(string value)
        {
            return "\"" + value + "\"";
        }
    }
}
