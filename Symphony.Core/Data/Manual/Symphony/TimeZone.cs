﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core.Data
{
    public partial class TimeZone
    {
        public int GMTOffset(bool includeDaylightSavings)
        {
            return GetOffset(this.Description, includeDaylightSavings);
        }

        public static int GetOffset(string timeZone, bool includeDaylightSavings)
        {
            int offset = 0;
            switch (timeZone)
            {
                case "Atlantic":
                    offset = -4;
                    break;
                case "Eastern":
                    offset = -5;
                    break;
                case "Central":
                    offset = -6;
                    break;
                case "Mountain":
                    offset = -7;
                    break;
                case "Pacific":
                    offset = -8;
                    break;
                case "Alaskan":
                case "Alaska":
                    offset = -9;
                    break;
                case "Hawaiian":
                case "Hawaii":
                    offset = -10;
                    break;
                case "West Pacific":
                case "Guam":
                    offset = +10;
                    break;
            }
            // if we're in daylight savings, shift the offset
            // we don't have to be too particular about this one
            if (includeDaylightSavings && DateTime.Now.IsDaylightSavingTime())
            {
                offset += 1;
            }
            return offset;
        }
    }
}
