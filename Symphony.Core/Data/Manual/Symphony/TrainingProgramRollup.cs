﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using Symphony.Core.Extensions;

namespace Symphony.Core.Data
{
    public partial class TrainingProgramRollup
    {
        private int? _minCourseWork = null;

        /// <summary>
        /// Will generate a db request if this object is not 
        /// created through a query that joins TrainingProgram
        /// and includes the column MinCourseWork from TrainingProgram
        /// </summary>
        public int MinCourseWork
        {
            get
            {
                if (!_minCourseWork.HasValue) {
                    _minCourseWork = TrainingProgram.MinCourseWork;
                }

                return _minCourseWork.HasValue ? _minCourseWork.Value : 0;
            }
            set {
                _minCourseWork = value > 0 ? value : 0;
            }
        }
        /// <summary>
        /// Temporary - Should add foreign keys to this table. 
        /// There appears to be an issue with backfilling the training program rollup table
        /// which has created entries for training programs that don't exist. We should 
        /// fix the data and add the key.
        /// </summary>
        private Symphony.Core.Data.TrainingProgram _trainingProgram;
        public Symphony.Core.Data.TrainingProgram TrainingProgram
        {
            get
            {
                if (_trainingProgram == null)
                {
                    _trainingProgram = Symphony.Core.Data.TrainingProgram.FetchByID(this.TrainingProgramID);
                }
                return _trainingProgram;
            }
            set
            {
                SetColumnValue("TrainingProgramID", value.Id);
                _trainingProgram = value;
            }
        }
        /// <summary>
        /// Temporary - Should add foreign keys to this table. 
        /// Assuming since there are invalid training program ids there are also invalid user ids. 
        /// </summary>
        private Symphony.Core.Data.User _user;
        public Symphony.Core.Data.User User
        {
            get
            {
                if (_user == null)
                {
                    _user = Symphony.Core.Data.User.FetchByID(this.UserID);
                }
                return _user;
            }
            set
            {
                SetColumnValue("UserID", value.Id);
                _user = value;
            }
        }

        public string CompletedDay
        {
            get
            {
                string timezone = TimeZoneInfo.Utc.StandardName;
                if (this.TrainingProgram != null && this.TrainingProgram.Customer != null && !string.IsNullOrWhiteSpace(this.TrainingProgram.Customer.TimeZone))
                {
                    timezone = this.TrainingProgram.Customer.TimeZone;
                }
                return this.DateCompleted.HasValue ? this.DateCompleted.Value.UtcToFormattedTimeZone(timezone, DateTimeFormatInfo.CurrentInfo.ShortDatePattern) : "";
            }
        }
    }
}
