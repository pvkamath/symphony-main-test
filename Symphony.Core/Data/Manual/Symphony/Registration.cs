﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core.Data
{
    public partial class Registration
    {
        /// <summary>
        /// A simple property to add easy access to the course id
        /// </summary>
        public int CourseID
        {
            get
            {
                return this.ClassX.CourseID;
            }
        }

        public string Attendance
        {
            get
            {
                return this.AttendedIndicator ? "Attended" : "Not Attended";
            }
        }

        private string _userFullName = null;
        public string UserFullName
        {
            get
            {
                if (this._userFullName == null)
                {
                    return new Data.User(this.RegistrantID).FullName;
                }
                return this._userFullName;
            }
            set
            {
                this._userFullName = value;
            }
        }
    }
}
