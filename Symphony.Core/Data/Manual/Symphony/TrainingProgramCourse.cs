﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;

namespace Symphony.Core.Data
{
    public partial class TrainingProgramCourse
    {
        private OnlineCourse onlineCourse;
        /// <summary>
        /// For notification only. Loads the online course associated with the trainign program course if this is an online course.
        /// </summary>
        /// <returns></returns>
        private void LoadOnlineCourse() {
            if (CourseTypeID == (int)Symphony.Core.CourseType.Online)
            {
                if (onlineCourse == null)
                {
                    onlineCourse = new OnlineCourse(CourseID);
                }
            }
        }
        

        private Course colCourse;
        /// <summary>
        /// Only for use with notifications. The course may be an online course or a classroom course.
        /// Not for saving.
        /// </summary>
        public Course Course
        {
            get
            {
                if (colCourse == null)
                {
                    if (CourseTypeID == (int)Symphony.Core.CourseType.Classroom)
                    {
                        colCourse = new Course(CourseID);
                    }
                    else
                    {
                        LoadOnlineCourse();
                        if (onlineCourse != null)
                        {
                            colCourse = new Course();
                            colCourse.CategoryID = onlineCourse.CategoryID;
                            colCourse.Credit = onlineCourse.Credit;
                            colCourse.Description = onlineCourse.Description;
                            colCourse.Name = onlineCourse.Name;
                        }
                    }
                }
                return colCourse;
            }
            set
            {
                colCourse = value;
            }
        }

        private ClassXCollection colClasses;
        /// <summary>
        /// For use with notifications only. 
        /// </summary>
        public ClassXCollection Classes {
            get 
            {
               
                if (colClasses == null)
                {
                    if (CourseTypeID == (int)Symphony.Core.CourseType.Classroom)
                    {
                        DateTime maxDate = ProgramEndDate;

                        if (ProgramDueDate.CompareTo(ProgramStartDate) > 0 && ProgramDueDate.CompareTo(ProgramEndDate) < 0)
                        {
                            maxDate = ProgramDueDate;
                        }

                        ClassXCollection allClassesForCourse = new ClassXCollection();
                        allClassesForCourse.Where(ClassX.Columns.CourseID, CourseID);
                        allClassesForCourse.Load();

                        colClasses = new ClassXCollection();

                        foreach (ClassX classX in allClassesForCourse)
                        {
                            if (classX.MinStartDateTime.CompareTo(ProgramStartDate) > 0 &&
                                classX.MinStartDateTime.CompareTo(maxDate) < 0)
                            {
                                colClasses.Add(classX);
                            }
                        }                    
                    }
                    else
                    {
                        LoadOnlineCourse();
                        if (onlineCourse != null)
                        {
                            // Since online courses do not have classes, lets make one up
                            // Start date will be either the program start date, or the online course active after date
                            // Will make up a location named virtual

                            colClasses = new ClassXCollection();
                            ClassX classX = new ClassX();
                            classX.Name = onlineCourse.Name;
                            classX.Description = onlineCourse.Description;
                            classX.Location = "Vritual";
                            classX.CreatedOn = onlineCourse.CreatedOn.HasValue ? onlineCourse.CreatedOn.Value : new DateTime();
                            classX.ClassDates = new ClassDateCollection();
                            
                            ClassDate classDate = new ClassDate();

                            DateTime startDate = ProgramStartDate;
                            if (ActiveAfterDate.HasValue &&
                                ActiveAfterDate.Value.CompareTo(ProgramStartDate) > 0 &&
                                ActiveAfterDate.Value.CompareTo(ProgramEndDate) < 0)
                            {
                                startDate = ActiveAfterDate.Value;
                            }

                            classDate.StartDateTime = startDate;
                            classDate.Customer = new Customer(CustomerID);

                            classX.ClassDates.Add(classDate);

                            classX.Room = new Room();
                            classX.Room.Name = "Virtual";

                            classX.Room.Venue = new Venue();
                            classX.Room.Venue.Name = "Virtual";

                            colClasses.Add(classX);
                        }
                    }
                }
                return colClasses;
            }
            set
            {
                colClasses = value;
            }
        }
    }
}
