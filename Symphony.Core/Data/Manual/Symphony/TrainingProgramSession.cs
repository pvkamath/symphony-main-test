﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using SubSonic;
using Symphony.Core.Extensions;
using Symphony.Core.Interfaces;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;

namespace Symphony.Core.Data
{
    [Serializable]
    public partial class TrainingProgramSession : TrainingProgram
    {
        public int SessionClassID { get; set; }

        public string SessionClassName { get; set; }

        public int SessionDailyDuration { get; set; }

        public int SessionNumberOfDays { get; set; }

        public DateTime MinSessionStartDate { get; set; }

        public DateTime MaxSessionStartDate { get; set; }

        public DateTime SessionEndDate { get; set; }

        public DateTime SessionStartDate { get; set; }

        public string SessionStartDay
        {
            get
            {
                return SessionStartDate.UtcToFormattedTimeZone(this.GetTimeZone());
            }
        }
        public string SessionEndDay
        {
            get
            {
                return SessionEndDate.UtcToFormattedTimeZone(this.GetTimeZone());
            }
        }

        private string GetTimeZone()
        {
            string timezone = TimeZoneInfo.Utc.StandardName;
            if (this.Customer != null && !string.IsNullOrWhiteSpace(this.Customer.TimeZone))
            {
                timezone = this.Customer.TimeZone;
            }
            return timezone;
        }
    }
}
