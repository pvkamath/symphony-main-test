﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core.Data
{
    public partial class Notification
    {
        public string Key
        {
            get
            {
                return string.Format("sId{0}-rId{1}-tId{2}-gId{3}",
                    this.SenderID, this.RecipientID, this.TemplateID, this.GroupID);
            }
        }
    }
}
