﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Data
{
    public partial class FastSearchableUser : IExportable
    {
        private Location _fullLocation;
        public Location FullLocation
        {
            get
            {
                if (_fullLocation == null)
                {
                    _fullLocation = new Location(LocationID);
                }
                return _fullLocation;
            }
        }

        private JobRole _fullJobRole;
        public JobRole FullJobRole
        {
            get
            {
                if (_fullJobRole == null)
                {
                    _fullJobRole = new JobRole(JobRoleID);
                }
                return _fullJobRole;
            }
        }
     
        public string Export()
        {
            Data.User user = null;
            if (SupervisorID > 0)
            {
                user = new User(SupervisorID);
            }

            return string.Join(",", new string[]{
                DataHelper.Quote(Username),
                FullLocation.InternalCode,
                DataHelper.Quote(EmployeeNumber),
                DataHelper.Quote(FirstName),
                DataHelper.Quote(MiddleName),
                DataHelper.Quote(LastName),
                HireDate.HasValue && HireDate.Value.Year != 1900 ? HireDate.Value.ToShortDateString() : "",
                NewHireIndicator.ToString(),
                DataHelper.Quote(Email),
                FullJobRole.InternalCode,
                DataHelper.Quote(Notes),
                user == null ? "" : user.EmployeeNumber,
                "",
                Status,
                DataHelper.Quote(Supervisor),
                DataHelper.Quote(ReportingSupervisor)
            });
        }

        public static string ExportHeader()
        {
            return string.Join(",", new string[]{
                "Username",
                "LocationCode",
                "EmployeeNumber",
                "FirstName",
                "MiddleName",
                "LastName",
                "HireDate",
                "NewHireIndicator",
                "Email",
                "JobRoleCode",
                "Notes",
                "SupervisorCode",
                "Password",
                "Status",
                "ClassroomSupervisor",
                "ReportingSupervisor",
            });
        }
    }
}
