﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Controllers;
using System.Configuration;
using System.Web;

namespace Symphony.Core.Data
{
    public partial class GTMInvitation
    {
        public List<int> GetInviteeSupervisor()
        {
            Data.User user = new User(this.UserID);
            return new List<int>() { user.SupervisorID, user.SecondarySupervisorID };
        }

        public List<int> GetInviteeReportingSupervisor()
        {
            Data.User user = new User(this.UserID);
            return new List<int>() { user.ReportingSupervisorID };
        }
    }
}
