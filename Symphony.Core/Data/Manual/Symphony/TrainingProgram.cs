﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;
using Symphony.Core.Extensions;
using Symphony.Core.Interfaces;
using Symphony.Core.Controllers;

namespace Symphony.Core.Data
{
    public partial class TrainingProgram : IAssociatedImage
    {

        /// <summary>
        /// Will list users who can see this training program on their student portal
        /// </summary>
        /// <returns></returns>
        public List<int> GetAssignedUsers()
        {
            return GetAssignedUsers(true);
        }

        /// <summary>
        /// Will list all users assigned to this training program (Will not filter out new hires
        /// and will return users regardless of the training program dates)
        /// </summary>
        /// <returns></returns>
        public List<int> GetUnfilteredAssignedUsers()
        {
            return GetAssignedUsers(false);
        }

        private List<int> GetAssignedUsers(bool isFilterToMatchPortal)
        {
            // If a Training Program has 'New Hire' set, only send to New-Hire users
            // otherwise only send to NON-New-Hire users
            List<int> userIds = new List<int>();
            if (this.Id > 0)
            {
                userIds = Data.SPs.GetTrainingProgramUsers(this.Id, this.IsNewHire)
                .ExecuteTypedList<Data.TrainingProgramUser>()
                .Select(tp => (int)tp.UserID).ToList();
            }

            if (userIds.Count == 0)
            {
                // bail out
                return new List<int>();
            }

            // get a list of the actual users from the above query
            // Possible to have more than 2000 users assigned
            List<User> users = new List<User>();

            List<int[]> userIdGroups = userIds.InGroupsOf(2000).ToList();

            foreach (int[] userIdGroup in userIdGroups)
            {
                users.AddRange(Select.AllColumnsFrom<Data.User>()
                    .Where(Data.User.Columns.Id).In(userIdGroup)
                    .ExecuteTypedList<User>());
            }

            List<int> filtered = new List<int>();

            if (isFilterToMatchPortal)
            {
                // ok, once we have the list of users, check to see if the program is *really* assigned;
                // this includes filtering out based on whether the user is new hire, the TP "transition" periods, customer transition periods, etc, all apply, and so on
                // since this is the same filter used when the portal loads, it should ensure that no user gets into this list incorrectly
                foreach (User user in users)
                {
                    List<TrainingProgram> programs =
                        SPs.GetTrainingProgramsForUser(user.Id, user.NewHireIndicator, string.Empty, string.Empty, string.Empty, 0, 100000, null, null)
                        .ExecuteTypedList<TrainingProgram>();

                    if (programs.Any(t => t.Id == this.Id || t.ParentTrainingProgramID == this.Id))
                    {
                        filtered.Add(user.Id);
                    }
                }
            }
            else
            {
                filtered = users.Select(u => u.Id).ToList();
            }

            return filtered;
        }

        public List<int> GetAssignedUsersSupervisors()
        {
            List<int> assignedUsers = this.GetAssignedUsers();
            if (assignedUsers.Count == 0)
            {
                return new List<int>();
            }
            return User.GetSupervisors(assignedUsers);
        }

        public List<int> GetAssignedUsersReportingSupervisors()
        {
            List<int> assignedUsers = this.GetAssignedUsers();
            if (assignedUsers.Count == 0)
            {
                return new List<int>();
            }
            return User.GetReportingSupervisors(assignedUsers);
        }

        /// <summary>
        /// For notifications
        /// </summary>
        public string StartDay
        {
            get { return this.StartDate.ToShortDateString(); }
        }

        /// <summary>
        /// For notifications
        /// </summary>
        public string EndDay
        {
            get { return this.EndDate.ToShortDateString(); }
        }

        /// <summary>
        /// For notifications
        /// </summary>
        public string DueDay
        {
            get { return this.DueDate.ToShortDateString(); }
        }

        /// <summary>
        /// The start date, rounded DOWN to the nearest day
        /// </summary>
        public DateTime StartDateWithoutTime
        {
            get { return this.StartDate.AddHours(-this.StartDate.Hour).AddMinutes(-this.StartDate.Minute); }
        }

        /// <summary>
        /// The end date, rounded UP to the nearest day
        /// </summary>
        public DateTime EndDateWithoutTime
        {
            get { return this.EndDate.AddHours(24 - this.EndDate.Hour).AddMinutes(60 - this.EndDate.Minute); }
        }

        /// <summary>
        /// Return the start time scaled to the customers time zone. 
        /// Training program start dates are all set without a time component. These are still saved in the db
        /// with a time component of 12am. This returns the start date as a UTC formated date that will 
        /// represent the beginning of the day for the customers time zone
        /// </summary>
        public DateTime CustomerTimezoneStartTime
        {
            get
            {
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(Customer.TimeZone);
                TimeSpan startOffset = tzi.GetUtcOffset(StartDate);
                return StartDate.WithUtcFlag().Subtract(startOffset);
            }
        }
        /// <summary>
        /// Return the end time scaled to the customers time zone. 
        /// Training program start dates are all set without a time component. These are still saved in the db
        /// with a time component of 12am. This returns the start date as a UTC formated date that will 
        /// represent the end of the day for the customers time zone
        /// </summary>
        public DateTime CustomerTimezoneEndTime
        {
            get
            {
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(Customer.TimeZone);
                TimeSpan endOffset = tzi.GetUtcOffset(EndDate).Subtract(TimeSpan.FromDays(1)); // add one day because any time during the enddate is valid
                return EndDate.WithUtcFlag().Subtract(endOffset);
            }
        }


        private Symphony.Core.Data.TrainingProgramCourseCollection colRequiredCourses;
        /// <summary>
        /// For use with notifications only
        /// </summary>
        public Symphony.Core.Data.TrainingProgramCourseCollection RequiredCourses
        {
            get
            {
                if (colRequiredCourses == null)
                {
                    colRequiredCourses = new Symphony.Core.Data.TrainingProgramCourseCollection();
                    colRequiredCourses.Where(TrainingProgramCourse.Columns.TrainingProgramID, Id);
                    colRequiredCourses.Where(TrainingProgramCourse.Columns.SyllabusTypeID, (int)Symphony.Core.SyllabusType.Required);
                    colRequiredCourses.Load();
                }
                return colRequiredCourses;
            }
            set
            {
                colRequiredCourses = value;
            }
        }

        private Symphony.Core.Data.TrainingProgramCourseCollection colElectiveCourses;
        /// <summary>
        /// For use with notifications only
        /// </summary>
        public Symphony.Core.Data.TrainingProgramCourseCollection ElectiveCourses
        {
            get
            {
                if (colElectiveCourses == null)
                {
                    colElectiveCourses = new Symphony.Core.Data.TrainingProgramCourseCollection();
                    colElectiveCourses.Where(TrainingProgramCourse.Columns.TrainingProgramID, Id);
                    colElectiveCourses.Where(TrainingProgramCourse.Columns.SyllabusTypeID, (int)Symphony.Core.SyllabusType.Elective);
                    colElectiveCourses.Load();
                }
                return colElectiveCourses;
            }
            set
            {
                colElectiveCourses = value;
            }
        }

        private Symphony.Core.Data.TrainingProgramCourseCollection colOptionalCourses;
        /// <summary>
        /// For use with notifications only
        /// </summary>
        public Symphony.Core.Data.TrainingProgramCourseCollection OptionalCourses
        {
            get
            {
                if (colOptionalCourses == null)
                {
                    colOptionalCourses = new Symphony.Core.Data.TrainingProgramCourseCollection();
                    colOptionalCourses.Where(TrainingProgramCourse.Columns.TrainingProgramID, Id);
                    colOptionalCourses.Where(TrainingProgramCourse.Columns.SyllabusTypeID, (int)Symphony.Core.SyllabusType.Optional);
                    colOptionalCourses.Load();
                }
                return colOptionalCourses;
            }
            set
            {
                colOptionalCourses = value;
            }
        }

        public Data.User PrimaryLeader
        {
            get
            {
                var leader = Select.AllColumnsFrom<Data.TrainingProgramLeader>()
                    .Where(Data.TrainingProgramLeader.Columns.TrainingProgramID).IsEqualTo(this.Id)
                    .And(Data.TrainingProgramLeader.Columns.IsPrimary).IsEqualTo(true)
                    .ExecuteSingle<Data.TrainingProgramLeader>();

                if (leader != null)
                {
                    return new Data.User(leader.UserID);
                }
                return new Data.User();
            }
        }

        private Symphony.Core.Data.TrainingProgramCourseCollection colFinalAssessments;
        /// <summary>
        /// For use with notifications only
        /// </summary>
        public Symphony.Core.Data.TrainingProgramCourseCollection FinalAssessments
        {
            get
            {
                if (colFinalAssessments == null)
                {
                    colFinalAssessments = new Symphony.Core.Data.TrainingProgramCourseCollection();
                    colFinalAssessments.Where(TrainingProgramCourse.Columns.TrainingProgramID, Id);
                    colFinalAssessments.Where(TrainingProgramCourse.Columns.SyllabusTypeID, (int)Symphony.Core.SyllabusType.Final);
                    colFinalAssessments.Load();
                }
                return colFinalAssessments;
            }
            set
            {
                colFinalAssessments = value;
            }
        }

    }
}
