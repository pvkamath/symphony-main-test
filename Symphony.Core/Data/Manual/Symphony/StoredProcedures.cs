﻿using Symphony.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;

namespace Symphony.Core.Data
{
    public partial class SPs
    {
        public static StoredProcedure GetTrainingProgramsForUserWithInProgressToggle(int? userId, bool? newHire, string searchText, string orderBy, string orderDir, int? pageIndex, int? pageSize, DateTime? startDate, DateTime? endDate)
        {
            return orderBy == "InProgress"
                ?
                    GetTrainingProgramsForUserWithInProgressSort(userId, newHire, searchText, orderBy, orderDir, pageIndex, pageSize, startDate, endDate)
                :
                    GetTrainingProgramsForUser(userId, newHire, searchText, orderBy, orderDir, pageIndex, pageSize, startDate, endDate);
        }
    }
}
