﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Extensions;

namespace Symphony.Core.Data
{
    public partial class ClassDateCollection
    {
        /// <summary>
        /// Checks to see if the specified dates overlap this list of dates. Dates must be in UTC
        /// </summary>
        /// <param name="dtFirst"></param>
        /// <param name="dtLast"></param>
        /// <returns></returns>
        public bool Overlaps(DateTime dtStart1, DateTime dtEnd1)
        {
            foreach (ClassDate cd in this)
            {
                DateTime dtStart2 = cd.StartDateTime.WithUtcFlag();
                DateTime dtEnd2 = cd.StartDateTime.WithUtcFlag();

                if ((dtStart1 <= dtEnd2) && (dtStart2 <= dtEnd1))
                {
                    return true;
                }
            }

            return false;
        }

    }
}
