﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;
using Symphony.Core.Extensions;

namespace Symphony.Core.Data
{
    public partial class TrainingProgramToCourseLink
    {
        // this stuff is all for messaging

        private OnlineCourse _onlineCourse;
        private Course _classroomCourse;
        private TrainingProgram _trainingProgram;

        public List<int> GetAssignedUsers()
        {
            // If a Training Program has 'New Hire' set, only send to New-Hire users
            // otherwise only send to NON-New-Hire users

            //TODO: this is REALLY slow
            List<int> userIds = new TrainingProgramUserCollection()
                .Where(TrainingProgramUser.Columns.TrainingProgramID, this.TrainingProgramID)
                .Where(TrainingProgramUser.Columns.IsUserNewHire, this.TrainingProgram.IsNewHire)
                .Load()
                .Select(tp => (int)tp.UserID).ToList();

            if (userIds.Count == 0)
            {
                // bail out
                return new List<int>();
            }

            // get a list of the actual users from the above query
            // 2000 at a time
            List<User> users = new List<User>();
            IEnumerable<int[]> userIdGroups = userIds.InGroupsOf(2000);
            foreach (int[] userIdGroup in userIdGroups)
            {
                users.AddRange(Select.AllColumnsFrom<Data.User>()
                    .Where(Data.User.Columns.Id).In(userIdGroup)
                    .ExecuteTypedList<User>());
            }

            // ok, once we have the list of users, check to see if the program is *really* assigned;
            // this includes filtering out based on whether the user is new hire, the TP "transition" periods, customer transition periods, etc, all apply, and so on
            // since this is the same filter used when the portal loads, it should ensure that no user gets into this list incorrectly
            List<int> filtered = new List<int>();
            foreach (User user in users)
            {
                List<TrainingProgram> programs =
                    SPs.GetTrainingProgramsForUser(user.Id, user.NewHireIndicator, string.Empty, string.Empty, string.Empty, 0, 100000, null, null)
                    .ExecuteTypedList<TrainingProgram>();

                if (programs.Any(t => t.Id == this.TrainingProgramID))
                {
                    filtered.Add(user.Id);
                }
            }
            
            return filtered;
        }

        public List<int> GetAssignedUsersSupervisors()
        {
            List<int> assignedUsers = this.GetAssignedUsers();
            if (assignedUsers.Count == 0)
            {
                return new List<int>();
            }
            return User.GetSupervisors(assignedUsers);
        }

        public List<int> GetAssignedUsersReportingSupervisors()
        {
            List<int> assignedUsers = this.GetAssignedUsers();
            if (assignedUsers.Count == 0)
            {
                return new List<int>();
            }
            return User.GetReportingSupervisors(assignedUsers);
        }

        private decimal? _cost = null;
        public decimal Cost
        {
            get
            {
                LoadCourse();
                if (_cost.HasValue)
                {
                    return _cost.Value;
                }
                if (_onlineCourse != null)
                {
                    _cost = _onlineCourse.Cost;
                }
                else if (_classroomCourse != null)
                {
                    _cost = _classroomCourse.PerStudentFee;
                }
                return _cost.HasValue ? _cost.Value : 0;
            }
            set { _cost = value; }
        }

        private decimal? _credit = null;
        public decimal Credit
        {
            get
            {
                LoadCourse();
                if (_credit.HasValue)
                {
                    return _credit.Value;
                }
                if (_onlineCourse != null)
                {
                    _credit = _onlineCourse.Credit;
                }
                else if (_classroomCourse != null)
                {
                    _credit = _classroomCourse.Credit;
                }
                return _credit.HasValue ? _credit.Value : 0;
            }
            set { _credit = value; }
        }

        private string _description = null;
        public string Description
        {
            get
            {
                LoadCourse();
                if (_description != null)
                {
                    return _description;
                }
                if (_onlineCourse != null)
                {
                    _description = _onlineCourse.Description;
                }
                else if (_classroomCourse != null)
                {
                    _description = _classroomCourse.Description;
                }
                return _description != null ? _description : string.Empty;
            }
            set { _description = value; }
        }

        private string _courseName = null;
        public string CourseName
        {
            get
            {
                LoadCourse();
                if (_courseName != null)
                {
                    return _courseName;
                }
                if (_onlineCourse != null)
                {
                    _courseName = _onlineCourse.Name;
                }
                else if (_classroomCourse != null)
                {
                    _courseName = _classroomCourse.Name;
                }
                return _courseName != null ? _courseName : string.Empty;
            }
            set { _courseName = value; }
        }

        private bool? _publicIndicator = null;
        public bool PublicIndicator
        {
            get
            {
                LoadCourse();
                if (_publicIndicator.HasValue)
                {
                    return _publicIndicator.Value;
                }
                if (_onlineCourse != null)
                {
                    _publicIndicator = _onlineCourse.PublicIndicator;
                }
                else if (_classroomCourse != null)
                {
                    _publicIndicator = _classroomCourse.PublicIndicator;
                }
                return _publicIndicator.HasValue ? _publicIndicator.Value : false;
            }
            set { _publicIndicator = value; }
        }

        private string _trainingProgramName = null;
        public string TrainingProgramName
        {
            get { return _trainingProgramName == null ? LoadTrainingProgram().Name : _trainingProgramName; }
            set { this._trainingProgramName = value; }
        }

        private TrainingProgram LoadTrainingProgram()
        {
            if (this._trainingProgram == null)
            {
                this._trainingProgram = new TrainingProgram(this.TrainingProgramID);
            }
            return this._trainingProgram;
        }

        private void LoadCourse()
        {
            if (this._onlineCourse == null && this._classroomCourse == null)
            {
                if (this.CourseTypeID == (int)Symphony.Core.CourseType.Online)
                {
                    _onlineCourse = new OnlineCourse(this.CourseID);
                }
                else
                {
                    _classroomCourse = new Course(this.CourseID);
                }
            }
        }

        /// <summary>
        /// For notifications
        /// </summary>
        public string DueDay
        {
            get { return this.DueDate.HasValue ? this.DueDate.Value.ToShortDateString() : ""; }
        }

    }
}
