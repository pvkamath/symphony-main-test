﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;
using Symphony.Web;
using Symphony.Core.Controllers;
using System.Configuration;
using System.Web;

namespace Symphony.Core.Data
{
    public partial class ClassX
    {
        private bool _registeredStudent;
        //private bool _unregisteredStudent;
        //private string _Location = "";
        //private string _CourseName = "";
        private string _regStatus = "";
        private string _roomName = "";
        private string _instructor = "";
        private DateTime? _MinStartDateTime = null;
        private DateTime? _MaxStartDateTime = null;
        private DateTime? _MaxEndDateTime = null;
        private List<string> _types = new List<string>();

        ClassDateCollection dates = null;

        public bool IsRegisteredStudent
        {
            get { return _registeredStudent; }

            set { _registeredStudent = value; }
        }

        public string Location
        {
            get
            {
                if (IsVirtual)
                {
                    return "Online";
                }
                string location = "";
                if (Room != null)
                {
                    location += Room.Name;

                    Venue v = Room.Venue;
                    if (v != null)
                    {
                        location += ", " + v.Name + ", " + v.City;
                        if (v.State != null)
                        {
                            location += ", " + v.State.Name;
                        }
                    }
                }
                return location;
            }

            set { }
        }

        public string CourseName
        {
            get { return Course == null ? "" : Course.Name; }

            set { }
        }

        public List<string> Types
        {
            get { return _types; }

            set { _types = value; }
        }

        public string RoomName
        {
            get { return _roomName; }

            set { _roomName = value; }
        }

        public string Instructor
        {
            get { return _instructor; }

            set { _instructor = value; }
        }

        public string RegistrationStatus
        {
            get { return _regStatus; }

            set { _regStatus = value; }
        }

        public DateTime MinStartDateTime
        {
            get
            {
                if (_MinStartDateTime.HasValue)
                {
                    return _MinStartDateTime.Value;
                }
                else
                {
                    if (dates == null)
                    {
                        dates = new ClassDateCollection()
                            .Where(ClassDate.Columns.ClassID, this.Id)
                            .OrderByAsc(ClassDate.Columns.StartDateTime)
                            .Load();
                    }
                    if (dates.Count > 0)
                    {
                        _MinStartDateTime = dates.First().StartDateTime;
                    }
                    else
                    {
                        _MinStartDateTime = new DateTime(1900, 1, 1);
                    }
                }
                return _MinStartDateTime.Value;
            }

            set { _MinStartDateTime = value; }
        }

        public DateTime MaxStartDateTime
        {
            get
            {
                if (_MaxStartDateTime.HasValue)
                {
                    return _MaxStartDateTime.Value;
                }
                else
                {
                    if (dates == null)
                    {
                        dates = new ClassDateCollection()
                            .Where(ClassDate.Columns.ClassID, this.Id)
                            .OrderByAsc(ClassDate.Columns.StartDateTime)
                            .Load();
                    }
                    if (dates.Count > 0)
                    {
                        _MaxStartDateTime = dates.Last().StartDateTime;
                    }
                    else
                    {
                        _MaxStartDateTime = new DateTime(1900, 1, 1);
                    }
                }
                return _MaxStartDateTime.Value;
            }

            set { _MaxStartDateTime = value; }
        }

        public DateTime MaxEndDateTime
        {
            get
            {
                if (_MaxEndDateTime.HasValue)
                {
                    return _MaxEndDateTime.Value;
                }
                else
                {
                    if (dates == null)
                    {
                        dates = new ClassDateCollection()
                            .Where(ClassDate.Columns.ClassID, this.Id)
                            .OrderByAsc(ClassDate.Columns.StartDateTime)
                            .Load();
                    }
                    if (dates.Count > 0)
                    {
                        _MaxEndDateTime = dates.Last().StartDateTime.AddHours((double)dates.Last().Duration);
                    }
                    else
                    {
                        _MaxEndDateTime = new DateTime(1900, 1, 1);
                    }
                }
                return _MaxEndDateTime.Value;
            }

            set { _MaxEndDateTime = value; }
        }

        private ResourceCollection _resources = null;
        public ResourceCollection Resources
        {
            get
            {
                if (_resources == null)
                {
                    _resources = this.GetResourceCollection();
                }
                return _resources;
            }
        }

        public List<int> GetILTManagers()
        {
            return Controllers.UserController.GetUsersByRole(
                this.CustomerID, 0, Roles.ClassroomManager, "").Select(u => u.ID).ToList();
        }

        public List<int> GetAllRegistrantsSupervisors()
        {
            List<int> registrantIds = new Select(Data.Registration.RegistrantIDColumn)
                .From<Data.Registration>()
                .Where(Data.Registration.ClassIDColumn).IsEqualTo(this.Id)
                .ExecuteTypedList<int>();

            return User.GetSupervisors(registrantIds);
        }

        public List<int> GetAllRegistrants()
        {
            List<int> registrantIds = new Select(Data.Registration.RegistrantIDColumn)
                .From<Data.Registration>()
                .Where(Data.Registration.ClassIDColumn).IsEqualTo(this.Id)
                .ExecuteTypedList<int>();

            return registrantIds;
        }

        public List<int> GetAllRegistrantsReportingSupervisors()
        {
            List<int> registrantIds = new Select(Data.Registration.RegistrantIDColumn)
                .From<Data.Registration>()
                .Where(Data.Registration.ClassIDColumn).IsEqualTo(this.Id)
                .ExecuteTypedList<int>();

            return User.GetReportingSupervisors(registrantIds);
        }

        public string DirectRegistrationUrl
        {
            get
            {
                string baseUrl = ConfigurationManager.AppSettings["PortalBaseUrl"];
                baseUrl = Utilities.ParseCustomerUrl(baseUrl, new Customer(CustomerID), HttpContext.Current);
                // make sure the url never has a trailing slash
                baseUrl = baseUrl.TrimEnd('/');
                baseUrl += "#classroomtraining/registrations";

                return "<a href='" + baseUrl + "'>Click here for Course Registration Management</a>";
            }
        }

    }
}
