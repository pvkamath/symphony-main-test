﻿using Symphony.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core.Data
{
    public partial class Audience: IExportable
    {
        public string Export()
        {
            return string.Join(",", new string[]{
                DataHelper.Quote(Name),
                ParentAudience == null ? "" : ParentAudience.InternalCode,
                InternalCode
            });
        }

        public static string ExportHeader()
        {
            return string.Join(",", new string[]{
                "Name",
                "ParentCode",
                "Code"
            });
        }
    }
}
