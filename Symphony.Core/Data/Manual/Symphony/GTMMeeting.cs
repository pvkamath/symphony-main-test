﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Controllers;
using System.Configuration;
using System.Web;

namespace Symphony.Core.Data
{
    public partial class GTMMeeting
    {
        public DateTime StartDate
        {
            get
            {
                return ConvertTimeZone(DateTime.SpecifyKind(this.StartDateTime, DateTimeKind.Utc));
            }
        }

        public DateTime EndDate
        {
            get
            {
                return ConvertTimeZone(DateTime.SpecifyKind(this.EndDateTime, DateTimeKind.Utc));
            }
        }

        private DateTime ConvertTimeZone(DateTime dt)
        {
            switch (this.TimeZoneKey)
            {
                case GTMController.TimeZoneEastern:
                    return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt, "Eastern Standard Time");
                case GTMController.TimeZoneAtlantic:
                    return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt, "Atlantic Standard Time");
                case GTMController.TimeZoneMountain:
                    return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt, "Mountain Standard Time");
                case GTMController.TimeZoneCentral:
                    return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt, "Central Standard Time");
                case GTMController.TimeZonePacific:
                    return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt, "Pacific Standard Time");
                case GTMController.TimeZoneAlaska:
                    return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt, "Alaska Standard Time");
                case GTMController.TimeZoneHawaii:
                    return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt, "Hawaiian Standard Time");
            }
            return dt;
        }

        public string ExternalJoinURL
        {
            get
            {
                string target = ConfigurationManager.AppSettings["PortalDomain"];
                if (string.IsNullOrEmpty(target))
                {
                    target = "http://portal.betraining.com/";
                }
                string customer = string.Empty;
                if (HttpContext.Current != null && HttpContext.Current.Session != null)
                {
                    customer = new SymphonyController().ApplicationName;
                }
                else
                {
                    var gtmorganizer = new GTMOrganizer(this.GTMOrganizerID);
                    var user = new User(gtmorganizer.UserID);
                    if (user.Id > 0 && user.Customer != null)
                    {
                        customer = user.Customer.SubDomain;
                    }
                }

                if (!target.EndsWith("/"))
                {
                    target += "/";
                }
                //tack on the site & customer id
                target += "site/?customer=" + customer;
                return target;
            }
        }
    }
}
