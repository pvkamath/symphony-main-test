﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core.Data
{
    public partial class Template
    {
        private bool? _isEnabledByTrainingProgram = null;

        /// <summary>
        /// WARNING - CAN CREATE DB QUERY!
        /// 
        /// Returns true if there are associated training programs for this template
        /// This value can be set if included in the original query used to load the templates. 
        /// </summary>
        public bool IsEnabledByTrainingProgram
        {
            get
            {
                if (!_isEnabledByTrainingProgram.HasValue)
                {
                    _isEnabledByTrainingProgram = this.TrainingProgramTemplates.Where(t => t.TrainingProgram.IsCustomNotificationsEnabled == true).Count() > 0;
                }

                return _isEnabledByTrainingProgram.Value;
            }
            set
            {
                _isEnabledByTrainingProgram = value;
            }
        }
    }
}
