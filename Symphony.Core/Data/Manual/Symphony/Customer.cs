﻿using Symphony.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Symphony.Core.Data
{
    public partial class Customer : IAssociatedImage
    {
        User _customerCareRep;
        User _accountExecutive;
        Location _location;

        public List<ExternalSystem> ExternalSystems
        {
            get
            {
                return CustomerExternalSystems.Select(e => e.ExternalSystem).OrderByDescending(t => t.IsRequired).ToList();
            }
        }

        public User CustomerCareRepUser
        {
            get
            {
                if (_customerCareRep == null) {
                    _customerCareRep = new User(CustomerCareRep);
                }
                return _customerCareRep;
            }
        }

        public User AccountExecutiveUser
        {
            get
            {
                if (_accountExecutive == null)
                {
                    _accountExecutive = new User(AccountExecutive);
                }
                return _accountExecutive;
            }
        }

        public Location Location
        {
            get
            {
                if (_location == null)
                {
                    _location = new Location(LocationID);
                }
                return _location;
            }
        }

        public string BaseUrl
        {
            get
            {
                return HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "");
            }
        }

        public string LoginUrl
        {
            get
            {
                return string.Format("{0}/login/{1}", BaseUrl, SubDomain);
            }
        }

        public string LogoData
        {
            get
            {
                SalesChannel sc = new SalesChannel(this.SalesChannelID);
                return !string.IsNullOrEmpty(AssociatedImageData) ? this.AssociatedImageData : sc.Logo;
            }
        }

        public string LogoHtml
        {
            get
            {
                SalesChannel sc = new SalesChannel(this.SalesChannelID);
                var customerLogo = !string.IsNullOrEmpty(AssociatedImageData) ? this.AssociatedImageData : sc.Logo;
                customerLogo = string.Format("<img src='{0}' />", customerLogo);
                return customerLogo;
            }
        }


    }
}
