﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;
using Symphony.Core.Extensions;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Data
{
    public partial class User: IAssociatedImage
    {
        /// <summary>
        /// The user's status description
        /// </summary>
        public string Status
        {
            get
            {
                if (this.UserStatus != null)
                    return this.UserStatus.Description;
                return "";
            }
        }

        // for notifications
        public string HireDay
        {
            get { return this.HireDate.HasValue ? this.HireDate.Value.ToShortDateString() : "[No date specified]"; }
        }

        public string FullName
        {
            get
            {
                return Models.Model.FormatFullName(FirstName, MiddleName, LastName);
            }
        }

        private AudienceCollection _audiences = null;
        public AudienceCollection Audiences
        {
            get
            {
                if (_audiences == null)
                {
                    _audiences = this.GetAudienceCollection();
                }
                return _audiences;
            }
        }

        /// <summary>
        /// The customer managers for this customer
        /// </summary>
        /// <returns></returns>
        public List<int> GetCustomerManagers()
        {
            return new SearchableUserCollection()
                .Where(SearchableUser.Columns.CanManageCustomers, true)
                .Where(SearchableUser.Columns.CustomerID, this.CustomerID)
                .Load()
                .Select(m => m.Id).ToList();
        }

        public List<int> GetSupervisors()
        {
            return new List<int>() { this.SupervisorID, this.SecondarySupervisorID };
        }
        
        public static List<int> GetSupervisors(List<int> userIds)
        {
            // In case we pass in too many users
            IEnumerable<int[]> userIdGroups = userIds.InGroupsOf(2000);
            List<Data.User> users = new List<User>();
            foreach (int[] userIdGroup in userIdGroups)
            {
                users.AddRange(Select.AllColumnsFrom<Data.User>()
                    .Where(Data.User.Columns.Id).In(userIdGroup)
                    .ExecuteTypedList<Data.User>());
            }

            List<int> supervisors = users.Select(c => c.SupervisorID).Where(i => i > 0).ToList()
                                    .Union(users.Select(c => c.SecondarySupervisorID).Where(i => i > 0).ToList())
                                    .Distinct()
                                    .ToList();

            return supervisors;
        }

        public static List<int> GetReportingSupervisors(List<int> userIds)
        {
            // Only get reporting supervisors 2000 at a time
            IEnumerable<int[]> userIdGroups = userIds.InGroupsOf(2000);
            List<int> supervisors = new List<int>();
            foreach (int[] userIdGroup in userIdGroups)
            {
                supervisors.AddRange(new Select(Data.User.ReportingSupervisorIDColumn)
                    .From<Data.User>()
                    .Where(Data.User.Columns.Id).In(userIdGroup)
                    .And(Data.User.Columns.ReportingSupervisorID).IsNotEqualTo(0)
                    .Distinct()
                    .ExecuteTypedList<int>());
            }

            supervisors = supervisors.Distinct().ToList();

            return supervisors;
        }

    }
}
