﻿using Symphony.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core.Data
{
    public partial class Location: IExportable
    {
        public string Export()
        {
            return string.Join(",", new string[]{
                DataHelper.Quote(Name),
                ParentLocation == null ? "" : ParentLocation.InternalCode,
                InternalCode,
                DataHelper.Quote(Address1),
                DataHelper.Quote(Address2),
                DataHelper.Quote(City),
                DataHelper.Quote(State),
                DataHelper.Quote(PostalCode),
                "",
                DataHelper.Quote(TelephoneNbr)
            });
        }

        public static string ExportHeader()
        {
            return string.Join(",", new string[]{
                "Name",
                "ParentCode",
                "Code",
                "Address1",
                "Address2",
                "City",
                "State",
                "PostalCode",
                "MailStop",
                "TelephoneNbr"
            });
        }
    }
}
