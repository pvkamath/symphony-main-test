﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;
using Symphony.Core.Extensions;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Data
{
    public partial class UserLastActivity
    {
        public string FullName
        {
            get
            {
                return Models.Model.FormatFullName(FirstName, MiddleName, LastName);
            }
        }
    }
}
