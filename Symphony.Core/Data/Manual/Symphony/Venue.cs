﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core.Data
{
    public partial class Venue
    {
        public string StateName
        {
            get { return (State == null ? "" : State.Name); }
        }
    }
}
