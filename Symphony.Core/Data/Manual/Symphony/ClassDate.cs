﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Symphony.Core.Data
{
    public partial class ClassDate
    {
        public string StartDay
        {
            get { return GetStartDateTimeWithOffset().ToShortDateString(); }
        }

        // this is a leftover; it's used in a bunch of notification templates, so we're leaving it, but don't use it in the future
        [Obsolete]
        public string StartDate
        {
            get { return StartDay; }
        }

        public string StartTime
        {
            get
            {
                string timezone;
                DateTime dt = GetStartDateTimeWithOffset(out timezone);
                return dt.ToShortTimeString() + " (" + timezone + ")";
            }
        }

        public DateTime GetStartDateTimeWithOffset()
        {
            string unused;
            return GetStartDateTimeWithOffset(out unused);
        }

        /// <summary>
        /// Looks at the venue for this classdate's class, and determines based on the timezone what the *actual* start time is
        /// </summary>
        public DateTime GetStartDateTimeWithOffset(out string timezone)
        {
            TimeZoneInfo tzi;

            if (this.ClassX != null && this.ClassX.Room != null && this.ClassX.Room.Venue != null && this.ClassX.Room.Venue.TimeZone != null)
            {
                TimeZone tz = this.ClassX.Room.Venue.TimeZone;
                tzi = TimeZoneInfo.FindSystemTimeZoneById(tz.Description + " Standard Time");
            }
            else { // if there is no venue timezone, use the customer's default timezone, which will be UTC if not specified in the admin area
                tzi = TimeZoneInfo.FindSystemTimeZoneById(this.Customer.TimeZone);
            }

            DateTime dt = TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(this.StartDateTime, DateTimeKind.Utc), tzi);
            timezone = tzi.IsDaylightSavingTime(dt) ? tzi.DaylightName : tzi.StandardName;
            return dt;
        }

        /// <summary>
        /// For notifications
        /// </summary>
        public int DurationMinutes
        {
            get { return this.Duration.HasValue ? this.Duration.Value : 0; }
        }

        /// <summary>
        /// For notifications
        /// </summary>
        public string DurationHours
        {
            get { return (this.Duration.HasValue ? (this.Duration.Value / 60f).ToString("f2") : "0"); }
        }

        /// <summary>
        /// For notifications
        /// </summary>
        public string DurationText
        {
            get 
            {
                if (!this.Duration.HasValue)
                {
                    return "unknown";
                }

                var duration = TimeSpan.FromMinutes(this.Duration.Value);
                return string.Format("{0} hour{1} {2} minute{3}", 
                    duration.Hours, 
                    duration.Hours > 1 ? "s" : "",
                    duration.Minutes,
                    duration.Minutes > 1 ? "s" : "");
            }
        }
    }
}
