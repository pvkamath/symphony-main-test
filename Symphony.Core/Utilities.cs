﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Checksums;
using System.Collections;
using System.Web;
using Aspose.Words;
using Aspose.Slides;
using Pdf = Aspose.Pdf;
using System.Drawing;
using Aspose.Pdf.Facades;
using System.Configuration;
using Aspose.Slides.Pptx;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using System.Data;
using Symphony.Core.ThirdParty.Microsoft;
using System.Xml;
using System.Xml.Xsl;
using System.Reflection;
using System.Xml.Serialization;
using System.Transactions;
using ps = PdfSharp.Pdf;
using psIo = PdfSharp.Pdf.IO;


namespace Symphony.Core
{
    public class Utilities
    {
        /// <summary>
        /// Converts a list of models to a dataset, filtering out the properties not allowed according to the permission level.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static DataSet ConvertToDataSet<T>(IEnumerable<T> list, List<string> excludeProperties = null) where T : new()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ds.Tables.Add(dt);

            if (list.Count() == 0)
            {
                return ds;
            }

            PropertyInfo[] properties = typeof(T).GetProperties();
            if (excludeProperties != null)
            {
                excludeProperties = excludeProperties.Select(s => s.ToLower()).ToList();
            }

            foreach (PropertyInfo pi in properties)
            {
                if (excludeProperties != null && excludeProperties.Contains(pi.Name.ToLower()))
                {
                    continue;
                }

                // we only include properties with datamember definitions
                object[] attributes = pi.GetCustomAttributes(typeof(DataMemberAttribute), false);
                if (attributes.Count() > 0)
                {
                    dt.Columns.Add(new DataColumn()
                    {
                        ColumnName = pi.Name,
                        DataType = pi.PropertyType
                    });
                }
            }

            foreach (T t in list)
            {
                
                DataRow dr = dt.NewRow();
                foreach (PropertyInfo pi in properties)
                {
                    if (!dt.Columns.Contains(pi.Name))
                    {
                        continue;
                    }
                    dr[pi.Name] = pi.GetValue(t, null);
                }
                dt.Rows.Add(dr);
            }

            return ds;
        }

        /// <summary>
        /// Creates a valid URL for this customer
        /// </summary>
        /// <param name="url"></param>
        /// <param name="customer"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string ParseCustomerUrl(string url, Data.Customer customer, HttpContext context)
        {
            if (context == null)
            {
                return url.Replace("{customer.subdomain}", customer.SubDomain);
            }

            return url
                .Replace("{customer.subdomain}", customer.SubDomain)
                .Replace("{protocol}", context == null ? customer.DefaultProtocol : (context.Request.IsSecureConnection ? "https" : "http"))
                .Replace("{authority}", context == null ? customer.DefaultAuthority : (context.Request.Url.Authority));
        }

        /// <summary>
        /// Determines if 2 time ranges overlap
        /// </summary>
        /// <param name="dtStart1"></param>
        /// <param name="dtEnd1"></param>
        /// <param name="dtStart2"></param>
        /// <param name="dtEnd2"></param>
        /// <returns></returns>
        public static bool Overlaps(DateTime dtStart1, DateTime dtEnd1, DateTime dtStart2, DateTime dtEnd2)
        {
            if ((dtStart1 <= dtEnd2) && (dtStart2 <= dtEnd1))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Converts url parameters in a CSS sheet to fully qualified paths
        /// </summary>
        /// <param name="style">The CSS to analyse</param>
        /// <param name="domain">The base domain</param>
        /// <param name="imageFolderPath">The root folder for images with no path specified</param>
        /// <returns>The modified CSS</returns>
        public static string RebaseCSS(string style, string domain, string imageFolderPath)
        {
            // Don't find urls that start with 'http, http, 'data:, or data:
            return Regex.Replace(style, @"url\((?!'?(data:|http))(.+)\)", (args) =>
            {
                string path = args.Groups[2].Value;

                if (path.StartsWith("/"))
                {
                    path = path.Substring(1);
                }
                else
                {
                    // strip off the ~/ and prepend it to the path;
                    // CSS sheet urls are either absolute (see above) or
                    // relative to the CSS sheet itself (in which case, we can build up
                    // and absolute path here)
                    if (imageFolderPath.StartsWith("~/"))
                    {
                        path = imageFolderPath.Substring(2) + path;
                    }
                    else if (imageFolderPath.StartsWith("/"))
                    {
                        path = imageFolderPath.Substring(1) + path;
                    }
                    else
                    {
                        path = imageFolderPath + path;
                    }
                }
                return "url(" + domain + path + ")";
            });
        }

        public static string GetString(MemoryStream ms)
        {
            using (StreamReader sr = new StreamReader(ms))
            {
                ms.Position = 0;
                return sr.ReadToEnd();
            }
        }

        /// <summary>
        /// Recursively copies a directory, overwriting anything in the destination folder as needed; does *not* copy dot-files (like .svn)
        /// </summary>
        /// <param name="sourceFolder"></param>
        /// <param name="destFolder"></param>
        public static void CopyFolder(string sourceFolder, string destFolder)
        {
            if (Path.GetFileName(destFolder).StartsWith("."))
            {
                return;
            }

            if (!Directory.Exists(destFolder))
                Directory.CreateDirectory(destFolder);
            string[] files = Directory.GetFiles(sourceFolder);
            foreach (string file in files)
            {
                string name = Path.GetFileName(file);
                string dest = Path.Combine(destFolder, name);
                File.Copy(file, dest, true);
                // make sure the file shows a change in it's write time
                File.SetLastWriteTime(dest, DateTime.Now);
            }
            string[] folders = Directory.GetDirectories(sourceFolder);
            foreach (string folder in folders)
            {
                string name = Path.GetFileName(folder);
                string dest = Path.Combine(destFolder, name);
                CopyFolder(folder, dest);
            }
        }

        public class RijndaelSimple
        {

            private static string passPhrase = "b@Nk3rs3dg3";        // can be any string
            private static string saltValue = "nthp3nguin";        // can be any string
            private static string hashAlgorithm = "SHA1";             // can be "MD5"
            private static int passwordIterations = 2;                  // can be any number
            private static string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
            private static int keySize = 256;                // can be 192 or 128

            /// <summary>
            /// Encrypts a string with pre-set values for salts, passphrasem iterations, initVector, algorithm, and keysize.
            /// </summary>
            /// <param name="Text"></param>
            /// <returns></returns>
            public static string Encrypt(string plainText)
            {
                return Encrypt(plainText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
            }

            /// <summary>
            /// Decrypts a string with pre-set values for salts, passphrasem iterations, initVector, algorithm, and keysize.
            /// </summary>
            /// <param name="Text"></param>
            /// <returns></returns>
            public static string Decrypt(string cipherText)
            {
                return Decrypt(cipherText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
            }

            /// <summary>
            /// Encrypts specified plaintext using Rijndael symmetric key algorithm
            /// and returns a base64-encoded result.
            /// </summary>
            /// <param name="plainText">
            /// Plaintext value to be encrypted.
            /// </param>
            /// <param name="passPhrase">
            /// Passphrase from which a pseudo-random password will be derived. The
            /// derived password will be used to generate the encryption key.
            /// Passphrase can be any string. In this example we assume that this
            /// passphrase is an ASCII string.
            /// </param>
            /// <param name="saltValue">
            /// Salt value used along with passphrase to generate password. Salt can
            /// be any string. In this example we assume that salt is an ASCII string.
            /// </param>
            /// <param name="hashAlgorithm">
            /// Hash algorithm used to generate password. Allowed values are: "MD5" and
            /// "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
            /// </param>
            /// <param name="passwordIterations">
            /// Number of iterations used to generate password. One or two iterations
            /// should be enough.
            /// </param>
            /// <param name="initVector">
            /// Initialization vector (or IV). This value is required to encrypt the
            /// first block of plaintext data. For RijndaelManaged class IV must be 
            /// exactly 16 ASCII characters long.
            /// </param>
            /// <param name="keySize">
            /// Size of encryption key in bits. Allowed values are: 128, 192, and 256. 
            /// Longer keys are more secure than shorter keys.
            /// </param>
            /// <returns>
            /// Encrypted value formatted as a base64-encoded string.
            /// </returns>
            public static string Encrypt(string plainText,
                                         string passPhrase,
                                         string saltValue,
                                         string hashAlgorithm,
                                         int passwordIterations,
                                         string initVector,
                                         int keySize)
            {
                // Convert strings into byte arrays.
                // Let us assume that strings only contain ASCII codes.
                // If strings include Unicode characters, use Unicode, UTF7, or UTF8 
                // encoding.
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

                // Convert our plaintext into a byte array.
                // Let us assume that plaintext contains UTF8-encoded characters.
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

                // First, we must create a password, from which the key will be derived.
                // This password will be generated from the specified passphrase and 
                // salt value. The password will be created using the specified hash 
                // algorithm. Password creation can be done in several iterations.
                PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                                passPhrase,
                                                                saltValueBytes,
                                                                hashAlgorithm,
                                                                passwordIterations);

                // Use the password to generate pseudo-random bytes for the encryption
                // key. Specify the size of the key in bytes (instead of bits).
                byte[] keyBytes = password.GetBytes(keySize / 8);

                // Create uninitialized Rijndael encryption object.
                RijndaelManaged symmetricKey = new RijndaelManaged();

                // It is reasonable to set encryption mode to Cipher Block Chaining
                // (CBC). Use default options for other symmetric key parameters.
                symmetricKey.Mode = CipherMode.CBC;

                // Generate encryptor from the existing key bytes and initialization 
                // vector. Key size will be defined based on the number of the key 
                // bytes.
                ICryptoTransform encryptor = symmetricKey.CreateEncryptor(
                                                                 keyBytes,
                                                                 initVectorBytes);

                // Define memory stream which will be used to hold encrypted data.
                MemoryStream memoryStream = new MemoryStream();

                // Define cryptographic stream (always use Write mode for encryption).
                CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                             encryptor,
                                                             CryptoStreamMode.Write);
                // Start encrypting.
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

                // Finish encrypting.
                cryptoStream.FlushFinalBlock();

                // Convert our encrypted data from a memory stream into a byte array.
                byte[] cipherTextBytes = memoryStream.ToArray();

                // Close both streams.
                memoryStream.Close();
                cryptoStream.Close();

                // Convert encrypted data into a base64-encoded string.
                string cipherText = Convert.ToBase64String(cipherTextBytes);

                // Return encrypted string.
                return cipherText;
            }

            /// <summary>
            /// Decrypts specified ciphertext using Rijndael symmetric key algorithm.
            /// </summary>
            /// <param name="cipherText">
            /// Base64-formatted ciphertext value.
            /// </param>
            /// <param name="passPhrase">
            /// Passphrase from which a pseudo-random password will be derived. The
            /// derived password will be used to generate the encryption key.
            /// Passphrase can be any string. In this example we assume that this
            /// passphrase is an ASCII string.
            /// </param>
            /// <param name="saltValue">
            /// Salt value used along with passphrase to generate password. Salt can
            /// be any string. In this example we assume that salt is an ASCII string.
            /// </param>
            /// <param name="hashAlgorithm">
            /// Hash algorithm used to generate password. Allowed values are: "MD5" and
            /// "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
            /// </param>
            /// <param name="passwordIterations">
            /// Number of iterations used to generate password. One or two iterations
            /// should be enough.
            /// </param>
            /// <param name="initVector">
            /// Initialization vector (or IV). This value is required to encrypt the
            /// first block of plaintext data. For RijndaelManaged class IV must be
            /// exactly 16 ASCII characters long.
            /// </param>
            /// <param name="keySize">
            /// Size of encryption key in bits. Allowed values are: 128, 192, and 256.
            /// Longer keys are more secure than shorter keys.
            /// </param>
            /// <returns>
            /// Decrypted string value.
            /// </returns>
            /// <remarks>
            /// Most of the logic in this function is similar to the Encrypt
            /// logic. In order for decryption to work, all parameters of this function
            /// - except cipherText value - must match the corresponding parameters of
            /// the Encrypt function which was called to generate the
            /// ciphertext.
            /// </remarks>
            public static string Decrypt(string cipherText,
                                         string passPhrase,
                                         string saltValue,
                                         string hashAlgorithm,
                                         int passwordIterations,
                                         string initVector,
                                         int keySize)
            {
                // Convert strings defining encryption key characteristics into byte
                // arrays. Let us assume that strings only contain ASCII codes.
                // If strings include Unicode characters, use Unicode, UTF7, or UTF8
                // encoding.
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

                // Convert our ciphertext into a byte array.
                byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

                // First, we must create a password, from which the key will be 
                // derived. This password will be generated from the specified 
                // passphrase and salt value. The password will be created using
                // the specified hash algorithm. Password creation can be done in
                // several iterations.
                PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                                passPhrase,
                                                                saltValueBytes,
                                                                hashAlgorithm,
                                                                passwordIterations);

                // Use the password to generate pseudo-random bytes for the encryption
                // key. Specify the size of the key in bytes (instead of bits).
                byte[] keyBytes = password.GetBytes(keySize / 8);

                // Create uninitialized Rijndael encryption object.
                RijndaelManaged symmetricKey = new RijndaelManaged();

                // It is reasonable to set encryption mode to Cipher Block Chaining
                // (CBC). Use default options for other symmetric key parameters.
                symmetricKey.Mode = CipherMode.CBC;

                // Generate decryptor from the existing key bytes and initialization 
                // vector. Key size will be defined based on the number of the key 
                // bytes.
                ICryptoTransform decryptor = symmetricKey.CreateDecryptor(
                                                                 keyBytes,
                                                                 initVectorBytes);

                // Define memory stream which will be used to hold encrypted data.
                MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

                // Define cryptographic stream (always use Read mode for encryption).
                CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                              decryptor,
                                                              CryptoStreamMode.Read);

                // Since at this point we don't know what the size of decrypted data
                // will be, allocate the buffer long enough to hold ciphertext;
                // plaintext is never longer than ciphertext.
                byte[] plainTextBytes = new byte[cipherTextBytes.Length];

                // Start decrypting.
                int decryptedByteCount = cryptoStream.Read(plainTextBytes,
                                                           0,
                                                           plainTextBytes.Length);

                // Close both streams.
                memoryStream.Close();
                cryptoStream.Close();

                // Convert decrypted data into a string. 
                // Let us assume that the original plaintext string was UTF8-encoded.
                string plainText = Encoding.UTF8.GetString(plainTextBytes,
                                                           0,
                                                           decryptedByteCount);

                // Return decrypted string.   
                return plainText;
            }
        }

        public static string GetSortOrder(Type T)
        {
            string propertyName = string.Empty;
            object[] attributes = T.GetCustomAttributes(typeof(DefaultSortAttribute), false);
            foreach (object o in attributes)
            {
                DefaultSortAttribute attribute = (DefaultSortAttribute)o;
                propertyName = attribute.ColumnName;
                break;
            }
            if (string.IsNullOrEmpty(propertyName))
            {
                propertyName = "ID";
            }
            return propertyName;
        }

        public static OrderDirection GetOrderDirection(Type T)
        {
            string propertyName = string.Empty;
            object[] attributes = T.GetCustomAttributes(typeof(DefaultSortAttribute), false);
            foreach (object o in attributes)
            {
                DefaultSortAttribute attribute = (DefaultSortAttribute)o;
                return attribute.Direction;
            }
            return OrderDirection.Ascending;
        }

        public static T Deserialize<T>(string json)
        {
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
                return (T)serializer.ReadObject(ms);
            }
        }

        public static string Serialize(object o)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(o.GetType());
                serializer.WriteObject(ms, o);
                byte[] bytes = ms.ToArray();
                return Encoding.UTF8.GetString(bytes, 0, bytes.Length);
            }
        }

        private static void ZipFiles(ZipOutputStream zstream, Crc32 crc, string baseDirectory, string subDirectory, bool recurse)
        {
            string[] filenames = Directory.GetFiles(Path.Combine(baseDirectory, subDirectory));
            string[] directories = Directory.GetDirectories(Path.Combine(baseDirectory, subDirectory));

            foreach (string filename in filenames)
            {
                if (Path.GetExtension(filename).ToLower() == ".zip")
                    continue;

                FileStream fs = File.OpenRead(filename);
                try
                {
                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, buffer.Length);
                    ZipEntry entry = new ZipEntry(Path.Combine(subDirectory, Path.GetFileName(filename)));
                    entry.DateTime = DateTime.Now;
                    entry.Size = fs.Length;
                    fs.Close();
                    crc.Reset();
                    crc.Update(buffer);
                    zstream.PutNextEntry(entry);
                    zstream.Write(buffer, 0, buffer.Length);
                }
                finally
                {
                    fs.Close();
                }
            }

            if (recurse)
            {
                foreach (string directory in directories)
                {
                    if (directory.IndexOf(".svn") > -1) continue;

                    DirectoryInfo dirInfo = new DirectoryInfo(directory);
                    ZipFiles(zstream, crc, baseDirectory, Path.Combine(subDirectory, dirInfo.Name), recurse);
                }
            }
        }

        public static void UnZip(string archiveFilename, string outputFolder)
        {
            using (ZipInputStream s = new ZipInputStream(File.OpenRead(archiveFilename)))
            {
                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null)
                {
                    string directoryName = Path.Combine(outputFolder, Path.GetDirectoryName(theEntry.Name));
                    string fileName = Path.GetFileName(theEntry.Name);
                    string path = Path.Combine(directoryName, fileName);

                    // create directory
                    if (directoryName.Length > 0)
                    {
                        Directory.CreateDirectory(directoryName);
                    }

                    if (fileName != String.Empty)
                    {
                        using (FileStream streamWriter = File.Create(path))
                        {

                            int size = 2048;
                            byte[] data = new byte[2048];
                            while (true)
                            {
                                size = s.Read(data, 0, data.Length);
                                if (size > 0)
                                {
                                    streamWriter.Write(data, 0, size);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        public static string Zip(string directory, string archiveFilename)
        {
            Crc32 crc = new Crc32();
            ZipOutputStream zstream = new ZipOutputStream(File.Create(Path.Combine(directory, archiveFilename)));
            zstream.SetLevel(9);
            Hashtable fileHistory = new Hashtable();

            try
            {
                //add the xml schema files and other shared files
                //CopyDir(sharedDirectory, directory);
                // Zip files from the root directory
                ZipFiles(zstream, crc, directory, string.Empty, true);
                // add the xml schema files and other shared files
                //ZipFiles(zstream, crc, sharedDirectory, string.Empty, true);
            }
            finally
            {
                zstream.Finish();
                zstream.Close();
            }

            return archiveFilename;

        } // end of createArchive

        /// <summary>

        /// Returns a site relative HTTP path from a partial path starting out with a ~.

        /// Same syntax that ASP.Net internally supports but this method can be used

        /// outside of the Page framework.

        /// 

        /// Works like Control.ResolveUrl including support for ~ syntax

        /// but returns an absolute URL.

        /// </summary>

        /// <param name="originalUrl">Any Url including those starting with ~</param>

        /// <returns>relative url</returns>

        public static string ResolveUrl(string originalUrl)
        {

            if (originalUrl == null)

                return null;



            // *** Absolute path - just return

            if (originalUrl.IndexOf("://") != -1)

                return originalUrl;



            // *** Fix up image path for ~ root app dir directory

            if (originalUrl.StartsWith("~"))
            {

                string newUrl = "";

                if (HttpContext.Current != null)

                    newUrl = (HttpContext.Current.Request.ApplicationPath +

                          originalUrl.Substring(1)).Replace("//", "/");

                else

                    // *** Not context: assume current directory is the base directory

                    throw new ArgumentException("Invalid URL: Relative URL not allowed.");



                // *** Just to be sure fix up any double slashes

                return newUrl;

            }



            return originalUrl;

        }

        /// <summary>
        /// Converts a file path to a server url
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ReverseMapPath(string path)
        {
            string appPath = HttpContext.Current.Server.MapPath("~");
            string res = string.Format("~{0}", path.Replace(appPath, "").Replace("\\", "/"));
            return res;
        }

        private static bool Licensed = false;
        
        public static void LicenseAsposeByName()
        {
            if (!Licensed)
            {
                Licensed = true;

                Aspose.Words.License words = new Aspose.Words.License();
                Aspose.Slides.License slides = new Aspose.Slides.License();
                Aspose.Pdf.License pdf = new Aspose.Pdf.License();

                words.SetLicense("Symphony.Core.Licenses.Aspose.Words.lic");
                slides.SetLicense("Symphony.Core.Licenses.Aspose.Slides.lic");
                pdf.SetLicense("Symphony.Core.Licenses.Aspose.Pdf.lic");
            }
        }
        public static void LicenseAspose()
        {
            if (!Licensed)
            {
                Stream s1 = typeof(Utilities).Assembly.GetManifestResourceStream("Symphony.Core.Licenses.Aspose.Words.lic");
                Aspose.Words.License l1 = new Aspose.Words.License();
                l1.SetLicense(s1);

                Stream s2 = typeof(Utilities).Assembly.GetManifestResourceStream("Symphony.Core.Licenses.Aspose.Pdf.lic");
                Aspose.Pdf.License l2 = new Aspose.Pdf.License();
                l2.SetLicense(s2);

                Stream s3 = typeof(Utilities).Assembly.GetManifestResourceStream("Symphony.Core.Licenses.Aspose.Slides.lic");
                Aspose.Slides.License l3 = new Aspose.Slides.License();
                l3.SetLicense(s3);
            }
        }

        private static Size _size;
        private static Size GetDefaultImportPageSize()
        {
            if (_size == null)
            {
                string w = ConfigurationManager.AppSettings["ImportedPageImageWidth"] ?? "500";
                string h = ConfigurationManager.AppSettings["ImportedPageImageHeight"] ?? "600";
                _size = new Size()
                {
                    Width = int.Parse(w),
                    Height = int.Parse(h)
                };
            }
            return _size;
        }

        public static void ConvertPowerPointToPng(Stream ppt, string outputDirectory)
        {
            if (ppt.Length > 0)
            {
                LicenseAspose();

                string tempFile = Path.GetTempFileName();
                Presentation pres = new Presentation(ppt);
                int i = 1;
                foreach (Slide slide in pres.Slides)
                {
                    Image img = slide.GetThumbnail(1, 1);
                    string filename = i.ToString().PadLeft(3, '0') + ".jpg";
                    img.Save(Path.Combine(outputDirectory, filename), ImageFormat.Jpeg);
                    i++;
                }
                ppt.Close();
            }
        }

        public static void ConvertPowerPointXToPng(Stream ppt, string outputDirectory)
        {
            if (ppt.Length > 0)
            {
                LicenseAspose();

                string tempFile = Path.GetTempFileName();
                PresentationEx pres = new PresentationEx(ppt);
                int i = 1;
                foreach (SlideEx slide in pres.Slides)
                {
                    Image img = slide.GetThumbnail(1, 1);
                    string filename = i.ToString().PadLeft(3, '0') + ".jpg";
                    img.Save(Path.Combine(outputDirectory, filename), ImageFormat.Jpeg);
                    i++;
                }
                ppt.Close();
            }
        }

        public static void ConvertWordToHtml(Stream doc, string outputDirectory)
        {
            if (doc.Length > 0)
            {
                LicenseAspose();

                Document d = new Document();
                using (Stream html = File.OpenWrite(Path.GetTempFileName()))
                {
                    d.Save(html, SaveFormat.Html);
                    doc.Close();
                }

            }
        }

        public static void ConvertWordToPng(Stream doc, string outputDirectory)
        {
            if (doc.Length > 0)
            {
                LicenseAspose();
                Document d = new Document(doc);
                int pageCount = d.PageCount;
                float scale = 1.0f;
                float res = 200.0f;
                d.PageColor = System.Drawing.Color.White;

                for (int i = 0; i < pageCount; i++)
                {
                    Aspose.Words.Rendering.PageInfo info = d.GetPageInfo(i);
                    Size pageSize = info.GetSizeInPixels(scale, res);
                    Bitmap img = new Bitmap(pageSize.Width, pageSize.Height);
                    d.RenderToSize(i, Graphics.FromImage(img), 0, 0, pageSize.Width, pageSize.Height);
                    string filename = i.ToString().PadLeft(3, '0') + ".png";
                    img.Save(outputDirectory + "/" + filename);
                }
                doc.Close();
            }
        }

        public static void ConvertPdfToMultiplePdfPages(Stream pdf, string outputDirectory)
        {
            MemoryStream pdfMemStream = new MemoryStream();
            pdf.CopyTo(pdfMemStream);
            // Open the file
            ps.PdfDocument inputDocument = psIo.CompatiblePdfReader.Open(pdfMemStream);
            
            for (int idx = 0; idx < inputDocument.PageCount; idx++)
            {
                string filename = (idx+1).ToString().PadLeft(3, '0') + ".pdf";
                string filePath = Path.Combine(outputDirectory, filename);
                
                // Create new document
                ps.PdfDocument outputDocument = new ps.PdfDocument();
        
                // Add the page and save it
                outputDocument.AddPage(inputDocument.Pages[idx]);
                outputDocument.Save(filePath);
            }
        }

        public static void ConvertPdfToPng(Stream pdf, string outputDirectory)
        {
            if (pdf.Length > 0)
            {
                LicenseAspose();

                PdfConverter c = new PdfConverter();
                c.BindPdf(pdf);
                c.DoConvert();

                int count = 0;

                while (c.HasNextImage())
                {
                    string filename = (++count).ToString().PadLeft(3, '0') + ".png";
                    c.GetNextImage(outputDirectory + "/" + filename, System.Drawing.Imaging.ImageFormat.Png);
                }

                c.Close();
                pdf.Close();
            }
        }

        public static T Clone<T>(T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        public static string Base64UrlEncode(string str)
        {
            byte[] strBytes = Encoding.Unicode.GetBytes(str);
            string result = Convert.ToBase64String(strBytes);
            result = result.Replace('+', '_').Replace('/', '-').Replace('=', '~');
            return result;
        }

        public static string Base64UrlDecode(string str)
        {
            str = str.Replace('_', '+').Replace('-', '/').Replace('~', '=');
            byte[] strBytes = Convert.FromBase64String(str);
            string result = Encoding.Unicode.GetString(strBytes);
            return result;
        }

        public static DaysOfWeek DaysToFlags(int[] days)
        {
            DaysOfWeek scheduleDaysOfWeek = DaysOfWeek.None;

            foreach (int day in days)
            {
                // Translate from enum to flag, and set flag
                string dayName = Enum.GetName(typeof(DayOfWeek), day);
                var dayFlag = (DaysOfWeek)Enum.Parse(typeof(DaysOfWeek), dayName, true);
                scheduleDaysOfWeek |= dayFlag;
            }

            return scheduleDaysOfWeek;
        }

        public static int[] FlagsToDays(DaysOfWeek flags)
        {
            if (flags == DaysOfWeek.None)
            {
                return new int[] { };
            }

            return flags.ToString()
                 .Split(new[] { ", " }, StringSplitOptions.None)
                 .Select(d => (int)Enum.Parse(typeof(DayOfWeek), d))
                 .ToArray();
        }

        /// <summary>
        /// Creates a SQL-friendly transaction scope with a ReadCommitted isolation level to reduce deadlocks
        /// </summary>
        /// <returns></returns>
        public static TransactionScope CreateTransactionScope()
        {
            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            transactionOptions.Timeout = TransactionManager.MaximumTimeout;
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }


        /*public static string SerializeXml<T>(T obj)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            var writer = new StringWriter();
            XmlWriter xmlWriter = XmlWriter.Create(writer, settings);

            XmlSerializerNamespaces names = new XmlSerializerNamespaces();
            names.Add("", "");

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            serializer.Serialize(xmlWriter, obj, names);
            var xml = writer.ToString();
            return xml;
        }*/

        public static bool? ParseNullableBool (string b) {
            bool o;
            if (bool.TryParse(b, out o))
            {
                return o;
            }
            return null;
        }

        public static int? ParseNullableInt(string i)
        {
            int o;
            if (int.TryParse(i, out o))
            {
                return o;
            }
            return null;
        }

        public static string ExtractGuid(string g)
        {
            Regex r = new Regex("[a-z0-9]{8}[-][a-z0-9]{4}[-][a-z0-9]{4}[-][a-z0-9]{4}[-][a-z0-9]{12}");
            Match m = r.Match(g);

            if (m != null && m.Success && !string.IsNullOrEmpty(m.Value))
            {
                return m.Value;
            }

            return null;
        }

        public static byte[] SerializeToBytes(object source)
        {

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return null;
            }

            if (!source.GetType().IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            IFormatter formatter = new BinaryFormatter();
            
            using (var stream = new MemoryStream())
            {
                formatter.Serialize(stream, source);
                return stream.ToArray();
            }
        }
        
 
        public static T DeserializeFromBytes<T>(byte[] param)
        {
           using (MemoryStream ms = new MemoryStream(param))
           {
               IFormatter br = new BinaryFormatter();
               return (T)br.Deserialize(ms);
           }
        }

        public static string UpperCaseFirst(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }

            return str.Substring(0, 1).ToUpper() + str.Substring(1);
        }

        public static string CleanWordXmlComments(string html) {
            return Regex.Replace(html, "<\\!--.+?-->", "");
        }
    }
}
