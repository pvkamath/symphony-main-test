using System;
using System.Data;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Symphony.Core.Models;
using Symphony.Core;
using System.IO;
using System.Text;
using System.Net;
using Symphony.Core.Controllers;
using System.Security.Principal;
using System.Linq;

namespace Symphony.Web
{
    /// <summary>
    /// Determines the correct application name for the membership and role providers
    /// </summary>
    public class ApplicationProcessor : IHttpModule
    {
        //public static string SESSION_SUBDOMAIN = "SUBDOMAIN";

        // yes, IE doesn't really care what the value is, so we load up junk
        public readonly static string P3P_HEADER = "CP=\"POTATO\"";

        public static string CONTEXT_APPNAME = "ApplicationName";
        public static string CONTEXT_CUSTOMER = "CustomerName";
        public static string CONTEXT_USERDATA = "UserData";
        public static string APPLICATION_LOCKER = "ApplicationLocker";
        public static string NOT_SET = "unknown";
        public readonly static string[] ALLOWED_ORIGINS =
            !string.IsNullOrEmpty(ConfigurationManager.AppSettings["Access-Control-Allow-Origin"]) ? ConfigurationManager.AppSettings["Access-Control-Allow-Origin"].Split(',') : new string[] { };

        public void Dispose() { return; }

        public void Init(HttpApplication app)
        {
            app.BeginRequest += new EventHandler(this.DetermineApplicationName);
            app.AuthenticateRequest += new EventHandler(app_AuthenticateRequest);
            app.PreSendRequestHeaders += new EventHandler(app_PreSendRequestHeaders);

            // filtering of jsonp
            app.PreRequestHandlerExecute += new EventHandler(app_PreRequestHandlerExecute);
        }

        private string _jsonP = string.Empty;
        private bool IsJsonP(HttpApplication app)
        {
            _jsonP = app.Context.Request.QueryString["jsonp"];
            return !string.IsNullOrEmpty(_jsonP);
        }

        void app_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;
            if (IsJsonP(app))
            {
                app.Context.Response.Buffer = true;
                app.Context.Response.Filter = new ResponseCaptureStream(app.Context.Response.Filter, app.Context.Response.ContentEncoding);
            }
            if (app.Context.Items[CONTEXT_USERDATA] != null)
            {
                // DS:  This is required to handle a case when User doesn't have a Symphony account (UserID = -999) and does have account in External Systems
                //      like ProSchools.
                var userDataApp = app.Context.Items[CONTEXT_USERDATA] as AuthUserData;
                if (userDataApp.UserID < 0 &&
                    !app.Context.Request.Path.ToLower().Contains("login.aspx") &&
                    !app.Context.Request.Path.ToLower().Contains("samlhandler.ashx") &&
                    !app.Context.Request.Path.ToLower().Contains("samllogouthandler.ashx"))
                {
                    app.Context.Response.Redirect("/MultiLogin.aspx");
                }
            }
        }

        void app_AuthenticateRequest(object sender, EventArgs e)
        {
            bool forcedAuthenticationCookie = false;

            HttpApplication app = (HttpApplication)sender;
            HttpCookie authenticationCookie = app.Context.Request.Cookies[FormsAuthentication.FormsCookieName];

            AuthUserData userData = null;
            if (app.Context.Request.Headers["X-Symphony-Username"] != null)
            {
                string username = app.Context.Request.Headers["X-Symphony-Username"];
                string password = app.Context.Request.Headers["X-Symphony-Password"];
                string customer = app.Context.Request.Headers["X-Symphony-Customer"];

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(customer))
                {
                    UserController controller = new UserController();
                    AuthenticationResult result = controller.Login(customer, username, password);
                    if (result != null && result.Success)
                    {
                        userData = result.UserData;
                        var id = new GenericIdentity(username);
                        app.Context.User = new GenericPrincipal(id, new string[] { });
                    }
                }
            }

            if (!string.IsNullOrEmpty(app.Context.Request.QueryString["token"]) && !app.Context.Request.Url.AbsolutePath.EndsWith("websync.ashx"))
            {
                authenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, app.Context.Request.QueryString["token"]);

                // Make sure the authentication cookie is actually valid before we use it
                // Should protect us from any other future query params that might use "token"
                AuthUserData authenticationCookieCheck = GetUserData(authenticationCookie);
                if (authenticationCookieCheck != null && authenticationCookieCheck.UserID > 0)
                {
                    app.Context.Request.Cookies.Add(authenticationCookie);
                    app.Context.Response.Cookies.Add(authenticationCookie);
                    forcedAuthenticationCookie = true;
                }
            }

            if (userData == null && authenticationCookie != null)
            {
                userData = GetUserData(authenticationCookie);

                if (userData != null && app.Context.User == null && forcedAuthenticationCookie)
                {
                    var id = new GenericIdentity(userData.Username);
                    app.Context.User = new GenericPrincipal(id, new string[] { });
                }

                if (userData != null)
                {
                    if (!app.Context.Items.Contains(CONTEXT_APPNAME) || string.IsNullOrEmpty((string)app.Context.Items[CONTEXT_APPNAME]))
                    {
                        app.Context.Items[CONTEXT_APPNAME] = userData.ApplicationName;
                    }
                    if (!app.Context.Items.Contains(CONTEXT_CUSTOMER) || string.IsNullOrEmpty((string)app.Context.Items[CONTEXT_CUSTOMER]))
                    {
                        app.Context.Items[CONTEXT_CUSTOMER] = userData.ApplicationName;
                    }
                }
                else
                {
                    app.Context.Items[CONTEXT_CUSTOMER] = string.Empty;
                }
            }

            // store the results in the app context
            app.Context.Items[CONTEXT_USERDATA] = userData;
        }

        public static AuthUserData GetUserData(HttpCookie authenticationCookie)
        {
            FormsAuthenticationTicket authenticationTicket = null;
            try
            {
                authenticationTicket = FormsAuthentication.Decrypt(authenticationCookie.Value);
            }
            catch
            {
                // Cookie content could be compromised and no longer valid for decryption. 
                // We'll end up returning a 'null' in that case (ie: not logged in).
            }

            if (authenticationTicket != null && !authenticationTicket.Expired)
            {
                try
                {
                    // if we have a valid cookie containing a valid FormsAuthenticationTicket, they user must be authenticated
                    AuthUserData userData = Utilities.Deserialize<AuthUserData>(authenticationTicket.UserData);
                    return userData;
                }
                catch
                {
                }
            }
            return null;
        }

        void app_PreSendRequestHeaders(object sender, EventArgs e)
        {
            // always set the cookie back before the request finishes
            HttpApplication app = (HttpApplication)sender;
            string customer = GetCustomer(app);
            if (customer != NOT_SET)
            {
                //UpdateResponseCookie(app.Context, "customer", customer);
                //UpdateRequestCookie(app.Context, "customer", customer);
            }

            // add a p3p header so things works nicely in IE
            app.Context.Response.Headers["P3P"] = P3P_HEADER;

            ResponseCaptureStream filter = app.Context.Response.Filter as ResponseCaptureStream;

            if (filter != null)
            {
                if (app.Context.Response.StatusCode == 200)
                {
                    // wrap the contents in a jsonp wrapper
                    string responseText = filter.StreamContent;
                    string prefix = _jsonP + "(";
                    string postfix = ")";
                    string total = prefix + responseText + postfix;

                    // adjust the output
                    app.Context.Response.Clear();
                    app.Context.Response.ClearContent();
                    app.Context.Response.ClearHeaders();
                    app.Context.Response.Write(total);
                    app.Context.Response.Flush();
                }
            }
            else
            {
                var origin = app.Context.Request.Headers["Origin"];
                if (Allowed(origin))
                {
                    app.Context.Response.Headers.Remove("Access-Control-Allow-Origin");
                    app.Context.Response.Headers.Add("Access-Control-Allow-Origin", origin);
                }
            }
        }

        private bool Allowed(string origin)
        {
            return ALLOWED_ORIGINS.Any(x => { return x == origin; });
        }

        private void FixFlashSession(HttpContext context)
        {
            /* Fix for the Flash Player Cookie bug in Non-IE browsers.
             * Since Flash Player always sends the IE cookies even in FireFox
             * we have to bypass the cookies by sending the values as part of the POST or GET
             * and overwrite the cookies with the passed in values.
             * 
             * The theory is that at this point (BeginRequest) the cookies have not been read by
             * the Session and Authentication logic and if we update the cookies here we'll get our
             * Session and Authentication restored correctly
             */

            try
            {
                string session_param_name = "ASP.NET_SessionId";
                string session_cookie_name = "ASP.NET_SessionId";

                if (context.Request.Form[session_param_name] != null)
                {
                    UpdateRequestCookie(context, session_cookie_name, context.Request.Form[session_param_name]);
                }
                else if (context.Request.QueryString[session_param_name] != null)
                {
                    UpdateRequestCookie(context, session_cookie_name, context.Request.QueryString[session_param_name]);
                }
            }
            catch (Exception)
            {
                context.Response.StatusCode = 500;
                context.Response.Write("Error Initializing Session");
            }

            try
            {
                string auth_param_name = FormsAuthentication.FormsCookieName;
                string auth_cookie_name = FormsAuthentication.FormsCookieName;

                if (HttpContext.Current.Request.Form[auth_param_name] != null)
                {
                    UpdateRequestCookie(context, auth_cookie_name, context.Request.Form[auth_param_name]);
                }
                else if (HttpContext.Current.Request.QueryString[auth_param_name] != null)
                {
                    UpdateRequestCookie(context, auth_cookie_name, context.Request.QueryString[auth_param_name]);
                }

            }
            catch (Exception)
            {
                context.Response.StatusCode = 500;
                context.Response.Write("Error Initializing Forms Authentication");
            }
        }

        private static void UpdateRequestCookie(HttpContext context, string cookie_name, string cookie_value)
        {
            HttpCookie cookie = context.Request.Cookies.Get(cookie_name);
            if (cookie == null)
            {
                cookie = new HttpCookie(cookie_name);
                context.Request.Cookies.Add(cookie);
            }
            //cookie.Domain = ".";
            cookie.Value = cookie_value;
            context.Request.Cookies.Set(cookie);
        }

        public static void UpdateResponseCookie(HttpContext context, string cookie_name, string cookie_value)
        {
            HttpCookie cookie = context.Response.Cookies.Get(cookie_name);
            if (cookie == null)
            {
                cookie = new HttpCookie(cookie_name);
                context.Response.Cookies.Add(cookie);
            }
            cookie.Value = cookie_value;
            cookie.Path = "/";
            string domain = (new UserController()).GetFormsAuthenticationConfig().Domain;
            if (!string.IsNullOrEmpty(domain))
            {
                cookie.Domain = domain;
            }
            context.Response.Cookies.Set(cookie);
        }

        private void DetermineApplicationName(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;
            FixFlashSession(app.Context);

            string customer = GetCustomer(app);

            // store the customer id in the context of the request
            if (customer != NOT_SET)
            {
                app.Context.Items[CONTEXT_CUSTOMER] = customer;
            }
            else
            {
                app.Context.Items[CONTEXT_CUSTOMER] = string.Empty;
            }
            app.Context.Items[CONTEXT_APPNAME] = customer;

        }

        public static void SetApplicationName(HttpContext context, string name)
        {
            context.Items[CONTEXT_APPNAME] = name;
        }

        private string GetCustomer(HttpApplication app)
        {
            // if the request contains a query string value for the customer, we use that first
            string customer = app.Request.QueryString.Get("Customer") ?? app.Request.QueryString.Get("customer");

            if (string.IsNullOrEmpty(customer))
            {
                // if the request doesn't contain a customer identifier, check the cookies
                HttpCookie incoming = app.Request.Cookies.Get("customer");
                if (incoming != null && !string.IsNullOrEmpty(incoming.Value))
                {
                    customer = incoming.Value;
                }
                else
                {
                    customer = app.Request.Path.Substring(app.Request.Path.LastIndexOf("/") + 1);
                }
            }
            return customer;
        }

        private class ResponseCaptureStream : Stream
        {
            private readonly Stream _streamToCapture;
            private readonly Encoding _responseEncoding;

            private string _streamContent;
            public string StreamContent
            {
                get { return _streamContent; }
                private set
                {
                    _streamContent = value;
                }
            }

            public Encoding ResponseEncoding
            {
                get { return _responseEncoding; }
            }

            public ResponseCaptureStream(Stream streamToCapture, Encoding responseEncoding)
            {
                _responseEncoding = responseEncoding;
                _streamToCapture = streamToCapture;

            }

            public override bool CanRead
            {
                get { return _streamToCapture.CanRead; }
            }

            public override bool CanSeek
            {
                get { return _streamToCapture.CanSeek; }
            }

            public override bool CanWrite
            {
                get { return _streamToCapture.CanWrite; }
            }

            public override void Flush()
            {
                _streamToCapture.Flush();
            }

            public override long Length
            {
                get { return _streamToCapture.Length; }
            }

            public override long Position
            {
                get
                {
                    return _streamToCapture.Position;
                }
                set
                {
                    _streamToCapture.Position = value;
                }
            }

            public override int Read(byte[] buffer, int offset, int count)
            {
                return _streamToCapture.Read(buffer, offset, count);
            }

            public override long Seek(long offset, SeekOrigin origin)
            {
                return _streamToCapture.Seek(offset, origin);
            }

            public override void SetLength(long value)
            {
                _streamToCapture.SetLength(value);
            }

            public override void Write(byte[] buffer, int offset, int count)
            {
                _streamContent += _responseEncoding.GetString(buffer);
                _streamToCapture.Write(buffer, offset, count);
            }

            public override void Close()
            {
                _streamToCapture.Close();
                base.Close();
            }
        }
    }
}
