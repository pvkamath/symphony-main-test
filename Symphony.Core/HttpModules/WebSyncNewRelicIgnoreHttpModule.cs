﻿using System;
using System.Web;
using System.IO;
using FM;
using FM.WebSync;
using FM.WebSync.Server;
using System.Text;

namespace Symphony.Core.HttpModule
{
    public class WebSyncNewRelicIgnoreHttpModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.PostMapRequestHandler += (s, e) =>
            {
                if (HttpContext.Current.Handler is FM.WebSync.Server.RequestHandler)
                {
                    Message[] messages = null;
                    Publication[] publications = null;
                    Notification[] notifications = null;

                    string jsonContent = "";

                    Stream inputStream = new MemoryStream();
                    HttpContext.Current.Request.InputStream.CopyTo(inputStream);

                    using (StreamReader sr = new StreamReader(inputStream))
                    {
                        jsonContent = sr.ReadToEnd();
                    }
                    
                    byte[] bytes = Encoding.UTF8.GetBytes(jsonContent);


                    if (RequestParser.DeserializeMessages(jsonContent, bytes, out messages, out publications, out notifications))
                    {
                        if (messages != null && messages.Length > 0)
                        {
                            // messages coming in; we only want to ignore the long-held requests for data, 
                            // which have a single message (a "connect" meta request) with a valid client id
                            if (messages[0].Channel == FM.WebSync.MetaChannels.Connect && messages[0].ClientId.HasValue)
                            {
                                NewRelic.Api.Agent.NewRelic.IgnoreTransaction();
                            }
                        }
                    }
                }
            };
        }

        public void Dispose()
        { }
    }
}
