﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Symphony.Core.Models;
using System.Globalization;

namespace Symphony.Core.Extensions
{
    public static class DateExtensions
    {
        public static readonly DateTime DefaultDateTime = new DateTime(1900, 1, 1);

        /// <summary>
        /// In the db, everything is stored in universal time; however, when we load the date time, it doesn't know 
        /// that, so it uses local timezone info that makes the JSON serializer tacks on the -400 (or whatever) so we
        /// have to make sure the datetime instance as the "Kind" set to UTC.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime WithUtcFlag(this DateTime dt)
        {
            return DateTime.SpecifyKind(dt, DateTimeKind.Utc);
        }

        /// <summary>
        /// Converts a UTC date time to the specified timezone
        /// </summary>
        /// <param name="dtUtc"></param>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public static DateTime UtcToTimeZone(this DateTime dtUtc, string zoneId)
        {
            TimeZoneInfo timeZoneInfo = System.TimeZoneInfo.FindSystemTimeZoneById(zoneId);
            return dtUtc.UtcToTimeZone(timeZoneInfo);
        }


        public static DateTime UtcToTimeZone(this DateTime dtUtc, TimeZoneInfo tzi)
        {
            DateTime dataTimeByZoneId = System.TimeZoneInfo.ConvertTime(dtUtc, System.TimeZoneInfo.Utc, tzi);
            return dataTimeByZoneId;
        }

        public static string UtcToFormattedTimeZone(this DateTime dtUtc, string timezone, string format)
        {
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(timezone);
            return dtUtc.WithUtcFlag().UtcToTimeZone(tzi).ToString(format);
        }

        public static string UtcToFormattedTimeZone(this DateTime dtUtc, string timezone)
        {
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(timezone);
            string format = DateTimeFormatInfo.CurrentInfo.ShortDatePattern + " " + DateTimeFormatInfo.CurrentInfo.ShortTimePattern;
            return dtUtc.UtcToFormattedTimeZone(timezone, format) + " (" + tzi.StandardName + ")";
        }

        public static bool IsDefaultDateTime(this DateTime dt)
        {
            return dt == DefaultDateTime;
        }
    }
}
