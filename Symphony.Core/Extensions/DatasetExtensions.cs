﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;
using System.Data;
using System.IO;
using Symphony.Core.ThirdParty.Microsoft;

namespace Symphony.Core.Extensions
{
    public static class DatasetExtensions
    {
        public static List<T> WithUtcOffset<T>(this List<T> items)
        {
            PropertyInfo[] properties = typeof(T).GetProperties().Where(p => p.PropertyType == typeof(DateTime) || p.PropertyType == typeof(DateTime?)).ToArray();
            foreach (T t in items)
            {
                foreach (PropertyInfo pi in properties)
                {
                    object o = pi.GetValue(t, null);
                    if (o != null)
                    {
                        try
                        {
                            pi.SetValue(t, ((DateTime)o).WithUtcFlag(), null);
                        }
                        catch
                        {
                            // ignore
                        }
                    }
                }
            }
            return items;
        }

        public static DataSet WithUTCFlag(this DataSet ds)
        {
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.DataType == typeof(DateTime) || dc.DataType == typeof(DateTime?))
                    {
                        dc.DateTimeMode = DataSetDateTime.Utc;
                    }
                }
            }
            return ds;
        }

        public static DataSet InTimeZone(this DataSet ds, string timezoneId)
        {
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        
                        if (dc.DataType == typeof(DateTime) && dr[dc] != DBNull.Value)
                        {
                            DateTime dateTime = ((DateTime)dr[dc]);
                            dr[dc] = dateTime.UtcToTimeZone(timezoneId);
                        }
                    }
                }
            }
            return ds;
        }

        /// <summary>
        /// Converts column names from camel case format to "nice" names for human readability
        /// </summary>
        /// <param name="ds"></param>
        public static DataSet EnsureNiceColumnNames(this DataSet ds)
        {
            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    dc.ColumnName = SubSonic.Sugar.Strings.CamelToProper(dc.ColumnName);
                }
            }
            return ds;
        }

        /// <summary>
        /// Converts a dataset to Excel
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public static Stream ToExcel(this DataSet ds)
        {
            return Excel.CreateExcelDocumentAsStream(ds);
        }

        /// <summary>
        /// Converts a dataset to a CSV; note that it *only* handles table 0.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public static string ToCsv(this DataSet ds)
        {
            if (ds.Tables.Count == 0)
            {
                throw new Exception("No data found to convert to CSV.");
            }

            StringBuilder sb = new StringBuilder();
            DataTable dt = ds.Tables[0];

            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field =>
                    string.Concat("\"", field.ToString().Replace("\"", "\"\""), "\""));
                sb.AppendLine(string.Join(",", fields));
            }

            return sb.ToString();
        }
    }
}
