﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;
using Symphony.Core.Models;
using System.Text.RegularExpressions;

namespace Symphony.Core.Extensions
{
    public static class SubSonicSqlQueryExtensions
    {
        private class FunctionProvider : SqlDataProvider
        {
            public override string DelimitDbName(string columnName)
            {
                return columnName;
            }
        }

        public static SqlQuery CrossApply(this SqlQuery query, string functionName)
        {
            var table = new TableSchema.Table(functionName, TableType.View, new FunctionProvider());
            table.TableType = TableType.View;
            table.SchemaName = "dbo";
            table.Name = functionName;
            
            table.Columns = new TableSchema.TableColumnCollection() {};

            var tableColumn = new TableSchema.TableColumn(table);

            query.Joins.Add(new Join(tableColumn, tableColumn, Join.JoinType.CrossApply));
            
            return query;
        }

        public static SqlQuery ApplyLevelIndentText<T>(this SqlQuery query, TableSchema.TableColumn column) where T : RecordBase<T>, new()
        {
            T hierarchy = new T();

            string functionTemplate = "fGet{0}HierarchyPath({1})";

            // Make sure there is a valid default id for category
            string columnWithDefault = string.Format(@"coalesce(nullif({0},0), (
                select top 1 ID from Category where Name = 'Default' order by ID asc
            ))", column.QualifiedName);

            query.CrossApply(string.Format(functionTemplate,
                                    hierarchy.TableName,
                                    columnWithDefault));

            return query;
        }

        public static SqlQuery ApplyCustomerNetwork<T>(this SqlQuery query, int customerId, bool isReporting, bool isTrainingProgram) where T : RecordBase<T>, new() {
            T entity = new T();
            TableSchema.TableColumn customerColumn = entity.GetSchema().GetColumn("CustomerID");

            if (customerColumn != null)
            {
                query.ApplyCustomerNetwork(customerColumn, customerId, isReporting, isTrainingProgram);
            }
            
            return query;
        }

        public static SqlQuery ApplyCustomerNetwork(this SqlQuery query, TableSchema.TableColumn customerColumn, int customerId, bool isReporting, bool isTrainingProgram)
        {
            SqlQuery innerQuery = new Select(Data.CustomerNetworkDetail.SourceCustomerIdColumn)
                    .From<Data.CustomerNetworkDetail>()
                    .Where(Data.CustomerNetworkDetail.DestinationCustomerIdColumn).IsEqualTo(customerId);

            if (isReporting)
            {
                innerQuery.And(Data.CustomerNetworkDetail.AllowReportingColumn).IsEqualTo(true);
            }

            if (isTrainingProgram)
            {
                innerQuery.And(Data.CustomerNetworkDetail.AllowTrainingProgramSharingColumn).IsEqualTo(true);
            }

            if (query.HasWhere)
            {
                query.AndExpression(customerColumn.QualifiedName).IsEqualTo(customerId);
            }
            else
            {
                query.WhereExpression(customerColumn.QualifiedName).IsEqualTo(customerId);
            }

            query.Or(customerColumn).In(
                    innerQuery
                )
            .CloseExpression();

            return query;
        }
        public static SqlQuery AddAggregate(this SqlQuery query, string columnName, string alias, AggregateFunction function) {
            query.Aggregates.Add(new Aggregate(columnName, alias, function));
            return query;
        }

        public static SqlQuery Min(this SqlQuery query, TableSchema.TableColumn column)
        {
            return query.Min(column, column.ColumnName);
        }
        public static SqlQuery Min(this SqlQuery query, TableSchema.TableColumn column, string alias)
        {
            return query.Min(column.QualifiedName, alias);
        }
        public static SqlQuery Min(this SqlQuery query, string columnName, string alias)
        {
            return query.AddAggregate(columnName, alias, AggregateFunction.Min);
        }

        public static SqlQuery Max(this SqlQuery query, TableSchema.TableColumn column)
        {
            return query.Max(column, column.ColumnName);
        }
        public static SqlQuery Max(this SqlQuery query, TableSchema.TableColumn column, string alias)
        {
            return query.Max(column.QualifiedName, alias);
        }
        public static SqlQuery Max(this SqlQuery query, string columnName, string alias)
        {
            return query.AddAggregate(columnName, alias, AggregateFunction.Max);
        }

        public static SqlQuery Sum(this SqlQuery query, TableSchema.TableColumn column)
        {
            return query.Sum(column, column.ColumnName);
        }
        public static SqlQuery Sum(this SqlQuery query, TableSchema.TableColumn column, string alias)
        {
            return query.Sum(column.QualifiedName, alias);
        }
        public static SqlQuery Sum(this SqlQuery query, string columnName, string alias)
        {
            return query.AddAggregate(columnName, alias, AggregateFunction.Sum);
        }

        public static SqlQuery Count(this SqlQuery query, TableSchema.TableColumn column)
        {
            return query.Count(column, column.ColumnName);
        }
        public static SqlQuery Count(this SqlQuery query, TableSchema.TableColumn column, string alias)
        {
            return query.Count(column.QualifiedName, alias);
        }
        public static SqlQuery Count(this SqlQuery query, string columnName, string alias)
        {
            return query.AddAggregate(columnName, alias, AggregateFunction.Count);
        }

        public static SqlQuery GroupBy(this SqlQuery query, TableSchema.TableColumn column)
        {
            return query.GroupBy(column, column.ColumnName);
        }
        public static SqlQuery GroupBy(this SqlQuery query, TableSchema.TableColumn column, string alias)
        {
            return query.GroupBy(column.QualifiedName, alias);
        }
        public static SqlQuery GroupBy(this SqlQuery query, string columnName, string alias)
        {
            return query.AddAggregate(columnName, alias, AggregateFunction.GroupBy);
        }

        public static SqlQuery OrderByAggregate(this SqlQuery query, string alias, OrderDirection dir) {
            Aggregate aggregate = query.Aggregates.Where(a => a.Alias.ToLower() == alias.ToLower()).FirstOrDefault();

            if (aggregate != null && !string.IsNullOrEmpty(aggregate.ColumnName))
            {
                string aggregateStr = aggregate.WithoutAlias().Trim();
                switch (dir)
                {
                    case OrderDirection.Ascending:
                    case OrderDirection.Default:
                        query.OrderAsc(aggregateStr);
                        break;
                    case OrderDirection.Descending:
                        query.OrderDesc(aggregateStr);
                        break;
                }
            }

            return query;
        }

        /// <summary>
        /// Opens an or expression without automatically closing the parent expression if there is one open. 
        /// </summary>
        /// <param name="query"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static Constraint OrNestedExpression(this SqlQuery query, string columnName)
        {
            return new Constraint(ConstraintType.Or, columnName, columnName, "(" + columnName, query);
        }
        /// <summary>
        /// Opens an and expression without automatically closing the parent expression if there is one open.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static Constraint AndNestedExpression(this SqlQuery query, string columnName)
        {
            return new Constraint(ConstraintType.And, columnName, columnName, "(" + columnName, query);
        }

        private static bool IsMatchingColumn(TableSchema.TableColumn column, string property, string tableName = "", string columnName = "")
        {
            if (column.ColumnName.ToLower() == property.ToLower() ||
                column.PropertyName.ToLower() == property.ToLower() ||
                column.QualifiedName.ToLower() == property.ToLower() ||
                (column.Table.Name.ToLower() == tableName.ToLower() && column.ColumnName.ToLower() == columnName.ToLower()))
            {
                return true;
            }
            return false;
        }

        private static TableSchema.TableColumn FindMatchingColumn(TableSchema.TableColumnCollection tableColumns, string property)
        {
            string[] qualifiers = property.Split('.');
            string tableName = "";
            string columnName = "";

            if (qualifiers.Length > 1)
            {
                tableName = qualifiers[qualifiers.Length - 2].Replace("[", "").Replace("]", "");
                columnName = qualifiers[qualifiers.Length - 1].Replace("[", "").Replace("]", "");
            }

            foreach (var tableColumn in tableColumns)
            {
                if (IsMatchingColumn(tableColumn, property, tableName, columnName))
                {
                    return tableColumn;
                }
            }
            return null;
        }

        private static TableSchema.TableColumn FindMatchingColumn(List<Join> joins, string property)
        {
            foreach (var join in joins)
            {
                if (IsMatchingColumn(join.FromColumn, property))
                {
                    return join.FromColumn;
                }

                if (IsMatchingColumn(join.ToColumn, property))
                {
                    return join.ToColumn;
                }
            }
            return null;
        }

        public static SqlQuery ApplyGenericFilter<T>(this SqlQuery query, List<GenericFilter> filters) where T : RecordBase<T>, new() 
        {
            if (filters == null)
            {
                return query;
            }

            T record = new T();
           

            foreach (GenericFilter filter in filters)
            {
                string columnName = filter.Property;
                string search = filter.Value;
                TableSchema.TableColumn column = null;
                

                // look in the table columns
                foreach (var table in query.FromTables)
                {
                    column = FindMatchingColumn(table.Columns, filter.Property);
                    if (column != null)
                    {
                        break;
                    }
                }

                // look in the joins if no match
                if (column == null)
                {
                    foreach (var join in query.Joins)
                    {
                        TableSchema.Table fromTable = join.FromColumn.Table;
                        TableSchema.Table toTable = join.ToColumn.Table;

                        if (!query.FromTables.Contains(fromTable))
                        {
                            column = FindMatchingColumn(fromTable.Columns, filter.Property);
                            if (column != null)
                            {
                                break;
                            }
                        }
                        if (!query.FromTables.Contains(toTable))
                        {
                            column = FindMatchingColumn(toTable.Columns, filter.Property);
                            if (column != null)
                            {
                                break;
                            }
                        } 
                    }
                }

                if (column == null)
                {
                    throw new Exception("Failed to locate column " + filter.Property + " on query against " + record.GetType());
                }

                columnName = column.QualifiedName;
                
                if (filter.LogicOperator == 0)
                {
                    // Assume and
                    filter.LogicOperator = (int)LogicOperators.And;
                }

                if (filter.ComparisonOperator == 0)
                {
                    
                    // Check value and decide on some appropriate defaults
                    if (filter.Value.Contains(",") && filter.Value.Split(new char[] { ',' }).Count() > 1)
                    {
                        filter.ComparisonOperator = (int)ComparisonOperators.In;
                    }
                    else if (filter.Value.Contains("%"))
                    {
                        filter.ComparisonOperator = (int)ComparisonOperators.Like;
                    }
                    else
                    {
                        filter.ComparisonOperator = (int)ComparisonOperators.EqualTo;
                    }
                }

                switch (filter.LogicOperator)
                {
                    case (int)LogicOperators.And:
                        switch (filter.ComparisonOperator)
                        {
                            case (int)ComparisonOperators.EqualTo:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.AndExpression(columnName).IsEqualTo(search);
                                }
                                else
                                {
                                    query.And(columnName).IsEqualTo(search);
                                }
                                break;
                            case (int)ComparisonOperators.NotEqualTo:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.AndExpression(columnName).IsNotEqualTo(search);
                                }
                                else
                                {
                                    query.And(columnName).IsNotEqualTo(search);
                                }
                                break;
                            case (int)ComparisonOperators.GreaterThan:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.AndExpression(columnName).IsGreaterThan(search);
                                }
                                else
                                {
                                    query.And(columnName).IsGreaterThan(search);
                                }
                                break;
                            case (int)ComparisonOperators.GreaterThanOrEqual:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.AndExpression(columnName).IsGreaterThanOrEqualTo(search);
                                }
                                else
                                {
                                    query.And(columnName).IsGreaterThanOrEqualTo(search);
                                }
                                break;
                            case (int)ComparisonOperators.LessThan:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.AndExpression(columnName).IsLessThan(search);
                                }
                                else
                                {
                                    query.And(columnName).IsLessThan(search);
                                }
                                break;
                            case (int)ComparisonOperators.LessThanOrEqual:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.AndExpression(columnName).IsLessThanOrEqualTo(search);
                                }
                                else
                                {
                                    query.And(columnName).IsLessThanOrEqualTo(search);
                                }
                                break;
                            case (int)ComparisonOperators.Like:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.AndExpression(columnName).Like(search);
                                }
                                else
                                {
                                    query.And(columnName).Like(search);
                                }
                                break;
                            case (int)ComparisonOperators.In:
                                string[] searchParam = search.Split(new char[] { ','});

                                if (searchParam.Length == 0 || string.IsNullOrEmpty(search))
                                {
                                    searchParam = new string[] { "-1" };
                                }

                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.AndExpression(columnName).In(searchParam);
                                }
                                else
                                {
                                    query.And(columnName).In(searchParam);
                                }
                                break;
                        }
                        break;
                    case (int)LogicOperators.Or:
                        switch (filter.ComparisonOperator)
                        {
                            case (int)ComparisonOperators.EqualTo:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.OrExpression(columnName).IsEqualTo(search);
                                }
                                else
                                {
                                    query.Or(columnName).IsEqualTo(search);
                                }
                                break;
                            case (int)ComparisonOperators.NotEqualTo:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.OrExpression(columnName).IsNotEqualTo(search);
                                }
                                else
                                {
                                    query.Or(columnName).IsNotEqualTo(search);
                                }
                                break;
                            case (int)ComparisonOperators.GreaterThan:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.OrExpression(columnName).IsGreaterThan(search);
                                }
                                else
                                {
                                    query.Or(columnName).IsGreaterThan(search);
                                }
                                break;
                            case (int)ComparisonOperators.GreaterThanOrEqual:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.OrExpression(columnName).IsGreaterThanOrEqualTo(search);
                                }
                                else
                                {
                                    query.Or(columnName).IsGreaterThanOrEqualTo(search);
                                }
                                break;
                            case (int)ComparisonOperators.LessThan:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.OrExpression(columnName).IsLessThan(search);
                                }
                                else
                                {
                                    query.Or(columnName).IsLessThan(search);
                                }
                                break;
                            case (int)ComparisonOperators.LessThanOrEqual:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.OrExpression(columnName).IsLessThanOrEqualTo(search);
                                }
                                else
                                {
                                    query.Or(columnName).IsLessThanOrEqualTo(search);
                                }
                                break;
                            case (int)ComparisonOperators.Like:
                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.OrExpression(columnName).Like(search);
                                }
                                else
                                {
                                    query.Or(columnName).Like(search);
                                }
                                break;
                            case (int)ComparisonOperators.In:
                                string[] searchParam = search.Split(new char[] { ',' });

                                if (searchParam.Length == 0 || string.IsNullOrEmpty(search))
                                {
                                    searchParam = new string[] { "-1" };
                                }

                                if (filter.IsCloseExpression)
                                {
                                    query.CloseExpression();
                                }
                                if (filter.IsOpenExpression)
                                {
                                    query.OrExpression(columnName).In(searchParam);
                                }
                                else
                                {
                                    query.Or(columnName).In(searchParam);
                                }

                                break;
                        }
                    break;
                }
            }
            return query;
        }
    }
}
