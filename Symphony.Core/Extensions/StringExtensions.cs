﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;
using System.Xml;
using HtmlAgilityPack;
using System.Text.RegularExpressions;

namespace Symphony.Core.Extensions
{
    public static class StringExtensions
    {

        /// <summary>
        /// Truncates a string to the specified length. If null, returns an empty string.
        /// </summary>
        /// <param name="s">The string to truncate</param>
        /// <param name="length">The length of the returned string</param>
        /// <param name="ellipses">If true, truncates an extra 3 characters from the original string and appends 3 periods.</param>
        /// <returns></returns>
        public static string Truncate(this string s, int length, bool ellipses = false)
        {
            if (s == null)
            {
                return string.Empty;
            }
            if (s.Length <= length)
            {
                return s;
            }
            if (ellipses)
            {
                return s.Substring(0, length - 3) + "...";
            }
            else
            {
                return s.Substring(0, length);
            }
        }

        /// <summary>
        /// Converts all newline characters to a <br/> tag; useful for sending emails
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string NewlinesToBr(this string s)
        {
            return s.Replace("\n", "<br />");
        }

        /// <summary>
        /// Converts any <br/> tags to newlines
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string BrToNewlines(this string html)
        {
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(html ?? "");
            var selectNodes = document.DocumentNode.SelectNodes("//br");
            if (selectNodes != null)
            {
                foreach (var brTag in document.DocumentNode.SelectNodes("//br"))
                {
                    var newLine = document.CreateElement("span");
                    newLine.InnerHtml = "\n";
                    brTag.ParentNode.ReplaceChild(newLine, brTag);
                }
            }
            return document.DocumentNode.InnerText;
        }

        /// <summary>
        /// Escapes xml characters
        /// </summary>
        /// <param name="unescaped"></param>
        /// <returns></returns>
        public static string XmlEscape(this string unescaped)
        {
            XmlDocument doc = new XmlDocument();
            var node = doc.CreateElement("root");
            node.InnerText = unescaped;
            return node.InnerXml;
        }

        /// <summary>
        /// Removes any HTML markup
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string StripHtml(this string s)
        {
            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(s ?? "");
            return document.DocumentNode.InnerText;
        }

        /// <summary>
        /// Removes all newline characters
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string NewLinesToSpace(this string s)
        {
            return s.Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ");
        }

        /// <summary>
        /// Converts HTML to simple text markup, replacing br tags with newlines first, then stripping HTML markup
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static string HtmlToText(this string html)
        {
            return html.BrToNewlines().StripHtml();
        }

        /// <summary>
        /// Helper method to determine if a string is X% similar to another string
        /// </summary>
        /// <param name="s"></param>
        /// <param name="compareTo"></param>
        /// <param name="percent"></param>
        /// <returns></returns>
        public static bool IsSimilarTo(this string s, string compareTo, int percent)
        {
            int maxDistance = s.Length - (int)(s.Length * percent / 100);
            int actualDistance = s.ComputeLevenshteinDistance(compareTo);
            return actualDistance <= maxDistance;
        }

        /// <summary>
        /// Does approximate string matching
        /// </summary>
        public static int ComputeLevenshteinDistance(this string str1, string compareTo)
        {
            int n = str1.Length;
            int m = compareTo.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
            {
                return m;
            }

            if (m == 0)
            {
                return n;
            }

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }

            for (int j = 0; j <= m; d[0, j] = j++)
            {
            }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (compareTo[j - 1] == str1[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }
        // http://seattlesoftware.wordpress.com/2008/09/11/hexadecimal-value-0-is-an-invalid-character/
        /// <summary>
        /// Whether a given character is allowed by XML 1.0.
        /// </summary>
        public static bool IsLegalXmlChar(int character)
        {
            return
            (
                 character == 0x9 /* == '\t' == 9   */          ||
                 character == 0xA /* == '\n' == 10  */          ||
                 character == 0xD /* == '\r' == 13  */          ||
                (character >= 0x20 && character <= 0xD7FF) ||
                (character >= 0xE000 && character <= 0xFFFD) ||
                (character >= 0x10000 && character <= 0x10FFFF)
            );
        }

        /// <summary>
        /// Clean up any garbage
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string FilterXml(this string data)
        {
            if (data == null)
            {
                return data;
            }

            StringBuilder buffer = new StringBuilder(data.Length);
            foreach (char c in data)
            {
                if (IsLegalXmlChar(c))
                {
                    buffer.Append(c);
                }
            }
            return buffer.ToString();
        }

        /// <summary>
        /// Returns the current string if not null or empty, otherwise the default
        /// </summary>
        /// <param name="value">current string</param>
        /// <param name="alternate">default value</param>
        /// <returns></returns>
        public static string Or(this string value, string alternate)
        {
            return !string.IsNullOrEmpty(value) ? value : alternate;
        }

    }
}
