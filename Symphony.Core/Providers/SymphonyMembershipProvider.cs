using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Symphony.Core.Data;

namespace Symphony.Web
{
    /// <summary>
    /// Provides customer-specific login credentials
    /// </summary>
    public class SymphonyMembershipProvider : SqlMembershipProvider
    {
        private static int _MaxInvalidLoginAttempts = 0;

        public override string ApplicationName
        {
            get
            {
                string appNameFromContext = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                if (!string.IsNullOrEmpty(appNameFromContext) && appNameFromContext != "NOTSET")
                    return appNameFromContext;
                else
                    return base.ApplicationName;
            }
        }

        public static MembershipUser CreateUser(string subdomain, string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, out MembershipCreateStatus status)
        {
            return CreateUser(subdomain, username, password, email, passwordQuestion, passwordAnswer, isApproved, out status, false);
        }

        public static MembershipUser CreateUser(string subdomain, string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, out MembershipCreateStatus status, bool bOverrideSubdomain)
        {
            if (!BaseProvider.IsSalesChannelAdmin() && !bOverrideSubdomain)
                return Membership.CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, out status);

            MembershipUser mu;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                mu = Membership.CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, out status);
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return mu;
        }
        public static MembershipUser GetUser(string subdomain, string username)
        {
            return GetUser(subdomain, username, false);
        }

        public static MembershipUser GetUser(string subdomain, string username, bool forceOverride)
        {
            if (!BaseProvider.IsSalesChannelAdmin() && !forceOverride)
                return Membership.GetUser(username);

            MembershipUser mu;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                mu = Membership.GetUser(username);
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return mu;
        }

        public static bool DeleteUser(string subdomain, string username)
        {
            if (!BaseProvider.IsSalesChannelAdmin())
                return Membership.DeleteUser(username);

            bool success;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                success = Membership.DeleteUser(username);
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return success;
        }

        public static void UpdateUser(string subdomain, MembershipUser user)
        {
            UpdateUser(subdomain, user, false);
        }

        public static void UpdateUser(string subdomain, MembershipUser user, bool forceOverride)
        {
            if (!BaseProvider.IsSalesChannelAdmin() && !forceOverride)
                Membership.UpdateUser(user);

            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                Membership.UpdateUser(user);
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
        }

        public static bool ValidateUser(string subdomain, string username, string password)
        {
            bool result = false;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                result = Membership.ValidateUser(username, password);
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return result;
        }

        public static string ResetPassword(string subdomain, MembershipUser mu, string passwordAnswer)
        {
            return ResetPassword(subdomain, mu, passwordAnswer, false);
        }

        public static string ResetPassword(string subdomain, MembershipUser mu, string passwordAnswer, bool forceOverride)
        {
            if (!BaseProvider.IsSalesChannelAdmin() && !forceOverride)
                return mu.ResetPassword(passwordAnswer);

            string response;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                response = mu.ResetPassword(passwordAnswer);
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return response;
        }

        public static bool ChangePassword(string subdomain, MembershipUser mu, string oldPassword, string newPassword)
        {
            return ChangePassword(subdomain, mu, oldPassword, newPassword, false);
        }

        public static bool ChangePassword(string subdomain, MembershipUser mu, string oldPassword, string newPassword, bool forceOverride)
        {
            if (!BaseProvider.IsSalesChannelAdmin() && !forceOverride)
                return mu.ChangePassword(oldPassword, newPassword);

            bool success;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                success = mu.ChangePassword(oldPassword, newPassword);
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return success;
        }

        public static bool UnlockUser(string subdomain, MembershipUser mu)
        {
            return UnlockUser(subdomain, mu, false);
        }

        public static bool UnlockUser(string subdomain, MembershipUser mu, bool forceOverride)
        {
            if (!BaseProvider.IsSalesChannelAdmin() && !forceOverride)
                return mu.UnlockUser();

            bool success;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                success = mu.UnlockUser();
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return success;
        }

        public override int MaxInvalidPasswordAttempts
        {
            get
            {
                return _MaxInvalidLoginAttempts > 0 ? _MaxInvalidLoginAttempts : base.MaxInvalidPasswordAttempts;
            }

        }

        public static void SetMaxLoginAttempts(int maxAttempts)
        {
            _MaxInvalidLoginAttempts = maxAttempts;
        }
    }
}