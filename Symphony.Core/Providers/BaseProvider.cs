﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Symphony.Core.Models;
using Symphony.Core.Controllers;

namespace Symphony.Web
{
    public class BaseProvider
    {
        //private static string SALES_CHANNEL_ADMIN = "SALES_CHANNEL_ADMIN";
        public static bool IsSalesChannelAdmin()
        {
            SymphonyController controller = new SymphonyController();
            return controller.UserIsSalesChannelAdmin;

            //if (HttpContext.Current.Session[SALES_CHANNEL_ADMIN] == null)
            //{
            //    string username = HttpContext.Current.User.Identity.Name;
            //    string appName = "";
            //    lock (HttpContext.Current.Application["ApplicationLocker"])
            //    {
            //        appName = (string)HttpContext.Current.Items["ApplicationName"];
            //    }
            //    HttpContext.Current.Session[SALES_CHANNEL_ADMIN] = (new UserController().IsSalesChannelAdmin(username, appName));
            //}
            //return (bool)HttpContext.Current.Session[SALES_CHANNEL_ADMIN];
        }
    }
}
