using System;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Symphony.Core.Models;

namespace Symphony.Web
{
    /// <summary>
    /// Summary description for BankersEdgeRoleProvider
    /// </summary>
    public class SymphonyRoleProvider : SqlRoleProvider
    {
        public override string ApplicationName
        {
            get
            {
                string appNameFromContext = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                if (!string.IsNullOrEmpty(appNameFromContext) && appNameFromContext != ApplicationProcessor.NOT_SET)
                    return appNameFromContext;
                else
                    return base.ApplicationName;
            }
        }

        public static new string[] GetUsersInRole(string role)
        {
            return Roles.GetUsersInRole(role);
        }

        public static string[] GetUsersInRole(string subdomain, string role)
        {
            if (!BaseProvider.IsSalesChannelAdmin())
                return Roles.GetUsersInRole(role);

            string[] users;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                users = Roles.GetUsersInRole(role);
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return users;
        }

        public static string[] GetRolesForUser(string subdomain, string username)
        {
            if (!BaseProvider.IsSalesChannelAdmin())
                return Roles.GetRolesForUser(username);

            string[] roles;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                roles = Roles.GetRolesForUser(username);
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return roles;
        }

        public static void RemoveUserFromRoles(string subdomain, string username, string[] roleNames)
        {
            if (!BaseProvider.IsSalesChannelAdmin())
            {
                foreach (string s in roleNames)
                {
                    if (Roles.IsUserInRole(username, s))
                        Roles.RemoveUserFromRole(username, s);
                }                
            }
            else
            {
                lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
                {
                    string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                    HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                    foreach (string s in roleNames)
                    {
                        if (Roles.IsUserInRole(username, s))
                            Roles.RemoveUserFromRole(username, s);
                    }   
                    HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
                }
            }
        }

        public static void RemoveUserFromRole(string subdomain, string username, string roleName)
        {
            if (!BaseProvider.IsSalesChannelAdmin())
            {
                if (Roles.IsUserInRole(username, roleName))
                    Roles.RemoveUserFromRole(username, roleName);
            }
            else
            {
                lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
                {
                    string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                    HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                    if (Roles.IsUserInRole(username, roleName))
                        Roles.RemoveUserFromRole(username, roleName);
                    HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
                }
            }
        }

        public static void AddUserToRoles(string subdomain, string username, string[] roleNames, int customerID)
        {
            // Case #1 - first we check if this user is being added to a customer that allows self sign up
            if (new Symphony.Core.Data.Customer(customerID).AllowSelfAccountCreation)
            {
                SymphonyRoleProvider.domainAddUserToRoll(subdomain, username, roleNames);
                return;
            }

            // Case #2 - Now we check if the user is a sales channel admin
            if (!BaseProvider.IsSalesChannelAdmin())
                Roles.AddUserToRoles(username, roleNames);
            else
            {
                SymphonyRoleProvider.domainAddUserToRoll(subdomain, username, roleNames);
            }
        }

        // internal func used to reduce code duplication. Called from AddUserToRoles
        private static void domainAddUserToRoll(string subdomain, string username, string[] roleNames)
        {
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                foreach (string role in roleNames)
                {
                    if (!Roles.IsUserInRole(username, role))
                    {
                        Roles.AddUserToRole(username, role);
                    }
                }
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
        }

        public static void AddUserToRole(string subdomain, string username, string roleName)
        {
            if (!BaseProvider.IsSalesChannelAdmin())
                Roles.AddUserToRole(username, roleName);
            else
            {
                lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
                {
                    string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                    HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                    Roles.AddUserToRole(username, roleName);
                    HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
                }
            }
        }

        public static bool IsUserInRole(string subdomain, string username, string roleName)
        {
            if (!BaseProvider.IsSalesChannelAdmin())
                return Roles.IsUserInRole(username, roleName);

            bool success;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                success = Roles.IsUserInRole(username, roleName);
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return success;
        }

        public static string[] GetAllRoles(string subdomain)
        {
            if (!BaseProvider.IsSalesChannelAdmin())
                return Roles.GetAllRoles();

            string[] roles;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                roles = Roles.GetAllRoles();
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return roles;
        }

        public static void CreateRole(string subdomain, string roleName)
        {
            if (!BaseProvider.IsSalesChannelAdmin())
                Roles.CreateRole(roleName);
            else
            {
                lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
                {
                    string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                    HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                    Roles.CreateRole(roleName);
                    HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
                }
            }
        }

        public static bool DeleteRole(string subdomain, string roleName)
        {
            if (!BaseProvider.IsSalesChannelAdmin())
                return Roles.DeleteRole(roleName);

            bool success;
            lock (HttpContext.Current.Application[ApplicationProcessor.APPLICATION_LOCKER])
            {
                string originalApp = (string)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME];
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = subdomain;
                success = Roles.DeleteRole(roleName);
                HttpContext.Current.Items[ApplicationProcessor.CONTEXT_APPNAME] = originalApp;
            }
            return success;
        }

        
        
    }
}