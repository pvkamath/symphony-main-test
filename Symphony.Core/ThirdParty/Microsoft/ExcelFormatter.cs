﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Reflection;

namespace Symphony.Core.ThirdParty.Microsoft
{
    /// <summary>
    /// This class is used when converting a dataset to an Excel spreadsheet
    /// </summary>
    public class ExcelFormatter
    {
        /// <summary>
        /// Builds an excel workbook from a dataset and writes to and returns a StringWriter.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public static StringWriter CreateWorkbook(DataSet ds)
        {
            StringWriter swDataset = new StringWriter();
            ds.WriteXml(swDataset);
            ExcelFormatter xf = new ExcelFormatter() { DataSet = ds };
            // clean up the xml
            string xml = xf.FormatString(swDataset.ToString());

            XmlDataDocument xmlDataDoc = new XmlDataDocument(ds);

            // load in the transformation xsl
            XslCompiledTransform xt = new XslCompiledTransform();
            StreamReader reader = new StreamReader(Assembly.GetAssembly(typeof(ExcelFormatter)).GetManifestResourceStream("Symphony.Core.Resources.Excel.xsl"));
            XmlTextReader xRdr = new XmlTextReader(reader);
            xt.Load(xRdr, null, null);

            StringWriter sw = new StringWriter();

            // add the excel formatter so we can call some additional methods so we can
            // do things like determine if an object is an int or date, etc
            XsltArgumentList args = new XsltArgumentList();
            args.AddExtensionObject("urn:symphony", xf);

            // convert the dataset to xml
            XmlReader xmlReader = XmlReader.Create(new StringReader(xml));
            xt.Transform(xmlReader, args, sw);

            return sw;
        }

        /// <summary>
        /// Converts a dataset into an excel workbook, and returns that workbook as a stream. Any dates that are in the dataset will be converted to the specified
        /// timezone before export.
        /// </summary>
        /// <param name="dsResult">The dataset to export</param>
        /// <param name="timezone">The timezone to convert all dates to; assumes the incoming dataset is in UTC; null means ignore</param>
        /// <returns></returns>
        public static Stream CreateWorkbookStream(DataSet dsResult, TimeSpan? offset = null)
        {
            // make sure all dates in the dataset are converted to the specified timezone
            if (offset != null)
            {
                foreach (DataColumn dc in dsResult.Tables[0].Columns)
                {
                    if (dc.DataType != typeof(DateTime))
                    {
                        continue;
                    }

                    foreach (DataRow dr in dsResult.Tables[0].Rows)
                    {
                        DateTime? dt = dr[dc] as DateTime?;
                        if (dt.HasValue)
                        {
                            dr[dc] = dt.Value.Subtract(offset.Value);
                        }
                    }
                    dsResult.AcceptChanges();
                }
            }

            // convert to an excel workbook and write out out
            StringWriter sw = CreateWorkbook(dsResult);

            byte[] byteArray = System.Text.UnicodeEncoding.ASCII.GetBytes(sw.ToString());
            MemoryStream memStream = new MemoryStream();
            memStream.Write(byteArray, 0, byteArray.GetLength(0));
            memStream.Seek(0, SeekOrigin.Begin);

            return memStream;
        }

        public DataSet DataSet { get; set; }

        /// <summary>
        /// Determines the column type given the column name; this lets us format dates and numbers properly in the output Excel sheet
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public string GetColumnType(string column)
        {
            DataColumn dc = DataSet.Tables[0].Columns[column];
            if (dc != null)
            {
                if (dc.DataType == typeof(Int32))
                {
                    return "Number";
                }
                else if (dc.DataType == typeof(DateTime))
                {
                    return "DateTime";
                }
            }
            return "String";
        }

        /// <summary>
        /// Takes a date formatted according to the DataSet datetime format and converts it into the Excel format
        /// </summary>
        /// <param name="utcDate"></param>
        /// <returns></returns>
        public string FormatDate(string utcDate)
        {
            DateTime dt = DateTime.SpecifyKind(DateTime.Parse(utcDate), DateTimeKind.Utc);
            return dt.ToString("yyyy-MM-ddTHH:mm:ss.fff");
        }

        /// <summary>
        /// Clean up any garbage
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string FormatString(string data)
        {
            if (data == null)
            {
                return data;
            }

            StringBuilder buffer = new StringBuilder(data.Length);
            foreach (char c in data)
            {
                if (IsLegalXmlChar(c))
                {
                    buffer.Append(c);
                }
            }

            return buffer.ToString();
        }

        // http://seattlesoftware.wordpress.com/2008/09/11/hexadecimal-value-0-is-an-invalid-character/
        /// <summary>
        /// Whether a given character is allowed by XML 1.0.
        /// </summary>
        private bool IsLegalXmlChar(int character)
        {
            return
            (
                 character == 0x9 /* == '\t' == 9   */          ||
                 character == 0xA /* == '\n' == 10  */          ||
                 character == 0xD /* == '\r' == 13  */          ||
                (character >= 0x20 && character <= 0xD7FF) ||
                (character >= 0xE000 && character <= 0xFFFD) ||
                (character >= 0x10000 && character <= 0x10FFFF)
            );
        }


    }
}
