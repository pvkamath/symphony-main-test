﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSass;
using Symphony.Core.Models;
using System.Reflection;
using System.IO;
using log4net;

namespace Symphony.Core.Generators
{
    public class DynamicThemeBuilder
    {
        ILog Log = LogManager.GetLogger(typeof(DynamicThemeBuilder));

        string defaultVariablesResource = "Symphony.Core.Resources.default_variables.scss";
        string variablesOutputFilename = "_variables.scss";

        public string GetDynamicThemeCss(Theme theme, string scssPath, string scssVariablesTemplatePath = null)
        {
            try
            {
                SassCompiler compiler = new SassCompiler();

                string variablesTemplate = string.Empty;
                string variablesOutputPath = string.Empty;

                if (!string.IsNullOrWhiteSpace(scssVariablesTemplatePath))
                {
                    if (File.Exists(scssVariablesTemplatePath))
                    {
                        variablesTemplate = File.ReadAllText(scssVariablesTemplatePath);
                    }
                }

                if (string.IsNullOrWhiteSpace(variablesTemplate))
                {
                    Type t = this.GetType();
                    using (StreamReader reader = new StreamReader(t.Assembly.GetManifestResourceStream(defaultVariablesResource)))
                    {
                        variablesTemplate = reader.ReadToEnd();
                    }
                }

                if (!string.IsNullOrWhiteSpace(variablesTemplate))
                {
                    List<PropertyInfo> variableProperties = theme.GetScssVariableFields();
                    foreach (PropertyInfo pi in variableProperties)
                    {
                        string value = pi.GetValue(theme).ToString();
                        string name = pi.Name;

                        variablesTemplate = variablesTemplate.Replace("{" + name + "}", value);
                    }

                    variablesOutputPath = Path.Combine(Path.GetDirectoryName(scssPath), variablesOutputFilename);
                    File.WriteAllText(variablesOutputPath, variablesTemplate);
                }

                string compiledCss = compiler.CompileFile(scssPath.Replace("/", "\\"), OutputStyle.Nested, true);

                if (File.Exists(variablesOutputPath))
                {
                    File.Delete(variablesOutputPath);
                }

                return compiledCss;
            }
            catch (Exception e)
            {
                Log.Error("Could not compile SCSS for Theme ID: " + theme.ID + " SCSS Path: " + scssPath + " Template Path: " + scssVariablesTemplatePath, e);
                throw;
            }
        }
    }
}
