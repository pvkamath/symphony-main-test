﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Symphony.Core.Models;
using System.Reflection;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using log4net;
using System.Configuration;

namespace Symphony.Core.Generators
{
    public class ArtisanCourseTextExport : IArtisanCourseExport
    {
        private ILog Log = LogManager.GetLogger(typeof(ArtisanCourseTextExport));

        private string OutputFolder = ConfigurationManager.AppSettings["ArtisanExportFolder"] ?? @"c:\temp\exportcourse\";

        /// <summary>
        /// The course we are packaging.
        /// </summary>
        public Models.ArtisanCourse Course { get; set; }

        /// <summary>
        /// The root path of the course package.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Creates a new exporter with the selected course
        /// </summary>
        /// <param name="course"></param>
        public ArtisanCourseTextExport(Models.ArtisanCourse course)
        {
            this.Course = course;
            this.FileName = Path.Combine(this.OutputFolder, this.Course.Id.ToString() + ".txt");

            if (!Directory.Exists(this.OutputFolder))
            {
                Directory.CreateDirectory(this.OutputFolder);
            }

            try
            {
                File.Delete(this.FileName);
            }
            catch { }
        }

        /// <summary>
        /// Exports the course and returns the output file
        /// </summary>
        /// <returns></returns>
        public FileInfo Export()
        {
            if (this.Course != null)
            {
                StreamWriter writer = new StreamWriter(this.FileName, false);
                int tabs = 0;
                writer.WriteLine(this.Course.Name);
                try
                {
                    if (this.Course.Sections != null)
                    {
                        foreach (ArtisanSection section in this.Course.Sections)
                        {
                            BuildSection(section, writer, tabs);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
                finally
                {
                    writer.Close();
                }
            }

            return new FileInfo(this.FileName);
        }

        private void BuildSection(ArtisanSection section, StreamWriter writer, int tabs)
        {
            tabs++;
            if (section.Sections != null)
            {
                string line = "";
                for (int i = 0; i < tabs; i++)
                {
                    line += "  ";
                }
                writer.WriteLine(line + section.Name);
                if (section.Objectives != "")
                {
                    writer.WriteLine(line + "  " + section.Objectives);
                }

                foreach (ArtisanSection subsection in section.Sections)
                {
                    BuildSection(subsection, writer, ++tabs);
                }
            }
            else if (section.Pages != null)
            {
                tabs++;
                foreach (ArtisanPage page in section.Pages)
                {
                    BuildPage(page, writer, tabs);
                }
            }
        }

        private void BuildPage(ArtisanPage page, StreamWriter writer, int tabs)
        {
            string line = "";
            for (int i = 0; i < tabs; i++)
            {
                line += "  ";
            }
            writer.WriteLine(line + page.Name + ":");
            if (page.QuestionType > 0)
            {
                writer.WriteLine(line + "  " + page.Html);
                foreach (ArtisanAnswer ans in page.Answers)
                {
                    string answer = line + "    " + ans.Text;
                    if (ans.IsCorrect)
                    {
                        answer += " - CORRECT";
                    }

                    writer.WriteLine(answer);
                }
            }
            else
            {
                HtmlDocument d = new HtmlDocument();
                d.LoadHtml(Regex.Replace(page.Html, @"<br\s?\/?>", "\r\n"));

                StringBuilder sb = new StringBuilder();
                foreach (string nodeName in new string[] { "h1", "h2", "h3", "h4", "h5", "p" })
                {
                    HtmlNodeCollection nodes = d.DocumentNode.SelectNodes("//" + nodeName);
                    if (nodes != null)
                    {
                        foreach (HtmlNode node in nodes)
                        {
                            if (!string.IsNullOrEmpty(node.InnerText.Trim()))
                            {
                                sb.AppendLine(line + "    " + node.InnerText.Replace("&nbsp;", " "));
                            }
                        }
                    }
                    sb.AppendLine();
                }
                writer.Write(sb.ToString());
            }

        }
    }
}
