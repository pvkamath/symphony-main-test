﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Generators
{
    static class AnswerTemplate
    {
        public static string GetTemplate(ArtisanPage page)
        {
            string answerContainer = @"
                <ol class='artisan-answers'>
                    {1}
                </ol>
                <div style='display:none' class='artisan-question-feedback'>
                    <div class='answer-image'></div>
                </div>";

            string answer = @"
                    <li class='artisan-answer'>
                        <input id='artisan-answer-{1}' class='artisan-answer' value='{5}' type='{3}' {4}/>
                        <label for='artisan-answer-{1}' class='control control-{3}'>
                            {2}
                            <div class='control-indicator'></div><div class='control-center'></div>
                        </label>
                        <div style='display:none' id='artisan-answer-{1}-feedback' class='artisan-answer-feedback'></div>
                    </li>";

            string imageQuestionAnswerContainer = @"
                <div class='artisan-answers'>
                    {1}
                </div>
                <div style='display:none' class='artisan-question-feedback'>
                    <div class='answer-image'></div>
                </div>";

            string imageQuestionAnswer = @"
                    <img class='artisan-answer' src='{2}' />
                    <input id='artisan-answer-{1}' class='artisan-answer' type='hidden'/>
                    <div style='display:none' id='artisan-answer-{1}-feedback' class='artisan-answer-feedback'></div>";

            string longAnswer = @"
                <div class='artisan-answer text-wrapper'>
                   <{3} id='artisan-answer-{1}' class='artisan-answer' name='artisan-answer' {4}>{5}</{3}> 
                </div>
            ";

            StringBuilder answers = new StringBuilder();

            if (page.Answers != null)
            {
                if (page.QuestionType == ArtisanQuestionType.Matching)
                {
                    return GetMatchingTemplate(page);
                }
                else if (page.QuestionType == ArtisanQuestionType.CategoryMatching)
                {
                    return GetCategoryMatchingTemplate(page);
                }

                foreach (ArtisanAnswer a in page.Answers)
                {
                    switch (page.QuestionType)
                    {
                        case ArtisanQuestionType.AllThatApply:
                            answers.AppendFormat(answer, "allthatapply", a.Id, a.Text, "checkbox", string.Empty, a.Id);
                            break;
                        case ArtisanQuestionType.FillInTheBlank:
                            answers.AppendFormat(answer, "fillintheblank", a.Id, "", "text", string.Empty, string.Empty);
                            break;
                        case ArtisanQuestionType.MultipleChoice:
                            answers.AppendFormat(answer, "multiplechoice", a.Id, a.Text, "radio", "name='question" + page.Id + "'", a.Id);
                            break;
                        case ArtisanQuestionType.TrueFalse:
                            answers.AppendFormat(answer, "truefalse", a.Id, a.Text, "radio", "name='question" + page.Id + "'", a.Id);
                            break;
                        case ArtisanQuestionType.ImageQuestion:
                            answers.AppendFormat(imageQuestionAnswer, "imagequestion", a.Id, a.ImageQuestionAnswer.ImageUrl, a.Id);
                            break;
                        case ArtisanQuestionType.LongAnswer:
                            answers.AppendFormat(longAnswer, "longanswer", a.Id, a.Text, "textarea", string.Empty, string.Empty);
                            break;
                    }
                }

                switch (page.QuestionType)
                {
                    case ArtisanQuestionType.AllThatApply:
                        answerContainer = string.Format(answerContainer, "allthatapply", answers);
                        break;
                    case ArtisanQuestionType.FillInTheBlank:
                        answerContainer = string.Format(answerContainer, "fillintheblank", answers);
                        break;
                    case ArtisanQuestionType.MultipleChoice:
                        answerContainer = string.Format(answerContainer, "multiplechoice", answers);
                        break;
                    case ArtisanQuestionType.TrueFalse:
                        answerContainer = string.Format(answerContainer, "truefalse", answers);
                        break;
                    case ArtisanQuestionType.ImageQuestion:
                        answerContainer = string.Format(imageQuestionAnswerContainer, "imagequestion", answers);
                        break;
                    case ArtisanQuestionType.LongAnswer:
                        answerContainer = string.Format(answerContainer, "longanswer", answers);
                        break;
                }
            }

            return answerContainer;
        }

        private static string GetMatchingTemplate(ArtisanPage page)
        {
            string matchingAnswer = @"
                    <li class='artisan-matching-answer {0}'>
                        {3}
                        <span id='artisan-answer-{0}-{1}' class='artisan-matching-text-{0}'>{2}</span>
                        {4}
                    </li>";

            string matchingAnswerContainer = @"
                <div class='artisan-answers'>
                    {1}
                    <div style='clear:both'></div>
                </div>
                <div style='display:none' class='artisan-question-feedback'>
                    <div class='answer-image'></div>
                </div>";

            StringBuilder answers = new StringBuilder();

            // Randomize the items for the left side
            var randomList = page.Answers.OrderBy(a => Guid.NewGuid()).ToList();

            // Create the left side list
            answers.Append("<ul class='artisan-matching-answers'>");
            foreach (var leftItem in randomList)
            {
                answers.AppendFormat(matchingAnswer, "left", leftItem.Id, leftItem.MatchingAnswer.LeftText, "", "");
            }
            answers.Append("</ul>");

            // Create the right side list
            answers.Append("<ul class='artisan-matching-answers'>");
            foreach (var rightItem in page.Answers)
            {
                string dropZoneText = string.Format("<div class='artisan-matching-dropzone' id='artisan-matching-dropzone-{0}'></div>", rightItem.Id);
                string feedbackText = string.Format("<div style='display:none' id='artisan-answer-{0}-feedback' class='artisan-answer-feedback'></div><div style='clear:both'></div>", rightItem.Id);
                answers.AppendFormat(matchingAnswer, "right", rightItem.Id, rightItem.MatchingAnswer.RightText, dropZoneText, feedbackText);
            }
            answers.Append("</ul>");

            return string.Format(matchingAnswerContainer, "matching", answers);
        }

        private static string GetCategoryMatchingTemplate(ArtisanPage page)
        {
            string matchingAnswerContainer = @"
                <div class='artisan-answers'>
                    {1}
                    <div style='clear:both'></div>
                </div>
                <div style='display:none' class='artisan-question-feedback'>
                    <div class='answer-image'></div>
                </div>";

            StringBuilder answers = new StringBuilder();

            // Put all matches together
            var allMatches = new List<KeyValuePair<string, string>>();
            foreach (var ans in page.Answers)
            {
                for (int i = 0; i < ans.CategoryMatchingAnswer.Matches.Count; i++)
                {
                    string id = string.Format("{0}-{1}", ans.Id, i);
                    allMatches.Add(new KeyValuePair<string, string>(id, ans.CategoryMatchingAnswer.Matches[i]));
                }
            }

            // Randomize the match value order
            allMatches = allMatches.OrderBy(a => Guid.NewGuid()).ToList();

            // Create the matches list
            answers.Append("<div class='artisan-category-matches'>");
            foreach (var match in allMatches)
            {
                answers.AppendFormat("<div id='artisan-category-match-{0}' class='artisan-category-match'><span>{1}</span></div>", match.Key, match.Value);
            }
            answers.Append("<div style='clear:both'></div>");
            answers.Append("</div>");

            string floatClear = "<div style='clear:both'></div>";

            // Create the category blocks
            answers.Append("<div class='artisan-categories'>");
            foreach (var ans in page.Answers)
            {
                string feedbackText = string.Format("<div style='display:none' id='artisan-answer-{0}-feedback' class='artisan-answer-feedback'></div>", ans.Id);
                string dropZoneText = string.Format("<div class='artisan-category-dropzone' id='artisan-category-dropzone-{0}'>{1}</div>", ans.Id, floatClear);
                answers.AppendFormat("<div class='artisan-category' id='artisan-category-{0}'><div class='artisan-category-head'><span>{1}</span>{2}</div>{3}</div>",
                                     ans.Id, ans.CategoryMatchingAnswer.Category, feedbackText, dropZoneText);
            }
            answers.Append(floatClear);
            answers.Append("</div>");

            return string.Format(matchingAnswerContainer, "categorymatching", answers);
        }

        internal static object GetOptions(ArtisanPage page)
        {
            switch (page.QuestionType)
            {
                case ArtisanQuestionType.ImageQuestion:
                    return "<input type='button' id='artisan-question-reset' value='Reset'>";
            }

            return string.Empty;
        }
    }
}
