using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Collections;
using ICSharpCode.SharpZipLib;
using ICSharpCode.SharpZipLib.Checksums;
using ICSharpCode.SharpZipLib.Zip;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for CourseArchive
/// </summary>
namespace BankersEdge.CourseGenerator
{
	public class ZipUtility
	{
        private void ZipFiles(ZipOutputStream zstream, Crc32 crc, string baseDirectory, string subDirectory, bool recurse)
        {
            string[] filenames = Directory.GetFiles(Path.Combine(baseDirectory, subDirectory));
            string[] directories = Directory.GetDirectories(Path.Combine(baseDirectory, subDirectory));

            foreach (string filename in filenames)
            {
                if (Path.GetExtension(filename).ToLower() == ".zip")
                    continue;

                FileStream fs = File.OpenRead(filename);
                try
                {
                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, buffer.Length);
                    ZipEntry entry = new ZipEntry(Path.Combine(subDirectory, Path.GetFileName(filename)));
                    entry.DateTime = DateTime.Now;
                    entry.Size = fs.Length;
                    fs.Close();
                    crc.Reset();
                    crc.Update(buffer);
                    zstream.PutNextEntry(entry);
                    zstream.Write(buffer, 0, buffer.Length);
                }
                finally
                {
                    fs.Close();
                }
            }

            if (recurse)
            {
                foreach (string directory in directories)
                {
                    if (directory.IndexOf(".svn") > -1) continue;

                    DirectoryInfo dirInfo = new DirectoryInfo(directory);
                    ZipFiles(zstream, crc, baseDirectory, Path.Combine(subDirectory, dirInfo.Name), recurse);
                }
            }
        }

        public void CreateArchive(string directory, string archiveFilename, string sPackageTypeCode)
        {
            Crc32 crc = new Crc32();
            ZipOutputStream zstream = new ZipOutputStream(File.Create(Path.Combine(directory, archiveFilename)));
            zstream.SetLevel(9);
            Hashtable fileHistory = new Hashtable();
            // hard code in this directory to find the location of xml schema files
            string sharedDirectory = CourseGenerator.GetSharedFolder(sPackageTypeCode);

            try
            {
                //add the xml schema files and other shared files
                //CopyDir(sharedDirectory, directory);
                // Zip files from the root directory
                ZipFiles(zstream, crc, directory, string.Empty, true);
                // add the xml schema files and other shared files
                //ZipFiles(zstream, crc, sharedDirectory, string.Empty, true);
            }
            finally
            {
                zstream.Finish();
                zstream.Close();
            }

        } // end of createArchive

        public static void CopyDir(string sourceDirectory, string targetDirectory)
        {
            DirectoryInfo diSource = new DirectoryInfo(sourceDirectory);
            DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);

            CopyAll(diSource, diTarget);
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            if (!source.Name.StartsWith("."))
            {
                // Check if the target directory exists, if not, create it.
                if (Directory.Exists(target.FullName) == false)
                {
                    Directory.CreateDirectory(target.FullName);
                }

                // Copy each file into it's new directory.
                foreach (FileInfo fi in source.GetFiles())
                {
                    //Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                    fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
                }

                // Copy each subdirectory using recursion.
                foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
                {
                    if (diSourceSubDir.Name.StartsWith(".")) continue;

                    DirectoryInfo nextTargetSubDir =
                        target.CreateSubdirectory(diSourceSubDir.Name);
                    CopyAll(diSourceSubDir, nextTargetSubDir);
                }
            }
        }

        
	}// end of zip utility

}
