using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.IO;

namespace BankersEdge.Artisan
{
    public class SCORM1_2Options
    {
        private int m_iCourseID;
        private int m_iMasteryScore;
        private string m_sOutputFile;
        private string m_sContentFolderPath;
        private string m_sSharedFolderPath;
        private bool m_bPostTest;
        private bool m_bPreTest;

        private string m_sPreTestType = "";
        private string m_sPostTestType = "";

        public string PostTestType
        {
            get { return this.m_sPostTestType; }
            set { this.m_sPostTestType = value; }
        }

        public string PreTestType
        {
            get { return this.m_sPreTestType; }
            set { this.m_sPreTestType = value; }
        }


        public bool PreTest
        {
            get { return this.m_bPreTest; }
            set { this.m_bPreTest = value; }
        }

        public bool PostTest
        {
            get { return this.m_bPostTest; }
            set { this.m_bPostTest = value; }
        }

        public int CourseID
        {
            get { return this.m_iCourseID; }
            set { this.m_iCourseID = value; }
        }

        public int MasteryScore
        {
            get { return this.m_iMasteryScore; }
            set { this.m_iMasteryScore = value; }
        }

        public string File
        {
            get { return this.m_sOutputFile; }
            set { this.m_sOutputFile = value; }
        }

        public string ContentFolderPath
        {
            get { return this.m_sContentFolderPath; }
            set { this.m_sContentFolderPath = value; }
        }

        public string SharedFolderPath
        {
            get { return this.m_sSharedFolderPath; }
            set { this.m_sSharedFolderPath = value; }
        }
    }

    /// <summary>
    /// Summary description for SCORM1_2Builder
    /// </summary>
    public class SCORM1_2Builder
    {
        private XmlTextWriter xw;

        private string metadataxml = "";

        private SCORM1_2Options m_cOptions;
        private TreeNode m_cCourseTree;

        public void CreateXML(TreeNode CourseTree, SCORM1_2Options Options)
        {
            this.m_cCourseTree = CourseTree;
            this.m_cOptions = Options;

            //basic xml stuff
            xw = new XmlTextWriter(this.m_cOptions.File, Encoding.Unicode);
            xw.Indentation = 1;
            xw.IndentChar = '\t';
            xw.WriteStartDocument();
            xw.Formatting = Formatting.Indented;

            Course cCourse = new Course(this.m_cOptions.CourseID);

            StartManifest();
            StartOrganizations();

            //title
            AddRootItem(cCourse.CourseName);

            //metadata
            AddMetaData();

            EndOrganizations();

            StartResources();
            AddResource(this.m_cOptions.CourseID.ToString(),"indexapi.html","index.html",true);
            EndResources();

            EndManifest();

            xw.Flush();
            xw.Close();

            /*if (m_cOptions.PostTest)
                AddTest(true);

            if (m_cOptions.PreTest)
                AddTest(false);
             * */
        }

        private void AddRootItem(string sCourseTitle)
        {
            xw.WriteElementString("title", sCourseTitle);
            
            xw.WriteStartElement("item");

            xw.WriteAttributeString("identifier", this.m_cOptions.CourseID.ToString());
            xw.WriteAttributeString("identifierref", "res-" + this.m_cOptions.CourseID);
            xw.WriteAttributeString("isvisible", "true");

            //title again. stupid ims manifest.
            xw.WriteElementString("title", sCourseTitle);
            xw.WriteElementString("adlcp:masteryscore", this.m_cOptions.MasteryScore.ToString());

            xw.WriteEndElement();
        }

        private void AddResource(string sID, string sHref, string sDefaultPage, bool bEndTag)
        {
            xw.WriteStartElement("resource");

            xw.WriteAttributeString("identifier", "res-" + sID);
            xw.WriteAttributeString("type", "webcontent");
            xw.WriteAttributeString("adlcp:scormtype", "sco"); //lower case t in scormtype is required for 1.2, and its a capital in 2004
            xw.WriteAttributeString("href", "content/" + sHref);

            AddFile(sDefaultPage);

            if (bEndTag)
                xw.WriteEndElement(); //end resource
        }

        private void AddFile(string sFileName)
        {
            xw.WriteStartElement("file");
            xw.WriteAttributeString("href", "content/" + sFileName);
            xw.WriteEndElement(); //end file
        }

        private void AddMetaData()
        {
            xw.WriteStartElement("metadata");

            xw.WriteElementString("schema", "ADL SCORM");
            xw.WriteElementString("version", "1.2");
            xw.WriteElementString("adlcp:location", metadataxml);

            xw.WriteEndElement();
        }

        private void StartManifest()
        {
            //begin the manifest file
            xw.WriteStartElement("manifest");
            xw.WriteAttributeString("identifier", "LMSPackageBankersEdge");
            xw.WriteAttributeString("version", "1.2");
            xw.WriteAttributeString("xmlns:adlcp", "http://www.adlnet.org/xsd/adlcp_rootv1p2");
            xw.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            xw.WriteAttributeString("xsi:schemaLocation", "http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd");
            xw.WriteAttributeString("xmlns", "http://www.imsproject.org/xsd/imscp_rootv1p1p2");
        }

        private void StartOrganizations()
        {
            //ok, now, build the packages. first, add the org stuff...
            xw.WriteStartElement("organizations");
            xw.WriteAttributeString("default", ScormSettings.ORGANIZATION_NAME);
            xw.WriteStartElement("organization");
            xw.WriteAttributeString("identifier", ScormSettings.ORGANIZATION_NAME);
        }

        private void StartResources()
        {
            xw.WriteStartElement("resources");
        }

        private void EndResources()
        {
            xw.WriteEndElement();
        }

        private void EndOrganizations()
        {
            xw.WriteEndElement();
            xw.WriteEndElement();
        }

        private void EndManifest()
        {
            xw.WriteEndElement();
            xw.WriteEndDocument();
        }

        private void AddTest(bool bIsPostTest)
        {
            string sID = ScormSettings.POSTTEST_SCO_ID;
            string sSubFolder = "posttest\\";
            if (!bIsPostTest)
            {
                sID = ScormSettings.PRETEST_SCO_ID;
                sSubFolder = "pretest\\";
            }
            string sPath = m_cOptions.ContentFolderPath + sSubFolder;

            //copy in the test.html file
            if (!Directory.Exists(sPath))
                Directory.CreateDirectory(sPath);

            if ((this.m_cOptions.PostTestType == TestType.JEOPARDY && bIsPostTest) ||
                (this.m_cOptions.PreTestType == TestType.JEOPARDY && !bIsPostTest))
            {

                File.Copy(m_cOptions.SharedFolderPath + "content\\jeopardy.html", sPath + "test.html", true);
                File.SetLastWriteTime(sPath + "test.html", DateTime.Now);
            }
            else
            {
                File.Copy(m_cOptions.SharedFolderPath + "content\\sequential.html", sPath + "test.html", true);
                File.SetLastWriteTime(sPath + "test.html", DateTime.Now);
            }

            File.Copy(m_cOptions.SharedFolderPath + "content\\review.html", sPath + "review.html", true);
            File.SetLastWriteTime(sPath + "review.html", DateTime.Now);
        }
    }
}