﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Symphony.Core.Models;
using System.Reflection;
using HtmlAgilityPack;
using Symphony.Core.Controllers;
using System.Xml;
using log4net;
using SubSonic.Sugar;
using System.Text.RegularExpressions;
using System.Web;


namespace Symphony.Core.Generators
{
    public class PackageBuilder
    {

        static ILog Log = LogManager.GetLogger(typeof(PackageBuilder));

        private bool hasGradeableInlineQuestions = false;

        /// <summary>
        /// If true, when packaging assets, the path will be the fully qualified local path on the server instead of the relative path in the package;
        /// has *no effect* on "grouped" assets. This is used to create HTML that can be used as part of the export process.
        /// </summary>
        public bool UseQualifiedLocalFilePaths { get; set; }

        /// <summary>
        /// A list of the ids of all built pages
        /// </summary>
        private List<int> builtPages = new List<int>();

        /// <summary>
        /// The output folder for generated packages
        /// </summary>
        private string OutputFolder = (new SymphonyController()).GetArtisanPackageLocation();

        /// <summary>
        /// We'll search for and replace this in the index page, substituting our various script and css tags
        /// </summary>
        //private string DynamicHeaderPlaceholder = "{!DynamicHeader}";

        /// <summary>
        /// We'll search for and replace this in the index page, substituting the proper skin css include
        /// </summary>
        private string SkinCssPlaceholder = "{!SkinCss}";

        /// <summary>
        /// Replace video support plugins with plugin selected for the course. 
        /// </summary>
        private string MediaPlayerPaceholder = "{!VideoSupport}";

        /// <summary>
        /// Default media player to use
        /// </summary>
        private string DefaultMediaPlayer = "Default";

        /// <summary>
        /// The root of embedded resources for artisan courses
        /// </summary>
        private string resourceRoot = "Symphony.Core.Resources.Packager.";

        /// <summary>
        /// The root of embedded resources for media players
        /// </summary>
        private string mediaPlayerRoot = "Symphony.Core.Resources.Packager.js.media_players.";

        /// <summary>
        /// init file containing script tags required for the media player.
        /// </summary>
        private string mediaPlayerInitFile = "InitTemplate.html";

        /// <summary>
        /// The course we are packaging.
        /// </summary>
        public Models.ArtisanCourse Course { get; set; }

        /// <summary>
        /// The root path of the course package.
        /// </summary>
        public string RootFolder { get; set; }

        /// <summary>
        /// The generated contents
        /// </summary>
        public string ContentFolder
        {
            get { return Path.Combine(RootFolder, this.ContentFolderName); }
        }

        /// <summary>
        /// The name of the folder within the package that holds any dynamic content
        /// </summary>
        public string ContentFolderName
        {
            get { return "content"; }
        }

        /// <summary>
        /// Gets the folder to be used for the zip file
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        private string GetZipFolder(int courseId)
        {
            return Path.Combine(this.OutputFolder, courseId + ".zip");
        }

        /// <summary>
        /// The FileInfo wrapper aroudn the ZipFolder
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public FileInfo GetPackageFolder(int courseId)
        {
            return new FileInfo(GetZipFolder(courseId));
        }

        public bool IsResource(string resource)
        {
            Type t = this.GetType();
            return t.Assembly.GetManifestResourceNames().Contains(resourceRoot + resource);
        }

        public string GetResourceText(string resource)
        {
            Type t = this.GetType();
            Stream stream = t.Assembly.GetManifestResourceStream(resourceRoot + resource);
            string resourceText = "";

            if (stream != null) {
                using (StreamReader r = new StreamReader(stream)) {
                    resourceText = r.ReadToEnd();
                }
            }

            return resourceText;
        }

        /// <summary>
        /// Creates a package builder with no course. The course must be set prior to calling Package()
        /// </summary>
        public PackageBuilder()
        {
        }

        /// <summary>
        /// Creates a package builder with the specified course object, and sets the root folder
        /// </summary>
        /// <param name="course"></param>
        public PackageBuilder(Models.ArtisanCourse course)
        {
            this.Course = course;
            this.RootFolder = Path.Combine(OutputFolder, this.Course.Id.ToString());
        }

        public PackageBuilder(Models.ArtisanCourse course, string outputFolder)
        {
            this.Course = course;
            this.OutputFolder = outputFolder;
            this.RootFolder = Path.Combine(OutputFolder, this.Course.Id.ToString());
        }

        /// <summary>
        /// Packages up a course into a SCORM compliant package
        /// </summary>
        /// <returns>The zip file containing the total package</returns>
        public FileInfo Package(string webRoot = null)
        {
            if (this.Course == null)
            {
                throw new Exception("A course must be specified before the course can be packaged.");
            }

            // try and clear out the folder
            try
            {
                Directory.Delete(this.RootFolder, true);
            }
            catch { }

            // first, we build out each page in the right course folder, etc
            BuildStructure();

            // then, we build out a JSON object that represents the course for playback
            BuildArtisanCourseJson();

            // next, we add the ims manifest file or .au files
            BuildSCORMControls();

            // add in the SCORM Driver API files
            AddApiFiles();

            // next, we add the default embedded content with a basically blank UI, and get the list of script/css tags to include
            string[] result = AddDefaultContents();
            string header = result.Length > 0 ? result[0] : "";
            string mediaPlayer = result.Length > 1 ? result[1] : "";

            // handle mastery
            if (this.Course.IsMasteryEnabled)
            {
                string betaPath = Path.Combine(this.RootFolder, "beta");
                Utilities.CopyFolder(betaPath, this.RootFolder);
            }

            // finally, we overwrite any of the embedded content with contents from the theme, adding the script tags to the final index page
            AddThemeContents(header, webRoot, mediaPlayer);

            // Copy over the course pdfjs viewer if required
            if (this.Course.IsRequirePdfJs)
            {
                string pdfjs = "/pdfjs";
                string fullPdfJsPath = "";
                if (!string.IsNullOrWhiteSpace(webRoot) || HttpContext.Current == null)
                {
                    fullPdfJsPath = Path.Combine(webRoot, pdfjs);
                }
                else
                {
                    fullPdfJsPath = HttpContext.Current.Server.MapPath(pdfjs);
                }

                Utilities.CopyFolder(fullPdfJsPath, Path.Combine(this.RootFolder, "pdfjs"));
            }

            // zip and return the file
            return new FileInfo(Utilities.Zip(this.RootFolder, GetZipFolder(this.Course.Id)));
        }

        /// <summary>
        /// Moves the SCORM Driver API into the output folder
        /// </summary>
        private void AddApiFiles()
        {
            byte[] buffer = new byte[32768];

            Type t = this.GetType();

            string root = "Symphony.Core.Resources.Packager.API";
            StringBuilder dynamicHeader = new StringBuilder();

            string[] contents = t.Assembly.GetManifestResourceNames();
            foreach (string item in contents)
            {
                if (!item.StartsWith(root))
                {
                    continue;
                }
                string noExtension = item.Substring(0, item.LastIndexOf("."));
                string file = noExtension.Substring(noExtension.LastIndexOf(".") + 1);
                string extension = item.Substring(noExtension.Length);
                string path = this.RootFolder + @"\API\" + file + extension;

                Stream stream = t.Assembly.GetManifestResourceStream(item);
                if (stream != null)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(path)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(path));
                    }
                    using (FileStream fs = new FileStream(path, FileMode.Create))
                    {
                        while (true)
                        {
                            int read = stream.Read(buffer, 0, buffer.Length);
                            if (read <= 0)
                            {
                                break;
                            }
                            fs.Write(buffer, 0, read);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get all media player embedded resource files.
        /// Assumtion is that all media player files will always have 1 extension
        /// </summary>
        /// <returns>Returns a dictionary of resource files for each media player keyed by the folder name of the media player.</returns>
        public Dictionary<string, List<string>> GetMediaPlayerResources()
        {
            Type t = this.GetType();
            Dictionary<string, List<string>> mediaPlayerResources = new Dictionary<string, List<string>>();
            
            List<string> resourceNames = t.Assembly.GetManifestResourceNames().ToList();

            foreach (string n in resourceNames) {
                if (n.StartsWith(mediaPlayerRoot)) {
                    string[] resourceNameParts = n.Split('.');

                    if (resourceNameParts.Length < 2)
                    {
                        throw new Exception("Could not determine the file name for " + n);
                    }
                    
                    // Files must have 1 extension for this to work. 
                    string fileName = resourceNameParts[resourceNameParts.Length - 2] + "." + resourceNameParts[resourceNameParts.Length - 1];
                    
                    string file = n.Replace(mediaPlayerRoot, "");
                    string mediaPlayerName = file.Substring(0, file.IndexOf("."));
                    
                    string filePath = n.Replace(fileName, "") // Remove the file name first
                                       .Replace(resourceRoot, "") // Don't need the resource root "Symphony.Core.Resources.Packager."
                                       .Replace(".", "/") // Convert remaining resource name to a path
                                       + fileName; // Tack on the file name.

                                        
                    if (!mediaPlayerResources.ContainsKey(mediaPlayerName))
                    {
                        mediaPlayerResources.Add(mediaPlayerName, new List<string>());
                    }

                    mediaPlayerResources[mediaPlayerName].Add(filePath);
                }
            }

            return mediaPlayerResources;
        }

        /// <summary>
        /// Copies the contents from the embedded resources to the output folder, and builds up a appropriate script/style tags for injection into the output header
        /// </summary>
        /// <returns>The script/style tags needed to make the default contents work</returns>
        private string[] AddDefaultContents()
        {
            byte[] buffer = new byte[32768];

            Type t = this.GetType();

            if (string.IsNullOrWhiteSpace(Course.MediaPlayer))
            {
                Course.MediaPlayer = DefaultMediaPlayer;
            }

            // Grab media player resources
            Dictionary<string, List<string>> mediaPlayerResources = GetMediaPlayerResources();

            List<string> selectedMediaPlayerFiles = new List<string>();

                if (!mediaPlayerResources.ContainsKey(Course.MediaPlayer))
                {
                    Course.MediaPlayer = DefaultMediaPlayer;
                }

                if (mediaPlayerResources.ContainsKey(Course.MediaPlayer))
                {
                    selectedMediaPlayerFiles = mediaPlayerResources[Course.MediaPlayer];
                }
                else
                {
                    if (mediaPlayerResources.Count > 0)
                    {
                        selectedMediaPlayerFiles = mediaPlayerResources.First().Value;
                        Log.Error("Could not find media player selected, or default media player. Using first media player found.");
                    }
                    else
                    {
                        throw new Exception("Could not find selected media player, and no default has been defined. Ensure the package builder is configured with the proper media player default, and the media player exists in Resources/Packager/js/media-players");
                    }
                }
            

            // enumerated list of files to extract from the embedded resources and include in the output
            // note that order matters, as the js/css files will be included in the page in this order
            List<string> contents = new List<string>() {
                "index.html", 
                "certificate.html", 
                "cursor.png",
                "content/artisan.coursejson.js",
                "flash/gamewheel.swf",
                "flash/expressInstall.swf",
                "js/jquery.js", 
                "js/jquery.fancybox.pack.js",
                "js/jquery.fancybox-buttons.js",
                "js/jquery.fancybox-media.js",
                "js/jquery.fancybox-thumbs.js",
                "js/json2.js", 
                "js/artisan.utilities.js", 
                "js/artisan.models.js", 
                "js/artisan.playback.js",
                "beta/js/artisan.utilities.js",
                "beta/js/artisan.models.js",
                "beta/js/artisan.playback.js",
                "beta/js/artisan.mastery.js",
                "beta/index.html",
                "js/lz-string.min.js",
                "css/artisan.default.css",
                "css/certificate.css",
                "css/debug.css",
                "css/jquery.fancybox.css",
                "css/jquery.fancybox-buttons.css",
                "css/jquery.fancybox-thumbs.css",
                "images/bewatermark.gif",
                "images/coursecompletionheading.gif",
                "images/print.button.gif",
                "images/blank.gif",
                "images/fancybox_loading.gif",
                "images/fancybox_loading@2x.gif",
                "images/fancybox_overlay.png",
                "images/fancybox_sprite.png",
                "images/fancybox_sprite@2x.png",
                "images/print.button.gif",
                
            };

            bool hasInitTemplate = selectedMediaPlayerFiles.Where(f => f.IndexOf(mediaPlayerInitFile) > 0).ToList().Count() > 0;

            if (!hasInitTemplate && selectedMediaPlayerFiles.Count > 0)
            {
                throw new Exception("Could not find " + mediaPlayerInitFile + " for the selected media player.");
            }
            
            if (selectedMediaPlayerFiles.Count > 0 && hasInitTemplate) {
                contents.AddRange(selectedMediaPlayerFiles);
            } else {
                throw new Exception("No files found for selected media player.");
            }

            Dictionary<string, string> excludeFilesFromHeader = selectedMediaPlayerFiles.ToDictionary(f => f);
            
            StringBuilder dynamicHeader = new StringBuilder();

            string mediaPlayerInitText = "";
            foreach (string item in contents)
            {
                switch (Path.GetExtension(item))
                {
                    case ".css":
                        if (!excludeFilesFromHeader.ContainsKey(item))
                        {
                            dynamicHeader.AppendLine(string.Format("<link rel='stylesheet' href='{0}' />", item));
                        }
                        break;
                    case ".js":
                        if (!excludeFilesFromHeader.ContainsKey(item))
                        {
                            dynamicHeader.AppendLine(string.Format("<script type='text/javascript' src='{0}'></script>", item));
                        }
                        break;
                    default:
                        // nothing
                        break;
                }

                string path = Path.Combine(this.RootFolder, item);
                string directory = Path.GetDirectoryName(path);
                
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                Stream stream = t.Assembly.GetManifestResourceStream(resourceRoot + item.Replace('/', '.'));
                if (stream != null)
                {
                    if (item.IndexOf(mediaPlayerInitFile) > 0)
                    {
                        using (StreamReader r = new StreamReader(stream))
                        {
                            mediaPlayerInitText = r.ReadToEnd();
                        }
                    }
                    else
                    {
                        using (FileStream fs = new FileStream(path, FileMode.Create))
                        {
                            while (true)
                            {
                                int read = stream.Read(buffer, 0, buffer.Length);
                                if (read <= 0)
                                {
                                    break;
                                }
                                fs.Write(buffer, 0, read);
                            }
                        }
                    }
                }
            }

            return new string[] {
                dynamicHeader.ToString(), mediaPlayerInitText
            };
        }

        /// <summary>
        /// Moves the theme contents into the output folder, and adjusts (if necessary) the index.htm[l] replacing the dynamic header
        /// </summary>
        private void AddThemeContents(string header, string webRoot = null, string mediaPlayerReplacement = null)
        {
            string themeFolder = Course.Theme.GetFolder(webRoot);
            string skinFolder = Course.Theme.GetSkinFolder();
            string finalFolder = this.RootFolder;
            
            Utilities.CopyFolder(themeFolder, finalFolder);

            string finalSkinsFolder = Path.Combine(finalFolder, "skins");
            string finalSkinFolder = Path.Combine(finalSkinsFolder, Course.Theme.SkinName);
            if (Directory.Exists(finalSkinFolder))
            {
                // Copy background image into main img folder
                //string bgPath = "img\\main_bg.jpg";

                // copy a couple other folders overz
                // img, js, css
                foreach (string s in new string[] { "img", "js", "css" })
                {
                    string skinOverride = Path.Combine(finalSkinFolder, s);
                    if (Directory.Exists(skinOverride))
                    {
                        Utilities.CopyFolder(skinOverride, Path.Combine(finalFolder, s));
                    }
                }


                // Delete other skins
                foreach (string finalSkin in Directory.GetDirectories(finalSkinsFolder))
                {
                    string finalSkinPath = Path.Combine(finalSkinsFolder, finalSkin);
                    if (finalSkinPath != finalSkinFolder)
                    {
                        Directory.Delete(finalSkinPath, true);
                    }
                }
            }

            // Set the skin to the dynamic theme if compiled
            if (Course.Theme.IsDynamic)
            {
                string compiledCss = new ArtisanController().GenerateDynamicThemeCss(Course.Theme, Course.ThemeFlavor);
                
                // Clear the skin directory of all files/folders coppied over
                // these wont be required, just the compiled skin.css
                Directory.Delete(finalSkinFolder, true);
                Directory.CreateDirectory(finalSkinFolder);
                
                File.WriteAllText(Path.Combine(finalSkinFolder, "skin.css"), compiledCss);
            }

            // We don't use the Dynamic placeholder for all the css/js anymore 
            // because some of the js needs cache bust and some of the css we don't want right away
            foreach (string s in new[] { "index.htm", "index.html" })
            {
                string index = Path.Combine(finalFolder, s);
                if (File.Exists(index))
                {
                    string indexText = File.ReadAllText(index);
                    //indexText = indexText.Replace(DynamicHeaderPlaceholder, header); // not used anymore
                    string skinCssReplacement = (String.IsNullOrEmpty(skinFolder) ? "" : "<link id='artisan_skin' rel='stylesheet' href='" + Path.Combine(skinFolder, "skin.css") + "' />");
                    
                    indexText = indexText.Replace(SkinCssPlaceholder, skinCssReplacement);
                    indexText = indexText.Replace(MediaPlayerPaceholder, mediaPlayerReplacement);
                    
                    File.WriteAllText(index, indexText);
                }
            }
        }

        private void BuildArtisanQuiz(ArtisanSection section)
        {
            List<ArtisanPage> pages = new List<ArtisanPage>();
            foreach (ArtisanPage page in section.Pages)
            {
                if (page.PageType == (int)ArtisanPageType.Question)
                {
                    pages.Add(page);
                }
            }
            section.Pages = pages;

            BuildIntroPage(section);
        }
        private void BuildStructure()
        {
            if (this.Course != null)
            {
                if (!Directory.Exists(this.ContentFolder))
                {
                    Directory.CreateDirectory(this.ContentFolder);
                }

                if (this.Course.Sections != null)
                {
                    hasGradeableInlineQuestions = false;

                    switch (this.Course.CompletionMethod)
                    {
                        // if completion is Inline Questions, Navigation, or Survey remove any existing pretest or posttest
                        case ArtisanCourseCompletionMethod.Navigation:
                        case ArtisanCourseCompletionMethod.Survey:
                        case ArtisanCourseCompletionMethod.InlineQuestions:
                            this.Course.Sections.RemoveAll(s => s.SectionType == (int)ArtisanSectionType.Pretest
                                                             || s.SectionType == (int)ArtisanSectionType.Posttest);
                            break;
                        case ArtisanCourseCompletionMethod.Test:
                        case ArtisanCourseCompletionMethod.TestForceRestart:
                            if (!this.Course.Sections.Any(s => s.SectionType == (int)ArtisanSectionType.Posttest))
                                throw new Exception("A course with completion type 'Test' must contain a Posttest.");
                            break;
                    }

                    // If we are using mastery mode, remove any surveys
                    if (this.Course.IsMasteryEnabled)
                    {
                        this.Course.Sections.RemoveAll(s => s.SectionType == (int)ArtisanSectionType.Survey);
                    }

                    foreach (ArtisanSection section in this.Course.Sections)
                    {
                        // depending on the section type, we may need to make some minor adjustments
                        // to the pages
                        switch ((ArtisanSectionType)section.SectionType)
                        {
                            case ArtisanSectionType.Objectives:
                                BuildObjectives(section);
                                break;
                            case ArtisanSectionType.Pretest:
                                if (section.Pages == null || !section.Pages.Any())
                                    throw new Exception("Pretest must contain at least one page.");
                                BuildIntroPage(section);
                                break;
                            case ArtisanSectionType.Posttest:
                                if (section.Pages == null || !section.Pages.Any())
                                    throw new Exception("Posttest must contain at least one page.");
                                BuildIntroPage(section);
                                break;
                            default:
                                break;
                        }

                        BuildSection(section, this.Course.Id, this.ContentFolder);
                    }

                    if (!string.IsNullOrEmpty(this.Course.PopupMenu))
                    {
                        this.Course.PopupMenu = ProcessHtmlAssets(this.Course.PopupMenu);
                    }

                    if (!hasGradeableInlineQuestions && this.Course.CompletionMethod == ArtisanCourseCompletionMethod.InlineQuestions)
                    {
                        throw new Exception("A course with completion type 'Inline Questions' must contain at least one page with a gradeable question in a learning object that is not a quiz.");
                    }
                }
            }
        }

        /// <summary>
        /// Builds objectives pages within the course
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        internal void BuildObjectives(ArtisanSection section)
        {
            // build up the objectives from the various course elements
            StringBuilder sb = new StringBuilder();
            sb.Append("<h3>Course Description</h3>");
            sb.Append("<div class='p'>");
            sb.Append(Course.Description);
            sb.Append("</div>");
            sb.Append("<h3>Course Objectives</h3>");
            sb.Append("<div class='p'>");
            sb.Append("<div class='objectives'>"); //this is the div jstree is looking for
            sb.Append("<ul>");

            sb.Append("<li class='course-objectives jstree-open'>");
            sb.Append(Course.Objectives);
            sb.Append("<ul class='subsection'>");

            bool hasSubObjectives = false;

            foreach (ArtisanSection subsection in Course.Sections)
            {
                if ((ArtisanSectionType)subsection.SectionType == ArtisanSectionType.Objectives)
                {
                    // ignore the objectives when building the objectives
                    continue;
                }
                bool sectionHasObjective = BuildObjectives(subsection, sb);
                if (sectionHasObjective) hasSubObjectives = true;
            }
            sb.Append("</ul>");
            sb.Append("</li>"); //course objective
            sb.Append("</ul>");
            sb.Append("</div>");
            sb.Append("</div>");
            if (!hasSubObjectives)
            {
                // if only the course objectives is set, hide the tree icon
                sb.Append(@"<script type='text/javascript'>
                                $('div.objectives').bind('loaded.jstree', function (event, data) {
	                                $('li.course-objectives > ins').remove();
                                });
                            </script>");
            }
            sb.Append(@"<script type='text/javascript'>

                        $('div.objectives').bind('loaded.jstree', function (event, data) {
                            $('li.course-objectives li:not(.course,.sco,.learningobject)').removeClass('jstree-leaf jstree-last').addClass('jstree-ignore');
                            $('li.course-objectives ul:not(.subsection)').addClass('jstree-ignore');
                        });

                        $('div.objectives').jstree({ 
                            'themes' : {
			                    'theme' : 'classic',
			                    'dots' : true,
			                    'icons' : false
		                    },
		                    'plugins' : [ 'themes', 'html_data' ]
	                    });
                        </script>");

            // create a page with the objectives html
            if (section.Pages == null)
            {
                section.Pages = new List<ArtisanPage>();
            }
            section.Pages.Add(new ArtisanPage()
            {
                Name = "Objectives",
                Html = sb.ToString(),
                CourseId = Course.Id,
                PageType = (int)ArtisanPageType.Objectives
            });
        }

        private bool BuildObjectives(ArtisanSection section, StringBuilder objectives)
        {
            if (String.IsNullOrEmpty(section.Objectives))
                return false;

            bool hasSubSCOs = (section.Sections == null ? false : section.Sections.Any(s => { return (s.SectionType == (int)ArtisanSectionType.Sco); }));
            switch (section.SectionType)
            {
                case (int)ArtisanSectionType.Sco:
                    objectives.Append("<li class='sco " + (hasSubSCOs ? "jstree-open" : "") + "'>");
                    break;
                case (int)ArtisanSectionType.LearningObject:
                    objectives.Append("<li class='learningobject'>");
                    break;
                default:
                    objectives.Append("<li class='course'>");
                    break;
            }
            objectives.Append(section.Objectives);

            if (section.Sections != null)
            {
                objectives.Append("<ul class='subsection'>");
                foreach (ArtisanSection subsection in section.Sections)
                {
                    BuildObjectives(subsection, objectives);
                }
                objectives.Append("</ul>");
            }

            objectives.Append("</li>");

            return true;
        }

        private void BuildIntroPage(ArtisanSection section)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<h3>");
            sb.Append(section.Name);
            sb.Append("</h3>");
            sb.Append("<div class='p'>");
            sb.Append(section.Description);
            sb.Append("</div>");

            // create a page with the section introduction html
            if (section.Pages == null)
            {
                section.Pages = new List<ArtisanPage>();
            }
            section.Pages.Insert(0, new ArtisanPage()
            {
                Name = "Introduction",
                Html = sb.ToString(),
                CourseId = Course.Id,
                PageType = (int)ArtisanPageType.Introduction
            });
        }

        private void BuildSection(ArtisanSection section, int parentId, string path)
        {
            switch ((ArtisanSectionType)section.SectionType)
            {
                case ArtisanSectionType.Sco:
                    if (section.Sections == null || !section.Sections.Any())
                    {
                        throw new Exception("SCO '" + section.Name + "' must contain at least one learning object or sub-SCO.");
                    }
                    break;
                case ArtisanSectionType.LearningObject:
                    if (section.Pages == null || !section.Pages.Any())
                    {
                        throw new Exception("Learning object '" + section.Name + "' must contain at least one page.");
                    }
                    if (section.Pages.Any(p => p.IsGradable) && !section.IsQuiz)
                    {
                        hasGradeableInlineQuestions = true;
                    }
                    if (section.IsQuiz)
                    {
                        BuildArtisanQuiz(section);
                    }
                    break;
            }

            path = Path.Combine(path, section.Id.ToString());

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (section.Sections != null)
            {
                foreach (ArtisanSection subsection in section.Sections)
                {
                    BuildSection(subsection, section.Id, path);
                }
            }
            else if (section.Pages != null)
            {
                foreach (ArtisanPage page in section.Pages)
                {
                    // Only build the page if it hasn't already been built
                    if (!page.Processed)
                    {
                        BuildPage(page, section.Id, path);
                        page.Processed = true;
                    }
                }
                if (section.IsSinglePage)
                {

                    ArtisanPage combinedPage = new ArtisanPage()
                    {
                        Id = section.Pages[0].Id, // Use the first section page id for this page
                                                  // that way bookmarking should work properly
                        SectionID = section.Id,
                        CourseId = Course.Id,
                        Processed = true,
                        Html = "",
                        PageType = (int)ArtisanPageType.Question, // For now, if it works. Can't use content as the wrapper will be lost. 
                                                                 // If weird stuff is happening, consider creating a new page type
                        IsGroup = true
                    };

                    section.CombinedPages = new List<ArtisanPage>();

                    foreach (ArtisanPage page in section.Pages)
                    {
                        combinedPage.Html = combinedPage.Html + page.Html;
                        section.CombinedPages.Add(page);
                    }

                    section.Pages = new List<ArtisanPage>(){combinedPage};
                }
            }
        }

        private void BuildPage(ArtisanPage page, int parentId, string path)
        {
            if (page.QuestionType > 0)
            {
                page.Html = QuestionTemplate.GetTemplate(page);
            }
            else if (page.PageType == (int)ArtisanPageType.Survey)
            {
                page.Html = SurveyTemplate.GetTemplate(page);
            }

            page.Html = ProcessHtmlAssets(page.Html);
            page.CorrectResponse = ProcessHtmlAssets(page.CorrectResponse);
            page.IncorrectResponse = ProcessHtmlAssets(page.IncorrectResponse);
            builtPages.Add(page.Id);
        }

        /// <summary>
        /// Looks through the html content for any assets (href, src, movie, etc), and copies it into the packaged course. It then renames the inline
        /// html references to point to the local copy. As of 2/19/15, it also strips out any references that attempt to pull in the entire jQuery library
        /// </summary>
        /// <param name="html">The HTML to parse</param>
        /// <returns>The updated HTML</returns>
        private string ProcessHtmlAssets(string html)
        {
            if (string.IsNullOrEmpty(html))
            {
                return html;
            }
            string assetsFolderName = "assets";
            string inputFolder = (new SymphonyController()).GetArtisanAssetFilesLocation();
            string outputFolder = Path.Combine(this.ContentFolder, assetsFolderName);

            // for some reason, people have embedded jQuery directly inline in some content
            // normally I don't care what they do to the course, it's wide open,
            // but reloading jQuery can cause all kinds of havoc, specifically causing custom events to 
            // no longer fire which means menus won't rebuild; extremely annoying. As such, all jquery 
            // references get pulled.
            Regex jQueryFinder = new Regex(@"<script(.+?)/jquery-(\d+|\.)+(\.min)?\.js.+?</script>");
            html = jQueryFinder.Replace(html, string.Empty);

            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);
            }

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);
            // images, iframes, flash, etc; grab any nodes that contain urls and rearrange them
            foreach (string selector in new string[] { "//*[@src]", "//*[@href]", "//*[@name='movie' and @value]", "//*[@name='movie' and @src]", "//*[@name='Movie' and @value]", "//*[@name='Movie' and @src]", "//*[@name='Src' and @value]" })
            {
                HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(selector);
                if (nodes != null)
                {
                    foreach (HtmlNode node in nodes)
                    {
                        HtmlAttribute attribute = node.Attributes["href"] ?? node.Attributes["src"] ?? node.Attributes["value"] ?? node.Attributes["Src"] ?? node.Attributes["Value"];
                        if (attribute != null)
                        {
                            // ok, we have the path in the attribute value; need to determine the file path for that,
                            // ensure it exists in our file system, and make sure the same path works in the packaged course
                            // also, trash newlines, the most common error when people are copy/pasting image urls around
                            string url = attribute.Value.Replace("\n", "");
                            if (!url.StartsWith("http") && !url.StartsWith("#"))
                            {
                                string hash = string.Empty;
                                if (url.IndexOf("#") > -1)
                                {
                                    // allow hash to persist
                                    hash = url.Substring(url.IndexOf("#"));
                                }
                                // not a remote url, so it's on our local system somewhere
                                // get the file name, and the input folder based on the upload directory
                                string file = Path.GetFileName(url);

                                // file will be something like "8.jpg" for "normal" assets, or "index.html" for grouped assets
                                string source = Path.Combine(inputFolder, file);

                                if (File.Exists(source))
                                {
                                    // add it to the output for this course
                                    string outFile = Path.Combine(outputFolder, file);
                                    File.Copy(source, outFile, true);

                                    // update the final url
                                    if (this.UseQualifiedLocalFilePaths)
                                    {
                                        attribute.Value = outFile;
                                    }
                                    else
                                    {
                                        attribute.Value = this.ContentFolderName + "/" + assetsFolderName + "/" + file;
                                    }

                                    if (node.Name == "iframe")
                                    {

                                        HtmlAttribute pdfViewerSrc = node.Attributes["data-pdfviewersrc"];
                                        if (pdfViewerSrc != null)
                                        {
                                            attribute.Value = pdfViewerSrc.Value + attribute.Value;
                                            this.Course.IsRequirePdfJs = true;
                                        }
                                    }
                                    
                                    attribute.Value += hash;

                                    // Inject alternate content if necessary
                                    ProcessAlternateContent(node, file);
                                }
                                else
                                {
                                    // it's not a simple asset; take a look and see if the parent asset folder exists
                                    // and if so, it's a grouped asset and we need to copy the whole folder over
                                    try
                                    {
                                        string fullWebDirectory = Path.GetDirectoryName(url);
                                        string subdirectory = fullWebDirectory.Substring(fullWebDirectory.LastIndexOf("\\") + 1);
                                        string fullFileDirectory = Path.Combine(inputFolder, subdirectory);
                                        source = Path.Combine(fullFileDirectory, file);

                                        if (File.Exists(source))
                                        {
                                            string targetDirectory = Path.Combine(outputFolder, subdirectory);
                                            if (!Directory.Exists(targetDirectory))
                                            {
                                                Directory.CreateDirectory(targetDirectory);
                                            }
                                            Utilities.CopyFolder(fullFileDirectory, targetDirectory);

                                            // update the final url
                                            attribute.Value = this.ContentFolderName + "/" + assetsFolderName + "/" + subdirectory + "/" + file;

                                            attribute.Value += hash;

                                            // Inject alternate content if necessary
                                            ProcessAlternateContent(node, subdirectory);
                                        }
                                        else
                                        {
                                            Log.Warn("File at '" + source + "' does not exist; this is *not* a critical failure.");
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Log.Warn("File at '" + source + "' does not exist; this is *not* a critical failure.", e);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return doc.DocumentNode.WriteTo();
        }

        // itemName is the file or folder name that has the potential to be an asset ID
        private void ProcessAlternateContent(HtmlNode node, string itemName)
        {
            if (node.Name == "embed")
            {
                return; // Flash asset have 2 urls, need to skip one of them
            }

            try
            {
                int assetId = Convert.ToInt32(Path.GetFileNameWithoutExtension(itemName));
                var asset = new Data.ArtisanAsset(assetId);
                var assetType = new Data.ArtisanAssetType(asset.AssetTypeID);

                // Make sure this asset type supports alternate content
                if (assetType.HasAlternateHtml && asset.AlternateHtml != null)
                {
                    var altDoc = new HtmlDocument();
                    altDoc.LoadHtml(asset.AlternateHtml);
                    // Strip off all tags and then remove all whitespace
                    string text = Regex.Replace(altDoc.DocumentNode.InnerHtml, @"\s+", "");
                    if (!string.IsNullOrEmpty(text))
                    {
                        HtmlNode assetNode = (node.Name == "param" && node.ParentNode.Name == "object" ? node.ParentNode : node);
                        string alternateHtml = ProcessHtmlAssets(asset.AlternateHtml);
                        var newNode = HtmlNode.CreateNode(string.Format("<div class='artisan-asset-alternate'>{0}</div>", alternateHtml));
                        assetNode.ParentNode.AppendChild(newNode);

                        string assetClass = "artisan-asset-primary";
                        // If there already is a class, append new one
                        string classAttr = assetNode.GetAttributeValue("class", null);
                        if (classAttr != null)
                        {
                            assetClass = string.Format("{0} {1}", classAttr, assetClass);
                        }
                        assetNode.SetAttributeValue("class", assetClass);
                    }
                }
            }
            catch { }
        }

        private string ReplaceParameters(Match m, Dictionary<string, ParameterValue> parameterDictionary, string paramTemplate)
        {
            string code = m.Captures[0].ToString();
            string replacement;

            if (parameterDictionary.Keys.Contains(code))
            {
                replacement = parameterDictionary[code].Value;
            }
            else
            {
                replacement = code;
            }

            string result = string.Format(paramTemplate, code, System.Web.HttpUtility.HtmlEncode(replacement));

            result = Utilities.Serialize(result);
            result = result.Substring(1, result.Length - 2);

            return result;
        }
        private void BuildArtisanCourseJson()
        {
            if (this.Course != null)
            {
                string path = Path.Combine(this.ContentFolder, "artisan.coursejson.js");

                if (this.Course.DefaultParameters != null)
                {
                    // We only want to operate on course sections
                    // since if a parameter appears in a different place
                    // in the json object it shouldn't be replaced.

                    string sectionJson = Utilities.Serialize(this.Course.Sections);

                    Regex regex = new Regex(@"{!([a-z0-9_]+)}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                    string paramTemplate = "<span class='artisan-parameter' data-code='{0}'>{1}</span>";

                    Dictionary<string, ParameterValue> parameterDictionary = this.Course.DefaultParameters.ToDictionary(p => p.Code);

                    sectionJson = regex.Replace(sectionJson, new MatchEvaluator(m => ReplaceParameters(m, parameterDictionary, paramTemplate)));

                    this.Course.Sections = Utilities.Deserialize<List<ArtisanSection>>(sectionJson);
                }

                string courseJson = Utilities.Serialize(this.Course);

                using (StreamWriter writer = new StreamWriter(path, false))
                {
                    writer.WriteLine("window.Artisan = window.Artisan || {};");
                    writer.WriteLine("window.Artisan.CourseJson = " + courseJson);
                }
            }
        }

        private void BuildSCORMControls()
        {
            Scorm12ManifestBuilder builder = new Scorm12ManifestBuilder();
            string manifest = builder.Build(Course);
            File.WriteAllText(Path.Combine(RootFolder, "imsmanifest.xml"), manifest);
        }
    }
}
