﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Generators
{
    static class QuestionTemplate
    {
        public static string GetTemplate(ArtisanPage page)
        {
            string html = @"<h3 class='artisan-question-header'>
                                <span class='artisan-question-name'>{0}</span> 
                                <span class='artisan-question-type artisan-question-type-{2}'>
                                    <span class='artisan-question-type-brackets'>(</span><span class='artisan-question-type-text'>{1}</span><span class='artisan-question-type-brackets'>)</span>
                                </span>
                            </h3>
                            <div class='artisan-question artisan-question-{2} p artisan-question-pageid-{6}'>
                                <div class = 'artisan-question-text'>
                                    {3}
                                </div>
                                {4}
                                <div id='artisan_answer_feedback'></div>
                                {5}
                                <input type='button' id='artisan-question-submit' value='Submit' data-pageid='{6}' />
                                <div class='gameboard-button'>Gameboard</div>
                            </div>";
                        
            var er = new EnumRecord(typeof(ArtisanQuestionType), false);
            string cssClass = er.GetCodeName<ArtisanQuestionType>(page.QuestionType);

            return string.Format(html, page.Name, er.GetDisplayName<ArtisanQuestionType>(page.QuestionType), cssClass, page.Html, AnswerTemplate.GetTemplate(page), AnswerTemplate.GetOptions(page), page.Id);
        }
    }
}
