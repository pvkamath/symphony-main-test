using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Collections;
using System.Text.RegularExpressions;
using BankersEdge.Artisan;

namespace BankersEdge.Artisan
{
    //these need to match the values in the db
    public struct Navigation
    {
        public static string FREEFORM = "freeform";
        public static string SEQENTIAL = "sequential";
    }

    public struct Completion
    {
        public static string NAVIGATION_ONLY = "navonly";
        public static string NAVIGATION_AND_TEST = "navplustest";
        public static string TEST_ONLY = "testonly";
        public static string INLINE_QUESTIONS_ONLY = "inlinequestonly";
        public static string NAVIGATION_AND_QUESTIONS = "navplusinline";
    }

    public struct TestType
    {
        public static string JEOPARDY = "jeopardy";
        public static string SEQUENTIAL = "seuential";
        public static string RANDOM = "random";
    }

    public struct ScormSettings
    {
        public static string ORGANIZATION_NAME = "ORG-BankersEdge";
        public static string IMSMANIFEST_FILENAME = "imsManifest.xml";
        public static string SHARED_FILES_ID = "SharedFiles";
        public static string PRETEST_SCO_ID = "pretest";
        public static string POSTTEST_SCO_ID = "posttest";
    }

    public class SCORM2k4Options
    {
        private bool m_bPostTest;
        private bool m_bPreTest;
        private string m_sCompletionType;
        private string m_sNavigationType;
        private string m_sOutputFile;
        private int m_iCourseID;
        private string m_sPostTestTitle = "Post-Test";
        private string m_sPreTestTitle = "Pre-Test";
        private string m_sSharedFolderPath;
        private string m_sContentFolderPath;
        private int m_iPostTestScore;
        private int m_iPreTestScore;
        private bool m_bTestOut = false;
        private string m_sPreTestType = "";
        private string m_sPostTestType = "";
        private bool m_bDisplayTestOnly = false;
        private bool m_bDisplaySummary = true;

        public bool TestOnly
        {
            get { return m_bDisplayTestOnly; }
            set { this.m_bDisplayTestOnly = value; }
        }

        public string ContentFolderPath
        {
            get { return this.m_sContentFolderPath; }
            set { this.m_sContentFolderPath = value; }
        }

        public bool TestOut
        {
            get { return this.m_bTestOut; }
            set { this.m_bTestOut = value; }
        }

        public string PostTestType
        {
            get { return this.m_sPostTestType; }
            set { this.m_sPostTestType = value; }
        }

        public string PreTestType
        {
            get { return this.m_sPreTestType; }
            set { this.m_sPreTestType = value; }
        }

        public int PostTestScore
        {
            get { return this.m_iPostTestScore; }
            set { this.m_iPostTestScore = value; }
        }

        public int PreTestScore
        {
            get { return this.m_iPostTestScore; }
            set { this.m_iPreTestScore = value; }
        }

        public string SharedFolderPath
        {
            get { return this.m_sSharedFolderPath; }
            set { this.m_sSharedFolderPath = value; }
        }

        public string PostTestTitle
        {
            get { return this.m_sPostTestTitle; }
            set { this.m_sPostTestTitle = value; }
        }

        public string PreTestTitle
        {
            get { return this.m_sPreTestTitle; }
            set { this.m_sPreTestTitle = value; }
        }

        public bool PreTest
        {
            get { return this.m_bPreTest; }
            set { this.m_bPreTest = value; }
        }

        public bool PostTest
        {
            get { return this.m_bPostTest; }
            set { this.m_bPostTest = value; }
        }

        public string CompletionType
        {
            get { return this.m_sCompletionType; }
            set { this.m_sCompletionType = value; }
        }

        public string NavigationType
        {
            get { return this.m_sNavigationType; }
            set { this.m_sNavigationType = value; }
        }

        public int CourseID
        {
            get { return this.m_iCourseID; }
            set { this.m_iCourseID = value; }
        }

        public string File
        {
            get { return this.m_sOutputFile; }
            set { this.m_sOutputFile = value; }
        }

        
        public bool DisplaySummary
        {
            get { return this.m_bDisplaySummary; }
            set {  this.m_bDisplaySummary = value; }
        }
    }


    /// <summary>
    /// Summary description for SCORM2k5Builder
    /// </summary>
    public class SCORM2k4Builder
    {
        private TreeNode m_cRootNode;
        private XmlTextWriter xw;
        private SCORM2k4Options m_cOptions;

        private string sCompletionType = "";
        private string sNavType = "";
        private string sPrevTargetObjective = "";
        private bool m_bFirstSCO = true;

        private string NAMESPACE = "com.scorm.bankersedge.";
        private string PREFIX = "sco";
        private string POSTFIX = "_satisfied";

        private string ASSET_FOLDER_NAME = "assets";

        private string POSTTEST_CONTENT_GROUP = "contentgroup";
        private string POSTTEST_CONTENT_OBJECTIVEID = "content_completed";
        private string PRETEST_CONTENT_OBJECTIVEID = "pretest_completed";
        private string POSTTEST_GLOBAL_ID = "com.scorm.bankersedge.content_completed";

        private string PRETEST_GLOBAL_ID = "com.scorm.bankersedge.pretest_satisfaction";
        private string PRETEST_COURSE_SATISFACTION_ID = "com.scorm.bankersedge.test_satisfaction";

        private string ID_DELIM = "-";
        private int DEFAULT_SCO_TYPE = 1;

        //due to a bug in playback in Rustici (and probably other LMSes as well), you can't assign a weight of "0"
        //to a SCO because you get a divide by zero. Sooo...we assign a very small weight so it doesn't impact anything else.
        //i'm going with 1/100th to try and come up with a reasonable balance...i expect some LMSes will limit scores to 1/100th,
        //which may cause issues if this is < 1/100th, and 1/100th should still be small enough to not matter (this means, theoretically,
        //if there are 51 scos + a post-test, they fail all 51 scos, but pass the post-test with 100, they'll get a 99 instead of 100...
        //but that's a pretty edge case i think we can live with.
        //1/100th in this case = 0.0001, because 1 = 100%
        private string EMPTY_WEIGHT = "0.0001";

        public void CreateXML(TreeNode cRootNode, SCORM2k4Options cOptions)
        {
            this.m_cOptions = cOptions;
            this.m_cRootNode = cRootNode;

            //next, write out all the SCOs...
            Course cCourse = new Course(m_cOptions.CourseID);

            //basic xml stuff
            xw = new XmlTextWriter(m_cOptions.File, Encoding.Unicode);
            xw.Indentation = 1;
            xw.IndentChar = '\t';
            xw.WriteStartDocument();
            xw.Formatting = Formatting.Indented;

            StartHeader(cCourse.CourseName, cCourse.CourseDescription);

            //objectives, sco descriptions, etc
            if (cOptions.DisplaySummary)
            {
                WriteSummarySCO();
            }

            if (!cOptions.TestOnly)
            {
                if (cOptions.PreTest)
                {
                    WritePreTest();
                }

                //post/pre-tests mean we need a separate aggregate group for the non-test stuff
                if (cOptions.PostTest || cOptions.PreTest)
                {
                    //starts the content group
                    StartGroup("Content Group", "content_aggregate", false);
                }

                //write out the content scos
                WriteSCORecursive(cRootNode);


                //as before, we have an aggregate group that needs closing now.
                if (cOptions.PostTest || cOptions.PreTest)
                {
                    //close out the content aggregate, and then write the post-test
                    WriteSequencingForContentGroup();

                    EndGroup(); //content group
                }
            }

            if (cOptions.PostTest)
            {
                WritePostTest();
            }

            if (!cOptions.TestOnly)
            {
                if (cOptions.TestOut)
                {
                    WriteDummySCO();
                }

                if ((cOptions.CompletionType == Completion.NAVIGATION_AND_QUESTIONS || cOptions.CompletionType == Completion.INLINE_QUESTIONS_ONLY) && !cOptions.PostTest)
                {
                    WriteInlineQuestionSequencingForContentGroup();

                    //EndGroup(); //content group
                }
                else if (cOptions.TestOut && !cOptions.TestOnly)
                {
                    WritePrePlusPostTestSequencing();
                }
                else
                {
                    WriteAutoForwardSequence(); //makes the scos auto-select next, and handles navigation types of freeform vs. sequential
                }
            }
            else
            {
                WriteAutoForwardSequence();
            }

            EndHeader();

            WriteResources(cRootNode);

            xw.Flush();
            xw.Close();
        }

        private void StartHeader(string sPrimaryTitle, string sDescription)
        {
            //begin the manifest file
            xw.WriteStartElement("manifest");
            xw.WriteAttributeString("identifier", "LMSPackageBankersEdge");
            xw.WriteAttributeString("version", "1.0");
            xw.WriteAttributeString("xmlns", "http://www.imsglobal.org/xsd/imscsw00p_v1p1");
            xw.WriteAttributeString("xmlns:adlcp", "http://www.adlnet.org/xsd/adlcp_v1p3");
            xw.WriteAttributeString("xmlns:adlseq", "http://www.adlnet.org/xsd/adlseq_v1p3");
            xw.WriteAttributeString("xmlns:adlnav", "http://www.adlnet.org/xsd/adlnav_v1p3");
            xw.WriteAttributeString("xmlns:imsss", "http://www.imsglobal.org/xsd/imsss");
            xw.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

            //stuff about the manifest file itself
            xw.WriteStartElement("metadata");
            xw.WriteStartElement("schema");
            xw.WriteString("ADL SCORM");
            xw.WriteEndElement(); //end schema
            xw.WriteStartElement("schemaversion");
            xw.WriteString("2004 3rd Edition");
            xw.WriteEndElement(); //end schema version
            xw.WriteEndElement(); //end metadata

            //ok, now, build the packages. first, add the org stuff...
            xw.WriteStartElement("organizations");
            xw.WriteAttributeString("default", ScormSettings.ORGANIZATION_NAME);
            xw.WriteStartElement("organization");
            xw.WriteAttributeString("identifier", ScormSettings.ORGANIZATION_NAME);
            xw.WriteAttributeString("adlseq:objectivesGlobalToSystem", "false");

            //then add the title for the package...
            xw.WriteStartElement("title");
            xw.WriteString(sPrimaryTitle);
            xw.WriteEndElement(); //end title

            //and a description
            xw.WriteStartElement("description");
            xw.WriteString(sDescription);
            xw.WriteEndElement(); //end title
        }

        private void WriteSummarySCO()
        {
            xw.WriteStartElement("item");

            xw.WriteAttributeString("identifier", "0");
            xw.WriteAttributeString("identifierref", "res-0");
            xw.WriteAttributeString("parameters", "?id=objectives");

            xw.WriteStartElement("title");
            xw.WriteString("Objectives");
            xw.WriteEndElement(); //end title

            xw.WriteStartElement("imsss:sequencing");

            xw.WriteStartElement("imsss:rollupRules");
            xw.WriteAttributeString("rollupObjectiveSatisfied", "false");
            xw.WriteAttributeString("rollupProgressCompletion", "false");
            xw.WriteAttributeString("objectiveMeasureWeight", EMPTY_WEIGHT); //objectives sco has no weight
            xw.WriteEndElement(); //end rollup rules

            //delivery controls
            xw.WriteStartElement("imsss:deliveryControls");
            xw.WriteAttributeString("tracked", "true");
            xw.WriteAttributeString("completionSetByContent", "true");
            xw.WriteAttributeString("objectiveSetByContent", "true");
            xw.WriteEndElement(); //delivery controls

            xw.WriteEndElement(); //end sequencing

            xw.WriteEndElement();
        }

        private void EndHeader()
        {
            xw.WriteEndElement(); //organization
            xw.WriteEndElement(); //organizations
        }

        private void StartGroup(string sTitle)
        {
            StartGroup(sTitle, "", true);
        }

        private void StartGroup(string sTitle, string sID, bool bVisible)
        {
            xw.WriteStartElement("item");
            if (!string.IsNullOrEmpty(sID))
                xw.WriteAttributeString("identifier", sID);
            if (!bVisible)
                xw.WriteAttributeString("isvisible", "false");

            xw.WriteStartElement("title");
            xw.WriteString(sTitle);
            xw.WriteEndElement(); //end title
        }

        private void EndGroup()
        {

            xw.WriteEndElement(); //ends an item tag
        }

        private void WriteSequencingForContentGroup()
        {
            xw.WriteStartElement("imsss:sequencing");

            if (this.m_cOptions.PreTest && this.m_cOptions.TestOut)
            {
                WriteHideFromChoiceSequence();
            }

            xw.WriteStartElement("imsss:controlMode");
            if (this.m_cOptions.NavigationType == Navigation.FREEFORM)
            {
                xw.WriteAttributeString("choice", "true");
            }
            else
            {
                xw.WriteAttributeString("choice", "false");
            }

            xw.WriteAttributeString("flow", "true");
            xw.WriteEndElement(); //control mode

            xw.WriteStartElement("imsss:rollupRules");
            xw.WriteAttributeString("rollupObjectiveSatisfied", "false");
            xw.WriteAttributeString("rollupProgressCompletion", "false");
            xw.WriteAttributeString("objectiveMeasureWeight", EMPTY_WEIGHT);
            xw.WriteEndElement(); //rollup rules

            xw.WriteStartElement("imsss:objectives");
            xw.WriteStartElement("imsss:primaryObjective");
            xw.WriteAttributeString("objectiveID", POSTTEST_CONTENT_OBJECTIVEID);

            xw.WriteStartElement("imsss:mapInfo");
            xw.WriteAttributeString("targetObjectiveID", POSTTEST_GLOBAL_ID);
            xw.WriteAttributeString("readSatisfiedStatus", "false");
            xw.WriteAttributeString("writeSatisfiedStatus", "true");
            xw.WriteEndElement();//mapinfo

            xw.WriteEndElement(); //primary objective

            if (this.m_cOptions.PreTest && this.m_cOptions.TestOut)
            {
                xw.WriteStartElement("imsss:objective");
                xw.WriteAttributeString("objectiveID", PRETEST_CONTENT_OBJECTIVEID);
                xw.WriteStartElement("imsss:mapInfo");
                xw.WriteAttributeString("targetObjectiveID", PRETEST_GLOBAL_ID);
                xw.WriteAttributeString("readSatisfiedStatus", "true");
                xw.WriteAttributeString("writeSatisfiedStatus", "false");
                xw.WriteEndElement();
                xw.WriteEndElement();
            }

            xw.WriteEndElement(); //objectives

            xw.WriteEndElement(); //sequencing
        }

        private void WritePreTest()
        {
            xw.WriteStartElement("item");

            xw.WriteAttributeString("identifier", ScormSettings.PRETEST_SCO_ID);
            xw.WriteAttributeString("identifierref", "res-" + ScormSettings.PRETEST_SCO_ID);
            xw.WriteAttributeString("parameters", "?id=" + ScormSettings.PRETEST_SCO_ID);

            xw.WriteStartElement("title");
            xw.WriteString(m_cOptions.PreTestTitle);
            xw.WriteEndElement(); //end title

            xw.WriteStartElement("imsss:sequencing");

            xw.WriteStartElement("imsss:sequencingRules");

            xw.WriteStartElement("imsss:preConditionRule");

            xw.WriteStartElement("imsss:ruleConditions");

            xw.WriteStartElement("imsss:ruleCondition");
            xw.WriteAttributeString("condition", "attemptLimitExceeded");
            xw.WriteEndElement(); //end rule condition

            xw.WriteEndElement(); //end rule conditions

            xw.WriteStartElement("imsss:ruleAction");
            xw.WriteAttributeString("action", "hiddenFromChoice");
            xw.WriteEndElement(); //rule action

            xw.WriteEndElement(); //end precondition rule

            xw.WriteEndElement(); //end sequencing rules

            //limit conditions
            xw.WriteStartElement("imsss:limitConditions");
            xw.WriteAttributeString("attemptLimit", "1");
            xw.WriteEndElement();

            //rollup rules
            xw.WriteStartElement("imsss:rollupRules");
            xw.WriteAttributeString("rollupObjectiveSatisfied", this.m_cOptions.TestOut.ToString().ToLower());
            xw.WriteAttributeString("rollupProgressCompletion", this.m_cOptions.TestOut.ToString().ToLower());

            xw.WriteAttributeString("objectiveMeasureWeight", EMPTY_WEIGHT);
            xw.WriteEndElement();

            //objectives
            xw.WriteStartElement("imsss:objectives");

            xw.WriteStartElement("imsss:primaryObjective");
            xw.WriteAttributeString("objectiveID", "SCO_PreTest");
            xw.WriteAttributeString("satisfiedByMeasure", "true");


            xw.WriteStartElement("imsss:minNormalizedMeasure");
            xw.WriteValue(((double)this.m_cOptions.PreTestScore / 100.0).ToString("f2")); 
            xw.WriteEndElement(); //minNormalizedMeasure

            //test-out option
            if (this.m_cOptions.TestOut)
            {
                WriteMapToDummySCO(false);

                WritePretestObjective();
            }

            xw.WriteEndElement(); //primary objective

            xw.WriteEndElement(); //objectives

            //delivery controls
            xw.WriteStartElement("imsss:deliveryControls");
            xw.WriteAttributeString("tracked", "true");
            xw.WriteAttributeString("completionSetByContent", "true");
            xw.WriteAttributeString("objectiveSetByContent", "true");
            xw.WriteEndElement(); //delivery controls

            xw.WriteEndElement(); //end sequencing

            xw.WriteEndElement(); //end item
        }

        private void WriteMapToDummySCO(bool bIsDummySCO)
        {
            //in this case, we need to set a global objective for course satisfaction and scoring, AND
            //we need a global objective for enabling/disabling by the post test
            //the dummy sco reads statuses, the other test scos write them.
            xw.WriteStartElement("imsss:mapInfo");
            xw.WriteAttributeString("targetObjectiveID", PRETEST_COURSE_SATISFACTION_ID);
            xw.WriteAttributeString("readSatisfiedStatus", bIsDummySCO ? "true" : "false");
            xw.WriteAttributeString("writeSatisfiedStatus", bIsDummySCO ? "false" : "true"); //true in post. false in pre
            xw.WriteAttributeString("writeNormalizedMeasure", bIsDummySCO ? "false" : "true");
            xw.WriteAttributeString("readNormalizedMeasure", bIsDummySCO ? "true" : "false");
            xw.WriteEndElement();

        }

        private void WritePretestObjective()
        {
            xw.WriteStartElement("imsss:mapInfo");
            xw.WriteAttributeString("targetObjectiveID", PRETEST_GLOBAL_ID);
            xw.WriteAttributeString("readSatisfiedStatus", "false");
            xw.WriteAttributeString("writeSatisfiedStatus", "true");
            xw.WriteAttributeString("readNormalizedMeasure", "false");
            xw.WriteAttributeString("writeNormalizedMeasure", "true");
            xw.WriteEndElement();
        }

        private void WriteDummySCO()
        {
            xw.WriteStartElement("item");

            xw.WriteAttributeString("identifier", "dummy_sco");
            xw.WriteAttributeString("identifierref", "res-dummy_sco");
            xw.WriteAttributeString("isvisible", "false");

            xw.WriteStartElement("title");
            xw.WriteString("Dummy");
            xw.WriteEndElement(); //end title

            xw.WriteStartElement("imsss:sequencing");

            //more sequencing
            xw.WriteStartElement("imsss:sequencingRules");

            xw.WriteStartElement("imsss:preConditionRule");

            xw.WriteStartElement("imsss:ruleConditions");

            xw.WriteStartElement("imsss:ruleCondition");
            xw.WriteAttributeString("condition", "always");
            xw.WriteEndElement(); //rule condition

            xw.WriteEndElement(); //end rule conditions

            xw.WriteStartElement("imsss:ruleAction");
            xw.WriteAttributeString("action", "skip");
            xw.WriteEndElement(); //rule action

            xw.WriteEndElement(); //pre condition rule
            xw.WriteEndElement(); //sequencing rules

            //- <!-- In order for the correct value for a single test to be sent to the parent activity, this is the only activity
            //          with an objectiveMeasureWeight above 0.  So the current value of the global test_satisfaction objective
            //          is the only one known to the parent course-level activity.
            //  --> 
            xw.WriteStartElement("imsss:rollupRules");
            xw.WriteAttributeString("rollupObjectiveSatisfied", "false");
            xw.WriteAttributeString("rollupProgressCompletion", "false");
            xw.WriteAttributeString("objectiveMeasureWeight", "1.0000");
            xw.WriteEndElement(); //rollup rules


            xw.WriteStartElement("imsss:objectives");

            xw.WriteStartElement("imsss:primaryObjective");
            xw.WriteAttributeString("objectiveID", "test_satisfied");
            xw.WriteAttributeString("satisfiedByMeasure", "true");

            xw.WriteStartElement("imsss:minNormalizedMeasure");
            xw.WriteValue(((double)this.m_cOptions.PostTestScore / 100.0).ToString("f2"));
            xw.WriteEndElement(); //minNormalizedMeasure

            WriteMapToDummySCO(true);

            xw.WriteEndElement(); //primary objective

            xw.WriteEndElement(); //objectives

            xw.WriteStartElement("imsss:deliveryControls");
            xw.WriteAttributeString("tracked", "true");
            xw.WriteAttributeString("completionSetByContent", "true");
            xw.WriteAttributeString("objectiveSetByContent", "true");
            xw.WriteEndElement(); //end delivery controls

            xw.WriteEndElement(); //end sequencing

            xw.WriteEndElement(); //end item
        }

        private void WriteInlineQuestionSequencingForContentGroup()
        {
            xw.WriteStartElement("imsss:sequencing");

            xw.WriteStartElement("imsss:controlMode");
            xw.WriteAttributeString("choice", "true"); //doesn't really matter here.
            xw.WriteAttributeString("flow", "true");
            xw.WriteEndElement(); //control mode

            xw.WriteStartElement("imsss:rollupRules");

            /* start rollup rule */
            xw.WriteStartElement("imsss:rollupRule");
            xw.WriteAttributeString("childActivitySet", "any");

            xw.WriteStartElement("imsss:rollupConditions");
            xw.WriteAttributeString("conditionCombination", "all");

            xw.WriteStartElement("imsss:rollupCondition");
            xw.WriteAttributeString("condition", "completed");
            xw.WriteEndElement();

            xw.WriteStartElement("imsss:rollupCondition");
            xw.WriteAttributeString("operator", "not");
            xw.WriteAttributeString("condition", "satisfied");
            xw.WriteEndElement();

            xw.WriteEndElement(); //rollupCondition

            xw.WriteStartElement("imsss:rollupAction");
            xw.WriteAttributeString("action", "incomplete");
            xw.WriteEndElement(); //rollup action

            xw.WriteEndElement(); //rollup rule
            /* end rollup rule */

            /* start rollup rule */
            xw.WriteStartElement("imsss:rollupRule");
            xw.WriteAttributeString("childActivitySet", "any");

            xw.WriteStartElement("imsss:rollupConditions");
            xw.WriteAttributeString("conditionCombination", "all");

            xw.WriteStartElement("imsss:rollupCondition");
            xw.WriteAttributeString("condition", "completed");
            xw.WriteEndElement();

            xw.WriteStartElement("imsss:rollupCondition");
            xw.WriteAttributeString("operator", "not");
            xw.WriteAttributeString("condition", "objectiveStatusKnown");
            xw.WriteEndElement();

            xw.WriteEndElement(); //rollupConditions

            xw.WriteStartElement("imsss:rollupAction");
            xw.WriteAttributeString("action", "incomplete");
            xw.WriteEndElement(); //rollup action

            xw.WriteEndElement(); //rollup rule
            /* end rollup rule */

            /* start rollup rule */
            xw.WriteStartElement("imsss:rollupRule");
            xw.WriteAttributeString("childActivitySet", "any");

            xw.WriteStartElement("imsss:rollupConditions");
            xw.WriteAttributeString("conditionCombination", "all");

            xw.WriteStartElement("imsss:rollupCondition");
            xw.WriteAttributeString("operator", "not");
            xw.WriteAttributeString("condition", "completed");
            xw.WriteEndElement();

            xw.WriteEndElement(); //rollupConditions

            xw.WriteStartElement("imsss:rollupAction");
            xw.WriteAttributeString("action", "incomplete");
            xw.WriteEndElement(); //rollup action

            xw.WriteEndElement(); //rollup rule
            /* end rollup rule */

            /* start rollup rule */
            xw.WriteStartElement("imsss:rollupRule");
            xw.WriteAttributeString("childActivitySet", "all");

            xw.WriteStartElement("imsss:rollupConditions");
            xw.WriteAttributeString("conditionCombination", "all");

            xw.WriteStartElement("imsss:rollupCondition");
            xw.WriteAttributeString("condition", "completed");
            xw.WriteEndElement();

            xw.WriteStartElement("imsss:rollupCondition");
            xw.WriteAttributeString("condition", "satisfied");
            xw.WriteEndElement();

            xw.WriteEndElement(); //rollupConditions

            xw.WriteStartElement("imsss:rollupAction");
            xw.WriteAttributeString("action", "completed");
            xw.WriteEndElement(); //rollup action

            xw.WriteEndElement(); //rollup rule
            /* end rollup rule */

            xw.WriteEndElement(); //rollup rules

            xw.WriteEndElement(); //sequencing
        }

        private void WritePrePlusPostTestSequencing()
        {
            xw.WriteStartElement("imsss:sequencing");

            xw.WriteStartElement("imsss:controlMode");
            xw.WriteAttributeString("choice", "true"); //doesn't really matter here.
            xw.WriteAttributeString("flow", "true");
            xw.WriteEndElement(); //control mode

            xw.WriteStartElement("imsss:rollupRules");

            /* start rollup rule */
            xw.WriteStartElement("imsss:rollupRule");
            xw.WriteAttributeString("childActivitySet", "any");

            xw.WriteStartElement("imsss:rollupConditions");

            xw.WriteStartElement("imsss:rollupCondition");
            xw.WriteAttributeString("condition", "satisfied");
            xw.WriteEndElement(); //rollupCondition

            xw.WriteEndElement(); //rollup conditions

            xw.WriteStartElement("imsss:rollupAction");
            xw.WriteAttributeString("action", "completed");
            xw.WriteEndElement(); //rollup action

            xw.WriteEndElement(); //rollup rule
            /* end rollup rule */

            xw.WriteEndElement(); //rollup rules

            xw.WriteStartElement("imsss:objectives");

            xw.WriteStartElement("imsss:primaryObjective");
            xw.WriteAttributeString("objectiveID", "course_satisfied");
            xw.WriteAttributeString("satisfiedByMeasure", "true");

            xw.WriteElementString("imsss:minNormalizedMeasure", ((double)this.m_cOptions.PostTestScore / 100.0).ToString("f2"));

            xw.WriteEndElement(); //primary objective

            xw.WriteEndElement(); //objectives

            xw.WriteEndElement(); //sequencing
        }

        private void WritePostTest()
        {
            xw.WriteStartElement("item");

            xw.WriteAttributeString("identifier", ScormSettings.POSTTEST_SCO_ID);
            xw.WriteAttributeString("identifierref", "res-" + ScormSettings.POSTTEST_SCO_ID);
            xw.WriteAttributeString("parameters", "?id=" + ScormSettings.POSTTEST_SCO_ID);

            xw.WriteStartElement("title");
            xw.WriteString(m_cOptions.PostTestTitle);
            xw.WriteEndElement(); //end title

            xw.WriteStartElement("imsss:sequencing");

            //more sequencing
            if (m_cOptions.TestOut)
            {
                WriteHideFromChoiceSequence();
            }
            //if (m_cOptions.TestOut) //m_cOptions.NavigationType == Navigation.SEQENTIAL || 
            //{
            //    xw.WriteStartElement("imsss:sequencingRules");

            //    xw.WriteStartElement("imsss:preConditionRule");

            //    xw.WriteStartElement("imsss:ruleConditions");

            //    if (m_cOptions.NavigationType == Navigation.SEQENTIAL)
            //    {
            //        xw.WriteAttributeString("conditionCombination", "any");

            //        xw.WriteStartElement("imsss:ruleCondition");
            //        xw.WriteAttributeString("referencedObjective", POSTTEST_CONTENT_OBJECTIVEID);
            //        xw.WriteAttributeString("operator", "not");
            //        xw.WriteAttributeString("condition", "satisfied");
            //        xw.WriteEndElement(); //rulecondition

            //        xw.WriteStartElement("imsss:ruleCondition");
            //        xw.WriteAttributeString("referencedObjective", POSTTEST_CONTENT_OBJECTIVEID);
            //        xw.WriteAttributeString("operator", "not");
            //        xw.WriteAttributeString("condition", "objectiveStatusKnown");
            //        xw.WriteEndElement(); //rulecondition
            //    }

            //    //this rule disables the post test if the pretest is satisfied
            //    if (m_cOptions.TestOut)
            //    {
            //        xw.WriteStartElement("imsss:ruleCondition");
            //        xw.WriteAttributeString("referencedObjective", PRETEST_CONTENT_OBJECTIVEID);
            //        xw.WriteAttributeString("condition", "satisfied");
            //        xw.WriteEndElement(); //rulecondition
            //    }

            //    xw.WriteEndElement(); //rule conditions

            //    xw.WriteStartElement("ruleAction");
            //    //hmmm...we really want the action to be "hiddenFromChoice" in the "testout" scenario, but i dunno how to set multiple actions up
            //    xw.WriteAttributeString("action", "hiddenFromChoice"); 
            //    xw.WriteEndElement(); //ruleaction

            //    xw.WriteEndElement(); //precondition rule

            //    xw.WriteEndElement(); //sequencing rules
            //}

            xw.WriteStartElement("imsss:rollupRules");
            xw.WriteAttributeString("rollupObjectiveSatisfied", "true");
            xw.WriteAttributeString("rollupProgressCompletion", "true");
            //post test always has 100% of the weight, unless the pre-test exists and test-out is enabled
            if (m_cOptions.PreTest && m_cOptions.TestOut && !m_cOptions.TestOnly)
            {
                xw.WriteAttributeString("objectiveMeasureWeight", EMPTY_WEIGHT);
            }
            else
            {
                xw.WriteAttributeString("objectiveMeasureWeight", "1.0000");
            }
            xw.WriteEndElement(); //rollupRules

            //objectives
            xw.WriteStartElement("imsss:objectives");

            xw.WriteStartElement("imsss:primaryObjective");
            xw.WriteAttributeString("objectiveID", "SCO_PostTest");
            xw.WriteAttributeString("satisfiedByMeasure", "true");


            xw.WriteStartElement("imsss:minNormalizedMeasure");
            xw.WriteValue(((double)this.m_cOptions.PostTestScore / 100.0).ToString("f2"));
            xw.WriteEndElement(); //minNormalizedMeasure

            //MAP INFO FOR PRETEST
            WriteMapToDummySCO(false);

            xw.WriteEndElement(); //primary objective


            xw.WriteStartElement("imsss:objective");

            xw.WriteAttributeString("objectiveID", POSTTEST_CONTENT_OBJECTIVEID);
            xw.WriteStartElement("imsss:mapInfo");

            xw.WriteAttributeString("targetObjectiveID", POSTTEST_GLOBAL_ID);
            xw.WriteAttributeString("readSatisfiedStatus", "true");
            xw.WriteAttributeString("writeSatisfiedStatus", "false");

            xw.WriteEndElement(); //mapinfo

            xw.WriteEndElement(); //objective


            if (m_cOptions.TestOut)
            {
                xw.WriteStartElement("imsss:objective");

                xw.WriteAttributeString("objectiveID", PRETEST_CONTENT_OBJECTIVEID);
                xw.WriteStartElement("imsss:mapInfo");

                xw.WriteAttributeString("targetObjectiveID", PRETEST_GLOBAL_ID);
                xw.WriteAttributeString("readSatisfiedStatus", "true");
                xw.WriteAttributeString("writeSatisfiedStatus", "false");

                xw.WriteEndElement(); //mapinfo

                xw.WriteEndElement(); //objective
            }


            xw.WriteEndElement(); //objectives

            //delivery
            xw.WriteStartElement("imsss:deliveryControls");
            xw.WriteAttributeString("tracked", "true"); //post tests are always tracked
            xw.WriteAttributeString("completionSetByContent", "true");
            xw.WriteAttributeString("objectiveSetByContent", "true");
            xw.WriteEndElement(); //end deliveryControls
            xw.WriteEndElement(); //end sequencing

            xw.WriteEndElement(); //end item
        }

        private void WriteHideFromChoiceSequence()
        {
            xw.WriteStartElement("imsss:sequencingRules");

            xw.WriteStartElement("imsss:preConditionRule");

            xw.WriteStartElement("imsss:ruleConditions");

            xw.WriteStartElement("imsss:ruleCondition");
            xw.WriteAttributeString("referencedObjective", PRETEST_CONTENT_OBJECTIVEID);
            xw.WriteAttributeString("condition", "satisfied");
            xw.WriteEndElement(); //rulecondition

            xw.WriteEndElement(); //rule conditions

            xw.WriteStartElement("ruleAction");
            xw.WriteAttributeString("action", "hiddenFromChoice");
            xw.WriteEndElement(); //ruleaction

            xw.WriteEndElement(); //precondition rule

            xw.WriteEndElement(); //sequencing rules
        }

        private void WriteSCORecursive(TreeNode seedNode)
        {

            //standard scenario, just creates a basic, "you can go anywhere" navigation
            foreach (TreeNode child in seedNode.ChildNodes)
            {
                if (!child.Checked) //not a page, so add it.
                {
                    bool bIsLearningObject = child.ChildNodes.Count > 0 && child.ChildNodes[0].Checked;

                    xw.WriteStartElement("item");
                    string sID = child.ValuePath.Replace("/", ID_DELIM);

                    xw.WriteAttributeString("identifier", sID);
                    xw.WriteAttributeString("identifierref", "res-" + sID);
                    xw.WriteAttributeString("parameters", "?id=" + child.Value);

                    xw.WriteStartElement("title");
                    xw.WriteString(child.Text);
                    xw.WriteEndElement(); //end title

                    if (bIsLearningObject)
                    {
                        xw.WriteStartElement("imsss:sequencing");

                        if (m_cOptions.PostTest || m_cOptions.CompletionType == Completion.INLINE_QUESTIONS_ONLY ||
                            m_cOptions.CompletionType == Completion.NAVIGATION_AND_QUESTIONS || m_cOptions.CompletionType == Completion.NAVIGATION_ONLY)
                        {
                            xw.WriteStartElement("imsss:rollupRules");
                            xw.WriteAttributeString("rollupObjectiveSatisfied", "true");
                            string sCompletion = "false";
                            if (m_cOptions.CompletionType == Completion.INLINE_QUESTIONS_ONLY ||
                                    m_cOptions.CompletionType == Completion.NAVIGATION_AND_QUESTIONS ||
                                    m_cOptions.NavigationType == Navigation.SEQENTIAL)
                                sCompletion = "true";

                            xw.WriteAttributeString("rollupProgressCompletion", sCompletion);

                            //if there's a post test, the inline questions have a weight of 0
                            //but if there are inline questions, they have a weight of "1"
                            string sWeight = EMPTY_WEIGHT;
                            if (!m_cOptions.PostTest)
                                sWeight = "1";
                            xw.WriteAttributeString("objectiveMeasureWeight", sWeight);
                            xw.WriteEndElement(); //end rollupRules
                        }

                        if (m_cOptions.NavigationType == Navigation.SEQENTIAL && !m_bFirstSCO)
                        {
                            AddPreviousSCOCompletedPreCondition();
                        }

                        //if the sco has inline questions, it'll need the weight...
                        if (m_cOptions.CompletionType == Completion.INLINE_QUESTIONS_ONLY)
                        {
                            xw.WriteStartElement("imsss:objectives");

                            xw.WriteStartElement("imsss:primaryObjective");
                            string sTargetObjective = PREFIX + child.ValuePath.Replace("/", ID_DELIM) + POSTFIX;

                            xw.WriteAttributeString("objectiveID", sTargetObjective);

                            if (m_cOptions.PostTest)
                                xw.WriteStartElement("imsss:mapInfo");

                            xw.WriteAttributeString("targetObjectiveID", NAMESPACE + sTargetObjective);

                            if (m_cOptions.CompletionType == Completion.INLINE_QUESTIONS_ONLY)
                            {
                                xw.WriteAttributeString("satisfiedByMeasure", "true");
                                int iValue = 80;
                                if (child.ToolTip != "")
                                    int.TryParse(child.ToolTip, out iValue);
                                xw.WriteElementString("imsss:minNormalizedMeasure", ((double)iValue / 100.0).ToString("f2"));
                            }

                            //if (m_cOptions.CompletionType == Completion.NAVIGATION_AND_TEST ||
                            //    m_cOptions.CompletionType == Completion.NAVIGATION_ONLY)
                            //{
                            //    xw.WriteAttributeString("readSatisfiedStatus", "true");
                            //    xw.WriteAttributeString("writeSatisfiedStatus", "true");
                            //}

                            if (m_cOptions.PostTest)
                                xw.WriteEndElement(); //mapinfo

                            xw.WriteEndElement(); //primary objective

                            ////add the target objective
                            //if (m_cOptions.NavigationType == Navigation.SEQENTIAL && !m_bFirstSCO)
                            //{
                            //    xw.WriteStartElement("imsss:objective");

                            //    xw.WriteAttributeString("objectiveID", "sco_previous_satisfied");

                            //    xw.WriteStartElement("imsss:mapInfo");

                            //    xw.WriteAttributeString("targetObjectiveID", sNamespace + sPrevTargetObjective);

                            //    //test setting to false to ignore question results

                            //    xw.WriteAttributeString("readSatisfiedStatus", "true");
                            //    xw.WriteAttributeString("writeSatisfiedStatus", "true");

                            //    xw.WriteEndElement();//mapinfo

                            //    xw.WriteEndElement();//objective

                            //}

                            xw.WriteEndElement(); //objectives

                            sPrevTargetObjective = sTargetObjective; //cache for the next loop

                        }

                        xw.WriteStartElement("imsss:deliveryControls");

                        //my assumption here is "tracked" basically means whether to score the SCO, which we only do if we have inline questions
                        //bad assumption. we want to set "tracked" to "true" for all cases, unless we want the sco to have no impact on the overall progress at all
                        //NOTE: we MAY need to set "tracked" to false in the case that we care about inline questions only..not sure.
                        xw.WriteAttributeString("tracked", "true");
                        xw.WriteAttributeString("completionSetByContent", "true");
                        xw.WriteAttributeString("objectiveSetByContent", "true");
                        xw.WriteEndElement(); //end deliveryControls

                        xw.WriteEndElement(); //end sequencing

                        m_bFirstSCO = false;
                        //xw.WriteEndElement(); //end item

                    }

                    if (child.ChildNodes.Count > 0) //recurse
                        WriteSCORecursive(child);

                    xw.WriteEndElement(); //end item

                }
            }
        }

        private void AddPreviousSCOCompletedPreCondition()
        {
            xw.WriteStartElement("imsss:sequencingRules");

            xw.WriteStartElement("imsss:preConditionRule");

            xw.WriteStartElement("imsss:ruleConditions");
            xw.WriteAttributeString("conditionCombination", "any");

            xw.WriteStartElement("imsss:ruleCondition");
            xw.WriteAttributeString("referencedObjective", "sco_previous_satisfied");
            xw.WriteAttributeString("operator", "not");
            xw.WriteAttributeString("condition", "satisfied");
            xw.WriteEndElement(); //rulecondition

            xw.WriteStartElement("imsss:ruleCondition");
            xw.WriteAttributeString("referencedObjective", "sco_previous_satisfied");
            xw.WriteAttributeString("operator", "not");
            xw.WriteAttributeString("condition", "objectiveStatusKnown");
            xw.WriteEndElement(); //rulecondition

            xw.WriteEndElement(); //rule conditions

            xw.WriteStartElement("ruleAction");
            xw.WriteAttributeString("action", "disabled");
            xw.WriteEndElement(); //ruleaction

            xw.WriteEndElement(); //precondition rule

            xw.WriteEndElement(); //sequencingRules
        }

        private void WriteAutoForwardSequence()
        {
            xw.WriteStartElement("imsss:sequencing");
            xw.WriteStartElement("imsss:controlMode");

            string sChoice = "true"; //default to free-form
            if (m_cOptions.NavigationType == Navigation.SEQENTIAL)
                sChoice = "false";

            xw.WriteAttributeString("choice", sChoice);
            xw.WriteAttributeString("flow", "true");

            xw.WriteEndElement();
            xw.WriteEndElement();
        }

        private void WriteResources(TreeNode rootNode)
        {
            xw.WriteStartElement("resources");

            //objectives page
            AddResource(xw, "0", "indexapi.html", "index.html", false);
            AddFile(xw, "0/objectives.html");
            xw.WriteEndElement();

            AddResource(xw, "dummy_sco");
            xw.WriteEndElement();

            AddResourcesToIMS(rootNode);

            if (m_cOptions.PostTest)
                BuildTestResources(true);

            if (m_cOptions.PreTest)
                BuildTestResources(false);

            AddSharedResourcesToIMS();

            xw.WriteEndElement(); //end resources
        }

        /// <summary>
        /// Writes out all the "resource" tags
        /// </summary>
        /// <param name="xw"></param>
        /// <param name="cTree"></param>
        /// <param name="rootCourse"></param>
        /// <param name="seedNode"></param>
        private void AddResourcesToIMS(TreeNode seedNode)
        {
            foreach (TreeNode child in seedNode.ChildNodes)
            {
                if (!child.Checked) //we have a container...
                {
                    string sID = child.ValuePath.Replace("/", ID_DELIM);
                    AddResource(xw, sID);

                    //check to see if it has pages
                    //pages are always all at the same level, so we just need to check the first one.
                    if (child.ChildNodes.Count > 0 && child.ChildNodes[0].Checked)
                    {
                        foreach (TreeNode childPageNode in child.ChildNodes)
                        {
                            string sFileName = childPageNode.ValuePath + ".html";
                            AddFile(xw, sFileName);
                        }

                        xw.WriteEndElement(); //end resource
                    }
                    else
                    {
                        xw.WriteEndElement(); //end resource

                        AddResourcesToIMS(child);
                    }

                }
            }
        }

        private void AddSharedResourcesToIMS()
        {
            //add shared files
            xw.WriteStartElement("resource");
            xw.WriteAttributeString("identifier", ScormSettings.SHARED_FILES_ID);
            xw.WriteAttributeString("adlcp:scormType", "asset");
            xw.WriteAttributeString("type", "webcontent");

            ArrayList sFiles = new ArrayList();
            string sSharedFolder = m_cOptions.SharedFolderPath;
            string sContentFolder = m_cOptions.ContentFolderPath;

            sFiles.AddRange(Directory.GetFiles(sSharedFolder, "*.js"));
            sFiles.AddRange(Directory.GetFiles(sSharedFolder + @"resources\", "*.js"));
            sFiles.AddRange(Directory.GetFiles(sSharedFolder + @"resources\css\", "*.css"));
            sFiles.AddRange(Directory.GetFiles(sSharedFolder + @"resources\images\", "*.gif"));
            sFiles.AddRange(Directory.GetFiles(sSharedFolder + @"resources\images\", "*.png"));
            sFiles.AddRange(Directory.GetFiles(sSharedFolder + @"resources\images\default\", "*.png"));
            sFiles.AddRange(Directory.GetFiles(sSharedFolder + @"resources\images\default\", "*.gif"));

            AddFilesRecursive(sContentFolder + @"assets\", sFiles);

            foreach (string s in sFiles)
            {
                xw.WriteStartElement("file");
                xw.WriteAttributeString("href", s.Replace(sSharedFolder, "").Replace(sContentFolder, @"content\"));
                xw.WriteEndElement(); //end file
            }

            xw.WriteEndElement(); //end resource
        }

        private void AddFilesRecursive(string sDir, ArrayList sFiles)
        {
            foreach (string sSubDir in Directory.GetDirectories(sDir))
            {
                AddFilesRecursive(sSubDir, sFiles);
            }
            sFiles.AddRange(Directory.GetFiles(sDir));
        }

        private void AddResource(XmlWriter xw, string sID)
        {
            AddResource(xw, sID, "indexapi.html", "index.html", false);
        }

        private void AddResource(XmlWriter xw, string sID, string sHref, string sDefaultPage, bool bEndTag)
        {
            xw.WriteStartElement("resource");

            xw.WriteAttributeString("identifier", "res-" + sID);
            xw.WriteAttributeString("type", "webcontent");
            xw.WriteAttributeString("adlcp:scormType", "sco");
            xw.WriteAttributeString("href", "content/" + sHref);

            AddFile(xw, sDefaultPage);

            xw.WriteStartElement("dependency");
            xw.WriteAttributeString("identifierref", ScormSettings.SHARED_FILES_ID);
            xw.WriteEndElement(); //end dependency

            if (bEndTag)
                xw.WriteEndElement(); //end resource
        }

        private void AddFile(XmlWriter xw, string sFileName)
        {
            xw.WriteStartElement("file");
            xw.WriteAttributeString("href", "content/" + sFileName);
            xw.WriteEndElement(); //end file
        }

        private void BuildTestResources(bool bIsPostTest)
        {
            string sID = ScormSettings.POSTTEST_SCO_ID;
            string sSubFolder = "posttest\\";
            if (!bIsPostTest)
            {
                sID = ScormSettings.PRETEST_SCO_ID;
                sSubFolder = "pretest\\";
            }
            string sPath = m_cOptions.ContentFolderPath + sSubFolder;

            AddResource(xw, sID);

            AddFile(xw, sSubFolder.Replace("\\", "/") + "test.html");
            AddFile(xw, sSubFolder.Replace("\\", "/") + "review.html");

            foreach (string s in Directory.GetFiles(sPath, ".html"))
            {
                AddFile(xw, sSubFolder.Replace("\\", "/") + s);
            }

            xw.WriteEndElement(); //ends the resource tag
        }
    }
}