﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;

namespace Symphony.Core.Generators
{
    static class SurveyTemplate
    {
        public static string GetTemplate(ArtisanPage page)
        {
            return string.Format(@"
                <h3>{0}</h3>
                <div class='artisan-survey p'>
                    <div class = 'artisan-survey-text'>
                        {1}
                    </div>
                    {2}
                    <div id='survey-comments-field'>
                        <label for='survey-comments'>Comments</label><br />
                        <textarea id='survey-comments' name='survey-comments' rows='6' cols='55'></textarea>
                    </div>
                    <input type='button' id='artisan-survey-submit' value='Submit' />
                </div>", 
                page.Name, page.Html, GetSurveyItemsTemplate(page));
        }

        private static string GetSurveyItemsTemplate(ArtisanPage page)
        {
            int numOptions = 5;
            StringBuilder answers = new StringBuilder();

            foreach (ArtisanAnswer a in page.Answers)
            {
                StringBuilder surveyOptions = new StringBuilder();

                for (int i = 1; i <= numOptions; i++)
                {
                    surveyOptions.AppendFormat("<td class='artisan-survey-item-value'><input type='radio' id='survey-{0}-{1}' name='survey-{0}' value='{1}' /><br /><label for='survey-{0}-{1}'>{1}</label></td>", a.Id, i);
                }

                answers.AppendFormat(@"
                    <tr class='artisan-survey-item' id='artisan-survey-item-{0}'>
                        <td class='artisan-survey-item-text' colspan='{3}'>{1}</td>
                    </tr>
                    <tr>
                        {2}
                    </tr>", a.Id, a.Text, surveyOptions, numOptions);
            }

            return string.Format(@"
                <table class='artisan-survey-items'>
                    {0}
                </table>", answers);
        }
    }
}
