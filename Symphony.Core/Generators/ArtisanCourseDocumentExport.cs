﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Symphony.Core.Models;
using System.Reflection;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using log4net;
using System.Configuration;
using Aspose.Words;
using Aspose.Words.Fonts;

namespace Symphony.Core.Generators
{
    public class ArtisanCourseDocumentExport : IArtisanCourseExport
    {
        public bool ContentOnly { get; set; }

        public class HandleNodeChanging_FontChanger : INodeChangingCallback
        {
            // Implement the NodeInserted handler to set default font settings for every Run node inserted into the Document
            void INodeChangingCallback.NodeInserted(NodeChangingArgs args)
            {
                // Change the font of inserted text contained in the Run nodes.
                if (args.Node.NodeType == NodeType.Run)
                {
                    Aspose.Words.Font font = ((Run)args.Node).Font;
                    font.Size = 12;
                    font.Name = "Arial";
                }
            }

            void INodeChangingCallback.NodeInserting(NodeChangingArgs args)
            {
                // Do Nothing
            }

            void INodeChangingCallback.NodeRemoved(NodeChangingArgs args)
            {
                // Do Nothing
            }

            void INodeChangingCallback.NodeRemoving(NodeChangingArgs args)
            {
                // Do Nothing
            }
        }

        private ILog Log = LogManager.GetLogger(typeof(ArtisanCourseTextExport));

        private string OutputFolder = ConfigurationManager.AppSettings["ArtisanExportFolder"] ?? @"c:\temp\exportcourse\";

        /// <summary>
        /// The course we are packaging.
        /// </summary>
        public Models.ArtisanCourse Course { get; set; }

        /// <summary>
        /// The root path of the course package.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Creates a new exporter with the selected course
        /// </summary>
        /// <param name="course"></param>
        public ArtisanCourseDocumentExport(Models.ArtisanCourse course)
        {
            this.Course = course;
            this.FileName = Path.Combine(this.OutputFolder, this.Course.Id.ToString() + ".doc");

            if (!Directory.Exists(this.OutputFolder))
            {
                Directory.CreateDirectory(this.OutputFolder);
            }

            try
            {
                File.Delete(this.FileName);
            }
            catch { }
        }

        /// <summary>
        /// Exports the course and returns the output file
        /// </summary>
        /// <returns></returns>
        public FileInfo Export()
        {
            if (this.Course != null)
            {
                // package the course to ensure all html is set properly
                PackageBuilder pb = new PackageBuilder(this.Course);
                pb.UseQualifiedLocalFilePaths = true;
                pb.Package();

                Symphony.Core.Utilities.LicenseAspose();

                Aspose.Words.Document document = new Aspose.Words.Document();
                document.NodeChangingCallback = new HandleNodeChanging_FontChanger();

                DocumentBuilder builder = new DocumentBuilder(document);
                builder.PageSetup.LeftMargin = 30;
                builder.PageSetup.RightMargin = 40;

                AddHeaderAndFooter(builder);

                builder.InsertParagraph();
                builder.InsertParagraph();
                builder.InsertParagraph();
                builder.InsertHtml("<h1>" + Course.Name + "</h1>");
                builder.InsertHtml("<h2>" + Course.Description + "</h2>");
                builder.InsertHtml("<h3>" + Course.Objectives + "</h3>");
                builder.InsertBreak(BreakType.PageBreak);

                foreach (ArtisanSection section in this.Course.Sections)
                {
                    BuildSection(section, builder, 0);
                }
                document.Save(this.FileName);
            }

            return new FileInfo(this.FileName);
        }

        private void AddHeaderAndFooter(DocumentBuilder builder)
        {
            // header/footer
            Section currentSection = builder.CurrentSection;
            builder.MoveToHeaderFooter(HeaderFooterType.HeaderPrimary);
            Stream stream = this.GetType().Assembly.GetManifestResourceStream("Symphony.Core.Resources.belogo.gif");
            PageSetup pageSetup = currentSection.PageSetup;
            pageSetup.HeaderDistance = 0;

            builder.ParagraphFormat.LeftIndent = -pageSetup.LeftMargin;
            builder.InsertImage(stream);

            builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary);
            builder.StartTable();

            // clear table borders.
            builder.CellFormat.ClearFormatting();
            //builder.RowFormat.LeftIndent = -pageSetup.LeftMargin + 10;

            builder.InsertCell();

            // Set first cell to 1/3 of the page width.
            builder.CellFormat.Width = builder.PageSetup.PageWidth / 2;

            // Insert page numbering text here.
            // It uses PAGE and NUMPAGES fields to auto calculate current page number and total number of pages.
            builder.Write("Page ");
            builder.InsertField("PAGE", "");
            builder.Write(" of ");
            builder.InsertField("NUMPAGES", "");

            builder.InsertCell();
            // Set the second cell to 2/3 of the page width.
            builder.CellFormat.Width = builder.PageSetup.PageWidth / 2;


            builder.Write("Copyright 1997+ BankersEdge");
            builder.CurrentParagraph.ParagraphFormat.Alignment = ParagraphAlignment.Right;
            builder.Writeln();
            builder.Write("Created " + DateTime.UtcNow.ToString() + " UTC");
            builder.CurrentParagraph.ParagraphFormat.Alignment = ParagraphAlignment.Right;

            builder.EndRow();
            builder.EndTable();

            builder.RowFormat.ClearFormatting();
            builder.MoveToDocumentStart();
        }

        private void BuildSection(ArtisanSection section, DocumentBuilder builder, int tabs)
        {
            if (section.SectionType == (int)ArtisanSectionType.Sco)
            {
                builder.ParagraphFormat.StyleIdentifier = StyleIdentifier.Heading1;
                builder.Font.Color = System.Drawing.Color.Gold;
                builder.Font.Size = 16f;
            }
            else if (section.SectionType == (int)ArtisanSectionType.LearningObject)
            {
                builder.ParagraphFormat.StyleIdentifier = StyleIdentifier.Heading2;
                builder.Font.Color = System.Drawing.Color.DeepSkyBlue;
                builder.Font.Size = 14f;
            }
            else
            {
                builder.Font.Color = System.Drawing.Color.Black;
                builder.ParagraphFormat.StyleIdentifier = StyleIdentifier.Normal;
                builder.Font.Size = 12f;
            }

            // titles/objectives
            builder.Writeln(section.Name);
            if (!string.IsNullOrEmpty(section.Objectives))
            {
                builder.Writeln("Objectives: " + section.Objectives);
            }

            // recurse
            tabs++;
            if (section.Sections != null)
            {
                foreach (ArtisanSection subsection in section.Sections)
                {
                    BuildSection(subsection, builder, ++tabs);
                }
            }
            else if (section.Pages != null)
            {
                // pages
                tabs++;
                foreach (ArtisanPage page in section.Pages)
                {
                    if ((page.PageType == (int)ArtisanPageType.Question || page.PageType == (int)ArtisanPageType.Survey) && ContentOnly)
                    {
                        continue;
                    }
                    builder.ParagraphFormat.StyleIdentifier = StyleIdentifier.Heading3;
                    builder.Font.Color = System.Drawing.Color.ForestGreen;
                    builder.Writeln(page.Name ?? "");

                    builder.Font.Color = System.Drawing.Color.Black;
                    builder.ParagraphFormat.StyleIdentifier = StyleIdentifier.Normal;

                    BuildPage(page, builder, tabs);
                    builder.InsertBreak(BreakType.PageBreak);
                }
            }
        }

        private void BuildPage(ArtisanPage page, DocumentBuilder builder, int tabs)
        {
            // Aspose doesn't support inline images using style attributes - we need to convert 
            // them (particularly width/height) to inline attributes, without the "px" or % extensions
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(page.Html);
            foreach (string selector in new string[] { "//img[@src]" })
            {
                HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes(selector);
                if (nodes != null)
                {
                    foreach (HtmlNode node in nodes)
                    {
                        int width = 0; int height = 0; int maxWidth = 0; int maxHeight =0;
                        string style = node.GetAttributeValue("style", string.Empty);
                        // pull out height/width
                        Match m = Regex.Match(style, @"height\s*:\s*(\d+)px");
                        if (m.Success)
                        {
                            height = int.Parse(m.Groups[1].Value);

                        }
                        m = Regex.Match(style, @"width\s*:\s*(\d+)px");
                        if (m.Success)
                        {
                            width = int.Parse(m.Groups[1].Value);
                        }

                        m = Regex.Match(style, @"max-height\s*:\s*(\d+)px");
                        if (m.Success)
                        {
                            maxHeight = int.Parse(m.Groups[1].Value);
                            if (height > maxHeight)
                            {
                                height = maxHeight;
                            }
                        }

                        m = Regex.Match(style, @"max-width\s*:\s*(\d+)px");
                        if (m.Success)
                        {
                            maxWidth = int.Parse(m.Groups[1].Value);
                            if (width > maxWidth)
                            {
                                width = maxWidth;
                            }
                        }
                        
                        // ok, we have the height/width that would apply in the html now; 
                        // take the result, and scale it to a max of 200px in either direction
                        int heightLimit = 200;
                        int widthLimit = 200;
                        if (height > heightLimit)
                        {
                            float factor = (float)heightLimit / (float)height;
                            height = (int)(height * factor);
                            width = (int)(width * factor);
                        }
                        if (width > widthLimit)
                        {
                            float factor = (float)widthLimit / (float)width;
                            height = (int)(height * factor);
                            width = (int)(width * factor);
                        }

                        node.SetAttributeValue("height", height.ToString());
                        node.SetAttributeValue("width", width.ToString());
                    }
                }
            }
            builder.InsertHtml(doc.DocumentNode.WriteTo());
            if (page.QuestionType > 0)
            {
                builder.InsertBreak(BreakType.LineBreak);

                builder.InsertHtml("<p><i>Correct Response</i>: " + page.CorrectResponse + "</p>");
                builder.InsertHtml("<p><i>Incorrect Response</i>: " + page.IncorrectResponse + "</p>");

                string answersHtml = string.Join(", ", page.Answers.OrderBy(a => a.Sort).Where(a => a.IsCorrect).Select(a => a.Text).ToArray());
                builder.InsertHtml("<p><i>Correct Answer(s)</i>: " + answersHtml + "</p>");
            }
            /*
            string line = "";
            for (int i = 0; i < tabs; i++)
            {
                line += "  ";
            }
            builder.Writeln(line + page.Name + ":");
            if (page.QuestionType > 0)
            {
                builder.Writeln(line + "  " + page.Html);
                foreach (ArtisanAnswer ans in page.Answers)
                {
                    string answer = line + "    " + ans.Text;
                    if (ans.IsCorrect)
                    {
                        answer += " - CORRECT";
                    }

                    builder.Writeln(answer);
                }
            }
            else
            {
                HtmlDocument d = new HtmlDocument();
                d.LoadHtml(Regex.Replace(page.Html, @"<br\s?\/?>", "\r\n"));

                StringBuilder sb = new StringBuilder();
                foreach (string nodeName in new string[] { "h1", "h2", "h3", "h4", "h5", "p" })
                {
                    HtmlNodeCollection nodes = d.DocumentNode.SelectNodes("//" + nodeName);
                    if (nodes != null)
                    {
                        foreach (HtmlNode node in nodes)
                        {
                            if (!string.IsNullOrEmpty(node.InnerText.Trim()))
                            {
                                sb.AppendLine(line + "    " + node.InnerText.Replace("&nbsp;", " "));
                            }
                        }
                    }
                    sb.AppendLine();
                }
                builder.Write(sb.ToString());
            }
            */
        }
    }
}
