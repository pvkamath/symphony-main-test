using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Collections;
using System.Text.RegularExpressions;
using BankersEdge.Artisan;

namespace BankersEdge.Artisan
{

    public class AICCOptions
    {
        private int m_iCourseID;
        private int m_iMasteryScore;
        private string m_sContentFolderPath;
        private string m_sSharedFolderPath;

        public int CourseID
        {
            get { return this.m_iCourseID; }
            set { this.m_iCourseID = value; }
        }

        public string ContentFolderPath
        {
            get { return this.m_sContentFolderPath; }
            set { this.m_sContentFolderPath = value; }
        }

        public string SharedFolderPath
        {
            get { return this.m_sSharedFolderPath; }
            set { this.m_sSharedFolderPath = value; }
        }

        public int MasteryScore
        {
            get { return this.m_iMasteryScore; }
            set { this.m_iMasteryScore = value; }
        }
    }


    /// <summary>
    /// Summary description for AICCBuilder
    /// </summary>
    public class AICCBuilder
    {
        private TreeNode m_cRootNode;
        private AICCOptions m_cOptions;
        private string sNamespace = "com.scorm.bankersedge.";

        public void CreateAUFile(CourseTree cCourseTree, AICCOptions cOptions)
        {
            StreamWriter sw = null;
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(cOptions.ContentFolderPath))
                Directory.CreateDirectory(cOptions.ContentFolderPath);

            PackagerParamController controller = new PackagerParamController();
            string sURL = controller.GetAICCUrl(cCourseTree.CourseHierarchy[0].Id);

            // Header
            //                    1           2          3                  4                     5                 6              7                 8               9              10               11              12    
            sb.AppendLine(@"""system_id"",""type"",""command_line"",""max_time_allowed"",""time_limit_action"",""max_score"",""core_vendor"",""system_vendor"",""file_name"",""mastery_score"",""web_launch"",""au_password""");
            // Course root
            //                                        1                                     2           3          4   5     6    7   8                                          9                                                                                                         10  11   12
            sb.AppendLine(@"""A" + cCourseTree.CourseHierarchy[0].Id.ToString() + @""",""course"",""C,N"",""00:00:00"","""",""100"","""","""",""" + sURL + @""",""" + cOptions.MasteryScore.ToString() + @""","""",""""");
            //OK... the following isn't being used - the AU is a single entity which is the course
            //// Each Assignable Unit (equivalent to learning object)
            //foreach (LearningObjectPageHierarchy loph in cCourseTree.PageHierarchy)
            //    //                                           1                      2             3       4    5     6    7    8                                               9                                                                                                                                                                                                               10    11     12
            //    sb.AppendLine(@"""A" + loph.LearningObjectID.ToString() + @""",""course"",""C,N"",""00:00:00"","""","""","""","""",""http://testtrack.scorm.com/testtrackuserspace/c/billcoff/" + cCourseTree.CourseHierarchy[0].Id.ToString() + @"/content/" + cCourseTree.CourseHierarchy[0].Id.ToString() + @"/" + loph.LearningObjectID.ToString() + @"/" + loph.Id.ToString() + @".html"", """", """", """"");

            sw = new StreamWriter(cOptions.ContentFolderPath + cOptions.CourseID.ToString() + ".AU", false, Encoding.UTF8);
            sw.Write(sb);
            sw.Flush();
            sw.Close();
        }

        public void CreateDESFile(CourseTree cCourseTree, AICCOptions cOptions, string sName, string sDescription, string sAuthor)
        {
            StreamWriter sw = null;
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(cOptions.ContentFolderPath))
                Directory.CreateDirectory(cOptions.ContentFolderPath);
            //Header Line
            sb.AppendLine(@"""system_id"",""title"",""description"",""developer_id""");
            sb.AppendLine(@"""A" + cCourseTree.CourseHierarchy[0].Id.ToString() + @""",""" + sName + @""",""" + sDescription + @""",""" + sAuthor + @"""");
            //Only dealing with Course Level
            //foreach (LearningObjectPageHierarchy loph in cCourseTree.PageHierarchy)
            //    sb.AppendLine(@"""A" + loph.LearningObjectID.ToString() + @""",""" + loph.PageName + @""",""" + loph.PageName + @""",""" + loph.CreatedBy + @"""");
            sw = new StreamWriter(cOptions.ContentFolderPath + cOptions.CourseID.ToString() + ".DES", false, Encoding.UTF8);
            sw.Write(sb);
            sw.Flush();
            sw.Close();
        }

        public void CreateCRSFile(TreeNode cRootNode, AICCOptions cOptions, int iVersion, string sName, string sDescription)
        {
            StreamWriter sw = null;
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(cOptions.ContentFolderPath))
                Directory.CreateDirectory(cOptions.ContentFolderPath);
            //Levels of complexity...///////////////////////////////////////////////////////////////////////////////////////////////////
            //Level 2 
            //This level of complexity adds a possible single prerequisite for each structure element  -- an assignable unit or a block. 
            //The evaluation of each prerequisite � true or false � is done by default.  The order in which the student moves through 
            //the course can be affected by these prerequisites (see Course Elements.Prequisite ). 

            //This level also introduces the ability to identify simple completion requirements.  This means a structural element�s 
            //completion status can affect another element.  This concept enables (among other things) the use of separate 
            //assignable units as pre-tests.  Thus the completion of one assignable unit (such as a pre-test) can result in the �Pass� 
            //status of another unit (such as an instructional lesson).  

            //Includes all information (data elements) defined as Level 1 and 2.  May include additional data elements defined as 
            //level 3a or 3b. (See section 3.5.1 for data elements included in this level) 

            //Level 3a 
            //Level 3a adds to level 2 the ability to define complex prerequisites and complex completion requirements.  Logical 
            //expressions (see section 4.2.3) may be used to describe these requirements.  Completion requirements may be used 
            //to force assignable unit sequences without breaks between each.  

            //Includes all information (data elements) defined as level 1, 2, and 3a.  May include additional data elements defined 
            //as level 3b. (See section 3.5.1 for data elements included in this level) 

            //Level 3b  
            //Level 3b adds the description and use of objectives to the course description and sequencing information.  It 
            //includes the description of the relationship of objectives to the course structural elements.    

            //Includes all information (data elements) defined as level 1, 2, and 3b.  May include additional data elements and 
            //features defined as level 3a. (See section 3.5.1 for data elements included in this level) 
            //Level 3   
            //Includes all information and features defined as level 1, 2, 3a, and 3b. Supporting 3a and 3b allows the use of 
            //complex prerequisites and completions with objectives.  (See section 3.5.1 for data elements included in this level) 
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            sb.AppendLine(@"[Course]");
            sb.AppendLine(@"Course_Creator=BankersEdge");
            sb.AppendLine(@"Course_ID=" + cOptions.CourseID);
            sb.AppendLine(@"Course_System=BE Course Generator");
            sb.AppendLine(@"Course_Title=" + sName);
            sb.AppendLine(@"Level=1"); //Level of complexity [See above comment]
            sb.AppendLine(@"Max_Fields_CST=2"); //HardCoded because there is only the course level that we are calling an AU + getAUCount(cRootNode));//Same as AU's
            sb.AppendLine(@"Total_Aus=1");// HardCoded because there is only the course level that we are calling an AU + getAUCount(cRootNode));//Total Assignable Units
            sb.AppendLine(@"Total_Blocks=0");//Total grouping block count, no nesting implied
            sb.AppendLine(@"Version=" + iVersion.ToString());
            sb.AppendLine(@"[Course_Behavior]");
            sb.AppendLine(@"Max_Normal=1");
            sb.AppendLine(@"[Course_Description]");
            sb.AppendLine(sDescription);
            sw = new StreamWriter(cOptions.ContentFolderPath + cOptions.CourseID.ToString() + ".CRS", false, Encoding.UTF8);
            sw.Write(sb);
            sw.Flush();
            sw.Close();
        }

        public void CreateCSTFile(CourseTree cCourseTree, AICCOptions cOptions)
        {
            //cst file////////////
            //"block","member"  // first line defines element and their children(can have variable amount of "members")
            //"root","A13"      // second line shows all Au's at this level
            //"A13", "A14"      // 3rd line is the block grouping
            //////////////////////
            StreamWriter sw = null;
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(cOptions.ContentFolderPath))
                Directory.CreateDirectory(cOptions.ContentFolderPath);
            sb.Append(@"""Block"",""Member""");
            //We're assuming there will be no nesting of au's for AICC packages
            //So their will only be one tier to the cst config file structure
            //Establish width of the tier by adding "member" for each AU
            //foreach (LearningObjectPageHierarchy loph in cCourseTree.PageHierarchy)
            //{
            //    sb.Append(@"""Member"",");
            //}            
            //sb.Remove(sb.Length - 1, 1);
            sb.AppendLine("");
            //AICC Root AU assumption is there is only one at this level
            sb.Append(@"""Root"",""A" + cCourseTree.CourseHierarchy[0].Id.ToString() + @"""");
            //foreach (LearningObjectPageHierarchy loph in cCourseTree.PageHierarchy)
            //    sb.Append(@"""A" + loph.LearningObjectID.ToString() + @""",");
            //sb.Remove(sb.Length - 1, 1);
            sb.AppendLine("");
            sw = new StreamWriter(cOptions.ContentFolderPath + cOptions.CourseID.ToString() + ".CST", false, Encoding.UTF8);
            sw.Write(sb);
            sw.Flush();
            sw.Close();
        }

        /// <summary>
        /// Get AU Count
        /// </summary>
        /// <param name="tnRoot"></param>
        /// <returns></returns>
        private int getAUCount(TreeNode tnRoot)
        {
            int auCount = 1; //we're counting the root as an au
            foreach (TreeNode tn in tnRoot.ChildNodes)
            {
                if (!tn.Checked) //au's do not have Checked Flag, just pages
                {
                    auCount += 1;
                }
            }
            return auCount;
        }
        /// <summary>
        /// Build 
        /// </summary>
        /// <param name="tnRoot"></param>
        /// <returns></returns>
        //private void BuildTestResources(bool bIsPostTest)
        //{
        //    string sID = POSTTEST_SCO_ID;
        //    string sSubFolder = "posttest\\";
        //    if (!bIsPostTest)
        //    {
        //        sID = PRETEST_SCO_ID;
        //        sSubFolder = "pretest\\";
        //    }
        //    string sPath = m_cOptions.ContentFolderPath + sSubFolder;

        //    AddResource(xw, sID);

        //    AddFile(xw, sSubFolder.Replace("\\", "/") + "test.html");
        //    AddFile(xw, sSubFolder.Replace("\\", "/") + "review.html");

        //    //copy in the test.html file
        //    if (!Directory.Exists(sPath))
        //        Directory.CreateDirectory(sPath);

        //    if ((this.m_cOptions.PostTestType == TestType.JEOPARDY && bIsPostTest) ||
        //        (this.m_cOptions.PreTestType == TestType.JEOPARDY && !bIsPostTest))

        //        File.Copy(m_cOptions.SharedFolderPath + "content\\jeopardy.html", sPath + "test.html");
        //    else
        //        File.Copy(m_cOptions.SharedFolderPath + "content\\sequential.html", sPath + "test.html");

        //    File.Copy(m_cOptions.SharedFolderPath + "content\\review.html", sPath + "review.html");

        //    foreach (string s in Directory.GetFiles(sPath, ".html"))
        //    {
        //        AddFile(xw, sSubFolder.Replace("\\", "/") + s);
        //    }

        //    xw.WriteEndElement(); //ends the resource tag
        //}
    }
}