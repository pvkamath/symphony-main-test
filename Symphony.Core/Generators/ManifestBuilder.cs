﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Symphony.Core.Models;
using System.Reflection;
using HtmlAgilityPack;
using Symphony.Core.Controllers;
using System.Xml;

namespace Symphony.Core.Generators
{
    public class Scorm12ManifestBuilder
    {
        private AccreditationController Accreditation = new AccreditationController();

        public string Build(ArtisanCourse course)
        {
            //basic xml stuff
            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlTextWriter xw = new XmlTextWriter(ms, Encoding.UTF8))
                {
                    xw.Indentation = 1;
                    xw.IndentChar = '\t';

                    xw.WriteStartDocument();
                    xw.Formatting = Formatting.Indented;

                    StartManifest(xw, course.Id);

                    AddAccreditationMetaData(xw, course.Accreditations);

                    StartOrganizations(xw);

                    //title and score
                    AddRootItem(xw, course.Name, course.Id, course.PassingScore > 0 ? course.PassingScore : 80);

                    AddMetaData(xw);

                    EndOrganizations(xw);

                    AddIndex(xw);

                    EndManifest(xw);

                    xw.Flush();

                    // needs to stay within the xw using statement so we don't close the writer too soon
                    return Utilities.GetString(ms);
                }
            }
        }

        private void AddRootItem(XmlTextWriter xw, string title, int courseId, int masteryScore)
        {
            xw.WriteElementString("title", title);

            xw.WriteStartElement("item");

            xw.WriteAttributeString("identifier", "course-" + courseId.ToString());
            xw.WriteAttributeString("identifierref", "res-index");
            xw.WriteAttributeString("isvisible", "true");

            //title again. stupid ims manifest.
            xw.WriteElementString("title", title);
            xw.WriteElementString("adlcp:masteryscore", masteryScore.ToString());

            xw.WriteEndElement();
        }

        private void AddIndex(XmlTextWriter xw)
        {
            xw.WriteStartElement("resources");
            xw.WriteStartElement("resource");

            xw.WriteAttributeString("identifier", "res-index");
            xw.WriteAttributeString("type", "webcontent");
            xw.WriteAttributeString("adlcp:scormtype", "sco"); //lower case t in scormtype is required for 1.2, and its a capital in 2004
            xw.WriteAttributeString("href", "API/indexAPI.html");

            xw.WriteStartElement("file");
            xw.WriteAttributeString("href", "API/indexAPI.html");
            xw.WriteEndElement();

            xw.WriteEndElement(); //end resource
            xw.WriteEndElement(); //end resources
        }

        private void AddMetaData(XmlTextWriter xw)
        {
            xw.WriteStartElement("metadata");

            xw.WriteElementString("schema", "ADL SCORM");
            xw.WriteElementString("version", "1.2");
            xw.WriteElementString("adlcp:location", "");

            xw.WriteEndElement();
        }

        private void AddAccreditationMetaData(XmlTextWriter xw, List<ArtisanCourseAccreditation> accreditations)
        {
            xw.WriteStartElement("metadata");

            xw.WriteStartElement("artisan:accreditations");

            foreach (var accreditation in accreditations)
            {
                string err;
                if (!accreditation.IsValid(out err))
                {
                    // This is not strictly necessary as we validate earlier but better to be safe
                    // as everything following this assumes the accreditations are valid.
                    continue;
                }

                // Don't add accreditations that are pending or expired when packaging.
                if (accreditation.Status != (int)AccreditationStatus.Accredited)
                {
                    continue;
                }
                if (DateTime.Compare(DateTime.Now, accreditation.ExpiryDate.Value) >0)
                {
                    continue;
                }

                Accreditation.ResolveDisclaimer(accreditation);

                xw.WriteStartElement("artisan:accreditation");
                xw.WriteAttributeString("artisan:id", accreditation.Id.ToString());
                xw.WriteAttributeString("artisan:artisanCourseId", accreditation.ArtisanCourseId.ToString());
                xw.WriteAttributeString("artisan:accreditationBoardId", accreditation.AccreditationBoardId.ToString());
                if (accreditation.ProfessionId.HasValue)
                {
                    xw.WriteAttributeString("artisan:professionId", accreditation.ProfessionId.ToString());
                }

                xw.WriteElementString("artisan:startDate", accreditation.StartDate.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                xw.WriteElementString("artisan:expiryDate", accreditation.ExpiryDate.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                xw.WriteElementString("artisan:accreditationCode", accreditation.AccreditationCode);
                xw.WriteElementString("artisan:courseNumber", accreditation.CourseNumber);
                xw.WriteElementString("artisan:creditHours", accreditation.CreditHours.ToString());
                xw.WriteElementString("artisan:creditHoursLabel", accreditation.CreditHoursLabel);
                xw.WriteElementString("artisan:disclaimer", accreditation.Disclaimer);

                xw.WriteEndElement();
            }

            xw.WriteEndElement();
            xw.WriteEndElement();
        }

        private void StartManifest(XmlTextWriter xw, int courseId)
        {
            //begin the manifest file
            xw.WriteStartElement("manifest");
            xw.WriteAttributeString("identifier", "ArtisanPackage-" + courseId);
            xw.WriteAttributeString("version", "1.2");
            xw.WriteAttributeString("xmlns:adlcp", "http://www.adlnet.org/xsd/adlcp_rootv1p2");
            xw.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            xw.WriteAttributeString("xsi:schemaLocation", "http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd");
            xw.WriteAttributeString("xmlns", "http://www.imsproject.org/xsd/imscp_rootv1p1p2");
            xw.WriteAttributeString("xmlns:artisan", "urn:1");
        }

        private void StartOrganizations(XmlTextWriter xw)
        {
            //ok, now, build the packages. first, add the org stuff...
            xw.WriteStartElement("organizations");
            xw.WriteAttributeString("default", "ORG-BankersEdge");
            xw.WriteStartElement("organization");
            xw.WriteAttributeString("identifier", "ORG-BankersEdge");
        }

        private void EndOrganizations(XmlTextWriter xw)
        {
            xw.WriteEndElement();
            xw.WriteEndElement();
        }

        private void EndManifest(XmlTextWriter xw)
        {
            xw.WriteEndElement();
            xw.WriteEndDocument();
        }
    }
}
