﻿using System;
using System.IO;
using Symphony.Core.Models;
namespace Symphony.Core.Generators
{
    interface IArtisanCourseExport
    {
        ArtisanCourse Course { get; set; }
        FileInfo Export();
        string FileName { get; set; }
    }
}
