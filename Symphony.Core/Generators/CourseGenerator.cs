using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Collections;
using System.Text.RegularExpressions;
using BankersEdge.Artisan;
using Microsoft.Win32;
using log4net;
using BankersEdge.Data.Admin;

/// <summary>
/// Generates the course package
/// </summary>
namespace BankersEdge.CourseGenerator
{
    #region Helpers
    /// <summary>
    /// Helper class for Image Question types
    /// </summary>
    public class Coords
    {
        public int x1;
        public int x2;
        public int y1;
        public int y2;
    }

    public struct PackageTypeCode
    {
        public static string AICC = "AICC";
        public static string SCORM12 = "SCORM";
        public static string SCORM2k4 = "SCORM2004";
    }

    #endregion

    public class CourseGenerator
    {
        #region Globals

        private static ILog Log = log4net.LogManager.GetLogger(typeof(CourseGenerator));

        private static bool Preview = false;
        private static string SelectedPackageType = "";

        private bool m_bFirstSCO = true;

        string sCompletionType = "";
        string sNavType = "";
        string sNamespace = "com.scorm.bankersedge.";
        string sPrefix = "sco";
        string sPostfix = "_satisfied";
        string sPrevTargetObjective = "";

        /*private enum PackagerScenario
        {
            Scenario1_JumpingNavigation = 1,
            Scenario2_SequentialNavigation = 2,
            Scenario3_InlineQuestions = 3,
            Scenario4_PassingScorePostTest = 4,
            Scenario5_PassingScorePostTestWithPreTest = 5
        }*/

        //private PackagerScenario m_eCurrentPackagerScenario = PackagerScenario.Scenario1_JumpingNavigation;

        private string JSON_FILE_NAME = "coursetree.json.js";
        private string ARTISAN_COURSE_FILE_NAME = "artisan.course.js";
        private string JSON_OBJECT_NAME = "CourseTreeJSON";
        private string ORGANIZATION_NAME = "ORG-BankersEdge";
        private string IMSMANIFEST_FILENAME = "imsManifest.xml";
        private string ASSET_FOLDER_NAME = "Assets";
        private string SHARED_FILES_ID = "SharedFiles";
        private string PRETEST_SCO_ID = "pretest";
        private string POSTTEST_SCO_ID = "posttest";

        private string FILL_IN_THE_BLANK_REPLACE = "_______________";

        private bool bTrackQuestionPages = true;

        Course cCourse;
        CoursePackage cPackage;

        //these need to match the values in the db
        private struct Navigation
        {
            public static string FREEFORM = "freeform";
            public static string SEQENTIAL = "sequential";
        }

        private struct Completion
        {
            public static string NAVIGATION_ONLY = "navonly";
            public static string NAVIGATION_AND_TEST = "navplustest";
            public static string TEST_ONLY = "testonly";
            public static string INLINE_QUESTIONS_ONLY = "inlinequestonly";
            public static string NAVIGATION_AND_QUESTIONS = "navplusinline";
        }


        private System.Collections.ArrayList addedCourses = new ArrayList();
        private BankersEdge.Artisan.PageCollection questionPages = new PageCollection();
        private System.Collections.Generic.List<TreeNode> cCourseNodes = new System.Collections.Generic.List<TreeNode>();

        private string ID_DELIM = "-";
        private int DEFAULT_SCO_TYPE = 1;

        #endregion

        #region Folders

        /// <summary>
        /// Gets the folder for outputting the SCORM data. Passing a 0 gets the root SCORM folder, passing a valid course id gets that course's folder
        /// </summary>
        /// <returns></returns>
        public static string GetSCORMFolder(int iCourseID)
        {
            string sFolder = "";

            System.Collections.Specialized.NameValueCollection settings = System.Configuration.ConfigurationSettings.AppSettings;
            sFolder = settings["ScormProductionOutputFolder"];
            if (sFolder.StartsWith("~/"))
                sFolder = HttpContext.Current.Server.MapPath(sFolder);
            if (!sFolder.EndsWith("\\"))
                sFolder += "\\";

            //dump everything in a preview folder
            if (iCourseID > 0)
            {
                sFolder += iCourseID.ToString() + "\\";
                if (Preview)
                {
                    sFolder += "preview\\";
                }
                else
                {
                    sFolder += SelectedPackageType.ToLower().Replace(".", "") + "\\";
                }
            }

            return sFolder;
        }


        /// <summary>
        /// Gets the content folder within a given course
        /// </summary>
        /// <param name="iCourseID"></param>
        /// <returns></returns>
        public static string GetContentFolder(int iCourseID)
        {
            return GetSCORMFolder(iCourseID) + "content\\";
        }

        /// <summary>
        /// Gets the folder where all assets will be stored
        /// </summary>
        /// <param name="iCourseID"></param>
        /// <returns></returns>
        public string GetAssetFolder(int iCourseID, string sPackageTypeCode)
        {
            return GetContentFolder(iCourseID) + ASSET_FOLDER_NAME + "\\";
        }

        /// <summary>
        /// Gets the folder that needs to be copied into each package
        /// </summary>
        /// <returns></returns>
        public static string GetSharedFolder(string sPackageTypeCode)
        {
            return GetSCORMFolder(0) + "shared\\";
        }

        /// <summary>
        /// Gets the folder that needs to be copied into each package
        /// </summary>
        /// <returns></returns>
        public static string GetSkinFolder(string sPackageTypeCode)
        {
            return GetSCORMFolder(0) + "skins\\";
        }

        /// <summary>
        /// Builds the tree structure, and returns the root path.
        /// </summary>
        /// <param name="tnRoot"></param>
        /// <returns></returns>
        private string CreateFolderStructure(TreeNode tnRoot, CourseHierarchy rootCourse, string sPackageTypeCode)
        {
            foreach (TreeNode tn in tnRoot.ChildNodes)
            {
                if (!tn.Checked) //pages get a "checked" flag
                {
                    //course or learning object. trash any existing files, and then make sure the directory exists
                    string sDir = "";
                    //if (sPackageTypeCode == "AICC")
                    //    sDir = GetSCORMFolder(int.Parse(tn.Parent.Value)) + tn.Value;
                    //else
                    sDir = GetContentFolder(rootCourse.Id) + tn.ValuePath;
                    System.IO.Directory.CreateDirectory(sDir);
                    CreateFolderStructure(tn, rootCourse, sPackageTypeCode);
                }
            }
            return GetSCORMFolder(rootCourse.Id) + tnRoot.ValuePath;
        }


        /// <summary>
        /// Packages up the folder into a single big zip file.
        /// </summary>
        /// <param name="rootCourse"></param>
        /// <param name="bPreview"></param>
        /// <returns></returns>
        private int PackageZip(CourseHierarchy rootCourse, bool bPreview, string sPackageTypeCode)
        {
            if (Directory.Exists(GetContentFolder(rootCourse.Id)) || Directory.Exists(GetSCORMFolder(rootCourse.Id)))
            {
                // Create the zip file
                ZipUtility newZip = new ZipUtility();
                //copy the shared stuff
                ZipUtility.CopyDir(GetSharedFolder(sPackageTypeCode), GetSCORMFolder(rootCourse.Id));
                //rename the appropriate indexapi.html
                string sContentFolder = GetSCORMFolder(rootCourse.Id) + @"\content\";
                string sMainIndexFile = "";
                string sPreviewIndexFile = "indexapi_preview.html";
                string sActualIndexFile = "indexapi.html";
                if (bPreview == false)
                {
                    if (sPackageTypeCode == PackageTypeCode.AICC)
                    {
                        sMainIndexFile = "indexapi_aicc.html";
                    }
                    else if (sPackageTypeCode == PackageTypeCode.SCORM12)
                    {
                        sMainIndexFile = "indexapi_scorm.html";
                    }
                    else if (sPackageTypeCode == PackageTypeCode.SCORM2k4)
                    {
                        sMainIndexFile = "indexapi_scorm2004.html";
                    }
                    File.Copy(sContentFolder + sMainIndexFile, sContentFolder + sActualIndexFile, true);
                    File.SetLastWriteTime(sContentFolder + sActualIndexFile, DateTime.Now);
                    newZip.CreateArchive(GetSCORMFolder(rootCourse.Id), rootCourse.Id.ToString() + ".zip", sPackageTypeCode);
                }
                else
                {
                    File.Copy(sContentFolder + sPreviewIndexFile, sContentFolder + sActualIndexFile, true);
                    File.SetLastWriteTime(sContentFolder + sActualIndexFile, DateTime.Now);
                }

                return 1;
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region Tree/Page/Content management

        /// <summary>
        /// Recursive function to build a tree node structure from the course tree
        /// </summary>
        /// <param name="cTree"></param>
        /// <param name="seedCourse"></param>
        /// <param name="seedNode"></param>
        private void BuildTree(int iCoursePackageID, CourseTree cTree, CourseHierarchy seedCourse, TreeNode seedNode, TreeNode rootNode)
        {
            /*CourseHierarchyCollection cSubScos = new CourseHierarchyCollection();
            foreach (CourseHierarchy sco in cTree.CourseHierarchy)
            {
                //grab the sub-scos
                if (sco.ParentCourseID == seedCourse.Id)
                    cSubScos.Add(sco);
            }
            */
            //if (cSubScos.Count > 0)
            //{
            foreach (CourseHierarchy sco in cTree.CourseHierarchy)
            {
                if (addedCourses.Contains(sco.Id)) continue;
                addedCourses.Add(sco.Id);

                TreeNode tnCourse = null;
                if (sco.ParentCourseID > 0)
                {
                    //if (seedNode.Value.ToString() != sco.Id.ToString())
                    //{
                    //for the non-root nodes, add a sub-sco
                    tnCourse = new TreeNode(sco.CourseName, sco.Id.ToString());
                    TreeNode parentNode = null;
                    //find the node whose id = the sco's parent id
                    foreach (TreeNode tn in cCourseNodes)
                    {
                        if (tn.Value == sco.ParentCourseID.ToString())
                        {
                            parentNode = tn;
                            break;
                        }
                    }
                    if (parentNode == null)
                        parentNode = rootNode;
                    parentNode.ChildNodes.Add(tnCourse);
                }
                else
                {
                    tnCourse = rootNode;
                }
                //}
                //else
                //{
                //    tnCourse = seedNode;
                //}

                cCourseNodes.Add(tnCourse);

                foreach (CourseLearningObjectsHierarchy clo in cTree.LearningObjectHierarchy)
                {
                    if (clo.CourseID == sco.Id)
                    {
                        TreeNode tnLearningObject = new TreeNode(clo.LearningObjectName, clo.Id.ToString());
                        tnCourse.ChildNodes.Add(tnLearningObject);

                        //set the tooltip to the passing score
                        CoursePackageLearningObject cplo = new CoursePackageLearningObject();
                        SubSonic.Query q = new SubSonic.Query(CoursePackageLearningObject.Schema)
                        .WHERE(CoursePackageLearningObject.Columns.LearningObjectID, clo.LearningObjectID)
                        .WHERE(CoursePackageLearningObject.Columns.CoursePackageID, iCoursePackageID);
                        cplo.LoadAndCloseReader(q.ExecuteReader());

                        tnLearningObject.ToolTip = cplo.Id == 0 ? "80" : cplo.PassingScore.ToString();

                        foreach (LearningObjectPageHierarchy lop in cTree.PageHierarchy)
                        {
                            if (lop.LearningObjectID == clo.Id)
                            {
                                TreeNode tnPage = new TreeNode(lop.PageName, lop.Id.ToString());
                                tnPage.Checked = true; //for easier tracking of pages
                                tnLearningObject.ChildNodes.Add(tnPage);
                            }
                        }
                    }
                }

                if (tnCourse != seedNode)
                {
                    BuildTree(iCoursePackageID, cTree, sco, tnCourse, rootNode);
                }
            }
            //}
        }

        private void BuildObjectivesPage(CourseTree cTree, TreeNode rootNode)
        {
            string sFile = GetContentFolder(cCourse.Id) + @"\0\" + "objectives.html";
            System.IO.StreamWriter sw = null;
            try
            {
                if (!System.IO.Directory.Exists(Path.GetDirectoryName(sFile)))
                    System.IO.Directory.CreateDirectory(Path.GetDirectoryName(sFile));

                if (File.Exists(sFile))
                    File.Delete(sFile);

                System.Text.StringBuilder sb = new StringBuilder();
                sb.AppendLine("<div id='objectiveswrapper'>");
                sb.AppendLine("<h2 id='objectivescoursename'>");
                sb.AppendLine(cCourse.CourseName);
                sb.AppendLine("</h2>");

                sb.AppendLine("<h3 class='coursedescription'>");
                sb.AppendLine(cCourse.CourseDescription);
                sb.AppendLine("</h3>");

                if (cPackage.DisplayOverallSummary)
                {
                    sb.AppendLine("<h3 class='summarymessage'>");
                    sb.AppendLine(cPackage.SummaryMessage);
                    sb.AppendLine("</h3>");
                }

                sb.AppendLine("<div id='objectivesTree'>");
                sb.AppendLine("<ul class='scosummary'>");

                BuildDescriptionAndObjectivesList(sb, rootNode, cTree);

                sb.AppendLine("</ul>");

                sb.AppendLine("</div>");

                if (cPackage.Disclaimer.Trim() != "")
                {
                    sb.AppendLine("<h3 class='disclaimer'>");
                    sb.AppendLine(cPackage.Disclaimer);
                    sb.AppendLine("</h3>");
                }

                sb.AppendLine("<script type='text/javascript'>");
                sb.AppendLine("var tree = treeMenu($('objectivesTree'));");
                sb.AppendLine("tree.expandAll();");
                sb.AppendLine("$('courseName').innerHTML = $('objectivescoursename').innerHTML;");
                sb.AppendLine("$('objectivescoursename').remove();");
                sb.AppendLine("</script>");

                sb.AppendLine("</div>");

                sw = new StreamWriter(sFile, false);
                sw.WriteLine(sb.ToString());
            }
            catch { throw; }
            finally
            {
                if (sw != null)
                {
                    sw.Flush();
                    sw.Close();
                }
            }
        }

        private void BuildDescriptionAndObjectivesList(System.Text.StringBuilder sb, TreeNode node, CourseTree courses)
        {
            foreach (TreeNode child in node.ChildNodes)
            {
                CourseLearningObjectsHierarchy cloh;

                //has child nodes, and the child nodes are checked (indicating that it has pages)
                if (child.ChildNodes.Count > 0 && child.ChildNodes[0].Checked)
                {
                    //learning object
                    //find the object
                    cloh = courses.LearningObjectHierarchy.Find(delegate(CourseLearningObjectsHierarchy inner)
                    {
                        return inner.Id.ToString() == child.Value;
                    });

                    //this removes the ">" from the ul, so we can add a class
                    int idx = sb.ToString().LastIndexOf(">");
                    sb.Remove(idx, sb.Length - idx);
                    sb.AppendLine(" class='learningobject'>");

                    sb.AppendLine("<li class='sconame'>");

                    sb.AppendLine("<span class='loName'>" + cloh.LearningObjectName + "</span>");

                    sb.AppendLine("<span class='descriptionTitle'>Description:</span>");

                    sb.AppendLine("<span class='descriptionContent'>" + cloh.LearningObjectDescription + "</span>");

                    sb.AppendLine("<span class='objectivesTitle'>Objectives:</span>");

                    sb.AppendLine("<span class='objectivesContent'>" + cloh.Objectives + "</span>");

                    sb.AppendLine("</li>");
                }
                else if (child.ChildNodes.Count > 0 && !child.ChildNodes[0].Checked)
                {
                    //group
                    sb.Append("<li class='aggregate'><span>");
                    sb.Append(child.Text);

                    sb.AppendLine("</span>");
                    sb.AppendLine("<ul>");

                    BuildDescriptionAndObjectivesList(sb, child, courses);

                    sb.AppendLine("</ul>");
                    sb.AppendLine("</li>");
                }
            }
        }

        /// <summary>
        /// Reads through the tree, calling WritePageContent for each page
        /// </summary>
        /// <param name="cTree"></param>
        /// <param name="rootCourse"></param>
        /// <param name="seedNode"></param>
        private void BuildPages(CourseTree cTree, CourseHierarchy rootCourse, TreeNode seedNode, string sPackageTypeCode)
        {
            foreach (TreeNode child in seedNode.ChildNodes)
            {
                if (child.Checked) //our nice page indicator
                {
                    //find the page
                    LearningObjectPageHierarchy page = cTree.PageHierarchy.Find(delegate(LearningObjectPageHierarchy searchPage)
                    {
                        return searchPage.Id.ToString() == child.Value; //when we build the nodes, we stuff in id in the value param
                    });
                    string sFolder = "";
                    //write out the page contents
                    //if (sPackageTypeCode == "AICC")
                    //    sFolder = GetSCORMFolder(0); //want the root here
                    //else
                    sFolder = GetContentFolder(rootCourse.Id);
                    WritePageContent(sFolder + seedNode.ValuePath + @"\", rootCourse.Id, child.Depth, page, sPackageTypeCode);
                }
                //recurse
                BuildPages(cTree, rootCourse, child, sPackageTypeCode);
            }
        }

        private void BuildTests(CourseTree cTree, CourseHierarchy rootCourse, string sPackageTypeCode)
        {
            string sContentFolder = GetContentFolder(rootCourse.Id);
            string sSharedFolder = GetSharedFolder(sPackageTypeCode);

            if (cPackage.PostTest)
            {
                string sTestType = "sequential";
                TestStyleType tst = new TestStyleType(cPackage.PostTestStyleTypeID);
                if (tst.TypeName == TestType.JEOPARDY)
                {
                    sTestType = "jeopardy";
                }
                if (!Directory.Exists(sContentFolder + "posttest"))
                {
                    Directory.CreateDirectory(sContentFolder + "posttest");
                }
                File.Copy(sSharedFolder + "content\\" + sTestType + ".html", sContentFolder + "posttest\\test.html", true);
                File.SetLastWriteTime(sContentFolder + "posttest\\test.html", DateTime.Now);
                File.Copy(sSharedFolder + "content\\review.html", sContentFolder + "posttest\\review.html", true);
                File.SetLastWriteTime(sContentFolder + "posttest\\review.html", DateTime.Now);
            }

            if (cPackage.PreTest)
            {
                string sTestType = "sequential";
                TestStyleType tst = new TestStyleType(cPackage.PreTestStyleTypeID);
                if (tst.TypeName == TestType.JEOPARDY)
                {
                    sTestType = "jeopardy";
                }
                if (!Directory.Exists(sContentFolder + "pretest"))
                {
                    Directory.CreateDirectory(sContentFolder + "pretest");
                }
                File.Copy(sSharedFolder + "content\\" + sTestType + ".html", sContentFolder + "pretest\\test.html", true);
                File.SetLastWriteTime(sContentFolder + "pretest\\test.html", DateTime.Now);
                File.Copy(sSharedFolder + "content\\review.html", sContentFolder + "pretest\\review.html", true);
                File.SetLastWriteTime(sContentFolder + "pretest\\review.html", DateTime.Now);
            }
        }

        private void BuildSkin(CourseTree cTree, CourseHierarchy rootCourse, string sPackageTypeCode)
        {
            string sContentFolder = GetContentFolder(rootCourse.Id);
            string sSkinFolder = GetSkinFolder(sPackageTypeCode);

            object o = HttpContext.Current.Session[BankersEdgeApplicationProcessor.SESSION_SUBDOMAIN];
            if (o == null)
            {
                return; //this should never happen.
            }
            string sSubDomain = o.ToString();
            string sSrc = sSkinFolder + sSubDomain + ".playback.css";
            string sDest = sContentFolder + "playback.skin.css";
            if (File.Exists(sSrc))
            {
                File.Copy(sSrc, sDest, true);
                File.SetLastWriteTime(sDest, DateTime.Now);
            }

        }

        /// <summary>
        /// Responsible for doing the actual writing of content to HTML
        /// </summary>
        /// <param name="sFolder"></param>
        /// <param name="lop"></param>
        private void WritePageContent(string sFolder, int iCourseID, int iDepth, LearningObjectPageHierarchy lop, string sPackageTypeCode)
        {
            //to make our lives a little easier, we'll get the pages here...
            BankersEdge.Artisan.Page page = new BankersEdge.Artisan.Page(lop.Id);

            string sID = page.Id.ToString();
            string sFileName = sID + ".html";

            string sContent = GetContentForPage(iCourseID, iDepth, page, sPackageTypeCode);

            System.IO.StreamWriter sw = null;
            System.IO.StreamWriter sw2 = null;
            try
            {
                //write out the content for the sco
                sw = new StreamWriter(sFolder + sFileName, false, Encoding.UTF8);
                sw.Write(sContent);

                //write out a review page (that may or may not get used) to the sco
                string sReviewPage = GetSharedFolder(sPackageTypeCode) + "content\\review.html";
                File.Copy(sReviewPage, sFolder + "review.html", true);
                File.SetLastWriteTime(sFolder + "review.html", DateTime.Now);

                //write out the question pages to the post and pre test folders
                if (questionPages.Contains(page))
                {
                    string[] sTestFolders = new string[] { "posttest\\", "pretest\\" };

                    foreach (string s in sTestFolders)
                    {
                        string sTestFolder = GetContentFolder(iCourseID) + s;

                        if (!System.IO.Directory.Exists(sTestFolder))
                            System.IO.Directory.CreateDirectory(sTestFolder);
                        sw2 = new StreamWriter(sTestFolder + sFileName, false, Encoding.UTF8);
                        sw2.Write(sContent);
                        sw2.Flush();
                        sw2.Close();
                    }
                }
            }
            catch { throw; }
            finally
            {
                if (sw != null)
                {
                    sw.Flush();
                    sw.Close();
                }
            }
        }

        /// <summary>
        /// Gets a string value represeting the entire page content
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        private string GetContentForPage(int iCourseID, int iDepth, BankersEdge.Artisan.Page page, string sPackageTypeCode)
        {
            //start building the content
            string sContent = page.PageTemplate.Html;

            // deal with the page elements
            string sPattern = @"{element(\d*)_(text|media|question|imagequestion)}";
            Regex rPattern = new Regex(sPattern);
            PageElementCollection pageElements = page.PageElementRecords();

            if (page.PageTemplate.TemplateName.ToLower().Replace(" ", "") == "imagequestion")
            {
                // special handling for image questions (hotspots)
                // image questions come with an asset and a question. we won't count on a particular order
                Asset asset = null; Question quest = null; PageElement assetElement = null;
                foreach (PageElement element in pageElements)
                {
                    if (element.AssetID > 0)
                    {
                        sContent = GetAssetContent(iCourseID, iDepth, sContent, rPattern, element, sPackageTypeCode);
                        sContent = rPattern.Replace(sContent, ""); //trims off the extra placeholder for design mode
                        assetElement = element;
                        asset = element.Asset;
                    }
                    else if (element.QuestionID > 0)
                        quest = element.Question;
                }


                if (quest == null)
                    throw new Exception("No question has been defined for the page '" + page.PageName + "'.");
                if (quest.AnswerRecords() == null)
                    throw new Exception("No answers have been defined for the page '" + page.PageName + "'.");

                Coords coords = null;
                if (quest.AnswerRecords().Count > 0)
                    coords = AjaxPro.JavaScriptDeserializer.DeserializeFromJson<Coords>(quest.AnswerRecords()[0].TextX);

                if (coords == null)
                    throw new Exception("Image question found, with no coordinates for the answer.");
                if (assetElement == null)
                    throw new Exception("No asset element found in an image question template!");

                //ok, now we have the asset content and the coordinates, and just need to write the whole thing out
                string sTemplateFileName = quest.QuestionType.TypeName.ToLower() + ".html";
                string sPageTemplate = GetQuestionAnswerTemplate(sTemplateFileName, sPackageTypeCode);

                sPageTemplate = sPageTemplate
                    .Replace("{question}", quest.TextX)
                    .Replace("{correctFeedback}", quest.CorrectFeedback)
                    .Replace("{incorrectFeedback}", quest.IncorrectFeedback)
                    .Replace("{assetcontent}", sContent)
                    .Replace("{asset_element_id}", assetElement.Id.ToString())
                    .Replace("{coords}", coords.x1 + "," + coords.y1 + "," + coords.x2 + "," + coords.y2);

                sContent = sPageTemplate;

                if (bTrackQuestionPages)
                    this.questionPages.Add(page);
            }
            else
            {
                foreach (PageElement element in pageElements)
                {
                    if (element.AssetID > 0)
                    {
                        //validation
                        Asset asset = element.Asset;
                        if (asset == null)
                            throw new Exception("Whoops! Invalid data! An asset ID was found in the PageElements with no corresponding asset!");

                        sContent = GetAssetContent(iCourseID, iDepth, sContent, rPattern, element, sPackageTypeCode);
                    }
                    else if (element.QuestionID > 0)
                    {
                        //validation
                        Question quest = element.Question;
                        if (quest == null)
                            throw new Exception("Whoops! Invalid data! A question ID was found in the PageElements with no corresponding question!");
                        if (bTrackQuestionPages)
                            this.questionPages.Add(page);
                        //this method does't need the content element passed in, as it completely replaces all content
                        //using the html templates in the shared folder
                        sContent = GetQuestionAnswerContent(quest, sPackageTypeCode);
                    }
                    else
                    {
                        // get the page content
                        string sText = element.PageElementValue;
                        sText = Regex.Replace(sText, "^Click here to enter text", ""); //if it's left in the template, ignore it.

                        // save the base url for all assets
                        string sURL = Asset.GetBaseURL(false);
                        if (!sURL.EndsWith("/"))
                            sURL += "/";

                        // rebase the url in the page content to point to the packaged folder location
                        sText = sText.Replace(sURL, "");

                        // load up the html in a simple document
                        HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                        doc.LoadHtml(sText);

                        // grab all the tagged assets
                        HtmlAgilityPack.HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes("//*[@class='artisan_asset']");

                        if (nodes != null)
                        {
                            // save them to disk
                            foreach (HtmlAgilityPack.HtmlNode node in nodes)
                            {
                                // extract the asset and save it in the package
                                if (node.Attributes["assetid"] != null)
                                {
                                    string id = node.Attributes["assetid"].Value;
                                    Asset cAsset = new Asset(id);
                                    SaveAsset(iCourseID, cAsset, sPackageTypeCode);
                                }
                            }
                        }

                        sContent = rPattern.Replace(sContent, sText, 1);
                    }
                }
            }
            return sContent;
        }

        /// <summary>
        /// Gets the content for the specified element, following the "asset" pattern
        /// </summary>
        /// <param name="sCurrentContent">The current content of the page, with a placeholder for the asset</param>
        /// <param name="rPattern">The pattern used to find the asset placeholder within the sCurrentContent</param>
        /// <param name="element">The page element itself</param>
        /// <param name="iCourseID">The ID of the current course, for determining the asset folder</param>
        /// <param name="iDepth">The depth of the current page, for creating relative URLs</param>
        /// <returns>A string containing sCurrentContent with rPattern run over it containing the content of the page element.</returns>
        private string GetAssetContent(int iCourseID, int iDepth, string sCurrentContent, Regex rPattern, PageElement element, string sPackageTypeCode)
        {
            //for now, just a blank string, but potentially it'll be "../" for however deep in the tree this asset is.
            string sURLReplacement = "";
            //for (int i = 0; i < iDepth + 1; i++) //+1 accounts for the fact that we actually go 1 level above the root to get to the assets folder
            //    sURLReplacement += "../";
            //sURLReplacement = sURLReplacement.TrimEnd('/'); //remove the last one, since the url itself will have a /

            string sTemplate = element.Asset.AssetType.TemplateHTML;
            //ok, in preview mode, we want the full asset url...
            //in package mode, we want a relative url.
            string sBaseURL = Asset.GetBaseURL(false);
            if (!sBaseURL.EndsWith("/"))
                sBaseURL += "/"; //the final output needs to NOT have the initial "/", so we add it here so it'll get removed in the replace

            string sURL = element.PageElementValue;
            if (!Preview)
                sURL = sURL.Replace(sBaseURL, sURLReplacement);

            SaveAsset(iCourseID, element.Asset, sPackageTypeCode);

            //some handling for additional parameters...
            string sQueryString = "?";
            foreach (PageElementParameter param in element.PageElementParameterRecords())
            {
                string sValue = param.ValueX;
                if (!Preview)
                    sValue = sValue.Replace(sBaseURL, sURLReplacement);
                sQueryString += HttpContext.Current.Server.UrlEncode(param.Key) + "=" + HttpContext.Current.Server.UrlEncode(sValue);
            }

            if (sQueryString.Length > 1)
                sURL += sQueryString;

            sTemplate = sTemplate.Replace("{value}", sURL);
            sTemplate = sTemplate.Replace("{element_id}", element.Id.ToString());
            sTemplate = sTemplate.Replace("{title}", element.Asset.Title);

            return rPattern.Replace(sCurrentContent, sTemplate, 1);
        }

        /// <summary>
        /// Deletes Folder Contents
        /// </summary>
        /// <param name="sFolderName"></param>

        private void DeleteFolderContents(string sContentFolder)
        {
            try
            {
                string[] sFiles;
                if (Directory.Exists(sContentFolder))
                {
                    sFiles = Directory.GetFiles(sContentFolder);
                    for (int i = 0; i < sFiles.Length; i++)
                    {
                        File.Delete(sFiles[i]);
                    }
                    try
                    {
                        Directory.Delete(sContentFolder, true);
                    }
                    catch { }
                }
                if (!Directory.Exists(sContentFolder))
                    Directory.CreateDirectory(sContentFolder);

                DirectoryInfo di = Directory.GetParent(sContentFolder);
                sFiles = Directory.GetFiles(di.Parent.FullName);
                for (int i = 0; i < sFiles.Length; i++)
                {
                    File.Delete(sFiles[i]);
                }

            }
            catch { }
        }

        private void SaveAsset(int iCourseID, Asset asset, string sPackageTypeCode)
        {
            //copy the asset into the folder for use with the .zip
            string sSourceFolder = HttpContext.Current.Server.MapPath(asset.UploadPath);
            string sSourceFile = Path.Combine(sSourceFolder, asset.FileName);

            //get the subfolder within the source...
            string sSubFolder = asset.UploadPath.Replace("~/Assets/", "").Replace("/", "\\");
            string sDestFolder = GetAssetFolder(iCourseID, sPackageTypeCode) + sSubFolder;
            string sDestFile = Path.Combine(sDestFolder, asset.FileName);

            if (!Directory.Exists(sDestFolder))
                Directory.CreateDirectory(sDestFolder);

            //if this is a group based asset, make sure we get all the files in the folder...
            if (asset.GroupID != null && asset.GroupID > 0)
            {
                DirectoryInfo sourceDirectoryInfo = new DirectoryInfo(sSourceFolder);
                foreach (FileInfo fileInfo in sourceDirectoryInfo.GetFiles())
                {
                    File.Copy(fileInfo.FullName, Path.Combine(sDestFolder, fileInfo.Name), true);
                    File.SetLastWriteTime(Path.Combine(sDestFolder, fileInfo.Name), DateTime.Now);
                }
            }
            else
            {
                if (File.Exists(sSourceFile) && !File.Exists(sDestFile))
                {
                    File.Copy(sSourceFile, sDestFile, true);
                    File.SetLastWriteTime(sDestFile, DateTime.Now);
                }
            }
        }

        /// <summary>
        /// Gets the content for the specified element, following the "question/answer" pattern.
        /// This basically reads an HTML file in, and duplicates the answer template for each answer.
        /// </summary>
        /// <param name="quest"></param>
        /// <returns></returns>
        private string GetQuestionAnswerContent(Question quest, string sPackageTypeCode)
        {
            //read in the appropriate template
            string sTemplateFileName = quest.QuestionType.TypeName.ToLower() + ".html";
            string sPageTemplate = GetQuestionAnswerTemplate(sTemplateFileName, sPackageTypeCode);

            //swap out the answer pattern with real answers
            string sAnswerPattern = "{answertemplate:(?<content>.*):endanswertemplate}";
            Regex rAnswerPattern = new Regex(sAnswerPattern);

            Match m = Regex.Match(sPageTemplate, sAnswerPattern);
            if (!m.Success)
                throw new Exception("No match found! Invalid template!");

            //make sure we keep the full pattern, and just replace the content
            string sLineTemplate = m.Captures[0].Value;
            string sLineContent = m.Groups["content"].Value;

            //add the question
            sPageTemplate = sPageTemplate
                .Replace("{question}", quest.TextX)
                .Replace("{correctFeedback}", quest.CorrectFeedback)
                .Replace("{incorrectFeedback}", quest.IncorrectFeedback);

            //add the answers
            string sLines = "";

            //standard q/a
            foreach (Answer answer in quest.AnswerRecords())
            {
                sLines += sLineContent
                    .Replace("{value}", answer.Id.ToString())
                    .Replace("{text}", answer.TextX)
                    .Replace("{id}", answer.Id.ToString());
            }

            //build the content
            sLines = sLines.Replace("$", "#dollar#");
            string temp = rAnswerPattern.Replace(sPageTemplate, sLines);
            temp = temp.Replace("#dollar#", "$");
            return temp;
        }

        /// <summary>
        /// Reads in the specified HTML template file for a question/answer, and returns the content
        /// </summary>
        /// <param name="sTemplateFileName"></param>
        /// <returns></returns>
        private string GetQuestionAnswerTemplate(string sTemplateFileName, string sPackageTypeCode)
        {
            StreamReader sr = null;
            string sPageTemplate;
            try
            {
                sr = new StreamReader(GetSharedFolder(sPackageTypeCode) + @"\content\" + sTemplateFileName);
                sPageTemplate = sr.ReadToEnd();
            }
            catch { throw; }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                    sr.Dispose();
                }
            }
            return sPageTemplate;
        }

        #endregion
        #region AICC Descriptors

        private void BuildAICCDescriptorFiles(CourseTree cCourseTree, CourseHierarchy chRoot, TreeNode seedNode, string sPackageTypeCode, int iVersion, string sName, string sDescription, string sAuthor)
        {
            AICCBuilder cBuilder = new AICCBuilder();
            AICCOptions cOptions = new AICCOptions();

            int iPreTestScore = 80;
            if (cPackage.PreTestScore != "")
                int.TryParse(cPackage.PreTestScore, out iPreTestScore);

            int iPostTestScore = 80;
            if (cPackage.PostTestScore != "")
                int.TryParse(cPackage.PostTestScore, out iPostTestScore);

            int iPassingScore = 80;
            if (cPackage.PassingScore != "")
                int.TryParse(cPackage.PassingScore, out iPassingScore);

            cOptions.CourseID = chRoot.Id;
            cOptions.MasteryScore = cPackage.PostTest ? iPostTestScore : iPassingScore;
            cOptions.ContentFolderPath = GetContentFolder(chRoot.Id);
            cOptions.SharedFolderPath = GetSharedFolder(sPackageTypeCode);
            cBuilder.CreateAUFile(cCourseTree, cOptions);
            cBuilder.CreateCSTFile(cCourseTree, cOptions);
            cBuilder.CreateDESFile(cCourseTree, cOptions, sName, sDescription, sAuthor);
            cBuilder.CreateCRSFile(seedNode, cOptions, iVersion, sName, sDescription);
        }

        private void BuildAICCContentFiles()
        {
        }
        #endregion
        #region IMS Manifest

        /// <summary>
        /// Creates the IMS Manifest file
        /// </summary>
        /// <param name="cTree"></param>
        private void BuildIMSManifest(CourseTree cTree, CourseHierarchy rootCourse, TreeNode seedNode, string sPackageTypeCode)
        {
            string sFile = GetSCORMFolder(rootCourse.Id) + IMSMANIFEST_FILENAME;

            int iPreTestScore = 80;
            if (cPackage.PreTestScore != "")
                int.TryParse(cPackage.PreTestScore, out iPreTestScore);

            int iPostTestScore = 80;
            if (cPackage.PostTestScore != "")
                int.TryParse(cPackage.PostTestScore, out iPostTestScore);

            int iPassingScore = 80;
            if (cPackage.PassingScore != "")
                int.TryParse(cPackage.PassingScore, out iPassingScore);

            if (sPackageTypeCode == PackageTypeCode.SCORM2k4)
            {
                SCORM2k4Builder cBuilder = new SCORM2k4Builder();
                SCORM2k4Options cOptions = new SCORM2k4Options();


                cOptions.PostTestScore = iPostTestScore;
                cOptions.PostTest = cPackage.PostTest;
                cOptions.PostTestTitle = cPackage.PostTestLabel;
                cOptions.PostTestType = cPackage.PostTestStyleTypeID == 1 ? "jeopardy" : "sequentialorrandom";

                cOptions.PreTestScore = iPreTestScore;
                cOptions.PreTest = cPackage.PreTest;
                cOptions.PreTestTitle = cPackage.PreTestLabel;
                cOptions.PreTestType = cPackage.PreTestStyleTypeID == 1 ? "jeopardy" : "sequentialorrandom";

                cOptions.TestOut = cPackage.TestOut;
                cOptions.TestOnly = cPackage.DisplayTestOnly;
                cOptions.SharedFolderPath = GetSharedFolder("");
                cOptions.ContentFolderPath = GetContentFolder(rootCourse.Id);
                cOptions.CompletionType = cPackage.CompletionType.TypeName.ToString().ToLower();
                cOptions.CourseID = rootCourse.Id;
                cOptions.File = sFile;
                cOptions.NavigationType = cPackage.NavigationType.TypeName.ToString().ToLower();
                cOptions.DisplaySummary = cPackage.DisplayOverallSummary;

                cBuilder.CreateXML(seedNode, cOptions);
            }
            else
            {
                SCORM1_2Builder cBuidler = new SCORM1_2Builder();
                SCORM1_2Options cOptions = new SCORM1_2Options();

                cOptions.CourseID = rootCourse.Id;
                cOptions.File = sFile;
                cOptions.PreTest = cPackage.PreTest;
                cOptions.PostTest = cPackage.PostTest;

                cOptions.PreTestType = cPackage.PreTestStyleTypeID == 1 ? "jeopardy" : "sequentialorrandom";
                cOptions.PostTestType = cPackage.PostTestStyleTypeID == 1 ? "jeopardy" : "sequentialorrandom";

                cOptions.MasteryScore = cPackage.PostTest ? iPostTestScore : iPassingScore;
                cOptions.ContentFolderPath = GetContentFolder(rootCourse.Id);
                cOptions.SharedFolderPath = GetSharedFolder("");

                cBuidler.CreateXML(seedNode, cOptions);
            }
        }

        #endregion
        
        #region Javascript

        /// <summary>
        /// Builds the JS to be used in the SCORM player
        /// </summary>
        /// <param name="cTree"></param>
        /// <param name="rootCourse"></param>
        /// <param name="seedNode"></param>
        /// <remarks>
        /// We're creating something like this:
        /// <code>
        /// var disclaimer = 'Corporate Training Technologies, Inc.';
        /// var emailAddress = 'jkirkell@bankersedge.com;nlouangaphay@bankersedge.com';
        ///                              id  name          bk    note  help  debug  n c   p  
        /// var CourseTree = new Course(26, 'Test Package',true, true, true, false, 0, 1, 1, 0, 80, 'Pre-Test', false, 0, 80, 'Post-Test');
        /// objSco = CourseTree.newSco(0, 'objective', '0/', 1, 80);
        /// objPage = objSco.newPage('objective.html', 1, false);
        /// objSco = CourseTree.newSco(30, 'Sub-Sub Group #1', '2/1/', 1, 80);
        /// objPage = objSco.newPage('allthatapply.html', 1, false);
        /// //answers take the format id, answertext, iscorrect
        /// objAns = objPage.newAnswer(15,'blue',true);
        /// </code>
        /// </remarks>
        private void CreateJS(CourseTree cTree, CourseHierarchy rootCourse, TreeNode seedNode, string sPackageTypeCode)
        {
            
            Course cCourse = new Course(rootCourse.Id);
            if (cCourse.CoursePackageRecords().Count == 0)
                throw new Exception("The course hasn't been packaged!");
            CoursePackage cPackage = cCourse.CoursePackageRecords()[cCourse.CoursePackageRecords().Count - 1];


            System.Text.StringBuilder sb = new StringBuilder();
            sb.Append("var disclaimer = 'Corporate Training Technologies, Inc.';");
            sb.AppendLine();
            sb.Append("var emailAddress = '" + cPackage.EmailAddresses + "';");
            sb.AppendLine();

            sb.Append("var CourseTree = new Course(");
            AddParam(sb, cCourse.Id);
            AddParam(sb, cCourse.CourseName);
            AddParam(sb, cPackage.BookmarkingEnabled);
            AddParam(sb, cPackage.NotesEnabled);
            AddParam(sb, cPackage.HelpEnabled);
            AddParam(sb, cPackage.DebugEnabled);
            AddParam(sb, cPackage.CertificateEnabled);
            AddParam(sb, cPackage.NavigationTypeID);
            AddParam(sb, cPackage.CompletionTypeID);
            AddParam(sb, cPackage.PreTestStyleTypeID);
            AddParam(sb, cPackage.PreTestScore.Trim() == "" ? 80 : int.Parse(cPackage.PreTestScore.Trim()));
            AddParam(sb, cPackage.PreTestLabel);
            AddParam(sb, cPackage.TestOut);
            AddParam(sb, cPackage.PostTestStyleTypeID);
            AddParam(sb, cPackage.PostTestScore.Trim() == "" ? 80 : int.Parse(cPackage.PostTestScore.Trim()));
            AddParam(sb, cPackage.PostTestLabel);
            AddParam(sb, Preview);
            AddParam(sb, cPackage.DisplayQAFeedback);
            AddParam(sb, sPackageTypeCode);

            //remove the last extra ","
            sb.Remove(sb.Length - 1, 1);
            sb.Append(");");
            sb.AppendLine();

            if (cPackage.DisplayOverallSummary)
                CreateObjectivesJS(sb);

            if (cPackage.PostTest)
                CreateTestJS(sb, true);

            //ok, now write out all of what are considered "SCOs" in the JS...which in our case, is really a "LearningObject".
            //don't ask why.
            bool bHideContent = (cPackage.CompletionType.TypeName == Completion.TEST_ONLY && cPackage.DisplayTestOnly);
            if (!bHideContent)
            {
                CreateSCOJS(cTree, rootCourse, seedNode, sb, cPackage.PostTest);
            }

            if (cPackage.PreTest)
                CreateTestJS(sb, false);

            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(GetContentFolder(rootCourse.Id) + ARTISAN_COURSE_FILE_NAME);
                sw.Write(sb.ToString());
            }
            catch { throw; }
            finally
            {
                if (sw != null)
                {
                    sw.Flush();
                    sw.Close();
                }
            }
        }

        private void CreateObjectivesJS(StringBuilder sb)
        {
            //objectives sco
            string sID = "objectives";
            string sTitle = "Objectives";
            string sFolder = "0/";
            sb.Append("objSco = CourseTree.newSco(");
            AddParam(sb, sID);
            AddParam(sb, sTitle);
            AddParam(sb, sFolder);
            AddParam(sb, 2);
            AddParam(sb, 0);//TODO: allow non-default passing score
            sb.Remove(sb.Length - 1, 1);
            sb.Append(");");
            sb.AppendLine();

            //single page
            sb.Append("objPage = objSco.newPage(");
            AddParam(sb, "objectives");
            AddParam(sb, sTitle);
            AddParam(sb, 1);
            AddParam(sb, 1);
            AddParam(sb, true);
            AddParam(sb, 0); //question type
            AddParam(sb, ""); //question text

            sb.Remove(sb.Length - 1, 1);
            sb.Append(");");
            sb.AppendLine();
        }

        /// <summary>
        /// Builds the pre/post test JS
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="bIsPostTest"></param>
        private void CreateTestJS(StringBuilder sb, bool bIsPostTest)
        {
            string sID = POSTTEST_SCO_ID;
            string sTitle = "Post-Test";
            string sFolder = "posttest/";
            int iScore = 80;
            int.TryParse(this.cPackage.PostTestScore, out iScore);
            if (!bIsPostTest)
            {
                sID = PRETEST_SCO_ID;
                sTitle = "Pre-Test";
                sFolder = "pretest/";
                int.TryParse(this.cPackage.PreTestScore, out iScore);
            }

            //test sco
            sb.AppendLine();
            sb.Append("objSco = CourseTree.newSco(");
            AddParam(sb, sID);
            AddParam(sb, sTitle);
            AddParam(sb, sFolder);
            AddParam(sb, 2);
            AddParam(sb, iScore);
            sb.Remove(sb.Length - 1, 1);
            sb.Append(");");
            sb.AppendLine();

            //single page
            sb.Append("objPage = objSco.newPage(");
            AddParam(sb, "test"); //id

            AddParam(sb, sTitle); //title
            AddParam(sb, 1); //page type
            AddParam(sb, true); //calculate grade

            //this page has no question or question text
            AddParam(sb, 0); //question type
            AddParam(sb, ""); //question text

            sb.Remove(sb.Length - 1, 1);
            sb.Append(");");
            sb.AppendLine();

            foreach (BankersEdge.Artisan.Page page in questionPages)
            {
                string sQuestionText = "";
                bool bQuestionVisibleInContent = false;
                //pull out any question test and the visibility indicator
                foreach (PageElement pe in page.PageElementRecords())
                {
                    if (pe.QuestionID > 0)
                    {
                        if (page.CoursePackagePageRecords().Count > 0)
                        {
                            bQuestionVisibleInContent = page.CoursePackagePageRecords()[0].Visible;
                        }
                        sQuestionText = pe.Question.TextX;
                    }
                }

                //if the question is visible inline, skip it in the post/pre-test
                if (bQuestionVisibleInContent) continue;

                sb.Append("objPage = objSco.newPage(");
                AddParam(sb, page.Id); //id
                AddParam(sb, page.PageName); //title
                AddParam(sb, page.PageTypeID); //page type
                AddParam(sb, true); //calculate grade
                AddParam(sb, page.PageTemplate.QuestionTypeID);
                AddParam(sb, sQuestionText);

                sb.Remove(sb.Length - 1, 1);
                sb.Append(");");
                sb.AppendLine();

                foreach (PageElement pe in page.PageElementRecords())
                {
                    if (pe.QuestionID > 0)
                    {

                        foreach (Answer answer in pe.Question.AnswerRecords())
                        {
                            sb.Append("objAns = objPage.newAnswer(");
                            AddParam(sb, answer.Id);
                            AddParam(sb, answer.TextX);
                            AddParam(sb, answer.IsCorrect);
                            sb.Remove(sb.Length - 1, 1);
                            sb.Append(");");
                            sb.AppendLine();
                        }
                    }
                }
            }
            sb.AppendLine();
        }

        /// <summary>
        /// Creates the JS for the individual LOs, AKA SCOs
        /// </summary>
        /// <param name="cTree"></param>
        /// <param name="rootCourse"></param>
        /// <param name="seedNode"></param>
        /// <param name="sb"></param>
        private void CreateSCOJS(CourseTree cTree, CourseHierarchy rootCourse, TreeNode seedNode, StringBuilder sb, bool bPostTest)
        {
            foreach (TreeNode childNode in seedNode.ChildNodes)
            {
                if (childNode.ChildNodes.Count > 0 && childNode.ChildNodes[0].Checked)
                {
                    //learning object. write out the js.
                    CourseLearningObjectsHierarchy clo = cTree.LearningObjectHierarchy.Find(delegate(CourseLearningObjectsHierarchy searchItem)
                    {
                        return searchItem.Id.ToString() == childNode.Value; //when we build the nodes, we stuff in id in the value param
                    });

                    CoursePackageLearningObject package = new CoursePackageLearningObject();
                    package.LoadByParam(CoursePackageLearningObject.Columns.LearningObjectID, clo.Id);
                    if (package.Id == 0)
                        package.PassingScore = 80; //default passing score

                    //uuuugh. we call learning objects "SCOs" in the JS. just to be nice and consistent.
                    sb.Append("objSco = CourseTree.newSco(");
                    AddParam(sb, clo.Id);
                    AddParam(sb, clo.LearningObjectName);
                    AddParam(sb, childNode.ValuePath + "/");
                    //maybe just grab from the parent sco???
                    AddParam(sb, DEFAULT_SCO_TYPE); //sco type 
                    AddParam(sb, package.PassingScore); // passing score.

                    sb.Remove(sb.Length - 1, 1);
                    sb.Append(");");
                    sb.AppendLine();

                    foreach (TreeNode pageNode in childNode.ChildNodes)
                    {
                        LearningObjectPageHierarchy page = cTree.PageHierarchy.Find(delegate(LearningObjectPageHierarchy searchPage)
                        {
                            return searchPage.Id.ToString() == pageNode.Value; //when we build the nodes, we stuff in id in the value param
                        });
                        CoursePackagePage pagePackage = new CoursePackagePage();
                        pagePackage.LoadByParam(CoursePackagePage.Columns.PageID, page.Id);

                        BankersEdge.Artisan.Page fullPage = new BankersEdge.Artisan.Page(page.Id);


                        // Handle when/if question should be displayed
                        if (
                            (cPackage.CompletionType.TypeName.ToLower() == Completion.TEST_ONLY ||
                             cPackage.CompletionType.TypeName.ToLower() == Completion.INLINE_QUESTIONS_ONLY ||
                             cPackage.CompletionType.TypeName.ToLower() == Completion.NAVIGATION_ONLY) &&
                            pagePackage.Id > 0 &&
                            !pagePackage.Visible &&
                            fullPage.PageType.TypeName.ToLower() == "question") continue; //don't include q pages that are not marked as visible

                        sb.Append("objPage = objSco.newPage(");
                        AddParam(sb, page.Id); //id
                        AddParam(sb, page.PageName); //title
                        AddParam(sb, page.PageTypeID); //page type

                        //if there's a post test or course is navigation only, inline questions cannot be for grade
                        if (bPostTest || (cPackage.CompletionType.TypeName.ToLower() == Completion.NAVIGATION_ONLY))
                            AddParam(sb, false);
                        else
                            AddParam(sb, pagePackage.CalculateScore); //calculate grade

                        //add question type id
                        PageTemplate pt = new PageTemplate(page.PageTemplateID);
                        AddParam(sb, pt.QuestionTypeID);

                        //add question text
                        bool bQuestionHolderInPlace = false;
                        if (pt.QuestionTypeID > 0)
                        {
                            sb.Append("{" + page.Id + "_QuestionText}" + ",");
                            bQuestionHolderInPlace = true;
                        }
                        else
                            AddParam(sb, "");

                        sb.Remove(sb.Length - 1, 1);
                        sb.Append(");");
                        sb.AppendLine();

                        //now write out the answers, if this is a q/a page
                        if (fullPage.PageType.TypeName.ToString().ToLower() == "question")
                        {
                            foreach (PageElement element in fullPage.PageElementRecords())
                            {
                                if (element.QuestionID > 0)
                                {
                                    foreach (Answer answer in element.Question.AnswerRecords())
                                    {
                                        sb.Append("objAns = objPage.newAnswer(");
                                        AddParam(sb, answer.Id);
                                        AddParam(sb, answer.TextX);
                                        AddParam(sb, answer.IsCorrect);

                                        sb.Remove(sb.Length - 1, 1);
                                        sb.Append(");");
                                        sb.AppendLine();
                                    }

                                    AjaxPro.JavaScriptString s = new AjaxPro.JavaScriptString(element.Question.TextX);
                                    sb.Replace("{" + page.Id + "_QuestionText}", s.Value);
                                    bQuestionHolderInPlace = false;
                                }
                            }
                        }

                        if (bQuestionHolderInPlace)
                            throw new Exception("The page " + fullPage.PageName + " is incomplete! It is a question page, but no question has been defined!");
                    }
                }
                else if (childNode.ChildNodes.Count > 0 && !childNode.ChildNodes[0].Checked)
                {
                    //course...recurse
                    CreateSCOJS(cTree, rootCourse, childNode, sb, bPostTest);
                }
            }
        }

        private void AddParam(StringBuilder sb, int iParam)
        {
            sb.Append(iParam + ",");
        }

        private void AddParam(StringBuilder sb, string sParam)
        {
            //using this method means we get "smart" quotes
            AjaxPro.JavaScriptString js = new AjaxPro.JavaScriptString(sParam);
            sb.Append(js.Value + ",");
        }

        private void AddParam(StringBuilder sb, bool bParam)
        {
            sb.Append(bParam.ToString().ToLower() + ",");
        }

        #endregion

        #region Main

        /// <summary>
        /// Generates a course package based on a given course id
        /// </summary>
        /// <param name="courseID"></param>
        /// <param name="packageType"></param>
        /// <param name="bPreview"></param>
        /// <returns>The URL of the course, if successful.</returns>
        public string GenerateCourse(int iCourseID, string sPackageTypeCode, bool bPreview)
        {
            Preview = bPreview;
            SelectedPackageType = sPackageTypeCode;
            //this.m_eCurrentPackagerScenario = DetermineScenario(iCourseID);
            try
            {
                if (bPreview)
                {
                    sPackageTypeCode = PackageTypeCode.SCORM2k4;//always use 2k4 for previewing
                }

                // first of all, get the tree.
                CourseTree cCourseTree = new CourseTree(iCourseID);
                
                if (cCourseTree.CourseHierarchy.Count == 0)
                    throw new Exception("The course with ID '" + iCourseID + "' could not be found");

                // build the tree structure from the result
                // first, find the item with a ParentID of 0...thats our root
                CourseHierarchy chRoot = null;
                foreach (CourseHierarchy ch in cCourseTree.CourseHierarchy)
                {
                    if (ch.ParentCourseID == 0)
                    {
                        chRoot = ch;
                        break;
                    }
                }
                //create a root node...
                TreeView tv = new TreeView();
                TreeNode tnRoot = new TreeNode(chRoot.CourseName, chRoot.Id.ToString());
                tv.Nodes.Add(tnRoot);

                //get the package params
                cCourse = new Course(iCourseID);

                if (cCourse.CoursePackageRecords().Count == 0)
                    throw new Exception("The course hasn't been packaged!");

                cPackage = cCourse.CoursePackageRecords()[cCourse.CoursePackageRecords().Count - 1];

                //build the folder structure from the tree...
                BuildTree(cPackage.Id, cCourseTree, chRoot, tnRoot, tnRoot);
                //create the "content" folder
                string sContentFolder = "";
                //if (sPackageTypeCode == "AICC")
                //    sContentFolder = GetSCORMFolder(iCourseID); //this is the level we need for AICC no content subdirectory
                //else
                sContentFolder = GetContentFolder(iCourseID); //SCORM2004 content directory Level  
                string sAssetFolder = GetAssetFolder(iCourseID, sPackageTypeCode);

                DeleteFolderContents(sContentFolder);
                if (!Directory.Exists(sAssetFolder))
                    Directory.CreateDirectory(sAssetFolder);
                //create the folder structure on disk
                CreateFolderStructure(tnRoot, chRoot, sPackageTypeCode);

                //write out the JSON structure of the course tree
                string sJSON = AjaxPro.JavaScriptSerializer.Serialize(cCourseTree);
                System.IO.File.WriteAllText(sContentFolder + JSON_FILE_NAME, "window." + JSON_OBJECT_NAME + " = " + sJSON +
                    ";\nwindow." + JSON_OBJECT_NAME + ".testonly = " + (cPackage.CompletionType.TypeName == Completion.TEST_ONLY && cPackage.DisplayTestOnly ? "true" : "false") + ";");


                ////ok, now on to the good stuff...creating content.
                ////first, we create the objectives page
                BuildObjectivesPage(cCourseTree, tnRoot);

                //then, we add pages
                BuildPages(cCourseTree, chRoot, tnRoot, sPackageTypeCode);

                BuildTests(cCourseTree, chRoot, sPackageTypeCode);

                BuildSkin(cCourseTree, chRoot, sPackageTypeCode);

                // define package sructure
                if (sPackageTypeCode == PackageTypeCode.AICC)
                    //write oute AICC descriptors
                    BuildAICCDescriptorFiles(cCourseTree, chRoot, tnRoot, sPackageTypeCode, cCourse.VersionID, cCourse.CourseName, cCourse.CourseDescription, cCourse.CreatedBy);
                else if (sPackageTypeCode == PackageTypeCode.SCORM12 || sPackageTypeCode == PackageTypeCode.SCORM2k4)
                    BuildIMSManifest(cCourseTree, chRoot, tnRoot, sPackageTypeCode);

                //create a JS file for navigation
                CreateJS(cCourseTree, chRoot, tnRoot, sPackageTypeCode);
                //finally, pack it all up nicely...
                PackageZip(chRoot, bPreview, sPackageTypeCode);

                return Asset.GetBaseURL(false) + @"/scorm/" + iCourseID + "/" + (Preview ? @"preview" : sPackageTypeCode.ToLower()) + @"/content/default.html";
            }
            catch (Exception ex)
            {
                Log.Warn(ex.Message, ex);
                throw ex;
            }
        }

        //public static string Serialize(object o)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    string json;
        //    try
        //    {
        //        DataContractJsonSerializer serializer = new DataContractJsonSerializer(o.GetType());
        //        serializer.WriteObject(ms, o);
        //        json = Encoding.ASCII.GetString(ms.ToArray());
        //    }
        //    finally
        //    {
        //        ms.Close();
        //        ms.Dispose();
        //    }
        //    return json;
        //}

        #endregion
    }
}