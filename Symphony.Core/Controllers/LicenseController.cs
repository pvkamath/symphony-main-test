﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Symphony.Core.Models;
using System.Configuration;
using SubSonic;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Transactions;
using Newtonsoft.Json;

namespace Symphony.Core.Controllers
{
    public class LicenseController : Controllers.SymphonyController
    {

        public static SingleResult<License> SaveLicense(int licenseId, License license, bool reparent = false)
        {
            Data.License licenseData = new Data.License(licenseId);
            if (license.CustomerId.HasValue && license.CustomerId.Value == 0) license.CustomerId = null;

            if (reparent) // just change the parent id
            {
                licenseData.ParentID = license.ParentId;
                licenseData.Save();
            }
            else // save the full license
            {
                if (license != null)
                {
                    license.CopyTo(licenseData);
                }

                licenseData.Id = licenseId;
                licenseData.AccreditationBoardID = license.AccreditationBoardID;

                if (licenseData.JurisdictionID == 0 && !string.IsNullOrWhiteSpace(license.Jurisdiction))
                {
                    Data.Jurisdiction j = null;
                    int jurisdictionId = 0;

                    int.TryParse(license.Jurisdiction, out jurisdictionId);
                    if (jurisdictionId > 0)
                    {
                        j = new Data.Jurisdiction(jurisdictionId);
                    }
                    else
                    {
                        j = new Data.Jurisdiction(Data.Jurisdiction.Columns.Name, license.Jurisdiction);
                    }
                    
                    if (j.Id == 0)
                    {
                        j.Name = license.Jurisdiction;
                        j.Save();
                    }

                    licenseData.JurisdictionID = j.Id;
                }

                if (licenseData.ProfessionID == 0 && !string.IsNullOrWhiteSpace(license.Profession))
                {
                    Data.Profession p = null;
                    int professionId = 0;

                    int.TryParse(license.Profession, out professionId);
                    if (professionId > 0)
                    {
                        p = new Data.Profession(professionId);
                    }
                    else
                    {
                        p = new Data.Profession(Data.Profession.Columns.Name, license.Jurisdiction);
                    }
                    
                    if (p.Id == 0)
                    {
                        p.Name = license.Profession;
                        p.Save();
                    }
                    licenseData.ProfessionID = p.Id;
                }

                

                // save the data fields
                // We save this as json to make it easy to work with using the
                // componentBuilder. Extract it from the xml and save the fields.
                // Doing this for backwards compatibility in case anything else 
                // relies on the license to user field mapping.
                UserDataFieldForm form = JsonConvert.DeserializeObject<UserDataFieldForm>(license.CertificateFormFieldJson);

                licenseData.RequiredUserFields = "";

                List<Data.UserDataField> fields = new List<Data.UserDataField>();
                foreach (UserDataField field in form.Items)
                {
                    if (string.IsNullOrWhiteSpace(field.Name) || field.Xtype == "component")
                    {
                        continue;
                    }

                    if (field.ID > 0)
                    {
                        Data.UserDataField fieldData = new Data.UserDataField(field.ID);
                        if (fieldData != null)
                        {
                            fields.Add(fieldData);
                        }
                    }
                    else
                    {
                        licenseData.RequiredUserFields += (licenseData.RequiredUserFields.Length > 0 ? ", " : "") + field.CodeName;
                    }
                }

                // save the license
                licenseData.Save();

                // save license assignments
                foreach (LicenseAssignment la in license.LicenseAssignments)
                {
                    SaveLicenseAssignment(licenseId, la);
                }

                // map the data fields to the license
                var query = Select.AllColumnsFrom<Data.UserDataFieldLicenseMap>()
                    .Where(Data.UserDataFieldLicenseMap.Columns.LicenseID).IsEqualTo(licenseData.Id);
                query.QueryCommandType = SqlQuery.QueryType.Delete;
                query.Execute();
                foreach (var field in fields)
                {
                    Data.UserDataFieldLicenseMap map = new Data.UserDataFieldLicenseMap();
                    map.UserDataFieldID = field.Id;
                    map.LicenseID = licenseData.Id;
                    map.Save();
                }
            }

            return LicenseController.GetLicense(licenseData.Id);
        }

        private static void SaveLicenseAssignment(int licenseId, LicenseAssignment licenseAssignment)
        {
            if (licenseAssignment.UserId > 0)
            {
                _SaveUserLicenseAssignment(licenseId, licenseAssignment);
                }

            if (licenseAssignment.JobRoleId > 0)
            {
                var qCheckExistingAssignment = new Select().From<Data.LicenseAssignment>()
                    .Where(Data.LicenseAssignment.Columns.JobRoleID).IsEqualTo(licenseAssignment.JobRoleId)
                    .And(Data.LicenseAssignment.Columns.LicenseID).IsEqualTo(licenseId).ExecuteTypedList<Data.LicenseAssignment>();

                if (qCheckExistingAssignment.Count == 0)
                {
                    var newLicenseAssignment = new Data.LicenseAssignment()
                    {
                        LicenseID = licenseId,
                        JobRoleID = licenseAssignment.JobRoleId,
                        StartDate = licenseAssignment.StartDate,
                        ExpiryDate = licenseAssignment.ExpiryDate,
                        AssignmentStatusId = licenseAssignment.AssignmentStatusId,
                        CustomerId = licenseAssignment.CustomerId
                    };
                    newLicenseAssignment.Save();
                }
                else 
                {
                    var editExistingAssignment = qCheckExistingAssignment.First();
                    editExistingAssignment.StartDate = licenseAssignment.StartDate;
                    editExistingAssignment.ExpiryDate = licenseAssignment.ExpiryDate;
                    editExistingAssignment.AssignmentStatusId = licenseAssignment.AssignmentStatusId;

                    editExistingAssignment.Save();
                }
            }

            if (licenseAssignment.LocationId > 0)
            {
                var qCheckExistingAssignment = new Select().From<Data.LicenseAssignment>()
                    .Where(Data.LicenseAssignment.Columns.LocationID).IsEqualTo(licenseAssignment.LocationId)
                    .And(Data.LicenseAssignment.Columns.LicenseID).IsEqualTo(licenseId).ExecuteTypedList<Data.LicenseAssignment>();

                if (qCheckExistingAssignment.Count == 0)
                {
                    var newLicenseAssignment = new Data.LicenseAssignment()
                    {
                        LicenseID = licenseId,
                        LocationID = licenseAssignment.LocationId,
                        StartDate = licenseAssignment.StartDate,
                        ExpiryDate = licenseAssignment.ExpiryDate,
                        AssignmentStatusId = licenseAssignment.AssignmentStatusId,
                        CustomerId = licenseAssignment.CustomerId
                    };
                    newLicenseAssignment.Save();
                }
                else
                {
                    var editExistingAssignment = qCheckExistingAssignment.First();
                    editExistingAssignment.StartDate = licenseAssignment.StartDate;
                    editExistingAssignment.ExpiryDate = licenseAssignment.ExpiryDate;
                    editExistingAssignment.AssignmentStatusId = licenseAssignment.AssignmentStatusId;

                    editExistingAssignment.Save();
                }
            }

            if (licenseAssignment.AudienceId > 0)
            {
                var qCheckExistingAssignment = new Select().From<Data.LicenseAssignment>()
                    .Where(Data.LicenseAssignment.Columns.AudienceId).IsEqualTo(licenseAssignment.AudienceId)
                    .And(Data.LicenseAssignment.Columns.LicenseID).IsEqualTo(licenseId).ExecuteTypedList<Data.LicenseAssignment>();

                if (qCheckExistingAssignment.Count == 0)
                {
                    var newLicenseAssignment = new Data.LicenseAssignment()
                    {
                        LicenseID = licenseId,
                        AudienceId = licenseAssignment.AudienceId,
                        StartDate = licenseAssignment.StartDate,
                        ExpiryDate = licenseAssignment.ExpiryDate,
                        AssignmentStatusId = licenseAssignment.AssignmentStatusId,
                        CustomerId = licenseAssignment.CustomerId
                    };
                    newLicenseAssignment.Save();
                }
                else
                {
                    var editExistingAssignment = qCheckExistingAssignment.First();
                    editExistingAssignment.StartDate = licenseAssignment.StartDate;
                    editExistingAssignment.ExpiryDate = licenseAssignment.ExpiryDate;
                    editExistingAssignment.AssignmentStatusId = licenseAssignment.AssignmentStatusId;

                    editExistingAssignment.Save();
                }
            }

        }

        public static SingleResult<bool> DeleteLicense(int licenseId)
        {
            Data.License license = new Data.License(licenseId);

            if (!true)
            {
                return new SingleResult<bool>(new Exception("You do not have permission to update this license."));
            }

            if (license.Id == 0 && licenseId > 0)
            {
                return new SingleResult<bool>(new Exception("The specified license couldn't be found."));
            }

            Data.License.Delete(licenseId);

            return new SingleResult<bool>(true);
        }

        public static SingleResult<Profession> SaveProfession(int professionId, Profession profession)
        {
            Data.Profession professionData = new Data.Profession(professionId);

            if (profession != null)
            {
                profession.CopyTo(professionData);
            }

            professionData.Id = professionId;

            professionData.Save();

            return LicenseController.GetProfession(professionData.Id);
        }

        public static SingleResult<Profession> GetProfession(int professionId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Profession>()
                .Where(Data.Profession.IdColumn).IsEqualTo(professionId);

            return new SingleResult<Profession>(query);
        }

        public static SingleResult<Jurisdiction> GetJurisdiction(int jurisdictionId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Jurisdiction>()
                .Where(Data.Jurisdiction.IdColumn).IsEqualTo(jurisdictionId);

            return new SingleResult<Jurisdiction>(query);
        }

        public static SingleResult<Jurisdiction> SaveJurisdiction(int jurisdictionId, Jurisdiction jurisdiction)
        {
            Data.Jurisdiction jurisdictionData = new Data.Jurisdiction(jurisdictionId);

            if (jurisdiction != null)
            {
                jurisdiction.CopyTo(jurisdictionData);
            }

            jurisdictionData.Id = jurisdictionId;

            jurisdictionData.Save();

            return LicenseController.GetJurisdiction(jurisdictionData.Id);
        }

        public static SingleResult<Jurisdiction> DeleteJurisdiction()
        {
            throw new NotImplementedException();
        }

        public static SingleResult<License> GetLicense(int LicenseId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.License>()
                .IncludeColumn(Data.Profession.NameColumn.QualifiedName, "Profession")
                .IncludeColumn(Data.Jurisdiction.NameColumn.QualifiedName, "Jurisdiction")
                .IncludeColumn(Data.Customer.NameColumn.QualifiedName, "CustomerName")
                .IncludeColumn(Data.AccreditationBoard.NameColumn.QualifiedName, "AccreditationBoard")
                .LeftOuterJoin(Data.Profession.IdColumn, Data.License.ProfessionIDColumn)
                .LeftOuterJoin(Data.Jurisdiction.IdColumn, Data.License.JurisdictionIDColumn)
                .LeftOuterJoin(Data.Customer.IdColumn, Data.License.CustomerIdColumn)
                .LeftOuterJoin(Data.AccreditationBoard.IdColumn, Data.License.AccreditationBoardIDColumn)
                .Where(Data.License.IdColumn).IsEqualTo(LicenseId)
                .And(Data.License.IsDeletedColumn).IsEqualTo(0);

            var result = new SingleResult<License>(query);

            var dataFields = Select.AllColumnsFrom<Data.UserDataField>()
                .Where(Data.UserDataField.Columns.Id)
                .In(
                    new Select(Data.UserDataFieldLicenseMap.Columns.UserDataFieldID)
                    .From<Data.UserDataFieldLicenseMap>()
                    .Where(Data.UserDataFieldLicenseMap.Columns.LicenseID).IsEqualTo(LicenseId)
                )
                .ExecuteTypedList<Data.UserDataField>();

            if (result.Data == null)
            {
                throw new Exception("Unable to find the license with ID: " + LicenseId);
            }

            
            result.Data.DataFields = dataFields.Select(s => s.DisplayName).ToList();
            if (result.Data.RequiredUserFields.Trim().Length > 0)
            {
                result.Data.UserFields = result.Data.RequiredUserFields.Split(',').Select(s => s.Trim()).ToList();
            }
            else
            {
                result.Data.UserFields = new List<string>();
            }

            if (string.IsNullOrEmpty(result.Data.CertificateFormFieldJson) &&
               (result.Data.DataFields.Count > 0 || result.Data.RequiredUserFields.Length > 0))
            {
                UserDataFieldForm certificateForm = new UserDataFieldForm();
                certificateForm.Items = Data.SPs.GetUserDataFieldsForLicense(result.Data.Id).ExecuteTypedList<UserDataField>();
                foreach (UserDataField field in certificateForm.Items)
                {
                    if (string.IsNullOrEmpty(field.DisplayName))
                    {
                        field.DisplayName = SubSonic.Inflector.ToTitleCase(field.CodeName);
                        field.FieldLabel = field.DisplayName;
                        field.UserFieldName = field.CodeName;
                        field.CodeName = field.CodeName.Substring(0, 1).ToLower()
                                       + field.CodeName.Substring(1);
                        
                    }

                }
                result.Data.CertificateFormFieldJson = JsonConvert.SerializeObject(certificateForm);
            }

            return result;
        }

        public static PagedResult<LicenseAssignment> GetLicenseUserAssignments(int LicenseId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = "ExpiryDate";

            SqlQuery query = Select.AllColumnsFrom<Data.UserLicenseAssignment>()
                .IncludeColumn(Data.UserLicenseAssignment.Columns.Id, "UserId")
                .Where(Data.UserLicenseAssignment.Columns.LicenseID).IsEqualTo(LicenseId);

            var res = new PagedResult<LicenseAssignment>(query, pageIndex, pageSize, orderBy, OrderDirection.Ascending).WithUtcFlag();
            return res; 
        }

        private static SqlQuery _GetBaseLicenseAssignmentsQuery()
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LicenseAssignment>()
                  .IncludeColumn(Data.LicenseStatus.Columns.StatusName, "AssignmentStatus")
                  .IncludeColumn("LicenseAssignments.ID", "LicenseAssignmentId")
                  .IncludeColumn(Data.License.ExpirationRuleIdColumn)
                  .IncludeColumn(Data.License.ExpirationRuleAfterDaysColumn)
                  .IncludeColumn(Data.License.RenewalLeadTimeInDaysColumn)
                  .InnerJoin(Data.LicenseStatus.IdColumn, Data.LicenseAssignment.AssignmentStatusIdColumn)
                  .InnerJoin(Data.License.IdColumn, Data.LicenseAssignment.LicenseIDColumn);

            return query;
        }


        public static PagedResult<LicenseAssignment> GetLicenseJobRoleAssignments(int LicenseId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = "ExpiryDate";
         
            SqlQuery query = _GetBaseLicenseAssignmentsQuery();

            query.IncludeColumn("JobRole.Name", "EntityName");
            query.InnerJoin(Data.JobRole.IdColumn, Data.LicenseAssignment.JobRoleIDColumn);

            query.Where(Data.LicenseAssignment.Columns.LicenseID).IsEqualTo(LicenseId)
                .And(Data.LicenseAssignment.IsDeletedColumn).IsNotEqualTo(1); ;

            return new PagedResult<LicenseAssignment>(query, pageIndex, pageSize, orderBy, OrderDirection.Ascending).WithUtcFlag();
        }

        public static PagedResult<LicenseAssignment> GetLicenseLocationAssignments(int LicenseId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = "ExpiryDate";

            SqlQuery query = _GetBaseLicenseAssignmentsQuery();

            query.IncludeColumn("Location.Name", "EntityName");
            query.InnerJoin(Data.Location.IdColumn, Data.LicenseAssignment.LocationIDColumn);

            query.Where(Data.LicenseAssignment.Columns.LicenseID).IsEqualTo(LicenseId)
                .And(Data.LicenseAssignment.IsDeletedColumn).IsNotEqualTo(1); ;

            return new PagedResult<LicenseAssignment>(query, pageIndex, pageSize, orderBy, OrderDirection.Ascending).WithUtcFlag();
        }

        public static PagedResult<LicenseAssignment> GetLicenseAudienceAssignments(int LicenseId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = "ExpiryDate";

            SqlQuery query = _GetBaseLicenseAssignmentsQuery();

            query.IncludeColumn("Audience.Name", "EntityName");
            query.InnerJoin(Data.Audience.IdColumn, Data.LicenseAssignment.AudienceIdColumn);

            query.Where(Data.LicenseAssignment.Columns.LicenseID).IsEqualTo(LicenseId)
                .And(Data.LicenseAssignment.IsDeletedColumn).IsNotEqualTo(1);

            return new PagedResult<LicenseAssignment>(query, pageIndex, pageSize, orderBy, OrderDirection.Ascending).WithUtcFlag();
        }

        public PagedResult<License> GetLicenses(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LicenseHierarchy>();

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query = query.Where(Data.LicenseHierarchy.Columns.Name).ContainsString(searchText);
            }

            return new PagedResult<License>(query, pageIndex, pageSize, Data.LicenseHierarchy.Columns.LevelIndentText, OrderDirection.Ascending);
        }

        public MultipleResult<TrainingProgramLicense> GetLicensesForTrainingProgram(int trainingProgramId, string searchText)
        {
            var usedTrainingProgram = new Data.TrainingProgram("ID", trainingProgramId);
            List<Data.TrainingProgram> tps = new List<Data.TrainingProgram>() {usedTrainingProgram};

            return GetLicensesForTrainingProgram(tps, searchText);
        }

        public MultipleResult<TrainingProgramLicense> GetLicensesForTrainingProgram(List<Data.TrainingProgram> trainingPrograms, string searchText)
        {
            List<int> trainingProgramIds = trainingPrograms.Select(t => t.ParentTrainingProgramID.HasValue && t.ParentTrainingProgramID > 0 ?
                                                                        t.ParentTrainingProgramID.Value :
                                                                        t.Id)
                                                                        .ToList();

            List<string> columns = Data.License.Schema.Columns.Select(c => c.QualifiedName).ToList();
            columns.AddRange(Data.TrainingProgramLicense.Schema.Columns.Where(c => c.ColumnName != "ID").Select(c => c.QualifiedName).ToList());

            SqlQuery query = new Select(columns.ToArray()).From<Data.TrainingProgramLicense>()
                .InnerJoin(Data.License.IdColumn, Data.TrainingProgramLicense.LicenseIDColumn)
                .Where(Data.TrainingProgramLicense.TrainingProgramIDColumn)
                .In(trainingProgramIds)
                .OrderDesc(Data.TrainingProgramLicense.IsPrimaryColumn.ColumnName)
                .OrderAsc(Data.TrainingProgramLicense.IdColumn.ColumnName);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.And(Data.License.NameColumn).ContainsString(searchText);
            }

            var result = new MultipleResult<TrainingProgramLicense>(query);
            foreach (var license in result.Data)
            {
                var dataFields = Select.AllColumnsFrom<Data.LicenseDataField>()
                    .Where(Data.LicenseDataField.Columns.Id)
                    .In(
                        new Select(Data.LicenseDataFieldsToLicensesMap.Columns.LicenseDataFieldID)
                        .From<Data.LicenseDataFieldsToLicensesMap>()
                        .Where(Data.LicenseDataFieldsToLicensesMap.Columns.LicenseID).IsEqualTo(license.Id)
                    )
                    .ExecuteTypedList<Data.LicenseDataField>();


                license.DataFields = dataFields.Select(s => s.Name).ToList();

                if (!string.IsNullOrWhiteSpace(license.RequiredUserFields))
                {
                    license.UserFields = license.RequiredUserFields.Split(',').ToList();
                }
                else
                {
                    license.UserFields = new List<string>();
                }
            }

            return result;
        }

        public MultipleResult<TrainingProgramLicense> SaveLicensesForTrainingProgram(int trainingProgramId, List<TrainingProgramLicense> licenses)
        {
            new Delete().From<Data.TrainingProgramLicense>()
                .Where(Data.TrainingProgramLicense.TrainingProgramIDColumn)
                .IsEqualTo(trainingProgramId)
                .Execute();

            Data.TrainingProgramLicenseCollection newLicenses = new Data.TrainingProgramLicenseCollection();

            var hasPrimary = false;

            foreach (TrainingProgramLicense l in licenses)
            {
                Data.TrainingProgramLicense newLicense = new Data.TrainingProgramLicense();
                newLicense.ApprovalCode = l.ApprovalCode;
                newLicense.TrainingProgramID = trainingProgramId;
                newLicense.LicenseID = l.Id;
                newLicense.IsPrimary = l.IsPrimary;

                if (l.IsPrimary)
                {
                    hasPrimary = true;
                }

                newLicenses.Add(newLicense);
            }

            // Make the first license default to primary
            if (!hasPrimary)
            {
                if (licenses.Count > 0)
                {
                    newLicenses[0].IsPrimary = true;
                }
            }

            newLicenses.BatchSave();

            return GetLicensesForTrainingProgram(trainingProgramId, "");
        }

        public PagedResult<Profession> GetProfessions(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Profession>()
               .Where(Data.Profession.Columns.Name).ContainsString(searchText);

            return new PagedResult<Profession>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Jurisdiction> GetJurisdictions(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Jurisdiction>()
               .Where(Data.Jurisdiction.Columns.Name).ContainsString(searchText);

            return new PagedResult<Jurisdiction>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public MultipleResult<LicenseDataField> GetLicenseDataFields(string searchText, string sort, OrderDirection orderDirection, int p, int limit)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LicenseDataField>();
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.Where(Data.LicenseDataField.Columns.Name).ContainsString(searchText);
            }

            return new MultipleResult<LicenseDataField>(query);
        }

        public static MultipleResult<SimpleLicenseList> GetSimpleLicenseList()
        {
            SqlQuery query = new Select("ID", "Name", "Description").From("License");
            var res = new MultipleResult<SimpleLicenseList>(query);

            return res;
        }

        public static SingleResult<LicenseAssignmentNote> SaveLicenseNote(LicenseAssignmentNote note)
        {
            var newNote = new Data.LicenseAssignmentNote();

            newNote.Body = note.Body;
            newNote.LicenseAssignmentID = note.LicenseAssignmentId;

            newNote.Save(SymphonyController.GetUserID());

            return new SingleResult<LicenseAssignmentNote>(note);
        }

        public static MultipleResult<LicenseAssignmentNote> GetLicenseNotes(int licenseAssignmentId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LicenseAssignmentNote>()
                .Where(Data.LicenseAssignmentNote.LicenseAssignmentIDColumn)
                .IsEqualTo(licenseAssignmentId);

            var results = new MultipleResult<LicenseAssignmentNote>(query);
            return results;
        }

        public static SingleResult<LicenseAssignment> UserSelfSave(int licenseId, LicenseAssignment la)
        {
            return _SaveUserLicenseAssignment(licenseId, la, 1);
        }

        private static SingleResult<LicenseAssignment> _SaveUserLicenseAssignment(int licenseId, LicenseAssignment la, int isSelfAdd=0)
        {
            try
            {
                var qCheckExistingAssignment = new Select().From<Data.LicenseAssignment>()
                    .Where(Data.LicenseAssignment.Columns.UserID).IsEqualTo(la.UserId)
                    .And(Data.LicenseAssignment.Columns.LicenseID).IsEqualTo(licenseId)
                    .And(Data.LicenseAssignment.Columns.IsUserAdded).IsEqualTo(isSelfAdd)
                    .ExecuteTypedList<Data.LicenseAssignment>();
                    
                bool selfAdd = isSelfAdd > 0 ? true : false;

                if (qCheckExistingAssignment.Count == 0)
                {
                    var l2 = new Data.LicenseAssignment()
                    {
                        LicenseID = licenseId,
                        UserID = la.UserId,
                        StartDate = la.StartDate,
                        ExpiryDate = la.ExpiryDate,
                        CustomerId = la.CustomerId,
                        AssignmentStatusId = la.AssignmentStatusId,
                        IsUserAdded = selfAdd
                    };
                    l2.Save();

                    la.CopyFrom(l2);
                }
                else
                {
                    var editUserAssignment = qCheckExistingAssignment.First();
                    editUserAssignment.AssignmentStatusId = la.AssignmentStatusId;
                    editUserAssignment.StartDate = la.StartDate;
                    editUserAssignment.ExpiryDate = la.ExpiryDate;

                    editUserAssignment.Save();

                    la.CopyFrom(editUserAssignment);
                }

                // Because some queries use the ID field for other values, 
                // explicitly set the LicenseAssignmnetId field
                la.LicenseAssignmentId = la.Id;
                return new SingleResult<LicenseAssignment>(la);
            }
            catch (Exception e)
            {
                return new SingleResult<LicenseAssignment>(e);
            }
        }

        public static SingleResult<Boolean> DeleteLicenseAssignmentById(int licenseAssignmentId)
        {
            try
            {
                // NOTE: There seems to be a bug in Subsonic's delete: 
                // it always returns 0 rows affected. Ergo, this always returns false :(
                var wasDeleted = Data.LicenseAssignment.Delete(licenseAssignmentId) > 0;
                return new SingleResult<Boolean>(wasDeleted);
            }
            catch (Exception e)
            {
                return new SingleResult<Boolean>(e);
            }
        }

        // Base Query
        private static SqlQuery _GetBaseQuery()
        {
            SqlQuery query = new Select()
                    .IncludeColumn("License.ID", "LicenseID")
                    .IncludeColumn("License.Name", "LicenseName")
                    .IncludeColumn("LicenseAssignments.LicenseNumber", "LicenseNumber")
                    .IncludeColumn(Data.License.DescriptionColumn)
                    .IncludeColumn(Data.License.ExpirationRuleIdColumn)
                    .IncludeColumn(Data.License.ExpirationRuleAfterDaysColumn)
                    .IncludeColumn(Data.License.RenewalLeadTimeInDaysColumn)
                    .IncludeColumn(Data.LicenseAssignment.ExpiryDateColumn)
                    .IncludeColumn(Data.LicenseAssignment.StartDateColumn)
                    .IncludeColumn(Data.LicenseAssignment.RenewalSubmittedDateColumn)
                    .IncludeColumn("LicenseAssignments.AssignmentStatusId", "StatusId")
                    .IncludeColumn("LicenseStatus.StatusName", "Status")
                .From<Data.License>()
                .InnerJoin(Data.LicenseAssignment.LicenseIDColumn, Data.License.IdColumn)
                .InnerJoin(Data.LicenseStatus.IdColumn, Data.LicenseAssignment.AssignmentStatusIdColumn);

            return query;
        }

        // License Report for User, Location, JobRole, and Audience
        // Returns reports of licenses directly assigned to thos entities
        public static MultipleResult<EntityLicenseReport> GetEntityLicenseReport(string entityType, int entityId)
        {
            SqlQuery query = _GetBaseQuery();

            switch (entityType)
            {
                case "user":
                    _GetUserLicenseReport(ref query, entityId);
                    break;
                
                case "location":
                    _GetLocationLicenseReport(ref query, entityId);
                    break;
                
                case "jobrole":
                    _GetJobRoleLicenseReport(ref query, entityId);
                    break;

                case "audience":
                    _GetAudienceLicenseReport(ref query, entityId);
                    break;
                default:
                    throw (new Exception("Invalid Entity Type: " + entityType));
            }

            var results = new MultipleResult<EntityLicenseReport>(query);

            return results;
        }

        private static void _GetUserLicenseReport(ref SqlQuery query, int entityId)
        {
            query.IncludeColumn("[User].FirstName + ' ' + [User].LastName", "EntityName");
            query.IncludeColumn("LicenseAssignments.UserID", "EntityID");
            query.IncludeColumn("'User'", "EntityType");
            query.InnerJoin(Data.User.IdColumn, Data.LicenseAssignment.UserIDColumn);
            if (entityId != 0)
            {
                query.Where(Data.LicenseAssignment.UserIDColumn).IsEqualTo(entityId);
            }
            else
            {
                query.Where(Data.LicenseAssignment.UserIDColumn).IsNotNull();
            }
        }

        private static void _GetLocationLicenseReport(ref SqlQuery query, int entityId)
        {
            query.IncludeColumn("Location.Name", "EntityName");
            query.IncludeColumn("LicenseAssignments.LocationID", "EntityID");
            query.IncludeColumn("'Location'", "EntityType");
            query.InnerJoin(Data.Location.IdColumn, Data.LicenseAssignment.LocationIDColumn);
            if (entityId != 0)
            {
                query.Where(Data.LicenseAssignment.LocationIDColumn).IsEqualTo(entityId);
            } 
            else
            {
                query.Where(Data.LicenseAssignment.LocationIDColumn).IsNotNull();
            }
        }

        private static void _GetJobRoleLicenseReport(ref SqlQuery query, int entityId)
        {
            query.IncludeColumn("JobRole.Name", "EntityName");
            query.IncludeColumn("LicenseAssignments.JobRoleID", "EntityID");
            query.IncludeColumn("'JobRole'", "EntityType");
            //query.InnerJoin("JobRole", "ID", "LicenseAssignments", "JobRoleID");
            if (entityId != 0)
            {
                query.Where(Data.LicenseAssignment.JobRoleIDColumn).IsEqualTo(entityId);
            }
            else
            {
                query.Where(Data.LicenseAssignment.JobRoleIDColumn).IsNotNull();
            }
        }

        private static void _GetAudienceLicenseReport(ref SqlQuery query, int entityId)
        {
            query.IncludeColumn("Audience.Name", "EntityName");
            query.IncludeColumn("LicenseAssignments.AudienceID", "EntityID");
            query.IncludeColumn("'Audience'", "EntityType");
            query.InnerJoin(Data.Audience.IdColumn, Data.LicenseAssignment.AudienceIdColumn);
            if (entityId != 0)
            {
                query.Where(Data.LicenseAssignment.AudienceIdColumn).IsEqualTo(entityId);
            }
            else
            {
                query.Where(Data.LicenseAssignment.AudienceIdColumn).IsNotNull();
            }
        }

        // User reports by Entity type
        // Returns list of user to whom a license for a given entity will apply/cover. 
        public static MultipleResult<EntityLicenseReport> GetUserLicenseReportForEntity(string entityType, int entityId)
        {
            SqlQuery query = _GetBaseQuery();
            query.IncludeColumn("[User].FirstName + ' ' + [User].LastName", "EntityName");
            query.IncludeColumn("'User'", "EntityType");
            query.IncludeColumn("'" + entityType + "'", "BelongsToType");
            query.IncludeColumn("'" + entityId.ToString() + "'", "BelongsToId");

            switch(entityType)
            {
                case "location":
                    _GetUserLicenseReportForLocation(ref query, entityId);
                    break;

                case "audience":
                    _GetUserLicenseReportForAudience(ref query, entityId);
                    break;
                case "jobrole":
                    _GetUserLicenseReportForJobRole(ref query, entityId);
                    break;

                default:
                    throw (new Exception("Invalid Entity Type: " + entityType));
            }

            var results = new MultipleResult<EntityLicenseReport>(query);
            return results;
        }

        private static void _GetUserLicenseReportForLocation(ref SqlQuery query, int locationId)
        {
            SqlQuery subquery = new Select("ID")
                .From("User")
                .Where("LocationID").IsEqualTo(locationId);

            query.InnerJoin(Data.User.LocationIDColumn, Data.LicenseAssignment.LocationIDColumn);
            query.Where(Data.User.IdColumn).In(subquery);
        }

        private static void _GetUserLicenseReportForJobRole(ref SqlQuery query, int jobroleId)
        {
            SqlQuery subquery = new Select("ID")
                .From("User")
                .Where("JobRoleID").IsEqualTo(jobroleId);
            query.InnerJoin(Data.User.JobRoleIDColumn, Data.LicenseAssignment.JobRoleIDColumn);

            query.Where(Data.User.IdColumn).In(subquery);
        }
        
        private static void _GetUserLicenseReportForAudience(ref SqlQuery query, int audienceId)
        {
            SqlQuery subquery = new Select("UserID")
                .From("UserAudience")
                .Where("AudienceID").IsEqualTo(audienceId);

            query.InnerJoin(Data.UserAudience.AudienceIDColumn, Data.LicenseAssignment.AudienceIdColumn);
            query.InnerJoin(Data.User.IdColumn, Data.UserAudience.UserIDColumn);

            query.Where(Data.User.IdColumn).In(subquery);
        }

        // Expiration Reports
        // This can be extended to allow for filterable dates to be passed in, i.e., [30,60, 90, 120, ...]
        private static SqlQuery _GetSummaryBaseQuery(string entityType)
        {
            SqlQuery query = _GetBaseQuery();
            switch (entityType)
            {
                case "user":
                    query.Where(Data.LicenseAssignment.UserIDColumn).IsNotNull();
                    break;
                case "location":
                    query.Where(Data.LicenseAssignment.LocationIDColumn).IsNotNull();
                    break;
                case "jobrole":
                    query.Where(Data.LicenseAssignment.JobRoleIDColumn).IsNotNull();
                    break;
                case "audience":
                    query.Where(Data.LicenseAssignment.AudienceIdColumn).IsNotNull();
                    break;
                case "all":
                    break;
                default:
                    throw (new Exception("Invalid Entity Type: " + entityType));
            }

            return query;
        }

        public static SingleResult<LicenseSummaryReport> GetExpirationReportForEntity(string entityType)
        {
            SqlQuery query = _GetSummaryBaseQuery(entityType);
            var results = new MultipleResult<EntityLicenseReport>(query);

            LicenseSummaryReport lsr = new LicenseSummaryReport();
            if (lsr.Data == null)
            {
                lsr.Data = new Dictionary<string, int>();
            }

            lsr.Data.Add("30", 0);
            lsr.Data.Add("60", 0);
            lsr.Data.Add("90", 0);
            lsr.Data.Add("other", 0);

            foreach(EntityLicenseReport res in results.Data){
                var exp = res.DaysUntilExpiry;
                if (exp <= 30)
                {
                    lsr.Data["30"]++;
                }
                else if (exp <= 60)
                {
                    lsr.Data["60"]++;
                }
                else if (exp <= 90)
                {
                    lsr.Data["90"]++;
                }
                else
                {
                    lsr.Data["other"]++;
                }
            }
            return new SingleResult<LicenseSummaryReport>(lsr);
        }

        public static SingleResult<LicenseSummaryReport> GetLicenseStatusSummaryReport(string entityType)
        {
            SqlQuery query = _GetSummaryBaseQuery(entityType);
            var results = new MultipleResult<EntityLicenseReport>(query);

            LicenseSummaryReport lsr = new LicenseSummaryReport();
            if (lsr.Data == null)
            {
                lsr.Data = new Dictionary<string, int>();
            }

            foreach(EntityLicenseReport res in results.Data){
                var status = res.Status;
                if (!lsr.Data.ContainsKey(status))
                {
                    lsr.Data.Add(status, 0);
                }

                lsr.Data[status]++;
            }
            return new SingleResult<LicenseSummaryReport>(lsr);
        }


        public LicenseAssignmentDocumentInfo SaveAssignmentDocument(int licenseAssignmentId, HttpPostedFile uploadedFile)
        {
            var laFileInfo = new Data.LicenseAssignmentDocument();

            // Save info to db
            laFileInfo.FileName = uploadedFile.FileName;
            laFileInfo.FileType = Path.GetExtension(uploadedFile.FileName);
            laFileInfo.LicenseAssignmentID = licenseAssignmentId;

            laFileInfo.Save(UserID);

            // save file
            // - create a directory for each license assignment as there may be multiple files saved for a given assignment
            string path = HttpContext.Current.Server.MapPath ( LicenseAssignmentDocumentsLocation + "\\" + laFileInfo.LicenseAssignmentID + "\\" + laFileInfo.FileName);
            string directory = Path.GetDirectoryName(path);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            uploadedFile.SaveAs(path);

            LicenseAssignmentDocumentInfo laDoc = new LicenseAssignmentDocumentInfo();
            laDoc.CopyFrom(laFileInfo);
   
            return laDoc;
        }

        public static MultipleResult<LicenseAssignmentDocumentInfo> GetLicenseAssignmentDocuments(int licenseAssignmentId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LicenseAssignmentDocument>()
                .Where(Data.LicenseAssignmentDocument.LicenseAssignmentIDColumn).IsEqualTo(licenseAssignmentId);


            return new MultipleResult<LicenseAssignmentDocumentInfo>(query);
        }


        public static MultipleResult<LicenseAssignment> GetUserLicenseReport(int userId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.UserLicenseAssignment>()
                .IncludeColumn(Data.UserLicenseAssignment.Columns.Id, "UserId")
                .Where(Data.UserLicenseAssignment.Columns.Id).IsEqualTo(userId);

            var res = new MultipleResult<LicenseAssignment>(query);
            return res;
        }

    }
}
