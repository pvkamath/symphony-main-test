﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using SubSonic;
using Symphony.Web;
using System.Web.Security;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using SubSonic.Sugar;
using System.Data;
using log4net;
using System.Web;
using System.Net;
using System.ServiceModel.Web;
using Symphony.Core.Extensions;
using Symphony.Core.Comparers;
using System.Transactions;
using Data = Symphony.Core.Data;
using Newtonsoft.Json.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using Symphony.Core.Data;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Controllers
{
    public class AssociatedImageController : SymphonyController
    {
        private ILog Log = LogManager.GetLogger(typeof(AssociatedImageController));


        public object SaveAssociatedImage(int customerId, string association, int associatedId, Stream data, int width = 100, int height = 100)
        {
            //TODO: make sure all these types implement the IAssociatedData interface!!
            IActiveRecord record;
            switch (association.ToLowerInvariant())
            {
                case "customer":
                    record = new Data.Customer(associatedId);
                    break;
                case "artisancourse":
                    record = new Data.ArtisanCourse(associatedId);
                    break;
                case "onlinecourse":
                    record = new Data.OnlineCourse(associatedId);
                    break;
                case "trainingprogram":
                    record = new Data.TrainingProgram(associatedId);
                    break;
                case "course":
                    record = new Data.Course(associatedId);
                    break;
                case "class":
                    record = new Data.ClassX(associatedId);
                    break;
                case "room":
                    record = new Data.Room(associatedId);
                    break;
                case "venue":
                    record = new Data.Venue(associatedId);
                    break;
                case "library":
                    record = new Data.Library(associatedId);
                    break;
                case "user":
                    record = new Data.User(associatedId);
                    break;
                default:
                    throw new Exception("Invalid association specified - " + association);
            }

            if ((int)record.GetColumnValue("ID") == 0)
            {
                throw new Exception("The record must be saved before an associated image can be added.");
            }

            if ((int)record.GetColumnValue("CustomerID") != customerId)
            {
                throw new Exception("You don't have permission to access this record.");
            }

            if (!(record is IAssociatedImage))
            {
                throw new Exception("The record type " + record.GetType() + " does not implement IAssociatedImage.");
            }

            using (var img = Image.FromStream(data))
            {
                // scale down the image
                using (var scaled = new Bitmap(width, height))
                {
                    using (var graphics = Graphics.FromImage(scaled))
                    {
                        graphics.DrawImage(img, new Rectangle(0, 0, scaled.Width, scaled.Height));
                    }

                    // base64 encode the bytes
                    string encoded = string.Empty;
                    using (var ms = new MemoryStream())
                    {
                        scaled.Save(ms, ImageFormat.Png);

                        encoded = Convert.ToBase64String(ms.ToArray());
                    }

                    var associated = (IAssociatedImage)record;
                    associated.AssociatedImageData = "data:image/png;base64," + encoded;
                    record.Save();
                }

            }

            return 0;
        }
    }
}
