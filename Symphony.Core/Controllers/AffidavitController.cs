﻿using System;
using Symphony.Core.Models;
using SubSonic;
using log4net;
using System.Diagnostics;
using AffidavitFinalExamData = Symphony.Core.Data.AffidavitFinalExam;
using AffidavitFinalExamResponseData = Symphony.Core.Data.AffidavitFinalExamResponse;
using System.Web;

namespace Symphony.Core.Controllers
{
    /// <summary>
    /// AffidavitController is a domain service that performs various CRUD operations on entities
    /// relating to the affidavit module. It also handles registrations of affidavit responses.
    /// </summary>
    public class AffidavitController : SymphonyController
    {
        static ILog Log = LogManager.GetLogger(typeof(AffidavitController));

        /// <summary>
        /// Finds the Affidavit with an id equal to the id parameter.
        /// </summary>
        /// <param name="id">Id of the Affidavit to return.</param>
        /// <returns>Affidavit that matches the provided id, wrapped in a SingleResult object. If no
        /// matching entity is found, then null will be returned, also wrapped in a SingleResult
        /// object.</returns>
        public SingleResult<AffidavitFinalExam> FindAffidavit(int id)
        {
            SqlQuery query = null;

            try
            {
                var cached = CacheController.Get<AffidavitFinalExam>(id);
                if (cached != null)
                {
                    return new SingleResult<AffidavitFinalExam>(cached);
                }

                Log.InfoFormat("Performing operation to find an affidavit with id {0}.", id);
                query = Select.AllColumnsFrom<AffidavitFinalExamData>()
                    .Where(AffidavitFinalExamData.Columns.Id).IsEqualTo(id)
                    .AndExpression(AffidavitFinalExamData.Columns.IsDeleted).IsEqualTo(false);

                var affidavit = query.ExecuteSingle<AffidavitFinalExam>();
                if (affidavit == null)
                {
                    Log.DebugFormat("Operation to find an affidavit returned null.");
                }
                else
                {
                    CacheController.Add<AffidavitFinalExam>(id, affidavit);
                }

                return new SingleResult<AffidavitFinalExam>(affidavit);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to find an affidavit: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to find an affidavit.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }
        
        /// <summary>
        /// Queries the database for the set of Affidavits that match the specified query
        /// parameters.
        /// </summary>
        /// <param name="queryParams">Query parameter object defining what to query.</param>
        /// <returns>The set of Affidavits that match the query parameters, wrapped in a PagedResult
        /// object. If no matching entities are found, then an empty array will be returned, also
        /// wrapped in a PagedResult object.</returns>
        public PagedResult<AffidavitFinalExam> QueryAffidavits(PagedQueryParams<AffidavitFinalExam> queryParams)
        {
            SqlQuery query = null;

            try
            {
                Log.Info("Performing operation to query affidavits.");

                query = Select.AllColumnsFrom<AffidavitFinalExamData>()
                    .Where(AffidavitFinalExamData.Columns.IsDeleted).IsEqualTo(false);

                if (!string.IsNullOrWhiteSpace(queryParams.SearchText))
                {
                    query.AndExpression(AffidavitFinalExamData.Columns.Name).ContainsString(queryParams.SearchText)
                        .OrExpression(AffidavitFinalExamData.Columns.Description).ContainsString(queryParams.SearchText);
                }

                Log.DebugFormat("queryParams: {0}", queryParams.ToString());

                return new PagedResult<AffidavitFinalExam>(query, queryParams.PageIndex, queryParams.PageSize, queryParams.OrderBy, queryParams.OrderDirection);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query affidavits: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query affidavits.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Creates a new Affidavit with the provided model data.
        /// </summary>
        /// <param name="model">Model data to use when creating the Affidavit. The model shoudl not
        /// have a positive integer value for its id parameter.</param>
        /// <returns>The newly created Affidavit model. The data will be identical to what was
        /// posted, except that it will have a new database id.</returns>
        public SingleResult<AffidavitFinalExam> CreateAffidavit(AffidavitFinalExam model)
        {
            try
            {
                Log.Info("Performing operation to create an affidavit.");

                if (model.ID > 0)
                {
                    throw new ArgumentException("Cannot create affidavit when model data already has an associated id.");
                }

                var data = new AffidavitFinalExamData();
                model.CopyTo(data);

                data.Save();

                Log.DebugFormat("New affidavit created with id {0}.", data.Id);
                model.ID = data.Id;

                return new SingleResult<AffidavitFinalExam>(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to create an affidavit.", ex);
#if DEBUG
                Debugger.Break();
#endif
                return new SingleResult<AffidavitFinalExam>(model);
            }
        }

        /// <summary>
        /// Updates an existing Affidavit with the provided model data.
        /// </summary>
        /// <param name="model">Model data to update the Affidavit with. This data should already
        /// have an id associated with it.</param>
        /// <returns>The newly created Affidavit model. The data will be identical to what was
        /// posted.</returns>
        public SingleResult<AffidavitFinalExam> UpdateAffidavit(AffidavitFinalExam model)
        {
            try
            {
                Log.InfoFormat("Performing operation to update an affidavit with id {0}", model.ID);

                if (model.ID == 0)
                {
                    throw new ArgumentException("Cannot update an affidavit when model data does not contain an id.");
                }

                var data = new AffidavitFinalExamData(model.ID);
                model.CopyTo(data);

                data.Save();
                Log.InfoFormat("Updated affidavit with id {0}.", data.Id);

                return new SingleResult<AffidavitFinalExam>(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to update an affidavit.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Deletes an existing Affidavit. Note that the delete is a soft delete operation. Users
        /// who are already using an Affidavit can continue to do so, but the Affidavit will no
        /// longer show up when queried.
        /// </summary>
        /// <param name="model">Model data representing the Affidavit to delete.</param>
        /// <returns>The deleted Affidavit model.</returns>
        public SingleResult<AffidavitFinalExam> DeleteAffidavit(AffidavitFinalExam model)
        {
            try
            {
                Log.InfoFormat("Performing operation to soft delete an affidavit with id {0}", model.ID);

                if (model.ID == 0)
                {
                    throw new ArgumentException("Cannot delete an affidavit when model data does not contain an id.");
                }

                var data = new AffidavitFinalExamData(model.ID);
                model.IsDeleted = true;
                data.IsDeleted = true;

                data.Save();
                Log.InfoFormat("Soft deleted affidavit with id {0}.", data.Id);

                return new SingleResult<AffidavitFinalExam>(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to delete an affidavit.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Saves an response to an Affidavit.
        /// </summary>
        /// <param name="response">The response to save.</param>
        /// <returns>A result indicating success or failure.</returns>
        public SimpleSingleResult<bool> SaveAffidavitResponse(AffidavitFinalExamResponse response)
        {
            try
            {
                Log.Info("Performing operation to save an affidavit response.");

                var data = new AffidavitFinalExamResponseData();
                response.CopyTo(data);

                data.Save(UserID);

                return new SimpleSingleResult<bool>(true);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to delete an affidavit.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }
    }
}
