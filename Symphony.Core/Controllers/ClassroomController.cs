﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using SubSonic;
using System.Web;
using Symphony.RusticiIntegration.Core;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Net;
using HtmlAgilityPack;
using System.Security.Cryptography.X509Certificates;
using Symphony.Core.Comparers;
using System.Transactions;
using Symphony.Core.Extensions;
using Newtonsoft.Json;
using log4net;

namespace Symphony.Core.Controllers
{
    public class ClassroomController : Controllers.SymphonyController
    {
        ILog Log = LogManager.GetLogger(typeof(ClassroomController));

        public Data.GTMOrganizer GetOganizer(Data.ClassX cls)
        {
            string organizerKey = string.Empty;
            foreach (var instructor in cls.ClassInstructors)
            {
                Data.GTMOrganizer organizer = new Data.GTMOrganizer(Data.GTMOrganizer.Columns.UserID, instructor.InstructorID);
                if (organizer.Id != 0)
                {
                    if (!string.IsNullOrEmpty(organizer.OrganizerKey.ToString()))
                    {
                        return organizer;
                    }
                }
            }

            throw new Exception("No GoToWebinar organizer could be found for this class.");
        }

        /// <summary>
        /// Creates all the registrations for all registered users in the specified class that don't already have a valid webinar registration id
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="classId"></param>
        /// <returns></returns>
        public MultipleResult<WebinarRegistrationAttempt> CreateRegistrations(int customerId, int userId, int classId)
        {
            var cls = new Data.ClassX(classId);
            string webinarKey = cls.WebinarKey;
            var organizer = GetOganizer(cls);

            List<Data.Registration> registrations = Select.AllColumnsFrom<Data.Registration>()
                .Where(Data.Registration.ClassIDColumn).IsEqualTo(classId)
                .And(Data.Registration.RegistrationStatusIDColumn).IsEqualTo(RegistrationStatusType.Registered)
                .ExecuteTypedList<Data.Registration>();

            List<WebinarRegistrationAttempt> attempts = new List<WebinarRegistrationAttempt>();
            foreach (Data.Registration reg in registrations)
            {
                if (!string.IsNullOrEmpty(reg.WebinarRegistrationID)) { continue; }

                Data.User u = new Data.User(reg.RegistrantID);

                WebinarRegistrationAttempt attempt = CreateWebinarRegistration(u, organizer, webinarKey);
                if (attempt.Success)
                {
                    reg.WebinarRegistrationID = attempt.RegistrantID;
                    reg.Save();
                }
                attempts.Add(attempt);
            }

            return new MultipleResult<WebinarRegistrationAttempt>(attempts);
        }

        /// <summary>
        /// Creates a single registration for the specified user/webinar key
        /// </summary>
        /// <param name="u">The user to register</param>
        /// <param name="webinarKey">The webinar to register them for</param>
        /// <returns></returns>
        public WebinarRegistrationAttempt CreateWebinarRegistration(Data.User u, Data.GTMOrganizer organizer, string webinarKey)
        {
            WebinarRegistrationAttempt attempt;
            if (string.IsNullOrEmpty(u.Email))
            {
                attempt = new WebinarRegistrationAttempt()
                {
                    Email = string.Empty,
                    Error = "Email addresses are required to register a user.",
                    Success = false
                };
            }
            else
            {
                attempt = SendToGoToWebinar(organizer, webinarKey, u);
            }
            return attempt;
        }

        private class GoToWebinarTokenResponse
        {
            public string access_token { get; set; }
        }

        private string GetGoToWebinarAccessToken(Data.GTMOrganizer organizer)
        {
            object token = HttpContext.Current.Items[(object)("gotowebinartoken-" + organizer.Id.ToString())];

            if (token == null || string.IsNullOrEmpty(token.ToString()))
            {
                string loginUrl = "https://api.citrixonline.com/oauth/access_token?grant_type=password" +
                        "&user_id=" + organizer.Email +
                        "&password=" + organizer.Password +
                        "&client_id=" + ConfigurationManager.AppSettings["goToWebinarApplicationKey"];

                try
                {

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(loginUrl);
                    request.AllowAutoRedirect = true;
                    request.MaximumAutomaticRedirections = 3;

                    // allow all ssl certs
                    ServicePointManager.ServerCertificateValidationCallback = (unused, unused2, unused3, unused4) =>
                    {
                        return true;
                    };

                    ASCIIEncoding ascii = new ASCIIEncoding();

                    ServicePointManager.Expect100Continue = false;
                    request.KeepAlive = false;
                    request.UserAgent = "Symphony";

                    request.Method = "GET";
                    request.Accept = "application/json";
                    request.ContentType = "application/json";

                    CookieContainer cookies = new CookieContainer();
                    request.CookieContainer = cookies;

                    string results = string.Empty;
                    //using (Stream requestStrem = request.GetRequestStream())
                    //{
                        HttpWebResponse httpResponse = (HttpWebResponse)request.GetResponse();
                        using (Stream responseStream = httpResponse.GetResponseStream())
                        {
                            using (StreamReader responseStreamReader = new StreamReader(responseStream))
                            {
                                results = responseStreamReader.ReadToEnd();
                            }
                        }
                    //}
                    var response = JsonConvert.DeserializeObject<GoToWebinarTokenResponse>(results);

                    /*
                     * response:
    {
    "access_token":"3645136ef45675a113adb18027dd7df8",
    "expires_in":"30758399",
    "refresh_token":"1dd48bf4aa453162521250d772a03ae6",
    "organizer_key":"300000000000384444",
    "account_key":"300000000000329487",
    "account_type":"corporate",
    "firstName":"Test",
    "lastName":"Test",
    "email":"test@test.com"
    }
                     * */
                    
                    return response.access_token;
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }

            return token == null ? string.Empty : token.ToString();
        }

        /// <summary>
        /// Posts the user info to the GoToWebinar specified
        /// </summary>
        /// <param name="webinarKey"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private WebinarRegistrationAttempt SendToGoToWebinar(Data.GTMOrganizer organizer, string webinarKey, Data.User user)
        {
            try
            {
                // https://developer.citrixonline.com/api/gotowebinar-rest-api
                string street = "Unknown";
                string city = "Unknown";
                string state = "Unknown";
                string zip = "00000";
                string country = "USA";
                if (user.Location != null)
                {
                    street = (user.Location.Address1 + " " + user.Location.Address2).Trim();
                    if (string.IsNullOrEmpty(street))
                    {
                        street = "Unknown";
                    }
                    city = string.IsNullOrEmpty(user.Location.City) ? city : user.Location.City;
                    state = string.IsNullOrEmpty(user.Location.State) ? state : user.Location.State;
                    zip = string.IsNullOrEmpty(user.Location.PostalCode) ? zip : user.Location.PostalCode;
                }


                string writeContent = JsonConvert.SerializeObject(new
                {
                    firstName = user.FirstName,
                    lastName = user.LastName,
                    email = user.Email,
                    address = street,
                    city = city,
                    state = state,
                    zipCode = zip,
                    country = country
                });

                string url = ConfigurationManager.AppSettings["goToWebinarRegisterLink"];
                url = url.Replace("{organizerKey}", organizer.OrganizerKey.ToString()).Replace("{webinarKey}", webinarKey);
                string readContent = PostToGoToWebinar(organizer, url, writeContent);

                // https://developer.citrixonline.com/api/gotowebinar-rest-api/apimethod/create-registrant
                // example response: 
                // HTTP/1.1 201 OK Content-Type: application/json { "registrantKey":3531655999964222475,"joinUrl":"https://global.gotowebinar.com/join/2599994867611407361/89999121" }

                WebinarRegistrationAttempt attempt = new WebinarRegistrationAttempt();
                attempt = JsonConvert.DeserializeObject<WebinarRegistrationAttempt>(readContent);
                attempt.Email = user.Email;
                attempt.WebinarKey = webinarKey;
                attempt.RegistrantID = attempt.RegistrantKey; // map from new api to old value
                attempt.Success = true;

                return attempt;
            }
            catch (Exception ex)
            {
                return new WebinarRegistrationAttempt()
                {
                    Email = user.Email,
                    Success = false,
                    Error = ex.Message
                };
            }
        }

        private string PostToGoToWebinar(Data.GTMOrganizer organizer, string url, string data)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AllowAutoRedirect = true;
            request.MaximumAutomaticRedirections = 3;

            // allow all ssl certs
            ServicePointManager.ServerCertificateValidationCallback = (unused, unused2, unused3, unused4) =>
            {
                return true;
            };

            ASCIIEncoding ascii = new ASCIIEncoding();
            byte[] postBytes = ascii.GetBytes(data.ToString());

            request.Method = "POST";
            request.Accept = "application/vnd.citrix.g2wapi-v1.1+json";
            request.ContentType = "application/json";
            request.ContentLength = postBytes.Length;
            request.Headers[HttpRequestHeader.Authorization] = "OAuth oauth_token=" + GetGoToWebinarAccessToken(organizer);
            CookieContainer cookies = new CookieContainer();
            request.CookieContainer = cookies;

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(postBytes, 0, postBytes.Length);
                postStream.Close();
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (Stream responseStream = response.GetResponseStream())
            {
                using (StreamReader responseStreamReader = new StreamReader(responseStream))
                {
                    return responseStreamReader.ReadToEnd();
                }
            }
        }

        private string Get(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            CookieContainer cookies = new CookieContainer();
            request.CookieContainer = cookies;
            request.AllowAutoRedirect = true;
            request.MaximumAutomaticRedirections = 3;
            ServicePointManager.ServerCertificateValidationCallback = (unused, unused2, unused3, unused4) =>
            {
                return true;
            };

            request.Method = "GET";
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (Stream responseStream = response.GetResponseStream())
            {
                using (StreamReader responseStreamReader = new StreamReader(responseStream))
                {
                    return responseStreamReader.ReadToEnd();
                }
            }
        }

        /// <summary>
        /// Gets a specific training program value. Returns null if the customer ids don't match.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public Stream GetCourseFile(int customerId, int userId, int fileId, ref string name)
        {
            Data.CourseFile file = new Data.CourseFile(fileId);
            Data.Course c = new Data.Course(file.CourseID);
            if (c.CustomerID != customerId)
            {
                return null;
            }

            string path = HttpContext.Current.Server.MapPath(CourseFilesLocation + fileId);
            name = file.FileName;

            if (System.IO.File.Exists(path))
            {
                // grab it from the file system
                return new MemoryStream(System.IO.File.ReadAllBytes(path));
            }
            else
            {
                // grab it from the database
                return new MemoryStream(file.FileBytes);
            }
        }

        public PagedResult<Course> FindCourses(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.Course.Columns.Name;
            orderBy = "[dbo].[" + Data.Tables.Course + "].[" + orderBy + "]";

            SqlQuery query = Select.AllColumnsFrom<Data.Course>()
                .Where(Data.Course.NameColumn).ContainsString(searchText)
                .And(Data.Course.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.Course.IsThirdPartyColumn).IsEqualTo(false);

            return new PagedResult<Course>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<ClassOrOnlineCourse> FindEnrolledClass(int customerId, int userId, int trainingProgramId, int courseId, CourseType courseType)
        {

            List<int> classIds = new Select(Data.Registration.ClassIDColumn.QualifiedName)
                            .From(Data.Tables.Registration)
                            .Where(Data.Registration.RegistrantIDColumn).IsEqualTo(UserID)
                            .And(Data.Registration.RegistrationStatusIDColumn).IsEqualTo(RegistrationStatusType.Registered)
                            .And(Data.Registration.ClassIDColumn).IsEqualTo(courseId)
                            .ExecuteTypedList<int>();

            List<int> tpIds =
                    Data.SPs.GetTrainingProgramsForUser(UserID, UserIsNewHire, string.Empty, string.Empty, string.Empty, 0, int.MaxValue, null, null)
                    .ExecuteTypedList<TrainingProgram>()
                    .Select(tpl => tpl.Id)
                    .Where(tpid => tpid == trainingProgramId)
                    .ToList<int>();

            if (tpIds.Count == 0)
            {
                throw new Exception("User is not registered for the requested training program.");
            }

            if (classIds.Count == 0 && courseType == CourseType.Classroom)
            {
                throw new Exception("User is not registered for the requested class.");
            }

            ClassOrOnlineCourse course = Select.AllColumnsFrom<Data.ClassesAndTPOnlineCourse>()
                .Where(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                .And(Data.ClassesAndTPOnlineCourse.Columns.CourseTypeID).IsEqualTo((int)courseType)
                .And(Data.ClassesAndTPOnlineCourse.Columns.Id).IsEqualTo(courseId)
                .ExecuteSingle<ClassOrOnlineCourse>();

            return new SingleResult<ClassOrOnlineCourse>(course);
        }

        public PagedResult<ClassOrOnlineCourse> FindEnrolledClasses(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, int trainingProgramId = 0, int courseOrClassId = 0, CourseType courseType = CourseType.Online)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ClassesAndTPOnlineCourse>();

            List<int> classIds = new Select(Data.Registration.ClassIDColumn.QualifiedName)
                            .From(Data.Tables.Registration)
                            .Where(Data.Registration.RegistrantIDColumn).IsEqualTo(UserID)
                            .And(Data.Registration.RegistrationStatusIDColumn).IsEqualTo(RegistrationStatusType.Registered)
                            .ExecuteTypedList<int>();

            List<int> tpIds =
                    Data.SPs.GetTrainingProgramsForUser(UserID, UserIsNewHire, string.Empty, string.Empty, string.Empty, 0, int.MaxValue, null, null)
                    .ExecuteTypedList<TrainingProgram>()
                    .Select(tpl => tpl.Id)
                    .ToList<int>();

            if (tpIds.Count == 0)
            {
                tpIds.Add(-1);
            }
            if (classIds.Count == 0)
            {
                classIds.Add(-1);
            }

            query.Where(Data.ClassesAndTPOnlineCourse.Columns.CourseTypeID).IsEqualTo((int)CourseType.Classroom)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.Id).In(classIds)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramID).IsEqualTo(0)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.Id).NotIn(
                        new Select(Data.ClassesAndTPOnlineCourse.Columns.Id)
                            .From<Data.ClassesAndTPOnlineCourse>()
                            .Where(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramID).In(tpIds)
                            .And(Data.ClassesAndTPOnlineCourse.Columns.CourseTypeID).IsEqualTo((int)CourseType.Classroom)
                            .And(Data.ClassesAndTPOnlineCourse.Columns.Id).In(classIds)
                            .And(Data.ClassesAndTPOnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                    )
                    .Or(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramID).In(tpIds)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.CourseTypeID).IsEqualTo((int)CourseType.Online)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                    .Or(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramID).In(tpIds)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.CourseTypeID).IsEqualTo((int)CourseType.Classroom)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.Id).In(classIds)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.CustomerID).IsEqualTo(customerId);

            query.OrderAsc(new string[] { Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramID, 
                                          Data.ClassesAndTPOnlineCourse.Columns.SyllabusTypeID, 
                                          Data.ClassesAndTPOnlineCourse.Columns.RequiredSyllabusOrder });

            return new PagedResult<ClassOrOnlineCourse>(query, pageIndex, pageSize, orderBy, orderDir).WithUtcFlag();
        }

        public PagedResult<ClassOrOnlineCourse> FindInstructedClasses(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ClassesAndTPOnlineCourse>();

            // Classes the user instructs
            var classIds = new Select(Data.ClassInstructor.ClassIDColumn.QualifiedName)
                        .From(Data.Tables.ClassInstructor)
                        .Where(Data.ClassInstructor.InstructorIDColumn).IsEqualTo(UserID)
                        .ExecuteTypedList<int>();

            // Training programs the user leads
            var tpIds = new Select(Data.TrainingProgramLeader.TrainingProgramIDColumn.QualifiedName)
                            .From(Data.Tables.TrainingProgramLeader)
                            .Where(Data.TrainingProgramLeader.UserIDColumn).IsEqualTo(UserID)
                            .ExecuteTypedList<int>();

            if (tpIds.Count == 0)
            {
                tpIds.Add(-1);
            }
            if (classIds.Count == 0)
            {
                classIds.Add(-1);
            }

            // Get all the classes and online courses the user leads or instructs
            query
                    .WhereExpression(Data.ClassesAndTPOnlineCourse.Columns.CourseTypeID).IsEqualTo((int)CourseType.Classroom)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.Id).In(classIds)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramID).IsEqualTo(0)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.Id).NotIn(
                        new Select(Data.ClassesAndTPOnlineCourse.Columns.Id)
                            .From<Data.ClassesAndTPOnlineCourse>()
                            .Where(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramID).IsGreaterThan(0)
                            .And(Data.ClassesAndTPOnlineCourse.Columns.Id).In(classIds)
                            .And(Data.ClassesAndTPOnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                    )
                    .Or(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramID).In(tpIds)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                    .Or(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramID).NotIn(tpIds)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramID).IsNotEqualTo(0)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.Id).In(classIds)
                    .And(Data.ClassesAndTPOnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                    .CloseExpression();
           
            if (!string.IsNullOrEmpty(searchText))
            {
                query.And(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramName).ContainsString(searchText);
            }
            query.OrderAsc(Data.ClassesAndTPOnlineCourse.Columns.TrainingProgramName);

            return new PagedResult<ClassOrOnlineCourse>(query, pageIndex, pageSize, orderBy, orderDir).WithUtcFlag();

        }

        public PagedResult<ClassOrOnlineCourse> FindInstructedClassesOptimized(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // Classes the user instructs
            SqlQuery classIdQuery = new Select(Data.ClassInstructor.ClassIDColumn.QualifiedName)
                        .From(Data.Tables.ClassInstructor)
                        .Where(Data.ClassInstructor.InstructorIDColumn).IsEqualTo(UserID);

            // Training programs the user leads
            SqlQuery trainingProgramIdQuery = new Select(Data.TrainingProgramLeader.TrainingProgramIDColumn.QualifiedName)
                            .InnerJoin(Data.TrainingProgram.IdColumn, Data.TrainingProgramLeader.TrainingProgramIDColumn)
                            .From<Data.TrainingProgramLeader>()
                            .Where(Data.TrainingProgramLeader.UserIDColumn).IsEqualTo(UserID)
                            // We don't want to consider child training programs
                            .And(Data.TrainingProgram.ParentTrainingProgramIDColumn).IsEqualTo(0);

            // Get the classes
            SqlQuery classQuery = new Select(
                Data.ClassX.IdColumn.QualifiedName,
                Data.ClassX.CustomerIDColumn.QualifiedName,
                Data.ClassX.NameColumn.QualifiedName,
                Data.ClassX.IsVirtualColumn.QualifiedName,
                Data.ClassMinStartDateTime.Columns.MinStartDateTime + " as MinClassDate",
                Data.MessageBoard.IdColumn.QualifiedName + " as MessageBoardID"
            ).From<Data.ClassX>()
            .LeftOuterJoin(Data.ClassDateMinMax.Schema.Name, Data.ClassDateMinMax.Columns.ClassID,
                            Data.ClassX.Schema.Name, Data.ClassX.IdColumn.ColumnName)
            .LeftOuterJoin(Data.MessageBoard.ClassIdColumn, Data.ClassX.IdColumn)
            .Where(Data.ClassX.IdColumn).In(classIdQuery)
            .And(Data.ClassX.CustomerIDColumn).IsEqualTo(customerId);

            List<ClassOrOnlineCourse> classes = classQuery.ExecuteTypedList<ClassOrOnlineCourse>();

            classes.ForEach(c =>
            {
                c.CourseTypeID = (int)CourseType.Classroom;
                c.TrainingProgramID = 0;
                c.TrainingProgramName = null;
                c.RequiredSyllabusOrder = 0;
                c.SyllabusTypeId = 0;
                c.TrainingProgramMessageBoardID = 0;
                c.GroupByTrainingProgramID = 0;
                c.ParentTrainingProgramID = 0;
            });

            // Get the Training Program online courses
            // Removed grouping and parent training program id from this
            // query since we will only be loading the parent training programs
            // now due to the change in the trainingProgramIdQuery
            SqlQuery trainingProgramOnlineCourseQuery = new Select(
                Data.OnlineCourse.IdColumn,
                Data.OnlineCourse.NameColumn,
                Data.TrainingProgramToCourseLink.CustomerIDColumn,
                Data.TrainingProgramToCourseLink.TrainingProgramIDColumn,
                Data.TrainingProgramToCourseLink.CourseTypeIDColumn,
                Data.TrainingProgramToCourseLink.RequiredSyllabusOrderColumn,
                Data.TrainingProgramToCourseLink.SyllabusTypeIDColumn
            ).From<Data.TrainingProgramToCourseLink>()
            .IncludeColumn(Data.TrainingProgram.NameColumn.QualifiedName, "TrainingProgramName")
            .IncludeColumn(Data.MessageBoard.IdColumn.QualifiedName, "MessageBoardID")
            .InnerJoin(Data.OnlineCourse.IdColumn, Data.TrainingProgramToCourseLink.CourseIDColumn)
            .LeftOuterJoin(Data.MessageBoard.OnlineCourseIdColumn, Data.OnlineCourse.IdColumn)
            .InnerJoin(Data.TrainingProgram.IdColumn, Data.TrainingProgramToCourseLink.TrainingProgramIDColumn)
            .Where(Data.TrainingProgram.IdColumn).In(trainingProgramIdQuery)
            .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo(CourseType.Online)
            .And(Data.TrainingProgramToCourseLink.CustomerIDColumn).IsEqualTo(customerId)
            .OrderAsc(Data.TrainingProgramToCourseLink.RequiredSyllabusOrderColumn.QualifiedName)
            .OrderAsc(Data.TrainingProgram.NameColumn.QualifiedName);

            List<ClassOrOnlineCourse> trainingProgramOnlineCourses = trainingProgramOnlineCourseQuery.ExecuteTypedList<ClassOrOnlineCourse>();

            trainingProgramOnlineCourses.ForEach(c =>
            {
                c.IsVirtual = true;
            });

            // Get the Training program classroom courses
            SqlQuery trainingProgramClassroomCourseQuery = new Select(
                Data.ClassX.IdColumn,
                Data.ClassX.NameColumn,
                Data.ClassX.IsVirtualColumn,
                Data.ClassDateMinMax.Schema.GetColumn(Data.ClassDateMinMax.Columns.MinStartDateTime),
                Data.TrainingProgramToCourseLink.CustomerIDColumn,
                Data.TrainingProgramToCourseLink.TrainingProgramIDColumn,
                Data.TrainingProgramToCourseLink.CourseTypeIDColumn,
                Data.TrainingProgramToCourseLink.RequiredSyllabusOrderColumn,
                Data.TrainingProgramToCourseLink.SyllabusTypeIDColumn
            ).From<Data.TrainingProgramToCourseLink>()
            .IncludeColumn(Data.TrainingProgram.NameColumn.QualifiedName, "TrainingProgramName")
            .IncludeColumn(Data.MessageBoard.IdColumn.QualifiedName, "MessageBoardID")
            .InnerJoin(Data.ClassX.CourseIDColumn, Data.TrainingProgramToCourseLink.CourseIDColumn)
            .InnerJoin(Data.TrainingProgram.IdColumn, Data.TrainingProgramToCourseLink.TrainingProgramIDColumn)
            .LeftOuterJoin(Data.ClassDateMinMax.Schema.Name, Data.ClassDateMinMax.Columns.ClassID,
                            Data.ClassX.Schema.Name, Data.ClassX.IdColumn.ColumnName)
            .LeftOuterJoin(Data.MessageBoard.ClassIdColumn, Data.ClassX.IdColumn)
            .Where(Data.TrainingProgram.IdColumn.QualifiedName).In(trainingProgramIdQuery)
            .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo(CourseType.Classroom)
            .And(Data.TrainingProgramToCourseLink.CustomerIDColumn).IsEqualTo(customerId)
            .OrExpression(Data.TrainingProgram.IdColumn.QualifiedName).NotIn(trainingProgramIdQuery)
                    .And(Data.ClassX.IdColumn.QualifiedName).In(classIdQuery)
            .CloseExpression()
            .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo(CourseType.Classroom)
            .And(Data.TrainingProgramToCourseLink.CustomerIDColumn).IsEqualTo(customerId)
            .OrderAsc(Data.TrainingProgram.NameColumn.QualifiedName);

            List<ClassOrOnlineCourse> trainingProgramClasses = trainingProgramClassroomCourseQuery.ExecuteTypedList<ClassOrOnlineCourse>();

            Dictionary<int, ClassOrOnlineCourse> trainingProgramClassDictionary = trainingProgramClasses
                .DistinctBy(c => c.ID)
                .ToDictionary(c => c.ID);

            classes = classes.Where(c => !trainingProgramClassDictionary.ContainsKey(c.ID)).ToList();


            List<ClassOrOnlineCourse> allCourses = classes.Concat(trainingProgramOnlineCourses).Concat(trainingProgramClasses).ToList();

            if (!string.IsNullOrEmpty(searchText))
            {
                allCourses = allCourses.Where(c => c.Name.Contains(searchText) || (!string.IsNullOrEmpty(c.TrainingProgramName) && c.TrainingProgramName.Contains(searchText))).ToList();
            }

            List<int> actualTrainingProgramIds = allCourses.Where(c => c.TrainingProgramID > 0 || c.ParentTrainingProgramID > 0)
                .Select(c => c.ParentTrainingProgramID > 0 ? c.ParentTrainingProgramID : c.TrainingProgramID)
                .Distinct()
                .ToList();

            List<int> allTrainingProgramIds = allCourses.Where(c => c.TrainingProgramID > 0).Select(c => c.TrainingProgramID).ToList();

            if (actualTrainingProgramIds.Count() == 0)
            {
                actualTrainingProgramIds.Add(-1);
            }

            Dictionary<int, int> trainingProgramMessageBoardDictionary = new Select(
                Data.MessageBoard.IdColumn,
                Data.MessageBoard.TrainingProgramIdColumn
            )
            .From<Data.MessageBoard>()
            .Where(Data.MessageBoard.TrainingProgramIdColumn).In(actualTrainingProgramIds)
            .ExecuteTypedList<MessageBoard>()
            .DistinctBy(m => m.TrainingProgramId)
            .ToList()
            .ToDictionary(m => m.TrainingProgramId.Value, m => m.ID);

            if (allTrainingProgramIds.Count == 0)
            {
                allTrainingProgramIds.Add(-1);
            }

            // This query just got too complicated to run within subsonic. 
            // Don't want to create a view out of this since that would 
            // load a lot of extra data
            Dictionary<int, int> trainingProgramsWithUnmarkedAssignments = Data.SPs.GetTrainingProgramsWithUnmarkedAssignments(string.Join(",", actualTrainingProgramIds))
                .ExecuteTypedList<int>()
                .ToDictionary(i => i);
            
            allCourses.ForEach(c =>
            {
                if (c.ParentTrainingProgramID > 0)
                {
                    c.GroupByTrainingProgramID = c.ParentTrainingProgramID;
                    if (trainingProgramMessageBoardDictionary.ContainsKey(c.ParentTrainingProgramID))
                    {
                        c.TrainingProgramMessageBoardID = trainingProgramMessageBoardDictionary[c.ParentTrainingProgramID];
                    }
                }
                else if (c.TrainingProgramID > 0)
                {
                    c.GroupByTrainingProgramID = c.TrainingProgramID;
                    if (trainingProgramMessageBoardDictionary.ContainsKey(c.TrainingProgramID))
                    {
                        c.TrainingProgramMessageBoardID = trainingProgramMessageBoardDictionary[c.TrainingProgramID];
                    }
                }
                
                c.HasUnmarkedAssignments = false;

                if (c.TrainingProgramID > 0)
                {
                    c.HasUnmarkedAssignments = trainingProgramsWithUnmarkedAssignments.ContainsKey(c.TrainingProgramID) || trainingProgramsWithUnmarkedAssignments.ContainsKey(c.ParentTrainingProgramID);
                }
            });

            return new PagedResult<ClassOrOnlineCourse>(allCourses.AsQueryable(), pageIndex, pageSize, "TrainingProgramName", orderDir).WithUtcFlag();
        }

        public PagedResult<ClassroomClass> FindClasses(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, ClassesFilter classFilter)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.ClassX.Columns.Name;
            if (orderBy.ToLower() == "location")
            {
                orderBy = "[dbo].[" + Data.Tables.Room + "].[" + Data.Room.Columns.Name + "], " +
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.Name + "], " +
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.City + "], " +
                    "[dbo].[" + Data.Tables.State + "].[" + Data.State.Columns.Name + "]";
            }
            else if (orderBy.ToLower() == "minclassdate")
            {
                orderBy = "[dbo].[" + Data.Views.ClassDateMinMax + "].[" + Data.ClassDateMinMax.Columns.MinStartDateTime + "]";
            }
            else if (orderBy.ToLower() == "maxclassdate")
            {
                orderBy = "[dbo].[" + Data.Views.ClassDateMinMax + "].[" + Data.ClassDateMinMax.Columns.MaxStartDateTime + "]";
            }
            else
            {
                orderBy = "[dbo].[" + Data.Tables.ClassX + "].[" + orderBy + "]";
            }


            SqlQuery query = new Select(
                    Data.ClassX.IdColumn.QualifiedName,
                    Data.ClassX.LockedIndicatorColumn.QualifiedName,
                    Data.ClassX.NameColumn.QualifiedName,
                    Data.ClassX.IsVirtualColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.Room + "].[" + Data.Room.Columns.Name + "] AS [RoomName]",
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.Name + "] AS [VenueName]",
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.City + "] AS [VenueCity]",
                    "[dbo].[" + Data.Tables.State + "].[" + Data.State.Columns.Name + "] AS [StateName]",
                    Data.ClassDateMinMax.Columns.MinStartDateTime + " as MinClassDate",
                    Data.ClassDateMinMax.Columns.MaxStartDateTime + " as MaxClassDate"
                )
                .From(Data.Tables.ClassX)
                .Where(Data.ClassX.NameColumn).ContainsString(searchText)
                .And(Data.ClassX.CustomerIDColumn).IsEqualTo(customerId)
                .LeftOuterJoin(Data.Tables.Room, Data.Room.Columns.Id, Data.Tables.ClassX, Data.ClassX.Columns.RoomID)
                .LeftOuterJoin(Data.Tables.Venue, Data.Venue.Columns.Id, Data.Tables.Room, Data.Room.Columns.VenueID)
                .LeftOuterJoin(Data.Tables.State, Data.State.Columns.Id, Data.Tables.Venue, Data.Venue.Columns.StateID)
                .LeftOuterJoin(Data.Views.ClassDateMinMax, Data.ClassDateMinMax.Columns.ClassID, Data.Tables.ClassX, Data.ClassX.Columns.Id);

            // additional filtering
            // New rules 11/19/2009, TP feature #754
            // From now on, supervisors can see all classes, but when registering users, the user list will be limited to their supervised users
            if (!(this.HasRole(Roles.ClassroomManager) || this.HasRole(Roles.ClassroomSupervisor)))
            {
                // the rules cascade...
                bool conditionAdded = false;

                if (this.HasRole(Roles.ClassroomInstructor))
                {
                    var classIds = new Select(Data.ClassInstructor.ClassIDColumn.QualifiedName)
                        .From(Data.Tables.ClassInstructor)
                        .Where(Data.ClassInstructor.InstructorIDColumn).IsEqualTo(UserID)
                        .ExecuteTypedList<int>();
                    if (classIds.Count > 0)
                    {
                        query.And(Data.ClassX.Columns.Id).In(classIds);
                        conditionAdded = true;
                    }
                }

                if (this.HasRole(Roles.ClassroomResourceManager))
                {
                    var venueIds = new Select(Data.VenueResourceManager.VenueIDColumn.QualifiedName)
                        .From(Data.Tables.VenueResourceManager)
                        .Where(Data.VenueResourceManager.ResourceManagerIDColumn).IsEqualTo(UserID)
                        .ExecuteTypedList<int>();

                    if (venueIds.Count > 0)
                    {

                        var roomIds = new Select(Data.Room.IdColumn.QualifiedName)
                            .From(Data.Tables.Room)
                            .Where(Data.Room.VenueIDColumn).In(venueIds)
                            .ExecuteTypedList<int>();

                        if (roomIds.Count > 0)
                        {
                            query.And(Data.ClassX.Columns.RoomID).In(roomIds);
                            conditionAdded = true;
                        }
                    }
                }

                if (!conditionAdded)
                {
                    // either they don't have any of the permissions above, or no classes fall into their permission level
                    // either way, deny them everything
                    query.And(Data.ClassX.Columns.Id).IsEqualTo(-1);
                }
            }

            // More filtering, based on the passed params
            if (classFilter != null)
            {
                if (classFilter.Archived)
                {
                    query.And(Data.ClassDateMinMax.Columns.MaxStartDateTime).IsLessThan(DateTime.Today);
                }
                else
                {
                    query.And(Data.ClassDateMinMax.Columns.MaxStartDateTime).IsGreaterThanOrEqualTo(DateTime.Today);
                }
            }

            return new PagedResult<ClassroomClass>(query, pageIndex, pageSize, orderBy, orderDir).WithUtcFlag();
        }

        public PagedResult<Venue> FindVenues(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.Venue.Columns.Name;
            if (orderBy.ToLower() == "statename")
            {
                // one exception to the ordering, since we have multiple tables in the query
                orderBy = "[dbo].[" + Data.Tables.State + "].[" + Data.State.Columns.Name + "]";
            }
            else
            {
                orderBy = "[dbo].[" + Data.Tables.Venue + "].[" + orderBy + "]";
            }

            SqlQuery query = new Select(
                    Data.Venue.CityColumn.QualifiedName,
                    Data.Venue.IdColumn.QualifiedName,
                    Data.Venue.InternalCodeColumn.QualifiedName,
                    Data.Venue.NameColumn.QualifiedName,
                    Data.Venue.StateIDColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.State + "].[" + Data.State.Columns.Name + "] AS [StateName]",
                    "[dbo].[" + Data.Tables.State + "].[" + Data.State.Columns.Description + "] AS [StateDescription]",
                    Data.Venue.TimeZoneIDColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.TimeZone + "].[" + Data.TimeZone.Columns.Description + "] AS [TimeZoneDescription]"
                )
                .From(Data.Tables.Venue)
                .Where(Data.Venue.NameColumn).ContainsString(searchText)
                .And(Data.Venue.CustomerIDColumn).IsEqualTo(customerId)
                .LeftOuterJoin(Data.Tables.State, Data.State.Columns.Id, Data.Tables.Venue, Data.Venue.Columns.StateID)
                .LeftOuterJoin(Data.Tables.TimeZone, Data.TimeZone.Columns.Id, Data.Tables.Venue, Data.Venue.Columns.TimeZoneID);

            return new PagedResult<Venue>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Room> FindRooms(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.Room.Columns.Name;
            orderBy = "[dbo].[" + Data.Tables.Room + "].[" + orderBy + "]";

            SqlQuery query = new Select(
                    Data.Room.IdColumn,
                    Data.Room.NameColumn
                )
                .From(Data.Tables.Room)
                .Where(Data.Room.Columns.Name).ContainsString(searchText)
                .And(Data.Room.Columns.CustomerID).IsEqualTo(customerId);

            return new PagedResult<Room>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Room> GetVenueRooms(int customerId, int userId, string venueId, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.Room.Columns.Name;
            orderBy = "[dbo].[" + Data.Tables.Room + "].[" + orderBy + "]";

            SqlQuery query = Select.AllColumnsFrom<Data.Room>()
                .Where(Data.Room.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.Room.Columns.VenueID).IsEqualTo(venueId);

            return new PagedResult<Room>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<PresentationType> FindPresentationTypes(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.PresentationType.Columns.Description;
            orderBy = "[dbo].[" + Data.Tables.PresentationType + "].[" + orderBy + "]";

            SqlQuery query = Select.AllColumnsFrom<Data.PresentationType>()
                .Where(Data.PresentationType.Columns.Description).StartsWith(searchText);

            return new PagedResult<PresentationType>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Models.CourseCompletionType> FindCourseCompletionTypes(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.CourseCompletionType.Columns.Description;
            orderBy = "[dbo].[" + Data.Tables.CourseCompletionType + "].[" + orderBy + "]";

            SqlQuery query = Select.AllColumnsFrom<Data.CourseCompletionType>()
                .Where(Data.CourseCompletionType.Columns.Description).StartsWith(searchText);

            return new PagedResult<Models.CourseCompletionType>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<State> FindStates(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.State.Columns.Description;
            orderBy = "[dbo].[" + Data.Tables.State + "].[" + orderBy + "]";

            SqlQuery query = Select.AllColumnsFrom<Data.State>()
                .Where(Data.State.Columns.Description).StartsWith(searchText)
                .Or(Data.State.Columns.Name).StartsWith(searchText);

            return new PagedResult<State>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Models.TimeZone> FindTimeZones(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.TimeZone.Columns.Description;
            orderBy = "[dbo].[" + Data.Tables.TimeZone + "].[" + orderBy + "]";

            SqlQuery query = Select.AllColumnsFrom<Data.TimeZone>()
                .Where(Data.TimeZone.Columns.Description).StartsWith(searchText);

            return new PagedResult<Models.TimeZone>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Course> FindOnlineCourses(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.OnlineCourse.Columns.Name;
            orderBy = "[dbo].[" + Data.Tables.OnlineCourse + "].[" + orderBy + "]";

            SqlQuery query = new Select(
                    Data.OnlineCourse.IdColumn,
                    Data.OnlineCourse.NameColumn
                ).From(Data.Tables.OnlineCourse)
                .Where(Data.OnlineCourse.Columns.Name).StartsWith(searchText)
                .And(Data.OnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.OnlineCourse.Columns.IsTest).IsEqualTo(true);

            return new PagedResult<Course>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Course> FindOnlineSurveys(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.OnlineCourse.Columns.Name;
            orderBy = "[dbo].[" + Data.Tables.OnlineCourse + "].[" + orderBy + "]";

            SqlQuery query = new Select(
                    Data.OnlineCourse.IdColumn,
                    Data.OnlineCourse.NameColumn
                ).From(Data.Tables.OnlineCourse)
                .Where(Data.OnlineCourse.Columns.Name).StartsWith(searchText)
                .And(Data.OnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.OnlineCourse.Columns.IsSurvey).IsEqualTo(true)
                .And(Data.OnlineCourse.Columns.DuplicateFromID).IsEqualTo(0);

            return new PagedResult<Course>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ClassInstructor> GetClassInstructors(int customerId, int userId, string classId, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.User.Columns.FirstName;
            orderBy = "[dbo].[" + Data.Tables.User + "].[" + orderBy + "]";

            SqlQuery query = new Select(
                    Data.ClassInstructor.IdColumn.QualifiedName,
                    Data.ClassInstructor.ClassIDColumn.QualifiedName,
                    Data.ClassInstructor.InstructorIDColumn.QualifiedName,
                    Data.User.FirstNameColumn.QualifiedName,
                    Data.User.MiddleNameColumn.QualifiedName,
                    Data.User.LastNameColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.GTMOrganizer + "].[" + Data.GTMOrganizer.Columns.Status + "] AS [GTMOrganizerStatus]"
                ).From(Data.Tables.ClassInstructor)
                .LeftOuterJoin(Data.Tables.User, Data.User.Columns.Id, Data.Tables.ClassInstructor, Data.ClassInstructor.Columns.InstructorID)
                .LeftOuterJoin(Data.Tables.GTMOrganizer, Data.GTMOrganizer.Columns.UserID, Data.Tables.ClassInstructor, Data.ClassInstructor.Columns.InstructorID)
                .Where(Data.User.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.ClassInstructor.ClassIDColumn).IsEqualTo(classId);

            return new PagedResult<ClassInstructor>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ClassResource> GetClassResources(int customerId, int userId, string classId, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.Resource.Columns.Name;
            orderBy = "[dbo].[" + Data.Tables.Resource + "].[" + orderBy + "]";

            SqlQuery query = new Select(
                    Data.ClassResource.IdColumn.QualifiedName,
                    Data.ClassResource.ClassIDColumn.QualifiedName,
                    Data.ClassResource.ResourceIDColumn.QualifiedName,
                    Data.Resource.NameColumn.QualifiedName,
                    Data.Resource.CostColumn.QualifiedName,
                    Data.Resource.VenueIDColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.Name + "] AS [VenueName]",
                    Data.Resource.RoomIDColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.Room + "].[" + Data.Room.Columns.Name + "] AS [RoomName]"
                ).From(Data.Tables.ClassResource)
                .LeftOuterJoin(Data.Tables.Resource, Data.Resource.Columns.Id, Data.Tables.ClassResource, Data.ClassResource.Columns.ResourceID)
                .LeftOuterJoin(Data.Tables.Venue, Data.Venue.Columns.Id, Data.Tables.Resource, Data.Resource.Columns.VenueID)
                .LeftOuterJoin(Data.Tables.Room, Data.Room.Columns.Id, Data.Tables.Resource, Data.Resource.Columns.RoomID)
                .Where(Data.Resource.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.ClassResource.ClassIDColumn).IsEqualTo(classId);

            return new PagedResult<ClassResource>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ClassDate> GetClassDates(int customerId, int userId, string classId, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.ClassDate.Columns.StartDateTime;
            orderBy = "[dbo].[" + Data.Tables.ClassDate + "].[" + orderBy + "]";

            SqlQuery query = Select.AllColumnsFrom<Data.ClassDate>()
                .Where(Data.ClassDate.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.ClassDate.ClassIDColumn).IsEqualTo(classId);

            return new PagedResult<ClassDate>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Registration> GetRegistrations(int customerId, int userId, string classId, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (Data.Registration.Schema.GetColumn(orderBy) != null)
            {
                orderBy = "[dbo].[" + Data.Tables.Registration + "].[" + orderBy + "]";
            }
            else if (Data.User.Schema.GetColumn(orderBy) != null)
            {
                orderBy = "[dbo].[" + Data.Tables.User + "].[" + orderBy + "]";
            }
            else
            {
                orderBy = Data.Registration.Columns.Score;
            }
            
            SqlQuery query = new Select(
                    Data.Registration.IdColumn.QualifiedName,
                    Data.Registration.ClassIDColumn.QualifiedName,
                    Data.Registration.RegistrationStatusIDColumn.QualifiedName,
                    Data.Registration.RegistrantIDColumn.QualifiedName,
                    Data.Registration.RegistrationTypeIDColumn.QualifiedName,
                    Data.Registration.AttendedIndicatorColumn.QualifiedName,
                    Data.Registration.CanMoveForwardIndicatorColumn.QualifiedName,
                    Data.Registration.ScoreColumn.QualifiedName,
                    Data.User.LastNameColumn.QualifiedName,
                    Data.User.MiddleNameColumn.QualifiedName,
                    Data.User.FirstNameColumn.QualifiedName,
                    Data.User.SupervisorIDColumn.QualifiedName,
                    Data.User.SecondarySupervisorIDColumn.QualifiedName,
                    Data.ClassX.CapacityOverrideColumn.QualifiedName + " AS [ClassCapacity]"
                ).From(Data.Tables.Registration)
                .InnerJoin(Data.ClassX.IdColumn, Data.Registration.ClassIDColumn)
                .InnerJoin(Data.User.IdColumn, Data.Registration.RegistrantIDColumn)
                .Where(Data.ClassX.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.Registration.ClassIDColumn).IsEqualTo(classId);

            return new PagedResult<Registration>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public MultipleResult<ClassroomClass> GetRegisteredClassesForCourses(int userId, int[] courseIds)
        {
            List<string> columns = Data.ClassX.Schema.Columns.Select(c => c.QualifiedName).ToList();
            columns.AddRange(Data.Course.Schema.Columns.Select(c => c.QualifiedName).ToList());
            columns.Add(Data.ClassDateMinMax.Columns.MaxStartDateTime + " as [MaxClassDate]");
            columns.Add(Data.ClassDateMinMax.Columns.MinStartDateTime + " as [MinClassDate]");

            if (courseIds.Length == 0)
            {
                courseIds = new int[] { -1 };
            }

            SqlQuery query = new Select(columns.ToArray())
                .From<Data.Registration>()
                .InnerJoin(Data.ClassX.IdColumn, Data.Registration.ClassIDColumn)
                .InnerJoin(Data.ClassDateMinMax.Schema.TableName, Data.ClassDateMinMax.Columns.ClassID,
                            Data.ClassX.Schema.TableName, Data.ClassX.IdColumn.ColumnName)
                .InnerJoin(Data.Course.IdColumn, Data.ClassX.CourseIDColumn)
                .Where(Data.Registration.RegistrationStatusIDColumn).IsEqualTo((int)RegistrationStatusType.Registered)
                .And(Data.Registration.RegistrantIDColumn).IsEqualTo(userId)
                .And(Data.ClassX.CourseIDColumn).In(courseIds)
                .OrderAsc(Data.ClassDateMinMax.Columns.MinStartDateTime);

            return new MultipleResult<ClassroomClass>(query);
        }

        /// <summary>
        /// Gets a list of the instructors that are available to be assigned to the specified class
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="searchText"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="exclude"></param>
        /// <returns></returns>
        public PagedResult<User> GetClassInstructorList(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, List<int> exclude)
        {
            List<User> list = UserController.GetUsersByRole(customerId, userId, Roles.ClassroomInstructor, searchText);

            FlagCollaborators(customerId, userId, list);

            list.RemoveAll(x => exclude.Contains(x.ID));


            if (string.IsNullOrEmpty(orderBy)) { orderBy = "LastName"; }

            return new PagedResult<User>(list.AsQueryable(), pageIndex, pageSize, orderBy, orderDir);
        }

        private void FlagCollaborators(int customerId, int userId, List<User> users)
        {
            List<User> canHostVirtual = GetCollaboratorList(customerId, userId);

            // set the flag for any users that have GTM or GTW roles
            users.ForEach(u => u.Collaborator = canHostVirtual.Any(h => h.ID == u.ID));
        }

        private void FlagCollaborators(int customerId, int userId, List<ClassInstructor> users)
        {
            List<User> canHostVirtual = GetCollaboratorList(customerId, userId);

            // set the flag for any users that have GTM or GTW roles
            users.ForEach(u => u.Collaborator = canHostVirtual.Any(h => h.ID == u.InstructorID));
        }

        private List<User> GetCollaboratorList(int customerId, int userId)
        {
            List<User> canHostVirtual = UserController.GetUsersByRole(customerId, userId, Roles.GTMOrganizer, "");
            canHostVirtual.AddRange(UserController.GetUsersByRole(customerId, userId, Roles.GTWOrganizer, ""));

            return canHostVirtual;
        }

        /// <summary>
        /// Gets a list of the registrants that are available to be assigned to the specified class
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="searchText"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="exclude"></param>
        /// <returns></returns>
        public PagedResult<User> GetRegistrantList(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, List<int> exclude)
        {
            searchText = "%" + (searchText ?? string.Empty) + "%";

            if (orderBy == "name")
            {
                orderBy = Data.User.Columns.LastName + ", " + Data.User.Columns.FirstName;
            }

            if (exclude.Count == 0)
            {
                exclude.Add(-1);
            }

            SqlQuery query;
            if (HasRole(Roles.ClassroomManager))
            {
                query = Select.AllColumnsFrom<Data.User>()
                    .Where(Data.User.CustomerIDColumn).IsEqualTo(customerId)
                    .And(Data.User.Columns.SalesChannelID).IsEqualTo(0)
                    .And(Data.User.Columns.IsExternal).IsEqualTo(false)
                    .And(Data.User.StatusIDColumn).IsNotEqualTo(UserStatusType.Deleted)
                    .And(Data.User.IdColumn).NotIn(exclude)
                    .AndExpression(Data.User.Columns.FirstName).Like(searchText)
                    .Or(Data.User.MiddleNameColumn).Like(searchText)
                    .Or(Data.User.LastNameColumn).Like(searchText);
            }
            else
            {
                query = Select.AllColumnsFrom<Data.User>()
                    .WhereExpression(Data.User.SupervisorIDColumn.QualifiedName).IsEqualTo(userId)
                        .Or(Data.User.SecondarySupervisorIDColumn).IsEqualTo(userId)
                    .CloseExpression()
                    .And(Data.User.Columns.SalesChannelID).IsEqualTo(0)
                    .And(Data.User.Columns.IsExternal).IsEqualTo(false)
                    .And(Data.User.CustomerIDColumn).IsEqualTo(customerId)
                    .And(Data.User.StatusIDColumn).IsNotEqualTo(UserStatusType.Deleted)
                    .And(Data.User.IdColumn).NotIn(exclude)
                    .AndExpression(Data.User.Columns.FirstName).Like(searchText)
                    .Or(Data.User.MiddleNameColumn).Like(searchText)
                    .Or(Data.User.LastNameColumn).Like(searchText);
            }

            if (string.IsNullOrEmpty(orderBy)) { orderBy = "LastName"; }

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        /// <summary>
        /// Gets a list of the resource managers that are available to be added to the specified venue
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="searchText"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="exclude"></param>
        /// <returns></returns>
        public PagedResult<User> GetVenueResourceManagerList(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, List<int> exclude)
        {
            List<User> list = UserController.GetUsersByRole(customerId, userId, Roles.ClassroomResourceManager, searchText);

            list.RemoveAll(x => exclude.Contains(x.ID));
            if (string.IsNullOrEmpty(orderBy)) { orderBy = "LastName"; }

            return new PagedResult<User>(list.AsQueryable(), pageIndex, pageSize, orderBy, orderDir);
        }

        /// <summary>
        /// Gets a list of the resources that are available to be added to the specified class
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="searchText"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="exclude"></param>
        /// <returns></returns>
        public PagedResult<Resource> GetClassResourceList(int customerId, int userId, int venueId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, List<int> exclude)
        {
            if (exclude.Count == 0)
            {
                exclude = new List<int>() { -1 };
            }
            SqlQuery query = Select.AllColumnsFrom<Data.Resource>()
                .Where(Data.Resource.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.Resource.Columns.VenueID).IsEqualTo(venueId)
                .And(Data.Resource.IdColumn).NotIn(exclude)
                .And(Data.Resource.NameColumn).Like("%" + (searchText ?? string.Empty) + "%");

            if (string.IsNullOrEmpty(orderBy)) { orderBy = "Name"; }

            return new PagedResult<Resource>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<List<CalendarWeek>> GetCalendarWeeks(int customerId, int userId, string sYear, string sMonth, string[] sFilters)
        {
            int year;
            int month;
            if (!int.TryParse(sYear, out year))
            {
                return new SingleResult<List<CalendarWeek>>(new Exception("Invalid year."));
            }
            if (!int.TryParse(sMonth, out month))
            {
                return new SingleResult<List<CalendarWeek>>(new Exception("Invalid month."));
            }

            DateTime start = new DateTime(year, month, 1);
            DateTime stop = start.AddMonths(1).AddMilliseconds(-1);

            string startString = start.ToString("yyyy-MM-dd HH:mm:ss");
            string stopString = stop.ToString("yyyy-MM-dd HH:mm:ss");

            string sql = @"
DECLARE @SupervisedUserIds TABLE ( [UserID] int )

INSERT @SupervisedUserIds
    SELECT u.[ID]
    FROM [dbo].[User] u
    WHERE u.[CustomerID] = " + customerId + @"
    AND (u.[SupervisorID] = " + userId + @"
        OR u.[SecondarySupervisorID] = " + userId + @") -- Users can see registrations if they are either the primary supervisor, or secondary supervisor

SELECT
    cd.[ID] AS [ClassDateID],
    cd.[ClassID],
    cd.[StartDateTime],
    c.[Name],
    COUNT(mr1.[ID]) AS [ManagerUsersAwaitingApproval],
    COUNT(mr2.[ID]) AS [ManagerUsersWaitListed],
    COUNT(mr3.[ID]) AS [ManagerUsersDenied],
    COUNT(mr4.[ID]) AS [ManagerUsersRegistered],
    COUNT(ci.[ID]) AS [Instructors],
    COUNT(cr.[ID]) AS [RequiredResources],
    COUNT(sr1.[ID]) AS [SupervisorUsersAwaitingApproval],
    COUNT(sr2.[ID]) AS [SupervisorUsersWaitListed],
    COUNT(sr3.[ID]) AS [SupervisorUsersDenied],
    COUNT(sr4.[ID]) AS [SupervisorUsersRegistered]
FROM [dbo].[ClassDate] cd
INNER JOIN [dbo].[Class] c
    ON cd.[ClassID] = c.[ID]
LEFT JOIN [dbo].[Registration] mr1
        ON  mr1.[ClassID] = cd.[ClassID]
        AND mr1.[RegistrationStatusID] = 1
LEFT JOIN [dbo].[Registration] mr2
    ON  mr2.[ClassID] = cd.[ClassID]
    AND mr2.[RegistrationStatusID] = 2
LEFT JOIN [dbo].[Registration] mr3
    ON  mr3.[ClassID] = cd.[ClassID]
    AND mr3.[RegistrationStatusID] = 3
LEFT JOIN [dbo].[Registration] mr4
    ON  mr4.[ClassID] = cd.[ClassID]
    AND mr4.[RegistrationStatusID] = 4
LEFT JOIN [dbo].[ClassInstructors] ci
    ON  ci.[ClassID] = cd.[ClassID]
    AND ci.[InstructorID] = " + userId + @"
LEFT JOIN [dbo].[ClassResources] cr
    ON  cr.[ClassID] = cd.[ClassID]
LEFT JOIN [dbo].[Registration] sr1
    ON  sr1.[ClassID] = cd.[ClassID]
    AND sr1.[RegistrationStatusID] = 1
    AND sr1.[RegistrantID] IN (
        SELECT [UserID]
        FROM @SupervisedUserIds
    )
LEFT JOIN [dbo].[Registration] sr2
    ON  sr2.[ClassID] = cd.[ClassID]
    AND sr2.[RegistrationStatusID] = 2
    AND sr2.[RegistrantID] IN (
        SELECT [UserID]
        FROM @SupervisedUserIds
    )
LEFT JOIN [dbo].[Registration] sr3
    ON  sr3.[ClassID] = cd.[ClassID]
    AND sr3.[RegistrationStatusID] = 3
    AND sr3.[RegistrantID] IN (
        SELECT [UserID]
        FROM @SupervisedUserIds
    )
LEFT JOIN [dbo].[Registration] sr4
    ON  sr4.[ClassID] = cd.[ClassID]
    AND sr4.[RegistrationStatusID] = 4
    AND sr4.[RegistrantID] IN (
        SELECT [UserID]
        FROM @SupervisedUserIds
    )
WHERE cd.[CustomerID] = " + customerId + @"
AND c.[CustomerID] = " + customerId + @"
AND cd.[StartDateTime] BETWEEN '" + startString + @"' AND '" + stopString + @"'
GROUP BY cd.[ID], cd.[ClassID], cd.[StartDateTime], c.[Name]
ORDER BY c.[Name]
";

            // get classes
            List<CalendarClass> classes = new InlineQuery().ExecuteTypedList<CalendarClass>(sql);

            // next, we have to filter the classes
            bool? managerUsersRegistered = null,
                managerUsersWaitListed = null,
                managerUsersAwaitingApproval = null,
                managerUsersDenied = null,
                managerAllClasses = null,
                resourceManagerResourcesRequired = null,
                resourceManagerNoResourcesRequired = null,
                instructors = null,
                supervisorUsersRegistered = null,
                supervisorUsersWaitListed = null,
                supervisorUsersAwaitingApproval = null,
                supervisorUsersDenied = null;

            // true/false only applies if the role applies
            if (HasRole(Roles.ClassroomManager))
            {
                managerUsersRegistered = sFilters.Contains("m-ur");
                managerUsersWaitListed = sFilters.Contains("m-uwl");
                managerUsersAwaitingApproval = sFilters.Contains("m-uaa");
                managerUsersDenied = sFilters.Contains("m-ud");
                managerAllClasses = sFilters.Contains("m-ac");
            }
            if (HasRole(Roles.ClassroomResourceManager))
            {
                resourceManagerResourcesRequired = sFilters.Contains("rm-rr");
                resourceManagerNoResourcesRequired = sFilters.Contains("rm-nrr");
            }
            if (HasRole(Roles.ClassroomInstructor))
            {
                instructors = sFilters.Contains("i");
            }
            if (HasRole(Roles.ClassroomSupervisor))
            {
                supervisorUsersRegistered = sFilters.Contains("s-ur");
                supervisorUsersWaitListed = sFilters.Contains("s-uwl");
                supervisorUsersAwaitingApproval = sFilters.Contains("s-uaa");
                supervisorUsersDenied = sFilters.Contains("s-ud");
            }

            // check each filter and set the pass/fail property on the class
            classes.ForEach(
                x =>
                {
                    if (managerUsersRegistered.HasValue
                        && managerUsersRegistered.Value
                        && x.ManagerUsersRegistered > 0)
                    {
                        x.ManagerUsersRegisteredPass = true;
                    }
                    if (managerUsersWaitListed.HasValue
                        && managerUsersWaitListed.Value
                        && x.ManagerUsersWaitListed > 0)
                    {
                        x.ManagerUsersWaitListedPass = true;
                    }
                    if (managerUsersAwaitingApproval.HasValue
                        && managerUsersAwaitingApproval.Value
                        && x.ManagerUsersAwaitingApproval > 0)
                    {
                        x.ManagerUsersAwaitingApprovalPass = true;
                    }
                    if (managerUsersDenied.HasValue
                        && managerUsersDenied.Value
                        && x.ManagerUsersDenied > 0)
                    {
                        x.ManagerUsersDeniedPass = true;
                    }
                    if (managerAllClasses.HasValue
                        && managerAllClasses.Value)
                    {
                        x.ManagerAllClassesPass = true;
                    }
                    if (resourceManagerResourcesRequired.HasValue
                        && resourceManagerResourcesRequired.Value
                        && x.RequiredResources > 0)
                    {
                        x.ResourceManagerRequiredResourcesPass = true;
                    }
                    if (resourceManagerNoResourcesRequired.HasValue
                        && resourceManagerNoResourcesRequired.Value
                        && x.RequiredResources == 0)
                    {
                        x.ResourceManagerNoRequiredResourcesPass = true;
                    }
                    if (instructors.HasValue
                        && instructors.Value
                        && x.Instructors > 0)
                    {
                        x.InstructorsPass = true;
                    }
                    if (supervisorUsersRegistered.HasValue
                        && supervisorUsersRegistered.Value
                        && x.SupervisorUsersRegistered > 0)
                    {
                        x.SupervisorUsersRegisteredPass = true;
                    }
                    if (supervisorUsersWaitListed.HasValue
                        && supervisorUsersWaitListed.Value
                        && x.SupervisorUsersWaitListed > 0)
                    {
                        x.SupervisorUsersWaitListedPass = true;
                    }
                    if (supervisorUsersAwaitingApproval.HasValue
                        && supervisorUsersAwaitingApproval.Value
                        && x.SupervisorUsersAwaitingApproval > 0)
                    {
                        x.SupervisorUsersAwaitingApprovalPass = true;
                    }
                    if (supervisorUsersDenied.HasValue
                        && supervisorUsersDenied.Value
                        && x.SupervisorUsersDenied > 0)
                    {
                        x.SupervisorUsersDeniedPass = true;
                    }
                });

            // now we can exclude the classes with no filter passes
            classes.RemoveAll(
                x => !x.ManagerUsersRegisteredPass
                    && !x.ManagerUsersWaitListedPass
                    && !x.ManagerUsersAwaitingApprovalPass
                    && !x.ManagerUsersDeniedPass
                    && !x.ManagerAllClassesPass
                    && !x.ResourceManagerRequiredResourcesPass
                    && !x.ResourceManagerNoRequiredResourcesPass
                    && !x.InstructorsPass
                    && !x.SupervisorUsersRegisteredPass
                    && !x.SupervisorUsersWaitListedPass
                    && !x.SupervisorUsersAwaitingApprovalPass
                    && !x.SupervisorUsersDeniedPass);

            // flag first class in each group
            // first, we do a single iteration O(n) of the classes,
            // storing the one with the earliest start date/time
            Hashtable classHash = new Hashtable();
            foreach (CalendarClass klass in classes)
            {
                CalendarClass prevClass = classHash[klass.ClassID] as CalendarClass;
                if (prevClass == null)
                {
                    classHash[klass.ClassID] = klass;
                }
                else
                {
                    if (klass.StartDateTime < prevClass.StartDateTime)
                    {
                        classHash[klass.ClassID] = klass;
                    }
                }
                // we'll also flag each time as being in UTC
                klass.StartDateTime = klass.StartDateTime.WithUtcFlag();
            }
            // what's left are the minimum classes
            foreach (CalendarClass klass in classHash.Values)
            {
                klass.IsMinimum = true;
            }

            List<CalendarWeek> weeks = new List<CalendarWeek>();
            DateTime weekStart = start.AddDays(-(int)start.DayOfWeek);
            do
            {
                CalendarWeek calWeek = new CalendarWeek();
                for (int i = 0; i < 7; i++)
                {
                    DateTime date = weekStart.AddDays(i);
                    calWeek[i] = new ClassroomCalendarDay
                    {
                        Year = date.Year,
                        Month = date.Month,
                        Day = date.Day,
                        InCurrentMonth = (date.Month == month && date.Year == year),
                        Classes = classes.Where(
                            x => x.StartDateTime.Year == date.Year
                                && x.StartDateTime.Month == date.Month
                                && x.StartDateTime.Day == date.Day).ToList()
                    };
                }
                weeks.Add(calWeek);
                weekStart = weekStart.AddDays(7);
            } while (weekStart.Month == month && weekStart.Year == year);
            return new SingleResult<List<CalendarWeek>>(weeks);
        }

        public SingleResult<Course> GetCourse(int customerId, int userId, int courseId)
        {
            SqlQuery query = new Select(
                    Data.Course.CourseCompletionTypeParameterIDColumn.QualifiedName,
                    Data.Course.CourseObjectiveColumn.QualifiedName,
                    Data.Course.CreditColumn.QualifiedName,
                    Data.Course.DescriptionColumn.QualifiedName,
                    Data.Course.HasSurveyColumn.QualifiedName,
                    Data.Course.IdColumn.QualifiedName,
                    Data.Course.InternalCodeColumn.QualifiedName,
                    Data.Course.InTrainingCycleColumn.QualifiedName,
                    Data.Course.NameColumn.QualifiedName,
                    Data.Course.PerStudentFeeColumn.QualifiedName,
                    Data.Course.PublicIndicatorColumn.QualifiedName,
                    Data.Course.SurveyIDColumn.QualifiedName,
                    Data.Course.IsSurveyMandatoryColumn.QualifiedName,
                    Data.Course.OnlineCourseIDColumn.QualifiedName,
                    Data.Course.NumberOfDaysColumn.QualifiedName,
                    Data.Course.DailyDurationColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.OnlineCourse + "].[" + Data.OnlineCourse.Columns.Name + "] AS [OnlineCourseName]",
                    Data.Course.CourseCompletionTypeIDColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.CourseCompletionType + "].[" + Data.CourseCompletionType.Columns.Description + "] AS [CourseCompletionTypeDescription]",
                    Data.Course.PresentationTypeIDColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.PresentationType + "].[" + Data.PresentationType.Columns.Description + "] AS [PresentationTypeDescription]",
                    Data.Category.NameColumn.QualifiedName + " as CategoryName ",
                    Data.Course.CategoryIDColumn.QualifiedName,
                    Data.CategoryHierarchy.Columns.LevelIndentText,
                    Data.Course.CertificateEnabledColumn.QualifiedName,
                    Data.Course.CertificatePathColumn.QualifiedName,
                    Data.Course.MetaDataJsonColumn.QualifiedName,
                    Data.Course.CertificateTemplateIdColumn.QualifiedName,
                    Data.OnlineCourse.ProctorRequiredIndicatorColumn.QualifiedName
                ).From(Data.Tables.Course)
                .Where(Data.Course.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.Course.IdColumn).IsEqualTo(courseId)
                .LeftOuterJoin(Data.Tables.OnlineCourse, Data.OnlineCourse.Columns.Id, Data.Tables.Course, Data.Course.Columns.OnlineCourseID)
                .LeftOuterJoin(Data.Tables.CourseCompletionType, Data.CourseCompletionType.Columns.Id, Data.Tables.Course, Data.Course.Columns.CourseCompletionTypeID)
                .LeftOuterJoin(Data.Tables.PresentationType, Data.PresentationType.Columns.Id, Data.Tables.Course, Data.Course.Columns.PresentationTypeID)
                .LeftOuterJoin(Data.Category.Schema.TableName, Data.Category.Columns.Id, Data.Course.Schema.TableName, Data.Course.Columns.CategoryID)
                .ApplyLevelIndentText<Data.Category>(Data.Course.CategoryIDColumn);
                
            SingleResult<Course> result = new SingleResult<Course>(query);

            if (result.Data.SurveyID > 0)
            {
                result.Data.SurveyName = new Select(Data.OnlineCourse.NameColumn)
                    .From<Data.OnlineCourse>()
                    .Where(Data.OnlineCourse.IdColumn).IsEqualTo(result.Data.SurveyID)
                    .ExecuteScalar<string>();
            }

            result.Data.HasClasses = Select.AllColumnsFrom<Data.ClassX>()
                .Where(Data.ClassX.CourseIDColumn).IsEqualTo(result.Data.Id)
                .GetRecordCount() > 0;

            return result;
        }

        public List<Registration> GetRegistrationsForClass(int userId, int customerId, int classId)
        {
            return GetRegistrations(customerId, userId, classId.ToString(), Data.User.Columns.LastName, OrderDirection.Ascending, 0, int.MaxValue).Data.ToList();
        }

        public SingleResult<ClassroomClass> GetClass(int customerId, int userId, int classId) {
            return GetClass(customerId, userId, classId, false);
        }

        public SingleResult<ClassroomClass> GetClass(int customerId, int userId, int classId, bool ignoreRegistrations)
        {
            SqlQuery query = new Select(
                    Data.ClassX.AdditionalInstructionsColumn.QualifiedName,
                    Data.ClassX.CapacityOverrideColumn.QualifiedName,
                    Data.ClassX.DescriptionColumn.QualifiedName,
                    Data.ClassX.IdColumn.QualifiedName,
                    Data.ClassX.InternalCodeColumn.QualifiedName,
                    Data.ClassX.IsVirtualColumn.QualifiedName,
                    Data.ClassX.LockedIndicatorColumn.QualifiedName,
                    Data.ClassX.NameColumn.QualifiedName,
                    Data.ClassX.NotesColumn.QualifiedName,
                    Data.ClassX.ObjectiveColumn.QualifiedName,
                    Data.ClassX.SurveyIDColumn.QualifiedName,
                    Data.ClassX.CourseIDColumn.QualifiedName,
                    Data.ClassX.WebinarKeyColumn.QualifiedName,
                    Data.ClassX.ApprovalRequiredColumn.QualifiedName,
                    Data.ClassX.IsThirdPartyColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.Course + "].[" + Data.Course.Columns.Name + "] AS [CourseName]",
                    "[dbo].[" + Data.Tables.Course + "].[" + Data.Course.Columns.OnlineCourseID + "] AS [OnlineCourseID]",
                    "[dbo].[" + Data.Tables.Course + "].[" + Data.Course.Columns.CourseCompletionTypeID + "] AS [CourseCompletionTypeID]",
                    Data.ClassX.RoomIDColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.Room + "].[" + Data.Room.Columns.Name + "] AS [RoomName]",
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.Id + "] AS [VenueId]",
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.Name + "] AS [VenueName]",
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.TimeZoneID + "] AS [TimeZoneID]",
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.City + "] AS [VenueCity]",
                    "[dbo].[" + Data.Tables.State + "].[" + Data.State.Columns.Name + "] AS [StateName]",
                    "[dbo].[" + Data.Tables.TimeZone + "].[" + Data.TimeZone.Columns.Description + "] AS [TimeZoneDescription]",
                    Data.ClassX.MetaDataJsonColumn.QualifiedName,
                    Data.OnlineCourse.ProctorRequiredIndicatorColumn.QualifiedName,
                    Data.OnlineCourse.IsValidationEnabledColumn.QualifiedName,
                    Data.ClassX.IsAdminRegistrationColumn.QualifiedName
                )
                .From(Data.Tables.ClassX);
            // EXTREME DANGER! SALESFORCE HACK HACK HACK - SECURITY BREACHED!
            // I am temporarily removing the customer restriction here
            // TODO: Fix temporary constomer restriction.
            // - Problem is this: Training programs get copied to a new customer when they are 
            // purchased through salesforce. They still reference the courses from the original
            // customer. 
            // To make it not a crazy hack, I'm just going to open it up to users that 
            // have been created by salesforce. Otherwise leave it locked down
            // But this should be fixed so that salesforce users can't just access
            // any old class they want

            // SO: Get the user data
            Data.User userData = new Data.User(userId);

            if (userData.IsCreatedBySalesforce)
            {
                query.Where(Data.ClassX.IdColumn).IsEqualTo(classId);
            }
            else
            {
                query.Where(Data.ClassX.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.ClassX.IdColumn).IsEqualTo(classId);
            }

            query.LeftOuterJoin(Data.Tables.Course, Data.Course.Columns.Id, Data.Tables.ClassX, Data.ClassX.Columns.CourseID)
            .LeftOuterJoin(Data.Tables.OnlineCourse, Data.OnlineCourse.Columns.Id, Data.Tables.Course, Data.Course.Columns.OnlineCourseID)
            .LeftOuterJoin(Data.Tables.Room, Data.Room.Columns.Id, Data.Tables.ClassX, Data.ClassX.Columns.RoomID)
            .LeftOuterJoin(Data.Tables.Venue, Data.Venue.Columns.Id, Data.Tables.Room, Data.Room.Columns.VenueID)
            .LeftOuterJoin(Data.Tables.State, Data.State.Columns.Id, Data.Tables.Venue, Data.Venue.Columns.StateID)
            .LeftOuterJoin(Data.Tables.TimeZone, Data.TimeZone.Columns.Id, Data.Tables.Venue, Data.Venue.Columns.TimeZoneID);

            ClassroomClass cc = query.ExecuteSingle<ClassroomClass>();

            SqlQuery instructors = Select.AllColumnsFrom<Data.User>()
                .Where(Data.User.Columns.Id).In(new Select(Data.ClassInstructor.Columns.InstructorID)
                    .From<Data.ClassInstructor>()
                    .Where(Data.ClassInstructor.Columns.ClassID).IsEqualTo(classId))
                    .OrderAsc(Data.User.Columns.LastName, Data.User.Columns.FirstName);

            cc.ClassInstructorList = instructors.ExecuteTypedList<ClassInstructor>();

            FlagCollaborators(customerId, userId, cc.ClassInstructorList);

            SqlQuery resources = Select.AllColumnsFrom<Data.Resource>()
                .Where(Data.Resource.Columns.Id).In(new Select(Data.ClassResource.Columns.ResourceID)
                    .From<Data.ClassResource>()
                    .Where(Data.ClassResource.Columns.ClassID).IsEqualTo(classId))
                    .OrderAsc(Data.Resource.Columns.Name);

            cc.ResourceList = resources.ExecuteTypedList<ClassResource>();

            SqlQuery dates = Select.AllColumnsFrom<Data.ClassDate>()
                .Where(Data.ClassDate.Columns.ClassID).IsEqualTo(classId)
                .OrderAsc(Data.ClassDate.Columns.StartDateTime);

            Data.Course c = new Data.Course(Data.Course.Columns.Id, cc.CourseID);

            cc.DailyDuration = c.DailyDuration.Value;

            cc.DateList = dates.ExecuteTypedList<ClassDate>();
            cc.NumberOfDays = cc.DateList.Count();

            // if online test, is user validation required?
            if (cc.OnlineCourseID > 0)
            {
                cc.IsUserValidationAllowed = HasValidationParameters || (!cc.IsUserValidationAllowed);
            }
            else
            {
                cc.IsUserValidationAllowed = true;
            }

            // in the db, everything is stored in universal time
            // however, when we load the date time, it doesn't know that, so it uses local timezone info
            // that makes the JSON serializer tacks on the -400 (or whatever) so we convert to universal time,
            // then substract the offset to make everyone happy            
            cc.DateList.ForEach(d => d.StartDateTime = d.StartDateTime.WithUtcFlag());


            // Only return student data to roles that should see it
            if (
                    !ignoreRegistrations && 
                    (
                        HasRole(Roles.ClassroomInstructor) ||
                        HasRole(Roles.ClassroomManager) ||
                        HasRole(Roles.ClassroomSupervisor) ||
                        HasRole(Roles.CourseAssignmentAdministrator) ||
                        HasRole(Roles.CourseAssignmentTrainingManager)
                    )
                )
            {
                cc.RegistrationList = GetRegistrationsForClass(userId, customerId, cc.Id); 
            }

            //int totalRegistered = cc.RegistrationList.Count(reg => reg.RegistrationStatusID == (int)RegistrationStatusType.Registered);

            // if class is at capacity and is "locked", registration is closed
            //cc.RegistrationClosed = ((totalRegistered >= cc.CapacityOverride) && cc.LockedIndicator);

            if (cc.OnlineCourseID > 0)
            {
                // and match them to the registrations
                if (cc.RegistrationList != null)
                {
                    // get a list of all the scores for this course id
                    List<OnlineCourseRegistration> scores = new Select(Data.OnlineCourseRollup.ScoreColumn, Data.OnlineCourseRollup.UserIDColumn)
                            .From(Data.Tables.OnlineCourseRollup)
                            .Where(Data.OnlineCourseRollup.CourseIDColumn).IsEqualTo(cc.OnlineCourseID)
                            .ExecuteTypedList<OnlineCourseRegistration>();

                    foreach (Registration reg in cc.RegistrationList)
                    {
                        OnlineCourseRegistration rollup = scores.FirstOrDefault(score => score.UserID == reg.RegistrantID);
                        if (rollup != null)
                        {
                            reg.Score = rollup.Score.ToString();
                        }
                    }
                }
            }

            return new SingleResult<ClassroomClass>(cc);
        }

        public SingleResult<Venue> GetVenue(int customerId, int userId, int venueId)
        {
            SqlQuery query = new Select(
                    Data.Venue.CityColumn.QualifiedName,
                    Data.Venue.IdColumn.QualifiedName,
                    Data.Venue.InternalCodeColumn.QualifiedName,
                    Data.Venue.NameColumn.QualifiedName,
                    Data.Venue.StateIDColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.State + "].[" + Data.State.Columns.Name + "] AS [StateName]",
                    "[dbo].[" + Data.Tables.State + "].[" + Data.State.Columns.Description + "] AS [StateDescription]",
                    Data.Venue.TimeZoneIDColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.TimeZone + "].[" + Data.TimeZone.Columns.Description + "] AS [TimeZoneDescription]"
                )
                .From(Data.Tables.Venue)
                .Where(Data.Venue.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.Venue.IdColumn).IsEqualTo(venueId)
                .LeftOuterJoin(Data.Tables.State, Data.State.Columns.Id, Data.Tables.Venue, Data.Venue.Columns.StateID)
                .LeftOuterJoin(Data.Tables.TimeZone, Data.TimeZone.Columns.Id, Data.Tables.Venue, Data.Venue.Columns.TimeZoneID);

            SqlQuery resourceManagers = new Select(
                    Data.VenueResourceManager.IdColumn,
                    Data.VenueResourceManager.VenueIDColumn,
                    Data.VenueResourceManager.ResourceManagerIDColumn,
                    Data.User.FirstNameColumn,
                    Data.User.MiddleNameColumn,
                    Data.User.LastNameColumn
                ).From(Data.Tables.VenueResourceManager)
                .LeftOuterJoin(Data.Tables.User, Data.User.Columns.Id, Data.Tables.VenueResourceManager, Data.VenueResourceManager.Columns.ResourceManagerID)
                .Where(Data.User.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.VenueResourceManager.VenueIDColumn).IsEqualTo(venueId)
                .OrderAsc(Data.User.Columns.LastName);

            SqlQuery resources = Select.AllColumnsFrom<Data.Resource>()
                .Where(Data.Resource.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.Resource.Columns.VenueID).IsEqualTo(venueId)
                .OrderAsc(Data.Resource.Columns.Name);

            SqlQuery rooms = Select.AllColumnsFrom<Data.Room>()
                .Where(Data.Room.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.Room.Columns.VenueID).IsEqualTo(venueId)
                .OrderAsc(Data.Room.Columns.Name);

            // load the venue
            Venue venue = query.ExecuteSingle<Venue>();
            if (venue == null)
            {
                throw new Exception("Invalid Venue ID specified.");
            }

            // load the supporting data
            venue.Rooms = rooms.ExecuteTypedList<Room>();
            venue.ResourceManagers = resourceManagers.ExecuteTypedList<VenueResourceManager>();
            venue.Resources = resources.ExecuteTypedList<Resource>();

            return new SingleResult<Venue>(venue);
        }

        public SingleResult<Resource> GetResource(int customerId, int userId, int resourceId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Resource>()
                .Where(Data.Room.Columns.CustomerID).IsEqualTo(customerId);

            return new SingleResult<Resource>(query);
        }

        public SingleResult<Room> GetRoom(int customerId, int userId, int roomId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Room>()
                .Where(Data.Room.Columns.CustomerID).IsEqualTo(customerId);

            return new SingleResult<Room>(query);
        }

        public SingleResult<CalendarClass> SaveCalendarClass(int customerId, int userId, CalendarClass klass)
        {
            // get all class dates for this class
            SqlQuery query = Select.AllColumnsFrom<Data.ClassDate>()
                .Where(Data.ClassDate.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.ClassDate.Columns.ClassID).IsEqualTo(klass.ClassID);
            List<Data.ClassDate> classDates = query.ExecuteTypedList<Data.ClassDate>();

            // find the class date that matches the submitted class
            DateTime? oldStartDateTime = null;
            foreach (Data.ClassDate classDate in classDates)
            {
                if (classDate.Id == klass.ClassDateID)
                {
                    oldStartDateTime = classDate.StartDateTime;
                }
            }

            // bad data
            if (!oldStartDateTime.HasValue)
            {
                return new SingleResult<CalendarClass>(new Exception("Invalid class/class date."));
            }

            // calculate the number of days that were added or subtracted from the submitted
            // class's start date
            int days = (klass.StartDateTime - oldStartDateTime.Value).Days;

            // modify each class by that number of days
            foreach (Data.ClassDate classDate in classDates)
            {
                classDate.StartDateTime = classDate.StartDateTime.AddDays(days);

                // and save
                classDate.Save(Username);
            }

            //klass.StartDateTime = klass.StartDateTime.AddDays(milliChange);

            return new SingleResult<CalendarClass>(klass);
        }

        public SingleResult<Course> SaveCourse(int customerId, int userId, int courseId, Course course)
        {
            Data.Course data = new Data.Course(courseId); 

            if (data.CourseCompletionTypeID != course.CourseCompletionTypeID)
            {
                if (data.ClassXes.Count > 0)
                {
                    throw new Exception("The completion type cannot be changed once classes have been created for the course.");
                }
            }

            if (courseId > 0 && course.PublicIndicator)
            {
                // if the course is in a training program, and they're trying to make it public, fail it
                if (Select.AllColumnsFrom<Data.TrainingProgramToCourseLink>()
                    .Where(Data.TrainingProgramToCourseLink.Columns.CourseID).IsEqualTo(courseId)
                    .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo((int)CourseType.Classroom)
                    .GetRecordCount() > 0)
                {
                    throw new Exception("Courses cannot be both public and in a training program. You must remove this course from all training programs to make it public.");
                }
            }

            course.CopyTo(data);
            data.Id = courseId;
            data.CustomerID = customerId;
            data.CategoryID = course.CategoryId;

            data.Save(Username);

            return new SingleResult<Course>(Model.Create<Course>(data));
        }

        private Data.RegistrationCollection SaveRegistrations(int classId, List<Data.Registration> currentRegistrations, ClassroomClass klass, Data.Course course)
        {
            Data.RegistrationCollection newRegistrationList = new Data.RegistrationCollection();
            Dictionary<int, string> existingScores = new Dictionary<int, string>();
            List<int> userIdsForRollup = new List<int>();

            // Look up the existing registrations
            Data.ClassX existingClass = new Data.ClassX(classId);
            if (existingClass != null)
            {
                existingScores = existingClass.Registrations.ToDictionary(k => k.RegistrantID, v => v.Score);
            }

            foreach (Registration reg in klass.RegistrationList)
            {
                Data.Registration existingRegistration = Select.AllColumnsFrom<Data.Registration>()
                    .Where(Data.Registration.ClassIDColumn).IsEqualTo(reg.ClassID)
                    .And(Data.Registration.RegistrantIDColumn).IsEqualTo(reg.RegistrantID)
                    .ExecuteSingle<Data.Registration>();

                Data.Registration newRegistration = new Symphony.Core.Data.Registration();

                if (!string.IsNullOrEmpty(reg.Score))
                {
                    decimal d; char c;
                    if (course.CourseCompletionTypeID == (int)CourseCompletionType.NumericGrade)
                    {
                        if (!decimal.TryParse(reg.Score, out d) || !(d >= 0 && d <= 100))
                        {
                            throw new Exception("Score must be a number between 0 and 100.");
                        }
                    }
                    else if (course.CourseCompletionTypeID == (int)CourseCompletionType.LetterGrade)
                    {
                        if (!char.TryParse(reg.Score, out c) || !((c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')))
                        {
                            throw new Exception("Score must be a letter value from A to F.");
                        }
                    }
                }
                else
                {
                    reg.Score = string.Empty;
                }

                // Training program rollup rules:
                //  1. When there is an existing registration, and the score has changed
                //  2. When there is a new registration, and the new registration has a score
                //  3. When the registration status has either changed to Registered, or changed from Registered
                //  4. When there is a new registration, and the attendance indicator is true
                //  5. When the registration attendance status changes and the score type is attendance
                //  6. When the attendance status changes, and there is a score
                bool isScoreBasedCompletion = course.CourseCompletionTypeID == (int)CourseCompletionType.LetterGrade ||
                                              course.CourseCompletionTypeID == (int)CourseCompletionType.NumericGrade;

                // 1
                bool isScoreChange = existingRegistration != null && existingRegistration.Score != reg.Score && isScoreBasedCompletion;
                // 2
                bool isNewRegistrationWithScore = existingRegistration == null && !string.IsNullOrEmpty(reg.Score) && isScoreBasedCompletion;
                // 3
                bool isStatusChange = existingRegistration != null &&
                    ((existingRegistration.RegistrationStatusID == (int)RegistrationStatusType.Registered && reg.RegistrationStatusID != (int)RegistrationStatusType.Registered) ||
                    (existingRegistration.RegistrationStatusID != (int)RegistrationStatusType.Registered && reg.RegistrationStatusID == (int)RegistrationStatusType.Registered));
                // 4
                bool isNewRegistrationWithAttendance = existingRegistration == null && reg.AttendedIndicator == true && course.CourseCompletionTypeID == (int)CourseCompletionType.Attendance;
                // 5
                bool isRegistrationAttendanceChange = existingRegistration != null && existingRegistration.AttendedIndicator != reg.AttendedIndicator && course.CourseCompletionTypeID == (int)CourseCompletionType.Attendance;
                // 6
                bool isRegistrationAttendanceWithScoreChange = existingRegistration != null && existingRegistration.AttendedIndicator != reg.AttendedIndicator && !string.IsNullOrEmpty(reg.Score);

                if (isScoreChange || 
                    isNewRegistrationWithScore || 
                    isStatusChange || 
                    isNewRegistrationWithAttendance || 
                    isRegistrationAttendanceChange || 
                    isRegistrationAttendanceWithScoreChange)
                {
                    userIdsForRollup.Add(reg.RegistrantID);
                }

                reg.CopyTo(newRegistration);

                if (existingRegistration != null)
                {
                    newRegistration.WebinarRegistrationID = existingRegistration.WebinarRegistrationID;
                }

                newRegistration.ClassID = classId;
                newRegistration.RegistrationTypeID = (int)RegistrationType.Manual;
                newRegistrationList.Add(newRegistration);
            }

            // kill the old ones, save the new ones
            Data.Registration.Destroy(Data.Registration.Columns.ClassID, classId);
            newRegistrationList.SaveAll(Username);

            // check if this is a class used for a training program session
            bool isSession = new Select(Data.TrainingProgram.IdColumn)
                .From<Data.TrainingProgram>()
                .InnerJoin(Data.Course.IdColumn, Data.TrainingProgram.SessionCourseIDColumn)
                .Where(Data.Course.IdColumn).IsEqualTo(klass.CourseID)
                .ExecuteSingle<int>() > 0;

            // Rollup any training programs for this course for all users in the course.
            // only if this isn't a session course since there really isn't a point in triggering
            // a session rollup.
            if (!isSession && userIdsForRollup.Count > 0)
            {
                new RollupController().EnqueueTrainingProgramRollup(userIdsForRollup, course.Id, CourseType.Classroom);
            }

            return newRegistrationList;
        }

        private void NotifyRegistrationChange(List<Data.Registration> currentRegistrations, Data.RegistrationCollection newRegistrationList, Data.ClassX data)
        {
            List<NotificationDate> notificationDates = new List<NotificationDate>();
            for (var i = 0; i < data.ClassDates.Count(); i++)
            {
                var cd = data.ClassDates[i];

                notificationDates.Add(new NotificationDate()
                {
                    StartDate = cd.StartDateTime,
                    EndDate = cd.StartDateTime.AddMinutes((double)cd.Duration),
                    Type = CalendarType.Timed,
                    Method = CalendarMethod.Update,
                    InstanceID = data.Id + "-" + i,
                    Name = data.Name + " (Class " + (i + 1) + " of " + data.ClassDates.Count() + ")"
                });
            }

            // if the current registration has them un-attended, but the new registration has them attended,
            // then send a notification
            foreach (Data.Registration registration in currentRegistrations)
            {
                Data.User user = new Symphony.Core.Data.User(registration.RegistrantID);
                Data.Registration newRegistration = newRegistrationList.FirstOrDefault(r => r.RegistrantID == registration.RegistrantID);
                if (registration.AttendedIndicator == false && newRegistration != null && newRegistration != default(Data.Registration) && newRegistration.AttendedIndicator == true)
                {
                    NotificationController.SendNotification(NotificationType.MarkedAsAttended, new NotificationOptions()
                    {
                        TemplateObjects = new object[] { user, newRegistration, data },
                        NotificationDates = notificationDates.ToArray()
                    });
                }

                if (newRegistration != null && newRegistration != default(Data.Registration) && newRegistration.RegistrationStatusID != registration.RegistrationStatusID)
                {
                    NotificationController.SendNotification(NotificationType.RegistrationStatusChanged, new NotificationOptions()
                    {
                        TemplateObjects = new object[] { user, newRegistration, data },
                        NotificationDates = notificationDates.ToArray()
                    });
                }

                if (registration.CanMoveForwardIndicator == false && newRegistration != null && newRegistration != default(Data.Registration) && newRegistration.CanMoveForwardIndicator == true)
                {
                    Course course = new Course();
                    course.CopyFrom(data);

                    NotificationController.SendNotification(NotificationType.InstructorPermissionGranted, new NotificationOptions()
                    {
                        TemplateObjects = new object[] { user, course },
                        NotificationDates = notificationDates.ToArray()
                    });
                }
            }

            // any new registrations get a changed status too
            foreach (Data.Registration registration in newRegistrationList)
            {
                if (!currentRegistrations.Any(r => r.RegistrantID == registration.RegistrantID))
                {
                    Data.User user = new Symphony.Core.Data.User(registration.RegistrantID);
                    
                    NotificationController.SendNotification(NotificationType.RegistrationStatusChanged, new NotificationOptions()
                    {
                        TemplateObjects = new object[] { user, registration, data },
                        NotificationDates = notificationDates.ToArray()
                    });
                }
            }
        }

        public SingleResult<ClassroomClass> UpdateRegistrations(int customerId, int userId, int classId, ClassroomClass klass)
        {
            Data.ClassX data = new Data.ClassX(classId);
            Data.Course course = new Data.Course(data.CourseID);
            List<Data.Registration> currentRegistrations = Select.AllColumnsFrom<Data.Registration>()
                .Where(Data.Registration.Columns.ClassID).IsEqualTo(data.Id)
                .ExecuteTypedList<Data.Registration>();

            Data.RegistrationCollection newRegistrations = SaveRegistrations(classId, currentRegistrations, klass, course);

            NotifyRegistrationChange(currentRegistrations, newRegistrations, data);

            return GetClass(customerId, userId, data.Id);
        }

        public SingleResult<ClassroomClass> SaveClass(int customerId, int userId, int classId, ClassroomClass klass)
        {
            SingleResult<ClassroomClass> result;
            Data.ClassX data;

            TransactionOptions options = new TransactionOptions()
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = new TimeSpan(0, 0, 300)
            };


            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, options, EnterpriseServicesInteropOption.Automatic))
            {
                if (klass.IsVirtual && klass.ClassInstructorList.Count > 0)
                {
                    // get all the GTM/GTW organizers
                    List<User> users = UserController.GetUsersByRole(customerId, userId, Roles.GTWOrganizer, "");
                    users.AddRange(UserController.GetUsersByRole(customerId, userId, Roles.GTMOrganizer, ""));

                    if (!klass.ClassInstructorList.Any(ci =>
                    {
                        return users.Any(u => u.ID == ci.InstructorID);
                    }))
                    {
                        throw new Exception("You must have at least one instructor with the ability to host virtual classes.");
                    }

                }

                data = new Data.ClassX(classId);

                // grab the corresponding course so we can check the completion type
                Data.Course course = new Data.Course(klass.CourseID);

                bool roomChanged = (klass.RoomID != data.RoomID);
                bool isNew = (classId == 0);
                bool scheduleChanged = false;

                klass.CopyTo(data);
                data.Id = classId;
                data.CustomerID = customerId;
                if (data.Notes == null)
                {
                    data.Notes = "";
                }


                if (course.SurveyID > 0)
                {
                    // We want a unique survey for each class instance
                    if (data.SurveyID == 0) // No survey for this class yet, create a new one based on the course reference
                    {
                        data.SurveyID = CourseController.DuplicateSurvey(course.SurveyID, CustomerID, Username).Id;
                    }
                    else
                    {
                        // We have a survey for this class, make sure it matches up with the course survey
                        Data.OnlineCourse currentSurvey = new Data.OnlineCourse(data.SurveyID);
                        if (currentSurvey.DuplicateFromID != course.SurveyID)
                        {
                            // Since it doesn't, make a duplicate for it
                            data.SurveyID = CourseController.DuplicateSurvey(course.SurveyID, CustomerID, Username).Id;
                        }
                    }
                }

                data.Save(Username);

                // get the existing list of instructors
                List<Data.ClassInstructor> currentInstructors = Select.AllColumnsFrom<Data.ClassInstructor>()
                    .Where(Data.ClassInstructor.Columns.ClassID).IsEqualTo(data.Id)
                    .ExecuteTypedList<Data.ClassInstructor>();


                // kill the existing instructor list
                List<int> instructorsAdded = new List<int>();
                Data.ClassInstructor.Destroy(Data.ClassInstructor.Columns.ClassID, data.Id);
                foreach (ClassInstructor ci in klass.ClassInstructorList)
                {
                    Data.ClassInstructor dataCI = new Symphony.Core.Data.ClassInstructor();
                    ci.CopyTo(dataCI);
                    dataCI.ClassID = data.Id;
                    dataCI.Save(Username);

                    // if the instructor we just saved didn't exist in the list already, send a message
                    if (!currentInstructors.Any(c => c.InstructorID == ci.InstructorID))
                    {
                        instructorsAdded.Add(ci.InstructorID);
                    }
                }


                List<int> resourcesRequested = new List<int>();

                // get the existing list of resources
                List<Data.ClassResource> currentResources = Select.AllColumnsFrom<Data.ClassResource>()
                    .Where(Data.ClassResource.Columns.ClassID).IsEqualTo(data.Id)
                    .ExecuteTypedList<Data.ClassResource>();

                Data.ClassResource.Destroy(Data.ClassResource.Columns.ClassID, data.Id);
                foreach (ClassResource cr in klass.ResourceList)
                {
                    Data.ClassResource dataCR = new Symphony.Core.Data.ClassResource();
                    cr.CopyTo(dataCR);
                    dataCR.ClassID = data.Id;
                    dataCR.Save(Username);

                    // if the resource we just saved didn't exist in the list already, send a message
                    if (!currentResources.Any(c => c.ResourceID == cr.ResourceID))
                    {
                        resourcesRequested.Add(cr.ResourceID);
                    }
                }

                List<Data.Registration> currentRegistrations = Select.AllColumnsFrom<Data.Registration>()
                    .Where(Data.Registration.Columns.ClassID).IsEqualTo(data.Id)
                    .ExecuteTypedList<Data.Registration>();

                Data.RegistrationCollection newRegistrationList = SaveRegistrations(data.Id, currentRegistrations, klass, course);

                // update the class dates
                List<ClassDate> dates = Select.AllColumnsFrom<Data.ClassDate>()
                    .Where(Data.ClassDate.Columns.ClassID).IsEqualTo(classId)
                    .ExecuteTypedList<ClassDate>();


                if (dates.Count == klass.DateList.Count)
                {
                    foreach (ClassDate date in dates)
                    {
                        // check each existing date by comparing it to what
                        // was submitted - if the submitted list does not contain
                        // a date that matches the existing date, then the schedule
                        // has indeed changed
                        if (!klass.DateList.Any(
                            x => x.StartDateTime.Year == date.StartDateTime.Year
                                && x.StartDateTime.Month == date.StartDateTime.Month
                                && x.StartDateTime.Day == date.StartDateTime.Day
                                && x.StartDateTime.Hour == date.StartDateTime.Hour
                                && x.StartDateTime.Minute == date.StartDateTime.Minute))
                        {
                            scheduleChanged = true;
                            break;
                        }
                    }
                }
                else
                {
                    scheduleChanged = true;
                }

                Data.ClassDate.Destroy(Data.ClassDate.Columns.ClassID, data.Id);
                foreach (ClassDate cd in klass.DateList)
                {
                    Data.ClassDate dataCD = new Data.ClassDate();
                    cd.CopyTo(dataCD);
                    dataCD.CustomerID = customerId;
                    dataCD.ClassID = data.Id;
                    dataCD.Save(Username);
                }

                // check for any conflicts.
                List<string> warnings = CheckForConflicts(klass.Id);

                // send notifications
                // if it's a third party (quick add class), no notifications get sent
                if (!data.IsThirdParty)
                {
                    // used to send messages only to registered users
                    Func<List<int>, List<int>> registeredFilter = (users) =>
                    {

                        List<Data.Registration> classRegs = Select.AllColumnsFrom<Data.Registration>()
                            .Where(Data.Registration.Columns.ClassID).IsEqualTo(data.Id)
                            .ExecuteTypedList<Data.Registration>();

                        List<int> filtered = new List<int>();
                        foreach (int user in users)
                        {
                            Data.Registration reg = classRegs.SingleOrDefault(r => r.RegistrantID == user);

                            if (reg != null)
                            {
                                // only keep the user if they have Registered status
                                if (reg.RegistrationStatusID == (int)RegistrationStatusType.Registered)
                                    filtered.Add(user);
                            }
                            else
                            {
                                // if no registration is found, we need to still keep the user because they were added for a different reason
                                filtered.Add(user);
                            }
                        }
                        return filtered;
                    };

                    List<NotificationDate> notificationDates = new List<NotificationDate>();
                    for (var i = 0; i < klass.DateList.Count(); i++)
                    {
                        var cd = klass.DateList[i];

                        notificationDates.Add(new NotificationDate()
                        {
                            StartDate = cd.StartDateTime,
                            EndDate = cd.StartDateTime.AddMinutes((double)cd.Duration),
                            Type = CalendarType.Timed,
                            InstanceID = klass.Id + "-" + i,
                            Name = klass.Name + " (Class " + (i + 1) + " of " + klass.DateList.Count() + ")"
                        });
                    }

                    if (roomChanged && !isNew)
                    {
                        notificationDates.ForEach(c => c.Method = CalendarMethod.Update);

                        NotificationController.SendNotification(NotificationType.ClassRoomChange, new NotificationOptions()
                        {
                            UserModifier = registeredFilter,
                            TemplateObjects = new object[] { data },
                            NotificationDates = notificationDates.ToArray()
                        });
                    }

                    if (scheduleChanged && !isNew)
                    {
                        notificationDates.ForEach(c =>
                        {
                            c.Method = CalendarMethod.Update;
                        });

                        NotificationController.SendNotification(NotificationType.SchedulingChange, new NotificationOptions()
                        {
                            UserModifier = registeredFilter,
                            TemplateObjects = new object[] { data },
                            NotificationDates = notificationDates.ToArray()
                        });
                    }

                    if (isNew)
                    {
                        notificationDates.ForEach(c => c.Method = CalendarMethod.Create);

                        NotificationController.SendNotification(NotificationType.NewClassScheduled, new NotificationOptions()
                        {
                            UserModifier = registeredFilter,
                            TemplateObjects = new object[] { data },
                            NotificationDates = notificationDates.ToArray()
                        });
                    }

                    NotifyRegistrationChange(currentRegistrations, newRegistrationList, data);

                    // get the list of instructors removed and send notifications
                    List<Data.ClassInstructor> removed = currentInstructors.FindAll(ci => !klass.ClassInstructorList.Any(incoming => incoming.InstructorID == ci.InstructorID));
                    foreach (Data.ClassInstructor ci in removed)
                    {
                        notificationDates.ForEach(n => n.Method = CalendarMethod.Delete);

                        Data.User user = new Data.User(ci.InstructorID);
                        NotificationController.SendNotification(NotificationType.InstructorRemoved, new NotificationOptions()
                        {
                            TemplateObjects = new object[] { user, data },
                            NotificationDates = notificationDates.ToArray()
                        });
                    }

                    // added instructors
                    foreach (int i in instructorsAdded)
                    {
                        notificationDates.ForEach(n => n.Method = CalendarMethod.Create);

                        Data.User user = new Data.User(i);
                        NotificationController.SendNotification(NotificationType.InstructorAdded, new NotificationOptions()
                        {
                            TemplateObjects = new object[] { user, data },
                            NotificationDates = notificationDates.ToArray()
                        });
                    }

                    // added resources
                    foreach (int i in resourcesRequested)
                    {
                        Data.Resource r = new Data.Resource(i);
                        NotificationController.SendNotification(NotificationType.ResourceRequested, new object[] { r, data });
                    }
                }

                result = GetClass(customerId, userId, data.Id);
                if (warnings.Count > 0)
                {
                    result.Notice = "The registration request has been processed, but conflicts exist:<br/><br/>" + string.Join("<br/>", warnings.ToArray());
                }

                ts.Complete();
            }

            // Try to create a message board
            if (klass.IsCreateMessageBoard)
            {
                try
                {
                    MessageBoardController messageBoardController = new MessageBoardController();
                    SingleResult<MessageBoard> messageBoard = messageBoardController.GetMessageBoard(CustomerID, UserID, data.Id, MessageBoardType.Classroom);
                    if (messageBoard.Data.ID == 0)
                    {
                        MessageBoard mb = new MessageBoard();
                        mb.ClassId = data.Id;
                        mb.Name = String.IsNullOrEmpty(klass.MessageBoardName) ? klass.Name : klass.MessageBoardName;
                        mb.MessageBoardType = MessageBoardType.Classroom;
                        mb.IsDisableTopicCreation = klass.IsDisableTopicCreation;

                        messageBoardController.UpdateMessageBoard(CustomerID, UserID, 0, mb);
                    }
                }
                catch (Exception e)
                {
                    if (!String.IsNullOrEmpty(result.Notice))
                    {
                        result.Notice += "<br/><br/>";
                    }
                    else
                    {
                        result.Notice = "";
                    }
                    result.Notice += "The class was saved, but the message board could not be created. You will need to create the message board at the message board tab, or ask an administrator for assistance.";
                    result.Notice += "<br/><br/>The error was: \"" + e.Message + "\"";
                }
            }

            return result;
        }

        /// <summary>
        /// Checks a class for conflicts with any of the registered users and returns a list of warnings
        /// </summary>
        /// <param name="classId">The class to check</param>
        /// <returns>A list of warnings</returns>
        public List<string> CheckForConflicts(int classId)
        {
            // select all the users currently registered for this class
            List<Data.User> registrants = null;
            SqlQuery query = Select.AllColumnsFrom<Data.User>()
                    .InnerJoin(Data.Registration.RegistrantIDColumn, Data.User.IdColumn)
                    .Where(Data.Registration.ClassIDColumn).IsEqualTo(classId)
                    .And(Data.Registration.RegistrationStatusIDColumn).IsEqualTo((int)RegistrationStatusType.Registered)
                    .Distinct();

            registrants = query.ExecuteTypedList<Data.User>();

            return CheckForConflicts(classId, registrants);
        }

        /// <summary>
        /// Checks a class for conflicts with any of the specified users and returns a list of warnings
        /// </summary>
        /// <param name="klass">The class to check</param>
        /// <param name="registrants">The registrants to check</param>
        /// <returns>A list of warnings</returns>
        public List<string> CheckForConflicts(int classId, List<Data.User> registrants)
        {
            List<string> warnings = new List<string>();

            // get the incoming class start and end dates, in UTC
            Data.ClassDateCollection newClassDates = new Data.ClassDateCollection().Where(Data.ClassDate.ClassIDColumn.ColumnName, classId).Load();

            SqlQuery query = null;

            if (registrants.Count == 0)
            {
                // empty result
                return warnings;
            }

            query = Select.AllColumnsFrom<Data.Registration>()
                .Where(Data.Registration.ClassIDColumn).IsNotEqualTo(classId)
                .And(Data.Registration.RegistrantIDColumn).In(registrants.Select(r => r.Id));
            List<Data.Registration> otherRegistrations = query.ExecuteTypedList<Data.Registration>();

            // for each user, get their list of classes, excluding the current one
            List<Data.ClassX> otherClasses = new List<Data.ClassX>();

            foreach (var registrantsSubList in registrants.InGroupsOf(2000))
            {
                otherClasses.AddRange(Select.AllColumnsFrom<Data.ClassX>()
                    .InnerJoin(Data.Registration.ClassIDColumn, Data.ClassX.IdColumn)
                    .Where(Data.Registration.RegistrantIDColumn).In(registrantsSubList.Select(r => r.Id))
                    .And(Data.ClassX.Columns.Id).IsNotEqualTo(classId)
                    .ExecuteTypedList<Data.ClassX>());
            }

            if (otherClasses.Count == 0)
            {
                // empty result
                return warnings;
            }

            // for all the classes, get the class dates
            List<Data.ClassDate> allClassDates = new List<Data.ClassDate>();
            foreach (var otherClassesSubList in otherClasses.InGroupsOf(2000))
            {
                allClassDates.AddRange(Select.AllColumnsFrom<Data.ClassDate>()
                    .Where(Data.ClassDate.ClassIDColumn).In(otherClassesSubList.Select(c => c.Id))
                    .ExecuteTypedList<Data.ClassDate>());
            }

            // ok, at this point we've loaded up a crapload of data. time to do some checking

            // run through each registrant
            foreach (Data.User user in registrants)
            {
                // get the list of registrations for this user
                List<Data.Registration> userOtherRegistrations = otherRegistrations.Where(r => r.RegistrantID == user.Id).ToList();

                // get the list of classes for each registration
                List<Data.ClassX> userOtherClasses = otherClasses.Where(c => userOtherRegistrations.Any(uor => uor.ClassID == c.Id)).ToList();

                // for each class, check the date range
                foreach (Data.ClassX usersClass in userOtherClasses)
                {
                    List<Data.ClassDate> datesBusy = allClassDates.Where(cd => cd.ClassID == usersClass.Id).ToList();

                    foreach (Data.ClassDate date in datesBusy)
                    {
                        DateTime dtStart = date.StartDateTime.WithUtcFlag();
                        DateTime dtEnd = date.StartDateTime.AddMinutes(date.Duration.Value).WithUtcFlag();

                        if (newClassDates.Overlaps(dtStart, dtEnd))
                        {
                            warnings.Add(string.Format("\"{0}\" is registered to attend \"{1}\" on the selected date.",
                                Model.FormatFullName(user.FirstName, user.MiddleName, user.LastName),
                                usersClass.Name));
                        }
                    }
                }
            }

            return warnings.Distinct().ToList();
        }

        public SingleResult<Venue> SaveVenue(int customerId, int userId, int venueId, Venue venue)
        {
            Data.Venue data = new Data.Venue(venueId);
            if (data.Id > 0 && data.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to edit the Venue with id " + venueId);
            }
            venue.CopyTo(data);
            data.Id = venueId;
            data.CustomerID = customerId;
            data.Save(Username);

            // destroy and rebuild rooms, resources, and resource managers
            //Data.RoomCollection currentRooms = new Symphony.Core.Data.RoomCollection().Where(Data.Room.Columns.VenueID, venueId);
            //currentRooms.Load();

            //foreach (Room room in venue.Rooms)
            //{
            //    Data.Room dataRoom = new Data.Room();

            //    // load up the inital set of classes
            //    Data.ClassXCollection classes = new Data.ClassXCollection().Where(Data.ClassX.Columns.RoomID, dataRoom.Id);
            //    classes.Load();

            //    // create the new room
            //    room.CopyTo(dataRoom);
            //    dataRoom.CustomerID = customerId;
            //    dataRoom.VenueID = data.Id;

            //    // save the new room
            //    dataRoom.Save(Username);

            //    // re-reference the classes
            //    foreach (Data.ClassX c in classes)
            //    {
            //        c.RoomID = dataRoom.Id;
            //        c.Save(Username);
            //    }
            //}
            //// kill the old references
            //foreach (Data.Room room in currentRooms)
            //{
            //    Data.Room.Destroy(room.Id);
            //}


            // delete the removed rooms
            SqlQuery query = new SqlQuery().From<Data.Room>().Where(Data.Room.Columns.Id)
                .NotIn(venue.Rooms.Count > 0 ? venue.Rooms.Select(r => r.Id) : new int[] { 0 })
                .And(Data.Room.VenueIDColumn).IsEqualTo(venueId);
            query.QueryCommandType = SqlQuery.QueryType.Delete;
            query.Execute();

            // save the new rooms
            Data.RoomCollection dataRooms = new Data.RoomCollection();
            foreach (Room room in venue.Rooms)
            {
                Data.Room dataRoom = null;
                if (room.Id > 0)
                {
                    dataRoom = new Data.Room(room.Id);
                }
                else
                {
                    dataRoom = new Data.Room();
                }

                room.CopyTo(dataRoom);
                dataRoom.CustomerID = customerId;
                dataRoom.VenueID = data.Id;
                dataRooms.Add(dataRoom);
            }
            dataRooms.SaveAll(Username);

            // delete the removed resources
            query = new SqlQuery().From<Data.Resource>().Where(Data.Resource.Columns.Id)
                .NotIn(venue.Resources.Count > 0 ? venue.Resources.Select(r => r.Id) : new int[] { 0 })
                .And(Data.Resource.VenueIDColumn).IsEqualTo(venueId);
            query.QueryCommandType = SqlQuery.QueryType.Delete;
            query.Execute();

            // save the new resources
            Data.ResourceCollection dataResources = new Data.ResourceCollection();
            foreach (Resource resource in venue.Resources)
            {
                Data.Resource dataResource = null;
                if (resource.Id > 0)
                {
                    dataResource = new Data.Resource(resource.Id);
                }
                else
                {
                    dataResource = new Data.Resource();
                }

                resource.CopyTo(dataResource);
                dataResource.CustomerID = customerId;
                dataResource.VenueID = data.Id;
                dataResources.Add(dataResource);
            }
            dataResources.SaveAll(Username);



            // delete the removed venue resource managers
            query = new SqlQuery().From<Data.VenueResourceManager>().Where(Data.VenueResourceManager.Columns.ResourceManagerID)
                .NotIn(venue.ResourceManagers.Count > 0 ? venue.ResourceManagers.Select(r => r.ResourceManagerID) : new int[] { 0 })
                .And(Data.VenueResourceManager.VenueIDColumn).IsEqualTo(venueId);

            query.QueryCommandType = SqlQuery.QueryType.Delete;
            query.Execute();


            // save the new
            Data.VenueResourceManagerCollection dataManagers = new Data.VenueResourceManagerCollection();
            foreach (VenueResourceManager vrm in venue.ResourceManagers)
            {
                Data.VenueResourceManager dataManager = null;
                if (vrm.Id > 0)
                {
                    dataManager = new Data.VenueResourceManager(Data.VenueResourceManager.Columns.Id, vrm.Id);
                }
                else
                {
                    dataManager = new Data.VenueResourceManager();
                }
                vrm.CopyTo(dataManager);
                dataManager.VenueID = data.Id;
                dataManagers.Add(dataManager);
            }
            dataManagers.SaveAll(Username);

            return GetVenue(customerId, userId, data.Id);
        }

        public SingleResult<ClassInstructor> SaveClassInstructor(int customerId, int userId, int classInstructorId, ClassInstructor classInstructor)
        {
            Data.ClassX klass = new Data.ClassX(classInstructor.ClassID);
            if (klass.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the class with id " + klass.Id);
            }

            Data.ClassInstructor data = new Data.ClassInstructor(classInstructorId);
            classInstructor.CopyTo(data);
            data.Id = classInstructorId;
            data.IsNew = (data.Id == 0);
            data.Save(Username);

            List<NotificationDate> notificationDates = new List<NotificationDate>();
            for (var i = 0; i < klass.ClassDates.Count(); i++)
            {
                var cd = klass.ClassDates[i];

                notificationDates.Add(new NotificationDate()
                {
                    StartDate = cd.StartDateTime,
                    EndDate = cd.StartDateTime.AddMinutes((double)cd.Duration),
                    Type = CalendarType.Timed,
                    InstanceID = klass.Id + "-" + i,
                    Method = CalendarMethod.Create,
                    Name = klass.Name + " (Class " + (i + 1) + " of " + klass.ClassDates.Count() + ")"
                });
            }

            NotificationController.SendNotification(NotificationType.InstructorAdded, new NotificationOptions()
            {
                TemplateObjects = new object[] { klass },
                NotificationDates = notificationDates.ToArray()
            });

            return new SingleResult<ClassInstructor>(Model.Create<ClassInstructor>(data));
        }

        public SingleResult<ClassResource> SaveClassResource(int customerId, int userId, int classResourceId, ClassResource classResource)
        {
            Data.ClassX klass = new Data.ClassX(classResource.ClassID);
            if (klass.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the class with id " + klass.Id);
            }

            Data.ClassResource data = new Data.ClassResource(classResourceId);
            classResource.CopyTo(data);
            data.Id = classResourceId;
            data.IsNew = (data.Id == 0);
            data.Save(Username);

            NotificationController.SendNotification(NotificationType.ResourceRequested, new object[] { data.Resource, klass });

            return new SingleResult<ClassResource>(Model.Create<ClassResource>(data));
        }

        public SingleResult<VenueResourceManager> SaveVenueResourceManager(int customerId, int userId, int venueResourceManagerId, VenueResourceManager venueResourceManager)
        {
            Data.Venue venue = new Data.Venue(venueResourceManager.VenueID);
            if (venue.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the venue with id " + venue.Id);
            }

            Data.VenueResourceManager data = new Data.VenueResourceManager(venueResourceManagerId);
            venueResourceManager.CopyTo(data);
            data.Id = venueResourceManagerId;
            data.IsNew = (data.Id == 0);
            data.Save(Username);

            return new SingleResult<VenueResourceManager>(Model.Create<VenueResourceManager>(data));
        }

        public SingleResult<Resource> SaveResource(int customerId, int userId, int resourceId, Resource resource)
        {
            Data.Resource data = new Data.Resource(resourceId);
            resource.CopyTo(data);
            data.Id = resourceId;
            data.CustomerID = customerId;
            data.Save(Username);

            return new SingleResult<Resource>(Model.Create<Resource>(data));
        }

        public SingleResult<Room> SaveRoom(int customerId, int userId, int roomId, Room room)
        {
            Data.Room data = new Data.Room(roomId);
            room.CopyTo(data);
            data.Id = roomId;
            data.CustomerID = customerId;
            data.Save(Username);

            return new SingleResult<Room>(Model.Create<Room>(data));
        }

        public SingleResult<Registration> SaveRegistration(int customerId, int userId, int registrationId, Registration registration)
        {
            Data.ClassX klass = new Data.ClassX(registration.ClassID);
            if (klass.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the class with id " + klass.Id);
            }

            Data.Registration data = new Data.Registration(Data.Registration.Columns.Id, registrationId);

            data.IsNew = (data.Id == 0);
            bool isScoreChanged = data.Score != registration.Score;

            registration.CopyTo(data);
            data.Id = registrationId;
            
            data.Save(Username);

            if (data.IsNew || isScoreChanged)
            {
                new RollupController().EnqueueTrainingProgramRollup(new List<int>{data.RegistrantID}, klass.CourseID, CourseType.Classroom);
            }

            return new SingleResult<Registration>(Model.Create<Registration>(data));
        }

        public SingleResult<List<ClassDate>> RescheduleClass(int customerId, int userId, int classId, ClassDate classDate)
        {
            Data.ClassX klass = new Data.ClassX(classId);
            if (klass.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the class with id " + klass.Id);
            }

            Data.ClassDate.Destroy(Data.ClassDate.Columns.ClassID, classId);
            List<ClassDate> classDates = new List<ClassDate>();
            DateTime datetime = classDate.StartDateTime;
            for (int i = 0; i < klass.Course.NumberOfDays; i++)
            {
                ClassDate cd = new ClassDate
                {
                    CustomerID = customerId,
                    ClassID = classId,
                    StartDateTime = datetime,
                    Duration = klass.Course.DailyDuration.Value
                };

                classDates.Add(cd);

                datetime = datetime.AddDays(1);

                Data.ClassDate dataCD = new Data.ClassDate();
                cd.CopyTo(dataCD);
                dataCD.Save(Username);
            }

            return new SingleResult<List<ClassDate>>(classDates);
        }

        public SingleResult<bool> DeleteCourse(int customerId, int userId, int courseId)
        {
            Data.Course data = new Data.Course(courseId);
            if (data.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the course with id " + courseId);
            }
            if (Select.AllColumnsFrom<Data.ClassX>().Where(Data.ClassX.Columns.CourseID).IsEqualTo(courseId).GetRecordCount() > 0)
            {
                throw new Exception("You cannot delete a course that is assigned to a class.");
            }
            Data.Course.Destroy(courseId);
            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteClass(int customerId, int userId, int classId)
        {
            Data.ClassX data = new Data.ClassX(classId);
            if (data.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the class with id " + classId);
            }
            if (!(UserIsSalesChannelAdmin || HasRole(Roles.ClassroomManager)))
            {
                throw new Exception("You do not have permission to delete this class (" + classId + ")");
            }

            List<NotificationDate> notificationDates = new List<NotificationDate>();
            for (var i = 0; i < data.ClassDates.Count(); i++)
            {
                var cd = data.ClassDates[i];

                notificationDates.Add(new NotificationDate()
                {
                    StartDate = cd.StartDateTime,
                    EndDate = cd.StartDateTime.AddMinutes((double)cd.Duration),
                    Type = CalendarType.Timed,
                    InstanceID = data.Id + "-" + i,
                    Method = CalendarMethod.Delete,
                    Name = data.Name + " (Class " + (i + 1) + " of " + data.ClassDates.Count() + ")"
                });
            }

            // send notifications
            NotificationController.SendNotification(
                NotificationType.ClassCancelled,
                new NotificationOptions()
                {
                    TemplateObjects = new object[] { data },
                    NotificationDates = notificationDates.ToArray()
                });

            Data.ClassX.Destroy(classId);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteVenue(int customerId, int userId, int venueId)
        {
            Data.Venue data = new Data.Venue(venueId);
            if (data.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the venue with id " + venueId);
            }
            Data.Venue.Destroy(venueId);
            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteClassInstructor(int customerId, int userId, int classInstructorId)
        {
            Data.ClassInstructor data = new Data.ClassInstructor(Data.ClassInstructor.Columns.Id, classInstructorId);
            Data.ClassX klass = new Data.ClassX(data.ClassID);
            if (klass.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the class with id " + klass.Id);
            }

            List<NotificationDate> notificationDates = new List<NotificationDate>();
            for (var i = 0; i < klass.ClassDates.Count(); i++)
            {
                var cd = klass.ClassDates[i];

                notificationDates.Add(new NotificationDate()
                {
                    StartDate = cd.StartDateTime,
                    EndDate = cd.StartDateTime.AddMinutes((double)cd.Duration),
                    Type = CalendarType.Timed,
                    InstanceID = klass.Id + "-" + i,
                    Method = CalendarMethod.Delete,
                    Name = klass.Name + " (Class " + (i + 1) + " of " + klass.ClassDates.Count() + ")"
                });
            }

            NotificationController.SendNotification(NotificationType.InstructorRemoved, new NotificationOptions()
            {
                TemplateObjects = new object[] { klass },
                NotificationDates = notificationDates.ToArray()
            });

            Data.ClassInstructor.Destroy(Data.ClassInstructor.Columns.Id, classInstructorId);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteClassResource(int customerId, int userId, int classResourceId)
        {
            Data.ClassResource data = new Data.ClassResource(Data.ClassResource.Columns.Id, classResourceId);
            Data.ClassX klass = new Data.ClassX(data.ClassID);
            if (klass.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the class with id " + klass.Id);
            }

            Data.ClassResource.Destroy(Data.ClassResource.Columns.Id, classResourceId);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteVenueResourceManager(int customerId, int userId, int venueResourceManagerId)
        {
            Data.VenueResourceManager data = new Data.VenueResourceManager(Data.VenueResourceManager.Columns.Id, venueResourceManagerId);
            Data.Venue venue = new Data.Venue(data.VenueID);
            if (venue.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the venue with id " + venue.Id);
            }
            Data.VenueResourceManager.Destroy(Data.VenueResourceManager.Columns.Id, venueResourceManagerId);
            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteResource(int customerId, int userId, int resourceId)
        {
            Data.Resource data = new Data.Resource(resourceId);
            if (data.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the resource with id " + resourceId);
            }
            Data.Resource.Destroy(resourceId);
            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteRoom(int customerId, int userId, int roomId)
        {
            Data.Room data = new Data.Room(roomId);
            if (data.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the room with id " + roomId);
            }
            Data.Room.Destroy(roomId);
            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteRegistration(int customerId, int userId, int registrationId)
        {
            Data.Registration data = new Data.Registration(Data.Registration.Columns.Id, registrationId);
            Data.ClassX klass = new Data.ClassX(data.ClassID);
            if (klass.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the class with id " + klass.Id);
            }
            Data.Registration.Destroy(Data.Registration.Columns.Id, registrationId);
            return new SingleResult<bool>(true);
        }

        public PagedResult<CourseFile> FindCourseFiles(int customerId, int userId, int courseId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.CourseFile.Columns.FileName;

            SqlQuery q = Select.AllColumnsFrom<Data.CourseFile>()
                .Where(Data.CourseFile.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.CourseFile.Columns.CourseID).IsEqualTo(courseId);

            return new PagedResult<CourseFile>(q, pageIndex, pageSize, orderBy, orderDir);
        }

        public Models.CourseFile UploadCourseFile(int customerId, int courseId, string filename, string title, string description, HttpPostedFile fileData)
        {
            Data.CourseFile file = new Symphony.Core.Data.CourseFile();
            file.FileName = filename;
            file.Title = title ?? filename;
            file.Description = description ?? filename;
            file.FileBytes = new byte[] { };
            file.CourseID = courseId;
            file.CustomerID = customerId;
            file.Save(Username);

            string path = HttpContext.Current.Server.MapPath(CourseFilesLocation + file.Id);
            string directory = Path.GetDirectoryName(path);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            fileData.SaveAs(path);

            return Model.Create<Models.CourseFile>(file);
        }

        public SingleResult<bool> DeleteCourseFile(int customerId, int userId, int courseFileId)
        {
            Data.CourseFile data = new Data.CourseFile(courseFileId);
            if (data.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to delete the course file with id " + courseFileId);
            }
            Data.CourseFile.Destroy(courseFileId);
            return new SingleResult<bool>(true);
        }

        public MultipleResult<PendingRegistration> GetPendingRegistrations(int customerId, int userId)
        {
            SqlQuery query = null;
            if (HasRole(Roles.ClassroomManager))
            {
                query = Select.AllColumnsFrom<Data.PendingRegistration>()
                    .Where(Data.PendingRegistration.Columns.CustomerId).IsEqualTo(customerId);
            }
            else if (HasRole(Roles.ClassroomSupervisor))
            {
                query = Select.AllColumnsFrom<Data.PendingRegistration>()
                    .Where(Data.PendingRegistration.Columns.RegistrationId)
                    .In(new Select(Data.Registration.IdColumn.QualifiedName + " as RegistrationId")
                        .From<Data.Registration>()
                        .InnerJoin(Data.User.IdColumn, Data.Registration.RegistrantIDColumn)
                        .WhereExpression(Data.User.SupervisorIDColumn.QualifiedName).IsEqualTo(userId)
                            .Or(Data.User.SecondarySupervisorIDColumn.QualifiedName).IsEqualTo(userId)
                        .CloseExpression()
                    );
            }
            else if (HasRole(Roles.ClassroomInstructor))
            {
                query = Select.AllColumnsFrom<Data.PendingRegistration>()
                    .Where(Data.PendingRegistration.Columns.ClassId)
                    .In(new Select(Data.ClassInstructor.ClassIDColumn)
                         .From<Data.ClassInstructor>()
                         .Where(Data.ClassInstructor.InstructorIDColumn)
                         .IsEqualTo(userId)
                     );
            }
            else
            {
                throw new Exception("Permission denied");
            }

            // order by class, then name of student
            query = query.OrderAsc(Data.PendingRegistration.Columns.Name, Data.PendingRegistration.Columns.LastName);
            MultipleResult<PendingRegistration> result = new MultipleResult<PendingRegistration>(query);

            // utc
            foreach (PendingRegistration registration in result.Data)
            {
                registration.StartDate = registration.StartDate.WithUtcFlag();
            }

            return result;
        }

        public SingleResult<bool> SavePendingRegistrations(int customerId, int userId, List<PendingRegistration> registrations)
        {
            List<ClassOverage> overages = new List<ClassOverage>();
 
            foreach (PendingRegistration pr in registrations)
            {
                Data.Registration registration = new Data.Registration(Data.Registration.Columns.Id, pr.RegistrationId);
                Data.ClassX cls = new Data.ClassX(registration.ClassID);

                int totalRegistered = Select.AllColumnsFrom<Data.Registration>()
                               .Where(Data.Registration.Columns.ClassID).IsEqualTo(registration.ClassID)
                               .And(Data.Registration.Columns.RegistrationStatusID).IsEqualTo((int)RegistrationStatusType.Registered)
                               .GetRecordCount();

                if (pr.Status == RegistrationStatusType.Registered)
                {
                    if (totalRegistered == cls.CapacityOverride)
                    {
                        pr.Status = RegistrationStatusType.WaitList;

                        bool found = false;
                        foreach (ClassOverage co in overages)
                        {
                            if (co.ClassID == cls.Id)
                            {
                                co.Overage++;
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            ClassOverage co = new ClassOverage();
                            co.ClassID = cls.Id;
                            co.ClassName = cls.Name;
                            co.Overage = 1;
                            overages.Add(co);
                        }
                    }
                }

                bool changed = registration.RegistrationStatusID != (int)pr.Status;

                registration.RegistrationStatusID = (int)pr.Status;
                registration.Save(Username);

                // if the status changed, send out a notification
                if (changed)
                {
                    List<NotificationDate> notificationDates = new List<NotificationDate>();
                    for (var i = 0; i < cls.ClassDates.Count(); i++)
                    {
                        var cd = cls.ClassDates[i];

                        notificationDates.Add(new NotificationDate()
                        {
                            StartDate = cd.StartDateTime,
                            EndDate = cd.StartDateTime.AddMinutes((double)cd.Duration),
                            Type = CalendarType.Timed,
                            Method = CalendarMethod.Update,
                            InstanceID = cls.Id + "-" + i,
                            Name = cls.Name + " (Class " + (i + 1) + " of " + cls.ClassDates.Count() + ")"
                        });
                    };

                    Data.User user = new Symphony.Core.Data.User(registration.RegistrantID);
                    NotificationController.SendNotification(NotificationType.RegistrationStatusChanged, new NotificationOptions()
                    {
                        TemplateObjects = new object[] { user, registration, cls },
                        NotificationDates = notificationDates.ToArray()
                    });
                }
            }

            SingleResult<bool> result = new SingleResult<bool>(true);

            if (overages.Count > 0)
            {
                result.Notice = "The following classes have reached the maxmimum occupancy and the corresponding number of users have been added to the wait list.<br /><br />Please increase the room capacity for these classes to allow additional registrants.<br />";

                foreach (ClassOverage co in overages)
                {
                    result.Notice += "<br />" + co.ClassName + ": " + co.Overage;
                }
            }

            return result;
        }

        public MultipleResult<User> GetUsersByHierarchy(int customerId, int userId, int hierarchyId, List<int> nodeIds)
        {
            // get all users in the given node

            List<User> users = new List<User>();

            foreach (int id in nodeIds)
            {
                StoredProcedure sp = Data.SPs.GetUsersByHierarchy(hierarchyId, id);
                users.AddRange(sp.ExecuteTypedList<User>());
            }
            users = users.Distinct(new UserComparer()).AsQueryable().OrderBy("FullName").ToList();

            return new MultipleResult<User>(users);
        }

        public SingleResult<ClassroomClass> SaveThirdPartyClass(int customerId, int userId, ThirdPartyClass klass)
        {
            // always create a new course with the same name as the class
            Data.Course course = new Symphony.Core.Data.Course();
            course.IsThirdParty = true;
            course.NumberOfDays = 1;
            course.DailyDuration = 60;
            course.CourseCompletionTypeID = klass.CourseCompletionTypeId;
            course.Name = klass.Name;
            course.CustomerID = customerId;
            course.Save();

            Data.ClassX c = new Data.ClassX();
            c.Name = klass.Name;
            c.CourseID = course.Id;
            c.IsThirdParty = true;
            c.IsVirtual = true;
            c.CapacityOverride = 1000;
            c.ApprovalRequired = true;
            c.CustomerID = customerId;
            c.Description = klass.Name;
            c.Save();

            // create list of dates
            Data.ClassDate date = new Data.ClassDate();
            date.ClassID = c.Id;
            date.CustomerID = customerId;
            date.StartDateTime = klass.Date;
            date.Duration = 60; // 1 hour
            date.Save();

            return GetClass(customerId, userId, c.Id);
        }

        #region Course Categories

        public PagedResult<Category> FindCategories(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Category>()
                .Where(Data.Category.Columns.CustomerID).IsEqualTo(customerId)
                .AndExpression(Data.Category.Columns.Name).ContainsString(searchText)
                .Or(Data.Category.Columns.Description).ContainsString(searchText);

            return new PagedResult<Category>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public MultipleResult<Category> SaveCategories(int customerId, int userId, List<Category> categories)
        {
            foreach (Category category in categories)
            {
                Data.Category data = new Symphony.Core.Data.Category(category.Id);
                data.Description = category.Description;
                data.Name = category.Name;
                data.CustomerID = customerId;
                data.CategoryTypeID = category.CategoryTypeID;
                data.Save();

                category.Id = data.Id;
            }

            return new MultipleResult<Category>(categories);
        }

        #endregion

        #region sessions
        /// <summary>
        /// Automatically assigns a list of users to sessions for a training program
        /// </summary>
        /// <param name="users">Users to assign</param>
        /// <returns></returns>
        public MultipleResult<SessionAssignedUser> SaveSessionRegistrations(List<SessionAssignedUser> users, bool isPreview)
        {
            if (users.Count == 0)
            {
                throw new Exception("No users to assign.");
            }

            Dictionary<int, Data.TrainingProgram> trainingPrograms = Select.AllColumnsFrom<Data.TrainingProgram>()
                .Where(Data.TrainingProgram.IdColumn).In(users.Select(u => u.TrainingProgramID))
                .ExecuteTypedList<Data.TrainingProgram>()
                .DistinctBy(t => t.Id)
                .ToDictionary(t => t.Id);

            List<int> courseIds = trainingPrograms.Select(kv => kv.Value.SessionCourseID.Value).DistinctBy(i => i).ToList();

            if (courseIds.Count == 0)
            {
                throw new Exception("Session course could not be found for this training program.");
            }

            List<Data.ClassroomClassDetail> classes = Select.AllColumnsFrom<Data.ClassroomClassDetail>()
                .Where(Data.ClassroomClassDetail.Columns.MinClassDate).IsGreaterThan(DateTime.UtcNow)
                .And(Data.ClassroomClassDetail.Columns.CourseID).In(courseIds)
                .OrderAsc(Data.ClassroomClassDetail.Columns.MinClassDate)
                .ExecuteTypedList<Data.ClassroomClassDetail>();

            if (classes.Count == 0)
            {
                throw new Exception("No upcoming classes to register users in.");
            }

            Dictionary<int, int> classCapacity = Select.AllColumnsFrom<Data.ClassX>()
                .Where(Data.ClassX.IdColumn).In(classes.Select(c => c.ClassID))
                .ExecuteTypedList<Data.ClassX>()
                .ToDictionary(k => k.Id, v => v.CapacityOverride);

            List<Data.Registration> registrations = Select.AllColumnsFrom<Data.Registration>()
                .Where(Data.Registration.ClassIDColumn).In(classes.Select(c => c.ClassID))
                .And(Data.Registration.RegistrationStatusIDColumn).IsEqualTo((int)RegistrationStatusType.Registered)
                .ExecuteTypedList<Data.Registration>();

            Dictionary<int, List<Data.ClassroomClassDetail>> classroomDictionary = new Dictionary<int, List<Data.ClassroomClassDetail>>();
            Dictionary<int, List<Data.Registration>> registrationDictionary = new Dictionary<int, List<Data.Registration>>();
            
            courseIds.ForEach(courseId =>
            {
                classroomDictionary.Add(courseId, classes.Where(c => c.CourseID == courseId).ToList());
            });

            classes.ForEach(cls =>
            {
                registrationDictionary.Add(cls.ClassID, registrations.Where(r => r.ClassID == cls.Id).ToList());
            });

            List<SessionAssignedUser> assignedUsers = new List<SessionAssignedUser>();

            
            foreach (KeyValuePair<int, Data.TrainingProgram> kv in trainingPrograms)
            {
                Data.TrainingProgram tp = kv.Value;

                // Possibiliity of no sessions available for a training program
                List<Data.ClassroomClassDetail> sessions = classroomDictionary.ContainsKey(tp.SessionCourseID.Value) ?
                    classroomDictionary[tp.SessionCourseID.Value] :
                    new List<Data.ClassroomClassDetail>();

                List<SessionAssignedUser> tpUsers = users.Where(u => u.TrainingProgramID == tp.Id).ToList();

                // Assign users to sessions sequentially where space is available
                for (var i = 0; i < sessions.Count; i++)
                {
                    Data.ClassroomClassDetail session = sessions[i];
                    int registrationCount = registrationDictionary[session.ClassID].Count;

                    if (registrationCount < classCapacity[session.ClassID] && tpUsers.Count > 0)
                    {
                        List<SessionAssignedUser> assignUsers = tpUsers.Take(classCapacity[session.ClassID] - registrationCount).ToList();
                        
                        try
                        {
                            assignedUsers.AddRange(
                                SaveSessionRegistrations(session.ClassID, assignUsers, isPreview).Data
                                    .Where(u => u.RegistrationStatusID == (int)RegistrationStatusType.Registered)
                                    .ToList()
                            );
                        }
                        catch (Exception e)
                        {
                            // Couldn't save a chunk of users. Log this for now
                            // We will return all the users that could not be assigned
                            Log.Error(e);
                        }

                        // Safe to run either way - the registeredForTp will just be an empty
                        // dictionary and we wont end up removing anything from tpUsers.
                        // any left over tpUsers will be added to the list we return, but with registration
                        // status nulled
                        Dictionary<int, SessionAssignedUser> registeredForTp = assignedUsers.Where(u =>
                                u.TrainingProgramID == tp.Id &&
                                u.RegistrationStatusID == (int)RegistrationStatusType.Registered &&
                                u.ClassID == session.ClassID
                            )
                            .DistinctBy(u => u.ID)
                            .ToDictionary(u => u.ID);

                        tpUsers.RemoveAll(u => registeredForTp.ContainsKey(u.ID));

                    }
                }

                if (tpUsers.Count > 0)
                {
                    tpUsers.ForEach(u =>
                    {
                        u.ClassID = null;
                        u.ClassName = "";
                        u.RegistrationStatusID = null;
                        u.RegistrationStatusName = "";
                    });
                    assignedUsers.AddRange(tpUsers);
                }
            }

            List<SessionAssignedUser> unassigned = assignedUsers.Where(a => a.RegistrationStatusID != (int)RegistrationStatusType.Registered).ToList();
            List<string> notices = new List<string>();
            if (unassigned.Count > 0)
            {
                foreach (KeyValuePair<int, Data.TrainingProgram> kv in trainingPrograms)
                {
                    List<SessionAssignedUser> unassignedForTp = unassigned.Where(u => u.TrainingProgramID == kv.Value.Id).ToList();
                    
                    notices.Add("Could not assign sessions for the following users in training program \"" + kv.Value.Name + "\"" +
                                    "<br/><br/>" + string.Join("<br/>", unassignedForTp.Select(u => u.FirstName + " " + u.LastName + " " + u.RegistrationStatusName)));
                }
            }

            MultipleResult<SessionAssignedUser> result = new MultipleResult<SessionAssignedUser>(assignedUsers);
            if (notices.Count > 0) {
                result.Notice = string.Join("<br/><br/>", notices);
            }

            return result;
        }
        public MultipleResult<SessionAssignedUser> SaveSessionRegistrations(int sessionId, List<SessionAssignedUser> users)
        {
            return SaveSessionRegistrations(sessionId, users, false);
        }
        /// <summary>
        /// Moves the user registrations to the selected session
        /// </summary>
        /// <param name="sessionId">The session to register users to</param>
        /// <param name="users">The users to register in the session</param>
        /// <param name="isPreview">Will return the changes being made only</param>
        public MultipleResult<SessionAssignedUser> SaveSessionRegistrations(int sessionId, List<SessionAssignedUser> users, bool isPreview)
        {
            ClassroomClass classroom = GetClass(CustomerID, UserID, sessionId, true).Data;
            classroom.RegistrationList = GetRegistrationsForClass(UserID, CustomerID, sessionId);

            List<Data.Registration> currentRegistrations = Select.AllColumnsFrom<Data.Registration>()
                    .Where(Data.Registration.Columns.ClassID).IsEqualTo(sessionId)
                    .ExecuteTypedList<Data.Registration>();

            Data.Course course = new Data.Course(classroom.CourseID);

            Dictionary<int, Registration> registrations = classroom.RegistrationList.ToDictionary(k => k.RegistrantID);

            int registeredCount = classroom.RegistrationList.Where(r => r.RegistrationStatusID == (int)RegistrationStatusType.Registered).ToList().Count;

            List<SessionAssignedUser> registeredUsers = new List<SessionAssignedUser>();
            List<SessionAssignedUser> waitlistedUsers = new List<SessionAssignedUser>();

            foreach (SessionAssignedUser user in users)
            {
                user.ClassID = sessionId;
                user.ClassName = classroom.Name;

                if (registrations.ContainsKey(user.ID))
                {
                    if (registrations[user.ID].RegistrationStatusID != (int)RegistrationStatusType.Registered)
                    {
                        if (registeredCount < classroom.CapacityOverride) {
                            registeredUsers.Add(user);
                            registrations[user.ID].RegistrationStatusID = (int)RegistrationStatusType.Registered;
                            user.RegistrationStatusID = (int)RegistrationStatusType.Registered;
                            registeredCount++;
                        } else {
                            waitlistedUsers.Add(user);
                            registrations[user.ID].RegistrationStatusID = (int)RegistrationStatusType.WaitList;
                            user.RegistrationStatusID = (int)RegistrationStatusType.WaitList;
                        }
                    }
                }
                else
                {
                    Registration newReg = new Registration()
                    {
                        Id = 0,
                        RegistrantID = user.ID,
                        RegistrationTypeID = (int)RegistrationType.Manual
                    };
                    if (registeredCount < classroom.CapacityOverride)
                    {
                        registeredUsers.Add(user);
                        newReg.RegistrationStatusID = (int)RegistrationStatusType.Registered;
                        user.RegistrationStatusID = (int)RegistrationStatusType.Registered;
                        registeredCount++;
                    }
                    else
                    {
                        waitlistedUsers.Add(user);
                        newReg.RegistrationStatusID = (int)RegistrationStatusType.WaitList;
                        user.RegistrationStatusID = (int)RegistrationStatusType.WaitList;
                    }

                    classroom.RegistrationList.Add(newReg);
                }
            }
            if (!isPreview)
            {
                using (TransactionScope t = Utilities.CreateTransactionScope())
                {
                    SaveRegistrations(sessionId, currentRegistrations, classroom, course);

                    // Soft delete the old registrations by just setting the class id and the user id to * -1
                    // This way we don't have to deal with a user waitlisted or denied in a previous session and registered
                    // in the next session. 
                    //
                    // Updates all registrations that are for classes OTHER than the class being assigned to
                    // AND for the SAME course
                    List<Data.Registration> oldRegistrations = Select.AllColumnsFrom<Data.Registration>()
                        .InnerJoin(Data.ClassX.IdColumn, Data.Registration.ClassIDColumn)
                        .Where(Data.Registration.ClassIDColumn).IsNotEqualTo(sessionId)
                        .And(Data.ClassX.CourseIDColumn).IsEqualTo(classroom.CourseID)
                        .And(Data.Registration.RegistrantIDColumn).In(users.Select(u => u.ID))
                        .ExecuteTypedList<Data.Registration>();

                    string registrationIdsToClear = string.Join(",", oldRegistrations.Select(c => c.Id).ToArray());

                    Data.SPs.ClearRegistrations(registrationIdsToClear, Username).Execute();

                    t.Complete();
                }
            }

            MultipleResult<SessionAssignedUser> result = new MultipleResult<SessionAssignedUser>(users);

            if (waitlistedUsers.Count > 0)
            {
                result.Notice = "The following users have been added to the waitlist for " + classroom.Name + ": <br/><br/>" + string.Join("<br/>", waitlistedUsers.Select(u => u.FirstName + " " + u.LastName)); 
            }

            return result;
        }

        /// <summary>
        /// Saves session registrations without knowing what training program is assigned to the passed in user
        /// </summary>
        /// <param name="userIds">The user ids to look up the training program and the session registraiton info</param>
        public MultipleResult<SessionAssignedUser> SaveSessionRegistrations(List<int> userIds, bool isPreview)
        {
            CourseAssignmentController courseAssignment = new CourseAssignmentController();
            List<int> trainingProgramIds = courseAssignment.GetTrainingProgramsForUsers(userIds).Data.Select(tp => tp.Id).ToList();
            List<SessionAssignedUser> unassignedUsers = courseAssignment.GetSessionUsers(trainingProgramIds, null, "none", false, false, null, userIds, "", "", OrderDirection.Ascending, 0, int.MaxValue).Data.ToList();

            return SaveSessionRegistrations(unassignedUsers, isPreview);
        }


        #endregion

    }
}
