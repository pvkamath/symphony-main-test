﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Symphony.Core.Models;
using System.Configuration;
using SubSonic;
using Symphony.Core.Extensions;
using log4net;
using System.Transactions;

namespace Symphony.Core.Controllers
{
    public class AssignmentController : Controllers.SymphonyController
    {
        private ILog Log = LogManager.GetLogger(typeof(AssignmentController));

        public PagedResult<UserAssignment> GetUsersWithAssignments(int trainingProgramId, int courseId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                searchText = "";
            }

            if (trainingProgramId == 0)
            {
                throw new Exception("Training program ID required.");
            }

            Data.TrainingProgram trainingProgram = new Data.TrainingProgram(trainingProgramId);
            trainingProgramId = trainingProgram.ParentTrainingProgramID.HasValue && trainingProgram.ParentTrainingProgramID.Value > 0 ?
                trainingProgram.ParentTrainingProgramID.Value :
                trainingProgram.Id;

            List<UserAssignment> userAssignments = Data.SPs.GetUsersWithAssignmentsForTrainingProgram(
                 trainingProgramId,
                 trainingProgram.SessionCourseID,
                 searchText,
                 orderBy,
                 orderDir == OrderDirection.Descending ? "desc" : "asc",
                 pageIndex,
                 pageSize)
            .ExecuteTypedList<UserAssignment>();

            SqlQuery ungradedAssignmentQuery = new Select()
                .GroupBy(Data.OnlineCourseAssignment.UserIDColumn)
                .From<Data.OnlineCourseAssignment>()
                .Where(Data.OnlineCourseAssignment.ResponseStatusColumn).IsEqualTo((int)AssignmentResponseStatus.Unmarked);

            if (trainingProgramId > 0)
            {
                ungradedAssignmentQuery 
                    //Add training program details
                    .InnerJoin(Data.TrainingProgram.IdColumn, Data.OnlineCourseAssignment.TrainingProgramIDColumn)
                    // Get parent and child tps
                    .AndExpression(Data.TrainingProgram.IdColumn.QualifiedName).IsEqualTo(trainingProgramId)
                        .Or(Data.TrainingProgram.ParentTrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                    .CloseExpression();
            }

            Dictionary<int, int> ungradedAssignmentsDictionary = ungradedAssignmentQuery.ExecuteTypedList<int>()
                .ToDictionary(i => i);

            // Fix time issue on assignments - submit date stored in server time
            userAssignments.ForEach(ua => {
                ua.MaxSubmitDate = ua.MaxSubmitDate.Add(DateTime.UtcNow.Subtract(DateTime.Now));

                ua.MaxSubmitDate = ua.MaxSubmitDate.WithUtcFlag();

                if (ua.MaxGradedDate.HasValue)
                {
                    ua.MaxGradedDate = ua.MaxGradedDate.Value.WithUtcFlag();
                }

                ua.IsNew = false;
                if (ungradedAssignmentsDictionary.ContainsKey(ua.ID))
                {
                    ua.IsNew = true;
                }
            });

            UserAssignment first = userAssignments.FirstOrDefault();
            int totalRows = first != null ? first.TotalRows : 0;
            
            return new PagedResult<UserAssignment>(userAssignments, pageIndex, pageSize, totalRows);
        }

        public MultipleResult<AssignmentResponse> GetAssignmentResponses(int trainingProgramId, int courseId, int sectionId, int attempt, int userId)
        {
            List<string> columns = Data.OnlineCourseAssignment.Schema.Columns.Select(c => c.QualifiedName).ToList();
            columns.Add(Data.ArtisanAnswer.TextXColumn.QualifiedName + " as CorrectResponse");
            columns.Add(Data.ArtisanPage.HtmlColumn.QualifiedName + " as Question");

            SqlQuery query = Select.AllColumnsFrom<Data.OnlineCourseAssignment>()
                .IncludeColumn(Data.AssignmentSummaryQuestion.CorrectResponseColumn.QualifiedName, "CorrectResponse")
                .IncludeColumn(Data.AssignmentSummaryQuestion.QuestionColumn.QualifiedName, "Question")
                .InnerJoin(Data.AssignmentSummaryQuestion.ArtisanPageIDColumn, Data.OnlineCourseAssignment.PageIDColumn)
                .Where(Data.OnlineCourseAssignment.UserIDColumn).IsEqualTo(userId)
                .And(Data.OnlineCourseAssignment.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                .And(Data.OnlineCourseAssignment.CourseIDColumn).IsEqualTo(courseId)
                .And(Data.AssignmentSummaryQuestion.ArtisanSectionIDColumn).IsEqualTo(sectionId)
                .And(Data.OnlineCourseAssignment.AttemptColumn).IsEqualTo(attempt)
                .OrderAsc(Data.AssignmentSummaryQuestion.SortColumn.QualifiedName);

            List<AssignmentResponse> responses = query.ExecuteTypedList<AssignmentResponse>();

            return new MultipleResult<AssignmentResponse>(responses);
        }

        public MultipleResult<AssignmentResponse> GetLastAssignmentResponses(int trainingProgramId, int courseId, int userId)
        {
            List<AssignmentResponse> responses = Data.SPs.GetLatestAssignmentResponses(userId, trainingProgramId, courseId)
                .ExecuteTypedList<AssignmentResponse>();

            return new MultipleResult<AssignmentResponse>(responses);
        }

        public MultipleResult<AssignmentResponse> PostAssignmentResponses(int trainingProgramId, int courseId, int sectionId, int attempt, int userId, List<AssignmentResponse> assignmentResponses)
        {
            Data.OnlineCourseAssignmentCollection assignment = new Data.OnlineCourseAssignmentCollection();
            int countOfUnmarked = 0;
            foreach (AssignmentResponse response in assignmentResponses)
            {
                Data.OnlineCourseAssignment assignmentData = new Data.OnlineCourseAssignment(response.ID);
                response.CopyTo(assignmentData);
                
                assignmentData.DateX = response.Date;
                assignmentData.InstructorFeedback = response.InstructorFeedback;
                assignmentData.ResponseStatus = response.ResponseStatus;
                assignmentData.GradedDate = DateTime.UtcNow;

                if (assignmentData.ResponseStatus == (int)AssignmentResponseStatus.Unmarked)
                    countOfUnmarked++;

                if (assignmentData.ResponseStatus == (int)AssignmentResponseStatus.Correct)
                {
                    assignmentData.IsCorrect = true;
                }
                else
                {
                    assignmentData.IsCorrect = false;
                }

                assignmentData.Save(UserID);
            }

            SummarizeGradedAssignmentAttempt(trainingProgramId, courseId, sectionId, userId, attempt);

            if (countOfUnmarked == 0)
            {
                SendNotification(trainingProgramId, courseId, sectionId, userId, attempt);
                new RollupController().EnqueueTrainingProgramRollup(trainingProgramId, userId, courseId, CourseType.Online);
            }

            SendRequiredCoursesCompleteNotification(trainingProgramId, courseId, sectionId, userId, attempt);

            return GetAssignmentResponses(trainingProgramId, courseId, sectionId, attempt, userId);
        }

        private void SendRequiredCoursesCompleteNotification(int trainingProgramId, int courseId, int sectionId, int userId, int attempt)
        {
            PortalController portal = new PortalController();
            Data.User user = new Data.User(userId);

            Assignment assignment = GetAssignments(trainingProgramId, courseId, userId, "", "", OrderDirection.Ascending, 0, int.MaxValue)
                                       .Data
                                       .Where(a => a.AttemptID == attempt && a.SectionID == sectionId)
                                       .FirstOrDefault();

            List<TrainingProgram> trainingPrograms = portal.FindTrainingProgramsForUser(userId, user.NewHireIndicator, "", "", OrderDirection.Ascending, 0, int.MaxValue).Data.ToList();
            
            Dictionary<int, int> trainingProgramIdsLinkedToCourse = new Select(Data.TrainingProgram.IdColumn)
                .From<Data.TrainingProgram>()
                .InnerJoin(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn, Data.TrainingProgram.IdColumn)
                .Where(Data.TrainingProgram.IdColumn).In(trainingPrograms.Select(t => t.Id))
                .And(Data.TrainingProgramToCourseLink.CourseIDColumn).IsEqualTo(courseId)
                .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo(CourseType.Online)
                .ExecuteTypedList<int>()
                .ToDictionary(i => i);
           
            trainingPrograms = trainingPrograms.Where(t => trainingProgramIdsLinkedToCourse.ContainsKey(t.Id)).ToList();

            if (trainingPrograms.Count() > 0)
            {
                portal.AddTrainingProgramDetails(userId, trainingPrograms, false, false);

                foreach (TrainingProgram tp in trainingPrograms)
                {
                    Data.TrainingProgram tpData = new Data.TrainingProgram();
                    tp.CopyTo(tpData);

                    /*bool assignmentCountsForTP = CourseController.AssignmentCountsForTP(tpData, assignment);

                    if (!assignmentCountsForTP)
                    {
                        continue;
                    }*/

                    bool assignmentsMarked = true; // Assume all assignments for courses in the tp have been marked
                    bool completedAssignments = true; // assume all assignments for the course have been completed
                    bool tpHasAssignments = false; // assume there are no assignments
                    foreach (Course c in tp.RequiredCourses)
                    {
                        if (c.HasAssignments)
                        {
                            tpHasAssignments = true; // We only need one assignment to send a notification
                            if (c.AssignmentsMarked < c.AssignmentsInCourse)
                            {
                                assignmentsMarked = false; // There is one assignment not marked, we wont send a notification
                                                           // since we only want to send the notification when all have been marked
                                break;
                            }
                            if (c.IsAssignmentRequired)
                            {
                                completedAssignments = false; // If all assignments are marked, and there is an assigment required
                                                              // then at least one assignment has failed and we should send
                                                              // the assignments incomplete notification
                            }
                        }
                    }

                    // The training program should always have assignments
                    // as they have already been filtered by a course with an 
                    // assignment, but just in case. 
                    string notificationType = "";
                    if (tpHasAssignments)
                    {
                        if (assignmentsMarked && completedAssignments)
                        {
                            // Send assignments passed notification
                            notificationType = NotificationType.AssignmentsGradedPassed;
                        }
                        else if (assignmentsMarked && !completedAssignments)
                        {
                            // Send assignments failed notification
                            notificationType = NotificationType.AssignmentsGradedFailed;
                        }

                        if (!string.IsNullOrEmpty(notificationType))
                        {
                            NotificationController.SendNotification(notificationType, new NotificationOptions()
                            {
                                TemplateObjects = new object[] { user, tpData },
                                CustomerOverride = user.Customer
                            });
                        }
                    }

                }
            }


        }

        private void SendNotification(int trainingProgramId, int courseId, int sectionId, int userId, int attempt)
        {

            Data.OnlineCourse course = new Data.OnlineCourse(courseId);
            Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);
            Data.User user = new Data.User(userId);

            Assignment assignment = GetAssignments(trainingProgramId, courseId, userId, "", "", OrderDirection.Ascending, 0, int.MaxValue)
                                        .Data
                                        .Where(a => a.AttemptID == attempt && a.SectionID == sectionId)
                                        .FirstOrDefault();

            if (assignment == null)
            {
                assignment = new Assignment();
            }

            NotificationOptions options = new NotificationOptions
            {
                TemplateObjects = new object[] { user, course, assignment, tp },
                CustomerOverride = user.Customer
            };

            NotificationController.SendNotification(NotificationType.AssignmentGraded, options);

            if (assignment.IsPassed)
            {
                NotificationController.SendNotification(NotificationType.SingleAssignmentGradedPassed, options);
            }
            else
            {
                NotificationController.SendNotification(NotificationType.SingleAssignmentGradedFailed, options);
            }
        }
        
        public SingleResult<AssignmentStatus> GetAssignmentStatus(int trainingProgramId, int courseId, int userId) {


            SqlQuery query = new Select()
                .GroupBy(Data.AssignmentSummaryAttempt.TrainingProgramIDColumn)
                .GroupBy(Data.AssignmentSummaryAttempt.OnlineCourseIDColumn)
                .GroupBy(Data.AssignmentSummaryAttempt.UserIDColumn)
                .GroupBy(Data.AssignmentSummaryAttempt.AssignmentSummaryIDColumn)
                .Max(Data.AssignmentSummaryAttempt.QuestionsAnsweredColumn)
                .Max(Data.AssignmentSummaryAttempt.QuestionsMarkedColumn)
                .Max(Data.AssignmentSummaryAttempt.QuestionsCorrectColumn)
                .Max(Data.AssignmentSummaryAttempt.GradedOnColumn)
                .Max(Data.AssignmentSummaryAttempt.SubmittedOnColumn)
                .Max(Data.AssignmentSummaryAttempt.ScoreColumn)
                .Min(string.Format("cast({0} as int)", Data.AssignmentSummaryAttempt.IsMarkedColumn.QualifiedName), Data.AssignmentSummaryAttempt.IsMarkedColumn.ColumnName)
                .Max(string.Format("cast({0} as int)", Data.AssignmentSummaryAttempt.IsCompleteColumn.QualifiedName), Data.AssignmentSummaryAttempt.IsCompleteColumn.ColumnName)
                .Min(string.Format("cast({0} as int)", Data.AssignmentSummaryAttempt.IsFailedColumn.QualifiedName), Data.AssignmentSummaryAttempt.IsFailedColumn.ColumnName)
                .From<Data.AssignmentSummaryAttempt>()
                .Where(Data.AssignmentSummaryAttempt.OnlineCourseIDColumn).IsEqualTo(courseId)
                .And(Data.AssignmentSummaryAttempt.UserIDColumn).IsEqualTo(userId)
                .And(Data.AssignmentSummaryAttempt.TrainingProgramIDColumn).IsEqualTo(trainingProgramId);

            List<Data.AssignmentSummaryAttempt> assignmentSummaryAttempts = query.ExecuteTypedList<Data.AssignmentSummaryAttempt>();

            AssignmentStatus assignmentStatus = new AssignmentStatus()
            {
                CourseID = courseId,
                UserID = userId,
                AssignmentsMarked = 0,
                AssignmentsCompleted = 0,
                AssignmentsInCourse = 0,
                IsAssignmentsComplete = false,
                IsAssignmentsFailed = false,
                AssignmentsAttempted = 0
            };

            if (assignmentSummaryAttempts != null && assignmentSummaryAttempts.Count > 0)
            {
                query = new Select()
                    .Count(Data.AssignmentSummary.IdColumn)
                    .From<Data.OnlineCourseRollup>()
                    .InnerJoin(Data.ArtisanAudit.IdColumn, Data.OnlineCourseRollup.ArtisanAuditIDColumn)
                    .InnerJoin(Data.AssignmentSummary.ArtisanCourseIDColumn, Data.ArtisanAudit.ArtisanCourseIDColumn)
                    .Where(Data.OnlineCourseRollup.UserIDColumn).IsEqualTo(userId)
                    .And(Data.OnlineCourseRollup.CourseIDColumn).IsEqualTo(courseId)
                    .And(Data.OnlineCourseRollup.TrainingProgramIDColumn).IsEqualTo(trainingProgramId);

                int totalAssignments = query.ExecuteScalar<int>();

                assignmentStatus.AssignmentsAttempted = assignmentSummaryAttempts.Count;
                assignmentStatus.AssignmentsInCourse = totalAssignments;

                foreach (Data.AssignmentSummaryAttempt attempt in assignmentSummaryAttempts)
                {
                    if (attempt.IsMarked)
                    {
                        assignmentStatus.AssignmentsMarked++;
                    }
                    if (attempt.IsComplete)
                    {
                        assignmentStatus.AssignmentsCompleted++;
                    }
                }
            } 
            else 
            {
                // There are no completed assignments for this course and student. 
                // Need to look up the details of the course
                List<Data.AssignmentSummary> assignmentSummaryList = Select.AllColumnsFrom<Data.AssignmentSummary>()
                    .InnerJoin(Data.OnlineCourse.PublishedArtisanCourseIDColumn, Data.AssignmentSummary.ArtisanCourseIDColumn)
                    .Where(Data.OnlineCourse.IdColumn).IsEqualTo(courseId)
                    .ExecuteTypedList<Data.AssignmentSummary>();

                if (assignmentSummaryList == null || assignmentSummaryList.Count == 0)
                {
                    // There are no assignmnets for this course, so return an
                    // assignment status result that indicates that the 
                    // assignments are complete
                    assignmentStatus = new AssignmentStatus()
                    {
                        CourseID = courseId,
                        UserID = userId,
                        AssignmentsMarked = 0,
                        AssignmentsCompleted = 0,
                        AssignmentsInCourse = 0,
                        IsAssignmentsComplete = true,
                        IsAssignmentsFailed = false,
                        AssignmentsAttempted = 0
                    };
                }
                else
                {
                    // There are assignments for the coure but the student hasn't
                    // completed any, so return a status indicating that the 
                    // student has not completed any assignments and the number
                    // of assignments required.
                    assignmentStatus = new AssignmentStatus()
                    {
                        CourseID = courseId,
                        UserID = userId,
                        AssignmentsMarked = 0,
                        AssignmentsCompleted = 0,
                        AssignmentsInCourse = assignmentSummaryList.Count,
                        IsAssignmentsComplete = false,
                        IsAssignmentsFailed = false,
                        AssignmentsAttempted = 0
                    };
                }
            }

            return new SingleResult<AssignmentStatus>(assignmentStatus);
        }

        public PagedResult<Assignment> GetAssignments(int trainingProgramId, int courseId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                searchText = "";
            }

            Dictionary<int, bool> completedAssignments = new Dictionary<int, bool>();
            
            SqlQuery query = null;
            try
            {
                query = new Select()
                    .GroupBy(Data.AssignmentSummaryAttempt.TrainingProgramIDColumn)
                    .GroupBy(Data.AssignmentSummaryAttempt.OnlineCourseIDColumn)
                    .GroupBy(Data.AssignmentSummaryAttempt.UserIDColumn)
                    .GroupBy(Data.AssignmentSummaryAttempt.AssignmentSummaryIDColumn)
                    .Max(Data.AssignmentSummaryAttempt.AttemptIDColumn)
                    .From<Data.AssignmentSummaryAttempt>()
                    .Where(Data.AssignmentSummaryAttempt.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                    .And(Data.AssignmentSummaryAttempt.OnlineCourseIDColumn).IsEqualTo(courseId)
                    .And(Data.AssignmentSummaryAttempt.UserIDColumn).IsEqualTo(userId);

                Dictionary<int, Data.AssignmentSummaryAttempt> lastAttempts = query.ExecuteTypedList<Data.AssignmentSummaryAttempt>()
                    .DistinctBy(s => s.AssignmentSummaryID)
                    .ToDictionary(s => s.AssignmentSummaryID);

                query = Select.AllColumnsFrom<Data.AssignmentSummaryAttempt>()
                    .IncludeColumn(Data.OnlineCourse.NameColumn.QualifiedName, "CourseName")
                    .IncludeColumn(Data.AssignmentSummary.NameColumn.QualifiedName, "Name")
                    .IncludeColumn(Data.AssignmentSummary.ArtisanCourseIDColumn.QualifiedName, "ArtisanCourseID")
                    .IncludeColumn(Data.AssignmentSummary.ArtisanSectionIDColumn.QualifiedName, "SectionID")
                    .IncludeColumn(Data.AssignmentSummaryAttempt.IsCompleteColumn.QualifiedName, "IsPassed")
                    .InnerJoin(Data.OnlineCourse.IdColumn, Data.AssignmentSummaryAttempt.OnlineCourseIDColumn)
                    .InnerJoin(Data.AssignmentSummary.IdColumn, Data.AssignmentSummaryAttempt.AssignmentSummaryIDColumn)
                    .WhereExpression(Data.AssignmentSummary.NameColumn.QualifiedName).ContainsString(searchText)
                        .Or(Data.OnlineCourse.NameColumn).ContainsString(searchText)
                    .CloseExpression()
                    .And(Data.AssignmentSummaryAttempt.UserIDColumn).IsEqualTo(userId);

                if (trainingProgramId > 0) {
                    Data.TrainingProgram trainingProgram = new Data.TrainingProgram(trainingProgramId);
                    int parentTrainingProgramId = trainingProgram.ParentTrainingProgramID.Value > 0 ?
                        trainingProgram.ParentTrainingProgramID.Value :
                        trainingProgram.Id;

                    List<int> trainingProgramIds = new Select(Data.TrainingProgram.IdColumn)
                        .From<Data.TrainingProgram>()
                        .Where(Data.TrainingProgram.ParentTrainingProgramIDColumn).IsEqualTo(parentTrainingProgramId)
                        .Or(Data.TrainingProgram.IdColumn).IsEqualTo(parentTrainingProgramId)
                        .ExecuteTypedList<int>();


                    query.And(Data.AssignmentSummaryAttempt.TrainingProgramIDColumn).In(trainingProgramIds);
                }

                if (courseId > 0)
                {
                    query.And(Data.AssignmentSummaryAttempt.OnlineCourseIDColumn).IsEqualTo(courseId);
                }

                query.OrderAsc(Data.AssignmentSummaryAttempt.IsMarkedColumn.QualifiedName);
                query.OrderAsc(Data.OnlineCourse.NameColumn.QualifiedName);
                query.OrderDesc(Data.AssignmentSummary.ArtisanCourseIDColumn.QualifiedName);
                query.OrderDesc(Data.AssignmentSummaryAttempt.AttemptIDColumn.QualifiedName);

                if (orderBy == "groupId")
                {
                    orderBy = null;
                }

                if (!string.IsNullOrEmpty(orderBy))
                {
                    if (orderDir == OrderDirection.Descending)
                    {
                        query.OrderDesc(orderBy);
                    }
                    else
                    {
                        query.OrderAsc(orderBy);
                    }
                }
                query.OrderAsc(Data.AssignmentSummaryAttempt.OnlineCourseIDColumn.QualifiedName, Data.AssignmentSummary.ArtisanSectionIDColumn.QualifiedName);

                PagedResult<Assignment> result = new PagedResult<Assignment>(query, pageIndex, pageSize, orderBy, orderDir); 

                int maxDeliveredArtisanId = 0;
                int deliveredCourseAssignmentCount = 0;
                List<Assignment> latestAssignments = new List<Assignment>();
                Dictionary<string, int> groupIdDictionary = new Dictionary<string, int>();
                int maxGroupId = 0;
                foreach (Assignment a in result.Data)
                {
                    a.Date = a.Date.WithUtcFlag();

                    if (a.GradedOn.HasValue)
                    {
                        a.GradedOn = a.GradedOn.Value.WithUtcFlag();
                    }

                    if (lastAttempts.ContainsKey(a.AssignmentSummaryID))
                    {
                        a.IsOld = lastAttempts[a.AssignmentSummaryID].AttemptID > a.AttemptID;
                    }

                    if (a.ArtisanCourseID < a.PublishedArtisanCourseId)
                    {
                        a.IsOldArtisanCourse = true;
                    }

                    // Keep track of the assignments that are for the most 
                    // current course deployment that the user has attempted
                    if (a.ArtisanCourseID > maxDeliveredArtisanId)
                    {
                        maxDeliveredArtisanId = a.ArtisanCourseID;
                        deliveredCourseAssignmentCount = a.TotalAssignments;
                        latestAssignments = new List<Assignment>() { a };
                    }
                    else if (a.ArtisanCourseID == maxDeliveredArtisanId)
                    {
                        latestAssignments.Add(a);
                    }

                    if (!groupIdDictionary.ContainsKey(a.GroupKey))
                    {
                        groupIdDictionary.Add(a.GroupKey, maxGroupId);
                        maxGroupId++;
                    }

                    a.GroupId = groupIdDictionary[a.GroupKey];
                }
                // If an assignment is complete in the latest delivered
                // version of the course, track it. If the number of 
                // completed assignments is equal to the total assignments
                // in the current delivered course, then the assignments
                // are complete. This means the student will potentially
                // get a new version of the course, so don't bother showing
                // the assignment links. 
                foreach (Assignment assignment in latestAssignments)
                {
                    if (assignment.IsMarked && assignment.IsPassed)
                    {
                        if (!completedAssignments.ContainsKey(assignment.SectionID))
                        {
                            completedAssignments.Add(assignment.SectionID, true);
                        }
                    }
                }

                if (completedAssignments.Keys.Count < deliveredCourseAssignmentCount)
                {
                    // Because if any assignment is incomplete the user will not receive
                    // a registration for the new version of the course. This means we 
                    // can safely display the launch assignment link for this course
                    // without it failing.
                    latestAssignments.ForEach(a => a.IsOldArtisanCourse = false);
                }

                return result;
            }
            catch (SqlQueryException e)
            {
                Log.Fatal(string.Format("Fatal SQL error while listing assignments. Training Program ID: {0} Course ID: {1} User ID: {2} Query: {3}",
                    trainingProgramId, courseId, userId, query.ToString()), e);
                throw;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public SimpleSingleResult<bool> PostForcedAssignmentResponses(int trainingProgramId, int courseId, int userId, AssignmentResponseStatus status, bool isCorrect)
        {
            List<AssignmentResponse> responses = Data.SPs.GetAssignmentWithForcedStatus(userId, courseId, trainingProgramId, (int)status, isCorrect ? 1 : 0, "Auto Response", "Auto Feedback")
                .ExecuteTypedList<AssignmentResponse>();

            List<List<AssignmentResponse>> responseGroups = responses
                .GroupBy(r => r.SectionID)
                .Select(a => a.ToList())
                .ToList();

            foreach (List<AssignmentResponse> responseGroup in responseGroups)
            {
                int attempt = responseGroup.Max(r => r.Attempt);

                if (responseGroup.Count() == 0)
                {
                    continue;
                }

                int sectionId = responseGroup.FirstOrDefault().SectionID;

                PostAssignmentResponses(trainingProgramId, courseId, sectionId, attempt, userId, responseGroup);
            }

            return new SimpleSingleResult<bool>(true);
        }

        #region Summaries
        /// <summary>
        /// Creates AssignmentSummary and AssignmentSummaryQuestions for a course.
        /// </summary>
        /// <param name="onlineCourseId">The course id</param>
        /// <param name="artisanCourseId">The artisan course id</param>
        public void SummarizeAssignments(int onlineCourseId, int artisanCourseId)
        {
            SqlQuery query = null;
            try
            {
                using (TransactionScope ts = Utilities.CreateTransactionScope())
                {
                    query = new Select()
                        .GroupBy(Data.ArtisanCourse.IdColumn, Data.AssignmentSummary.ArtisanCourseIDColumn.ColumnName)
                        .GroupBy(Data.ArtisanSection.NameColumn, Data.AssignmentSummary.NameColumn.ColumnName)
                        .GroupBy(Data.ArtisanSection.IdColumn, Data.AssignmentSummary.ArtisanSectionIDColumn.ColumnName)
                        .Count(Data.ArtisanPage.IdColumn, Data.AssignmentSummary.NumQuestionsColumn.ColumnName)
                        .GroupBy(Data.ArtisanCourse.PassingScoreColumn, Data.AssignmentSummary.PassingScoreColumn.ColumnName)
                        .GroupBy(Data.AssignmentSummary.IdColumn)
                        .From<Data.OnlineCourse>()
                        .InnerJoin(Data.ArtisanCourse.IdColumn, Data.OnlineCourse.PublishedArtisanCourseIDColumn)
                        .InnerJoin(Data.ArtisanSectionPage.CourseIDColumn, Data.ArtisanCourse.IdColumn)
                        .InnerJoin(Data.ArtisanSection.IdColumn, Data.ArtisanSectionPage.SectionIDColumn)
                        .InnerJoin(Data.ArtisanPage.IdColumn, Data.ArtisanSectionPage.PageIDColumn)
                        .LeftOuterJoin(Data.AssignmentSummary.ArtisanSectionIDColumn, Data.ArtisanSection.IdColumn)
                        .Where(Data.OnlineCourse.IdColumn).IsEqualTo(onlineCourseId)
                        .And(Data.ArtisanPage.QuestionTypeColumn).IsEqualTo((int)ArtisanQuestionType.LongAnswer);

                    List<Data.AssignmentSummary> summaries = query.ExecuteTypedList<Data.AssignmentSummary>();

                    Data.AssignmentSummaryCollection summaryCollection = new Data.AssignmentSummaryCollection();

                    summaries.ForEach(s => {
                        if (s.Id > 0)
                        {
                            s.SetColumnValue(Data.AssignmentSummary.IdColumn.ColumnName, s.Id);
                            s.DirtyColumns = Data.AssignmentSummary.Schema.Columns;
                        }
                        else
                        {
                            s.IsNew = true;
                        }
                        summaryCollection.Add(s);
                    });

                    summaryCollection.BatchSave(UserID);

                    query = new Select()
                        .IncludeColumn(Data.AssignmentSummaryQuestion.IdColumn.QualifiedName, Data.AssignmentSummaryQuestion.IdColumn.ColumnName)
                        .IncludeColumn(Data.AssignmentSummary.IdColumn.QualifiedName, Data.AssignmentSummaryQuestion.AssignmentSummaryIDColumn.ColumnName)
                        .IncludeColumn(Data.ArtisanPage.IdColumn.QualifiedName, Data.AssignmentSummaryQuestion.ArtisanPageIDColumn.ColumnName)
                        .IncludeColumn(Data.ArtisanPage.HtmlColumn.QualifiedName, Data.AssignmentSummaryQuestion.QuestionColumn.ColumnName)
                        .IncludeColumn(Data.ArtisanAnswer.TextXColumn.QualifiedName, Data.AssignmentSummaryQuestion.CorrectResponseColumn.ColumnName)
                        .IncludeColumn(Data.ArtisanSectionPage.SectionIDColumn.QualifiedName, Data.AssignmentSummaryQuestion.ArtisanSectionIDColumn.ColumnName)
                        .IncludeColumn(Data.ArtisanSectionPage.SortColumn.QualifiedName, Data.AssignmentSummaryQuestion.SortColumn.ColumnName)
                        .From<Data.AssignmentSummary>()
                        .InnerJoin(Data.ArtisanSectionPage.SectionIDColumn, Data.AssignmentSummary.ArtisanSectionIDColumn)
                        .InnerJoin(Data.ArtisanPage.IdColumn, Data.ArtisanSectionPage.PageIDColumn)
                        .InnerJoin(Data.ArtisanAnswer.PageIDColumn, Data.ArtisanPage.IdColumn)
                        .LeftOuterJoin(Data.AssignmentSummaryQuestion.ArtisanPageIDColumn, Data.ArtisanPage.IdColumn)
                        .And(Data.AssignmentSummary.ArtisanCourseIDColumn).IsEqualTo(artisanCourseId)
                        .And(Data.ArtisanPage.QuestionTypeColumn).IsEqualTo((int)ArtisanQuestionType.LongAnswer);

                    List<Data.AssignmentSummaryQuestion> summaryQuestions = query.ExecuteTypedList<Data.AssignmentSummaryQuestion>();
                    
                    Data.AssignmentSummaryQuestionCollection summaryQuestionCollection = new Data.AssignmentSummaryQuestionCollection();

                    summaryQuestions.ForEach(sq =>
                    {
                        if (sq.Id > 0)
                        {
                            sq.SetColumnValue(Data.AssignmentSummaryQuestion.IdColumn.ColumnName, sq.Id);
                            sq.DirtyColumns = Data.AssignmentSummaryQuestion.Schema.Columns;
                        }
                        else
                        {
                            sq.IsNew = true;
                        }
                        summaryQuestionCollection.Add(sq);
                    });

                    summaryQuestionCollection.BatchSave(UserID);

                    ts.Complete();
                }
            }
            catch (SqlQueryException e)
            {
                Log.Fatal("Fatal SQL error while querying assignments to summarize for the course id " + onlineCourseId + ". Query: "  + query.ToString(), e);
                throw;
            }
            catch (Exception e)
            {
                throw;
            }
  
        }

        /// <summary>
        /// Summarizes entries in the OnlineCourseAssignment table matching a paricular user/course/tp/attempt to an AssignmentSummaryAttempt
        /// Will update the graded date to the current date. Should be called when updating the assignment grade.
        /// </summary>
        /// <param name="trainingProgramId"></param>
        /// <param name="onlineCourseId"></param>
        /// <param name="artisanSectionId"></param>
        /// <param name="userId"></param>
        /// <param name="attemptId"></param>
        public void SummarizeGradedAssignmentAttempt(int trainingProgramId, int onlineCourseId, int artisanSectionId, int userId, int attemptId)
        {
            SummarizeAssignmentAttempt(trainingProgramId, onlineCourseId, artisanSectionId, userId, attemptId, true);
        }
        /// <summary>
        /// Summarizes entries in the OnlineCourseAssignment table matching a particular user/course/tp/attempt to an AssignmentSummaryAttempt
        /// </summary>
        /// <param name="trainingProgramId">The training program id to summarize</param>
        /// <param name="onlineCourseId">Online course id to summarize</param>
        /// <param name="artisanSectionId">Section id to summarize</param>
        /// <param name="userId">user id to summarize</param>
        /// <param name="attemptId">attempted id to summarize</param>
        /// <param name="isGrading">if true, the graded date will be updated, false the submitted date will be updated</param>
        public void SummarizeAssignmentAttempt(int trainingProgramId, int onlineCourseId, int artisanSectionId, int userId, int attemptId, bool isGrading = false)
        {
            SqlQuery query = null;
            try
            {
                User user = (new UserController()).GetUser(userId);
                Customer customer = (new CustomerController()).GetCustomer(CustomerID, UserID, user.CustomerID).Data;

                Data.AssignmentSummaryAttempt summaryAttempt = new Data.AssignmentSummaryAttempt();
                Data.AssignmentSummaryAttempt existingSummaryAttempt = new Data.AssignmentSummaryAttempt();

                string questionMarked = string.Format("case when ({0} != {1}) then 1 else 0 end",
                    Data.OnlineCourseAssignment.ResponseStatusColumn.QualifiedName, (int)AssignmentResponseStatus.Unmarked);

                string questionsCorrect = string.Format("cast({0} as int)",
                    Data.OnlineCourseAssignment.IsCorrectColumn.QualifiedName);

                string questionsFailed = string.Format("case when ({0} = {1}) then 1 else 0 end",
                    Data.OnlineCourseAssignment.ResponseStatusColumn, (int)AssignmentResponseStatus.Incorrect);

                query = new Select()
                    .GroupBy(Data.OnlineCourseAssignment.TrainingProgramIDColumn, Data.AssignmentSummaryAttempt.Columns.TrainingProgramID)
                    .GroupBy(Data.OnlineCourseAssignment.CourseIDColumn, Data.AssignmentSummaryAttempt.Columns.OnlineCourseID)
                    .GroupBy(Data.OnlineCourseAssignment.UserIDColumn, Data.AssignmentSummaryAttempt.Columns.UserID)
                    .GroupBy(Data.AssignmentSummaryQuestion.AssignmentSummaryIDColumn, Data.AssignmentSummaryAttempt.Columns.AssignmentSummaryID)
                    .GroupBy(Data.OnlineCourseAssignment.AttemptColumn, Data.AssignmentSummaryAttempt.Columns.AttemptID)
                    .GroupBy(Data.AssignmentSummary.NumQuestionsColumn, Data.AssignmentSummaryAttempt.Columns.TotalQuestions)
                    .GroupBy(Data.AssignmentSummary.PassingScoreColumn, Data.AssignmentSummaryAttempt.Columns.PassingScore)
                    .Count(Data.OnlineCourseAssignment.PageIDColumn, Data.AssignmentSummaryAttempt.Columns.QuestionsAnswered)
                    .Sum(questionMarked, Data.AssignmentSummaryAttempt.Columns.QuestionsMarked)
                    .Sum(questionsCorrect, Data.AssignmentSummaryAttempt.Columns.QuestionsCorrect)
                    .Sum(questionsFailed, Data.AssignmentSummaryAttempt.Columns.QuestionsFailed)
                    .From<Data.OnlineCourseAssignment>()
                    .InnerJoin(Data.AssignmentSummaryQuestion.ArtisanPageIDColumn, Data.OnlineCourseAssignment.PageIDColumn)
                    .InnerJoin(Data.AssignmentSummary.IdColumn, Data.AssignmentSummaryQuestion.AssignmentSummaryIDColumn)
                    .Where(Data.OnlineCourseAssignment.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                    .And(Data.OnlineCourseAssignment.CourseIDColumn).IsEqualTo(onlineCourseId)
                    .And(Data.AssignmentSummaryQuestion.ArtisanSectionIDColumn).IsEqualTo(artisanSectionId)
                    .And(Data.OnlineCourseAssignment.UserIDColumn).IsEqualTo(userId)
                    .And(Data.OnlineCourseAssignment.AttemptColumn).IsEqualTo(attemptId);

                AssignmentSummaryAttempt attempt = query.ExecuteSingle<AssignmentSummaryAttempt>();
                attempt.CopyTo(summaryAttempt);

                summaryAttempt.Score = (int)Math.Round(((float)summaryAttempt.QuestionsCorrect / (float)summaryAttempt.TotalQuestions) * 100);

                query = Select.AllColumnsFrom<Data.AssignmentSummaryAttempt>()
                    .InnerJoin(Data.AssignmentSummary.IdColumn, Data.AssignmentSummaryAttempt.AssignmentSummaryIDColumn)
                    .Where(Data.AssignmentSummaryAttempt.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                    .And(Data.AssignmentSummaryAttempt.OnlineCourseIDColumn).IsEqualTo(onlineCourseId)
                    .And(Data.AssignmentSummary.ArtisanSectionIDColumn).IsEqualTo(artisanSectionId)
                    .And(Data.AssignmentSummaryAttempt.UserIDColumn).IsEqualTo(userId)
                    .And(Data.AssignmentSummaryAttempt.AttemptIDColumn).IsEqualTo(attemptId);

                AssignmentSummaryAttempt existing = query.ExecuteSingle<AssignmentSummaryAttempt>();

                if (existing != null && existing.ID > 0)
                {
                    existing.CopyTo(existingSummaryAttempt);
                    existingSummaryAttempt.IsLoaded = true;
                    existingSummaryAttempt.IsNew = false;
                    summaryAttempt.Id = existingSummaryAttempt.Id;
                    summaryAttempt.CopyTo(existingSummaryAttempt);
                    summaryAttempt = existingSummaryAttempt;

                }
                else
                {
                    summaryAttempt.IsNew = true;
                }

                bool isMarked = summaryAttempt.TotalQuestions == summaryAttempt.QuestionsMarked;

                if (isGrading)
                {
                    summaryAttempt.GradedOn = DateTime.UtcNow;
                } 
                else 
                {
                    summaryAttempt.SubmittedOn = DateTime.UtcNow;
                }

                summaryAttempt.PassingScore = customer.PassingScoreOverride ?? summaryAttempt.PassingScore;
                summaryAttempt.IsMarked = summaryAttempt.TotalQuestions == summaryAttempt.QuestionsMarked;
                summaryAttempt.IsFailed = summaryAttempt.IsMarked && (summaryAttempt.Score < summaryAttempt.PassingScore);
                summaryAttempt.IsComplete = summaryAttempt.IsMarked && (summaryAttempt.Score >= summaryAttempt.PassingScore);


                summaryAttempt.Save(UserID == 0 ? userId : UserID);
            }
            catch (SqlQueryException e)
            {
                Log.Fatal(string.Format("Fatal SQL error while summarizing assignment attempts for Training Program: {0} Course: {1} Artisan Course: {2} Attempt: {3} Query: {4}",
                                trainingProgramId, onlineCourseId, artisanSectionId, attemptId, query.ToString()), e);

                throw;
            }
            catch (Exception e)
            {
                throw;
            }
        }
        #endregion


        /*public SimpleSingleResult<bool> PostAssignmentGrade(int aid, AssignmentResponseStatus status) 
        {
            Data.OnlineCourseAssignment assignment = new Data.OnlineCourseAssignment(aid);
            assignment.ResponseStatus = (int)status;

            if (status == AssignmentResponseStatus.Correct)
            {
                assignment.IsCorrect = true;
            }
            else
            {
                assignment.IsCorrect = false;
            }

            assignment.Save();
            return new SimpleSingleResult<bool>(true);
        }

        public SimpleSingleResult<bool> PostBulkAssignmentGrade(int trainingProgramId, int courseId, int sectionId, int attempt, int userId, AssignmentResponseStatus status)
        {
            MultipleResult<AssignmentResponse> responses = GetAssignmentResponses(trainingProgramId, courseId, sectionId, attempt, userId);

            // Will be slow since this means trip to db for each question
            // but avoids some code duplication. TODO: Consider cleanup if slow
            // Not expecting assignments to be very big so likely this wont be 
            // that noticeable.
            foreach (AssignmentResponse response in responses.Data)
            {
                PostAssignmentGrade(response.ID, status);
            }

            return new SimpleSingleResult<bool>();
        }*/
    }
}
