﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using SubSonic;
using Symphony.Web;
using System.Web.Security;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using SubSonic.Sugar;
using System.Data;
using log4net;
using System.Web;
using System.Net;
using System.ServiceModel.Web;
using Symphony.Core.Extensions;
using Symphony.Core.Comparers;
using System.Transactions;

namespace Symphony.Core.Controllers
{
    public class NetworkController : SymphonyController
    {
        private ILog Log = LogManager.GetLogger(typeof(NetworkController));

        public PagedResult<Network> FindAllNetworks(int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Network>();

            if (!string.IsNullOrEmpty(searchText))
            {
                query.And(Data.Network.Columns.Name).ContainsString(searchText);
            }

            return new PagedResult<Network>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<Network> GetNetwork(int networkId)
        {
            Network network = null;
            Data.Network existingNetwork = new Symphony.Core.Data.Network(networkId);
            network = Model.Create<Network>(existingNetwork);
            
            var query = Select.AllColumnsFrom<Data.CustomerNetwork>().Where(Data.CustomerNetwork.NetworkIdColumn).IsEqualTo(networkId);
            var resultCustomerNetwork = query.ExecuteTypedList<Data.CustomerNetwork>().OfType<Data.CustomerNetwork>().ToList();

            var queryRelationshipDetails = Select.AllColumnsFrom<Data.CustomerNetworkDetailsInfo>()
                .Where(Data.CustomerNetworkDetailsInfo.Columns.NetworkId).IsEqualTo(networkId);

            var resultCustomerNetworkDetails = queryRelationshipDetails.ExecuteTypedList<CustomerNetworkDetail>();

            if (network.CustomerNetwork == null)
            {
                network.CustomerNetwork = new List<Customer>();
            }
            foreach (Data.CustomerNetwork c in resultCustomerNetwork)
            {
                Customer customer = new Customer();
                customer.CopyFrom(c.Customer);
                network.CustomerNetwork.Add(customer);
            }
            if (network.CustomerNetworkDetails == null)
            {
                network.CustomerNetworkDetails = new List<CustomerNetworkDetail>();
            }
            foreach (CustomerNetworkDetail cnd in resultCustomerNetworkDetails)
            {
                List<Data.NetworkSharedTrainingProgram> networkPrograms = Select.AllColumnsFrom<Data.NetworkSharedTrainingProgram>()
                    .Where(Data.NetworkSharedTrainingProgram.Columns.CustomerNetworkDetailId).IsEqualTo(cnd.DetailId).ExecuteTypedList<Data.NetworkSharedTrainingProgram>();

                List<NetworkSharedEntity> networkSharedEntities = Select.AllColumnsFrom<Data.NetworkSharedEntity>()
                    .Where(Data.NetworkSharedEntity.Columns.CustomerNetworkDetailID).IsEqualTo(cnd.DetailId).ExecuteTypedList<NetworkSharedEntity>();

                if (networkPrograms.Count > 0)
                {
                    cnd.TrainingProgramIds = networkPrograms.Where(p => p.TrainingProgramId.HasValue)
                        .Select(p => p.TrainingProgramId.Value).ToList();
                }
                else
                {
                    cnd.TrainingProgramIds = new List<int>();
                }

                if (networkSharedEntities.Count > 0)
                {
                    cnd.NetworkSharedEntities = networkSharedEntities;
                }
                else
                {
                    cnd.NetworkSharedEntities = new List<NetworkSharedEntity>();
                }

                network.CustomerNetworkDetails.Add(cnd);
            }

            return new SingleResult<Network>(network);
        }
        /// <summary>
        /// Save network previously contained a lot of duplicated queries to deal with all the relationships
        /// at once. It also did entirely different things on update vs save. 
        /// 
        /// This is now cleaned up somewhat, still not perfect. May be better to move to an adhoc delete/save on the relationships
        /// 
        /// </summary>
        /// <param name="networkId"></param>
        /// <param name="network"></param>
        /// <returns></returns>
        public SingleResult<Network> SaveNetwork(int networkId, Network network)
        {
            Data.Network networkData = null;
            Dictionary<int, bool> customersInNetwork = new Dictionary<int, bool>(); // Keep track of unique customer ids used in the relationships

            // for building the resulting network as we save to avoid
            // additional loops at the end of the save, or a separate
            // call back to the db to re-load the network
            Network resultNetwork = new Network()
            {
                CustomerNetwork = new List<Customer>(),
                CustomerNetworkDetails = new List<CustomerNetworkDetail>()
            }; 

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                // Delete existing network relationships
                new Delete().From<Data.NetworkSharedTrainingProgram>()
                    .Where(Data.NetworkSharedTrainingProgram.CustomerNetworkDetailIdColumn)
                    .In(
                        new Select(Data.CustomerNetworkDetail.DetaildIdColumn)
                        .From<Data.CustomerNetworkDetail>()
                        .Where(Data.CustomerNetworkDetail.NetworkIdColumn)
                        .IsEqualTo(networkId)
                    )
                    .Execute();

                new Delete().From<Data.NetworkSharedEntity>()
                    .Where(Data.NetworkSharedEntity.NetworkIDColumn).IsEqualTo(networkId)
                    .Execute();

                new Delete().From<Data.CustomerNetworkDetail>()
                    .Where(Data.CustomerNetworkDetail.NetworkIdColumn).IsEqualTo(networkId)
                    .Execute();

                new Delete().From<Data.CustomerNetwork>()
                    .Where(Data.CustomerNetwork.NetworkIdColumn).IsEqualTo(networkId)
                    .Execute();
                               
                // Save the network first to ensure we have a network id other than 0
                networkData = new Data.Network(networkId);
                network.CopyTo(networkData);
                networkData.Id = networkId;

                networkData.Save(Username);

                resultNetwork.CopyFrom(networkData);

                // build the relationships in the network
                foreach (CustomerNetworkDetail detail in network.CustomerNetworkDetails)
                {
                    int clientId = detail.DetailId; // So we can update the original on the client

                    Data.CustomerNetworkDetail detailData = new Data.CustomerNetworkDetail();

                    detail.CopyTo(detailData);
                    detailData.NetworkId = networkData.Id;

                    detailData.Save(Username);

                    // Build training program relationships - TODO - update to use NetworkSharedEntity
                    if (detail.TrainingProgramIds != null)
                    {
                        foreach (int tpId in detail.TrainingProgramIds)
                        {
                            detailData.NetworkSharedTrainingPrograms.Add(new Data.NetworkSharedTrainingProgram
                            {
                                TrainingProgramId = tpId,
                                CustomerNetworkDetailId = detailData.DetaildId
                            });
                        }
                    }

                    // Build entity relationships.
                    if (detail.NetworkSharedEntities != null)
                    {
                        foreach (NetworkSharedEntity entity in detail.NetworkSharedEntities)
                        {
                            Data.NetworkSharedEntity entityData = new Data.NetworkSharedEntity();

                            entity.CopyTo(entityData);
                            entityData.NetworkID = networkData.Id;
                            entityData.CustomerNetworkDetailID = detailData.DetaildId;

                            detailData.NetworkSharedEntities.Add(entityData);
                        }
                    }

                    // Keep customers in the network to always be the customers used
                    // in the relationships.
                    //
                    // The CustomerNetwork table is somewhate obsolete at this point but
                    // leaving this in place for now to avoid changing the datastructure too much. 
                    // The customers in the network are essentially defined by the customers
                    // used in the relationship. 
                    //
                    // This just saves each customer on the fly as a new customer is found.
                    //
                    if (!customersInNetwork.ContainsKey(detail.SourceCustomerId))
                    {
                        new Data.CustomerNetwork
                        {
                            CustomerId = detail.SourceCustomerId,
                            NetworkId = networkData.Id
                        }.Save(Username);

                        // On save, we don't really need the full customer details to return as part
                        // of the network. Just returning the ID to avoid a customer lookup
                        resultNetwork.CustomerNetwork.Add(new Customer() { ID = detail.SourceCustomerId });
                    }
                    if (!customersInNetwork.ContainsKey(detail.DestinationCustomerId))
                    {
                        new Data.CustomerNetwork
                        {
                            CustomerId = detail.SourceCustomerId,
                            NetworkId = networkData.Id
                        }.Save(Username);

                        // On save, we don't really need the full customer details to return as part
                        // of the network. Just returning the ID to avoid a customer lookup
                        resultNetwork.CustomerNetwork.Add(new Customer() { ID = detail.DestinationCustomerId });
                    }

                    detailData.NetworkSharedEntities.BatchSave(Username);
                    detailData.NetworkSharedTrainingPrograms.BatchSave(Username);

                    // Copy out the network detail to the result
                    CustomerNetworkDetail detailResult = Model.Create<CustomerNetworkDetail>(detailData);

                    // Customer names should be the same after save
                    detailResult.SourceCustomerName = detail.SourceCustomerName;
                    detailResult.DestinationCustomerName = detail.DestinationCustomerName;

                    detailResult.NetworkSharedEntities = detailData.NetworkSharedEntities.ToList().Select(e => e != null ? Model.Create<NetworkSharedEntity>(e) : null).ToList<NetworkSharedEntity>();
                    detailResult.TrainingProgramIds = detailData.NetworkSharedTrainingPrograms.ToList().Select(t => t != null && t.TrainingProgramId.HasValue ? t.TrainingProgramId.Value : 0).ToList<int>();

                    detailResult.ClientID = clientId;

                    resultNetwork.CustomerNetworkDetails.Add(detailResult);
                }

                ts.Complete();
            }
        
            return new SingleResult<Network>(resultNetwork);
        }
    }
}
