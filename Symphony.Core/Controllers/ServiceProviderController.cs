﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Symphony.Core.Models;
using System.Configuration;
using SubSonic;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Configuration;

namespace Symphony.Core.Controllers
{
    public class ServiceProviderController : Controllers.SymphonyController
    {

        public static PagedResult<ServiceProvider> GetServiceProviders(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ServiceProvider>();

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query = query.Where(Data.ServiceProvider.UrlColumn).ContainsString(searchText)
                        .Or(Data.ServiceProvider.ArtifactUrlColumn).ContainsString(searchText)
                        .Or(Data.ServiceProvider.LogoutUrlColumn).ContainsString(searchText);
            }

            return new PagedResult<ServiceProvider>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public static SingleResult<ServiceProvider> SaveServiceProvider(int id, ServiceProvider serviceProvider)
        {
            Data.ServiceProvider provider = new Data.ServiceProvider(id);

            serviceProvider.CopyTo(provider);
            provider.Id = id;

            provider.Save();

            serviceProvider.CopyFrom(provider);

            return new SingleResult<ServiceProvider>(serviceProvider);
        }

        public static SingleResult<ServiceProvider> GetServiceProviderFromNameIdentifier(string nameIdentifier)
        {
            ServiceProvider provider = Select.AllColumnsFrom<Data.ServiceProvider>()
                .Where(Data.ServiceProvider.NameIdentifierColumn).IsEqualTo(nameIdentifier)
                .ExecuteSingle<ServiceProvider>();

            return new SingleResult<ServiceProvider>(provider);
        }

        public static SingleResult<ServiceProvider> GetServiceProvider(Uri referrerUri, Uri requestUri)
        {
            string baseUrl = referrerUri.Scheme + "://" + referrerUri.Host;

            if (baseUrl == GetTestServiceProviderUrl() && !IsAllowTest(requestUri))
            {
                throw new Exception("Test service providers not allowed in this environment.");
            }

            ServiceProvider provider = Select.AllColumnsFrom<Data.ServiceProvider>()
                .Where(Data.ServiceProvider.UrlColumn).StartsWith(baseUrl)
                .ExecuteSingle<ServiceProvider>();

            if (provider == null)
            {
                throw new Exception("Invalid service provider.");
            }

            return new SingleResult<ServiceProvider>(provider);
        }

        public static bool IsAllowTest(Uri requestUri) {
            return GetTestEnvironmentUrls().Contains(requestUri.Scheme + "://" + requestUri.Host);
        }

        public static string GetTestServiceProviderUrl()
        {
            if (ConfigurationManager.AppSettings.AllKeys.Contains("TestServiceProviderUrl")) {
                return ConfigurationManager.AppSettings["TestServiceProviderUrl"];   
            }

            return "http://localhost";
        }

        public static string[] GetTestEnvironmentUrls()
        {
            if (ConfigurationManager.AppSettings.AllKeys.Contains("TestEnvironmentUrls"))
            {
                return ConfigurationManager.AppSettings["TestEnvironmentUrls"].Split(';');
            }

            return new string[] { "http://localhost", "http://test2.portal.betraining.com" };
        }
        
    }
}
