using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using Data = Symphony.Core.Data;
using Symphony.Core.Models;
using SubSonic;
using System.Threading;
using System.Threading.Tasks;
using Symphony.Core.Extensions;
using System.Transactions;
using System.Data.SqlClient;
using Hangfire;

namespace Symphony.Core.Controllers
{
    public class RollupController : SymphonyController
    {
        private ILog Log = LogManager.GetLogger(typeof(RollupController).Name);

        /// <summary>
        /// Get the training program rollup for the specified user/training program.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="trainingProgramId"></param>
        /// <returns></returns>
        public Data.TrainingProgramRollup GetRollup(int trainingProgramId, int userId)
        {
            Data.TrainingProgramRollup tprollup = Select.AllColumnsFrom<Data.TrainingProgramRollup>()
                .InnerJoin(Data.TrainingProgram.IdColumn, Data.TrainingProgramRollup.TrainingProgramIDColumn)
                .Where(Data.TrainingProgramRollup.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                .And(Data.TrainingProgramRollup.UserIDColumn).IsEqualTo(userId)
                .WithNoLock()
                .ExecuteSingle<Data.TrainingProgramRollup>();

            return tprollup;
        }

        public Data.TrainingProgramRollup GetRollupObserveChildren(int trainingProgramId, int userId)
        {
            Data.TrainingProgramRollup tprollup = Select.AllColumnsFrom<Data.TrainingProgramRollup>()
                .InnerJoin(Data.TrainingProgram.IdColumn, Data.TrainingProgramRollup.TrainingProgramIDColumn)
                .WhereExpression(Data.TrainingProgramRollup.TrainingProgramIDColumn.QualifiedName).IsEqualTo(trainingProgramId)
                    .Or(Data.TrainingProgram.ParentTrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                .CloseExpression()
                .And(Data.TrainingProgramRollup.UserIDColumn).IsEqualTo(userId)
                .WithNoLock()
                .ExecuteSingle<Data.TrainingProgramRollup>();

            return tprollup;
        }

        private int GetTotalTimeForRequiredCourses(int userId, int trainingProgramId)
        {
            int totalTimeForRequiredCourses = new Select().From<Data.OnlineCourseRollup>()
                .Sum(Data.OnlineCourseRollup.TotalSecondsRoundedColumn)
                .Where(Data.OnlineCourseRollup.UserIDColumn).IsEqualTo(userId)
                .And(Data.OnlineCourseRollup.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                .And(Data.OnlineCourseRollup.CourseIDColumn).In(
                    new Select(
                        Data.TrainingProgramToCourseLink.CourseIDColumn
                    )
                        .From<Data.TrainingProgramToCourseLink>()
                        .Where(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo(CourseType.Online)
                        .And(Data.TrainingProgramToCourseLink.SyllabusTypeIDColumn).IsEqualTo(SyllabusType.Required)
                        .And(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                )
                .WithNoLock()
                .ExecuteScalar<int>();

            return totalTimeForRequiredCourses;
        }

        /// <summary>
        /// Coppies the training program state to the db
        /// </summary>
        /// <param name="userId">User id to save for</param>
        /// <param name="trainingProgramId">training program id to save</param>
        /// <param name="rollup">Rollup dataobject</param>
        /// <param name="trainingProgram">The training program state</param>
        /// <param name="isAdmin"></param>
        public void SaveRollup(int userId, int trainingProgramId, Data.TrainingProgramRollup rollup, TrainingProgram trainingProgram, bool isAdmin = false)
        {
            rollup.UserID = userId;
            rollup.TrainingProgramID = trainingProgramId;
            
            rollup.RequiredCompletedCount = trainingProgram.TotalCourseWorkComplete(trainingProgram.RequiredCourses, false);
            rollup.ElectiveCompletedCount = trainingProgram.TotalCourseWorkComplete(trainingProgram.ElectiveCourses, false);
            rollup.FinalCompletedCount = trainingProgram.TotalCourseWorkComplete(trainingProgram.FinalAssessments, false);

            if (trainingProgram.AssignmentCounts != null)
            {
                rollup.AssignmentsCompletedCount = trainingProgram.AssignmentCounts.TotalAssignmentsComplete;
                rollup.IsAssignmentsComplete = trainingProgram.IsAssignmentsComplete;
            }

            rollup.FinalExamScore = trainingProgram.FinalExamScore;
            if (trainingProgram.DateCompleted != null && trainingProgram.DateCompleted > DateTime.MinValue)
            {
                rollup.DateCompleted = trainingProgram.DateCompleted;
            }

            rollup.IsRequiredComplete = trainingProgram.IsRequiredComplete;
            rollup.IsElectiveComplete = trainingProgram.IsElectiveComplete;
            rollup.IsFinalComplete = trainingProgram.IsFinalComplete;

            if (trainingProgram.DisplayCompleted != null)
            {
                rollup.IsComplete = trainingProgram.DisplayCompleted.Icon == IconState.Certificate;
            }
            rollup.TotalSeconds = trainingProgram.TotalSeconds;
            
            rollup.IsSurveyComplete = trainingProgram.IsSurveyComplete;
            rollup.PercentComplete = trainingProgram.PercentComplete;

            rollup.IsFinalAvailable = trainingProgram.IsFinalAvailable;
            if (rollup.IsFinalAvailable && !rollup.FinalUnlockedOn.HasValue)
            {
                rollup.FinalUnlockedOn = DateTime.UtcNow;

                // we need a reference to the course for proctor information.
                foreach (TrainingProgramCourse tpc in trainingProgram.FinalAssessments)
                {
                    if (tpc.ProctorRequiredIndicator.HasValue && tpc.ProctorRequiredIndicator.Value)
                    {
                        _GenerateProctorKeysForAllFinallAssessments(trainingProgram.Id, userId);
                    }
                }
            }
            else if (!rollup.IsFinalAvailable)
            {
                rollup.FinalUnlockedOn = null;
            }

            NotificationController.SendTrainingProgramPercentCompleteNotification(rollup);
            
            rollup.Save((isAdmin ? "admin" : userId.ToString()));
        }

        [Queue("critical")]
        public void RunCriticalTrainingProgramRollupForUser(int trainingProgramId, int userId, int courseId, CourseType courseType)
        {
            RunTrainingProgramRollupForUser(trainingProgramId, userId, null, courseId, courseType);
        }

        [Queue("default")]
        public void RunDefaultTrainingProgramRollupForUser(int trainingProgramId, int userId) 
        {
            RunTrainingProgramRollupForUser(trainingProgramId, userId);
        }

        public void UpdateTrainingProgramRollupProfession(int trainingProgramId, int userId, int professionId)
        {
            RunTrainingProgramRollupForUser(trainingProgramId, userId, professionId);
        }

        /// <summary>
        /// Rolls up a particular training program for a user. Can optionally roll up training programs
        /// with reused courses.
        /// </summary>
        /// <param name="trainingProgramId">The training program to rollup</param>
        /// <param name="userId">The user id to rollup for</param>
        /// <param name="professionId">Optional - Sets the profession id on the rollup. Should only be used when updating a single tp/user (No course params)</param>
        /// <param name="courseId">Optional - The course id that triggered the change. If this course appears in other training programs for the user, they will also be rolled up</param>
        /// <param name="courseType">Optional (Required if course id is set) Indicates whether to treat the course as an online course or a classroom course.</param>
        public void RunTrainingProgramRollupForUser(int trainingProgramId, int userId, int? professionId = null, int? courseId = null, CourseType? courseType = null)
        {
            
            try
            {
                bool isCheckReusedCourses = false;
                if (courseId.HasValue || courseType.HasValue)
                {
                    if (!courseId.HasValue || (courseId.HasValue && courseId.Value < 1))
                    {
                        throw new Exception("Invalid course id - RunTrainingProgramRollupForUser requires a valid course id if a course type id is provided");
                    }
                    if (!courseType.HasValue)
                    {
                        throw new Exception("Missing course type. RunTrainingProgramRollupFor user requires a valid course type if a course id has been provided");
                    }
                    if (courseId.HasValue && courseType.HasValue)
                    {
                        isCheckReusedCourses = true;
                    }
                }

                List<TrainingProgram> trainingPrograms = Select.AllColumnsFrom<Data.TrainingProgram>()
                    .Where(Data.TrainingProgram.IdColumn).IsEqualTo(trainingProgramId)
                    .WithNoLock()
                    .ExecuteTypedList<TrainingProgram>();

                PortalController portal = new PortalController();
                portal.AddTrainingProgramDetails(userId, trainingPrograms, false, false, 0);

                TrainingProgram trainingProgram = trainingPrograms.FirstOrDefault();

                if (trainingProgram != null)
                {
                    using (TransactionScope ts = Utilities.CreateTransactionScope())
                    {
                        Data.TrainingProgramRollup tprollup = GetRollup(trainingProgramId, userId);

                        int totalTime = GetTotalTimeForRequiredCourses(userId, trainingProgramId);

                        trainingProgram.TotalSeconds = totalTime;

                        if (tprollup == null || tprollup.Id == 0)
                        {
                            tprollup = new Data.TrainingProgramRollup();
                        }

                        if (professionId.HasValue && professionId.Value > 0)
                        {
                            tprollup.ProfessionID = professionId;
                        }

                        tprollup.IsAccreditationSurveyComplete = RollupTrainingProgramSurveys(trainingProgramId, userId, professionId);

                        SaveRollup(userId, trainingProgramId, tprollup, trainingProgram);

                        ts.Complete();
                    }

                    // Rollup any other training programs that also contain the course that triggered this rollup
                    // Only doing this if this rollup was triggered by a course. 
                    // We must not trigger any of these rollups with course ids, otherwise we will create
                    // infinite recursion as each rollup will look for training programs that have the 
                    // matching course.
                    //
                    // Doing this outside the original transaction scope as it is more important
                    // for the original rollup to run to completion. Don't want any issues with
                    // additional rollups to prevent the original training program from rolling up
                    //
                    // Not wrapping this in a transaction as each call to RunTrainingProgramRollup
                    // will run under it's own transaction
                    if (isCheckReusedCourses)
                    {
                        Dictionary<int, int> trainingProgramIdsWithCourse = new Select(Data.TrainingProgram.IdColumn)
                            .From<Data.TrainingProgram>()
                            .InnerJoin(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn, Data.TrainingProgram.IdColumn)
                            .Where(Data.TrainingProgramToCourseLink.CourseIDColumn).IsEqualTo(courseId.Value)
                            .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo((int)courseType.Value)
                            .And(Data.TrainingProgram.IdColumn).IsNotEqualTo(trainingProgramId)
                            .ExecuteTypedList<int>()
                            .ToDictionary<int, int>(x => x);

                        if (trainingProgramIdsWithCourse != null && trainingProgramIdsWithCourse.Count > 0)
                        {
                            User user = new UserController().GetUser(userId);
                            // Loading all training programs including inactive ones. 
                            // This will result in more than necessary rollups, however, this is
                            // the easiest way to handle any cases where the data for an expired
                            // training program is being edited. (Score/duration changes through the
                            // CSR Admin tool) If we only focus on active training programs,
                            // the rollup will not apply to the previous tps. 
                            // The training program rollup will apply the courses based on 
                            // our business logici without a need to recreate the CourseCountsForTP
                            // logic here.
                            IEnumerable<TrainingProgramSummaryDetail> trainingProgramsForUser = portal.ListTrainingProgramsForUser(userId, user.NewHireIndicator, "", "", OrderDirection.Ascending, 0, int.MaxValue, new DateTime(1900, 1, 1), new DateTime(9999, 1, 1)).Data;

                            foreach (TrainingProgramSummaryDetail tp in trainingProgramsForUser)
                            {
                                if (trainingProgramIdsWithCourse.ContainsKey(tp.Id))
                                {
                                    // ensure the rest of the training programs roll up should anything
                                    // go run when running the rollup for a single training program
                                    try
                                    {
                                        // This should never include a profession/courseid/coursetypeid
                                        // We never want to change any other training program's selected professions
                                        // If we include the courseid and course type, we will create infinite
                                        // recursion as each call will attempt to update all other training
                                        // programs with matching courses. 
                                        EnqueueTrainingProgramRollup(tp.Id, new int[] {userId});                                       
                                    }
                                    catch (Exception e)
                                    {
                                        Log.Error(e);
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
            catch (SqlException e)
            {
                if (e.Number == 2601 || e.Number == 2827)
                {
                    Log.Error("Ignoring duplicate training program rollup.");
                }
                else
                {
                    Log.Error("Could not rollup training program duration for user: " + userId + " and training program: " + trainingProgramId);
                    Log.Error(e);
                }
            }
            catch (Exception e)
            {
                Log.Error("Could not rollup training program status for user: " + userId + " and training program: " + trainingProgramId);
                Log.Error(e);
            }
        }

        // returns true if all surveys complete
        private bool RollupTrainingProgramSurveys(int trainingProgramId, int userId, int? professionId)
        {
            var now = DateTime.Now.ToString();

            var surveyQuery = new Select()
                .IncludeColumn(Data.AccreditationBoardSurveyAssignment.OnlineCourseIDColumn.QualifiedName, "Id")
                .From(Data.TrainingProgramAccreditation.Schema)
                .InnerJoin(Data.AccreditationBoard.Schema)
                .InnerJoin(Data.AccreditationBoardSurveyAssignment.AccreditationBoardIDColumn, Data.AccreditationBoard.IdColumn)
                .InnerJoin(Data.AccreditationBoardProfession.AccreditationBoardIDColumn, Data.AccreditationBoard.IdColumn)
                .Where(Data.TrainingProgramAccreditation.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                .And(Data.TrainingProgramAccreditation.IsDeletedColumn).IsEqualTo(false)
                .And(Data.TrainingProgramAccreditation.StatusColumn).IsEqualTo((int)AccreditationStatus.Accredited)
                .And(Data.TrainingProgramAccreditation.StartDateColumn).IsLessThanOrEqualTo(now)
                .And(Data.TrainingProgramAccreditation.ExpiryDateColumn).IsGreaterThanOrEqualTo(now)
                .And(Data.AccreditationBoard.Columns.IsDeleted).IsEqualTo(false);

            if (professionId != null)
            {
                surveyQuery = surveyQuery.And(Data.AccreditationBoardProfession.ProfessionIDColumn).IsEqualTo(professionId);
            }                

            var surveys = surveyQuery.OrderAsc(Data.AccreditationBoardSurveyAssignment.RankColumn.QualifiedName)
                .ExecuteTypedList<AccreditationBoardSurvey>();

            if (surveys.Count > 0)
            {
                int completeCount = 0;
                var ids = surveys.Select(s => s.Id).Distinct().ToList();

                var surveyResponses = Select.AllColumnsFrom<Data.OnlineCourseRollup>()
                    .Where(Data.OnlineCourseRollup.UserIDColumn).IsEqualTo(userId)
                    .And(Data.OnlineCourseRollup.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                    .And(Data.OnlineCourseRollup.CourseIDColumn).In(ids)
                    .WithNoLock()
                    .ExecuteTypedList<Data.OnlineCourseRollup>()
                    .ToDictionary(
                        ocr => ocr.CourseID,
                        ocr => ocr.Completion.HasValue ? ocr.Completion.Value : false);

                var surveyRollups = Select.AllColumnsFrom<Data.TrainingProgramAccreditationSurveyRollup>()
                    .Where(Data.TrainingProgramAccreditationSurveyRollup.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                    .And(Data.TrainingProgramAccreditationSurveyRollup.UserIDColumn).IsEqualTo(userId)
                    .WithNoLock()
                    .ExecuteTypedList<Data.TrainingProgramAccreditationSurveyRollup>()
                    .ToDictionary(
                        r => r.OnlineCourseID,
                        r => r);

                var batch = new Data.TrainingProgramAccreditationSurveyRollupCollection();
                foreach (var response in surveyResponses)
                {
                    Data.TrainingProgramAccreditationSurveyRollup roll;
                    if (surveyRollups.ContainsKey(response.Key))
                    {
                        roll = surveyRollups[response.Key];
                    }
                    else
                    {
                        roll = new Data.TrainingProgramAccreditationSurveyRollup
                        {
                            TrainingProgramID = trainingProgramId,
                            UserID = userId,
                            OnlineCourseID = response.Key,
                            ProfessionID = professionId
                        };
                    }

                    roll.IsSurveyComplete = response.Value;
                    batch.Add(roll);

                    if (response.Value)
                    {
                        completeCount++;
                    }
                }

                batch.BatchSave();

                return completeCount == ids.Count;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Updates the training program total time. will run a full rollup if total time exceeds required time
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="trainingProgramId"></param>
        public void UpdateTrainingProgramTotalTime(int trainingProgramId, int userId)
        {
            try
            {
                using (TransactionScope ts = Utilities.CreateTransactionScope())
                {
                    Data.TrainingProgramRollup tprollup = GetRollup(trainingProgramId, userId);

                    if (tprollup == null || tprollup.Id == 0)
                    {
                        tprollup = new Data.TrainingProgramRollup();
                        tprollup.UserID = userId;
                        tprollup.TrainingProgramID = trainingProgramId;
                    }

                    long currentTotalSeconds = tprollup.TotalSeconds;

                    tprollup.TotalSeconds = GetTotalTimeForRequiredCourses(userId, trainingProgramId);

                    // If current seconds was previously less than min course work
                    if ((currentTotalSeconds / 60) < tprollup.MinCourseWork && (tprollup.TotalSeconds / 60) >= tprollup.MinCourseWork)
                    {
                        RunTrainingProgramRollupForUser(trainingProgramId, userId);
                    }

                    tprollup.Save(userId);

                    ts.Complete();
                }
            }
            catch (SqlException e)
            {
                if (e.Number == 2601 || e.Number == 2627)
                {
                    Log.Error("Ignoring duplicate training program rollup.");
                }
                else
                {
                    Log.Error("Could not rollup training program duration for user: " + userId + " and training program: " + trainingProgramId);
                    Log.Error(e);
                }
            }
            catch (Exception e)
            {
                Log.Error("Could not rollup training program duration for user: " + userId + " and training program: " + trainingProgramId);
                Log.Error(e);
            }
        }

        /// <summary>
        /// Rolls up the any training programs assigned to the specified user containing the specified course.
        /// This could take a long time due to possibility of overlapping courses in multiple training programs
        /// this should be called using a background thread
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <param name="courseType"></param>
        public void RunTrainingProgramRollupForCourse(int userId, bool isNewhire, int courseId, CourseType courseType)
        {
            Dictionary<int, int> trainingProgramIds = new Select(
                    Data.TrainingProgramToCourseLink.TrainingProgramIDColumn
                )
                .From<Data.TrainingProgramToCourseLink>()
                .Where(Data.TrainingProgramToCourseLink.CourseIDColumn).IsEqualTo(courseId)
                .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo((int)courseType)
                .ExecuteTypedList<int>()
                .ToDictionary<int, int>(x => x);

            // Get all the users training programs, including the expired ones in case we are updating
            // a class that applies to an expired training program. 
            IEnumerable<TrainingProgramSummaryDetail> trainingProgramsForUser = new PortalController().ListTrainingProgramsForUser(userId, isNewhire, "", "", OrderDirection.Ascending, 0, int.MaxValue, new DateTime(1900, 1, 1), new DateTime(9999, 1, 1)).Data;

            foreach (TrainingProgramSummaryDetail trainingProgram in trainingProgramsForUser)
            {
                if (trainingProgramIds.ContainsKey(trainingProgram.Id))
                {
                    // Not passing in the course id or the type here since we are already calling this for all 
                    // training programs this user is assigned to that have this course.
                    EnqueueTrainingProgramRollup(trainingProgram.Id, new int[] {userId});
                }
            }
        }

        /// <summary>
        /// Enqueues and update for training program total time.
        /// </summary>
        /// <param name="userId">User to set time for</param>
        /// <param name="trainingProgramId">Training program to set time for</param>
        public void EnqueueUpdateTrainingProgramTotalTime(int trainingProgramId, int userId)
        {
            BackgroundJob.Enqueue(() => UpdateTrainingProgramTotalTime(trainingProgramId, userId));
        }

        /// <summary>
        /// Adds a background job to hangfire for training program rollup for multiple users
        /// This will create a separate background job per user
        /// </summary>
        /// <param name="trainingProgramId">The training program to roll up</param>
        /// <param name="userIds">All of the users to include in the rollup</param>
        [Queue("critical")]
        public void EnqueueTrainingProgramRollup(int trainingProgramId, int[] userIds)
        {
            foreach (int id in userIds)
            {
                BackgroundJob.Enqueue(() => RunDefaultTrainingProgramRollupForUser(trainingProgramId, id));
            }
        }

        public void EnqueueTrainingProgramRollups(int trainingProgramId, int[] userIds)
        {
            BackgroundJob.Enqueue(() => EnqueueTrainingProgramRollup(trainingProgramId, userIds));
        }

        /// <summary>
        /// updates the training program rollup for a specific user. Will rollup other training programs that
        /// contain the course that triggered this rollup.
        /// </summary>
        /// <param name="userId">The user id that triggered the rollup</param>
        /// <param name="trainingProgramId">Training program that triggered the rollup - this tp will for sure be rolled up</param>
        /// <param name="courseId">The course id that triggered the rollup. Any other training programs with this course id will also be rolled up</param>
        /// <param name="courseTypeId">The course type that triggered the rollup - no sense in rolling up other training programs with a classroom course that matches
        /// the online course id or vice versa.</param>
        public void EnqueueTrainingProgramRollup(int trainingProgramId, int userId, int courseId, CourseType courseType)
        {
            // In this case we are rolling up a single training program after a change to a particular course
            // when we do this, we must also check other training programs for this user that might share the same
            // courses. We will have to roll up those training programs as well to ensure the rollup remains
            // accurate.
            //
            // The course id is the course that triggered the rollup. The rollup wil use this course id to look
            // up all other training programs for the user with this course.
            //
            // This is not a case where the profession should change, so this is left as null for all rollups.
            BackgroundJob.Enqueue(() => RunCriticalTrainingProgramRollupForUser(trainingProgramId, userId, courseId, courseType));
        }

        /// <summary>
        /// Runs a training program rollup for all training programs that have the specified course.
        /// Classroom courses are updated usually without the training program context by saving
        /// the classroom itself. So every time we save a score for a class, we have to check
        /// the training programs that contain the course for the user.
        /// </summary>
        /// <param name="userId">User to rollup training programs for</param>
        /// <param name="courseId">Course id to rollup - for looking up other training programs with this course</param>
        /// <param name="courseType">Type of the course</param>
        public void EnqueueTrainingProgramRollup(List<int> userIds, int courseId, CourseType courseType)
        {
            IEnumerable<int[]> userIdGroups = userIds.Select(i => i).InGroupsOf(2000);
            List<User> users = new List<User>();
            
            foreach (int[] userIdGroup in userIdGroups)
            {
                users.AddRange(Select
                    .AllColumnsFrom<Data.User>()
                    .Where(Data.User.Columns.Id)
                    .In(userIdGroup)
                    .ExecuteTypedList<User>());
            }

            foreach (User user in users)
            {
                BackgroundJob.Enqueue(() => RunTrainingProgramRollupForCourse(user.ID, user.NewHireIndicator, courseId, courseType));
            }
        }

        private void _GenerateProctorKeysForAllFinallAssessments(int trainingProgramId, int userId)
        {
            SqlQuery subQuery = new Select(Data.TrainingProgramToCourseLink.CourseIDColumn)
                .From<Data.TrainingProgramToCourseLink>()
                .Where(Data.TrainingProgramToCourseLink.SyllabusTypeIDColumn)
                    .IsEqualTo(SyllabusType.Final)
                .And(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn)
                    .IsEqualTo(trainingProgramId);

            var res = subQuery.ExecuteTypedList<int>();

            foreach(int courseId in res) {
                SetProctorKey(courseId, userId, trainingProgramId);
            }
        }

        public void SetProctorKey(int courseId, int userId, int trainingProgramId)
        {
            var proctorKey = GenerateProctorKey();
            Data.OnlineCourseRollup ocr = new Data.OnlineCourseRollup()
            {
                CourseID = (int)courseId,
                TrainingProgramID = trainingProgramId,
                UserID = userId,
                ProctorKey = proctorKey
            };

            ocr.Save();
        }

        public string GenerateProctorKey()
        {
            string raw = System.Web.Security.Membership.GeneratePassword(10, 1);
            string b64 = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(raw));

            return b64.Substring(0, 7).ToUpper();
        }
    }
}
