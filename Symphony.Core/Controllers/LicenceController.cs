﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Symphony.Core.Models;
using System.Configuration;
using SubSonic;
using System.Text.RegularExpressions;
using System.Transactions;

namespace Symphony.Core.Controllers
{
    public class LicenceController : Controllers.SymphonyController
    {

        public static SingleResult<Licence> SaveLicence(int licenceId, Licence licence, bool reparent = false)
        {
            Data.Licence licenceData = new Data.Licence(licenceId);

            if (reparent) // just change the parent id
            {
                licenceData.ParentID = licence.ParentId;
                licenceData.Save();
            }
            else // save the full license
            {
                if (licence != null)
                {
                    licence.CopyTo(licenceData);
                }

                licenceData.Id = licenceId;

                if (licenceData.JurisdictionID == 0 && !string.IsNullOrWhiteSpace(licence.Jurisdiction))
                {
                    var j = new Data.Jurisdiction(Data.Jurisdiction.Columns.Name, licence.Jurisdiction);
                    if (j.Id == 0)
                    {
                        j.Name = licence.Jurisdiction;
                        j.Save();
                    }
                    licenceData.JurisdictionID = j.Id;
                }

                if (licenceData.ProfessionID == 0 && !string.IsNullOrWhiteSpace(licence.Profession))
                {
                    var p = new Data.Profession(Data.Profession.Columns.Name, licence.Profession);
                    if (p.Id == 0)
                    {
                        p.Name = licence.Profession;
                        p.Save();
                    }
                    licenceData.ProfessionID = p.Id;
                }

                // save the license
                licenceData.RequiredUserFields = string.Join(",", licence.UserFields.ToArray());
                licenceData.Save();

                // save the data fields
                List<Data.LicenseDataField> fields = new List<Data.LicenseDataField>();
                foreach (var fieldName in licence.DataFields)
                {
                    if (string.IsNullOrWhiteSpace(fieldName))
                    {
                        continue;
                    }

                    int id = 0;
                    Data.LicenseDataField field = null;
                    if (int.TryParse(fieldName, out id))
                    {
                        field = new Data.LicenseDataField(id);
                    }
                    else
                    {
                        field = new Data.LicenseDataField(Data.LicenseDataField.Columns.Name, fieldName);
                    }

                    if (field.Id == 0)
                    {
                        field.Name = fieldName;
                        field.Save();
                    }
                    fields.Add(field);
                }

                // map the data fields to the license
                var query = Select.AllColumnsFrom<Data.LicenseDataFieldsToLicensesMap>()
                    .Where(Data.LicenseDataFieldsToLicensesMap.Columns.LicenseID).IsEqualTo(licenceData.Id);
                query.QueryCommandType = SqlQuery.QueryType.Delete;
                query.Execute();
                foreach (var field in fields)
                {
                    Data.LicenseDataFieldsToLicensesMap map = new Data.LicenseDataFieldsToLicensesMap();
                    map.LicenseDataFieldID = field.Id;
                    map.LicenseID = licenceData.Id;
                    map.Save();
                }
            }

            return LicenceController.GetLicence(licenceData.Id);
        }

        public static SingleResult<bool> DeleteLicence(int licenceId)
        {
            Data.Licence licence = new Data.Licence(licenceId);

            if (!true)
            {
                return new SingleResult<bool>(new Exception("You do not have permission to update this licence."));
            }

            if (licence.Id == 0 && licenceId > 0)
            {
                return new SingleResult<bool>(new Exception("The specified licence couldn't be found."));
            }

            Data.Licence.Delete(licenceId);

            return new SingleResult<bool>(true);
        }

        public static SingleResult<Profession> SaveProfession(int professionId, Profession profession)
        {
            Data.Profession professionData = new Data.Profession(professionId);

            if (profession != null)
            {
                profession.CopyTo(professionData);
            }

            professionData.Id = professionId;

            professionData.Save();

            return LicenceController.GetProfession(professionData.Id);
        }

        public static SingleResult<Profession> GetProfession(int professionId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Profession>()
                .Where(Data.Profession.IdColumn).IsEqualTo(professionId);

            return new SingleResult<Profession>(query);
        }

        public static SingleResult<Jurisdiction> GetJurisdiction(int jurisdictionId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Jurisdiction>()
                .Where(Data.Jurisdiction.IdColumn).IsEqualTo(jurisdictionId);

            return new SingleResult<Jurisdiction>(query);
        }

        public static SingleResult<Jurisdiction> SaveJurisdiction(int jurisdictionId, Jurisdiction jurisdiction)
        {
            Data.Jurisdiction jurisdictionData = new Data.Jurisdiction(jurisdictionId);

            if (jurisdiction != null)
            {
                jurisdiction.CopyTo(jurisdictionData);
            }

            jurisdictionData.Id = jurisdictionId;

            jurisdictionData.Save();

            return LicenceController.GetJurisdiction(jurisdictionData.Id);
        }

        public static SingleResult<Jurisdiction> DeleteJurisdiction()
        {
            throw new NotImplementedException();
        }

        public static SingleResult<Licence> GetLicence(int LicenceId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Licence>()
                .IncludeColumn(Data.Profession.NameColumn.QualifiedName, "Profession")
                .IncludeColumn(Data.Jurisdiction.NameColumn.QualifiedName, "Jurisdiction")
                .LeftOuterJoin(Data.Profession.IdColumn, Data.Licence.ProfessionIDColumn)
                .LeftOuterJoin(Data.Jurisdiction.IdColumn, Data.Licence.JurisdictionIDColumn)
                .Where(Data.Licence.IdColumn).IsEqualTo(LicenceId);

            var result = new SingleResult<Licence>(query);

            var dataFields = Select.AllColumnsFrom<Data.LicenseDataField>()
                .Where(Data.LicenseDataField.Columns.Id)
                .In(
                    new Select(Data.LicenseDataFieldsToLicensesMap.Columns.LicenseDataFieldID)
                    .From<Data.LicenseDataFieldsToLicensesMap>()
                    .Where(Data.LicenseDataFieldsToLicensesMap.Columns.LicenseID).IsEqualTo(LicenceId)
                )
                .ExecuteTypedList<Data.LicenseDataField>();

            result.Data.DataFields = dataFields.Select(s => s.Name).ToList();
            if (result.Data.RequiredUserFields.Trim().Length > 0)
            {
                result.Data.UserFields = result.Data.RequiredUserFields.Split(',').Select(s => s.Trim()).ToList();
            }
            else
            {
                result.Data.UserFields = new List<string>();
            }

            return result;
        }

        public PagedResult<Licence> GetLicences(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LicenseHierarchy>();

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query = query.Where(Data.Licence.NameColumn).ContainsString(searchText);
            }

            var res = new PagedResult<Licence>(query, pageIndex, pageSize, Data.JobRoleHierarchy.Columns.LevelIndentText, OrderDirection.Ascending);
            return res;
        }

        public MultipleResult<TrainingProgramLicence> GetLicencesForTrainingProgram(int trainingProgramId, string searchText)
        {
            Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);

            if (tp.ParentTrainingProgramID.HasValue && tp.ParentTrainingProgramID.Value > 0)
            {
                // Child training program, we will want to pull the licences from
                // the parent training program instead of having to copy them when
                // we create the training program copies for salesforce
                trainingProgramId = tp.ParentTrainingProgramID.Value;
            }

            List<string> columns = Data.Licence.Schema.Columns.Select(c => c.QualifiedName).ToList();
            columns.AddRange(Data.TrainingProgramLicence.Schema.Columns.Where(c => c.ColumnName != "ID").Select(c => c.QualifiedName).ToList());

            SqlQuery query = new Select(columns.ToArray()).From<Data.TrainingProgramLicence>()
                .InnerJoin(Data.Licence.IdColumn, Data.TrainingProgramLicence.LicenceIDColumn)
                .Where(Data.TrainingProgramLicence.TrainingProgramIDColumn)
                .IsEqualTo(trainingProgramId);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.And(Data.Licence.NameColumn).ContainsString(searchText);
            }

            var result = new MultipleResult<TrainingProgramLicence>(query);
            foreach (var license in result.Data)
            {
                var dataFields = Select.AllColumnsFrom<Data.LicenseDataField>()
                    .Where(Data.LicenseDataField.Columns.Id)
                    .In(
                        new Select(Data.LicenseDataFieldsToLicensesMap.Columns.LicenseDataFieldID)
                        .From<Data.LicenseDataFieldsToLicensesMap>()
                        .Where(Data.LicenseDataFieldsToLicensesMap.Columns.LicenseID).IsEqualTo(license.Id)
                    )
                    .ExecuteTypedList<Data.LicenseDataField>();


                license.DataFields = dataFields.Select(s => s.Name).ToList();

                if (!string.IsNullOrWhiteSpace(license.RequiredUserFields))
                {
                    license.UserFields = license.RequiredUserFields.Split(',').ToList();
                }
                else
                {
                    license.UserFields = new List<string>();
                }
            }

            return result;
        }

        public MultipleResult<TrainingProgramLicence> SaveLicencesForTrainingProgram(int trainingProgramId, List<TrainingProgramLicence> licences)
        {
            new Delete().From<Data.TrainingProgramLicence>()
                .Where(Data.TrainingProgramLicence.TrainingProgramIDColumn)
                .IsEqualTo(trainingProgramId)
                .Execute();

            Data.TrainingProgramLicenceCollection newLicences = new Data.TrainingProgramLicenceCollection();

            var hasPrimary = false;

            foreach (TrainingProgramLicence l in licences)
            {
                Data.TrainingProgramLicence newLicence = new Data.TrainingProgramLicence();
                newLicence.ApprovalCode = l.ApprovalCode;
                newLicence.TrainingProgramID = trainingProgramId;
                newLicence.LicenceID = l.Id;
                newLicence.IsPrimary = l.IsPrimary;

                if (l.IsPrimary)
                {
                    hasPrimary = true;
                }

                newLicences.Add(newLicence);
            }

            // Make the first license default to primary
            if (!hasPrimary)
            {
                if (licences.Count > 0)
                {
                    newLicences[0].IsPrimary = true;
                }
            }

            newLicences.BatchSave();

            return GetLicencesForTrainingProgram(trainingProgramId, "");
        }

        public PagedResult<Profession> GetProfessions(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Profession>()
               .Where(Data.Licence.Columns.Name).ContainsString(searchText);

            return new PagedResult<Profession>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Jurisdiction> GetJurisdictions(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Jurisdiction>()
               .Where(Data.Licence.Columns.Name).ContainsString(searchText);

            return new PagedResult<Jurisdiction>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public MultipleResult<LicenseDataField> GetLicenceDataFields(string searchText, string sort, OrderDirection orderDirection, int p, int limit)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LicenseDataField>();
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.Where(Data.LicenseDataField.Columns.Name).ContainsString(searchText);
            }

            return new MultipleResult<LicenseDataField>(query);
        }
    }
}
