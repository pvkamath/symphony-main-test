﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using SubSonic;
using Symphony.Web;
using System.Web.Security;
using log4net;
using Symphony.Core.Models;
using Symphony.Core.GTM;
using Symphony.Core.CNRService;
using System.Net.Mail;
using Data = Symphony.Core.Data;
using System.Web.Configuration;
using System.Configuration;
using System.Security.Cryptography;
using Symphony.Core.Extensions;
using System.Reflection;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Symphony.ExternalSystemIntegration.Controllers;

namespace Symphony.Core.Controllers
{
    public class UserController : SymphonyController
    {
        private class MarsSignOn
        {
            public string UserID { get; set; }

            public string LaunchSite { get; set; }
        }

        private string USER_COUNT_PREFIX = "USER_COUNT";
        private ILog Log = LogManager.GetLogger(typeof(UserController));

        /// <summary>
        /// Determines if a given user is a sales channel admin
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool IsSalesChannelAdmin(int userId)
        {
            Data.User user = new Data.User(userId);
            return user.SalesChannelID > 0;
        }

        public bool IsSalesChannelAdmin(string username, string applicationName)
        {
            if (string.IsNullOrEmpty(username) || applicationName == ApplicationProcessor.NOT_SET)
            {
                return false;
            }
            return GetUser(username, applicationName).SalesChannelID > 0;
        }

        public bool CurrentUserIsSalesChannelAdmin()
        {
            return IsSalesChannelAdmin(UserID);
        }

        public bool UserIsGTMOrganizer(int userId)
        {
            Data.GTMOrganizer cOrganizer = new Data.GTMOrganizer(Data.GTMOrganizer.Columns.UserID, userId);
            return cOrganizer.Id > 0 && cOrganizer.Status == GTMController.OrganizerStatusActive;
        }

        public enum UserPermissionType
        {
            Location,
            JobRole,
            Audience
        }

        public Models.User GetUser(string username, string applicationName)
        {
            int userId = CacheController.GetValue<int>(username + applicationName);

            if (userId != default(int))
            {
                return GetUser(userId);
            }

            User user = new Select(Data.User.IdColumn)
                .From<Data.User>()
                .InnerJoin<Data.Customer>()
                .Where(Data.User.Columns.Username).IsEqualTo(username)
                .And(Data.Customer.Columns.SubDomain).IsEqualTo(applicationName)
                .ExecuteSingle<Models.User>();

            if (user == null)
            {
                return null;
            }

            CacheController.AddValue<int>(username + applicationName, user.ID);

            return GetUser(user.ID);
        }

        public Models.User GetUser(int id)
        {
            Models.User user = CacheController.Get<Models.User>(id);

            if (user == null)
            {
                user = Select.AllColumnsFrom<Data.User>()
                    .Where(Data.User.Columns.Id).IsEqualTo(id)
                    .ExecuteSingle<Models.User>();
            }

            if (user != null)
            {
                if (user.HireDate.HasValue)
                {
                    user.HireDate = user.HireDate.Value.WithUtcFlag();
                }

                CacheController.Add<Models.User>(user.ID, user);
            }

            return user;
        }

        public static List<User> GetUsersByRole(int customerId, int userId, string role, string searchText)
        {
            /*
            select * from [User] u
                inner join [Customer] c
                    on c.ID = u.CustomerID
                inner join aspnet_Applications aspa
                    on aspa.ApplicationName = c.SubDomain
                inner join aspnet_Users aspu
                    on aspu.UserName = u.Username
                    and aspu.ApplicationId = aspa.ApplicationId
                inner join aspnet_UsersInRoles aspuir
                    on aspuir.UserId = aspu.UserId
                inner join aspnet_Roles aspr
                    on aspr.RoleId = aspuir.RoleId
                where c.ID = CUSTOMERID
                    and aspr.RoleName = ROLE
                order by FirstName, MiddleName, LastName
            */

            string aspnetApplications = "aspnet_Applications";
            string aspnetUsers = "aspnet_Users";
            string aspnetUsersInRoles = "aspnet_UsersInRoles";
            string aspnetRoles = "aspnet_Roles";

            // don't use null values
            searchText = "%" + (searchText ?? string.Empty) + "%";

            SqlQuery query = Select.AllColumnsFrom<Data.User>()
                .InnerJoin(Data.Tables.Customer, Data.Customer.Columns.Id, Data.Tables.User, Data.User.Columns.CustomerID)
                .InnerJoin(aspnetApplications, "ApplicationName", Data.Tables.Customer, Data.Customer.Columns.SubDomain)
                .InnerJoin(aspnetUsers, "UserName", Data.Tables.User, Data.User.Columns.Username)
                .InnerJoin(aspnetUsersInRoles, "UserId", aspnetUsers, "UserId")
                .InnerJoin(aspnetRoles, "RoleId", aspnetUsersInRoles, "RoleId")
                .Where(Data.Customer.IdColumn.QualifiedName).IsEqualTo(customerId)
                .And("[dbo].[" + aspnetRoles + "].[RoleName]").IsEqualTo(role)
                // ugh, can't join on multiple columns...
                // this will be replaced below using inline query
                .And("[dbo].[" + aspnetUsers + "].[ApplicationId]").IsEqualTo("[dbo].[" + aspnetApplications + "].[ApplicationId]")
                // the search criteria
                .AndExpression(Data.User.FirstNameColumn.QualifiedName).Like(searchText)
                .Or(Data.User.MiddleNameColumn.QualifiedName).Like(searchText)
                .Or(Data.User.LastNameColumn.QualifiedName).Like(searchText)
                .CloseExpression()
                //.And(Data.User.StatusIDColumn.QualifiedName).IsEqualTo((int)UserStatusType.Active)
                .OrderAsc(
                    Data.User.FirstNameColumn.QualifiedName,
                    Data.User.MiddleNameColumn.QualifiedName,
                    Data.User.LastNameColumn.QualifiedName);

            // fix up the query presented above
            string s = query.BuildSqlStatement();
            s = s.Replace(query.Constraints[2].ParameterName, "[dbo].[" + aspnetApplications + "].[ApplicationId]");

            return new InlineQuery().ExecuteTypedList<User>(s, customerId, role, searchText, searchText, searchText);
        }

        public User CreateExternalUser(int customerId, string email)
        {
            Data.UserCollection exists = new Data.UserCollection()
                .Where(Data.User.Columns.Username, email)
                .Where(Data.User.Columns.CustomerID, customerId)
                .Load();

            if (exists.Count > 0)
            {
                // create a user from the data object
                if (exists[0].IsExternal)
                    return Model.Create<User>(exists[0]);

                return null;
            }
            User user = new User();
            user.CustomerID = customerId;
            user.SalesChannelID = 0;
            user.FirstName = "External User";
            user.MiddleName = "";
            user.LastName = email;
            user.Email = email;
            user.Username = email;

            user.Password = GetExternalUserPassword(email);
            user.EmployeeNumber = email;
            user.HireDate = DateTime.UtcNow.Date;
            user.NewHireIndicator = false;
            user.LocationID = 0;
            user.JobRoleID = 0;
            user.TelephoneNumber = "";
            user.IsExternal = true;
            user.StatusID = (int)UserStatusType.Active;

            this.Save(user);

            return user;
        }

        public int GetCurrentUserCount()
        {
            // check the cache
            object cached = HttpContext.Current.Cache[USER_COUNT_PREFIX + CustomerID];
            if (cached != null)
            {
                return (int)cached;
            }

            // get the user count, excluding external and deleted users
            int count = Select.AllColumnsFrom<Data.User>()
                .Where(Data.User.Columns.CustomerID).IsEqualTo(CustomerID)
                .And(Data.User.Columns.IsExternal).IsEqualTo(false)
                .And(Data.User.Columns.StatusID).IsNotEqualTo((int)UserStatusType.Deleted)
                .GetRecordCount();

            // cache it
            HttpContext.Current.Cache.Add(USER_COUNT_PREFIX + CustomerID, count, null, Cache.NoAbsoluteExpiration, new TimeSpan(12, 0, 0), CacheItemPriority.Normal, null);

            // return it
            return count;
        }

        public int GetMaxUserCount(int customerID, bool forceAccountCreation = false)
        {
            Data.Customer c = new Data.Customer(Data.Customer.Columns.Id, CustomerID);
            if (c.Id == 0)
            {
                // If there is no customer ID for the logged in user, the passed account might be set up to handle self account creation
                // So check for that here
                c = new Data.Customer(Data.Customer.Columns.Id, customerID);
                if (c.Id == 0) throw new Exception("Invalid customer ID specified when determining user count.");
                if (!c.AllowSelfAccountCreation && !forceAccountCreation) throw new Exception("Sorry, This customer doesn't allow self account creation.");
            }

            return c.UserLimit;
        }

        public void ValidateSupervisor(User user)
        {
            // cannot be one's own supervisor - infinite recursion
            if (user.SupervisorID == user.ID)
                user.SupervisorID = 0;

            if (user.SecondarySupervisorID == user.ID)
                user.SecondarySupervisorID = 0;

            // Don't allow the secondary supervisor to be the same as the primary
            if (user.SecondarySupervisorID == user.SupervisorID)
                user.SecondarySupervisorID = 0;

            // Just because it looks weird if there is a secondary but no primary supervisor
            // they both do exactly the same thing so it doesn't matter who they put where
            // removed by jerod, 10/14, was causing confusion
            /*if (user.SupervisorID < 1 && user.SecondarySupervisorID >= 1)
            {
                user.SupervisorID = user.SecondarySupervisorID;
                user.SecondarySupervisorID = 0;
            }*/
        }

        public void ValidateReportingSupervisor(User user)
        {
            // cannot be one's own reporting supervisor - infinite recursion
            if (user.ReportingSupervisorID == user.ID)
                user.ReportingSupervisorID = 0;
        }

        public void ValidateUserCount(User user, bool forceAccountCreation = false)
        {
            // never bother with this check for sales channel admins
            if (!CurrentUserIsSalesChannelAdmin() && GetCurrentUserCount() >= GetMaxUserCount(user.CustomerID, forceAccountCreation))
            {
                if (user.ID > 0)
                {
                    Data.User current = new Data.User(user.ID);
                    // if they're activating a user, that should fail too...
                    // but they can change users from active to deleted, even if they're at the limit
                    if (current.StatusID == (int)UserStatusType.Deleted && user.StatusID == (int)UserStatusType.Active)
                    {
                        throw new Exception("You have reached the maximum number of users. You cannot activate new users until you delete existing users or upgrade your account.");
                    }
                }
                else
                {
                    // new user, but they're at the limit
                    throw new Exception("You have reached the maximum number of users. Please delete some users, or contact support for assistance.");
                }
            }
        }

        public void ValidateSalesChannel(User user, int overrideCustomerID = 0)
        {
            if (CurrentUserIsSalesChannelAdmin())
            {
                // sales channel admins get put in the "BE" bank
                if (user.CustomerID == 0 && user.SalesChannelID > 0)
                    user.CustomerID = new Data.Customer(Data.Customer.Columns.SubDomain, "be").Id;
            }
            else
            {
                // non-sales-channel-admins don't get an option - always set to the customer id
                user.CustomerID = (overrideCustomerID > 0) ? overrideCustomerID : CustomerID;
                user.SalesChannelID = 0;
            }
        }

        public void ValidateUserNameNumberAndHireDate(User user)
        {
            Data.UserCollection exists = new Data.UserCollection()
                    .Where(Data.User.Columns.EmployeeNumber, user.EmployeeNumber)
                    .Where(Data.User.Columns.CustomerID, user.CustomerID)
                    .Load();

            // if we have a user w/ that employee # and customer id, make sure that it's different than the one 
            // we're trying to save, and if so, error
            if (exists.Count > 0 && exists[0].Id != user.ID && user.SalesChannelID <= 0)
            {
                throw new Exception("The employee '" + exists[0].FirstName + ' ' + exists[0].LastName + "' already has the employee number specified.");
            }

            // validate the username is unique
            Data.UserCollection exists2 = new Data.UserCollection()
                .Where(Data.User.Columns.Username, user.Username)
                .Where(Data.User.Columns.CustomerID, user.CustomerID)
                .Load();

            if (exists2.Count > 0 && exists2[0].Id != user.ID)
                throw new Exception("The username '" + exists2[0].Username + "' is already in use for this customer. Please try a different username.");

            // we'll put this in the bypass because it shouldn't ever cause problems with imports
            // if they don't specify a new hire date, it'll just default to 1900.
            // anything less than 1950 is considered invalid.
            if (user.NewHireIndicator && (!user.HireDate.HasValue || user.HireDate.Value.Year < 1950))
            {
                throw new Exception("A valid hire date is required for new hires.");
            }
        }

        private void UpdateReportingPermissions(Data.User storedUser, User user)
        {
            if (user.UserReportingPermissions == null)
            {
                // User didn't choose UserReportPermission tab and no data has been changed.
                return;
            }
            List<Data.UserReportingPermissionsType> permissiontypes = Select.AllColumnsFrom<Data.UserReportingPermissionsType>().ExecuteTypedList<Data.UserReportingPermissionsType>();

            List<UserReportingPermission> userReportingPermissions = Select.AllColumnsFrom<Data.UserReportingPermission>()
                .IncludeColumn(Data.UserReportingPermissionsType.Columns.Name, "permissionType")
                .IncludeColumn(Data.UserReportingPermissionsType.Columns.Code, "permissionTypeCode")
                .InnerJoin(Data.Tables.UserReportingPermissionsType, Data.UserReportingPermissionsType.Columns.Id, Data.Tables.UserReportingPermission, Data.UserReportingPermission.Columns.PermissionTypeId)
                .Where(Data.UserReportingPermission.Columns.UserId).IsEqualTo(user.ID).ExecuteTypedList<UserReportingPermission>();
            var newPermSet = user.UserReportingPermissions.Where(p => userReportingPermissions.Count(ep => ep.ObjectId == p.ObjectId && ep.PermissionTypeCode == p.PermissionType.ToLower()) == 0).ToList();

            storedUser.UserReportingPermissions.Clear();

            foreach (UserReportingPermission urp in newPermSet)
            {
                var permtypeid = permissiontypes.Where(pt => pt.Name.ToLower() == urp.PermissionType.ToLower()).First().Id;
                storedUser.UserReportingPermissions.Add(new Data.UserReportingPermission() { UserId = user.ID, PermissionTypeId = permtypeid, ObjectId = urp.ObjectId });
            }

            if (storedUser.UserReportingPermissions.Count > 0)
            {
                storedUser.UserReportingPermissions.SaveAll();
            }

            List<UserReportingPermission> delPermSet = userReportingPermissions.Where(p => user.UserReportingPermissions.Count(up => up.ObjectId == p.ObjectId && up.PermissionType.ToLower() == p.PermissionType.ToLower()) == 0).ToList();

            if (delPermSet.Count > 0)
            {
                new Delete().From<Data.UserReportingPermission>().Where(Data.UserReportingPermission.UserIdColumn).IsEqualTo(user.ID)
                    .And(Data.UserReportingPermission.IdColumn).In(delPermSet.Select(p => p.ID)).Execute();
            }

        }

        public void CreateNewASPNetUser(User user, string subdomain, bool overrideSubdomain = false)
        {
            if (SymphonyMembershipProvider.GetUser(subdomain, user.Username) == null)
            {
                // default password if not set
                if (string.IsNullOrEmpty(user.Password))
                    user.Password = "bankersedge123!";

                // create the user in the asp.net tables
                MembershipCreateStatus status;
                SymphonyMembershipProvider.CreateUser(subdomain, user.Username, user.Password, user.Email, "foo", "bar", true, out status, overrideSubdomain);
                if (status != MembershipCreateStatus.Success)
                {
                    switch (status)
                    {
                        case MembershipCreateStatus.DuplicateUserName:
                            throw new Exception("Create failed! The username you have specified is already in use.");
                        case MembershipCreateStatus.DuplicateEmail:
                            throw new Exception("Create failed! The email address you have specified is already in use.");
                        case MembershipCreateStatus.InvalidEmail:
                            throw new Exception("Create failed! The email address you have specified is invalid.");
                        case MembershipCreateStatus.InvalidPassword:
                            throw new Exception("Create failed! The password you have specified does not meet the minimum requirements. Passwords must be at least 7 characters.");
                        default:
                            throw new Exception("Create failed! " + status.ToString());
                    }
                }
            }
            else
            {
                // technically, the checks before this should prevent this from occuring,
                // but better safe than sorry
                throw new Exception("The specified username is already in use.");
            }
        }

        public void UpdateExistingASPNetUser(User user, string subdomain)
        {
            // prevent the ability to rename yourself
            if (UserID == user.ID)
            {
                // set the username in the incoming user to be the current user's username
                user.Username = Username;
            }

            // get the user being updated
            MembershipUser mu = SymphonyMembershipProvider.GetUser(subdomain, user.Username);

            // this should not be null, but we'll do a sanity check and wrap it anyway
            if (mu != null)
            {
                // set the email and update
                mu.Email = user.Email;
                SymphonyMembershipProvider.UpdateUser(subdomain, mu);

                // change the password if it was set
                if (user.Password != null && user.Password.Trim() != "")
                {
                    SetPassword(subdomain, user.Username, user.Password);
                }
            }
        }

        private void SetPassword(string subdomain, string username, string password)
        {
            MembershipUser mu = SymphonyMembershipProvider.GetUser(subdomain, username);
            if (mu != null)
            {

                // password change => unlock user
                // Unlocking the user first in order to change the password
                if (mu.IsLockedOut)
                {
                    SymphonyMembershipProvider.UnlockUser(subdomain, mu, false);
                }

                string sNew = SymphonyMembershipProvider.ResetPassword(subdomain, mu, "bar");
                try
                {
                    if (!SymphonyMembershipProvider.ChangePassword(subdomain, mu, sNew, password))
                    {
                        throw new Exception("Update failed! The password you have specified does not meet the minimum requirements. Passwords must be 7 characters and include a number and a letter.");
                    }
                }
                catch (ArgumentException ex)
                {
                    Log.Error(ex);
                    throw new Exception("Update failed! The password you have specified does not meet the minimum requirements. Passwords must be 7 characters and include a number and a letter.");
                }
            }
        }

        public void UpdateUserRoles(User user, string subdomain)
        {
            Data.User target = new Data.User(user.ID);

            if (target.Id != UserID && target.SalesChannelID > 0)
            {
                // sales channel user - they automatically get all roles, period
                // but only other sales channel admins can do this
                if (CurrentUserIsSalesChannelAdmin())
                {
                    SymphonyRoleProvider.AddUserToRoles(subdomain, target.Username, Symphony.Core.Roles.GetAllRoles(), target.CustomerID);
                }
            }

            // if the user being edited is NOT the logged in user, allow this to continue
            // if they are the same, weird things happen if you change permissions
            else if (!(UserID == user.ID) && user.ApplicationPermissions != null && user.ApplicationPermissions.Count() > 0)
            {
                // if the customer id is > 0, they're a customer-level user, 
                // in which case, they get whatever role is assigned them
                if (user.CustomerID > 0)
                {
                    // start by grabbing all the current roles
                    string[] existingRoles = SymphonyRoleProvider.GetRolesForUser(subdomain, target.Username);

                    // remove the user from all roles
                    if (existingRoles.Length > 0)
                        SymphonyRoleProvider.RemoveUserFromRoles(subdomain, target.Username, existingRoles);

                    // and add the new ones
                    SymphonyRoleProvider.AddUserToRoles(subdomain, target.Username, user.ApplicationPermissions.ToArray(), target.CustomerID);

                    // check how organizer role has changed
                    bool wasOrganizer = false;
                    bool isOrganizer = false;
                    foreach (string existingRole in existingRoles)
                    {
                        if (existingRole.ToLower() == "collaboration - gtm organizer")
                            wasOrganizer = true;
                    }
                    foreach (string newRole in user.ApplicationPermissions)
                    {
                        if (newRole.ToLower() == "collaboration - gtm organizer")
                            isOrganizer = true;
                    }

                    // deactivate organizer
                    //TODO: implement organizer goodies
                    if (wasOrganizer && !isOrganizer)
                    {
                        // check to see if user is an instructor for a virtual class
                        foreach (Data.ClassX klass in new Data.ClassXCollection().Where(Data.ClassX.Columns.IsVirtual, true).Load())
                        {
                            if (new Data.ClassInstructorCollection().Where(Data.ClassInstructor.Columns.ClassID, klass.Id).Where(Data.ClassInstructor.Columns.InstructorID, user.ID).Load().Count > 0)
                            {
                                SymphonyRoleProvider.AddUserToRole(subdomain, target.Username, Roles.GTMOrganizer);
                                throw new Exception("Cannot remove collaboration organizer role.  User is an instructor in a virtual class.");
                            }
                        }

                        try
                        {
                            DeactivateOrganizer(user.ID);
                        }
                        catch (Exception ex)
                        {
                            SymphonyRoleProvider.AddUserToRole(subdomain, target.Username, Roles.GTMOrganizer);
                            throw new Exception("User could not be deactivated as a collaboration GTM organizer: " + ex.Message);
                        }
                    }
                    // activate organizer
                    else if (!wasOrganizer && isOrganizer)
                    {
                        try
                        {
                            ActivateOrganizer(user.ID, user.CustomerID);
                        }
                        catch (Exception ex)
                        {
                            if (user.ID == 0)
                            {
                                //If new user - .net may have some residual stuff from earlier save operation clean out .net membership user
                                if (existingRoles.Length > 0)
                                    SymphonyRoleProvider.RemoveUserFromRoles(subdomain, target.Username, existingRoles);
                                SymphonyMembershipProvider.DeleteUser(subdomain, target.Username);
                            }
                            else
                                SymphonyRoleProvider.RemoveUserFromRole(subdomain, user.Username, Roles.GTMOrganizer);
                            throw new Exception("User could not be activated as a collaboration GTM organizer: " + ex.Message);
                        }
                    }
                }
            }
        }

        private void DeactivateOrganizer(int userID)
        {
            GTMController gtm = new GTMController();
            Data.GTMOrganizer gtmOrganizer = new Data.GTMOrganizer(Data.GTMOrganizer.Columns.UserID, userID);
            if (gtmOrganizer.Id > 0)
            {
                Organizer organizer = gtm.GetOrganizer(gtmOrganizer.OrganizerKey);

                gtmOrganizer.UserID = 0;
                gtmOrganizer.Status = GTMController.OrganizerStatusSuspended;
                gtmOrganizer.Save();

                if (GTMController.GetFieldValue(organizer.organizerFields, "status").ToLower() != GTMController.OrganizerStatusSuspended.ToLower())
                    gtm.SetOrganizerStatus(gtmOrganizer.OrganizerKey, GTMController.OrganizerStatusSuspended);
            }
        }

        public void ActivateOrganizer(int userID, int customerId)
        {
            GTMController gtm = new GTMController();
            //int customerID = CustomerID;

            Data.Customer customer = new Data.Customer(customerId);

            if (customer.OrganizerSeats == 0)
                throw new Exception("Customer is not currently licensed for collaboration.");

            Data.GTMOrganizerCount gtmOrganizerCount = new Data.GTMOrganizerCount(Data.GTMOrganizerCount.Columns.CustomerID, customerId);
            if (gtmOrganizerCount.OrganizerCount >= customer.OrganizerSeats)
                throw new Exception("Customer has no more available organizer seats.");

            Data.GTMOrganizer gtmOrganizer = new Data.GTMOrganizer(Data.GTMOrganizer.Columns.UserID, userID);
            if (gtmOrganizer.Id == 0)
            {
                Data.GTMOrganizerCollection gtmOrganizers = new Data.GTMOrganizerCollection().Where(Data.GTMOrganizer.Columns.UserID, 0).Load();
                if (gtmOrganizers.Count == 0)
                    throw new Exception("No GoToMeeting organizer licenses are available for assignment.  Please add more licenses to the pool.");
                int randomIndex = new Random().Next(gtmOrganizers.Count);
                gtmOrganizer = gtmOrganizers[randomIndex];
            }

            Organizer organizer = gtm.GetOrganizer(gtmOrganizer.OrganizerKey);

            gtmOrganizer.UserID = userID;
            gtmOrganizer.Status = GTMController.OrganizerStatusActive;
            gtmOrganizer.Save();

            if (GTMController.GetFieldValue(organizer.organizerFields, "status").ToLower() != GTMController.OrganizerStatusActive.ToLower())
                gtm.SetOrganizerStatus(gtmOrganizer.OrganizerKey, GTMController.OrganizerStatusActive);

        }

        public void TryChangeUserName(User user, string subdomain)
        {
            Data.User current = new Data.User(user.ID);
            if (user.SalesChannelID > 0 && CurrentUserIsSalesChannelAdmin())
            {
                //otherwise, they're a sales channel admin, in which case they always just get the "admin" priv.
                if (!SymphonyRoleProvider.IsUserInRole(subdomain, user.Username, "Customer - Administrator"))
                    SymphonyRoleProvider.AddUserToRole(subdomain, user.Username, "Customer - Administrator");
            }

            /* renaming a user */
            if (user.Username != "" && user.ID > 0 && current.Username != user.Username)
            {
                if (user.ID != UserID)
                {
                    Data.SPs.ChangeLoginName(current.Username, user.Username, subdomain).Execute();
                }
            }

        }

        public User Save(User user, bool forceAccountCreation = false)
        {
            bool isNewUser = false;
            if (user.ID == 0)
            {
                // new user, kill the count in the cache
                HttpContext.Current.Cache.Remove(USER_COUNT_PREFIX + user.CustomerID);
                isNewUser = true;
            }

            // all or nothing
            using (var ts = Utilities.CreateTransactionScope())
            {
                string subdomain = new Data.Customer(user.CustomerID).SubDomain;
                user.Username = user.Username.Trim();

                ValidateSupervisor(user);
                ValidateReportingSupervisor(user);

                ValidateUserCount(user, forceAccountCreation);

                if (user.CustomerID > 0) ValidateSalesChannel(user, user.CustomerID);
                else ValidateSalesChannel(user);

                ValidateUserNameNumberAndHireDate(user);

                // first, we make the attempt to change usernames, if this is an existing user
                if (user.ID > 0)
                {
                    TryChangeUserName(user, subdomain);
                }

                // next, we create or update the user as necessary in asp.net
                if (user.ID == 0)
                {
                    // Override the subdomain when we allow self account creation (since it implies users arent always logged in)
                    if (new Data.Customer(user.CustomerID).AllowSelfAccountCreation)
                        CreateNewASPNetUser(user, subdomain, true);
                    else
                        CreateNewASPNetUser(user, subdomain);

                    // Set the last enforced password for a new user, so they can use their saved password for some time
                    if (new Data.Customer(user.CustomerID).EnforcePasswordReset)
                    {
                        user.LastEnforcedPasswordReset = DateTime.UtcNow;
                    }
                }
                else
                {
                    UpdateExistingASPNetUser(user, subdomain);
                }

                // copy and store the data
                Data.User newUser = new Symphony.Core.Data.User(user.ID);

                // We have no way of changing the IsCreatedBySalesforce flag in the UI
                // We don't want to overwrite this
                if (newUser.IsCreatedBySalesforce)
                {
                    user.IsCreatedBySalesforce = true;
                }


                if (newUser.LastEnforcedPasswordReset != null &&
                    newUser.LastEnforcedPasswordReset != DateTime.MinValue &&
                    !user.LastEnforcedPasswordReset.HasValue)
                {
                    user.LastEnforcedPasswordReset = newUser.LastEnforcedPasswordReset;
                }

                user.CopyTo(newUser);
                newUser.TelephoneNumber = user.TelephoneNumber ?? string.Empty;
                newUser.AlternateEmail1 = user.AlternateEmail1 ?? newUser.AlternateEmail1;
                newUser.AlternateEmail2 = user.AlternateEmail2 ?? newUser.AlternateEmail2;
                newUser.HomePhone = user.HomePhone ?? newUser.HomePhone;
                newUser.WorkPhone = user.WorkPhone ?? newUser.WorkPhone;
                newUser.CellPhone = user.CellPhone ?? newUser.CellPhone;
                newUser.AddressLine1 = user.AddressLine1 ?? newUser.AddressLine1;
                newUser.AddressLine2 = user.AddressLine2 ?? newUser.AddressLine2;
                newUser.City = user.City ?? newUser.City;
                newUser.State = user.State ?? newUser.State;
                newUser.ZipCode = user.ZipCode ?? newUser.ZipCode;
                newUser.AssociatedImageData = user.AssociatedImageData ?? newUser.AssociatedImageData;
                //newUser.SecondarySupervisorID = user.SecondarySupervisorID

                newUser.Mobile = user.Mobile ?? string.Empty;
                newUser.Id = user.ID;
                newUser.Password = string.Empty;

                if (string.IsNullOrEmpty(newUser.EncryptedSSN))
                {
                    newUser.EncryptedSSN = string.Empty;
                }

                // only other sales channel admins can create new sales channel admins
                if (!UserIsSalesChannelAdmin)
                {
                    newUser.SalesChannelID = 0;
                }
                else
                {
                    // they're a sales channel admin...
                    if (newUser.SalesChannelID > 0)
                    {
                        // and they're editing or updating an admin; make the user active
                        newUser.StatusID = (int)UserStatusType.Active;
                    }
                }
                if (user.HireDate == null)
                {
                    newUser.HireDate = DefaultDateTime;
                }
                else
                {
                    // strip off any time component
                    newUser.HireDate = user.HireDate.Value.WithUtcFlag().Date;
                }

                if (user.LastEnforcedPasswordReset < DefaultDateTime || user.LastEnforcedPasswordReset == null || user.LastEnforcedPasswordReset > DateTime.MaxValue)
                {
                    newUser.LastEnforcedPasswordReset = DefaultDateTime;
                }

                if (newUser.DateOfBirth != null)
                {
                    newUser.DateOfBirth = newUser.DateOfBirth.Value.WithUtcFlag().Date;
                }

                if (isNewUser && string.IsNullOrWhiteSpace(newUser.JanrainUserUuid))
                {
                    string email = !string.IsNullOrWhiteSpace(newUser.Email) ? newUser.Email : newUser.Username;
                    string uuid = null;

                    if (IsValidEmailAddress(email))
                    {
                        uuid = new Select(Data.User.JanrainUserUuidColumn)
                        .From<Data.User>()
                        .Where(Data.User.JanrainUserUuidColumn).IsNotNull()
                        .And(Data.User.EmailColumn.ColumnName).IsEqualTo(email)
                        .ExecuteScalar<string>();
                    }

                    if (string.IsNullOrWhiteSpace(uuid))
                    {
                        uuid = Guid.NewGuid().ToString();
                    }

                    newUser.JanrainUserUuid = uuid;
                }

                UpdateReportingPermissions(newUser, user);

                // add professions
                if (user.PrimaryProfessionID == 0 && !string.IsNullOrWhiteSpace(user.PrimaryProfessionName))
                {
                    var p = new Data.Profession(Data.Profession.Columns.Name, user.PrimaryProfessionName);
                    if (p.Id == 0)
                    {
                        p.Name = user.PrimaryProfessionName;
                        p.Save();
                    }
                    newUser.PrimaryProfessionID = p.Id;
                }
                if (user.SecondaryProfessionID == 0 && !string.IsNullOrWhiteSpace(user.SecondaryProfessionName))
                {
                    var p = new Data.Profession(Data.Profession.Columns.Name, user.SecondaryProfessionName);
                    if (p.Id == 0)
                    {
                        p.Name = user.SecondaryProfessionName;
                        p.Save();
                    }
                    newUser.SecondaryProfessionID = p.Id;
                }

                newUser.Save(Username);

                // update the userid
                user.ID = newUser.Id;

                // if the new hire indicator is set, the new hire end date *cannot* be set
                if (newUser.NewHireIndicator)
                {
                    newUser.NewHireEndDate = null;
                }
                else
                {
                    if (newUser.NewHireEndDate == null)
                    {
                        newUser.NewHireEndDate = DateTime.Now.Date;
                    }
                }

                // set permissions
                UpdateUserRoles(user, subdomain);

                if (!isNewUser)
                {
                    // Update janrain
                    user.IsJanrainPasswordFail = false;
                    /*if (!string.IsNullOrEmpty(user.Password) && !string.IsNullOrEmpty(user.JanrainUserUuid))
                    {
                        user.IsJanrainPasswordFail = !UpdateJanrainPassword(user);
                    }*/
                }


                if (user.IsAuthor.HasValue && user.IsAuthor.Value)
                {
                    Data.AuthorDetail authorDetails = new Data.AuthorDetail(Data.AuthorDetail.Columns.UserId, user.ID);
                    authorDetails.UserId = user.ID;
                    authorDetails.Speciality = user.Speciality;
                    authorDetails.Save();
                }

                // add meta
                if (user.UserDataFields != null && user.UserDataFields.Count > 0)
                {
                    foreach (var userDataField in user.UserDataFields)
                    {
                        if (string.IsNullOrWhiteSpace(userDataField.UserValue))
                        {
                            if (userDataField.UserDataFieldUserMapID > 0)
                            {
                                Data.UserDataFieldUserMap.Delete(userDataField.UserDataFieldUserMapID);
                            }
                            continue;
                        }

                        Data.UserDataFieldUserMap map = new Data.UserDataFieldUserMap(userDataField.UserDataFieldUserMapID);
                        map.UserID = user.ID;
                        map.UserDataFieldID = userDataField.ID;
                        map.UserValue = userDataField.UserValue;

                        map.Save();
                    }
                }

                ts.Complete();
            }

            //SystemLoginController.SetNewPassword(user.Email, user.Password);

            return user;
        }

        private bool UpdateJanrainPassword(User user)
        {
            try
            {
                string baseUrl = ConfigurationManager.AppSettings["JanrainBaseUrl"];
                string url = baseUrl + "/entity.update";
                string clientSecret = ConfigurationManager.AppSettings["JanrainSecret"];
                string clientId = ConfigurationManager.AppSettings["JanrainClientId"];

                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";

                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("client_id", clientId);
                data.Add("client_secret", clientSecret);
                data.Add("attributes", "{\"password\":\"" + user.Password + "\"}");
                data.Add("uuid", user.JanrainUserUuid);
                data.Add("type_name", "user");

                StringBuilder sb = new StringBuilder();
                foreach (KeyValuePair<string, string> e in data)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append('&');
                    }
                    sb.Append(System.Web.HttpUtility.UrlEncode(e.Key, Encoding.UTF8));
                    sb.Append('=');
                    sb.Append(HttpUtility.UrlEncode(e.Value, Encoding.UTF8));
                }

                string postData = sb.ToString();

                byte[] postDataBytes = System.Text.Encoding.ASCII.GetBytes(postData);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = postDataBytes.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(postDataBytes, 0, postDataBytes.Length);
                requestStream.Close();

                WebResponse response = request.GetResponse();

                Stream responseStream = response.GetResponseStream();

                StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.Default);

                string content = reader.ReadToEnd();

                reader.Close();
                responseStream.Close();
                response.Close();

                JanrainResult jResult = Utilities.Deserialize<JanrainResult>(content);

                if (jResult.Stat != "ok")
                {
                    throw new Exception("Janrain response did not return OK. The response was: " + content);
                }

                return true;
            }
            catch (Exception e)
            {
                // We don't really care about exceptions in this case. If we 
                // managed to update the janrain password, great, if not,
                // lets just log it and carry on.
                Log.Error("Error changing janrain password user " + user.ID + " " + e.Message);
            }

            return false;
        }

        public User UpdateUserSecurity(int userId, UserSecurity user)
        {
            Data.User userData = new Data.User(userId);

            if (userData == null)
            {
                throw new Exception("Invalid user id.");
            }

            if (!string.IsNullOrEmpty(user.SSN))
            {
                userData.Ssn = user.HashSSN();
            }
            else
            {
                // remove any existing hashed values, too
                userData.Ssn = null;
            }

            if (!string.IsNullOrEmpty(user.DOB))
            {
                userData.Dob = user.HashDOB();
            }
            else
            {
                // remove any existing hashed values, too
                userData.DateOfBirth = null;
                userData.Dob = null;
            }

            userData.Save(Username);

            return (User)user;
        }

        public bool ValidateUser(int userId, int courseId, int trainingProgramId, int attempt, UserSecurity user)
        {
            // TODO: Move all validation handling to the ValidationController
            Data.User userData = new Data.User(userId);
            string type = !String.IsNullOrEmpty(user.SSN) ? "SSN" : !String.IsNullOrEmpty(user.DOB) ? "DOB" : "Unkown";
            if (userData == null)
            {
                LogToAffidavit(userId, courseId, trainingProgramId, false, type, attempt);
                throw new Exception("Validation failed");
            }

            switch (type)
            {
                case "SSN":
                    if (!user.HashSSN().SequenceEqual(userData.Ssn))
                    {
                        LogToAffidavit(userId, courseId, trainingProgramId, false, type, attempt);
                        throw new Exception("Validation failed");
                    }
                    break;
                case "DOB":
                    if (!user.HashDOB().SequenceEqual(userData.Dob))
                    {
                        LogToAffidavit(userId, courseId, trainingProgramId, false, type, attempt);
                        throw new Exception("Validation failed");
                    }
                    break;
                default:
                    LogToAffidavit(userId, courseId, trainingProgramId, false, type, attempt);
                    throw new Exception("Validation failed");
            }
            LogToAffidavit(userId, courseId, trainingProgramId, true, type, attempt);
            return true;
        }

        public bool SavePostValidationState(int userId, int trainingProgramId, bool wasSuccessful)
        {
            var tpRollup = (new RollupController()).GetRollup(trainingProgramId, userId);
            if (tpRollup == null)
            {
                return false;
            }

            tpRollup.PostValidationComplete = wasSuccessful;
            tpRollup.Save();

            return true;
        }

        private void LogToAffidavit(int userId, int courseId, int trainingProgramId, bool result, string type, int attempt)
        {
            Data.Affidavit affidavit = new Data.Affidavit();
            affidavit.CourseID = courseId;
            affidavit.TrainingProgramID = trainingProgramId;
            affidavit.UserID = userId;
            affidavit.QuestionType = type;
            affidavit.Result = result;
            affidavit.Attempt = attempt;

            affidavit.Save(Username);

        }

        public static string GetExternalUserPassword(string email)
        {
            string password = "";
            string[] emailParts = email.Split('@');
            if (emailParts.Length > 0)
            {
                password = emailParts[0];
            }
            else
            {
                password = email;
            }
            return password + "123!"; ;
        }


        /// <summary>
        /// Unlocks the specified user
        /// </summary>
        /// <param name="sUser"></param>
        /// <returns></returns>
        public bool ResetLoginCounter(string username)
        {
            Data.User u = new Data.User(Data.User.Columns.Username, username);
            u.LoginCounter = 0;
            u.Save(Username);
            MembershipUser mu = SymphonyMembershipProvider.GetUser(u.Customer.SubDomain, u.Username);
            if (mu != null)
                mu.UnlockUser();
            return true;
        }

        /// <summary>
        /// Updates new hire indicators; if they're past the new hire duration, un-flags them as new hires.
        /// </summary>
        /// <remarks>
        /// In reality, this could all be done with a sproc and a scheduled job in SQL, but since our taskrunner
        /// is already set up to run on a schedule (and we need c# code for the notifications), we'll re-use
        /// it for this purpose.
        /// </remarks>
        public static void UpdateNewHires()
        {
            // run through the customers
            foreach (Data.Customer customer in (new Data.CustomerCollection().Load()))
            {
                if (customer.SubDomain == "newhiretest")
                {
                    Console.WriteLine("New HIre");
                }
                // if a given customer errors out, keep trying the next one
                try
                {
                    // determine how long a user should be a new hire
                    int duration = customer.NewHireDuration;

                    // a duration of 0 means don't process new hires here
                    if (duration == 0) continue;

                    // load up any users for this customer that are new hires
                    Data.UserCollection newHires = new Data.UserCollection()
                        .Where(Data.User.Columns.NewHireIndicator, true)
                        .Where(Data.User.Columns.CustomerID, customer.Id).Load();

                    // and update the date-time as appropriate
                    Data.UserCollection usersToUpdate = new Data.UserCollection();
                    foreach (Data.User user in newHires)
                    {
                        if (!user.HireDate.HasValue)
                        {
                            continue;
                        }

                        // strip out the hours minutes and seconds
                        DateTime hireDate = new DateTime(user.HireDate.Value.Ticks);
                        hireDate.AddHours(-hireDate.Hour);
                        hireDate.AddMinutes(-hireDate.Minute);
                        hireDate.AddSeconds(-hireDate.Second);

                        if (hireDate.AddDays(duration + 1) < DateTime.Now) // add 1 because we want to expire at the end of the day
                        {
                            user.NewHireIndicator = false;
                            user.NewHireEndDate = DateTime.Now.Date; // store the date in the database
                            usersToUpdate.Add(user);
                        }
                    }
                    // save them in segments by customer
                    usersToUpdate.SaveAll("TaskRunner");

                    List<NotificationOptions> options = new List<NotificationOptions>();
                    foreach (Data.User user in usersToUpdate)
                    {
                        options.Add(new NotificationOptions
                        {
                            TemplateObjects = new object[] { user }
                        });
                    }

                    NotificationController.SendNotification(NotificationType.NewHireStatusChange, options);
                }
                catch (Exception ex)
                {
                    ILog Log = LogManager.GetLogger(typeof(UserController));
                    Log.Error(ex);
                }
            }
        }

        /// <summary>
        /// Sends an email to the specified user with a reset password link, and flags that user in the db as appropriate
        /// </summary>
        /// <param name="email">The email address of the user that needs to be reset</param>
        public static void SendPasswordResetLink(string subdomain, string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("Email address is required.");
            }

            var customer = new CustomerController().GetCustomer(subdomain);
            if (!customer.Success)
            {
                throw new Exception(customer.Error);
            }

            if (customer.Data == null)
            {
                throw new Exception("Invalid customer specified.");
            }

            Data.UserCollection users = new Symphony.Core.Data.UserCollection();
            users.Where(Data.User.Columns.Email, email)
                .Where(Data.User.Columns.CustomerID, customer.Data.ID)
                .Load();

            if (users.Count == 0)
            {
                throw new Exception("Unrecognized email address.");
            }
            else if (users.Count > 1)
            {
                throw new Exception("Email address could not be confirmed.");
            }

            Data.User u = users[0];

            MembershipUser mu = SymphonyMembershipProvider.GetUser(u.Customer.SubDomain, u.Username, true);
            if (mu == null)
            {
                throw new Exception("Failed to find a user matching the specified email address.");
            }


            string guid = Guid.NewGuid().ToString();
            mu.Comment = DateTime.UtcNow.Ticks + "|" + guid;
            SymphonyMembershipProvider.UpdateUser(u.Customer.SubDomain, mu, true);

            // build the url for the user
            string url =
                string.Format("https://{0}/resetpassword/?email={1}&guid={2}&customer={3}",
                 HttpContext.Current.Request.Url.DnsSafeHost,
                 email,
                 guid,
                 subdomain) + Skins.BuildQuerySkin("&");

            // build the message
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append("<h3>Password Reset</h3>");
            bodyBuilder.Append("<p>A request was made to reset the password for this email address.</p>");
            bodyBuilder.Append("<p>If you believe you have received this message in error, you can simply ignore it.</p>");
            bodyBuilder.Append("<p>Otherwise, simply click on the following link to reset your password:</p>");
            bodyBuilder.Append(string.Format("<p><a href=\"{0}\">Reset My Password</a></p>", url));
            
            // TODO: move this into a "real" notification that is forced to be enabled so the text can be customized

            // send it; any errors throw back to the calling method
            if (!NotificationController.SendMessageSMTP(u.Customer, bodyBuilder.ToString(), "Symphony Password Reset", null, email, "normal"))
            {
                throw new Exception("Failed to send password reset request.");
            }
            
        }

        /// <summary>
        /// Resets a specified user's password
        /// </summary>
        /// <param name="email">The user's email address</param>
        /// <param name="guid">The guid from the email; stored in the "comment" field of the user</param>
        /// <param name="password">The new password</param>
        /// <returns>The user's subdomain</returns>
        public static string ResetPassword(string subdomain, string email, string guid, string password)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new Exception("Email address is required.");
            }

            var customer = new CustomerController().GetCustomer(subdomain);
            if (!customer.Success)
            {
                throw new Exception(customer.Error);
            }

            if (customer.Data == null)
            {
                throw new Exception("Invalid customer specified.");
            }

            Data.UserCollection users = new Symphony.Core.Data.UserCollection();
            users.Where(Data.User.Columns.Email, email)
                .Where(Data.User.Columns.CustomerID, customer.Data.ID)
                .Load();

            if (users.Count == 0)
            {
                throw new Exception("Unrecognized email address.");
            }
            else if (users.Count > 1)
            {
                throw new Exception("Email address could not be confirmed.");
            }

            Data.User u = users[0];

            MembershipUser mu = SymphonyMembershipProvider.GetUser(u.Customer.SubDomain, u.Username, true);
            if (mu == null)
            {
                throw new Exception("Failed to find a user matching the specified email address.");
            }

            if (string.IsNullOrEmpty(mu.Comment))
            {
                throw new Exception("The specified user has not requested a password reset.");
            }

            var parts = mu.Comment.Split('|'); // DateTime.UtcNow.Ticks + "|" + Guid.NewGuid().ToString();
            if (parts.Length != 2)
            {
                throw new Exception("Token mismatch (1). The token you specified is invalid. Please <a href='/forgotpassword/" + u.Customer.SubDomain + "'>request a new one</a>.");
            }
            if (guid != parts[1])
            {
                throw new Exception("Token mismatch (2). The token you specified is no longer valid. Please <a href='/forgotpassword/" + u.Customer.SubDomain + "'>request a new one</a>.");
            }

            var created = new DateTime(long.Parse(parts[0]));
            if (created.AddHours(4) < DateTime.UtcNow)
            {
                throw new Exception("Token expired.");
            }
           

            // change the password; first, reset it so we get a new auto-genned password
            string tempPassword = SymphonyMembershipProvider.ResetPassword(u.Customer.SubDomain, mu, "bar", true);
            try
            {
                // then change it using the new password we created
                if (!SymphonyMembershipProvider.ChangePassword(u.Customer.SubDomain, mu, tempPassword, password, true))
                {
                    throw new Exception("Update failed! The password you have specified does not meet the minimum requirements. Passwords must be 7 characters and include a number and a letter.");
                }
            }
            catch (ArgumentException)
            {
                throw new Exception("Update failed! The password you have specified does not meet the minimum requirements. Passwords must be 7 characters and include a number and a letter.");
            }

            // clear the comment field
            mu.Comment = string.Empty;
            SymphonyMembershipProvider.UpdateUser(u.Customer.SubDomain, mu, true);

            // everything else is good, we'll update the last password reset date as well
            u.LastEnforcedPasswordReset = DateTime.UtcNow;
            u.Save();

            return u.Customer.SubDomain;
        }

        /// <summary>
        /// Changes the current user's password
        /// </summary>
        /// <param name="oldPassword">The old password</param>
        /// <param name="newPassword">The new password</param>
        /// <returns>The user's subdomain</returns>
        public bool ChangePassword(string oldPassword, string newPassword)
        {

            MembershipUser mu = SymphonyMembershipProvider.GetUser(ApplicationName, Username, true);
            if (mu == null)
            {
                throw new Exception("Failed to find the current user.");
            }

            try
            {
                // then change it using the new password we created
                if (!SymphonyMembershipProvider.ChangePassword(ApplicationName, mu, oldPassword, newPassword, true))
                {
                    throw new Exception("Update failed! Either the old password entered is incorrect or the new password you have specified does not meet the minimum requirements. Passwords must be 7 characters and include a number and a letter.");
                }
                Data.Customer c = new Data.Customer(CustomerID);
                // Update the user record with the time the password was saved
                if (c.EnforcePasswordReset)
                {
                    Data.User data = new Symphony.Core.Data.User(UserID);
                    data.LastEnforcedPasswordReset = DateTime.UtcNow;
                    data.Save(Username);
                }
            }
            catch (ArgumentException)
            {
                throw new Exception("Update failed! Either the old password entered is incorrect or the new password you have specified does not meet the minimum requirements. Passwords must be 7 characters and include a number and a letter.");
            }

            return true;
        }

        public string SwitchUser(int newUserId)
        {
            //TODO: Security check needed

            if (HttpContext.Current == null || HttpContext.Current.Items == null || HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] == null)
            {
                return string.Empty;
            }

            var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];

            Data.User newUser = new Data.User(newUserId);
            Data.Customer newCustomer = new Data.Customer(newUser.CustomerID);

            ApplicationProcessor.SetApplicationName(HttpContext.Current, newCustomer.SubDomain);

            // Update current User to be new requested user
            var userData = new AuthUserData()
            {
                ActualUserID = currentUserData.UserID, // Store who I actually am inside ActualUser
                ActualCustomerID = currentUserData.CustomerID,
                ActualSalesChannelID = currentUserData.SalesChannelID,
                ActualApplicationName = currentUserData.ApplicationName,
                UserID = newUser.Id,
                Username = newUser.Username,
                CustomerID = newUser.CustomerID,
                ApplicationName = newCustomer.SubDomain,
                SalesChannelID = newUser.SalesChannelID,
                IsNewHire = newUser.NewHireIndicator,
                CNRToken = GetCNRToken(newUser.Id, newCustomer)
            };
            SetAuthenticationCookie(userData);
            ApplicationProcessor.UpdateResponseCookie(HttpContext.Current, "customer", newCustomer.SubDomain);

            return newCustomer.SubDomain;
        }

        public bool ExitUser()
        {
            //TODO: Security check needed

            if (HttpContext.Current == null || HttpContext.Current.Items == null || HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] == null)
            {
                return false;
            }

            var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];

            Data.User actualUser = new Data.User(currentUserData.ActualUserID);
            Data.Customer actualCustomer = new Data.Customer(currentUserData.ActualCustomerID);

            ApplicationProcessor.SetApplicationName(HttpContext.Current, actualCustomer.SubDomain);

            // Update current User to be actual user
            var userData = new AuthUserData()
            {
                ActualUserID = actualUser.Id,
                ActualCustomerID = actualUser.CustomerID,
                ActualSalesChannelID = actualUser.SalesChannelID,
                ActualApplicationName = actualCustomer.SubDomain,
                UserID = actualUser.Id,
                Username = actualUser.Username,
                CustomerID = actualUser.CustomerID,
                ApplicationName = actualCustomer.SubDomain,
                SalesChannelID = actualUser.SalesChannelID,
                IsNewHire = actualUser.NewHireIndicator,
                CNRToken = GetCNRToken(actualUser.Id, actualCustomer)
            };
            SetAuthenticationCookie(userData);
            ApplicationProcessor.UpdateResponseCookie(HttpContext.Current, "customer", actualCustomer.SubDomain);

            return true;
        }


        public FormsAuthenticationConfiguration GetFormsAuthenticationConfig(string path = null)
        {
            var configuration = WebConfigurationManager.OpenWebConfiguration(path);
            var authenticationSection = (AuthenticationSection)configuration.GetSection("system.web/authentication");
            return authenticationSection.Forms;
        }

        public string GetCNRToken(int userId, Data.Customer customer)
        {
            string token = string.Empty;
            try
            {
                if (bool.Parse(ConfigurationManager.AppSettings["enableCizer"]))
                {
                    CNRService.CNRService cnrService = new CNRService.CNRService();
                    Symphony.Core.Data.User user = new Symphony.Core.Data.User(userId);
                    cnrService.Url = Utilities.ParseCustomerUrl(ConfigurationManager.AppSettings["CNRServiceUrl"], customer, HttpContext.Current); ;
                    // pull the timeout, and if it doesn't exist, set to 5s
                    string timeout = ConfigurationManager.AppSettings["CNRServiceTimeout"];
                    int iTimeout = 0;
                    if (!int.TryParse(timeout, out iTimeout))
                    {
                        iTimeout = 5000;
                    }
                    cnrService.Timeout = iTimeout;
                    token = cnrService.AuthenticateForms(GetReportingUsername(user), GetSharedReportingPassword());
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex);
            }
            return token;
        }

        /// <summary>
        /// Gets the name of the user according to cizer.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string GetReportingUsername(Symphony.Core.Data.User user)
        {
            return user.CustomerID + " " + user.Id + " " + user.FirstName.Replace("'", "") + " " + user.LastName.Replace("'", "");
        }

        /// <summary>
        /// Gets the shared password used by all reporting users
        /// </summary>
        /// <returns></returns>
        public string GetSharedReportingPassword()
        {
            try
            {
                string cipherText = ConfigurationManager.AppSettings["CNRPassword"];
                return Utilities.RijndaelSimple.Decrypt(cipherText);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return string.Empty;
        }

        /// <summary>
        /// To only be used by console applications that need to connect to symphony
        /// will allow console apps to login as an actual user and make direct api
        /// calls.
        /// 
        /// If used elsewhere it will likely break the session as we are going to 
        /// overwrite the HttpContext object
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool LoginBatch(string customer, string userName)
        {
            HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org/", null), new HttpResponse(null));
            HttpContext.Current.ApplicationInstance = new HttpApplication();
            HttpContext.Current.Application.Add(Symphony.Web.ApplicationProcessor.APPLICATION_LOCKER, new object());

            SqlQuery query = Select.AllColumnsFrom<Data.User>().ExcludeColumn(Data.User.AddressLine1Column, Data.User.AddressLine2Column,
                Data.User.CityColumn, Data.User.StateColumn, Data.User.AlternateEmail1Column, Data.User.AlternateEmail2Column, Data.User.HomePhoneColumn,
                Data.User.WorkPhoneColumn, Data.User.CellPhoneColumn, Data.User.ZipCodeColumn)
                .InnerJoin(Data.Customer.IdColumn, Data.User.CustomerIDColumn)
                .Where(Data.User.UsernameColumn).IsEqualTo(userName);

            Data.Customer cust;

            int customerId = 0;
            if (int.TryParse(customer, out customerId))
            {
                query.And(Data.Customer.IdColumn).IsEqualTo(customerId);
                cust = new Data.Customer(customerId);
            }
            else
            {
                query.And(Data.Customer.SubDomainColumn).IsEqualTo(customer);
                cust = new Data.Customer("subdomain", customer);
            }

            User user = query.ExecuteSingle<User>();


            if (user != null)
            {

                HttpContext.Current.Items.Add("UserData", new AuthUserData()
                        {
                            ActualUserID = user.ID,
                            ActualCustomerID = user.CustomerID,
                            ActualSalesChannelID = user.SalesChannelID,
                            ActualApplicationName = cust.SubDomain,
                            UserID = user.ID,
                            Username = user.Username,
                            CustomerID = user.CustomerID,
                            ApplicationName = cust.SubDomain,
                            SalesChannelID = user.SalesChannelID,
                            IsNewHire = user.NewHireIndicator,
                            CNRToken = GetCNRToken(user.ID, cust)
                        });

                HttpContext.Current.Items.Add(ApplicationProcessor.CONTEXT_CUSTOMER, cust.SubDomain);


                return true;
            }

            return false;
        }

        public AuthenticationResult Login(string customer, string userName, string password)
        {
            if (customer.StartsWith("mars_"))
            {
                return LoginMars(customer, userName, password);
            }
            else
            {
                return LoginSymphony(customer, userName, password, false);
            }
        }

        // WARNING: This should only be used for SSO SAML Authentication
        /// <summary>
        /// Allows forcing a particular user to be logged in to Symphony without password validation. Please do not call
        /// this with preauthenticated = true unless 100% certain the user is authenticated.
        /// </summary>
        /// <param name="customer">Customer to log in as</param>
        /// <param name="userName">User to log in as</param>
        /// <param name="preAuthenticated">is the user already authenticated</param>
        /// <param name="isSkipExternalSystems">If true, skip loading external systems for this user</param>
        /// <returns></returns>
        public AuthenticationResult Login(string customer, string userName, bool preAuthenticated, bool isSkipExternalSystems = false)
        {
            return LoginSymphony(customer, userName, null, preAuthenticated, null, isSkipExternalSystems);
        }

        //For janrain login
        /// <summary>
        /// Return a list of users that match a janrain id, and have janrain enabled for their customer
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public List<User> GetJanrainCapableUsers(string identifier)
        {
            List<string> columns = Data.User.Schema.Columns.Select(c => c.QualifiedName).ToList();
            columns.Add(Data.Customer.NameColumn.QualifiedName + " as CustomerName");
            columns.Add(Data.Customer.SubDomainColumn.QualifiedName + " as CustomerSubDomain");

            List<User> users = new Select(columns.ToArray()).From<Data.User>()
                                .InnerJoin(Data.Customer.IdColumn, Data.User.CustomerIDColumn)
                                .Where(Data.Customer.SsoEnabledColumn).IsEqualTo(true)
                                .And(Data.Customer.SsoTypeColumn).IsEqualTo((int)SsoType.Janrain)
                                .And(Data.User.JanrainUserUuidColumn).IsEqualTo(identifier)
                                .ExecuteTypedList<User>();

            return users;

        }
        /// <summary>
        /// Logs in a single user matching a specific janrain id and userId
        /// Customer account must be configured to allow janrain login
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public AuthenticationResult JanrainLogin(string identifier, int userId)
        {
            List<string> columns = Data.User.Schema.Columns.Select(c => c.QualifiedName).ToList();
            columns.Add(Data.Customer.SubDomainColumn.QualifiedName + " as CustomerSubDomain");

            User user = new Select(columns.ToArray()).From<Data.User>()
                                .InnerJoin(Data.Customer.IdColumn, Data.User.CustomerIDColumn)
                                .Where(Data.Customer.SsoEnabledColumn).IsEqualTo(true)
                                .And(Data.Customer.SsoTypeColumn).IsEqualTo((int)SsoType.Janrain)
                                .And(Data.User.JanrainUserUuidColumn).IsEqualTo(identifier)
                                .And(Data.User.IdColumn).IsEqualTo(userId)
                                .ExecuteSingle<User>();

            if (user == null)
            {
                return new AuthenticationResult()
                {
                    Success = false,
                };
            }

            return Login(user.CustomerSubDomain, user.Username, true);
        }

        public AuthenticationResult JanrainLoginHack(string identifier)
        {
            List<string> columns = Data.User.Schema.Columns.Select(c => c.QualifiedName).ToList();
            columns.Add(Data.Customer.SubDomainColumn.QualifiedName + " as CustomerSubDomain");

            User user = new Select(columns.ToArray()).From<Data.User>()
                                .InnerJoin(Data.Customer.IdColumn, Data.User.CustomerIDColumn)
                                .Where(Data.Customer.SsoEnabledColumn).IsEqualTo(true)
                                .And(Data.Customer.SsoTypeColumn).IsEqualTo((int)SsoType.Janrain)
                                .And(Data.User.JanrainUserUuidColumn).IsEqualTo(identifier)
                //.And(Data.User.IdColumn).IsEqualTo(userId)
                                .ExecuteSingle<User>();

            if (user == null)
            {
                return new AuthenticationResult()
                {
                    Success = false,
                    Message = "No results found"
                };
            }

            return Login(user.CustomerSubDomain, user.Username, true);
        }

        private AuthenticationResult LoginMars(string customer, string userName, string password)
        {
            CodingHorror ch = new CodingHorror();
            string marsShortName = customer.Replace("mars_", string.Empty);
            MarsSignOn signOn = ch.ExecuteTypedList<MarsSignOn>("select UserID, [LaunchSite] from MarsSignOn where BankShortName = @marsShortName and Username = @username and Password = @password", customer, userName, password).FirstOrDefault();
            if (signOn == null)
            {
                return new AuthenticationResult()
                {
                    Message = "Invalid username or password",
                    Success = false
                };
            }
            else
            {
                Guid token = Guid.NewGuid();
                ch.Execute("insert MarsUserToken (TokenID, UID, ExpirationDate) values (@token, @userId, dateadd(day, 1, getdate()))", token.ToString(), signOn.UserID);
                Dictionary<string, string> attributes = new Dictionary<string, string>();
                attributes.Add("MarsLaunchSite", signOn.LaunchSite);
                attributes.Add("MarsToken", token.ToString());
                attributes.Add("MarsUserId", signOn.UserID.ToString());

                // success vs. mars. next, log in to symphony,
                // add a token, then return that along with the launch site to the user
                AuthenticationResult result = LoginSymphony(customer, userName, password, true, attributes);
                return result;
            }
        }

        private AuthenticationResult LoginSymphony(string customer, string userName, string password, bool preAuthenticated = false, Dictionary<string, string> attributes = null, bool isSkipExternalSystems = false)
        {
            AuthenticationResult loginResult = new AuthenticationResult();
            ExternalLoginController externalLoginController = new ExternalLoginController();

            Data.Customer cust = new Data.Customer(Data.Customer.Columns.SubDomain, customer);


            // check for max sign in override:
            if (cust.MaxSignInAttempts.HasValue && cust.MaxSignInAttempts.Value > 0)
            {
                SymphonyMembershipProvider.SetMaxLoginAttempts((int)cust.MaxSignInAttempts);
            }

            if (cust.Id == 0)
            {
                //validate the subdomain
                loginResult.Message = "Invalid customer specified.";
                loginResult.Success = false;
            }
            else if (cust.EvaluationIndicator && cust.EvaluationEndDate != null && cust.EvaluationEndDate < DateTime.Now)
            {
                // validate the evaluation
                loginResult.Message = "Your evaluation period has expired.";
                loginResult.Success = false;
            }
            else if (cust.SuspendedIndicator)
            {
                loginResult.Message = "This account has been suspended. Please contact your customer care representative.";
                loginResult.Success = false;
            }
            /*else if (preAuthenticated == true && cust.SsoEnabled == false)
            {
                loginResult.Message = "This account does not have single sign-on enabled. Please contact your customer care representative.";
                loginResult.Success = false;
            }*/
            else
            {
                loginResult.CustomerSubDomain = cust.SubDomain;
                ApplicationProcessor.SetApplicationName(HttpContext.Current, cust.SubDomain);
                MembershipUser mu = SymphonyMembershipProvider.GetUser(cust.SubDomain, userName);

                // Check if user is locked out and if there's additional config for un-locking the user:
                if (mu != null && mu.IsLockedOut && cust.LockedOutPeriodInMinutes!= null && cust.LockedOutPeriodInMinutes > 0)
                {
                    var loPeriodInSeconds = cust.LockedOutPeriodInMinutes * 60;
                    var lockedOutTime = mu.LastLockoutDate;
                    var now = DateTime.Now;

                    // if enough time has passed unlock the user and proceed.
                    if ((now - lockedOutTime).TotalSeconds >= loPeriodInSeconds)
                    {
                        ResetLoginCounter(userName);
                    }
                }

                User user = null;

                bool usernameAndPassValidInSymphony = false;
                usernameAndPassValidInSymphony = (preAuthenticated && mu != null) ||
                    SymphonyMembershipProvider.ValidateUser(cust.SubDomain, userName, password);

                if (usernameAndPassValidInSymphony)
                {
                    user = SubSonic.Select.AllColumnsFrom<Symphony.Core.Data.User>()
                        .Where(Symphony.Core.Data.User.Columns.Username)
                        .IsEqualTo(userName)
                        .And(Symphony.Core.Data.User.Columns.CustomerID)
                        .IsEqualTo(cust.Id)
                        .ExecuteSingle<User>();
                }
                else
                {
                    user = new User()
                    {
                        Username = userName
                    };
                }

                if (!isSkipExternalSystems)
                {
                    externalLoginController.UpdateLoginSystems(cust, user, password, usernameAndPassValidInSymphony);
                }

                if (user != null && user.ID > 0)
                {
                    if (mu != null && mu.IsLockedOut)
                    {
                        SymphonyMembershipProvider.UnlockUser(cust.SubDomain, mu);
                    }

                    if (user.StatusID != (int)UserStatusType.Active)
                    {
                        loginResult.Message = "This account is currently inactive. Please contact your customer care representative.";
                        loginResult.Success = false;
                    }
                    else
                    {
                        loginResult.Success = true;
                        loginResult.UserId = user.ID;
                        loginResult.CustomerId = user.CustomerID;
                        //FormsAuthentication.SetAuthCookie(user.Username, false, "/");
                        var userData = new AuthUserData()
                        {
                            ActualUserID = user.ID,
                            ActualCustomerID = user.CustomerID,
                            ActualSalesChannelID = user.SalesChannelID,
                            ActualApplicationName = cust.SubDomain,
                            UserID = user.ID,
                            Username = user.Username,
                            CustomerID = user.CustomerID,
                            ApplicationName = cust.SubDomain,
                            SalesChannelID = user.SalesChannelID,
                            IsNewHire = user.NewHireIndicator,
                            CNRToken = GetCNRToken(user.ID, cust),
                            Attributes = attributes
                        };
                        SetAuthenticationCookie(userData);
                        if (attributes != null)
                        {
                            foreach (KeyValuePair<string, string> pair in attributes)
                            {
                                loginResult.Attributes.Add(pair.Key, pair.Value);
                            }
                        }

                        ApplicationProcessor.UpdateResponseCookie(HttpContext.Current, "customer", cust.SubDomain);

                        loginResult.UserData = userData;
                    }
                }
                else
                {
                    var alias = cust.UserNameXAlias;
                    if (string.IsNullOrEmpty(alias))
                    {
                        alias = "username";
                    }
                    loginResult.Success = false;
                    MembershipUser cUser = SymphonyMembershipProvider.GetUser(cust.SubDomain, userName);

                    if (cUser == null) 
                    {
                        loginResult.Message = "Invalid " + alias;
                    }
                    else if (cUser.IsLockedOut)
                    {
                        loginResult.Message = string.IsNullOrEmpty(cust.LockedOutMessage) ? "The account has been locked out." : cust.LockedOutMessage;
                    }
                    else
                    {
                        loginResult.Message = "Invalid password.";
                    }
                }
            }

            return loginResult;
        }

        private static bool IsValidEmailAddress(string EmailAddress)
        {
            string patternRegexEmail = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            // from microsoft: https://msdn.microsoft.com/en-us/library/01escwtf(v=vs.110).aspx
            return System.Text.RegularExpressions.Regex.IsMatch(EmailAddress, patternRegexEmail);
        }

        public void SetAuthenticationCookie(AuthUserData userData)
        {
            string encTicket = GetEncryptedTicket(userData);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            var domain = GetFormsAuthenticationConfig().Domain;
            if (!string.IsNullOrEmpty(domain))
            {
                cookie.Domain = domain;
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public string GetEncryptedTicket(AuthUserData userData)
        {
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
              userData.Username,
              DateTime.Now,
              DateTime.Now.Add(GetFormsAuthenticationConfig().Timeout),
              false,
              Utilities.Serialize(userData),
              FormsAuthentication.FormsCookiePath);

            return FormsAuthentication.Encrypt(ticket);
        }

        public static void Logout(HttpContext context)
        {
            // create a new, expired session
            FormsAuthenticationTicket ticket =
                new FormsAuthenticationTicket(
                    1,
                    "",
                    DateTime.Now,
                    DateTime.Now,
                    false,
                    Guid.NewGuid().ToString(), "/");
            string encrypted_ticket = FormsAuthentication.Encrypt(ticket);

            HttpCookie authCookie = new HttpCookie(
                FormsAuthentication.FormsCookieName,
                encrypted_ticket);
            authCookie.Path = "/";
            context.Response.Cookies.Add(authCookie);

            // kill the customer cookie
            /*HttpCookie customerCookie = new HttpCookie("customer");
            customerCookie.Expires = DateTime.Now.AddDays(-1d);
            customerCookie.Path = "/";
            context.Response.Cookies.Add(customerCookie);*/

            // kill the session cookie
            HttpCookie sessionCookie = new HttpCookie("ASP.NET_SessionId");
            sessionCookie.Expires = DateTime.Now.AddDays(-1d);
            sessionCookie.Path = "/";
            context.Response.Cookies.Add(sessionCookie);

            // kill all session variables
            if (context.Session != null)
            {
                context.Session.Clear();
                context.Session.Abandon();
            }
        }

        public SingleResult<OnlineStatus> GetOnlineStatus(int userId)
        {
            OnlineStatus status = (OnlineStatus)new Select(Data.User.OnlineStatusColumn).From<Data.User>()
                .Where(Data.User.IdColumn).IsEqualTo(userId)
                .And(Data.User.CustomerIDColumn).IsEqualTo(CustomerID)
                .ExecuteScalar<int>();

            return new SingleResult<OnlineStatus>(status);
        }

        public SingleResult<OnlineStatus> UpdateOnlineStatus(int userId, OnlineStatus status)
        {
            Data.User dataUser = new Data.User(userId);
            if (dataUser.CustomerID != CustomerID)
            {
                throw new Exception("User does not exist.");
            }

            dataUser.OnlineStatus = (int)status;

            dataUser.Save();

            return new SingleResult<OnlineStatus>(status);
        }

        public string GetCertificateFieldValue(Models.User user, string propertyName)
        {
            propertyName = Utilities.UpperCaseFirst(propertyName);

            var pi = typeof(Symphony.Core.Models.User).GetProperty(propertyName, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            if (IsValidUserCertificateField(pi))
            {
                object val = pi.GetValue(user, null);
                if (val != null)
                {
                    return val.ToString();
                }
            }
            return string.Empty;
        }


        public bool IsValidUserCertificateField(string name)
        {
            name = Utilities.UpperCaseFirst(name);

            var pi = typeof(Symphony.Core.Models.User).GetProperty(name, System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            return IsValidUserCertificateField(pi);
        }

        public bool IsValidUserCertificateField(PropertyInfo pi)
        {
            if (pi != null)
            {
                var attributes = pi.GetCustomAttributes(typeof(Symphony.Core.Attributes.CertificateField), false);
                if (attributes != null && attributes.Length > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public List<string> GetValidUserCertificateFields()
        {
            List<string> fields = new List<string>();
            var pis = typeof(Symphony.Core.Models.User).GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            if (pis != null)
            {
                foreach (var pi in pis)
                {
                    if (IsValidUserCertificateField(pi))
                    {
                        fields.Add(pi.Name);
                    }
                }
            }
            return fields;
        }

        public List<Dictionary<string, string>> GetUserHierarcyAccess(int userId)
        {
            Data.User user = new Data.User(userId);

            Data.UserCollection relatedUsers;

            if (!string.IsNullOrEmpty(user.JanrainUserUuid))
            {
                relatedUsers = new Data.UserCollection().Where(Data.User.Columns.JanrainUserUuid, user.JanrainUserUuid).Load();
            }
            else
            {
                relatedUsers = new Data.UserCollection();
                relatedUsers.Add(user);
            }

            CustomerController customerController = new CustomerController();
            Dictionary<int, Customer> customers = customerController
                .GetNetworkedCustomers(true, false, user.Customer.Id, "", "ID", OrderDirection.Ascending, 0, int.MaxValue)
                .Data
                .ToDictionary(c => c.ID);

            List<Dictionary<string, string>> userHierarchyAccess = new List<Dictionary<string, string>>();

            foreach (Data.User u in relatedUsers)
            {
                if (!customers.ContainsKey(u.CustomerID))
                {
                    continue;
                }

                customers.Remove(u.CustomerID);

                userHierarchyAccess.Add(new Dictionary<string, string>() {
                    { "customerId", u.Customer.Id.ToString() },
                    { HierarchyType.Audience.ToString().ToLower(), "none" },
                    { HierarchyType.Location.ToString().ToLower(), "none" },
                    { HierarchyType.JobRole.ToString().ToLower(), "none" }
                });

                if (SymphonyRoleProvider.IsUserInRole(u.Customer.SubDomain, u.Username, Roles.ReportingAnalyst))
                {
                    userHierarchyAccess[userHierarchyAccess.Count - 1][HierarchyType.Audience.ToString().ToLower()] = "all";
                    userHierarchyAccess[userHierarchyAccess.Count - 1][HierarchyType.Location.ToString().ToLower()] = "all";
                    userHierarchyAccess[userHierarchyAccess.Count - 1][HierarchyType.JobRole.ToString().ToLower()] = "all";
                }
                else
                {
                    if (SymphonyRoleProvider.IsUserInRole(u.Customer.SubDomain, u.Username, Roles.ReportingAudienceAnalyst))
                    {
                        var userAccessAudiences = string.Join(",", this.GetUserHierarchyPermissions(userId, UserPermissionType.Audience));
                        userHierarchyAccess[userHierarchyAccess.Count - 1][HierarchyType.Audience.ToString().ToLower()] =
                            string.IsNullOrEmpty(userAccessAudiences) ? string.Join(",", u.Audiences.Select(a => a.Id).ToArray()) :
                            string.Join(",", u.Audiences.Select(a => a.Id).ToArray()) + "," + userAccessAudiences;
                    }
                    if (SymphonyRoleProvider.IsUserInRole(u.Customer.SubDomain, u.Username, Roles.ReportingLocationAnalyst))
                    {
                        var userAccessLocations = string.Join(",", this.GetUserHierarchyPermissions(userId, UserPermissionType.Location));
                        userHierarchyAccess[userHierarchyAccess.Count - 1][HierarchyType.Location.ToString().ToLower()] =
                            string.IsNullOrEmpty(userAccessLocations) ? u.Location.Id.ToString() : u.Location.Id.ToString() + "," + userAccessLocations;
                    }
                    if (SymphonyRoleProvider.IsUserInRole(u.Customer.SubDomain, u.Username, Roles.ReportingJobRoleAnalyst))
                    {
                        var userAccessJobRoles = string.Join(",", this.GetUserHierarchyPermissions(userId, UserPermissionType.JobRole));
                        userHierarchyAccess[userHierarchyAccess.Count - 1][HierarchyType.JobRole.ToString().ToLower()] =
                            string.IsNullOrEmpty(userAccessJobRoles) ? u.JobRole.Id.ToString() : u.JobRole.Id.ToString() + "," + userAccessJobRoles;
                    }
                }
            }

            if (customers.Count > 0)
            {
                foreach (KeyValuePair<int, Customer> customerKeyValue in customers)
                {
                    Customer c = customerKeyValue.Value;

                    if (SymphonyRoleProvider.IsUserInRole(user.Customer.SubDomain, user.Username, Roles.ReportingAnalyst))
                    {
                        userHierarchyAccess.Add(new Dictionary<string, string>() {
                            { "customerId", c.ID.ToString() },
                            { HierarchyType.Audience.ToString().ToLower(), "all" },
                            { HierarchyType.Location.ToString().ToLower(), "all" },
                            { HierarchyType.JobRole.ToString().ToLower(), "all" }
                        });
                    }
                }
            }

            return userHierarchyAccess;
        }

        public List<string> GetUserHierarchyPermissions(int userId, UserPermissionType userPermissionType)
        {
            List<string> ret = new List<string>();
            int userPermType = new Select().From<Data.UserReportingPermissionsType>().Where(Data.UserReportingPermissionsType.NameColumn)
                .IsEqualTo(userPermissionType.ToString()).ExecuteScalar<int>();

            List<UserReportingPermission> q = Select.AllColumnsFrom<Data.UserReportingPermission>()
                .Where(Data.UserReportingPermission.UserIdColumn).IsEqualTo(userId)
                .And(Data.UserReportingPermission.PermissionTypeIdColumn).IsEqualTo(userPermType).ExecuteTypedList<UserReportingPermission>();

            ret = q.Select(t => t.ObjectId.ToString()).AsEnumerable<string>().ToList<string>();

            return ret;
        }
    }
}
