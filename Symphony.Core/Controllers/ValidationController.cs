﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using SubSonic;
using System.Web;
using Symphony.RusticiIntegration.Core;
using System.IO;
using System.Configuration;
using RusticiSoftware.ScormContentPlayer.Logic;
using ICSharpCode.SharpZipLib.Zip;
using Symphony.Core.Comparers;
using System.Transactions;
using log4net;
using Symphony.Core.Extensions;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Web.Hosting;

namespace Symphony.Core.Controllers
{
    public class ValidationController : SymphonyController
    {
        private static List<ValidationField> _ValidationQuestions;

        static ValidationController () {
            _ValidationQuestions = new List<ValidationField>();
            _ValidationQuestions.Add(new ValidationField()
            {
                Title = "Enter Your SSN Number",
                Description = "",
                Type = "text",
                Name = "SSN"
            });

            _ValidationQuestions.Add(new ValidationField()
            {
                Title = "Enter Your Date of Birth (MM/DD/YYYY)",
                Description = "",
                Type = "text",
                Name = "DOB"
            });
        }

        public static List<ValidationField> GetStudentValidationQuestion()
        {
            return _ValidationQuestions;
        }

        public static ValidationField GetRandomValidationQuestion()
        {
            Random rnd = new Random();
            return _ValidationQuestions[rnd.Next(_ValidationQuestions.Count)];
        }

        public static ValidationField GetValidationQuestionByName(string name)
        {
            var question = _ValidationQuestions.Find(x => x.Name == name);
            return question;
        }
    }
}
