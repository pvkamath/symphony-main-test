﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using SubSonic;
using System.Web;
using Symphony.RusticiIntegration.Core;
using System.IO;
using System.Configuration;
using RusticiSoftware.ScormContentPlayer.Logic;
using ICSharpCode.SharpZipLib.Zip;
using Symphony.Core.Comparers;
using System.Transactions;
using log4net;
using Symphony.Core.Extensions;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Web.Hosting;
namespace Symphony.Core.Controllers
{
    public class CourseAssignmentController : Controllers.SymphonyController
    {
        private ILog Log = LogManager.GetLogger(typeof(ArtisanController));
        private AccreditationController Accreditation = new AccreditationController();

        #region Online Course Importing

        public string GetImportPath(string originalFilename)
        {
            string path = Path.Combine(ImportFolder, Guid.NewGuid().ToString() + Path.GetExtension(originalFilename));
            if (!Directory.Exists(ImportFolder))
            {
                Directory.CreateDirectory(ImportFolder);
            }
            return path;
        }

        public void SetImportFolder(string folder)
        {
            this.ImportFolder = folder;
        }

        public List<ImportAttempt<Course>> ImportCourses(int customerId, int userId, HttpPostedFile file)
        {
            string path = Path.GetTempFileName();
            file.SaveAs(path);
            return ImportCourses(customerId, userId, path);
        }

        public List<ImportAttempt<Course>> ImportCourses(int customerId, int userId, string zipFilename)
        {
            Utilities.UnZip(zipFilename, TempFolder);

            List<ImportAttempt<Course>> results = new List<ImportAttempt<Course>>();
            foreach (string filename in Directory.GetFiles(TempFolder, "*.zip"))
            {

                ImportAttempt<Course> attempt = new ImportAttempt<Course>();
                try
                {
                    // find a matching course based on the name, most recently created first if there is > 1 match
                    Data.OnlineCourse oc = Select.AllColumnsFrom<Data.OnlineCourse>()
                        .Where(Data.OnlineCourse.Columns.Name).IsEqualTo(Path.GetFileNameWithoutExtension(filename))
                        .And(Data.OnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                        .OrderDesc(Data.OnlineCourse.Columns.Id)
                        .ExecuteSingle<Data.OnlineCourse>();

                    // if the course already exists, update it
                    int courseId = 0;
                    if (oc != null)
                    {
                        courseId = oc.Id;
                    }

                    Course c = ImportCourseFromZipFile(customerId, userId, courseId, filename, customerId.ToString());
                    attempt.Result = c;
                    attempt.Success = true;
                }
                catch (Exception ex)
                {
                    attempt.Result = new Course() { Name = filename };
                    attempt.Success = false;
                    attempt.Error = ex.Message;
                }
                results.Add(attempt);
            }

            return results;
        }

        public Course ImportCourse(int customerId, int userId, int courseId, HttpPostedFile file)
        {
            string path = GetImportPath(file.FileName);
            file.SaveAs(path);
            return ImportCourseFromZipFile(customerId, userId, courseId, path, customerId.ToString());
        }

        public Course ImportCourseWithCallback(int customerId, int userId, int courseId, Func<SymphonyExternalConfiguration, ImportResult> callback)
        {
            Data.OnlineCourse course = null;
            string path = string.Empty;
            ImportResult result = null;
            try
            {
                SymphonyExternalConfiguration cfg = new SymphonyExternalConfiguration();
                cfg.ExistingCourseID = courseId;
                cfg.CustomerID = customerId;

                result = callback(cfg);

                Data.OnlineCourse original = new Data.OnlineCourse(courseId);
                if (result != null)
                {
                    if (result.WasSuccessful)
                    {
                        // get the imported course's id
                        SymphonyExternalPackage package = new SymphonyExternalPackage();
                        package.ParseFromString(result.SerializedExternalPackageId);

                        // load up the course
                        course = new Symphony.Core.Data.OnlineCourse(package.CourseId);
                        if (original.Id > 0)
                        {
                            // don't override this on an update
                            course.Name = original.Name;
                            course.Description = original.Description;

                            var manifestAccreditations = Accreditation.ParseManifestAccreditations(result.ManifestFilePath);
                            var oldAccreditations = Accreditation.QueryOnlineAccreditations(package.CourseId);

                            Accreditation.UpdateOnlineAccreditations(package.CourseId, oldAccreditations.Data.ToList(), manifestAccreditations.Data);
                        }
                        else
                        {
                            // new course
                            Data.Category category = GetDefaultCategory(this.CustomerID);
                            if (category != null)
                            {
                                course.CategoryID = category.Id;
                            }
                            course.Retries = null;
                            course.Save();

                            var manifestAccreditations = Accreditation.ParseManifestAccreditations(result.ManifestFilePath);
                            var accreditations = Accreditation.ConvertFromArtisan(course.Id, manifestAccreditations.Data);

                            Accreditation.SyncOnlineAccreditations(course.Id, accreditations.Data);
                        }
                    }
                    else
                    {
                        throw new Exception(result.Message);
                    }
                }
                else
                {
                    throw new Exception("Import failed, no results found.");
                }
            }
            catch (Exception ex)
            {
                if (path != string.Empty && File.Exists(path))
                {
                    try
                    {
                        File.Delete(path);
                    }
                    catch { }
                }
                throw;
            }
            var model = Model.Create<Course>(course);
            model.ImportResult = result;

            return model;
        }

        public Course ImportCourseFromFolder(int customerId, int userId, int courseId, string folder)
        {
            return ImportCourseWithCallback(customerId, userId, courseId, (cfg) =>
            {
                return SymphonyIntegrationController.ImportSingleCourseFromFolder(folder, cfg, "system");
                //return SymphonyIntegrationController.GetFakePackageImportResult(courseId); //.ImportSingleCourseFromFolder(folder, cfg, userId.ToString());
            });
        }

        public Course ImportCourseFromZipFile(int customerId, int userId, int courseId, string filename, string subfolder)
        {
            return ImportCourseWithCallback(customerId, userId, courseId, (cfg) =>
            {
                // if the file doesn't exist on the right path, move it
                string path = GetImportPath(filename);
                if (filename.ToLower() != path.ToLower())
                {
                    File.Copy(filename, path, true);
                }

                return SymphonyIntegrationController.ImportSingleCourseFromZip(path, cfg, userId.ToString(), subfolder);
            });
        }

        #endregion

        #region Online Courses

        #region Course Categories

        public PagedResult<Category> FindCategories(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Category>()
                .Where(Data.Category.Columns.CustomerID).IsEqualTo(customerId)
                .AndExpression(Data.Category.Columns.Name).ContainsString(searchText)
                .Or(Data.Category.Columns.Description).ContainsString(searchText);

            return new PagedResult<Category>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public MultipleResult<Category> SaveCategories(int customerId, int userId, List<Category> categories)
        {
            foreach (Category category in categories)
            {
                Data.Category data = new Symphony.Core.Data.Category(category.Id);
                data.Description = category.Description;
                data.Name = category.Name;
                data.CustomerID = customerId;
                data.CategoryTypeID = category.CategoryTypeID;
                data.Save();

                category.Id = data.Id;
            }

            return new MultipleResult<Category>(categories);
        }

        #endregion

        public PagedResult<Course> FindCourses(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {

            List<string> columns = Data.OnlineCourse.Schema.Columns.Select(c => c.QualifiedName).ToList();
            columns.Add(Data.ArtisanCourse.ParametersColumn.QualifiedName);

            SqlQuery query = new Select(columns.ToArray())
                .From<Data.OnlineCourse>()
                .LeftOuterJoin(Data.ArtisanCourse.IdColumn, Data.OnlineCourse.ArtisanCourseIDColumn)
                .Where(Data.OnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                .AndExpression(Data.OnlineCourse.Columns.Name).ContainsString(searchText)
                .Or(Data.OnlineCourse.Columns.Keywords).ContainsString(searchText)
                .CloseExpression();

            if (string.IsNullOrEmpty(orderBy))
            {
                orderBy = Data.OnlineCourse.NameColumn.QualifiedName;
            }
            else
            {
                orderBy = Data.OnlineCourse.Schema.TableName + "." + orderBy;
            }

            return new PagedResult<Course>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        /// <summary>
        /// Get the course, but also include flags that indicate if this course can be launched, and how it should be launched
        /// We need the training program id for this since the training program may override those details
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <param name="trainingProgramId"></param>
        /// <returns></returns>
        public SingleResult<Course> GetCourse(int customerId, int userId, int courseId, int trainingProgramId, Data.Customer customer)
        {
            Data.TrainingProgram tpData = null;
            if (customer == null)
            {
                customer = new Data.Customer(customerId);
            }

            if (trainingProgramId > 0)
            {
                // SALESFORCE! - Need to be able to access courses accross customers!
                //
                // Check if this is a training program from another customer
                // this happens if it is purchased through salesforce
                tpData = new Data.TrainingProgram(trainingProgramId);

                if (tpData.IsSalesforce) // set when salesforce coppies the training program
                {
                    // Look up the training program parent by using the salesforce entitlement
                    Data.SalesforceEntitlement salesforceEntitlement = Select.AllColumnsFrom<Data.SalesforceEntitlement>()
                        .Where(Data.SalesforceEntitlement.TrainingProgramIdColumn).IsEqualTo(trainingProgramId)
                        .And(Data.SalesforceEntitlement.UserIdColumn).IsEqualTo(userId)
                        .ExecuteSingle<Data.SalesforceEntitlement>();

                    if (salesforceEntitlement == null || salesforceEntitlement.ParentTrainingProgramID < 1)
                    {
                        throw new Exception("Could not find the reference training program purchased through Salesforce.");
                    }

                    Data.TrainingProgram parentTrainingProgram = new Data.TrainingProgram(salesforceEntitlement.ParentTrainingProgramID);

                    if (parentTrainingProgram.CustomerID < 1)
                    {
                        throw new Exception("Could not find the reference training program purchased through Salesforce.");
                    }

                    // Swap out the customer id so we can find the courses contained in this training program.
                    customerId = parentTrainingProgram.CustomerID;
                }
            }

            SingleResult<Course> courseResult = GetCourse(customerId, userId, courseId);

            if (trainingProgramId > 0)
            {
                TrainingProgram tp = (new PortalController()).GetTrainingProgramDetails(userId, trainingProgramId, false).Data;

                List<Course> allCourses = new List<Course>();
                allCourses.AddRange(tp.RequiredCourses);
                allCourses.AddRange(tp.OptionalCourses);
                allCourses.AddRange(tp.ElectiveCourses);
                allCourses.AddRange(tp.FinalAssessments);

                courseResult.Data.ProctorRequiredIndicator = (allCourses.Where(
                                                                    c => c.ProctorRequiredIndicator == true &&
                                                                    c.Id == courseId
                                                                ).Count() > 0) ?
                                                                true :
                                                                courseResult.Data.ProctorRequiredIndicator;

                courseResult.Data.IsCourseWorkAllowed = tp.IsCourseWorkAllowed;
            }

            courseResult.Data.IsUserValidationAllowed = IsUserValidationAllowed(courseId, trainingProgramId);
            courseResult.Data.AffidavitID = GetAffidavitID(courseId, trainingProgramId);

            return courseResult;
        }
        /// <summary>
        /// Only retrieves online courses
        /// </summary>
        /// <param name="customerId">Customer the user is in</param>
        /// <param name="userId">User requesting the course</param>
        /// <param name="courseId">The course id of the online course</param>
        /// <returns></returns>
        public SingleResult<Course> GetCourse(int customerId, int userId, int courseId)
        {
            List<string> columns = Data.OnlineCourse.Schema.Columns.Select(c => c.QualifiedName).ToList<string>();
            columns.Add(Data.CategoryHierarchy.Columns.LevelIndentText);

            SqlQuery query = new Select(columns.ToArray()).From<Data.OnlineCourse>()
                .Where(Data.OnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.OnlineCourse.Columns.Id).IsEqualTo(courseId)
                .ApplyLevelIndentText<Data.Category>(Data.OnlineCourse.CategoryIDColumn);

            SingleResult<Course> result = new SingleResult<Course>(query);

            result.Data.CourseTypeID = (int)CourseType.Online;

            if (result.Data.ArtisanCourseID > 0)
            {
                string parameters = new Select(Data.ArtisanCourse.ParametersColumn).From<Data.ArtisanCourse>()
                    .Where(Data.ArtisanCourse.IdColumn).IsEqualTo(result.Data.ArtisanCourseID)
                    .ExecuteScalar<string>();

                if (!String.IsNullOrEmpty(parameters))
                {
                    try
                    {
                        List<string> parameterList = Utilities.Deserialize<List<string>>(parameters);

                        if (parameterList.Count > 0)
                        {
                            result.Data.HasCourseParameters = true;

                            result.Data.OnlineCourseParameterOverrides = Select.AllColumnsFrom<Data.OnlineCourseOverrideDetail>()
                                .Where(Data.OnlineCourseOverrideDetail.Columns.OnlineCourseID).IsEqualTo(courseId)
                                .And(Data.OnlineCourseOverrideDetail.Columns.Code).In(parameterList.ToArray())
                                .ExecuteTypedList<OnlineCourseParameterOverride>();
                        }
                    }
                    catch (System.Runtime.Serialization.SerializationException e)
                    {
                        // We shouldn't get to this point, but log it if we do. 
                        // Would mean the list of assigned parameters for the course is corrupt. Resaving the artisan course should fix this
                        Log.Warn("Could not deserialize parameter list for Artisan Course ID " + result.Data.ArtisanCourseID + " the error was: " + e.Message);
                    }
                }
            }

            if (result.Data != null)
            {
                Data.Category category = new Symphony.Core.Data.Category(result.Data.CategoryId);
                result.Data.CategoryName = category.Name;

                Models.ArtisanTheme theme = Select.AllColumnsFrom<Data.ArtisanTheme>()
                    .Where(Data.ArtisanTheme.Columns.Id).IsEqualTo(result.Data.SkinOverride)
                    .ExecuteSingle<ArtisanTheme>();
                result.Data.Theme = theme;

                Models.Theme themeFlavor = Select.AllColumnsFrom<Data.Theme>()
                    .Where(Data.Theme.Columns.Id).IsEqualTo(result.Data.ThemeFlavorOverrideID)
                    .ExecuteSingle<Theme>();
                result.Data.ThemeFlavor = themeFlavor;

                result.Data.Authors = Select.AllColumnsFrom<Data.User>()
                    .Where(Data.User.IdColumn).In(new Select(Data.OnlineCourseAuthorLink.Columns.UserId)
                        .From<Data.OnlineCourseAuthorLink>()
                        .Where(Data.OnlineCourseAuthorLink.Columns.OnlineCourseId).IsEqualTo(result.Data.Id)
                    )
                    .ExecuteTypedList<Models.Author>();

                List<Data.OnlineCourseSecondaryCategoryLink> querySecondaryLinks = Select.AllColumnsFrom<Data.OnlineCourseSecondaryCategoryLink>()
                    .Where(Data.OnlineCourseSecondaryCategoryLink.Columns.OnlineCourseId).IsEqualTo(result.Data.Id).ExecuteTypedList<Data.OnlineCourseSecondaryCategoryLink>();

                if (result.Data.SecondaryCategories == null) result.Data.SecondaryCategories = new List<Category>();
                foreach (Data.OnlineCourseSecondaryCategoryLink c in querySecondaryLinks)
                {
                    Data.SecondaryCategory secondaryCategory = new Data.SecondaryCategory(c.SecondaryCategoryId);
                    result.Data.SecondaryCategories.Add(new Category() { Id = c.SecondaryCategoryId, Name = secondaryCategory.Name });
                }

                var accreditations = Accreditation.QueryOnlineAccreditations(result.Data.Id);
                result.Data.Accreditations = accreditations.Data.ToList();
            }
            return result;
        }



        public MultipleResult<Course> GetCourseByArtisanCourseId(int customerId, int artisanCourseId)
        {
            // get the most recently created version of the specified course
            SqlQuery query = Select.AllColumnsFrom<Data.OnlineCourse>()
                .Where(Data.OnlineCourse.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.OnlineCourse.Columns.ArtisanCourseID).IsEqualTo(artisanCourseId)
                .OrderDesc(Data.OnlineCourse.Columns.Id);

            MultipleResult<Course> result = new MultipleResult<Course>(query);

            if (result.Data != null)
            {
                foreach (Course c in result.Data)
                {
                    Data.Category category = new Symphony.Core.Data.Category(c.CategoryId);
                    c.CategoryName = category.Name;
                }
            }

            return result;
        }

        public List<Data.Customer> GetCustomersWithArtisanCourses(List<Data.ArtisanCourse> artisanCourses)
        {
            if (artisanCourses.Count < 1)
            {
                throw new Exception("No courses specified.");
            }

            SqlQuery query = Select.AllColumnsFrom<Data.Customer>()
                .InnerJoin(Data.OnlineCourse.Schema.TableName, Data.OnlineCourse.Columns.CustomerID, Data.Customer.Schema.TableName, Data.Customer.Columns.Id)
                .Where(Data.OnlineCourse.Columns.ArtisanCourseID).In(artisanCourses.Select(c => c.OriginalCourseId.Value).ToList());

            var customers = query.ExecuteTypedList<Data.Customer>();

            return customers;
        }

        public SingleResult<Course> SaveCourse(int customerId, int userId, int courseId, Course course)
        {
            Data.OnlineCourse data = new Symphony.Core.Data.OnlineCourse(courseId);
            int currentVersion = data.Version;

            if (course.CertificateEnabled == null)
            {
                course.CertificateEnabled = data.CertificateEnabled;
            }

            var oldPublishedCourseId = data.PublishedArtisanCourseID;

            course.CopyTo(data);

            data.Id = courseId;
            data.CustomerID = customerId;
            data.PublishedArtisanCourseID = oldPublishedCourseId; // don't allow this to ever be overwritten by a save

            if (data.IsSurvey && data.IsTest)
            {
                throw new Exception("Course cannot be a survey and a test.");
            }

            // version only changes with uploads
            data.Version = currentVersion;
            if (data.CategoryID == 0)
            {
                // make sure we set a default category
                Data.Category category = GetDefaultCategory(customerId);
                if (category != null)
                {
                    data.CategoryID = category.Id;
                }
            }
            else
            {
                data.CategoryID = course.CategoryId;
            }

            data.Save(Username);

            // Save parameter overrides
            if (course.OnlineCourseParameterOverrides != null)
            {
                Data.OnlineCourseParameterOverride.Destroy(Data.OnlineCourseParameterOverride.Columns.OnlineCourseID, data.Id);
                Data.OnlineCourseParameterOverrideCollection overrides = new Data.OnlineCourseParameterOverrideCollection();
                foreach (OnlineCourseParameterOverride onlineCourseOverrirde in course.OnlineCourseParameterOverrides)
                {
                    Data.OnlineCourseParameterOverride overrideData = new Data.OnlineCourseParameterOverride();
                    overrideData.ParameterSetOptionID = onlineCourseOverrirde.ParameterSetOptionID;
                    overrideData.OnlineCourseID = data.Id;
                    overrides.Add(overrideData);
                }
                overrides.SaveAll(Username);
            }

            return GetCourse(customerId, userId, data.Id);
        }

        public MultipleResult<ScormRecord> GetOnlineCourseStatus(int onlineCourseRollupId)
        {
            Data.OnlineCourseRollup rollup = new Data.OnlineCourseRollup(onlineCourseRollupId);

            if (!UserIsSalesChannelAdmin)
            {
                Data.User user = new Data.User(rollup.UserID);
                if (user.CustomerID != CustomerID)
                {
                    throw new Exception("You cannot access course data for students outside of your customer.");
                }
                if (!HasRole(Roles.CourseAssignmentAdministrator)) {
                    throw new Exception("You do not have permission to access student data");
                }
            }

            List<ScormRecord> scormRecords = Data.SPs.GetScormData(rollup.UserID, rollup.CourseID, rollup.TrainingProgramID)
                .ExecuteTypedList<ScormRecord>();

            return new MultipleResult<ScormRecord>(scormRecords);
        }

        public MultipleResult<ScormRecord> UpdateOnlineCourseStatus(ScormCourseProgress progress, int onlineCourseRollupId)
        {
            Data.OnlineCourseRollup rollup = new Data.OnlineCourseRollup(onlineCourseRollupId);

            if (!UserIsSalesChannelAdmin)
            {
                throw new Exception("You must be a sales channel admin to edit student course progress.");
            }

            int registrationId = 0;

            if (!int.TryParse(rollup.CurrentRegistrationID, out registrationId))
            {
                throw new Exception("The student registration ID is " + rollup.CurrentRegistrationID + " which could not be parsed to an integer. This student cannot have their course progress updated.");
            }

            Data.SPs.UpdateScormActivity(registrationId, progress.Bookmark, progress.DataChunk).Execute();

            return GetOnlineCourseStatus(onlineCourseRollupId);
        }

        #endregion

        #region Training Programs

        public PagedResult<TrainingProgramBundle> GetTrainingProgramBundles(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.TrainingProgramBundle>()
                .WhereExpression(Data.TrainingProgramBundle.Columns.Name).ContainsString(searchText)
                    .Or(Data.TrainingProgramBundle.Columns.Sku).ContainsString(searchText)
                .CloseExpression()
                .And(Data.TrainingProgramBundle.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.TrainingProgramBundle.Columns.IsDeleted).IsEqualTo(false);

            return new PagedResult<TrainingProgramBundle>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        private void ValidateSku(int customerId, string sku, int entityId, bool isTrainingProgram)
        {
            int trainingProgramId = 0;
            int trainingProgramBundleId = 0;

            if (isTrainingProgram)
            {
                trainingProgramId = entityId;
            }
            else
            {
                trainingProgramBundleId = entityId;
            }

            string existingTpSku = new Select(Data.TrainingProgram.SkuColumn)
                    .From<Data.TrainingProgram>()
                    .Where(Data.TrainingProgram.SkuColumn).IsEqualTo(sku)
                    .And(Data.TrainingProgram.CustomerIDColumn).IsEqualTo(customerId)
                    .And(Data.TrainingProgram.IdColumn).IsNotEqualTo(trainingProgramId)
                    .ExecuteScalar<string>();

            string existingBundleSku = new Select(Data.TrainingProgramBundle.SkuColumn)
                .From<Data.TrainingProgramBundle>()
                .Where(Data.TrainingProgramBundle.SkuColumn).IsEqualTo(sku)
                .And(Data.TrainingProgramBundle.IdColumn).IsNotEqualTo(trainingProgramBundleId)
                .And(Data.TrainingProgramBundle.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.TrainingProgramBundle.IsDeletedColumn).IsEqualTo(0)
                .ExecuteScalar<string>();

            if (!string.IsNullOrEmpty(existingTpSku) || !string.IsNullOrEmpty(existingBundleSku))
            {
                throw new Exception("The SKU entered is already in use.");
            }
        }

        public PagedResult<TrainingProgram> FindTrainingPrograms(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, TrainingProgramFilter filter, List<GenericFilter> genericFilters = null)
        {
            // If the training program customer id is not equal to the current customer id, then it must be shared
            string sharedColumn = string.Format("CASE WHEN [dbo].[TrainingProgram].[CustomerID] = {0} THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END", customerId);
            
            // Get all the training programs, join shared training programs, this may create duplicates
            SqlQuery query = new Select().From<Data.TrainingProgram>()
                .LeftOuterJoin(Data.NetworkSharedTrainingProgram.TrainingProgramIdColumn, Data.TrainingProgram.IdColumn)
                .LeftOuterJoin(Data.CustomerNetworkDetail.DetaildIdColumn, Data.NetworkSharedTrainingProgram.CustomerNetworkDetailIdColumn);

            query
                // Don't include salesforce copies
                .Where(Data.TrainingProgram.Columns.IsSalesforce).IsEqualTo(false)
                // Include tps for this customer + shared to this customer
                .AndNestedExpression(Data.TrainingProgram.CustomerIDColumn.QualifiedName).IsEqualTo(customerId)
                    .OrNestedExpression(Data.CustomerNetworkDetail.DestinationCustomerIdColumn.QualifiedName).IsEqualTo(customerId)
                        .And(Data.CustomerNetworkDetail.AllowTrainingProgramSharingColumn.QualifiedName).IsEqualTo(true)
                    .CloseExpression()
                .CloseExpression();

            if (genericFilters != null)
            {
                query.ApplyGenericFilter<Data.TrainingProgram>(genericFilters);
            }

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                // Allow search by sku or name
                query.AndExpression(Data.TrainingProgram.NameColumn.QualifiedName).ContainsString(searchText)
                    .Or(Data.TrainingProgram.Columns.Sku).ContainsString(searchText)
                .CloseExpression();
            }

            // Filter data here based on the passed variables
            if (filter != null)
            {
                if (filter.Archived) // Archived will show training programs that have expired end dates
                {
                    DateTime archiveDate = filter.Archived ? DateTime.Now : new DateTime(9999, 12, 31);
                    query.And(Data.TrainingProgram.Columns.EndDate).IsLessThan(archiveDate);
                }

                if (filter.Enabled) // Training programs with the enabled flag on
                {
                    query.And(Data.TrainingProgram.Columns.IsLive).IsEqualTo(true);
                }

                if (filter.Disabled) // Training programs without the enabled flag
                {
                    query.And(Data.TrainingProgram.Columns.IsLive).IsEqualTo(false);
                }

                if (filter.Active) // Training programs that have an end date in the future. 
                {
                    query.And(Data.TrainingProgram.Columns.EndDate).IsGreaterThanOrEqualTo(DateTime.Now)
                        .And(Data.TrainingProgram.Columns.IsLive).IsEqualTo(true);
                }
            }

            if (orderBy == "sharedFlag")
            {
                orderBy = sharedColumn;
            }

            // Remove duplicates caused by the left joins
            foreach (TableSchema.TableColumn col in Data.TrainingProgram.Schema.Columns)
            {
                query.GroupBy(col);
            }
            query.GroupBy(sharedColumn, "SharedFlag");

            if (orderBy == "isActive")
            {
                orderBy = "";
            }

            return new PagedResult<TrainingProgram>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<TrainingProgram> FindTrainingProgramsForTemplate(int customerId, int templateId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.TrainingProgram>()
                .InnerJoin(Data.TrainingProgramTemplate.TrainingProgramIDColumn, Data.TrainingProgram.IdColumn)
                .Where(Data.TrainingProgramTemplate.TemplateIDColumn).IsEqualTo(templateId)
                .And(Data.TrainingProgram.CustomerIDColumn).IsEqualTo(customerId);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.And(Data.TrainingProgram.NameColumn).ContainsString(searchText);
            }

            return new PagedResult<TrainingProgram>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<TrainingProgram> GetTrainingProgramsHoldByCustomer(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.TrainingProgram>()
                .Where(Data.TrainingProgram.Columns.IsSalesforce).IsEqualTo(false)
                .And(Data.TrainingProgram.Columns.CustomerID).IsEqualTo(customerId);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                // Allow search by sku or name
                query.AndExpression(Data.TrainingProgram.NameColumn.QualifiedName).ContainsString(searchText)
                    .Or(Data.TrainingProgram.Columns.Sku).ContainsString(searchText)
                .CloseExpression();
            }

            return new PagedResult<TrainingProgram>(query, pageIndex, pageSize, orderBy, orderDir);
 
        }

        public SingleResult<TrainingProgramBundle> GetTrainingProgramBundle(int customerId, int userId, int trainingProgramBundleId)
        {
            TrainingProgramBundle bundle = Select.AllColumnsFrom<Data.TrainingProgramBundle>()
                .IncludeColumn(Data.CategoryHierarchy.Columns.LevelIndentText, "LevelIndentText")
                .Where(Data.TrainingProgramBundle.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.TrainingProgramBundle.IdColumn).IsEqualTo(trainingProgramBundleId)
                .And(Data.TrainingProgramBundle.IsDeletedColumn).IsEqualTo(false)
                .ApplyLevelIndentText<Data.Category>(Data.TrainingProgramBundle.CategoryIDColumn)
                .ExecuteSingle<TrainingProgramBundle>();

            if (bundle == null)
            {
                throw new Exception("Bundle not found.");
            }

            bundle.TrainingPrograms = Select.AllColumnsFrom<Data.TrainingProgram>()
                .InnerJoin(Data.TrainingProgramBundleMap.TrainingProgramIDColumn, Data.TrainingProgram.IdColumn)
                .Where(Data.TrainingProgramBundleMap.TrainingProgramBundleIDColumn).IsEqualTo(trainingProgramBundleId)
                .And(Data.TrainingProgram.CustomerIDColumn).IsEqualTo(customerId)
                .OrderAsc(Data.TrainingProgramBundleMap.OrderColumn.QualifiedName)
                .ExecuteTypedList<TrainingProgram>();

            return new SingleResult<TrainingProgramBundle>(bundle);
        }

        public SingleResult<TrainingProgramBundle> SaveTrainingProgramBundle(int customerId, int userId, int trainingProgramBundleId, TrainingProgramBundle trainingProgramBundle)
        {
            Data.TrainingProgramBundle bundle = new Data.TrainingProgramBundle(trainingProgramBundleId);

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                if (string.IsNullOrEmpty(trainingProgramBundle.Sku))
                {
                    throw new Exception("Please provide a SKU.");
                }

                ValidateSku(customerId, trainingProgramBundle.Sku, trainingProgramBundleId, false);

                trainingProgramBundle.CopyTo(bundle);

                bundle.Id = trainingProgramBundleId;
                bundle.CustomerID = customerId;

                bundle.Save(userId);

                Data.TrainingProgramBundleMap.Destroy(Data.TrainingProgramBundleMap.Columns.TrainingProgramBundleID, bundle.Id);

                int order = 0;
                foreach (TrainingProgram trainingProgram in trainingProgramBundle.TrainingPrograms)
                {
                    var map = new Data.TrainingProgramBundleMap()
                    {
                        TrainingProgramID = trainingProgram.Id,
                        TrainingProgramBundleID = bundle.Id,
                        Order = order
                    };
                    order++;
                    map.Save();
                }

                ts.Complete();
            }

            return GetTrainingProgramBundle(customerId, userId, bundle.Id);
        }

        public SimpleSingleResult<bool> DeleteTrainingProgramBundle(int customerId, int userId, int trainingProgramId)
        {
            Data.TrainingProgramBundle bundle = Select.AllColumnsFrom<Data.TrainingProgramBundle>()
                .Where(Data.TrainingProgramBundle.IdColumn).IsEqualTo(trainingProgramId)
                .And(Data.TrainingProgramBundle.CustomerIDColumn).IsEqualTo(customerId)
                .ExecuteSingle<Data.TrainingProgramBundle>();

            bundle.IsDeleted = true;

            bundle.Save(userId);

            return new SimpleSingleResult<bool>(true);
        }

        public SingleResult<TrainingProgram> SaveTrainingProgram(int customerId, int userId, int trainingProgramId, TrainingProgram trainingProgram, bool isCheckForRollup = false)
        {
            bool isNew = trainingProgramId == 0;

            bool isLiveChanged = false;
            bool isCourseChanged = false;
            bool isUserAssignmentChanged = false;
            bool isDateChanged = false;
            List<string> currentCourseKeys = new List<string>();
            List<int> currentUserIds = new List<int>();
            List<int> newUserIds = null;
            List<int> duplicateUserIds = null;
            
            Data.TrainingProgramToCourseLinkCollection links = new Data.TrainingProgramToCourseLinkCollection();

            Data.TrainingProgram data = new Symphony.Core.Data.TrainingProgram(trainingProgramId);
            Data.TrainingProgram dataCustomerTrainingProgram = null;
            if (trainingProgram.SharedFlag)
            {
                dataCustomerTrainingProgram = new Symphony.Core.Data.TrainingProgram("ID", trainingProgramId);
            }

            if (!string.IsNullOrEmpty(trainingProgram.Sku))
            {
                ValidateSku(customerId, trainingProgram.Sku, trainingProgramId, true);
            }

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                bool surveyMatches = false;

                if (data.SurveyID > 0 && trainingProgram.SurveyID > 0)
                {
                    Data.OnlineCourse newSurvey = new Data.OnlineCourse(trainingProgram.SurveyID);
                    Data.OnlineCourse dupSurvey = new Data.OnlineCourse(data.SurveyID);
                    if (newSurvey.Id == dupSurvey.DuplicateFromID)
                    {
                        surveyMatches = true;
                        trainingProgram.SurveyID = data.SurveyID;
                    }
                }

                if (trainingProgram.IsAdvancedCompletionPercentage)
                {
                    List<TrainingProgramCourse> coursesMissingDuration = 
                        trainingProgram.RequiredCourses.Where(r => r.Duration == 0 && r.AutomaticDuration == 0)
                        .Concat(trainingProgram.ElectiveCourses.Where(e => e.Duration == 0 && e.AutomaticDuration == 0))
                        .Concat(trainingProgram.FinalAssessments.Where(f => f.Duration == 0 && f.AutomaticDuration == 0))
                        .ToList();

                    if (coursesMissingDuration.Count > 0)
                    {
                        string missingCourses = "<br/><br/>" +
                            string.Join("<br/>", coursesMissingDuration.Select(c => c.Name));

                        throw new Exception("All courses must have a duration set when using duration based completion percentage. The following courses are missing durations: " + missingCourses);
                    }

                }

                bool customerIdSet = false;
                if (data.ParentTrainingProgramID > 0)
                {
                    // If the training program exists already and there
                    // is a parent training program id set make sure it 
                    // doesn't change, or get set to 0. 
                    trainingProgram.ParentTrainingProgramID = data.ParentTrainingProgramID.Value;
                    
                    // We also need to make sure the customer id does not 
                    // get changed to the user who is altering this training program
                    // In the case of a saleschannel admin altering a salesforce
                    // training program
                    trainingProgram.CustomerID = data.CustomerID;

                    // Don't change the purchased from bundle ID. It will 0 it out otherwise
                    trainingProgram.PurchasedFromBundleId = data.PurchasedFromBundleID;

                    customerIdSet = true;
                }

                if (isCheckForRollup)
                {
                
                    isLiveChanged = !data.IsLive && trainingProgram.IsLive;
                    isDateChanged = trainingProgram.StartDate.HasValue && trainingProgram.StartDate.Value.Date != data.StartDate.Date ||
                                    trainingProgram.EndDate.HasValue && trainingProgram.EndDate.Value.Date != data.EndDate.Date ||
                                    trainingProgram.DueDate.HasValue && trainingProgram.DueDate.Value.Date != data.DueDate.Date;

                    currentCourseKeys = data.TrainingProgramToCourseLinks.Select(l => l.CourseID + "-" + l.CourseTypeID).ToList();

                    currentUserIds = data.GetUnfilteredAssignedUsers();
                }

                trainingProgram.CopyTo(data);
                data.Id = trainingProgramId;

                if (!customerIdSet)
                {
                    if (trainingProgram.CustomerID != 0)
                    {
                        data.CustomerID = trainingProgram.CustomerID;
                    }
                    else
                    {
                        data.CustomerID = customerId;
                    }
                }
                if (trainingProgram.SharedFlag && dataCustomerTrainingProgram != null)
                {
                    data.CustomerID = dataCustomerTrainingProgram.CustomerID;
                }

                if (!data.IsNewHire && data.IsLive)
                {
                    if (trainingProgram.StartDate == null || trainingProgram.EndDate == null)
                    {
                        throw new Exception("Start date and end dates are required in order to enable a non-new-hire training program.");
                    }
                }

                // make sure we have nice date times for sql so we don't get overflows
                if (trainingProgram.DueDate == null)
                {
                    data.DueDate = DefaultDateTime;
                }
                if (trainingProgram.StartDate == null)
                {
                    data.StartDate = DefaultDateTime;
                }
                if (trainingProgram.EndDate == null)
                {
                    data.EndDate = DefaultDateTime;
                }
                if (trainingProgram.CategoryID == 0)
                {
                    Data.Category category = GetDefaultCategory(customerId);
                    if (category != null)
                    {
                        data.CategoryID = category.Id;
                    }
                }

                if (!trainingProgram.NewHireOffsetEnabled)
                {
                    data.NewHireStartDateOffset = 0;
                    data.NewHireEndDateOffset = 0;
                    data.NewHireDueDateOffset = 0;
                }

                // if this is a duplication operation, move the dates out by a year and change the text a bit to make it obvious
                if (trainingProgram.DuplicateFromID > 0)
                {
                    // if this is being created from salesforce then the values below will have already been set
                    if (!trainingProgram.IsSalesforce)
                    {
                        if (trainingProgram.DueDate.HasValue)
                        {
                            data.DueDate = trainingProgram.DueDate.Value.AddYears(1);
                        }
                        data.StartDate = data.StartDate.AddYears(1);
                        data.EndDate = data.EndDate.AddYears(1);
                        data.Name = "Duplicate of " + data.Name;
                    }

                    foreach (TrainingProgramCourse course in trainingProgram.RequiredCourses)
                    {
                        course.DueDate = null;
                    }
                    foreach (TrainingProgramCourse course in trainingProgram.OptionalCourses)
                    {
                        course.DueDate = null;
                    }
                    foreach (TrainingProgramCourse course in trainingProgram.ElectiveCourses)
                    {
                        course.DueDate = null;
                    }
                    foreach (TrainingProgramCourse course in trainingProgram.FinalAssessments)
                    {
                        course.DueDate = null;
                    }

                    foreach (TrainingProgramCourse course in trainingProgram.RequiredCourses)
                    {
                        course.ActiveAfterDate = null;
                    }
                    foreach (TrainingProgramCourse course in trainingProgram.OptionalCourses)
                    {
                        course.ActiveAfterDate = null;
                    }
                    foreach (TrainingProgramCourse course in trainingProgram.ElectiveCourses)
                    {
                        course.ActiveAfterDate = null;
                    }
                    foreach (TrainingProgramCourse course in trainingProgram.FinalAssessments)
                    {
                        course.ActiveAfterDate = null;
                    }

                    using (TransactionScope surveyTransaction = new TransactionScope(TransactionScopeOption.Suppress))
                    {
                        if (data.SurveyID > 0)
                        {
                            Data.OnlineCourse survey = new Data.OnlineCourse(data.SurveyID);
                            if (survey.DuplicateFromID > 0)
                            {
                                survey = new Data.OnlineCourse(survey.DuplicateFromID);
                            }

                            data.SurveyID = CourseController.DuplicateSurvey(survey.Id, customerId, Username).Id;
                        }
                    }
                }
                else
                {
                    if (data.SurveyID > 0 && !surveyMatches)
                    {
                        using (TransactionScope surveyTransaction = new TransactionScope(TransactionScopeOption.Suppress))
                        {
                            data.SurveyID = CourseController.DuplicateSurvey(data.SurveyID, customerId, Username).Id;
                        }
                    }
                }


                /*if (data.EndDate != null && data.EndDate.Hour == 0)
                {
                    // make the end date be the last second of the selected day
                    data.EndDate = data.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                }*/

                data.EndDate = data.EndDate.Date;
                data.StartDate = data.StartDate.Date;
                data.DueDate = data.DueDate.Date;

                if (data.EnforceCanMoveForwardIndicator)
                {
                    data.EnforceRequiredOrder = true;
                }

                // save the tp right away so all links get the right id
                data.Save(Username);
                trainingProgramId = data.Id;

                // create new links to courses
                int count = 0;
                foreach (TrainingProgramCourse course in trainingProgram.RequiredCourses)
                {
                    Data.TrainingProgramToCourseLink link = new Data.TrainingProgramToCourseLink();
                    link.TrainingProgramID = trainingProgramId;
                    link.CourseID = course.Id;
                    if (course.DueDate.HasValue)
                    {
                        link.DueDate = course.DueDate.Value.Date;
                    }
                    else
                    {
                        link.DueDate = DefaultDateTime;
                    }

                    if (course.ActiveAfterDate.HasValue)
                    {
                        link.ActiveAfterDate = course.ActiveAfterDate.Value.Date;
                    }
                    else
                    {
                        link.ActiveAfterDate = DefaultDateTime;
                    }

                    if (course.ActiveAfterMinutes.HasValue)
                    {
                        link.ActiveAfterMinutes = course.ActiveAfterMinutes.Value;
                    }
                    else
                    {
                        link.ActiveAfterMinutes = null;
                    }

                    if (course.ExpiresAfterMinutes.HasValue)
                    {
                        link.ExpiresAfterMinutes = course.ExpiresAfterMinutes.Value;
                    }
                    else
                    {
                        link.ExpiresAfterMinutes = null;
                    }

                    if (course.ProctorRequiredIndicator.HasValue)
                    {
                        link.ProctorRequiredIndicator = course.ProctorRequiredIndicator.Value;
                    }
                    else
                    {
                        link.ProctorRequiredIndicator = null;
                    }

                    if (course.EnforceCanMoveForwardIndicator.HasValue)
                    {
                        link.EnforceCanMoveForwardIndicator = course.EnforceCanMoveForwardIndicator.Value;
                    }
                    else
                    {
                        link.EnforceCanMoveForwardIndicator = null;
                    }

                    if (course.ActiveAfterMode > 0)
                    {
                        link.ActiveAfterMode = course.ActiveAfterMode;
                    }
                    else
                    {
                        link.DueMode = null;
                    }

                    if (course.DueMode > 0)
                    {
                        link.DueMode = course.DueMode;
                    }
                    else
                    {
                        link.DueMode = null;
                    }

                    if ((link.DueMode == (int)RelativeTimeMode.Session || link.ActiveAfterMode == (int)RelativeTimeMode.Session) && trainingProgram.SessionCourseID <= 1)
                    {
                        throw new Exception("You cannot use the session date for course timing if you do not have a session selected. Please select a session.");
                    }

                    link.DenyAccessAfterDueDateIndicator = course.DenyAccessAfterDueDateIndicator;


                    link.RequiredSyllabusOrder = count++;
                    link.CourseTypeID = course.CourseTypeID;
                    
                    //make sure we use the SAME customer ID as the trainingprogram uses.
                    link.CustomerID = data.CustomerID;
                    
                    link.SyllabusTypeID = (int)SyllabusType.Required;
                    links.Add(link);
                }

                foreach (TrainingProgramCourse course in trainingProgram.ElectiveCourses)
                {
                    Data.TrainingProgramToCourseLink link = new Data.TrainingProgramToCourseLink();
                    link.TrainingProgramID = trainingProgramId;
                    link.CourseID = course.Id;
                    if (course.DueDate.HasValue)
                    {
                        link.DueDate = course.DueDate.Value.Date;
                    }
                    else
                    {
                        link.DueDate = DefaultDateTime;
                    }
                    if (course.ActiveAfterDate.HasValue)
                    {
                        link.ActiveAfterDate = course.ActiveAfterDate.Value.Date;
                    }
                    else
                    {
                        link.ActiveAfterDate = DefaultDateTime;
                    }

                    if (course.ActiveAfterMinutes.HasValue)
                    {
                        link.ActiveAfterMinutes = course.ActiveAfterMinutes.Value;
                    }
                    else
                    {
                        link.ActiveAfterMinutes = null;
                    }

                    if (course.ExpiresAfterMinutes.HasValue)
                    {
                        link.ExpiresAfterMinutes = course.ExpiresAfterMinutes.Value;
                    }
                    else
                    {
                        link.ExpiresAfterMinutes = null;
                    }

                    if (course.ProctorRequiredIndicator.HasValue)
                    {
                        link.ProctorRequiredIndicator = course.ProctorRequiredIndicator.Value;
                    }
                    else
                    {
                        link.ProctorRequiredIndicator = null;
                    }

                    if (course.EnforceCanMoveForwardIndicator.HasValue)
                    {
                        link.EnforceCanMoveForwardIndicator = course.EnforceCanMoveForwardIndicator.Value;
                    }
                    else
                    {
                        link.EnforceCanMoveForwardIndicator = null;
                    }

                    if (course.ActiveAfterMode > 0)
                    {
                        link.ActiveAfterMode = course.ActiveAfterMode;
                    }
                    else
                    {
                        link.DueMode = null;
                    }

                    if (course.DueMode > 0)
                    {
                        link.DueMode = course.DueMode;
                    }
                    else
                    {
                        link.DueMode = null;
                    }

                    if ((link.DueMode == (int)RelativeTimeMode.Session || link.ActiveAfterMode == (int)RelativeTimeMode.Session) && trainingProgram.SessionCourseID <= 1)
                    {
                        throw new Exception("You cannot use the session date for course timing if you do not have a session selected. Please select a session.");
                    }

                    link.DenyAccessAfterDueDateIndicator = course.DenyAccessAfterDueDateIndicator;

                    link.CourseTypeID = course.CourseTypeID;
                    link.CustomerID = customerId;
                    link.SyllabusTypeID = (int)SyllabusType.Elective;
                    links.Add(link);
                }

                foreach (TrainingProgramCourse course in trainingProgram.OptionalCourses)
                {
                    Data.TrainingProgramToCourseLink link = new Data.TrainingProgramToCourseLink();
                    link.TrainingProgramID = trainingProgramId;
                    link.CourseID = course.Id;
                    if (course.DueDate.HasValue)
                    {
                        link.DueDate = course.DueDate.Value.Date;
                    }
                    else
                    {
                        link.DueDate = DefaultDateTime;
                    }
                    if (course.ActiveAfterDate.HasValue)
                    {
                        link.ActiveAfterDate = course.ActiveAfterDate.Value.Date;
                    }
                    else
                    {
                        link.ActiveAfterDate = DefaultDateTime;
                    }

                    if (course.ActiveAfterMinutes.HasValue)
                    {
                        link.ActiveAfterMinutes = course.ActiveAfterMinutes.Value;
                    }
                    else
                    {
                        link.ActiveAfterMinutes = null;
                    }

                    if (course.ExpiresAfterMinutes.HasValue)
                    {
                        link.ExpiresAfterMinutes = course.ExpiresAfterMinutes.Value;
                    }
                    else
                    {
                        link.ExpiresAfterMinutes = null;
                    }

                    if (course.ProctorRequiredIndicator.HasValue)
                    {
                        link.ProctorRequiredIndicator = course.ProctorRequiredIndicator.Value;
                    }
                    else
                    {
                        link.ProctorRequiredIndicator = null;
                    }

                    if (course.EnforceCanMoveForwardIndicator.HasValue)
                    {
                        link.EnforceCanMoveForwardIndicator = course.EnforceCanMoveForwardIndicator.Value;
                    }
                    else
                    {
                        link.EnforceCanMoveForwardIndicator = null;
                    }

                    if (course.ActiveAfterMode > 0)
                    {
                        link.ActiveAfterMode = course.ActiveAfterMode;
                    }
                    else
                    {
                        link.DueMode = null;
                    }

                    if (course.DueMode > 0)
                    {
                        link.DueMode = course.DueMode;
                    }
                    else
                    {
                        link.DueMode = null;
                    }

                    if ((link.DueMode == (int)RelativeTimeMode.Session || link.ActiveAfterMode == (int)RelativeTimeMode.Session) && trainingProgram.SessionCourseID <= 1)
                    {
                        throw new Exception("You cannot use the session date for course timing if you do not have a session selected. Please select a session.");
                    }

                    link.DenyAccessAfterDueDateIndicator = course.DenyAccessAfterDueDateIndicator;

                    link.CourseTypeID = course.CourseTypeID;
                    link.CustomerID = customerId;
                    link.SyllabusTypeID = (int)SyllabusType.Optional;
                    links.Add(link);
                }

                foreach (TrainingProgramCourse course in trainingProgram.FinalAssessments)
                {
                    Data.TrainingProgramToCourseLink link = new Data.TrainingProgramToCourseLink();
                    link.TrainingProgramID = trainingProgramId;
                    link.CourseID = course.Id;
                    if (course.DueDate.HasValue)
                    {
                        link.DueDate = course.DueDate.Value.Date;
                    }
                    else
                    {
                        link.DueDate = DefaultDateTime;
                    }
                    if (course.ActiveAfterDate.HasValue)
                    {
                        link.ActiveAfterDate = course.ActiveAfterDate.Value.Date;
                    }
                    else
                    {
                        link.ActiveAfterDate = DefaultDateTime;
                    }

                    if (course.ActiveAfterMinutes.HasValue)
                    {
                        link.ActiveAfterMinutes = course.ActiveAfterMinutes.Value;
                    }
                    else
                    {
                        link.ActiveAfterMinutes = null;
                    }

                    if (course.ExpiresAfterMinutes.HasValue)
                    {
                        link.ExpiresAfterMinutes = course.ExpiresAfterMinutes.Value;
                    }
                    else
                    {
                        link.ExpiresAfterMinutes = null;
                    }

                    if (course.ProctorRequiredIndicator.HasValue)
                    {
                        link.ProctorRequiredIndicator = course.ProctorRequiredIndicator.Value;
                    }
                    else
                    {
                        link.ProctorRequiredIndicator = null;
                    }

                    if (course.EnforceCanMoveForwardIndicator.HasValue)
                    {
                        link.EnforceCanMoveForwardIndicator = course.EnforceCanMoveForwardIndicator.Value;
                    }
                    else
                    {
                        link.EnforceCanMoveForwardIndicator = null;
                    }

                    if (course.ActiveAfterMode > 0)
                    {
                        link.ActiveAfterMode = course.ActiveAfterMode;
                    }
                    else
                    {
                        link.DueMode = null;
                    }

                    if (course.DueMode > 0)
                    {
                        link.DueMode = course.DueMode;
                    }
                    else
                    {
                        link.DueMode = null;
                    }

                    if ((link.DueMode == (int)RelativeTimeMode.Session || link.ActiveAfterMode == (int)RelativeTimeMode.Session) && trainingProgram.SessionCourseID <= 1)
                    {
                        throw new Exception("You cannot use the session date for course timing if you do not have a session selected. Please select a session.");
                    }

                    link.DenyAccessAfterDueDateIndicator = course.DenyAccessAfterDueDateIndicator;
                    link.RequiredSyllabusOrder = count++;
                    link.CourseTypeID = course.CourseTypeID;
                    link.CustomerID = customerId;
                    link.SyllabusTypeID = (int)SyllabusType.Final;
                    links.Add(link);
                }

                // create new links to users
                Data.HierarchyToTrainingProgramLinkCollection hierarchies = new Data.HierarchyToTrainingProgramLinkCollection();
                if (trainingProgram.PrimaryAssignment != null)
                {
                    foreach (HierarchyLink hierarchy in trainingProgram.PrimaryAssignment)
                    {
                        Data.HierarchyToTrainingProgramLink link = new Data.HierarchyToTrainingProgramLink();
                        link.TrainingProgramID = trainingProgramId;
                        link.HierarchyNodeID = hierarchy.HierarchyNodeID;
                        link.HierarchyTypeID = hierarchy.HierarchyTypeID;
                        link.IsPrimary = true;
                        hierarchies.Add(link);
                    }
                }

                if (trainingProgram.SecondaryAssignment != null)
                {
                    foreach (HierarchyLink hierarchy in trainingProgram.SecondaryAssignment)
                    {
                        Data.HierarchyToTrainingProgramLink link = new Data.HierarchyToTrainingProgramLink();
                        link.TrainingProgramID = trainingProgramId;
                        link.HierarchyNodeID = hierarchy.HierarchyNodeID;
                        link.HierarchyTypeID = hierarchy.HierarchyTypeID;
                        link.IsPrimary = false;
                        hierarchies.Add(link);
                    }
                }

                // trash any existing links
                Data.TrainingProgramToCourseLink.Destroy(Data.TrainingProgramToCourseLink.Columns.TrainingProgramID, trainingProgramId);
                // save the new links
                links.SaveAll(Username);

                // trash any existing hierarchy setups
                Data.HierarchyToTrainingProgramLink.Destroy(Data.HierarchyToTrainingProgramLink.Columns.TrainingProgramID, trainingProgramId);
                // save the new hierarichies
                hierarchies.SaveAll(Username);

                Data.TrainingProgramBook.Destroy(Data.TrainingProgramBook.Columns.TrainingProgramId, trainingProgramId);
                foreach (Book b in trainingProgram.Books)
                {
                    Data.TrainingProgramBook.Insert(trainingProgramId, b.ID);
                }

                // Save the licenses
                (new LicenseController()).SaveLicensesForTrainingProgram(trainingProgramId, trainingProgram.Licenses);

                //Save leaders
                //  Get the current leaders
                List<Data.TrainingProgramLeader> currentLeaders = Select.AllColumnsFrom<Data.TrainingProgramLeader>()
                    .Where(Data.TrainingProgramLeader.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                    .ExecuteTypedList<Data.TrainingProgramLeader>();


                // kill the existing leader list
                List<int> leadersAdded = new List<int>();
                var tpIds = new Select(Data.TrainingProgramLeader.UserIDColumn)
                    .From(Data.Tables.TrainingProgramLeader)
                    .InnerJoin(Data.Tables.User, Data.User.Columns.Id, Data.Tables.TrainingProgramLeader, Data.TrainingProgramLeader.Columns.UserID)
                    .InnerJoin(Data.Tables.Customer, Data.Customer.Columns.Id, Data.Tables.User, Data.User.Columns.CustomerID)
                    .Where(Data.TrainingProgramLeader.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                    .And(Data.Customer.IdColumn).IsNotEqualTo(customerId)
                    .ExecuteTypedList<int>();

                if (trainingProgram.Leaders.Count > 0) {
                    TrainingProgramLeader primary = trainingProgram.Leaders.Where(l => l.IsPrimary).ToList().FirstOrDefault<TrainingProgramLeader>();
                    if (primary == null)
                    {
                        trainingProgram.Leaders[0].IsPrimary = true;
                    }
                }

                foreach (TrainingProgramLeader leader in trainingProgram.Leaders)
                {
                    Data.TrainingProgramLeader dataLeader = new Data.TrainingProgramLeader();

                    bool leaderExists = currentLeaders.Where(e => e.UserID == leader.UserID).Count() > 0;
                    if (leaderExists)
                    {
                        // Just always update the leaders as the primary flag may have changed.
                        dataLeader = new Data.TrainingProgramLeader(leader.Id);
                    }

                    dataLeader.UserID = leader.UserID;
                    dataLeader.TrainingProgramID = trainingProgramId;
                    dataLeader.IsPrimary = leader.IsPrimary;

                    dataLeader.Save(Username);

                    // if the instructor we just saved didn't exist in the list already, send a message
                    if (!currentLeaders.Any(c => c.UserID == leader.UserID))
                    {
                        leadersAdded.Add(leader.UserID);
                    }
                }
                foreach (var leader in currentLeaders)
                {
                    bool leaderRemoved = trainingProgram.Leaders.Where(l => l.UserID == leader.UserID).Count() == 0;
                    if (leaderRemoved)
                    {
                        bool allowToRemoveLeader = !tpIds.Contains(leader.UserID);
                        if (allowToRemoveLeader)
                        {
                            var delLeaderCommand = new Delete().From(Data.Tables.TrainingProgramLeader)
                                .Where(Data.TrainingProgramLeader.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                                .And(Data.TrainingProgramLeader.Columns.UserID).IsEqualTo(leader.UserID).Execute();
                        }
                    }
                }

                if (trainingProgram.Accreditations != null)
                {
                    foreach (var accreditation in trainingProgram.Accreditations)
                    {
                        accreditation.TrainingProgramId = trainingProgramId;
                        if (isNew)
                        {
                            accreditation.Id = 0;
                        }
                    }

                    Accreditation.SyncTrainingProgramAccreditations(trainingProgramId, trainingProgram.Accreditations);
                }

                // Save training program overrides
                if (trainingProgram.TrainingProgramParameterOverrides != null && trainingProgram.TrainingProgramParameterOverrides.Count > 0)
                {
                    Data.TrainingProgramParameterOverride.Destroy(Data.TrainingProgramParameterOverride.Columns.TrainingProgramID, trainingProgramId);
                    foreach (TrainingProgramParameterOverride tpo in trainingProgram.TrainingProgramParameterOverrides)
                    {
                        Data.TrainingProgramParameterOverride tpoData = new Data.TrainingProgramParameterOverride();
                        tpoData.TrainingProgramID = trainingProgramId;
                        tpoData.ParameterSetOptionID = tpo.ParameterSetOptionID;

                        tpoData.Save(Username);
                    }
                }


                // send training program assigned notifications
                // No longer looking up the template first, the send notification call will look up the 
                // appropriate template and ensure it is actually enabled for the training program. 
                Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);
                if (tp.IsLive)
                {
                    List<NotificationDate> notificationDates = new List<NotificationDate>();

                    if (trainingProgram.StartDate.HasValue)
                    {
                        notificationDates.Add(new NotificationDate()
                        {
                            StartDate = trainingProgram.StartDate.Value,
                            EndDate = trainingProgram.StartDate.Value,
                            Type = CalendarType.AllDay,
                            InstanceID = tp.Id.ToString(),
                            Name = trainingProgram.Name + " Start Date",
                            Method = CalendarMethod.Create
                        });
                    }

                    if (trainingProgram.DueDate.HasValue || trainingProgram.EndDate.HasValue)
                    {
                        notificationDates.Add(new NotificationDate()
                        {
                            StartDate = trainingProgram.DueDate.HasValue ? trainingProgram.DueDate.Value : trainingProgram.EndDate.Value,
                            EndDate = trainingProgram.DueDate.HasValue ? trainingProgram.DueDate.Value : trainingProgram.EndDate.Value,
                            Type = CalendarType.AllDay,
                            InstanceID = tp.Id.ToString(),
                            Name = trainingProgram.Name + " Due Date",
                            Method = CalendarMethod.Create
                        });
                    }

                    if (trainingProgram.EndDate.HasValue)
                    {
                        notificationDates.Add(new NotificationDate()
                        {
                            StartDate = trainingProgram.EndDate.Value,
                            EndDate = trainingProgram.EndDate.Value,
                            Type = CalendarType.AllDay,
                            InstanceID = tp.Id.ToString(),
                            Name = trainingProgram.Name + " End Date",
                            Method = CalendarMethod.Create
                        });

                    }

                    // Get list of existing users for exclusion in notifications 
                    var updatedIds = data.GetUnfilteredAssignedUsers().ToList();
                    newUserIds = updatedIds.Except(currentUserIds).ToList();
                    duplicateUserIds = currentUserIds.Intersect(updatedIds).ToList();

                    // no need to send if no one to send to...
                    if (newUserIds.Count() > 0)
                    {
                        NotificationController.SendNotification(NotificationType.TrainingProgramAssigned, new NotificationOptions()
                        {
                            Sync = false,
                            NotificationDates = notificationDates.ToArray(),
                            UserModifier = (users) =>
                            {
                                if (users.Count == 0)
                                {
                                    users = new List<int>() { -1 };
                                }
                                // filter to only include assigned new hires if the TP is a new hire,
                                // and vice versa
                                if (tp.IsNewHire)
                                {
                                    // include new hires
                                    List<Data.User> filtered = new List<Data.User>();
                                    foreach (int[] userIdGroup in users.InGroupsOf(2000))
                                    {
                                        filtered.AddRange(Select.AllColumnsFrom<Data.User>()
                                            .Where(Data.User.IdColumn).In(userIdGroup)
                                            .And(Data.User.IdColumn).In(newUserIds)
                                            .And(Data.User.NewHireIndicatorColumn).IsEqualTo(true)
                                            .ExecuteTypedList<Data.User>());
                                    }

                                    return filtered.Select(u => u.Id).ToList();
                                }
                                else
                                {
                                    // exclude new hires
                                    List<Data.User> filtered = new List<Data.User>();
                                    foreach (int[] userIdGroup in users.InGroupsOf(2000))
                                    {
                                        filtered.AddRange(Select.AllColumnsFrom<Data.User>()
                                            .Where(Data.User.IdColumn).In(userIdGroup)
                                            .And(Data.User.IdColumn).NotIn(duplicateUserIds)
                                            .And(Data.User.NewHireIndicatorColumn).IsEqualTo(false)
                                            .ExecuteTypedList<Data.User>());
                                    }

                                    return filtered.Select(u => u.Id).ToList();
                                }
                            },
                            TemplateObjects = new object[] { tp },
                            CustomerOverride = tp.Customer
                        });
                    }
                }
                
                // Try to create a message board
                if (trainingProgram.IsCreateMessageBoard)
                {
                    MessageBoardController messageBoardController = new MessageBoardController();
                    SingleResult<MessageBoard> messageBoard = messageBoardController.GetMessageBoard(CustomerID, UserID, data.Id, MessageBoardType.TrainingProgram);
                    if (messageBoard.Data.ID == 0)
                    {
                        MessageBoard mb = new MessageBoard();
                        mb.TrainingProgramId = data.Id;
                        mb.Name = String.IsNullOrEmpty(trainingProgram.MessageBoardName) ? trainingProgram.Name : trainingProgram.MessageBoardName;
                        mb.MessageBoardType = MessageBoardType.TrainingProgram;
                        mb.IsDisableTopicCreation = trainingProgram.IsDisableTopicCreation;

                        messageBoardController.UpdateMessageBoard(CustomerID, UserID, 0, mb);
                    }

                }


                if (trainingProgram.NotificationTemplates != null && trainingProgram.IsCustomNotificationsEnabled)
                {
                    Data.TrainingProgramTemplateCollection trainingProgramTemplates = new Data.TrainingProgramTemplateCollection();

                    foreach (NotificationTemplate template in trainingProgram.NotificationTemplates)
                    {
                        trainingProgramTemplates.Add(new Data.TrainingProgramTemplate { TrainingProgramID = data.Id, TemplateID = template.ID });
                    }

                    Data.TrainingProgramTemplate.Destroy(Data.TrainingProgramTemplate.Columns.TrainingProgramID, trainingProgramId);
                    trainingProgramTemplates.SaveAll(Username);
                }


                ts.Complete();
                
            }

            // Before we do any rollup processing, load the fresh training program details
            // otherwise the rollup could slow down loading this training program
            SingleResult<TrainingProgram> result = (new PortalController()).GetTrainingProgramDetails(userId, data.Id, true);

            if (isCheckForRollup)
            {
                // Rollup Rules:
                /* 
                 * Rollup will ONLY occur when training program isLive is true
                 *    Allows preventing any rollups from clogging up the queue while building the training program. 
                 *    Once the isLive is true, rollup will occur for all assigned users
                 * All assigned users need rollup when:
                 *    The training program is a new
                 *    OR Courses in the training program have changed - Either added or removed
                 *    OR the dates for the training program have changed
                 *    OR The isLive flag has changed from false to true (This allows rollups to be disabled while the isLive flag is off)
                 * IF User assignments have changed:
                 *    ONLY rollup the training program for the newly assigned users
                 *    We do not need to worry about any removed users since they will not see the training program anyway
                 */
                // Clear the current course links to force a full rel
                List<string> newCourseKeys = links.Select(l => l.CourseID + "-" + l.CourseTypeID).ToList();

                // in case we make it here with out hitting the notification block, set the newUserIds and duplicateUserIds.
                if (newUserIds == null)
                {
                    newUserIds = data.GetUnfilteredAssignedUsers();
                    duplicateUserIds = currentUserIds.Intersect(newUserIds).ToList();
                }

                RollupController rollup = new RollupController();

                isCourseChanged = newCourseKeys.Count < currentCourseKeys.Count || currentCourseKeys.Intersect(newCourseKeys).Count() != newCourseKeys.Count;
                isUserAssignmentChanged = duplicateUserIds.Count != newUserIds.Count;

                bool isFullRollupRequired = isCourseChanged || isDateChanged || isLiveChanged || isNew;

                if (HostingEnvironment.IsHosted)
                {
                    if (data.IsLive) // Only rollup when active and this is triggered from the web - console apps are excluded
                    {
                        if (isFullRollupRequired) // Rollup for everyone
                        {
                            rollup.EnqueueTrainingProgramRollup(data.Id, newUserIds.ToArray());
                        }
                        else if (isUserAssignmentChanged) // Rollup for only new users
                        {
                            List<int> onlyAddedUserIds = newUserIds.Except(duplicateUserIds).ToList();
                            rollup.EnqueueTrainingProgramRollup(data.Id, onlyAddedUserIds.ToArray());
                        }
                    }
                }
            }

            return result;
        }

        private Data.Category GetDefaultCategory(int customerId)
        {
            Data.Category category = Select.AllColumnsFrom<Data.Category>()
                    .Where(Data.Category.NameColumn).IsEqualTo("Default")
                    .And(Data.Category.CustomerIDColumn).IsEqualTo(customerId)
                    .ExecuteSingle<Data.Category>();

            return category;
        }

        public SingleResult<bool> UpdateTrainingProgramNewHire(int customerId, int userId, int trainingProgramId, bool isNewHire)
        {
            Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);
            if (tp.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the training program with id " + trainingProgramId);
            }
            tp.IsNewHire = isNewHire;
            tp.Save(Username);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> UpdateTrainingProgramLive(int customerId, int userId, int trainingProgramId, bool isLive)
        {
            Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);
            if (tp.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the training program with id " + trainingProgramId);
            }
            tp.IsLive = isLive;
            tp.Save(Username);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> UpdateCourseUserStartTime(int customerId, int userId, int onlineCourseId, int trainingProgramId)
        {
            Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);

            SqlQuery query = Select.AllColumnsFrom<Data.CourseStartTime>()
                .Where(Data.CourseStartTime.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                .And(Data.CourseStartTime.UserIDColumn).IsEqualTo(userId)
                .And(Data.CourseStartTime.OnlineCourseIDColumn).IsEqualTo(onlineCourseId);

            Data.CourseStartTime st = query.ExecuteSingle<Data.CourseStartTime>();

            if (st == null)
            {
                st = new Data.CourseStartTime()
                {
                    TrainingProgramID = trainingProgramId,
                    OnlineCourseID = onlineCourseId,
                    UserID = userId,
                    StartTime = DateTime.Now
                };
                st.Save(UserID);
            }

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteCourseExtension(int trainingProgramId, int courseId, int userId)
        {
            Data.OnlineCourseRollupStartTime rst = Select.AllColumnsFrom<Data.OnlineCourseRollupStartTime>()
                .Where(Data.OnlineCourseRollupStartTime.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                .And(Data.OnlineCourseRollupStartTime.Columns.UserID).IsEqualTo(userId)
                .And(Data.OnlineCourseRollupStartTime.Columns.CourseID).IsEqualTo(courseId)
                .ExecuteSingle<Data.OnlineCourseRollupStartTime>();

            if (rst == null || !rst.ExpiresAfterMinutes.HasValue || !rst.Remaining.HasValue)
            {
                throw new Exception("Cannot clear time remaining on this course as it is not applicable.");
            }

            double subtractMinutes = 0;

            if (rst.Remaining.Value >= 0)
            {
                subtractMinutes = -(rst.Remaining.Value + 1);
            }

            return SaveCourseExtension(trainingProgramId, courseId, userId, subtractMinutes);
        }

        public SingleResult<bool> SaveCourseExtension(int trainingProgramId, int courseId, int userId, double minutes)
        {
            Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);
            Data.User u = new Data.User(userId);
            Data.OnlineCourse o = new Data.OnlineCourse(courseId);

            TrainingProgram leadsTrainingProgram =
                    Select.AllColumnsFrom<Data.TrainingProgram>()
                    .InnerJoin(Data.TrainingProgramLeader.TrainingProgramIDColumn, Data.TrainingProgram.IdColumn)
                    .Where(Data.TrainingProgramLeader.UserIDColumn).IsEqualTo(UserID)
                    .And(Data.TrainingProgram.IdColumn).IsEqualTo(trainingProgramId)
                    .ExecuteSingle<TrainingProgram>();

            bool isSuperAdmin = HasRole(Roles.SuperHero);

            Data.CourseStartTime st = Select.AllColumnsFrom<Data.CourseStartTime>()
                .Where(Data.CourseStartTime.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                .And(Data.CourseStartTime.UserIDColumn).IsEqualTo(userId)
                .And(Data.CourseStartTime.OnlineCourseIDColumn).IsEqualTo(courseId)
                .ExecuteSingle<Data.CourseStartTime>();

            Data.OnlineCourseRollupStartTime rst = Select.AllColumnsFrom<Data.OnlineCourseRollupStartTime>()
                .Where(Data.OnlineCourseRollupStartTime.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                .And(Data.OnlineCourseRollupStartTime.Columns.UserID).IsEqualTo(userId)
                .And(Data.OnlineCourseRollupStartTime.Columns.CourseID).IsEqualTo(courseId)
                .ExecuteSingle<Data.OnlineCourseRollupStartTime>();

            if (!HasRole(Roles.ClassroomInstructor) &&
                !HasRole(Roles.ClassroomManager) &&
                !HasRole(Roles.CourseAssignmentAdministrator) &&
                !isSuperAdmin)
            {
                throw new Exception("You do not have permission to create extensions.");
            }

            if (leadsTrainingProgram == null && !isSuperAdmin)
            {
                throw new Exception("You need to be the leader of the training program in order to allow an extension.");
            }

            if (tp.CustomerID != CustomerID)
            {
                throw new Exception("You do not have permission create extensions for this training program.");
            }

            if (u.CustomerID != CustomerID)
            {
                throw new Exception("You do not have permission to create extensions for this user.");
            }

            if (o.CustomerID != CustomerID)
            {
                throw new Exception("You do not have permission to create extensions for this course.");
            }

            if (st == null)
            {
                throw new Exception("The user specified has not yet started the course specified. Extension not allowed.");
            }

            if (!rst.ExpiresAfterMinutes.HasValue || !rst.Remaining.HasValue)
            {
                throw new Exception("Cannot extend time on this acourse as it is not applicable.");
            }

            double extendTo;

            if (rst.Remaining <= 0)
            {
                extendTo = minutes;
            }
            else
            {
                extendTo = minutes + rst.Remaining.Value;
            }

            double timeOffset = extendTo - rst.ExpiresAfterMinutes.Value;
            DateTime newStartTime = DateTime.Now.AddMinutes(timeOffset);

            st.StartTime = newStartTime;

            st.Save(UserID);

            return new SingleResult<bool>(true);
        }


        public SingleResult<bool> DeleteTrainingProgram(int customerId, int userId, int trainingProgramId)
        {
            Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);
            if (tp.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the training program with id " + trainingProgramId);
            }
            Data.TrainingProgram.Destroy(trainingProgramId);
            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> UnassignTrainingProgram(int customerId, int userId, int trainingProgramId, int tpUserId, bool isBundle)
        {
            Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);
            if (tp.CustomerID != customerId && !UserIsSalesChannelAdmin)
            {
                throw new Exception("You don't have permission to the training program with id " + trainingProgramId);
            }

            if (!tp.IsSalesforce)
            {
                throw new Exception("You can only unassign Salesforce training programs.");
            }

            List<int> trainingProgramIds = new List<int>();

            if (isBundle && tp.PurchasedFromBundleID > 0)
            {
                trainingProgramIds = new Select(Data.SalesforceEntitlement.TrainingProgramIdColumn)
                    .From<Data.SalesforceEntitlement>()
                    .Where(Data.SalesforceEntitlement.BundleIDColumn).IsEqualTo(tp.PurchasedFromBundleID)
                    .And(Data.SalesforceEntitlement.UserIdColumn).IsEqualTo(tpUserId)
                    .ExecuteTypedList<int>();
            }
            else
            {
                trainingProgramIds.Add(trainingProgramId);
            }

            foreach (int tpid in trainingProgramIds)
            {
                Data.TrainingProgram.Destroy(tpid);
                Data.TrainingProgramToCourseLink.Delete(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn.ColumnName, tpid);
                Data.HierarchyToTrainingProgramLink.Delete(Data.HierarchyToTrainingProgramLink.TrainingProgramIDColumn.ColumnName, tpid);
                Data.SalesforceEntitlement.Delete(Data.SalesforceEntitlement.TrainingProgramIdColumn.ColumnName, tpid);
            }

            return new SingleResult<bool>(true);
        }

        public PagedResult<Course> GetAvailableTrainingProgramCourses(int customerId, List<CourseRecord> exclude, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.AvailableTrainingProgramCourse>()
                .Where(Data.AvailableTrainingProgramCourse.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.AvailableTrainingProgramCourse.Columns.Key).NotIn(exclude.Select(cr =>
                (
                    cr.Id + "-" + cr.CourseTypeID
                )))
                .AndExpression(Data.AvailableTrainingProgramCourse.Columns.Name).ContainsString(searchText)
                .Or(Data.AvailableTrainingProgramCourse.Columns.LevelIndentText).ContainsString(searchText)
                .Or(Data.AvailableTrainingProgramCourse.Columns.Keywords).ContainsString(searchText)
                .CloseExpression()
                .And(Data.AvailableTrainingProgramCourse.Columns.IsThirdParty).IsEqualTo(false)
                .AndExpression(Data.AvailableTrainingProgramCourse.Columns.PublicIndicator).IsEqualTo(false)
                .And(Data.AvailableTrainingProgramCourse.Columns.CourseTypeID).IsEqualTo((int)CourseType.Classroom)
                .Or(Data.AvailableTrainingProgramCourse.Columns.CourseTypeID).IsEqualTo((int)CourseType.Online);

            if (string.IsNullOrEmpty(orderBy))
            {
                orderBy = Data.AvailableTrainingProgramCourse.Columns.Name;
            }

            return new PagedResult<Course>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Course> GetAssignedTrainingProgramCourses(int customerId, int trainingProgramId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.AssignedTrainingProgramCourse>()
                .Where(Data.AssignedTrainingProgramCourse.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.AssignedTrainingProgramCourse.Columns.TrainingProgramID).IsEqualTo(trainingProgramId);

            if (string.IsNullOrEmpty(orderBy))
            {
                orderBy = Data.AssignedTrainingProgramCourse.Columns.RequiredSyllabusOrder;
            }

            return new PagedResult<Course>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<TrainingProgramFile> GetTrainingProgramFiles(int customerId, int trainingProgramId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.TrainingProgramFile>()
                .Where(Data.TrainingProgramFile.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                .AndExpression(Data.TrainingProgramFile.Columns.FileName).ContainsString(searchText)
                .Or(Data.TrainingProgramFile.Columns.Title).ContainsString(searchText)
                .Or(Data.TrainingProgramFile.Columns.Description).ContainsString(searchText);

            return new PagedResult<TrainingProgramFile>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public TrainingProgramFile UploadTrainingProgramFile(int trainingProgramId, string filename, string title, string description, HttpPostedFile file)
        {
            Data.TrainingProgramFile tpFile = new Symphony.Core.Data.TrainingProgramFile();
            tpFile.FileName = filename;
            tpFile.Title = title ?? filename;
            tpFile.Description = description ?? filename;
            tpFile.FileBytes = new byte[] { };
            tpFile.TrainingProgramID = trainingProgramId;
            tpFile.Save(Username);

            string path = HttpContext.Current.Server.MapPath(TrainingProgramFilesLocation + tpFile.Id);
            string directory = Path.GetDirectoryName(path);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            file.SaveAs(path);

            return Model.Create<Models.TrainingProgramFile>(tpFile);
        }

        public SingleResult<bool> DeleteTrainingProgramFile(int customerId, int userId, int trainingProgramFileId)
        {
            Data.TrainingProgramFile tp = new Data.TrainingProgramFile(trainingProgramFileId);
            if (new Data.TrainingProgram(tp.TrainingProgramID).CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the training program file with id " + trainingProgramFileId);
            }
            Data.TrainingProgramFile.Destroy(trainingProgramFileId);
            return new SingleResult<bool>(true);
        }

        /// <summary>
        /// Gets the details for a specific list of users; used when viewing only the assigned users in a training program
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userIds"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedResult<User> GetSpecificUsers(int customerId, int[] userIds, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery q = Select.AllColumnsFrom<Data.User>()
                .Where(Data.User.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.User.Columns.StatusID).IsNotEqualTo(UserStatusType.Deleted);

            if (userIds.Length == 0)
            {
                q.And(Data.User.Columns.Id).IsEqualTo(-1);
            }
            else
            {
                q.And(Data.User.Columns.Id).In(userIds);
            }


            return new PagedResult<User>(q, pageIndex, pageSize, orderBy, orderDir);
        }

        public MultipleResult<Location> GetLocations(int customerId, int userId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LocationHierarchy>()
                .Where(Data.LocationHierarchy.Columns.CustomerID).IsEqualTo(customerId)
                .OrderAsc(Data.LocationHierarchy.Columns.LevelIndentText);

            return new MultipleResult<Location>(query);
        }

        public MultipleResult<JobRole> GetJobRoles(int customerId, int userId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.JobRoleHierarchy>()
                .Where(Data.JobRoleHierarchy.Columns.CustomerID).IsEqualTo(customerId)
                .OrderAsc(Data.JobRoleHierarchy.Columns.LevelIndentText);

            return new MultipleResult<JobRole>(query);
        }

        public MultipleResult<Audience> GetAudiences(int customerId, int userId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.AudienceHierarchy>()
                .Where(Data.AudienceHierarchy.Columns.CustomerID).IsEqualTo(customerId)
                .OrderAsc(Data.AudienceHierarchy.Columns.LevelIndentText);

            return new MultipleResult<Audience>(query);
        }

        public PagedResult<OnlineCourseRollup> GetOnlineCourseRollups(int trainingProgramId, int onlineCourseId, bool lockedOnly, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LockedUserCourse>()
                .Where(Data.LockedUserCourse.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                .And(Data.LockedUserCourse.Columns.CourseID).IsEqualTo(onlineCourseId)
                .And(Data.LockedUserCourse.Columns.IsLockedOut).IsEqualTo(true);
            

            return new PagedResult<OnlineCourseRollup>(query, pageIndex, pageSize, orderBy, orderDir);

            //if (!string.IsNullOrEmpty(searchText))
            //{
            //    query.AndExpression(Data.OnlineCourseRollup.Columns.UserID).ContainsString(searchText)
            //        .CloseExpression();
            //}

            // Retrieve all the usernames for display
            /*var results = new PagedResult<OnlineCourseRollup>(query, pageIndex, pageSize, orderBy, orderDir);
            List<Data.User> user = Select.AllColumnsFrom<Data.User>().Where(Data.User.
            foreach (var result in results.Data)
            {
                Data.User user = new Data.User(result.UserID);
                result.Username = user.Username;
            }

            return results;*/
        }

        public SingleResult<OnlineCourseRollup> GetOnlineCourseRollup(int onlineCourseRollupId)
        {
            OnlineCourseRollup rollup = Select.AllColumnsFrom<Data.OnlineCourseRollup>()
                .Where(Data.OnlineCourseRollup.IdColumn).IsEqualTo(onlineCourseRollupId)
                .ExecuteSingle<OnlineCourseRollup>();

            if (!UserIsSalesChannelAdmin) {
                if (!HasRole(Roles.CourseAssignmentAdministrator))
                {
                    throw new Exception("You do not have permission to view student course data.");
                }
                Data.User user = new Data.User(rollup.UserID);
                if (user.CustomerID != CustomerID)
                {
                    throw new Exception("You do not have permission to view student data outside of the current customer.");
                }
            }

            return new SingleResult<OnlineCourseRollup>(rollup);
        }

        public PagedResult<OnlineCourseRegistration> GetOnlineCourseRegistrations(int trainingProgramId, int onlineCourseId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            List<int> trainingProgramIds = new List<int>() {
                trainingProgramId
            };
            Data.TrainingProgram trainingProgram = new Data.TrainingProgram(trainingProgramId);

            List<string> columns = Data.OnlineCourseRollupStartTime.Schema.Columns.Select(c => c.QualifiedName).ToList();

            SqlQuery query = new Select(columns.Concat(new List<string>() {
                Data.User.UsernameColumn.QualifiedName,
                Data.User.FirstNameColumn.QualifiedName,
                Data.User.LastNameColumn.QualifiedName,
                Data.User.MiddleNameColumn.QualifiedName
            }).ToArray())
                    .From<Data.OnlineCourseRollupStartTime>()
                    .InnerJoin(Data.User.Schema.TableName, Data.User.IdColumn.ColumnName, Data.OnlineCourseRollupStartTime.Schema.Name, Data.OnlineCourseRollupStartTime.Columns.UserID)
                    .Where(Data.OnlineCourseRollupStartTime.Columns.CourseID).IsEqualTo(onlineCourseId);

            if (trainingProgram.ParentTrainingProgramID == 0 && trainingProgram.Id > 0)
            {
                // Force loading of child training programs if for some reason we end up with the 
                // parent training program in the list. 
                trainingProgram.ParentTrainingProgramID = trainingProgram.Id;
            }

            if (trainingProgram.ParentTrainingProgramID > 0)
            {
                trainingProgramIds = new Select(Data.TrainingProgram.IdColumn)
                    .From<Data.TrainingProgram>()
                    .Where(Data.TrainingProgram.ParentTrainingProgramIDColumn).IsEqualTo(trainingProgram.ParentTrainingProgramID)
                    .Or(Data.TrainingProgram.IdColumn).IsEqualTo(trainingProgram.ParentTrainingProgramID)
                    .ExecuteTypedList<int>();

                if (trainingProgram.SessionCourseID > 0)
                {
                    query.InnerJoin(Data.Registration.RegistrantIDColumn, Data.User.IdColumn)
                        .InnerJoin(Data.ClassX.IdColumn, Data.Registration.ClassIDColumn)
                        .InnerJoin(Data.ClassMinStartDateTime.Schema.TableName, Data.ClassMinStartDateTime.Columns.ClassID,
                                       Data.ClassX.Schema.TableName, Data.ClassX.Columns.Id)
                        .InnerJoin(Data.TrainingProgram.SessionCourseIDColumn, Data.ClassX.CourseIDColumn)
                        .And(Data.TrainingProgram.IdColumn).In(trainingProgramIds)
                        .And(Data.TrainingProgram.ParentTrainingProgramIDColumn).IsEqualTo(0)
                        .And(Data.Registration.RegistrationStatusIDColumn).IsEqualTo(RegistrationStatusType.Registered)
                        .And(Data.ClassMinStartDateTime.Schema.GetColumn(Data.ClassMinStartDateTime.Columns.MinStartDateTime).QualifiedName)
                            .IsLessThan(DateTime.UtcNow.AddDays(1));

                    query.IncludeColumn(Data.ClassX.IdColumn.QualifiedName, "ClassID");
                    query.IncludeColumn(Data.ClassX.NameColumn.QualifiedName, "ClassName");
                    query.IncludeColumn(Data.ClassMinStartDateTime.Columns.MinStartDateTime, "ClassStartTime");
                    query.OrderDesc(Data.ClassMinStartDateTime.Schema.GetColumn(Data.ClassMinStartDateTime.Columns.MinStartDateTime).QualifiedName)
                        .OrderDesc(Data.ClassMinStartDateTime.Schema.GetColumn(Data.ClassMinStartDateTime.Columns.ClassID).QualifiedName);


                }
            }

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.AndExpression(Data.User.FirstNameColumn.QualifiedName).ContainsString(searchText)
                    .Or(Data.User.LastNameColumn.QualifiedName).ContainsString(searchText)
                    .Or(Data.User.MiddleNameColumn.QualifiedName).ContainsString(searchText);

                if (trainingProgram.SessionCourseID > 0)
                {
                    query.Or(Data.ClassX.NameColumn.QualifiedName).ContainsString(searchText);
                }

                query.CloseExpression();

            }

            query.And(Data.OnlineCourseRollupStartTime.Columns.TrainingProgramID).In(trainingProgramIds);

            if (orderBy != null)
            {
                if (orderBy == "fullName")
                {
                    if (orderDir == OrderDirection.Ascending)
                    {
                        query.OrderAsc(Data.User.LastNameColumn.QualifiedName);
                        query.OrderAsc(Data.User.FirstNameColumn.QualifiedName);
                    }
                    else
                    {
                        query.OrderDesc(Data.User.LastNameColumn.QualifiedName);
                        query.OrderDesc(Data.User.LastNameColumn.QualifiedName);
                    }
                }
                else if (orderBy == "canMoveForwardIndicator")
                {
                    if (orderDir == OrderDirection.Ascending)
                    {
                        query.OrderAsc(Data.OnlineCourseRollupStartTime.Schema.GetColumn(Data.OnlineCourseRollupStartTime.Columns.CanMoveForwardIndicator).QualifiedName);
                    }
                    else
                    {
                        query.OrderDesc(Data.OnlineCourseRollupStartTime.Schema.GetColumn(Data.OnlineCourseRollupStartTime.Columns.CanMoveForwardIndicator).QualifiedName);
                    }
                }
                else if (orderBy == "score")
                {
                    if (orderDir == OrderDirection.Ascending)
                    {
                        query.OrderAsc(Data.OnlineCourseRollupStartTime.Schema.GetColumn(Data.OnlineCourseRollupStartTime.Columns.Score).QualifiedName);
                    }
                    else
                    {
                        query.OrderDesc(Data.OnlineCourseRollupStartTime.Schema.GetColumn(Data.OnlineCourseRollupStartTime.Columns.Score).QualifiedName);
                    }
                }
                else if (orderBy == "classId")
                {
                    if (orderDir == OrderDirection.Ascending)
                    {
                        query.OrderAsc(Data.ClassX.IdColumn.QualifiedName);
                    }
                    else
                    {
                        query.OrderDesc(Data.ClassX.IdColumn.QualifiedName);
                    }
                }
                else
                {
                    if (orderBy == "createdOn")
                    {
                        orderBy = Data.OnlineCourseRollupStartTime.Schema.GetColumn(Data.OnlineCourseRollupStartTime.Columns.HighScoreDate).QualifiedName;
                    }

                    if (orderDir == OrderDirection.Ascending)
                    {
                        query.OrderAsc(orderBy);
                    }
                    else
                    {
                        query.OrderDesc(orderBy);
                    }
                }
            }

            query.WithNoLock();

            return new PagedResult<OnlineCourseRegistration>(query, pageIndex, pageSize, orderBy, orderDir).WithUtcFlag();
        }

        public SingleResult<OnlineCourseRegistration> GetOnlineCourseRegistration(int onlineCourseRollupId)
        {
            OnlineCourseRegistration registration = Select.AllColumnsFrom<Data.OnlineCourseRollupStartTime>()
                .Where(Data.OnlineCourseRollupStartTime.Columns.Id).IsEqualTo(onlineCourseRollupId)
                .ExecuteSingle<OnlineCourseRegistration>();

            return new SingleResult<OnlineCourseRegistration>(registration);
        }

        public SingleResult<bool> SaveOnlineCourseRegistrations(int trainingProgramId, int onlineCourseId, OnlineCourseRegistration[] registrations)
        {
            Data.OnlineCourseRollupCollection rollups = new Data.OnlineCourseRollupCollection();

            List<OnlineCourseRegistration> canMoveForwardRegistrations = new List<OnlineCourseRegistration>();

            // When saving training program rollups, we need the course id/user pair in order to 
            // ensure that other training programs with the course will also be rolled up.
            Dictionary<int, List<int>> coursesForRollup = new Dictionary<int, List<int>>();

            Data.TrainingProgram trainingProgram = new Data.TrainingProgram(trainingProgramId);
            
            foreach (OnlineCourseRegistration reg in registrations)
            {
                if (trainingProgramId > 0 && trainingProgram.IsNewHire)
                {
                    CourseController.CalculateTrainingProgramNewHireDates(reg.UserID, trainingProgram);
                }

                Data.OnlineCourseRollup roll = new Data.OnlineCourseRollup(reg.ID);

                if (!roll.CanMoveForwardIndicator && reg.CanMoveForwardIndicator)
                {
                    canMoveForwardRegistrations.Add(reg);
                }

                // Only want to allow specific fields to be changed here.
                // so only copy over all if it is a new rollup.
                if (roll.Id == 0)
                {
                    reg.CopyTo(roll);
                }
                
                //  Alllowing these fields to be changed
                roll.CanMoveForwardIndicator = reg.CanMoveForwardIndicator;

                // If it's a new rollup (roll.Id = 0) then currentSuccessState will be false - otherwise we miss the rollup since we've already 
                // copied reg to roll, so reg.Success and roll.Success will be the same. 
                // This should catch the case where the success flag is updated for an online course rollup that doesn't exist yet.
                bool currentSuccessState = roll.Id > 0 && roll.Success.HasValue && roll.Success.Value;
                if (!currentSuccessState && reg.Success)
                {
                    if (coursesForRollup.ContainsKey(reg.UserID))
                    {
                        if (!coursesForRollup[reg.UserID].Contains(reg.CourseID))
                        {
                            coursesForRollup[reg.UserID].Add(reg.CourseID);
                        }
                    }
                    else
                    {
                        // If any course has been changed to success, rollup the training program data for this user
                        coursesForRollup.Add(reg.UserID, new List<int> { reg.CourseID });
                    }
                }

                roll.Success = reg.Success;

                roll.Completion = reg.Completion;
                if (
                    (!roll.Score.HasValue && reg.Score > 0) || 
                    (roll.Score.HasValue && reg.Score > roll.Score.Value) || 
                    (roll.Id == 0 && roll.Score.HasValue && roll.Score > 0) 
                )
                {
                    if (roll.HighScoreDate.HasValue && !string.IsNullOrWhiteSpace(roll.ResetNotes))
                    {
                        roll.ResetNotes += " (Last High Score Date: " + roll.HighScoreDate.Value.ToString() + ") ";
                    }

                    roll.HighScoreDate = DateTime.UtcNow;
                    
                    if (trainingProgramId > 0 && roll.HighScoreDate > trainingProgram.EndDate)
                    {
                        // The minimum difference between a start and end date of a training program is 1 day. 
                        // So we should be able to subtract 23 hours safely from the training program end date
                        // without pushing the highscore date beyond the start date
                        //
                        // Allowing this here since this is end point is only used for instructor / admin purposes
                        // to modify or create course rollups for students. 
                        //
                        // This does not mean that when a user takes a course their highscore date will be restricted
                        // within the training program time boundries. 
                        roll.HighScoreDate = trainingProgram.EndDate.AddHours(-23);
                    }
                }
                roll.Score = reg.Score;

                roll.TotalSeconds = reg.TotalSeconds ?? roll.TotalSeconds;
                roll.TotalSecondsLastAttempt = reg.TotalSecondsLastAttempt ?? roll.TotalSecondsLastAttempt;
                roll.TotalSecondsRounded = reg.TotalSecondsRounded ?? roll.TotalSecondsRounded;

                roll.ResetNotes += (string.IsNullOrWhiteSpace(roll.ResetNotes) ? reg.ResetNotes : (";\r\n" + reg.ResetNotes));

                if (!roll.Completion.HasValue || !roll.Completion.Value)
                {
                    roll.FirstCompletionDate = null;
                }


                rollups.Add(roll);
            }

            rollups.SaveAll(UserID);

            SendNotifications(canMoveForwardRegistrations);

            RollupController rollupController = new RollupController();
            foreach (KeyValuePair<int, List<int>> kv in coursesForRollup)
            {
                int userId = kv.Key;
                foreach (int courseId in kv.Value)
                {
                    rollupController.EnqueueTrainingProgramRollup(trainingProgramId, userId, courseId, CourseType.Online);
                }
            }

            return new SingleResult<bool>(true);
        }

        private void SendNotifications(List<OnlineCourseRegistration> registrations)
        {
            foreach (OnlineCourseRegistration reg in registrations)
            {
                Data.User user = new Data.User(reg.UserID);
                Data.OnlineCourse oc = new Data.OnlineCourse(reg.CourseID);

                Course course = new Course();
                course.CopyFrom(oc);

                NotificationController.SendNotification(NotificationType.InstructorPermissionGranted, new NotificationOptions()
                {
                    TemplateObjects = new object[] { user, course }
                });
            }
        }

        private SingleResult<OnlineCourseRegistration> GetRollup(int onlineCourseRollupId)
        {
            Models.OnlineCourseRegistration rollup = Select.AllColumnsFrom<Data.OnlineCourseRollup>()
                .Where(Data.Template.Columns.Id).IsEqualTo(onlineCourseRollupId)
                .ExecuteSingle<OnlineCourseRegistration>();

            return new SingleResult<OnlineCourseRegistration>(rollup);
        }

        public SingleResult<OnlineCourseRegistration> ResetAttemptCount(int onlineCourseRollupId, string notes)
        {
            Data.OnlineCourseRollup data = new Symphony.Core.Data.OnlineCourseRollup(onlineCourseRollupId);
            if (data.Id == 0)
            {
                throw new Exception("The specified rollup ID is invalid.");
            }
            //TODO: lock down to the copmany!

            data.TestAttemptCount = 0;
            if (!string.IsNullOrEmpty(data.ResetNotes))
            {
                data.ResetNotes += "; ";
            }
            data.LastResetDate = DateTime.UtcNow;
            data.ResetNotes += notes;
            data.Save(Username);

            return GetRollup(onlineCourseRollupId);
        }

        public SingleResult<OnlineCourseRegistration> DeleteRegistration(int onlineCourseRollupId, string notes, bool cleanUpAssignments)
        {
            Data.OnlineCourseRollup data = new Symphony.Core.Data.OnlineCourseRollup(onlineCourseRollupId);
            if (data.Id == 0)
            {
                throw new Exception("The specified rollup ID is invalid.");
            }

            Symphony.Core.Data.SPs.CourseReset(onlineCourseRollupId, cleanUpAssignments).Execute();

            // save notes
            if (!string.IsNullOrEmpty(data.ResetNotes))
            {
                data.ResetNotes += "; ";
            }
            data.ResetNotes += notes;
            data.Save(Username);

            // Update training program rollup
            if (data.TrainingProgramID > 0)
            {
                new RollupController().EnqueueTrainingProgramRollup(data.TrainingProgramID, data.UserID, data.CourseID, CourseType.Online);
            }

            return GetRollup(onlineCourseRollupId);
        }

        private bool IsUserValidationAllowed(int courseId, int trainingProgramId)
        {
            bool? validationEnabled = trainingProgramId > 0 ?
                new Data.TrainingProgram(trainingProgramId).IsValidationEnabled :
                new Data.OnlineCourse(courseId).IsValidationEnabled;

            if (!validationEnabled.HasValue || !validationEnabled.Value)
            {
                return true;
            }

            return HasValidationParameters;
        }

        private int GetAffidavitID(int courseId, int trainingProgramId)
        {
            int affidavitId = new Select(Data.TrainingProgram.AffidavitIDColumn)
                .From<Data.TrainingProgramToCourseLink>()
                .InnerJoin(Data.TrainingProgram.IdColumn, Data.TrainingProgramToCourseLink.TrainingProgramIDColumn)
                .Where(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                .And(Data.TrainingProgramToCourseLink.CourseIDColumn).IsEqualTo(courseId)
                .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo((int)CourseType.Online)
                .And(Data.TrainingProgramToCourseLink.SyllabusTypeIDColumn).IsEqualTo((int)SyllabusType.Final)
                .And(Data.TrainingProgram.AffidavitIDColumn).IsGreaterThan(0)
                .ExecuteScalar<int>();

            if (affidavitId > 0)
            {
                return affidavitId;
            }

            return 0;
        }

        /// <summary>
        /// Returns the amount of time the user has spent taking 
        /// course work for today
        /// </summary>
        /// <param name="userId">user id to lookup</param>
        /// <returns></returns>
        public long GetTodaysDurationInCourse(int userId)
        {
            return new Select()
                .From<Data.OnlineCourseTimeTracking>()
                .Sum(Data.OnlineCourseTimeTracking.DurationColumn, "Duration")
                .GroupBy(Data.OnlineCourseTimeTracking.UserIDColumn)
                .GroupBy(Data.OnlineCourseTimeTracking.DateXColumn)
                .Where(Data.OnlineCourseTimeTracking.DateXColumn).IsEqualTo(DateTime.Today)
                .And(Data.OnlineCourseTimeTracking.UserIDColumn).IsEqualTo(userId)
                .ExecuteScalar<long>();
        }

        #endregion

        #region Sessions

        public MultipleResult<TrainingProgram> GetTrainingProgramsForUsers(List<int> userIds)
        {
            List<TrainingProgram> tps = Data.SPs.GetTrainingProgramsForUsers(string.Join(",", userIds)).ExecuteTypedList<TrainingProgram>();

            return new MultipleResult<TrainingProgram>(tps);
        }

        public PagedResult<SessionAssignedUser> GetSessionUsers(
            List<int> trainingProgramIds, int? sessionCourseId, string dateRange, bool? showActive, bool? showInRangeOnly, int? registrationStatusId, List<int> userIds,
            string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (trainingProgramIds.Count == 0)
            {
                throw new Exception("Please supply at least one training program id.");
            }

            DateTime? minStartDate = null;
            DateTime? maxStartDate = null;
            DateTime? minEndDate = null;
            DateTime? maxEndDate = null;

            if (dateRange.Contains("past"))
            {
                minStartDate = SqlDateTime.MinValue.Value;
                maxStartDate = DateTime.UtcNow.AddSeconds(-1);

                minEndDate = minStartDate;
                maxEndDate = maxStartDate;
            }
            else if (dateRange.Contains("active"))
            {
                minStartDate = SqlDateTime.MinValue.Value;
                maxStartDate = DateTime.UtcNow.AddSeconds(-1);

                minEndDate = DateTime.UtcNow;
                maxEndDate = SqlDateTime.MaxValue.Value;
            }
            else if (dateRange.Contains("future"))
            {
                minStartDate = DateTime.UtcNow.AddSeconds(1);
                maxStartDate = SqlDateTime.MaxValue.Value;

                minEndDate = minStartDate;
                maxEndDate = maxStartDate;
            }
            else if (!dateRange.Contains("none"))
            {
                if (!dateRange.Contains(","))
                {
                    dateRange = dateRange + "," + dateRange;
                }

                LocalDateRange range = DateRangeManager.Parse(dateRange);

                var start = new DateTime(range.StartDate.Year, range.StartDate.Month, range.StartDate.Day, 0, 0, 0);
                var end = new DateTime(range.EndDate.Year, range.EndDate.Month, range.EndDate.Day, 23, 59, 59);

                minStartDate = SqlDateTime.MinValue.Value;
                maxStartDate = end;

                minEndDate = start;
                maxEndDate = SqlDateTime.MaxValue.Value;
            }

            // Get child training program ids so we can view users assigned to the paretns as well. 
            List<int> childTrainingPrograms = new Select(Data.TrainingProgram.IdColumn).From<Data.TrainingProgram>().Where(Data.TrainingProgram.ParentTrainingProgramIDColumn).In(trainingProgramIds).ExecuteTypedList<int>();

            trainingProgramIds = trainingProgramIds.Concat(childTrainingPrograms).Distinct().ToList();

            List<SessionAssignedUser> users = Data.SPs.GetSessionAssignedUsers(
                string.Join(",", trainingProgramIds),
                sessionCourseId,
                searchText, 
                orderBy, 
                orderDir == OrderDirection.Descending ? "desc" : "asc",
                pageIndex,
                pageSize,
                minStartDate,
                maxStartDate,
                minEndDate,
                maxEndDate,
                showActive,
                showInRangeOnly,
                registrationStatusId,
                userIds.Count > 0 ? string.Join(",", userIds) : null
            ).ExecuteTypedList<SessionAssignedUser>().WithUtcOffset();
            
            SessionAssignedUser first = users.FirstOrDefault();
            int total = first != null ? first.TotalRows : 0;

            return new PagedResult<SessionAssignedUser>(users, pageIndex, pageSize, total);
        }

        #endregion

        #region Public Documents

        #region Public Document Categories

        public PagedResult<PublicDocumentCategory> FindPublicDocumentCategories(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.PublicDocumentCategory>()
                .Where(Data.PublicDocumentCategory.Columns.CustomerID).IsEqualTo(customerId)
                .AndExpression(Data.PublicDocumentCategory.Columns.Name).ContainsString(searchText)
                .Or(Data.PublicDocumentCategory.Columns.Description).ContainsString(searchText);

            return new PagedResult<PublicDocumentCategory>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public MultipleResult<PublicDocumentCategory> SavePublicDocumentCategories(int customerId, int userId, List<PublicDocumentCategory> categories)
        {
            foreach (PublicDocumentCategory category in categories)
            {
                Data.PublicDocumentCategory data = new Symphony.Core.Data.PublicDocumentCategory(category.Id);
                data.Description = category.Description;
                data.Name = category.Name;
                data.CustomerID = customerId;
                data.Save();

                category.Id = data.Id;
            }

            return new MultipleResult<PublicDocumentCategory>(categories);
        }

        #endregion

        public PagedResult<PublicDocument> FindPublicDocuments(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = new Select(
                    Data.PublicDocument.IdColumn.QualifiedName,
                    Data.PublicDocument.NameColumn.QualifiedName,
                    Data.PublicDocument.DescriptionColumn.QualifiedName,
                    Data.PublicDocument.Columns.CategoryID,
                    Data.PublicDocument.UploadedColumn.QualifiedName,
                    Data.PublicDocumentCategory.NameColumn.QualifiedName + " as CategoryName")
                .From<Data.PublicDocument>()
                .InnerJoin(Data.PublicDocumentCategory.IdColumn, Data.PublicDocument.CategoryIDColumn)
                .Where(Data.PublicDocument.Columns.CustomerID).IsEqualTo(customerId)
                .AndExpression(Data.PublicDocument.NameColumn.QualifiedName).ContainsString(searchText)
                .Or(Data.PublicDocument.DescriptionColumn.QualifiedName).ContainsString(searchText);

            orderBy = orderBy ?? string.Empty;

            switch (orderBy.ToLowerInvariant())
            {
                case "":
                case "categoryname":
                    orderBy = Data.PublicDocumentCategory.NameColumn.QualifiedName;
                    break;
                case "name":
                    orderBy = Data.PublicDocument.NameColumn.QualifiedName;
                    break;
                case "description":
                    orderBy = Data.PublicDocument.DescriptionColumn.QualifiedName;
                    break;
            }
            PagedResult<PublicDocument> result = new PagedResult<PublicDocument>(query, pageIndex, pageSize, orderBy, orderDir);

            foreach (PublicDocument doc in result.Data)
            {
                doc.Uploaded = doc.Uploaded.WithUtcFlag();
            }

            return result;
        }

        public SingleResult<PublicDocument> GetPublicDocument(int customerId, int userId, int publicDocumentId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.PublicDocument>()
                .Where(Data.PublicDocument.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.PublicDocument.Columns.Id).IsEqualTo(publicDocumentId);

            SingleResult<PublicDocument> result = new SingleResult<PublicDocument>(query);

            result.Data.CategoryName = new Data.PublicDocumentCategory(result.Data.CategoryId).Name;

            return result;
        }

        /// <summary>
        /// Imports the specified file for the given customer. If the documentId is non-zero, the existing document with that id will be overwritten.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="documentId"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public PublicDocument ImportPublicDocument(int customerId, int userId, int documentId, HttpPostedFile file)
        {
            Data.PublicDocument publicDocument = new Symphony.Core.Data.PublicDocument(documentId);
            if (publicDocument.Id == 0)
            {
                // initial upload, default everything to the filename
                publicDocument.Filename = file.FileName;
                publicDocument.Name = file.FileName;
                publicDocument.Description = file.FileName;
            }
            publicDocument.Uploaded = DateTime.UtcNow;
            publicDocument.CustomerID = customerId;
            publicDocument.Save(Username);

            string path = HttpContext.Current.Server.MapPath(PublicDocumentFilesLocation + publicDocument.Id);
            string directory = Path.GetDirectoryName(path);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            file.SaveAs(path);

            return Model.Create<Models.PublicDocument>(publicDocument);
        }

        public Stream DownloadPublicDocument(int customerId, int userId, int fileId)
        {
            Data.PublicDocument file = new Data.PublicDocument(fileId);
            if (file.CustomerID != customerId)
            {
                return null;
            }
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Filename.Replace(" ", "_").Replace(";", "_"));
            string path = HttpContext.Current.Server.MapPath(PublicDocumentFilesLocation + fileId);
            if (System.IO.File.Exists(path))
            {
                // grab it from the file system
                return new MemoryStream(System.IO.File.ReadAllBytes(path));
            }
            return null;
        }

        public SingleResult<bool> DeletePublicDocument(int customerId, int userId, int documentId)
        {
            Data.PublicDocument doc = new Symphony.Core.Data.PublicDocument(documentId);
            if (doc.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to delete this document.");
            }
            Data.PublicDocument.Delete(documentId);

            return new SingleResult<bool>(true);
        }

        public SingleResult<PublicDocument> SavePublicDocument(int customerId, int userId, int documentId, PublicDocument document)
        {
            Data.PublicDocument data = new Symphony.Core.Data.PublicDocument(documentId);

            data.Id = documentId;
            data.CustomerID = customerId;
            data.CategoryID = document.CategoryId;
            data.Name = document.Name;
            data.Description = document.Description;

            data.Save(Username);

            return new SingleResult<PublicDocument>(Model.Create<PublicDocument>(data));
        }

        #endregion

        #region Notifications/Messaging

        public PagedResult<NotificationTemplate> GetNotificationTemplates(int customerId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, List<GenericFilter> filters = null)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Template>()
                .LeftOuterJoin(Data.ScheduleParameter.IdColumn, Data.Template.ScheduleParameterIDColumn)
                .Where(Data.Template.Columns.CustomerID).IsEqualTo(customerId);
            
            if (string.IsNullOrEmpty(orderBy))
            {
                orderBy = Data.Template.DisplayNameColumn.QualifiedName;
            }

            // if there are no message templates yet, create them from the default set
            if (query.GetRecordCount() == 0)
            {
                // load up the default set of templates
                Data.TemplateCollection templates = new Data.TemplateCollection()
                    .Where(Data.Template.Columns.CustomerID, 0)
                    .Load();

                // and recreate them for this customer
                foreach (Data.Template t in templates)
                {
                    // load up the original list of recipients for this template
                    Data.TemplatesToRecipientGroupsMapCollection maps = new Data.TemplatesToRecipientGroupsMapCollection()
                        .Where(Data.TemplatesToRecipientGroupsMap.Columns.TemplateID, t.Id)
                        .Load();

                    // create a copy of the template for this customer
                    t.Id = 0;
                    t.CustomerID = CustomerID;
                    t.MarkNew();
                    t.Save(Username);

                    // save the recipients with the new template id
                    foreach (Data.TemplatesToRecipientGroupsMap m in maps)
                    {
                        m.TemplateID = t.Id;
                        m.MarkNew();
                        m.Save(Username);
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.AndExpression(Data.Template.DisplayNameColumn.QualifiedName).ContainsString(searchText)
                    .Or(Data.Template.SubjectColumn).ContainsString(searchText)
                .CloseExpression();
            }

            if (filters != null && filters.Count > 0)
            {
                query.ApplyGenericFilter<Data.Template>(filters);
            }

            return new PagedResult<NotificationTemplate>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<bool> DeleteNotificationTemplate(int customerId, int userId, int notificationTemplateId)
        {
            Data.Template tp = new Data.Template(notificationTemplateId);
            if (tp.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to the training program with id " + notificationTemplateId);
            }
            Data.Template.Destroy(notificationTemplateId);
            return new SingleResult<bool>(true);
        }

        public MultipleResult<ScheduleParameter> GetScheduleParameters()
        {
            //TODO: this entire list should be cached and never re-calculated
            List<ScheduleParameter> parameters = Select.AllColumnsFrom<Data.ScheduleParameter>().ExecuteTypedList<ScheduleParameter>();

            foreach (ScheduleParameter parameter in parameters)
            {
                Data.ScheduleParameter sp = new Data.ScheduleParameter(parameter.ID);
                // add the sub-elements as a tree structure
                parameter.ValidTemplateParameters = NotificationTemplateController.GetTemplateParametersForScheduleParameter(sp);
                parameter.PossibleRecipients = NotificationTemplateController.GetRecipientGroupsForScheduleParameter(sp);
            }

            return new MultipleResult<ScheduleParameter>(parameters);
        }
        public PagedResult<NotificationTemplate> GetNotificationTemplatesDetail(int customerId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, List<GenericFilter> filters)
        {
            if (filters == null)
            {
                filters = new List<GenericFilter>();
            }

            filters.Add(new GenericFilter
            {
                IsOpenExpression = true,
                LogicOperator = (int)LogicOperators.And,
                Property = Data.Template.IsConfigurableForTrainingProgramColumn.QualifiedName,
                Value = "true"
            });
            filters.Add(new GenericFilter
            {
                LogicOperator = (int)LogicOperators.Or,
                Property = Data.ScheduleParameter.IsConfigurableForTrainingProgramColumn.QualifiedName,
                Value = "true"
            });

            PagedResult<NotificationTemplate> result = GetNotificationTemplates(customerId, searchText, orderBy, orderDir, pageIndex, pageSize, filters);

            foreach (NotificationTemplate t in result.Data)
            {
                AddNotificationDetails(t);
            }
            
            return result;
        }

        private void AddNotificationDetails(NotificationTemplate template)
        {
            Data.Template dataTemplate = new Data.Template(template.ID);
            Data.ScheduleParameter dataParameter = new Data.ScheduleParameter(template.ScheduleParameterID);

            if (template.ScheduleParameterID > 0)
            {
                // for scheduled templates, use the schedule parameter id to get the list of recipients
                template.PossibleRecipients = NotificationTemplateController.GetRecipientGroupsForScheduleParameter(dataParameter);
                template.ValidTemplateParameters = NotificationTemplateController.GetTemplateParametersForScheduleParameter(dataParameter);

                if (template.ScheduleParameter == null)
                {
                    template.ScheduleParameter = new ScheduleParameter();
                }

                template.ScheduleParameter.CopyFrom(dataParameter);
            }
            else
            {
                // for non-scheduled templates, the parameters list needs to be loaded based on the template name
                // instead of the scheduleparameterid
                template.PossibleRecipients = NotificationTemplateController.GetRecipientGroupsForTemplate(dataTemplate);
                template.ValidTemplateParameters = NotificationTemplateController.GetTemplateParametersForTemplate(dataTemplate);
            }

            template.SelectedRecipients = Select.AllColumnsFrom<Data.RecipientGroup>()
                .InnerJoin(Data.TemplatesToRecipientGroupsMap.RecipientGroupIDColumn, Data.RecipientGroup.IdColumn)
                .Where(Data.TemplatesToRecipientGroupsMap.TemplateIDColumn).IsEqualTo(template.ID)
                .ExecuteTypedList<RecipientGroup>();

            template.PossibleRecipients = template.PossibleRecipients.OrderBy(p => p.DisplayName).ToList();

            if (template.OwnerID > 0)
            {
                Data.User user = new Data.User(template.OwnerID);
                template.OwnerName = Model.FormatFullName(user.FirstName, user.MiddleName, user.LastName);
            }

            Data.TrainingProgram t = new Data.TrainingProgram();
        }

        public SingleResult<NotificationTemplate> GetNotificationTemplate(int customerId, int templateId)
        {
            SqlQuery q = Select.AllColumnsFrom<Data.Template>().Where(Data.Template.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.Template.Columns.Id).IsEqualTo(templateId);

            NotificationTemplate template = q.ExecuteSingle<NotificationTemplate>();
            
            AddNotificationDetails(template);

            return new SingleResult<NotificationTemplate>(template);
        }

        public MultipleResult<NotificationTemplate> GetNotificationTemplatesForTrainingProgram(int trainingProgramId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Template>()
                .InnerJoin(Data.TrainingProgramTemplate.TemplateIDColumn, Data.Template.IdColumn)
                .Where(Data.TrainingProgramTemplate.TrainingProgramIDColumn).IsEqualTo(trainingProgramId);
            
            return new MultipleResult<NotificationTemplate>(query);
        }

        public SingleResult<NotificationTemplate> SaveNotificationTemplate(int customerId, int templateId, Models.NotificationTemplate template)
        {
            Data.Template data = new Data.Template(templateId);

            // basic information
            data.Id = templateId;
            data.DisplayName = template.DisplayName;
            data.Description = template.Description;
            data.Enabled = template.Enabled;
            data.Subject = template.Subject;
            data.Body = Utilities.CleanWordXmlComments(template.Body);
            data.CustomerID = customerId;
            data.OwnerID = template.OwnerID;

            // scheduling information
            data.IsScheduled = template.IsScheduled;
            data.FilterComplete = template.FilterComplete;
            data.RelativeScheduledMinutes = template.RelativeScheduledMinutes;
            data.ScheduleParameterID = template.ScheduleParameterID;
            data.Percentage = template.Percentage;
            data.CcClassroomSupervisors = template.CcClassroomSupervisors;
            data.CcReportingSupervisors = template.CcReportingSupervisors;
            data.IncludeCalendarInvite = template.IncludeCalendarInvite;

            // Check for user created system templates
            if (!data.IsScheduled && data.Id == 0)
            {
                data.IsUserCreated = true;
                data.CodeName = template.CodeName; // Make sure the codename is saved for system templates.
                data.IsConfigurableForTrainingProgram = true; // Always make sure it is configurable for a training program
                                                              // as this is the only way we can enable this template.
            }


            // save the data so if we have a new template we get an id
            data.Save(Username);


            // add the recipients
            Data.TemplatesToRecipientGroupsMapCollection recipients = new Symphony.Core.Data.TemplatesToRecipientGroupsMapCollection();
            foreach (RecipientGroup group in template.SelectedRecipients)
            {
                Data.TemplatesToRecipientGroupsMap map = new Symphony.Core.Data.TemplatesToRecipientGroupsMap();
                map.RecipientGroupID = group.ID;
                map.TemplateID = data.Id;
                recipients.Add(map);
            }

            // kill the existing maps
            Data.TemplatesToRecipientGroupsMap.Destroy(Data.TemplatesToRecipientGroupsMap.Columns.TemplateID, data.Id);

            // save everything
            recipients.SaveAll(Username);

            return GetNotificationTemplate(customerId, data.Id);
        }

        public SingleResult<bool> SendMessage(QuickMessage message)
        {
            bool success = false;

            // Lookup all of the actual users based on the selected hierarchy nodes
            List<User> users = new List<User>();
            foreach (var recipentNode in message.Recipients)
            {
                StoredProcedure sp = Data.SPs.GetUsersByHierarchy(recipentNode.HierarchyTypeID, recipentNode.HierarchyNodeID);
                users.AddRange(sp.ExecuteTypedList<User>());
            }
            users = users.Distinct(new UserComparer()).AsQueryable().OrderBy("FullName").ToList();


            if ((new NotificationController()).QuickSendNotification(this.CustomerID, this.UserID, users, message.Subject, message.Body).Count == users.Count)
            {
                success = true;
            }

            return new SingleResult<bool>(success);
        }

        public PagedResult<Message> GetTemplateHistory(int customerId, int templateId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            Data.Template template = new Data.Template(templateId);
            if (template.Id == 0)
            {
                throw new Exception("Invalid template specified.");
            }
            if (template.CustomerID != customerId)
            {
                throw new Exception("Permission denied.");
            }
            SqlQuery query = Select.AllColumnsFrom<Data.NotificationExtended>()
                .Where(Data.NotificationExtended.Columns.TemplateID).IsEqualTo(templateId)
                .And(Data.NotificationExtended.Columns.DeletedFromHistory).IsEqualTo(false);

            if (!string.IsNullOrEmpty(searchText))
            {
                query.AndExpression(Data.NotificationExtended.Columns.Recipient).ContainsString(searchText)
                    .CloseExpression();
            }

            PagedResult<Message> result = new PagedResult<Message>(query, pageIndex, pageSize, orderBy, orderDir);
            return result;
        }

        public SingleResult<bool> ClearTemplateHistory(int customerId, int templateId)
        {
            Data.Template template = new Data.Template(templateId);
            if (template.Id == 0)
            {
                throw new Exception("Invalid template specified.");
            }
            if (template.CustomerID != customerId)
            {
                throw new Exception("Permission denied.");
            }
            new CodingHorror().Execute("update Notifications set DeletedFromHistory=1 where TemplateID = @templateId", templateId);
            return new SingleResult<bool>(true);
        }

        public MultipleResult<NotificationTemplate> GetUserCreatableSystemTemplates()
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Template>()
                .Where(Data.Template.IsConfigurableForTrainingProgramColumn).IsEqualTo(true)
                .And(Data.Template.CustomerIDColumn).IsEqualTo(0);

            List<NotificationTemplate> templates = query.ExecuteTypedList<NotificationTemplate>();

            foreach (NotificationTemplate t in templates)
            {
                AddNotificationDetails(t);
            }

            return new MultipleResult<NotificationTemplate>(templates);
        }

        #endregion

        #region Locked Users

        public PagedResult<LockedUser> GetLockedUsers(int customerId, int userId, bool lockedOnly, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LockedUser>()
                .Where(Data.LockedUser.Columns.CustomerID).IsEqualTo(customerId);

            if (!string.IsNullOrEmpty(searchText))
            {
                query.AndExpression(Data.LockedUser.Columns.FirstName).ContainsString(searchText).Or(Data.LockedUser.Columns.LastName).ContainsString(searchText)
                    .CloseExpression();
            }

            if (lockedOnly)
            {
                query.And(Data.LockedUser.Columns.LockedCourseCount).IsGreaterThan(0);
            }

            if (string.IsNullOrEmpty(orderBy))
            {
                orderBy = Data.LockedUser.Columns.Username;
            }

            return new PagedResult<LockedUser>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public MultipleResult<OnlineCourseRegistration> GetLockedUserCourses(int customerId, int userId, int targetUserId, bool lockedOnly)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LockedUserCourse>()
                .Where(Data.LockedUserCourse.Columns.UserID).IsEqualTo(targetUserId);

            if (lockedOnly)
            {
                query.And(Data.LockedUserCourse.Columns.IsLockedOut).IsEqualTo(true);
            }

            return new MultipleResult<OnlineCourseRegistration>(query);
        }

        #endregion

        #region affidavit

        public PagedResult<AffidavitFinalExam> FindAffidavits(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.AffidavitFinalExam>();

            if (!string.IsNullOrEmpty(searchText))
            {
                query.Where(Data.AffidavitFinalExam.NameColumn).ContainsString(searchText);
            }

            return new PagedResult<AffidavitFinalExam>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<AffidavitFinalExam> GetAffidavit(int id)
        {
            AffidavitFinalExam affidavit = Select.AllColumnsFrom<Data.AffidavitFinalExam>().Where(Data.AffidavitFinalExam.IdColumn).IsEqualTo(id).ExecuteSingle<AffidavitFinalExam>();

            return new SingleResult<AffidavitFinalExam>(affidavit);
        }

        #endregion

        /// <summary>
        /// Returns the requested launch step
        /// </summary>
        /// <param name="codename"></param>
        /// <param name="trainingProgramId"></param>
        /// <param name="courseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public SingleResult<DisplayLaunchStep> GetLaunchStep(string codename, int trainingProgramId, int courseId, int userId)
        {
            Data.TrainingProgram trainingProgram = new Data.TrainingProgram(trainingProgramId);
            Data.TrainingProgram parentTrainingProgram = null;

            int actualTrainingProgramId = trainingProgram.ParentTrainingProgramID.HasValue && trainingProgram.ParentTrainingProgramID.Value > 0 ?
                trainingProgram.ParentTrainingProgramID.Value : trainingProgram.Id;

            if (trainingProgram.ParentTrainingProgramID > 0)
            {
                parentTrainingProgram = new Data.TrainingProgram(trainingProgram.ParentTrainingProgramID);
            }

            if (codename == DisplayLaunchStep.ValidationRequiredLaunchStep.Code)
            {
                // add check to see if the user's info is already present.
                DisplayLaunchStep validationLaunchStep = DisplayLaunchStep.ValidationRequiredLaunchStep.GetInstance();
                
                JObject validationFormContent = JsonConvert.DeserializeObject<JObject>(validationLaunchStep.Content.Value);

                JObject userField = new JObject();
                userField["xtype"] = "hiddenfield";
                userField["value"] = userId;
                userField["name"] = "userId";

                JArray validationItems = (JArray)validationFormContent["items"];
                validationItems.Add(userField);

                validationLaunchStep.Content = JsonString.Dynamic(validationFormContent.ToString());

                return new SingleResult<DisplayLaunchStep>(validationLaunchStep);

            } else if (codename == DisplayLaunchStep.ProctorRequiredLaunchStep.Code)
            {
                Data.ProctorForm proctorForm = parentTrainingProgram != null ? parentTrainingProgram.ProctorForm :
                    trainingProgram.ProctorForm;

                DisplayLaunchStep launchStep = DisplayLaunchStep.ProctorRequiredLaunchStep.GetInstance();

                JObject proctorFormContent = JsonConvert.DeserializeObject<JObject>(proctorForm.ProctorJSON);

                JObject userField = new JObject();
                userField["xtype"] = "hiddenfield";
                userField["value"] = userId;
                userField["name"] = "userId";

                JObject courseField = new JObject();
                courseField["xtype"] = "hiddenfield";
                courseField["value"] = courseId;
                courseField["name"] = "courseId";

                JObject trainingProgramField = new JObject();
                trainingProgramField["xtype"] = "hiddenfield";
                trainingProgramField["value"] = trainingProgramId; // saving to the child training program id if this is a child. 
                trainingProgramField["name"] = "trainingProgramId";

                JObject proctorFormIdField = new JObject();
                proctorFormIdField["xtype"] = "hiddenfield";
                proctorFormIdField["value"] = proctorForm.Id;
                proctorFormIdField["name"] = "proctorFormId";

                JArray proctorItems = (JArray)proctorFormContent["items"];
                proctorItems.Add(userField);
                proctorItems.Add(courseField);
                proctorItems.Add(trainingProgramField);
                proctorItems.Add(proctorFormIdField);

                launchStep.Content = JsonString.Dynamic(proctorFormContent.ToString());

                return new SingleResult<DisplayLaunchStep>(launchStep);
            }
            else if (codename == DisplayLaunchStep.AffidavitRequiredLaunchStep.Code)
            {
                int? affidavitId = trainingProgram.ParentTrainingProgramID > 0 ? parentTrainingProgram.TrainingProgramAffidavitID : trainingProgram.TrainingProgramAffidavitID;

                // Only one exam per training program
                if (trainingProgram.FinalAssessments.Count > 0 && trainingProgram.FinalAssessments[0].Id == courseId)
                {
                    affidavitId = trainingProgram.ParentTrainingProgramID > 0 ? parentTrainingProgram.AffidavitID : trainingProgram.AffidavitID;
                }

                if (affidavitId > 0)
                {
                    AffidavitFinalExam affidavit = new AffidavitController().FindAffidavit(affidavitId.Value).Data;
                    DisplayLaunchStep launchStep = DisplayLaunchStep.AffidavitRequiredLaunchStep.GetInstance();

                    JObject affidavitContent = JsonConvert.DeserializeObject<JObject>(affidavit.AffidavitJSON);

                    JObject userField = new JObject();
                    userField["xtype"] = "hiddenfield";
                    userField["value"] = userId;
                    userField["name"] = "userId";

                    JObject courseField = new JObject();
                    courseField["xtype"] = "hiddenfield";
                    courseField["value"] = courseId;
                    courseField["name"] = "courseId";

                    JObject trainingProgramField = new JObject();
                    trainingProgramField["xtype"] = "hiddenfield";
                    trainingProgramField["value"] = trainingProgramId; // saving to the child training program id if this is a child. 
                    trainingProgramField["name"] = "trainingProgramId";

                    JArray affidavitItems = (JArray)affidavitContent["items"];
                    affidavitItems.Add(userField);
                    affidavitItems.Add(courseField);
                    affidavitItems.Add(trainingProgramField);

                    launchStep.Content = JsonString.Dynamic(affidavitContent.ToString());

                    return new SingleResult<DisplayLaunchStep>(launchStep);
                }

                throw new Exception("Training program is not configured with an affidavit. This launch step should not be requested.");
            }
            else if (codename == DisplayLaunchStep.AccreditationDisclaimerLaunchStep.Code)
            {
                AccreditationController accreditations = new AccreditationController();
                Data.TrainingProgramRollup rollup = new RollupController().GetRollup(trainingProgramId, userId);
                int professionId = 0;
                if (rollup != null && rollup.ProfessionID.HasValue && rollup.ProfessionID.Value > 0)
                {
                    professionId = rollup.ProfessionID.Value;
                }

                List<AccreditationBoard> accreditationBoards = accreditations.QueryAccreditationBoardsForTrainingProgram(actualTrainingProgramId, professionId, courseId);

                AccreditationDocument accreditationStatement = new AccreditationDocument
                {
                    TrainingProgram = new TrainingProgram(),
                    AccreditationBoards = accreditationBoards,
                    Profession = new Profession(),
                    // Todo
                    // 1. Could be slow - trainingProgram.TrainingProgramToCourseLinks will generated an additional query, but since 
                    // we aren't looping here this should be fine. As long as GetLaunchStep is not called when retrieving training program
                    // details, which should be the case as we only return placeholder data for these steps in that case and then we call 
                    // the actual data later. 
                    // 2. This is somewhat repeated from calls above. May consider a refactor for better reuse. But for now just slapping
                    // this in so we can get a demo / proof of concept here. 
                    Authors = Select.AllColumnsFrom<Data.FastSearchableUser>()
                        .Where(Data.FastSearchableUser.Columns.Id).In(
                            new Select(Data.OnlineCourseAuthorLink.Columns.UserId)
                                .From<Data.OnlineCourseAuthorLink>()
                                .Where(Data.OnlineCourseAuthorLink.Columns.OnlineCourseId).In(trainingProgram.TrainingProgramToCourseLinks.Where(l => l.CourseTypeID == (int)CourseType.Online).Select(l => l.CourseID))
                         )
                    .ExecuteTypedList<Models.Author>()
                };
                accreditationStatement.TrainingProgram.CopyFrom(trainingProgram);
                accreditationStatement.Profession.CopyFrom(new Data.Profession(professionId));

                JArray items = new JArray();
                JObject disclaimer = new JObject();
                JObject container = new JObject();
                
                if (accreditationBoards.Count > 0)
                {
                    disclaimer["xtype"] = "portal.disclaimer";
                    disclaimer["accreditationBoardData"] = JToken.FromObject(accreditationBoards);
                    disclaimer["html"] = accreditations.GetAccreditationHtml(accreditationStatement);
                    disclaimer["border"] = false;

                    container["width"] = 700;
                    container["height"] = 500;
                }
                else
                {
                    disclaimer["xtype"] = "panel";
                    disclaimer["html"] = Text.LaunchStep_Disclaimer_None.Value;
                    disclaimer["htmlCode"] = Text.LaunchStep_Disclaimer_None.Code;
                    disclaimer["border"] = false;

                    container["width"] = 200;
                    container["height"] = 120;
                }
                 items.Add(disclaimer);

                
                container["items"] = items;
                

                DisplayLaunchStep launchStep = DisplayLaunchStep.AccreditationDisclaimerLaunchStep.GetInstance();
                launchStep.Content = JsonString.Dynamic(container.ToString());

                return new SingleResult<DisplayLaunchStep>(launchStep);

            }
            else if (codename == DisplayLaunchStep.ProfessionRequiredLaunchStep.Code)
            {
                Data.TrainingProgramRollup rollup = new RollupController().GetRollup(trainingProgramId, userId); // training progoram rollups are stored using the child id. 

                // Loading the professions from the parent or the current training program id. 
                List<Profession> professions = new AccreditationController().QueryProfessionsForTrainingProgram(actualTrainingProgramId, courseId);

                JArray items = new JArray();
                JArray professionData = new JArray();

                JObject professionInfo = new JObject();
                professionInfo["xtype"] = "panel";
                professionInfo["html"] = Text.LaunchStep_ProfessionRequired_Info.Value;
                professionInfo["htmlCode"] = Text.LaunchStep_ProfessionRequired_Info.Code;
                professionInfo["border"] = false;
                professionInfo["cls"] = "x-form-item";

                JObject professionSelector = new JObject();
                professionSelector["xtype"] = "portal.professionselector";
                professionSelector["fieldLabel"] = "Profession";
                professionSelector["name"] = "professionId";
                professionSelector["professions"] = JArray.FromObject(professions);

                JObject trainingProgramField = new JObject();
                trainingProgramField["xtype"] = "hiddenfield";
                trainingProgramField["value"] = trainingProgramId; // Not using the parent, possibly the child since rollup is attached to the child if used
                trainingProgramField["name"] = "trainingProgramId";

                JObject userField = new JObject();
                userField["xtype"] = "hiddenfield";
                userField["value"] = userId;
                userField["name"] = "userId";

                items.Add(professionInfo);
                items.Add(professionSelector);
                items.Add(trainingProgramField);
                items.Add(userField);

                JObject container = new JObject();
                container["items"] = items;
                container["width"] = 300;
                container["height"] = 170;
                
                DisplayLaunchStep launchStep = DisplayLaunchStep.ProfessionRequiredLaunchStep.GetInstance();
                launchStep.Content = JsonString.Dynamic(container.ToString());

                if (professions.Count == 0 || (rollup != null && rollup.ProfessionID.HasValue && rollup.ProfessionID.Value > 0))
                {
                    launchStep.IsSkip = true;
                }

                return new SingleResult<DisplayLaunchStep>(launchStep);
            }
            else if (codename == DisplayLaunchStep.PrevalidationRequiredLaunchStep.Code)
            {
                // get a random validation question
                var question = ValidationController.GetRandomValidationQuestion();

                // Build a form here with the random question
                JArray items = new JArray();

                JObject textfield = new JObject();
                textfield["xtype"] = "textfield";
                textfield["value"] = "";
                textfield["name"] = "validationValue";
                textfield["fieldLabel"] = question.Title;

                JObject userField = new JObject();
                userField["xtype"] = "hiddenfield";
                userField["value"] = userId;
                userField["name"] = "userId";

                JObject validationKeyField = new JObject();
                validationKeyField["xtype"] = "hiddenfield";
                validationKeyField["value"] = question.Name.ToLower();
                validationKeyField["name"] = "validationKey";

                JObject trainingProgramIdField = new JObject();
                trainingProgramIdField["xtype"] = "hiddenfield";
                trainingProgramIdField["value"] = trainingProgram.Id;
                trainingProgramIdField["name"] = "trainingProgramId";

                items.Add(userField);
                items.Add(textfield);
                items.Add(validationKeyField);
                items.Add(trainingProgramIdField);

                JObject container = new JObject();
                container["items"] = items;
                container["width"] = 300;
                container["height"] = 170;

                DisplayLaunchStep launchStep = DisplayLaunchStep.PrevalidationRequiredLaunchStep.GetInstance();
                launchStep.Content = JsonString.Dynamic(container.ToString());

                return new SingleResult<DisplayLaunchStep>(launchStep);
            }

            else if (codename == DisplayLaunchStep.ConfirmatiionLaunchStep.Code)
            {
                JArray items = new JArray();

                JObject postValidationInfo = new JObject();
                postValidationInfo["xtype"] = "panel";
                postValidationInfo["html"] = Text.LaunchStep_Confirmation_Content.Value;
                postValidationInfo["border"] = false;
                postValidationInfo["cls"] = "x-form-item";

                items.Add(postValidationInfo);

                JObject container = new JObject();
                container["items"] = items;
                container["width"] = 300;
                container["height"] = 170;

                DisplayLaunchStep launchStep = DisplayLaunchStep.PrevalidationRequiredLaunchStep.GetInstance();
                launchStep.Content = JsonString.Dynamic(container.ToString());

                return new SingleResult<DisplayLaunchStep>(launchStep);
            }

            throw new Exception("Invalid code name provided: " + codename);
        }

        public SingleResult<LaunchMeta> SaveProfession(int trainingProgramId, int userId, int professionId, LaunchMeta meta)
        {
            new RollupController().UpdateTrainingProgramRollupProfession(trainingProgramId, userId, professionId);
            return new SingleResult<LaunchMeta>(meta);
        }
        public SingleResult<LaunchMeta> SaveAffidavitResponse(int trainingProgramId, int userId, int courseId, LaunchMeta meta)
        {
            AffidavitFinalExamResponse affidavitResponse = new AffidavitFinalExamResponse();

            affidavitResponse.ResponseJSON = meta.AffidavitJsonString;
            affidavitResponse.UserID = userId;
            affidavitResponse.CourseID = courseId;
            affidavitResponse.TrainingProgramID = trainingProgramId;

            Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);

            int affidavitId = 0;

            if (courseId == 0 && tp.TrainingProgramAffidavitID.HasValue)
            {
                affidavitId = tp.TrainingProgramAffidavitID.Value;
            }
            else if (courseId > 0)
            {
                Data.TrainingProgramToCourseLink tpLink = Select.AllColumnsFrom<Data.TrainingProgramToCourseLink>()
                    .Where(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                    .And(Data.TrainingProgramToCourseLink.CourseIDColumn).IsEqualTo(courseId)
                    .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo((int)CourseType.Online) // Because affidavits only apply to online courses
                    .ExecuteSingle<Data.TrainingProgramToCourseLink>();

                if (tpLink != null && tpLink.CourseID > 0)
                {
                    if (tpLink.SyllabusTypeID == (int)SyllabusType.Final)
                    {
                        affidavitId = tp.AffidavitID.HasValue ? tp.AffidavitID.Value : 0;
                    }
                    else
                    {
                        affidavitId = tp.TrainingProgramAffidavitID.HasValue ? tp.TrainingProgramAffidavitID.Value : 0;
                        affidavitResponse.CourseID = 0; // because we don't want to record a course id if the affidavit is for the whole training program
                    }
                }
            }

            affidavitResponse.AffidavitID = affidavitId;

            var affidavitController = new AffidavitController();
            affidavitController.SaveAffidavitResponse(affidavitResponse);

            return new SingleResult<LaunchMeta>(meta);
        }
        public SingleResult<LaunchMeta> SaveProctor(int trainingProgramId, int courseId, int userId, int proctorFormId, LaunchMeta meta)
        {
            var course = _GetOnlineCourseRollup(trainingProgramId, courseId, userId, meta);
            JObject proctorParams = JsonConvert.DeserializeObject<JObject>(meta.ProctorJsonString);

            if (course == null || course.ProctorKey == null)
            {
                throw new Exception("No Proctor Code Available. Please Contact Your Supervisor");
            }

            if (proctorParams["Code"] == null || proctorParams["Code"].ToString() != course.ProctorKey)
            {
                throw new Exception("Incorrect Proctor Key");
            }

            //regenerate the PK as it is a one time use thing
            var newKey = new RollupController().GenerateProctorKey();
            course.ProctorKey = newKey;

            course.Save();

            var proctorResponse = new ProctorFormResponse();

            proctorResponse.ResponseJSON = meta.ProctorJsonString;
            proctorResponse.UserID = userId;
            proctorResponse.CourseID = courseId;
            proctorResponse.TrainingProgramID = trainingProgramId;
            proctorResponse.ProctorFormID = proctorFormId;

            var proctorController = new ProctorController();
            proctorController.SaveProctor(proctorResponse);

            return new SingleResult<LaunchMeta>(meta);
        }
        public SingleResult<bool> SaveValidationParameters(int userId, string ssn, string dob)
        {
            UserSecurity user = User.Create<UserSecurity>(new Data.User(userId));
            
            user.DOB = dob;
            user.SSN = ssn;

            new CustomerController().SaveUser(CustomerID, UserID, userId, user);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> SavePreValidation(int userId, string validationKey, string validationValue, int tpid)
        {
            UserSecurity user = User.Create<UserSecurity>(new Data.User(userId));
            user.GetType().GetProperty(validationKey.ToUpper()).SetValue(user, validationValue);

            Data.User userData = new Data.User(userId);
            if (userData == null)
            {
                throw new Exception("Validation failed");
            }

            switch (validationKey.ToUpper())
            {
                case "SSN":
                    var ssnHash = user.HashSSN();
                    if (!ssnHash.SequenceEqual(userData.Ssn))
                    {
                        throw new Exception("Validation failed");
                    }
                    break;
                case "DOB":
                    if (user.DateOfBirth.Value.Date != DateTime.ParseExact(validationValue, "dd/MM/yyyy", null))
                    {
                        throw new Exception("Validation failed");
                    }
                    break;
                default:
                    throw new Exception("Validation failed");
            }
            var RUC = new RollupController();
            var tpru = RUC.GetRollup(tpid, userId);
            if (tpru == null)
            {
                // no rollup, so make one!
                RUC.RunDefaultTrainingProgramRollupForUser(tpid, userId);
                tpru = RUC.GetRollup(tpid, userId);
            }

            tpru.PreValidationComplete = true;
            tpru.Save();

            return new SingleResult<bool>(true);
        }

        public SimpleSingleResult<bool> SaveLaunchMeta(int userId, int courseId, int trainingProgramId, LaunchMeta launchMeta)
        {
            if (!string.IsNullOrWhiteSpace(launchMeta.AffidavitJsonString))
            {
                AffidavitFinalExamResponse affidavitResponse = new AffidavitFinalExamResponse();

                affidavitResponse.ResponseJSON = launchMeta.AffidavitJsonString;
                affidavitResponse.UserID = userId;
                affidavitResponse.CourseID = courseId;
                affidavitResponse.TrainingProgramID = trainingProgramId;

                Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);

                int affidavitId = 0;

                if (courseId == 0 && tp.TrainingProgramAffidavitID.HasValue)
                {
                    affidavitId = tp.TrainingProgramAffidavitID.Value;
                }
                else if (courseId > 0)
                {
                    Data.TrainingProgramToCourseLink tpLink = Select.AllColumnsFrom<Data.TrainingProgramToCourseLink>()
                        .Where(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                        .And(Data.TrainingProgramToCourseLink.CourseIDColumn).IsEqualTo(courseId)
                        .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo((int)CourseType.Online) // Because affidavits only apply to online courses
                        .ExecuteSingle<Data.TrainingProgramToCourseLink>();

                    if (tpLink != null && tpLink.CourseID > 0)
                    {
                        if (tpLink.SyllabusTypeID == (int)SyllabusType.Final)
                        {
                            affidavitId = tp.AffidavitID.HasValue ? tp.AffidavitID.Value : 0;
                        }
                        else
                        {
                            affidavitId = tp.TrainingProgramAffidavitID.HasValue ? tp.TrainingProgramAffidavitID.Value : 0;
                            affidavitResponse.CourseID = 0; // because we don't want to record a course id if the affidavit is for the whole training program
                        }
                    }
                }

                affidavitResponse.AffidavitID = affidavitId;
                
                var affidavitController = new AffidavitController();
                affidavitController.SaveAffidavitResponse(affidavitResponse);
            }

            if (!string.IsNullOrWhiteSpace(launchMeta.ProctorJsonString))
            {
                var proctorResponse = new ProctorFormResponse();

                try
                {
                    proctorResponse = JsonConvert.DeserializeObject<ProctorFormResponse>(launchMeta.ProctorJsonString);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }

                proctorResponse.UserID = userId;
                proctorResponse.CourseID = courseId;
                proctorResponse.TrainingProgramID = trainingProgramId;

                // If no responseJson is included in the response, just store the whole thing.
                // This will occur when the old proctor form is used. The response json would
                // have been used to generate the old proctor object so for now just stuff 
                // exactly what was passed in.
                if (string.IsNullOrWhiteSpace(proctorResponse.ResponseJSON))
                {
                    proctorResponse.ResponseJSON = launchMeta.ProctorJsonString;
                }

                // There wont be a proctor form id passed in if using the old proctor form
                // Use the default form, assuming it exists, or load the min proctor form id
                // just to satisfy the foriegn key constraint
                if (proctorResponse.ProctorFormID == 0)
                {
                    ProctorForm proctorForm = Select.AllColumnsFrom<Data.ProctorForm>().Where(Data.ProctorForm.NameColumn)
                        .IsEqualTo("Default")
                        .ExecuteSingle<ProctorForm>();

                    if (proctorForm != null && proctorForm.ID > 0)
                    {
                        proctorResponse.ProctorFormID = proctorForm.ID;
                    }
                    else
                    {
                        // For some reason, default doesn't exist anymore?
                        int proctorFormId = new Select()
                            .From<Data.ProctorForm>()
                            .Min(Data.ProctorForm.IdColumn)
                            .ExecuteScalar<int>();

                        proctorResponse.ProctorFormID = proctorFormId;
                    }
                }

                var proctorController = new ProctorController();
                proctorController.SaveProctor(proctorResponse);
            }

            if (!string.IsNullOrWhiteSpace(launchMeta.ValidationJsonString))
            {
                UserSecurity user = User.Create<UserSecurity>(new Data.User(userId));

                try {
                    UserSecurity userValidation = JsonConvert.DeserializeObject<UserSecurity>(launchMeta.ValidationJsonString);
                    user.DOB = userValidation.DOB;
                    user.SSN = userValidation.SSN;
                } catch (Exception ex) {
                    Log.Error(ex);
                }

                new CustomerController().SaveUser(CustomerID, UserID, userId, user);
            }

            return new SimpleSingleResult<bool>(true);
        }


        public SingleResult<AffidavitFinalExam> SaveAffidavit(int affidavitId, AffidavitFinalExam affidavit)
        {
            Data.AffidavitFinalExam data = new Data.AffidavitFinalExam(affidavit);
            affidavit.CopyTo(data);

            data.Id = affidavitId;

            data.Save();

            return GetAffidavit(data.Id);
        }

        public SingleResult<OnlineCourseRollup> GenerateProctorKey(int rollupId, int courseId, int tpId, int uId)
        {
            Data.OnlineCourseRollup d_ocr;
            if (rollupId != null && rollupId > 0)
            {
                d_ocr = new Data.OnlineCourseRollup(rollupId);
            }
            else
            {
                d_ocr = _GetOnlineCourseRollup(tpId, courseId, uId, null);
            }
            // no rollup, we need to create one... maybe?
            if (d_ocr == null)
            {
                throw new Exception("No Rollup available for this course");
            }
            else
            {
            d_ocr.ProctorKey = new RollupController().GenerateProctorKey();
            d_ocr.Save();
            }
            var ocr = new OnlineCourseRollup();
            ocr.CopyFrom(d_ocr);

            return new SingleResult<OnlineCourseRollup>(ocr);
        }

        private Data.OnlineCourseRollup _GetOnlineCourseRollup(int trainingProgramId, int courseId, int userId, LaunchMeta meta)
        {
            SqlQuery q = Select.AllColumnsFrom<Data.OnlineCourseRollup>()
                .Where(Data.OnlineCourseRollup.Columns.CourseID).IsEqualTo(courseId)
                .And(Data.OnlineCourseRollup.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                .And(Data.OnlineCourseRollup.Columns.UserID).IsEqualTo(userId);

            var course = q.ExecuteSingle<Data.OnlineCourseRollup>();
            if (course == null)
            {
                var RUC = new RollupController();
                RUC.RunDefaultTrainingProgramRollupForUser(trainingProgramId, userId);
                course = q.ExecuteSingle<Data.OnlineCourseRollup>();
    }

            return course;
        }
    }
}