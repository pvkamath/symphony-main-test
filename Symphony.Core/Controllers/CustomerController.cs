﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using SubSonic;
using Symphony.Web;
using System.Web.Security;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using SubSonic.Sugar;
using System.Data;
using log4net;
using System.Web;
using System.Net;
using System.ServiceModel.Web;
using Symphony.Core.Extensions;
using Symphony.Core.Comparers;
using Symphony.Core.Interfaces;
using System.Transactions;

namespace Symphony.Core.Controllers
{
    public class CustomerController : SymphonyController
    {
        private static string[] SubdomainBlacklist = new string[] { };
        private ILog Log = LogManager.GetLogger(typeof(CustomerController));

        static CustomerController()
        {
            string temp = ConfigurationManager.AppSettings["SubdomainBlacklist"];
            if (!string.IsNullOrEmpty(temp))
            {
                SubdomainBlacklist = temp.Split(',');
            }
        }
        public PagedResult<User> FindUsers(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, List<GenericFilter> filter)
        {
            if (!UserIsSalesChannelAdmin)
            {
                throw new Exception("You do not have permission to search for users outside your own customer. Please use the regular user editor.");
            }

            SqlQuery query = new Select(
                Data.User.IdColumn.QualifiedName,
                Data.User.UsernameColumn.QualifiedName,
                Data.User.FirstNameColumn.QualifiedName,
                Data.User.LastNameColumn.QualifiedName,
                Data.User.EmailColumn.QualifiedName,
                Data.Customer.NameColumn.QualifiedName + " as CustomerName",
                Data.SalesChannel.NameColumn.QualifiedName + " as SalesChannelName",
                Data.Customer.IdColumn.QualifiedName + " as CustomerID"
            ).From<Data.User>()
            .InnerJoin(Data.Customer.IdColumn, Data.User.CustomerIDColumn)
            .InnerJoin(Data.SalesChannel.IdColumn, Data.Customer.SalesChannelIDColumn);

            if (!string.IsNullOrWhiteSpace(searchText))
            {

                bool search = true;
                if (searchText.StartsWith("#"))
                {
                    var num = searchText.Trim('#');
                    int x = 0;
                    if (int.TryParse(num, out x))
                    {
                        // specific ID
                        query.Where(Data.User.IdColumn.QualifiedName).IsEqualTo(x);
                        search = false;
                    }
                }

                if (search)
                {
                    query.WhereExpression(Data.User.FirstNameColumn.QualifiedName).ContainsString(searchText)
                        .Or(Data.User.UsernameColumn).ContainsString(searchText)
                        .Or(Data.User.LastNameColumn).ContainsString(searchText)
                        .Or(Data.User.MiddleNameColumn).ContainsString(searchText)
                        .Or(Data.User.EmailColumn).ContainsString(searchText)
                        .Or(Data.Customer.NameColumn).ContainsString(searchText)
                        .Or(Data.SalesChannel.NameColumn).ContainsString(searchText)
                     .CloseExpression();
                }
            }
            else
            {
                query.Where(Data.User.IdColumn).IsGreaterThan(0); // To make sure we have a where clause set when applying generic filter
            }

            if (orderBy == "email")
            {
                orderBy = Data.User.EmailColumn.QualifiedName;
            }

            if (filter != null && filter.Count > 0)
            {
                query.ApplyGenericFilter<Data.User>(filter);
            }

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }
        public PagedResult<User> FindUsers(List<int> exclude, int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, CustomerUserFilter filter, int jobRoleId, int locationId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>()
                .Where(Data.FastSearchableUser.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.FastSearchableUser.Columns.IsExternal).IsEqualTo(false)
                .And(Data.FastSearchableUser.Columns.SalesChannelID).IsEqualTo(0)
                .And(Data.FastSearchableUser.Columns.Search).ContainsString(searchText);

            if (filter != null)
            {
                query = query.And(Data.FastSearchableUser.Columns.StatusID).IsEqualTo((int)filter.Status);
            }

            if (exclude != null && exclude.Count > 0)
            {
                query.And(Data.FastSearchableUser.Columns.Id).NotIn(exclude);
            }

            if (jobRoleId > 0)
            {
                List<User> users = Data.SPs.GetUsersByHierarchy((int)HierarchyType.JobRole, jobRoleId).ExecuteTypedList<User>();
                List<int> ids = users.Select(u => u.ID).ToList();
                if (ids.Count == 0)
                {
                    ids = new List<int>() { -1 };
                }
                query = query.And(Data.FastSearchableUser.Columns.Id)
                        .In(ids)
                        .And(Data.FastSearchableUser.Columns.Id)
                        .IsNotEqualTo(userId);
            }
            if (locationId > 0)
            {
                List<User> users = Data.SPs.GetUsersByHierarchy((int)HierarchyType.Location, locationId).ExecuteTypedList<User>();
                List<int> ids = users.Select(u => u.ID).ToList();
                if (ids.Count == 0)
                {
                    ids = new List<int>() { -1 };
                }
                query = query.And(Data.FastSearchableUser.Columns.Id)
                    .In(ids)
                    .And(Data.FastSearchableUser.Columns.Id)
                    .IsNotEqualTo(userId);
            }


            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<User> FindUnassignedUsers(HierarchyType hierarchyType, int hierarchyId, int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>()
                .Where(Data.FastSearchableUser.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.FastSearchableUser.Columns.IsExternal).IsEqualTo(false)
                .And(Data.FastSearchableUser.Columns.SalesChannelID).IsEqualTo(0)
                .And(Data.FastSearchableUser.Columns.Search).ContainsString(searchText);

            switch (hierarchyType)
            {
                case HierarchyType.Audience:
                    query = query
                        .And(Data.FastSearchableUser.Columns.Id)
                        .NotIn(new Select(Data.UserAudience.Columns.UserID)
                            .From<Data.UserAudience>()
                            .Where(Data.UserAudience.Columns.AudienceID)
                            .IsEqualTo(hierarchyId)
                        );
                    break;
                case HierarchyType.JobRole:
                    query = query
                        .And(Data.FastSearchableUser.Columns.Id)
                        .NotIn(new Select(Data.FastSearchableUser.Columns.Id)
                            .From<Data.FastSearchableUser>()
                            .Where(Data.FastSearchableUser.Columns.JobRoleID)
                            .IsEqualTo(hierarchyId)
                        );
                    break;
                case HierarchyType.Location:
                    query = query
                        .And(Data.FastSearchableUser.Columns.Id)
                        .NotIn(new Select(Data.FastSearchableUser.Columns.Id)
                            .From<Data.FastSearchableUser>()
                            .Where(Data.FastSearchableUser.Columns.LocationID)
                            .IsEqualTo(hierarchyId)
                        );
                    break;
            }

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ImportHistory> FindImportHistory(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ImportJobQueue>()
                .Where(Data.ImportJobQueue.Columns.CustomerID).IsEqualTo(customerId)
                .AndExpression(Data.ImportJobQueue.Columns.StatusMsg).ContainsString(searchText)
                .Or(Data.ImportJobQueue.Columns.ErrorMsg).ContainsString(searchText);

            switch (orderBy)
            {
                case "lastUpdated":
                    orderBy = Data.ImportJobQueue.Columns.StatusDate;
                    break;
                case "status":
                    orderBy = Data.ImportJobQueue.Columns.StatusMsg + ", " + Data.ImportJobQueue.Columns.ErrorMsg;
                    break;
            }

            if (string.IsNullOrEmpty(orderBy))
            {
                orderBy = Data.ImportJobQueue.Columns.StartDate;
            }
            if (orderDir == OrderDirection.Default)
            {
                orderDir = OrderDirection.Descending;
            }

            PagedResult<Data.ImportJobQueue> initalResult = new PagedResult<Data.ImportJobQueue>(query, pageIndex, pageSize, orderBy, orderDir);

            IEnumerable<ImportHistory> result = initalResult.Data.Select(i => new ImportHistory()
            {
                ID = i.Id,
                EndDate = i.EndDate.Value.WithUtcFlag(),
                StartDate = i.StartDate.Value.WithUtcFlag(),
                LastUpdated = i.StatusDate.Value.WithUtcFlag(),
                Description = i.Description,
                Errored = string.IsNullOrEmpty(i.ErrorMsg) ? false : true,
                Status = string.IsNullOrEmpty(i.ErrorMsg) ? i.StatusMsg : i.ErrorMsg,
                StatusCode = i.StatusCode.Value,
                SessionUserIDs = i.SessionUserIDs,
                SessionUserJson = i.SessionUserJson
            });

            return new PagedResult<ImportHistory>(result, pageIndex, pageSize, initalResult.TotalSize);
        }

        public SingleResult<bool> SaveLocationUsers(int customerId, int userId, int locationId, int[] userIds)
        {
            Data.UserCollection users = new Symphony.Core.Data.UserCollection();
            foreach (int id in userIds)
            {
                Data.User user = new Symphony.Core.Data.User(id);
                user.LocationID = locationId;
                users.Add(user);
            }
            users.SaveAll(Username);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> SaveJobRoleUsers(int customerId, int userId, int jobRoleId, int[] userIds)
        {
            Data.UserCollection users = new Symphony.Core.Data.UserCollection();
            foreach (int id in userIds)
            {
                Data.User user = new Symphony.Core.Data.User(id);
                user.JobRoleID = jobRoleId;
                users.Add(user);
            }
            users.SaveAll(Username);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> SaveAudienceUsers(int customerId, int userId, int audienceId, int[] userIds)
        {
            Data.UserAudienceCollection userAudiences = new Symphony.Core.Data.UserAudienceCollection();

            foreach (int id in userIds)
            {
                if (Select.AllColumnsFrom<Data.UserAudience>()
                    .Where(Data.UserAudience.Columns.UserID).IsEqualTo(id)
                    .And(Data.UserAudience.Columns.AudienceID).IsEqualTo(audienceId)
                    .GetRecordCount() == 0)
                {
                    Data.UserAudience ua = new Symphony.Core.Data.UserAudience();
                    ua.UserID = id;
                    ua.AudienceID = audienceId;
                    userAudiences.Add(ua);
                }
            }
            userAudiences.SaveAll(Username);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteLocationUsers(int customerId, int userId, int locationId, int[] userIds)
        {
            Data.UserCollection users = new Symphony.Core.Data.UserCollection();
            foreach (int id in userIds)
            {
                Data.User user = new Symphony.Core.Data.User(id);
                user.LocationID = 0;
                users.Add(user);
            }
            users.SaveAll(Username);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteJobRoleUsers(int customerId, int userId, int jobRoleId, int[] userIds)
        {
            Data.UserCollection users = new Symphony.Core.Data.UserCollection();
            foreach (int id in userIds)
            {
                Data.User user = new Symphony.Core.Data.User(id);
                user.JobRoleID = 0;
                users.Add(user);
            }
            users.SaveAll(Username);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteAudienceUsers(int customerId, int userId, int audienceId, int[] userIds)
        {
            // get all the users that are mapped to the specified audience
            Data.UserAudienceCollection userAudiences = new Symphony.Core.Data.UserAudienceCollection()
                .Where(Data.UserAudience.Columns.AudienceID, audienceId)
                .Load();

            // any of those that match the specified user ids as well are trashed
            foreach (Data.UserAudience ua in userAudiences)
            {
                if (userIds.Contains(ua.UserID))
                {
                    Data.UserAudience.Destroy(Data.User.Columns.Id, ua.Id);
                }
            }

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteUser(int customerId, int currentUserId, int targetUserId)
        {
            Data.User user = new Symphony.Core.Data.User(targetUserId);
            if (user.CustomerID != customerId)
            {
                throw new Exception("Permission denied.");
            }

            user.StatusID = (int)UserStatusType.Deleted;
            user.Save(Username);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> ResetUser(int customerId, int currentUserId, int targetUserId)
        {
            Data.User user = new Symphony.Core.Data.User(targetUserId);
            if (user.CustomerID != customerId)
            {
                throw new Exception("Permission denied.");
            }
            string subdomain = new Data.Customer(customerId).SubDomain;
            if (!SymphonyMembershipProvider.UnlockUser(subdomain, SymphonyMembershipProvider.GetUser(subdomain, user.Username), false))
            {
                throw new Exception("Failed to unlock the account. Please contact your representative for further assistance.");
            }

            return new SingleResult<bool>(true);
        }

        public PagedResult<User> FindUsersForAudience(int customerId, int audienceId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>()
                .InnerJoin(Data.UserAudience.Schema.TableName, Data.UserAudience.Columns.UserID, Data.FastSearchableUser.Schema.TableName, Data.FastSearchableUser.Columns.Id)
                .Where(Data.FastSearchableUser.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.UserAudience.Columns.AudienceID).IsEqualTo(audienceId)
                .And(Data.FastSearchableUser.Columns.Search).ContainsString(searchText);

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<User> FindUsersForJobRole(int customerId, int jobRoleId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>()
                .Where(Data.FastSearchableUser.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.FastSearchableUser.Columns.JobRoleID).IsEqualTo(jobRoleId)
                .And(Data.FastSearchableUser.Columns.Search).ContainsString(searchText);

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<User> FindUsersForLocation(int customerId, int locationId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>()
                .Where(Data.FastSearchableUser.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.FastSearchableUser.Columns.LocationID).IsEqualTo(locationId)
                .And(Data.FastSearchableUser.Columns.Search).ContainsString(searchText);

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public string GetCustomerNameBySalesChannel(string salesChannelId, string customerName)
        {
            var subdomainBySalesChannel = new Select(Data.Customer.Columns.SubDomain).From<Data.Customer>()
                .InnerJoin(Data.Tables.SalesChannel, Data.SalesChannel.Columns.Id, Data.Tables.Customer, Data.Customer.Columns.SalesChannelID)
                .Where(Data.SalesChannel.CodeColumn).IsEqualTo(salesChannelId)
                .And(Data.Customer.NameColumn).IsEqualTo(customerName).ExecuteScalar().ToString();
            return subdomainBySalesChannel;
        }

        public PagedResult<JobRole> FindJobRoles(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.JobRoleHierarchy>()
                .Where(Data.JobRoleHierarchy.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.JobRoleHierarchy.Columns.LevelText).ContainsString(searchText);

            return new PagedResult<JobRole>(query, pageIndex, pageSize, Data.JobRoleHierarchy.Columns.LevelIndentText, OrderDirection.Ascending);
        }

        public PagedResult<Location> FindLocations(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.LocationHierarchy>()
                .Where(Data.LocationHierarchy.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.LocationHierarchy.Columns.LevelText).ContainsString(searchText);

            return new PagedResult<Location>(query, pageIndex, pageSize, Data.LocationHierarchy.Columns.LevelIndentText, OrderDirection.Ascending);
        }

        public PagedResult<Audience> FindAudiences(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.AudienceHierarchy>()
                .Where(Data.AudienceHierarchy.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.AudienceHierarchy.Columns.LevelText).ContainsString(searchText);

            return new PagedResult<Audience>(query, pageIndex, pageSize, Data.AudienceHierarchy.Columns.LevelIndentText, OrderDirection.Ascending);
        }

        public PagedResult<UserStatus> FindUserStatuses(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.UserStatus>()
                .Where(Data.UserStatus.Columns.Description).ContainsString(searchText);

            return new PagedResult<UserStatus>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Customer> FindCustomers(int salesChannelId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Customer>()
                .Where(Data.Customer.Columns.SalesChannelID).IsEqualTo(salesChannelId)
                .And(Data.Customer.Columns.Name).ContainsString(searchText);

            return new PagedResult<Customer>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Customer> FindAllCustomers(int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, int[] selectedIds = null, List<GenericFilter> filter = null)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Customer>()
                .IncludeColumn(Data.SalesChannel.NameColumn.QualifiedName, "SalesChannelName")
                .InnerJoin(Data.SalesChannel.IdColumn, Data.Customer.SalesChannelIDColumn);

            if (!string.IsNullOrEmpty(searchText))
            {
                bool search = true;
                if (searchText.StartsWith("#"))
                {
                    var num = searchText.Trim('#');
                    int x = 0;
                    if (int.TryParse(num, out x))
                    {
                        // specific ID
                        query.Where(Data.Customer.IdColumn.QualifiedName).IsEqualTo(x);
                        search = false;
                    }
                }

                if (search)
                {
                    query.WhereExpression(Data.Customer.NameColumn.QualifiedName).ContainsString(searchText)
                        .Or(Data.Customer.Columns.SubDomain).ContainsString(searchText)
                        .Or(Data.SalesChannel.NameColumn).ContainsString(searchText)
                     .CloseExpression();
                }
            }

            if (orderBy == "Name" || orderBy == "name" || string.IsNullOrWhiteSpace(orderBy))
            {
                orderBy = Data.Customer.NameColumn.QualifiedName;
            }

            if (selectedIds != null && selectedIds.Length > 0)
            {
                if (query.HasWhere)
                {
                    query.And(Data.Customer.IdColumn).In(selectedIds);
                }
                else
                {
                    query.Where(Data.Customer.IdColumn).In(selectedIds);
                }
            }

            if (filter != null && filter.Count > 0)
            {
                if (!query.HasWhere)
                {
                    query.Where(Data.Customer.IdColumn).IsGreaterThan(0);
                }

                query.ApplyGenericFilter<Data.Customer>(filter);
            }

            return new PagedResult<Customer>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Customer> GetNetworkedCustomers(bool isReporting, bool isTrainingProgram, int customerId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Customer>()
                .ApplyCustomerNetwork(Data.Customer.IdColumn, customerId, isReporting, isTrainingProgram);

            if (!string.IsNullOrEmpty(searchText))
            {
                query.And(Data.Customer.Columns.Name).ContainsString(searchText);
            }

            return new PagedResult<Customer>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<User> FindAdministrators(int salesChannelId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>()
                .Where(Data.FastSearchableUser.Columns.SalesChannelID).IsEqualTo(salesChannelId)
                .And(Data.FastSearchableUser.Columns.FullName).ContainsString(searchText);

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<User> FindAccountExecutives(int salesChannelId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>()
                .Where(Data.FastSearchableUser.Columns.SalesChannelID).IsEqualTo(salesChannelId)
                .And(Data.FastSearchableUser.Columns.IsAccountExec).IsEqualTo(true)
                .And(Data.FastSearchableUser.Columns.FullName).ContainsString(searchText);

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<User> FindCustomerCareReps(int salesChannelId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>()
                .Where(Data.FastSearchableUser.Columns.SalesChannelID).IsEqualTo(salesChannelId)
                .And(Data.FastSearchableUser.Columns.IsCustomerCare).IsEqualTo(true)
                .And(Data.FastSearchableUser.Columns.FullName).ContainsString(searchText);

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<User> FindSupervisors(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            List<User> users = UserController.GetUsersByRole(customerId, userId, Roles.ClassroomSupervisor, searchText);

            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>();

            if (users.Count > 0)
            {
                query.Where(Data.User.Columns.Id).In(users.Select(u => u.ID));
            }
            else
            {
                query.Where(Data.User.Columns.Id).IsEqualTo(-1);
            }

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<User> FindReportingSupervisors(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            List<User> users = UserController.GetUsersByRole(customerId, userId, Roles.ReportingSupervisor, searchText);

            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>();

            if (users.Count > 0)
            {
                query.Where(Data.User.Columns.Id).In(users.Select(u => u.ID));
            }
            else
            {
                query.Where(Data.User.Columns.Id).IsEqualTo(-1);
            }

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<User> FindUsersByRoles(int customerId, int userId, string[] roles, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            List<User> users = new List<User>();
            foreach (string role in roles)
            {
                users.AddRange(UserController.GetUsersByRole(customerId, userId, role, searchText));
            }

            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>();

            if (users.Count > 0)
            {
                query.Where(Data.User.Columns.Id).In(users.Select(u => u.ID));
            }
            else
            {
                query.Where(Data.User.Columns.Id).IsEqualTo(-1);
            }

            return new PagedResult<User>(query, pageIndex, pageSize, orderBy, orderDir);
        }



        public MultipleResult<SalesChannel> GetSalesChannels(int customerId, int userId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.SalesChannelHierarchy>()
                .OrderAsc(Data.SalesChannelHierarchy.Columns.Level, Data.SalesChannelHierarchy.Columns.Name);

            return new MultipleResult<SalesChannel>(query);
        }

        public SingleResult<SalesChannel> GetSalesChannel(int salesChannelId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.SalesChannel>()
                .Where(Data.SalesChannel.IdColumn).IsEqualTo(salesChannelId);

            return new SingleResult<SalesChannel>(query);
        }

        public SingleResult<User> GetUser(int customerId, int currentUserId, int targetUserId, bool bypassPermissions = false)
        {
            if (!bypassPermissions)
            {
                if (!(this.HasRole(Roles.CustomerManager) || this.HasRole(Roles.CustomerAdministrator) || this.HasRole(Roles.CustomerLocationManager) || this.HasRole(Roles.CustomerJobRoleManager) || this.ActualUserIsSalesChannelAdmin))
                {
                    if (currentUserId != targetUserId)
                    {
                        throw new Exception("Permission denied");
                    }
                }
            }

            User user = CacheController.Get<User>(targetUserId);
            if (user == null)
            {

                SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>()
                    .WithNoLock()
                    .Where(Data.FastSearchableUser.Columns.Id).IsEqualTo(targetUserId);

                if (!bypassPermissions)
                {
                    query.And(Data.FastSearchableUser.Columns.CustomerID).IsEqualTo(customerId);
                }

                user = query.ExecuteSingle<User>();

                if (user == null)
                {
                    throw new Exception("The user with id " + targetUserId + " could not be found in the customer with id " + customerId);
                }

                Data.Customer customer = new Data.Customer(customerId);

                user.CustomerName = customer.Name;

                user.IsPasswordResetRequired = false;

                if (user.EnforcePasswordReset)
                {
                    if (!user.LastEnforcedPasswordReset.HasValue)
                    {
                        user.LastEnforcedPasswordReset = DateTime.MinValue;
                    }

                    if (user.LastEnforcedPasswordReset.Value.AddDays(user.EnforcePasswordDays).WithUtcFlag() < DateTime.UtcNow)
                    {
                        user.IsPasswordResetRequired = true;
                    }
                }

                if (user.IsAuthor.HasValue && user.IsAuthor.Value)
                {
                    var authorDetail = Select.AllColumnsFrom<Data.AuthorDetail>()
                        .Where(Data.AuthorDetail.UserIdColumn).IsEqualTo(user.ID)
                        .ExecuteSingle<Data.AuthorDetail>();
                    if (authorDetail != null)
                    {
                        user.Speciality = authorDetail.Speciality;
                    }
                }

                string customerSubdomain = customer.SubDomain;

                // list permissions
                user.ApplicationPermissions = SymphonyRoleProvider.GetRolesForUser(customerSubdomain, user.Username).ToList();

                // determine if they're locked out
                MembershipUser mu = SymphonyMembershipProvider.GetUser(customerSubdomain, user.Username);
                if (mu != null)
                {
                    user.IsLockedOut = mu.IsLockedOut;
                }

                if (user.LastEnforcedPasswordReset == DateTime.MinValue)
                {
                    user.LastEnforcedPasswordReset = new DateTime(1970, 1, 1);
                }

                // get the failed attempt count
                Guid applicationId = new Select("ApplicationID")
                            .From("aspnet_Applications")
                            .Where("ApplicationName")
                            .IsEqualTo(customerSubdomain)
                            .ExecuteScalar<Guid>();
                if (mu != null)
                {
                    user.FailedAttempts = new Select("FailedPasswordAttemptCount")
                        .From("vw_aspnet_MembershipUsers")
                        .Where("UserName").IsEqualTo(mu.UserName)
                        .And("ApplicationId").IsEqualTo(applicationId)
                        .ExecuteScalar<int>();
                }

                user.CustomerSubDomain = customerSubdomain;
                user.CustomerID = customerId;

                // Fast searchable users does have an XML field for loading user data fields, 
                // however, in this case, we need all user data fields including the ones that do not
                // have values so we can properly populate the user editor. 
                // This will override the data fields saved in the xml with all data fields
                user.UserDataFields = GetUserDataFields(user.ID, "", "", OrderDirection.Ascending, 0, int.MaxValue).Data.ToList();
                
                // AM-OCT-7-16: Removed need to look up the user for the AssociatedImage and the Primary/Secondary Profession
                // moved to FastSearchableUser now. Performance of FastSearchableUser has been increased by 50%
                // since Oct 7 2016 so we have the capacity to add more to that view without impacting performance. 

                if (!bypassPermissions)
                {
                    user.Customer = GetCustomer(this.CustomerID, user.ID, user.CustomerID).Data;
                }

                user.IsSpoofing = this.UserID != this.ActualUserID;

                CacheController.Add<User>(user.ID, user);
            }

            return new SingleResult<User>(user);
        }

        public PagedResult<UserDataField> GetUserDataFields(int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {

            Data.User user = new Data.User(userId);
            int customerId = userId > 0 ? user.CustomerID : CustomerID;

            string globalColumn = string.Format(@"
                cast((case when {0} > 0 then 0 else 1 end) as bit)
            ", Data.UserDataField.CustomerIDColumn.QualifiedName);

            SqlQuery dataFieldQuery = Select.AllColumnsFrom<Data.UserDataField>()
                .WithNoLock()
                .IncludeColumn(globalColumn, "IsGlobal")
                .WhereExpression(Data.UserDataField.CustomerIDColumn.QualifiedName).IsEqualTo(customerId)
                    .Or(Data.UserDataField.CustomerIDColumn).IsEqualTo(0)
                .CloseExpression()
                .OrderAsc(Data.UserDataField.Columns.CategoryDisplayName);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                dataFieldQuery.AndExpression(Data.UserDataField.XtypeColumn.QualifiedName).ContainsString(searchText)
                    .Or(Data.UserDataField.CategoryCodeNameColumn).ContainsString(searchText)
                    .Or(Data.UserDataField.CategoryDisplayNameColumn).ContainsString(searchText)
                    .Or(Data.UserDataField.CodeNameColumn).ContainsString(searchText)
                    .Or(Data.UserDataField.DisplayNameColumn).ContainsString(searchText)
                    .CloseExpression();
            }

            PagedResult<UserDataField> userDataFields = new PagedResult<UserDataField>(dataFieldQuery, pageIndex, pageSize, orderBy, orderDir);

            List<Data.UserDataFieldUserMap> userDataFieldValues = Select.AllColumnsFrom<Data.UserDataFieldUserMap>()
                .WithNoLock()
                .Where(Data.UserDataFieldUserMap.UserIDColumn).IsEqualTo(userId)
                .ExecuteTypedList<Data.UserDataFieldUserMap>();

            Dictionary<int, UserDataField> userDataFieldDictionary = userDataFields.Data.ToDictionary(df => df.ID);

            userDataFieldValues.ForEach(v =>
            {
                if (userDataFieldDictionary.ContainsKey(v.UserDataFieldID))
                {
                    userDataFieldDictionary[v.UserDataFieldID].UserValue = v.UserValue;
                    userDataFieldDictionary[v.UserDataFieldID].UserDataFieldUserMapID = v.Id;
                }
            });

            return userDataFields;
        }

        public SingleResult<UserDataField> SaveUserDataField(int userDataFieldId, UserDataField userDataField)
        {


            Data.UserDataField udf = new Data.UserDataField(userDataFieldId);
            userDataField.CopyTo(udf);
            udf.Id = userDataFieldId;

            // set some defaults
            if (!string.IsNullOrWhiteSpace(udf.CategoryDisplayName) && udf.CategoryCodeName == udf.CategoryDisplayName)
            {
                userDataField.CategoryCodeName.ToLower().Replace(" ", "");
            }
            if (string.IsNullOrWhiteSpace(udf.Xtype))
            {
                udf.Xtype = "textfield";
            }
            if (string.IsNullOrWhiteSpace(udf.CategoryCodeName) && !string.IsNullOrWhiteSpace(udf.CategoryDisplayName))
            {
                udf.CategoryCodeName = userDataField.CategoryCodeName.ToLower().Replace(" ", "");
            }
            if (string.IsNullOrWhiteSpace(udf.CategoryCodeName) && string.IsNullOrWhiteSpace(udf.CategoryDisplayName))
            {
                udf.CategoryCodeName = "other";
                udf.CategoryDisplayName = "Other";
            }
            if (string.IsNullOrWhiteSpace(udf.DisplayName))
            {
                throw new Exception("Name is required for user data fields.");
            }
            if (string.IsNullOrWhiteSpace(udf.CodeName))
            {
                udf.CodeName = userDataField.DisplayName.ToLower().Replace(" ", "");
            }

            // Check code names
            UserDataField existing = Select.AllColumnsFrom<Data.UserDataField>()
                .Where(Data.UserDataField.IdColumn).IsNotEqualTo(userDataFieldId)
                .And(Data.UserDataField.CustomerIDColumn).In(new int[] { udf.CustomerID, 0 })
                .And(Data.UserDataField.CodeNameColumn).IsEqualTo(udf.CodeName)
                .ExecuteSingle<UserDataField>();

            if (existing != null)
            {
                throw new Exception("User data field must have a unique code name.");
            }

            if (userDataField.IsGlobal || userDataField.CustomerID == 0)
            {
                udf.CustomerID = 0;
                if (!UserIsSalesChannelAdmin)
                {
                    throw new Exception("You do not have permission to edit global profile fields.");
                }
            }

            udf.Save();

            userDataField = Select.AllColumnsFrom<Data.UserDataField>().Where(Data.UserDataField.IdColumn).IsEqualTo(udf.Id).ExecuteSingle<UserDataField>();

            return new SingleResult<UserDataField>(userDataField);
        }

        public SimpleSingleResult<bool> DeleteUserDataField(int userDataFieldId)
        {
            new Delete().From<Data.UserDataField>().Where(Data.UserDataField.IdColumn).IsEqualTo(userDataFieldId).Execute();

            return new SimpleSingleResult<bool>(true);
        }

        public MultipleResult<Author> FindAuthours(int customerId, int currentUserId, string searchText, int start, int limit, string sort, string dir, string filter)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.User>()
                .Where(Data.User.Columns.IsAuthor).IsEqualTo(true)
                .And(Data.FastSearchableUser.Columns.Search).ContainsString(searchText);

            return new MultipleResult<Author>(query);
        }

        public MultipleResult<UserReportingPermission> GetUserReportingPermissions(int customerId, int currentUserId, int targetUserId, int permissionTypeId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.UserReportingPermission>()
                .IncludeColumn(Data.UserReportingPermissionsType.Columns.Name, "permissionType")
                .InnerJoin(Data.Tables.UserReportingPermissionsType, Data.UserReportingPermissionsType.Columns.Id, Data.Tables.UserReportingPermission, Data.UserReportingPermission.Columns.PermissionTypeId)
                .Where(Data.UserReportingPermission.UserIdColumn).IsEqualTo(targetUserId)
                .And(Data.UserReportingPermission.PermissionTypeIdColumn).IsEqualTo(permissionTypeId);

            return new MultipleResult<UserReportingPermission>(query);

        }

        public SingleResult<User> GetCustomerCareRep()
        {
            Data.Customer customer = new Data.Customer(this.CustomerID);
            if (customer.CustomerCareRep != null)
            {
                User user = new Select(Data.User.Columns.Id)
                    .From<Data.User>()
                    .Where(Data.User.Columns.Id).IsEqualTo(customer.CustomerCareRep.Value)
                    .ExecuteSingle<User>();

                if (user != null)
                {
                    return GetUser(this.CustomerID, this.UserID, user.ID, true);
                }
            }
            return null;
        }

        public SingleResult<User> GetAccountExecutive()
        {
            Data.Customer customer = new Data.Customer(this.CustomerID);
            if (customer.AccountExecutive != null)
            {
                User user = Select.AllColumnsFrom<Data.User>().Where(Data.User.Columns.Id).IsEqualTo(customer.AccountExecutive.Value)
                    .ExecuteSingle<User>();
                return new SingleResult<User>(user);
            }
            return null;
        }

        public SingleResult<JobRole> GetJobRole(int customerId, int currentUserId, int jobRoleId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.JobRole>()
                .Where(Data.JobRole.Columns.Id).IsEqualTo(jobRoleId)
                .And(Data.JobRole.Columns.CustomerID).IsEqualTo(customerId);

            JobRole jobRole = query.ExecuteSingle<JobRole>();

            return new SingleResult<JobRole>(jobRole);
        }

        public SingleResult<Location> GetLocation(int customerId, int currentUserId, int locationId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Location>()
                .Where(Data.Location.Columns.Id).IsEqualTo(locationId)
                .And(Data.Location.Columns.CustomerID).IsEqualTo(customerId);

            Location location = query.ExecuteSingle<Location>();

            return new SingleResult<Location>(location);
        }

        public SingleResult<Audience> GetAudience(int customerId, int currentUserId, int audienceId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Audience>()
                .Where(Data.Audience.Columns.Id).IsEqualTo(audienceId)
                .And(Data.Audience.Columns.CustomerID).IsEqualTo(customerId);

            Audience audience = query.ExecuteSingle<Audience>();

            return new SingleResult<Audience>(audience);
        }

        public SingleResult<bool> DeleteJobRole(int customerId, int currentUserId, int jobRoleId)
        {
            Data.JobRole jr = new Data.JobRole(jobRoleId);
            if (!UserIsSalesChannelAdmin && jr.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to delete the specified job role.");
            }
            if (jr.Users.Count > 0)
            {
                throw new Exception("You must remove all users before this item can be deleted.");
            }
            if (jr.ChildJobRoles.Count > 0)
            {
                throw new Exception("You must remove all sub-items before this item can be deleted.");
            }
            Data.JobRole.Destroy(jobRoleId);
            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteLocation(int customerId, int currentUserId, int locationId)
        {
            Data.Location location = new Data.Location(locationId);
            if (!UserIsSalesChannelAdmin && location.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to delete the specified location.");
            }
            if (location.Users.Count > 0)
            {
                throw new Exception("You must remove all users before this item can be deleted.");
            }
            if (location.ChildLocations.Count > 0)
            {
                throw new Exception("You must remove all sub-items before this item can be deleted.");
            }
            Data.Location.Destroy(locationId);
            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteAudience(int customerId, int currentUserId, int audienceId)
        {
            Data.Audience audience = new Data.Audience(audienceId);
            if (!UserIsSalesChannelAdmin && audience.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to delete the specified audience.");
            }
            int userCount = Select.AllColumnsFrom<Data.UserAudience>()
                .Where(Data.UserAudience.Columns.AudienceID).IsEqualTo(audienceId)
                .GetRecordCount();
            if (userCount > 0)
            {
                throw new Exception("You must remove all users before this item can be deleted.");
            }
            if (audience.ChildAudiences.Count > 0)
            {
                throw new Exception("You must remove all sub-items before this item can be deleted.");
            }
            Data.Audience.Destroy(audienceId);
            return new SingleResult<bool>(true);
        }

        public SingleResult<User> SaveUser(int customerId, int userId, int targetUserId, User user)
        {
            UserController u = new UserController();
            user.CustomerID = customerId;
            user.ID = targetUserId;

            SingleResult<User> userResult = new SingleResult<User>(u.Save(user));

            if (userResult.Data.IsJanrainPasswordFail)
            {
                userResult.Notice = "User details were saved, but the password could not be updated in Janrain.";
            }

            return userResult;
        }

        public SingleResult<User> SaveUser(int customerId, int userId, int targetUserId, User user, bool forceAccountCreation)
        {
            UserController u = new UserController();
            user.CustomerID = customerId;
            user.ID = targetUserId;

            SingleResult<User> userResult = new SingleResult<User>(u.Save(user, forceAccountCreation));

            if (userResult.Data.IsJanrainPasswordFail)
            {
                userResult.Notice = "User details were saved, but the password could not be updated in Janrain.";
            }

            return userResult;
        }

        public SingleResult<User> SaveUser(int customerId, int userId, int targetUserId, UserSecurity userSecurity)
        {
            User user = new User();
            user.CopyFrom(userSecurity);

            DateTime parsedDob;
            DateTime.TryParse(userSecurity.DOB, out parsedDob);

            if (parsedDob != DateTime.MinValue)
            {
                user.DateOfBirth = parsedDob;
            }

            if (!string.IsNullOrEmpty(userSecurity.SSN))
            {
                user.SocialSecurityNumber = userSecurity.SSN;
            }

            SingleResult<User> userResult = SaveUser(customerId, userId, targetUserId, user);

            user.ID = userResult.Data.ID;

            (new UserController()).UpdateUserSecurity(user.ID, userSecurity);

            return userResult;
        }

        public SingleResult<SalesChannel> SaveSalesChannel(int customerId, int userId, int salesChannelId, SalesChannel salesChannel, bool reparent)
        {
            Data.SalesChannel data = new Symphony.Core.Data.SalesChannel(salesChannelId);
            if (reparent)
            {
                data.ParentSalesChannelID = salesChannel.ParentID;
            }
            else
            {
                salesChannel.CopyTo(data);
                data.Id = salesChannelId;
            }
            data.Save(Username);

            return new SingleResult<SalesChannel>(Model.Create<SalesChannel>(data));
        }

        public SingleResult<Customer> SaveCustomer(int CustomerID, int UserID, int targetCustomerId, Customer customer)
        {
            Data.Customer existing = new Symphony.Core.Data.Customer(Data.Customer.Columns.SubDomain, customer.SubDomain);
            if (existing.Id > 0 && existing.Id != targetCustomerId)
            {
                throw new Exception("The specified subdomain is already in use.");
            }
            if (targetCustomerId > 0 && existing.Id == 0)
            {
                throw new Exception("You cannot change the subdomain once it has been set.");
            }

            if (SubdomainBlacklist.Contains(customer.SubDomain))
            {
                throw new Exception("The subdomain " + customer.SubDomain + " is a reserved name.");
            }

            Data.Customer data = new Symphony.Core.Data.Customer(targetCustomerId);

            customer.CopyTo(data);
            data.Id = targetCustomerId;
            if (customer.EvaluationEndDate == null)
            {
                data.EvaluationEndDate = DefaultDateTime;
            }

            data.UserNameXAlias = customer.UsernameAlias;

            data.Save(Username);

            List<ExternalSystem> requiredSystems = Select.AllColumnsFrom<Data.ExternalSystem>()
                .Where(Data.ExternalSystem.IsRequiredColumn).IsEqualTo(true)
                .ExecuteTypedList<ExternalSystem>();

            if (customer.ExternalSystems == null)
            {
                customer.ExternalSystems = new List<ExternalSystem>();
            }

            customer.ExternalSystems = customer.ExternalSystems.Union(requiredSystems, new ExternalSystemCompareer()).ToList();

            if (customer.ExternalSystems != null)
            {
                Data.CustomerExternalSystem.Delete(Data.CustomerExternalSystem.CustomerIDColumn.ColumnName, data.Id);
                foreach (ExternalSystem es in customer.ExternalSystems)
                {
                    Data.CustomerExternalSystem ces = new Data.CustomerExternalSystem();
                    ces.CustomerID = data.Id;
                    ces.ExternalSystemID = es.Id;
                    ces.Save();
                }
            }

            if (customer.PortalSettings != null)
            {
                Data.PortalSetting.Destroy(Data.PortalSetting.Columns.CustomerId, data.Id);
                foreach (PortalSetting ps in customer.PortalSettings)
                {
                    Data.PortalSetting psData = new Data.PortalSetting();

                    ps.CopyTo(psData);
                    psData.CustomerId = data.Id;

                    psData.Save(Username);
                }
            }

            if (targetCustomerId == 0)
            {
                // add default categories
                Data.Category cc = new Symphony.Core.Data.Category();
                cc.CustomerID = data.Id;
                cc.Name = "Default";
                cc.Description = "Default Category";
                cc.Save(Username);

                Data.PublicDocumentCategory pdc = new Symphony.Core.Data.PublicDocumentCategory();
                pdc.CustomerID = data.Id;
                pdc.Name = "Default";
                pdc.Description = "Default Category";
                pdc.Save(Username);

                Data.SPs.AspnetApplicationsCreateApplication(customer.SubDomain, Guid.NewGuid()).Execute();

                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ArtisanAuthor).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ArtisanPackager).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ArtisanViewer).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ClassroomInstructor).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ClassroomManager).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ClassroomResourceManager).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ClassroomSupervisor).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.CourseAssignmentAdministrator).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.CourseAssignmentTrainingManager).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.CustomerAdministrator).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.CustomerManager).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.CustomerUser).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.GTMOrganizer).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.GTWOrganizer).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ReportingAnalyst).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ReportingDeveloper).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.CustomerLocationManager).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.CustomerJobRoleManager).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ReportingLocationAnalyst).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ReportingJobRoleAnalyst).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ReportingSupervisor).Execute();
                Data.SPs.AspnetRolesCreateRole(customer.SubDomain, Roles.ReportingAudienceAnalyst).Execute();
            }

            return new SingleResult<Customer>(Model.Create<Customer>(data));
        }

        public SingleResult<CustomerSettings> SaveCustomerSettings(int CustomerID, int UserID, int targetCustomerId, CustomerSettings settings)
        {
            if (CustomerID != targetCustomerId)
            {
                throw new Exception("You don't have permission to set this customer's settings.");
            }

            Data.Customer data = new Symphony.Core.Data.Customer(targetCustomerId);

            if (HasRole(Roles.CourseAssignmentAdministrator) || HasRole(Roles.CourseAssignmentTrainingManager))
            {
                data.ShowPretestOverride = settings.ShowPretestOverride;
                data.ShowPostTestOverride = settings.ShowPostTestOverride;
                data.PassingScoreOverride = settings.PassingScoreOverride;
                data.SkinOverride = settings.SkinOverride;
                data.PreTestTypeOverride = settings.PretestTypeOverride;
                data.PostTestTypeOverride = settings.PosttestTypeOverride;
                data.NavigationMethodOverride = settings.NavigationMethodOverride;
                data.CertificateEnabledOverride = settings.CertificateEnabledOverride;
                data.DisclaimerOverride = settings.DisclaimerOverride;
                data.ContactEmailOverride = settings.ContactEmailOverride;
                data.ShowObjectivesOverride = settings.ShowObjectivesOverride;
                data.PretestTestOutOverride = settings.PretestTestOutOverride;
                data.RetestMode = settings.RetestMode;
                data.Retries = settings.Retries;
                data.RandomizeAnswersOverride = settings.RandomizeAnswersOverride;
                data.RandomizeQuestionsOverride = settings.RandomizeQuestionsOverride;
                data.ReviewMethodOverride = settings.ReviewMethodOverride;
                data.MarkingMethodOverride = settings.MarkingMethodOverride;
                data.InactivityMethodOverride = settings.InactivityMethodOverride;
                data.InactivityTimeoutOverride = settings.InactivityTimeoutOverride;
                data.IsForceLogoutOverride = settings.IsForceLogoutOverride;
                data.MinimumPageTimeOverride = settings.MinimumPageTimeOverride;
                data.MaximumQuestionTimeOverride = settings.MaximumQuestionTimeOverride;
                data.MaxTimeOverride = settings.MaxTimeOverride;
                data.MinTimeOverride = settings.MinTimeOverride;
                data.MinLoTimeOverride = settings.MinLoTimeOverride;
                data.MinScoTimeOverride = settings.MinScoTimeOverride;
                data.ThemeFlavorOverrideID = settings.ThemeFlavorOverrideID;
            }

            data.Save(Username);

            return new SingleResult<CustomerSettings>(Model.Create<CustomerSettings>(data));
        }

        public SingleResult<Customer> GetCustomer(int CustomerID, int UserID, int targetCustomerId)
        {
            Customer customer = Select.AllColumnsFrom<Data.CustomerDetail>()
                .Where(Data.CustomerDetail.Columns.Id).IsEqualTo(targetCustomerId)
                .ExecuteSingle<Customer>();

            if (customer != null && customer.ID > 0)
            {
                customer.ExternalSystems = Select.AllColumnsFrom<Data.ExternalSystem>()
                    .InnerJoin(Data.CustomerExternalSystem.ExternalSystemIDColumn, Data.ExternalSystem.IdColumn)
                    .Where(Data.CustomerExternalSystem.CustomerIDColumn).IsEqualTo(targetCustomerId)
                    .ExecuteTypedList<ExternalSystem>();

                Data.Customer dataCustomer = new Data.Customer(customer.ID);

                customer.AssociatedImageData = dataCustomer.AssociatedImageData;

                // adding Locked out params here as it is the path of least impact
                customer.LockedOutMessage = dataCustomer.LockedOutMessage;
                customer.MaxSignInAttempts = dataCustomer.MaxSignInAttempts;
                customer.LockedOutPeriodInMinutes = dataCustomer.LockedOutPeriodInMinutes;

                customer.PortalSettings = (new PortalController()).GetListPortalSetting(customer.ID).Data;

                if (customer.ThemeID > 0)
                {
                    customer.Theme = Select.AllColumnsFrom<Data.Theme>().Where(Data.Theme.IdColumn).IsEqualTo(customer.ThemeID).ExecuteSingle<Models.Theme>();
                }
            }

            return new SingleResult<Customer>(customer);
        }

        public SingleResult<Customer> GetCustomer(string subdomain)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Customer>()
                .Where(Data.Customer.SubDomainColumn).IsEqualTo(subdomain);

            return new SingleResult<Customer>(query);
        }

        public SingleResult<SalesChannel> GetSalesChannel(string salesChannelCode)
        {
            //Data.SalesChannel salesChannel = new SalesChannel("code", sc);
            SqlQuery query = Select.AllColumnsFrom<Data.SalesChannel>()
                .Where(Data.SalesChannel.Columns.Code).IsEqualTo(salesChannelCode);

            return new SingleResult<SalesChannel>(query);

        }

        public List<string> GetListOfCustomers(string salesChannelCode)
        {
            var q = new Select(Data.Customer.Columns.Name).From<Data.Customer>()
                            .InnerJoin(Data.Tables.SalesChannel, Data.SalesChannel.Columns.Id, Data.Tables.Customer, Data.Customer.Columns.SalesChannelID)
                            .Where(Data.SalesChannel.CodeColumn).IsEqualTo(salesChannelCode)
                            .And(Data.SalesChannel.IsLoginEnabledColumn).IsEqualTo(true);

            List<string> listCustomers = q.ExecuteTypedList<Data.Customer>().Select(cd => cd.Name).ToList<string>();
            return listCustomers;
        }

        public SingleResult<CustomerSettings> GetCustomerSettings(int CustomerID, int UserID, int targetCustomerId)
        {
            if (CustomerID != targetCustomerId)
            {
                throw new Exception("You don't have permission to get this customer's settings.");
            }

            SqlQuery query = new Select(
                    Data.Customer.ShowPretestOverrideColumn.QualifiedName,
                    Data.Customer.ShowPostTestOverrideColumn.QualifiedName,
                    Data.Customer.ShowObjectivesOverrideColumn.QualifiedName,
                    Data.Customer.PretestTestOutOverrideColumn.QualifiedName,
                    Data.Customer.PassingScoreOverrideColumn.QualifiedName,
                    Data.Customer.NavigationMethodOverrideColumn.QualifiedName,
                    Data.Customer.CertificateEnabledOverrideColumn.QualifiedName,
                    Data.Customer.DisclaimerOverrideColumn.QualifiedName,
                    Data.Customer.ContactEmailOverrideColumn.QualifiedName,
                    Data.Customer.PreTestTypeOverrideColumn.QualifiedName,
                    Data.Customer.PostTestTypeOverrideColumn.QualifiedName,
                    Data.Customer.SkinOverrideColumn.QualifiedName,
                    Data.Customer.RetestModeColumn.QualifiedName,
                    Data.Customer.RetriesColumn.QualifiedName,
                    Data.Customer.RandomizeAnswersOverrideColumn.QualifiedName,
                    Data.Customer.RandomizeQuestionsOverrideColumn.QualifiedName,
                    Data.Customer.ReviewMethodOverrideColumn.QualifiedName,
                    Data.Customer.MarkingMethodOverrideColumn.QualifiedName,
                    Data.Customer.InactivityTimeoutOverrideColumn.QualifiedName,
                    Data.Customer.InactivityMethodOverrideColumn.QualifiedName,
                    Data.Customer.IsForceLogoutOverrideColumn.QualifiedName,
                    Data.Customer.MinimumPageTimeOverrideColumn.QualifiedName,
                    Data.Customer.MaximumQuestionTimeOverrideColumn.QualifiedName,
                    Data.Customer.MaxTimeOverrideColumn.QualifiedName,
                    Data.Customer.MinTimeOverrideColumn.QualifiedName,
                    Data.Customer.MinLoTimeOverrideColumn.QualifiedName,
                    Data.Customer.MinScoTimeOverrideColumn.QualifiedName,
                    Data.Customer.ThemeFlavorOverrideIDColumn.QualifiedName
                )
                .From(Data.Customer.Schema.TableName)
                .Where(Data.Customer.Columns.Id).IsEqualTo(targetCustomerId);

            var result = new SingleResult<CustomerSettings>(query);
            if (result.Data != null)
            {
                Models.ArtisanTheme theme = Select.AllColumnsFrom<Data.ArtisanTheme>()
                    .Where(Data.ArtisanTheme.Columns.Id).IsEqualTo(result.Data.SkinOverride)
                    .ExecuteSingle<ArtisanTheme>();
                result.Data.Theme = theme;

                Models.Theme themeFlavor = Select.AllColumnsFrom<Data.Theme>()
                    .Where(Data.Theme.Columns.Id).IsEqualTo(result.Data.ThemeFlavorOverrideID)
                    .ExecuteSingle<Models.Theme>();
                result.Data.ThemeFlavor = themeFlavor;
            }

            return result;
        }

        public SingleResult<User> GetAdministrator(int customerId, int userId, int targetUserId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.User>()
                .Where(Data.User.Columns.Id).IsEqualTo(targetUserId);

            return new SingleResult<User>(query);
        }

        public SingleResult<User> SaveAdministrator(int customerId, int userId, int targetUserId, User administrator)
        {
            UserController u = new UserController();
            administrator.CustomerID = customerId;
            administrator.ID = targetUserId;
            administrator.MiddleName = string.Empty;
            administrator.EmployeeNumber = Guid.NewGuid().ToString(); // we don't care
            administrator.Notes = string.Empty;
            return new SingleResult<User>(u.Save(administrator));
        }

        public SingleResult<JobRole> SaveJobRole(int customerId, int userId, int jobRoleId, bool justParent, JobRole jobRole)
        {
            Data.JobRole data = new Symphony.Core.Data.JobRole(jobRoleId);
            if (justParent)
            {
                data.ParentJobRoleID = jobRole.ParentId;
            }
            else
            {
                jobRole.CopyTo(data);
                data.CustomerID = customerId;
                data.Id = jobRoleId;
            }
            data.Save(Username);
            return new SingleResult<JobRole>(Model.Create<JobRole>(data));
        }

        public SingleResult<Location> SaveLocation(int customerId, int userId, int locationId, bool justParent, Location location)
        {
            Data.Location data = new Symphony.Core.Data.Location(locationId);
            if (justParent)
            {
                data.ParentLocationID = location.ParentId;
            }
            else
            {
                location.CopyTo(data);
                data.CustomerID = customerId;
                data.Id = locationId;
            }
            data.Save(Username);
            return new SingleResult<Location>(Model.Create<Location>(data));
        }

        public SingleResult<Audience> SaveAudience(int customerId, int userId, int audienceId, bool justParent, Audience audience)
        {
            Data.Audience data = new Symphony.Core.Data.Audience(audienceId);
            if (justParent)
            {
                data.ParentAudienceID = audience.ParentID;
            }
            else
            {
                audience.CopyTo(data);
                data.CustomerID = customerId;
                data.Id = audienceId;
            }
            data.Save(Username);
            return new SingleResult<Audience>(Model.Create<Audience>(data));
        }

        public SingleResult<User> ToggleCustomerCare(int customerId, int userId, int targetUserId)
        {
            Data.User u = new Symphony.Core.Data.User(targetUserId);
            u.IsCustomerCare = !u.IsCustomerCare;
            u.Save(Username);

            return new SingleResult<User>(Model.Create<User>(u));
        }

        public SingleResult<User> ToggleAccountExec(int customerId, int userId, int targetUserId)
        {
            Data.User u = new Symphony.Core.Data.User(targetUserId);
            u.IsAccountExec = !u.IsAccountExec;
            u.Save(Username);

            return new SingleResult<User>(Model.Create<User>(u));
        }

        public void BeginImport(int importType, int customerId, int userId, byte[] fileContents, bool execute)
        {
            try
            {
                // get some settings for the command line
                ImportType t = (ImportType)Enum.Parse(typeof(ImportType), importType.ToString());
                string filename = string.Empty;
                string typeString = string.Empty;
                switch (t)
                {
                    case ImportType.Audience:
                        filename = AudienceFileName;
                        typeString = "A";
                        break;
                    case ImportType.JobRole:
                        filename = JobFileName;
                        typeString = "J";
                        break;
                    case ImportType.Location:
                        filename = LocationFileName;
                        typeString = "L";
                        break;
                    case ImportType.User:
                        filename = EmployeeFileName;
                        typeString = "E";
                        break;
                    case ImportType.Order:
                        filename = OrderFileName;
                        typeString = "O";
                        break;
                }

                Data.User user = new Data.User(userId);

                // create the path, write out the file
                string directory = Path.Combine(Path.Combine(ImportUploadFolder, customerId.ToString()), DateTime.Now.ToString("yyyy-MM-dd_hh-mm-ss"));
                string path = Path.Combine(directory, filename);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                File.WriteAllBytes(path, fileContents);

                // shell out the process
                Process p = new Process();
                p.StartInfo = new ProcessStartInfo()
                {
                    FileName = "\"" + ImportCommandLine + "\"",
                    UseShellExecute = false,
                    WorkingDirectory = HttpContext.Current.Server.MapPath("~/"),
                    Arguments = string.Format("-C {0} -D \"{1}\" -{2} " + (execute ? "-X " : "") + "-Q -1 -U {3} -Sessions", customerId, directory, typeString, user.Username)
                };
                Log.DebugFormat("Starting {0} with arguments {1}...", p.StartInfo.FileName, p.StartInfo.Arguments);

                bool result = p.Start();

                Log.Debug("Start result: " + result);

            }
            catch (Exception ex)
            {
                Log.Error(ex);
                throw ex;
            }
        }

        public Stream GetImportResults(int CustomerID, int UserID, int importId, ref string name)
        {
            name = "export_results_" + importId + ".csv";

            var job = new Data.ImportJobQueue(importId);
            if (!File.Exists(job.OutputFile))
            {
                return null;
            }

            byte[] bytes = File.ReadAllBytes(job.OutputFile);
            return new MemoryStream(bytes);
        }

        public Stream Export(int customerID, int userID, int exportType, ref string name)
        {
            ImportType t = (ImportType)Enum.Parse(typeof(ImportType), exportType.ToString());
            Data.Customer customer = new Data.Customer(customerID);
            SqlQuery query = null;
            List<IExportable> results = null;
            string header = string.Empty;
            switch (t)
            {
                case ImportType.Audience:
                    name = customer.AudienceAlias;
                    query = Select.AllColumnsFrom<Data.Audience>().Where(Data.Audience.CustomerIDColumn).IsEqualTo(customerID);
                    results = query.ExecuteTypedList<Data.Audience>().OfType<IExportable>().ToList();
                    header = Data.Audience.ExportHeader();
                    break;
                case ImportType.JobRole:
                    name = customer.JobRoleAlias;
                    query = Select.AllColumnsFrom<Data.JobRole>().Where(Data.JobRole.CustomerIDColumn).IsEqualTo(customerID);
                    results = query.ExecuteTypedList<Data.JobRole>().OfType<IExportable>().ToList();
                    header = Data.JobRole.ExportHeader();
                    break;
                case ImportType.Location:
                    name = customer.LocationAlias;
                    query = Select.AllColumnsFrom<Data.Location>().Where(Data.Location.CustomerIDColumn).IsEqualTo(customerID);
                    results = query.ExecuteTypedList<Data.Location>().OfType<IExportable>().ToList();
                    header = Data.Location.ExportHeader();
                    break;
                case ImportType.User:
                    name = "User";
                    query = Select.AllColumnsFrom<Data.FastSearchableUser>().Where(Data.FastSearchableUser.Columns.CustomerID).IsEqualTo(customerID)
                        .And(Data.FastSearchableUser.Columns.SalesChannelID).IsEqualTo(0);
                    results = query.ExecuteTypedList<Data.FastSearchableUser>().OfType<IExportable>().ToList();
                    header = Data.FastSearchableUser.ExportHeader();
                    break;
            }
            name = Strings.SingularToPlural(name) + ".csv";

            // create the csv
            StringBuilder sb = new StringBuilder();
            sb.Append(header + Environment.NewLine);
            foreach (IExportable result in results)
            {
                sb.Append(result.Export());
                sb.Append(Environment.NewLine);
            }

            byte[] bytes = Encoding.Unicode.GetBytes(sb.ToString());

            return new MemoryStream(bytes);
        }

        public PagedResult<GTMOrganizer> FindGTMOrganizers(int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.GTMOrganizer>()
                .And(Data.GTMOrganizer.Columns.Email).ContainsString(searchText);

            return new PagedResult<GTMOrganizer>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<GTMOrganizer> GetGTMOrganizer(int currentUserId, int gtmOrganizerId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.GTMOrganizer>()
                .Where(Data.GTMOrganizer.Columns.Id).IsEqualTo(gtmOrganizerId);

            GTMOrganizer gtmOrganizer = query.ExecuteSingle<GTMOrganizer>();

            return new SingleResult<GTMOrganizer>(gtmOrganizer);
        }

        public SingleResult<bool> DeleteGTMOrganizer(int gtmOrganizerId)
        {
            Data.GTMOrganizer o = new Data.GTMOrganizer(gtmOrganizerId);
            if (!UserIsSalesChannelAdmin)
            {
                throw new Exception("You don't have permission to delete the specified GTM Organizer.");
            }
            Data.GTMOrganizer.Destroy(gtmOrganizerId);
            return new SingleResult<bool>(true);
        }

        public SingleResult<GTMOrganizer> SaveGTMOrganizer(int userId, int gtmOrganizerId, GTMOrganizer gtmOrganizer)
        {
            Data.GTMOrganizer data = new Data.GTMOrganizer(gtmOrganizerId);
            gtmOrganizer.CopyTo(data);
            data.Id = gtmOrganizerId;
            data.Save(Username);
            return new SingleResult<GTMOrganizer>(Model.Create<GTMOrganizer>(data));
        }

        public string SaveCustomerCertificate(int customerId, HttpPostedFile file)
        {
            if (file == null || file.ContentLength == 0)
            {
                throw new Exception("Uploaded certificate file is empty.");
            }
            else if (String.IsNullOrEmpty(file.FileName))
            {
                throw new Exception("Uploaded certificate file name is empty.");
            }

            byte[] fileData = new byte[file.ContentLength];
            file.InputStream.Read(fileData, 0, file.ContentLength);

            // Save the file in the customer table
            Data.Customer customer = new Symphony.Core.Data.Customer(customerId);
            customer.CertificateFile = fileData;
            customer.CertificateFileName = file.FileName;
            customer.Save(Username);

            return file.FileName;
        }

        public Data.Customer GetCustomerByUrl(string identityProviderUrl)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Customer>()
                .Where(Data.Customer.Columns.IdentityProviderURL).IsEqualTo(identityProviderUrl);

            Data.Customer customer = query.ExecuteSingle<Data.Customer>();

            return customer;
        }

        public PagedResult<ExternalSystem> GetExternalSystems(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ExternalSystem>()
                .Where(Data.ExternalSystem.SystemCodeNameColumn).ContainsString(searchText)
                .Or(Data.ExternalSystem.SystemNameColumn).ContainsString(searchText);

            return new PagedResult<ExternalSystem>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        #region Tiles/Applications

        public PagedResult<Application> FindApplications(int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Application>()
                .And(Data.Application.Columns.Name).ContainsString(searchText)
                .Or(Data.Application.Columns.AuthenticationUri).ContainsString(searchText);

            return new PagedResult<Application>(query, pageIndex, pageSize, Data.Application.Columns.Name, OrderDirection.Ascending);
        }


        public SingleResult<Application> GetApplication(int currentUserId, int applicationId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Application>()
                .Where(Data.Application.Columns.Id).IsEqualTo(applicationId);

            Application application = query.ExecuteSingle<Application>();

            return new SingleResult<Application>(application);
        }
        public SingleResult<Application> SaveApplication(int userId, int applicationId, Application jobRole)
        {
            Data.Application data = new Symphony.Core.Data.Application(applicationId);
            jobRole.CopyTo(data);
            data.Id = applicationId;
            data.Save(Username);
            return new SingleResult<Application>(Model.Create<Application>(data));
        }

        public SingleResult<bool> DeleteApplication(int applicationId)
        {
            Data.SPs.DeleteApplication(applicationId).Execute();
            return new SingleResult<bool>(true);
        }

        //Tiles

        public PagedResult<Tile> FindTiles(int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.TileApplication>()
                .And(Data.TileApplication.Columns.Name).ContainsString(searchText)
                .Or(Data.TileApplication.Columns.ApplicationName).ContainsString(searchText)
                .Or(Data.TileApplication.Columns.TileUri).ContainsString(searchText);

            return new PagedResult<Tile>(query, pageIndex, pageSize, Data.Tile.Columns.Name, OrderDirection.Ascending);
        }


        public SingleResult<Tile> GetTile(int currentUserId, int TileId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.TileApplication>()
                .Where(Data.Tile.Columns.Id).IsEqualTo(TileId);

            Tile Tile = query.ExecuteSingle<Tile>();

            return new SingleResult<Tile>(Tile);
        }
        public SingleResult<Tile> SaveTile(int userId, int TileId, Tile jobRole)
        {
            Data.Tile data = new Symphony.Core.Data.Tile(TileId);
            jobRole.CopyTo(data);
            data.Id = TileId;
            data.Save(Username);
            return new SingleResult<Tile>(Model.Create<Tile>(data));
        }

        public SingleResult<bool> DeleteTile(int tileId)
        {
            Data.SPs.DeleteTile(tileId).Execute();
            return new SingleResult<bool>(true);
        }


        public PagedResult<Tile> FindTilesForAudience(int customerId, int audienceId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            return FindTilesForHierarchyNode(customerId, userId, searchText, orderBy, orderDir, pageIndex, pageSize, HierarchyType.Audience, audienceId);
        }

        public PagedResult<Tile> FindTilesForJobRole(int customerId, int jobRoleId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            return FindTilesForHierarchyNode(customerId, userId, searchText, orderBy, orderDir, pageIndex, pageSize, HierarchyType.JobRole, jobRoleId);
        }

        public PagedResult<Tile> FindTilesForLocation(int customerId, int locationId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            return FindTilesForHierarchyNode(customerId, userId, searchText, orderBy, orderDir, pageIndex, pageSize, HierarchyType.Location, locationId);
        }

        private PagedResult<Tile> FindTilesForHierarchyNode(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, HierarchyType hierarchyType, int hierarchyNodeId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.TileApplication>()
                .InnerJoin(Data.HierarchyToTileLink.Schema.TableName, Data.HierarchyToTileLink.Columns.TileID, Data.TileApplication.Schema.TableName, Data.TileApplication.Columns.Id)
                .And(Data.HierarchyToTileLink.Columns.HierarchyNodeID).IsEqualTo(hierarchyNodeId)
                .And(Data.HierarchyToTileLink.Columns.HierarchyTypeID).IsEqualTo(hierarchyType);

            if (!string.IsNullOrEmpty(searchText))
            {
                query.And(Data.Tile.Columns.Name).ContainsString(searchText);
            }
            return new PagedResult<Tile>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<Tile> FindUnassignedTiles(HierarchyType hierarchyType, int hierarchyId, int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Tile>()
                .Where(Data.Tile.IdColumn).NotIn(new Select(Data.HierarchyToTileLink.Columns.TileID)
                    .From<Data.HierarchyToTileLink>()
                    .Where(Data.HierarchyToTileLink.Columns.HierarchyNodeID).IsEqualTo(hierarchyId)
                    .And(Data.HierarchyToTileLink.Columns.HierarchyTypeID).IsEqualTo(hierarchyType)
                );

            return new PagedResult<Tile>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<bool> SaveLocationTiles(int customerId, int userId, int locationId, int[] tileIds)
        {
            return SaveTilesFor(customerId, userId, HierarchyType.Location, locationId, tileIds);
        }

        public SingleResult<bool> SaveJobRoleTiles(int customerId, int userId, int jobRoleId, int[] tileIds)
        {
            return SaveTilesFor(customerId, userId, HierarchyType.JobRole, jobRoleId, tileIds);
        }

        public SingleResult<bool> SaveAudienceTiles(int customerId, int userId, int audienceId, int[] tileIds)
        {
            return SaveTilesFor(customerId, userId, HierarchyType.Audience, audienceId, tileIds);
        }

        public SingleResult<bool> SaveUserTiles(int customerId, int userId, int userNodeId, int[] tileIds)
        {
            return SaveTilesFor(customerId, userId, HierarchyType.User, userNodeId, tileIds);
        }


        public SingleResult<bool> SaveTilesFor(int customerId, int userId, HierarchyType hierarchyType, int nodeId, int[] tileIds)
        {
            Data.HierarchyToTileLinkCollection links = new Symphony.Core.Data.HierarchyToTileLinkCollection();
            foreach (int id in tileIds)
            {
                Data.HierarchyToTileLink link = new Symphony.Core.Data.HierarchyToTileLink();
                link.TileID = id;
                link.HierarchyNodeID = nodeId;
                link.HierarchyTypeID = (int)hierarchyType;
                links.Add(link);
            }
            links.SaveAll(Username);

            return new SingleResult<bool>(true);
        }

        public SingleResult<bool> DeleteLocationTiles(int customerId, int userId, int locationId, int[] tileIds)
        {
            return DeleteHierarchyTiles(customerId, userId, HierarchyType.Location, locationId, tileIds);
        }

        public SingleResult<bool> DeleteJobRoleTiles(int customerId, int userId, int jobRoleId, int[] tileIds)
        {
            return DeleteHierarchyTiles(customerId, userId, HierarchyType.JobRole, jobRoleId, tileIds);
        }

        public SingleResult<bool> DeleteAudienceTiles(int customerId, int userId, int audienceId, int[] tileIds)
        {
            return DeleteHierarchyTiles(customerId, userId, HierarchyType.Audience, audienceId, tileIds);
        }
        public SingleResult<bool> DeleteUserTiles(int customerId, int userId, int userNodeId, int[] tileIds)
        {
            return DeleteHierarchyTiles(customerId, userId, HierarchyType.User, userNodeId, tileIds);
        }

        public SingleResult<bool> DeleteHierarchyTiles(int customerId, int userId, HierarchyType hierarchyType, int hierarchyId, int[] tileIds)
        {
            // get all the users that are mapped to the specified audience
            Data.HierarchyToTileLinkCollection links = new Symphony.Core.Data.HierarchyToTileLinkCollection()
                .Where(Data.HierarchyToTileLink.Columns.HierarchyNodeID, hierarchyId)
                .Where(Data.HierarchyToTileLink.Columns.HierarchyTypeID, hierarchyType)
                .Load();

            // any of those that match the specified tile ids as well are trashed
            foreach (Data.HierarchyToTileLink link in links)
            {
                if (tileIds.Contains(link.TileID))
                {
                    Data.HierarchyToTileLink.Destroy(Data.HierarchyToTileLink.Columns.Id, link.Id);
                }
            }

            return new SingleResult<bool>(true);
        }

        public MultipleResult<UserTileAssignment> GetAssignedTiles(int userId, int applicationId)
        {
            return new MultipleResult<UserTileAssignment>(Data.SPs.GetUserTilesForApp(userId, applicationId, 0).ExecuteTypedList<UserTileAssignment>());
        }

        public MultipleResult<UserTileAssignment> GetUnassignedTiles(int userId)
        {
            return new MultipleResult<UserTileAssignment>(Data.SPs.GetUserUnassignedTiles(userId).ExecuteTypedList<UserTileAssignment>());
        }

        public MultipleResult<UserApplicationAssignment> GetAssignedApplications(int userId)
        {
            return new MultipleResult<UserApplicationAssignment>(Data.SPs.GetUserApplications(userId).ExecuteTypedList<UserApplicationAssignment>());
        }

        public SingleResult<Credentials> SaveCredentials(int customerId, int userId, int applicationId, int targetUserId, int credentialId, Credentials credentials)
        {
            Data.Credential data = new Data.Credential(credentialId);
            data.Id = credentialId;
            data.ApplicationID = applicationId;
            data.UserID = targetUserId;

            if (credentials.UserName != null && credentials.UserName.Length > 0)
            {
                data.UserNameX = credentials.UserName;
            }
            if (credentials.Pass != null && credentials.Pass.Length > 0)
            {
                data.Password = credentials.Password;
            }

            data.Save(Username);
            return new SingleResult<Credentials>(Model.Create<Credentials>(data));
        }

        public MultipleResult<AppLink> GetUserAppLinks(int customerId, int userId, int targetUserId)
        {
            List<AppLink> appLinks = new List<AppLink>();
            foreach (UserTileAssignment assign in Data.SPs.GetUserTilesForApp(targetUserId, 0, 0).ExecuteTypedList<UserTileAssignment>())
            {
                AppLink lnk = new AppLink();
                lnk.AppName = assign.ApplicationName;
                lnk.AppInstanceId = assign.ApplicationId;
                lnk.TileID = assign.TileID;
                lnk.Label = assign.Name;
                lnk.TileUri = assign.TileUri;
                if (assign.PassUserNamePass)
                {
                    lnk.LinkUrl = String.Format("/services/customer.svc/applications/appLinks/{0}", assign.ID);
                    lnk.AuthenticationUrl = String.Format("/services/customer.svc/applications/appLinks/{0}", assign.ID);
                }
                else if (assign.Name.ToLower().Contains("mars") || assign.ApplicationName.ToLower().Contains("mars"))
                {
                    Dictionary<string, string> attributes = Attributes;
                    if (attributes != null)
                    {
                        lnk.LinkUrl = string.Format("{0}?tokenid={1}&uid={2}", assign.TileUri, Attributes["MarsToken"], Attributes["MarsUserId"]);
                    }
                    else
                    {
                        Log.Warn("Skipping MARS link because this isn't a MARS customer.");
                        continue;
                    }
                }
                else
                {
                    lnk.LinkUrl = assign.TileUri;
                }

                appLinks.Add(lnk);
            }

            return new MultipleResult<AppLink>(appLinks);
        }

        public MultipleResult<AppLink> GetAllApplinks()
        {
            Data.TileApplicationCollection tileApplications = new Data.TileApplicationCollection().Load();
            List<AppLink> appLinks = new List<AppLink>();

            foreach (var tileApp in tileApplications)
            {
                AppLink lnk = new AppLink();
                lnk.AppName = tileApp.ApplicationName;
                lnk.AppInstanceId = tileApp.ApplicationID;
                lnk.Label = tileApp.Name;
                lnk.LinkUrl = tileApp.TileUri;
                lnk.TileID = tileApp.Id;
                appLinks.Add(lnk);
            }




            return new MultipleResult<AppLink>(appLinks);
        }

        public SingleResult<bool> ResolveAppLink(int tileID)
        {
            /*
             * TODO: Modify this to Resolve All types of Tiles, specifically Mars and Symphony. To do this a few changes are needed:
             * 1) Replace PassuserNamePassword with an Int (or enum) for Authentication Method to allow for None (symphony), Mars and Post auth types
             * 2) refactor user app links to use resolveAppLinks for all link types
             * 3) refactor this method to check the authentication method and redirect accordingly. 
             */

            List<AppLink> appLinks = new List<AppLink>();
            foreach (UserTileAssignment assign in Data.SPs.GetUserTilesForApp(UserID, 0, tileID).ExecuteTypedList<UserTileAssignment>())
            {
                // pass the jsonp parameter through
                string jsonp = HttpContext.Current.Request.QueryString["jsonp"];
                if (!string.IsNullOrEmpty(jsonp))
                {
                    if (assign.TileUri.IndexOf("?") > -1)
                    {
                        assign.TileUri += "&jsonp=" + jsonp;
                    }
                    else
                    {
                        assign.TileUri += "?jsonp=" + jsonp;
                    }
                }

                if (assign.PassUserNamePass)
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(assign.AuthenticationUri);
                    request.AllowAutoRedirect = true;
                    request.MaximumAutomaticRedirections = 3;

                    // allow all ssl certs
                    ServicePointManager.ServerCertificateValidationCallback = (unused, unused2, unused3, unused4) =>
                    {
                        return true;
                    };



                    StringBuilder postData = new StringBuilder();
                    postData.Append("username=" + HttpUtility.UrlEncode(assign.CredentialsUserName) + "&");
                    postData.Append("password=" + HttpUtility.UrlEncode(assign.Pass));

                    ASCIIEncoding ascii = new ASCIIEncoding();
                    byte[] postBytes = ascii.GetBytes(postData.ToString());

                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = postBytes.Length;
                    CookieContainer cookies = new CookieContainer();
                    request.CookieContainer = cookies;

                    using (Stream postStream = request.GetRequestStream())
                    {
                        postStream.Write(postBytes, 0, postBytes.Length);
                        postStream.Close();
                    }

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader responseStreamReader = new StreamReader(responseStream))
                        {
                            string jsonData = responseStreamReader.ReadToEnd();

                            try
                            {
                                AppLinkAuthenticationResponse data = Utilities.Deserialize<AppLinkAuthenticationResponse>(jsonData);
                                if (data.Success)
                                {
                                    try
                                    {
                                        WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Redirect;
                                        WebOperationContext.Current.OutgoingResponse.Headers.Add("Location", string.Format("{0}{1}{2}", assign.TileUri, (assign.TileUri.IndexOf("?") > -1) ? "&" : "?", data.Token));
                                        return null;
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                                    return new SingleResult<bool>(true);

                                }
                                else
                                {
                                    return new SingleResult<bool>(new Exception(jsonData));
                                }

                            }
                            catch (Exception ex)
                            {
                                return new SingleResult<bool>(new Exception(jsonData));
                            }

                        }
                    }
                }
                else
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Redirect;
                    WebOperationContext.Current.OutgoingResponse.Headers.Add("Location", assign.TileUri);
                }
            }
            return new SingleResult<bool>(false);
        }
        public MultipleResult<AppGroup> GetUserGroups(int userId)
        {
            List<AppGroup> groups = new List<AppGroup>();
            if (UserIsSalesChannelAdmin)
            {
                AppGroupProfile profile = new AppGroupProfile()
                {
                    Name = "Homebase Administrators",
                    Description = "Administers Homebase"
                };
                groups.Add(new AppGroup()
                {
                    Id = "0",
                    Profile = profile
                });
            }
            return new MultipleResult<AppGroup>(groups);
        }

        #endregion

        #region Themes

        /// <summary>
        /// Finds themes that match the query parameters.
        /// </summary>
        /// <param name="searchText">
        /// Optional search parameter that is matched against the theme. If specified, the value
        /// must appear somewhere in the name of the theme.
        /// </param>
        /// <param name="orderBy">
        /// Name of the column to order the results by.
        /// </param>
        /// <param name="orderDir">
        /// Where to order the results in ascending or descending order.
        /// </param>
        /// <param name="pageIndex">
        /// The page number of results to return, begins at 1.
        /// </param>
        /// <param name="pageSize">
        /// The number of results to return, should be greater than 0.
        /// </param>
        /// <param name="f">
        /// An ExtJS filter object to be applied to the results.
        /// </param>
        /// <returns>
        /// A paged result set of themes matching the query parameters.
        /// </returns>
        public PagedResult<Theme> FindThemes(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, List<GenericFilter> f)
        {
            SqlQuery query = null;

            try
            {
                if (string.IsNullOrWhiteSpace(orderBy))
                {
                    orderBy = "Name";
                }

                Log.Info("Performing operation to find a set of themes.");

                Log.DebugFormat("Finding themes with searchText={0}, orderBy={1}, pageIndex={2}, pageSize={3}", searchText, orderBy, pageIndex, pageSize);

                query = Select.AllColumnsFrom<Data.Theme>()
                    .Where(Data.Theme.IsDeletedColumn).IsEqualTo(false);

                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    query.And(Data.Theme.NameColumn).ContainsString(searchText);
                }
                if (f != null)
                {
                    //query.ApplyGenericFilter<Data.Theme>(f);
                }

                var result = new PagedResult<Theme>(query, pageIndex, pageSize, orderBy, orderDir);

                Log.InfoFormat("Retrieved {0} themes.", result.Data.Count());

                return result;
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to find a set of themes." + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to find a aset of themes.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Gets the theme with the specified id.
        /// </summary>
        /// <param name="id">
        /// Id of the theme to return.
        /// </param>
        /// <returns>
        /// Theme with a matching id.
        /// </returns>
        public SingleResult<Theme> GetTheme(int id)
        {
            SqlQuery query = null;

            try
            {
                Log.InfoFormat("Performing operation to find a theme with id {0}.", id);

                query = Select.AllColumnsFrom<Data.Theme>()
                    .Where(Data.Theme.IdColumn).IsEqualTo(id);

                var result = new SingleResult<Theme>(query);

                Log.Info("Retrieved the theme.");

                return result;
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to get theme with id " + id + ": " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to get theme with id " + id + ".", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Creates a new theme from the provided model data.
        /// </summary>
        /// <param name="model">
        /// Data to use when creating the theme. The data should not have a positive integer value
        /// for its id parameter.
        /// </param>
        /// <returns>
        /// The new theme. The data will include the new database id.
        /// </returns>
        public SingleResult<Theme> CreateTheme(Theme model)
        {
            try
            {
                Log.Info("Performing operation to create a theme");

                if (model.ID > 0)
                {
                    throw new ArgumentException("Cannot create theme when model data already has an associated id.");
                }

                var data = new Data.Theme();
                model.CopyTo(data);

                data.Save();

                Log.DebugFormat("New theme created with id {0}.", data.Id);
                model.ID = data.Id;

                return new SingleResult<Theme>(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to create a theme.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Updates an existing new theme from the provided model data.
        /// </summary>
        /// <param name="model">
        /// Data to use when creating the theme. The data should have a positive integer value for
        /// its id parameter.
        /// </param>
        /// <returns>
        /// The existing theme.
        /// </returns>
        public SingleResult<Theme> UpdateTheme(Theme model)
        {
            try
            {
                Log.Info("Performing operation to update a theme");

                if (model.ID == 0)
                {
                    throw new ArgumentException("Cannot update theme when model data does not have an associated id.");
                }

                var data = new Data.Theme(model.ID);
                model.CopyTo(data);

                data.Save();

                Log.DebugFormat("Updated theme with id {0}.", data.Id);
                model.ID = data.Id;

                return new SingleResult<Theme>(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to update a theme.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Deletes an existing theme.
        /// </summary>
        /// <param name="model">
        /// Theme to delete. The theme should have a positive integer for its ID.
        /// </param>
        /// <returns>
        /// The deleted theme.
        /// </returns>
        public SingleResult<Theme> DeleteTheme(Theme model)
        {
            try
            {
                Log.Info("Performing operation to delete a theme");

                if (model.ID == 0)
                {
                    throw new ArgumentException("Cannot delete theme when model data does not have an associated id.");
                }

                model.IsDeleted = true;

                var data = new Data.Theme(model.ID);
                model.CopyTo(data);

                data.Save();

                Log.DebugFormat("Deleted theme with id {0}.", data.Id);
                model.ID = data.Id;

                return new SingleResult<Theme>(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to delete a theme.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        #endregion
    }
}
