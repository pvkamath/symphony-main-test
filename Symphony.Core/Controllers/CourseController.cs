﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using Symphony.Core.Models;
using System.Configuration;
using Symphony.RusticiIntegration.Core;
using SubSonic;
using System.Text.RegularExpressions;
using log4net;
using Symphony.Core.Extensions;

namespace Symphony.Core.Controllers
{
    public class CourseController : Controllers.SymphonyController
    {
        static ILog Log = LogManager.GetLogger(typeof(CourseController));

        public static bool AddTranscriptDetails(int userId, List<Course> courses, int trainingProgramID)
        {
            DateTime ignored = new DateTime();
            return AddTranscriptDetails(userId, courses, trainingProgramID, out ignored);
        }

        /// <summary>
        /// Updates the training program start and end dates to account for new hire parameters
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="trainingProgram"></param>
        public static void CalculateTrainingProgramNewHireDates(int userId, Data.TrainingProgram trainingProgram)
        {
            if (trainingProgram.IsNewHire)
            {
                User user = (new UserController()).GetUser(userId);
                Customer customer = (new CustomerController()).GetCustomer(SymphonyController.GetCustomerID(), SymphonyController.GetUserID(), user.CustomerID).Data;

                if (!user.HireDate.HasValue)
                {
                    user.HireDate = DateTime.UtcNow;
                }

                trainingProgram.StartDate = user.HireDate.Value;
                // set the end and due dates based on the new hire duration
                trainingProgram.DueDate = trainingProgram.EndDate = user.HireDate.Value.AddDays(customer.NewHireDuration);
                
                // add any tp-level offsets
                if (trainingProgram.NewHireStartDateOffset.HasValue)
                {
                    trainingProgram.StartDate = user.HireDate.Value.AddDays(trainingProgram.NewHireStartDateOffset.Value);
                }
                if (trainingProgram.NewHireDueDateOffset.HasValue)
                {
                    trainingProgram.DueDate = user.HireDate.Value.AddDays(trainingProgram.NewHireDueDateOffset.Value);
                }
                if (trainingProgram.NewHireEndDateOffset.HasValue && trainingProgram.NewHireEndDateOffset != 0)
                {
                    trainingProgram.EndDate = user.HireDate.Value.AddDays(trainingProgram.NewHireEndDateOffset.Value);
                }
                else 
                {
                    trainingProgram.EndDate = user.HireDate.Value.AddDays(customer.NewHireDuration);
                }
            }
        }

        /// <summary>
        /// Returns true if the given transcript entry attempt date falls within the TP range
        /// </summary>
        /// <param name="tp"></param>
        /// <param name="entry"></param>
        /// <returns></returns>
        public static bool ClassroomCourseCountsForTP(Data.TrainingProgram tp, TranscriptEntry entry)
        {
            if (!entry.StartDate.HasValue)
            {
                return false;
            }
            DateTime entryStart = entry.StartDate.Value.WithUtcFlag();
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(tp.Customer.TimeZone);
            TimeSpan startOffset = tzi.GetUtcOffset(tp.StartDate);
            TimeSpan endOffset = tzi.GetUtcOffset(tp.EndDate).Subtract(TimeSpan.FromDays(1)); // add one day because any time during the enddate is valid
            return entryStart >= tp.StartDate.WithUtcFlag().Subtract(startOffset) && entryStart <= tp.EndDate.WithUtcFlag().Subtract(endOffset);
        }

        /// <summary>
        /// Returns true if the given entry for an Online course falls within the TP range
        /// </summary>
        /// <param name="tp"></param>
        /// <param name="entry"></param>
        /// <returns></returns>
        public static bool OnlineCourseCountsForTP(Data.TrainingProgram tp, TranscriptEntry entry)
        {
            if (!entry.AttemptDate.HasValue)
            {
                return false;
            }
            DateTime entryAttempt = entry.AttemptDate.Value.WithUtcFlag();
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(tp.Customer.TimeZone);
            TimeSpan startOffset = tzi.GetUtcOffset(tp.StartDate);
            TimeSpan endOffset = tzi.GetUtcOffset(tp.EndDate).Subtract(TimeSpan.FromDays(1)); // add one day because any time during the enddate is valid
            return entryAttempt >= tp.StartDate.WithUtcFlag().Subtract(startOffset) && entryAttempt <= tp.EndDate.WithUtcFlag().Subtract(endOffset);
        }

        public static bool AssignmentCountsForTP(Data.TrainingProgram tp, Assignment assignment)
        {
            TimeZoneInfo tzi_Customer = TimeZoneInfo.FindSystemTimeZoneById(tp.Customer.TimeZone);
            TimeZoneInfo tzi_Server = TimeZoneInfo.Local;

            TimeSpan startOffset = tzi_Customer.GetUtcOffset(tp.StartDate);
            TimeSpan endOffset = tzi_Customer.GetUtcOffset(tp.EndDate).Subtract(TimeSpan.FromDays(1));

            DateTime tpStart = tp.StartDate.WithUtcFlag().Subtract(startOffset);
            DateTime tpEnd = tp.EndDate.WithUtcFlag().Subtract(endOffset);

            DateTime assignmentAttempt = TimeZoneInfo.ConvertTime(assignment.Date, tzi_Server, TimeZoneInfo.Utc);

            return assignmentAttempt >= tpStart && assignmentAttempt <= tpEnd;
        }

        /// <summary>
        /// Adds transcript details to a give list of courses.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="courses"></param>
        /// <param name="transcriptEntries">Optional list of transcript entries to use while adding details. Saves on trips to db in a loop</param>
        /// <returns>True if all the requirements for this training program have been met, otherwise false.</returns>
        public static bool AddTranscriptDetails(int userId, List<Course> courses, int trainingProgramID, out DateTime maxPassedDate, List<TranscriptEntry> transcriptEntries = null, Data.TrainingProgram activeTP = null)
        {
            // Here's how this works.
            //
            // If the isPublic flag is set, then we're retrieving data for public courses *only*.
            // That means that:
            // 1) Classroom courses must be flagged as public to see the score at all
            // 2) Online courses must have a record for a training program with id "0"
            //
            // If the isPublic flag is NOT set, we're retrieving data for training programs.
            // That means that:
            //

            if (trainingProgramID > 0)
            {
                if (activeTP == null)
                {
                    activeTP = new Data.TrainingProgram(trainingProgramID);
                }
                if (activeTP.IsNewHire)
                {
                    CalculateTrainingProgramNewHireDates(userId, activeTP);
                }
            }

            // We allow passing in transcript entries to avoid repeated hits to the stored procedure
            // in the event we are loading multiple training programs. Load them up if we haven't passed them in.
            if (transcriptEntries == null)
            {
                transcriptEntries = Data.SPs.GetTranscript(userId).ExecuteTypedList<TranscriptEntry>();
            }
            
            maxPassedDate = DateTime.MinValue;
            foreach (Course course in courses)
            {
                List<TranscriptEntry> matches = null;

                // find the course that matches this type and course id, within the training program date range
                if (course.CourseTypeID == (int)CourseType.Classroom)
                {
                    // classroom courses are relatively simple
                    matches = transcriptEntries.FindAll(e =>
                        // matching course ids
                        e.CourseID == course.Id &&
                            // matching course types
                        e.CourseTypeID == course.CourseTypeID &&
                            // public vs tp course
                        (trainingProgramID == 0 ?
                            e.IsPublic == true : e.IsPublic == false)
                    );

                    // for training programs, we now need to match the class dates to ensure it falls within the TP range
                    // we only care about if it *started* in the TP
                    if (trainingProgramID > 0)
                    {
                        matches = matches.FindAll(entry =>
                        {
                            if (entry.StartDate.HasValue)
                            {
                                return ClassroomCourseCountsForTP(activeTP, entry);
                            }
                            return false;
                        });
                    }
                }
                else
                {
                    matches = transcriptEntries.FindAll(e =>
                        // matching course ids
                        e.CourseID == course.Id &&
                            // matching course types
                        e.CourseTypeID == course.CourseTypeID &&
                            // public vs tp course
                        (trainingProgramID == 0 ?
                            e.TrainingProgramID == 0 : e.TrainingProgramID != 0)
                    );

                    // for training programs, we now need to match the online course attempt date to ensure it falls within the TP range
                    // we also make sure the end time is midnight of the end date
                    if (trainingProgramID > 0)
                    {
                        matches = matches.FindAll(entry =>
                        {
                            if (entry.AttemptDate.HasValue)
                            {
                                return OnlineCourseCountsForTP(activeTP, entry);
                            }
                            return false;
                        });
                    }
                }

                if (matches.Count() > 0)
                {
                    // regardless of how many entries were returned, grab the one with the highest score and use that
                    // note that the "score" can be A-F, or a numeric score, but it'll never be mixed for a given course
                    int temp = 0;
                    bool numeric = false;
                    TranscriptEntry entry = null;

                    // first, check to see if any entries are numbers
                    foreach (TranscriptEntry tempEntry in matches)
                    {
                        if (int.TryParse(tempEntry.Score, out temp))
                        {
                            numeric = true;
                            break;
                        }
                    }

                    if (numeric)
                    {
                        // if we have any numbers, make sure *all* items have a valid number
                        foreach (TranscriptEntry tempEntry in matches)
                        {
                            if (string.IsNullOrEmpty(tempEntry.Score))
                            {
                                tempEntry.Score = "0";
                            }
                        }
                        entry = matches.MaxBy(m => int.Parse(m.Score));
                    }
                    else
                    {
                        entry = matches.MaxBy(m => m.Score);
                    }

                    // Check can move forward status
                    // Since we are taking the best result, regardless of the result being linked
                    // to the specific training program or not, we need to check if any entry
                    // has the can move forward flag set. If that is the case, then we should return true
                    foreach (TranscriptEntry tempEntry in matches)
                    {
                        if (tempEntry.CanMoveForwardIndicator)
                        {
                            course.CanMoveForwardIndicator = true;
                            break;
                        }
                    }

                    //TODO: validate rounding
                    course.Score = entry.Score;
                    course.NavigationPercentage = entry.NavigationPercentage;

                    // set webinar stuff (applies to virtual classroom courses)
                    course.WebinarRegistrationID = entry.WebinarRegistrationID;
                    course.WebinarKey = entry.WebinarKey;

                    course.Completed = entry.Completed.HasValue && entry.Completed == 1 ? true : false;

                    // start date is the class date for classroom classes, and null for online courses
                    course.StartDate = entry.StartDate;
                    course.EndDate = entry.EndDate;
                    course.AttemptDate = entry.AttemptDate ?? entry.StartDate;
                    course.RegistrationID = entry.RegistrationID;
                    course.AttendedIndicator = entry.AttendedIndicator;
                    course.RegistrationStatusID = entry.RegistrationStatusID;
                    course.Passed = entry.Passed;
                    course.SurveyID = entry.SurveyID;
                    course.SurveyTaken = entry.SurveyTaken;
                    course.ClassID = entry.ClassID;
                    course.AttemptCount = entry.AttemptCount;

                    course.AttemptTime = entry.AttemptTime;
                    course.IsAssignmentRequired = entry.IsAssignmentRequired;
                    course.IsAssignmentMarked = entry.IsAssignmentMarked;
                    course.AssignmentsCompleted = entry.AssignmentsCompleted;
                    course.AssignmentsMarked = entry.AssignmentsMarked;
                    course.OnlineCourseRollupID = entry.OnlineCourseRollupID;
                    course.TestAttemptCount = entry.TestAttemptCount;
                    course.AssignmentsInCourse = entry.AssignmentsInCourse;

                    course.OnlineCourseRollupID = entry.OnlineCourseRollupID;

                    course.FirstCompletionDate = entry.FirstCompletionDate;

                    //Versioning Information
                    course.PlaybackCoreVersion = entry.PlaybackCoreVersion;
                    course.DeliveredCoreVersion = entry.DeliveredCoreVersion;
                    course.DeliveredOnlineCourseVersion = entry.DeliveredOnlineCourseVersion;
                    course.DeliveredArtisanCourseID = entry.DeliveredArtisanCourseID;
                    course.CurrentArtisanCourseID = entry.CurrentArtisanCourseID;
                    course.CurrentCoreVersion = entry.CurrentCoreVersion;
                    course.CurrentCourseVersion = entry.CurrentCourseVersion;
                    course.DeliveredArtisanCourseCreatedOn = entry.DeliveredArtisanCourseCreatedOn;
                    course.CurrentArtisanCourseCreatedOn = entry.CurrentArtisanCourseCreatedOn;
                    
                    // Record the user id the transcript info came from
                    course.TranscriptUserID = userId;

                    if (course.Passed.HasValue && course.Passed.Value == 1)
                    {
                        if (course.AttemptDate.HasValue && course.AttemptDate > maxPassedDate)
                        {
                            maxPassedDate = course.AttemptDate.Value;
                        }
                    }
                }
            }

            if (activeTP != null)
            {
                int requiredPassed = 0;
                int requiredTotal = 0;
                int electivesPassed = 0;
                bool finalPassed = false;
                bool finalRequired = false;

                foreach (Data.TrainingProgramToCourseLink link in activeTP.TrainingProgramToCourseLinks)
                {

                    Course course = courses.FirstOrDefault(c => c.Id == link.CourseID && c.CourseTypeID == c.CourseTypeID);
                    if (course != null)
                    {
                        if (course.Passed.HasValue && course.Passed.Value == 1)
                        {
                            switch (link.SyllabusTypeID)
                            {
                                case (int)SyllabusType.Required:
                                    requiredPassed++;
                                    break;
                                case (int)SyllabusType.Elective:
                                    electivesPassed++;
                                    break;
                                case (int)SyllabusType.Final:
                                    finalPassed = true;
                                    break;
                            }
                        }
                    }
                    if (link.SyllabusTypeID == (int)SyllabusType.Required)
                    {
                        requiredTotal++;
                    }
                    else if (link.SyllabusTypeID == (int)SyllabusType.Final)
                    {
                        finalRequired = true;
                    }
                }

                // if the minimum course requirements are met
                if (electivesPassed >= activeTP.MinimumElectives &&
                    requiredPassed >= requiredTotal)
                {
                    // and the final was required and passed, or not required
                    if ((finalRequired && finalPassed) || !finalRequired)
                    {
                        // the TP was successfully completed
                        return true;
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// Bulk download of passed in course ids. 
        /// Creates the packages and returns the filename for download
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="courseIds"></param>
        /// <param name="courseOverrides"></param>
        /// <param name="parameterOverridesJson"></param>
        /// <returns></returns>
        public SimpleSingleResult<string> GetPackagesFileName(int[] courseIds, Course courseOverrides, List<ParameterValue> parameterOverides)
        {
            Data.OnlineCourse course = new Data.OnlineCourse();
            if (courseOverrides != null)
            {
                courseOverrides.CopyTo(course);
            }

            string packageLocation = GetArtisanPackageLocation();
            string tempPackageDirectory = Path.Combine(packageLocation, "Export-" + Guid.NewGuid());
            Regex reg = new Regex("[^a-z0-9]", RegexOptions.IgnoreCase);

            Dictionary<int, Data.OnlineCourse> courseDictionary = Select.AllColumnsFrom<Data.OnlineCourse>()
                .Where(Data.OnlineCourse.IdColumn).In(courseIds)
                .ExecuteTypedList<Data.OnlineCourse>()
                .ToDictionary(c => c.Id);

            // Copy all the course directories to temporary directory
            foreach (int i in courseIds)
            {
                string pathToPackage = GetPathToPackage(i, CustomerID);

                string parameterOverridesJson = null;
                if (parameterOverides.Count() > 0)
                {
                    parameterOverridesJson = Utilities.Serialize(parameterOverides);
                }

                string overrideJs = (new ArtisanController()).GetArtisanHookJS(i, 0, course, parameterOverridesJson);
                string folderName = i + "_" + reg.Replace(courseDictionary[i].Name, "_");
                string tempPathToPackage = Path.Combine(tempPackageDirectory, folderName);

                // Copy the folder to our temp directory
                Utilities.CopyFolder(pathToPackage, tempPathToPackage);

                // Create the artisan course hook js
                // Assuming these downloads will be run outside of Symphony
                // this will now point to the pre-generated course hook and 
                // override the course parameters
                using (StreamWriter file = new StreamWriter(Path.Combine(tempPathToPackage, "js/artisan_course_hook.js")))
                {
                    file.Write(overrideJs);
                    file.Close();
                }
            }
            string fileName = "SymphonyOnlineCourses_" + DateTime.Now.ToString("yyyyMdhhmmss") + ".zip";
            string archive = Utilities.Zip(tempPackageDirectory, Path.Combine(packageLocation, fileName));

            Directory.Delete(tempPackageDirectory, true);
            
            return new SimpleSingleResult<string>(fileName);
            
        }
        /// <summary>
        /// Downloads a package of courses created with GetPackageFileName
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public Stream GetPackages(string fileName)
        {
            string filePath = Path.Combine(GetArtisanPackageLocation(), fileName);

            if (!File.Exists(filePath))
            {
                throw new Exception("Could not find the requested package.");
            }

            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);

            return new FileStream(filePath, FileMode.Open);
        }

        public static string GetPathToPackage(int courseId, int customerId) {
            // TODO: Fix this workaround for GetPathToPackage
            // Should look something like
            /*
                SymphonyExternalPackage package = new SymphonyExternalPackage();
                package.CourseId = surveyId;

                SymphonyExternalConfiguration cfg = new SymphonyExternalConfiguration();
                cfg.ExistingCourseID = surveyId;
                cfg.CustomerID = CustomerID;
             
                string path = RusticiSoftware.ScormContentPlayer.Logic.Integration.Implementation.GetPathToPackage(package, cfg);
            */
            // But, GetPathToPackage always returns null
            // SO...
            
            ConnectionStringSettings cns = ConfigurationManager.ConnectionStrings["OnlineTraining"];

            string webContentRoot;
            string fileContentRoot;
            string coursePath;
            string pathToPackage;

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(cns.ConnectionString))
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(@"
                select
                        p.web_path
                    from
                        scormpackage p
                    where
                        p.course_id = @courseId
                    order by
                        p.version_id DESC
                "))
                {
                    cmd.Connection = conn;
                    cmd.Parameters.AddWithValue("@courseId", courseId);
                    cmd.Connection.Open();

                    System.Data.SqlClient.SqlDataReader reader = cmd.ExecuteReader();

                    if (!reader.Read())
                    {
                        throw new Exception("Could not find scorm package for survey. Select a different survey or try re-deploying. Your changes have not been saved.");
                    }

                    webContentRoot = ConfigurationManager.AppSettings["WebPathToContentRoot"];
                    Console.WriteLine("WebContentRoot: " + webContentRoot);
                    fileContentRoot = new SymphonyController().FilePathToContentRoot;
                    Console.WriteLine("FileContentRoot: " + fileContentRoot);
                    coursePath = reader.GetString(0).Replace(webContentRoot, "").Replace("/", "\\");
                    coursePath = coursePath.Substring(System.IO.Path.GetPathRoot(coursePath).Length);

                    pathToPackage = System.IO.Path.Combine(fileContentRoot, coursePath);
                    Console.WriteLine("PathToPackage: " + pathToPackage);

                    cmd.Connection.Close();
                }
            }

            return pathToPackage;
        }


        public static Data.OnlineCourse DuplicateSurvey(int surveyId, int customerId, string username)
        {

            string pathToPackage = GetPathToPackage(surveyId, customerId);

            Data.OnlineCourse duplicateSurvey = new Data.OnlineCourse(surveyId);
            duplicateSurvey.DuplicateFromID = surveyId;
            duplicateSurvey.Id = 0;
            duplicateSurvey.IsNew = true;
            duplicateSurvey.Save();

            SymphonyExternalConfiguration cfg = new SymphonyExternalConfiguration();
            cfg.ExistingCourseID = duplicateSurvey.Id;
            cfg.CustomerID = customerId;
            cfg.IsSurvey = true;

            try
            {
                SymphonyIntegrationController.ImportSingleCourseFromFolder(pathToPackage, cfg, username);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw new Exception("The survey applied to the course selected could not be found (" + e.Message + "). Please select a different survey for the course, or re-deploy the survey. The class could not be saved.");
            }

            return duplicateSurvey;
        }

    }
}
