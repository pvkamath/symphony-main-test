﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Web;
using Symphony.Web;
using SubSonic;
using System.Configuration;
using System.IO;
using Symphony.Core.Models;
using Symphony.Core.Extensions;

namespace Symphony.Core.Controllers
{
    public class SymphonyController
    {
        protected string TrainingProgramFilesLocation = ConfigurationManager.AppSettings["TrainingProgramFilesUploadDirectory"] ?? "~/Uploads/TrainingProgramFiles/";
        protected string CourseFilesLocation = ConfigurationManager.AppSettings["CourseFilesUploadDirectory"] ?? "~/Uploads/CourseFiles/";
        protected string PublicDocumentFilesLocation = ConfigurationManager.AppSettings["PublicDocumentFilesUploadDirectory"] ?? "~/Uploads/PublicDocumentFiles/";
        protected string ArtisanAssetFilesLocation = ConfigurationManager.AppSettings["ArtisanAssetsUploadDirectoryOverride"] ?? ConfigurationManager.AppSettings["ArtisanAssetsUploadDirectory"] ?? "~/Uploads/ArtisanAssetFiles/";
        protected string ArtisanPackageLocation = ConfigurationManager.AppSettings["ArtisanPackageDirectory"] ?? "~/Package/";
        protected string AritsanPreviewLocation = ConfigurationManager.AppSettings["ArtisanPreviewDirectory"] ?? "~/PackagePreview/";
        protected string ArtisanAssetRoot = ConfigurationManager.AppSettings["ArtisanAssetRoot"] ?? "~/Assets/Artisan/";

        protected string CertificateAssetsDirectory = ConfigurationManager.AppSettings["CertificateAssetsDirectory"] ?? "~/Uploads/CertificateEditor/Assets";

        protected string LicenseAssignmentDocumentsLocation = ConfigurationManager.AppSettings["LicenseAssignmentDocumentsUploadDirectory"] ?? "~/Uploads/LicenseAssignmentDocuments/";

        public string FilePathToContentRoot = ConfigurationManager.AppSettings["FilePathToContentRoot"];

        protected string TempFolder = ConfigurationManager.AppSettings["CourseTempDirectory"] ?? Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
        protected string ImportFolder = ConfigurationManager.AppSettings["CourseUploadDirectory"];
        protected string ImportUploadFolder = ConfigurationManager.AppSettings["ImportUploadDirectory"];
        protected string ImportCommandLine = ConfigurationManager.AppSettings["ImportCommandLine"];
        protected string JobFileName = ConfigurationManager.AppSettings["JobFileName"] ?? "Job.csv";
        protected string LocationFileName = ConfigurationManager.AppSettings["LocationFileName"] ?? "Loc.csv";
        protected string AudienceFileName = ConfigurationManager.AppSettings["AudienceFileName"] ?? "Aud.csv";
        protected string EmployeeFileName = ConfigurationManager.AppSettings["EmployeeFileName"] ?? "Emp.csv";
        protected string OrderFileName = ConfigurationManager.AppSettings["OrderFileName"] ?? "order.csv";
        protected string CanDeployMultiple = ConfigurationManager.AppSettings["CanDeployMultipleCourses"];
        protected string CanViewMultiple = ConfigurationManager.AppSettings["CanViewMultipleCustomers"];
        protected string DeployedCoursesFolder = ConfigurationManager.AppSettings["DeployedCoursesFolder"];
           

        protected string ReportXLSLocation = ConfigurationManager.AppSettings["ReportXLSDirectory"] ?? "~/Report/XLS/";
        protected string ReportPDFLocation = ConfigurationManager.AppSettings["ReportPDFDirectory"] ?? "~/Report/PDF/";
        protected string ReportCSVLocation = ConfigurationManager.AppSettings["ReportCSVDirectory"] ?? "~/Report/CSV/";
        protected string ReportTemplateLocation = ConfigurationManager.AppSettings["ReportTemplateLocation"] ?? "~/Report/Template/";
        protected string MaxReportResultDataLength = ConfigurationManager.AppSettings["MaxReportResultDataLength"] ?? "-1";
        protected string MaxReportResultRowCount = ConfigurationManager.AppSettings["MaxReportResultRowCount"] ?? "-1";

        public SymphonyController()
        {
            if (!string.IsNullOrEmpty(ImportUploadFolder) && ImportUploadFolder.StartsWith("~/") && HttpContext.Current != null)
            {
                ImportUploadFolder = HttpContext.Current.Server.MapPath(ImportUploadFolder);
            }
            if (!string.IsNullOrEmpty(ImportFolder) && ImportFolder.StartsWith("~/") && HttpContext.Current != null)
            {
                ImportFolder = HttpContext.Current.Server.MapPath(ImportFolder);
            }
            if (!string.IsNullOrEmpty(DeployedCoursesFolder) && DeployedCoursesFolder.StartsWith("~/") && HttpContext.Current != null)
            {
                DeployedCoursesFolder = HttpContext.Current.Server.MapPath(DeployedCoursesFolder);
            }
            if (!string.IsNullOrEmpty(FilePathToContentRoot) && FilePathToContentRoot.StartsWith("~/") && HttpContext.Current != null)
            {
                FilePathToContentRoot = HttpContext.Current.Server.MapPath(FilePathToContentRoot);
            }
            if (!string.IsNullOrEmpty(ImportCommandLine) && ImportCommandLine.StartsWith("~/") && HttpContext.Current != null)
            {
                ImportCommandLine = HttpContext.Current.Server.MapPath("~/") + ImportCommandLine.Substring(2).Replace("/","\\");
            }
        }

        protected DateTime DefaultDateTime = DateExtensions.DefaultDateTime;

        public Modules Modules = new Modules();

        public bool CanDeployMultipleCourses(string subdomain)
        {
            if (string.IsNullOrEmpty(CanDeployMultiple))
            {
                throw new Exception("Missing a 'CanDeployMultipleCourses' setting.");
            }
            string[] subdomains = CanDeployMultiple.Split(',');
            if (subdomains.Contains(subdomain))
            {
                return true;
            }
            return false;
        }

        public bool CanViewMultipleCustomers(string subdomain)
        {
            if (string.IsNullOrEmpty(CanViewMultiple))
            {
                throw new Exception("Missing a 'CanViewMultipleCustomers' setting.");
            }
            string[] subdomains = CanViewMultiple.Split(',');
            if (subdomains.Contains(subdomain))
            {
                return true;
            }
            return false;
        }

        public string GetArtisanAssetFilesLocation()
        {
            return AdjustPath(this.ArtisanAssetFilesLocation);
        }

        public string GetArtisanPreviewLocation()
        {
            return AdjustPath(this.AritsanPreviewLocation);
        }

        public string GetArtisanPackageLocation()
        {
            return AdjustPath(this.ArtisanPackageLocation);
        }

        /// <summary>
        /// If a path starts with "~/", returns the mapped version of that path. Otherwise, just returns the path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string AdjustPath(string path)
        {
            return path.StartsWith("~/") && HttpContext.Current != null ? HttpContext.Current.Server.MapPath(path) : path;
        }

        /// <summary>
        /// The currently logged in user's username
        /// </summary>
        public string Username
        {
            get
            {
                if (HttpContext.Current != null && HttpContext.Current.Items != null)
                {
                    var userData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];
                    return userData != null ? userData.Username : string.Empty; // return HttpContext.Current.Items.User.Identity.Name;
                }
                return string.Empty;
            }
        }
        /// <summary>
        /// Static helper function to get the application name (Customer subdomain)
        /// </summary>
        public static string GetApplicationName()
        {
            if (HttpContext.Current != null && HttpContext.Current.Items != null)
            {
                object o = HttpContext.Current.Items[ApplicationProcessor.CONTEXT_CUSTOMER];
                if (o != null)
                {
                    return o.ToString();
                }
            }
            return ApplicationProcessor.NOT_SET;
        }

        /// <summary>
        /// The "ApplicationName" or "Subdomain" for the currently logged in user
        /// </summary>
        public string ApplicationName
        {
            get
            {
                return GetApplicationName();
            }
        }

        /// <summary>
        /// The customer's display name. WARNING: creates a database call
        /// </summary>
        public string CustomerName { get { return new Data.Customer(CustomerID).Name; } }

        /// <summary>
        /// Customer subdomain. WARNING: creates a database call
        /// </summary>
        public string CustomerSubdomain { get { return new Data.Customer(CustomerID).SubDomain; } }

        /// <summary>
        /// The authentication token from reporting
        /// </summary>
        public string CNRToken
        {
            get
            {
                string cnrToken = string.Empty;
                if (HttpContext.Current != null && HttpContext.Current.Items != null && HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] != null)
                {
                    var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];
                    cnrToken = currentUserData.CNRToken;
                }
                return cnrToken;
            }
        }

        /// <summary>
        /// The currently logged in user's cusotmer id
        /// For easy access elsewhere - specifically in 
        /// display models where occasionaly a CustomerID is
        /// required. 
        /// </summary>
        public static int GetCustomerID()
        {
            int customerId = 0;
            if (HttpContext.Current != null && HttpContext.Current.Items != null && HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] != null)
            {
                var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];
                customerId = currentUserData.CustomerID;
            }
            return customerId;
        }

        /// <summary>
        /// The currently logged in user's user id
        /// </summary>
        public int CustomerID
        {
            get
            {
                return GetCustomerID();
            }
        }
        /// <summary>
        /// The currently logged in user's sales channel id
        /// </summary>
        public int SalesChannelID
        {
            get
            {
                int salesChannelId = 0;
                if (HttpContext.Current != null && HttpContext.Current.Items != null && HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] != null)
                {
                    var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];
                    salesChannelId = currentUserData.SalesChannelID;
                }
                return salesChannelId;
            }
        }

        /// <summary>
        /// Whether or not the current user is a sales channel administrator
        /// </summary>
        public bool UserIsSalesChannelAdmin
        {
            get { return SalesChannelID > 0; }
        }

        /// <summary>
        /// Whether or not the current user has the specified role
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool HasRole(string role)
        {
            return SymphonyRoleProvider.IsUserInRole(ApplicationName, Username, role);
        }

        /// <summary>
        /// The currently logged in user's user id
        /// For easy access elsewhere - specifically in 
        /// display models where occasionaly a UserID is
        /// required. 
        /// </summary>
        public static int GetUserID()
        {
            int userId = 0;
            if (HttpContext.Current != null && HttpContext.Current.Items != null && HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] != null)
            {
                var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];
                userId = currentUserData.UserID;
            }
            return userId;
        }

        /// <summary>
        /// The currently logged in user's user id
        /// </summary>
        public int UserID
        {
            get
            {
                return GetUserID();
            }
        }

        /// <summary>
        /// Whether or not the current user is a new hire
        /// </summary>
        public bool UserIsNewHire
        {
            get
            {
                bool userIsNewHire = false;
                if (HttpContext.Current != null && HttpContext.Current.Items != null && HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] != null)
                {
                    var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];
                    userIsNewHire = currentUserData.IsNewHire;
                }
                return userIsNewHire;
            }
        }

        /// <summary>
        /// The users first + last name
        /// </summary>
        public string UserFullName
        {
            get
            {
                return Models.Model.Create<Models.User>(new Data.User(UserID)).FullName;
            }
        }

        /// <summary>
        /// The currently logged in user's real user id (if he is spoofing another user)
        /// </summary>
        public int ActualUserID
        {
            get
            {
                int userId = 0;
                if (HttpContext.Current != null && HttpContext.Current.Items != null && HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] != null)
                {
                    var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];
                    userId = currentUserData.ActualUserID;
                }
                return userId;
            }
        }

        public int ActualCustomerID
        {
            get
            {
                int customerId = 0;
                if (HttpContext.Current != null && HttpContext.Current.Items != null && HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] != null)
                {
                    var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];
                    customerId = currentUserData.ActualCustomerID;
                }
                return customerId;
            }
        }

        public int ActualSalesChannelID
        {
            get
            {
                int salesChannelId = 0;
                if (HttpContext.Current != null && HttpContext.Current.Items != null && HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] != null)
                {
                    var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];
                    salesChannelId = currentUserData.ActualSalesChannelID;
                }
                return salesChannelId;
            }
        }

        public bool ActualUserIsSalesChannelAdmin
        {
            get { return ActualSalesChannelID > 0; }
        }

        public string ActualApplicationName
        {
            get
            {
                string appName = null;
                if (HttpContext.Current != null && HttpContext.Current.Items != null && HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] != null)
                {
                    var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];
                    appName = currentUserData.ActualApplicationName;
                }
                return appName;
            }
        }

        public Dictionary<string, string> Attributes
        {
            get
            {
                Dictionary<string, string> attr = null;

                if (HttpContext.Current != null && HttpContext.Current.Items != null && HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA] != null)
                {
                    var currentUserData = (AuthUserData)HttpContext.Current.Items[ApplicationProcessor.CONTEXT_USERDATA];
                    attr = currentUserData.Attributes;
                }

                return attr;
            }
        }

        public bool HasValidationParameters
        {
            get
            {
                Data.User userData = new Data.User(UserID);
                if (userData.Dob != null && userData.Dob.Length > 0 && userData.Ssn != null && userData.Ssn.Length > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public bool IsUserValidationParametersRequired
        {
            get
            {
                if (HasValidationParameters)
                {
                    return false;
                }

                
                var ch = new CodingHorror();
                int count = ch.ExecuteScalar<int>(@"
select
	COUNT(*)
from
	Customer
left join
	TrainingProgram
on 
	TrainingProgram.CustomerID = customer.ID and TrainingProgram.IsValidationEnabled = 1
left join
	OnlineCourse
on 
	OnlineCourse.CustomerID = customer.ID and OnlineCourse.IsValidationEnabled = 1
where
	Customer.ID = " + CustomerID);
	

                if (count > 0)
                {
                    return true;
                }

                return false;
            }
        }

        public bool HasNMLSNumber
        {
            get
            {
                Data.User userData = new Data.User(UserID);
                if (userData.NmlsNumber != null && userData.NmlsNumber.Length > 0)
                {
                    return true;
                }

                return false;
            }
        }
        /// <summary>
        /// Temporary flag to allow toggling server side ui logic. 
        /// Can be removed after fully tested. 
        /// This only includes the new fields within the models
        /// as part of the returned data. Does not use these fields
        /// client side.
        /// </summary>
        public static bool IsServerSideUiLogic
        {
            get
            {
                string applicationName = GetApplicationName();
                string serverSideUiLogicKey = applicationName + ".ServerSideUiLogic";
                string releaseHounds = "releasehounds.ServerSideUiLogic";
                
                bool result;

                if (ConfigurationManager.AppSettings.AllKeys.Contains(serverSideUiLogicKey))
                {
                    if (bool.TryParse(ConfigurationManager.AppSettings[serverSideUiLogicKey], out result))
                    {
                        return result;
                    }
                }

                if (ConfigurationManager.AppSettings.AllKeys.Contains(releaseHounds))
                {
                    if (bool.TryParse(ConfigurationManager.AppSettings[releaseHounds], out result))
                    {
                        return result;
                    }
                }

                return false;
            }
        }
        /// <summary>
        /// Temporary flag to allow toggling server side ui logic logging. 
        /// Displays detailed logs in console about any deprecated ui functions
        /// being used, or functionality wrapped in toggles that should be removed
        /// </summary>
        public static bool IsServerSideUiLogicPortal
        {
            get
            {
                string applicationName = GetApplicationName();
                string serverSideUiLogicKey = applicationName + ".ServerSideUiLogic.Portal";
                string releaseHounds = "releasehounds.ServerSideUiLogic.Portal";

                bool result;

                if (!IsServerSideUiLogic) // Fields must be available in order to turn this on
                {
                    return false;
                }

                if (ConfigurationManager.AppSettings.AllKeys.Contains(serverSideUiLogicKey))
                {
                    if (bool.TryParse(ConfigurationManager.AppSettings[serverSideUiLogicKey], out result))
                    {
                        return result;
                    }
                }

                if (ConfigurationManager.AppSettings.AllKeys.Contains(releaseHounds))
                {
                    if (bool.TryParse(ConfigurationManager.AppSettings[releaseHounds], out result))
                    {
                        return result;
                    }
                }

                return false;
            }
        }
        /// <summary>
        /// Temporary flag to enable the usage of server side ui logic fields,
        /// isServerSideUiLogic must be true in order for this to be enabled.
        /// </summary>
        public static bool IsServerSideUiLogicLogging
        {
            get
            {
                string applicationName = GetApplicationName();
                string serverSideUiLogicKey = applicationName + ".ServerSideUiLogic.Logging";
                string releaseHounds = "releasehounds.ServerSideUiLogic.Logging";

                bool result;

                if (ConfigurationManager.AppSettings.AllKeys.Contains(serverSideUiLogicKey))
                {
                    if (bool.TryParse(ConfigurationManager.AppSettings[serverSideUiLogicKey], out result))
                    {
                        return result;
                    }
                }

                if (ConfigurationManager.AppSettings.AllKeys.Contains(releaseHounds))
                {
                    if (bool.TryParse(ConfigurationManager.AppSettings[releaseHounds], out result))
                    {
                        return result;
                    }
                }

                return false;
            }
        }
    }
}
