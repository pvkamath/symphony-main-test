﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Symphony.Core.Models;
using System.Configuration;
using SubSonic;
using System.Text.RegularExpressions;

namespace Symphony.Core.Controllers
{
    public class CategoryController : Controllers.SymphonyController
    {
        private static SqlQuery GetSiblingNameQuery(Category category, int customerId) {
            return new Select(Data.Category.NameColumn).From<Data.Category>()
                .Where(Data.Category.CategoryTypeIDColumn).IsEqualTo(category.CategoryTypeID)
                .And(Data.Category.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.Category.ParentCategoryIDColumn).IsEqualTo(category.ParentCategoryID)
                .And(Data.Category.IdColumn).IsNotEqualTo(category.Id);
        }

        private static SqlQuery GetCategoryQuery(int customerId, string searchText, CategoryType type)
        {
            return Select.AllColumnsFrom<Data.CategoryHierarchy>()
               .Where(Data.CategoryHierarchy.Columns.CustomerID).IsEqualTo(customerId)
               .And(Data.CategoryHierarchy.Columns.LevelText).ContainsString(searchText)
               .And(Data.CategoryHierarchy.Columns.CategoryTypeID).IsEqualTo((int)type);
        }

        private static SqlQuery GetSecondaryCategoryQuery(int customerId, string searchText, CategoryType type)
        {
            return Select.AllColumnsFrom<Data.SecondaryCategory>()
                .And(Data.CategoryHierarchy.Columns.Name).ContainsString(searchText);
        }

        private static List<Category> GetDuplicateSibling(Category category, int customerId)
        {
            return GetSiblingNameQuery(category, customerId).And(Data.Category.NameColumn).IsEqualTo(category.Name).ExecuteTypedList<Category>();
        }

        private static List<Category> GetSimilarSiblings(Category category, int customerId)
        {
            return GetSiblingNameQuery(category, customerId).And(Data.Category.NameColumn).Like(string.Format("%{0}%", category.Name)).ExecuteTypedList<Category>();
        }

        public static MultipleResult<Category> GetCategories(int customerId, string searchText, CategoryType type)
        {
           List<Category> categories = GetCategoryQuery(customerId, searchText, type).ExecuteTypedList<Category>();

           return new MultipleResult<Category>(categories);
        }

        public static PagedResult<Category> GetSecondaryCategoriesPaged(int customerId, string searchText, CategoryType type, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery categories = GetSecondaryCategoryQuery(customerId, searchText, type);

            return new PagedResult<Category>(categories, pageIndex, pageSize, orderBy, orderDir);
        }

        public static PagedResult<Category> GetCategoriesPaged(int customerId, string searchText, CategoryType type, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery categoryQuery = GetCategoryQuery(customerId, searchText, type);

            return new PagedResult<Category>(categoryQuery, pageIndex, pageSize, orderBy, orderDir);
        }

        private static List<Category> SelectCategories(string sqlSubSelect, string searchText)
        {
            string sql = string.Format(@"
               select 
                    ct.ID as Id,
                    ct.CustomerID,
                    ct.Name,
                    ct.Description,
                    ct.CreatedOn,
                    ct.ModifiedOn,
                    ct.CreatedBy,
                    ct.ModifiedBy,
                    ct.CategoryTypeID,
                    ct.CreatedByUserId,
                    ct.ModifiedByUserId,
                    ct.CreatedByActualUserId,
                    ct.ModifiedByActualUserId,
                    ct.ParentCategoryID,
                    ct.MaxCourseWork,
                    p.LevelIndentText
                from (
                    {0}
                ) ct
                cross apply fGetCategoryHierarchyPath(ct.Id) p
                where p.levelIndentText like '%{1}%'
            ",
            sqlSubSelect, searchText);

            return new CodingHorror().ExecuteTypedList<Category>(sql);
        }

        public static List<Category> GetRelativeCategories(int customerId, int categoryId, string searchText)
        {
            Data.Category category = new Data.Category(categoryId);

            // TODO: Fix this - this class extends from symphony controller but all 
            // of these functions are static. Doing this for now to avoid many changes
            SymphonyController symphonyController = new SymphonyController();
            if (symphonyController.CustomerID != category.CustomerID &&
                !symphonyController.UserIsSalesChannelAdmin &&
                !symphonyController.HasRole(Roles.CourseAssignmentAdministrator)
            )
            {
                throw new Exception("You do not have access to this customer's categories.");
            }

            string categorySubSelect = string.Format(@"
                    select c.* from Category c
                    where c.CategoryTypeID = {0}
                    and c.CustomerID = {1}
                ", category.CategoryTypeID, category.CustomerID);

            return SelectCategories(categorySubSelect, searchText);
        }

        public static List<Category> GetCategoriesCrossCustomer(int[] categoryIds, CategoryType type, string searchText = "")
        {
            return GetCategories(0, categoryIds, type, searchText);
        }

        public static List<Category> GetCategoriesForCustomer(int customerId, int[] categoryIds, CategoryType type, string searchText = "")
        {
            return GetCategories(customerId, categoryIds, type, searchText);
        }

        private static List<Category> GetCategories(int customerId, int[] categoryIds, CategoryType type, string searchText = "")
        {
            string categorySubSelect = string.Format(@"
                    select c.* from Category c
                    where c.Id in ({0})
                    and c.CategoryTypeID = {1}", string.Join(",", categoryIds), (int)type);

            if (customerId > 0) {
                categorySubSelect += string.Format(" and c.CustomerID = {0}", customerId);
            }

            return SelectCategories(categorySubSelect, searchText);
        }

        public static SingleResult<Category> GetCategory(int customerId, int categoryId, CategoryType type)
        {
            Category category = CacheController.Get<Category>(categoryId);

            if (category == null)
            {
                List<Category> categories = GetCategories(customerId, new int[] { categoryId }, type);

                if (categories.Count == 0)
                {
                    categories.Add(new Category());
                }

                category = categories[0];

                CacheController.Add<Category>(category.Id, category);
            }

            return new SingleResult<Category>(category);
        }
        public static SingleResult<Category> SaveCategory(int customerId, int categoryId, CategoryType type, Category category)
        {
            Data.Category categoryData = new Data.Category(categoryId);

            category.CategoryTypeID = (int)type;

            if (GetDuplicateSibling(category, customerId).Count > 0)
            {
                List<Category> similarSiblings = GetSimilarSiblings(category, customerId);

                string names = string.Join(" ", similarSiblings.Select(c => c.Name).ToArray());

                MatchCollection matches = Regex.Matches(names, string.Format(@"{0}(\s\(([0-9]+)\))*", Regex.Escape(category.Name)));

                int max = 1;

                foreach (Match match in matches)
                {
                    if (match.Groups[2].Success)
                    {
                        int i;
                        if (int.TryParse(match.Groups[2].Value, out i))
                        {
                            if (i >= max)
                            {
                                max = i + 1;
                            }
                        }
                    }
                }

                category.Name = string.Format("{0} ({1})", category.Name, max);
            }

            if (categoryId != 0) {
                if (categoryData.CategoryTypeID != (int)type)
                {
                    throw new Exception("Category does not match type specified.");
                }

                if (categoryData.CustomerID != customerId)
                {
                    throw new Exception("You do not have access to this category.");
                }
            }

            category.CopyTo(categoryData);
            categoryData.CustomerID = customerId;

            categoryData.Save();

            return CategoryController.GetCategory(customerId, categoryData.Id, type);
        }

        public static SingleResult<Category> DeleteCategory(int customerId, int categoryId, CategoryType type)
        {
            SingleResult<Category> category = GetCategory(customerId, categoryId, type);

            if (category == null)
            {
                throw new Exception("Category does not exist.");
            }

            List<Category> children = Select.AllColumnsFrom<Data.Category>()
                    .Where(Data.Category.ParentCategoryIDColumn).IsEqualTo(categoryId)
                    .ExecuteTypedList<Category>();

            if (children.Count > 0)
            {
                throw new Exception("Categories cannot be deleted if they contain sub-categories");
            }

            switch (type)
            {
                case CategoryType.Artisan:
                    List<ArtisanCourse> artisanCourses = Select.AllColumnsFrom<Data.ArtisanCourse>()
                        .Where(Data.ArtisanCourse.CategoryIDColumn).IsEqualTo(categoryId)
                        .ExecuteTypedList<ArtisanCourse>();

                    if (artisanCourses.Count > 0)
                    {
                        throw new Exception("This category cannot be deleted due to having artisan courses associated with it.");
                    }
                    break;
                case CategoryType.ClassroomCourse:
                    List<Course> courses = Select.AllColumnsFrom<Data.Course>()
                        .Where(Data.Course.CategoryIDColumn).IsEqualTo(categoryId)
                        .ExecuteTypedList<Course>();

                    if (courses.Count > 0)
                    {
                        throw new Exception("This category cannot be deleted due to having courses associated with it.");
                    }
                    break;
                case CategoryType.OnlineCourse:
                    List<Data.OnlineCourse> onlineCourses = Select.AllColumnsFrom<Data.OnlineCourse>()
                        .Where(Data.OnlineCourse.CategoryIDColumn).IsEqualTo(categoryId)
                        .ExecuteTypedList<Data.OnlineCourse>();

                    if (onlineCourses.Count > 0)
                    {
                        throw new Exception("This category cannot be deleted due to having online courses associated with it.");
                    }

                    break;
                case CategoryType.TrainingProgram:
                    List<Data.TrainingProgram> trainingPrograms = Select.AllColumnsFrom<Data.TrainingProgram>()
                        .Where(Data.TrainingProgram.CategoryIDColumn).IsEqualTo(categoryId)
                        .ExecuteTypedList<Data.TrainingProgram>();

                    if (trainingPrograms.Count > 0)
                    {
                        throw new Exception("This category cannot be deleted due to having training programs associated with it.");
                    }
                    break;
            }

            Data.Category.Delete(categoryId);

            return category;
        }

        public static Category CopyCategory(int customerId, CategoryType toType, int categoryId)
        {
            Category category = Select.AllColumnsFrom<Data.Category>().Where(Data.Category.IdColumn).IsEqualTo(categoryId).ExecuteSingle<Category>();

            if (category == null)
            {
                return null;
            }
            
            return CopyCategory(customerId, toType, category);
        }

        public static Category CopyCategory(int customerId, CategoryType toType, Category category)
        {

            List<Category> parentCategories = new List<Category>();
            Category c = category;
            parentCategories.Add(c);

            while (c.ParentCategoryID > 0)
            {
                c = Select.AllColumnsFrom<Data.Category>()
                        .Where(Data.Category.IdColumn).IsEqualTo(c.ParentCategoryID)
                        .ExecuteSingle<Category>();

                parentCategories.Insert(0, c);
            }

            int currentCategoryId = 0;
            foreach (Category p in parentCategories)
            {
                Category existing = Select.AllColumnsFrom<Data.Category>()
                    .Where(Data.Category.NameColumn).IsEqualTo(p.Name)
                    .And(Data.Category.CategoryTypeIDColumn).IsEqualTo((int)toType)
                    .And(Data.Category.ParentCategoryIDColumn).IsEqualTo(currentCategoryId)
                    .And(Data.Category.CustomerIDColumn).IsEqualTo(customerId)
                    .ExecuteSingle<Category>();

                if (existing != null)
                {
                    currentCategoryId = existing.Id;
                }
                else
                {
                    Data.Category newCategory = new Data.Category();
                    p.CopyTo(newCategory);
                    newCategory.Id = 0;
                    newCategory.ParentCategoryID = currentCategoryId;
                    newCategory.CategoryTypeID = (int)toType;
                    newCategory.CustomerID = customerId;
                    newCategory.Save();

                    currentCategoryId = newCategory.Id;
                }
            }

            return GetCategory(customerId, currentCategoryId, toType).Data;


        }

    }
}
