﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Symphony.Core.Models;
using System.Configuration;
using SubSonic;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Transactions;

namespace Symphony.Core.Controllers
{
    public class TrainingProgramFeedbackController : Controllers.SymphonyController
    {
        public static SingleResult<TrainingProgramFeedback> SaveTrainingProgramFeedback(int trainingProgramID, TrainingProgramFeedback feedback)
        {
            feedback.UserID = SymphonyController.GetUserID();
            
            Data.TrainingProgramFeedback dFeedback = new Data.TrainingProgramFeedback();
            feedback.CopyTo(dFeedback);
            dFeedback.Save();

            return new SingleResult<TrainingProgramFeedback>(feedback);
        }

        public static MultipleResult<TrainingProgramFeedback> GetFeedbackForTrainingProgram(int trainingProgramId)
        {
            return _GetFeedback(trainingProgramId, -1);
        }

        public static MultipleResult<TrainingProgramFeedback> GetFeedbackForTrainingProgramForUser(int trainingProgramId)
        {
            int userId = SymphonyController.GetUserID();

            return _GetFeedback(trainingProgramId, userId);
        }

        private static MultipleResult<TrainingProgramFeedback> _GetFeedback(int trainingProgramId, int userID)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.TrainingProgramFeedback>()
                .Where(Data.TrainingProgramFeedback.Columns.TrainingProgramID).IsEqualTo(trainingProgramId);

            if (userID != -1)
            {
                query.And(Data.TrainingProgramFeedback.Columns.UserID).IsEqualTo(userID);
            }

            return new MultipleResult<TrainingProgramFeedback>(query);
        }

        public static SingleResult<TrainingProgramFeedbackSummary> GetTrainingProgramFeedbackSummary(int trainingProgramId)
        {
            MultipleResult<TrainingProgramFeedback> feedback = _GetFeedback(trainingProgramId, -1);
            int scoreSum = 0;

            var tpfbSummary = new TrainingProgramFeedbackSummary();
            tpfbSummary.TrainingProgramID = trainingProgramId;
            tpfbSummary.FeedbackTotal = feedback.Data.Count();
            tpfbSummary.RecommendedTotal = 0;

            foreach(TrainingProgramFeedback fb in feedback.Data){
                if(fb.Recommended){
                    ++tpfbSummary.RecommendedTotal;
                }

                scoreSum += fb.Score;
            }

            tpfbSummary.ScoreAverage = scoreSum / (float)tpfbSummary.FeedbackTotal;

            return new SingleResult<TrainingProgramFeedbackSummary>(tpfbSummary);
        }
    }
}
