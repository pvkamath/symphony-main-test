﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Caching;

namespace Symphony.Core.Controllers
{
    public class CacheController
    {
        private static CacheItemPolicy ShortAbsoluteExpiration = new CacheItemPolicy
        {
            AbsoluteExpiration = DateTime.UtcNow.AddMinutes(2)
        };

        private static MemoryCache cache = MemoryCache.Default;

        public static string GetKey(string identifier, Type type)
        {
            return String.Format("{0}-{1}-{2}", type.ToString(), SymphonyController.GetCustomerID().ToString(), identifier);
        }

        /// <summary>
        /// Adds or Updates the specified object to the cache. The identifier will be prefixed with the type of the object.
        /// </summary>
        /// <typeparam name="T">Type of object add to the cache</typeparam>
        /// <param name="identifier">A unique identifier for this object, will be prefixed with object type</param>
        /// <param name="obj">The object to add</param>
        public static void Add<T>(int identifier, object obj) where T : class
        {
            Add<T>(identifier.ToString(), obj);
        }

        /// <summary>
        /// Adds or Updates the specified object to the cache. The identifier will be prefixed with the type of the object.
        /// </summary>
        /// <typeparam name="T">Type of object add to the cache</typeparam>
        /// <param name="identifier">A unique identifier for this object, will be prefixed with object type and customer id</param>
        /// <param name="obj">The object to add</param>
        public static void Add<T>(string identifier, object obj) where T : class
        {
            cache.Set(GetKey(identifier, typeof(T)), obj, ShortAbsoluteExpiration);
        }

        /// <summary>
        /// Adds or Updates the specified value to the cache. The identifier will be prefixed with the type of the object.
        /// </summary>
        /// <typeparam name="T">Type of object add to the cache</typeparam>
        /// <param name="identifier">A unique identifier for this object, will be prefixed with object type and customer id</param>
        /// <param name="obj">The value to add</param>
        public static void AddValue<T>(string identifier, object obj) where T : struct
        {
            cache.Set(GetKey(identifier, typeof(T)), obj, ShortAbsoluteExpiration);
        }

        /// <summary>
        /// Retrieves an object from the cache. The identifier will be prefixed with the type of the object
        /// </summary>
        /// <typeparam name="T">Type of object to retrieve from the cache</typeparam>
        /// <param name="identifier">identifier to retrieve from the cache, will be prefixed with object type and customer id</param>
        /// <returns></returns>
        public static T Get<T>(int identifier) where T : class
        {
            return Get<T>(identifier.ToString());
        }

        /// <summary>
        /// Retrieves an object from the cache. The identifier will be prefixed with the type of the object
        /// </summary>
        /// <typeparam name="T">Type of object to retrieve from the cache</typeparam>
        /// <param name="identifier">identifier to retrieve from the cache, will be prefixed with object type and customer id</param>
        /// <returns></returns>
        public static T Get<T>(string identifier) where T : class
        {
            try
            {
                return (T)cache.Get(GetKey(identifier, typeof(T)));
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Retrieves a value from the cache. The identifier will be prefixed with the type of the object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public static T GetValue<T>(string identifier) where T : struct
        {
            try
            {
                return (T)cache.Get(GetKey(identifier, typeof(T)));
            }
            catch
            {
                return default(T);
            }
        }
    }
}
