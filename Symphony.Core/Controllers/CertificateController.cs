﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using log4net;
using SubSonic;
using Symphony.Core.Models;
using CertificateTemplateData = Symphony.Core.Data.CertificateTemplate;
using System.Reflection;
using Newtonsoft.Json;
using log4net;

namespace Symphony.Core.Controllers
{
    /// <summary>
    /// Certificate controller is a domain service that performs CRUD operations on certificate
    /// templates and handles the generation of certificates to display to users.
    /// </summary>
    public class CertificateController : SymphonyController
    {
        static ILog Log = LogManager.GetLogger(typeof(CertificateController));

        /// <summary>
        /// Finds the certificate template with an id equal to the id parameter.
        /// </summary>
        /// <param name="id">
        /// Id of the certificate template to return.
        /// </param>
        /// <returns>
        /// Certificate template that matches the provided id, wrapped in a SingleResult object. If no
        /// matching entity is found, then null will be wrapped instead.
        /// </returns>
        public SingleResult<CertificateTemplate> FindCertificateTemplate(int id)
        {
            SqlQuery query = null;

            try
            {
                Log.InfoFormat("Performing operation to find a certificate template with id {0}.", id);

                query = Select.AllColumnsFrom<CertificateTemplateData>()
                    .Where(CertificateTemplateData.Columns.Id).IsEqualTo(id)
                    .AndExpression(CertificateTemplateData.Columns.IsDeleted).IsEqualTo(0);

                var template = query.ExecuteSingle<CertificateTemplate>();
                if (template == null)
                {
                    Log.DebugFormat("Operation to find a certificate template with id {0} returned null.", id);
                }

                return new SingleResult<CertificateTemplate>(template);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to find a certificate template: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to find a certificate template.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Queries the database for the set of certificate templates that match the specified query
        /// parameters.
        /// </summary>
        /// <param name="queryParams">
        /// Parameter object defining what certificates templates to search for.
        /// </param>
        /// <returns>
        /// The set of certificate templets that match the query parameters, wrapped in a PagedResult
        /// object. If no matching entities are found, then an empty array will be returned.
        /// </returns>
        public MultipleResult<CertificateTemplate> QueryCertificateTemplates()//PagedQueryParams<CertificateTemplate> queryParams)
        {
            SqlQuery query = null;

            try
            {
                Log.Info("Performing operation to query certificate templates.");

                query = Select.AllColumnsFrom<Data.CertificateTemplateHierarchy>()
                    .Where(Data.CertificateTemplateHierarchy.Columns.IsDeleted).IsEqualTo(0);
				// temp remove while testing the hierarchy stuff
                //if (queryParams != null && !string.IsNullOrWhiteSpace(queryParams.SearchText))
                //{
                //    query.AndExpression(CertificateTemplateData.Columns.Name).ContainsString(queryParams.SearchText)
                //        .OrExpression(CertificateTemplateData.Columns.Description).ContainsString(queryParams.SearchText);
                //}

                //Log.DebugFormat("queryParams: {0}", queryParams.ToString());

                var res = new MultipleResult<CertificateTemplate>(query);//, queryParams.PageIndex, queryParams.PageSize, queryParams.OrderBy, queryParams.OrderDirection);
                return res;
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query certificate templates: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query certificate templates.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        public MultipleResult<CertificateTemplate> GetCertificateTemplates()
        {
            SqlQuery query = null;

            try
            {
                query = Select.AllColumnsFrom<CertificateTemplateData>()
                    .Where(CertificateTemplateData.Columns.IsDeleted).IsEqualTo(false);

                return new MultipleResult<CertificateTemplate>(query);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query certificate templates: " + query.ToString(), ex);
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query certificate templates.", ex);
                throw;
            }
        }

        /// <summary>
        /// Creates a new certificate template with the provided model.
        /// </summary>
        /// <param name="model">
        /// Model to use when creating the certificate template. The model's ID value must be null or 0.
        /// </param>
        /// <returns>
        /// A model representing the new certificate template. The data will be identical to the
        /// original model, except that it will have a new database id.
        /// </returns>
        public SingleResult<CertificateTemplate> CreateCertificateTemplate(CertificateTemplate model)
        {
            QueryCommand cmd = null;

            try
            {
                Log.Info("Performing operation to create a certificate template.");
                 
                if (!UserIsSalesChannelAdmin)
                {
                    throw new UnauthorizedAccessException("You are not authorized to create a certificate template.");
                }
                if (model.ID.HasValue && model.ID > 0)
                {
                    throw new ArgumentException("Cannot create a certificate template when model data already has an associated id.");
                }

                var data = new CertificateTemplateData();
                model.CopyTo(data);

                cmd = data.GetInsertCommand(Username);
                data.Save(Username);

                Log.DebugFormat("New certificate template created with id {0}.", data.Id);
                model.ID = data.Id;

                return new SingleResult<CertificateTemplate>(model);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to create a certificate template: " + cmd.CommandSql, ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to create a certificate template.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Updates an existing certificate template with the provided model.
        /// </summary>
        /// <param name="model">
        /// Model to use when updating the certificate template. The model should already have an
        /// id associated with it.
        /// </param>
        /// <returns>
        /// A model representing the existing certificate template with updated data.
        /// </returns>
        public SingleResult<CertificateTemplate> UpdateCertificateTemplate(CertificateTemplate model)
        {
            QueryCommand cmd = null;

            try
            {
                Log.InfoFormat("Performing operation to update a certificate template with id {0}", model.ID);

                if (!UserIsSalesChannelAdmin)
                {
                    throw new UnauthorizedAccessException("You are not authorized to update a certificate template.");
                }
                if (model.ID == null || model.ID == 0)
                {
                    throw new ArgumentException("Cannot update a certificate template when model data does not contain an id.");
                }

                var data = new CertificateTemplateData(model.ID);
                model.CopyTo(data);

                cmd = data.GetUpdateCommand(Username);
                data.Save(Username);

                Log.InfoFormat("Updated certificate template with id {0}.", data.Id);

                return new SingleResult<CertificateTemplate>(model);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to update a certificate template: " + cmd.CommandSql, ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to update a certificate template.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Deletes an existing certificate template. Certificate templates are soft-deleting to
        /// avoid issues when deleting in-use certificates.
        /// </summary>
        /// <param name="model">
        /// Model representing the certificate template to delete. The ID of this model must be
        /// non-null and greater than zero.
        /// </param>
        /// <returns>
        /// A model representing the deleted certificate template.
        /// </returns>
        public SingleResult<CertificateTemplate> DeleteCertificateTemplate(CertificateTemplate model)
        {
            QueryCommand cmd = null;

            try
            {
                Log.InfoFormat("Performing operation to soft delete an affidavit with id {0}", model.ID);

                if (!UserIsSalesChannelAdmin)
                {
                    throw new UnauthorizedAccessException("You are not authorized to delete a certificate template.");
                }
                if (model.ID == 0)
                {
                    throw new ArgumentException("Cannot delete an affidavit when model data does not contain an id.");
                }

                model.IsDeleted = true;

                var data = new CertificateTemplateData(model.ID);
                model.CopyTo(data);

                cmd = data.GetUpdateCommand(Username);
                data.Save(Username);

                Log.InfoFormat("Deleted certificate template with id {0}.", model.ID);

                return new SingleResult<CertificateTemplate>(model);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to delete a certificate template: " + cmd.CommandSql, ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to delete a certificate template.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        public SimpleSingleResult<string> GenerateCourseCertificate(int courseId, int userId, bool isPreview = false)
        {
            var userData = new Data.User(userId);
            var courseData = new Data.OnlineCourse(courseId);
            var customerData = new Data.Customer(courseData.CustomerID);

            var user = new User();
            var course = new Course();
            user.CopyFrom(userData);
            course.CopyFrom(courseData);

            if (!course.CertificateEnabled.GetValueOrDefault())
            {
                throw new Exception("Cannot generate certificate; certificate is disabled for course.");
            }
            if (course.CertificateTemplateId == null)
            {
                throw new Exception("Cannot generate certificate; no certificate selected for course.");
            }

            var template = FindCertificateTemplate(course.CertificateTemplateId.Value).Data;

            var c = NotificationTemplateController.Parse(template.Content, new object[] { user, course });

            return new SimpleSingleResult<string>(c);
        }

        public SimpleSingleResult<string> GenerateTrainingProgramCertificate(int trainingProgramId, int userId, bool isPreview = false)
        {
            return new SimpleSingleResult<string>("<b>test</b>");
        }

        public const string DEFAULT_FOLDER = "Default";
        public const string DEFAULT_PATH = "/Certificates/" + DEFAULT_FOLDER;

        public static string CustomerDefaultPath(string customerSubdomain)
        {
            return DEFAULT_PATH.Replace(DEFAULT_FOLDER, customerSubdomain + "/" + DEFAULT_FOLDER);
        }

        public static string GetRelativeTemplatePath(string absoluteTemplatePath, string customerSubdomain)
        {
            absoluteTemplatePath = absoluteTemplatePath.Replace('\\', '/');
            string defaultAbsolutePath = HttpContext.Current.Server.MapPath(DEFAULT_PATH).Replace('\\', '/'); ;
            string defaultAbsoluteCustomerPath = HttpContext.Current.Server.MapPath(CustomerDefaultPath(customerSubdomain)).Replace('\\', '/');

            //HttpContext.Current.Response.Write(absoluteTemplatePath + "<br/>");
            //HttpContext.Current.Response.Write(defaultAbsolutePath + "<br/>");
            //HttpContext.Current.Response.Write(defaultAbsoluteCustomerPath + "<br/>");

            string relativePath = absoluteTemplatePath
                                        .Replace(Path.GetFileName(absoluteTemplatePath), "")
                                        .Replace(defaultAbsolutePath, "")
                                        .Replace(defaultAbsoluteCustomerPath, "")
                                        .Replace('\\', '/');

            if (relativePath[0] == '/')
            {
                relativePath = relativePath.Substring(1);
            }

            return relativePath;
        }


        private string GetWebPath(string rootPath, string directory)
        {
            string path = directory.Replace(rootPath, "");

            if (path[0] != '\\')
            {
                path = @"\" + path;
            }

            return path;
        }

        public MultipleResult<CertificateTemplate> GetCertificates()
        {
            string certificatePath = HttpContext.Current.Server.MapPath(DEFAULT_PATH);
            string rootPath = certificatePath.Replace(Path.Combine("Certificates", "Default"), "");


            CertificateTemplate rootCertificate = new CertificateTemplate()
            {
                ID = 1,
                Name = "Default",
                Path = GetWebPath(rootPath, certificatePath),
                ParentID = 0
            };

            rootCertificate.LevelIndentText = rootCertificate.Name;

            List<CertificateTemplate> certificates = new List<CertificateTemplate>();

            certificates.Add(rootCertificate);

            BuildCertificateNodes(rootPath, rootCertificate.Name, certificates, certificatePath, 1);

            return new MultipleResult<CertificateTemplate>(certificates);
        }



        private void BuildCertificateNodes(string rootPath, string levelIndentText, List<CertificateTemplate> certificates, string path, int parentId)
        {
            foreach (string dir in Directory.GetDirectories(path))
            {
                CertificateTemplate node = new CertificateTemplate()
                {
                    ID = certificates.Count + 1,
                    Name = Path.GetFileName(dir),
                    Path = GetWebPath(rootPath, dir),
                    ParentID = parentId
                };

                node.LevelIndentText = levelIndentText + " > " + node.Name;

                certificates.Add(node);

                BuildCertificateNodes(rootPath, node.LevelIndentText, certificates, dir, node.ID.Value);
            }
        }

        public Certificate GetCertificate(CertificateType certificateType, int trainingProgramId, int courseOrClassId, int userId, string scoreOverride = null, int primaryLicenseIdOverride = 0, bool isPreview = false)
        {

            PortalController portalController = new PortalController();
            ClassroomController classroomController = new ClassroomController();
            CustomerController customerController = new CustomerController();

            Certificate certificate = new Certificate()
            {
                User = new User(),
                TrainingProgram = new TrainingProgram(),
                Class = new ClassroomClass(),
                TranscriptEntry = new TranscriptEntry(),
                Course = new Course(),
                Customer = new Customer(),
                CustomerLocation = new Location(),
                UserLocation = new Location(),
                TrainingProgramMeta = new Dictionary<string, string>(),
                CourseMeta = new Dictionary<string, string>(),
                ClassMeta = new Dictionary<string, string>(),
                UserDataFields = new List<UserDataField>(),
                LicenseDataFieldNames = new List<string>(),
                LicenseDataFieldLabels = new List<string>(),
                LicenseUserFieldNames = new List<string>(),
                LicenseUserFieldLabels = new List<string>(),
                LicenseDataMeta = new Dictionary<string, string>(),
                LicenseUserMeta = new Dictionary<string, string>()
            };
            

            User user = new UserController().GetUser(userId);

            if (trainingProgramId > 0)
            {
                certificate.TrainingProgram = portalController.GetTrainingProgramDetails(userId, trainingProgramId, false).Data;

                if (primaryLicenseIdOverride > 0)
                {
                    certificate.TrainingProgram.PrimaryLicenseIdOverride = primaryLicenseIdOverride;
                }

                Regex reg = new Regex("[^a-zA-Z0-9]");

                List<Data.UserDataFieldUserMap> userSpecifiedDataFields = Select
                    .AllColumnsFrom<Data.UserDataFieldUserMap>()
                    .Where(Data.UserDataFieldUserMap.Columns.UserID).IsEqualTo(userId)
                    .ExecuteTypedList<Data.UserDataFieldUserMap>();


                Dictionary<string, UserDataField> userDataFields = new Dictionary<string, UserDataField>();
                int orderCount = 0;

                if (certificate.TrainingProgram.Licenses.Where(l => l.Id == primaryLicenseIdOverride).ToList().Count == 0)
                {
                    Data.TrainingProgramLicense licenseOverrideData = new Data.TrainingProgramLicense(primaryLicenseIdOverride);

                    TrainingProgramLicense licenseOverride = new TrainingProgramLicense();
                    licenseOverride.CopyFrom(licenseOverrideData);
                    licenseOverride.Id = primaryLicenseIdOverride;

                    certificate.TrainingProgram.Licenses.Add(licenseOverride);
                }


                foreach (License license in certificate.TrainingProgram.Licenses)
                {
                    List<UserDataField> dataFields = new List<UserDataField>();

                    if (string.IsNullOrEmpty(license.CertificateFormFieldJson) && license.Id > 0)
                    {
                        // This will fill in the certificate field json for a license if it doesn't exist (Old license that doesn't have this property saved yet)
                        license.CertificateFormFieldJson = LicenseController.GetLicense(license.Id).Data.CertificateFormFieldJson;
                    }

                    if (!string.IsNullOrEmpty(license.CertificateFormFieldJson))
                    {
                        try
                        {
                            UserDataFieldForm form = JsonConvert.DeserializeObject<UserDataFieldForm>(license.CertificateFormFieldJson);

                            foreach (UserDataField field in form.Items)
                            {
                                if (field.Type == (int)FieldType.Label)
                                {
                                    field.CodeName = field._Name;
                                }

                                if (!userDataFields.ContainsKey(field.CodeName))
                                {
                                    field.Order = orderCount;
                                    

                                    if (string.IsNullOrEmpty(field.Xtype))
                                    {
                                        field.Xtype = "textfield";
                                    }

                                    userDataFields.Add(field.CodeName, field);

                                    switch (field.Type) {
                                        // Backwards compatibility - old certs might use these arrays
                                        // They could be updated to point to certificate.UserDataFields instead
                                        // as that contains everything. 
                                        //
                                        // Also, apply the value to the field if one exists
                                        case (int)FieldType.SymphonyDataField:
                                            // Backwards compat
                                            certificate.LicenseDataFieldNames.Add(field.CodeName);
                                            certificate.LicenseDataFieldLabels.Add(field.DisplayName);

                                            // Set the value
                                            Data.UserDataField fieldData = new Data.UserDataField(Data.UserDataField.Columns.CodeName, field.CodeName);
                                            if (fieldData.Id > 0)
                                            {
                                                var userDataField = userSpecifiedDataFields.Where(x => x.UserDataFieldID == fieldData.Id).FirstOrDefault();
                                                if (userDataField != null) 
                                                {
                                                    field.Value = userDataField.UserValue;
                                                }
                                            }
                                            break;
                                        case (int)FieldType.SymphonyUserField:
                                            // Backwards compat
                                            certificate.LicenseUserFieldNames.Add(field.CodeName);
                                            certificate.LicenseUserFieldNames.Add(field.DisplayName);
                                            // Set the value
                                            // Make sure the first letter is upper case to ensure reflection
                                            // will pull the field from the user model
                                            field.Value = user.GetLicenseFieldValue(field.CodeName);
                                            break;
                                    }

                                    orderCount++;
                                }
                            }
                        }
                        catch(Exception e)
                        {
                            Log.Error("Could not generate form fields for the license: " + license.Id, e);
                        }
                    }

                    var userLicenseNumber = SubSonic.Select.AllColumnsFrom<Core.Data.UserLicenseNumber>()
                            .Where(Core.Data.UserLicenseNumber.Columns.UserID).IsEqualTo(userId)
                            .And(Core.Data.UserLicenseNumber.Columns.LicenseID).IsEqualTo(license.Id)
                            .ExecuteSingle<Core.Data.UserLicenseNumber>();

                    if (userLicenseNumber != null)
                    {
                        license.UserLicenseNumber = userLicenseNumber.LicenseNumber;
                    }
                }

                certificate.UserDataFields = userDataFields.Select(kv => kv.Value).OrderBy(f => f.Order).ToList();
                certificate.UserDataFields.ForEach(f =>
                {
                    if (f.Type == (int)FieldType.SymphonyDataField || f.Type == (int)FieldType.SymphonyUserField)
                    {
                        string prefix = f.Type == (int)FieldType.SymphonyDataField ?
                            "data-" :
                            "user-";
                        Dictionary<string, string> metaDictionary = f.Type == (int)FieldType.SymphonyDataField ?
                            certificate.LicenseDataMeta :
                            certificate.LicenseUserMeta;

                        // Prefixing code names to ensure when posting the data there
                        // will be no conflicts betwen data fields and user fields.
                        f.CodeName = prefix + f.CodeName;

                        // Adding field values to LicenseDataMeta and LicenseUserMeta for 
                        // backwards compatibiliity if any existing templates use these
                        if (!string.IsNullOrEmpty(f.Value))
                        {
                            metaDictionary.Add(f.CodeName, f.Value);
                        }
                    }
                });

                if (certificate.TrainingProgram.HasLicenses && certificate.TrainingProgram.PrimaryLicense.HasUserLicenseNumber) {
                    certificate.UserDataFields.Insert(0, new UserDataField()
                    {
                        Xtype = "textfield",
                        Value = certificate.TrainingProgram.PrimaryLicense.UserLicenseNumber,
                        CodeName = "licensenumber",
                        FieldLabel = "License Number"
                    });
                }
            }


            certificate.User = user;
            certificate.Customer.CopyFrom(new Data.Customer(certificate.User.CustomerID));
            certificate.Type = certificateType;

            if (certificate.User.LocationID > 0)
            {
                certificate.UserLocation.CopyFrom(new Data.Location(certificate.User.LocationID));
            }

            if (certificate.Customer.LocationID > 0)
            {
                certificate.CustomerLocation.CopyFrom(new Data.Location(certificate.Customer.LocationID));
            }

            List<TranscriptEntry> userTranscript = portalController.GetTranscript(userId).Data;


            switch (certificateType)
            {
                case CertificateType.Classroom:
                    certificate.Class = classroomController.GetClass(certificate.Customer.ID, certificate.User.ID, courseOrClassId).Data;
                    certificate.Course = classroomController.GetCourse(certificate.Customer.ID, certificate.User.ID, certificate.Class.CourseID).Data;

                    List<TranscriptEntry> entries = userTranscript.Where<TranscriptEntry>(t => t.CourseTypeID == (int)CourseType.Classroom &&
                                        t.CourseID == certificate.Course.Id &&
                                        t.ClassID == certificate.Class.Id).ToList<TranscriptEntry>();

                    if (entries.Count == 0)
                    {
                        throw new Exception("It appears this course has not yet been completed.");
                    }

                    certificate.TranscriptEntry = entries[0];

                    certificate.CourseType = CourseType.Classroom;

                    break;
                case CertificateType.Online:
                    certificate.Course.CopyFrom(new Data.OnlineCourse(courseOrClassId));
                    if (trainingProgramId > 0)
                    {
                        // entry that applies is the most recent attempt for that course within the TP range
                        Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);
                        if (tp.IsNewHire)
                        {
                            // new hires just match up directly with the requested
                            certificate.TranscriptEntry = userTranscript
                                .OrderByDescending(t => t.AttemptDate)
                                .Where(t => t.CourseID == certificate.Course.Id).ToList()[0];
                        }
                        else
                        {
                            // Since any course assigned to a training program with the 
                            // same date range as this training program counts, this course
                            // certificate may appear on a training program where the user
                            // has not taken the course. (Or the score may be different since
                            // we always take the highest score) Grab the highest score for
                            // this course where a training program has been assigned. 
                            // If there are none, assume incomplete.
                            List<TranscriptEntry> onlineCourseEntries = userTranscript
                                .OrderByDescending(t => t.Score)
                                .Where(t => 
                                      t.CourseTypeID == (int)CourseType.Online && 
                                      t.CourseID == certificate.Course.Id &&
                                      CourseController.OnlineCourseCountsForTP(tp, t) &&
                                      t.TrainingProgramID > 0
                                ).ToList();

                           
                            if (onlineCourseEntries.Count == 0)
                            {
                                throw new Exception("Your certificate will be available when you complete the course.");
                            }

                            certificate.TranscriptEntry = onlineCourseEntries[0];
                        }
                    }
                    else
                    {
                        List<TranscriptEntry> onlineCourseNoTpEntries = userTranscript
                            .OrderByDescending(t => t.Score)
                            .Where(t => 
                                t.CourseID == certificate.Course.Id
                             && t.CourseTypeID == (int)CourseType.Online
                             && t.TrainingProgramID == 0).ToList();

                        List<TranscriptEntry> matchingCourses = userTranscript.OrderByDescending(t => t.Score).Where(t => t.CourseID == certificate.Course.Id).ToList();
                        List<TranscriptEntry> matchingCourseTypes = userTranscript.OrderByDescending(t => t.Score).Where(t => t.CourseTypeID == certificate.Course.CourseTypeID).ToList();
                        List<TranscriptEntry> trainingProgramCourses = userTranscript.OrderByDescending(t => t.Score).Where(t => t.TrainingProgramID > 0).ToList();


                        if (onlineCourseNoTpEntries.Count == 0)
                        {
                            throw new Exception("Your certificate will be available when you complete the course.");
                        }

                        certificate.TranscriptEntry = onlineCourseNoTpEntries[0];
                        
                    }
                    certificate.CourseType = CourseType.Online;

                    break;
                case CertificateType.TrainingProgram:
                    // Don't normally need a transcript entry for a training program cert, but it's possible this cert is for 
                    // a historical training program where we do not have any records for the actual courses in the training program. 
                    // If this is the case then provie the certificate anyway. 
                    TranscriptEntry entry = userTranscript.Where(t => t.IsHistorical && t.TrainingProgramID == trainingProgramId).FirstOrDefault();

                    if (entry != null)
                    {
                        certificate.TranscriptEntry = entry;
                        certificate.TrainingProgram.DateCompleted = entry.DateCompleted;
                    }

                    break;
            }


            if (!string.IsNullOrEmpty(scoreOverride))
            {
                // Fake a transcript entry if there is a score override
                // this is used to pass in the score from an artisan course
                // as the transcript entry does not get generated until
                // after the course is closed.
                certificate.TranscriptEntry = new TranscriptEntry()
                {
                    CourseID = certificate.Course.Id,
                    AttemptDate = DateTime.UtcNow,
                    AttemptCount = 1,
                    EndDate = DateTime.UtcNow,
                    StartDate = DateTime.UtcNow,
                    CourseName = certificate.Course.Name,
                    DateCompleted = DateTime.UtcNow,
                    Score = scoreOverride,
                    Passed = 1,
                };
            }

            if (isPreview && certificateType == CertificateType.TrainingProgram)
            {
                certificate.TrainingProgram.DateCompleted = DateTime.UtcNow;
            }

            if (!String.IsNullOrEmpty(certificate.TrainingProgram.MetaDataJson))
            {
                try
                {
                    List<KeyValuePair<string, string>> metaData = Utilities.Deserialize<List<KeyValuePair<string, string>>>(certificate.TrainingProgram.MetaDataJson);

                    foreach (KeyValuePair<string, string> pair in metaData)
                    {
                        certificate.TrainingProgramMeta[pair.Key] = pair.Value;
                    }

                }
                catch
                {

                }
            }

            if (!String.IsNullOrEmpty(certificate.Course.MetaDataJson))
            {
                try
                {
                    List<KeyValuePair<string, string>> metaData = Utilities.Deserialize<List<KeyValuePair<string, string>>>(certificate.Course.MetaDataJson);

                    foreach (KeyValuePair<string, string> pair in metaData)
                    {
                        certificate.CourseMeta[pair.Key] = pair.Value;
                    }
                }
                catch
                {

                }
            }

            if (!String.IsNullOrEmpty(certificate.Class.MetaDataJson))
            {
                try
                {
                    List<KeyValuePair<string, string>> metaData = Utilities.Deserialize<List<KeyValuePair<string, string>>>(certificate.Class.MetaDataJson);

                    foreach (KeyValuePair<string, string> pair in metaData)
                    {
                        certificate.ClassMeta[pair.Key] = pair.Value;
                    }
                }
                catch
                {

                }
            }

            int s;
            if (int.TryParse(certificate.TranscriptEntry.Score, out s))
            {
                certificate.TranscriptEntry.Score += "%";
            }

            if (certificateType != CertificateType.TrainingProgram && certificate.TranscriptEntry != null
                && (certificate.TranscriptEntry.Completed < 1 || !certificate.TranscriptEntry.Passed.HasValue || certificate.TranscriptEntry.Passed.Value < 1))
            {
                throw new Exception("Sorry, your certificate will not be available until you pass the course.");
            }
            else if (certificateType == CertificateType.TrainingProgram && !certificate.TrainingProgram.Completed && !isPreview && !certificate.TranscriptEntry.IsHistorical)
            {
                throw new Exception("Sorry, your certificate will not be available until you complete the training program.");
            }

            return certificate;
        }

        public CertificateAsset UploadAssets(string filename, string name, HttpPostedFile file)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = Path.GetFileNameWithoutExtension(filename);
            }

            // use the HTTPContext to get any passed in params.
            var ca = new CertificateAsset()
            {
                Id = 0,
                Filename = filename,
                Name = name
            };

            // save the data:
            ca = AddAsset(ca);

            var path = Path.Combine(GetAssetsPath(), ca.Id.ToString() + Path.GetExtension(filename));
            byte[] fileData = null;
            
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }
            
            try
            {
                string directory = Path.GetDirectoryName(path);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw e;
            }

            File.WriteAllBytes(path, fileData);

            return ca;
        }

        public MultipleResult<CertificateAsset> GetAssetList()
        {
            // if the dir exists for this cert, return any files in it
            var path = GetAssetsPath();
            var assetsList = new List<CertificateAsset>();

            SqlQuery q = Select.AllColumnsFrom<Data.CertificateAsset>()
                .Where(Data.CertificateAsset.Columns.IsDeleted).IsNotEqualTo(1);

            var res = q.ExecuteTypedList<CertificateAsset>();

            return new MultipleResult<CertificateAsset>(res);
        }

        public string GetAssetsPath()
        {
            var path = CertificateAssetsDirectory;
            if (HttpContext.Current != null)
            {
                path = HttpContext.Current.Server.MapPath(path);
            }

            return path;
        }

        public CertificateAsset AddAsset(CertificateAsset ca)
        {
            Data.CertificateAsset dca = new Data.CertificateAsset();
            ca.CopyTo(dca);

            dca.Save();

            // return asset with new id...
            ca.CopyFrom(dca);

            return ca;
        }

        class CKEditorListItem
        {
            public string image;
        }

        // CKEditor imagae plugin requires a specific JSON format for listing images: [{ image: '' }[,...]]
        public List<object> GetAssetListForCKEditor()
        {
            var res = new List<object>();
            var data = GetAssetList().Data;

            foreach (CertificateAsset ca in data)
            {
                res.Add(new CKEditorListItem
                {
                    image = ca.RelativeUrl
                });
            }

            return res;
        }

        public string GeneratePreviewCertificate(CertificateTemplate tpl)
        {
            // NotificationTempalteController already has a parser, so let's reuse that.
            // first, get the objects for this template:
            List<object> listOfDummyObjects = GetDummyCertificateValues();

            var dTpl = new Data.CertificateTemplate();

            tpl.CopyTo(dTpl);

            // then pass the objects and the html to the notifications Controllers
            string generatedCertificate = NotificationTemplateController.Parse(dTpl.Content, listOfDummyObjects.ToArray());
            return generatedCertificate;
        }
        
        public string GenerateCertificate(int templateId, int userId, int trainingProgramId, Certificate certificate, Data.CertificateTemplate certificateTemplateOverride = null) 
        {
            var htmlTemplate = certificateTemplateOverride != null ? certificateTemplateOverride : new Data.CertificateTemplate(templateId);
            List<object> templateObjects = new List<object>();

            var sc = new Data.SalesChannel(certificate.Customer.SalesChannelID);
            sc.Logo = "<img src='" + sc.Logo + "'>";

            templateObjects.Add(new Data.OnlineCourse(4));
            templateObjects.Add(sc);
            templateObjects.Add(certificate);

            // then pass the objects and the html to the notifications Controllers
            string generatedCertificate = NotificationTemplateController.Parse(htmlTemplate.Content, templateObjects.ToArray());
            return generatedCertificate;
        }

        public List<object> GetDummyCertificateValues()
        {
            Dictionary<Type, object> dummyData = NotificationTemplateController.GetSampleInstances();


            var listOfDummyObjects = new List<object>();

            var sc = new Data.SalesChannel(1);
            sc.Logo = "<img src=\"" + sc.Logo + "\" />";

            listOfDummyObjects.Add(dummyData[typeof(Data.Customer)]);
            listOfDummyObjects.Add(dummyData[typeof(Data.TrainingProgram)]);
            listOfDummyObjects.Add(dummyData[typeof(Data.OnlineCourse)]);
            listOfDummyObjects.Add(dummyData[typeof(Data.User)]);
            listOfDummyObjects.Add(new Certificate());
            listOfDummyObjects.Add(sc);

            return listOfDummyObjects;
        }

        public MultipleResult<CertificateTemplate> GetAllCertificates()
        {
            var newCerts = GetCertificateTemplates().Data;
            var oldCerts = GetCertificates().Data;

            var allcerts = new List<CertificateTemplate>();

            foreach (var nc in newCerts)
            {
                var t = new CertificateTemplate();
                nc.CopyTo(t);
                t.LevelIndentText = nc.Name;
                t.isLegacy = false;
                t.CertificateTemplateId = nc.ID;

                allcerts.Add(t);
            }

            foreach (var oc in oldCerts)
            {
                var t = new CertificateTemplate();
                oc.CopyTo(t);
                t.isLegacy = true;

                allcerts.Add(t);
            }

            return new MultipleResult<CertificateTemplate>(allcerts);
        }

        public SingleResult<CertificateTemplate> ReparentTemplate(int templateId, int parentId)
        {
            var ct = new Data.CertificateTemplate(templateId);
            ct.ParentID = parentId;

            ct.Save();

            var formatted = new CertificateTemplate();
            formatted.CopyFrom(ct);

            return new SingleResult<CertificateTemplate>(formatted);
        }
    }
}
