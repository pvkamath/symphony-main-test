﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using SubSonic;
using System.Web;
using Symphony.RusticiIntegration.Core;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Net;
using HtmlAgilityPack;
using System.Security.Cryptography.X509Certificates;
using Aspose.Pdf;
using Aspose.Words;
using Aspose.Slides;
using Symphony.Core.Comparers;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Drawing2D;
using log4net;
using SmartXLS;
using System.Web.Script.Serialization;
using Quartz;
using Quartz.Impl;
using Quartz.Job;
using Symphony.Core.Jobs;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NodaTime;
using System.ServiceModel.Web;
using Symphony.Core.Extensions;
using System.Xml;
using System.Transactions;

namespace Symphony.Core.Controllers
{
    public class ReportController : SymphonyController
    {
        static ISchedulerFactory SchedulerFactory = new StdSchedulerFactory();
        static IScheduler Scheduler;
        static object ScheduleLock = new object();

        private string reportProcedureDatabase = ConfigurationManager.AppSettings["ReportProcedureDatabase"].Or("Symphony");
        private string reportProcedurePrefix = ConfigurationManager.AppSettings["ReportProcedurePrefix"].Or("report_");

        private ILog Log = LogManager.GetLogger(typeof(ReportController).Name);

        public static void EnableSchedules()
        {
            lock (SchedulerFactory)
            {
                if (Scheduler == null)
                {
                    Scheduler = SchedulerFactory.GetScheduler();
                    Scheduler.Start();
                }
            }
        }

        public SingleResult<Report> SaveReport(int customerId, int userId, int reportId, bool run, Report report)
        {
            Data.Report data = new Data.Report(reportId);
            report.CopyTo(data);
            data.Id = (reportId > 0) ? reportId : data.Id;
            data.ScheduleType = (int)report.ScheduleType;
            data.OwnerID = userId;
            data.CustomerID = customerId;
            report.CustomerId = customerId;
            report.OwnerId = userId;

            string reportParametersXML = JsonConvert.DeserializeXmlNode(data.Parameters, "report").OuterXml;

            string userHeirarchyAccessXML = GetUserHierarchyAccessXML(userId);

            Data.User user = new Data.User(userId);
            Dictionary<string, string> userInfo = new Dictionary<string, string>()
            {
                { "customerId", customerId.ToString() },
                { "customerSubDomain", user.Customer.SubDomain },
                { "userId", user.Id.ToString() },
                { "userGuid", user.JanrainUserUuid}
            };
            string userInfoJSON = JsonConvert.SerializeObject(userInfo);
            string userInfoXML = JsonConvert.DeserializeXmlNode(userInfoJSON, "userInfo").OuterXml;

            data.XMLParameters = string.Format("<parameters>{0}{1}{2}</parameters>", reportParametersXML, userInfoXML, userHeirarchyAccessXML);

            data.Save(Username);
            report.Id = data.Id; // store the new reportId

            var finalReport = GetReport(customerId, userId, data.Id);

            //try
            //{
                if (run)
                {
                    RunReport(finalReport.Data);
                }

                ProcessReportSchedule(finalReport.Data);
            //}
            //catch(Exception ex){
            //    throw new Exception("The report was saved successfully, but an error occurred on scheduling the report job to run.");
            //}


            return finalReport;
        }

        private string GetUserHierarchyAccessXML(int userId)
        {
            UserController userController = new UserController();
            List<Dictionary<string, string>> access = userController.GetUserHierarcyAccess(userId);

            string userAccessJSON = JsonConvert.SerializeObject(new Dictionary<string, List<Dictionary<string, string>>>() { { "customer", access } });
            string userHeirarchyAccessXML = JsonConvert.DeserializeXmlNode(userAccessJSON, "userHierarchyAccess").OuterXml;

            return userHeirarchyAccessXML;
        }

        public Dictionary<string, List<int>> GetAvailableHierarchies(int userId)
        {
            string hierarchyXML = GetUserHierarchyAccessXML(userId);

            Dictionary<string, List<int>> availableHierarchyIDs = new Dictionary<string, List<int>>();
            using (IDataReader reader = Data.SPs.GetHierarchiesFromXML(string.Format("<parameters>{0}</parameters>", hierarchyXML)).GetReader())
            {
                while (reader.Read())
                {
                    string codeName = reader.GetString(0);
                    int id = reader.GetInt32(1);

                    if (!availableHierarchyIDs.ContainsKey(codeName))
                    {
                        availableHierarchyIDs.Add(codeName, new List<int>());
                    }

                    availableHierarchyIDs[codeName].Add(id);
                }
            }

            return availableHierarchyIDs;
        }

        private void RunReport(Report report)
        {
            var triggerBuilder = TriggerBuilder.Create()
                        .WithIdentity(report.Id.ToString(), "now");

            triggerBuilder = triggerBuilder.StartNow();
            var trigger = triggerBuilder.Build();

            var jobDetail = GetJobDetail(report.Id, "now");
            if (jobDetail == null)
            {
                jobDetail = JobBuilder.Create<CizerReportJob>()
                    .WithIdentity(report.Id.ToString(), "now")
                    .Build();
            }


            //Symphony.Core.Jobs.CizerReportJob job = new CizerReportJob();
            //job.ExecuteReport(report.Id);

            if (Scheduler.CheckExists(GetJobKey(report.Id, "now")))
            {
                Scheduler.RescheduleJob(trigger.Key, trigger);
            }
            else
            {
                Scheduler.ScheduleJob(jobDetail, trigger);
            }

        }

        // Process it in quartz reportId, group
        private void ProcessReportSchedule(Report report)
        {
            if (report.ScheduleType == ReportSchedule.None)
            {
                return;
            }

            var customer = new Data.Customer(this.CustomerID);

            lock (ScheduleLock)
            {
                var jobDetail = GetJobDetail(report.Id, JobKey.DefaultGroup);
                if (jobDetail == null)
                {
                    jobDetail = JobBuilder.Create<CizerReportJob>()
                        .WithIdentity(report.Id.ToString(), JobKey.DefaultGroup)
                        .Build();
                }

                string cronSchedule = null;

                switch (report.ScheduleType)
                {
                    case ReportSchedule.Daily:
                        cronSchedule = string.Format("0 0 {0} * * ?", report.ScheduleHour);
                        break;
                    case ReportSchedule.Weekly:
                        if (report.ScheduleDaysOfWeekArray.Length < 1)
                        {
                            throw new Exception("At least one day of the week must be specified for a weekly schedule.");
                        }
                        cronSchedule = string.Format("0 0 {0} ? * {1}", report.ScheduleHour, report.GetCronDaysOfWeek());
                        break;
                    case ReportSchedule.Monthly:
                        string dayOfMonth = (report.ScheduleDayOfMonth == -1 ? "L" : report.ScheduleDayOfMonth.ToString());
                        cronSchedule = string.Format("0 0 {0} {1} * ?", report.ScheduleHour, dayOfMonth);
                        break;
                    case ReportSchedule.Yearly:
                        break;
                }

                var triggerBuilder = TriggerBuilder.Create()
                        .WithIdentity(report.Id.ToString());

                if (cronSchedule != null)
                {
                    triggerBuilder = triggerBuilder.WithCronSchedule(cronSchedule, x=>x.InTimeZone(TimeZoneInfo.FindSystemTimeZoneById(customer.TimeZone)));
                    var trigger = triggerBuilder.Build();
                    if (Scheduler.CheckExists(GetJobKey(report.Id, JobKey.DefaultGroup)))
                    {
                        Scheduler.RescheduleJob(trigger.Key, trigger);
                    }
                    else
                    {
                        Scheduler.ScheduleJob(jobDetail, trigger);
                    }
                }
            }
            return;
        }

        private JobKey GetJobKey(int reportId, string group)
        {
            return JobKey.Create(reportId.ToString(), group);
        }

        private IJobDetail GetJobDetail(int reportId, string group)
        {
            var jobKey = GetJobKey(reportId, group);
            IJobDetail jobDetail = Scheduler.GetJobDetail(jobKey);

            return jobDetail;
        }

        private Instant? GetNextRunTime(int reportId, string group)
        {
            var jobKey = GetJobKey(reportId, group);
            Instant? nextFireTime = null;

            bool isJobExisting = Scheduler.CheckExists(jobKey);
            if (isJobExisting)
            {
                //var detail = Scheduler.GetJobDetail(jobKey);
                var triggers = Scheduler.GetTriggersOfJob(jobKey);

                if (triggers.Count > 0)
                {
                    var nextFireTimeOffset = triggers[0].GetNextFireTimeUtc();
                    if (nextFireTimeOffset != null)
                    {
                        nextFireTime = Instant.FromDateTimeOffset((DateTimeOffset)nextFireTimeOffset); // TimeZone.CurrentTimeZone.ToLocalTime(nextFireTimeUtc.Value.DateTime);
                    }
                }
            }

            return nextFireTime;
        }

        public PagedResult<Report> FindReports(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            var customer = new Data.Customer(this.CustomerID);

            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.Report.Columns.Name;
            orderBy = "[dbo].[" + Data.Tables.Report + "].[" + orderBy + "]";

            SqlQuery query = new Select(
                    Data.Report.IdColumn.QualifiedName,
                    Data.Report.NameColumn.QualifiedName,
                    Data.Report.ReportTypeIDColumn.QualifiedName,
                    Data.Report.NotifyWhenReadyColumn.QualifiedName,
                    Data.Report.DownloadCSVColumn.QualifiedName,
                    Data.Report.DownloadXLSColumn.QualifiedName,
                    Data.Report.DownloadPDFColumn.QualifiedName,
                    Data.Report.ParametersColumn.QualifiedName,
                    Data.Report.ScheduleTypeColumn.QualifiedName,
                    Data.Report.ScheduleHourColumn.QualifiedName,
                    Data.Report.ScheduleDaysOfWeekColumn.QualifiedName,
                    Data.Report.ScheduleDayOfMonthColumn.QualifiedName,
                    Data.Report.ScheduleMonthColumn.QualifiedName,
                    Data.Report.OwnerIDColumn.QualifiedName,
                    Data.Report.CustomerIDColumn.QualifiedName,
                    Data.Report.IsFavoriteColumn.QualifiedName,
                    Data.Report.XMLParametersColumn.QualifiedName,
                    Data.Report.ReportTemplateIDColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.User + "].[" + Data.User.Columns.Username + "] AS [OwnerName]",
                    "[dbo].[" + Data.Tables.ReportType + "].[" + Data.ReportType.Columns.CodeName + "] AS [ReportTypeCode]"
                )
                .From(Data.Tables.Report)
                .Where(Data.Report.NameColumn).ContainsString(searchText)
                .And(Data.Report.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.Report.OwnerIDColumn).IsEqualTo(UserID)
                .LeftOuterJoin(Data.Tables.User, Data.User.Columns.Id, Data.Tables.Report, Data.Report.Columns.OwnerID)
                .LeftOuterJoin(Data.Tables.ReportType, Data.ReportType.Columns.Id, Data.Tables.Report, Data.Report.Columns.ReportTypeID);
                

            var result = new PagedResult<Report>(query, pageIndex, pageSize, orderBy, orderDir);
            
            foreach (var report in result.Data)
            {
                var instant = GetNextRunTime(report.Id, JobKey.DefaultGroup);
                if (instant != null){
                    var dt = new ZonedDateTime((Instant)instant, DateTimeZoneProviders.Bcl[customer.TimeZone]);
                    report.NextRunTime = dt.ToString("MM/dd/yyyy h:mm tt (z)", CultureInfo.InvariantCulture);
                }
            }

            return result;
        }

        public SingleResult<Report> GetReport(int customerId, int userId, int reportId)
        {

            Report report = new Select(
                    Data.Report.IdColumn.QualifiedName,
                    Data.Report.NameColumn.QualifiedName,
                    Data.Report.ReportTypeIDColumn.QualifiedName,
                    Data.Report.NotifyWhenReadyColumn.QualifiedName,
                    Data.Report.DownloadCSVColumn.QualifiedName,
                    Data.Report.DownloadXLSColumn.QualifiedName,
                    Data.Report.DownloadPDFColumn.QualifiedName,
                    Data.Report.ParametersColumn.QualifiedName,
                    Data.Report.ScheduleTypeColumn.QualifiedName,
                    Data.Report.ScheduleHourColumn.QualifiedName,
                    Data.Report.ScheduleDaysOfWeekColumn.QualifiedName,
                    Data.Report.ScheduleDayOfMonthColumn.QualifiedName,
                    Data.Report.ScheduleMonthColumn.QualifiedName,
                    Data.Report.OwnerIDColumn.QualifiedName,
                    Data.Report.CustomerIDColumn.QualifiedName,
                    Data.Report.IsFavoriteColumn.QualifiedName,
                    Data.Report.IsSymphonyIdModeColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.User + "].[" + Data.User.Columns.Username + "] AS [OwnerName]",
                    "[dbo].[" + Data.Tables.ReportType + "].[" + Data.ReportType.Columns.CodeName + "] AS [ReportTypeCode]",
                    Data.Report.ReportTemplateIDColumn.QualifiedName,
                    Data.Report.XMLParametersColumn.QualifiedName
                )
                .From(Data.Tables.Report)
                .Where(Data.Report.IdColumn).IsEqualTo(reportId)
                .LeftOuterJoin(Data.Tables.User, Data.User.Columns.Id, Data.Tables.Report, Data.Report.Columns.OwnerID)
                .LeftOuterJoin(Data.Tables.ReportType, Data.ReportType.Columns.Id, Data.Tables.Report, Data.Report.Columns.ReportTypeID)
                .ExecuteSingle<Report>();

            if (report == null)
            {
                throw new Exception("The specified report could not be found.");
            }
            try
            {
                report.AbsoluteXmlParameters = GetAbsoluteXmlParameters(report, true);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(report.AbsoluteXmlParameters);

                

                report.AbsoluteXmlParametersJson = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.None, true);

                JObject absoluteJOb = JObject.Parse(report.AbsoluteXmlParametersJson);
                JObject parameters = JObject.Parse(report.Parameters);
                JObject absoluteParameters = (JObject)absoluteJOb["report"];

                foreach (var parameter in absoluteParameters)
                {
                    if (parameter.Value.HasValues)
                    {
                        JArray selectedValues;
                        if (parameter.Value.Type != JTokenType.Array)
                        {
                            selectedValues = new JArray();
                            selectedValues.Add(parameter.Value);
            }
                        else
                        {
                            selectedValues = (JArray)parameter.Value;
                        }

                        string parameterName = parameter.Key;

                        string firstEntityName = selectedValues.First["@entityName"].Value<string>();
                        if (!string.IsNullOrEmpty(firstEntityName))
                        {
                            JArray entityArray = new JArray();
                            foreach (var value in selectedValues)
                            {
                                string entityName = value["@entityName"].Value<string>();
                                int entityId;

                                int.TryParse(value["#text"].Value<string>(), out entityId);

                                if (entityId > 0 && !string.IsNullOrWhiteSpace(entityName))
                                {
                                    JObject entity = new JObject();
                                    entity["id"] = entityId;
                                    entity["name"] = entityName;
                                    entityArray.Add(entity);
                                }

                            }
                            parameters[parameterName] = entityArray;
                        }
                        
                    }
                }

                report.Parameters = parameters.ToString();

            }
            catch (Exception ex)
            {
                report.AbsoluteXmlParametersWarning = ex.Message;
            }

            return new SingleResult<Report>(report);
        }

        public PagedResult<ReportTemplate> GetReportTemplates(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            Data.Customer customer = new Data.Customer(customerId);

            SqlQuery query = new Select()
                .From<Data.ReportTemplate>()
                .LeftOuterJoin(Data.ReportTemplatePermCustomer.ReportTemplateIdColumn, Data.ReportTemplate.IdColumn)
                .LeftOuterJoin(Data.ReportTemplatePermSalesChannel.ReportTemplateIdColumn, Data.ReportTemplate.IdColumn)
                .WhereExpression("1").IsEqualTo(UserIsSalesChannelAdmin ? "1" : "0")
                    .OrNestedExpression(Data.ReportTemplatePermCustomer.CustomerIdColumn.QualifiedName).IsEqualTo(customerId)
                        .Or(Data.ReportTemplatePermSalesChannel.SalesChannelIdColumn).IsEqualTo(customer.SalesChannelID)
                        .OrNestedExpression(Data.ReportTemplatePermCustomer.CustomerIdColumn.QualifiedName).IsNull()
                            .And(Data.ReportTemplatePermSalesChannel.SalesChannelIdColumn).IsNull()
                        .CloseExpression()
                    .CloseExpression()
                .CloseExpression();

            Data.ReportTemplate.Schema.Columns.ForEach(c =>
            {
                query.GroupBy(c);
            });

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.AndExpression(Data.ReportTemplate.CodeNameColumn.QualifiedName).ContainsString(searchText)
                        .Or(Data.ReportTemplate.NameColumn).ContainsString(searchText)
                    .CloseExpression();
            }

            return new PagedResult<ReportTemplate>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ReportEntity> GetReportEntities(int customerId, int userId, int templateId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            List<ReportEntity> entities = Data.SPs.GetReportEntities(templateId, false).ExecuteTypedList<ReportEntity>();
            return new PagedResult<ReportEntity>(entities, pageIndex, pageSize, entities.Count);
        }

        public SingleResult<ReportTemplate> GetReportTemplate(int customerId, int userId, int reportTemplateId)
        {
            Data.ReportTemplate reportTemplateData = new Data.ReportTemplate(reportTemplateId);
            ReportTemplate reportTemplate = new ReportTemplate();
            reportTemplate.CopyFrom(reportTemplateData);
            
            reportTemplate.ReportEntities = new List<ReportEntity>();

            foreach (Data.ReportTemplateEntity templateEntity in reportTemplateData.ReportTemplateEntities)
            {
                ReportEntity entity = new ReportEntity();
                entity.CopyFrom(templateEntity.ReportEntity);
                
                entity.Name = templateEntity.Label.Or(templateEntity.ReportEntity.Name);
                entity.Description = templateEntity.Description.Or(templateEntity.ReportEntity.Description);
                entity.TagName = templateEntity.TagName;
                entity.Order = templateEntity.Order;
                
                reportTemplate.ReportEntities.Add(entity);
            }

            reportTemplate.ReportEntities = reportTemplate.ReportEntities.OrderBy(e => e.Order).ToList();

            reportTemplate.ReportPermittedCustomers = new List<Customer>();

            foreach (Data.ReportTemplatePermCustomer customer in reportTemplateData.ReportTemplatePermCustomers)
            {
                reportTemplate.ReportPermittedCustomers.Add(new Customer() { ID = customer.CustomerId.Value });
            }

            reportTemplate.ReportPermittedSalesChannels = new List<SalesChannel>();
            if (reportTemplateData.ReportTemplatePermSalesChannels.Count > 0)
            {
                
                List<int> listOfSalesChannels = new List<int>();
                foreach (Data.ReportTemplatePermSalesChannel salesChannel in reportTemplateData.ReportTemplatePermSalesChannels)
                {
                    listOfSalesChannels.Add(salesChannel.SalesChannelId);
                    //reportTemplate.ReportPermittedSalesChannels.Add(new SalesChannel() { ID = salesChannel.SalesChannelId } );
                }

                var queryParents = new Select(Data.SalesChannel.IdColumn.ColumnName, Data.SalesChannel.Columns.ParentSalesChannelID).
                    From<Data.SalesChannel>().Where(Data.SalesChannel.IdColumn).In(listOfSalesChannels);

                var queryParentsResult = queryParents.ExecuteTypedList<Data.SalesChannel>();

                foreach (Data.ReportTemplatePermSalesChannel permSalesChannel in reportTemplateData.ReportTemplatePermSalesChannels)
                {
                    int parent = queryParentsResult.First(d => d.Id == permSalesChannel.SalesChannelId).ParentSalesChannelID.Value;
                    if (reportTemplateData.ReportTemplatePermSalesChannels.Count(d => d.SalesChannelId == parent) == 0)
                    {
                        reportTemplate.ReportPermittedSalesChannels.Add(new SalesChannel() { ID = permSalesChannel.SalesChannelId });
                    }
                }
            }

            return new SingleResult<ReportTemplate>(reportTemplate);
        }

        public SingleResult<ReportTemplate> SaveReportTemplate(int customerId, int userId, int id, ReportTemplate reportTemplate)
        {

            Data.ReportTemplate reportTemplateData = new Data.ReportTemplate(id);

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                
                reportTemplate.CopyTo(reportTemplateData);
                reportTemplateData.Id = id;
                reportTemplateData.Save();

                Data.ReportTemplateEntity.Delete(Data.ReportTemplateEntity.ReportTemplateIDColumn.ColumnName, reportTemplateData.Id);

                reportTemplateData.ReportTemplateEntities = new Data.ReportTemplateEntityCollection();

                Dictionary<string, bool> usedTagNames = new Dictionary<string, bool>();

                if (reportTemplate.ReportEntities != null)
                {
                    foreach (ReportEntity entity in reportTemplate.ReportEntities)
                    {
                        if (usedTagNames.ContainsKey(entity.TagName))
                        {
                            throw new Exception("You cannot use a tag name more than once per report.");
                        }

                        usedTagNames.Add(entity.TagName, true);

                        reportTemplateData.ReportTemplateEntities.Add(new Data.ReportTemplateEntity()
                        {
                            ReportEntityID = entity.Id,
                            ReportTemplateID = reportTemplateData.Id,
                            Order = entity.Order,
                            Label = entity.Name,
                            Description = entity.Description,
                            TagName = entity.TagName
                        });
                    }
                }

                reportTemplateData.ReportTemplateEntities.SaveAll();

                if (reportTemplate.ReportPermittedSalesChannels != null)
                {
                    foreach (SalesChannel sc in reportTemplate.ReportPermittedSalesChannels)
                    {
                        if (reportTemplateData.ReportTemplatePermSalesChannels.Count(d => d.SalesChannelId == sc.ID) == 0)
                        {
                            reportTemplateData.ReportTemplatePermSalesChannels.Add(new Data.ReportTemplatePermSalesChannel()
                            {
                                ReportTemplateId = id,
                                SalesChannelId = sc.ID
                            });
                        }
                    }
                    if (reportTemplateData.ReportTemplatePermSalesChannels.Count > 0)
                    {
                        reportTemplateData.ReportTemplatePermSalesChannels.SaveAll();
                    }
                    var queryDeletedPerm = reportTemplateData.ReportTemplatePermSalesChannels.Where(d => reportTemplate.ReportPermittedSalesChannels.Count(dd => dd.ID == d.SalesChannelId) == 0);
                    var deletedPermList = queryDeletedPerm.Select(d => d.SalesChannelId).ToList<int>();

                    if (deletedPermList.Count > 0)
                    {
                        new Delete().From<Data.ReportTemplatePermSalesChannel>().Where(Data.ReportTemplatePermSalesChannel.Columns.ReportTemplateId).IsEqualTo(id).And(Data.ReportTemplatePermSalesChannel.Columns.SalesChannelId).In(deletedPermList).Execute();
                    }
                }

                if (reportTemplate.ReportPermittedCustomers != null)
                {
                    foreach (Customer c in reportTemplate.ReportPermittedCustomers)
                    {
                        if (reportTemplateData.ReportTemplatePermCustomers.Count(d => d.CustomerId == c.ID) == 0)
                        {
                            reportTemplateData.ReportTemplatePermCustomers.Add(new Data.ReportTemplatePermCustomer()
                            {
                                ReportTemplateId = id,
                                CustomerId = c.ID
                            });
                        }
                    }
                    if (reportTemplateData.ReportTemplatePermCustomers.Count > 0)
                    {
                        reportTemplateData.ReportTemplatePermCustomers.SaveAll();
                    }

                    var queryDeletedPerm = reportTemplateData.ReportTemplatePermCustomers.Where(d => reportTemplate.ReportPermittedCustomers.Count(dd => dd.ID == d.CustomerId) == 0);
                    var deletedPermList = queryDeletedPerm.Select(d => d.CustomerId.Value).ToList<int>();

                    if (deletedPermList.Count > 0)
                    {
                        new Delete().From<Data.ReportTemplatePermCustomer>().Where(Data.ReportTemplatePermCustomer.Columns.ReportTemplateId).IsEqualTo(id).And(Data.ReportTemplatePermCustomer.Columns.CustomerId).In(deletedPermList).Execute();
                    }

                }

                ts.Complete();

               
            }
            return GetReportTemplate(customerId, userId, reportTemplateData.Id);
        }

        public MultipleResult<Customer> GetReportPermCustomersForSalesChannels(int reportTemplateId)
        {
            SqlQuery query = new Select().From<Data.Customer>()
                .InnerJoin(Data.Tables.ReportTemplatePermSalesChannel, Data.ReportTemplatePermSalesChannel.Columns.SalesChannelId, Data.Tables.Customer, Data.Customer.Columns.SalesChannelID)
                .Where(Data.ReportTemplatePermSalesChannel.ReportTemplateIdColumn).IsEqualTo(reportTemplateId);

            return new MultipleResult<Customer>(query);

        }

        public MultipleResult<ReportProcedure> GetReportProcedures(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            List<ReportProcedure> reportProcedures = Data.SPs.GetReportProcedures(reportProcedureDatabase, reportProcedurePrefix)
                .ExecuteTypedList<ReportProcedure>();
        
            if (!string.IsNullOrEmpty(searchText)) {
                reportProcedures = reportProcedures.Where(r => r.FullName.Contains(searchText)).ToList();
            }

            return new MultipleResult<ReportProcedure>(reportProcedures);
        }

        public PagedResult<ReportTypes> GetAvailableReportListing(int customerid, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // need to join with another to get the customer report for the specific customer
            SqlQuery query = Select.AllColumnsFrom<Data.ReportType>()    
            .Where(Data.ReportType.IsEnabledColumn).IsEqualTo(1);
            
            //ReportTypes type = query.ExecuteSingle<ReportTypes>();
            return new PagedResult<ReportTypes>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ReportJobRole> GetReportJobRole(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // need to join with another to get the customer report for the specific Jobrole hierarchy
            SqlQuery query = Select.AllColumnsFrom<Data.ReportJobRole>()
                .ApplyCustomerNetwork<Data.ReportJobRole>(customerId, true, false);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.And(Data.ReportJobRole.Columns.JobRoleLevelDetail).ContainsString(searchText);
            }

            //ReportJobRole jobrole = query.ExecuteSingle<ReportJobRole>();
            return new PagedResult<ReportJobRole>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ReportLocation> GetReportLocation(int customerid, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // need to join with another to get the customer report for the specific location hierarchy
            SqlQuery query = Select.AllColumnsFrom<Data.ReportLocation>()
                .ApplyCustomerNetwork<Data.ReportLocation>(customerid, true, false);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.And(Data.ReportLocation.Columns.LocationLevelDetail).ContainsString(searchText);
            }

            //ReportLocation location = query.ExecuteSingle<ReportLocation>();
            return new PagedResult<ReportLocation>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ReportCourse> GetReportCourse(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // need to join with another to get the customer report for the specific Course
            SqlQuery query = Select.AllColumnsFrom<Data.ReportCourse>()
                .ApplyCustomerNetwork<Data.ReportCourse>(customerId, true, false);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.And(Data.ReportCourse.Columns.Name).ContainsString(searchText);
            }

            //ReportCourse course = query.ExecuteSingle<ReportCourse>();
            return new PagedResult<ReportCourse>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ReportAudience> GetReportAudience(int customerid, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // need to join with another to get the customer report for the specific Audience hierarchy
            // and need to grab the actual customer id
            List<string> columns = Data.ReportAudience.Schema.Columns.Select(c => c.QualifiedName).ToList();
            columns.Add(Data.ReportCustomer.Schema.Columns.GetColumn(Data.ReportCustomer.Columns.CustomerID).QualifiedName + " as CustomerID");

            SqlQuery query = new Select(columns.ToArray())
            .From<Data.ReportAudience>()
            .InnerJoin(Data.ReportCustomer.Schema.TableName, Data.ReportAudience.Columns.CustomerKey, Data.ReportAudience.Schema.TableName, Data.ReportAudience.Columns.CustomerKey)
            .ApplyCustomerNetwork<Data.ReportAudience>(customerid, true, false);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.And(Data.ReportAudience.Columns.AudienceLevelDetail).ContainsString(searchText);
            }

            return new PagedResult<ReportAudience>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ReportUser> GetReportUser(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // need to join with another to get the customer report for the specific user hierarchy
            SqlQuery query = Select.AllColumnsFrom<Data.ReportUser>()
            .ApplyCustomerNetwork<Data.ReportUser>(customerId, true, false);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.AndExpression(Data.ReportUser.Columns.FirstName).ContainsString(searchText)
                    .Or(Data.ReportUser.Columns.LastName).ContainsString(searchText)
                    .CloseExpression();
            }

            return new PagedResult<ReportUser>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ReportUser> GetReportSupervisors(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // need to join with another to get the customer report for the specific user hierarchy
            SqlQuery query = Select.AllColumnsFrom<Data.ReportUser>()
            .ApplyCustomerNetwork<Data.ReportUser>(customerId, true, false)
            .And (Data.ReportUser.Columns.UserID).In(new Select(Data.User.Columns.SupervisorID).From(Data.User.Schema.TableName)
                .ApplyCustomerNetwork<Data.User>(customerId, true, false));

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.AndExpression(Data.ReportUser.Columns.FirstName).ContainsString(searchText)
                    .Or(Data.ReportUser.Columns.LastName).ContainsString(searchText)
                    .CloseExpression();
            }

            return new PagedResult<ReportUser>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ReportUser> GetReportSymphonyUser(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // need to join with another to get the customer report for the specific user hierarchy
            SqlQuery query = Select.AllColumnsFrom<Data.ReportUser>()
            .ApplyCustomerNetwork<Data.ReportUser>(customerId, true, false)
            .And(Data.ReportUser.Columns.IsImported).IsEqualTo(0);

            //ReportUser user = query.ExecuteSingle<ReportUser>();
            return new PagedResult<ReportUser>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<List<ReportScormRegistration>> GetScormRegistrationForUser(int userId)
        {
            return new SingleResult<List<ReportScormRegistration>>(new Exception());
            //return new SingleResult<List<ReportScormRegistration>>(Symphony.Core.Data.SPs.GetScormRegistrationForUser(userId).ExecuteTypedList<ReportScormRegistration>());
        }

        public PagedResult<ReportTrainingProgram> GetTrainingProgram(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // need to join with another to get the customer report for the specific user hierarchy
            SqlQuery query = Select.AllColumnsFrom<Data.ReportTrainingProgram>()
            .Where(Data.ReportTrainingProgram.Columns.IsCurrentIndicator).IsEqualTo(1)
            .InnerJoin(Data.TrainingProgram.IdColumn, Data.ReportTrainingProgram.Schema.GetColumn(Data.ReportTrainingProgram.Columns.TrainingProgramID))
            .ApplyCustomerNetwork<Data.ReportTrainingProgram>(customerId, true, false)
            .AndExpression(Data.TrainingProgram.ParentTrainingProgramIDColumn.QualifiedName).IsEqualTo(0)
                .Or(Data.TrainingProgram.ParentTrainingProgramIDColumn).IsNull()
            .CloseExpression();

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                query.And(Data.ReportTrainingProgram.Schema.GetColumn(Data.ReportTrainingProgram.Columns.Name)).ContainsString(searchText);
            }

            return new PagedResult<ReportTrainingProgram>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ReportTypes> GetReportTypes(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // need to join with another to get the customer report for the specific user hierarchy
            SqlQuery query = Select.AllColumnsFrom<Data.ReportType>()
            .Where(Data.ReportType.Columns.IsEnabled).IsEqualTo(1);

            return new PagedResult<ReportTypes>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ReportJobs> GetQueueListing(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ReportJob>()
            .Where(Data.ReportJob.Columns.CustomerID).IsEqualTo(customerId)
            .And(Data.ReportJob.Columns.IsScheduled).IsGreaterThan(0);

            return new PagedResult<ReportJobs>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ReportJobs> GetReportJobListing(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ReportJob>()
            .Where(Data.ReportJob.Columns.CustomerID).IsEqualTo(customerId);

            return new PagedResult<ReportJobs>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<ReportJobs> GetQueue(int customerId, int userId, int queueId)
        {
            // need to join with another to get the customer report for the specific user hierarchy
            SqlQuery query = Select.AllColumnsFrom<Data.ReportJob>()
                .InnerJoin(Data.ReportQueue.Schema.TableName, Data.ReportQueue.Columns.JobId, Data.ReportJob.Schema.TableName, Data.ReportJob.Columns.Id);

            return new SingleResult<ReportJobs>(query);

        }

        public SingleResult<ReportQueues> GetReportQueueStatus(int customerId, int userId, int queueId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ReportQueueStatus>();
            return new SingleResult<ReportQueues>(query);
        }

        public int GetQueueIdFromAJob(int customerId, int JobId)
        {
            int retValue = 0;
            try
            {
                SqlQuery query = new Select(Data.ReportQueue.Columns.Id)
                .From(Data.ReportQueue.Schema.TableName)                    
                .Where(Data.ReportQueue.Columns.JobId).IsEqualTo(JobId);

                retValue = (int)(query.ExecuteScalar());
            }
            catch
            {
                retValue = 0;
            }
            return retValue;

        }

        public int GetTriggerIdFromAJob(int customerId, int JobId)
        {
            int retValue = 0;
            try
            {
                SqlQuery query = new Select(Data.ReportJobTrigger.Columns.Id)
                .From(Data.ReportJobTrigger.Schema.TableName)
                .Where(Data.ReportJobTrigger.Columns.JobID).IsEqualTo(JobId);

                retValue = (int)(query.ExecuteScalar());
            }
            catch
            {
                retValue = 0;
            }
            return retValue;

        }

        private int GetReportTypeFromQueue(int customerId, int queueId)
        {
            SqlQuery query = new Select(Data.ReportQueue.Columns.ReportType)
            .From(Data.ReportQueue.Schema.TableName)
                //.InnerJoin(Data.ReportJob.Schema.TableName, Data.ReportJob.Columns.Id, Data.ReportQueue.Schema.TableName, Data.ReportQueue.Columns.JobId)
            .Where(Data.ReportQueue.Columns.Id).IsEqualTo(queueId);

            int retValue = (int)(query.ExecuteScalar());
            return retValue;

        }

        public PagedResult<ReportJobs> GetReportJob(int customerId, int userId, int queueId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ReportJob>()
                .Where(Data.ReportJob.Columns.CustomerID).IsEqualTo(customerId)
                .InnerJoin(Data.ReportJob.Schema.TableName, Data.ReportJob.Columns.Id, Data.ReportQueue.Schema.TableName, Data.ReportQueue.Columns.JobId)
                .Where(Data.ReportQueue.Columns.Id).IsEqualTo(queueId);

            
            //ReportJobs job = query.ExecuteSingle<ReportJobs>();
            return new PagedResult<ReportJobs>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public Stream DownloadXLS(string customerId, string queueId)
        {
            var queryPermCheck = Select.AllColumnsFrom<Data.ReportQueue>()
                .InnerJoin(Data.Tables.User, Data.User.Columns.CustomerID, Data.Tables.ReportQueue, Data.ReportQueue.Columns.CustomerID)
                .InnerJoin(Data.Tables.Report, Data.Report.Columns.Id, Data.Tables.ReportQueue, Data.ReportQueue.Columns.ReportID)
                .Where(Data.ReportQueue.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.ReportQueue.IdColumn).IsEqualTo(queueId)
                .And(Data.User.IdColumn).IsEqualTo(this.UserID)
                .And(Data.Report.OwnerIDColumn).IsEqualTo(this.UserID)
                .ExecuteTypedList<Data.ReportQueue>();

            if (queryPermCheck.Count == 0 && !(HasRole(Roles.CustomerAdministrator) || HasRole(Roles.ReportingAnalyst)))
            {
                // DS: if no records returned than queueId not match to the customer id
                throw new Exception("You don't have permissions to this report");
            }

            string path = HttpContext.Current.Server.MapPath(ReportXLSLocation + customerId + "_" + queueId + ".xlsx");

            string filename = customerId + "_" + queueId + ".xlsx";
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition", "attachment;filename=" + filename);

            if (System.IO.File.Exists(path))
            {
                // grab it from the file system
                return new MemoryStream(System.IO.File.ReadAllBytes(path));
            }
            else
            {
                CreateXLSFile(int.Parse(queueId), path);
                
                return new MemoryStream(System.IO.File.ReadAllBytes(path));
            }
            //return null;
        }

        public void CreateXLSFile(int queueId, string filePath)
        {
            // Get the data
            DataSet ds = GetReportDataSet(queueId.ToString()).ResultSet;

            Stream excelStream = ds.EnsureNiceColumnNames().ToExcel();

            using (Stream fileStream = File.Create(filePath))
            {
                excelStream.Seek(0, SeekOrigin.Begin);
                excelStream.CopyTo(fileStream);
            }
        }

        public DataTable GetReportDataTable(string queueId)
        {
            return GetReportDataSet(queueId).ResultSet.Tables[0];
        }


        public ReportDataSet GetReportDataSet(string queueID)
        {
            Data.ReportQueue entry = new Data.ReportQueue(Data.ReportQueue.IdColumn.ColumnName, queueID);
            if (!string.IsNullOrWhiteSpace(entry.ReportData))
            {
                ReportDataSet ds = new ReportDataSet();
                ds.ResultSet = JsonConvert.DeserializeObject<DataSet>(entry.ReportData);
                return ds;
            }
            else
            {
                ReportDataSet ds = new ReportDataSet();
                ds.ResultSet = Symphony.Core.Data.SPs.GetReportDataSet(int.Parse(queueID)).GetDataSet();
                return ds;
            }
        }

        public Stream DownloadPDF(string customerId, string queueId)
        {
            var queryPermCheck = Select.AllColumnsFrom<Data.ReportQueue>()
                .InnerJoin(Data.Tables.User, Data.User.Columns.CustomerID, Data.Tables.ReportQueue, Data.ReportQueue.Columns.CustomerID)
                .InnerJoin(Data.Tables.Report, Data.Report.Columns.Id, Data.Tables.ReportQueue, Data.ReportQueue.Columns.ReportID)
                .Where(Data.ReportQueue.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.ReportQueue.IdColumn).IsEqualTo(queueId)
                .And(Data.User.IdColumn).IsEqualTo(this.UserID)
                .And(Data.Report.OwnerIDColumn).IsEqualTo(this.UserID)
                .ExecuteTypedList<Data.ReportQueue>();

            if (queryPermCheck.Count == 0 && !(HasRole(Roles.CustomerAdministrator) || HasRole(Roles.ReportingAnalyst)))
            {
                // DS: if no records returned than queueId not match to the customer id
                throw new Exception("You don't have permissions to this report");
            }

            string filename = customerId + "_" + queueId + ".PDF";
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/pdf";
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition", "attachment;filename=" + filename);
            string path = HttpContext.Current.Server.MapPath(ReportPDFLocation +filename);
            if (System.IO.File.Exists(path))
            {
                // grab it from the file system
                return File.OpenRead(path); 
            }
            return null;
        }

        public Stream DownloadCSV(string customerId, string queueId)
        {
            var queryPermCheck = Select.AllColumnsFrom<Data.ReportQueue>()
                .InnerJoin(Data.Tables.User, Data.User.Columns.CustomerID, Data.Tables.ReportQueue, Data.ReportQueue.Columns.CustomerID)
                .InnerJoin(Data.Tables.Report, Data.Report.Columns.Id, Data.Tables.ReportQueue, Data.ReportQueue.Columns.ReportID)
                .Where(Data.ReportQueue.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.ReportQueue.IdColumn).IsEqualTo(queueId)
                .And(Data.User.IdColumn).IsEqualTo(this.UserID)
                .And(Data.Report.OwnerIDColumn).IsEqualTo(this.UserID)
                .ExecuteTypedList<Data.ReportQueue>();

            if (queryPermCheck.Count == 0 && !(HasRole(Roles.CustomerAdministrator) || HasRole(Roles.ReportingAnalyst)))
            {
                // DS: if no records returned than queueId not match to the customer id
                throw new Exception("You don't have permissions to this report");
            }

            string filename = customerId + "_" + queueId + ".CSV";
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/csv";
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition", "attachment;filename=" + filename);


            string path = HttpContext.Current.Server.MapPath(ReportCSVLocation + filename);

            if (System.IO.File.Exists(path))
            {
                // grab it from the file system
                return File.OpenRead(path); 
            }

            StringBuilder sb = new StringBuilder();
            DataTable dt = GetReportDataTable(queueId);

            foreach (DataColumn col in dt.Columns)
            {
                sb.Append(col.ColumnName + ',');
            }

            sb.Remove(sb.Length - 1, 1);
            sb.Append(Environment.NewLine);

            foreach (DataRow row in dt.Rows)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    sb.Append(row[i].ToString() + ",");
                }

                sb.Append(Environment.NewLine);
            }
            
            File.WriteAllText(path, sb.ToString());
            
             return File.OpenRead(path); 
        }

        // Get the Save Report 

        public SingleResult<ReportJobs> SaveJobDetail(int customerId, int userId, int jobId, ReportJobDetail jobs)
        {

            ReportJobDetail newJ = new ReportJobDetail();
            Data.ReportJob Jdata = new Data.ReportJob(jobId);

            Jdata.IsScheduled = int.Parse(!String.IsNullOrEmpty(jobs.Job.IsScheduled.ToString()) ? jobs.Job.IsScheduled.ToString() : "0");
            Jdata.CustomerID = customerId;
            Jdata.UserNotes = "";
            Jdata.Name = !String.IsNullOrEmpty(jobs.Job.Name) ? jobs.Job.Name : "Need a job";
            Jdata.Group = "";
            Jdata.Description = !String.IsNullOrEmpty(jobs.Job.Description) ? jobs.Job.Name : "Need a Description";
            Jdata.JobType = "0";

        
            if (jobId == 0)
            {

                Jdata.Id = 0;
            }
            else
            {
                Jdata.Id = jobId;
            }

            Jdata.Save(Username);

            Data.ReportQueue qData = new Data.ReportQueue();            
            qData = SaveQueueData(customerId, userId, Jdata.Id, jobs.Queue,jobs.Parameters);

            Data.ReportJobTrigger tData = new Data.ReportJobTrigger();
            tData = SaveTrigger(customerId, userId, Jdata.Id, jobs.Trigger);

            return new SingleResult<ReportJobs>(Model.Create<ReportJobs>(Jdata));


        }

        public SingleResult<ReportJobDetail> GetJobDetail(int customerId, int userId, int jobId)
        {

            ReportJobDetail newJ = new ReportJobDetail();
            Data.ReportJob j = new Data.ReportJob(jobId);
            int qId = GetQueueIdFromAJob(customerId, jobId);
            Data.ReportQueue q = new Data.ReportQueue(qId);
            int tId = GetTriggerIdFromAJob(customerId, jobId);
            Data.ReportJobTrigger t = new Data.ReportJobTrigger(tId);
            
            //ReportJobs job = new ReportJobs(jobId);
            //ReportJobTriggers trig = new ReportJobTriggers(jobId);
            try
            {
                newJ.Job.Id = jobId;
                newJ.Job.Name = j.Name;
                newJ.Job.IsScheduled = j.IsScheduled;

                newJ.Queue.JobId = jobId;
                newJ.Queue.ReportType = q.ReportType;
                newJ.Queue.queueCommand = q.QueueCommand;
                newJ.Queue.Parameters = q.Parameters;

                newJ.Trigger.CronSchedule = t.CronSchedule;
                

            }
            catch (Exception ex)
            {
                string error = ex.Message;

            }

            return new SingleResult<ReportJobDetail>(Model.Create<ReportJobDetail>(newJ));


        }

        public PagedResult<ReportQueues> FindReportQueueEntries(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
           /* if (string.IsNullOrEmpty(orderBy)) orderBy = Data.ReportQueueStatus.Columns.StartedOn;
            orderBy = "[dbo].[" + Data.Views.ReportQueueStatus + "].[" + orderBy + "]";
            

            SqlQuery query = Select.AllColumnsFrom<Data.ReportQueueStatus>()
                .Where(Data.ReportQueueStatus.Columns.ReportName).ContainsString(searchText)
                .And(Data.ReportQueueStatus.Columns.CustomerID).IsEqualTo(customerId);
            */

            Data.User user =  new Data.User(userId);

            SqlQuery query = Select.AllColumnsFrom<Data.ReportQueue>()
                .Where(Data.ReportQueue.ReportNameColumn).ContainsString(searchText)
                .And(Data.ReportQueue.CustomerIDColumn).IsEqualTo(customerId);

            if (!HasRole(Roles.ReportingAnalyst) && !UserIsSalesChannelAdmin)
            {
                query.InnerJoin(Data.Report.IdColumn, Data.ReportQueue.ReportIDColumn)
                    .And(Data.ReportQueue.CustomerIDColumn).IsEqualTo(customerId)
                    .And(Data.Report.Columns.OwnerID).IsEqualTo(userId)
                    .And(Data.ReportQueue.Columns.CreatedBy).IsEqualTo(user.Username);
            }

            if (this.MaxReportResultDataLength != "-1" && this.MaxReportResultRowCount != "-1")
            {
                query.IncludeColumn(string.Format("CASE WHEN [dbo].[ReportQueues].[ResultDataLength] > {0} OR [dbo].[ReportQueues].[ResultRowCount] > {1} THEN 1 ELSE 0 END",
                        this.MaxReportResultDataLength, this.MaxReportResultRowCount),
                            "StatusDataLength");
            }

            if (orderBy == "download")
            {
                string downloadColumn = string.Format("COALESCE(NULLIF({0}, 0), NULLIF({1},0), NULLIF({2},0))",
                    Data.ReportQueue.DownloadCSVColumn.QualifiedName,
                    Data.ReportQueue.DownloadPDFColumn.QualifiedName,
                    Data.ReportQueue.DownloadXLSColumn.QualifiedName);

                query.IncludeColumn(downloadColumn, "download");
                
                orderBy = downloadColumn + ", " + Data.ReportQueue.StartedOnColumn.QualifiedName;
            }

                        
            return new PagedResult<ReportQueues>(query, pageIndex, pageSize, orderBy, orderDir).WithUtcFlag();
        }

        // obsolet to be deleted
        public Data.ReportQueue SaveQueueData(int customerId, int userId, int jobId, ReportQueues queue, ReportParameters param)
        {
            int queueId = GetQueueIdFromAJob(customerId, jobId);
            Data.ReportQueue data = new Data.ReportQueue(queueId);

            //convert object to JSON
            var json = new JavaScriptSerializer().Serialize((object)param);
            //data.QueueCommand = SetQueueCommand(param, data.ReportType) + "@jobId = " + jobId.ToString();

            data.IsEnabled = queue.IsEnabled;
            data.Id = queueId;          
            data.Parameters = json;
            data.JobId = jobId;
            data.ReportType = queue.ReportType;

            data.Save(Username);
            return data;
        }

        public int SubmitJobToRunInCizer(Report report)
        {

            int queueId;
            queueId = 0;
            Data.ReportQueue data = new Data.ReportQueue();

            //deserialize object from JSON
            var json = report.Parameters;

            var jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            Dictionary<string, object> dict = (Dictionary<string, object>)jsSerializer.DeserializeObject(json);

            string sjson = report.Parameters;

            Dictionary<string, object> d = (Dictionary<string, object>)jsSerializer.DeserializeObject(json);
            // debug the JSON input
            foreach (KeyValuePair<string, object> pair in d)
            {
                string value;
                if (pair.Value is object[])
                {
                    value = string.Join(",", ((object[])(pair.Value)).Select(v=>v.ToString()).ToArray());
                }
            }

            string xmlParameters = GetAbsoluteXmlParameters(report);
            string sQueue = SetQueueCommand(report, xmlParameters);

            data.IsEnabled = 1;  // Enable the new Queue as a default
            data.Id = 0;  // New Queue
            data.QueueCommand = sQueue;
            data.XMLParameters = xmlParameters;
            data.JobId = report.Id;
            data.ReportType = report.ReportTypeId;

            data.Save(Username);
            queueId = data.Id;
            return queueId;
        }


        public Data.ReportJobTrigger SaveTrigger(int customerId, int userId, int jobId, ReportJobTriggers trig)
        {
            int trigId = GetTriggerIdFromAJob(customerId, jobId);
            Data.ReportJobTrigger data = new Data.ReportJobTrigger();
            // default these value in for now until we fully utilize the scheduler
            data.Name = "Job Detail - Save";
            data.Id = trigId;
            data.Group = "";
            data.Description = "";
            data.Name = "";
            data.JobID = jobId;
            data.Group = "";
            data.MisfireInstruction = "";
            data.RepeatCount = 0;
            data.RepeatInterval = 0;
            data.StartAt = DateTime.Now;
            data.EndAt = DateTime.Now;
            data.CronSchedule = trig.CronSchedule;
            //data.CronSchedule = "0 15 10 ? * 6#3";  // 3rd Friday at 10:15AM

            data.Save(Username);

            return data;
        }

        private string GetAbsoluteXmlParameters(Report report, bool isIncludeSelectionNames = false)
        {
            Data.Customer customer = new Data.Customer(report.CustomerId);
            string xmlParams = report.XMLParameters;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlParams);

            Dictionary<string, List<XmlElement>> entityElementIds = new Dictionary<string, List<XmlElement>>();

            foreach (XmlElement element in doc.SelectNodes("//report/*"))
            {
                int entityId;
                int.TryParse(element.InnerText, out entityId);

                if (entityId > 0) // It's an ID so will point to one of our entities
                {
                    if (!entityElementIds.ContainsKey(element.LocalName))
                    {
                        entityElementIds.Add(element.LocalName, new List<XmlElement>());
                    }
                    entityElementIds[element.LocalName].Add(element);
                } else { // Not an id, see if it's a date range
                string dateRange = element.InnerText;

                if (!dateRange.Contains(","))
                {
                    dateRange = dateRange + "," + dateRange;
                }

                try
                {
                    if (dateRange == ",")
                    {
                        continue;
                    }

                    LocalDateRange range = DateRangeManager.Parse(dateRange);
                    DateTimeZone zone = NodaTime.DateTimeZoneProviders.Bcl.GetZoneOrNull(customer.TimeZone);

                    if (zone == null)
                    {
                        zone = DateTimeZone.Utc;
                    }

                    LocalDateTime startDateTime = new LocalDateTime(range.StartDate.Year, range.StartDate.Month, range.StartDate.Day, 0, 0, 0);
                    LocalDateTime endDateTime = new LocalDateTime(range.EndDate.Year, range.EndDate.Month, range.EndDate.Day, 23, 59, 59);

                    ZonedDateTime zonedStartDateTimeLocal = zone.AtLeniently(startDateTime);
                    ZonedDateTime zonedEndDateTimeLocal = zone.AtLeniently(endDateTime);

                    ZonedDateTime zonedStartDateTimeUtc = new ZonedDateTime(zonedStartDateTimeLocal.ToInstant(), DateTimeZone.Utc);
                    ZonedDateTime zonedEndDateTimeUtc = new ZonedDateTime(zonedEndDateTimeLocal.ToInstant(), DateTimeZone.Utc);

                    element.InnerXml = string.Format("<from>{0}</from><to>{1}</to>",
                        zonedStartDateTimeUtc.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture),
                        zonedEndDateTimeUtc.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.CurrentCulture));
                }
                catch (Exception e)
                {
                    // if the value isn't a date range and can't be parsed, an exception is thrown. 
                    // in this case, who cares. 
                    // Log it in debug if we need to test
                    Log.Debug(e);
                }
                }
            }

            if (isIncludeSelectionNames)
            {
                AddSelectedNames(entityElementIds, report);
            }

            return doc.OuterXml;
        }
        /// <summary>
        /// Updates elements with attributes containing the name of the matching entity
        /// </summary>
        /// <param name="entityElementIds">Dictionary of elements. Must be indexed by the element tag name</param>
        /// <param name="report">The report</param>
        private void AddSelectedNames(Dictionary<string, List<XmlElement>> entityElementIds, Report report) {

            // Load the entity types in Symphony for the current report template
            // as a dictionary indexed by the tag name used in the xml parameter
            // document. This will include the db column information for the entity.
            //
            // Will only return entities that have a ReportEntityTypeID
            Dictionary<string, ReportEntity> reportEntities = new Select(
                Data.ReportTemplateEntity.TagNameColumn.QualifiedName,
                Data.ReportEntityType.NameColumn.QualifiedName,
                Data.ReportEntityType.SymphonyTableColumn.QualifiedName,
                Data.ReportEntityType.SymphonyIdColumnColumn.QualifiedName,
                Data.ReportEntityType.SymphonyNameColumnColumn.QualifiedName,
                Data.ReportEntityType.ReportingTableColumn.QualifiedName,
                Data.ReportEntityType.ReportingIdcolumnColumn.QualifiedName,
                Data.ReportEntityType.ReportingNameColumnColumn.QualifiedName
            )
            .From<Data.ReportTemplateEntity>()
            .InnerJoin(Data.ReportEntity.IdColumn, Data.ReportTemplateEntity.ReportEntityIDColumn)
            .InnerJoin(Data.ReportEntityType.IdColumn, Data.ReportEntity.ReportEntityTypeIDColumn)
            .Where(Data.ReportTemplateEntity.ReportTemplateIDColumn).IsEqualTo(report.ReportTemplateID)
            .ExecuteTypedList<ReportEntity>()
            .ToDictionary(r => r.TagName);

           
            // Loop through each list of entities
            // kv.Key = tagName
            foreach (KeyValuePair<string, List<XmlElement>> kv in entityElementIds)
            {
                List<int> ids = new List<int>(); // list of ids to filter the query by
                Dictionary<int, XmlElement> elementDictionary = new Dictionary<int, XmlElement>(); // mapping of elements back to their ids

                // Only bother if this entity has column info in our dictionary
                if (reportEntities.ContainsKey(kv.Key)) {
                    // Get all the ids associated with the current tag name
                    foreach (XmlElement element in kv.Value)
                    {
                        int elementId;
                        int.TryParse(element.InnerText, out elementId);

                        if (elementId > 0)
                        {
                            ids.Add(elementId); 
                            elementDictionary.Add(elementId, element);
                        }
                    }
                    // Look up the names for each item in the appropriate table
                    if (ids.Count > 0)
                    {
                        bool isSymphonyIdMode = report.IsSymphonyIdMode.HasValue && report.IsSymphonyIdMode.Value;
                        ReportEntity entity = reportEntities[kv.Key];
                        string idColumn = isSymphonyIdMode ? entity.SymphonyIdColumn : entity.ReportingIdColumn;
                        string nameColumn = isSymphonyIdMode ? entity.SymphonyNameColumn : entity.ReportingNameColumn;
                        string tableName = isSymphonyIdMode ? entity.SymphonyTable : entity.ReportingTable;

                        using (IDataReader reader = new Select(idColumn + " as ID", nameColumn + " as Name")
                                                        .From(tableName.Replace("]", "").Replace("[", "")).Where(idColumn.Replace("]", "").Replace("[", "")).In(ids.ToArray()).ExecuteReader())
                        {
                            // Assign each name back to the element as an attribute
                            while (reader.Read())
                            {
                                int id = reader.GetInt32(0);
                                string name = reader.GetString(1);

                                if (elementDictionary.ContainsKey(id))
                                {
                                    elementDictionary[id].SetAttribute("entityName", name);
                                }                            
                            }
                        }
                    }
                }
            }
        }

        private string SetQueueCommand(Report report, string xmlParameters)
        {
            Data.ReportTemplate template = new Data.ReportTemplate(report.ReportTemplateID);

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("declare @XML xml = '{0}'; ", xmlParameters);
            sb.AppendFormat("exec {0}.dbo.{1}{2} @XML", reportProcedureDatabase, reportProcedurePrefix, template.CodeName);

            return sb.ToString();
        }
        
        private void SetSolidPattern(RangeStyle m_RangeStyle, int fcolor, int bcolor)
        {
            short nPattern;
            nPattern = 1;
            m_RangeStyle.Pattern = nPattern;
            m_RangeStyle.PatternFG = fcolor;
            m_RangeStyle.PatternBG = bcolor;
        }

        private void ApplyBorder(RangeStyle m_RangeStyle)
        {
            m_RangeStyle.TopBorder = RangeStyle.BorderThin;
            m_RangeStyle.BottomBorder = RangeStyle.BorderThin;
            m_RangeStyle.LeftBorder = RangeStyle.BorderThin;
            m_RangeStyle.RightBorder = RangeStyle.BorderThin;
            m_RangeStyle.VerticalInsideBorder = RangeStyle.BorderThin;
            m_RangeStyle.HorizontalInsideBorder = RangeStyle.BorderThin;
        }
        
        public string GetReportDataInJSON(int customerId, int queueId)
        {
            var query = Select.AllColumnsFrom<Data.ReportQueue>()
                .InnerJoin(Data.Tables.User, Data.User.Columns.CustomerID, Data.Tables.ReportQueue, Data.ReportQueue.Columns.CustomerID)
                .InnerJoin(Data.Tables.Report, Data.Report.Columns.Id, Data.Tables.ReportQueue, Data.ReportQueue.Columns.ReportID)
                .Where(Data.ReportQueue.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.ReportQueue.IdColumn).IsEqualTo(queueId)
                .And(Data.User.IdColumn).IsEqualTo(this.UserID)
                .And(Data.Report.OwnerIDColumn).IsEqualTo(this.UserID)
                .ExecuteTypedList<Data.ReportQueue>();

            if (query.Count == 0 && !(HasRole(Roles.CustomerAdministrator) || HasRole(Roles.ReportingAnalyst)))
            {
                throw new Exception("You don't have permissions to this report");
            }

            if (query.Count > 0 && Convert.ToInt32(this.MaxReportResultDataLength) > 0)
            {
                var allowedMaxReportDataLength = Convert.ToInt32(this.MaxReportResultDataLength);
                var allowedMaxRowCount = Convert.ToInt32(this.MaxReportResultRowCount);

                if (query.First().ResultDataLength > allowedMaxReportDataLength || query.First().ResultRowCount > allowedMaxRowCount)
                {
                    //throw new Exception("The report is too big for the view");
                    DataSet ds = new DataSet();
                    DataRow myRow;
                    DataTable dt = new DataTable();
                    DataColumn colStatus = new DataColumn("Status", System.Type.GetType("System.String"));
                    dt.Columns.Add(colStatus);

                    myRow = dt.NewRow();
                    myRow["Status"] = "The report is too big for the view";
                    dt.Rows.Add(myRow);

                    ds.Tables.Add(dt);

                    string json = JsonConvert.SerializeObject(ds);
                    return json;

                }
            }

            Data.ReportQueue entry = new Data.ReportQueue(Data.ReportQueue.IdColumn.ColumnName, queueId);
            
            if (!string.IsNullOrWhiteSpace(entry.ReportData))
            {
                return entry.ReportData;
            }
            else
            {
                DataSet ds = Symphony.Core.Data.SPs.GetReportDataSet(queueId).GetDataSet();
                //ds.Tables.Add(new DataTable());
                string json = JsonConvert.SerializeObject(ds);
                return json;
            }
        }

        private DateTime ConvertJSONDate(string jDate)
        {
            string lDate = jDate;
            lDate = lDate.Replace(@"/Date", "");
            lDate = lDate.Replace(@"(", "");
            lDate = lDate.Replace(@")", "");
            lDate = lDate.Replace(@"\", "");
            lDate = lDate.Replace(@"/", "");

            long time = long.Parse(lDate);
            DateTime dt_1970 = new DateTime(1970, 1, 1);
            long tricks_1970 = dt_1970.Ticks;
            long time_tricks = tricks_1970 + time * 10000;
            DateTime dt = new DateTime(time_tricks);
            return dt;
        }

    }  // end of ReportController


}
