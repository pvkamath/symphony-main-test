using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.Services.Protocols;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Symphony.Core.GTM;
using Symphony.Core.Data;

namespace Symphony.Core.Controllers
{
    /// <summary>
    /// Summary description for GTMController
    /// </summary>
    public partial class GTMController : SymphonyController
    {
        
        private G2M_OrganizersService gtm = new G2M_OrganizersService();
        private string gtmConn;

        private string gtmUserID;
        private string gtmPassword;
        
        private int loginVersion = 2;

        #region Static Values

        #region Organizer Statuses

        public static string OrganizerStatusActive = "Active";
        public static string OrganizerStatusSuspended = "Suspended";
        public static string OrganizerStatusInvited = "Invited";

        #endregion

        #region Meeting Types

        public static string MeetingTypeImmediate = "Immediate";
        public static string MeetingTypeScheduled = "Scheduled";
        public static string MeetingTypeRecurring = "Recurring";
        public static string MeetingTypeScheduledEvent = "ScheduledEvent";
        public static string MeetingTypeRecurringEvent = "RecurringEvent";

        #endregion

        #region Time Zone Keys

        public const int TimeZoneAtlantic = 59;
        public const int TimeZoneEastern = 61;
        public const int TimeZoneCentral = 64;
        public const int TimeZoneMountain = 65;
        public const int TimeZonePacific = 67;

        public static int TimeZoneNukualofa = 1;
        public static int TimeZoneFijiKamchatkaMarshallIs = 2;
        public static int TimeZoneAucklandWellington = 3;
        public static int TimeZoneMagadanSolomonIsNewCaledonia = 4;
        public static int TimeZoneVladivostok = 5;
        public static int TimeZoneHobart = 6;
        public static int TimeZoneGuamPortMoresby = 7;
        public static int TimeZoneCanberraMelbourneSydney = 8;
        public static int TimeZoneBrisbane = 9;
        public static int TimeZoneDarwin = 10;
        public static int TimeZoneAdelaide = 11;
        public static int TimeZoneYakutsk = 12;
        public static int TimeZoneSeoul = 13;
        public static int TimeZoneOsakaSapporoTokyo = 14;
        public static int TimeZoneTaipei = 15;
        public static int TimeZonePerth = 16;
        public static int TimeZoneKualaLumpurSingapore = 17;
        public static int TimeZoneIrkutskUlaanBataar = 18;
        public static int TimeZoneBeijingChongqingHongKongUrumqi = 19;
        public static int TimeZoneKrasnoyarsk = 20;
        public static int TimeZoneBangkok = 21;
        public static int TimeZoneHanoiJakarta = 22;
        public static int TimeZoneRangoon = 23;
        public static int TimeZoneSriJayawardenepura = 24;
        public static int TimeZoneAstanaDhaka = 25;
        public static int TimeZoneAlmatyNovosibirsk = 26;
        public static int TimeZoneKathmandu = 27;
        public static int TimeZoneCalcuttaChennaiMumbaiNewDelhi = 28;
        public static int TimeZoneIslamabadKarachiTashkent = 29;
        public static int TimeZoneEkaterinburg = 30;
        public static int TimeZoneKabul = 31;
        public static int TimeZoneBakuTbilisiYerevan = 32;
        public static int TimeZoneAbuDhabiMuscat = 33;
        public static int TimeZoneTehran = 34;
        public static int TimeZoneNairobi = 35;
        public static int TimeZoneMoscowStPetersburgVolgograd = 36;
        public static int TimeZoneKuwaitRiyadh = 37;
        public static int TimeZoneBaghdad = 38;
        public static int TimeZoneJerusalem = 39;
        public static int TimeZoneHelsinkiRigaTallinn = 40;
        public static int TimeZoneHararePretoria = 41;
        public static int TimeZoneCairo = 42;
        public static int TimeZoneBucharest = 43;
        public static int TimeZoneAthensIstanbulMinsk = 44;
        public static int TimeZoneWestCentralAfrica = 45;
        public static int TimeZoneSarajevoSkopjeSofijaVilniusWarsawZagreb = 46;
        public static int TimeZoneBrusselsCopenhagenMadridParis = 47;
        public static int TimeZoneBelgradeBratislavaBudapestLjubljanaPrague = 48;
        public static int TimeZoneAmsterdamBerlinBernRomeStockholmVienna = 49;
        public static int TimeZoneGreenwichMeanTime = 50;
        public static int TimeZoneCasablancaMonrovia = 51;
        public static int TimeZoneCapeVerdeIs = 52;
        public static int TimeZoneAzores = 53;
        public static int TimeZoneBuenosAiresGeorgetown = 54;
        public static int TimeZoneBrasilia = 55;
        public static int TimeZoneNewfoundland = 56;
        public static int TimeZoneSantiago = 57;
        public static int TimeZoneCaracasLaPaz = 58;
        public static int TimeZoneAtlanticTimeCanada = 59;
        public static int TimeZoneIndianaEast = 60;
        public static int TimeZoneEasternTimeUSandCanada = 61;
        public static int TimeZoneBogotaLimaQuito = 62;
        public static int TimeZoneMexicoCity = 63;
        public static int TimeZoneCentralTimeUSandCanada = 64;
        public static int TimeZoneMountainTimeUSandCanada = 65;
        public static int TimeZoneArizona = 66;
        public static int TimeZonePacificTimeUSandCanadaTijuana = 67;
        public const int TimeZoneAlaska = 68;
        public const int TimeZoneHawaii = 69;
        public static int TimeZoneMidwayIslandSamoa = 70;
        public static int TimeZoneDublinEdinburghLisbonLondon = 71;

        #endregion

        #region Meeting Statuses
        public static string MeetingStatusActive = "ACTIVE";
        public static string MeetingStatusInactive = "INACTIVE";
        #endregion

        #region Conference Call Info Types
        public static string ConferenceCallInfoFree = "Free";
        #endregion

        #endregion

        #region Constructors

        public GTMController() : this(null, null) { }

        public GTMController(string userID, string password)
        {
            if (string.IsNullOrEmpty(userID) && string.IsNullOrEmpty(password))
            {
                this.gtmUserID = "jdaly@bankersedge.com";
                this.gtmPassword = "bankersedge1";
            }
            else
            {
                this.gtmUserID = userID;
                this.gtmPassword = password;
            }
        }

        #endregion

        #region Common API

        // The logon call authenticates the user and establishes a connection for issuing further
        // requests. The successful result of this call is a ConnectionID, a unique string used in all
        // other requests to identify the connection.
        public void Logon(string id, string password)
        {
            try
            {
                this.gtmConn = this.gtm.logon(id, password, this.loginVersion);
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The logoff call ends a connection and invalidates the ConnectionID.
        public void Logoff()
        {
            try
            {
                this.gtm.logoff(this.gtmConn);
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getGroups call obtains information about all the groups in the Corporate account
        // associated with the connectionId. Information for only active groups, not deleted groups
        // is returned. This call triggers a request to the Lucite Reporting server to obtain this
        // information.
        public Group[] GetGroups()
        {
            try
            {
                this.BeforeAPICall();
                Group[] groups = this.gtm.getGroups(this.gtmConn);
                this.AfterAPICall();
                return groups;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getOrganizers call obtains information about all organizers that belong to a particular
        // Group. If no groupKey is supplied, all the organizers within the corporate account are
        // returned. The organizers only with the status active, inactive, invited or suspended, but
        // not deleted are returned. This call triggers a request to the Lucite Reporting server to
        // obtain this information.
        //
        // Optional: groupKey
        public Organizer[] GetOrganizers(string groupKey)
        {
            try
            {
                this.BeforeAPICall();
                Organizer[] organizers;
                organizers = this.gtm.getOrganizers(this.gtmConn, groupKey);
                this.AfterAPICall();
                return organizers;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Administrator API

        // The signUpOrganizer call allows an administrator to send invitations to people allowing
        // them to become an organizer. This is done by specifying the email address of the person
        // to be signed up and the groupId of the group to which the organizer is being added. If the
        // groupId is not provided, the organizer will be added to the Corporate account itself. An
        // invitation email is sent to the specified email address with an activation link the organizer
        // can use to create their account. In the event the supplied email address is already in use
        // for a personal account or other corporate account, the organizer will need to provide
        // another email address.
        //
        // Optional: groupKey
        public void SignUpOrganizer(string organizerEmail, string groupKey)
        {
            try
            {
                this.BeforeAPICall();
                this.gtm.signUpOrganizer(this.gtmConn, organizerEmail, groupKey);
                this.AfterAPICall();
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The setOrganizerStatus call allows an administrator to change the status of an organizer
        // to �Active� or �Suspended�. Any organizer�s active meetings will be canceled if the
        // status is changed to �Suspended�.
        public void SetOrganizerStatus(long organizerKey, string status)
        {
            try
            {
                this.BeforeAPICall();
                this.gtm.setOrganizerStatus(this.gtmConn, organizerKey, status);
                this.AfterAPICall();
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getOrganizerStatus call allows an administrator to obtain the status of an organizer.
        public string GetOrganizerStatus(long organizerKey)
        {
            try
            {
                this.BeforeAPICall();
                string status = this.gtm.getOrganizerStatus(this.gtmConn, organizerKey);
                this.AfterAPICall();
                return status;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The deleteOrganizer call allows an administrator to delete an organizer.
        public void DeleteOrganizer(long organizerKey)
        {
            try
            {
                this.BeforeAPICall();
                this.gtm.deleteOrganizer(this.gtmConn, organizerKey);
                this.AfterAPICall();
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getTotalOrganizerCount call allows an administrator to get the total number of
        // organizers with a specified status. If the status is not supplied, all the organizers in the
        // corporate account (including the suspended and invited organizers) are returned.
        //
        // Optional: status
        public long GetTotalOrganizerCount(string status)
        {
            try
            {
                this.BeforeAPICall();
                long count = this.gtm.getTotalOrganizerCount(this.gtmConn, status);
                this.AfterAPICall();
                return count;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getOrganizer call obtains information about the organizer associated with the
        // organizerKey specified.
        public Organizer GetOrganizer(long organizerKey)
        {
            try
            {
                this.BeforeAPICall();
                Organizer organizer = this.gtm.getOrganizer(this.gtmConn, organizerKey);
                this.AfterAPICall();
                return organizer;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getOrganizerByEmail call obtains information about the organizer associated with
        // the organizerEmail specified.
        public Organizer GetOrganizerByEmail(string organizerEmail)
        {
            try
            {
                this.BeforeAPICall();
                Organizer organizer = this.gtm.getOrganizerByEmail(this.gtmConn, organizerEmail);
                this.AfterAPICall();
                return organizer;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Reporting API

        // The getMeetingInfoByOrganizer call obtains information about meetings for a particular
        // organizer, within the specified date range. If no date range is specified, the data for today
        // is returned.
        //
        // Optional: startDate, endDate, timeZoneKey
        public OrganizerMeetingInfo GetMeetingInfoByOrganizer(long organizerKey, DateTime startDate, DateTime endDate, int timeZoneKey)
        {
            try
            {
                this.BeforeAPICall();
                OrganizerMeetingInfo info = this.gtm.getMeetingInfoByOrganizer(this.gtmConn, organizerKey, startDate, endDate, timeZoneKey.ToString());
                this.AfterAPICall();
                return info;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getAllOrganizerMeetingInfo call obtains information about meetings for all the
        // organizers that belong to the supplied groupKey, for a specified date range. If no
        // groupKey is supplied, the meeting information for all the organizers that belong to the
        // corporate account (i.e. AccountId, associated with the connectionId) for the specified date
        // range is returned. If no date range is specified, the data for today is returned.
        //
        // Optional: groupKey, startDate, endDate, timeZoneKey
        public OrganizerMeetingInfo[] GetAllOrganizerMeetingInfo(string groupKey, DateTime startDate, DateTime endDate, int timeZoneKey)
        {
            try
            {
                this.BeforeAPICall();
                OrganizerMeetingInfo[] infos = this.gtm.getAllOrganizerMeetingInfo(this.gtmConn, groupKey, startDate, endDate, timeZoneKey.ToString());
                this.AfterAPICall();
                return infos;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getScheduledMeetingsByOrganizer call obtains information about the scheduled
        // meetings for a particular organizer and for a specified date range. If no date range is
        // specified, the data for today is returned.
        //
        // Optional: startDate, endDate, timeZoneKey
        public ScheduledMeetingInfo[] GetScheduledMeetingsByOrganizer(long organizerKey, DateTime startDate, DateTime endDate, int timeZoneKey)
        {
            try
            {
                this.BeforeAPICall();
                ScheduledMeetingInfo[] infos = this.gtm.getScheduledMeetingsByOrganizer(this.gtmConn, organizerKey, startDate, endDate, timeZoneKey.ToString());
                this.AfterAPICall();
                return infos;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getAllScheduledMeetings call obtains information about the scheduled meetings for
        // all the organizers that belong to the supplied groupKey, for a specified date range. If no
        // groupKey is supplied, the meeting information for all the organizers that belong to the
        // corporate account (i.e. AccountId, associated with the connectionId) for the specified date
        // range is returned. If no date range is specified, the data for today is returned.
        //
        // Optional: groupKey, startDate, endDate, timeZoneKey
        public ScheduledMeetingInfo[] GetAllScheduledMeetings(string groupKey, DateTime startDate, DateTime endDate, int timeZoneKey)
        {
            try
            {
                this.BeforeAPICall();
                ScheduledMeetingInfo[] infos = this.gtm.getAllScheduledMeetings(this.gtmConn, groupKey, startDate, endDate, timeZoneKey.ToString());
                this.AfterAPICall();
                return infos;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getMeetingHistoryByOrganizer call obtains information about the meetings that
        // occurred for a particular organizer within the specified date range. If no date range is
        // specified, the data for today is returned.
        //
        // Optional: startDate, endDate, timeZoneKey
        public OrganizerMeetingInstanceInfo[] GetMeetingHistoryByOrganizer(long organizerKey, DateTime startDate, DateTime endDate, int timeZoneKey)
        {
            try
            {
                this.BeforeAPICall();
                OrganizerMeetingInstanceInfo[] infos = this.gtm.getMeetingHistoryByOrganizer(this.gtmConn, organizerKey, startDate, endDate, timeZoneKey.ToString());
                this.AfterAPICall();
                return infos;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getAllMeetingHistory call obtains information about all the completed meetings for
        // all the organizers that belong to the supplied groupKey, within the specified date range. If
        // no groupKey is supplied, the meeting information for all the organizers that belong to the
        // corporate account (i.e. AccountId, associated with the connectionId) for the specified date
        // range is returned. If no date range is specified, the data for today is returned.
        //
        // Optional: groupKey, startDate, endDate, timeZoneKey
        public OrganizerMeetingInstanceInfo[] GetAllMeetingHistory(string groupKey, DateTime startDate, DateTime endDate, int timeZoneKey)
        {
            try
            {
                this.BeforeAPICall();
                OrganizerMeetingInstanceInfo[] infos = this.gtm.getAllMeetingHistory(this.gtmConn, groupKey, startDate, endDate, timeZoneKey.ToString());
                this.AfterAPICall();
                return infos;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getAttendanceHistoryByOrganizer call obtains attendance specific information about
        // the meetings that occurred for a particular organizer within the specified date range. If no
        // date range is specified, the data for today is returned.
        //
        // Optional: startDate, endDate, timeZoneKey
        public AttendanceInfo[] GetAttendanceHistoryByOrganizer(long organizerKey, DateTime startDate, DateTime endDate, int timeZoneKey)
        {
            try
            {
                this.BeforeAPICall();
                AttendanceInfo[] infos = this.gtm.getAttendanceHistoryByOrganizer(this.gtmConn, organizerKey, startDate, endDate, timeZoneKey.ToString());
                this.AfterAPICall();
                return infos;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getAllAttendanceHistory call obtains information about all the attendees for the
        // completed meetings, for all the organizers that belong to the supplied groupKey, within
        // the specified date range. If no groupKey is supplied, the meeting information for all the
        // organizers that belong to the corporate account (i.e. AccountId, associated with the
        // connectionId) for the specified date range is returned. If no date range is specified, the data
        // for today is returned.
        //
        // Optional: groupKey, startDate, endDate, timeZoneKey
        public AttendanceInfo[] GetAllAttendanceHistory(string groupKey, DateTime startDate, DateTime endDate, int timeZoneKey)
        {
            try
            {
                this.BeforeAPICall();
                AttendanceInfo[] infos = this.gtm.getAllAttendanceHistory(this.gtmConn, groupKey, startDate, endDate, timeZoneKey.ToString());
                this.AfterAPICall();
                return infos;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Organizer API

        // The createMeeting call creates a new meeting, using parameters specified in a supplied
        // MeetingParameters data type.
        public MeetingInfo CreateMeeting(MeetingParameters meetingParameters)
        {
            try
            {
                this.BeforeAPICall();
                MeetingInfo info = this.gtm.createMeeting(this.gtmConn, meetingParameters);
                this.AfterAPICall();
                return info;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The updateMeeting call updates an existing meeting, identified by the MeetingId
        // parameter, using parameters specified in a supplied MeetingParameters data type.
        //
        // Optional: uniqueMeetingID
        public void UpdateMeeting(long meetingID, string uniqueMeetingID, MeetingParameters meetingParameters)
        {
            try
            {
                this.BeforeAPICall();
                this.gtm.updateMeeting(this.gtmConn, meetingID, uniqueMeetingID, meetingParameters);
                this.AfterAPICall();
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The deleteMeeting call is used to remove a meeting, identified by the MeetingId
        // parameter, from the organizer�s list of meetings.
        //
        // Optional: uniqueMeetingID
        public void DeleteMeeting(long meetingID, string uniqueMeetingID)
        {
            try
            {
                this.BeforeAPICall();
                this.gtm.deleteMeeting(this.gtmConn, meetingID, uniqueMeetingID);
                this.AfterAPICall();
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The startMeeting call returns a GoToMeeting website URL that allows the meeting to be
        // started. This URL will navigate a browser to the existing Start Meeting Flow in the
        // Organizer website in which the Endpoint is downloaded if not already installed.
        //
        // Optional: uniqueMeetingID
        public string StartMeeting(long meetingID, string uniqueMeetingID)
        {
            try
            {
                this.BeforeAPICall();
                string url = this.gtm.startMeeting(this.gtmConn, meetingID, uniqueMeetingID) + "&sid=" + this.gtmConn;
                //this.AfterAPICall();
                return url;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The endMeeting call ends a meeting that is currently in progress, identified by the
        // MeetingId parameter.
        //
        // Optional: uniqueMeetingID
        public void EndMeeting(long meetingID, string uniqueMeetingID)
        {
            try
            {
                this.BeforeAPICall();
                this.gtm.endMeeting(this.gtmConn, meetingID, uniqueMeetingID);
                this.AfterAPICall();
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getMeetingInfo call gets information about a particular meeting.
        //
        // Optional: uniqueMeetingID
        public MeetingInfo GetMeetingInfo(long meetingID, string uniqueMeetingID)
        {
            try
            {
                this.BeforeAPICall();
                MeetingInfo info = this.gtm.getMeetingInfo(this.gtmConn, meetingID, uniqueMeetingID);
                this.AfterAPICall();
                return info;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getAllMeetingInfo call gets meeting information for all meetings owned by the
        // organizer associated with the WSAPI connection. Since the total number of meetings
        // may be large, the interface for this function will allow meetings to be fetched in batches.
        //
        // Up to 25 meetings at a time are returned. The Offset parameter allows a caller to page
        // through the entire result set with subsequent calls. If less than 25 meetings are returned,
        // the end of the result set has been reached.
        //
        // For example, if in the first call I pass no Offset=0 and get 25 meetings in the response, I
        // can infer that there may be additional meetings. If in the second call, I pass Offset=25
        // and get 10 meetings back, I know that there is a total of 35 meetings and I do not need to
        // make additional calls. Offset functions like an array subscript, i.e. Offset=25 starts with
        // the 26th meeting record.
        //
        // getAllMeetingInfo returns all scheduled, recurring, and active (ongoing) meetings with
        // the same semantics that apply to the My Meetings Page in the Organizer website and the
        // My Meetings dialog in the Endpoint software.
        public MeetingInfo[] GetAllMeetingInfo(int offset)
        {
            try
            {
                this.BeforeAPICall();
                MeetingInfo[] infos = this.gtm.getAllMeetingInfo(this.gtmConn, offset);
                this.AfterAPICall();
                return infos;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getMeetingHistory call gets records for completed meetings that have occurred in a
        // specified date range for the organizer associated with the WSAPI connection. The
        // returned records correspond to the information shown in the Meeting History Page of the
        // Organizers website.
        //
        // As with the getAllMeetingInfo function, records can be fetched in batches if the total
        // number of records is large.
        //
        // Optional: rangeStartTime, rangeEndTime, timeZoneKey
        public MeetingInstanceInfo[] GetMeetingHistory(DateTime rangeStartTime, DateTime rangeEndTime, int timeZoneKey, int offset)
        {
            try
            {
                this.BeforeAPICall();
                MeetingInstanceInfo[] infos = this.gtm.getMeetingHistory(this.gtmConn, rangeStartTime, rangeEndTime, timeZoneKey.ToString(), offset);
                this.AfterAPICall();
                return infos;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getMeetingInstance call gets the meeting instance record for the organizer associated
        // with the WSAPI connection provided. One record will be returned. The meeting instance
        // key can be obtained using the getMeetingInstances call which returns a list of instances
        // for a given meetingID.
        public MeetingInstanceInfo GetMeetingInstance(long meetingInstanceKey)
        {
            try
            {
                this.BeforeAPICall();
                MeetingInstanceInfo info = this.gtm.getMeetingInstance(this.gtmConn, meetingInstanceKey);
                this.AfterAPICall();
                return info;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getMeetingInstances call gets all meeting instance keys associated with the
        // meetingId provided and within a given date/time range. If no range is provided then all
        // instances are returned. For recurring meetings as many instances as have happened can be
        // returned.
        //
        // Optional: uniqueMeetingID, rangeStartTime, rangeEndTime, timeZoneKey
        public long[] GetMeetingInstances(long meetingID, string uniqueMeetingID, DateTime rangeStartTime, DateTime rangeEndTime, int timeZoneKey)
        {
            try
            {
                this.BeforeAPICall();
                long[] keys = this.gtm.getMeetingInstances(this.gtmConn, meetingID, uniqueMeetingID, rangeStartTime, rangeEndTime, timeZoneKey.ToString());
                this.AfterAPICall();
                return keys;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        // The getAttendeeInfo call gets information about attendees to a specific meeting instance,
        // identified by the meetingInstanceKey parameter.
        public AttendeeInfo[] GetAttendeeInfo(long meetingInstanceKey)
        {
            try
            {
                this.BeforeAPICall();
                AttendeeInfo[] infos = this.gtm.getAttendeeInfo(this.gtmConn, meetingInstanceKey);
                this.AfterAPICall();
                return infos;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Events

        public void BeforeAPICall()
        {
            try
            {
                this.Logon(this.gtmUserID, this.gtmPassword);
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        public void AfterAPICall()
        {
            try
            {
                this.Logoff();
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Tester Method

        public bool TestConnection()
        {
            try
            {
                Group[] groups = this.GetGroups();
                foreach (Group group in groups)
                {
                    Organizer[] organizers = this.GetOrganizers(group.groupKey.ToString());
                }
                return true;
            }
            catch (SoapException ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Helper Methods

        public static string GetFieldValue(Field[] fields, string fieldName)
        {
            foreach (Field field in fields)
            {
                if (field.name == fieldName)
                    return field.value;
            }
            return "";
        }

        #endregion

        public void CreateScheduledMeeting(Models.Meeting gtmMeeting)
        {
            try
            {
                MeetingParameters meetingParameters = this.BuildMeetingParameters(GTMController.MeetingTypeScheduled, gtmMeeting);
                MeetingInfo meetingInfo = this.CreateMeeting(meetingParameters);
                gtmMeeting.AddExtraData(meetingInfo);
            }
            catch
            {
                throw;
            }
        }

        public void CreateImmediateMeeting(Models.Meeting gtmMeeting)
        {
            try
            {
                MeetingParameters meetingParameters = this.BuildMeetingParameters(GTMController.MeetingTypeImmediate, gtmMeeting);
                MeetingInfo meetingInfo = this.CreateMeeting(meetingParameters);
                gtmMeeting.AddExtraData(meetingInfo);
            }
            catch
            {
                throw;
            }
        }

        public void UpdateScheduledMeeting(Models.Meeting gtmMeeting)
        {
            try
            {
                if (gtmMeeting.Id == 0)
                    throw new Exception("Invalid meeting specified.");

                MeetingParameters meetingParameters = this.BuildMeetingParameters(GTMController.MeetingTypeScheduled, gtmMeeting);
                this.UpdateMeeting(gtmMeeting.MeetingID, gtmMeeting.UniqueMeetingID, meetingParameters);
            }
            catch
            {
                throw;
            }
        }

        public void UpdateImmediateMeeting(Models.Meeting gtmMeeting)
        {
            try
            {
                if (gtmMeeting.Id == 0)
                    throw new Exception("Invalid meeting specified.");

                MeetingParameters meetingParameters = this.BuildMeetingParameters(GTMController.MeetingTypeImmediate, gtmMeeting);
                this.UpdateMeeting(gtmMeeting.MeetingID, gtmMeeting.UniqueMeetingID, meetingParameters);
            }
            catch
            {
                throw;
            }
        }

        public void DeleteMeeting(Models.Meeting gtmMeeting)
        {
            try
            {
                if (gtmMeeting.Id == 0)
                    throw new Exception("Invalid meeting specified.");

                this.DeleteMeeting(gtmMeeting.MeetingID, gtmMeeting.UniqueMeetingID);
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Code.Name.ToLower() != "nosuchmeeting")
                    throw ex;
            }
        }

        public string StartMeeting(int gtmMeetingID)
        {
            try
            {
                this.ValidateOrganizer();

                GTMMeeting gtmMeeting = new GTMMeeting(gtmMeetingID);
                if (gtmMeeting.Id == 0)
                    throw new Exception("Invalid meeting ID specified.");

                return this.StartMeeting(gtmMeeting.MeetingID, gtmMeeting.UniqueMeetingID);
            }
            catch
            {
                throw;
            }
        }

        public void EndMeeting(int gtmMeetingID)
        {
            try
            {
                this.ValidateOrganizer();

                GTMMeeting gtmMeeting = new GTMMeeting(gtmMeetingID);
                if (gtmMeeting.Id == 0)
                    throw new Exception("Invalid meeting ID specified.");

                this.EndMeeting(gtmMeeting.MeetingID, gtmMeeting.UniqueMeetingID);
            }
            catch
            {
                throw;
            }
        }

        private MeetingParameters BuildMeetingParameters(string meetingType, Models.Meeting gtmMeeting)
        {
            MeetingParameters meetingParameters = new MeetingParameters();
            meetingParameters.meetingType = meetingType;
            meetingParameters.subject = gtmMeeting.Subject;
            meetingParameters.startTime = gtmMeeting.StartDateTime;
            meetingParameters.endTime = gtmMeeting.EndDateTime;
            meetingParameters.timeZoneKey = gtmMeeting.TimeZoneKey.ToString();
            meetingParameters.conferenceCallInfo = GTMController.ConferenceCallInfoFree; //gtmMeeting.ConferenceCallInfo;
            meetingParameters.passwordRequired = gtmMeeting.PasswordRequired;
            return meetingParameters;
        }

        public void ValidateOrganizer()
        {
            if (!(new UserController()).UserIsGTMOrganizer(UserID))
            {
                throw new Exception("Currently logged-in user is not an organizer.");
            }
        }
    }
}