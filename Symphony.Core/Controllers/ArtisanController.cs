﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using SubSonic;
using System.Web;
using Symphony.RusticiIntegration.Core;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Net;
using HtmlAgilityPack;
using System.Security.Cryptography.X509Certificates;
using Aspose.Pdf;
using Aspose.Words;
using Aspose.Slides;
using Symphony.Core.Comparers;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Drawing.Drawing2D;
using log4net;
using System.Transactions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Symphony.Core.Extensions;
using System.Diagnostics;
using Symphony.Core.Generators;
//using Abot.Crawler;
//using Abot.Poco;

namespace Symphony.Core.Controllers
{
    public class ArtisanController : Controllers.SymphonyController
    {
        private ILog Log = LogManager.GetLogger(typeof(ArtisanController));
        private AccreditationController Accreditation = new AccreditationController();

        public static string WrapperMarkup = @"
            <div id=""Main"" class=""Main"">
              <div class=""MainWrapper"">
                <div id=""Content"" class=""Content"">
                  <div class=""ContentWrapper"">
                    <div class='asset-wrapper{1}'>
                     {0}
                    </div>
                  </div>
                </div>
              </div>
            </div>";

        public PagedResult<ArtisanTheme> FindThemes(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ArtisanTheme>()
                    .Where(Data.ArtisanTheme.NameColumn).ContainsString(searchText)
                    .AndExpression(Data.ArtisanTheme.Columns.CustomerID).IsEqualTo(customerId)
                    .Or(Data.ArtisanTheme.Columns.CustomerID).IsEqualTo(0)
                    .CloseExpression();

            return new PagedResult<ArtisanTheme>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ArtisanCourse> FindCourses(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, CategoryFilter filter)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.ArtisanCourse.Columns.Name;
            orderBy = "[dbo].[" + Data.Tables.ArtisanCourse + "].[" + orderBy + "]";

            SqlQuery query = new Select(
                    Data.ArtisanCourse.DescriptionColumn.QualifiedName,
                    Data.ArtisanCourse.IdColumn.QualifiedName,
                    Data.ArtisanCourse.KeywordsColumn.QualifiedName,
                    Data.ArtisanCourse.NameColumn.QualifiedName,
                    Data.CategoryHierarchy.Columns.LevelIndentText,
                    Data.ArtisanCourse.ModifiedOnColumn.QualifiedName
                ).From(Data.Tables.ArtisanCourse)
                .Where(Data.ArtisanCourse.CustomerIDColumn).IsEqualTo(customerId);

            if (!string.IsNullOrWhiteSpace(searchText))
            {
                bool search = true;
                if (searchText.StartsWith("#"))
                {
                    var num = searchText.Trim('#');
                    int x = 0;
                    if (int.TryParse(num, out x))
                    {
                        // specific ID
                        query.And(Data.ArtisanCourse.IdColumn.QualifiedName).IsEqualTo(x);
                        search = false;
                    }
                }

                if (search)
                {
                    query.AndExpression(Data.ArtisanCourse.Columns.Name).ContainsString(searchText)
                           .Or(Data.ArtisanCourse.Columns.Keywords).ContainsString(searchText)
                           .Or(Data.CategoryHierarchy.Columns.LevelIndentText).ContainsString(searchText)
                           .Or(Data.ArtisanCourse.Columns.ExternalID).ContainsString(searchText)
                       .CloseExpression();
                }
            }


            query.And(Data.ArtisanCourse.IsDeletedColumn).IsEqualTo(false)
                .And(Data.ArtisanCourse.IsPublishedColumn).IsEqualTo(false)
                .And(Data.ArtisanCourse.IsAutoPublishColumn).IsEqualTo(false)
                .ApplyLevelIndentText<Data.Category>(Data.ArtisanCourse.CategoryIDColumn);

            if (filter != null)
            {
                if (filter.CategoryID > 0)
                {
                    query = query.And(Data.ArtisanCourse.Columns.CategoryID).IsEqualTo(filter.CategoryID);
                }
                if (filter.SecondaryCategoryId > 0)
                {
                    query = new Select(
                    Data.ArtisanCourse.DescriptionColumn.QualifiedName,
                    Data.ArtisanCourse.IdColumn.QualifiedName,
                    Data.ArtisanCourse.KeywordsColumn.QualifiedName,
                    Data.ArtisanCourse.NameColumn.QualifiedName
                    ).From<Data.SecondaryCategory>()
                    .InnerJoin(Data.Tables.ArtisanCourseSecondaryCategoryLink, Data.ArtisanCourseSecondaryCategoryLink.Columns.SecondaryCategoryId, Data.Tables.SecondaryCategory, Data.SecondaryCategory.Columns.Id)
                    .InnerJoin(Data.Tables.ArtisanCourse, Data.ArtisanCourse.Columns.Id, Data.Tables.ArtisanCourseSecondaryCategoryLink, Data.ArtisanCourseSecondaryCategoryLink.Columns.ArtisanCourseId)
                    .Where(Data.SecondaryCategory.Columns.Id).IsEqualTo(filter.SecondaryCategoryId)
                    .And(Data.ArtisanCourse.Columns.Name).ContainsString(searchText)
                    .And(Data.ArtisanCourse.IsDeletedColumn).IsEqualTo(false)
                    .And(Data.ArtisanCourse.IsPublishedColumn).IsEqualTo(false)
                    .And(Data.ArtisanCourse.IsAutoPublishColumn).IsEqualTo(false);

                }
            }



            return new PagedResult<ArtisanCourse>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ArtisanCourse> FindPublishedCourses(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, bool hidePrevious)
        {
            SqlQuery query;

            List<string> columns = Data.ArtisanCourse.Schema.Columns.ToList().Select(c => c.QualifiedName).ToList<string>();
            columns.Add(Data.CategoryHierarchy.Columns.LevelIndentText);

            query = new Select(columns.ToArray()).From<Data.ArtisanCourse>()
                .WhereExpression(Data.ArtisanCourse.Columns.Name).ContainsString(searchText)
                    .Or(Data.ArtisanCourse.Columns.Keywords).ContainsString(searchText)
                    .Or(Data.CategoryHierarchy.Columns.LevelIndentText).ContainsString(searchText)
                .CloseExpression()
                .And(Data.ArtisanCourse.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.ArtisanCourse.IsDeletedColumn).IsEqualTo(false)
                .And(Data.ArtisanCourse.IsPublishedColumn).IsEqualTo(true)
                .And(Data.ArtisanCourse.IsAutoPublishColumn).IsEqualTo(false)
                .ApplyLevelIndentText<Data.Category>(Data.ArtisanCourse.CategoryIDColumn);

            if (hidePrevious)
            {
                query.And(Data.ArtisanCourse.IdColumn).In(
                        new Select(Data.LatestPublishedCourse.Columns.Id)
                            .From<Data.LatestPublishedCourse>()
                            .Where(Data.LatestPublishedCourse.Columns.CustomerID).IsEqualTo(customerId)
                            .And(Data.LatestPublishedCourse.Columns.IsDeleted).IsEqualTo(false));
            }

            if (string.IsNullOrEmpty(orderBy))
            {
                query.OrderAsc(Data.ArtisanCourse.NameColumn.QualifiedName)
                     .OrderDesc(Data.ArtisanCourse.CreatedOnColumn.QualifiedName);
            }

            return new PagedResult<ArtisanCourse>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ArtisanDeploymentPackage> FindDeployments(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            VerifyDeployPermissions(customerId, userId);

            SqlQuery query = Select.AllColumnsFrom<Data.ArtisanDeployment>();

            if (!string.IsNullOrEmpty(searchText))
            {
                query.Where(Data.ArtisanDeployment.NameColumn).ContainsString(searchText);
            }

            if (string.IsNullOrEmpty(orderBy))
            {
                query.OrderAsc(Data.ArtisanDeployment.Columns.Name)
                     .OrderAsc(Data.ArtisanDeployment.Columns.CreatedOn);
            }

            return new PagedResult<ArtisanDeploymentPackage>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public MultipleResult<ArtisanDeploymentInfo> GetDeployment(int id, int customerId, int userId)
        {
            VerifyDeployPermissions(customerId, userId);

            Data.ArtisanDeployment deployment = new Data.ArtisanDeployment(id);

            if (deployment == null || deployment.Id == 0)
            {
                throw new Exception("Deployment not found");
            }

            List<ArtisanDeploymentInfo> info = new List<ArtisanDeploymentInfo>();
            try
            {
                info = Utilities.Deserialize<List<ArtisanDeploymentInfo>>(deployment.Info);
            }
            catch (Exception e)
            {
                info.Add(new ArtisanDeploymentInfo()
                {
                    // Occurs when the first call to get log
                    // happens before the first log is created
                    // so just build a dummy log item to indicate
                    // we are still waiting for info to come in. 
                    Running = deployment.Running,
                    Queued = deployment.Queued,
                    Message = "Deployment Queued",
                    TotalCount = 1,
                    CurrentCount = 0,
                    CourseID = 0,
                    CustomerID = 0,
                    CustomerName = "",
                    CourseName = "",
                    Date = DateTime.Now,
                    ID = 0,
                    Result = true
                });
            }

            return new MultipleResult<ArtisanDeploymentInfo>(info);
        }

        public MultipleResult<ArtisanSourceImportInfo> GetArtisanSourceImportInfo(int id, string searchText)
        {
            Data.ExternalSystemSourceTracking trackingRecord = new Data.ExternalSystemSourceTracking(Data.ExternalSystemSourceTracking.Columns.ArtisanCourseId, id);
            if (trackingRecord.SourceId > 0)
            {
                SqlQuery query = Select.AllColumnsFrom<Data.ExternalSystemMetaDataRecord>()
                    .Where(Data.ExternalSystemMetaDataRecord.Columns.ExternalSourceCourseId).IsEqualTo(trackingRecord.SourceId)
                    .And(Data.ExternalSystemMetaDataRecord.Columns.Name).ContainsString(searchText);

                return new MultipleResult<ArtisanSourceImportInfo>(query);
            }
            return null;
        }

        public SingleResult<ArtisanCourse> GetCourse(int customerId, int userId, int courseId)
        {
            List<string> columns = Data.ArtisanCourse.Schema.Columns.Select(c => c.QualifiedName).ToList<string>();
            columns.Add(Data.CategoryHierarchy.Columns.LevelIndentText);

            SqlQuery query = new Select(columns.ToArray()).From<Data.ArtisanCourse>()
                .Where(Data.ArtisanCourse.CustomerIDColumn).IsEqualTo(customerId)
                .And(Data.ArtisanCourse.IdColumn).IsEqualTo(courseId)
                .ApplyLevelIndentText<Data.Category>(Data.ArtisanCourse.CategoryIDColumn);

            ArtisanCourse course = query.ExecuteSingle<ArtisanCourse>();

            course.DefaultParameters = new List<ParameterValue>();
            // Try to load the default parameters if any saved yet
            if (!string.IsNullOrEmpty(course.DefaultParametersJSON))
            {
                try
                {
                    course.DefaultParameters = Utilities.Deserialize<List<ParameterValue>>(course.DefaultParametersJSON);
                }
                catch (Exception e)
                {
                    // Just going to log the error, this really isn't critical, just useful for displaying what the default
                    // will be prior to overriding them at the online course. The defaults get injected directly into the 
                    // courseJson so this is just a handy reference thing anyway.
                    Log.Warn("Default parameters could not be deserialized. " + e.Message);
                }
            }

            course.Theme = Select.AllColumnsFrom<Data.ArtisanTheme>()
                   .Where(Data.ArtisanTheme.Columns.Id).IsEqualTo(course.ThemeId)
                   .ExecuteSingle<ArtisanTheme>();

            course.ThemeFlavor = course.ThemeFlavorID.HasValue ? Select.AllColumnsFrom<Data.Theme>()
                    .Where(Data.Theme.Columns.Id).IsEqualTo(course.ThemeFlavorID.Value)
                    .ExecuteSingle<Theme>() : null;

            course.LastPublishedOn = new Select(Data.ArtisanCourse.ModifiedOnColumn.QualifiedName)
                   .From(Data.ArtisanCourse.Schema.TableName)
                   .Where(Data.ArtisanCourse.Columns.OriginalCourseId).IsEqualTo(course.Id)
                   .OrderDesc(Data.ArtisanCourse.ModifiedOnColumn.QualifiedName)
                   .ExecuteSingle<DateTime>();

            var courseSections = Select.AllColumnsFrom<Data.ArtisanSection>()
                    .Where(Data.ArtisanSection.Columns.CourseID).IsEqualTo(courseId)
                    .OrderAsc(Data.ArtisanSection.Columns.Sort)
                    .ExecuteTypedList<ArtisanSection>();

            course.Authors = Select.AllColumnsFrom<Data.User>()
                   .Where(Data.User.IdColumn).In(new Select(Symphony.Core.Data.ArtisanCourseAuthorsLink.Columns.UserId)
                        .From<Symphony.Core.Data.ArtisanCourseAuthorsLink>()
                        .InnerJoin(Data.Tables.User, Data.User.Columns.Id, Data.Tables.ArtisanCourseAuthorsLink, Data.ArtisanCourseAuthorsLink.Columns.UserId)
                        .Where(Data.ArtisanCourseAuthorsLink.ArtisanCourseIdColumn).IsEqualTo(course.Id)
                   )
                   .ExecuteTypedList<Models.Author>();

            if (course.SecondaryCategories == null) course.SecondaryCategories = new List<Category>();
            var listOfSecondaryCategories = Select.AllColumnsFrom<Data.SecondaryCategory>()
                .InnerJoin(Data.Tables.ArtisanCourseSecondaryCategoryLink, Data.ArtisanCourseSecondaryCategoryLink.Columns.SecondaryCategoryId, Data.Tables.SecondaryCategory, Data.SecondaryCategory.Columns.Id)
                .Where(Data.ArtisanCourseSecondaryCategoryLink.Columns.ArtisanCourseId).IsEqualTo(course.Id).ExecuteTypedList<Data.SecondaryCategory>();

            foreach (Data.SecondaryCategory additionalCategory in listOfSecondaryCategories)
            {
                course.SecondaryCategories.Add(new Category() { Id = additionalCategory.Id, Name = additionalCategory.Name });
            }

            var accreditations = Accreditation.QueryArtisanAccreditations(course.Id);
            course.Accreditations = accreditations.Data.ToList();

            List<string> artisanPageColumns = Data.ArtisanPage.Schema.Columns.Select(c => c.QualifiedName).ToList();
            artisanPageColumns.Add(Data.ArtisanSectionPage.SectionIDColumn.QualifiedName + " as SectionID");
            artisanPageColumns.Add(Data.ArtisanSectionPage.SortColumn.QualifiedName + " as Sort");
            artisanPageColumns.Add(Data.ArtisanTemplate.CssTextColumn.QualifiedName + " as CssText");

            var cpQuery = new Select(artisanPageColumns.ToArray()).From<Data.ArtisanPage>()
                .InnerJoin(Data.ArtisanSectionPage.PageIDColumn, Data.ArtisanPage.IdColumn)
                .LeftOuterJoin(Data.ArtisanTemplate.IdColumn, Data.ArtisanPage.TemplateIDColumn)
                //.InnerJoin(Data.ArtisanSectionPage.Schema.TableName, Data.ArtisanSectionPage.Columns.PageID, Data.ArtisanPage.Schema.TableName, Data.ArtisanPage.Columns.Id)
                .Where(Data.ArtisanPage.Columns.CourseID).IsEqualTo(courseId)
                .OrderAsc(Data.ArtisanSectionPage.Columns.Sort);

            var coursePages = cpQuery.ExecuteTypedList<ArtisanPage>();

            var pageIds = coursePages.Select(p => p.Id);
            if (pageIds.Count() == 0)
            {
                pageIds = new int[] { 0 };
            }

            var answers = Select.AllColumnsFrom<Data.ArtisanAnswer>()
                    .Where(Data.ArtisanAnswer.Columns.PageID).In(new Select(Data.ArtisanPage.IdColumn).From<Data.ArtisanPage>().Where(Data.ArtisanPage.CourseIDColumn).IsEqualTo(courseId))
                    .OrderAsc(Data.ArtisanAnswer.Columns.Sort)
                    .ExecuteTypedList<ArtisanAnswer>();

            Dictionary<int, ArtisanSection> sectionDictionary = courseSections.DistinctBy(s => s.Id).ToDictionary(s => s.Id);
            Dictionary<int, ArtisanPage> pageDictionary = coursePages.DistinctBy(p => p.Id).ToDictionary(p => p.Id);

            foreach (ArtisanPage p in coursePages)
            {
                if (sectionDictionary.ContainsKey(p.SectionID))
                {
                    if (sectionDictionary[p.SectionID].Pages == null)
                    {
                        sectionDictionary[p.SectionID].Pages = new List<ArtisanPage>();
                    }

                    sectionDictionary[p.SectionID].Pages.Add(p);
                }
            }

            foreach (ArtisanAnswer a in answers)
            {
                if (pageDictionary.ContainsKey(a.PageId))
                {
                    if (pageDictionary[a.PageId].Answers == null)
                    {
                        pageDictionary[a.PageId].Answers = new List<ArtisanAnswer>();
                    }

                    pageDictionary[a.PageId].Answers.Add(a);
                }
            }

            course.Sections = new List<ArtisanSection>();
            foreach (ArtisanSection s in courseSections)
            {
                if (s.SectionId > 0)
                {
                    if (sectionDictionary.ContainsKey(s.SectionId))
                    {
                        if (sectionDictionary[s.SectionId].Sections == null)
                        {
                            sectionDictionary[s.SectionId].Sections = new List<ArtisanSection>();
                        }
                        sectionDictionary[s.SectionId].Sections.Add(s);
                    }
                }
                else
                {
                    course.Sections.Add(s);
                }
            }


            return new SingleResult<ArtisanCourse>(course);
        }

        private void BuildCourseSection(ArtisanSection section, List<ArtisanSection> courseSections, List<ArtisanPage> coursePages, List<ArtisanAnswer> answers, List<ArtisanTemplate> templates, List<Data.ArtisanSectionPage> sectionPages)
        {
            section.Sections = courseSections.Where(x => x.SectionId == section.Id).ToList();

            var pageIds = sectionPages.Where(sp => sp.SectionID == section.Id).Select(sp => sp.PageID);
            section.Pages = coursePages.Where(cp => pageIds.Contains(cp.Id)).ToList(); //cp. cp.SectionId == section.Id).ToList();

            if (pageIds.ToList().Count > 0)
            {
                foreach (Data.ArtisanSectionPage sp in sectionPages)
                {
                    List<ArtisanPage> p = section.Pages.Where(pg => pg.Id == sp.PageID).ToList();

                    if (p.Count == 1)
                    {
                        ArtisanPage page = p.First<ArtisanPage>();
                        if (page.Sort == 0)
                        {
                            page.Sort = sp.Sort;
                        }
                    }
                }
            }

            section.Pages.Sort(ArtisanPageComparer.CompareArtisanPageSort);
            /*if (sectionClientIds.ContainsKey(section.Id))
            {
                section.ClientId = sectionClientIds[section.Id];
            }*/

            var sort = 0;
            foreach (var page in section.Pages)
            {
                ArtisanTemplate t = templates.FirstOrDefault(x => x.Id == page.TemplateId);
                if (t != null)
                {
                    page.CssText = t.CssText;
                }
                page.Answers = answers.Where(a => a.PageId == page.Id).ToList();
                /*if (pageClientIds.ContainsKey(page.Id))
                {
                    page.ClientId = pageClientIds[page.Id];
                }*/
                page.Sort = sort++;
            }
            foreach (var subsection in section.Sections)
            {
                BuildCourseSection(subsection, courseSections, coursePages, answers, templates, sectionPages);
            }

            // null out zero count items
            section.Sections = (section.Sections.Count == 0 ? null : section.Sections);
            section.Pages = (section.Pages.Count == 0 ? null : section.Pages);
        }

        public string GenerateDynamicThemeCss(ArtisanTheme theme, Theme themeFlavor)
        {
            string themeCachePath = Path.Combine(theme.GetFolder(), theme.GetSkinFolder(), "_cache", themeFlavor.CodeName, "skin.css");

            // If we've already created the file, and the themes haven't changed, use it
            if (File.Exists(themeCachePath))
            {
                DateTime lastWrite;
                if (DataService.GetInstance("Symphony").UseUtc)
                {
                    lastWrite = File.GetLastWriteTimeUtc(themeCachePath);
                }
                else
                {
                    lastWrite = File.GetLastWriteTime(themeCachePath);
                }

                if (lastWrite > theme.ModifiedOn &&
                    lastWrite > themeFlavor.ModifiedOn)
                {
                    return File.ReadAllText(themeCachePath);
                }
            }

            string compiledCss = new DynamicThemeBuilder().GetDynamicThemeCss(themeFlavor, theme.GetScssPath(), theme.GetScssVariablesTemplatePath());
            FileInfo file = new FileInfo(themeCachePath);
            file.Directory.Create(); // make sure cache dir exists first
            File.WriteAllText(file.FullName, compiledCss);
            
            return compiledCss;
        }

        public string GenerateDynamicThemeCss(string themeName, string themeFlavorName)
        {
            // This points to the SCSS files to use when generating the theme
            ArtisanTheme theme = Select.AllColumnsFrom<Data.ArtisanTheme>()
                .Where(Data.ArtisanTheme.SkinNameColumn).IsEqualTo(themeName)
                .ExecuteSingle<ArtisanTheme>();

            // This points to the variables to use when generating the theme
            Theme themeFlavor = Select.AllColumnsFrom<Data.Theme>()
                .Where(Data.Theme.CodeNameColumn).IsEqualTo(themeFlavorName)
                .ExecuteSingle<Theme>();

            if (theme == null || theme.Id == 0)
            {
                throw new Exception("Could not find Symphony.dbo.ArtisanTheme with skin: " + themeName);
            }

            if (themeFlavor == null || themeFlavor.ID == 0)
            {
                throw new Exception("Could not find Symphony.dbo.Theme with code name: " + themeFlavorName);
            }

            if (!theme.IsDynamic)
            {
                throw new Exception("The Symphony.dbo.ArtisanTheme with skin " + themeName + " is not configured as a dynamic theme. There is nothing to generate.");
            }

            return GenerateDynamicThemeCss(theme, themeFlavor);
        }


        private void BuildCourseCollections(int courseId, int sectionSort, int sectionInternalParentId,
            ArtisanSection section, ref int sectionCount, ref int pageCount,
            Data.ArtisanSectionCollection sectionCollection,
            Data.ArtisanPageCollection pageCollection,
            Data.ArtisanSectionPageCollection sectionPageCollection,
            Data.ArtisanAnswerCollection answerCollection,
            bool isMasteryEnabled)
        {
            Data.ArtisanSection s = new Data.ArtisanSection();
            section.CopyTo(s);

            if (isMasteryEnabled)
            {
                // Make sure we always use a normal learning object.
                section.IsSinglePage = false;
                section.IsQuiz = false;
            }

            s.Id = section.Id;  // can this be removed?
            s.ClientID = section.Id;
            s.CourseID = courseId;
            s.SectionID = 0; // Updated after
            s.Sort = sectionSort;
            s.TestOut = section.TestOut;  // can this be removed?
            s.TestType = (int)section.TestType;
            s.IsQuiz = section.IsQuiz;
            s.MaxTime = section.MaxTime;
            s.MinimumPageTime = section.MinimumPageTime;
            s.MaximumQuestionTime = section.MaximumQuestionTime;

            if (s.Objectives == null)
            {
                s.Objectives = "";
            }
            if (s.Description == null)
            {
                s.Description = "";
            }

            sectionCollection.Add(s);
            sectionCount++;
            s.InternalID = sectionCount;
            s.InternalParentID = sectionInternalParentId;

            int sort = 1;

            if (section.Pages != null)
            {
                foreach (ArtisanPage p in section.Pages)
                {
                    pageCount++;

                    Data.ArtisanPage pg = new Data.ArtisanPage();
                    p.CopyTo(pg);
                    pg.ClientID = p.Id;
                    pg.CourseID = courseId;
                    pg.PageType = (int)p.PageType;
                    pg.QuestionType = (int)p.QuestionType;
                    pg.CorrectResponse = p.CorrectResponse ?? string.Empty;
                    pg.IncorrectResponse = p.IncorrectResponse ?? string.Empty;
                    pg.CalculateScore = p.CalculateScore;
                    pg.IsGradable = p.IsGradable;
                    pg.ReferencePageID = p.ReferencePageId;
                    pg.InternalSectionID = sectionCount;
                    pg.InternalID = pageCount;

                    pageCollection.Add(pg);


                    Data.ArtisanSectionPage sp = new Data.ArtisanSectionPage();
                    sp.PageID = 0; // Updated after
                    sp.SectionID = 0; // Updated after
                    sp.InternalPageID = pageCount;
                    sp.InternalSectionID = sectionCount;
                    sp.Sort = sort++;
                    sp.ExternalID = pg.ExternalID;
                    sp.CourseID = courseId;

                    sectionPageCollection.Add(sp);


                    if (p.Answers != null)
                    {
                        foreach (ArtisanAnswer answer in p.Answers)
                        {
                            Data.ArtisanAnswer answerData = new Data.ArtisanAnswer();
                            answerData.TextX = answer.Text;
                            answerData.PageID = 0; // Updated after
                            answerData.InternalPageID = pg.InternalID;
                            answerData.Sort = answer.Sort;
                            answerData.IsCorrect = answer.IsCorrect;
                            answerData.ExternalID = p.ExternalID;
                            answerData.CourseID = courseId;
                            answerData.Feedback = answer.Feedback;
                            answerData.Save();
                        }
                    }
                }
            }

            if (section.Sections != null)
            {
                int sSort = 1;
                foreach (ArtisanSection sc in section.Sections)
                {
                    BuildCourseCollections(courseId, sSort, s.InternalID, sc, ref sectionCount, ref pageCount, sectionCollection, pageCollection, sectionPageCollection, answerCollection, isMasteryEnabled);
                    sSort++;
                }
            }
        }

        public SingleResult<ArtisanCourse> SaveCourse(int customerId, int userId, int courseId, ArtisanCourse course)
        {
            Data.ArtisanCourse data = new Data.ArtisanCourse(courseId);

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                if (course.IsMasteryEnabled)
                {
                    // Default the mastery level
                    course.MasteryLevel = course.MasteryLevel > 0 ? course.MasteryLevel : (int)ArtisanMasteryLevel.LevelOne;
                    // Navigation method should be sequential or full sequential
                    course.NavigationMethod = (course.NavigationMethod == ArtisanCourseNavigationMethod.FullSequential ||
                                                course.NavigationMethod == ArtisanCourseNavigationMethod.Sequential)
                                                    ? course.NavigationMethod
                                                    : ArtisanCourseNavigationMethod.Sequential;
                    // Always using inline questions for mastery mode
                    course.CompletionMethod = ArtisanCourseCompletionMethod.InlineQuestions;
                }

                // Set the course image to the current image so we can decide later
                // if the image has been deleted or not. 
                string newAssociatedImageData = course.AssociatedImageData;
                course.AssociatedImageData = data.AssociatedImageData;

                course.CopyTo(data);
                
                data.Id = courseId;
                data.CustomerID = customerId;
                data.Name = course.Name;

                // Sending -1 in for the image data if it has been removed
                int imageData = 0;
                if (int.TryParse(newAssociatedImageData, out imageData))
                {
                    if (imageData == -1)
                    {
                        data.AssociatedImageData = null;
                        newAssociatedImageData = null;
                    }
                }

                if (!string.IsNullOrEmpty(newAssociatedImageData))
                {
                    data.AssociatedImageData = newAssociatedImageData;
                }

                data.Description = course.Description ?? string.Empty;
                data.Keywords = course.Keywords ?? string.Empty;
                data.ThemeID = course.ThemeId;
                data.ContactEmail = course.ContactEmail ?? string.Empty;
                data.Disclaimer = course.Disclaimer ?? string.Empty;
                data.CompletionMethod = (int)course.CompletionMethod;
                data.NavigationMethod = (int)course.NavigationMethod;
                data.Accreditation = course.Accreditation;
                data.PassingScore = course.PassingScore;
                data.IsPublished = course.IsPublished;
                data.IsAutoPublish = course.IsAutoPublish;
                data.MarkingMethod = (int)course.MarkingMethod;
                data.TimeoutMethod = (int)course.TimeoutMethod;
                data.ReviewMethod = (int)course.ReviewMethod;
                data.InactivityTimeout = course.InactivityTimeout;
                data.MinimumPageTime = course.MinimumPageTime;
                data.MaximumQuestionTime = course.MaximumQuestionTime;
                data.MaxTime = course.MaxTime;
                data.Parameters = course.Parameters;
                data.DefaultParametersJSON = "[]";
                data.BookmarkingMethod = (int)course.BookmarkingMethod;
                if (course.DefaultParameters != null)
                {
                    data.DefaultParametersJSON = Utilities.Serialize(course.DefaultParameters);
                }

                List<string> errors = new List<string>();

                if (data.CategoryID == 0)
                {
                    errors.Add("Please select a category.");
                }

                if (data.PassingScore < 0 || data.PassingScore > 100)
                {
                    errors.Add("Please use a passing score between 0 and 100");
                }

                if (data.CompletionMethod == 0)
                {
                    errors.Add("Please select a valid completion method.");
                }

                if (data.NavigationMethod == 0)
                {
                    errors.Add("Please select a valid navigation method.");
                }

                if (errors.Count > 0)
                {
                    throw new Exception(string.Join("<br/>", errors.ToArray()));
                }

                bool isNew = data.Id <= 0;
                data.Save(Username);

                courseId = data.Id;

                // delete old sections/maps/pages/answers
                new Delete().From<Data.ArtisanAnswer>().Where(Data.ArtisanAnswer.PageIDColumn).In(
                    new Select(Data.ArtisanPage.IdColumn)
                        .From<Data.ArtisanPage>()
                        .Where(Data.ArtisanPage.CourseIDColumn).IsEqualTo(courseId)
                ).Execute();
                new Delete().From<Data.ArtisanSectionPage>().Where(Data.ArtisanSectionPage.SectionIDColumn).In(
                    new Select(Data.ArtisanSection.IdColumn)
                        .From<Data.ArtisanSection>()
                        .Where(Data.ArtisanSection.CourseIDColumn).IsEqualTo(courseId)
                ).Execute();
                new Delete().From<Data.ArtisanSection>().Where(Data.ArtisanSection.CourseIDColumn).IsEqualTo(courseId).Execute();
                new Delete().From<Data.ArtisanPage>().Where(Data.ArtisanPage.CourseIDColumn).IsEqualTo(courseId).Execute();

                // Authours
                var authorsDbData = Select.AllColumnsFrom<Data.ArtisanCourseAuthorsLink>()
                    .Where(Data.ArtisanCourseAuthorsLink.Columns.ArtisanCourseId).IsEqualTo(courseId).ExecuteTypedList<Data.ArtisanCourseAuthorsLink>();

                if (course.Authors != null)
                {
                    var authorsNew = course.Authors.Where(a => authorsDbData.Count(adb => adb.UserId == a.ID) == 0);
                    foreach (Author a in authorsNew)
                    {
                        Data.ArtisanCourseAuthorsLink authorLink = new Data.ArtisanCourseAuthorsLink() { UserId = a.ID, ArtisanCourseId = courseId };
                        authorLink.Save();
                    }

                    var authorsDeleted = authorsDbData.Where(adb => course.Authors.Count(a => a.ID == adb.UserId) == 0);
                    foreach (Data.ArtisanCourseAuthorsLink alnk in authorsDeleted)
                    {
                        new Delete().From<Data.ArtisanCourseAuthorsLink>()
                            .Where(Data.ArtisanCourseAuthorsLink.UserIdColumn).IsEqualTo(alnk.UserId)
                            .And(Data.ArtisanCourseAuthorsLink.ArtisanCourseIdColumn).IsEqualTo(alnk.ArtisanCourseId).Execute();
                    }

                }

                //Secondary Categories
                if (course.SecondaryCategories != null)
                {
                    foreach (Category secondaryCategory in course.SecondaryCategories)
                    {
                        Data.SecondaryCategory secondaryCategoryDbData = new Data.SecondaryCategory(Data.SecondaryCategory.Columns.Name, secondaryCategory.Name);
                        if (secondaryCategoryDbData == null || string.IsNullOrEmpty(secondaryCategoryDbData.Name))
                        {
                            secondaryCategoryDbData = new Data.SecondaryCategory() { Name = secondaryCategory.Name };
                            secondaryCategoryDbData.Save();
                            secondaryCategoryDbData = new Data.SecondaryCategory(Data.SecondaryCategory.Columns.Name, secondaryCategory.Name);
                            Data.ArtisanCourseSecondaryCategoryLink scLink = new Data.ArtisanCourseSecondaryCategoryLink() { ArtisanCourseId = courseId, SecondaryCategoryId = secondaryCategoryDbData.Id };
                            scLink.Save();
                        }
                        else
                        {
                            Data.ArtisanCourseSecondaryCategoryLink scLink = new Data.ArtisanCourseSecondaryCategoryLink(Data.ArtisanCourseSecondaryCategoryLink.Columns.SecondaryCategoryId, secondaryCategoryDbData.Id);
                            if (scLink == null)
                            {
                                scLink = new Data.ArtisanCourseSecondaryCategoryLink() { ArtisanCourseId = courseId, SecondaryCategoryId = scLink.Id };
                                scLink.Save();
                            }
                        }
                    }

                    var secondaryCategoryData = Select.AllColumnsFrom<Data.SecondaryCategory>()
                        .InnerJoin(Data.Tables.ArtisanCourseSecondaryCategoryLink, Data.ArtisanCourseSecondaryCategoryLink.Columns.SecondaryCategoryId, Data.Tables.SecondaryCategory, Data.SecondaryCategory.Columns.Id)
                        .Where(Data.ArtisanCourseSecondaryCategoryLink.Columns.ArtisanCourseId).IsEqualTo(courseId).ExecuteTypedList<Data.SecondaryCategory>();

                    foreach (Data.SecondaryCategory secondaryCatergory in secondaryCategoryData)
                    {
                        if (course.SecondaryCategories.Count(d => d.Name == secondaryCatergory.Name) == 0)
                        {
                            new Delete().From<Data.ArtisanCourseSecondaryCategoryLink>().Where(Data.ArtisanCourseSecondaryCategoryLink.Columns.SecondaryCategoryId).IsEqualTo(secondaryCatergory.Id).Execute();
                        }
                    }
                }

                if (course.Accreditations != null)
                {
                    if (isNew)
                    {
                        foreach (var accreditation in course.Accreditations)
                        {
                            accreditation.Id = 0;
                        }
                    }

                    Accreditation.SyncArtisanAccreditations(courseId, course.Accreditations);
                }

                // Build flat lists for sections and pages
                Data.ArtisanSectionCollection sectionDataCollection = new Data.ArtisanSectionCollection();
                Data.ArtisanPageCollection pageDataCollection = new Data.ArtisanPageCollection();
                Data.ArtisanSectionPageCollection sectionPageCollection = new Data.ArtisanSectionPageCollection();
                Data.ArtisanAnswerCollection answerCollection = new Data.ArtisanAnswerCollection();
                int sectionCount = 0; int pageCount = 0;

                int sectionSort = 1;
                foreach (ArtisanSection s in course.Sections)
                {
                    BuildCourseCollections(courseId, sectionSort, 0, s, ref sectionCount, ref pageCount, sectionDataCollection, pageDataCollection, sectionPageCollection, answerCollection, course.IsMasteryEnabled);
                    sectionSort++;
                }

                // Send all data to db
                sectionDataCollection.BatchSave();
                pageDataCollection.BatchSave();
                sectionPageCollection.BatchSave();
                answerCollection.BatchSave();

                // Call sql updates to copy the ids
                string sql = string.Format(@"
                    -- 1. Update parent ids on sections
                    update
	                    ArtisanSections
                    set
	                    ArtisanSections.SectionID = COALESCE(parents.ID, 0)
                    from
	                    ArtisanSections
                    left join
	                    ArtisanSections parents
                    on
	                    ArtisanSections.InternalParentID = parents.InternalID
	                    and parents.CourseID = {0}
                    where
	                    ArtisanSections.CourseID = {0}
	                    and ArtisanSections.InternalParentID > 0;
                    -- 2. update ArtisanSectionPages to map pages to sections
                    update 
	                    ArtisanSectionPages
                    set
	                    ArtisanSectionPages.SectionID = s.ID,
	                    ArtisanSectionPages.PageID = p.ID
                    from
	                    ArtisanSectionPages
                    inner join
	                    ArtisanPages p
                    on
	                    ArtisanSectionPages.InternalPageID = p.InternalID
	                    and p.CourseID = {0}
                    inner join
	                    ArtisanSections s
                    on
	                    ArtisanSectionPages.InternalSectionID = s.InternalID
	                    and s.CourseID = {0}
                    where
	                    ArtisanSectionPages.CourseID = {0}
                    -- 3. Update artisan answers to map to a page
                    update
	                    ArtisanAnswers
                    set
	                    ArtisanAnswers.PageID = p.ID
                    from
	                    ArtisanAnswers
                    inner join
	                    ArtisanPages p
                    on
	                    ArtisanAnswers.InternalPageID = p.InternalID
	                    and p.CourseID = {0}
                    where
	                    ArtisanAnswers.CourseID = {0}
                    -- 4. Update reference pages. Reference pages will point to the Page ClientID
                    -- ClientID is the last ID used (As received from the Client) 
                    -- So transform the referenceID to the new pageID by looking up the clientID
                    update
                        ArtisanPages
                    set
                        ArtisanPages.ReferencePageID = p.ID
                    from
                        ArtisanPages
                    inner join
                        ArtisanPages p
                    on
                        ArtisanPages.ReferencePageID = p.ClientID
                        and p.CourseID = {0}
                    where
                        ArtisanPages.CourseID = {0}
                        and ArtisanPages.ReferencePageID > 0
", courseId);

                new InlineQuery().Execute(sql);

                if (!isNew)
                {
                    try
                    {
                        int courseDuration = (int)Data.SPs.GetCourseDuration(data.Id).ExecuteScalar<int>();
                        data.AutomaticDuration = courseDuration;
                    }
                    catch (Exception e)
                    {
                        // Couldn't calculate duration. Happens when empty course is saved. 
                        data.AutomaticDuration = 0;
                    }
                }

                data.Save(Username);

                ts.Complete();
            }
            return GetCourse(customerId, userId, data.Id);
        }

        public SingleResult<bool> DeleteCourse(int customerId, int userId, int courseId)
        {
            Data.ArtisanCourse data = new Data.ArtisanCourse(courseId);
            if (data.CustomerID != customerId)
            {
                throw new Exception("You don't have permission to delete the Artisan course with id " + courseId);
            }
            Data.ArtisanCourse.Delete(courseId);

            // delete sections
            var sectionIds = new Select(
                    Data.ArtisanSection.IdColumn
                ).From(Data.Tables.ArtisanSection)
                .Where(Data.ArtisanSection.Columns.CourseID).IsEqualTo(courseId)
                .ExecuteTypedList<int>();

            foreach (var sectionId in sectionIds)
            {
                Data.ArtisanSection.Destroy(sectionId);
            }

            // delete pages
            var pageIds = new Select(
                    Data.ArtisanPage.IdColumn
                ).From(Data.Tables.ArtisanPage)
                .Where(Data.ArtisanPage.Columns.CourseID).IsEqualTo(courseId)
                .ExecuteTypedList<int>();

            foreach (var pageId in pageIds)
            {
                Data.ArtisanPage.Destroy(pageId);
            }

            return new SingleResult<bool>(true);
        }

        public PagedResult<ArtisanTemplate> FindTemplates(int customerId, int userId, int pageType, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.ArtisanTemplate.Columns.Name;
            orderBy = "[dbo].[" + Data.Tables.ArtisanTemplate + "].[" + orderBy + "]";

            SqlQuery query = new Select(
                    Data.ArtisanTemplate.DescriptionColumn.QualifiedName,
                    Data.ArtisanTemplate.IdColumn.QualifiedName,
                    Data.ArtisanTemplate.HtmlColumn.QualifiedName,
                    Data.ArtisanTemplate.ThumbPathColumn.QualifiedName,
                    Data.ArtisanTemplate.PageTypeColumn.QualifiedName,
                    Data.ArtisanTemplate.QuestionTypeColumn.QualifiedName,
                    Data.ArtisanTemplate.NameColumn.QualifiedName,
                    Data.ArtisanTemplate.CssTextColumn.QualifiedName
                ).From(Data.Tables.ArtisanTemplate)
                .Where(Data.ArtisanTemplate.NameColumn).ContainsString(searchText)
                .And(Data.ArtisanTemplate.PageTypeColumn).IsEqualTo(pageType);

            PagedResult<ArtisanTemplate> result = new PagedResult<ArtisanTemplate>(query, pageIndex, pageSize, orderBy, orderDir);
            result.Data = result.Data.OrderBy(t =>
            {
                var digit = Regex.Match(t.Name, @"\d+").Value;
                if (!string.IsNullOrEmpty(digit))
                {
                    return int.Parse(digit);
                }
                return 0;
            });

            return result;
        }

        public SingleResult<ArtisanTemplate> GetTemplate(int customerId, int userId, int templateId)
        {
            SqlQuery query = new Select(
                    Data.ArtisanTemplate.DescriptionColumn.QualifiedName,
                    Data.ArtisanTemplate.IdColumn.QualifiedName,
                    Data.ArtisanTemplate.HtmlColumn.QualifiedName,
                    Data.ArtisanTemplate.ThumbPathColumn.QualifiedName,
                    Data.ArtisanTemplate.PageTypeColumn.QualifiedName,
                    Data.ArtisanTemplate.NameColumn.QualifiedName,
                    Data.ArtisanTemplate.CssTextColumn.QualifiedName
                ).From(Data.Tables.ArtisanTemplate)
                .Where(Data.ArtisanTemplate.IdColumn).IsEqualTo(templateId);

            return new SingleResult<ArtisanTemplate>(query);
        }

        public PagedResult<ArtisanAsset> FindAssets(int customerId, int userId, int assetTypeId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, List<ArtisanAssetFilter> filter)
        {
            // assets require a filter - if it's not set, they don't have any asset types
            // selected, so bail out immediately
            if (filter == null)
            {
                return new PagedResult<ArtisanAsset>(new List<ArtisanAsset>(), pageIndex, pageSize, 0);
            }

            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.ArtisanAsset.Columns.Name;
            orderBy = "[dbo].[" + Data.Tables.ArtisanAsset + "].[" + orderBy + "]";

            SqlQuery query = Select.AllColumnsFrom<Data.ArtisanAsset>();

            bool search = true;
            if (searchText.StartsWith("#"))
            {
                var num = searchText.Trim('#');
                int x = 0;
                if (int.TryParse(num, out x))
                {
                    query.Where(Data.ArtisanAsset.Columns.Id).IsEqualTo(x);
                    search = false;
                }
            }

            if (search)
            {
                query.WhereExpression(Data.ArtisanAsset.Columns.Name).ContainsString(searchText)
                    .Or(Data.ArtisanAsset.Columns.Description).ContainsString(searchText)
                    .Or(Data.ArtisanAsset.Columns.Keywords).ContainsString(searchText)
                    .CloseExpression();
            }

                
            query
                //.AndExpression(Data.ArtisanAsset.Columns.
                .AndExpression(Data.ArtisanAsset.Columns.FromImport).IsEqualTo(false)
                .CloseExpression()
                .AndExpression(Data.ArtisanAsset.Columns.CustomerID).IsEqualTo(customerId)
                .Or(Data.ArtisanAsset.Columns.IsPublic).IsEqualTo(true)
                .CloseExpression();

            foreach (ArtisanAssetFilter f in filter)
            {
                if (filter.First() == f)
                {
                    query = query.AndExpression(Data.ArtisanAsset.Columns.AssetTypeID).IsEqualTo(f.AssetTypeId);
                }
                else
                {
                    query = query.Or(Data.ArtisanAsset.Columns.AssetTypeID).IsEqualTo(f.AssetTypeId);
                }
            }

            var result = new PagedResult<ArtisanAsset>(query, pageIndex, pageSize, orderBy, orderDir);

            List<Data.ArtisanAssetType> assetTypes = Select.AllColumnsFrom<Data.ArtisanAssetType>().ExecuteTypedList<Data.ArtisanAssetType>();

            foreach (ArtisanAsset asset in result.Data)
            {
                AugmentAsset(asset, assetTypes.FirstOrDefault(t => t.Id == asset.AssetTypeId));
            }

            return result;
        }

        public SingleResult<ArtisanAsset> GetAsset(int customerId, int userId, int assetId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ArtisanAsset>()
                .Where(Data.ArtisanAsset.IdColumn).IsEqualTo(assetId)
                .And(Data.ArtisanAsset.CustomerIDColumn).IsEqualTo(customerId);

            var result = new SingleResult<ArtisanAsset>(query);

            if (result.Data != null)
            {
                AugmentAsset(result.Data, new Symphony.Core.Data.ArtisanAssetType(result.Data.AssetTypeId));
            }

            return result;
        }

        /// <summary>
        /// Responsible for adding the path, template, preview templates, and alternate html flag to the asset
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="assetType"></param>
        public void AugmentAsset(ArtisanAsset asset, Data.ArtisanAssetType assetType)
        {
            if (asset.Url != null)
            {
                asset.Path = asset.Url;
            }
            else
            {
                if (HttpContext.Current != null && Directory.Exists(HttpContext.Current.Server.MapPath(ArtisanAssetFilesLocation + asset.Id)))
                {
                    asset.Path = Utilities.ResolveUrl(ArtisanAssetFilesLocation + asset.Id + "/" + Path.GetFileName(asset.Filename));
                }
                else if (HttpContext.Current != null)
                {
                    asset.Path = Utilities.ResolveUrl(ArtisanAssetFilesLocation + asset.Id + Path.GetExtension(asset.Filename));
                }
                else
                {
                    asset.Path = ArtisanAssetRoot + asset.Id + Path.GetExtension(asset.Filename);
                }
            }

            if (assetType != null)
            {
                asset.Template = assetType.Template;
                asset.PreviewTemplate = assetType.PreviewTemplate;
                asset.HasAlternateHtml = assetType.HasAlternateHtml;
            }
        }

        public PagedResult<Models.ArtisanAssetType> FindAssetTypes(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (string.IsNullOrEmpty(orderBy)) orderBy = Data.ArtisanAssetType.Columns.Name;
            orderBy = "[dbo].[" + Data.Tables.ArtisanAssetType + "].[" + orderBy + "]";

            SqlQuery query = new Select(
                    Data.ArtisanAssetType.DescriptionColumn.QualifiedName,
                    Data.ArtisanAssetType.IdColumn.QualifiedName,
                    Data.ArtisanAssetType.TemplateColumn.QualifiedName,
                    Data.ArtisanAssetType.ExtensionsColumn.QualifiedName,
                    Data.ArtisanAssetType.NameColumn.QualifiedName
                ).From(Data.Tables.ArtisanAssetType)
                .Where(Data.ArtisanAssetType.NameColumn).ContainsString(searchText)
                .And(Data.ArtisanAssetType.TemplateColumn).IsNotEqualTo(string.Empty);

            return new PagedResult<Models.ArtisanAssetType>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<Models.ArtisanAssetType> GetAssetType(int customerId, int userId, int assetTypeId)
        {
            SqlQuery query = new Select(
                    Data.ArtisanAssetType.DescriptionColumn.QualifiedName,
                    Data.ArtisanAssetType.IdColumn.QualifiedName,
                    Data.ArtisanAssetType.TemplateColumn.QualifiedName,
                    Data.ArtisanAssetType.ExtensionsColumn.QualifiedName,
                    Data.ArtisanAssetType.NameColumn.QualifiedName
                ).From(Data.Tables.ArtisanAssetType)
                .Where(Data.ArtisanAssetType.IdColumn).IsEqualTo(assetTypeId);

            return new SingleResult<Models.ArtisanAssetType>(query);
        }

        public SimpleSingleResult<string> PackageCourse(int customerId, int userId, ArtisanCourse artisanCourse, bool preview, bool deploy, bool update, ArtisanDeploymentTarget target)
        {
            SymphonyController cntr = new SymphonyController();
            if (cntr.HasRole(Roles.ArtisanAuthor)) // Must have Author permission to edit course
            {
                SaveCourse(customerId, userId, artisanCourse.Id, artisanCourse);
            }

            SingleResult<ArtisanCourse> c = GetCourse(customerId, userId, artisanCourse.Id);

            //AMTODO Perhaps save the default parameters in the db instead of copying them to the new object after save
            //c.Data.DefaultParameters = artisanCourse.DefaultParameters;

            // We used to create the package here, but we have to do that as the last thing so 
            // it has been moved to the BuildPackage helper function. Has to be last since
            // saving the course will update the ids. PublishCourse will hit save and advance the 
            // ids. We are avoiding the double save now, howver, even by avoiding that, the 
            // json in the package will be linked to the original course instead of the published
            // course.

            string outputFolder = this.DeployedCoursesFolder;
            string returnStr = "";


            if (deploy)
            {
                var autoPublishedCourse = PublishCourse(customerId, userId, artisanCourse, true, false);

                if (target == ArtisanDeploymentTarget.Symphony)
                {
                    List<int> symphonyCourseIds = new List<int>(); // new Symphony course
                    CourseAssignmentController controller = new CourseAssignmentController();
                    if (update)
                    {
                        // if this is an update, locate the courses for this customer and artisan course id, if possible
                        // (There could be multiple courses using the same artisan ID so make sure we update all of them)
                        MultipleResult<Course> existingCourses = controller.GetCourseByArtisanCourseId(customerId, artisanCourse.Id);
                        if (existingCourses.Data.Count > 0)
                        {
                            // found them! use these course ids when we import
                            symphonyCourseIds = existingCourses.Data.Select(co => co.Id).ToList<int>();
                        }
                    }

                    // Build the package based on the published course data
                    FileInfo fi = BuildPackage(autoPublishedCourse.Data);

                    // No existing courses were found, so add 0 to ensure
                    // we still hit the loop.
                    if (symphonyCourseIds.Count == 0)
                    {
                        symphonyCourseIds.Add(0);
                    }

                    string path = ""; // so we only have to build the course once.
                    Course symphonyCourse;

                    foreach (int symphonyCourseId in symphonyCourseIds)
                    {
                        if (string.IsNullOrEmpty(path))
                        {
                            // import the course;
                            // slightly modified folder so we can track these differently from the "non-shared" imports
                            symphonyCourse = controller.ImportCourseFromZipFile(customerId, userId, symphonyCourseId, fi.ToString(), customerId.ToString());
                            path = symphonyCourse.ImportResult.ManifestFilePath;
                        }
                        else
                        {
                            symphonyCourse = controller.ImportCourseFromFolder(customerId, userId, symphonyCourseId, path);
                        }

                        // store the ArtisanCourse IDs so we can use it on future deploys
                        Data.OnlineCourse oc = Data.OnlineCourse.FetchByID(symphonyCourse.Id);
                        oc.ArtisanCourseID = autoPublishedCourse.Data.OriginalCourseId.Value;
                        oc.PublishedArtisanCourseID = autoPublishedCourse.Data.Id;
                        oc.CategoryID = CategoryController.CopyCategory(customerId, CategoryType.OnlineCourse, artisanCourse.CategoryId).Id;
                        oc.CertificateEnabled = autoPublishedCourse.Data.CertificateEnabled;
                        oc.Keywords = autoPublishedCourse.Data.Keywords;
                        oc.Duration = autoPublishedCourse.Data.Duration;
                        oc.AutomaticDuration = autoPublishedCourse.Data.AutomaticDuration;
                        oc.IsUseAutomaticDuration = autoPublishedCourse.Data.IsUseAutomaticDuration;
                        oc.Save();

                        AddSecondaryCategory(oc, c);
                        AddAuthors(oc, c);

                        // Left this here as a warning. The accreditations are imported above when the
                        // ImportFromX methods are called, so you if you do this here, it will double
                        // import, =)
                        // AddAccreditations(oc, c);

                        AuditVersion(oc, autoPublishedCourse.Data, path);

                        // Grabs the assignments from the course and saves them in the
                        // AssignmentSummary table for easy lookup later. This avoids having
                        // to look up assignments based on all of the artisan pages in the system
                        AssignmentController assignmentController = new AssignmentController();
                        assignmentController.SummarizeAssignments(oc.Id, oc.PublishedArtisanCourseID.Value);

                        if (string.IsNullOrEmpty(returnStr))
                        {
                            // TODO: Do something smarter here? 
                            // I assume we are looking for an id for the specific course
                            // that we updated. Since this is always assumed to be one course
                            // lets just return the id of the first one like we normally would.
                            // Since the query orders by id descending, the first one
                            // should be the most recent course.
                            // If no courses were found, then there will only be one id
                            // to return anyway. 
                            returnStr = symphonyCourse.Id.ToString();
                        }
                    }
                }
                else
                {
                    // deploying to MARS is just creating an entry in a table and packaging
                    Data.ArtisanDeployment deployment = new Data.ArtisanDeployment();
                    deployment.CourseIdsText = autoPublishedCourse.Data.Id.ToString();
                    deployment.CustomerIdsText = customerId.ToString();
                    deployment.Name = "Automatic";
                    deployment.Notes = "Deployed as single package";
                    deployment.IsUpdate = false;
                    deployment.IsUpdateOnly = false;
                    deployment.ToArtisan = false;
                    deployment.ToMars = true;
                    deployment.ToSymphony = false;
                    deployment.Save(Username);

                    // reload; some settings get killed if we don't
                    c = GetCourse(customerId, userId, artisanCourse.Id);
                    Generators.PackageBuilder newbuilder = new Symphony.Core.Generators.PackageBuilder(c.Data, outputFolder);
                    newbuilder.Package();
                }
            }
            else
            {
                // This is just a regular package, so we haven't created a published version fo this course
                // so we can just build the package from the last saved version of the course
                FileInfo fi = BuildPackage(c.Data);

                if (preview)
                {
                    string previewLocation = Path.Combine(new SymphonyController().GetArtisanPreviewLocation(), artisanCourse.Id.ToString());
                    Utilities.CopyFolder(Path.Combine(Path.GetDirectoryName(fi.FullName), artisanCourse.Id.ToString()), previewLocation);
                    fi = new FileInfo(previewLocation);
                }

                returnStr = Utilities.ResolveUrl(Utilities.ReverseMapPath(fi.FullName));
            }
            return new SimpleSingleResult<string>(returnStr);
        }

        private void AddAuthors(Data.OnlineCourse oc, SingleResult<ArtisanCourse> c)
        {
            foreach (Author author in c.Data.Authors)
            {
                Data.OnlineCourseAuthorLink link = new Data.OnlineCourseAuthorLink() { UserId = author.ID, OnlineCourseId = oc.Id };
                link.Save();
            }
        }

        private void AddSecondaryCategory(Data.OnlineCourse oc, SingleResult<ArtisanCourse> c)
        {
            foreach (Category secondaryCategory in c.Data.SecondaryCategories)
            {
                Data.OnlineCourseSecondaryCategoryLink link = new Data.OnlineCourseSecondaryCategoryLink() { OnlineCourseId = oc.Id, SecondaryCategoryId = secondaryCategory.Id };
                link.Save();
            }
        }

        /// <summary>
        /// Helper so we can build the package after we do save and copy of the course
        /// otherwise when packaging before the course save, the package wont have the correct ids
        /// </summary>
        /// <returns></returns>
        private FileInfo BuildPackage(ArtisanCourse course)
        {
            Generators.PackageBuilder builder = new Symphony.Core.Generators.PackageBuilder(course);
            FileInfo fi = builder.Package();

            return fi;
        }

        public SingleResult<ArtisanCourse> PublishCourse(int customerId, int userId, ArtisanCourse course, bool isAutoPublish = false, bool isSaveRequired = true)
        {
            if (isSaveRequired) // Sometimes this is called within other functions that already save the course
            // this will allow for a few less saves.
            {
                SaveCourse(customerId, userId, course.Id, course);
            }

            // get a new copy
            SingleResult<ArtisanCourse> c = GetCourse(customerId, userId, course.Id);

            // We don't need the package, but need to make sure it passes the package validation and packaging process
            ArtisanCourse ac = Utilities.Deserialize<ArtisanCourse>(Utilities.Serialize(c.Data));
            Generators.PackageBuilder builder = new Symphony.Core.Generators.PackageBuilder(ac, Path.GetTempPath());
            FileInfo fi = builder.Package();
            try
            {
                File.Delete(fi.FullName);
            }
            catch { }

            // create a copy using the retrieved course
            ArtisanCourse newCourse = c.Data;
            newCourse.IsPublished = true;
            newCourse.IsAutoPublish = isAutoPublish;
            newCourse.OriginalCourseId = c.Data.Id;
            newCourse.ClearIds();

            int newCourseId = -1; // -1 for a new course
            SingleResult<ArtisanCourse> finalCourse = SaveCourse(customerId, userId, newCourseId, newCourse);

            // now we need to map the course page ids from the original to the new one
            // This calls a save so all ids will be advanced so we need that final course back to build the package from
            finalCourse = TransformQuestionPageReferences(customerId, userId, finalCourse.Data, newCourse.CustomerId, newCourse.OriginalCourseId.Value);

            return finalCourse;
        }

        /// <summary>
        /// Duplicates an artisan course
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="artisanCourseId"></param>
        /// <returns></returns>
        public SingleResult<ArtisanCourse> DuplicateCourse(int customerId, int userId, ArtisanCourse course)
        {
            var temp = SaveCourse(customerId, userId, course.Id, course);

            // get a new copy
            SingleResult<ArtisanCourse> c = GetCourse(customerId, userId, course.Id);

            ArtisanCourse newCourse = c.Data;
            newCourse.IsPublished = false;
            newCourse.IsAutoPublish = false;
            newCourse.OriginalCourseId = 0;
            newCourse.Name = "Copy of " + newCourse.Name;
            newCourse.ClearIds();

            int newCourseId = -1; // -1 for a new course
            SingleResult<ArtisanCourse> finalCourse = SaveCourse(customerId, userId, newCourseId, newCourse);

            // now we need to map the course page ids from the original to the new one
            TransformQuestionPageReferences(customerId, userId, finalCourse.Data, newCourse.CustomerId, temp.Data.Id);

            return finalCourse;
        }

        public FileInfo ExportCourse(int customerId, int userId, int courseId, bool contentOnly)
        {
            SingleResult<ArtisanCourse> c = GetCourse(customerId, userId, courseId);
            Generators.ArtisanCourseDocumentExport exporter = new Symphony.Core.Generators.ArtisanCourseDocumentExport(c.Data);
            exporter.ContentOnly = contentOnly;
            return exporter.Export();
        }

        public Stream GetPackage(int customerId, int userId, int courseId)
        {
            Generators.PackageBuilder builder = new Symphony.Core.Generators.PackageBuilder();
            FileInfo fi = builder.GetPackageFolder(courseId);

            return new FileStream(fi.FullName, FileMode.Open);
        }

        public SingleResult<Models.ArtisanAsset> UploadArtisanAsset(int customerId, string filename, string name, string description, HttpPostedFile file)
        {
            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }
            return UploadArtisanAsset(customerId, filename, name, description, fileData);
        }

        private static Data.ArtisanAssetType ValidateAssetType(string extension)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ArtisanAssetType>()
                .Where(Data.ArtisanAssetType.Columns.Extensions).ContainsString(extension);

            Data.ArtisanAssetType result = query.ExecuteSingle<Data.ArtisanAssetType>();

            if (result != null)
            {
                return result;
            }
            else
            {
                throw new Exception("Invalid asset selected.");
            }
        }
        public SingleResult<Models.ArtisanAsset> UploadArtisanAsset(int customerId, string filename, string name, string description, byte[] file)
        {
            return UploadArtisanAsset(customerId, filename, name, description, file, null, null);
        }
        public SingleResult<Models.ArtisanAsset> UploadArtisanAsset(int customerId, string filename, string name, string description, byte[] file, string basePath, string externalId)
        {
            string extension = Path.GetExtension(filename).Remove(0, 1);
            Data.ArtisanAssetType type = ValidateAssetType(extension);

            Data.ArtisanAsset asset = new Symphony.Core.Data.ArtisanAsset();
            asset.Filename = filename;
            asset.AssetTypeID = type.Id;
            asset.Name = name ?? filename;
            asset.Description = description ?? filename;
            asset.Keywords = "";
            asset.CustomerID = customerId;
            asset.ExternalID = externalId;
            asset.Save(Username);

            string path = string.Empty;
            if (!string.IsNullOrEmpty(basePath))
            {
                path = basePath + ArtisanAssetFilesLocation.Replace("~", "") + asset.Id + "." + extension;
            }
            else
            {
                path = Path.Combine(ArtisanAssetFilesLocation, asset.Id + "." + extension);
                if (HttpContext.Current != null)
                {
                    path = HttpContext.Current.Server.MapPath(path);
                }
            }
            try
            {
                string directory = Path.GetDirectoryName(path);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw e;
            }

            File.WriteAllBytes(path, file);

            if (extension == "zip")
            {
                // special handling for zip files
                // for now, we're assuming all zip files are simply "grouped" assets
                // note that this means you can't have a zip file download within a course

                // create a directory matching the filename, unzip the file there, and trash the original
                string assetFilesLocation = string.Empty;
                if (HttpContext.Current != null)
                {
                    assetFilesLocation = HttpContext.Current.Server.MapPath(ArtisanAssetFilesLocation);
                }
                else
                {
                    assetFilesLocation = ArtisanAssetFilesLocation;
                }
                string folder = Path.Combine(Path.GetDirectoryName(assetFilesLocation), Path.GetFileNameWithoutExtension(path));
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                Utilities.UnZip(path, folder);
                File.Delete(path);

                // look for some specific files
                string primaryFile = string.Empty;
                foreach (string possiblePrimaryFile in new string[] { "index.html", "index.htm", "default.html", "default.htm" })
                {
                    if (File.Exists(Path.Combine(folder, possiblePrimaryFile)))
                    {
                        primaryFile = possiblePrimaryFile;
                        break;
                    }
                }

                // no default html file found; look for any swf files
                if (string.IsNullOrEmpty(primaryFile))
                {
                    foreach (string possibleSwfFile in Directory.GetFiles(folder))
                    {
                        if (possibleSwfFile.EndsWith("swf"))
                        {
                            primaryFile = possibleSwfFile;
                            break;
                        }
                    }
                }

                if (string.IsNullOrEmpty(primaryFile))
                {
                    Data.ArtisanAsset.Destroy(asset.Id);
                    throw new Exception("The zip file did not contain a file that could be determined to be a 'Primary' file.");
                }

                string primaryFileExtension = Path.GetExtension(primaryFile);
                SqlQuery query = Select.AllColumnsFrom<Data.ArtisanAssetType>()
                    .Where(Data.ArtisanAssetType.Columns.Extensions).ContainsString(primaryFileExtension.Trim('.'));

                Data.ArtisanAssetType result = query.ExecuteSingle<Data.ArtisanAssetType>();
                if (result == null)
                {
                    Data.ArtisanAsset.Destroy(asset.Id);
                    throw new Exception("The zip file did not contain a file that could be determined to be a 'Primary' file and could be matched to a valid file type.");
                }

                // make the appropriate adjustments
                asset.IsGrouped = true;
                asset.Filename = Path.Combine(folder, primaryFile);
                asset.AssetTypeID = result.Id;
                asset.Save();
            }

            return GetAsset(customerId, UserID, asset.Id);
        }

        public SingleResult<Models.ArtisanAsset> SaveAsset(int assetId, Models.ArtisanAsset asset)
        {
            Data.ArtisanAsset data = new Data.ArtisanAsset();
            if (assetId > 0)
            {
                data.LoadByKey(assetId);
                data.Name = asset.Name;
                data.Description = asset.Description;
                data.Keywords = asset.Keywords;
                data.Url = asset.Url;
                data.Width = asset.Width;
                data.Height = asset.Height;
                data.AlternateHtml = asset.AlternateHtml;
            }
            else
            {
                asset.CopyTo(data);
                string fullName = asset.Url ?? asset.Filename ?? " ";
                string extension = string.Empty;
                if (Path.GetExtension(fullName).Length > 0)
                {
                    extension = Path.GetExtension(fullName).Remove(0, 1);
                }
                string name = Path.GetFileNameWithoutExtension(fullName);

                Data.ArtisanAssetType type = ValidateAssetType(extension);
                if (string.IsNullOrEmpty(data.Keywords))
                {
                    data.Keywords = asset.Url ?? asset.Filename;
                }
                if (string.IsNullOrEmpty(data.Name))
                {
                    data.Name = name;
                }
                if (string.IsNullOrEmpty(data.Description))
                {
                    data.Description = name;
                }
                if (data.AssetTypeID == 0)
                {
                    data.AssetTypeID = type.Id;
                }

                if (type.Name == "Video")
                {
                    data.Width = 500;
                    data.Height = 500;
                }
            }

            data.CustomerID = this.CustomerID;
            data.Save(Username);

            return GetAsset(CustomerID, UserID, data.Id);
        }

        /// <summary>
        /// Map the course page ids from the original to the new one
        /// </summary>
        /// <param name="customerId">The current user's customer</param>
        /// <param name="userId">The current user</param>
        /// <param name="courseToTransform">The course whose reference pages will be modified</param>
        /// <param name="referenceCourseCustomerId">The original customer to whom the courseToTransform belongs to</param>
        /// <param name="referenceCourseId">The original course the courseToTransform was created from</param>
        public SingleResult<ArtisanCourse> TransformQuestionPageReferences(int customerId, int userId, ArtisanCourse courseToTransform, int referenceCourseCustomerId, int referenceCourseId)
        {
            if (courseToTransform.Id <= 0 || referenceCourseId <= 0)
            {
                throw new Exception("This method cannot run on a course without an id.");
            }

            SingleResult<ArtisanCourse> originalCourse = new ArtisanController().GetCourse(referenceCourseCustomerId, userId, referenceCourseId);

            if (courseToTransform.Sections != null)
            {
                foreach (var section in courseToTransform.Sections)
                {
                    section.TransformQuestionPageReferences(originalCourse.Data, courseToTransform);
                }
            }

            return SaveCourse(customerId, userId, courseToTransform.Id, courseToTransform);
        }

        public MultipleResult<ArtisanPage> ImportDocument(int customerId, string filename, HttpPostedFile file,
            int courseId, int sectionId, int? width, int? height, string scaleType, int? cropX, int? cropY)
        {
            return ImportDocument(customerId, filename, file, courseId, sectionId, width, height, scaleType, cropX,
                cropY, "", null, null, null);
        }
        public MultipleResult<ArtisanPage> ImportPDFDocumentToMultiplePDFPages(int customerId, string filename, HttpPostedFile file, int courseId, int sectionId, int? width, int? height, string scaleType, int? cropX, int? cropY, string externalId, Stream streamOverride, string nameOverride, string pathOverride)
        {
            return ImportDocument(customerId, filename, file, courseId, sectionId, width, height, scaleType, cropX, cropY, externalId, streamOverride, nameOverride, pathOverride, true);
        }
        public MultipleResult<ArtisanPage> ImportDocument(int customerId, string filename, HttpPostedFile file, int courseId, int sectionId, int? width, int? height, string scaleType, int? cropX, int? cropY, string externalId, Stream streamOverride, string nameOverride, string pathOverride, bool splitPdfPages = false)
        {
            string extension = Path.GetExtension(filename).Remove(0, 1).ToLower();
            string path;

            if (string.IsNullOrEmpty(pathOverride))
            {
                path = HttpContext.Current.Server.MapPath(ArtisanAssetFilesLocation + Guid.NewGuid() + "/" + filename);
            }
            else
            {
                path = pathOverride + ArtisanAssetFilesLocation.Replace("~", "") + Guid.NewGuid() + "/" + filename;
                //path = path.Replace("/", "\\");
            }
            string directory = Path.GetDirectoryName(path);


            try
            {
                if (!Directory.Exists(directory))
                {

                    Directory.CreateDirectory(directory);
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw e;
            }

            Stream stream;
            string fileType = "";
            if (streamOverride == null)
            {
                file.SaveAs(path);
                stream = File.OpenRead(path);
            }
            else
            {
                stream = streamOverride;
            }

            switch (extension)
            {
                case "ppt":
                    fileType = "PowerPoint";
                    Utilities.ConvertPowerPointToPng(stream, directory);
                    break;
                case "pptx":
                    fileType = "PowerPoint";
                    Utilities.ConvertPowerPointXToPng(stream, directory);
                    break;
                case "doc":
                case "docx":
                    fileType = "Word";
                    Utilities.ConvertWordToPng(stream, directory);
                    break;
                case "pdf":
                    fileType = "PDF";
                    if (splitPdfPages)
                    {
                        Utilities.ConvertPdfToMultiplePdfPages(stream, directory);
                    }
                    else
                    {
                        Utilities.ConvertPdfToPng(stream, directory);
                    }
                    break;
                default:
                    throw new Exception("The file type is invalid.");
            }

            try
            {
                File.Delete(path);
            }
            catch { }

            SqlQuery query = Select.AllColumnsFrom<Data.ArtisanAssetType>()
                .Where(Data.ArtisanAssetType.Columns.Extensions).ContainsString("png");

            SqlQuery htmlQuery = Select.AllColumnsFrom<Data.ArtisanAssetType>()
                .Where(Data.ArtisanAssetType.Columns.Extensions).ContainsString("html");

            Data.ArtisanAssetType result = query.ExecuteSingle<Data.ArtisanAssetType>();
            Data.ArtisanAssetType resultHtml = htmlQuery.ExecuteSingle<Data.ArtisanAssetType>();

            int typeId = 0;
            int typeIdHtml = 0;
            if (result != null)
            {
                typeId = result.Id;
            }
            if (resultHtml != null)
            {
                typeIdHtml = resultHtml.Id;
            }

            int count = 1;
            string[] files = Directory.GetFiles(directory);
            List<ArtisanPage> pages = new List<ArtisanPage>();
            foreach (string s in files)
            {
                // If needed, scale the image
                if (width != null && height != null && !splitPdfPages)
                {
                    Bitmap srcBmp = Image.FromFile(s) as Bitmap;

                    if (scaleType == "crop")
                    {
                        int x = cropX ?? 0;
                        int y = cropY ?? 0;

                        System.Drawing.Rectangle cropRect = new System.Drawing.Rectangle(x, y, (int)width, (int)height);
                        Bitmap targetBmp = new Bitmap(cropRect.Width, cropRect.Height);

                        using (Graphics g = Graphics.FromImage(targetBmp))
                        {
                            g.DrawImage(srcBmp, new System.Drawing.Rectangle(0, 0, targetBmp.Width, targetBmp.Height),
                                             cropRect,
                                             GraphicsUnit.Pixel);
                        }
                        srcBmp.Dispose();
                        targetBmp.Save(s);
                    }
                    else if (scaleType == "shrinkToFit")
                    {
                        float nPercent = 0;
                        float nPercentW = 0;
                        float nPercentH = 0;

                        nPercentW = ((float)width / (float)srcBmp.Width);
                        nPercentH = ((float)height / (float)srcBmp.Height);

                        if (nPercentH < nPercentW)
                            nPercent = nPercentH;
                        else
                            nPercent = nPercentW;

                        int destWidth = (int)(srcBmp.Width * nPercent);
                        int destHeight = (int)(srcBmp.Height * nPercent);

                        Bitmap targetBmp = new Bitmap(destWidth, destHeight);

                        using (Graphics g = Graphics.FromImage(targetBmp))
                        {
                            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            g.DrawImage(srcBmp, 0, 0, destWidth, destHeight);
                        }
                        srcBmp.Dispose();
                        targetBmp.Save(s);
                    }
                }

                string ext = extension == "pdf" && splitPdfPages ? ".pdf" : ".png";

                Data.ArtisanAsset asset = new Symphony.Core.Data.ArtisanAsset();
                asset.Filename = Path.GetFileNameWithoutExtension(s);
                asset.AssetTypeID = splitPdfPages ? typeIdHtml : typeId;
                asset.Name = "[" + fileType + "] " + (!string.IsNullOrEmpty(nameOverride) ? nameOverride : Path.GetFileNameWithoutExtension(filename)) + " (" + count++ + " of " + files.Length + ")";
                asset.Description = asset.Name;
                asset.Keywords = "";
                asset.FromImport = true;
                asset.CustomerID = customerId;
                asset.Save(Username);
                asset.ExternalID = externalId;
                string newPath;

                if (!string.IsNullOrEmpty(pathOverride))
                {
                    newPath = pathOverride + ArtisanAssetFilesLocation.Replace("~", "") + asset.Id + ext;
                    //newPath = path.Replace("/", "\\");
                }
                else
                {
                    newPath = HttpContext.Current.Server.MapPath(ArtisanAssetFilesLocation + asset.Id + ext);
                }
                File.Move(s, newPath);
                asset.Filename = Path.GetFileName(newPath);
                asset.Save(Username);

                string assetSource = Utilities.ResolveUrl(ArtisanAssetFilesLocation + asset.Id + ext);
                string template = "";
                
                if (splitPdfPages)
                {
                    Aspose.Pdf.Document doc = new Aspose.Pdf.Document(newPath);
                    
                    // Approximate page height, converting points to pixels
                    var pageHeight = Math.Floor(doc.Pages[1].MediaBox.Height * 1.333);

                    // Adding padding the pdf renderer uses (At least on Chrome anyway)
                    // This may still create a scrollbar on other browsers.
                    pageHeight += 100;

                    string pdfViewer = "pdfjs/web/viewer.html?file=";
                    template = "<iframe src='/{0}' data-assetsrc='"+assetSource+"' data-pdfviewersrc='" + pdfViewer + "../../' style='height:"+pageHeight+"px;width:100%;border:0;margin:0;padding:0' frameBorder='0'/>";
                    
                    assetSource = pdfViewer + assetSource;
                }
                else
                {
                    template = "<img style='max-width:100%' src='{0}' />";
                }

                string content = string.Format(template, assetSource);

                // create a page for the new asset
                Data.ArtisanPage page = new Symphony.Core.Data.ArtisanPage();
                page.Html = string.Format(WrapperMarkup, content, splitPdfPages ? " iframe-wrapper" : "");
                
                page.PageType = (int)ArtisanPageType.Content;
                page.Name = asset.Name;
                page.CourseID = courseId;
                page.ExternalID = externalId;

                pages.Add(Model.Create<Models.ArtisanPage>(page));
            }

            Directory.Delete(directory);

            return new MultipleResult<ArtisanPage>(pages);
        }
        private void VerifyDeployPermissions(int customerId, int userId)
        {
            Data.User user = new Data.User(userId);
            if (user.Id == 0)
            {
                // no user found
                throw new Exception("Permission denied.");
            }
            else
            {
                Data.Customer customer = user.Customer;
                if (customer == null || customer.Id == 0)
                {
                    // no customer found
                    throw new Exception("Permission denied.");
                }
                if (!CanDeployMultipleCourses(customer.SubDomain))
                {
                    // invalid user
                    throw new Exception("Permission denied.");
                }
            }
        }
        private void VerifyParameterPermissions(int customerId, int userId)
        {
            Data.User user = new Data.User(userId);
            if (user.Id == 0)
            {
                // no user found
                throw new Exception("Permission denied.");
            }
            else
            {
                Data.Customer customer = user.Customer;
                if (customer == null || customer.Id == 0)
                {
                    // no customer found
                    throw new Exception("Permission denied.");
                }

                if (!HasRole(Roles.ArtisanAuthor))
                {
                    throw new Exception("Permission denied.");
                }
            }
        }

        public SingleResult<Models.ArtisanDeploymentPackage> ApplyDeployment(int customerId, int userId, ArtisanDeploymentPackage deploymentPackage)
        {
            // first, we verify that this user has permission to do what they're attempting to do
            VerifyDeployPermissions(customerId, userId);

            if (deploymentPackage.CourseIds == null || deploymentPackage.CourseIds.Count == 0)
            {
                throw new Exception("No courses specified.");
            }
            if ((deploymentPackage.CustomerIds == null || deploymentPackage.CustomerIds.Count == 0) && (!deploymentPackage.IsAutoUpdate))
            {
                throw new Exception("No customers specified.");
            }
            if (String.IsNullOrEmpty(deploymentPackage.Name))
            {
                throw new Exception("No deployment name specified.");
            }
            if (deploymentPackage.ToArtisan == false && deploymentPackage.ToMars == false && deploymentPackage.ToSymphony == false)
            {
                throw new Exception("No deployment targets specified.");
            }

            // Saving a deployment - this will have the status as Queued
            // when the TaskRunner starts, it will run each of the queued deployments
            // Everything needed to run the deployment is saved in this deployment
            Data.ArtisanDeployment deployment = new Data.ArtisanDeployment();
            deployment.CourseIdsText = deploymentPackage.CourseIdsText;
            deployment.CustomerIdsText = deploymentPackage.CustomerIdsText;
            deployment.Name = deploymentPackage.Name;
            deployment.Notes = deploymentPackage.Notes;
            deployment.IsUpdate = deploymentPackage.IsUpdate;
            deployment.IsUpdateOnly = deploymentPackage.IsUpdateOnly;
            deployment.IsAutoUpdate = deploymentPackage.IsAutoUpdate;
            deployment.ToArtisan = deploymentPackage.ToArtisan;
            deployment.ToMars = deploymentPackage.ToMars;
            deployment.ToSymphony = deploymentPackage.ToSymphony;
            deployment.Running = false;
            deployment.Queued = true;
            deployment.PackageLocation = (new SymphonyController()).GetArtisanPackageLocation();
            deployment.ServerRoot = HttpContext.Current.Server.MapPath("~");
            deployment.Save(Username);

            deploymentPackage.Id = deployment.Id;

            StartDeploymentRunner();

            return new SingleResult<ArtisanDeploymentPackage>(deploymentPackage);
        }

        private void StartDeploymentRunner()
        {
            // Will run the deployment runner
            // The deployment runner will only do any work
            // if it is not already running.
            ProcessStartInfo processInfo;
            string processPath = ConfigurationManager.AppSettings["DeploymentRunnerExePath"];

            if (string.IsNullOrWhiteSpace(processPath))
            {
                Log.Warn("Deployment path not specified. Using default.");
                processPath = "~/runDeployments.bat";
            }

            processPath = processPath.StartsWith("~") ? HttpContext.Current.Server.MapPath(processPath) : processPath;

            processInfo = new ProcessStartInfo(processPath);
            processInfo.CreateNoWindow = true;
            processInfo.WindowStyle = ProcessWindowStyle.Hidden;
            processInfo.UseShellExecute = false;
            processInfo.WorkingDirectory = HttpContext.Current.Server.MapPath("~/");

            Process.Start(processInfo);
        }

        /// <summary>
        /// Runs a single deployment
        /// </summary>
        /// <param name="deployment"></param>
        private void RunDeployment(Data.ArtisanDeployment deployment)
        {
            // grab the output from the app settings; this lets us deploy
            string outputFolder = ConfigurationManager.AppSettings["DeployedCoursesFolder"];
            if (string.IsNullOrEmpty(outputFolder))
            {
                throw new Exception("Missing a 'DeployedCoursesFolder' setting.");
            }

            // create a few necessary items
            CourseAssignmentController controller = new CourseAssignmentController();
            List<ArtisanDeploymentInfo> info = new List<ArtisanDeploymentInfo>();

            ArtisanDeploymentPackage package = new ArtisanDeploymentPackage();
            package.CourseIdsText = deployment.CourseIdsText;
            package.CustomerIdsText = deployment.CustomerIdsText;

            List<int> courseIds = package.CourseIds;
            List<int> customerIds = package.CustomerIds;

            int currentCount = 0;
            int totalDeployments = 0;

            var customers = new List<Data.Customer>();

            try
            {
                // pre-load courses and customers
                var courses = Select.AllColumnsFrom<Data.ArtisanCourse>().Where(Data.ArtisanCourse.Columns.Id).In(courseIds).ExecuteTypedList<Data.ArtisanCourse>();

                if (deployment.IsAutoUpdate)
                {
                    customers = controller.GetCustomersWithArtisanCourses(courses);
                }
                else
                {
                    customers = Select.AllColumnsFrom<Data.Customer>().Where(Data.Customer.Columns.Id).In(customerIds).ExecuteTypedList<Data.Customer>();
                }

                Data.User user = new Data.User(deployment.CreatedByUserId);

                int platforms = 0;
                if (deployment.ToArtisan) platforms++;
                if (deployment.ToMars) platforms++;
                if (deployment.ToSymphony) platforms++;

                totalDeployments = courses.Count() * customers.Count() * platforms;

                if (deployment.ToArtisan == true)
                {
                    foreach (int courseId in courseIds)
                    {
                        string courseName = courses.Find(crs => crs.Id == courseId).Name;

                        foreach (int targetCustomerId in customerIds)
                        {
                            string customerName = customers.Find(cus => cus.Id == targetCustomerId).Name;
                            currentCount++;
                            try
                            {
                                SingleResult<ArtisanCourse> c = GetCourse(user.CustomerID, user.Id, courseId);

                                ArtisanCourse newCourse = c.Data;
                                newCourse.IsPublished = false;
                                newCourse.IsAutoPublish = false;
                                newCourse.ClearIds();

                                int newCourseId = -1; // -1 for a new course
                                SingleResult<ArtisanCourse> result = SaveCourse(targetCustomerId, user.Id, newCourseId, newCourse);

                                TransformQuestionPageReferences(targetCustomerId, user.Id, result.Data, user.CustomerID, courseId);

                                // record the results
                                info.Add(new ArtisanDeploymentInfo()
                                {
                                    ID = info.Count() + 1,
                                    Message = "Success - " + courseName + "(" + courseId + "), " + customerName + "(" + targetCustomerId + "), To Artisan",
                                    Result = true,
                                    Date = DateTime.Now,
                                    CourseID = courseId,
                                    CourseName = courseName,
                                    CustomerID = targetCustomerId,
                                    CustomerName = customerName,
                                    TotalCount = totalDeployments,
                                    CurrentCount = currentCount,
                                    Running = true,
                                    Queued = false
                                });
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex);
                                info.Add(new ArtisanDeploymentInfo()
                                {
                                    ID = info.Count() + 1,
                                    Message = "Failed - " + courseName + "(" + courseId + "), " + customerName + "(" + targetCustomerId + "), To Artisan - \"" + ex.Message + "\"",
                                    Result = false,
                                    Date = DateTime.Now,
                                    CourseID = courseId,
                                    CourseName = courseName,
                                    CustomerID = targetCustomerId,
                                    CustomerName = customerName,
                                    TotalCount = totalDeployments,
                                    CurrentCount = currentCount,
                                    Running = true,
                                    Queued = false
                                });
                            }

                            deployment.Info = Utilities.Serialize(info);
                            deployment.Save(Username);
                        }
                    }
                }

                if (deployment.ToMars == true)
                {
                    foreach (int courseId in courseIds)
                    {
                        string courseName = courses.Find(crs => crs.Id == courseId).Name;

                        foreach (int targetCustomerId in customerIds)
                        {
                            string customerName = customers.Find(cus => cus.Id == targetCustomerId).Name;
                            currentCount++;
                            try
                            {
                                // first, we package the course to the specified output folder
                                SingleResult<ArtisanCourse> c = GetCourse(user.CustomerID, user.Id, courseId);
                                Generators.PackageBuilder builder = new Symphony.Core.Generators.PackageBuilder(c.Data, outputFolder);
                                FileInfo fi = builder.Package(deployment.ServerRoot);

                                // record the results.
                                info.Add(new ArtisanDeploymentInfo()
                                {
                                    ID = info.Count() + 1,
                                    Message = "Success - " + courseName + "(" + courseId + "), " + customerName + "(" + targetCustomerId + "), To Mars",
                                    Result = true,
                                    Date = DateTime.Now,
                                    CourseID = courseId,
                                    CourseName = courseName,
                                    CustomerID = targetCustomerId,
                                    CustomerName = customerName,
                                    TotalCount = totalDeployments,
                                    CurrentCount = currentCount,
                                    Running = true,
                                    Queued = false
                                });
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex);
                                info.Add(new ArtisanDeploymentInfo()
                                {
                                    ID = info.Count() + 1,
                                    Message = "Failed - " + courseName + "(" + courseId + "), " + customerName + "(" + targetCustomerId + "), To Mars - \"" + ex.Message + "\"",
                                    Result = false,
                                    Date = DateTime.Now,
                                    CourseID = courseId,
                                    CourseName = courseName,
                                    CustomerID = targetCustomerId,
                                    CustomerName = customerName,
                                    TotalCount = totalDeployments,
                                    CurrentCount = currentCount,
                                    Running = true,
                                    Queued = false
                                });
                            }

                            deployment.Info = Utilities.Serialize(info);
                            deployment.Save(Username);
                        }
                    }
                }

                if (deployment.ToSymphony == true)
                {
                    // loop through courses
                    foreach (var course in courses)
                    {
                        // first, we package the course
                        SingleResult<ArtisanCourse> c = GetCourse(user.CustomerID, user.Id, course.Id);

                        Generators.PackageBuilder builder = null;
                        if (!string.IsNullOrWhiteSpace(deployment.PackageLocation))
                        {
                            builder = new Generators.PackageBuilder(c.Data, deployment.PackageLocation);
                        }
                        else
                        {
                            builder = new Generators.PackageBuilder(c.Data);
                        }
                        FileInfo fi = builder.Package(deployment.ServerRoot);

                        // get the path that we just used
                        var path = string.Empty;

                        // ok, now we package for each customer
                        foreach (var customer in customers)
                        {
                            currentCount++;
                            try
                            {
                                List<int> newCourseIds = new List<int>(); // new Symphony course ids

                                // determine if we allow updating
                                if (deployment.IsUpdate || deployment.IsUpdateOnly || deployment.IsAutoUpdate)
                                {
                                    // if so, locate the courses for this customer and artisan course id, if possible
                                    // (There could be more than one course using the same ArtisanID so make sure we update all of them)
                                    MultipleResult<Course> existing = controller.GetCourseByArtisanCourseId(customer.Id, c.Data.OriginalCourseId.Value);
                                    if (existing.Data.Count > 0)
                                    {
                                        // found it! use these course ids when we import
                                        newCourseIds = existing.Data.Select(co => co.Id).ToList<int>();
                                    }
                                    else
                                    {
                                        if (deployment.IsUpdateOnly || deployment.IsAutoUpdate)
                                        {
                                            info.Add(new ArtisanDeploymentInfo()
                                            {
                                                ID = info.Count() + 1,
                                                Message = "Skipped - " + c.Data.Name + "(" + c.Data.OriginalCourseId.Value + "), " + customer.Name + "(" + customer.Id + "), To Symphony - \"The customer does not have this course.\"",
                                                Result = false,
                                                Date = DateTime.Now,
                                                CourseID = c.Data.OriginalCourseId.Value,
                                                CourseName = c.Data.Name,
                                                CustomerID = customer.Id,
                                                CustomerName = customer.Name,
                                                TotalCount = totalDeployments,
                                                CurrentCount = currentCount,
                                                Running = true,
                                                Queued = false
                                            });
                                            continue; // no existing course to update and we don't want to add new
                                        }
                                    }
                                }

                                // now we import; if we found a match (and we have updating enabled), we'll update it, otherwise we'll create a new one
                                // the first time through, we build the course from the zip; from then on, use the folder
                                Course symphonyCourse = null;

                                // Add 0 if no course ids were found to update.
                                if (newCourseIds.Count == 0)
                                {
                                    newCourseIds.Add(0);
                                }

                                foreach (int newCourseId in newCourseIds)
                                {
                                    // TODO: Cleanup - we have code here that is very similar to the 
                                    // code in the function above PackageCourse. When we ma

                                    if (string.IsNullOrEmpty(path))
                                    {
                                        // import the course;
                                        // slightly modified folder so we can track these differently from the "non-shared" imports
                                        symphonyCourse = controller.ImportCourseFromZipFile(customer.Id, user.Id, newCourseId, fi.FullName, "Shared");
                                        path = symphonyCourse.ImportResult.ManifestFilePath;
                                    }
                                    else
                                    {
                                        symphonyCourse = controller.ImportCourseFromFolder(customer.Id, user.Id, newCourseId, path);
                                    }

                                    // store the ArtisanCourse IDs so we can use it on future deploys
                                    Data.OnlineCourse oc = Data.OnlineCourse.FetchByID(symphonyCourse.Id);
                                    oc.ArtisanCourseID = c.Data.OriginalCourseId.Value;
                                    oc.Keywords = c.Data.Keywords;
                                    oc.PublishedArtisanCourseID = c.Data.Id;
                                    oc.CertificateEnabled = c.Data.CertificateEnabled;
                                    oc.Retries = null;
                                    oc.Duration = c.Data.Duration;
                                    oc.AutomaticDuration = c.Data.AutomaticDuration;
                                    oc.IsUseAutomaticDuration = c.Data.IsUseAutomaticDuration;

                                    Category category = CategoryController.CopyCategory(c.Data.CustomerId, CategoryType.OnlineCourse, c.Data.CategoryId);

                                    oc.CategoryID = category != null ? category.Id : 0;
                                    oc.Save();

                                    AddSecondaryCategory(oc, c);
                                    AddAuthors(oc, c);

                                    AuditVersion(oc, c.Data, path);

                                    // Grabs the assignments from the course and saves them in the
                                    // AssignmentSummary table for easy lookup later. This avoids having
                                    // to look up assignments based on all of the artisan pages in the system
                                    AssignmentController assignmentController = new AssignmentController();
                                    assignmentController.SummarizeAssignments(oc.Id, oc.PublishedArtisanCourseID.Value);

                                    // record the results
                                    info.Add(new ArtisanDeploymentInfo()
                                    {
                                        ID = info.Count() + 1,
                                        Message = "Success - " + course.Name + "(" + course.Id + "), " + customer.Name + "(" + customer.Id + "), To Symphony",
                                        Result = true,
                                        Date = DateTime.Now,
                                        CourseID = course.Id,
                                        CourseName = course.Name,
                                        CustomerID = customer.Id,
                                        CustomerName = customer.Name,
                                        TotalCount = totalDeployments,
                                        CurrentCount = currentCount,
                                        Running = true,
                                        Queued = false
                                    });
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex);
                                info.Add(new ArtisanDeploymentInfo()
                                {
                                    ID = info.Count() + 1,
                                    Message = "Failed - " + course.Name + "(" + course.Id + "), " + customer.Name + "(" + customer.Id + "), To Symphony - \"" + ex.Message + "\"",
                                    Result = false,
                                    Date = DateTime.Now,
                                    CourseID = course.Id,
                                    CourseName = course.Name,
                                    CustomerID = customer.Id,
                                    CustomerName = customer.Name,
                                    TotalCount = totalDeployments,
                                    CurrentCount = currentCount,
                                    Running = true,
                                    Queued = false
                                });
                            }

                            deployment.Info = Utilities.Serialize(info);
                            deployment.Save(Username);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                Log.Error(e);
                try
                {
                    info.Add(new ArtisanDeploymentInfo()
                    {
                        ID = info.Count() + 1,
                        Message = "Error: " + e.Message,
                        Result = false,
                        Date = DateTime.Now,
                        CourseID = 0,
                        CourseName = "",
                        CustomerID = 0,
                        CustomerName = "",
                        TotalCount = totalDeployments,
                        CurrentCount = currentCount,
                        Running = false,
                        Queued = false
                    });
                    deployment.Info = Utilities.Serialize(info);
                    deployment.Save(Username);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }
            finally
            {
                try
                {
                    DateTime dtStart = DateTime.Now;
                    if (info.Count == 0)
                    {
                        dtStart = info[0].Date;
                    }
                    info.Add(new ArtisanDeploymentInfo()
                    {
                        ID = info.Count() + 1,
                        Message = "Finished (" + DateTime.Now.Subtract(dtStart).ToString(@"hh\:mm\:ss") + ")",
                        Result = true,
                        Date = DateTime.Now,
                        CourseID = 0,
                        CourseName = "",
                        CustomerID = 0,
                        CustomerName = "",
                        TotalCount = totalDeployments,
                        CurrentCount = currentCount,
                        Running = false,
                        Queued = false
                    });
                    deployment.Info = Utilities.Serialize(info);
                    package.CustomerIds = customers.Select(c => c.Id).ToList();
                    deployment.CustomerIdsText = package.CustomerIdsText;
                    deployment.Running = false;
                    deployment.Save(Username);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }
        }
        /// <summary>
        /// Will only ever return a task for the next queued deployment
        /// This means only one deployment will ever run at a time. 
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Task> StartNextQueuedDeployment()
        {
            Data.ArtisanDeployment queuedDeployment = Select.AllColumnsFrom<Data.ArtisanDeployment>()
                .Where(Data.ArtisanDeployment.QueuedColumn).IsEqualTo(true)
                .And(Data.ArtisanDeployment.RunningColumn).IsEqualTo(false)
                .OrderAsc(Data.ArtisanDeployment.CreatedOnColumn.ColumnName)
                .ExecuteSingle<Data.ArtisanDeployment>();

            List<Task> tasks = new List<Task>();

            if (queuedDeployment != null && queuedDeployment.Id > 0)
            {
                queuedDeployment.Running = true;
                queuedDeployment.Queued = false;
                queuedDeployment.Save();

                tasks.Add(Task.Factory.StartNew((deploy) =>
                {
                    RunDeployment((Data.ArtisanDeployment)deploy);
                }, queuedDeployment));
            }

            return tasks;
        }

        public void AuditVersion(Data.OnlineCourse onlineCourse, ArtisanCourse artisanCourse, string pathToPackage)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            AssemblyName name = new AssemblyName(assembly.FullName);
            Version version = name.Version;

            Data.ArtisanAudit audit = new Data.ArtisanAudit();
            audit.ArtisanCourseID = artisanCourse.Id;
            audit.OnlineCourseID = onlineCourse.Id;
            audit.OnlineCourseVersion = onlineCourse.Version;
            audit.PackageGuid = Utilities.ExtractGuid(pathToPackage);
            audit.CoreVersion = version.ToString();

            audit.Save(Username);
        }

        public MultipleResult<MediaPlayer> GetMediaPlayers()
        {
            PackageBuilder builder = new PackageBuilder();

            List<MediaPlayer> players = new List<MediaPlayer>();

            builder.GetMediaPlayerResources().Keys.ToList().ForEach(k =>
            {
                players.Add(new MediaPlayer
                {
                    Name = k
                });
            });

            return new MultipleResult<MediaPlayer>(players);
        }

        #region ArtisanCourseHook
        /// <summary>
        /// This is ported from ArtisanCourseHookHandler.ashx.cs
        /// TODO: Make ArtisanCourseHookHandler use this function instead
        /// This was added here since close to release we needed a bulk download feature
        /// rather than mess with the ArtisanCourseHook this was created here
        /// to avoid bugs. We should merge the two.
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="trainingProgramId"></param>
        /// <returns></returns>
        public string GetArtisanHookJS(int courseId, int trainingProgramId, Data.OnlineCourse courseOverrides = null, string parameterOverrideJson = null)
        {
            Data.OnlineCourse oc = new Data.OnlineCourse(courseId);
            Data.User user = new Data.User(UserID);
            Data.Customer customer = new Data.Customer(user.CustomerID);
            StringBuilder builder = new StringBuilder();
            HttpContext context = HttpContext.Current;

            if (oc.Id > 0)
            {
                // Validation settings
                bool isValidationEnabled = oc.IsValidationEnabled.HasValue ? oc.IsValidationEnabled.Value : false;
                int validationInterval = oc.ValidationInterval.HasValue ? oc.ValidationInterval.Value : 0;

                if (trainingProgramId > 0)
                {
                    Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);
                    validationInterval = tp.ValidationInterval.HasValue ? tp.ValidationInterval.Value : validationInterval;
                    isValidationEnabled = tp.IsValidationEnabled.HasValue ? tp.IsValidationEnabled.Value : isValidationEnabled;
                }

                if (isValidationEnabled && validationInterval <= 0)
                {
                    validationInterval = 300;
                }

                builder.AppendFormat(@"window.Artisan.IsValidationEnabled = {0}; 
                                                                window.Artisan.ValidationInterval = {1}; 
                                                                window.Artisan.ValidationURL = '/services/customer.svc/validate/{2}/{3}/{4}';",
                    isValidationEnabled.ToString().ToLower(), validationInterval, user.Id, courseId, trainingProgramId);

                builder.Append(@"
var maxAttempts = 3;
var attempts = maxAttempts;

var securityOptions = [{
    field: 'dob',
    msg: 'Please enter your date of birth: (MM/DD/YY)',
    validate: function (value) {
        var regex = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
        if (!value.match(regex)) {
            alert('Please enter a value in the format MM/DD/YY');
            return false;
        }
        return value;
    }
}, {
    field: 'ssn',
    msg: 'Please enter your social security number:',
    validate: function (value) {
        var value = value.replace(/[^0-9]/g, '');
        if (!value) {
            alert('Please enter a serries of 9 digits.');
            return false;
        }
        return value;
    }
}]

function validateTimeout() {
    attempts = maxAttempts;
    setTimeout(validateUser, window.Artisan.ValidationInterval * 1000);
}

function validateUser(optionOverride) {
    if (window.Artisan.IsValidationEnabled) {
        var option = typeof(optionOverride) != 'undefined' ? optionOverride : securityOptions[Math.floor(Math.random() * 2)];
        var result = prompt(option.msg);
        var data = {};

        if (result) {
            data[option.field] = option.validate(result);
        } else {
            $.ajax({
                url: window.Artisan.ValidationURL + '/' + (maxAttempts - attempts),
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'json',
                method: 'POST'
            });
            alert('You could not be validated, this session has ended.');
            top.close();
            return;
        }

        if (!data[option.field]) {
            validateUser(option);
            return;
        }

        attempts--;

        $.ajax({
            url: window.Artisan.ValidationURL + '/' + (maxAttempts - attempts),
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            method: 'POST',
            success: function (data) {
                if (data.success) {
                    validateTimeout();
                } else {
                    if (attempts > 0) {
                        alert('You did not enter the correct value. You have ' + attempts + ' more ' + (attempts > 1 ? 'tries' : 'try') + ' remaining.');
                        validateUser(option);
                    } else {
                        alert('You could not be validated. This session has ended.');
                        top.close();
                    }
                }
            },
            error: function() {
                alert('You could not be validated. This session has ended.');
                top.close();
            }
        });
    }
}

validateTimeout();

");
                Data.ArtisanCourse ac = new Data.ArtisanCourse(oc.ArtisanCourseID);
                if (oc.ArtisanCourseID > 0 && ac.Id > 0)
                {



                    string courseOverrideJSON;
                    // If parameters have been supplied (Bulk download), use those instead, otherwise look these up for the course
                    if (!string.IsNullOrEmpty(parameterOverrideJson))
                    {
                        courseOverrideJSON = parameterOverrideJson;
                    }
                    else
                    {
                        List<string> parameterCodes = Symphony.Core.Utilities.Deserialize<List<string>>(ac.Parameters);


                        List<Symphony.Core.Models.OnlineCourseParameterOverride> onlineCourseOverrides = Select.AllColumnsFrom<Data.OnlineCourseOverrideDetail>()
                                    .Where(Data.OnlineCourseOverrideDetail.Columns.OnlineCourseID).IsEqualTo(oc.Id)
                                    .And(Data.OnlineCourseOverrideDetail.Columns.Code).In(parameterCodes.ToArray())
                                    .ExecuteTypedList<Symphony.Core.Models.OnlineCourseParameterOverride>();

                        courseOverrideJSON = Symphony.Core.Utilities.Serialize(onlineCourseOverrides);

                        if (trainingProgramId > 0)
                        {
                            List<Symphony.Core.Models.TrainingProgramParameterOverride> trainingProgramOverrides = Select.AllColumnsFrom<Data.TrainingProgramParameterOverrideDetail>()
                                    .Where(Data.TrainingProgramParameterOverrideDetail.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                                    .And(Data.TrainingProgramParameterOverrideDetail.Columns.Code).In(parameterCodes.ToArray())
                                    .ExecuteTypedList<Symphony.Core.Models.TrainingProgramParameterOverride>();

                            string trainingProgramOverrideJSON = Symphony.Core.Utilities.Serialize(trainingProgramOverrides);
                            builder.AppendFormat("window.Artisan.TrainingProgramParameterOverrides = {0};", trainingProgramOverrideJSON);
                        }
                    }
                    builder.AppendFormat("window.Artisan.OnlineCourseParameterOverrides = {0};", courseOverrideJSON);



                    builder.Append(@"
function overrideArtisanCourseParameters(overrides) {
    var parameterOverrideMap = {};

    for (var i = 0; i < overrides.length; i++) {
        parameterOverrideMap[overrides[i].code.replace(/[{!}]/g, '')] = overrides[i];
    }

    var sectionJson = JSON.stringify(window.Artisan.CourseJson.sections);
    var regex = /(<span.+?artisan-parameter.+?{!)([a-z0-9_]+)(.+?>)(.*?)(<\/span>)/g;
    
    sectionJson = sectionJson.replace(regex, function(match, startSpan, code, endSpan, current, closeSpan) {
        var value = parameterOverrideMap[code] ? parameterOverrideMap[code].value : current;

        var replacement = startSpan + code + endSpan + value + closeSpan;
        
        replacement = JSON.stringify(replacement);
        replacement = replacement.substring(1, replacement.length - 1);

        return replacement;
    });

    window.Artisan.CourseJson.sections = JSON.parse(sectionJson);
}

var needsInit = false;

if (window.Artisan.OnlineCourseParameterOverrides && window.Artisan.OnlineCourseParameterOverrides.length) {
    overrideArtisanCourseParameters(window.Artisan.OnlineCourseParameterOverrides);
    needsInit = true;
}

if (window.Artisan.TrainingProgramParameterOverrides && window.Artisan.TrainingProgramParameterOverrides.length) {
    overrideArtisanCourseParameters(window.Artisan.TrainingProgramParameterOverrides);
    needsInit = true;
}

if (needsInit) {
    Artisan.App.init()
}");
                }

                string overridePath = context.Server.MapPath("~/CourseOverrides/" + oc.Id + ".js");
                if (System.IO.File.Exists(overridePath))
                {
                    builder.Append(overridePath);
                }
                else
                {
                    Data.OnlineCourseRollup rollup = Select.AllColumnsFrom<Data.OnlineCourseRollup>()
                       .Where(Data.OnlineCourseRollup.CourseIDColumn).IsEqualTo(courseId)
                       .And(Data.OnlineCourseRollup.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                       .And(Data.OnlineCourseRollup.UserIDColumn).IsEqualTo(user.Id)
                       .ExecuteSingle<Data.OnlineCourseRollup>();

                    Data.TrainingProgram tp;
                    if (trainingProgramId > 0)
                    {
                        tp = new Data.TrainingProgram(trainingProgramId);
                    }
                    else
                    {
                        tp = new Data.TrainingProgram();
                    }

                    Data.OnlineCourse overrideOc = oc;
                    if (courseOverrides != null)
                    {
                        // Course overrides have been passed in. This is for bulk course downloads
                        // keeps the overrides consistant for all courses downloaded
                        // swap out the original course with the passed in course to grab the parameters
                        overrideOc = courseOverrides;
                    }


                    // Use customer level overrides if there aren't any at the course level
                    // overrideOc will either point to oc, or courseOverrides (in the case of a bulk download request)
                    oc.ShowPretestOverride = tp.ShowPretestOverride ?? overrideOc.ShowPretestOverride ?? customer.ShowPretestOverride;
                    oc.ShowPostTestOverride = tp.ShowPostTestOverride ?? overrideOc.ShowPostTestOverride ?? customer.ShowPostTestOverride;
                    oc.PassingScoreOverride = tp.PassingScoreOverride ?? overrideOc.PassingScoreOverride ?? customer.PassingScoreOverride;
                    oc.SkinOverride = tp.SkinOverride ?? overrideOc.SkinOverride ?? customer.SkinOverride;
                    oc.PreTestTypeOverride = tp.PreTestTypeOverride ?? overrideOc.PreTestTypeOverride ?? customer.PreTestTypeOverride;
                    oc.PostTestTypeOverride = tp.PostTestTypeOverride ?? overrideOc.PostTestTypeOverride ?? customer.PostTestTypeOverride;
                    oc.NavigationMethodOverride = tp.NavigationMethodOverride ?? overrideOc.NavigationMethodOverride ?? customer.NavigationMethodOverride;
                    oc.CertificateEnabledOverride = tp.CertificateEnabledOverride ?? overrideOc.CertificateEnabledOverride ?? customer.CertificateEnabledOverride;
                    oc.DisclaimerOverride = tp.DisclaimerOverride ?? overrideOc.DisclaimerOverride ?? customer.DisclaimerOverride;
                    oc.ContactEmailOverride = tp.ContactEmailOverride ?? overrideOc.ContactEmailOverride ?? customer.ContactEmailOverride;
                    oc.ShowObjectivesOverride = tp.ShowObjectivesOverride ?? overrideOc.ShowObjectivesOverride ?? customer.ShowObjectivesOverride;
                    oc.PretestTestOutOverride = tp.PretestTestOutOverride ?? overrideOc.PretestTestOutOverride ?? customer.PretestTestOutOverride;

                    oc.RandomizeAnswersOverride = tp.RandomizeAnswersOverride ?? overrideOc.RandomizeAnswersOverride ?? customer.RandomizeAnswersOverride;
                    oc.RandomizeQuestionsOverride = tp.RandomizeQuestionsOverride ?? overrideOc.RandomizeQuestionsOverride ?? customer.RandomizeQuestionsOverride;
                    oc.ReviewMethodOverride = tp.ReviewMethodOverride ?? overrideOc.ReviewMethodOverride ?? customer.ReviewMethodOverride;
                    oc.MarkingMethodOverride = tp.MarkingMethodOverride ?? overrideOc.MarkingMethodOverride ?? customer.MarkingMethodOverride;
                    oc.InactivityMethodOverride = tp.InactivityMethodOverride ?? overrideOc.InactivityMethodOverride ?? customer.InactivityMethodOverride;
                    oc.InactivityTimeoutOverride = tp.InactivityTimeoutOverride ?? overrideOc.InactivityTimeoutOverride ?? customer.InactivityTimeoutOverride;
                    oc.MinimumPageTimeOverride = tp.MinimumPageTimeOverride ?? overrideOc.MinimumPageTimeOverride ?? customer.MinimumPageTimeOverride;
                    oc.MaximumQuestionTimeOverride = tp.MaximumQuestionTimeOverride ?? overrideOc.MaximumQuestionTimeOverride ?? customer.MinimumPageTimeOverride;
                    oc.MaxTimeOverride = tp.MaxTimeOverride ?? overrideOc.MaxTimeOverride ?? customer.MaxTimeOverride;

                    oc.MinTimeOverride = tp.MinTimeOverride ?? overrideOc.MinTimeOverride ?? customer.MinTimeOverride;
                    oc.MinLoTimeOverride = tp.MinLoTimeOverride ?? overrideOc.MinLoTimeOverride ?? customer.MinLoTimeOverride;
                    oc.MinScoTimeOverride = tp.MinScoTimeOverride ?? overrideOc.MinScoTimeOverride ?? customer.MinScoTimeOverride;

                    //oc.RetestMode = oc.RetestMode ?? customer.RetestMode;
                    //oc.Retries = oc.Retries ?? customer.Retries;

                    int retries = tp.Retries ?? overrideOc.Retries ?? customer.Retries ?? int.MaxValue - 1; //.onlin== null ? (customer.OnlineCourseRetries == null ? int.MaxValue : customer.OnlineCourseRetries.Value) : oc.Retries.Value;

                    // there was a bug in old artisan courses; rather than having to re-deploy all courses, 
                    // it's fixed in the course for future releases, but also back-ported here
                    bool applyMonkeyPatch = false;

                    string monkeyPatch = @"
// monkey patch
Artisan.App.removeSpecial = function (type) {
    var json = Artisan.CourseJson;
    for (var i = 0; i < json.sections.length; i++) {
        if (json.sections[i].sectionType == type) {
            json.sections.splice(i, 1);
        }
    }
}; 
Artisan.App.hasPosttest = function () {
    var json = Artisan.CourseJson;
    for (var i = 0; i < json.sections.length; i++) {
        if (json.sections[i].sectionType == ArtisanSectionType.Posttest) {
            return true;
        }
    }
    return false;
};
";
                    bool attemptLimitReached = false;
                    if (rollup != null && rollup.Id > 0 && rollup.TestAttemptCount >= retries + 1)
                    {
                        attemptLimitReached = true;
                    }


                    string prefix = "Artisan.App";
                    if (oc.PretestTestOutOverride.HasValue)
                    {
                        builder.AppendFormat("{0}.setPretestTestOut({1});", prefix, oc.PretestTestOutOverride.Value.ToString().ToLower());
                    }

                    if (oc.PassingScoreOverride.HasValue)
                    {
                        builder.AppendFormat("{0}.setPassingScore({1});", prefix, oc.PassingScoreOverride.Value);
                    }
                    if (oc.NavigationMethodOverride.HasValue)
                    {
                        builder.AppendFormat("{0}.setNavigationMethod({1});", prefix, oc.NavigationMethodOverride.Value);
                    }
                    if (oc.PreTestTypeOverride.HasValue)
                    {
                        builder.AppendFormat("{0}.setPretestType({1});", prefix, oc.PreTestTypeOverride.Value);
                    }
                    if (oc.PostTestTypeOverride.HasValue)
                    {
                        builder.AppendFormat("{0}.setPosttestType({1});", prefix, oc.PostTestTypeOverride.Value);
                    }
                    if ((oc.ShowPostTestOverride.HasValue && oc.ShowPostTestOverride == false) || attemptLimitReached)
                    {
                        applyMonkeyPatch = true;
                        if (attemptLimitReached)
                        {
                            builder.Append("if(Artisan.App.hasPosttest && Artisan.App.hasPosttest()){");
                            builder.Append("window.setTimeout(function(){ alert('You have reached the test attempt limit and the post test has been removed; please contact your manager if you wish to re take the test for this course.'); }, 3000);");
                            builder.Append("}");
                        }
                        builder.AppendFormat("{0}.removePosttest();", prefix);
                    }
                    if (oc.ShowObjectivesOverride.HasValue && oc.ShowObjectivesOverride == false)
                    {
                        applyMonkeyPatch = true;
                        builder.AppendFormat("{0}.removeObjectives();", prefix);
                    }
                    if (oc.ShowPretestOverride.HasValue && oc.ShowPretestOverride == false)
                    {
                        builder.AppendFormat("{0}.removePretest();", prefix);
                    }
                    if (oc.SkinOverride.HasValue)
                    {
                        // example value:
                        // 'http://localhost:63997/Handlers/ArtisanSkinHandler.ashx?href=http://localhost:63997/skins/artisan_content/default/skins/be_smooth/skin.css'
                        string port = (context.Request.Url.Port == 80 || context.Request.Url.Port == 443 ? "" : ":" + context.Request.Url.Port);
                        string baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Host + port;
                        string handlerUrl = baseUrl + "/Handlers/ArtisanSkinHandler.ashx?href=";

                        Data.ArtisanTheme theme = new Data.ArtisanTheme(oc.SkinOverride.Value);
                        string cssUrl = baseUrl + theme.Folder + "/skins/" + theme.SkinName + "/skin.css";
                        builder.AppendFormat("{0}.setSkin('{1}');", prefix, handlerUrl + cssUrl);
                    }

                    string setTestParameterStartLoop = @"
                                var json = Artisan.CourseJson;
                                for (var i = 0; i < json.sections.length; i++) {
                                    if (json.sections[i].sectionType == ArtisanSectionType.Posttest || json.sections[i].sectionType == ArtisanSectionType.PreTest) {
                            ";
                    string setTestParameter = "json.sections[i].{0} = {1};";
                    string setTestParameterEndLoop = @"
                                    }
                                }
                            ";

                    string setCourseJsonParameter = "Artisan.CourseJson.{0} = {1};";

                    if (oc.RandomizeAnswersOverride.HasValue)
                    {
                        builder.Append(setTestParameterStartLoop);
                        builder.AppendFormat(setTestParameter, "isRandomizeAnswers", oc.RandomizeAnswersOverride.Value.ToString().ToLower());
                        builder.Append(setTestParameterEndLoop);
                    }
                    if (oc.RandomizeQuestionsOverride.HasValue)
                    {
                        builder.Append(setTestParameterStartLoop);
                        builder.AppendFormat(setTestParameter, "isReRandomizeOrder", oc.RandomizeQuestionsOverride.Value.ToString().ToLower());
                        builder.Append(setTestParameterEndLoop);
                    }
                    if (oc.ReviewMethodOverride.HasValue)
                    {
                        builder.AppendFormat(setCourseJsonParameter, "reviewMethod", oc.ReviewMethodOverride.Value);
                    }
                    if (oc.MarkingMethodOverride.HasValue)
                    {
                        builder.AppendFormat(setCourseJsonParameter, "markingMethod", oc.MarkingMethodOverride.Value);
                    }
                    if (oc.InactivityMethodOverride.HasValue)
                    {
                        builder.AppendFormat(setCourseJsonParameter, "timeoutMethod", oc.InactivityMethodOverride.Value);
                    }
                    if (oc.InactivityTimeoutOverride.HasValue)
                    {
                        builder.AppendFormat(setCourseJsonParameter, "inactivityTimeout", oc.InactivityTimeoutOverride.Value);
                    }
                    if (oc.MinimumPageTimeOverride.HasValue)
                    {
                        builder.AppendFormat(setCourseJsonParameter, "minimumPageTime", oc.MinimumPageTimeOverride.Value);
                    }
                    if (oc.MaximumQuestionTimeOverride.HasValue)
                    {
                        builder.AppendFormat(setCourseJsonParameter, "maximumQuestionTime", oc.MaximumQuestionTimeOverride.Value);
                    }
                    if (oc.MaxTimeOverride.HasValue)
                    {
                        builder.AppendFormat(setCourseJsonParameter, "maxTime", oc.MaxTimeOverride.Value);
                    }
                    if (oc.MinTimeOverride.HasValue)
                    {
                        builder.AppendFormat(setCourseJsonParameter, "minTime", oc.MinTimeOverride.Value);
                    }
                    if (oc.MinLoTimeOverride.HasValue)
                    {
                        builder.AppendFormat(setCourseJsonParameter, "minLoTime", oc.MinLoTimeOverride.Value);
                    }
                    if (oc.MinScoTimeOverride.HasValue)
                    {
                        builder.AppendFormat(setCourseJsonParameter, "minScoTime", oc.MinScoTimeOverride.Value);
                    }
                    if (oc.CertificateEnabledOverride.HasValue)
                    {
                        builder.AppendFormat(setCourseJsonParameter, "certificateEnabled", oc.CertificateEnabledOverride.Value.ToString().ToLower());
                    }

                    var url = context.Request.Url;
                    string host = url.AbsoluteUri.Replace(url.AbsolutePath, "");

                    if (!string.IsNullOrEmpty(url.Query))
                    {
                        host = host.Replace(url.Query, "");
                    }

                    string certificateUrl = host + "/Handlers/CertificateHandler.ashx?certificateTypeId=" + CertificateType.Online + "&trainingProgramId={!training_program_id}&courseOrClassId={!course_id}&userId={!user_id}&score={!score}";

                    builder.Append("Artisan.CourseJson.useSymphonyCertificate = true;");
                    builder.Append("Artisan.CourseJson.certificateUrl = '" + certificateUrl + "';");

                    if (applyMonkeyPatch)
                    {
                        builder.Append(monkeyPatch);
                    }

                }

            }

            if (builder.Length > 0)
            {
                builder.Append("Artisan.App.init();");
            }

            return builder.ToString();
        }

        #endregion


        #region Parameters
        public PagedResult<Parameter> GetParameters(int customerId, int userId, string codes, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            List<string> columns = Data.Parameter.Schema.Columns.Select(c => c.ColumnName).ToList();

            columns.Add(string.Format("{0} as '{1}{2}'",
                Data.ParameterSet.NameColumn.QualifiedName,
                Data.ParameterSet.Schema.TableName,
                Data.ParameterSet.NameColumn));

            SqlQuery query = new Select(columns.ToArray())
                .From<Data.Parameter>()
                .InnerJoin(Data.ParameterSet.IdColumn, Data.Parameter.ParameterSetIDColumn)
                .ApplyCustomerNetwork(Data.Parameter.CustomerIDColumn, customerId, false, true)
                .And(Data.Parameter.NameColumn.QualifiedName).ContainsString(searchText);
            //.And(Data.Parameter.CustomerIDColumn).IsEqualTo(customerId);

            if (codes != null)
            {
                string[] codeArray = codes.Split('|');
                if (codes.Length > 0)
                {
                    query.And(Data.Parameter.CodeColumn).In(codeArray);
                }
            }

            return new PagedResult<Parameter>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ParameterSet> GetParameterSets(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ParameterSet>()
                .Where(Data.ParameterSet.NameColumn).ContainsString(searchText)
                .And(Data.ParameterSet.CustomerIDColumn).IsEqualTo(customerId);

            return new PagedResult<ParameterSet>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<Parameter> GetParameter(int parameterId)
        {
            Data.Parameter parameterData = new Data.Parameter(parameterId);
            Parameter parameter = new Parameter();
            parameter.CopyFrom(parameterData);

            if (parameterData.CustomerID != CustomerID)
            {
                throw new Exception("Parameter not found");
            }

            return new SingleResult<Parameter>(parameter);
        }


        public SingleResult<ParameterSet> GetParameterSet(int parameterSetId)
        {
            Data.ParameterSet parameterSetData = new Data.ParameterSet(parameterSetId);
            ParameterSet parameterSet = new ParameterSet();
            parameterSet.CopyFrom(parameterSetData);

            parameterSet.Options = GetParameterSetOptions(parameterSetId, "", "", OrderDirection.Ascending, 0, int.MaxValue).Data.ToList();

            if (parameterSetData.CustomerID != CustomerID)
            {
                throw new Exception("Parameter not found");
            }

            return new SingleResult<ParameterSet>(parameterSet);
        }

        public PagedResult<ParameterSetOption> GetParameterSetOptions(int parameterSetId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ParameterSetOption>()
                    .InnerJoin(Data.ParameterSet.IdColumn, Data.ParameterSetOption.ParameterSetIDColumn)
                    .Where(Data.ParameterSetOption.ParameterSetIDColumn).IsEqualTo(parameterSetId)
                    .And(Data.ParameterSetOption.ValueXColumn).ContainsString(searchText)
                    .And(Data.ParameterSet.CustomerIDColumn).IsEqualTo(CustomerID);

            return new PagedResult<ParameterSetOption>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ParameterSetOption> GetParameterSetOptions(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.ParameterSetOption>()
                    .InnerJoin(Data.ParameterSet.IdColumn, Data.ParameterSetOption.ParameterSetIDColumn)
                    .Where(Data.ParameterSetOption.ValueXColumn).ContainsString(searchText)
                    .And(Data.ParameterSet.CustomerIDColumn).IsEqualTo(CustomerID);

            return new PagedResult<ParameterSetOption>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        private SqlQuery GetParameterValuesQuery(string searchText)
        {
            List<string> columns = Data.ParameterValue.Schema.Columns.Select(c => c.ColumnName).ToList();

            columns.Add(string.Format("{0} as '{1}{2}'",
                Data.ParameterSet.NameColumn.QualifiedName,
                Data.ParameterSet.Schema.TableName,
                Data.ParameterSet.NameColumn));

            columns.Add(string.Format("{0} as '{1}{2}'",
                Data.ParameterSetOption.ValueXColumn.QualifiedName,
                Data.ParameterSetOption.Schema.TableName,
                Data.ParameterSetOption.ValueXColumn));

            SqlQuery query = new Select(columns.ToArray())
                .From<Data.ParameterValue>()
                .InnerJoin(Data.ParameterSetOption.IdColumn, Data.ParameterValue.ParameterSetOptionIDColumn)
                .InnerJoin(Data.ParameterSet.IdColumn, Data.ParameterSetOption.ParameterSetIDColumn)
                .InnerJoin(Data.Parameter.IdColumn, Data.ParameterValue.ParameterIDColumn)
                .Where(Data.ParameterValue.ValueXColumn).ContainsString(searchText)
                .And(Data.Parameter.CustomerIDColumn).IsEqualTo(CustomerID);

            return query;
        }

        public PagedResult<ParameterValue> GetParameterValues(int parameterId, int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = GetParameterValuesQuery(searchText)
                .And(Data.ParameterValue.ParameterIDColumn).IsEqualTo(parameterId);

            return new PagedResult<ParameterValue>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public PagedResult<ParameterValue> GetParameterValuesForOption(int parameterSetOptionId, int customerId, int userId, string codes, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = GetParameterValuesQuery(searchText)
                .And(Data.ParameterValue.ParameterSetOptionIDColumn).IsEqualTo(parameterSetOptionId);

            if (codes != null)
            {
                string[] codeArray = codes.Split('|');
                if (codes.Length > 0)
                {
                    query.And(Data.Parameter.CodeColumn).In(codeArray);
                }
            }

            return new PagedResult<ParameterValue>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<Parameter> SaveParameter(int parameterId, Parameter parameter)
        {
            VerifyParameterPermissions(CustomerID, UserID);

            Data.Parameter parameterData = new Data.Parameter(parameterId);
            parameter.CopyTo(parameterData);
            parameterData.Id = parameterId;
            parameterData.CustomerID = CustomerID;

            parameterData.Save(UserID);

            if (parameterId != 0)
            {
                // Remove any values from this parameter that are not part of the values passed in
                // AMTODO make this a soft delete
                new Delete().From<Data.ParameterValue>()
                       .Where(Data.ParameterValue.ParameterIDColumn)
                       .IsEqualTo(parameterId)
                       .And(Data.ParameterValue.IdColumn).NotIn(parameter.Values.Select(v => v.ID).ToArray<int>())
                       .Execute();
            }
            else
            {
                parameterId = parameterData.Id;
            }

            // Save/update the values
            Data.ParameterValueCollection valueCollection = new Data.ParameterValueCollection();
            foreach (ParameterValue value in parameter.Values)
            {
                Data.ParameterValue v = new Data.ParameterValue(value.ID);
                v.ParameterID = parameterId;
                v.ParameterSetOptionID = value.ParameterSetOptionId;
                v.ValueX = value.Value;
                valueCollection.Add(v);
            }

            valueCollection.SaveAll(UserID);

            return GetParameter(parameterData.Id);

        }

        public SingleResult<ParameterSet> SaveParameterSet(int parameterSetId, ParameterSet parameterSet)
        {

            VerifyParameterPermissions(CustomerID, UserID);

            Data.ParameterSet parameterSetData = new Data.ParameterSet(parameterSetId);
            parameterSet.CopyTo(parameterSetData);
            parameterSetData.Id = parameterSetId;
            parameterSetData.CustomerID = CustomerID;

            parameterSetData.Save(UserID);

            if (parameterSetId != 0)
            {
                // Remove any options from this set that are not part of the options passed in
                // AMTODO make this a soft delete
                new Delete().From<Data.ParameterSetOption>()
                       .Where(Data.ParameterSetOption.ParameterSetIDColumn)
                       .IsEqualTo(parameterSetId)
                       .And(Data.ParameterSetOption.IdColumn).NotIn(parameterSet.Options.Select(o => o.ID).ToArray<int>())
                       .Execute();
            }
            else
            {
                parameterSetId = parameterSetData.Id;
            }

            // Save/update the options
            Data.ParameterSetOptionCollection optionCollection = new Data.ParameterSetOptionCollection();

            foreach (ParameterSetOption option in parameterSet.Options)
            {
                Data.ParameterSetOption o = new Data.ParameterSetOption(option.ID);
                o.ValueX = option.Value;
                o.ParameterSetID = parameterSetId;
                optionCollection.Add(o);
            }

            optionCollection.SaveAll(UserID);

            return GetParameterSet(parameterSetData.Id);
        }

        #endregion

        #region courseImport


        private ArtisanSection AddSection(string courseCode, string chapterTitle)
        {
            return new ArtisanSection()
            {
                Name = chapterTitle,
                SectionType = (int)ArtisanSectionType.Sco,
                ExternalID = courseCode,
                Sections = new List<ArtisanSection>()
                {
                    new ArtisanSection()
                    {
                        Name = chapterTitle,
                        SectionType = (int)ArtisanSectionType.LearningObject,
                        ExternalID = courseCode
                    }
                }
            };
        }
        /*
        private List<ArtisanPage> AddPage(ArtisanCourse course, string courseCode, string chapterTitle, string filePath, string sectionTitle, Data.ArtisanImportJob batchJob)
        {
            
            MemoryStream memStream = new MemoryStream();
            using (FileStream fs = File.OpenRead(filePath))
            {
                fs.CopyTo(memStream);
            }

            List<ArtisanPage> pages = new List<ArtisanPage>();
        
            string extension = Path.GetExtension(filePath);
            bool valid = true;
            bool isSwf = false;
            switch (extension)
            {
                case ".ppt":
                case ".pptx":
                case ".doc":
                case ".docx":
                case ".pdf":
                    valid = true;
                    break;
                case ".swf":
                    isSwf = true;
                    valid = true;
                    break;
                default:
                    valid = false;
                    break;
            }

            if (valid && !isSwf)
            {
               
                 pages =
                    ImportDocument(CustomerID, Path.GetFileName(filePath), null, course.Id,
                        course.Sections.Last().Sections.Last().Id, null, null, "noscale", null, null, courseCode, memStream, "d", "d").Data;


            } else if (valid && isSwf)
            {

                byte[] fileData = null;
                using (var binaryReader = new BinaryReader(memStream))
                {
                    fileData = binaryReader.ReadBytes((int)memStream.Length);
                }

                ArtisanAsset asset = (new ArtisanController()).UploadArtisanAsset(CustomerID,
                    Path.GetFileName(filePath), Path.GetFileName(filePath), "", fileData).Data;

                AugmentAsset(asset, new Data.ArtisanAssetType(asset.AssetTypeId));

                string html = asset.Template.Replace("{path}", asset.Path)
                                            .Replace("{element_id}", "" + asset.Id)
                                            .Replace("height=\"100%\"", "height=\"500\"");

                ArtisanPage page = new ArtisanPage();
                page.Html = string.Format(WrapperMarkup, html);
                page.PageType = (int)ArtisanPageType.Content;
                page.Name = sectionTitle;
                page.CourseId = course.Id;
                page.ExternalID = course.ExternalID;

                pages.Add(page);

            }
            batchJob.PagesProcessed += pages.Count;
            return pages;

        }*/

        public SingleResult<ArtisanImportJob> GetArtisanImportJob(int id)
        {
            return new SingleResult<ArtisanImportJob>(Select.AllColumnsFrom<Data.ArtisanImportJob>().Where(Data.ArtisanImportJob.IdColumn).IsEqualTo(id));
        }

        private void DownloadContent(List<Resources.ProSchoolImportLine> lines, int batchId, string directory, ILog log, HttpContext ctx)
        {
            try
            {
                var _lock = new Object();

                Parallel.ForEach<Resources.ProSchoolImportLine>(lines, line =>
                {
                    HttpContext.Current = ctx;
                    if (!line.DownloadCompleted)
                    {
                        try
                        {
                            string extension = Path.GetExtension(line.ContentPath);

                            if (!line.ContentPath.Contains("http://"))
                            {
                                var baseUri = new Uri("http://lms.proschools.com/");
                                line.ContentPath = new Uri(baseUri, line.ContentPath).ToString();
                            }

                            line.ContentPath = line.ContentPath.Replace(" ", "%20");

                            string fileHash = "";
                            string path = "";

                            using (MD5 hash = MD5.Create())
                            {
                                fileHash = CalculateMD5Hash(hash, line.ContentPath);
                            }
                            path = Path.Combine(directory, fileHash + extension);
                            line.FilePath = path;

                            try
                            {
                                if (File.Exists(path))
                                {
                                    //log.Info("File already downloaded: " + line.ContentPath);
                                    line.DownloadCompleted = true;
                                    return;
                                }
                            }
                            catch (Exception e)
                            {
                                line.DownloadCompleted = true;
                                log.Info("Error checking if file exists. Assuming it does.");
                                return;
                            }

                            switch (extension)
                            {
                                case ".html":
                                case ".htm":
                                    //Don't do any downloading with these guys, only processing required

                                    break;
                                case ".ppt":
                                case ".pptx":
                                case ".doc":
                                case ".docx":
                                case ".pdf":
                                case ".swf":
                                    try
                                    {
                                        using (WebClient webClient = new WebClient())
                                        {
                                            webClient.DownloadFile(new Uri(line.ContentPath), path);
                                            line.DownloadCompleted = true;
                                        }
                                        /*var req = (HttpWebRequest)WebRequest.Create(line.ContentPath);
                                        using (var res = (HttpWebResponse)req.GetResponse())
                                        using (var resStream = res.GetResponseStream())
                                        using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
                                        {
                                            var buffer = new byte[8 * 1024];
                                            int len;
                                            while ((len = resStream.Read(buffer, 0, buffer.Length)) > 0)
                                            {
                                                fs.Write(buffer, 0, buffer.Length);
                                            }
                                        }*/
                                    }
                                    catch (Exception e)
                                    {
                                        line.DownloadCompleted = false;

                                        if (e.Message.Contains("404"))
                                        {
                                            line.FileNotFound = true;
                                            throw new Exception("File not found: " + line.ContentPath);
                                        }
                                        else if (e.Message.Contains("used by another process"))
                                        {
                                            if (line.IsDuplicate)
                                            {
                                                log.Info("[AlreadyDownloaded] File is duplicate - downloaded already");
                                            }
                                            else
                                            {
                                                log.Info("[PossibleDownload] Assuming downloaded. File in use.");
                                            }
                                            // Assume downloaded
                                            line.DownloadCompleted = true;
                                        }
                                        else
                                        {
                                            if (e is WebException)
                                            {
                                                using (var sr = new StreamReader(((WebException)e).Response.GetResponseStream()))
                                                {
                                                    var message = sr.ReadToEnd();
                                                    throw new Exception(message + " File could not be downloaded.");
                                                }
                                            }
                                            throw new Exception(e.Message + " Could not download file");
                                        }
                                    }


                                    /*
                                    using (WebClient webClient = new WebClient())
                                    {
                                        try
                                        {
                                            webClient.DownloadFile(new Uri(line.ContentPath), path);
                                            line.DownloadCompleted = true;
                                        }
                                        catch (Exception e)
                                        {
                                            line.DownloadCompleted = false;
                                            throw new Exception(e.Message + " File could not be downloaded.");
                                        }
                                    }*/

                                    break;
                                /*case ".swf":

                                    using (WebClient webClient = new WebClient())
                                    {
                                        try
                                        {
                                            webClient.DownloadFile(new Uri(line.ContentPath), path);
                                            line.DownloadCompleted = true;
                                        }
                                        catch (Exception e)
                                        {
                                            line.DownloadCompleted = false;
                                            throw new Exception(e.Message + " Could not download SWF");
                                        }
                                    }
                                    break;*/
                            }
                        }
                        catch (Exception e)
                        {
                            lock (_lock)
                            {
                                var tempBatch = new Data.ArtisanImportJob(batchId);
                                var error = "[DL Error]: " + line.ContentPath + " Course Code: " + line.CourseCode + "<br/>" +
                                            "Error: " + e.Message + "<br/>";

                                tempBatch.Errors += error;
                                log.Info(error);
                                tempBatch.Save(UserID);
                            }
                        }
                    }
                });
            }
            catch (AggregateException e)
            {
                foreach (var ex in e.InnerExceptions)
                {

                }
            }
        }

        private void ProcessContent(List<Resources.ProSchoolImportLine> lines, int batchId, ILog log, HttpContext ctx, string appPath, Dictionary<string, List<ArtisanAssetImport>> existingAssets)
        {

            try
            {
                log.Info("[Status] Processing Content");
                var _lock = new Object();
                int count = 0;
                foreach (Resources.ProSchoolImportLine line in lines)
                //Parallel.ForEach<Resources.ProSchoolImportLine>(lines, line =>
                {
                    HttpContext.Current = ctx;

                    if (!line.Processed && (line.DownloadCompleted || line.IsHtml))
                    {

                        try
                        {
                            HttpContext.Current = ctx;
                            log.Info("[Status] Line: " + count);
                            count++;

                            if (line.IsDuplicate && line.DuplicateOf.Processed)
                            {
                                line.Pages = line.DuplicateOf.Pages;
                                line.Processed = true;
                                log.Info("[Status] Skipping Dup - " + line.ContentPath);
                                continue;
                            }

                            string extension = Path.GetExtension(line.FilePath);

                            switch (extension)
                            {
                                case ".html":
                                case ".htm":
                                    log.Info("[Status] Processing HTML - " + line.FilePath);



                                    Regex rgx = new Regex("^http://[^/]+/");
                                    string s3url = rgx.Replace(line.ContentPath, "http://s3.amazonaws.com/");
                                    if (RemoteFileExists(s3url))
                                    {
                                        line.ContentPath = s3url;
                                    }
                                    else
                                    {
                                        line.PathProblem = true;
                                        log.Info("[PathProblem] Course " + line.CourseCode + " has HTML file not on S3 " + line.ContentPath);
                                    }

                                    line.IsHtml = true;

                                    Data.ArtisanAssetType assetType = new Data.ArtisanAssetType((int)ArtisanAssetType.Html);
                                    string template = assetType.Template.Replace("{path}", line.ContentPath)
                                        .Replace("height:100%;", "height:600px;");
                                    ArtisanPage htmlPage = new ArtisanPage()
                                    {
                                        Html = string.Format(WrapperMarkup, template, ""),
                                        PageType = (int)ArtisanPageType.Content,
                                        Name = line.SectionTitle,
                                        CourseId = 0,
                                        ExternalID = line.CourseCode + " " + Path.GetFileName(line.FilePath)
                                    };

                                    line.Pages = new List<ArtisanPage>() { htmlPage };
                                    line.Processed = true;
                                    break;
                                case ".ppt":
                                case ".pptx":
                                case ".doc":
                                case ".docx":
                                case ".pdf":

                                    log.Info("[Status] Filename " + Path.GetFileName(line.ContentPath));
                                    string filename = Path.GetFileName(line.ContentPath);
                                    if (existingAssets.Keys.Contains(filename))
                                    {
                                        log.Info("[Status] PDF Already processed, building pages...");
                                        line.Pages = new List<ArtisanPage>();
                                        foreach (ArtisanAssetImport asset in existingAssets[Path.GetFileName(line.ContentPath)])
                                        {
                                            Data.ArtisanPage page = new Symphony.Core.Data.ArtisanPage();
                                            page.Html = string.Format(WrapperMarkup, "<img style='max-width:100%' src='" + Utilities.ResolveUrl(ArtisanAssetFilesLocation + asset.Id + ".png") + "' />", "");
                                            page.PageType = (int)ArtisanPageType.Content;
                                            page.Name = asset.Name;
                                            page.CourseID = 0;
                                            page.ExternalID = line.CourseCode + " " + Path.GetFileName(line.FilePath);

                                            line.Pages.Add(Model.Create<Models.ArtisanPage>(page));
                                        }
                                        line.Processed = true;

                                    }
                                    else
                                    {

                                        log.Info("[Status] Processing PDF - " + line.FilePath);

                                        using (Stream file = File.OpenRead(line.FilePath))
                                        {
                                            //lock (_lock)
                                            //{
                                            line.Pages = ImportDocument(CustomerID, Path.GetFileName(line.FilePath), null, 0,
                                                     0, null, null, "noscale", null, null, line.CourseCode + " " + Path.GetFileName(line.FilePath), file, Path.GetFileName(line.ContentPath), appPath).Data;
                                            //}
                                            line.Processed = true;
                                        }
                                    }
                                    break;
                                case ".swf":
                                    if (existingAssets.Keys.Contains(Path.GetFileName(line.FilePath)))
                                    {
                                        ArtisanAssetImport asset = existingAssets[Path.GetFileName(line.FilePath)].First();

                                        log.Info("[Status] SWF Exists, building page...");

                                        string html = asset.Template.Replace("{path}", asset.Path)
                                                                        .Replace("{element_id}", "" + asset.Id)
                                                                        .Replace("height=\"100%\"", "height=\"500\"");

                                        ArtisanPage page = new ArtisanPage();
                                        page.Html = string.Format(WrapperMarkup, html, "");
                                        page.PageType = (int)ArtisanPageType.Content;
                                        page.Name = line.SectionTitle;
                                        page.CourseId = 0;
                                        page.ExternalID = line.CourseCode + " " + Path.GetFileName(line.FilePath);

                                        line.Pages = new List<ArtisanPage>() { page };
                                        line.Processed = true;

                                    }
                                    else
                                    {
                                        log.Info("[Status] Processing SWF - " + line.FilePath);

                                        using (Stream file = File.OpenRead(line.FilePath))
                                        {
                                            byte[] fileData = null;
                                            using (var binaryReader = new BinaryReader(file))
                                            {
                                                fileData = binaryReader.ReadBytes((int)file.Length);
                                            }

                                            ArtisanAsset asset = (new ArtisanController()).UploadArtisanAsset(CustomerID,
                                                Path.GetFileName(line.FilePath), Path.GetFileName(line.FilePath), "", fileData, appPath, line.CourseCode + " " + Path.GetFileName(line.FilePath)).Data;

                                            AugmentAsset(asset, new Data.ArtisanAssetType(asset.AssetTypeId));

                                            string html = asset.Template.Replace("{path}", asset.Path)
                                                                        .Replace("{element_id}", "" + asset.Id)
                                                                        .Replace("height=\"100%\"", "height=\"500\"");

                                            ArtisanPage page = new ArtisanPage();
                                            page.Html = string.Format(WrapperMarkup, html, "");
                                            page.PageType = (int)ArtisanPageType.Content;
                                            page.Name = line.SectionTitle;
                                            page.CourseId = 0;
                                            page.ExternalID = line.CourseCode + " " + Path.GetFileName(line.FilePath);

                                            line.Pages = new List<ArtisanPage>() { page };
                                            line.Processed = true;
                                        }
                                    }
                                    break;
                            }
                        }
                        catch (Exception e)
                        {
                            lock (_lock)
                            {
                                var tempBatchJob = new Data.ArtisanImportJob(batchId);
                                var error = "[ProcessError] : " + line.ContentPath + " Course Code: " + line.CourseCode + "<br/>" +
                                            "Error: " + e.Message + "<br/>";
                                tempBatchJob.Save(UserID);

                            }
                        }
                    }
                }//);
            }
            catch (AggregateException e)
            {
                foreach (var ex in e.InnerExceptions)
                {

                }
            }
        }

        public int ImportArtisanCourses(string basePath, ILog log, string appPath, int userId, int customerId, int categoryId, string username)
        {
            var reader = new StreamReader(File.OpenRead(@"C:\Working\symphony2x\course_content.csv"));
            var proschoolsUrl = "http://lms.proschools.com/";

            var lastCourse = "";
            var lastChapter = "";
            var lastSection = "";
            var lastCourseCode = "";

            HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org/", null), new HttpResponse(null));
            HttpContext.Current.Items.Add("UserData", new AuthUserData()
            {
                CustomerID = customerId,
                UserID = userId,
                Username = username
            });

            var runDate = DateTime.Now;

            var courseCreated = false;
            var csv = new CsvHelper.CsvReader(reader);

            string courseTitle = "", courseCode = "", chapterTitle = "", sectionTitle = "", filePath = "";

            var batchJob = new Data.ArtisanImportJob();
            batchJob.Running = true;
            batchJob.SectionsProcessed = 0;
            batchJob.PagesProcessed = 0;
            batchJob.CoursesProcessed = 0;
            batchJob.LinesProcessed = 0;
            batchJob.Course = "Processing CSV...";
            batchJob.Section = "Downloading";
            batchJob.Save(UserID);

            DateTime start = DateTime.Now;

            var ctx = HttpContext.Current;

            Thread t = new Thread((args) =>
            {
                try
                {
                    HttpContext.Current = ctx;
                    log.Info("[Status] Thread Started");

                    log.Info("[Status] loading existing assets...");
                    List<ArtisanAssetImport> assets = new InlineQuery().ExecuteTypedList<ArtisanAssetImport>(@"select 
                        max([dbo].[ArtisanAssets].[Id]) as [Id], 
                        [dbo].[ArtisanAssets].[CustomerID], 
                        [dbo].[ArtisanAssets].[Name],
                        REPLACE(STUFF(Name, PATINDEX('%(% of %)%', Name), 1000, ''), '[PDF] ', '') as [OriginalName],
                        [dbo].[ArtisanAssets].[Description],
                        [dbo].[ArtisanAssets].[AssetTypeID],
                        [dbo].[ArtisanAssets].[CreatedOn],
                        [dbo].[ArtisanAssets].[ModifiedOn],
                        [dbo].[ArtisanAssets].[ModifiedBy],
                        [dbo].[ArtisanAssets].[FileName],
                        [dbo].[ArtisanAssets].[KeyWords],
                        [dbo].[ArtisanAssets].[IsPublic],
                        [dbo].[ArtisanAssets].[FromImport],
                        [dbo].[ArtisanAssets].[IsGrouped],
                        [dbo].[ArtisanAssets].[Url], 
                        [dbo].[ArtisanAssets].[Width],
                        [dbo].[ArtisanAssets].[Height], 
                        [dbo].[ArtisanAssets].[CreatedByUserId], 
                        [dbo].[ArtisanAssets].[ModifiedByUserId],
                        [dbo].[ArtisanAssets].[CreatedByActualUserId],
                        [dbo].[ArtisanAssets].[ModifiedByActualUserId],
                        [dbo].[ArtisanAssets].[AlternateHtml],
                        [dbo].[ArtisanAssets].[ExternalID]
                        from ArtisanAssets
                        WHERE CustomerID = 384 and FromImport = 1 and CreatedByUserId = 44428 and CreatedOn > '2014-09-24'
                        group by 
                        [dbo].[ArtisanAssets].[Name], 
                        [dbo].[ArtisanAssets].[CustomerID], 
                        [dbo].[ArtisanAssets].[Description],
                        [dbo].[ArtisanAssets].[AssetTypeID],
                        [dbo].[ArtisanAssets].[CreatedOn],
                        [dbo].[ArtisanAssets].[ModifiedOn],
                        [dbo].[ArtisanAssets].[ModifiedBy],
                        [dbo].[ArtisanAssets].[FileName],
                        [dbo].[ArtisanAssets].[KeyWords],
                        [dbo].[ArtisanAssets].[IsPublic],
                        [dbo].[ArtisanAssets].[FromImport],
                        [dbo].[ArtisanAssets].[IsGrouped],
                        [dbo].[ArtisanAssets].[Url], 
                        [dbo].[ArtisanAssets].[Width],
                        [dbo].[ArtisanAssets].[Height], 
                        [dbo].[ArtisanAssets].[CreatedByUserId], 
                        [dbo].[ArtisanAssets].[ModifiedByUserId],
                        [dbo].[ArtisanAssets].[CreatedByActualUserId],
                        [dbo].[ArtisanAssets].[ModifiedByActualUserId],
                        [dbo].[ArtisanAssets].[AlternateHtml],
                        [dbo].[ArtisanAssets].[ExternalID]
                    ORDER BY max([dbo].[ArtisanAssets].[Id])");

                    Dictionary<string, List<ArtisanAssetImport>> existingAssets = new Dictionary<string, List<ArtisanAssetImport>>();
                    foreach (ArtisanAssetImport asset in assets)
                    {
                        string assetName = asset.OriginalName.Trim();
                        if (!existingAssets.Keys.Contains(assetName))
                        {
                            existingAssets.Add(assetName, new List<ArtisanAssetImport>());
                        }
                        if (existingAssets[assetName].Where(a => a.Name == asset.Name).Count() == 0)
                        {
                            existingAssets[assetName].Add(asset);
                        }
                    }


                    log.Info("[Status] Existing assets loaded");
                    log.Info("[Status] Existing Asset Count: " + existingAssets.Count);

                    List<Resources.ProSchoolImportLine> lines = new List<Resources.ProSchoolImportLine>();
                    Dictionary<string, List<Resources.ProSchoolImportLine>> courses = new Dictionary<string, List<Resources.ProSchoolImportLine>>();
                    Dictionary<string, Resources.ProSchoolImportLine> uniqueFiles = new Dictionary<string, Resources.ProSchoolImportLine>();

                    List<ArtisanCourse> existingCourses = Select.AllColumnsFrom<Data.ArtisanCourse>()
                                                            .Where(Data.ArtisanCourse.ExternalIDColumn)
                                                            .IsNotNull()
                                                            .ExecuteTypedList<ArtisanCourse>();

                    Dictionary<string, ArtisanCourse> existingCourseDictionary = existingCourses
                                                                                    .Distinct<ArtisanCourse>(new ExternalArtisanCourseComparer())
                                                                                    .ToList()
                                                                                    .ToDictionary<ArtisanCourse, string>(c => c.ExternalID);

                    int courseCount = 0;
                    int sectionCount = 0;
                    int lineCount = 0;
                    string lastSectionRead = "";
                    int duplicateCount = 0;
                    int fileCount = 0;

                    while (csv.Read())
                    {
                        Resources.ProSchoolImportLine line = new Resources.ProSchoolImportLine()
                        {
                            CourseTitle = csv.GetField<string>(0).Replace("\"", ""),
                            CourseCode = csv.GetField<string>(1),
                            ChapterTitle = csv.GetField<string>(2).Replace("\"", ""),
                            SectionTitle = csv.GetField<string>(3).Replace("\"", ""),
                            ContentPath = csv.GetField<string>(4),
                            PathProblem = false,
                            DownloadCompleted = false,
                            Processed = false,
                            IsDuplicate = false
                        };

                        if (existingCourseDictionary.Keys.Contains(line.CourseCode))
                        {
                            continue;
                        }

                        if (!line.ContentPath.Contains("http://"))
                        {
                            var uriBase = new Uri("http://lms.proschools.com/");

                            line.ContentPath = new Uri(uriBase, line.ContentPath).ToString();
                        }

                        string extension = Path.GetExtension(line.ContentPath);

                        if (extension != ".htm" && extension != ".html")
                        {
                            if (!uniqueFiles.Keys.Contains(line.ContentPath))
                            {
                                uniqueFiles[line.ContentPath] = line;
                                fileCount++;
                            }
                            else
                            {
                                line.IsDuplicate = true;
                                line.DuplicateOf = uniqueFiles[line.ContentPath];
                                duplicateCount++;
                            }
                        }
                        else
                        {
                            line.IsHtml = true;
                        }




                        if (lastSectionRead != line.ChapterTitle)
                        {
                            sectionCount++;
                        }
                        lastSectionRead = line.ChapterTitle;

                        lines.Add(line);

                        if (!courses.Keys.Contains(line.CourseCode))
                        {
                            courses[line.CourseCode] = new List<Resources.ProSchoolImportLine>();
                            courseCount++;
                        }
                        lineCount++;
                        courses[line.CourseCode].Add(line);
                    }
                    csv.Dispose();

                    var htmlFiles = lines.Where(c => c.IsHtml).Count();

                    log.Info("[Status] Need to download " + fileCount + " files");
                    log.Info("[Status] Html Files " + htmlFiles);
                    log.Info("[Status] Duplicated Files " + duplicateCount);
                    batchJob.Lines = lineCount;
                    batchJob.CourseCount = courseCount;
                    batchJob.SectionCount = sectionCount;
                    batchJob.TotalFiles = fileCount;
                    batchJob.FilesDuplicate = duplicateCount;
                    batchJob.HTMLFiles = htmlFiles;
                    batchJob.CoursesSkipped = 0;
                    batchJob.LinesSkipped = 0;
                    batchJob.Save(UserID);


                    if (!System.IO.Directory.Exists(basePath))
                    {
                        System.IO.Directory.CreateDirectory(basePath);
                    }

                    //int x = 0;
                    //while (x < lines.Count)
                    //{
                    //    batchJob.Course = "Downloading Line - " + x  + " to " + (x + 100) + "... ";
                    //   batchJob.Save(UserID);
                    //   DownloadContent(lines.GetRange(x, (x + 100 < lines.Count ? 100 : lines.Count - x)), batchJob.Id, basePath, log);
                    //   x = x + 100;
                    //    
                    //}

                    DownloadContent(lines, batchJob.Id, basePath, log, ctx);
                    batchJob.FilesNotFound = lines.Where(c => c.FileNotFound).Count();
                    batchJob.FilesDownloaded = lines.Where(c => c.DownloadCompleted && !c.IsHtml).Count();
                    batchJob.FilesError = lines.Where(c => !c.FileNotFound && !c.DownloadCompleted && !c.IsHtml).Count();

                    batchJob.DownloadTime = DateTime.Now.Subtract(start).ToString();
                    batchJob.Save(UserID);
                    log.Info("[Status] Download complete. " + batchJob.FilesDownloaded + " files downloaded in " + batchJob.DownloadTime);

                    var processStartTime = DateTime.Now;
                    ProcessContent(lines, batchJob.Id, log, ctx, appPath, existingAssets);
                    batchJob.ProcessTime = DateTime.Now.Subtract(processStartTime).ToString();
                    log.Info("[Status] Processing complete in " + batchJob.DownloadTime);
                    batchJob.Save();

                    var artisanStartTime = DateTime.Now;
                    var _lock = new object();
                    Category currentCategory = null;

                    //Parallel.ForEach(courses, courseLines =>
                    foreach (KeyValuePair<string, List<Resources.ProSchoolImportLine>> courseLines in courses)
                    {
                        ArtisanCourse course = new ArtisanCourse();
                        log.Info("[Artisan] (NEW TP) Course Start");
                        if (courseLines.Value.Where(c => (!c.DownloadCompleted || c.FileNotFound) && !c.IsHtml).Count() > 0)
                        {
                            string error = "[Skip] Skipping course: " + courseLines.Key + " " + courseLines.Value[0].CourseTitle + " some content was not downloaded.";
                            //lock (_lock)
                            //{
                            //    var b1 = new Data.ArtisanImportJob(batchJob.Id);
                            batchJob.Errors += error;
                            batchJob.CoursesSkipped++;
                            batchJob.LinesSkipped += courseLines.Value.Count;
                            batchJob.Save();
                            //}
                            log.Info(error);
                            continue;
                        }


                        try
                        {
                            courseCode = courseLines.Key;
                            courseTitle = courseLines.Value[0].CourseTitle;

                            currentCategory = CategoryController.SaveCategory(customerId, 0, CategoryType.Artisan, new Category()
                            {
                                ParentCategoryID = categoryId,
                                Description = courseCode,
                                Name = courseTitle,
                                CategoryTypeID = (int)CategoryType.Artisan
                            }).Data;

                            lastChapter = "";
                            log.Info("[Artisan] Adding " + courseLines.Value.Count + " lines");
                            int sectionOrder = 1;
                            foreach (Resources.ProSchoolImportLine line in courseLines.Value)
                            {
                                //var b3 = new Data.ArtisanImportJob(batchJob.Id);
                                courseTitle = line.CourseTitle;
                                courseCode = line.CourseCode;
                                chapterTitle = line.ChapterTitle;
                                sectionTitle = line.SectionTitle;
                                filePath = line.FilePath;

                                if (lastChapter != chapterTitle)
                                {
                                    batchJob.SectionsProcessed++;
                                }

                                if (chapterTitle != "Course Orientation" && courseTitle != "course_title" && chapterTitle != "Course Instructions" && line.Pages != null && line.Pages.Count > 0)
                                {


                                    if (line.PathProblem)
                                    {
                                        batchJob.PathIssues += "Amazon S3 Url not available for: " + line.ContentPath +
                                            ", Course: " + line.CourseTitle + "(" + line.CourseCode + ")" +
                                            ", Section: " + line.ChapterTitle +
                                            ", Page: " + line.SectionTitle +
                                            ", Artisan Course ID: " + course.Id + "<br/>";
                                    }

                                    if (lastChapter != chapterTitle)
                                    {
                                        if (!string.IsNullOrEmpty(lastChapter))
                                        {
                                            log.Info("[Artisan] Creating Quiz");
                                            // This will save the course 
                                            // before adding the quiz.
                                            // no need to save after as we are 
                                            // now creating a new course/
                                            AddQuiz(course, courseCode, lastChapter);
                                        }
                                        log.Info("[Artisan] Creating Course " + courseCode + " " + chapterTitle);
                                        course = new ArtisanCourse()
                                        {
                                            CategoryId = currentCategory.Id,
                                            CompletionMethod = ArtisanCourseCompletionMethod.InlineQuestions,
                                            NavigationMethod = ArtisanCourseNavigationMethod.Sequential,
                                            Objectives = "",
                                            Parameters = "[]",
                                            DefaultParametersJSON = "[]",
                                            DefaultParameters = new List<ParameterValue>(),
                                            CustomerId = CustomerID,
                                            ExternalID = courseCode,
                                            Name = ("(PRS-" + runDate.ToString("d/M/yy") + ") " + sectionOrder.ToString("00") + ". " + chapterTitle),
                                            ThemeId = 3,
                                            Keywords = courseCode,
                                            Id = 0,
                                            Sections = new List<ArtisanSection>()
                                        };
                                        course.Sections.Add(AddSection(courseCode, chapterTitle));

                                        lastChapter = chapterTitle;
                                        sectionOrder++;
                                    }

                                    if (course.Sections.Last().Sections.Last().Pages == null)
                                    {
                                        course.Sections.Last().Sections.Last().Pages = new List<ArtisanPage>();
                                    }

                                    course.Sections.Last().Sections.Last().Pages.AddRange(line.Pages);

                                    batchJob.CourseID = line.CourseCode;
                                    batchJob.Section = chapterTitle;
                                    batchJob.Course = courseTitle;
                                    batchJob.PagesProcessed += line.Pages.Count;
                                }

                                batchJob.LinesProcessed++;
                                batchJob.Save();

                            }

                            log.Info("[Artisan] Adding Last Quiz");
                            // This will save the current course and add a quiz
                            // we don't need to save afterwards since we will create a new
                            // course next
                            AddQuiz(course, courseCode, chapterTitle);

                            log.Info("[Artisan] Adding Final");
                            // Create a new course for the final
                            course = new ArtisanCourse()
                            {
                                CategoryId = currentCategory.Id,
                                CompletionMethod = ArtisanCourseCompletionMethod.Test,
                                NavigationMethod = ArtisanCourseNavigationMethod.Sequential,
                                Objectives = "",
                                Parameters = "[]",
                                DefaultParametersJSON = "[]",
                                DefaultParameters = new List<ParameterValue>(),
                                CustomerId = CustomerID,
                                ExternalID = courseCode,
                                Name = "(PRS-" + runDate.ToString("d/M/yy") + ") " + sectionOrder.ToString("00") + ". " + courseTitle + " Final",
                                ThemeId = 3,
                                Keywords = courseCode + ", Final",
                                Sections = new List<ArtisanSection>(),
                                Id = 0
                            };

                            course = AddFinal(course, courseCode, customerId, userId);

                            if (course.Sections.Last().SectionType != (int)ArtisanSectionType.Posttest)
                            {
                                // If no final was actually added, then delete it entirely
                                DeleteCourse(customerId, userId, course.Id);
                            }

                            batchJob.CoursesProcessed++;
                            batchJob.Save(UserID);
                            log.Info("[Artisan] Finished Course");
                        }
                        catch (Exception e)
                        {
                            string error = "[CreateError] ArtisanID: " + course.Id + "Course Code: " + courseCode + " Chapter Title: " + chapterTitle + " Content Path: " + " Section Title: " + sectionTitle + "<br/>" +
                                            "Error: " + e.Message + "<br/>";
                            //var b6 = new Data.ArtisanImportJob(batchJob.Id);
                            batchJob.Errors += error;
                            batchJob.Save();
                            log.Info(error);
                        }
                    }//);
                    //batchJob.ArtisanTime = DateTime.Now.Subtract(artisanStartTime).ToString();
                    batchJob.TotalTime = DateTime.Now.Subtract(start).ToString();
                    batchJob.Running = false;
                    batchJob.Save(UserID);

                    // Directory.Delete(basePath, true);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            });

            t.Start();

            return batchJob.Id;
        }

        private string CalculateMD5Hash(MD5 md5, string input)
        {
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        private void AddQuiz(ArtisanCourse course, string courseCode, string chapterTitle)
        {
            int id = SaveCourse(CustomerID, UserID, course.Id, course).Data.Id;
            try
            {
                Data.SPs.AddQuiz(id, courseCode, chapterTitle).Execute();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message + " QUIZ on " + courseCode + " " + chapterTitle + " " + course.Id);
            }
        }

        private ArtisanCourse AddFinal(ArtisanCourse course, string courseCode, int customerId, int userId)
        {
            int id = SaveCourse(CustomerID, UserID, course.Id, course).Data.Id;
            try
            {
                Data.SPs.AddFinal(id, courseCode).Execute();
            }
            catch (Exception e)
            {
                DeleteCourse(customerId, userId, id);
                throw new Exception(e.Message + " FINAL on " + courseCode + " " + course.Id);
            }
            return GetCourse(customerId, userId, id).Data;
        }
        private bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                var status = response.StatusCode;
                response.Close();

                return (status == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }
        #endregion

        #region ArtisanUpdater
        /// <summary>
        /// Coppies files to artisan courses in order to make js updates.
        /// </summary>
        /// <param name="isTestMode">if set to true will only log the paths that will be updated. Defaults to false.</param>
        /// <param name="contentsOverride">optional array of strings that point to files in the Symphony.Core.Resources.Packager assembly to update in the course</param>
        /// <param name="rusticiCoursePath">optional path to use for rustici course root - if left empty the value from this.FilePathToContentRoot will be used</param>
        /// <param name="log">optional logger to use - used when run from console app to send the console logger instead.</param>
        public void UpdateArtisanCourses(bool isTestMode = false, string[] contentsOverride = null, string rusticiCoursePath = null, ILog log = null)
        {
            if (log == null)
            {
                log = Log;
            }



            int courseCount = 0;
            int fileCount = 0;
            List<string> errors = new List<string>();

            if (string.IsNullOrEmpty(rusticiCoursePath))
            {
                rusticiCoursePath = this.FilePathToContentRoot;
            }

            log.Info("Updating courses in " + rusticiCoursePath);

            // Test mode for just checking that paths are correct
            if (isTestMode)
            {
                log.Info("Running in TEST MODE - No files will be updated");
            }

            string[] customerDirectories = Directory.GetDirectories(rusticiCoursePath);

            List<string> courseDirectories = new List<string>();

            foreach (string customerDir in customerDirectories)
            {
                courseDirectories.AddRange(Directory.GetDirectories(customerDir));
            }

            // These are the files we are most interested in updating
            // if we need others, add them to this array
            string[] contents = new string[] { 
                "js/artisan.utilities.js", 
                "js/artisan.models.js", 
                "js/artisan.playback.js",
                "js/lz-string.min.js"
            };

            // use contents override if set 
            if (contentsOverride != null)
            {
                contents = contentsOverride;
            }

            Type t = this.GetType();
            string root = "Symphony.Core.Resources.Packager.";
            StringBuilder dynamicHeader = new StringBuilder();
            Dictionary<string, Stream> streamDictionary = new Dictionary<string, Stream>();

            foreach (string item in contents)
            {
                try
                {
                    Stream stream = t.Assembly.GetManifestResourceStream(root + item.Replace('/', '.'));
                    streamDictionary.Add(item, stream);
                }
                catch (Exception e)
                {
                    string error = "Could not load file " + item + ". The error was: " + e.Message;
                    log.Info(error);
                    errors.Add(error);
                }
            }

            byte[] buffer = new byte[32768];



            // Iterate all course folders and update as needed
            foreach (string courseDir in courseDirectories)
            {
                courseCount++;
                log.Info("Updating course: " + courseDir + " (Course " + courseCount + " of " + courseDirectories.Count() + ")");

                int courseFileCount = 0;

                foreach (KeyValuePair<string, Stream> fileStream in streamDictionary)
                {
                    string path = Path.Combine(courseDir, fileStream.Key);
                    Stream stream = fileStream.Value;

                    if (stream != null && File.Exists(path))
                    {
                        fileCount++;
                        courseFileCount++;

                        log.Info("Adding " + fileStream.Key + " to " + path + "(File " + courseFileCount + " of " + contents.Count() + ")");

                        using (FileStream fs = new FileStream(path, FileMode.Create))
                        {

                            while (true)
                            {
                                int read = stream.Read(buffer, 0, buffer.Length);
                                if (read <= 0)
                                {
                                    break;
                                }

                                if (!isTestMode)
                                {
                                    fs.Write(buffer, 0, read);
                                }
                            }

                            stream.Seek(0, SeekOrigin.Begin);
                        }
                    }
                    else
                    {
                        string error = "File " + path + " does not exist. Not updating.";
                        log.Info(error);
                        errors.Add(error);
                    }
                }
            }

            log.Info("Completed - Updated " + courseCount + " of " + courseDirectories.Count());
            log.Info("Updated " + fileCount + " files. There were " + contents.Count() + " files per course.");

            if (errors.Count() > 0)
            {
                log.Info("Encountered the following errors:");
                foreach (string err in errors)
                {
                    log.Info(err);
                }
            }

            if (isTestMode)
            {
                log.Info("!!! This script ran in Test Mode! No files were actually modified. !!!");
            }
        }

        #endregion

    }
}
