﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Web;
using Symphony.Web;
using SubSonic;
using System.Configuration;
using System.IO;
using Symphony.Core;
using Symphony.Core.Models;
using Symphony.Core.Models.Salesforce;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;
using log4net;

namespace Symphony.Core.Controllers.Salesforce
{
    public class SalesforceController : SymphonyController
    {
        protected const string SalesforceDateFormat = "yyyy-MM-ddTHH:mm:ssZ";
        protected const string SalesforceFriendlyDateFormat = "M/d/yyyy h:mm";
        protected const string CustomerIDHeaderString = "X-Symphony-Target-Customer";
        private static int targetCustomerIDOverride = 0;
        private ILog Log = LogManager.GetLogger(typeof(SalesforceController));

        public static void OverrideTargetCustomerID(int targetCustomerID)
        {
            targetCustomerIDOverride = targetCustomerID;
        }

        private Email GetEmail(string email, string subdomain)
        {
            string defaultEmailPath = HttpContext.Current.Server.MapPath(string.Format("/SalesforceEmails/{0}.json", email));
            string customerEmailPath = HttpContext.Current.Server.MapPath(string.Format("/SalesforceEmails/{0}/{1}.json", subdomain, email));
            string emailPath = File.Exists(customerEmailPath) ? customerEmailPath : defaultEmailPath;

            if (!File.Exists(emailPath))
            {
                throw new Exception("Could not find email: " + emailPath);
            }

            string emailJson = File.ReadAllText(emailPath);

            emailJson = Regex.Replace(emailJson, @"\t|\n|\r", "");

            return Utilities.Deserialize<Email>(emailJson);
        }

        protected Email TryFindEmail(string codeName, string subDomain)
        {
            try
            {
                return GetEmail(codeName, subDomain);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        protected void SendNewUserEmail(Data.User user, bool? isForced = null)
        {
            try
            {
                if (isForced.HasValue && isForced.Value == false) {
                    return;
                }

                Data.Customer customer = new Data.Customer(user.CustomerID);
                string emailCode = (customer.SsoEnabled && customer.SsoType == (int)SsoType.Janrain) ? "new_user_janrain" : "new_user";
                Email email = TryFindEmail(emailCode, customer.SubDomain);

                if (email != null)
                {
                    SendEmail(email, user);
                    return;
                }

                NotificationController.SendNotification(NotificationType.SalesforceNewUser, new NotificationOptions()
                {
                    CustomerOverride = customer,
                    IsForced = isForced,
                    TemplateObjects = new object[] { user, customer }
                });

            } catch (Exception ex) {
                Log.Error(ex);
            }
        }

        protected void SendNewEnrollmentEmail(Data.User user, EntitlementList entitlements = null, bool? isForced = null)
        {
            try
            {
                if (isForced.HasValue && isForced.Value == false)
                {
                    return;
                }

                Data.Customer customer = new Data.Customer(user.CustomerID);
                Email email = TryFindEmail("new_enrollment", customer.SubDomain);

                if (email != null)
                {
                    SendEmail(email, user);
                    return;
                }

                if (entitlements != null)
                {
                    NotificationController.SendNotification(NotificationType.SalesforceNewEnrollment, new NotificationOptions()
                    {
                        CustomerOverride = customer,
                        IsForced = isForced,
                        Sync = false,
                        TemplateObjects = new object[] { user, entitlements, customer }
                    });
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }
        private void SendEmail(Email email, Data.User user)
        {
            try
            {
                Data.Customer customer = new Data.Customer(user.CustomerID);
                string baseUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "");
                string loginUrl = string.Format("{0}/login/{1}", baseUrl, customer.SubDomain);

                if (string.IsNullOrEmpty(email.Message))
                {
                    throw new Exception("Email template does not contain the key \"message\"");
                }
                else if (string.IsNullOrEmpty(email.Subject))
                {
                    throw new Exception("Email template does not contain the key \"subject\"");
                }

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("firstname", user.FirstName);
                parameters.Add("username", user.Username);
                parameters.Add("password", user.Password);
                parameters.Add("loginUrl", loginUrl);

                email.Message = Regex.Replace(email.Message, @"\{(.+?)\}", m => parameters[m.Groups[1].Value]);

                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SalesforceEmails"]))
                {
                    string sfEmails = ConfigurationManager.AppSettings["SalesforceEmails"];
                    bool doSFEmails = true;
                    if (bool.TryParse(sfEmails, out doSFEmails))
                    {
                        // there's a config, and it's parseable
                        // if this is false, bail out
                        if (!doSFEmails)
                        {
                            return;
                        }
                    }
                }

                NotificationController.SendMessageSMTP(customer, email.Message, email.Subject, null, user.Email, "normal");
            }
            catch (Exception e)
            {
                LogManager.GetLogger(typeof(SalesforceController)).Error(e);
                //throw e;
            }
        }

        protected int TargetCustomerID
        {
            get
            {
                if (targetCustomerIDOverride > 0)
                {
                    return targetCustomerIDOverride;
                }

                if (HttpContext.Current.Request.Headers[CustomerIDHeaderString] != null)
                {
                    string customerIdString = HttpContext.Current.Request.Headers[CustomerIDHeaderString];
                    int customerId;

                    if (int.TryParse(customerIdString, out customerId))
                    {
                        return customerId;
                    }

                    Data.Customer customer = new Data.Customer(Data.Customer.Columns.SubDomain, customerIdString);

                    if (customer != null && customer.Id > 0)
                    {
                        return customer.Id;
                    }
                    else
                    {
                        throw new Exception("Could not find customer with subdomain: " + customerIdString);
                    }

                }

                throw new Exception(CustomerIDHeaderString + " header was not set.");
            }
        }
    }
}
