﻿using System;
using System.ServiceModel.Web;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Controllers;
using Symphony.Core.Models.Salesforce;
using Symphony.Web;
using System.Web.Script.Serialization;
using SubSonic;
using System.Text.RegularExpressions;
using System.Transactions;

namespace Symphony.Core.Controllers.Salesforce
{

    public class CompanyController : SalesforceController
    {
        public Company GetCompany(int userId, int customerId, string domain)
        {
            CustomerController customerController = new CustomerController();

            Models.SingleResult<Models.Customer> customerResult = customerController.GetCustomer(domain);

            Company salesforceCompany = GetSalesforceCompany(customerResult.Data);

            salesforceCompany.Success = true;

            return salesforceCompany;
        }

        public Company GetCompany(int userId, int customerId, int id)
        {
            CustomerController customerController = new CustomerController();

            Models.SingleResult<Models.Customer> customerResult = customerController.GetCustomer(customerId, userId, id);

            Company salesforceCompany = GetSalesforceCompany(customerResult.Data);
            
            salesforceCompany.Success = true;
            
            return salesforceCompany;
        }

        public Company PostCompany(int userId, int customerId, Company company)
        {
            CustomerController customerController = new CustomerController();

            Models.Customer symphonyCustomer = GetSymphonyCustomer(company);

            Regex r = new Regex(@"[^a-zA-Z0-9]");
            string domain = "sf" + r.Replace(symphonyCustomer.Name, "");

            symphonyCustomer.SubDomain = domain.Substring(0, domain.Length < 50 ? domain.Length : 50);
            symphonyCustomer.TimeZone = "UTC";
            symphonyCustomer.IsCreatedBySalesforce = true;

            symphonyCustomer.SsoEnabled = true;
            symphonyCustomer.SsoType = (int)SsoType.Janrain;
            
            Data.Customer existing = new Symphony.Core.Data.Customer(Data.Customer.Columns.SubDomain, symphonyCustomer.SubDomain);
            if (existing.Id > 0)
            {
                symphonyCustomer.CopyFrom(existing);
                Company sfCompany = GetSalesforceCompany(symphonyCustomer);
                sfCompany.SetIssue("Warning", "EXISTS", "Company already exists with the subdomain " + symphonyCustomer.SubDomain);
                sfCompany.Success = true;
                return sfCompany;
            }

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                Models.SingleResult<Models.Customer> customerResult = customerController.SaveCustomer(customerId, userId, 0, symphonyCustomer);

                company.CompanyId = customerResult.Data.ID;

                company.Success = true;

                ts.Complete();
            }
            return company;
        }

        public Company PutCompany(int userId, int customerId, int id, Company company)
        {
            CustomerController customerController = new CustomerController();

            Models.Customer newCustomer = GetSymphonyCustomer(company);
            Models.Customer currentCustomer = customerController.GetCustomer(customerId, userId, id).Data;

            if (!currentCustomer.IsCreatedBySalesforce)
            {
                throw new Exception("Cannot edit companies not created by Salesforce.");
            }
            
            currentCustomer = ApplyNewCustomerData(newCustomer, currentCustomer);

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                Models.SingleResult<Models.Customer> customerResult = customerController.SaveCustomer(customerId, userId, id, currentCustomer);

                company = GetSalesforceCompany(customerResult.Data);

                company.IsEnabled = true;
                company.Success = true;

                ts.Complete();
            }
            return company;
        }

        public Company DeleteCompany(int userId, int customerId, int id)
        {
            CustomerController customerController = new CustomerController();

            Models.Customer disableCustomer = customerController.GetCustomer(customerId, userId, id).Data;

            if (!disableCustomer.IsCreatedBySalesforce)
            {
                throw new Exception("Cannot edit companies that were not created by Salesforce.");
            }

            disableCustomer.SalesforceIsEnabled = false;
            
            Company salesforceCompany;
            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                Models.SingleResult<Models.Customer> customerResult = customerController.SaveCustomer(customerId, userId, id, disableCustomer);

                salesforceCompany = GetSalesforceCompany(customerResult.Data);

                salesforceCompany.Success = true;

                ts.Complete();
            }

            return salesforceCompany;
        }

        private Models.Customer GetSymphonyCustomer(Company salesforceCompany)
        {
            Models.Customer symphonyCustomer = new Models.Customer();

            symphonyCustomer.ID = salesforceCompany.CompanyId;

            salesforceCompany.CopyTo(symphonyCustomer);

            if (salesforceCompany.IsEnabled != null) symphonyCustomer.SalesforceIsEnabled = (bool)salesforceCompany.IsEnabled;

            if (salesforceCompany.Addresses != null && salesforceCompany.Addresses.Count > 0)
            {
                symphonyCustomer.SalesforceAddress = new JavaScriptSerializer().Serialize(salesforceCompany.Addresses);
            }

            return symphonyCustomer;
        }
        private Company GetSalesforceCompany(Models.Customer symphonyCustomer)
        {
            Company salesforceCompany = new Company();

            symphonyCustomer.CopyTo(salesforceCompany);
            salesforceCompany.CompanyId = symphonyCustomer.ID;
            salesforceCompany.IsEnabled = symphonyCustomer.SalesforceIsEnabled;

            if (symphonyCustomer.SalesforceAddress != null)
            {
                salesforceCompany.Addresses = new JavaScriptSerializer().Deserialize<List<Address>>(symphonyCustomer.SalesforceAddress);
            }
            return salesforceCompany;
        }

        private Models.Customer ApplyNewCustomerData(Models.Customer newCustomer, Models.Customer currentCustomer)
        {
            if (newCustomer.Name != null) currentCustomer.Name = newCustomer.Name;
            if (newCustomer.SalesforceAddress != null) currentCustomer.SalesforceAddress = newCustomer.SalesforceAddress;
            if (newCustomer.Phone != null) currentCustomer.Phone = newCustomer.Phone;
            if (newCustomer.Fax != null) currentCustomer.Fax = newCustomer.Fax;
            if (newCustomer.SalesforceAccountId != null) currentCustomer.SalesforceAccountId = newCustomer.SalesforceAccountId;
            if (newCustomer.SalesforceContactId != null) currentCustomer.SalesforceContactId = newCustomer.SalesforceContactId;

            return currentCustomer;
        }
    }
}
