﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models.Salesforce;
using Symphony.Web;
using SubSonic;
using System.Text.RegularExpressions;

namespace Symphony.Core.Controllers.Salesforce
{

    public class ProductController : SalesforceController
    {

        private string GenerateSKU(Models.TrainingProgram tp)
        {
            Regex r = new Regex(@"[^a-zA-Z0-9\s]");
            string alphaNumeric = r.Replace(tp.Name, "");

            r = new Regex(@"\s+");
            return tp.Id + "_" + r.Replace(alphaNumeric.Trim(), "_");
        }

        /// <summary>
        /// Generates a query of either TrainingPrograms or TrainingProgramBundles depending on what type is passed in
        /// </summary>
        /// <typeparam name="TSchema"></typeparam>
        /// <param name="since"></param>
        /// <param name="productId"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        private SqlQuery GetProductQuery<TSchema>(DateTime? since, string productId, List<string> columns = null) where TSchema : RecordBase<TSchema>, new()
        {
            TSchema item = new TSchema();
            if (item.TableName != "TrainingProgram" && item.TableName != "TrainingProgramBundle")
            {
                throw new Exception("Invalid product type.");
            }

            TableSchema.Table schema = item.GetSchema();

            if (columns == null)
            {
                columns = schema.Columns.Select(c => c.ColumnName).ToList();
                columns.Add(Data.Customer.Columns.SubDomain);
                columns.Add(Data.Category.NameColumn.QualifiedName + " as CategoryName");
            }

            TableSchema.TableColumn sku = schema.Columns.Where(c => c.ColumnName == "Sku").ToList()[0];

            SqlQuery query = new Select(columns.ToArray())
                .From<TSchema>()
                .InnerJoin<Data.Customer>()
                .LeftOuterJoin<Data.Category>()
                .Where(Data.Customer.Columns.Modules).ContainsString(Modules.Salesforce)
                .And(sku).IsNotNull()
                .And(sku).IsNotEqualTo("");

            if (since != null)
            {
                TableSchema.TableColumn createdOn = schema.Columns.Where(c => c.ColumnName == "CreatedOn").ToList()[0];
                TableSchema.TableColumn modifiedOn = schema.Columns.Where(c => c.ColumnName == "ModifiedOn").ToList()[0];

                query.AndExpression(createdOn.QualifiedName).IsGreaterThanOrEqualTo(since)
                        .Or(modifiedOn.QualifiedName).IsGreaterThanOrEqualTo(since)
                        .CloseExpression();
            }

            if (!string.IsNullOrEmpty(productId))
            {
                query.And(sku.QualifiedName).IsEqualTo(productId)
                    .And(Data.Customer.IdColumn).IsEqualTo(TargetCustomerID);
            }

            List<TableSchema.TableColumn> isDeleted = schema.Columns.Where(c => c.ColumnName == "IsDeleted").ToList();

            if (isDeleted.Count > 0)
            {
                // Filter out any deleted training program bundles
                query.And(isDeleted[0].QualifiedName).IsEqualTo(false);
            }

            List<TableSchema.TableColumn> isSalesforce = schema.Columns.Where(c => c.ColumnName == "IsSalesforce").ToList();

            if (isSalesforce.Count > 0)
            {
                // We want to filter out any training programs that were
                // duplicated by the salesforce api
                // IsSalesforce = duplicate training program created on purchase.
                query.And(isSalesforce[0].QualifiedName).IsEqualTo(false);
            }

            return query;
        }

        public List<Product> GetProductList(int userId, int customerId, DateTime? since, string productId, bool isSession)
        {
            // Stick all the data into a Salesforce product
            List<Product> products = new List<Product>();

            CustomerController customerController = new CustomerController();
            Models.SingleResult<Models.Customer> customer = customerController.GetCustomer(customerId, userId, customerId);

            List<Models.TrainingProgram> trainingPrograms = GetProductQuery<Data.TrainingProgram>(since, productId).ExecuteTypedList<Models.TrainingProgram>();
            List<Models.TrainingProgramBundle> trainingProgramBundles = GetProductQuery<Data.TrainingProgramBundle>(since, productId).ExecuteTypedList<Models.TrainingProgramBundle>();


            if (trainingPrograms.Count <= 0 && trainingProgramBundles.Count <= 0)
            {
                throw new Exception("There are no products available to Salesforce");
            }

            if (trainingPrograms.Count > 0)
            {
                int[] trainingProgramIds = trainingPrograms.Select(tp => tp.Id).ToArray();

                SqlQuery getCoursesQuery = Select.AllColumnsFrom<Data.AssignedTrainingProgramCoursesDetail>()
                    .And(Data.AssignedTrainingProgramCoursesDetail.Columns.TrainingProgramID).In(
                        GetProductQuery<Data.TrainingProgram>(since, productId, new List<string>() { Data.TrainingProgram.Columns.Id })
                    );

                List<Models.AssignedTrainingProgramCourse> trainingProgramCourses = getCoursesQuery.ExecuteTypedList<Models.AssignedTrainingProgramCourse>();
                int[] trainingProgramCourseIds = trainingProgramCourses.Select(c => c.Id).ToArray();

                if (trainingProgramCourseIds.Count() == 0)
                {
                    trainingProgramCourseIds = new int[] { -1 };
                }

                SqlQuery getClassesQuery = Select.AllColumnsFrom<Data.SalesforceClass>()
                                 .Where(Data.SalesforceClass.Columns.CourseID).In(trainingProgramCourseIds);

                List<Models.ClassroomClass> trainingProgramClasses = getClassesQuery.ExecuteTypedList<Models.ClassroomClass>();

                foreach (Models.TrainingProgram trainingProgram in trainingPrograms)
                {
                    Product product = new Product();
                    product.ProductId = trainingProgram.Sku;
                    product.Name = trainingProgram.Name;

                    product.Sku = trainingProgram.Sku;
                    product.Sessions = new List<Session>();

                    if (!isSession)
                    {
                        product.Description = trainingProgram.Description;
                        product.ListPrice = trainingProgram.Cost;
                        product.Discipline = trainingProgram.CategoryName;
                        product.BusinessUnit = trainingProgram.SubDomain;

                        product.Status = trainingProgram.IsLive ? "Active" : "Inactive";
                        product.ContentType = "";
                        product.State = null;

                        product.Private = true;
                        product.PrivateCompanyId = trainingProgram.CustomerID;
                        product.Success = true;
                    }

                    bool hasClassroom = false;

                    decimal totalCost = 0;
                    int totalDuration = 0;

                    if (trainingProgram.SessionCourseID.HasValue && trainingProgram.SessionCourseID.Value > 0)
                    {
                        Data.Course course = new Data.Course(trainingProgram.SessionCourseID);

                        List<Models.ClassroomClass> sessionClasses = Select.AllColumnsFrom<Data.SalesforceClass>()
                                 .Where(Data.SalesforceClass.Columns.CourseID).IsEqualTo(trainingProgram.SessionCourseID)
                                 .OrderAsc(Data.SalesforceClass.Columns.MinClassDate)
                                 .ExecuteTypedList<Models.ClassroomClass>();

                        List<int> sessionClassIds = sessionClasses.Select(sc => sc.Id).ToList();
                        if (sessionClassIds.Count == 0)
                        {
                            sessionClassIds.Add(-1);
                        }

                        List<Models.ClassDate> sessionClassDates = Select.AllColumnsFrom<Data.ClassDate>()
                            .Where(Data.ClassDate.ClassIDColumn).In(sessionClassIds)
                            .ExecuteTypedList<Models.ClassDate>();

                        decimal cost = 0;
                        int duration = 0;

                        foreach (Models.ClassroomClass c in sessionClasses)
                        {
                            c.DateList = sessionClassDates.Where(scd => scd.ClassID == c.Id).ToList();

                            if (cost < c.TotalCost)
                            {
                                cost = c.TotalCost;
                            }

                            if (duration < c.TotalDuration)
                            {
                                duration = c.TotalDuration;
                            }

                            SqlQuery resources = Select.AllColumnsFrom<Data.Resource>()
                                .Where(Data.Resource.Columns.Id).In(new Select(Data.ClassResource.Columns.ResourceID)
                                    .From<Data.ClassResource>()
                                    .Where(Data.ClassResource.Columns.ClassID).IsEqualTo(c.Id))
                                    .OrderAsc(Data.Resource.Columns.Name);

                            c.ResourceList = resources.ExecuteTypedList<Models.ClassResource>();


                            DateTime beginDateTime = DateTime.SpecifyKind(c.Dates[0], DateTimeKind.Utc);
                            var lastDate = c.DateList.Last();
                            var endDate = lastDate.StartDateTime.AddMinutes((int)lastDate.Duration);
                            DateTime endDateTime = DateTime.SpecifyKind(endDate, DateTimeKind.Utc);

                            Session session = new Session();
                            session.SessionId = c.Id;
                            session.City = c.VenueCity;
                            session.State = new State();
                            session.State.Name = c.StateName;
                            session.State.Abbreviation = c.StateAbbreviation;
                            session.BeginDateTime = beginDateTime.ToString(SalesforceDateFormat);
                            session.EndDateTime = endDateTime.ToString(SalesforceDateFormat);
                            session.CurrentEnrollment = c.CurrentEnrollment;
                            session.MaxEnrollment = c.CapacityOverride;
                            session.Details = c.ClassDescription;
                            session.DurationInDays = course.NumberOfDays.Value;
                            session.Name = c.CourseName; // +": " + beginDateTime.ToString(SalesforceFriendlyDateFormat) + " - " + endDateTime.ToString(SalesforceFriendlyDateFormat);

                            session.Private = true;
                            session.PrivateCompanyId = trainingProgram.CustomerID;

                            if (endDateTime < DateTime.Now)
                            {
                                session.Status = "Completed";
                                session.OnlineRegistrationAllowed = false;
                            }
                            else if (c.CurrentEnrollment >= c.CapacityOverride && !c.LockedIndicator)
                            {
                                session.Status = "Waiting List";
                                session.OnlineRegistrationAllowed = true;
                            }
                            else if (c.CurrentEnrollment < c.CapacityOverride && !c.LockedIndicator)
                            {
                                session.Status = "Available";
                                session.OnlineRegistrationAllowed = true;
                            }
                            else if (c.LockedIndicator)
                            {
                                session.Status = "Registration Closed";
                                session.OnlineRegistrationAllowed = false;
                            }

                            product.Sessions.Add(session);
                        }

                    }

                    if (!isSession)
                    { // TODO - Ask about /product?since /product/session?since
                        product.DeliveryMethod = hasClassroom ? "Physical" : "Online Access";
                        product.DeliveryType = hasClassroom ? "Physical" : "Online Access";

                        product.Hours = totalDuration;
                        product.Cost = totalCost;
                    }

                    // If sessions were queried directly, excluded products without sessions
                    if (!isSession || isSession && product.Sessions.Count() > 0)
                    {
                        products.Add(product);
                    }

                }
            }

            // If we found bundles
            // and its not a session query. Since bundles do not have
            // sessions they will be excluded
            if (trainingProgramBundles.Count() > 0 && !isSession)
            {
                foreach (Models.TrainingProgramBundle bundle in trainingProgramBundles)
                {
                    Product product = new Product()
                    {
                        ProductId = bundle.Sku,
                        Sku = bundle.Sku,
                        Name = bundle.Name,
                        BusinessUnit = bundle.SubDomain,
                        Discipline = bundle.CategoryName,
                        Status = bundle.Status,
                        Private = true,
                        PrivateCompanyId = bundle.CustomerID,
                        Cost = bundle.Cost,
                        ListPrice = bundle.ListPrice,
                        ContentType = bundle.ContentType,
                        DeliveryMethod = bundle.DeliveryMethod,
                        DeliveryType = "Bundle",
                        Hours = bundle.Hours,
                        State = null,
                        Success = true
                    };

                    products.Add(product);
                }
            }

            return products;
        }

        public ProductList GetProducts(int userId, int customerId, DateTime? since)
        {
            ProductList products = new ProductList();
            products.Products = GetProductList(userId, customerId, since, null, false);

            return products;
        }

        public Product GetProduct(int userId, int customerId, string id)
        {
            List<Product> product = GetProductList(userId, customerId, null, id, false);

            return product[0];

        }

        public ProductList GetProductSession(int userId, int customerId, DateTime? since)
        {
            ProductList products = new ProductList();
            products.Products = GetProductList(userId, customerId, since, null, true);

            return products;


        }



    }
}
