﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using Symphony.Core.Controllers;
using Symphony.Core.Models;
using sf = Symphony.Core.Models.Salesforce;
using Symphony.Web;
using System.Web.Script.Serialization;
using SubSonic;
using System.Transactions;
using System.Web.Security;
using Symphony.Core.Extensions;

namespace Symphony.Core.Controllers.Salesforce
{

    public class TrainingProgramController : SalesforceController
    {

        public sf.TrainingProgram GetTrainingProgram(int userId, int customerId, string trainingProgramSku)
        {
            Data.TrainingProgram dataTp = new Data.TrainingProgram(Data.TrainingProgram.SkuColumn.ColumnName, trainingProgramSku);

            if (dataTp.Id == 0)
            {
                throw new Exception("Could not find training program for: " + trainingProgramSku);
            }

            TrainingProgram tp = (new PortalController()).GetTrainingProgramDetails(userId, dataTp.Id, false).Data;
            sf.TrainingProgram stp = new sf.TrainingProgram();

            stp.CopyFrom(tp);

            // Add venue, room., registration lists
            stp.RequiredCourses = AddCourseDetails(tp.RequiredCourses);
            stp.ElectiveCourses = AddCourseDetails(tp.ElectiveCourses);
            stp.OptionalCourses = AddCourseDetails(tp.OptionalCourses);
            stp.FinalAssessments = AddCourseDetails(tp.FinalAssessments);

            stp.Sessions = new List<sf.Session>();

            if (tp.SessionCourseID.HasValue && tp.SessionCourseID.Value > 0)
            {
                List<Models.ClassroomClass> sessionClasses = Select.AllColumnsFrom<Data.SalesforceClass>()
                                 .Where(Data.SalesforceClass.Columns.CourseID).IsEqualTo(tp.SessionCourseID)
                                 .OrderAsc(Data.SalesforceClass.Columns.MinClassDate)
                                 .ExecuteTypedList<Models.ClassroomClass>();

                //TODO: avoid "in" error here
                List<Models.ClassDate> sessionClassDates = Select.AllColumnsFrom<Data.ClassDate>()
                    .Where(Data.ClassDate.ClassIDColumn).In(sessionClasses.Select(sc => sc.Id))
                    .ExecuteTypedList<Models.ClassDate>();

                Data.Course course = new Data.Course(tp.SessionCourseID);

                foreach (Models.ClassroomClass c in sessionClasses)
                {
                    c.DateList = sessionClassDates.Where(scd => scd.ClassID == c.Id).ToList();

                    SqlQuery resources = Select.AllColumnsFrom<Data.Resource>()
                        .Where(Data.Resource.Columns.Id).In(new Select(Data.ClassResource.Columns.ResourceID)
                            .From<Data.ClassResource>()
                            .Where(Data.ClassResource.Columns.ClassID).IsEqualTo(c.Id))
                            .OrderAsc(Data.Resource.Columns.Name);

                    c.ResourceList = resources.ExecuteTypedList<Models.ClassResource>();


                    DateTime beginDateTime = DateTime.SpecifyKind(c.Dates[0], DateTimeKind.Utc);
                    var lastDate = c.DateList.Last();
                    var endDate = lastDate.StartDateTime.AddMinutes((int)lastDate.Duration);
                    DateTime endDateTime = DateTime.SpecifyKind(endDate, DateTimeKind.Utc);

                    sf.Session session = new sf.Session();
                    session.SessionId = c.Id;
                    session.City = c.VenueCity;
                    session.State = new sf.State();
                    session.State.Name = c.StateName;
                    session.State.Abbreviation = c.StateAbbreviation;
                    session.BeginDateTime = beginDateTime.ToString(SalesforceDateFormat);
                    session.EndDateTime = endDateTime.ToString(SalesforceDateFormat);
                    session.CurrentEnrollment = c.CurrentEnrollment;
                    session.MaxEnrollment = c.CapacityOverride;
                    session.Details = c.ClassDescription;
                    session.DurationInDays = course.NumberOfDays.Value;
                    session.Name = c.CourseName; // +": " + beginDateTime.ToString(SalesforceFriendlyDateFormat) + " - " + endDateTime.ToString(SalesforceFriendlyDateFormat);

                    session.Private = true;
                    session.PrivateCompanyId = tp.CustomerID;

                    if (endDateTime < DateTime.UtcNow)
                    {
                        session.Status = "Completed";
                        session.OnlineRegistrationAllowed = false;
                    }
                    else if (c.CurrentEnrollment >= c.CapacityOverride && !c.LockedIndicator)
                    {
                        session.Status = "Waiting List";
                        session.OnlineRegistrationAllowed = true;
                    }
                    else if (c.CurrentEnrollment < c.CapacityOverride && !c.LockedIndicator)
                    {
                        session.Status = "Available";
                        session.OnlineRegistrationAllowed = true;
                    }
                    else if (c.LockedIndicator)
                    {
                        session.Status = "Registration Closed";
                        session.OnlineRegistrationAllowed = false;
                    }

                    stp.Sessions.Add(session);
                }
            }

            return stp;
        }

        public sf.Registration PostRegistration(int userId, int customerId, string trainingProgramSku, sf.Registration registration)
        {
            Data.TrainingProgram dataTp = new Data.TrainingProgram(Data.TrainingProgram.SkuColumn.ColumnName, trainingProgramSku);

            if (dataTp.Id == 0)
            {
                throw new Exception("Could not find training program for: " + trainingProgramSku);
            }

            if (!(registration.ClassID > 0 && registration.RegistrantID > 0))
            {
                throw new WebFaultException<string>("Registration needs a valid class and registrant id.", System.Net.HttpStatusCode.BadRequest);
            }

            Data.ClassX classroom = new Data.ClassX(registration.ClassID);
            if (classroom.Id == 0)
            {
                throw new Exception("Invalid class: " + registration.ClassID);
            }


            Data.TrainingProgram tp = Select.AllColumnsFrom<Data.TrainingProgram>()
                .Where(Data.TrainingProgram.SessionCourseIDColumn).IsEqualTo(classroom.CourseID)
                .ExecuteSingle<Data.TrainingProgram>();

            if (tp == null)
            {
                throw new Exception("The specified class is not a valid session for the specified training program.");
            }

            Data.User user = new Data.User(registration.RegistrantID);

            if (user.Id == 0)
            {
                throw new Exception("The user could not be found.");
            }

            Data.Registration existingRegistration = Select.AllColumnsFrom<Data.Registration>()
                   .Where(Data.Registration.RegistrantIDColumn).IsEqualTo(registration.RegistrantID)
                   .And(Data.Registration.ClassIDColumn).IsEqualTo(registration.ClassID)
                   .ExecuteSingle<Data.Registration>();

            Data.Room room = new Data.Room(classroom.RoomID);

            SqlQuery registeredUsersQuery = new Select()
                .From<Data.Registration>()
                .Where(Data.Registration.ClassIDColumn)
                .IsEqualTo(classroom.Id)
                .And(Data.Registration.RegistrationStatusIDColumn)
                .IsEqualTo((int)RegistrationStatusType.Registered);

            registeredUsersQuery.Aggregates.Add(new Aggregate(Data.Registration.RegistrantIDColumn, AggregateFunction.Count));

            int registeredUsers = registeredUsersQuery.ExecuteScalar<int>();

            int capacity = classroom.CapacityOverride > 0 ? classroom.CapacityOverride : room.Capacity;

            RegistrationStatusType registrationStatus;

            if (classroom.LockedIndicator || registeredUsers >= capacity)
            {
                registrationStatus = RegistrationStatusType.WaitList;
            }
            else
            {
                registrationStatus = RegistrationStatusType.Registered;
            }

            Data.Registration registrationData = new Data.Registration()
            {
                ClassID = classroom.Id,
                RegistrantID = registration.RegistrantID,
                RegistrationTypeID = (int)RegistrationType.Manual,
                RegistrationStatusID = (int)registrationStatus
            };

            if (existingRegistration != null)
            {
                registrationData = existingRegistration;
            }

            registrationData.Save(UserID);

            sf.Registration sfRegistration = new sf.Registration();
            sfRegistration.CopyFrom(registrationData);
            sfRegistration.CopyFrom(user);

            return sfRegistration;
        }

        public sf.Registrations GetRegistrations(int userId, int customerId, string trainingProgramSku, int classId)
        {
            Data.TrainingProgram dataTp = new Data.TrainingProgram(Data.TrainingProgram.SkuColumn.ColumnName, trainingProgramSku);

            if (dataTp.Id == 0)
            {
                throw new Exception("Could not find training program for: " + trainingProgramSku);
            }

            ClassroomClass classroom = Select.AllColumnsFrom<Data.ClassX>()
                .InnerJoin(Data.Course.IdColumn, Data.ClassX.CourseIDColumn)
                .InnerJoin(Data.TrainingProgramToCourseLink.CourseIDColumn, Data.Course.IdColumn)
                .Where(Data.ClassX.IdColumn).IsEqualTo(classId)
                .And(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn).IsEqualTo(dataTp.Id)
                .ExecuteSingle<ClassroomClass>();

            if (classroom == null)
            {
                throw new Exception("The specified class was not found within the specified training program.");
            }

            List<sf.Registration> registrations = (new ClassroomController()).GetRegistrations(customerId, userId, classId.ToString(), "", OrderDirection.Ascending, 0, int.MaxValue)
                                                .Data
                                                .Select(x => new sf.Registration()
                                                {
                                                    ClassID = x.ClassID,
                                                    FirstName = x.FirstName,
                                                    MiddleName = x.MiddleName,
                                                    LastName = x.LastName,
                                                    RegistrantID = x.RegistrantID,
                                                    RegistrationStatusID = x.RegistrationStatusID
                                                }).ToList();

            sf.Registrations sfRegistrations = new sf.Registrations()
            {
                AwaitingApproval = registrations.Where(r => r.RegistrationStatusID == (int)RegistrationStatusType.AwaitingApproval).ToList(),
                Denied = registrations.Where(r => r.RegistrationStatusID == (int)RegistrationStatusType.Denied).ToList(),
                Registered = registrations.Where(r => r.RegistrationStatusID == (int)RegistrationStatusType.Registered).ToList(),
                WaitList = registrations.Where(r => r.RegistrationStatusID == (int)RegistrationStatusType.WaitList).ToList()
            };

            return sfRegistrations;
        }

        private List<sf.Course> AddCourseDetails(List<TrainingProgramCourse> courses)
        {
            List<sf.Course> sfCourses = new List<sf.Course>();

            List<int> courseIds = courses.Select(c => c.Id).ToList();

            if (courseIds.Count < 1)
            {
                return sfCourses;
            }

            List<string> columns = new List<string>();


            List<sf.ClassroomClass> classes = new Select(
                    Data.ClassX.AdditionalInstructionsColumn.QualifiedName,
                    Data.ClassX.CapacityOverrideColumn.QualifiedName,
                    Data.ClassX.DescriptionColumn.QualifiedName,
                    Data.ClassX.IdColumn.QualifiedName,
                    Data.ClassX.InternalCodeColumn.QualifiedName,
                    Data.ClassX.IsVirtualColumn.QualifiedName,
                    Data.ClassX.LockedIndicatorColumn.QualifiedName,
                    Data.ClassX.NameColumn.QualifiedName,
                    Data.ClassX.NotesColumn.QualifiedName,
                    Data.ClassX.ObjectiveColumn.QualifiedName,
                    Data.ClassX.SurveyIDColumn.QualifiedName,
                    Data.ClassX.CourseIDColumn.QualifiedName,
                    Data.ClassX.WebinarKeyColumn.QualifiedName,
                    Data.ClassX.ApprovalRequiredColumn.QualifiedName,
                    Data.ClassX.IsThirdPartyColumn.QualifiedName,
                    Data.ClassX.RoomIDColumn.QualifiedName,
                    Data.ClassX.ExternalIDColumn.QualifiedName,
                    "[dbo].[" + Data.Tables.Room + "].[" + Data.Room.Columns.Name + "] AS [RoomName]",
                    "[dbo].[" + Data.Tables.Room + "].[" + Data.Room.Columns.Capacity + "] AS [Capacity]",
                    "[dbo].[" + Data.Tables.Room + "].[" + Data.Room.Columns.Id + "] AS [RoomId]",
                    "[dbo].[" + Data.Tables.Room + "].[" + Data.Room.Columns.InternalCode + "] AS [RoomInternalCode]",
                    "[dbo].[" + Data.Tables.Room + "].[" + Data.Room.Columns.RoomCost + "] AS [RoomCost]",
                    "[dbo].[" + Data.Tables.Room + "].[" + Data.Room.Columns.NetworkAccessIndicator + "] AS [NetworkAccessIndicator]",
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.Id + "] AS [VenueId]",
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.InternalCode + "] AS [VenueInternalCode]",
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.Name + "] AS [VenueName]",
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.TimeZoneID + "] AS [TimeZoneID]",
                    "[dbo].[" + Data.Tables.Venue + "].[" + Data.Venue.Columns.City + "] AS [VenueCity]",
                    "[dbo].[" + Data.Tables.State + "].[" + Data.State.Columns.Name + "] AS [StateName]",
                    "[dbo].[" + Data.Tables.TimeZone + "].[" + Data.TimeZone.Columns.Description + "] AS [TimeZoneDescription]"
                )
                .From(Data.Tables.ClassX)
                .Where(Data.ClassX.CourseIDColumn).In(courseIds)
                .LeftOuterJoin(Data.Tables.Room, Data.Room.Columns.Id, Data.Tables.ClassX, Data.ClassX.Columns.RoomID)
                .LeftOuterJoin(Data.Tables.Venue, Data.Venue.Columns.Id, Data.Tables.Room, Data.Room.Columns.VenueID)
                .LeftOuterJoin(Data.Tables.State, Data.State.Columns.Id, Data.Tables.Venue, Data.Venue.Columns.StateID)
                .LeftOuterJoin(Data.Tables.TimeZone, Data.TimeZone.Columns.Id, Data.Tables.Venue, Data.Venue.Columns.TimeZoneID)
                .ExecuteTypedList<sf.ClassroomClass>();

            List<int> classIds = classes.Select(c => c.Id).ToList();

            if (classIds.Count < 1)
            {
                classIds.Add(-1);
            }

            List<sf.Registration> registrations = new Select(
                    Data.Registration.ClassIDColumn.QualifiedName,
                    Data.Registration.RegistrationStatusIDColumn.QualifiedName,
                    Data.Registration.RegistrantIDColumn.QualifiedName,
                    Data.User.LastNameColumn.QualifiedName,
                    Data.User.MiddleNameColumn.QualifiedName,
                    Data.User.FirstNameColumn.QualifiedName
                ).From(Data.Tables.Registration)
                .InnerJoin(Data.ClassX.IdColumn, Data.Registration.ClassIDColumn)
                .InnerJoin(Data.User.IdColumn, Data.Registration.RegistrantIDColumn)
                .And(Data.Registration.ClassIDColumn).In(classIds)
                .ExecuteTypedList<sf.Registration>();

            List<ClassDate> classDates = Select.AllColumnsFrom<Data.ClassDate>()
                .Where(Data.ClassDate.Columns.ClassID).In(classIds)
                .OrderAsc(Data.ClassDate.Columns.StartDateTime)
                .ExecuteTypedList<ClassDate>();

            Dictionary<int, TrainingProgramCourse> tpCourseDictionary = courses.ToDictionary(c => c.Id);
            Dictionary<int, sf.Course> sfCourseDictionary = new Dictionary<int, sf.Course>();

            foreach (TrainingProgramCourse course in courses)
            {
                sf.Course sfCourse = new sf.Course();
                sfCourse.Classes = new List<sf.Classroom>();
                sfCourse.CopyFrom(course);
                sfCourseDictionary.Add(sfCourse.Id, sfCourse);
                sfCourses.Add(sfCourse);
            }

            foreach (sf.ClassroomClass classroomClass in classes)
            {
                sf.Course sfCourse = sfCourseDictionary[classroomClass.CourseID];

                if (sfCourse.CourseTypeID == (int)CourseType.Online)
                {
                    continue;
                }

                sf.Classroom sfClass = new sf.Classroom();

                sfClass.CopyFrom(classroomClass);

                sfClass.Room = new Room()
                {
                    Id = classroomClass.RoomID,
                    Name = classroomClass.RoomName,
                    VenueID = classroomClass.VenueID,
                    Capacity = classroomClass.Capacity,
                    InternalCode = classroomClass.RoomInternalCode,
                    LockedIndicator = classroomClass.LockedIndicator,
                    NetworkAccessIndicator = classroomClass.NetworkAccessIndicator,
                    RoomCost = classroomClass.RoomCost
                };

                sfClass.Venue = new Venue()
                {
                    Id = classroomClass.VenueID,
                    Name = classroomClass.VenueName,
                    City = classroomClass.VenueCity,
                    StateName = classroomClass.StateName,
                    InternalCode = classroomClass.VenueInternalCode,
                    StateDescription = classroomClass.StateDescription,
                    StateID = classroomClass.StateID,
                    TimeZoneDescription = classroomClass.TimeZoneDescription,
                    TimeZoneID = classroomClass.TimeZoneID
                };

                sfClass.Registrations = new sf.Registrations()
                {
                    AwaitingApproval = registrations.Where(r => r.ClassID == sfClass.Id && r.RegistrationStatusID == (int)RegistrationStatusType.AwaitingApproval).ToList(),
                    Denied = registrations.Where(r => r.ClassID == sfClass.Id && r.RegistrationStatusID == (int)RegistrationStatusType.Denied).ToList(),
                    WaitList = registrations.Where(r => r.ClassID == sfClass.Id && r.RegistrationStatusID == (int)RegistrationStatusType.WaitList).ToList(),
                    Registered = registrations.Where(r => r.ClassID == sfClass.Id && r.RegistrationStatusID == (int)RegistrationStatusType.Registered).ToList()
                };

                sfClass.DateList = classDates.Where(c => c.ClassID == sfClass.Id).ToList();

                sfCourse.Classes.Add(sfClass);
            }

            return sfCourses;

        }
    }
}
