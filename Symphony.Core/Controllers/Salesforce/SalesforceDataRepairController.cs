﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Symphony.Core;
using SubSonic;
using Microsoft.VisualBasic.FileIO;
using Symphony.Core.Models.Salesforce;
using Symphony.Core.Controllers.Salesforce;
using log4net;
using System.Web;
using Symphony.Core.Models;



namespace Symphony.Core.Controllers.Salesforce
{
    public class SalesforceDataRepairController : SalesforceController
    {
        public void Repair(ILog log)
        {
            HttpContext.Current = new HttpContext(new HttpRequest(null, "http://tempuri.org/", null), new HttpResponse(null));
            HttpContext.Current.Items.Add("UserData", new AuthUserData()
            {
                CustomerID = 1,
                UserID = 44530,
                Username = "Salesforce"
            });

            
            CodingHorror c = new CodingHorror();

            List<int> missingTpIds = new List<int>();
            List<int> missingTpUsers = new List<int>();
            
            System.Data.IDataReader tpReader = c.ExecuteReader(@"select distinct trainingprogramid from OnlineCourseRollup
                left join trainingprogram
                on trainingprogram.id = onlinecourserollup.trainingprogramid
                --right join hierarchytotrainingprogramlink
                --on hierarchytotrainingprogramlink.hierarchytypeid = 4 and hierarchynodeid = onlinecourserollup.userid
                where trainingprogram.id is null
                and onlinecourserollup.ModifiedOn > '1/1/2015'
                and onlinecourserollup.trainingprogramid > 0
                and onlinecourserollup.trainingprogramid not in (select trainingprogramid from hierarchytotrainingprogramlink)");

            while (tpReader.Read())
            {
                missingTpIds.Add(tpReader.GetInt32(0));
            }

            System.Data.IDataReader userReader = c.ExecuteReader(@"select distinct UserID from OnlineCourseRollup
                left join trainingprogram
                on trainingprogram.id = onlinecourserollup.trainingprogramid
                --right join hierarchytotrainingprogramlink
                --on hierarchytotrainingprogramlink.hierarchytypeid = 4 and h`ierarchynodeid = onlinecourserollup.userid
                where trainingprogram.id is null
                and onlinecourserollup.ModifiedOn > '1/1/2015'
                and onlinecourserollup.trainingprogramid > 0
                and onlinecourserollup.trainingprogramid not in (select trainingprogramid from hierarchytotrainingprogramlink)");

            while (userReader.Read())
            {
                missingTpUsers.Add(userReader.GetInt32(0));
            }

            Dictionary<int, List<string>> skusToId = new Dictionary<int, List<string>>();

            TextFieldParser parser = new TextFieldParser(@"c:\temp\import.csv");
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            EntitlementController ec = new EntitlementController();
            var entitlements = 0;
            var skipped = 0;
            var processed = 0;
            var duplicateSkus = 0;
            var errors = 0;

            while (!parser.EndOfData)
            {
                try
                {
                    entitlements++;

                    string[] fields = parser.ReadFields();
                    
                    string id = fields[3];
                    int userId = int.Parse(id);
                    string sku = fields[1];
                    string salesforceOrderNumber = fields[5];
                    string salesforceEntitlementId = fields[6];

                    log.Debug("Processing entitlement line " + entitlements);
                    log.Debug("UserID: " + userId + " Order Number: " + salesforceOrderNumber + " EntID: " + salesforceEntitlementId + " Sku: " + sku);

                    if (missingTpUsers.Contains(userId))
                    {
                        log.Debug("This user is in the list of users with missing tps");
                        processed++;

                        if (!skusToId.ContainsKey(userId))
                        {
                            skusToId[userId] = new List<string>();
                        }

                        if (!skusToId[userId].Contains(sku))
                        {
                            skusToId[userId].Add(sku);

                            Data.TrainingProgram tp = Select.AllColumnsFrom<Data.TrainingProgram>()
                                .Where(Data.TrainingProgram.SkuColumn).IsEqualTo(sku)
                                .ExecuteSingle<Data.TrainingProgram>();

                            Data.TrainingProgramBundle tpBundle = Select.AllColumnsFrom<Data.TrainingProgramBundle>()
                                .Where(Data.TrainingProgramBundle.SkuColumn).IsEqualTo(sku)
                                .And(Data.TrainingProgramBundle.IsDeletedColumn).IsEqualTo(0)
                                .ExecuteSingle<Data.TrainingProgramBundle>();

                            if (tp != null && tp.Id > 0)
                            {
                                ec.OverrideTargetCustomerID(tp.CustomerID);      
                            }
                            else if (tpBundle != null && tpBundle.Id > 0)
                            {
                                ec.OverrideTargetCustomerID(tpBundle.CustomerID);
                            } else {
                                throw new Exception("Could not find product " + sku);
                            }

                            EntitlementList entitlments = new EntitlementList()
                            {
                                Entitlements = new List<Entitlement>()
                                {
                                    new Entitlement(){
                                        ProductId = sku,
                                        SalesforceEntitlementId = salesforceEntitlementId,
                                        SalesforceProductId = sku,
                                        EntitlementPurchaseDate = DateTime.Today.ToString("yyyy-MM-dd"),
                                        EntitlementDuration = 12,
                                        EntitlementDurationUnits = "Months",
                                        EntitlementEndDate = DateTime.Today.AddYears(1).ToString("yyyy-MM-dd"),
                                        IsSuppressEmail = true,
                                        IsEnabled = true,
                                        EntitlementStartDate = DateTime.Today.ToString("yyyy-MM-dd"),
                                        
                                    }
                                }
                            };

                            entitlments = ec.PostEntitlements(userId, entitlments, true);
                            var alreadyRegistered = false;
                            foreach (Issue issue in entitlments.Entitlements[0].Issues)
                            {
                                if (issue.Code == "ALREADY_REGISTERED")
                                {
                                    alreadyRegistered = true;
                                }
                            }


                            if (entitlments.Entitlements[0].Success && entitlments.Entitlements[0].Issues.Count == 0 || alreadyRegistered)
                            {
                                if (!alreadyRegistered)
                                {
                                    log.Debug("Updating tp ids on fresh tp");
                                }
                                else
                                {
                                    log.Debug("Already registered - going to update tp id just in case");
                                }

                                System.Data.IDataReader reader = c.ExecuteReader(@"select distinct OnlineCourseRollup.CourseID, TrainingProgramID from OnlineCourseRollup
left join trainingprogram
on trainingprogram.id = onlinecourserollup.trainingprogramid
--right join hierarchytotrainingprogramlink
--on hierarchytotrainingprogramlink.hierarchytypeid = 4 and h`ierarchynodeid = onlinecourserollup.userid
where trainingprogram.id is null
and OnlineCourseRollup.UserID = " + userId + @"
and onlinecourserollup.ModifiedOn > '1/1/2015'
and onlinecourserollup.trainingprogramid > 0
and onlinecourserollup.trainingprogramid not in (select trainingprogramid from hierarchytotrainingprogramlink)");

                                Dictionary<int, List<int>> trainingProgramToCourses = new Dictionary<int, List<int>>();

                                while (reader.Read())
                                {
                                    var courseId = reader.GetInt32(0);
                                    var trainingProgramId = reader.GetInt32(1);

                                    if (!trainingProgramToCourses.ContainsKey(trainingProgramId))
                                    {
                                        trainingProgramToCourses[trainingProgramId] = new List<int>();
                                    }

                                    if (!trainingProgramToCourses[trainingProgramId].Contains(courseId))
                                    {
                                        trainingProgramToCourses[trainingProgramId].Add(courseId);
                                    }
                                }

                                foreach (KeyValuePair<int, List<int>> pair in trainingProgramToCourses)
                                {
                                    var tpid = pair.Key;
                                    foreach (int cid in pair.Value)
                                    {
                                        log.Debug("Looking for course: " + cid + " for User: " + userId);
                                        int existingTpId;
                                        try
                                        {
                                            existingTpId = c.ExecuteScalar<int>(@"select tpl.TrainingProgramID from TrainingProgramToCourseLink tpl
                                            join TrainingProgram tp on tp.ID = tpl.TrainingProgramID
                                                                    and tp.IsSalesforce = 1
                                            join HierarchyToTrainingProgramLink htpl on htpl.TrainingProgramID = tpl.TrainingProgramID
                                                                                      and htpl.HierarchyTypeID = 4
                                                                                      and htpl.HierarchyNodeID = " + userId + @"
                                            where tpl.CourseID = " + cid + ";");
                                        }
                                        catch (Exception e)
                                        {
                                            continue;
                                        }
                                        try
                                        {
                                            int alreadyUpdated = c.ExecuteScalar<int>(@"select ID from TrainingProgram where ID = " + tpid + ";");
                                            if (alreadyUpdated > 0)
                                            {
                                                break;
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            // The original training program doesn't exist so create it below
                                        }

                                        if (existingTpId > 0)
                                        {
                                            log.Debug("Updating training program - resaving TP ID " + existingTpId + " as " + tpid);
                                            c.Execute(@"set identity_insert TrainingProgram ON;
insert into TrainingProgram ([ID]
      ,[CustomerID]
      ,[OwnerID]
      ,[Name]
      ,[InternalCode]
      ,[Cost]
      ,[Description]
      ,[IsNewHire]
      ,[IsLive]
      ,[StartDate]
      ,[EndDate]
      ,[DueDate]
      ,[EnforceRequiredOrder]
      ,[MinimumElectives]
      ,[FinalAssessmentCourseID]
      ,[FinalAssessmentCourseTypeID]
      ,[ModifiedBy]
      ,[CreatedBy]
      ,[ModifiedOn]
      ,[CreatedOn]
      ,[CategoryID]
      ,[NewHireStartDateOffset]
      ,[NewHireDueDateOffset]
      ,[NewHireEndDateOffset]
      ,[NewHireOffsetEnabled]
      ,[NewHireTransitionPeriod]
      ,[DisableScheduled]
      ,[CreatedByUserId]
      ,[ModifiedByUserId]
      ,[CreatedByActualUserId]
      ,[ModifiedByActualUserId]
      ,[PartnerID]
      ,[IsSalesforce]
      ,[EnforceCanMoveForwardIndicator]
      ,[SurveyID]
      ,[IsSurveyMandatory]
      ,[IsValidationEnabled]
      ,[ValidationInterval]
      ,[MaxCourseWork]
      ,[ShowPretestOverride]
      ,[ShowPostTestOverride]
      ,[PassingScoreOverride]
      ,[SkinOverride]
      ,[PreTestTypeOverride]
      ,[PostTestTypeOverride]
      ,[NavigationMethodOverride]
      ,[DisclaimerOverride]
      ,[ContactEmailOverride]
      ,[ShowObjectivesOverride]
      ,[PretestTestOutOverride]
      ,[Retries]
      ,[RandomizeQuestionsOverride]
      ,[RandomizeAnswersOverride]
      ,[ReviewMethodOverride]
      ,[MarkingMethodOverride]
      ,[InactivityMethodOverride]
      ,[InactivityTimeoutOverride]
      ,[MinimumPageTimeOverride]
      ,[MaximumQuestionTimeOverride]
      ,[CertificateEnabledOverride]
      ,[ExternalID]
      ,[AffidavitID]
      ,[MaxTimeOverride]
      ,[CertificatePath]
      ,[MinLoTimeOverride]
      ,[MinScoTimeOverride]
      ,[MinTimeOverride]
      ,[MetaDataJson]
      ,[Sku]
      ,[PurchasedFromBundleID]
      ,[ParentTrainingProgramID]
      ,[IsCourseTimingDisabled]
      ,[SessionCourseID])
 select " + tpid + @"
      ,[CustomerID]
      ,[OwnerID]
      ,[Name]
      ,[InternalCode]
      ,[Cost]
      ,[Description]
      ,[IsNewHire]
      ,[IsLive]
      ,[StartDate]
      ,[EndDate]
      ,[DueDate]
      ,[EnforceRequiredOrder]
      ,[MinimumElectives]
      ,[FinalAssessmentCourseID]
      ,[FinalAssessmentCourseTypeID]
      ,[ModifiedBy]
      ,[CreatedBy]
      ,[ModifiedOn]
      ,[CreatedOn]
      ,[CategoryID]
      ,[NewHireStartDateOffset]
      ,[NewHireDueDateOffset]
      ,[NewHireEndDateOffset]
      ,[NewHireOffsetEnabled]
      ,[NewHireTransitionPeriod]
      ,[DisableScheduled]
      ,[CreatedByUserId]
      ,[ModifiedByUserId]
      ,[CreatedByActualUserId]
      ,[ModifiedByActualUserId]
      ,[PartnerID]
      ,[IsSalesforce]
      ,[EnforceCanMoveForwardIndicator]
      ,[SurveyID]
      ,[IsSurveyMandatory]
      ,[IsValidationEnabled]
      ,[ValidationInterval]
      ,[MaxCourseWork]
      ,[ShowPretestOverride]
      ,[ShowPostTestOverride]
      ,[PassingScoreOverride]
      ,[SkinOverride]
      ,[PreTestTypeOverride]
      ,[PostTestTypeOverride]
      ,[NavigationMethodOverride]
      ,[DisclaimerOverride]
      ,[ContactEmailOverride]
      ,[ShowObjectivesOverride]
      ,[PretestTestOutOverride]
      ,[Retries]
      ,[RandomizeQuestionsOverride]
      ,[RandomizeAnswersOverride]
      ,[ReviewMethodOverride]
      ,[MarkingMethodOverride]
      ,[InactivityMethodOverride]
      ,[InactivityTimeoutOverride]
      ,[MinimumPageTimeOverride]
      ,[MaximumQuestionTimeOverride]
      ,[CertificateEnabledOverride]
      ,[ExternalID]
      ,[AffidavitID]
      ,[MaxTimeOverride]
      ,[CertificatePath]
      ,[MinLoTimeOverride]
      ,[MinScoTimeOverride]
      ,[MinTimeOverride]
      ,[MetaDataJson]
      ,[Sku]
      ,[PurchasedFromBundleID]
      ,[ParentTrainingProgramID]
      ,[IsCourseTimingDisabled]
      ,[SessionCourseID]
      from TrainingProgram
      where ID = " + existingTpId + @"
set identity_insert TrainingProgram OFF;");

                                        }


                                    }


                                }


                            }



                        }
                        else
                        {
                            duplicateSkus++;
                            log.Debug("Dupilate sku for user - entitlement already processed (Duplicates: " + duplicateSkus + ")");
                        }
                    }
                    else
                    {
                        skipped++;
                        log.Debug("Skipping. No unmatched training programs (Skipped: " + skipped + ")");
                    }

                }
                catch (Exception e)
                {
                    errors++;
                    log.Debug("Error!");
                    log.Debug(e);
                }
            }
            log.Debug("Entitlements: " + entitlements + " Skipped: " + skipped + " Processed: " + processed + " Duplicates " + duplicateSkus + " Errors: " + errors);
            
        }
        
    }
}
