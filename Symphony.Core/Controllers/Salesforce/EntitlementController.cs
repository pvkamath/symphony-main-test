﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models.Salesforce;
using Symphony.Web;
using Symphony.Core.Comparers;
using SubSonic;
using System.Transactions;
using Symphony.Core.Extensions;

namespace Symphony.Core.Controllers.Salesforce
{

    public class EntitlementController : SalesforceController
    {

        private Data.SalesforceEntitlement GetSalesforceEntitlement(int userId, int childTrainingProgramId, int parentTrainingProgramId)
        {
            Data.SalesforceEntitlement entitlement = Select.AllColumnsFrom<Data.SalesforceEntitlement>()
                    .Where(Data.SalesforceEntitlement.UserIdColumn).IsEqualTo(userId)
                    .And(Data.SalesforceEntitlement.ParentTrainingProgramIDColumn).IsEqualTo(parentTrainingProgramId)
                    .And(Data.SalesforceEntitlement.TrainingProgramIdColumn).IsEqualTo(childTrainingProgramId)
                    .ExecuteSingle<Data.SalesforceEntitlement>();

            if (entitlement == null)
            {
                // The training program group for the specified child/parent has not been created
                // This occurs when the child already contains the courses that exist in the
                // passed in parent. Just find the first instance of the child
                entitlement = Select.AllColumnsFrom<Data.SalesforceEntitlement>()
                    .Where(Data.SalesforceEntitlement.UserIdColumn).IsEqualTo(userId)
                    .And(Data.SalesforceEntitlement.TrainingProgramIdColumn).IsEqualTo(childTrainingProgramId)
                    .ExecuteSingle<Data.SalesforceEntitlement>();

                // If the entitlement is still null, something is wrong
                if (entitlement == null)
                {
                    throw new Exception("Could not find or create the entitlement.");
                }
            }

            return entitlement;
        }

        private Entitlement ConvertSalesforceEntitlement(Data.SalesforceEntitlement sfEntitlement, Entitlement entitlement)
        {
            entitlement.EntitlementDuration = sfEntitlement.SalesforceDuration;
            entitlement.EntitlementDurationUnits = sfEntitlement.SalesforceDurationUnits;
            entitlement.EntitlementEndDate = sfEntitlement.SalesforceEndDate.ToString(SalesforceDateFormat);
            entitlement.EntitlementId = sfEntitlement.Id;
            entitlement.EntitlementPurchaseDate = sfEntitlement.SalesforcePurchaseDate.ToString(SalesforceDateFormat);
            entitlement.EntitlementStartDate = sfEntitlement.SalesforceStartDate.ToString(SalesforceDateFormat);
            entitlement.IsEnabled = sfEntitlement.IsEnabled;
            entitlement.PartnerCode = sfEntitlement.SalesforcePartnerCode;
            entitlement.PaymentRefId = sfEntitlement.SalesforcePaymentRefId;
            entitlement.ProductCategory = sfEntitlement.SalesforceCategory;
            entitlement.ProductId = sfEntitlement.ProductId;
            entitlement.SalesforceEntitlementId = sfEntitlement.SalesforceEntitlementId;
            entitlement.SalesforceProductId = sfEntitlement.SalesforceProductId;

            return entitlement;
        }

        /// <summary>
        /// Excludes courses represented by the entilement id from the child training program
        /// Re-merges all other included courses in case there were any duplicates
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="entitlementId"></param>
        /// <returns>The child training program after the exclude and re-merge</returns>
        private Models.TrainingProgram ExcludeCourses(int userId, int entitlementId)
        {
            Data.SalesforceEntitlement sfEntitlement = Select.AllColumnsFrom<Data.SalesforceEntitlement>()
                    .Where(Data.SalesforceEntitlement.IdColumn).IsEqualTo(entitlementId)
                    .And(Data.SalesforceEntitlement.UserIdColumn).IsEqualTo(userId)
                    .ExecuteSingle<Data.SalesforceEntitlement>();

            PortalController portalController = new PortalController();
            Models.TrainingProgram childTrainingProgram = portalController.GetTrainingProgramDetails(userId, sfEntitlement.TrainingProgramId, true, true).Data;
            Models.TrainingProgram parentTrainingProgram = portalController.GetTrainingProgramDetails(userId, sfEntitlement.ParentTrainingProgramID, true, true).Data;

            // Remove the courses
            if (childTrainingProgram.ExcludeCoursesFromTrainingProgram(parentTrainingProgram))
            {
                // Add back any that were duplicates from the excluded program
                List<int> includedParentTrainingProgramIds = new Select(Data.SalesforceEntitlement.ParentTrainingProgramIDColumn)
                    .From<Data.SalesforceEntitlement>()
                    .Where(Data.SalesforceEntitlement.TrainingProgramIdColumn).IsEqualTo(sfEntitlement.TrainingProgramId)
                    .And(Data.SalesforceEntitlement.UserIdColumn).IsEqualTo(userId)
                    .And(Data.SalesforceEntitlement.ParentTrainingProgramIDColumn).IsNotEqualTo(sfEntitlement.ParentTrainingProgramID)
                    .ExecuteTypedList<int>();

                foreach (int trainingProgramId in includedParentTrainingProgramIds)
                {
                    Models.TrainingProgram includedTrainingProgram = portalController.GetTrainingProgramDetails(userId, trainingProgramId, true, true).Data;
                    childTrainingProgram.IncludeCoursesFromTrainingProgram(includedTrainingProgram);
                }
            }

            return childTrainingProgram;
        }


        private void SavePartner(Entitlement entitlement, int customerId, int userId)
        {
            // Stop creating empty audniences
            if (string.IsNullOrEmpty(entitlement.PartnerCode) || string.IsNullOrEmpty(entitlement.PartnerDisplayName))
            {
                return;
            }

            // Check if the partner code sent exists in our db
            // if it doesn't, add it in. 
            Data.SalesforcePartner sfPartner = Select.AllColumnsFrom<Data.SalesforcePartner>()
                                .Where(Data.SalesforcePartner.PartnerCodeColumn)
                                .IsEqualTo(entitlement.PartnerCode)
                                .And(Data.SalesforcePartner.CustomerIDColumn)
                                .IsEqualTo(customerId)
                                .ExecuteSingle<Data.SalesforcePartner>();

            Data.Audience partnerAudience = null;

            if (sfPartner == null)
            {
                sfPartner = new Data.SalesforcePartner();
                sfPartner.Id = 0;

                if (entitlement.PartnerId != null && entitlement.PartnerCode != null && entitlement.PartnerDisplayName != null)
                {
                    sfPartner.PartnerId = entitlement.PartnerId;
                    sfPartner.PartnerCode = entitlement.PartnerCode;
                    sfPartner.DisplayName = entitlement.PartnerDisplayName.Truncate(Data.SalesforcePartner.DisplayNameColumn.MaxLength);
                }
            }

            // Check for a audience that matches the partner code
            partnerAudience = Select.AllColumnsFrom<Data.Audience>()
                .Where(Data.Audience.SalesforcePartnerCodeColumn)
                .IsEqualTo(entitlement.PartnerCode)
                .And(Data.Audience.CustomerIDColumn)
                .IsEqualTo(customerId)
                .ExecuteSingle<Data.Audience>();

            if (partnerAudience == null || partnerAudience.Id == 0)
            {
                int parentAudienceId = new Select(Data.Audience.IdColumn)
                    .From<Data.Audience>()
                    .Where(Data.Audience.InternalCodeColumn).IsEqualTo("salesforce-partners")
                    .ExecuteScalar<int>();

                partnerAudience = new Data.Audience()
                {
                    Name = sfPartner.DisplayName.Truncate(Data.Audience.NameColumn.MaxLength),
                    SalesforcePartnerCode = sfPartner.PartnerCode,
                    InternalCode = "sf-partner-" + sfPartner.PartnerCode,
                    CustomerID = customerId
                };

                if (parentAudienceId > 0)
                {
                    partnerAudience.ParentAudienceID = parentAudienceId;
                }
                else
                {
                    Data.Audience parentAudience = new Data.Audience()
                    {
                        Name = "SalesforcePartners",
                        InternalCode = "salesforce-partners",
                        ParentAudienceID = 0,
                        CustomerID = customerId
                    };

                    parentAudience.Save();
                    partnerAudience.ParentAudienceID = parentAudience.Id;
                }

                partnerAudience.Save();
            }

            sfPartner.AudienceID = partnerAudience.Id;
            sfPartner.CustomerID = customerId;

            sfPartner.Save();

            if (partnerAudience != null)
            {
                CustomerController c = new CustomerController();
                c.SaveAudienceUsers(customerId, userId, partnerAudience.Id, new int[] { userId });
            }
        }

        private EntitlementList GetEntitlements(int userId, int entitlementId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.SalesforceEntitlement>()
                .Where(Data.SalesforceEntitlement.UserIdColumn).IsEqualTo(userId);

            if (entitlementId > 0)
            {
                query.And(Data.SalesforceEntitlement.IdColumn).IsEqualTo(entitlementId);
            }
            List<Data.SalesforceEntitlement> entitlements = query.ExecuteTypedList<Data.SalesforceEntitlement>();

            EntitlementList entitlementList = new EntitlementList();
            entitlementList.Entitlements = new List<Entitlement>();
            Dictionary<int, Data.SalesforceEntitlement> bundleEntitlements = new Dictionary<int, Data.SalesforceEntitlement>();

            foreach (Data.SalesforceEntitlement sfEntitlement in entitlements)
            {
                if (sfEntitlement.BundleID.HasValue && sfEntitlement.BundleID.Value > 0)
                {
                    // Track bundled entitlemetns. Skip them if the bundle has already been added to the list
                    if (!bundleEntitlements.ContainsKey(sfEntitlement.BundleID.Value))
                    {
                        bundleEntitlements.Add(sfEntitlement.BundleID.Value, sfEntitlement);
                    }
                    else
                    {
                        continue;
                    }
                }

                Entitlement entitlement = new Entitlement();
                entitlement = ConvertSalesforceEntitlement(sfEntitlement, entitlement);
                entitlement.Success = true;
                entitlementList.Entitlements.Add(entitlement);
            }

            return entitlementList;
        }
        /// <summary>
        /// Apply an entitlement to a user
        /// </summary>
        /// <param name="user">The user the entitlemment is being created for (Person purchasing the training program)</param>
        /// <param name="entitlement">Info about the training program being purchased</param>
        /// <returns></returns>
        private Entitlement SaveEntitlement(Data.User user, Entitlement entitlement)
        {
            PortalController portalController = new PortalController();
            CourseAssignmentController courseAssignmentController = new CourseAssignmentController();
            bool isBundle = false;
            bool isFirstTrainingProgram = true;
            int purchasingUserId = user.Id; // The user that is purchasing the training program
            int purchasingCustomerId = user.CustomerID; // customer for the user purchasing the training progarm
            int loggedInUserId = UserID; // logged in user context

            Models.TrainingProgram childTrainingProgram;

            List<Models.TrainingProgram> parentTrainingPrograms = new List<Models.TrainingProgram>();

            // We need to access the training programs or bundles based on the 
            // X-Symphony-Target-Customer header. This will map to the customer
            // that has the training program 
            List<Models.TrainingProgram> trainingPrograms = courseAssignmentController.FindTrainingPrograms(
                TargetCustomerID, UserID, "", "sku", OrderDirection.Ascending, 0, 1, null, 
                new List<Models.GenericFilter>(){
                    new Models.GenericFilter()
                    {
                        LogicOperator = (int)LogicOperators.And,
                        ComparisonOperator = (int)ComparisonOperators.EqualTo,
                        Property = Data.TrainingProgram.SkuColumn.QualifiedName,
                        Value = entitlement.ProductId
                    }
                }
            ).Data.ToList();

            Models.TrainingProgram tp = trainingPrograms.FirstOrDefault();

            Data.TrainingProgramBundle tpBundle = Select.AllColumnsFrom<Data.TrainingProgramBundle>()
                .Where(Data.TrainingProgramBundle.SkuColumn).IsEqualTo(entitlement.ProductId)
                .And(Data.TrainingProgramBundle.CustomerIDColumn).IsEqualTo(TargetCustomerID)
                .And(Data.TrainingProgramBundle.IsDeletedColumn).IsEqualTo(0)
                .ExecuteSingle<Data.TrainingProgramBundle>();


            if ((tp == null || tp.Id < 1) && (tpBundle == null || tpBundle.Id < 1))
            {
                // Error - We didn't find a training program, or a training program bundle for the 
                // current logged in customer that matches the passed in sku.
                entitlement.SetIssue("Error", "INVALID_PRODUCT", "Product Id " + entitlement.ProductId + " cannot be found");
                return entitlement;
            }
            else if (tp != null && tp.Id > 0 && tpBundle != null && tpBundle.Id > 0)
            {
                // Error - This shouldn't happen, unless the data was corrupted manually, or something else went wrong
                // but this would mean a sku has been duplicated in a bundle and a training program for a particular customer.
                // So in this case we cannot tell what the intended product was.
                entitlement.SetIssue("Error", "DATA_ERROR", "Product Id " + entitlement.ProductId + " has been duplicated in training programs and training program bundles. The data needs to be repaired before the requested product can be determined.");
                return entitlement;
            }
            else if (tp != null && tp.Id > 0)
            {
                // We found a training program matching the sku. Load the details
                // Retrieving the training program details using the logged in user
                Models.SingleResult<Models.TrainingProgram> trainingProgram = portalController.GetTrainingProgramDetails(loggedInUserId, tp.Id, true, true, TargetCustomerID);

                if (trainingProgram == null || trainingProgram.Data == null)
                {
                    // Some how we could not get training program details for this training program
                    // we will assume something is wrong with it and it is invalid.
                    entitlement.SetIssue("Error", "PRODUCT_ERROR", "Product Id " + entitlement.ProductId + " could not be loaded.");
                    return entitlement;
                }

                parentTrainingPrograms.Add(trainingProgram.Data);
            }
            else if (tpBundle != null && tpBundle.Id > 0)
            {
                isBundle = true;
                // We have a bundle, we will need to get all the training programs in this bundle so we can process them all at once
                // We will need to assign the user to each training program, and copy each training program to the user's customer
                List<int> trainingProgramIds = new Select(Data.TrainingProgramBundleMap.TrainingProgramIDColumn)
                    .From<Data.TrainingProgramBundleMap>()
                    .Where(Data.TrainingProgramBundleMap.TrainingProgramBundleIDColumn).IsEqualTo(tpBundle.Id)
                    .ExecuteTypedList<int>();

                foreach (int tpId in trainingProgramIds)
                {
                    // Load all the associated training programs
                    // **NOTE it will probably be slow loading the details for each of the training programs.
                    // Retrieving the training program details using the logged in user
                    Models.SingleResult<Models.TrainingProgram> trainingProgram = portalController.GetTrainingProgramDetails(loggedInUserId, tpId, true, true, TargetCustomerID);

                    if (trainingProgram == null || trainingProgram.Data == null)
                    {
                        // There is a missing training program in this bundle
                        // Throw an error and stop the whole process.
                        entitlement.SetIssue("Error", "BROKEN_PRODUCT", "Product Id " + entitlement.ProductId + " is a bundle, and there is a missing training program");
                        return entitlement;
                    }

                    parentTrainingPrograms.Add(trainingProgram.Data);
                }
            }
            else
            {
                // Should never get to this point. If somehow it did, just send an error
                entitlement.SetIssue("Error", "UNKNOWN_ERROR", "Something went wrong loading product: " + entitlement.ProductId);
                return entitlement;
            }

            foreach (Models.TrainingProgram parentTrainingProgram in parentTrainingPrograms)
            {

                int parentTrainingProgramId = parentTrainingProgram.Id;

                // Build the entitlement
                Data.SalesforceEntitlement salesforceEntitlement = new Data.SalesforceEntitlement();


                if (entitlement.EntitlementDuration > 0) salesforceEntitlement.SalesforceDuration = entitlement.EntitlementDuration;
                if (entitlement.EntitlementDurationUnits != null) salesforceEntitlement.SalesforceDurationUnits = entitlement.EntitlementDurationUnits;

                // Calculate start and end dates based on durations sent if any values are sent that would
                // suggest these be updated. These will be overridden if specific values are sent
                if (entitlement.EntitlementDuration > 0 ||
                    !string.IsNullOrEmpty(entitlement.EntitlementDurationUnits) ||
                    !string.IsNullOrEmpty(entitlement.EntitlementPurchaseDate) ||
                    !string.IsNullOrEmpty(entitlement.EntitlementStartDate))
                {
                    DateTime endDate = new DateTime();
                    DateTime startDate = new DateTime();

                    // Priority for start date
                    //  1. Start date sent
                    //  2. Start date existing in DB
                    //  3. Purchase date Sent
                    //  4. Current date
                    if (!string.IsNullOrEmpty(entitlement.EntitlementStartDate))
                    {
                        startDate = DateTime.Parse(entitlement.EntitlementStartDate);
                    }
                    else if (!string.IsNullOrEmpty(entitlement.EntitlementPurchaseDate) && salesforceEntitlement.SalesforceStartDate == DefaultDateTime)
                    {
                        startDate = DateTime.Parse(entitlement.EntitlementPurchaseDate);
                    }
                    else
                    {
                        if (salesforceEntitlement.SalesforceStartDate != null)
                        {
                            startDate = salesforceEntitlement.SalesforceStartDate;
                        }
                        else
                        {
                            startDate = DateTime.Now;
                        }
                    }

                    switch (salesforceEntitlement.SalesforceDurationUnits)
                    {
                        case "Days":
                            endDate = startDate.AddDays(salesforceEntitlement.SalesforceDuration);
                            break;
                        case "Months":
                            endDate = startDate.AddMonths(salesforceEntitlement.SalesforceDuration);
                            break;
                    }

                    salesforceEntitlement.SalesforceStartDate = startDate;
                    salesforceEntitlement.SalesforceEndDate = endDate;

                    // Default purchase date to current time, if there is none set
                    // it will be overridden below if one was passed in.
                    if (salesforceEntitlement.SalesforcePurchaseDate == DefaultDateTime)
                    {
                        salesforceEntitlement.SalesforcePurchaseDate = DateTime.Now;
                    }
                }

                // Override calculated dates, if the Start/End/Purchase dates have been supplied
                if (entitlement.EntitlementStartDate != null) salesforceEntitlement.SalesforceStartDate = DateTime.Parse(entitlement.EntitlementStartDate);
                if (entitlement.EntitlementEndDate != null) salesforceEntitlement.SalesforceEndDate = DateTime.Parse(entitlement.EntitlementEndDate);
                if (entitlement.EntitlementPurchaseDate != null) salesforceEntitlement.SalesforcePurchaseDate = DateTime.Parse(entitlement.EntitlementPurchaseDate);

                if (entitlement.SalesforceEntitlementId != null) salesforceEntitlement.SalesforceEntitlementId = entitlement.SalesforceEntitlementId;
                if (entitlement.SalesforceProductId != null) salesforceEntitlement.SalesforceProductId = entitlement.SalesforceProductId;

                if (entitlement.ProductCategory != null) salesforceEntitlement.SalesforceCategory = entitlement.ProductCategory;
                if (entitlement.PaymentRefId > 0) salesforceEntitlement.SalesforcePaymentRefId = entitlement.PaymentRefId;
                if (entitlement.PartnerCode != null) salesforceEntitlement.SalesforcePartnerCode = entitlement.PartnerCode;
                if (entitlement.IsEnabled != null) salesforceEntitlement.IsEnabled = entitlement.IsEnabled;
                if (!string.IsNullOrEmpty(entitlement.ProductId)) salesforceEntitlement.ProductId = entitlement.ProductId;

                // Track the partner, only saves if it doesn't exist yet
                // Will create an audience for the partner if it doesn't exist
                // in the purchaing user's customer and add the user to the audience
                SavePartner(entitlement, purchasingCustomerId, purchasingUserId);

                if (entitlement.AudienceCodes != null)
                {
                    foreach (var audience in entitlement.AudienceCodes)
                    {
                        if (!string.IsNullOrWhiteSpace(audience))
                        {
                            Data.Audience dataAudience = new Data.Audience(Data.Audience.Columns.InternalCode, audience);
                            if (dataAudience.Id == 0)
                            {
                                dataAudience.Save();
                            }
                            CustomerController c = new CustomerController();
                            c.SaveAudienceUsers(purchasingCustomerId, purchasingUserId, dataAudience.Id, new int[] { purchasingUserId });
                        }
                    }
                }

                // You can purchase a training program bundle that might have an existing training program registration
                // You can't purchase a training program that exists within a bundle that you have an existing registration for

                if (isBundle && isFirstTrainingProgram)
                {

                    // check if a bundle exists for the user

                    // This is only done once, since we will be using the same bundle id for each training program
                    Data.SalesforceEntitlement sfEntitlement = Select.AllColumnsFrom<Data.SalesforceEntitlement>()
                        .Where(Data.SalesforceEntitlement.UserIdColumn).IsEqualTo(purchasingUserId)
                        .And(Data.SalesforceEntitlement.BundleIDColumn).IsEqualTo(tpBundle.Id)
                        .And(Data.SalesforceEntitlement.SalesforceStartDateColumn).IsLessThan(DateTime.Now)
                        .And(Data.SalesforceEntitlement.SalesforceEndDateColumn).IsGreaterThan(DateTime.Now)
                        .ExecuteSingle<Data.SalesforceEntitlement>(); // One entitlement will exist per tp in bundle
                    // but they will all be the same so just return the first
                    // (Same except for child TP - parent TP mapping)

                    if (sfEntitlement != null && sfEntitlement.Id > 0)
                    {
                        entitlement = ConvertSalesforceEntitlement(sfEntitlement, entitlement);
                        entitlement.ProductId = tpBundle.Sku;

                        entitlement.SetIssue("Error", "ALREADY_REGISTERED", "User already has access to the products requested.");
                        entitlement.Success = true;
                        return entitlement;
                    }
                }
                else if (!isBundle) // Only want to check enrolment if this is a single training program, otherwise we already checked on the
                // first training program of the bundle. 
                {
                    // Check if an entitlemnet exists for a user for the current training program. 
                    Data.SalesforceEntitlement sfEntitlement = Select.AllColumnsFrom<Data.SalesforceEntitlement>()
                        .Where(Data.SalesforceEntitlement.UserIdColumn).IsEqualTo(purchasingUserId)
                        .And(Data.SalesforceEntitlement.ParentTrainingProgramIDColumn).IsEqualTo(parentTrainingProgramId)
                        .And(Data.SalesforceEntitlement.SalesforceStartDateColumn).IsLessThan(DateTime.Now)
                        .And(Data.SalesforceEntitlement.SalesforceEndDateColumn).IsGreaterThan(DateTime.Now)
                        .ExecuteSingle<Data.SalesforceEntitlement>();

                    if (sfEntitlement != null && sfEntitlement.Id > 0)
                    {
                        entitlement = ConvertSalesforceEntitlement(sfEntitlement, entitlement);
                        entitlement.ProductId = parentTrainingProgram.Sku;

                        entitlement.TrainingProgram = new Data.TrainingProgram(sfEntitlement.TrainingProgramId);

                        entitlement.SetIssue("Error", "ALREADY_REGISTERED", "User already has access to the products requested.");
                        entitlement.Success = true;
                        return entitlement;
                    }

                }

                // No training program to merge into this means date, or partner is different so create a new copy
                // Or it's a bundle in which case we always create new copies. 
                Models.HierarchyLink link = new Models.HierarchyLink();
                link.HierarchyNodeID = purchasingUserId;
                link.HierarchyTypeID = (int)Symphony.Core.HierarchyType.User;

                parentTrainingProgram.SecondaryAssignment = new List<Models.HierarchyLink>();
                parentTrainingProgram.PrimaryAssignment = new List<Models.HierarchyLink>();

                parentTrainingProgram.PrimaryAssignment.Add(link);

                parentTrainingProgram.StartDate = salesforceEntitlement.SalesforceStartDate;
                parentTrainingProgram.EndDate = salesforceEntitlement.SalesforceEndDate;
                parentTrainingProgram.DueDate = salesforceEntitlement.SalesforceEndDate;

                parentTrainingProgram.DuplicateFromID = parentTrainingProgram.Id; // not saved on table, just for the SaveTrainingProgram function
                parentTrainingProgram.ParentTrainingProgramID = parentTrainingProgram.Id; // this will be saved to table.
                parentTrainingProgram.Id = 0;
                parentTrainingProgram.IsLive = true;

                parentTrainingProgram.PartnerID = entitlement.PartnerId;

                parentTrainingProgram.IsSalesforce = true;
                parentTrainingProgram.Name = parentTrainingProgram.Name;

                // Need to copy this training program to the passed in user's customer
                parentTrainingProgram.CustomerID = purchasingCustomerId;

                parentTrainingProgram.Sku = null;

                if (isBundle)
                {
                    parentTrainingProgram.PurchasedFromBundleId = tpBundle.Id;
                }

                // Saving the training program in the context of the passed in user
                bool isRollupCheck = true;
                if (entitlement.IsSuppressRollup.HasValue && entitlement.IsSuppressRollup.Value) {
                    isRollupCheck = false;
                }
                childTrainingProgram = courseAssignmentController.SaveTrainingProgram(purchasingCustomerId, purchasingUserId, 0, parentTrainingProgram, isRollupCheck).Data;
                if (childTrainingProgram != null)
                {
                    if (entitlement.SessionId.HasValue)
                    {
                        new TrainingProgramController().PostRegistration(purchasingUserId, purchasingCustomerId, entitlement.ProductId, new Registration()
                        {
                            ClassID = entitlement.SessionId.Value,
                            RegistrantID = purchasingUserId
                        });
                    }
                }

                salesforceEntitlement.UserId = purchasingUserId;
                salesforceEntitlement.ParentTrainingProgramID = parentTrainingProgramId;
                salesforceEntitlement.TrainingProgramId = childTrainingProgram.Id;

                if (isBundle)
                {
                    salesforceEntitlement.BundleID = tpBundle.Id;
                    salesforceEntitlement.BundleSku = tpBundle.Sku;
                }

                salesforceEntitlement.Save();

                // Going to set the entitlement info only once. 
                // So in the case of bundles:
                //     We will create multiple entitlements in our system. 
                //     We will only return one entitlement. 
                //     Each entitlement should have the same info, other than that we will get a different trainingprogram/parent training program map
                //     Salesforce doesn't care about this, so we can just return the first entitlement that we create.
                //     All the entitlements we create on our end will have the same SKU 
                //      So when salesforce asks about an entitlement, we will be able to check all of our mappings if needed.
                if (isFirstTrainingProgram)
                {
                    entitlement.EntitlementId = salesforceEntitlement.Id;
                    entitlement.EntitlementStartDate = salesforceEntitlement.SalesforceStartDate.ToString(SalesforceDateFormat);
                    entitlement.EntitlementEndDate = salesforceEntitlement.SalesforceEndDate.ToString(SalesforceDateFormat);
                    entitlement.TrainingProgram = new Data.TrainingProgram(childTrainingProgram.Id);
                    if (entitlement.SessionId.HasValue && entitlement.SessionId.Value > 0)
                    {
                        entitlement.Session = new Data.ClassX(entitlement.SessionId);
                    }
                    entitlement.Success = true;
                    

                    entitlement.ProductId = salesforceEntitlement.ProductId;

                    isFirstTrainingProgram = false;
                }

            }


            return entitlement;
        }

        public EntitlementList GetEntitlements(int userId)
        {
            return GetEntitlements(userId, 0);
        }

        public EntitlementList GetEntitlement(int userId, int entitlementId)
        {
            return GetEntitlements(userId, entitlementId);
        }

        public EntitlementList PostEntitlements(int userId, EntitlementList entitlements, bool isSuppressEmail = false)
        {
            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                Data.User user = new Data.User(userId);

                for (int i = 0; i < entitlements.Entitlements.Count; i++)
                {
                    entitlements.Entitlements[i] = SaveEntitlement(user, entitlements.Entitlements[i]);
                }

                // Strip out any entitlements that have the isSuppressEmail flag set - this will 
                // remove them from the email notification. If all have this flag set, no email will send.
                // Also exclude any failures to avoid having an empty training program reference in 
                // the notification. 
                //
                // Copying this out to a new EntitlementList so we still get the same return data that
                // we would had the emails been enabled. 
                EntitlementList entitlementsForEmail = new EntitlementList();
                entitlementsForEmail.Entitlements = new List<Entitlement>();
                foreach (Entitlement entitlement in entitlements.Entitlements)
                {
                    if (entitlement.Success && !entitlement.IsSuppressEmail)
                    {
                        entitlementsForEmail.Entitlements.Add(entitlement);
                    }
                }
                
                // Only send an email if there is at least one successful entitlement created
                if (!isSuppressEmail && entitlementsForEmail.Entitlements.Count > 0)
                {
                    try
                    {
                        SendNewEnrollmentEmail(user, entitlementsForEmail);
                    }
                    catch (Exception e)
                    {
                        log4net.LogManager.GetLogger(typeof(UserController)).Error(e);

                        for (int i = 0; i < entitlements.Entitlements.Count; i++)
                        {
                            entitlements.Entitlements[i].SetIssue("Warning", "EMAIL_FAILURE", "The user was not notified by email about this entitlement. Email error: " + e.Message);
                        }
                    }
                }

                ts.Complete();
            }

            return entitlements;
        }

        public EntitlementList DeleteEntitlements(int userId, EntitlementList entitlements)
        {
            PortalController portalController = new PortalController();
            CourseAssignmentController courseAssignementController = new CourseAssignmentController();

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                foreach (Entitlement entitlement in entitlements.Entitlements)
                {
                    Data.SalesforceEntitlement salesforceEntitlement = new Data.SalesforceEntitlement(entitlement.EntitlementId);

                    if (salesforceEntitlement == null || salesforceEntitlement.Id == 0)
                    {
                        entitlement.SetIssue("Error", "UNKOWN_ENTITLEMENT", "The entitlement with the id " + entitlement.EntitlementId + " cannot be found.");
                    }
                    else
                    {
                        if (salesforceEntitlement.BundleID.HasValue && salesforceEntitlement.BundleID.Value > 0)
                        {

                            List<int> trainingProgramIds = new Select(Data.SalesforceEntitlement.TrainingProgramIdColumn)
                                .From<Data.SalesforceEntitlement>()
                                .Where(Data.SalesforceEntitlement.BundleIDColumn).IsEqualTo(salesforceEntitlement.BundleID.Value)
                                .And(Data.SalesforceEntitlement.UserIdColumn).IsEqualTo(salesforceEntitlement.UserId)
                                .ExecuteTypedList<int>();

                            // Deleting entire bundle entitlement
                            // We do not have to handle merged cases, but we do have to delete each entitlement created
                            // for each training program
                            new Delete().From<Data.SalesforceEntitlement>()
                                .Where(Data.SalesforceEntitlement.BundleIDColumn).IsEqualTo(salesforceEntitlement.BundleID.Value)
                                .And(Data.SalesforceEntitlement.UserIdColumn).IsEqualTo(salesforceEntitlement.UserId)
                                .Execute();

                            // Now also delete the training programs involved
                            new Delete().From<Data.TrainingProgram>()
                                .Where(Data.TrainingProgram.IsSalesforceColumn).IsEqualTo(true)
                                .And(Data.TrainingProgram.PurchasedFromBundleIDColumn).IsEqualTo(salesforceEntitlement.BundleID)
                                .And(Data.TrainingProgram.IdColumn).In(trainingProgramIds)
                                .Execute();
                        }
                        else
                        {
                            // Deleting single training program entitlement
                            // In this case we have to handle merged training programs
                            Models.TrainingProgram trainingProgram = ExcludeCourses(userId, entitlement.EntitlementId);

                            if (trainingProgram.RequiredCourses.Count +
                                trainingProgram.ElectiveCourses.Count +
                                trainingProgram.FinalAssessments.Count +
                                trainingProgram.OptionalCourses.Count == 0)
                            {
                                // No courses left so get rid of the training program as well
                                new Delete().From<Data.TrainingProgram>()
                                    .Where(Data.TrainingProgram.IsSalesforceColumn).IsEqualTo(true)
                                    .And(Data.TrainingProgram.IdColumn).IsEqualTo(salesforceEntitlement.TrainingProgramId)
                                    .Execute();
                            }

                            Data.SalesforceEntitlement.Delete(salesforceEntitlement.Id);
                        }

                        entitlement.Success = true;
                    }
                }
                ts.Complete();
            }

            return entitlements;
        }
    }
}
