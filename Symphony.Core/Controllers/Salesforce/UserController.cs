﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Web;
using Symphony.Core.Controllers;
using Symphony.Core.Models.Salesforce;
using Symphony.Web;
using System.Web.Script.Serialization;
using SubSonic;
using System.Transactions;
using System.Web.Security;
using System.Web;

namespace Symphony.Core.Controllers.Salesforce
{

    public class UserController : SalesforceController
    {

        public User GetUser(int userId, int id)
        {
            CustomerController customerController = new CustomerController();

            var customerId = TargetCustomerID > 0 ? TargetCustomerID : CustomerID;

            Models.SingleResult<Models.User> userResult = customerController.GetUser(customerId, userId, id);

            User salesforceUser = GetSalesforceUser(userResult.Data);

            return salesforceUser;
        }

        public User PostUser(int userId, User user)
        {
            bool isAuthRequired = user.IsAuthRequired;
            CustomerController customerController = new CustomerController();

            Models.User symphonyUser = GetSymphonyUser(user);

            if (symphonyUser.Username == null)
            {
                symphonyUser.Username = symphonyUser.Email;
            }

            symphonyUser.EmployeeNumber = symphonyUser.SalesforceContactId;
            symphonyUser.MiddleName = " ";
            symphonyUser.Notes += "Created from Salesforce API\n";
            symphonyUser.Password = !string.IsNullOrWhiteSpace(user.Password) ? user.Password : Membership.GeneratePassword(8, 1) + new Random().Next(0, 9);
            if (user.ResetPasswordRequired != null)
            {
                if (user.ResetPasswordRequired.Value)
                {
                    symphonyUser.IsPasswordResetRequired = true;
                    symphonyUser.LastEnforcedPasswordReset = new DateTime(1975, 1, 1);
                }
            }
            symphonyUser.StatusID = (int)UserStatusType.Active;
            symphonyUser.IsCreatedBySalesforce = true;
            if (!string.IsNullOrEmpty(user.JanrainUserUuid))
            {
                symphonyUser.JanrainUserUuid = user.JanrainUserUuid;
            }

            Data.UserCollection exists = new Data.UserCollection()
                    .Where(Data.User.Columns.EmployeeNumber, symphonyUser.EmployeeNumber)
                    .Where(Data.User.Columns.CustomerID, TargetCustomerID)
                    .Load();

            if (exists.Count > 0)
            {

                Data.User userData = exists[0];

                symphonyUser.CopyFrom(userData);

                User sfUser = GetSalesforceUser(symphonyUser);
                sfUser.SetIssue("Warning", "EXISTS", "User already exists with Employee Number: " + symphonyUser.EmployeeNumber);
                sfUser.Success = true;

                return sfUser;
            }

            Data.UserCollection exists2 = new Data.UserCollection()
                .Where(Data.User.Columns.Username, symphonyUser.Username)
                .Where(Data.User.Columns.CustomerID, TargetCustomerID)
                .Load();

            if (exists2.Count > 0)
            {
                Data.User userData = exists2[0];

                symphonyUser.CopyFrom(userData);

                User sfUser = GetSalesforceUser(symphonyUser);
                sfUser.SetIssue("Warning", "EXISTS", "User already exists with email/username: " + symphonyUser.Username);
                sfUser.Success = true;

                return sfUser;
            }

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                Models.SingleResult<Models.User> userResult = customerController.SaveUser(TargetCustomerID, userId, 0, symphonyUser);

                Data.JobRole defaultRole = Select.AllColumnsFrom<Data.JobRole>()
                        .Where(Data.JobRole.CustomerIDColumn).IsEqualTo(TargetCustomerID)
                        .And(Data.JobRole.InternalCodeColumn).IsEqualTo("default")
                        .ExecuteSingle<Data.JobRole>();

                Data.Location defaultLocation = Select.AllColumnsFrom<Data.Location>()
                    .Where(Data.Location.CustomerIDColumn).IsEqualTo(TargetCustomerID)
                    .And(Data.Location.InternalCodeColumn).IsEqualTo("default")
                    .ExecuteSingle<Data.Location>();


                if (defaultRole == null || defaultRole.Id == 0)
                {
                    defaultRole = new Data.JobRole()
                    {
                        Name = "Default",
                        InternalCode = "default",
                        CustomerID = TargetCustomerID
                    };

                    defaultRole.Save();
                }

                if (defaultLocation == null || defaultLocation.Id == 0)
                {
                    defaultLocation = new Data.Location()
                    {
                        Name = "Default",
                        InternalCode = "default",
                        CustomerID = TargetCustomerID
                    };

                    defaultLocation.Save();
                }

                CustomerController c = new CustomerController();
                c.SaveLocationUsers(TargetCustomerID, userResult.Data.ID, defaultLocation.Id, new int[] { userResult.Data.ID });
                c.SaveJobRoleUsers(TargetCustomerID, userResult.Data.ID, defaultRole.Id, new int[] { userResult.Data.ID });

                Data.User userData = new Data.User();
                symphonyUser.CopyTo(userData);

                user = GetSalesforceUser(userResult.Data);

                user.IsEnabled = true;
                user.Success = true;

                try
                {
                    SendNewUserEmail(userData);
                }
                catch (Exception e)
                {
                    log4net.LogManager.GetLogger(typeof(UserController)).Error(e);
                    user.SetIssue("Warning", "EMAIL_ERROR", "Could not send an email to the user. The user has still been created. Email error: " + e.Message);
                }
                ts.Complete();
            }

            if (isAuthRequired)
            {
                Controllers.UserController userController = new Controllers.UserController();
                Data.Customer customer = new Data.Customer(symphonyUser.CustomerID);
                Models.AuthenticationResult result = userController.Login(customer.SubDomain, symphonyUser.Username, true);
                string authToken = userController.GetEncryptedTicket(result.UserData);
                user.Auth = authToken;
            }

            return user;
        }

        public User PutUser(int userId, int id, User user)
        {
            CustomerController customerController = new CustomerController();

            Models.User symphonyUser = GetSymphonyUser(user);
            Models.User updatedUser = customerController.GetUser(TargetCustomerID, userId, id).Data;

            //SystemLoginController.SetNewPassword(user.Email, user.Password);

            /*if (!updatedUser.IsCreatedBySalesforce)
            {
                throw new Exception("Cannot edit users that were not created by Salesforce.");
            }*/
            updatedUser = ApplyNewUserData(symphonyUser, updatedUser);

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                Models.SingleResult<Models.User> userResult = customerController.SaveUser(TargetCustomerID, userId, id, updatedUser);

                user = GetSalesforceUser(userResult.Data);

                user.IsEnabled = true;
                user.Success = true;

                ts.Complete();
            }

            return user;
        }

        public User DeleteUser(int userId, int id)
        {
            CustomerController customerController = new CustomerController();

            Models.SingleResult<Models.User> userResult = customerController.GetUser(TargetCustomerID, userId, id);
            /*if (!userResult.Data.IsCreatedBySalesforce)
            {
                throw new Exception("Cannot edit users that were not created by Salesforce.");
            }*/
            User salesforceUser = GetSalesforceUser(userResult.Data);

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                customerController.DeleteUser(TargetCustomerID, userId, id);

                salesforceUser.Success = true;
                salesforceUser.IsEnabled = false;

                ts.Complete();
            }
            return salesforceUser;
        }

        private Models.User GetSymphonyUser(User salesforceUser)
        {
            Models.User symphonyUser = new Models.User();

            symphonyUser.ID = salesforceUser.UserID;

            salesforceUser.CopyTo(symphonyUser);
            if (salesforceUser.Phone != null) symphonyUser.TelephoneNumber = salesforceUser.Phone;

            if (salesforceUser.Addresses != null && salesforceUser.Addresses.Count > 0)
            {
                symphonyUser.SalesforceAddress = new JavaScriptSerializer().Serialize(salesforceUser.Addresses);
            }

            return symphonyUser;
        }
        private User GetSalesforceUser(Models.User symphonyUser)
        {
            User salesforceUser = new User();

            symphonyUser.CopyTo(salesforceUser);
            salesforceUser.UserID = symphonyUser.ID;
            salesforceUser.Phone = symphonyUser.TelephoneNumber;

            if (symphonyUser.SalesforceAddress != null)
            {
                salesforceUser.Addresses = new JavaScriptSerializer().Deserialize<List<Address>>(symphonyUser.SalesforceAddress);
            }
            return salesforceUser;
        }

        private Models.User ApplyNewUserData(Models.User newUser, Models.User currentUser)
        {
            if (newUser.JanrainUserUuid != null) currentUser.JanrainUserUuid = newUser.JanrainUserUuid;
            if (newUser.Username != null) currentUser.Username = newUser.Username;
            if (newUser.FirstName != null) currentUser.FirstName = newUser.FirstName;
            if (newUser.LastName != null) currentUser.LastName = newUser.LastName;
            if (newUser.SalesforceAddress != null) currentUser.SalesforceAddress = newUser.SalesforceAddress;
            if (newUser.TelephoneNumber != null) currentUser.TelephoneNumber = newUser.TelephoneNumber;
            if (newUser.Mobile != null) currentUser.Mobile = newUser.Mobile;
            if (newUser.SalesforceContactId != null) currentUser.SalesforceContactId = newUser.SalesforceContactId;
            if (newUser.SalesforceAccountName != null) currentUser.SalesforceAccountName = newUser.SalesforceAccountName;
            if (newUser.SalesforceAccountId != null) currentUser.SalesforceAccountId = newUser.SalesforceAccountId;
            if (!string.IsNullOrWhiteSpace(newUser.Password)) currentUser.Password = newUser.Password;
            if (!string.IsNullOrWhiteSpace(newUser.NmlsNumber)) currentUser.NmlsNumber = newUser.NmlsNumber;
            return currentUser;
        }



        public List<User> FindUsers(string search, string orderBy, OrderDirection orderDirEnum, int pageIndex, int pageSize)
        {

            CustomerController customerController = new CustomerController();

            var customerId = TargetCustomerID > 0 ? TargetCustomerID : CustomerID;
            var userId = customerController.UserID;

            var userResults = customerController.FindUsers(null, customerId, userId, search, orderBy, orderDirEnum, pageIndex, pageSize, null, 0, 0);


            List<User> salesforceUsers = new List<User>();
            if (userResults.Success)
            {
                foreach (var userResult in userResults.Data)
                {
                    User salesforceUser = GetSalesforceUser(userResult);
                    salesforceUsers.Add(salesforceUser);
                }
            }
            else
            {
                throw new WebFaultException<string>(userResults.Error, System.Net.HttpStatusCode.BadRequest);
            }
            return salesforceUsers;

        }
    }
}
