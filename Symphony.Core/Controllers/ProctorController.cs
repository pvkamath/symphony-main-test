﻿using System;
using Symphony.Core.Models;
using SubSonic;
using log4net;
using System.Diagnostics;
using ProctorFormData = Symphony.Core.Data.ProctorForm;
using ProctorFormResponseData = Symphony.Core.Data.ProctorFormResponse;

namespace Symphony.Core.Controllers
{
    /// <summary>
    /// ProctorController is a domain service that performs various CRUD operations on entities
    /// relating to the proctoring module.
    /// </summary>
    public class ProctorController : SymphonyController
    {
        static ILog Log = LogManager.GetLogger(typeof(ProctorController));

        /// <summary>
        /// Finds the ProctorForm with an id equal to the id parameter.
        /// </summary>
        /// <param name="id">Id of the ProctorForm to return.</param>
        /// <returns>ProctorForm that matches the provided id, wrapped in a SingleResult object. If no
        /// matching entity is found, then null will be returned, also wrapped in a SingleResult
        /// object.</returns>
        public SingleResult<ProctorForm> FindProctorForm(int id)
        {
            SqlQuery query = null;

            try
            {
                Log.InfoFormat("Performing operation to find a proctor form with id {0}.", id);

                query = Select.AllColumnsFrom<ProctorFormData>()
                    .Where(ProctorFormData.Columns.Id).IsEqualTo(id)
                    .AndExpression(ProctorFormData.Columns.IsDeleted).IsEqualTo(false);

                var proctorForm = query.ExecuteSingle<ProctorForm>();
                if (proctorForm == null)
                {
                    Log.DebugFormat("Operation to find a proctor form returned null.");
                }

                return new SingleResult<ProctorForm>(proctorForm);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to find a proctor form: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to find a proctor form.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }
        
        /// <summary>
        /// Queries the database for the set of ProctorForms that match the specified query
        /// parameters.
        /// </summary>
        /// <param name="queryParams">Query parameter object defining what to query.</param>
        /// <returns>The set of ProctorForms that match the query parameters, wrapped in a
        /// PagedResult object. If no matching entities are found, then an empty array will be
        /// returned, also wrapped in a PagedResult object.</returns>
        public PagedResult<ProctorForm> QueryProctorForms(PagedQueryParams<ProctorForm> queryParams)
        {
            SqlQuery query = null;

            try
            {
                Log.Info("Performing operation to query proctor forms.");

                query = Select.AllColumnsFrom<ProctorFormData>()
                    .Where(ProctorFormData.Columns.IsDeleted).IsEqualTo(false);

                if (!string.IsNullOrWhiteSpace(queryParams.SearchText))
                {
                    query.AndExpression(ProctorFormData.Columns.Name).ContainsString(queryParams.SearchText)
                        .OrExpression(ProctorFormData.Columns.Description).ContainsString(queryParams.SearchText);
                }

                Log.DebugFormat("queryParams: {0}", queryParams.ToString());

                return new PagedResult<ProctorForm>(query, queryParams.PageIndex, queryParams.PageSize, queryParams.OrderBy, queryParams.OrderDirection);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query proctor forms: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query proctor forms.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Creates a new ProctorForm with the provided model data.
        /// </summary>
        /// <param name="model">Model data to use when creating the ProctorForm. The model should
        /// not have a positive integer value for its id parameter.</param>
        /// <returns>The newly created ProctorForm model. The data will be identical to what was
        /// posted, except that it will have a new database id.</returns>
        public SingleResult<ProctorForm> CreateProctorForm(ProctorForm model)
        {
            try
            {
                Log.Info("Performing operation to create a proctor form.");

                if (model.ID > 0)
                {
                    throw new ArgumentException("Cannot create proctor form when model data already has an associated id.");
                }

                var data = new ProctorFormData();
                model.CopyTo(data);

                data.Save();

                Log.DebugFormat("New proctor form created with id {0}.", data.Id);
                model.ID = data.Id;

                return new SingleResult<ProctorForm>(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to create a proctor form.", ex);
#if DEBUG
                Debugger.Break();
#endif
                return new SingleResult<ProctorForm>(model);
            }
        }

        /// <summary>
        /// Updates an existing ProctorForm with the provided model data.
        /// </summary>
        /// <param name="model">Model data to update the ProctorForm with. This data should already
        /// have an id associated with it.</param>
        /// <returns>The newly created ProctorForm model. The data will be identical to what was
        /// posted.</returns>
        public SingleResult<ProctorForm> UpdateProctorForm(ProctorForm model)
        {
            try
            {
                Log.InfoFormat("Performing operation to update a proctor form with id {0}", model.ID);

                if (model.ID == 0)
                {
                    throw new ArgumentException("Cannot update a proctor form when model data does not contain an id.");
                }

                var data = new ProctorFormData(model.ID);
                model.CopyTo(data);

                data.Save();
                Log.InfoFormat("Updated proctor form with id {0}.", data.Id);

                return new SingleResult<ProctorForm>(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to update a proctor form.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Deletes an existing ProctorForm. Note that the delete is a soft delete operation. Users
        /// who are already using an ProctorForm can continue to do so, but the ProctorForm will no
        /// longer show up when queried.
        /// </summary>
        /// <param name="model">Model data representing the ProctorForm to delete.</param>
        /// <returns>The deleted ProctorForm model.</returns>
        public SingleResult<ProctorForm> DeleteProctorForm(ProctorForm model)
        {
            try
            {
                Log.InfoFormat("Performing operation to soft delete a proctor form with id {0}", model.ID);

                if (model.ID == 0)
                {
                    throw new ArgumentException("Cannot delete a proctor form when model data does not contain an id.");
                }

                var data = new ProctorFormData(model.ID);
                model.IsDeleted = true;
                data.IsDeleted = true;

                data.Save();
                Log.InfoFormat("Soft deleted proctor form with id {0}.", data.Id);

                return new SingleResult<ProctorForm>(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to delete a proctor form.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Saves a proctors information..
        /// </summary>
        /// <param name="response">The proctor form response to save.</param>
        /// <returns>A result indicating success or failure.</returns>
        public SimpleSingleResult<bool> SaveProctor(ProctorFormResponse response)
        {
            try
            {
                Log.Info("Performing operation to save a proctor form response.");

                var data = new ProctorFormResponseData();
                response.CopyTo(data);

                data.Save(UserID);

                return new SimpleSingleResult<bool>(true);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to delete a proctor form response.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }
    }
}
