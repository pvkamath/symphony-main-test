﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using Symphony.Web;
using SubSonic;
using Symphony.Core.Comparers;
using System.Transactions;
using log4net;

namespace Symphony.Core.Controllers
{

    public class MessageBoardController : Controllers.SymphonyController
    {

        private ILog Log = LogManager.GetLogger(typeof(MessageBoardController));

        /// <summary>
        /// These roles have full admin access to all message boards
        /// </summary>
        /// <returns></returns>
        private bool canUpdateMessageBoards()
        {
            return (HasRole(Roles.ClassroomManager) ||
                    HasRole(Roles.CourseAssignmentAdministrator) ||
                    HasRole(Roles.CourseAssignmentTrainingManager));
        }
        /// <summary>
        /// You can view the message board if you are enrolled, or if you have full access
        /// isEnrolled does not care if the user is enrolled as a student, instructor or a leader
        /// </summary>
        /// <param name="messageBoard"></param>
        /// <returns></returns>
        private bool canViewMessageBoard(Data.MessageBoard messageBoard)
        {
            return isEnrolled(messageBoard) || canUpdateMessageBoards();
        }
        /// <summary>
        /// You can view the topic, if you can view the message board
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        private bool canViewTopic(Data.MessageBoardTopic topic)
        {
            Data.MessageBoard messageBoard = new Data.MessageBoard(topic.MessageBoardID);
            return canViewMessageBoard(messageBoard);
        }
        /// <summary>
        /// is the user linked to the course as an instructor directly as a classroom instructor, or as a training program leader
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="messageBoard"></param>
        /// <returns></returns>
        private bool isInstructor(int userId, Data.MessageBoard messageBoard)
        {
            switch (messageBoard.MessageBoardType)
            {
                case (int)MessageBoardType.Classroom:
                    // Is the current user directly linked to this class as an instructor?
                    UpcomingEvent instructorEvent = Select.AllColumnsFrom<Data.SimpleUpcomingEvent>()
                            .Where(Data.SimpleUpcomingEvent.Columns.EventID).IsEqualTo(messageBoard.ClassId)
                            .And(Data.SimpleUpcomingEvent.Columns.EventTypeID).IsEqualTo((int)EventType.InstructedClass)
                            .And(Data.SimpleUpcomingEvent.Columns.UserID).IsEqualTo(userId)
                            .ExecuteSingle<UpcomingEvent>();

                    if (instructorEvent != null)
                    {
                        return true;
                    }

                    // Is this class linked to a course that is part of a training program that the current user is a training program leader for?
                    Data.ClassX classX = Select.AllColumnsFrom<Data.ClassX>()
                        .InnerJoin(Data.Course.IdColumn, Data.ClassX.CourseIDColumn)
                        .InnerJoin(Data.TrainingProgramToCourseLink.CourseIDColumn, Data.Course.IdColumn)
                        .InnerJoin(Data.TrainingProgramLeader.TrainingProgramIDColumn, Data.TrainingProgramToCourseLink.TrainingProgramIDColumn)
                        .Where(Data.TrainingProgramLeader.UserIDColumn).IsEqualTo(userId)
                        .And(Data.ClassX.IdColumn).IsEqualTo(messageBoard.ClassId)
                        .ExecuteSingle<Data.ClassX>();

                    if (classX != null)
                    {
                        return true;
                    }

                    break;
                case (int)MessageBoardType.Online:
                    // Is this online course linked to a training program that the current user is a leader for?
                    Data.OnlineCourse course = Select.AllColumnsFrom<Data.OnlineCourse>()
                        .InnerJoin(Data.TrainingProgramToCourseLink.CourseIDColumn, Data.OnlineCourse.IdColumn)
                        .InnerJoin(Data.TrainingProgramLeader.TrainingProgramIDColumn, Data.TrainingProgramToCourseLink.TrainingProgramIDColumn)
                        .Where(Data.TrainingProgramLeader.UserIDColumn).IsEqualTo(UserID)
                        .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo((int)CourseType.Online)
                        .And(Data.OnlineCourse.IdColumn).IsEqualTo(messageBoard.OnlineCourseId)
                        .ExecuteSingle<Data.OnlineCourse>();

                    if (course != null)
                    {
                        return true;
                    }

                    break;
                case (int)MessageBoardType.TrainingProgram:
                    // Is the current user a leader for this training program?
                    TrainingProgram trainingProgram = Select.AllColumnsFrom<Data.TrainingProgram>()
                        .InnerJoin(Data.TrainingProgramLeader.TrainingProgramIDColumn, Data.TrainingProgram.IdColumn)
                        .Where(Data.TrainingProgram.IdColumn).IsEqualTo(messageBoard.TrainingProgramId)
                        .And(Data.TrainingProgramLeader.UserIDColumn).IsEqualTo(userId)
                        .ExecuteSingle<TrainingProgram>();

                    if (trainingProgram != null)
                    {
                        return true;
                    }

                    break;
            }

            // If we are here, then the user is not an instructor of the course

            return false;
        }
        /// <summary>
        /// To update a topic:
        ///     It must be yours OR
        ///     It must be a new topic, AND you must be enrolled in the course/tp linked to the message board OR
        ///     You are linked to the message board as an instructor OR
        ///     You have full admin access to the message boards
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="messageBoard"></param>
        /// <returns></returns>
        private bool canUpdateTopic(Data.MessageBoardTopic topic, Data.MessageBoard messageBoard)
        {
            bool enrolled = isEnrolled(messageBoard);

            return UserID == topic.UserID ||
                    (topic.Id == 0 && enrolled) ||
                    isInstructor(UserID, messageBoard) ||
                    canUpdateMessageBoards();
        }
        /// <summary>
        /// You can sticky if you are the instructor of the course linked to the message board,
        /// or you have full admin access to the message board
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        private bool canSticky(Data.MessageBoardTopic topic)
        {
            Data.MessageBoard messageBoard = new Data.MessageBoard(topic.MessageBoardID);
            return isInstructor(UserID, messageBoard) ||
                    canUpdateMessageBoards();
        }
        /// <summary>
        /// You can update a post if:
        ///     It is yours OR
        ///     It is a new post and you are enrolled in the course/tp linked to the message board OR
        ///     You are an instructor or leader of the course/tp linked to the message board OR
        ///     You have full admin rights to the message board
        /// </summary>
        /// <param name="post"></param>
        /// <param name="messageBoard"></param>
        /// <returns></returns>
        private bool canUpdatePost(Data.MessageBoardPost post, Data.MessageBoard messageBoard)
        {
            bool enrolled = isEnrolled(messageBoard);

            return UserID == post.UserID ||
                    (post.Id == 0 && enrolled) ||
                    isInstructor(UserID, messageBoard) ||
                    canUpdateMessageBoards();
        }
        /// <summary>
        /// You are enrolled in the message board if you have a registration for the course linked to the message board
        /// or if you an instructor for the course linked to the message board
        /// or you are a leader for the training progam linked to the message board (either directly, or indirectly)
        /// </summary>
        /// <param name="messageBoard"></param>
        /// <returns></returns>
        private bool isEnrolled(Data.MessageBoard messageBoard)
        {
            switch (messageBoard.MessageBoardType)
            {
                case (int)MessageBoardType.Classroom:
                    Dictionary<long, UpcomingEvent> courseDictionary = getCourses();
                    if (courseDictionary.ContainsKey((long)messageBoard.ClassId)) {
                        return true;
                    }
                    break;
                case (int)MessageBoardType.Online:
                    Dictionary<int, Course> onlineCourseDictionary = getOnlineCourses();
                    if (onlineCourseDictionary.ContainsKey((int)messageBoard.OnlineCourseId))
                    {
                        return true;
                    }
                    break;
                case (int)MessageBoardType.TrainingProgram:
                    Dictionary<int, TrainingProgram> trainingProgramDictionary = getTrainingPrograms();
                    // Get a list of the parent ids so we can see if the message board
                    // points to any of the parent ids. It can be assumed that if the
                    // user is enrolled in the child training program they have access
                    // to the parent training program message board. (This is how 
                    // salesforce will access the message boards)
                    Dictionary<int, int> parentIds = trainingProgramDictionary
                        .ToList()
                        .Select(e => e.Value.ParentTrainingProgramID)
                        .Distinct()
                        .ToDictionary(pid => pid);

                    if (trainingProgramDictionary.ContainsKey((int)messageBoard.TrainingProgramId) ||
                        parentIds.ContainsKey((int)messageBoard.TrainingProgramId))
                    {
                        return true;
                    }
                    break;
            }
            return false;
        }
        /// <summary>
        /// Gets all online courses the current user has access to (either directly, or by training program registration)
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, Course> getOnlineCourses()
        {

            // All the public courses
            List<Course> publicCourses = Select.AllColumnsFrom<Data.SimplePublicCourse>()
                .Where(Data.SimplePublicCourse.Columns.CustomerID).IsEqualTo(CustomerID)
                .And(Data.SimplePublicCourse.Columns.IsThirdParty).IsEqualTo(false)
                .And(Data.SimplePublicCourse.Columns.CourseTypeID).IsEqualTo((int)CourseType.Online)
                .ExecuteTypedList<Course>();
             
            // Training program ids the user is registered in
            List<int> trainingProgramIds =
                    Data.SPs.GetTrainingProgramsForUser(UserID, UserIsNewHire, string.Empty, string.Empty, string.Empty, 0, int.MaxValue, null, null)
                    .ExecuteTypedList<TrainingProgram>()
                    .Select(tpl => tpl.Id)
                    .ToList<int>();

            if (trainingProgramIds.Count == 0)
            {
                trainingProgramIds.Add(-1);
            }

            // Online courses in training programs the user leads
            // OR online courses in training programs the user is registered in

            List<Course> onlineCoursesForUser = 
                Select.AllColumnsFrom<Data.OnlineCourse>()
                .InnerJoin(Data.TrainingProgramToCourseLink.CourseIDColumn, Data.OnlineCourse.IdColumn)
                .LeftOuterJoin(Data.TrainingProgramLeader.TrainingProgramIDColumn, Data.TrainingProgramToCourseLink.TrainingProgramIDColumn)
                .Where(Data.TrainingProgramLeader.UserIDColumn).IsEqualTo(UserID)
                .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo((int)CourseType.Online)
                .Or(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn).In(trainingProgramIds)
                .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo((int)CourseType.Online)
                .ExecuteTypedList<Course>();

            List<Course> onlineCourses = publicCourses.Concat(onlineCoursesForUser).Distinct(new CourseComparer()).ToList<Course>();

            return onlineCourses.ToDictionary(e => e.Id);
        }
        /// <summary>
        /// Gets all training programs the current user is registered in
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, TrainingProgram> getTrainingPrograms()
        {
            List<TrainingProgram> programsUserEnrolled =
                    Data.SPs.GetTrainingProgramsForUser(UserID, UserIsNewHire, string.Empty, string.Empty, string.Empty, 0, int.MaxValue, null, null)
                    .ExecuteTypedList<TrainingProgram>();

            List<TrainingProgram> programsUserLeads =
                    Select.AllColumnsFrom<Data.TrainingProgram>()
                    .InnerJoin(Data.TrainingProgramLeader.TrainingProgramIDColumn, Data.TrainingProgram.IdColumn)
                    .Where(Data.TrainingProgramLeader.UserIDColumn).IsEqualTo(UserID)
                    .ExecuteTypedList<TrainingProgram>();

            List<TrainingProgram> programs = programsUserEnrolled.Concat(programsUserLeads).Distinct(new TrainingProgramComparer()).ToList<TrainingProgram>();

            return programs.ToDictionary(e => e.Id);
        }
        /// <summary>
        /// Gets all courses the user has access to directly, or by training program registration
        /// </summary>
        /// <returns></returns>
        private Dictionary<long, UpcomingEvent> getCourses()
        {
            // The events the user is registered in
            List<UpcomingEvent> registeredEvents = (new PortalController()).FindAllEventsForUser(UserID);
            
            // List of events linked to the user because they are a training program leader
            List<UpcomingEvent> eventsUserLeads = Select.AllColumnsFrom<Data.SimpleUpcomingEvent>()
                    .InnerJoin(Data.Tables.ClassX, Data.ClassX.IdColumn.ColumnName, Data.Views.SimpleUpcomingEvent, Data.SimpleUpcomingEvent.Columns.EventID)
                    .InnerJoin(Data.TrainingProgramToCourseLink.CourseIDColumn, Data.ClassX.CourseIDColumn)
                    .InnerJoin(Data.TrainingProgramLeader.TrainingProgramIDColumn, Data.TrainingProgramToCourseLink.TrainingProgramIDColumn)
                    .Where(Data.SimpleUpcomingEvent.Columns.EventTypeID).In(new int[] { (int)EventType.InstructedClass, (int)EventType.PublicClass, (int)EventType.RegisteredClass, (int)EventType.TrainingProgramClass })
                    .And(Data.TrainingProgramToCourseLink.CourseTypeIDColumn).IsEqualTo((int)CourseType.Classroom)
                    .ExecuteTypedList<UpcomingEvent>();

            // Combine registered and the events the user leads, make them unique based on eventID, strip out any meetings, send them to a dictionary
            Dictionary<long, UpcomingEvent> eventDictionary = registeredEvents
                                                .Concat(eventsUserLeads)
                                                .Distinct(new UpcomingEventComparer())
                                                .Where<UpcomingEvent>(e => e.EventTypeID == (int)EventType.RegisteredClass ||
                                                                           e.EventTypeID == (int)EventType.InstructedClass ||
                                                                           e.EventTypeID == (int)EventType.PublicClass ||
                                                                           e.EventTypeID == (int)EventType.TrainingProgramClass)
                                                .ToDictionary(e => e.EventID);

            return eventDictionary;
        }

        private void UnrollPosts(List<MessageBoardPost> posts, MessageBoardPost post) {
            foreach (MessageBoardPost p in post.Replies)
            {
                posts.Add(p);
                if (p.Replies != null)
                {
                    UnrollPosts(posts, p);
                }
            }
        }

        private bool MessageBoardExists(TableSchema.TableColumn column, int id)
        {
            int existing = new Select(column)
                           .From<Data.MessageBoard>()
                           .Where(column).IsEqualTo(id)
                           .ExecuteScalar<int>();

            return existing > 0;
        }

        public PagedResult<MessageBoard> GetMessageBoards(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            Data.User user = new Data.User(userId);

            string customerSubdomain = new Data.Customer(customerId).SubDomain;

            bool isManager = canUpdateMessageBoards();

            string sql = string.Format(@"
                SELECT [dbo].[MessageBoard].[ID],
                    [dbo].[MessageBoard].[Name],
                    [dbo].[MessageBoard].[CreatedOn],
                    [dbo].[MessageBoard].[ModifiedOn],
                    [dbo].[MessageBoard].[CreatedBy],
                    [dbo].[MessageBoard].[ModifiedBy],
                    [dbo].[MessageBoard].[MessageBoardType],
                    [dbo].[MessageBoard].[TrainingProgramId],
                    [dbo].[MessageBoard].[ClassId],
                    [dbo].[MessageBoard].[OnlineCourseId],
                    COALESCE([dbo].[TrainingProgram].[Name], [dbo].[Class].[Name], [dbo].[OnlineCourse].[Name]) as 'CourseName',
                    (SELECT
                        COUNT([dbo].[MessageBoardPost].[ID])
                        FROM [dbo].[MessageBoardTopic]
                        INNER JOIN [dbo].[MessageBoardPost] on [dbo].[MessageBoardPost].[TopicID] = [dbo].[MessageBoardTopic].[ID]
                        WHERE [dbo].[MessageBoardTopic].[MessageBoardID] = [dbo].[MessageBoard].[ID]
                        AND [dbo].[MessageBoardPost].[IsDeleted] != 1
                        AND [dbo].[MessageBoardTopic].[IsDeleted] != 1
                    ) as 'Posts',
                    (SELECT
                        MAX([dbo].[MessageBoardPost].[CreatedOn])
                        FROM [dbo].[MessageBoardTopic]
                        INNER JOIN [dbo].[MessageBoardPost] on [dbo].[MessageBoardPost].[TopicID] = [dbo].[MessageBoardTopic].[ID]
                        WHERE [dbo].[MessageBoardTopic].[MessageBoardID] = [dbo].[MessageBoard].[ID]
                        AND [dbo].[MessageBoardPost].[IsDeleted] != 1
                        AND [dbo].[MessageBoardTopic].[IsDeleted] != 1
                    ) as 'LastUpdated',
                    (SELECT TOP 1 
                        [dbo].[MessageBoardPost].CreatedBy
                        FROM [dbo].[MessageBoardTopic]
                        INNER JOIN [dbo].[MessageBoardPost] on [dbo].[MessageBoardPost].[TopicID] = [dbo].[MessageBoardTopic].[ID]
                        WHERE [dbo].[MessageBoardTopic].[MessageBoardID] = [dbo].[MessageBoard].[ID]
                        AND [dbo].[MessageBoardPost].[IsDeleted] != 1
                        AND [dbo].[MessageBoardTopic].[IsDeleted] != 1
                        ORDER BY [dbo].[MessageBoardPost].[CreatedOn] DESC
                    ) as 'LastPostBy',
				    CAST(CASE WHEN (SELECT TOP 1 e.[UserID]
                        FROM [dbo].[SimpleUpcomingEvents] e
                        WHERE e.[MessageBoardID] = [dbo].[MessageBoard].[ID]
                        AND e.[UserID] = {0}
                        AND e.[EventTypeID] = {1}
                    ) = {0} THEN 1 ELSE 0 END as bit) as IsInstructor
                    FROM [dbo].[MessageBoard]
                    LEFT OUTER JOIN [dbo].[TrainingProgram] on [dbo].[TrainingProgram].[ID] = [dbo].[MessageBoard].[TrainingProgramId]
                    LEFT OUTER JOIN [dbo].[Class] on [dbo].[Class].[ID] = [dbo].[MessageBoard].[ClassId]
                    LEFT OUTER JOIN [dbo].[OnlineCourse] on [dbo].[OnlineCourse].[ID] = [dbo].[MessageBoard].[OnlineCourseId]
            ", UserID, (int)EventType.InstructedClass);

            // Get training programs this user is registered in as a dictionary
            Dictionary<int, TrainingProgram> programDictionary = getTrainingPrograms();

            // Find registered courses for this user as dictionary
            Dictionary<long, UpcomingEvent> eventDictionary = getCourses();

            // Get public online courses, or online courses part of a training program the user is part of
            Dictionary<int, Course> coursesDictionary = getOnlineCourses();

            List<MessageBoard> messageBoards = new InlineQuery().ExecuteTypedList<MessageBoard>(sql);

            // Foreach message board, if it is linked to a training program, indicate if the current user is enrolled
            for (int i = messageBoards.Count - 1; i >= 0; i--)
            {
                MessageBoard messageBoard = messageBoards[i];

                messageBoard.Enrolled = false;

                switch (messageBoard.MessageBoardType)
                {
                    case MessageBoardType.Classroom:
                        // All registered classes will be found in the events dictionary
                        if (eventDictionary.ContainsKey((long)messageBoard.ClassId))
                        {
                            messageBoard.Enrolled = true;
                        }
                        break;
                    case MessageBoardType.Online:
                        // Online courses will be found in the public courses dictionary
                        if (coursesDictionary.ContainsKey((int)messageBoard.OnlineCourseId))
                        {
                            messageBoard.Enrolled = true;
                        }
                        break;
                    case MessageBoardType.TrainingProgram:
                        // Training programs from the tp dictionary
                        if (programDictionary.ContainsKey((int)messageBoard.TrainingProgramId)) {
                            messageBoard.Enrolled = true;
                        }
                        break;
                }

                if (!isManager && !messageBoard.Enrolled)
                {
                    messageBoards.RemoveAt(i);
                }
            }

            return new PagedResult<MessageBoard>(messageBoards.AsQueryable<MessageBoard>(), pageIndex, pageSize, orderBy, orderDir);
        }

        

        public SingleResult<MessageBoard> UpdateMessageBoard(int customerId, int userId, int messageBoardId, MessageBoard messageBoard)
        {

            Data.MessageBoard msgBoard = new Data.MessageBoard(messageBoardId);
            Data.CustomerMessageBoardLink msBoardCustomerLink = new Data.CustomerMessageBoardLink(Data.CustomerMessageBoardLink.Columns.MessageBoardId, messageBoardId);

            messageBoard.CopyTo(msgBoard);
            msgBoard.MessageBoardType = (int)messageBoard.MessageBoardType;

            if (!canUpdateMessageBoards() && !isInstructor(userId, msgBoard))
            {
                return new SingleResult<MessageBoard>(new Exception("You do not have permission to update or create message boards."));
            }


            if (msgBoard.Id == 0 && messageBoardId > 0)
            {
                return new SingleResult<MessageBoard>(new Exception("The specified message board couldn't be found."));
            }

            
            if (messageBoardId > 0)
            {
                msgBoard.Id = messageBoardId;
            }

            switch (messageBoard.MessageBoardType)
            {
                case MessageBoardType.Classroom:
                    if (msgBoard.ClassId == null || msgBoard.ClassId == 0)
                    {
                        return new SingleResult<MessageBoard>(new Exception("Please select a class for this message board."));
                    }

                    if (MessageBoardExists(Data.MessageBoard.ClassIdColumn, (int)msgBoard.ClassId) && msgBoard.Id == 0)
                    {
                        return new SingleResult<MessageBoard>(new Exception("This class already has a message board associated with it."));
                    }

                    break;
                case MessageBoardType.Online:
                    if (msgBoard.OnlineCourseId == null || msgBoard.OnlineCourseId == 0)
                    {
                        return new SingleResult<MessageBoard>(new Exception("Please select an online course for this message board."));
                    }

                    if (MessageBoardExists(Data.MessageBoard.OnlineCourseIdColumn, (int)msgBoard.OnlineCourseId) && msgBoard.Id == 0)
                    {
                        return new SingleResult<MessageBoard>(new Exception("This online class already has a message board associated with it."));
                    }

                    break;
                case MessageBoardType.TrainingProgram:
                    if (msgBoard.TrainingProgramId == null || msgBoard.TrainingProgramId == 0)
                    {
                        return new SingleResult<MessageBoard>(new Exception("Please select a training program for this message board."));
                    }

                    if (MessageBoardExists(Data.MessageBoard.TrainingProgramIdColumn, (int)msgBoard.TrainingProgramId) && msgBoard.Id == 0)
                    {
                        Data.TrainingProgram tp = new Data.TrainingProgram((int)msgBoard.TrainingProgramId);
                        if (tp.CustomerID == customerId)
                        {
                            return new SingleResult<MessageBoard>(new Exception("This training program already has a message board associated with it."));
                        }
                    }

                    break;
            }

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                int currentMessageBoardId = msgBoard.Id;

                msgBoard.Save(Username);

                int newMessageBoardId = msgBoard.Id;

                if (currentMessageBoardId != newMessageBoardId)
                {
                    msBoardCustomerLink = new Data.CustomerMessageBoardLink() { MessageBoardId = newMessageBoardId, CustomerId = customerId };
                    msBoardCustomerLink.Save(Username);
                }

                ts.Complete();
            }

            return GetMessageBoard(customerId, userId, msgBoard.Id);
        }

        public SingleResult<MessageBoard> GetMessageBoard(int customerId, int userId, int classId, MessageBoardType type)
        {
            bool useSharedTPMessageBoard = false;

            TableSchema.TableColumn classIdColumn = Data.MessageBoard.ClassIdColumn;

            switch (type) {
                case MessageBoardType.Classroom:
                    classIdColumn = Data.MessageBoard.ClassIdColumn;
                    break;
                case MessageBoardType.Online:
                    classIdColumn = Data.MessageBoard.OnlineCourseIdColumn;
                break;
                case MessageBoardType.TrainingProgram:
                    classIdColumn = Data.MessageBoard.TrainingProgramIdColumn;
                    Data.TrainingProgram tp = new Data.TrainingProgram(classId);
                    useSharedTPMessageBoard = tp.CustomerID != customerId;

                    // Always use the parent training program id so we can share the message
                    // boards accross parent/child training programs (Child training program
                    // always gets the parent message board)
                    if (tp.ParentTrainingProgramID.HasValue && tp.ParentTrainingProgramID > 0)
                    {
                        classId = tp.ParentTrainingProgramID.Value;
                    }
                break;
            }

            Data.MessageBoard messageBoardData = null;
            if (useSharedTPMessageBoard)
            {
                messageBoardData = Select.AllColumnsFrom<Data.MessageBoard>()
                    .InnerJoin(Data.Tables.CustomerMessageBoardLink, Data.CustomerMessageBoardLink.Columns.MessageBoardId, Data.Tables.MessageBoard, Data.MessageBoard.Columns.Id)
                    .Where(Data.CustomerMessageBoardLink.CustomerIdColumn).IsEqualTo(customerId)
                    .ExecuteSingle<Data.MessageBoard>();
            }
            else
            {
                messageBoardData = new Data.MessageBoard(classIdColumn.ColumnName, classId);
            }

            /*
            Data.MessageBoard messageBoardData = Select.AllColumnsFrom<Data.MessageBoard>()
                .Where(Data.MessageBoard.ClassIdColumn).IsEqualTo(classId)
                .ExecuteScalar<Data.MessageBoard>();
            */

            if (messageBoardData.Id != 0 && !canViewMessageBoard(messageBoardData))
            {
                return new SingleResult<MessageBoard>(new Exception("You do not have permission to view this message board."));
            }

            MessageBoard messageBoard = new MessageBoard();
            messageBoard.CopyFrom(messageBoardData);

            messageBoard.IsInstructor = isInstructor(userId, messageBoardData);

            return new SingleResult<MessageBoard>(messageBoard);
        }

        public SingleResult<MessageBoard> GetMessageBoard(int customerId, int userId, int messageBoardId)
        {
            List<string> columns = Data.MessageBoard.Schema.Columns.Select(c => c.QualifiedName).ToList<string>();
            columns.Add(String.Format("{0} as {1}", Data.OnlineCourse.NameColumn.QualifiedName, "OnlineCourseName"));
            columns.Add(String.Format("{0} as {1}", Data.ClassX.NameColumn.QualifiedName, "ClassName"));
            columns.Add(String.Format("{0} as {1}", Data.TrainingProgram.NameColumn.QualifiedName, "TrainingProgramName"));

            MessageBoard messageBoard = new Select(columns.ToArray()).From<Data.MessageBoard>()
                .LeftOuterJoin(Data.OnlineCourse.IdColumn, Data.MessageBoard.OnlineCourseIdColumn)
                .LeftOuterJoin(Data.TrainingProgram.IdColumn, Data.MessageBoard.TrainingProgramIdColumn)
                .LeftOuterJoin(Data.ClassX.IdColumn, Data.MessageBoard.ClassIdColumn)
                .Where(Data.MessageBoard.IdColumn).IsEqualTo(messageBoardId)
                .ExecuteSingle<MessageBoard>();

            Data.MessageBoard messageBoardData = new Data.MessageBoard();

            messageBoard.CopyTo(messageBoardData);
            messageBoardData.MessageBoardType = (int)messageBoard.MessageBoardType;

            if (!canViewMessageBoard(messageBoardData))
            {
                return new SingleResult<MessageBoard>(new Exception("You do not have permission to view this message board."));
            }

            switch (messageBoard.MessageBoardType)
            {
                case MessageBoardType.Classroom:
                    messageBoard.CourseName = messageBoard.ClassName;
                    break;
                case MessageBoardType.Online:
                    messageBoard.CourseName = messageBoard.OnlineCourseName;
                    break;
                case MessageBoardType.TrainingProgram:
                    messageBoard.CourseName = messageBoard.TrainingProgramName;
                    break;
            }

            messageBoard.IsInstructor = isInstructor(userId, messageBoardData);

            return new SingleResult<MessageBoard>(messageBoard);
        }

        public PagedResult<MessageBoardTopic> GetMessageBoardTopics(int customerId, int userId, int messageBoardId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, int lastAdded)
        {


            if (!canViewMessageBoard(new Data.MessageBoard(messageBoardId))) {
                return new PagedResult<MessageBoardTopic>(new Exception("You do not have permission to view this message board."));
            }

            SqlQuery query = Select.AllColumnsFrom<Data.MessageBoardTopicsDetail>()
                .Where(Data.MessageBoardTopicsDetail.Columns.MessageBoardID).IsEqualTo(messageBoardId);

            if (!canUpdateMessageBoards()) {
                query.And(Data.MessageBoardTopicsDetail.Columns.IsDeleted).IsEqualTo(false);
            }
    
            query.OrderDesc(Data.MessageBoardTopicsDetail.Columns.IsSticky);

            if (orderBy != null)
            {
                switch (orderDir) {
                    case OrderDirection.Ascending:
                        query.OrderAsc(orderBy);
                        break;
                    case OrderDirection.Default:
                    case OrderDirection.Descending:
                        query.OrderDesc(orderBy);
                        break;

                }
            }

            return new PagedResult<MessageBoardTopic>(query, pageIndex, pageSize, orderBy, orderDir);
        }


        public PagedResult<MessageBoardPost> GetMessageBoardPosts(int customerId, int userId, int topicId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            Data.MessageBoardTopic topic = new Data.MessageBoardTopic(topicId);

            if (topic.IsDeleted && !canUpdateMessageBoards())
            {
                return new PagedResult<MessageBoardPost>(new Exception("This topic no longer exists."));
            }

            if (!canViewTopic(topic))
            {
                return new PagedResult<MessageBoardPost>(new Exception("You do not have permission to view this topic."));
            }

            string query = string.Format(@"SELECT [dbo].[JobRole].[Name] as 'JobRole',
                [dbo].[User].[Username],
               (SELECT COUNT( [dbo].[MessageBoardPost].[ID] ) as 'Posts' FROM
			        [dbo].[MessageBoardPost]
                    JOIN [dbo].[MessageBoardTopic] topic ON topic.[ID] = [dbo].[MessageBoardPost].[TopicID]
			        WHERE [dbo].[MessageBoardPost].[UserID] = [dbo].[User].[ID]
                    AND [dbo].[MessageBoardPost].[IsDeleted] != 1
                    AND topic.[IsDeleted] != 1                    
               ) as 'Posts', 
               [dbo].[MessageBoardPost].[ID], 
               [dbo].[MessageBoardPost].[TopicID], 
               [dbo].[MessageBoardPost].[UserID], 
               [dbo].[MessageBoardPost].[PostID], 
               [dbo].[MessageBoardPost].[IsPrivate],
               [dbo].[MessageBoardPost].[IsFirst], 
               [dbo].[MessageBoardPost].[Content], 
               [dbo].[MessageBoardPost].[CreatedOn],
               [dbo].[MessageBoardPost].[ModifiedOn], 
               [dbo].[MessageBoardPost].[CreatedBy], 
               [dbo].[MessageBoardPost].[ModifiedBy],
               CASE WHEN i.[UserID] = {1} THEN 1 ELSE 0 END as IsInstructor,
               [dbo].[MessageBoardPost].[IsDeleted],
               ReplyTo.Content as ReplyContent,
               ReplyUser.Username as ReplyUsername
            FROM [dbo].[MessageBoardPost]
            INNER JOIN [dbo].[User] ON [dbo].[MessageBoardPost].[UserID] = [dbo].[User].[ID]
            LEFT OUTER JOIN [dbo].[JobRole] ON [dbo].[User].[JobRoleID] = [dbo].[JobRole].[ID]
            INNER JOIN [dbo].[MessageBoardTopic] ON [dbo].[MessageBoardPost].[TopicID] = [dbo].[MessageBoardTopic].[ID]
            LEFT JOIN [dbo].[MessageBoardPost] ReplyTo ON ReplyTo.ID = [dbo].[MessageBoardPost].[PostID]
            LEFT JOIN [dbo].[User] ReplyUser ON ReplyTo.UserID = ReplyUser.ID
            LEFT JOIN (
				SELECT TOP 1 e.[UserID], t.ID as TopicID
                    FROM [dbo].[SimpleUpcomingEvents] e
                    LEFT JOIN [dbo].[MessageBoardTopic] t ON t.MessageBoardID = e.MessageBoardID
                    WHERE e.[UserID] = {1}
                    AND t.[ID] = {0}
                    AND e.[EventTypeID] = {3}
            ) i on i.TopicID = [dbo].[MessageBoardPost].[TopicID]
            WHERE [dbo].[MessageBoardPost].[TopicID] = {0}
            AND ([dbo].[MessageBoardPost].[IsDeleted] != 1 OR {2})
            AND (
                [dbo].[MessageBoardPost].[IsPrivate] = 'false'
                OR (
                    [dbo].[MessageBoardPost].[IsPrivate] = 'true' 
                    AND (
                        [dbo].[MessageBoardPost].[UserID] = {1}
                        OR ([dbo].[MessageBoardTopic].[UserID] = {1} AND [dbo].[MessageBoardPost].[PostID] = 0)
                        OR  ReplyTo.UserID = {1}
                    )
                )
                OR i.[UserID] = {1}
                OR ({2})
            )
            ORDER BY [dbo].[MessageBoardPost].[CreatedOn] ASC", topicId, UserID, canUpdateMessageBoards() ? "0 = 0" : "0 = 1", (int)EventType.InstructedClass);
            // Reorder the posts so replies will always come after the original
            // post and stay in chronological order.
            List<MessageBoardPost> posts = new InlineQuery().ExecuteTypedList<MessageBoardPost>(query);
            
            Dictionary<int, MessageBoardPost> dictionaryMap = new Dictionary<int, MessageBoardPost>();

            foreach (MessageBoardPost post in posts)
            {
                if (!dictionaryMap.ContainsKey(post.ID))
                {
                    dictionaryMap.Add(post.ID, post);
                }

                if (dictionaryMap.ContainsKey(post.PostID))
                {
                    if (dictionaryMap[post.PostID].Replies == null) {
                        dictionaryMap[post.PostID].Replies = new List<MessageBoardPost>();
                    }
                    dictionaryMap[post.PostID].Replies.Insert(0, post);
                }
            }

            posts = posts.Where(p => p.PostID == 0).ToList<MessageBoardPost>();
            posts.Reverse();

            List<MessageBoardPost> unrolledPosts = new List<MessageBoardPost>();

            foreach (MessageBoardPost post in posts)
            {
                unrolledPosts.Add(post);
                if (post.Replies != null)
                {
                    UnrollPosts(unrolledPosts, post);
                }
            }

            posts = unrolledPosts;

            //Pageing
            int count = Math.Min(posts.Count() - (pageIndex * pageSize), pageSize);
            List<MessageBoardPost> postsPaged = posts.GetRange(pageIndex * pageSize, count);

            return new PagedResult<MessageBoardPost>(postsPaged.AsEnumerable<MessageBoardPost>(), pageIndex, pageSize, posts.Count);
        }

        public SingleResult<MessageBoardPost> DeleteMessageBoardPost(int customerId, int userId, int messageBoardPostId)
        {
            Data.MessageBoardPost post = new Data.MessageBoardPost(messageBoardPostId);
            Data.MessageBoardTopic messageBoardTopic = new Data.MessageBoardTopic(post.TopicID);

            if (!isInstructor(UserID, new Data.MessageBoard(messageBoardTopic.MessageBoardID)) && !canUpdateMessageBoards())
            {
                return new SingleResult<MessageBoardPost>(new Exception("You do not have permission to update this post."));
            }

            if (post.Id == 0 && messageBoardPostId > 0)
            {
                return new SingleResult<MessageBoardPost>(new Exception("The specified post couldn't be found."));
            }

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                SetIsDeleted(post, true);
                ts.Complete();
            }

            return GetMessageBoardPost(customerId, userId, post.TopicID, post.Id);
        }

        private void SetIsDeleted(Data.MessageBoardPost post, bool isDeleted)
        {
            post.IsDeleted = isDeleted;
            post.Save(Username);

            List<Data.MessageBoardPost> posts;

            
            if (isDeleted) //DELETE: Delete child posts as well since they wont appear if the parent is deleted
            {
                posts = Select.AllColumnsFrom<Data.MessageBoardPost>()
                    .Where(Data.MessageBoardPost.PostIDColumn).IsEqualTo(post.Id)
                    .ExecuteTypedList<Data.MessageBoardPost>();
            }
            else // RESTORE: Restore all parent posts that may be deleted since the child post will not appear if the parent is deleted
            {
                posts = Select.AllColumnsFrom<Data.MessageBoardPost>()
                    .Where(Data.MessageBoardPost.IdColumn).IsEqualTo(post.PostID)
                    .ExecuteTypedList<Data.MessageBoardPost>();
            }

            foreach (Data.MessageBoardPost p in posts)
            {
                SetIsDeleted(p, isDeleted);
            }
        }

        public SingleResult<MessageBoardPost> UpdateMessageBoardPost(int customerId, int userId, int messageBoardPostId, MessageBoardPost messageBoardPost)
        {
            Data.MessageBoardPost post = new Data.MessageBoardPost(messageBoardPostId);
            Data.MessageBoardTopic topic = new Data.MessageBoardTopic(messageBoardPost.TopicID);

            if (!canUpdatePost(post, new Data.MessageBoard(topic.MessageBoardID)))
            {
                return new SingleResult<MessageBoardPost>(new Exception("You do not have permission to update this post."));
            }

            if (topic.IsLocked)
            {
                return new SingleResult<MessageBoardPost>(new Exception("You cannot edit or create a post in a locked thread."));
            }

            if (post.Id == 0 && messageBoardPostId > 0) {
                return new SingleResult<MessageBoardPost>(new Exception("The specified post couldn't be found."));
            }

            if (post.IsFirst && messageBoardPost.IsPrivate)
            {
                return new SingleResult<MessageBoardPost>(new Exception("This post cannot be private."));
            }

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                if (messageBoardPostId > 0 && messageBoardPost.IsDeleted != post.IsDeleted)
                {
                    SetIsDeleted(post, messageBoardPost.IsDeleted);
                }

                messageBoardPost.CopyTo(post);

                if (messageBoardPostId == 0)
                {
                    post.UserID = userId;
                }

                post.Save(Username);

                ts.Complete();
            }
            return GetMessageBoardPost(customerId, userId, post.TopicID, post.Id);
        }

        public SingleResult<MessageBoardPost> GetMessageBoardPost(int customerId, int userId, int messageBoardTopicId, int messageBoardPostId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.MessageBoardPost>()
                .Where(Data.MessageBoardPost.IdColumn).IsEqualTo(messageBoardPostId)
                .And(Data.MessageBoardPost.TopicIDColumn).IsEqualTo(messageBoardTopicId);

            return new SingleResult<MessageBoardPost>(query);

        }

        public SingleResult<MessageBoardTopic> GetMessageBoardTopic(int customerId, int userId, int messageBoardId, int messageBoardTopicId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.MessageBoardTopic>()
                .Where(Data.MessageBoardTopic.IdColumn).IsEqualTo(messageBoardTopicId)
                .And(Data.MessageBoardTopic.MessageBoardIDColumn).IsEqualTo(messageBoardId);

            return new SingleResult<MessageBoardTopic>(query);

        }

        public SingleResult<MessageBoardTopic> DeleteMessageBoardTopic(int customerId, int userId, int messageBoardTopicId)
        {
            Data.MessageBoardTopic topic = new Data.MessageBoardTopic(messageBoardTopicId);
            if (!isInstructor(UserID, new Data.MessageBoard(topic.MessageBoardID)) && !canUpdateMessageBoards())
            {
                return new SingleResult<MessageBoardTopic>(new Exception("You do not have permission to delete this topic."));
            }

            topic.IsDeleted = true;
            topic.Save();

            return GetMessageBoardTopic(customerId, userId, messageBoardTopicId);
        }

        public SingleResult<MessageBoardTopic> UpdateMessageBoardTopic(int customerId, int userId, int messageBoardTopicId, MessageBoardTopic messageBoardTopic)
        {
            Data.MessageBoardTopic topic = new Data.MessageBoardTopic(messageBoardTopicId);
            
            if (!canUpdateTopic(topic, new Data.MessageBoard(messageBoardTopic.MessageBoardID)))
            {
                return new SingleResult<MessageBoardTopic>(new Exception("You do not have permission to update this topic."));
            }

            if (topic.IsSticky != messageBoardTopic.IsSticky && !canSticky(topic))
            {
                return new SingleResult<MessageBoardTopic>(new Exception("You do not have permission to alter the sticky status of this topic."));
            }

            if (topic.Id == 0 && messageBoardTopicId > 0)
            {
                return new SingleResult<MessageBoardTopic>(new Exception("The specified topic couldn't be found."));
            }

            messageBoardTopic.CopyTo(topic);


            if (messageBoardTopicId == 0)
            {
                topic.UserID = userId;
            }

            topic.Save(Username);

            if (messageBoardTopicId == 0)
            {
                Data.MessageBoardPost post = new Data.MessageBoardPost();
                post.Id = 0;
                post.UserID = userId;
                post.PostID = 0;
                post.TopicID = topic.Id;
                post.Content = messageBoardTopic.NewTopicPost;
                post.IsFirst = true;

                post.Save(Username);
            }

            return GetMessageBoardTopic(customerId, userId, topic.Id);
        }

        public SingleResult<MessageBoardTopic> GetMessageBoardTopic(int customerId, int userId, int messageBoardTopicId)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.MessageBoardTopic>()
                 .Where(Data.MessageBoardTopic.IdColumn).IsEqualTo(messageBoardTopicId);

            return new SingleResult<MessageBoardTopic>(query);
        }

    }
}
