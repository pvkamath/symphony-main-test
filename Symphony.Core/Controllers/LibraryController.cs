﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using SubSonic;
using Symphony.Web;
using System.Web.Security;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using SubSonic.Sugar;
using System.Data;
using log4net;
using System.Web;
using System.Net;
using System.ServiceModel.Web;
using Symphony.Core.Extensions;
using Symphony.Core.Comparers;
using System.Transactions;
using Data = Symphony.Core.Data;
using Newtonsoft.Json.Linq;

namespace Symphony.Core.Controllers
{
    public class LibraryController : SymphonyController
    {
        private ILog Log = LogManager.GetLogger(typeof(LibraryController));

        private void CheckLibraryAccess()
        {
            if (!HasRole(Roles.CourseAssignmentAdministrator) &&
                 !HasRole(Roles.CourseAssignmentTrainingManager) &&
                 !HasRole(Roles.SuperHero) &&
                 !ActualUserIsSalesChannelAdmin)
            {
                throw new Exception("Permission denied to libraries.");
            }
        }

        private void CheckLibraryOwnership(int libraryId, bool isAllowSharing = false)
        {
            CheckLibraryAccess();

            Data.Library library = new Data.Library(libraryId);
            bool isShared = false;

            if (isAllowSharing)
            {
                Data.CustomerNetworkDetail shared = Select.AllColumnsFrom<Data.CustomerNetworkDetail>()
                    .InnerJoin(Data.NetworkSharedEntity.CustomerNetworkDetailIDColumn, Data.CustomerNetworkDetail.DetaildIdColumn)
                    .Where(Data.CustomerNetworkDetail.AllowLibrariesColumn).IsEqualTo(true)
                    .And(Data.CustomerNetworkDetail.DestinationCustomerIdColumn).IsEqualTo(CustomerID)
                    .And(Data.NetworkSharedEntity.EntityIDColumn).IsEqualTo(libraryId)
                    .And(Data.NetworkSharedEntity.EntityTypeIDColumn).IsEqualTo(EntityType.Library)
                    .ExecuteSingle<Data.CustomerNetworkDetail>();

                if (shared != null)
                {
                    isShared = true;
                }
            }

            if (library.CustomerID != CustomerID && (!isAllowSharing || !isShared))
            {
                throw new Exception("Access denied to library " + libraryId + ".");
            }

            if (Username != library.CreatedBy && UserID.ToString() != library.CreatedBy && HasRole(Roles.CourseAssignmentTrainingManager))
            {
                throw new Exception("You cannot edit libraries created by other users.");
            }
        }

        private string GetLibraryIdsCommaSeparated(LibraryItemFilter filter, int? userId, int libraryId)
        {
            if (libraryId == 0)
            {
                if (filter.LibraryIds == null || filter.LibraryIds.Length == 0)
                {
                    if (!userId.HasValue)
                    {
                        throw new Exception("Either provide a library ids to filter by, or a user id to load libraries for.");
                    }

                    filter.LibraryIds = GetLibrariesForUser(userId.Value, "", "", OrderDirection.Ascending, 0, int.MaxValue).Data.Select(l => l.ID).ToArray();
                }
            }
            else
            {
                filter.LibraryIds = new int[] { libraryId };
            }

            return string.Join(",", filter.LibraryIds);
        }

        public SingleResult<Library> GetLibrary(int libraryId)
        {
            Data.Library libraryData = new Data.Library(libraryId);

            if (libraryData.IsDeleted)
            {
                throw new Exception("This library has been deleted");
            }
            
            Library library = new Library();
            library.CopyFrom(libraryData);

            library.IsShared = CustomerID != library.CustomerID;

            //library.CourseCount = libraryData.LibraryCourses.Count();

            return new SingleResult<Library>(library);
        }
        
        public PagedResult<Library> GetLibraries(string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            return GetLibraries(CustomerID, searchText, orderBy, orderDir, pageIndex, pageSize, true);
        }
        public PagedResult<Library> GetLibraries(int customerId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, bool isAllowShared = false)
        {
            CheckLibraryAccess();

            List<Library> libraries = Data.SPs.LibraryGetLibraries(
                customerId,
                searchText,
                orderBy,
                orderDir == OrderDirection.Ascending ? "asc" : "desc",
                pageIndex,
                pageSize,
                isAllowShared)
            .ExecuteTypedList<Library>();

            Library first = libraries.FirstOrDefault();
            int totalRows = first != null ? first.TotalRows : 0;

            return new PagedResult<Library>(libraries, pageIndex, pageSize, totalRows);
        }
        /// <summary>
        /// Returns a tree of filter options that are available for the libraries assigned to the user id provided
        /// </summary>
        /// <param name="userId">The user to load filter options for</param>
        /// <returns></returns>
        public MultipleResult<LibraryFilterOption> GetLibraryFilterOptions(int userId)
        {
            IEnumerable<LibraryGrant> libraries = GetLibrariesForUser(userId, "", "", OrderDirection.Ascending, 0, int.MaxValue).Data;

            string libraryIdsCommaSeparated = string.Join(",", libraries.Select(l => l.ID).ToArray());

            LibraryFilterOption categoryFilterOptions = new LibraryFilterOption()
            {
                DisplayName = "Categories",
                Data = new List<LibraryFilterOption>()
            };
            LibraryFilterOption libraryFilterOptions= new LibraryFilterOption()
            {
                DisplayName = "Libraries",
                Data = new List<LibraryFilterOption>()
            };

            List<LibraryFilterableCategory> filterableCategories = Data.SPs.LibraryGetFilterableCategoriesForLibraries(libraryIdsCommaSeparated).ExecuteTypedList<LibraryFilterableCategory>();

            LibraryFilterOption lastOption = null;
            foreach (LibraryFilterableCategory fc in filterableCategories)
            {
                if (lastOption == null || lastOption.Value != fc.CategoryID)
                {
                    lastOption = new LibraryFilterOption()
                    {
                        Name = "categoryIds",
                        DisplayName = fc.CategoryName,
                        Value = fc.CategoryID,
                        Data = new List<LibraryFilterOption>(),
                        Checked = false
                    };

                    categoryFilterOptions.Data.Add(lastOption);
                }

                if (fc.SecondaryCategoryID > 0)
                {
                    lastOption.Data.Add(new LibraryFilterOption() {
                        Name = "secondaryCategoryIds",
                        DisplayName = fc.SecondaryCategoryName,
                        Value = fc.SecondaryCategoryID,
                        Checked = false
                    });
                }
            }

            foreach (Library l in libraries)
            {
                libraryFilterOptions.Data.Add(new LibraryFilterOption()
                {
                    Name = "libraryIds",
                    DisplayName = l.Name,
                    Value = l.ID,
                    Checked = false
                });
            }

            return new MultipleResult<LibraryFilterOption>(new List<LibraryFilterOption>() { categoryFilterOptions, libraryFilterOptions });
            
        }
        /// <summary>
        /// Returns library categories along with the count of items for each category
        /// </summary>
        /// <param name="libraryId">Library ID to find categories for</param>
        /// <param name="libraryItemTypeId">Item type to load - Optional, use when loading all items</param>
        /// <param name="userId">UserID - Optional - only needed to show favorites</param>
        /// <param name="isShowAll">True - lists all items even if they are not assigned to the library. False, show library items only</param>
        /// <param name="customerId">Customer id to load items from when showing all</param>
        /// <param name="searchText">Searches item name + category names</param>
        /// <param name="orderBy">TotalItems, CategoryName, LevelIndentText</param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        ///              
        public PagedResult<LibraryCategory> GetLibraryCategories(int libraryId, int? userId, int customerId, string filterJson,
            string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            LibraryItemFilter filter = new LibraryItemFilter();
            if (!string.IsNullOrWhiteSpace(filterJson))
            {
                filter = Symphony.Core.Utilities.Deserialize<LibraryItemFilter>(filterJson);
            }

            string libraryIdsCommaSeparated = GetLibraryIdsCommaSeparated(filter, userId, libraryId);

            Data.Library library = new Data.Library(libraryId);

            DateTime? startDate = null;
            DateTime? endDate = null;

            if (filter.DateRange.HasValue)
            {
                startDate = filter.DateRange.Value.StartDate.ToDateTimeUtc();
                endDate = filter.DateRange.Value.EndDate.ToDateTimeUtc();
            }

            string authorSearch = string.Join(
                " OR ",
                filter.Authors
                   .Where(a => a != null && a.ID == 0)
                   .Select(a => "\"*" + a.FullName + "*\"")
                   .ToList()
                );

            string secondaryCategorySearch = string.Join(
                " OR ",
                filter.Categories
                   .Where(c => c != null && c.Id == 0)
                   .Select(c => "\"*" + c.Name + "*\"")
                   .ToList()
                );
            

            List<int> secondaryCategoryIds = filter.Categories.Where(c => c.Id > 0).Select(c => c.Id).ToList();
            List<int> authorIds = filter.Authors.Where(a => a.ID > 0).Select(a => a.ID).ToList();

            int? categoryId = null;
            
            if (filter.CategoryID > 0) {
                categoryId = filter.CategoryID;
            }

            List<LibraryCategory> categories = Data.SPs.LibraryGetCategoriesForLibrary(
                libraryIdsCommaSeparated, 
                library.LibraryItemTypeID, 
                userId, 
                filter.IsShowAll, 
                customerId, 
                searchText, 
                orderBy,
                orderDir == OrderDirection.Descending ? "desc" : "asc", 
                pageIndex, 
                pageSize,
                filter.IsHideInLibrary,
                categoryId,
                string.Join(",", secondaryCategoryIds),
                secondaryCategorySearch,
                string.Join(",", authorIds),
                authorSearch,
                filter.IsFavorite,
                filter.IsInProgress,
                filter.IsNotStarted,
                startDate,
                endDate)
            .ExecuteTypedList<LibraryCategory>();

            LibraryCategory first = categories.FirstOrDefault();
            int totalRows = first != null ? first.TotalRows : 0;

            return new PagedResult<LibraryCategory>(categories, pageIndex, pageSize, totalRows);
        }
        /// <summary>
        /// Returns the libraries the user has access to
        /// </summary>
        /// <param name="userId">user id to load libraries for</param>
        /// <param name="searchText">search by library</param>
        /// <param name="orderBy">HierarchyTypeID, Name, StartDate</param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedResult<LibraryGrant> GetLibrariesForUser(int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            if (userId != UserID && !this.UserIsSalesChannelAdmin)
            {
                throw new Exception("Cannot access libraries for this user.");
            }

            List<LibraryGrant> grants = Data.SPs.LibraryGetLibrariesForUser(
                userId,
                searchText,
                orderBy,
                orderDir == OrderDirection.Descending ? "desc" : "asc",
                pageIndex,
                pageSize)
            .ExecuteTypedList<LibraryGrant>();

            LibraryGrant first = grants.FirstOrDefault();
            int totalRows = first != null ? first.TotalRows : 0;

            return new PagedResult<LibraryGrant>(grants, pageIndex, pageSize, totalRows);
        }
        /// <summary>
        /// Returns the grants for this library
        /// </summary>
        /// <param name="libraryId">Id of the library</param>
        /// <param name="searchText">search by hierarchy name, hierarchy type, user email, hierarchy alises/param>
        /// <param name="orderBy">HierarchyTypeID, Name, StartDate</param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedResult<LibraryGrantHierarchy> GetHierarchiesForLibrary(int libraryId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            List<LibraryGrantHierarchy> grants = Data.SPs.LibraryGetGrantsForLibrary(
                libraryId,
                searchText,
                !string.IsNullOrWhiteSpace(orderBy) ? orderBy : "HierarchyTypeID",
                orderDir == OrderDirection.Descending ? "desc" : "asc",
                pageIndex,
                pageSize,
                CustomerID,
                true)
            .ExecuteTypedList<LibraryGrantHierarchy>();
            /*
             * In case we need this at some point
            List<int> audienceIds = grants.Where(g => g.AudienceID > 0).Select(g => g.AudienceID).ToList();
            List<int> locationIds = grants.Where(g => g.LocationID > 0).Select(g => g.LocationID).ToList();
            List<int> userIds = grants.Where(g => g.UserID > 0).Select(g => g.UserID).ToList();
            List<int> jobRoleIds = grants.Where(g => g.JobRoleID > 0).Select(g => g.JobRoleID).ToList();

            Dictionary<int, Audience> audienceDictionary = new Dictionary<int, Audience>();
            Dictionary<int, Location> locationDictionary = new Dictionary<int, Location>();
            Dictionary<int, JobRole> jobRoleDictionary = new Dictionary<int, JobRole>();
            Dictionary<int, User> userDictionary = new Dictionary<int, User>();

            if (audienceIds.Count > 0)
            {
                audienceDictionary = Select.AllColumnsFrom<Data.Audience>().Where(Data.Audience.IdColumn).In(audienceIds).ExecuteTypedList<Audience>().ToDictionary(a => a.Id);
            }
            if (locationIds.Count > 0)
            {
                locationDictionary = Select.AllColumnsFrom<Data.Location>().Where(Data.Location.IdColumn).In(locationIds).ExecuteTypedList<Location>().ToDictionary(l => l.Id);
            }
            if (userIds.Count > 0)
            {
                userDictionary = Select.AllColumnsFrom<Data.User>().Where(Data.User.IdColumn).In(userIds).ExecuteTypedList<User>().ToDictionary(u => u.ID);
            }
            if (jobRoleIds.Count > 0)
            {
                jobRoleDictionary = Select.AllColumnsFrom<Data.JobRole>().Where(Data.JobRole.IdColumn).In(jobRoleIds).ExecuteTypedList<JobRole>().ToDictionary(j => j.ID);
            }

            foreach (LibraryGrantHierarchy g in grants)
            {
                switch (g.HierarchyTypeID)
                {
                    case (int)HierarchyType.Audience:
                        g.HierarchyObject = audienceDictionary.ContainsKey(g.HierarchyNodeID) ? audienceDictionary[g.HierarchyNodeID] : null;
                        break;
                    case (int)HierarchyType.JobRole:
                        g.HierarchyObject = jobRoleDictionary.ContainsKey(g.HierarchyNodeID) ? jobRoleDictionary[g.HierarchyNodeID] : null;
                        break;
                    case (int)HierarchyType.Location:
                        g.HierarchyObject = locationDictionary.ContainsKey(g.HierarchyNodeID) ? locationDictionary[g.HierarchyNodeID] : null;
                        break;
                    case (int)HierarchyType.User:
                        g.HierarchyObject = userDictionary.ContainsKey(g.HierarchyNodeID) ? userDictionary[g.HierarchyNodeID] : null;
                        break;
                }
           }*/

            LibraryGrantHierarchy first = grants.FirstOrDefault();
            Int64 totalRows = first != null ? first.TotalRows : 0;

            return new PagedResult<LibraryGrantHierarchy>(grants, pageIndex, pageSize, (int)totalRows);
        }
        /// <summary>
        /// Returns items for the library
        /// </summary>
        /// <param name="libraryId">The library</param>
        /// <param name="userId">User id - Optional - For listing items for a user to show favorites</param>
        /// <param name="customerId">Customer id to load items from</param>
        /// <param name="filterJson">Optional - filters passed in as json</param>
        /// <param name="searchText">Search by item name or category name</param>
        /// <param name="orderBy">CourseTypeID, Name, Category, CreatedOn</param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedResult<LibraryItem> GetLibraryItems(int libraryId, int? userId, int customerId, string filterJson,
            string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            LibraryItemFilter filter = new LibraryItemFilter();
            if (!string.IsNullOrWhiteSpace(filterJson))
            {
                filter = Symphony.Core.Utilities.Deserialize<LibraryItemFilter>(filterJson);
            }

            string libraryIdsCommaSeparated = GetLibraryIdsCommaSeparated(filter, userId, libraryId);

            DateTime? startDate = null;
            DateTime? endDate = null;

            if (filter.DateRange.HasValue)
            {
                startDate = filter.DateRange.Value.StartDate.ToDateTimeUtc();
                endDate = filter.DateRange.Value.EndDate.ToDateTimeUtc();
            }

            string authorSearch = string.Join(
                " OR ",
                filter.Authors
                   .Where(a => a != null && a.ID == 0)
                   .Select(a => "\"*" + a.FullName + "*\"")
                   .ToList()
                );

            string secondaryCategorySearch = string.Join(
                " OR ",
                filter.Categories
                   .Where(c => c != null && c.Id == 0)
                   .Select(c => "\"*" + c.Name + "*\"")
                   .ToList()
                );
            

            List<int> secondaryCategoryIds = filter.Categories.Where(c => c.Id > 0).Select(c => c.Id).ToList();
            List<int> authorIds = filter.Authors.Where(a => a.ID > 0).Select(a => a.ID).ToList();

            if (filter.SecondaryCategoryIds != null && filter.SecondaryCategoryIds.Length > 0)
            {
                secondaryCategoryIds = filter.SecondaryCategoryIds.ToList();
            };

            List<int> categoryIds = new List<int>();
            if (filter.CategoryIds != null && filter.CategoryIds.Length > 0)
            {
                categoryIds = filter.CategoryIds.ToList();
            }
            else if (filter.CategoryID > 0)
            {
                categoryIds.Add(filter.CategoryID);
            }


            List<LibraryItem> items = Data.SPs.LibraryGetItemsForLibraryV2(
                libraryIdsCommaSeparated,
                userId,
                customerId,
                filter.IsShowAll,
                filter.IsHideInLibrary,
                categoryIds.Count > 0 ? string.Join(",", categoryIds) : null,
                string.Join(",", secondaryCategoryIds),
                secondaryCategorySearch,
                string.Join(",", authorIds),
                authorSearch,
                searchText,
                orderBy,
                orderDir == OrderDirection.Ascending ? "asc" : "desc",
                pageIndex,
                pageSize,
                filter.IsFavorite,
                filter.IsInProgress,
                filter.IsNotStarted,
                startDate,
                endDate,
                filter.LibraryItemTypeId)
            .ExecuteTypedList<LibraryItem>();

            LibraryItem first = items.FirstOrDefault();
            int totalRows = first != null ? first.TotalRows : 0;

            // Not the most efficient way to go, however it is only one extra
            // query after the original sp. Page size is generally 20 anyway
            // so we wont be loading 1000s of courses/tps with these queries
            List<int> courseIds = items.Where(i => i.LibraryItemTypeID == (int)LibraryItemType.OnlineCourse).Select(i => i.Id).ToList();
            List<int> trainingProgramIds = items.Where(i => i.LibraryItemTypeID == (int)LibraryItemType.TrainingProgram).Select(i => i.Id).ToList();

            // Just in case we ever have a case where the page size is overridden to something like 9999999 
            // and the library has > 2000 items in it. If this is happening a lot, then we should review this
            IEnumerable<int[]> courseIdGroups = courseIds.InGroupsOf(2000);
            IEnumerable<int[]> trainingProgramIdGroups = trainingProgramIds.InGroupsOf(2000);

            List<Course> courses = new List<Course>();
            List<TrainingProgram> trainingPrograms = new List<TrainingProgram>();
            List<Author> authors = new List<Author>();
            List<Category> secondaryCategories = new List<Category>();

            foreach (int[] ids in courseIdGroups) {
                if (ids.Length > 0)
                {
                    courses.AddRange(Select.AllColumnsFrom<Data.OnlineCourse>()
                        .IncludeColumn(((int)CourseType.Online).ToString(), "CourseTypeID")
                        .Where(Data.OnlineCourse.IdColumn).In(ids)
                        .ExecuteTypedList<Course>());

                    authors.AddRange(
                        new Select(
                            Data.User.IdColumn,
                            Data.User.FirstNameColumn,
                            Data.User.LastNameColumn,
                            Data.User.EmailColumn,
                            Data.OnlineCourseAuthorLink.OnlineCourseIdColumn
                        )
                        .From<Data.OnlineCourseAuthorLink>()
                        .InnerJoin(Data.User.IdColumn, Data.OnlineCourseAuthorLink.UserIdColumn)
                        .Where(Data.OnlineCourseAuthorLink.OnlineCourseIdColumn).In(ids)
                        .ExecuteTypedList<Author>());

                    secondaryCategories.AddRange(
                        new Select(
                            Data.SecondaryCategory.IdColumn,
                            Data.SecondaryCategory.NameColumn,
                            Data.OnlineCourseSecondaryCategoryLink.OnlineCourseIdColumn
                            )
                        .From<Data.OnlineCourseSecondaryCategoryLink>()
                        .InnerJoin(Data.SecondaryCategory.IdColumn, Data.OnlineCourseSecondaryCategoryLink.SecondaryCategoryIdColumn)
                        .Where(Data.OnlineCourseSecondaryCategoryLink.OnlineCourseIdColumn).In(ids)
                        .ExecuteTypedList<Category>());

                }
            }

            foreach (int[] ids in trainingProgramIdGroups)
            {
                if (ids.Length > 0)
                {
                    trainingPrograms.AddRange(Select.AllColumnsFrom<Data.TrainingProgram>()
                        .Where(Data.TrainingProgram.IdColumn).In(ids)
                        .ExecuteTypedList<TrainingProgram>());
                }
            }

            Dictionary<string, LibraryItem> itemDictionary = items.DistinctBy(i => i.LibraryItemTypeID + "_" + i.Id).ToDictionary(i => i.LibraryItemTypeID + "_" + i.Id);
            Dictionary<int, List<Author>> authorDictionary = authors.GroupBy(a => a.OnlineCourseID).ToDictionary(g => g.Key, a => a.ToList());
            Dictionary<int, List<Category>> categoryDictionary = secondaryCategories.GroupBy(c => c.OnlineCourseID).ToDictionary(g => g.Key, c => c.ToList());

            if (trainingPrograms.Count > 0)
            {
                new PortalController().AddTrainingProgramDetails(UserID, trainingPrograms, false, false);
            }
            if (courses.Count > 0)
            {
                CourseController.AddTranscriptDetails(UserID, courses, 0);
            }

            foreach (Course c in courses) {
                string key = (int)LibraryItemType.OnlineCourse + "_" + c.Id;

                if (authorDictionary.ContainsKey(c.Id))
                {
                    c.Authors = authorDictionary[c.Id];
                }

                if (categoryDictionary.ContainsKey(c.Id))
                {
                    c.SecondaryCategories = categoryDictionary[c.Id];
                }

                if (itemDictionary.ContainsKey(key))
                {
                    itemDictionary[key].Item = c;
                }
            }

            foreach (TrainingProgram tp in trainingPrograms)
            {
                string key = (int)LibraryItemType.TrainingProgram + "_" + tp.Id;

                if (itemDictionary.ContainsKey(key))
                {
                    itemDictionary[key].Item = tp;
                }
            }

            return new PagedResult<LibraryItem>(items, pageIndex, pageSize, totalRows);            
        }

        public MultipleResult<LibraryGrantHierarchy> SaveLibraryGrantHierarchies(int libraryId, List<LibraryGrantHierarchy> grants)
        {
            CheckLibraryOwnership(libraryId, true);

            Data.LibraryGrantCollection grantCollection = new Data.LibraryGrantCollection();

            Dictionary<int, List<int>> delete = new Dictionary<int, List<int>>();

            foreach (LibraryGrantHierarchy lgh in grants)
            {
                Data.LibraryGrant g = new Data.LibraryGrant();
                lgh.CopyTo(g);
                grantCollection.Add(g);

                g.LibraryID = libraryId;

                if (!delete.ContainsKey(g.HierarchyTypeID))
                {
                    delete.Add(g.HierarchyTypeID, new List<int>());
                }
                delete[g.HierarchyTypeID].Add(g.HierarchyNodeID);
            }

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                foreach (KeyValuePair<int, List<int>> kv in delete)
                {
                    IEnumerable<int[]> idGroups = kv.Value.InGroupsOf(2000);

                    foreach (int[] ids in idGroups)
                    {
                        new Delete().From<Data.LibraryGrant>()
                            .Where(Data.LibraryGrant.LibraryIDColumn).IsEqualTo(libraryId)
                            .And(Data.LibraryGrant.HierarchyTypeIDColumn).IsEqualTo(kv.Key)
                            .And(Data.LibraryGrant.HierarchyNodeIDColumn).In(ids)
                            .Execute();
                    }
                }

                grantCollection.SaveAll();

                ts.Complete();
            }

            return new MultipleResult<LibraryGrantHierarchy>(grants);
        }

        public MultipleResult<LibraryItem> SaveLibraryItems(int libraryId, List<LibraryItem> items)
        {
            CheckLibraryOwnership(libraryId);

            Data.LibraryItemCollection libraryItems = new Data.LibraryItemCollection();

            foreach (LibraryItem item in items)
            {
                Data.LibraryItem i = new Data.LibraryItem();
                item.CopyTo(i);
                i.ItemID = item.Id;
                
                i.LibraryID = libraryId;
    
                libraryItems.Add(i);
            }

            libraryItems.SaveAll(Username);

            return new MultipleResult<LibraryItem>(items);
        }

        public MultipleResult<LibraryItem> DeleteLibraryItems(int libraryId, List<LibraryItem> items)
        {
            CheckLibraryOwnership(libraryId);

            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                foreach (LibraryItem item in items)
                {
                    new Delete().From<Data.LibraryItem>()
                        .Where(Data.LibraryItem.LibraryIDColumn).IsEqualTo(libraryId)
                        .And(Data.LibraryItem.ItemIDColumn).IsEqualTo(item.Id)
                        .And(Data.LibraryItem.LibraryItemTypeIDColumn).IsEqualTo(item.LibraryItemTypeID)
                        .Execute();
                }

                ts.Complete();
            }

            return new MultipleResult<LibraryItem>(items);
        }

        public SimpleSingleResult<bool> DeleteLibraryGrantHierarchies(int libraryId, List<LibraryGrantHierarchy> grants)
        {
            using (TransactionScope ts = Utilities.CreateTransactionScope())
            {
                foreach (LibraryGrantHierarchy lgh in grants)
                {
                    new Delete().From<Data.LibraryGrant>()
                        .Where(Data.LibraryGrant.LibraryIDColumn).IsEqualTo(libraryId)
                        .And(Data.LibraryGrant.HierarchyNodeIDColumn).IsEqualTo(lgh.HierarchyNodeID)
                        .And(Data.LibraryGrant.HierarchyTypeIDColumn).IsEqualTo(lgh.HierarchyTypeID)
                        .Execute();
                }

                ts.Complete();
            }

            return new SimpleSingleResult<bool>(true);
        }

        public SingleResult<Library> SaveLibrary(int libraryId, Library library)
        {
            if (libraryId > 0)
            {
                CheckLibraryOwnership(libraryId);
            }
            else
            {
                CheckLibraryAccess();
            }

            Data.Library dataLibrary = new Data.Library(libraryId);
            library.CopyTo(dataLibrary);
            dataLibrary.Id = libraryId;

            if (!string.IsNullOrEmpty(library.Sku))
            {
                Library libraryMatchingSku = Select.AllColumnsFrom<Data.Library>()
                    .Where(Data.Library.IdColumn).IsNotEqualTo(libraryId)
                    .And(Data.Library.CustomerIDColumn).IsEqualTo(dataLibrary.CustomerID)
                    .And(Data.Library.SkuColumn).IsEqualTo(library.Sku)
                    .ExecuteSingle<Library>();

                if (libraryMatchingSku != null)
                {
                    throw new Exception("The library sku must be unique.");
                }
            }
            if (dataLibrary.CustomerID == 0)
            {
                dataLibrary.CustomerID = CustomerID;
            }

            dataLibrary.Save(Username);

            return GetLibrary(dataLibrary.Id); 
        }

        public SimpleSingleResult<bool> DeleteLibrary(int libraryId)
        {
            throw new NotImplementedException();
        }

        public SingleResult<DisplayIconState> SaveFavorite(int itemId, int userId)
        {
            LibraryItem item = new LibraryItem()
            {
                IsFavorite = true,
                LibraryItemID = itemId
            };

            Data.LibraryFavorite favorite = new Data.LibraryFavorite(){
                UserID = userId,
                LibraryItemID = itemId
            };

            List<Data.LibraryFavorite> existingFavorite = Select.AllColumnsFrom<Data.LibraryFavorite>()
                .Where(Data.LibraryFavorite.LibraryItemIDColumn).IsEqualTo(itemId)
                .And(Data.LibraryFavorite.UserIDColumn).IsEqualTo(userId)
                .ExecuteTypedList<Data.LibraryFavorite>();

            if (existingFavorite.Count == 0)
            {
                favorite.Save();
            }

            return new SingleResult<DisplayIconState>(DisplayHelpers.GetIsFavoriteIcon(item));
        }

        public SimpleSingleResult<DisplayIconState> DeleteFavorite(int itemId, int userId)
        {
            new Delete()
                .From<Data.LibraryFavorite>()
                .Where(Data.LibraryFavorite.LibraryItemIDColumn).IsEqualTo(itemId)
                .And(Data.LibraryFavorite.UserIDColumn).IsEqualTo(userId)
                .Execute();

            LibraryItem item = new LibraryItem()
            {
                LibraryItemID = itemId,
                IsFavorite = false
            };

            return new SingleResult<DisplayIconState>(DisplayHelpers.GetIsFavoriteIcon(item));
        }


    }
}
