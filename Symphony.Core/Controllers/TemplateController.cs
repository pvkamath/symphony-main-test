﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using WebWidgetry.Utilities;
using System.Reflection;
using BankersEdge.Customer;
using BankersEdge.Classroom;
using BankersEdge.CourseAssignment;
using BankersEdge.Collaboration;
using BankersEdge.Artisan;
using System.Text.RegularExpressions;
using SubSonic.Utilities;
using SubSonic;
using log4net;
using Symphony.Core.Models;
using Symphony.Core.Data;

namespace Symphony.Core.Controllers
{
    /// <summary>
    /// Used to control and manage all functions of messaging templates
    /// </summary>
    public partial class TemplateController
    {
        static ILog Log = LogManager.GetLogger(typeof(TemplateController));

        

        //private void TemplateController_BeforeFetch(object sender, TemplateFetchEventArgs e)
        //{
        //    // make sure that if there aren't any explicit templates for this customer yet,
        //    // that we create them.
        //    int count = new TemplateCollection()
        //            .Where(Template.Columns.CustomerID, GetCustomerID())
        //            .Load()
        //            .Count();

        //    if (count == 0)
        //    {
        //        // load up the base set of templates
        //        TemplateCollection templates = new TemplateCollection()
        //            .Where(Template.Columns.CustomerID, 0)
        //            .Load();

        //        // and recreate them for this customer
        //        templates.ForEach(t =>
        //        {
        //            // load up the original list of recipients for this template
        //            TemplatesToRecipientGroupsMapCollection maps = new TemplatesToRecipientGroupsMapCollection()
        //                .Where(TemplatesToRecipientGroupsMap.Columns.TemplateID, t.Id)
        //                .Load();

        //            // create a copy of the template for this customer
        //            t.Id = 0;
        //            t.CustomerID = GetCustomerID();
        //            t.MarkNew();
        //            t.Save();

        //            // save the recipients with the new template id
        //            maps.ForEach(m =>
        //            {
        //                m.TemplateID = t.Id;
        //                m.MarkNew();
        //                m.Save();
        //            });
        //        });
        //    }

        //    // filter the query to this customer
        //    e.Query.WHERE(Template.Columns.CustomerID, GetCustomerID());
        //    if (e.Query.OrderByCollection.Count == 0)
        //    {
        //        e.Query
        //            .ORDER_BY(Template.Columns.IsScheduled, "ASC")
        //            .ORDER_BY(Template.Columns.Area, "ASC")
        //            .ORDER_BY(Template.Columns.DisplayName, "ASC");
        //    }
        //}

        /// <summary>
        /// Parses the specified templateText value using the types that are valid for the specified templateCodeName
        /// </summary>
        /// <param name="templateCodeName">The name of the template</param>
        /// <param name="templateText">The text to parse</param>
        /// <returns>The parsed template</returns>
        public static string PreviewTemplate(int templateId, string templateText)
        {
            // get a list of the types that can be used in this template
            Type[] types = TemplateController.GetValidTypesForTemplate(new Template(templateId));
            List<object> objects = new List<object>();

            // for each type that can be used in the template, get an instance
            StringBuilder sb = new StringBuilder();
            foreach (Type t in types)
            {
                objects.Add(TemplateController.GetObjectInstance(t));
            }

            // parse the template, using the sample objects
            return TemplateController.Parse(templateText, true, objects.ToArray());
        }

        #region Normal Notifications
        // standard
        public const string MarkedAsAttended = "MarkedAsAttended";
        public const string SchedulingChange = "SchedulingChange";
        public const string InstructorRemoved = "InstructorRemoved";
        public const string InstructorAdded = "InstructorAdded";
        public const string ResourceRequested = "ResourceRequested";
        public const string ClassCancelled = "ClassCancelled";
        public const string ClassRoomChange = "ClassRoomChange";
        public const string NewClassScheduled = "NewClassScheduled";
        public const string LearningObjectDeletedCopy = "LearningObjectDeletedCopy";
        public const string LearningObjectDeletedCourse = "LearningObjectDeletedCourse";
        public const string StudentRegistered = "StudentRegistered";
        public const string StudentUnregistered = "StudentUnregistered";
        public const string TrainingProgramAssigned = "TrainingProgramAssigned";
        public const string MeetingCreatedInternal = "MeetingCreatedInternal";
        public const string MeetingCreatedExternal = "MeetingCreatedExternal";
        public const string MeetingUpdatedInternal = "MeetingUpdatedInternal";
        public const string MeetingUpdatedExternal = "MeetingUpdatedExternal";
        public const string MeetingCancelledInternal = "MeetingCancelledInternal";
        public const string MeetingCancelledExternal = "MeetingCancelledExternal";
        public const string UserRegistered = "UserRegistered";

        #endregion

        #region Scheduled Notifications

        // scheduled
        public const string ScheduledTrainingProgramStarting = "TrainingProgramStarting";
        public const string ScheduledTrainingProgramStarted = "TrainingProgramStarted";

        #endregion

        #region Parsing Regexes

        private static Regex reCollection = new Regex(@"{!foreach (?<placeholder>[\w+|\.|\(|\)]+)}(?<contents>.+)", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
        private static Regex reEnd = new Regex(@"{!?end}", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
        private static Regex reItem = new Regex(@"{!(?!foreach)(?<placeholder>[\w|\.|\(|\)|""]+?)}", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
        private static Regex reMethod = new Regex(@"(?<method>.+?)\((?<params>.*?)\)", RegexOptions.Compiled);

        #endregion

        /// <summary>
        /// Gets additional information about the template, including a list of properties and it's schedules
        /// </summary>
        /// <param name="templateName"></param>
        /// <returns></returns>
        public static TemplateDetail GetTemplateDetails(int templateID)
        {
            return new TemplateDetail(templateID);
        }

        /// <summary>
        /// Returns a list of types that can be specified as top-level objects in a given template
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        public static Type[] GetValidTypesForTemplate(Template template)
        {
            // pre-defined templates
            switch (template.CodeName)
            {
                case TemplateController.MarkedAsAttended:
                    return new Type[] { typeof(Registration) };
                case TemplateController.NewClassScheduled:
                case TemplateController.ClassCancelled:
                case TemplateController.SchedulingChange:
                case TemplateController.InstructorRemoved:
                case TemplateController.InstructorAdded:
                    return new Type[] { typeof(ClassX) };
                case TemplateController.ResourceRequested:
                    return new Type[] { typeof(Resource), typeof(ClassX) };
                case TemplateController.ClassRoomChange:
                    return new Type[] { typeof(ClassX) }; //NOTE: this room needs some add'l info, specifying that it's the original room
                case TemplateController.StudentUnregistered:
                case TemplateController.StudentRegistered:
                    return new Type[] { typeof(Data.User), typeof(ClassX) };
                case TemplateController.TrainingProgramAssigned:
                case TemplateController.ScheduledTrainingProgramStarted:
                case TemplateController.ScheduledTrainingProgramStarting:
                    return new Type[] { typeof(Data.TrainingProgram) };
                //case TemplateController.LearningObjectDeletedCopy:
                //    return new Type[] { typeof(LearningObject) };
                //case TemplateController.LearningObjectDeletedCourse:
                //    return new Type[] { typeof(Artisan.Course), typeof(Artisan.LearningObject) };
                case TemplateController.MeetingCreatedInternal:
                case TemplateController.MeetingCreatedExternal:
                case TemplateController.MeetingUpdatedInternal:
                case TemplateController.MeetingUpdatedExternal:
                case TemplateController.MeetingCancelledInternal:
                case TemplateController.MeetingCancelledExternal:
                    return new Type[] { typeof(GTMInvitation), typeof(GTMMeeting) };
                // customer bb
                case TemplateController.UserRegistered:
                    return new Type[] { typeof(Data.User) };
            }
            // scheduled templates
            if (template.IsScheduled)
            {
                ScheduleDefinition sd = ParseScheduleParameter(template.ScheduleParameterID);
                if (sd != null)
                {
                    return new Type[] { sd.Type };
                }
            }
            return new Type[] { };
        }

        /// <summary>
        /// Given a schedule parameter ID, returns a list of template parameters that can be used with that parameter.
        /// </summary>
        /// <param name="scheduleParameterId"></param>
        /// <returns></returns>
        public static TreeNode GetTemplateParametersForScheduleParameter(int scheduleParameterId)
        {
            ScheduleDefinition sd = ParseScheduleParameter(scheduleParameterId);
            StringBuilder sb = new StringBuilder();
            TreeNode parameters = new TreeNode("");
            if (sd == null)
            {
                return parameters;
            }
            TreeNode tn = TemplateController.GetAllProperties(sd.Type);
            parameters.Nodes.Add(tn);
            return parameters;
        }

        /// <summary>
        /// Given a template, returns a list of template parameters that can be used with that template.
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        public static TreeNode GetTemplateParametersForTemplate(Template template)
        {
            Type[] types = TemplateController.GetValidTypesForTemplate(template);
            StringBuilder sb = new StringBuilder();
            TreeNode templateParameters = new TreeNode(template.DisplayName);
            foreach (Type t in types)
            {
                TreeNode tn = TemplateController.GetAllProperties(t);
                templateParameters.Nodes.Add(tn);
            }
            return templateParameters;
        }

        /// <summary>
        /// Given a schedule parameter ID, returns a list of recipients that can be used with that parameter
        /// </summary>
        /// <param name="scheduleParameterId"></param>
        /// <returns></returns>
        public static RecipientGroupCollection GetRecipientsForScheduleParameter(int scheduleParameterId)
        {
            ScheduleDefinition sd = ParseScheduleParameter(scheduleParameterId);
            if (sd == null)
            {
                return new RecipientGroupCollection();
            }
            return GetRecipientsForType(sd.Type);
        }

        /// <summary>
        /// Gets a list of possible recipients for a given type
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static RecipientGroupCollection GetRecipientsForType(Type t)
        {
            return new RecipientGroupCollection()
                .Where(RecipientGroup.Columns.ValidForType, t.Name)
                .Load();
        }

        public static RecipientGroupCollection GetRecipientsForTemplate(Template template)
        {
            RecipientGroupCollection totalList = new RecipientGroupCollection();
            foreach (Type t in GetValidTypesForTemplate(template))
            {
                foreach (RecipientGroup current in GetRecipientsForType(t))
                {
                    if (!totalList.Exists(total => total.Id == current.Id))
                    {
                        totalList.Add(current);
                    }
                }
            }
            return totalList;
        }

        /// <summary>
        /// Parses the "ScheduleFor" property, validating that it meets all parameters, and doing any cleanup
        /// 
        /// </summary>
        /// <returns></returns>
        public static ScheduleDefinition ParseScheduleParameter(int scheduleParameterID)
        {
            //TODO: need to make this fancier so it works for class dates
            // we want the option to use methods, etc
            // BankersEdge.Classroom.ClassX.ClassDateRecords().StartDateTime
            string err = "";
            ScheduleParameter p = new ScheduleParameter(scheduleParameterID);
            if (p.Id == 0) return null;

            string trimmed = p.Parameter.Trim(new char[] { '{', '}', '!' });

            string[] parts = trimmed.Split('.');
            string typeName = string.Empty;
            // loop through the parts, excluding either the last value (the property)
            // or a method and any subsequent properties
            for (int i = 0; i < parts.Length - 1; i++)
            {
                string part = parts[i];
                if (part.IndexOf("(") > -1)
                {
                    break;
                }
                typeName += part + ".";
            }
            typeName = typeName.Trim('.');

            string additional = trimmed.Substring(typeName.Length + 1);

            Type type = Type.GetType(typeName, false);

            if (type == null)
            {
                foreach (AssemblyName a in Assembly.GetExecutingAssembly().GetReferencedAssemblies())
                {

                    type = Type.GetType(typeName, false);
                    if (type != null)
                        break;
                }
            }

            if (type == null)
            {
                err = string.Format("The specified item, {0}, cannot be located in the current assembly.", typeName);
                Log.Error(err);
                return null;
            }

            Type collectionType = Type.GetType(typeName + "Collection", false);
            if (type == null)
            {
                err = string.Format("The specified item, {0}, cannot be located in the current assembly.", typeName + "Collection");
                Log.Error(err);
                return null;
            }

            return new ScheduleDefinition() { Type = type, CollectionType = collectionType, Property = additional };
        }

        /// <summary>
        /// Gets a list of users for the specified template
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static List<int> GetUsersForTemplate(int templateId, params object[] objects)
        {
            List<int> userIds = new List<int>();
            Template template = new Template(templateId);
            TemplatesToRecipientGroupsMapCollection maps = new TemplatesToRecipientGroupsMapCollection()
                .Where(TemplatesToRecipientGroupsMap.Columns.TemplateID, templateId)
                .Load();
            
            Log.DebugFormat("Found {0} maps for the template.", maps.Count);

            // determine which people are set up to receive notifications for this template
            foreach (TemplatesToRecipientGroupsMap map in maps)
            {
                try
                {
                    RecipientGroup group = new RecipientGroup(map.RecipientGroupID);
                    object currentObject = null;

                    // loop through the objects and find the referenced users
                    foreach (object o in objects)
                    {
                        // templates each have multiple maps. a given map may apply to zero, one, or more objects,
                        // but in all cases right now, only applies to one object, so this'll get hit fairly reguarly
                        // for any templates that take > 1 object
                        if (group.ValidForType != o.GetType().Name)
                        {
                            Log.DebugFormat("The type {0} is not valid for the group with selector {1}, display name {2}", o.GetType(), group.Selector, group.DisplayName);
                            continue;
                        }

                        if (currentObject == null)
                        {
                            currentObject = o;
                        }

                        Log.DebugFormat("Searching for users...");

                        string[] parts = group.Selector.Split('.');

                        for (int i = 0; i < parts.Length; i++)
                        {
                            if (currentObject == null)
                            {
                                // if we ever reach a null object in the chain, just skip it and move on

                                Log.DebugFormat("Null object found.");
                                break;
                            }

                            string currentValue = parts[i];

                            PropertyInfo pi = currentObject.GetType().GetProperty("Id", BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                            string id = pi != null ? pi.GetValue(currentObject, null).ToString() : "Unknown";
                            Log.DebugFormat("Object of type {0} found, id: {1}, searching for property {2}", currentObject.GetType(), id, currentValue);

                            if (currentValue.IndexOf("(") == -1)
                            {
                                if (typeof(ICollection).IsAssignableFrom(currentObject.GetType()))
                                {
                                    foreach (object item in (ICollection)currentObject)
                                    {
                                        pi = item.GetType().GetProperty(currentValue, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                                        if (pi == null)
                                        {
                                            Log.ErrorFormat("The type {0} does not have a property {1}", item.GetType(), currentValue);
                                            continue;
                                        }
                                        if (i == parts.Length - 1)
                                        {
                                            userIds.Add((int)pi.GetValue(item, null));
                                        }
                                        else
                                        {
                                            currentObject = pi.GetValue(item, null);
                                        }
                                    }
                                }
                                else
                                {
                                    pi = currentObject.GetType().GetProperty(currentValue, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                                    if (pi == null)
                                    {
                                        Log.ErrorFormat("The type {0} does not have a property {1}", currentObject.GetType(), currentValue);
                                        continue;
                                    }
                                    if (i == parts.Length - 1)
                                    {
                                        userIds.Add((int)pi.GetValue(currentObject, null));
                                    }
                                    else
                                    {
                                        currentObject = pi.GetValue(currentObject, null);
                                    }
                                }
                            }
                            else
                            {
                                MethodInfo mi = currentObject.GetType().GetMethod(currentValue.Replace("(", "").Replace(")", ""), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                                if (mi == null)
                                {
                                    Log.ErrorFormat("The type {0} does not have a method {1}", currentObject.GetType(), currentValue);
                                    continue;
                                }

                                if (i == parts.Length - 1)
                                {
                                    if (typeof(ICollection).IsAssignableFrom(mi.ReturnType))
                                    {
                                        userIds.AddRange((IEnumerable<int>)mi.Invoke(currentObject, null));
                                    }
                                    else
                                    {
                                        userIds.Add((int)mi.Invoke(currentObject, null));
                                    }
                                }
                                else
                                {
                                    currentObject = mi.Invoke(currentObject, null);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    // errors here should never prevent things fron continuing
                    Log.Error(ex);
                }
            }
            // make sure if the user id is in the list > 1 time (for ex, both a student and a resource manager),
            // that we only send it 1x to them

            Log.DebugFormat("The following user IDs were found: {0}", string.Join(",", userIds.Distinct().ToList().ConvertAll(s => s.ToString()).ToArray()));
            return userIds.Distinct().ToList();
        }

        ///// <summary>
        ///// Gets a list of the user IDs to whom a given message should be sent. Note that we need
        ///// the template because the users change per template, and the objects because the
        ///// users are specific to a given object.
        ///// </summary>
        ///// <param name="template"></param>
        ///// <param name="objects"></param>
        ///// <returns></returns>
        //public static List<int> GetUsersForTemplate(string template, params object[] objects)
        //{
        //    List<int> userIds = new List<int>();
        //    foreach (object o in objects)
        //    {
        //        switch (template)
        //        {
        //            case TemplateController.MarkedAsAttended:
        //                if (o is Registration)
        //                {
        //                    Registration reg = o as Registration;
        //                    userIds.Add(reg.RegistrantID);
        //                }
        //                break;
        //            case TemplateController.NewClassScheduled:
        //            case TemplateController.ClassCancelled:
        //            case TemplateController.SchedulingChange:
        //            case TemplateController.ClassRoomChange:
        //                if (o is ClassX)
        //                {
        //                    ClassX classx = o as ClassX;
        //                    WebWidgetry.SubSonic.Controller c = new WebWidgetry.SubSonic.Controller();

        //                    // ilt managers should know
        //                    userIds.AddRange(c.GetILTManagers().Select(s => int.Parse(s)));

        //                    // instructors should know
        //                    userIds.AddRange(classx.ClassInstructorRecords().Select(ci => ci.InstructorID));

        //                    // students should know
        //                    //ICollection collection = (ICollection)o.GetType().GetMethod("RegistrationRecords").Invoke(o, null);
        //                    //foreach (object item in collection)
        //                    //{
        //                    //    userIds.Add((int)item.GetType().GetProperty("RegistrantID").GetValue(item, null));
        //                    //}
        //                    //userIds.AddRange(classx.RegistrationRecords().Select(r => r.RegistrantID));

        //                    // resource managers should know
        //                    if (classx.Room != null && classx.Room.Venue != null)
        //                    {
        //                        userIds.AddRange(classx.Room.Venue.VenueResourceManagerRecords().Select(vrm => vrm.ResourceManagerID));
        //                    }
        //                }
        //                break;
        //            case TemplateController.InstructorRemoved:
        //            case TemplateController.InstructorAdded:
        //                if (o is ClassX)
        //                {
        //                    ClassX classx = o as ClassX;

        //                    // ilt managers should know
        //                    userIds.AddRange(classx.GetILTManagers());

        //                    // instructors should know
        //                    userIds.AddRange(classx.ClassInstructorRecords().Select(ci => ci.InstructorID));

        //                    // students should know
        //                    userIds.AddRange(classx.RegistrationRecords().Select(r => r.RegistrantID));
        //                }
        //                break;
        //            case TemplateController.ResourceRequested:
        //                if (o is ClassX)
        //                {
        //                    ClassX classx = o as ClassX;

        //                    // resource managers should know
        //                    if (classx.Room != null && classx.Room.Venue != null)
        //                    {
        //                        userIds.AddRange(classx.Room.Venue.VenueResourceManagerRecords().Select(vrm => vrm.ResourceManagerID));
        //                    }
        //                }
        //                break;
        //            case TemplateController.StudentUnregistered:
        //            case TemplateController.StudentRegistered:
        //                if (o is User)
        //                {
        //                    User user = o as User;
        //                    userIds.Add(user.Id);
        //                    if (user.SupervisorID > 0)
        //                        userIds.Add(user.SupervisorID);
        //                }
        //                break;
        //            case TemplateController.TrainingProgramAssigned:
        //            case TemplateController.ScheduledTrainingProgramStarted:
        //            case TemplateController.ScheduledTrainingProgramStarting:
        //                if (o is TrainingProgram)
        //                {
        //                    TrainingProgram tp = o as TrainingProgram;
        //                    userIds.AddRange(tp.GetAssignedUsers());
        //                }
        //                break;
        //            case TemplateController.LearningObjectDeletedCourse:
        //            case TemplateController.LearningObjectDeletedCopy:
        //                if (o is User)
        //                {

        //                }
        //                break;

        //            case TemplateController.MeetingCreatedInternal:
        //            case TemplateController.MeetingCreatedExternal:
        //            case TemplateController.MeetingUpdatedInternal:
        //            case TemplateController.MeetingUpdatedExternal:
        //            case TemplateController.MeetingCancelledInternal:
        //            case TemplateController.MeetingCancelledExternal:
        //                if (o is GTMInvitation)
        //                {
        //                    GTMInvitation invite = o as GTMInvitation;
        //                    userIds.Add(invite.Id);
        //                }
        //                break;
        //            case TemplateController.UserRegistered:
        //                if (o is User)
        //                {
        //                    userIds.AddRange(((User)o).GetCustomerManagers());

        //                }
        //                break;
        //        }
        //    }
        //    return userIds;
        //}

        ///// <summary>
        ///// TODO: merge this method and getusersfortemplate, so we can specify for a given template which users
        ///// should receive the message
        ///// </summary>
        ///// <param name="type"></param>
        ///// <param name="objects"></param>
        ///// <returns></returns>
        //public static List<int> GetUsersForType(Type type, params object[] objects)
        //{
        //    List<int> userIds = new List<int>();
        //    foreach (object o in objects)
        //    {
        //        if (o is Registration)
        //        {
        //            Registration reg = o as Registration;
        //            userIds.Add(reg.RegistrantID);
        //        }
        //        else if (o is ClassX)
        //        {
        //            ClassX classx = o as ClassX;
        //            WebWidgetry.SubSonic.Controller c = new WebWidgetry.SubSonic.Controller();

        //            // instructors should know
        //            userIds.AddRange(classx.ClassInstructorRecords().Select(ci => ci.InstructorID));

        //            // students should know
        //            userIds.AddRange(classx.RegistrationRecords().Select(r => r.RegistrantID));
        //        }
        //        else if (o is User)
        //        {
        //            User user = o as User;
        //            userIds.Add(user.Id);
        //        }
        //        else if (o is TrainingProgram)
        //        {
        //            userIds.AddRange(new TrainingProgramUserCollection()
        //                .Where(TrainingProgramUser.Columns.TrainingProgramID, ((TrainingProgram)o).Id)
        //                .Load()
        //                .Select(tp => tp.UserID)
        //            );
        //        }
        //        else if (o is GTMInvitation)
        //        {
        //            GTMInvitation invite = o as GTMInvitation;
        //            userIds.Add(invite.Id);
        //        }
        //    }
        //    return userIds;
        //}

        /// <summary>
        /// For a given type, lists the properties on that type that are valid for use in a template
        /// </summary>
        /// <param name="type">The type to inspect</param>
        /// <returns>A string enumerating the properties that can be used.</returns>
        private static string[] GetValidPropertiesForTemplateType(Type type)
        {
            if (type == typeof(Registration))
            {
                return new string[] { "Attendance", "Class", "Score", "Status", "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(ClassX))
            {
                return new string[] { "Room", "CapacityOverride", "ClassDates", "Course", "Description", 
                    "InternalCode", "Location", "Name", "Notes", "Resources", "Room", "TimeZone", "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(Resource))
            {
                return new string[] { "Cost", "Name", "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(Room))
            {
                return new string[] { "Capacity", "Name", "NetworkAccessIndicator", "RoomCost", "Venue", "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(Venue))
            {
                return new string[] { "City", "InternalCode", "Name", "Resources", "Rooms", "StateName", "CreatedOn", "ModifiedOn" };
            }
            else if (type == typeof(LearningObject))
            {
                return new string[] { "LearningObjectCode", "LearningObjectName","LearningObjectDescription",
                    "Courses", "CreatedOn", "ModifiedOn", "Pages", "VersionID" };
            }
            else if (type == typeof(Artisan.Course))
            {
                return new string[] { "LearningObjects", "CourseCode", "CourseDescription", "CourseName", "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(Artisan.Page))
            {
                return new string[] { "PageName", "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(User))
            {
                return new string[] { "Audiences", "JobRole", "Location", "Email", "EmployeeNumber", "FirstName", "FullName", 
                    "FullNameReversed", "HireDate", "LastName", "MiddleName", "Notes", "TelephoneNumber", "Username", "Status",
                    "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(TrainingProgram))
            {
                return new string[] { "Name", "Cost", "Description", "DueDate", "EndDate", "EnforceRequiredOrder", "MinimumElectives", 
                    "StartDate", "ModifiedOn", "CreatedOn" };
            }
            else if (type == typeof(GTMInvitation))
            {
                return new string[] { "IsExternal", "CreatedOn", "ModifiedOn" };
            }
            else if (type == typeof(GTMMeeting))
            {
                return new string[] { "Subject", "JoinURL", "ExternalJoinURL", "MaxParticipants", "MeetingType", "StartDateTime", 
                    "EndDateTime", "PasswordRequired", "ConferenceCallInfo", "IsEditable", "CreatedOn", "ModifiedOn" };
            }
            else if (type == typeof(ClassDate))
            {
                return new string[] { "ClassX", "Duration", "StartDate", "StartTime" };
            }
            else if (type == typeof(Classroom.Course))
            {
                return new string[]{ "CourseObjective", "CourseTypeDescription", "Credit", "Description", "InternalCode", "Name", "PublicIndicator",
                    "CreatedOn", "ModifiedOn" };
            }
            else if (type == typeof(Classroom.TimeZone))
            {
                return new string[] { "Description" };
            }
            else if (type == typeof(Location))
            {
                return new string[] { "Address1", "Address2", "City", "Name", "PostalCode", "State", "TelephoneNumber" };
            }
            else if (type == typeof(JobRole))
            {
                return new string[] { "InternalCode", "Name" };
            }
            else if (type == typeof(Audience))
            {
                return new string[] { "InternalCode", "Name" };
            }
            else if (type == typeof(Status))
            {
                return new string[] { "Description" };
            }
            return null;
        }

        /// <summary>
        /// Creates a nicely loaded object instance
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object GetObjectInstance(Type type)
        {
            Dictionary<Type, object> instances = GetSampleInstances();
            if (instances.ContainsKey(type))
            {
                return instances[type];
            }
            return null;
        }

        /// <summary>
        /// Creates a bunch of sample objects for use in the templates
        /// </summary>
        /// <returns></returns>
        private static Dictionary<Type, object> GetSampleInstances()
        {
            Dictionary<Type, object> sampleInstances = new Dictionary<Type, object>();

            UserStatus us = new UserStatus();
            us.Description = "Active";
            us.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(UserStatus), us);

            User u = new User();
            u.FirstName = "John";
            u.LastName = "Doe";
            u.MiddleName = "Q.";
            u.Notes = "Excellent at counting change";
            u.NewHireIndicator = true;
            u.Email = "john.doe@gmail.com";
            u.EmployeeNumber = "867";
            u.HireDate = DateTime.Now.AddDays(-5);
            u.TelephoneNumber = "919-555-5555";
            u.Username = "jdoe";
            u.ModifiedOn = DateTime.Now;
            u.CreatedOn = DateTime.Now.AddSeconds(-500);
            u.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(User), u);

            Status s = new Status();
            s.Description = "Wait List";
            s.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Status), s);

            State st = new State();
            st.Name = "NC";
            st.Description = "North Carolina";
            st.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(State), st);

            Venue venue = new Venue();
            venue.Name = "Green Venue";
            venue.City = "Raleigh";
            venue.InternalCode = "5309";
            venue.ModifiedOn = DateTime.Now;
            venue.CreatedOn = DateTime.Now.AddSeconds(-500);
            venue.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Venue), venue);

            Room room = new Room();
            room.Name = "Blue Room";
            room.Capacity = 25;
            room.NetworkAccessIndicator = true;
            room.RoomCost = 100;
            room.ModifiedOn = DateTime.Now;
            room.CreatedOn = DateTime.Now.AddSeconds(-500);
            room.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Room), room);

            Classroom.Course course = new Classroom.Course();
            course.CourseObjective = "To take the course";
            course.Credit = 10;
            course.Description = "A sample course";
            course.InternalCode = "8675309";
            course.Name = "Sample Course";
            course.PublicIndicator = false;
            course.ModifiedOn = DateTime.Now;
            course.CreatedOn = DateTime.Now.AddSeconds(-500);
            course.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Classroom.Course), course);

            Classroom.CourseType ct = new Classroom.CourseType();
            ct.Description = "Seminar";
            ct.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Classroom.CourseType), ct);

            Resource resource = new Resource();
            resource.Name = "Projector";
            resource.Cost = 5;
            resource.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Resource), resource);

            Classroom.TimeZone tz = new Classroom.TimeZone();
            tz.Description = "EST";
            tz.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Classroom.TimeZone), tz);

            ClassX classx = new ClassX();
            classx.Name = "Sample Class";
            classx.CapacityOverride = 30;
            classx.Description = "This class was created for testing";
            classx.InternalCode = "1592";
            classx.Notes = "Some notes about the class go here.";
            classx.ModifiedOn = DateTime.Now;
            classx.CreatedOn = DateTime.Now.AddSeconds(-500);
            classx.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Classroom.ClassX), classx);

            ClassDate cd = new ClassDate();
            cd.StartDateTime = DateTime.Now.AddDays(10);
            cd.Duration = 4;
            cd.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Classroom.ClassDate), cd);

            Registration reg = new Registration();
            reg.AttendedIndicator = true;
            reg.Score = "95";
            reg.AttendedIndicator = false;
            reg.CreatedOn = DateTime.Now;
            reg.ModifiedOn = DateTime.Now.AddSeconds(1);
            reg.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Classroom.Registration), reg);

            Audience audience = new Audience();
            audience.Name = "My Audience";
            audience.InternalCode = "a1234";
            audience.CreatedOn = DateTime.Now;
            audience.ModifiedOn = DateTime.Now.AddSeconds(1);
            audience.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Audience), audience);

            JobRole jobrole = new JobRole();
            jobrole.Name = "My Job";
            jobrole.InternalCode = "j1234";
            jobrole.CreatedOn = DateTime.Now;
            jobrole.ModifiedOn = DateTime.Now.AddSeconds(1);
            jobrole.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(JobRole), jobrole);

            Location location = new Location();
            location.Address1 = "123 Main Street";
            location.Address2 = "Suite 200";
            location.City = "Some City";
            location.State = "Some State";
            location.Name = "My Location";
            location.TelephoneNbr = "919-555-1234";
            location.CreatedOn = DateTime.Now;
            location.ModifiedOn = DateTime.Now.AddSeconds(1);
            location.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Location), location);

            TrainingProgram tp = new TrainingProgram();
            tp.Name = "My Training Program";
            tp.Cost = 50;
            tp.Description = "A training program needs a description";
            tp.DueDate = DateTime.Now.AddDays(14);
            tp.EndDate = tp.DueDate;
            tp.StartDate = DateTime.Now;
            tp.EnforceRequiredOrder = true;
            tp.MinimumElectives = 5;
            tp.CreatedOn = DateTime.Now;
            tp.ModifiedOn = DateTime.Now.AddSeconds(1);
            tp.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(TrainingProgram), tp);

            GTMInvitation invitation = new GTMInvitation();
            invitation.IsExternal = true;
            invitation.CreatedOn = DateTime.Now.AddMinutes(-5);
            invitation.ModifiedOn = DateTime.Now;
            invitation.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(GTMInvitation), invitation);

            GTMMeeting meeting = new GTMMeeting();
            meeting.Subject = "A meeting!";
            meeting.JoinURL = "http://someurl.com/join/";
            meeting.MaxParticipants = 10;
            meeting.MeetingType = "Scheduled";
            meeting.StartDateTime = DateTime.Now;
            meeting.EndDateTime = meeting.StartDateTime.AddHours(1);
            meeting.PasswordRequired = false;
            meeting.ConferenceCallInfo = "Dial (507) 552-7185, access code 125-701-754";
            meeting.IsEditable = false;
            meeting.CreatedOn = DateTime.Now.AddMinutes(-5);
            meeting.ModifiedOn = DateTime.Now;
            meeting.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(GTMMeeting), meeting);

            ClassInstructor ci = new ClassInstructor();
            ci.ClassX = classx;
            ci.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(ClassInstructor), ci);

            // add the class to the class date
            cd.ClassX = classx;

            // add the class dates to the class
            classx.Room = room;
            classx.TimeZone = tz;
            classx.Course = course;
            classx.ClassDatesWrite = new ClassDateCollection();
            classx.ClassDatesWrite.Add(cd);
            classx.ResourcesWrite = new ResourceCollection();
            classx.ResourcesWrite.Add(resource);
            classx.RegistrationsWrite = new RegistrationCollection();
            classx.RegistrationsWrite.Add(reg);

            // create the registration status and class
            reg.Status = s;
            reg.ClassX = classx;

            // set the venue for the room
            room.Venue = venue;

            // set the venue state resources and rooms
            venue.State = st;
            venue.ResourcesWrite = new ResourceCollection();
            venue.ResourcesWrite.Add(resource);
            venue.RoomsWrite = new RoomCollection();
            venue.RoomsWrite.Add(room);

            // add the audience/jobrole/location
            u.UserStatus = us;
            u.AudiencesWrite = new AudienceCollection();
            u.AudiencesWrite.Add(audience);
            u.JobRole = jobrole;
            u.Location = location;

            course.CourseType = ct;

            Page page = new Page();
            page.PageName = "An Artisan page";
            page.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Page), page);

            LearningObject lo = new LearningObject();
            lo.LearningObjectCode = "lo123";
            lo.LearningObjectName = "The name of the LO";
            lo.LearningObjectDescription = "The description of the LO";
            lo.CreatedOn = DateTime.Now.AddMinutes(-5);
            lo.ModifiedOn = DateTime.Now;
            lo.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(LearningObject), lo);

            Artisan.Course acourse = new Artisan.Course();
            acourse.CourseCode = "c123";
            acourse.CourseName = "The name of the course";
            acourse.CourseDescription = "The description of the course";
            acourse.CreatedOn = DateTime.Now.AddMinutes(-5);
            acourse.ModifiedOn = DateTime.Now;
            acourse.SerializeLinkedTables = true;
            sampleInstances.Add(typeof(Artisan.Course), acourse);

            lo.CoursesWrite = new BankersEdge.Artisan.CourseCollection();
            lo.CoursesWrite.Add(acourse);
            lo.PagesWrite = new PageCollection();
            lo.PagesWrite.Add(page);

            acourse.LearningObjectsWrite = new LearningObjectCollection();
            acourse.LearningObjectsWrite.Add(lo);

            acourse.CourseAuthorNotesWrite = new CourseAuthorNoteCollection();
            acourse.CourseAuthorNotesWrite.Add(new CourseAuthorNote()
            {
                Note = "Some note"
            });

            return sampleInstances;
        }

        /// <summary>
        /// Takes a given type and returns a treenode representing a list of all the properties on that type that can be used in a template
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        internal static TreeNode GetAllProperties(Type t)
        {
            TreeNode root = new TreeNode(t.Name);
            Stack<Type> parentTypes = new Stack<Type>();
            parentTypes.Push(t);
            GetAllProperties(t, root, parentTypes);
            return root;
        }



        /// <summary>
        /// Recursive function that lists out the properties on a specified type and puts them into a nice tree structure.
        /// </summary>
        /// <param name="t">The type to analyze</param>
        /// <param name="node">The parent node</param>
        /// <param name="parentTypes">A list of types that are in the parent path, to avoid infinite recursion</param>
        private static void GetAllProperties(Type t, TreeNode node, Stack<Type> parentTypes)
        {
            if (typeof(ICollection).IsAssignableFrom(t))
            {
                t = Util.GetElementType(t); // make sure its valid...for collections, this will get the element type
            }
            string[] properties = TemplateController.GetValidPropertiesForTemplateType(t);

            if (properties == null)
            {
                throw new Exception("Invalid property found...fix me.");
            }
            foreach (PropertyInfo pi in t.GetProperties())
            {
                // avoid invalid properties...ones we haven't explicitly allowed, and ones that'll cause an infinite loop
                if (!properties.Contains(pi.Name) || parentTypes.Contains(pi.PropertyType) || parentTypes.Contains(Util.GetElementType(pi.PropertyType)))
                {
                    continue;
                }

                // based on where we are, create a new node
                Type propertyType = pi.PropertyType;
                string name = Utility.GetProperName(pi.Name, SubSonic.DataService.Provider);
                TreeNode child = new TreeNode(name);
                // save the type with this node
                child.Tag = propertyType;

                if (propertyType.IsPrimitive || propertyType == typeof(string) || propertyType == typeof(DateTime) || propertyType == typeof(decimal))
                {
                    node.Nodes.Add(child);
                }
                else
                {
                    // push the type onto a stack so we never end up in a recursive loop
                    parentTypes.Push(t);

                    GetAllProperties(propertyType, child, parentTypes);

                    // once we're out, this type can be removed from our stack
                    parentTypes.Pop();

                    // then we can keep going
                    node.Nodes.Add(child);
                }
            }
        }

        /// <summary>
        /// Takes a treenode, and builds (recursively) a template based on that node.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="sb"></param>
        /// <param name="withinCollection"></param>
        private static void BuildTemplateForNode(TreeNode node, StringBuilder sb, bool withinCollection)
        {
            foreach (TreeNode n in node.Nodes)
            {
                string tabs = string.Join("", Enumerable.Repeat("\t", n.Level).ToArray<string>());
                if (n.Nodes.Count == 0)
                {
                    // leaf node, this gets our string value
                    if (!withinCollection)
                    {
                        sb.AppendLine(tabs + "{!" + n.FullPath + "}");
                    }
                    else
                    {
                        string parent = SubSonic.Utilities.Utility.GetProperName(SubSonic.Utilities.Utility.PluralToSingular(n.Parent.Text));
                        sb.AppendLine(tabs + "{!" + parent + "." + n.Text + "}");
                    }
                }
                else
                {
                    // here, we determine if the type is a collection. if it is, we wrap it in a foreach
                    bool isEnumerable = (Util.FindIEnumerable((Type)n.Tag) != null);
                    if (isEnumerable)
                    {
                        string loopOver = n.FullPath;
                        if (withinCollection)
                        {
                            string parent = SubSonic.Utilities.Utility.GetProperName(SubSonic.Utilities.Utility.PluralToSingular(n.Parent.Text));
                            loopOver = parent + "." + n.Text;
                        }
                        sb.AppendLine(tabs + "{!foreach " + loopOver + "}");
                    }
                    BuildTemplateForNode(n, sb, isEnumerable);
                    if (isEnumerable)
                    {
                        sb.AppendLine(tabs + "{end}");
                    }
                }
            }
        }

        /// <summary>
        /// Builds a sample template that uses all the properties on the specified type.
        /// </summary>
        /// <param name="t">The type from which a template will be built.</param>
        /// <param name="sb">The builder into which the template will be placed</param>
        /// <returns>The list of objects used in the template</returns>
        public static string BuildSampleTemplate(Type t)
        {
            StringBuilder sb = new StringBuilder();
            BuildSampleTemplate(t, sb);
            return sb.ToString();
        }

        /// <summary>
        /// Nice overload to allow building up of a string builder
        /// </summary>
        /// <param name="t"></param>
        /// <param name="sb"></param>
        public static void BuildSampleTemplate(Type t, StringBuilder sb)
        {
            // get a list of all the properties for that type
            TreeNode tree = GetAllProperties(t);

            // build a template that matches that list of properties
            TemplateController.BuildTemplateForNode(tree, sb, false);
        }

        /// <summary>
        /// Parses a template
        /// </summary>
        /// <param name="message"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static string Parse(string message, params object[] objects)
        {
            return ParseTemplate(message, false, null, objects).Replace("{end}", "").Replace("_\r\n", "");
        }

        /// <summary>
        /// Parses a template
        /// </summary>
        /// <param name="message"></param>
        /// <param name="useWriteProperties"></param>
        /// <param name="objects"></param>
        /// <returns></returns>
        public static string Parse(string message, bool useWriteProperties, params object[] objects)
        {
            return ParseTemplate(message, useWriteProperties, null, objects).Replace("{end}", "").Replace("_\r\n", "");
        }

        /// <summary>
        /// Takes a string and replaces the contents in the format {![name]} with the appropriate properties
        /// from the specified objects
        /// </summary>
        /// <param name="message">The template message</param>
        /// <param name="objects">The objects the use in the message</param>
        /// <param name="useWriteProperties">If set to true, any time a [PropertyName]Write property is found, it will be used in place of the [PropertyName]
        /// property. This is to enable unit testing.</param>
        /// <returns>A formatted message</returns>
        private static string ParseTemplate(string message, bool useWriteProperties, object source, params object[] objects)
        {
            // then, take care of all the "foreach" loops, so we know those are cleared
            message = reCollection.Replace(message, m =>
            {
                // get the string
                string placeholder = m.Groups["placeholder"].Value;

                object o = ExtractObject(placeholder, 1, useWriteProperties, null, objects);
                if (o != null && o is ICollection)
                {
                    // loop over the collection, formatting each item
                    foreach (object item in (ICollection)o)
                    {
                        return ParseTemplate(m.Groups["contents"].Value, useWriteProperties, item, objects);
                    }
                }
                return "[Unknown " + placeholder.Split('.').Last<string>() + "]";
            });

            foreach (Match match in reItem.Matches(message))
            {
                if (message.IndexOf("{!") > message.IndexOf("{end}") && message.IndexOf("{end}") > -1)
                {
                    return reEnd.Replace(message, "", 1);
                }
                message = reItem.Replace(message, m =>
                {
                    // get the string
                    string placeholder = m.Groups["placeholder"].Value;
                    object o = ExtractObject(placeholder, 1, useWriteProperties, source, objects);

                    if (o != null)
                        return o.ToString();

                    return "[Unknown " + placeholder.Split('.').Last<string>() + "]";
                }, 1);
            }


            return message;
        }

        /// <summary>
        /// Extracts an object from the source properties. If the source is null, it is found by searching
        /// based on the types in the objects list for a type matching the name specified as part of the placeholder.
        /// </summary>
        /// <param name="placeholder">A string indicating the object to be found, such as "ClassX.Name". If the source is specified, the first part
        /// of the property is ignored and the second part is used as the property. If the source is NOT specified (null), then the first part
        /// is used to search the objects list for a corresponding type, and the second part is still used as the property.</param>
        /// <param name="placeholderPartIndex">Since this is a recursive function, this is the index into the placeholder string.</param>
        /// <param name="useWriteProperties">If true, and a "[property]Write" method is found, it will be used in place of the property.
        /// This allows us to use "dummy" values for testing, without having to hit the database.</param>
        /// <param name="source">The object to be analyzed</param>
        /// <param name="objects">A list of possible source objects, if one isn't specified. This allows us to pass in a bunch
        /// of objects for the top level parameters.</param>
        /// <returns>An object instance</returns>
        /// <remarks>
        /// This method is fairly versatile, including the ability to take simple method calls, such as:
        /// ClassX.Cost.ToString("c2"), which will give the class cost nicely formatted to 2 decimals with a currency sign.
        /// </remarks>
        private static object ExtractObject(string placeholder, int placeholderPartIndex, bool useWriteProperties, object source, params object[] objects)
        {
            string[] parts = placeholder.Split('.');

            // first, cache the incoming objects by their type so we can find them easily

            Hashtable typeCache = new Hashtable();
            foreach (object obj in objects)
            {
                if (obj == null)
                    continue;

                string typeName = obj.GetType().Name;
                if (!typeCache.ContainsKey(typeName))
                {
                    typeCache.Add(typeName, obj);
                }
                else
                {
                    throw new Exception("Message formatting currently doesn't handle multiple objects of the same type in the 'objects' parameter.");
                }
            }

            // if no "source" is specified, locate the source based on its type name
            // this is always the case for the first value in a string. for example,
            // if "Class.StartDateTime.ToShortDateString()" is used,
            // the "Class" parameter isn't an object, so it will be looked up in the objects.
            // from then on, we're dealing with concrete objects and can work off those instances.
            if (source == null)
            {
                // first part is the type, and should match the params object passed in
                string typeName = parts[placeholderPartIndex - 1];
                // allow secondary name, so in the template we can use "Class" instead of "ClassX".
                // NOTE: we're using the default provider here, so this will screw up if we use different
                // suffixes for different providers.
                string amendedTypeName = Utility.GetParameterName(typeName, SubSonic.DataService.Provider);

                // locate the object. if it's not there, bail out
                if (!typeCache.ContainsKey(typeName) && !typeCache.ContainsKey(amendedTypeName))
                    return null;

                source = typeCache[typeName] ?? typeCache[amendedTypeName];
            }

            // locate the value   
            if (source == null)
                return null;

            object result = null;

            // for now, we only support passing string literal parameters to methods
            // that lets us do formatting via ToString("[my format]"), but that's about it
            if (parts[placeholderPartIndex].IndexOf(")") > 0)
            {
                // extract out the method name and parameters
                Match match = reMethod.Match(parts[placeholderPartIndex]);
                string[] parameters = null;
                Type[] types = new Type[0];
                if (!match.Success)
                {
                    throw new FormatException("Error parsing template; invalid method structure");
                }

                // check for parameters for the method call
                if (!string.IsNullOrEmpty(match.Groups["params"].Value))
                {
                    // for now, we only accept plain string parameters
                    parameters = match.Groups["params"].Value.Split(',').Select(s => s.Trim('"')).ToArray();
                    // we need an array of types to pass to the method invokation to tell it which
                    // overload to use. since we only accept string types, that's relatively easy...
                    types = new Type[parameters.Length];
                    for (int i = 0; i < types.Length; i++)
                    {
                        types[i] = typeof(String);
                    }
                }

                // find the method
                string methodName = match.Groups["method"].Value;
                MethodInfo mi = source.GetType().GetMethod(methodName, types);
                if (mi == null)
                {
                    mi = source.GetType().GetMethod(Utility.GetParameterName(methodName, SubSonic.DataService.Provider), types);
                    if (mi == null)
                        throw new FormatException("Error parsing template; the method " + parts[placeholderPartIndex] + " could not be found on " + source.GetType() + ".");
                }

                // try and invoke it
                try
                {
                    result = mi.Invoke(source, parameters);
                }
                catch (TargetInvocationException ex)
                {
                    // always throws with an inner exception that has the real error
                    throw ex.InnerException;
                }

                // if we're not done...
                if (placeholderPartIndex != parts.Length - 1)
                {
                    // recurse, adding our new object into the mix
                    result = ExtractObject(placeholder, placeholderPartIndex + 1, useWriteProperties, result, objects);
                }
            }
            else
            {

                // get the property we're looking for
                string propertyName = parts[placeholderPartIndex];
                string alternateName = propertyName + "Write";
                PropertyInfo pi = source.GetType().GetProperty(propertyName);
                if (pi == null)
                {
                    pi = source.GetType().GetProperty(Utility.GetParameterName(propertyName, SubSonic.DataService.Provider));
                    if (pi == null)
                        throw new FormatException("Error parsing template; the property " + parts[placeholderPartIndex] + " could not be found on " + source.GetType() + ".");
                }
                if (useWriteProperties && pi != null && !pi.CanWrite && source.GetType().GetProperty(alternateName) != null)
                {
                    pi = source.GetType().GetProperty(alternateName);
                }

                // our "SerializeLinkedTables" properties screws with property accessors,
                // so here we'll temporarily set it true, then set it back when we're done
                bool previous = false;
                PropertyInfo serializedLinked = source.GetType().GetProperty("SerializeLinkedTables");
                if (serializedLinked != null)
                {
                    previous = (bool)serializedLinked.GetValue(source, null);
                    // temporarily set it to true
                    serializedLinked.SetValue(source, true, null);
                }
                // get the value
                result = pi.GetValue(source, null);
                // set it back to its previous value
                if (serializedLinked != null)
                {
                    serializedLinked.SetValue(source, previous, null);
                }
                if (placeholderPartIndex != parts.Length - 1)
                {
                    // recurse, adding our new object into the mix
                    result = ExtractObject(placeholder, placeholderPartIndex + 1, useWriteProperties, result, objects);
                }
            }
            return result;
        }
    }

}
