﻿using System;
using Symphony.Core.Models;
using SubSonic;
using log4net;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Transactions;
using ArtisanCourseData = Symphony.Core.Data.ArtisanCourse;
using AccreditationBoardData = Symphony.Core.Data.AccreditationBoard;
using ArtisanCourseAccreditationData = Symphony.Core.Data.ArtisanCourseAccreditation;
using ArtisanCourseAccreditationDataCollection = Symphony.Core.Data.ArtisanCourseAccreditationCollection;
using OnlineCourseAccreditationData = Symphony.Core.Data.OnlineCourseAccreditation;
using OnlineCourseAccreditationDataCollection = Symphony.Core.Data.OnlineCourseAccreditationCollection;
using TrainingProgramData = Symphony.Core.Data.TrainingProgram;
using TrainingProgramAccreditationData = Symphony.Core.Data.TrainingProgramAccreditation;
using TrainingProgramAccreditationDataCollection = Symphony.Core.Data.TrainingProgramAccreditationCollection;
using ProfessionData = Symphony.Core.Data.Profession;
using AccreditationBoardProfessionData = Symphony.Core.Data.AccreditationBoardProfession;
using AccreditationBoardProfessionDataCollection = Symphony.Core.Data.AccreditationBoardProfessionCollection;
using AccreditationBoardProfessionWithUnassignedData = Symphony.Core.Data.AccreditationBoardProfessionWithUnassigned;
using AccreditationBoardSurveyAssignmentData = Symphony.Core.Data.AccreditationBoardSurveyAssignment;
using AccreditationBoardSurveyAssignmentDataCollection = Symphony.Core.Data.AccreditationBoardSurveyAssignmentCollection;
using OnlineCourseData = Symphony.Core.Data.OnlineCourse;
using System.Linq;
using Symphony.Core.Extensions;
using RazorEngine;
using RazorEngine.Configuration;

namespace Symphony.Core.Controllers
{
    /// <summary>
    /// AccreditationController is a domain service that performs various CRUD operations on
    /// entities relating to the accreditation module.
    /// </summary>
    public class AccreditationController : SymphonyController
    {
        static ILog Log = LogManager.GetLogger(typeof(AccreditationController));
        
        /// <summary>
        /// Finds the AccreditationBoard with an id equal to the id parameter.
        /// </summary>
        /// <param name="id">Id of the AccreditationBoard to return.</param>
        /// <returns>AccreditationBoard that matches the provided id, wrapped in a SingleResult
        /// object. If no matching entity is found, then null will be returned, also wrapped in a
        /// SingleResult object.</returns>
        public SingleResult<AccreditationBoard> FindAccreditationBoard(int id)
        {
            try
            {
                Log.InfoFormat("Performing operation to find an accreditation board with id {0}.", id);

                var board = Select.AllColumnsFrom<AccreditationBoardData>()
                    .Where(AccreditationBoardData.Columns.Id).IsEqualTo(id)
                    .And(AccreditationBoardData.Columns.IsDeleted).IsEqualTo(false)
                    .ExecuteSingle<AccreditationBoard>();

                if (board != null)
                {
                    Log.InfoFormat("Querying professions for accreditation board with id {0}.", id);

                    var professions = Select.AllColumnsFrom<AccreditationBoardProfessionWithUnassignedData>()
                        .Where(AccreditationBoardProfessionWithUnassignedData.Columns.AccreditationBoardID).IsEqualTo(id)
                        .Or(AccreditationBoardProfessionWithUnassignedData.Columns.AccreditationBoardID).IsNull()
                        .OrderAsc(AccreditationBoardProfessionWithUnassignedData.Columns.Name)
                        .ExecuteTypedList<AccreditationBoardProfession>();

                    Log.InfoFormat("Found {0} professions.", professions.Count);
                    board.Professions = professions;

                    Log.InfoFormat("Querying surveys for accreditation board with id {0}.", id);

                    var surveys = new Select()
                        .From(AccreditationBoardSurveyAssignmentData.Schema)
                        .IncludeColumn(OnlineCourseData.IdColumn)
                        .IncludeColumn(OnlineCourseData.NameColumn)
                        .IncludeColumn(Data.Category.NameColumn.QualifiedName, "Category")
                        .InnerJoin(OnlineCourseData.Schema)
                        .InnerJoin(Data.Category.Schema)
                        .Where(AccreditationBoardSurveyAssignmentData.AccreditationBoardIDColumn).IsEqualTo(id)
                        .OrderAsc(AccreditationBoardSurveyAssignmentData.RankColumn.QualifiedName)
                        .ExecuteTypedList<AccreditationBoardSurvey>();

                    Log.InfoFormat("Found {0} surveys.", surveys.Count);
                    board.Surveys = surveys;
                }
                else
                {
                    Log.DebugFormat("Operation to find an accreditation board returned null.");
                }

                return new SingleResult<AccreditationBoard>(board);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to find an accreditation board.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Queries the database for the set of ArtisanCourseAccreditations that are tied to the
        /// AccreditationBoard. This lists all of the accreditations and their associated courses.
        /// </summary>
        /// <param name="accreditationBoardId">The id of the AccreditationBoard to retrieve accreditations for.</param>
        /// <param name="queryParams">Additional query parameters to filter the set of returned accreditations.
        /// <returns>A PagedResult containing all of the ArtisanCourseAccrediations matching the provided arguments.</returns>
        public PagedResult<ArtisanCourseAccreditation> QueryArtisanCourseAccreditationsForAccreditationBoard(int accreditationBoardId, PagedQueryParams<ArtisanCourseAccreditation> queryParams = null)
        {
            SqlQuery query = null;

            try
            {
                Log.InfoFormat("Performing operation to query artisan course accreditations for AccreditationBoard with id {0}.", accreditationBoardId);

                query = Select.AllColumnsFrom<ArtisanCourseAccreditationData>()
                    .InnerJoin(ArtisanCourseData.Schema)
                    .IncludeColumn(ArtisanCourseData.Columns.Name, "courseName")
                    .Where(ArtisanCourseAccreditationData.Columns.AccreditationBoardID).IsEqualTo(accreditationBoardId)
                    .And(ArtisanCourseAccreditationData.Columns.IsDeleted).IsEqualTo(false);

                if (queryParams == null)
                {
                    Log.DebugFormat("No query parameters specified, creating default set.");
                    queryParams = new PagedQueryParams<ArtisanCourseAccreditation>(null, 0, 10000, null, null);
                }

                if (!string.IsNullOrWhiteSpace(queryParams.SearchText))
                {
                    // nothing to really search on here.
                }

                Log.DebugFormat("queryParams: {0}", queryParams.ToString());

                return new PagedResult<ArtisanCourseAccreditation>(query, queryParams.PageIndex, queryParams.PageSize, queryParams.OrderBy, queryParams.OrderDirection);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query artisan course accreditations for AccreditationBoard." + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query artisan course accreditations for AccreditationBoard.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Queries the database for the set of training program accreditations that are tied to the
        /// accreditation board. This lists all of the accreditations and their associated courses.
        /// </summary>s
        /// <param name="accreditationBoardId">The id of the accreditation board to retrieve accreditations for.</param>
        /// <param name="queryParams">Additional query parameters to filter the set of returned accreditations.
        /// <returns>A PagedResult containing all of the training program accreditations matching the provided arguments.</returns>s
        public PagedResult<TrainingProgramAccreditation> QueryTrainingProgramAccreditationsForAccreditationBoard(int accreditationBoardId, PagedQueryParams<TrainingProgramAccreditation> queryParams = null)
        {
            SqlQuery query = null;

            try
            {
                Log.InfoFormat("Performing operation to query training program accreditations for accreditation board with id {0}.", accreditationBoardId);

                query = Select.AllColumnsFrom<TrainingProgramAccreditationData>()
                    .InnerJoin(TrainingProgramData.Schema)
                    .IncludeColumn(TrainingProgramData.Columns.Name, "trainingProgramName")
                    .Where(TrainingProgramAccreditationData.Columns.AccreditationBoardID).IsEqualTo(accreditationBoardId)
                    .And(TrainingProgramAccreditationData.Columns.IsDeleted).IsEqualTo(false);

                if (queryParams == null)
                {
                    Log.DebugFormat("No query parameters specified, creating default set.");
                    queryParams = new PagedQueryParams<TrainingProgramAccreditation>(null, 0, 10000, null, null);
                }

                if (!string.IsNullOrWhiteSpace(queryParams.SearchText))
                {
                    // nothing to really search on here.
                }

                Log.DebugFormat("queryParams: {0}", queryParams.ToString());

                return new PagedResult<TrainingProgramAccreditation>(query, queryParams.PageIndex, queryParams.PageSize, queryParams.OrderBy, queryParams.OrderDirection);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query training program accreditations for accreditation board." + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query training program accreditations for accreditation board.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Queries the database for the set of AccreditationBoards that match the specified query
        /// parameters.
        /// </summary>
        /// <param name="queryParams">Query parameter object defining what to query.</param>
        /// <returns>The set of AccreditationBoards that match the query parameters, wrapped in a
        /// PagedResult object. If no matching entities are found, then an empty array will be
        /// returned, also wrapped in a PagedResult object.</returns>
        public PagedResult<AccreditationBoard> QueryAccreditationBoards(PagedQueryParams<AccreditationBoard> queryParams)
        {
            try
            {
                Log.Info("Performing operation to query accreditation boards.");

                var query = Select.AllColumnsFrom<AccreditationBoardData>()
                    .Where(AccreditationBoardData.Columns.IsDeleted).IsNotEqualTo(true);
                if (!string.IsNullOrWhiteSpace(queryParams.SearchText))
                {
                    query.And(AccreditationBoardData.Columns.Name).ContainsString(queryParams.SearchText);
                }

                Log.DebugFormat("queryParams: {0}", queryParams.ToString());

                return new PagedResult<AccreditationBoard>(query, queryParams.PageIndex, queryParams.PageSize, queryParams.OrderBy, queryParams.OrderDirection);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query accreditation boards.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Creates a new AccreditationBoard from the provided model data.
        /// </summary>
        /// <param name="model">Model data to use when creating the AccreditationBoard. The model
        /// should not have a positive integer value for its id parameter.</param>
        /// <returns>The newly created AccreditationBoard model. The data will be identical to
        /// what was posted, except that it will have a new database id.</returns>
        public SingleResult<AccreditationBoard> CreateAccreditationBoard(AccreditationBoard model)
        {
            try
            {
                Log.Info("Performing operation to create an accreditation board.");

                if (model.Id > 0)
                {
                    throw new ArgumentException("Cannot create accreditation board when model data already has an associated id.");
                }

                using (var ts = new TransactionScope())
                {
                    var data = new AccreditationBoardData();
                    model.CopyTo(data);

                    data.Save();

                    Log.DebugFormat("New accreditation board created with id {0}.", data.Id);
                    model.Id = data.Id;

                    SyncAccreditationBoardProfessions(data.Id, model.Professions);
                    SyncAccreditationBoardSurveys(data.Id, model.Surveys);

                    ts.Complete();
                }

                return new SingleResult<AccreditationBoard>(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to create an accreditation board.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Updates an existing AccreditationBoard from the provided model data.
        /// </summary>
        /// <param name="model">Model data to update the AccreditationBoard with. This data should
        /// already have an id associated with it.</param>
        /// <returns>The newly created AccreditationBoard model. The data will be identical to
        /// what was posted.</returns>
        public SingleResult<AccreditationBoard> UpdateAccreditationBoard(AccreditationBoard model)
        {
            try
            {
                Log.InfoFormat("Performing operation to update an accreditation board width id {0}", model.Id);

                if (model.Id == 0)
                {
                    throw new ArgumentException("Cannot update an accreditation board when model data does not contain an id.");
                }

                using (var ts = new TransactionScope())
                {
                    var data = new AccreditationBoardData(model.Id);
                    model.CopyTo(data);

                    data.Save();
                    Log.InfoFormat("Updated accreditation board with id {0}.", data.Id);

                    SyncAccreditationBoardProfessions(data.Id, model.Professions);
                    SyncAccreditationBoardSurveys(data.Id, model.Surveys);

                    ts.Complete();
                }

                return new SingleResult<AccreditationBoard>(model);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to update an accreditation board.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        private void SyncAccreditationBoardProfessions(int accreditationBoardId, List<AccreditationBoardProfession> professions)
        {
            new DeleteQuery(AccreditationBoardProfessionData.Schema)
                .WHERE(AccreditationBoardProfessionData.Columns.AccreditationBoardID, accreditationBoardId)
                .Execute();

            var batch = new AccreditationBoardProfessionDataCollection();
            foreach (var profession in professions)
            {
                if (!profession.IsAssigned)
                {
                    continue;
                }

                batch.Add(new AccreditationBoardProfessionData()
                {
                    AccreditationBoardID = accreditationBoardId,
                    ProfessionID = profession.ProfessionID
                });
            }

            batch.SaveAll();
        }

        private void SyncAccreditationBoardSurveys(int accreditationBoardId, List<AccreditationBoardSurvey> surveys)
        {
            new DeleteQuery(AccreditationBoardSurveyAssignmentData.Schema)
                .WHERE(AccreditationBoardSurveyAssignmentData.Columns.AccreditationBoardID, accreditationBoardId)
                .Execute();

            var batch = new AccreditationBoardSurveyAssignmentDataCollection();
            foreach (var survey in surveys)
            {
                batch.Add(new AccreditationBoardSurveyAssignmentData()
                {
                    AccreditationBoardID = accreditationBoardId,
                    OnlineCourseID = survey.Id,
                    Rank = survey.Rank
                });
            }

            batch.SaveAll();
        }

        /// <summary>
        /// Queries the database for the set of surveys that are available to the accreditation board.
        /// </summary>
        /// <param name="id">The id of the accreditation board to retrieve available surveys for.</param>
        /// <param name="excludeIds">A set of ids to exclude from the results. Used to exclude assigned surveys.</param>
        /// <param name="queryParams">Additional query parameters to filter the set of returned surveys.
        /// <returns>A PagedResult containing all of the surveys matching the provided arguments.</returns>
        public PagedResult<AccreditationBoardSurvey> QueryAvailableAccreditationBoardSurveys(int id, List<int> excludeIds, PagedQueryParams<AccreditationBoardSurvey> queryParams = null)
        {
            SqlQuery query = null;

            // accreditation board id isn't used right now. I thought I would need it but we don't really have
            // any criteria to restrict surveys based on it for the moment.

            try
            {
                Log.InfoFormat("Performing operation to query available surveys for accreditation board with id {0}.", id);

                var cols = new TableSchema.TableColumn[]{
                    OnlineCourseData.IdColumn,
                    OnlineCourseData.NameColumn,
                    OnlineCourseData.IsSurveyColumn
                };

                query = new Select(cols)
                    .IncludeColumn(Data.Category.NameColumn.QualifiedName, "Category")
                    .From(OnlineCourseData.Schema)
                    .LeftOuterJoin(Data.Category.Schema)
                    .Where(OnlineCourseData.Columns.IsSurvey).IsEqualTo(true)
                    .And(OnlineCourseData.DuplicateFromIDColumn).IsEqualTo(0);

                if (excludeIds.Count > 0)
                {
                    query = query.And(OnlineCourseData.Columns.Id).NotIn(excludeIds.ToArray());
                }

                if (queryParams == null)
                {
                    Log.DebugFormat("No query parameters specified, creating default set.");
                    queryParams = new PagedQueryParams<AccreditationBoardSurvey>(null, 0, 10000, null, null);
                }

                Log.DebugFormat("queryParams: {0}", queryParams.ToString());

                return new PagedResult<AccreditationBoardSurvey>(query, queryParams.PageIndex, queryParams.PageSize, queryParams.OrderBy, queryParams.OrderDirection);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query available surveys for accreditation board: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query available surveys for accreditation board.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Queries the database for the set of ArtisanCourseAccreditations that are tied to the
        /// ArtisanCourse matching the provided artisanCourseId.
        /// </summary>
        /// <param name="artisanCourseId">The id of the ArtisanCourse to retrieve accreditations for.</param>
        /// <param name="queryParams">Additional query parameters to filter the set of returned
        /// accreditations.
        /// <returns>A PagedResult containing all of the ArtisanCourseAccrediations matching the
        /// provided arguments.</returns>
        public PagedResult<ArtisanCourseAccreditation> QueryArtisanAccreditations(int artisanCourseId, PagedQueryParams<ArtisanCourseAccreditation> queryParams = null)
        {
            SqlQuery query = null;

            try
            {
                Log.InfoFormat("Performing operation to query accreditations for Artisan course with id {0}.", artisanCourseId);

                query = Select.AllColumnsFrom<ArtisanCourseAccreditationData>()
                    .Where(ArtisanCourseAccreditationData.Columns.ArtisanCourseID).IsEqualTo(artisanCourseId)
                    .And(ArtisanCourseAccreditationData.Columns.IsDeleted).IsEqualTo(false);

                if (queryParams == null)
                {
                    Log.DebugFormat("No query parameters specified, creating default set.");
                    queryParams = new PagedQueryParams<ArtisanCourseAccreditation>(null, 0, 10000, null, null);
                }

                if (!string.IsNullOrWhiteSpace(queryParams.SearchText))
                {
                    // nothing to really search on here.
                }

                Log.DebugFormat("queryParams: {0}", queryParams.ToString());

                return new PagedResult<ArtisanCourseAccreditation>(query, queryParams.PageIndex, queryParams.PageSize, queryParams.OrderBy, queryParams.OrderDirection);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query accreditations for artisan course: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query accreditations for Artisan course.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Queries the database for the set of OnlineCourseAccreditations that are tied to the
        /// OnlineCourse matching the provided onlineCourseId.
        /// </summary>
        /// <param name="onlineCourseId">The id of the OnlineCourse to retrieve accreditations for.</param>
        /// <param name="queryParams">Additional query parameters to filter the set of returned
        /// accreditations.
        /// <returns>A PagedResult containing all of the OnlineCourseAccreditations matching the
        /// provided arguments.</returns>
        public PagedResult<OnlineCourseAccreditation> QueryOnlineAccreditations(int onlineCourseId, PagedQueryParams<OnlineCourseAccreditation> queryParams = null)
        {
            SqlQuery query = null;

            try
            {
                Log.InfoFormat("Performing operation to query accreditations for online course with id {0}.", onlineCourseId);

                query = Select.AllColumnsFrom<OnlineCourseAccreditationData>()
                    .LeftOuterJoin(ProfessionData.Schema)
                    .InnerJoin(AccreditationBoardData.Schema)
                    .IncludeColumn(AccreditationBoardData.NameColumn.QualifiedName, "accreditationBoardName")
                    .IncludeColumn(ProfessionData.NameColumn.QualifiedName, "professionName")
                    .Where(OnlineCourseAccreditationData.Columns.OnlineCourseID).IsEqualTo(onlineCourseId)
                    .And(OnlineCourseAccreditationData.Columns.IsDeleted).IsEqualTo(false)
                    .And(AccreditationBoardData.Columns.IsDeleted).IsEqualTo(false);

                if (queryParams == null)
                {
                    Log.DebugFormat("No query parameters specified, creating default set.");
                    queryParams = new PagedQueryParams<OnlineCourseAccreditation>(null, 0, 10000, null, null);
                }

                if (!string.IsNullOrWhiteSpace(queryParams.SearchText))
                {
                    // nothing to really search on here.
                }

                Log.DebugFormat("queryParams: {0}", queryParams.ToString());

                return new PagedResult<OnlineCourseAccreditation>(query, queryParams.PageIndex, queryParams.PageSize, queryParams.OrderBy, queryParams.OrderDirection);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query accreditations for online course: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query accreditations for online course.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Queries the database for the set of training program accreditations that are tied to the
        /// training program matching the provided training program id.
        /// </summary>
        /// <param name="trainingProgramId">The id of the training program to retrieve accreditations for.</param>
        /// <param name="queryParams">Additional query parameters to filter the set of returned accreditations.</param>
        /// <returns>A PagedResult containing all of the training program accreditations matching the
        /// provided arguments.</returns>
        public PagedResult<TrainingProgramAccreditation> QueryTrainingProgramAccreditation(int trainingProgramId, PagedQueryParams<TrainingProgramAccreditation> queryParams = null)
        {
            SqlQuery query = null;

            try
            {
                Log.InfoFormat("Performing operation to query accreditations for training program with id {0}.", trainingProgramId);

                query = Select.AllColumnsFrom<TrainingProgramAccreditationData>()
                    .InnerJoin(AccreditationBoardData.Schema)
                    .IncludeColumn(AccreditationBoardData.NameColumn.QualifiedName, "accreditationBoardName")
                    .Where(TrainingProgramAccreditationData.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                    .And(TrainingProgramAccreditationData.Columns.IsDeleted).IsEqualTo(false)
                    .And(AccreditationBoardData.Columns.IsDeleted).IsEqualTo(false);

                if (queryParams == null)
                {
                    Log.DebugFormat("No query parameters specified, creating default set.");
                    queryParams = new PagedQueryParams<TrainingProgramAccreditation>(null, 0, 10000, null, null);
                }

                if (!string.IsNullOrWhiteSpace(queryParams.SearchText))
                {
                    // nothing to really search on here.
                }

                Log.DebugFormat("queryParams: {0}", queryParams.ToString());

                return new PagedResult<TrainingProgramAccreditation>(query, queryParams.PageIndex, queryParams.PageSize, queryParams.OrderBy, queryParams.OrderDirection);
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query accreditations for training program: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query accreditations for training program.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Return accreditation boards along with the accreditations for a training program and profession
        /// </summary>
        /// <param name="trainingProgramId">Training program id to load the accreditations for</param>
        /// <param name="professionId">Profession id to use</param>
        /// <param name="courseId">Optional - to load accreditations for the course as well</param>
        /// <returns></returns>
        public List<AccreditationBoard> QueryAccreditationBoardsForTrainingProgram(int trainingProgramId, int professionId, int courseId = 0)
        {
            SqlQuery query = null;

            try
            {
                query = Select.AllColumnsFrom<Data.TrainingProgramAccreditationBoard>()
                    .Where(Data.TrainingProgramAccreditationBoard.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                    .And(Data.TrainingProgramAccreditationBoard.Columns.Status).IsEqualTo((int)AccreditationStatus.Accredited)
                    .And(Data.TrainingProgramAccreditationBoard.Columns.StartDate).IsLessThanOrEqualTo(DateTime.UtcNow.ToString())
                    .And(Data.TrainingProgramAccreditationBoard.Columns.ExpiryDate).IsGreaterThanOrEqualTo(DateTime.UtcNow.ToString())
                    .AndExpression(Data.TrainingProgramAccreditationBoard.Columns.ProfessionID).IsEqualTo(professionId)
                        .Or(Data.TrainingProgramAccreditationBoard.Columns.ProfessionID).IsNull() // For boards with no professions
                    .CloseExpression()
                    .OrderDesc(Data.TrainingProgramAccreditationBoard.Columns.AccreditationCreatedOn);

                

                List<Data.TrainingProgramAccreditationBoard> trainingProgramAccreditationBoards = query.ExecuteTypedList<Data.TrainingProgramAccreditationBoard>();
                List<Data.OnlineCourseAccreditationBoard> onlineCourceAccreditationBoards = new List<Data.OnlineCourseAccreditationBoard>();

                if (courseId > 0)
                {
                    query = Select.AllColumnsFrom<Data.OnlineCourseAccreditationBoard>()
                        .Where(Data.OnlineCourseAccreditationBoard.Columns.OnlineCourseID).IsEqualTo(courseId)
                        .And(Data.OnlineCourseAccreditationBoard.Columns.ProfessionID).IsEqualTo(professionId)
                        .And(Data.OnlineCourseAccreditationBoard.Columns.StartDate).IsLessThanOrEqualTo(DateTime.UtcNow.ToString())
                        .And(Data.OnlineCourseAccreditationBoard.Columns.ExpiryDate).IsGreaterThanOrEqualTo(DateTime.UtcNow.ToString())
                        .OrderDesc(Data.OnlineCourseAccreditationBoard.Columns.AccreditationCreatedOn);
                    
                    onlineCourceAccreditationBoards = query.ExecuteTypedList<Data.OnlineCourseAccreditationBoard>();
                }

                Dictionary<int, AccreditationBoard> accreditationBoards = new Dictionary<int, AccreditationBoard>();

                foreach (Data.TrainingProgramAccreditationBoard boardData in trainingProgramAccreditationBoards)
                {
                    if (!accreditationBoards.ContainsKey(boardData.AccreditationBoardID))
                    {
                        // Board data is accreditation board with the accreditation joined
                        AccreditationBoard board = Model.Create<AccreditationBoard>(boardData);
                        board.Id = boardData.AccreditationBoardID;

                        board.OnlineCourseAccreditations = new List<OnlineCourseAccreditation>();
                        board.TrainingProgramAccreditations = new List<TrainingProgramAccreditation>();

                        accreditationBoards.Add(board.Id, board);
                    }

                    //boardData is accreditation and board joined
                    TrainingProgramAccreditation accreditation = Model.Create<TrainingProgramAccreditation>(boardData);
                    
                    accreditation.Id = boardData.AccreditationID;
                    accreditation.Disclaimer = boardData.DisclaimerOverride;

                    accreditationBoards[boardData.AccreditationBoardID].TrainingProgramAccreditations.Add(accreditation);
                }

                foreach (Data.OnlineCourseAccreditationBoard boardData in onlineCourceAccreditationBoards) 
                {
                    if (!accreditationBoards.ContainsKey(boardData.AccreditationBoardID))
                    {
                        // Board data is accreditation board with the accreditation joined
                        AccreditationBoard board = Model.Create<AccreditationBoard>(boardData);
                        board.Id = boardData.AccreditationBoardID;

                        board.OnlineCourseAccreditations = new List<OnlineCourseAccreditation>();
                        board.TrainingProgramAccreditations = new List<TrainingProgramAccreditation>();

                        accreditationBoards.Add(board.Id, board);
                    }

                    //boardData is accreditation and board joined
                    OnlineCourseAccreditation accreditation = Model.Create<OnlineCourseAccreditation>(boardData);

                    accreditation.Id = boardData.AccreditationID;
                    accreditation.Disclaimer = boardData.DisclaimerOverride;

                    accreditationBoards[boardData.AccreditationBoardID].OnlineCourseAccreditations.Add(accreditation);
                }

                return accreditationBoards.Select(b => b.Value).ToList();
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query accreditations for training program: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query accreditation boards training program.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Return professions for training program
        /// </summary>
        /// <param name="trainingProgramId">Training program id to load the accreditations for</param>
        /// <param name="courseId">Optional - to load accreditations for the course as well</param>
        /// <returns></returns>
        public List<Profession> QueryProfessionsForTrainingProgram(int trainingProgramId, int courseId = 0)
        {
            SqlQuery query = null;

            try
            {
                query = new Select()
                    .GroupBy(ProfessionData.IdColumn)
                    .GroupBy(ProfessionData.NameColumn)
                    .From<ProfessionData>()
                    .InnerJoin(AccreditationBoardProfessionData.ProfessionIDColumn, ProfessionData.IdColumn)
                    .LeftOuterJoin(TrainingProgramAccreditationData.AccreditationBoardIDColumn, AccreditationBoardProfessionData.AccreditationBoardIDColumn)
                    .LeftOuterJoin(OnlineCourseAccreditationData.AccreditationBoardIDColumn, AccreditationBoardProfessionData.AccreditationBoardIDColumn)
                    .WhereExpression(TrainingProgramAccreditationData.TrainingProgramIDColumn.QualifiedName).IsEqualTo(trainingProgramId)
                        .And(TrainingProgramAccreditationData.StatusColumn).IsEqualTo((int)AccreditationStatus.Accredited)
                        .And(TrainingProgramAccreditationData.IsDeletedColumn).IsEqualTo(false)
                        .And(TrainingProgramAccreditationData.StartDateColumn).IsLessThanOrEqualTo(DateTime.UtcNow.ToString())
                        .And(TrainingProgramAccreditationData.ExpiryDateColumn).IsGreaterThanOrEqualTo(DateTime.UtcNow.ToString())
                    .OrExpression(OnlineCourseAccreditationData.OnlineCourseIDColumn.QualifiedName).IsEqualTo(courseId)
                        .And(OnlineCourseAccreditationData.IsDeletedColumn).IsEqualTo(false)
                        .And(OnlineCourseAccreditationData.StartDateColumn).IsLessThanOrEqualTo(DateTime.UtcNow.ToString())
                        .And(OnlineCourseAccreditationData.ExpiryDateColumn).IsGreaterThanOrEqualTo(DateTime.UtcNow.ToString());
                
                List<Profession> professions = query.ExecuteTypedList<Profession>();

                return professions;
            }
            catch (SqlQueryException ex)
            {
                Log.Fatal("Fatal SQL error while performing operation to query accreditations for training program: " + query.ToString(), ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to query accreditation boards training program.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Synchronizes a list of ArtisanCourseAccreditations to the database. Performs all
        /// necessary INSERT and UPDATE queries. ArtisanCourseAccreditations are soft-deleted, so no
        /// DELETE operations will be performed.
        /// </summary>
        /// <param name="artisanCourseId">Id of the ArtisanCourse to sync these accreditations for.
        /// All accreditations will be assigned this id before the sync.</param>
        /// <param name="accreditations">The set of ArtisanCourseAccreditations that should be updated
        /// in the database.</returns>
        public void SyncArtisanAccreditations(int artisanCourseId, List<ArtisanCourseAccreditation> accreditations)
        {
            try
            {
                Log.InfoFormat("Performing operation to sync accreditations for Artisan course with id {0}.", artisanCourseId);

                var batch = new ArtisanCourseAccreditationDataCollection();

                foreach (ArtisanCourseAccreditation accreditation in accreditations)
                {
                    accreditation.ArtisanCourseId = artisanCourseId;

                    if (accreditation.IsDeleted)
                    {
                        Log.DebugFormat("Accreditation with id {0} will be soft-deleted.", accreditation.Id);
                    }
                    else if (accreditation.Id == 0)
                    {
                        Log.DebugFormat("Accreditation will be created.");
                    }
                    else
                    {
                        Log.DebugFormat("Accreditation with id {0} will be updated.", accreditation.Id);
                    }

                    string error;
                    if (!accreditation.IsValid(out error))
                    {
                        throw new InvalidOperationException(error);
                    }

                    batch.Add(accreditation.ToDataObject());
                }

                batch.BatchSave();
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to sync accreditations for Artisan course.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Synchronizes a list of OnlineCourseAccreditations to the database. Performs all
        /// necessary INSERT and UPDATE queries. OnlineArtisanAccreditations are soft-deleted, so no
        /// DELETE operations will be performed.
        /// </summary>
        /// <param name="artisanCourseId">Id of the OnlineCourse to sync these accreditations for.
        /// All accreditations will be assigned this id before the sync.</param>
        /// <param name="accreditations">The set of OnlineCourseAccreditations that should be updated
        /// in the database.</returns>
        public void SyncOnlineAccreditations(int onlineCourseId, List<OnlineCourseAccreditation> accreditations)
        {
            try
            {
                Log.InfoFormat("Performing operation to sync accreditations for online course with id {0}.", onlineCourseId);

                var batch = new OnlineCourseAccreditationDataCollection();

                foreach (OnlineCourseAccreditation accreditation in accreditations)
                {
                    accreditation.OnlineCourseId = onlineCourseId;

                    if (accreditation.Id == 0)
                    {
                        Log.DebugFormat("Accreditation will be created.");
                    }
                    else
                    {
                        Log.DebugFormat("Accreditation with id {0} will be updated.", accreditation.Id);
                    }

                    string error;
                    if (!accreditation.IsValid(out error))
                    {
                        throw new InvalidOperationException(error);
                    }

                    batch.Add(accreditation.ToDataObject());
                }

                batch.BatchSave();
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to sync accreditations for online course.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Synchronizes a list of training program accreditations to the database. Performs all
        /// necessary INSERT and UPDATE queries. Training program accreditations are soft-deleted,
        /// so no DELETE operations will be performed.
        /// </summary>
        /// <param name="trainingProgramId">Id of the training program to sync these accreditations
        /// for. All accreditations will be assigned this id before the sync.</param>
        /// <param name="accreditations">The set of training program accreditatinos that should be
        /// updated in the database.</returns>
        public void SyncTrainingProgramAccreditations(int trainingProgramId, List<TrainingProgramAccreditation> accreditations)
        {
            try
            {
                Log.InfoFormat("Performing operation to sync accreditations for training program with id {0}.", trainingProgramId);

                var batch = new TrainingProgramAccreditationDataCollection();

                foreach (TrainingProgramAccreditation accreditation in accreditations)
                {
                    accreditation.TrainingProgramId = trainingProgramId;

                    if (accreditation.Id == 0)
                    {
                        Log.DebugFormat("Accreditation will be created.");
                    }
                    else
                    {
                        Log.DebugFormat("Accreditation with id {0} will be updated.", accreditation.Id);
                    }

                    string error;
                    if (!accreditation.IsValid(out error))
                    {
                        throw new InvalidOperationException(error);
                    }

                    batch.Add(accreditation.ToDataObject());
                }

                batch.BatchSave();
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while performing operation to sync accreditations for training program.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        /// <summary>
        /// Parses a SCORM 1.2 manifest file and returns a list of ArtisanCourseAccreditations.
        /// </summary>
        /// <param name="manifestPath">Path to the manifest file.</param>
        /// <returns>The ArtisanCourseAccreditations stored in the manifest file.</returns>
        public MultipleResult<ArtisanCourseAccreditation> ParseManifestAccreditations(string manifestPath)
        {
            try
            {
                manifestPath = Path.Combine(manifestPath, "imsmanifest.xml");

                Log.InfoFormat("Attempting to parse accreditations from manifest at {0}", manifestPath);

                if (!File.Exists(manifestPath))
                {
                    throw new FileNotFoundException("Could not find manifest file.", manifestPath);
                }

                List<ArtisanCourseAccreditation> data;
                using (var file = new FileStream(manifestPath, FileMode.Open))
                {
                    using (var xr = new XmlTextReader(file))
                    {
                        data = ParseManifestAccreditations(xr);
                    }
                }

                return new MultipleResult<ArtisanCourseAccreditation>(data);
            }
            catch (Exception ex)
            {
                Log.Fatal("Fatal error while attempting to parse accreditations from manifest.", ex);
#if DEBUG
                Debugger.Break();
#endif
                throw;
            }
        }

        private List<ArtisanCourseAccreditation> ParseManifestAccreditations(XmlTextReader xr)
        {
            // In the XML file, these are represented under the top level metadata element like so:
            //  <metadata>
            //      <artisan:accreditations>
            //          <artisan:accreditation artisan:id="1" artisan:artisanCourseId="1" artisan:accreditationBoardId="1">
            //              <artisan:startDate>yyyy-MM-dd HH:ii:ss</artisan:startDate>
            //              <artisan:endDate>yyyy-MM-dd HH:ii:ss</artisan:endDate>
            //              <artisan:accreditationCode>SYM-1703</artisan:accreditationCode>
            //              <artisan:creditHours>1.0</artisan:creditHours>
            //              <artisan:creditHoursLabel>Credit Hours</artisan:creditHoursLabel>
            //              <artisan:disclaimer></artisan:disclaimer>
            //          </artisan:accreditation>
            //      </artisan:accreditations>
            //  </metadata>

            bool result;
            var data = new List<ArtisanCourseAccreditation>();

            Log.DebugFormat(@"Trying to find ""artisan:accreditations"" element. ({0}, {1})", xr.LineNumber, xr.LinePosition);

            result = xr.ReadToFollowing("artisan:accreditations");
            if (!result)
            {
                Log.Error("No artisan:accreditations manifest element found.");
                return data;
            }

            Log.DebugFormat(@"Found ""artisan:accreditations"" element. ({0}, {1})", xr.LineNumber, xr.LinePosition);

            int state = 0;
            int elFound = 0;
            var current = new ArtisanCourseAccreditation();

            // Simple FSM with two states.
            //
            // State == 0 means that we're outside of an artisan:accreditation element and we
            // are looking for the next one.
            //
            // State == 1 means that we're inside of an artisan:accreditation element and we are
            // parsing elemnents related to that accreditation.
            //
            // Big block of code but it's dead simple. Use bits to keep track of which elements are
            // found for validation. Could have used a bunch of booleans but I thought this was
            // cleaner.
            while (!xr.EOF)
            {
                if (state == 0)
                {
                    // Read to the next accreditation start tag, if one exists.
                    result = xr.ReadToFollowing("artisan:accreditation");
                    if (!result)
                    {
                        Log.DebugFormat(@"No more ""artisan:accreditation"" elements. Ending parse. ({0}, {1})", xr.LineNumber, xr.LinePosition);
                        break;
                    }

                    Log.DebugFormat(@"Found ""artisan:accreditation"" element. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                    int attrFound = 0;
                    if (xr.HasAttributes)
                    {
                        // Iterate over all of the attributes of this element, which will be an
                        // artisan:accreditation element.
                        while (xr.MoveToNextAttribute())
                        {
                            switch (xr.Name)
                            {
                                case "artisan:id":
                                    Log.DebugFormat(@"Found ""artisan:id"" attribute. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                                    current.Id = Convert.ToInt32(xr.Value);
                                    attrFound |= 1;
                                    break;

                                case "artisan:accreditationBoardId":
                                    Log.DebugFormat(@"Found ""artisan:accreditationBoardId"" attribute. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                                    current.AccreditationBoardId = Convert.ToInt32(xr.Value);
                                    attrFound |= 2;
                                    break;

                                case "artisan:artisanCourseId":
                                    Log.DebugFormat(@"Found ""artisan:artisanCourseId"" attribute. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                                    current.ArtisanCourseId = Convert.ToInt32(xr.Value);
                                    attrFound |= 4;
                                    break;

                                case "artisan:professionId":
                                    Log.DebugFormat(@"Found ""artisan:professionId"" attribute. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                                    current.ProfessionId = Convert.ToInt32(xr.Value);
                                    break;
                            }
                        }
                    }

                    // Validate we found all the expected attributes.
                    if ((attrFound & 1) == 0)
                    {
                        throw new Exception(string.Format(@"The ""artisan:accreditatons"" element did not have the ""artisan:id"" attribute. ({0}, {1})", xr.LineNumber, xr.LinePosition));
                    }
                    if ((attrFound & 2) == 0)
                    {
                        throw new Exception(string.Format(@"The ""artisan:accreditatons"" element did not have the ""artisan:accreditationBoardId"" attribute. ({0}, {1})", xr.LineNumber, xr.LinePosition));
                    }
                    if ((attrFound & 4) == 0)
                    {
                        throw new Exception(string.Format(@"The ""artisan:accreditatons"" element did not have the ""artisan:accreditationCourseId"" attribute. ({0}, {1})", xr.LineNumber, xr.LinePosition));
                    }

                    // Enter the element and reset the elFound flags object to 0, we use this to
                    // track if we find all required elements.
                    xr.ReadStartElement();
                    state = 1;
                    elFound = 0;
                }
                else
                {
                    xr.Read();

                    // Keep reading until we hit an element. For anyone who hasn't worked with XML
                    // readers before, whitespace and other stuff are counted as nodes, so we skip
                    // over all of those.
                    if (xr.NodeType == XmlNodeType.Element)
                    {
                        // ReadElementContentAsString will advance the reader, so get the name of
                        // the element first.
                        string name = xr.Name;
                        string value = xr.ReadElementContentAsString();

                        Log.DebugFormat(@"At element ""{0}"" with raw value {1}. ({2}, {3})", name, value, xr.LineNumber, xr.LinePosition);

                        switch (name)
                        {
                            case "artisan:startDate":
                                Log.DebugFormat(@"Found ""artisan:startDate"" element. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                                current.StartDate = DateTime.Parse(value);
                                elFound |= 1;
                                break;

                            case "artisan:expiryDate":
                                Log.DebugFormat(@"Found ""artisan:expiryDate"" element. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                                current.ExpiryDate = DateTime.Parse(value);
                                elFound |= 2;
                                break;

                            case "artisan:accreditationCode":
                                Log.DebugFormat(@"Found ""artisan:accreditationCode"" element. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                                current.AccreditationCode = value;
                                elFound |= 4;
                                break;

                            case "artisan:creditHours":
                                Log.DebugFormat(@"Found ""artisan:creditHours"" element. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                                current.CreditHours = Convert.ToSingle(value);
                                elFound |= 8;
                                break;

                            case "artisan:creditHoursLabel":
                                Log.DebugFormat(@"Found ""artisan:creditHoursLabel"" element. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                                current.CreditHoursLabel = value;
                                elFound |= 16;
                                break;

                            case "artisan:disclaimer":
                                Log.DebugFormat(@"Found ""artisan:disclaimer"" element. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                                current.Disclaimer = value;
                                elFound |= 32;
                                break;

                            case "artisan:courseNumber":
                                Log.DebugFormat(@"Found ""artisan:courseNumber"" element. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                                current.CourseNumber = value;
                                elFound |= 64;
                                break;
                        }
                    }
                    else if (xr.NodeType == XmlNodeType.EndElement)
                    {
                        Log.DebugFormat(@"Exiting ""artisan:accreditation"" element. ({0}, {1})", xr.LineNumber, xr.LinePosition);

                        // Validate that we found all the required elements.
                        if ((elFound & 1) == 0)
                        {
                            throw new Exception(string.Format(@"The ""artisan:startDate"" element was not found. ({0}, {1})", xr.LineNumber, xr.LinePosition));
                        }
                        if ((elFound & 2) == 0)
                        {
                            throw new Exception(string.Format(@"The ""artisan:expiryDate"" element was not found. ({0}, {1})", xr.LineNumber, xr.LinePosition));
                        }
                        if ((elFound & 4) == 0)
                        {
                            throw new Exception(string.Format(@"The ""artisan:accreditationCode"" element was not found. ({0}, {1})", xr.LineNumber, xr.LinePosition));
                        }
                        if ((elFound & 8) == 0)
                        {
                            throw new Exception(string.Format(@"The ""artisan:creditHours"" element was not found. ({0}, {1})", xr.LineNumber, xr.LinePosition));
                        }
                        if ((elFound & 16) == 0)
                        {
                            throw new Exception(string.Format(@"The ""artisan:creditHoursLabel"" element was not found. ({0}, {1})", xr.LineNumber, xr.LinePosition));
                        }
                        if ((elFound & 32) == 0)
                        {
                            throw new Exception(string.Format(@"The ""artisan:disclaimer"" element was not found. ({0}, {1})", xr.LineNumber, xr.LinePosition));
                        }

                        // Add our current data item and start again. The login in the state == 0
                        // branch will terminate the loop if there are no further accreditations to
                        // be found.
                        data.Add(current);
                        current.Status = (int)AccreditationStatus.Accredited;

                        current = new ArtisanCourseAccreditation();

                        state = 0;
                        xr.Read();
                        continue;
                    }
                }
            }

            return data;
        }

        /// <summary>
        /// Converts a list of ArtisanCourseAccreditations into corresponding OnlineCourseAccreditations.
        /// Most of the work is done by the model itself. This is a small sugar to make converting
        /// between the two types of accreditations easier.
        /// 
        /// Note that this does not perform any actions on the database. If you want to sync the
        /// new OnlineCourseAccreditations, call SyncONlineCourseAccreditations afterwards.
        /// </summary>
        /// <param name="onlineCourseId">Id of the OnlineCourse that the accreditations are being
        /// assigned to. Not the artisan course id.</param>
        /// <param name="artisanAccreditations">List of Artisan accreditations to convert.</param>
        /// <returns>List of new OnlineCourseAccreditations.</returns>
        public MultipleResult<OnlineCourseAccreditation> ConvertFromArtisan(int onlineCourseId, List<ArtisanCourseAccreditation> artisanAccreditations)
        {
            var list = new List<OnlineCourseAccreditation>();

            foreach (var artisanAccreditation in artisanAccreditations)
            {
                var onlineAccreditation = new OnlineCourseAccreditation
                {
                    OnlineCourseId = onlineCourseId,
                    ArtisanCourseAccreditationId = artisanAccreditation.Id
                };

                string error;
                if (onlineAccreditation.CopyFromArtisan(artisanAccreditation, out error))
                {
                    list.Add(onlineAccreditation);
                }
                else
                {
                    Log.Error(error);
                }
            }

            return new MultipleResult<OnlineCourseAccreditation>(list);
        }

        /// <summary>
        /// Given a set of artisan course accreditations and online course accreditations, update
        /// the online course accreditations with new accreditation data.
        /// 
        /// The use case for this method is for updating a course with a new package, so in that
        /// case, the deleted artisan accreditations will not be here. We'll assume that every
        /// existing accreditation that hasn't been matched yet is deleted.
        ///
        /// If you use this method for anything else, it'll be fine as well, because both of
        /// these entities are soft-delete and the deleted status will be handled by the update
        /// operation above.
        /// </summary>
        /// <param name="onlineCourseId">The id of the online course to be updated.</param>
        /// <param name="onlineAccreditations">The set of existing online course accreditations that should be updated.</param>
        /// <param name="artisanAccreditations">The set of new artisan course accreditations to update with.</param>
        public void UpdateOnlineAccreditations(int onlineCourseId, List<OnlineCourseAccreditation> onlineAccreditations, List<ArtisanCourseAccreditation> artisanAccreditations)
        {
            var batch = new OnlineCourseAccreditationDataCollection();

            var hash = new Dictionary<int, OnlineCourseAccreditation>();
            foreach (var onlineAccreditation in onlineAccreditations)
            {
                hash[onlineAccreditation.ArtisanCourseAccreditationId] = onlineAccreditation;
            }

            foreach (var artisanAccreditation in artisanAccreditations)
            {
                // Update operations.
                if (hash.ContainsKey(artisanAccreditation.Id))
                {
                    var onlineAccreditation = hash[artisanAccreditation.Id];
                    var onlineAccreditationData = new OnlineCourseAccreditationData(onlineAccreditation.Id);

                    string error;
                    if (!onlineAccreditation.CopyFromArtisan(artisanAccreditation, out error))
                    {
                        throw new InvalidOperationException(error);
                    }

                    onlineAccreditation.CopyTo(onlineAccreditationData);

                    hash.Remove(artisanAccreditation.Id);
                    batch.Add(onlineAccreditationData);
                }
                // Create operations.
                else
                {
                    var onlineAccreditation = new OnlineCourseAccreditation
                    {
                        OnlineCourseId = onlineCourseId
                    };
                    var onlineAccreditationData = new OnlineCourseAccreditationData();

                    string error;
                    if (!onlineAccreditation.CopyFromArtisan(artisanAccreditation, out error))
                    {
                        throw new InvalidOperationException(error);
                    }

                    onlineAccreditation.CopyTo(onlineAccreditationData);

                    batch.Add(onlineAccreditationData);
                }
            }

            foreach (var kvp in hash)
            {
                var onlineAccreditation = kvp.Value;
                var onlineAccreditationData = new OnlineCourseAccreditationData(onlineAccreditation.Id);

                onlineAccreditation.IsDeleted = true;
                onlineAccreditation.CopyTo(onlineAccreditationData);

                batch.Add(onlineAccreditationData);
            }

            batch.BatchSave();
        }

        private Dictionary<int, AccreditationBoard> _BoardCache = new Dictionary<int, AccreditationBoard>();

        /// <summary>
        /// Returns the appropriate disclaimer for an artisan course accreditation. If the
        /// accreditation does not have a disclaimer, the accreditation board's one will be used.
        /// </summary>
        /// <param name="accreditation">The accreditation to return the disclaimer for.</param>
        public void ResolveDisclaimer(ArtisanCourseAccreditation accreditation)
        {
            if (accreditation.Disclaimer != null)
            {
                return;
            }

            if (!_BoardCache.ContainsKey(accreditation.AccreditationBoardId))
            {
                var result = FindAccreditationBoard(accreditation.AccreditationBoardId);
                _BoardCache[accreditation.AccreditationBoardId] = result.Data;
            }

            accreditation.Disclaimer = _BoardCache[accreditation.AccreditationBoardId].Disclaimer;
        }

        /// <summary>
        /// Returns the appropriate disclaimer for a training program accreditation. If the
        /// accreditation does not have a disclaimer, the accreditation board's one will be used.
        /// </summary>
        /// <param name="accreditation">The accreditation to return the disclaimer for.</param>
        public void ResolveDisclaimer(TrainingProgramAccreditation accreditation)
        {
            if (accreditation.Disclaimer != null)
            {
                return;
            }

            if (!_BoardCache.ContainsKey(accreditation.AccreditationBoardId))
            {
                var result = FindAccreditationBoard(accreditation.AccreditationBoardId);
                _BoardCache[accreditation.AccreditationBoardId] = result.Data;
            }

            accreditation.Disclaimer = _BoardCache[accreditation.AccreditationBoardId].Disclaimer;
        }

        public SingleResult<OnlineCourseAccreditation> DeleteAccreditationBoard(int id)
        {
            Data.AccreditationBoard.Delete(id);
            var ab = new Data.AccreditationBoard(id);

            var oca = new OnlineCourseAccreditation();

            oca.CopyFrom(ab);

            return new SingleResult<OnlineCourseAccreditation>(oca); // new SingleResult<OnlineCourseAccreditation>();
        }

        public string GetAccreditationHtml(AccreditationDocument accreditationDocument)
        {
            var config = new TemplateServiceConfiguration();
            config.EncodedStringFactory = new RazorEngine.Text.RawStringFactory();
            var service = new RazorEngine.Templating.TemplateService(config);
            

            string baseLayoutPath = System.Web.HttpContext.Current.Server.MapPath("/Accreditation/accreditation_template.html");
            string baseLayoutName = "accreditation";
            string baseLayout = System.IO.File.ReadAllText(baseLayoutPath);

            Razor.SetTemplateService(service);
            Razor.Compile(baseLayout, typeof(AccreditationDocument), baseLayoutName);

            string wrapperLayout = "/Accreditation/accreditation_template_wrapper.html";
            string layoutPath = System.Web.HttpContext.Current.Server.MapPath(wrapperLayout);
            string layout = System.IO.File.ReadAllText(layoutPath);

            return Razor.Parse<AccreditationDocument>(layout, accreditationDocument);
        }
    }
}
