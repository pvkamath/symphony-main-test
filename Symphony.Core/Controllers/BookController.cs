﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Symphony.Core.Models;
using SubSonic;
using Symphony.Web;
using System.Web.Security;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using SubSonic.Sugar;
using System.Data;
using log4net;
using System.Web;
using System.Net;
using System.ServiceModel.Web;
using Symphony.Core.Extensions;
using Symphony.Core.Comparers;
using System.Transactions;

namespace Symphony.Core.Controllers
{
    public class BookController : SymphonyController
    {
        private ILog Log = LogManager.GetLogger(typeof(BookController));

        public PagedResult<Book> FindAllBooks(int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Book>();

            if (!string.IsNullOrEmpty(searchText))
            {
                query.And(Data.Network.Columns.Name).ContainsString(searchText);
            }

            return new PagedResult<Book>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<Book> GetBook(int bookId)
        {
            Book book = Model.Create<Book>(new Symphony.Core.Data.Book(bookId));
            return new SingleResult<Book>(book);
        }

        public PagedResult<Book> FindTrainingProgramBooks(int trainingProgramId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.Book>()
                .InnerJoin(Data.Tables.TrainingProgramBook, Data.TrainingProgramBook.Columns.BookId, Data.Tables.Book, Data.Book.Columns.Id)
                .Where(Data.TrainingProgramBook.TrainingProgramIdColumn).IsEqualTo(trainingProgramId)
                .And(Data.Book.NameColumn).Like("%" + (searchText ?? string.Empty) + "%");

            return new PagedResult<Book>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        public SingleResult<Book> SaveBook(int bookId, Book book)
        {
            Data.Book data = new Symphony.Core.Data.Book(bookId);
            if (bookId == 0)
            {
                data = new Data.Book { Name = book.Name, Author = book.Author, Publisher = book.Publisher, Isbn = book.Isbn, TypeFormat = book.TypeFormat };
                data.Save();
            }
            else 
            {
                data.Author = book.Author;
                data.TypeFormat = book.TypeFormat;
                data.Name = book.Name;
                data.Publisher = book.Publisher;
                data.Isbn = book.Isbn;
                data.Save();
            }
            return new SingleResult<Book>(Model.Create<Book>(data));
        }

    }
}
