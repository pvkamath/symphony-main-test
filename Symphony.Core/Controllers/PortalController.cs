﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using Symphony.Core;
using SubSonic;
using Symphony.Core.Data;
using System.IO;
using log4net;
using Symphony.Core.Models;
using Symphony.Core.Extensions;
using Symphony.Core.Interfaces;

namespace Symphony.Core.Controllers
{
    public class PortalController : SymphonyController
    {
        private ILog Log = LogManager.GetLogger(typeof(PortalController));

        /// <summary>
        /// Gets a list of public documents for the specified customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="searchText"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedResult<Models.PublicDocument> FindPublicDocumentsForCustomer(int customerId, int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            return new CourseAssignmentController().FindPublicDocuments(customerId, userId, searchText, orderBy, orderDir, pageIndex, pageSize);
        }

        /// <summary>
        /// Gets a list of public courses, and their associated scores, for a given user.
        /// </summary>
        /// <remarks>
        /// Note that we still need to do some sort of accounting for scores by looking at the dates. For example,
        /// if you have a transcript that shows you completed Course #1, because it was in a TP, we need to somehow
        /// account for that here.
        /// </remarks>
        /// <param name="userId">The id of the user for whome we're finding courses</param>
        /// <param name="searchText">The text to filter on</param>
        /// <param name="orderBy">The column to sort by</param>
        /// <param name="orderDir">The direction to sort</param>
        /// <param name="pageIndex">The 0-based page index</param>
        /// <param name="pageSize">The number of items to retrieve</param>
        /// <returns>A paged list of courses</returns>
        public PagedResult<Models.PublicCourse> FindPublicCoursesForUser(int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, int[] courseIds = null)
        {
            int customerId = new Data.User(userId).CustomerID;

            // get the public course list
            SqlQuery query = Select.AllColumnsFrom<Data.SimplePublicCourse>()
                .WhereExpression(Data.SimplePublicCourse.Columns.CustomerID).IsEqualTo(customerId)
                .And(Data.SimplePublicCourse.Columns.IsThirdParty).IsEqualTo(false)
                .CloseExpression();

            if (!string.IsNullOrEmpty(searchText))
            {
                query.AndExpression(Data.SimplePublicCourse.Columns.Name).ContainsString(searchText)
                    .Or(Data.SimplePublicCourse.Columns.Description).ContainsString(searchText)
                    .Or(Data.SimplePublicCourse.Columns.Keywords).ContainsString(searchText)
                    .CloseExpression();
            }

            if (courseIds != null && courseIds.Length > 0)
            {
                query.AndExpression(Data.SimplePublicCourse.Columns.Id).In(courseIds)
                    .CloseExpression();
            }

            // get the total
            int total = query.GetRecordCount();

            // order and page the query and re-execute
            orderBy = orderBy ?? Utilities.GetSortOrder(typeof(Models.PublicCourse));
            if (orderDir == OrderDirection.Descending)
            {
                query = query.OrderDesc(orderBy);
            }
            else
            {
                query = query.OrderAsc(orderBy);
            }
            List<Models.PublicCourse> courses = query.Paged(pageIndex + 1, pageSize).ExecuteTypedList<Models.PublicCourse>();

            CourseController.AddTranscriptDetails(userId, courses.ToCourseList(), 0);

            bool userHasValidationParameters = HasValidationParameters;
            CourseAssignmentController courseAssignment = new CourseAssignmentController();

            foreach (Models.PublicCourse course in courses)
            {
                course.IsCourseWorkAllowed = true;
                course.IsUserValidationAllowed = userHasValidationParameters || (!course.IsValidationEnabled);
            }

            return new PagedResult<Models.PublicCourse>(courses, pageIndex, pageSize, total);
        }

        public MultipleResult<Models.PortalSetting> GetListPortalSetting(int CustomerId)
        {
            var query = Select.AllColumnsFrom<Data.PortalSetting>()
                .Where(Data.PortalSetting.CustomerIdColumn).IsEqualTo(CustomerId).ExecuteTypedList<Models.PortalSetting>();

            MultipleResult<Models.PortalSetting> result = new MultipleResult<Models.PortalSetting>(query);

            if (result.Data.Count == 0)
            {
                List<Models.PortalSetting> defaults = new List<Models.PortalSetting>()
                {
                    new Models.PortalSetting() { WidgetName = "Training Programs", WidgetOrderNumber = 1, WidgetIsVisible = true },
                    new Models.PortalSetting() { WidgetName = "Upcoming Events", WidgetOrderNumber = 2, WidgetIsVisible = true },
                    new Models.PortalSetting() { WidgetName = "Public Courses", WidgetOrderNumber = 3, WidgetIsVisible = true },
                    new Models.PortalSetting() { WidgetName = "Public Documents", WidgetOrderNumber = 4, WidgetIsVisible = true },
                };

                result = new MultipleResult<Models.PortalSetting>(defaults);
            }

            return result;
        }

        /// <summary>
        /// Gets a user transcript
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public SingleResult<List<TranscriptEntry>> GetTranscript(int userId)
        {
            return new SingleResult<List<TranscriptEntry>>(SPs.GetTranscript(userId).ExecuteTypedList<TranscriptEntry>());
        }

        /// <summary>
        /// Gets a user transcript in the form of courses
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public SingleResult<List<TranscriptEntryCourse>> GetTranscriptCourses(int userId, int? tpId = null)
        {
            if (userId != UserID && !UserIsSalesChannelAdmin)
            {
                throw new Exception("You do not have permission to view this user's transcript.");
            }

            List<TranscriptEntry> transcript = GetTranscript(userId).Data; // current transcript
            List<TranscriptEntryCourse> courses = new List<TranscriptEntryCourse>(); // the transcript with course/training program info to be returned

            Data.User user = new Data.User(userId);
            Data.Customer customer = new Data.Customer(user.CustomerID);

            // Store any entries that are historic separately
            Dictionary<int, TranscriptEntry> historicEntries = transcript.Where(te => te.IsHistorical)
                .DistinctBy(te => te.TrainingProgramID)
                .ToDictionary(te => te.TrainingProgramID);

            // Store any completed training program entries separately (We need to do this as we may generate false positives for IsComplete on the training program
            // rollup - we may copy a complete entry from one training program to the other when applying the transcript)
            Dictionary<int, TranscriptEntry> completeTrainingPrograms = transcript.Where(te => te.IsTrainingProgramRollupComplete)
                .DistinctBy(te => te.TrainingProgramID)
                .ToDictionary(te => te.TrainingProgramID);

            // get the training program ids and grab all the courses that are part of each training program id.
            // Ensure the ids are ordered by tp name so we don't have to bother ordering later
            List<int> trainingProgramIds = transcript.Where(t => t.TrainingProgramID > 0 && (!tpId.HasValue || (tpId.HasValue && t.TrainingProgramID == tpId.Value)))
                .OrderBy(t => t.TrainingProgramName)
                .Select(t => t.TrainingProgramID)
                .Distinct()
                .ToList();

            List<Models.TrainingProgramCourse> allTpCourses = new List<Models.TrainingProgramCourse>();
            Dictionary<int, Models.TrainingProgram> trainingProgramsDictionary = new Dictionary<int, Models.TrainingProgram>();

            if (tpId.HasValue && tpId.Value > 0 && !trainingProgramIds.Contains(tpId.Value))
            {
                trainingProgramIds.Add(tpId.Value);
            }

            if (trainingProgramIds.Count > 0)
            {
                // We need the full training program record (So we have access to artisan overrides at the tp level)
                trainingProgramsDictionary = Select.AllColumnsFrom<Data.TrainingProgram>()
                    .Where(Data.TrainingProgram.IdColumn).In(trainingProgramIds)
                    .ExecuteTypedList<Models.TrainingProgram>()
                    .ToDictionary(tp => tp.Id);

                string courseNameColumn = @"
                    CASE TrainingProgramToCourseLink.CourseTypeID
                        WHEN 1 THEN Course.Name
                        WHEN 2 THEN OnlineCourse.Name
                    END";

                string certificateEnabledColumn = @"
                    CASE TrainingProgramToCourseLink.CourseTypeID
                        WHEN 1 THEN Course.CertificateEnabled
                        WHEN 2 THEN OnlineCourse.CertificateEnabled
                    END";

                // List of all courses and online courses for each tp with the training program name
                allTpCourses = Select.AllColumnsFrom<Data.TrainingProgramToCourseLink>()
                    .IncludeColumn(courseNameColumn, "Name")
                    .IncludeColumn(certificateEnabledColumn, "CertificateEnabled")
                    .IncludeColumn(Data.OnlineCourse.CertificateEnabledOverrideColumn)
                    .IncludeColumn(Data.TrainingProgram.NameColumn.QualifiedName, "TrainingProgramName")
                    .Where(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn).In(trainingProgramIds)
                    .LeftOuterJoin(Data.Course.IdColumn, Data.TrainingProgramToCourseLink.CourseIDColumn)
                    .LeftOuterJoin(Data.OnlineCourse.IdColumn, Data.TrainingProgramToCourseLink.CourseIDColumn)
                    .InnerJoin(Data.TrainingProgram.IdColumn, Data.TrainingProgramToCourseLink.TrainingProgramIDColumn)
                    .OrderAsc(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn.QualifiedName,
                              Data.TrainingProgramToCourseLink.SyllabusTypeIDColumn.QualifiedName, 
                              Data.TrainingProgramToCourseLink.RequiredSyllabusOrderColumn.QualifiedName)
                    .ExecuteTypedList<Models.TrainingProgramCourse>();
            }

            Dictionary<int, List<TranscriptEntryCourse>> possibleSessionCourseDictionary = new Dictionary<int, List<TranscriptEntryCourse>>();

            // Public Courses:
            // Add any courses that arn't part of a training program first. They can all be displayed 
            // at the top as one group without a training program. 
            if (!tpId.HasValue || (tpId.HasValue && tpId.Value == 0))
            {
                transcript.Where(entry => entry.TrainingProgramID == 0).ToList().ForEach(entry =>
                {
                    TranscriptEntryCourse course = new TranscriptEntryCourse();
                    course.CopyFrom(entry);
                    course.Id = entry.CourseID;
                    course.Customer = customer;
                    course.TranscriptUserID = userId;
                    courses.Add(course);

                    if (course.CourseTypeID == (int)CourseType.Classroom)
                    {
                        if (!possibleSessionCourseDictionary.ContainsKey(course.Id))
                        {
                            possibleSessionCourseDictionary.Add(course.Id, new List<TranscriptEntryCourse>());
                        }

                        possibleSessionCourseDictionary[course.Id].Add(course);
                    }
                });
            }

            // Create regular courses mapped to training programs. This is a dictionary of all training program courses
            // for each tp, not just the ones that the user has a transcript record for. We will copy the transcript 
            // details on to each of these courses. 
            Dictionary<int, List<Models.Course>> trainingProgramCoursesDictionary = new Dictionary<int, List<Models.Course>>();
            allTpCourses.ForEach(c =>
            {
                if (c.TrainingProgramId > 0 && (!tpId.HasValue || (tpId.HasValue && c.TrainingProgramId == tpId.Value))) {
                    if (!trainingProgramCoursesDictionary.ContainsKey(c.TrainingProgramId))
                    {
                        trainingProgramCoursesDictionary.Add(c.TrainingProgramId, new List<Models.Course>());
                    }

                    Models.Course course = new Models.Course();
                    course.CopyFrom(c);
                    course.Customer = customer; // For any customer specific overrides
                    
                    /// Update any possible session courses if this training program uses a session.
                    if (trainingProgramsDictionary.ContainsKey(c.TrainingProgramId))
                    {
                        course.TrainingProgram = trainingProgramsDictionary[c.TrainingProgramId];

                        if (course.TrainingProgram.SessionCourseID.HasValue && 
                            course.TrainingProgram.SessionCourseID.Value > 0 && 
                            possibleSessionCourseDictionary.ContainsKey(course.TrainingProgram.SessionCourseID.Value)) {
                                possibleSessionCourseDictionary[course.TrainingProgram.SessionCourseID.Value].ForEach(sc => {
                                    sc.IsSessionCourse = true;
                                });
                        }
                    }

                    trainingProgramCoursesDictionary[c.TrainingProgramId].Add(course);
                }
            });

            // Training Program Courses:
            // Build up the transcript one training program at a time. 
            foreach (int trainingProgramId in trainingProgramIds)
            {
                if (!tpId.HasValue || (tpId.HasValue && trainingProgramId == tpId.Value))
                {
                    if (historicEntries.ContainsKey(trainingProgramId))
                    {
                        // If this is a historic entry, then we don't need to worry about any courses since we wont have data for them anyway.
                        // Just make one entry to indicate the training program is complete

                        TranscriptEntryCourse transcriptEntryCourse = new TranscriptEntryCourse();
                        transcriptEntryCourse.CopyFrom(historicEntries[trainingProgramId]);
                        transcriptEntryCourse.TrainingProgram = new Models.TrainingProgram()
                        {
                            Id = historicEntries[trainingProgramId].TrainingProgramID,
                            IsHistorical = historicEntries[trainingProgramId].IsHistorical,
                            IsTrainingProgramRollupComplete = historicEntries[trainingProgramId].IsTrainingProgramRollupComplete
                        };

                        courses.Add(transcriptEntryCourse);
                    }
                    else
                    {

                        // This isn't a historic entry, so need to pull in the course information

                        // Grab the courses for this tp.

                        List<Models.Course> tpCourses = new List<Models.Course>();
                        if (trainingProgramCoursesDictionary.ContainsKey(trainingProgramId))
                        {
                            tpCourses = trainingProgramCoursesDictionary[trainingProgramId];
                        }

                        // Copy any valid transcript info from the transcript to this training programs courses
                        DateTime maxPassed;
                        CourseController.AddTranscriptDetails(userId, tpCourses, trainingProgramId, out maxPassed, transcript);

                        // Convert each course to a transcript entry course, and add any additional details for display
                        tpCourses.ForEach(c =>
                        {
                            if (completeTrainingPrograms.ContainsKey(c.TrainingProgram.Id))
                            {
                                c.TrainingProgram.DateCompleted = maxPassed;
                            }

                            TranscriptEntryCourse transcriptEntryCourse = new TranscriptEntryCourse();
                            transcriptEntryCourse.CopyFrom(c);

                            if (completeTrainingPrograms.ContainsKey(trainingProgramId))
                            {
                                transcriptEntryCourse.IsTrainingProgramRollupComplete = true;
                            }

                            transcriptEntryCourse.TrainingProgramID = c.TrainingProgram.Id;
                            transcriptEntryCourse.CourseName = c.Name;
                            transcriptEntryCourse.TrainingProgramName = c.TrainingProgram.Name;
                            transcriptEntryCourse.TrainingProgram.IsTrainingProgramRollupComplete = transcriptEntryCourse.IsTrainingProgramRollupComplete;

                            courses.Add(transcriptEntryCourse);
                        });
                    }
                }
            };

            courses = courses
                .Where(c => !tpId.HasValue || (tpId.HasValue && tpId.Value == c.TrainingProgramID))
                .OrderBy(c => c.TrainingProgramName).ToList();

            return new SingleResult<List<TranscriptEntryCourse>>(courses);
        }

        /// <summary>
        /// Gets a list of messages for the given user.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="searchText"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedResult<Message> FindMessagesForUser(int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.MyMessage>()
                .WhereExpression(Data.MyMessage.Columns.Body).ContainsString(searchText)
                .Or(Data.MyMessage.Columns.Subject).ContainsString(searchText)
                .CloseExpression()
                .And(Data.MyMessage.Columns.RecipientID).IsEqualTo(userId);

            if (string.IsNullOrEmpty(orderBy))
            {
                orderBy = Data.MyMessage.Columns.ModifiedOn;
            }
            if (orderDir == OrderDirection.Default)
            {
                orderDir = OrderDirection.Descending;
            }

            return new PagedResult<Message>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        /// <summary>
        /// Updates a meeting with the specified values
        /// </summary>
        /// <param name="customerId">The customer to whom the meeting belongs</param>
        /// <param name="userId">The user to whom the meeting belongs</param>
        /// <param name="meetingId">The meeting ID to update</param>
        /// <param name="meeting">The new meeting information</param>
        /// <returns>The updated meeting</returns>
        public SingleResult<Meeting> UpdateMeeting(int customerId, int userId, string meetingId, Meeting meeting)
        {
            // make sure our dates include the UTC indicator;
            // the client sends them in in UTC, but they're auto-parsed by WCF into local time
            //meeting.StartDateTime = meeting.StartDateTime.WithUTCFlag();
            //meeting.EndDateTime = meeting.EndDateTime.WithUTCFlag();

            Data.GTMOrganizer organizer = new Data.GTMOrganizer(Data.GTMOrganizer.Columns.UserID, userId);

            if (organizer.Id == 0)
            {
                throw new Exception("You are not a registered GTM Organizer.");
            }

            if (meeting.MeetingType.ToLower() == GTMController.MeetingTypeScheduled.ToLower()
                       && meeting.EndDateTime != null && meeting.StartDateTime != null
                       && meeting.StartDateTime >= meeting.EndDateTime)
            {
                throw new Exception("The end date/time must be later than the start date/time.");
            }

            // these methods invoke the GTM api, and update the passed in meeting
            // with the appropriate values
            GTMController gtm = new GTMController(organizer.Email, organizer.Password);

            if (meeting.MeetingType.ToLower() == GTMController.MeetingTypeScheduled.ToLower())
            {
                meeting.IsEditable = true;
                if (meeting.Id == 0)
                    gtm.CreateScheduledMeeting(meeting);
                else
                {
                    gtm.UpdateScheduledMeeting(meeting);
                }
            }
            else if (meeting.MeetingType.ToLower() == GTMController.MeetingTypeImmediate.ToLower())
            {
                if (meeting.Id == 0)
                    gtm.CreateImmediateMeeting(meeting);
                else
                    gtm.UpdateImmediateMeeting(meeting);
            }
            else
                throw new Exception("Unsupported meeting type.");

            // todo: more validation, see the gmtinvitationcontroller in symphony1

            // create the meeting internally
            Data.GTMMeeting m = new GTMMeeting(meetingId);
            meeting.CopyTo(m);
            m.Id = int.Parse(meetingId);
            m.GTMOrganizerID = organizer.Id;

            bool isNew = (m.Id == 0);

            m.Save(Username);

            // trash anyone who was already invited
            GTMInvitation.Destroy(GTMInvitation.Columns.GTMMeetingID, m.Id);

            List<NotificationDate> notificationDates = new List<NotificationDate>() {
                new NotificationDate(){
                    StartDate = meeting.StartDateTime,
                    EndDate = meeting.EndDateTime,
                    Type = CalendarType.Timed,
                    InstanceID = meeting.Id.ToString(),
                    Method = CalendarMethod.Create,
                    Name = "Meeting"
                }
            };

            // save the internal user list
            int inviteCount = 0;
            foreach (int user in meeting.InternalInvitees)
            {
                inviteCount++;
                if (inviteCount > meeting.MaxParticipants)
                {
                    throw new Exception("The meeting was created, but the maximum number of invitations was hit.");
                }
                Data.GTMInvitation invite = new GTMInvitation();
                invite.UserID = user;
                invite.GTMMeetingID = m.Id;
                invite.IsExternal = false;
                invite.Save(Username);

                string template = NotificationType.MeetingUpdatedInternal;
                if (isNew)
                {
                    template = NotificationType.MeetingCreatedInternal;
                }

                NotificationController.SendNotification(template, new NotificationOptions()
                {
                    Sync = false,
                    Users = new int[] { user },
                    TemplateObjects = new object[] { m, invite },
                    NotificationDates = notificationDates.ToArray()
                });
            }

            // save the external user list
            UserController controller = new UserController();
            foreach (string email in meeting.ExternalInvitees)
            {
                inviteCount++;
                if (inviteCount > meeting.MaxParticipants)
                {
                    throw new Exception("The meeting was created, but the maximum number of invitations was hit.");
                }
                // create a temporary user account
                Models.User user = controller.CreateExternalUser(customerId, email);

                if (user == null || user.ID == 0)
                {
                    Log.Warn(string.Format("The user with email {0} was not created.", email));
                    continue;
                }

                // save the invitation
                Data.GTMInvitation invite = new GTMInvitation();
                invite.UserID = user.ID;
                invite.GTMMeetingID = m.Id;
                invite.IsExternal = true;
                invite.Save(Username);

                string template = NotificationType.MeetingUpdatedInternal;
                if (isNew)
                {
                    template = NotificationType.MeetingCreatedInternal;
                }

                NotificationController.SendNotification(template, new NotificationOptions()
                {
                    Sync = false,
                    Users = new int[] { user.ID },
                    TemplateObjects = new object[] { m, invite },
                    NotificationDates = notificationDates.ToArray()
                });
            }

            return new SingleResult<Meeting>(meeting);
        }

        /// <summary>
        /// Searches for a set of users
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="searchText"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedResult<Models.User> FindUsers(int customerId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.FastSearchableUser>()
                .WhereExpression(Data.FastSearchableUser.Columns.FirstName).ContainsString(searchText)
                .Or(Data.FastSearchableUser.Columns.LastName).ContainsString(searchText)
                .Or(Data.FastSearchableUser.Columns.JobRole).ContainsString(searchText)
                .Or(Data.FastSearchableUser.Columns.Location).ContainsString(searchText)
                .Or(Data.FastSearchableUser.Columns.Audiences).ContainsString(searchText)
                .Or(Data.FastSearchableUser.Columns.Email).ContainsString(searchText)
                .CloseExpression()
                .And(Data.FastSearchableUser.Columns.StatusID).IsEqualTo((int)UserStatusType.Active)
                .And(Data.User.Columns.SalesChannelID).IsEqualTo(0)
                .And(Data.User.Columns.IsExternal).IsEqualTo(false)
                .And(Data.FastSearchableUser.Columns.CustomerID).IsEqualTo(customerId);

            return new PagedResult<Models.User>(query, pageIndex, pageSize, orderBy, orderDir);
        }

        /// <summary>
        /// Marks a message as read.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="classId"></param>
        /// <returns></returns>
        public SingleResult<bool> ReadMessage(int userId, int messageId)
        {
            Data.Notification n = new Notification(messageId);
            if (n.RecipientID != userId)
            {
                throw new InvalidOperationException("You don't have permission to read this message.");
            }
            n.IsRead = true;
            n.Save(Username);
            return new SingleResult<bool>(true);
        }

        /// <summary>
        /// Gets the number of unread messages.
        /// </summary>
        /// <returns></returns>
        public SingleResult<int> GetUnreadCount(int userId)
        {
            int count = Select.AllColumnsFrom<Data.MyMessage>()
                .Where(Data.MyMessage.Columns.RecipientID).IsEqualTo(userId)
                .And(Data.MyMessage.Columns.IsRead).IsEqualTo(false)
                .GetRecordCount();

            return new SingleResult<int>(count);
        }

        /// <summary>
        /// Gets a list of meetings for the given user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="searchText"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedResult<Meeting> FindMeetingsForUser(int userId, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, long[] meetingIds = null)
        {
            SqlQuery query = Select.AllColumnsFrom<Data.MyGTMMeeting>()
                .Where(Data.MyGTMMeeting.Columns.UserID).IsEqualTo(userId);

            if (!string.IsNullOrEmpty(searchText))
            {
                query.And(Data.MyGTMMeeting.Columns.Subject).ContainsString(searchText);
            }

            if (meetingIds != null && meetingIds.Length > 0)
            {
                query.And(Data.MyGTMMeeting.Columns.Id).In(meetingIds);
            }

            PagedResult<Meeting> result = new PagedResult<Meeting>(query, pageIndex, pageSize, orderBy, orderDir);

            // add the external and internal invitees
            foreach (Meeting meeting in result.Data)
            {
                List<Models.User> users = new Select(Data.User.Columns.Email, Data.User.Columns.Id, Data.User.Columns.IsExternal)
                    .From<Data.User>().Where(Data.User.Columns.Id).In(
                        new Select(Data.GTMInvitation.Columns.UserID)
                        .From<Data.GTMInvitation>()
                        .Where(Data.GTMInvitation.Columns.GTMMeetingID).IsEqualTo(meeting.Id)
                    ).ExecuteTypedList<Models.User>();

                meeting.ExternalInvitees = users.Where(u => u.IsExternal).Select(u => u.Email).ToList();
                meeting.InternalInvitees = users.Where(u => !u.IsExternal).Select(u => u.ID).ToList();

                meeting.StartDateTime = meeting.StartDateTime.WithUtcFlag();
                meeting.EndDateTime = meeting.EndDateTime.WithUtcFlag();
            }
            return result;
        }

        /// <summary>
        /// Gets detailed information about classes for a given course
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedResult<Class> GetClassesForCourse(int courseId, int userId, DateTime? minDate, DateTime? maxDate, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            // set some sanity checks
            if (!minDate.HasValue || minDate.Value.Year < 1900 || minDate.Value.Year > 3000)
            {
                minDate = new DateTime(1900, 1, 1);
            }
            if (!maxDate.HasValue || maxDate.Value.Year < 1900 || maxDate.Value.Year > 3000)
            {
                maxDate = new DateTime(3000, 1, 1);
            }
            SqlQuery query = Select.AllColumnsFrom<Data.ClassroomClassDetail>()
                .Where(Data.ClassroomClassDetail.Columns.MinClassDate).IsGreaterThan(minDate)
                .And(Data.ClassroomClassDetail.Columns.MinClassDate).IsLessThan(maxDate)
                .And(Data.ClassroomClassDetail.Columns.CourseID).IsEqualTo(courseId)
                .And(Data.ClassroomClassDetail.Columns.ClassID).NotIn(
                // exclude classes for which there's already a registration
                    new Select(Data.Registration.Columns.ClassID)
                    .From<Data.Registration>()
                    .Where(Data.Registration.Columns.RegistrantID).IsEqualTo(userId))
                .OrderAsc(Data.ClassroomClassDetail.Columns.MinClassDate);

            PagedResult<Class> result = new PagedResult<Class>(query, pageIndex, pageSize, orderBy, orderDir);

            List<Data.ClassDate> dates = Select.AllColumnsFrom<Data.ClassDate>()
                .Where(Data.ClassDate.Columns.ClassID).In(result.Data.Count() == 0 ? new int[] { 0 } : result.Data.Select(c => c.ClassID))
                .ExecuteTypedList<Data.ClassDate>();

            foreach (Class klass in result.Data)
            {
                List<Data.ClassDate> specific = dates.Where(d => d.ClassID == klass.ClassID).ToList();
                klass.Dates = new List<DateTime>(specific.Select(d => d.StartDateTime.WithUtcFlag()));

                // TODO: Merge Class and ClassroomClass classes so we don't need this lookup
                ClassroomClass cc = Select.AllColumnsFrom<Data.ClassX>().Where(Data.ClassX.Columns.Id).IsEqualTo(klass.ClassID).ExecuteSingle<ClassroomClass>();

                int totalRegistered = Select.AllColumnsFrom<Data.Registration>()
                            .Where(Data.Registration.Columns.ClassID).IsEqualTo(cc.Id)
                            .And(Data.Registration.Columns.RegistrationStatusID).IsEqualTo((int)RegistrationStatusType.Registered)
                            .GetRecordCount();

                // if class is at capacity and is "locked", registration is closed
                klass.RegistrationClosed = ((totalRegistered >= cc.CapacityOverride) && cc.LockedIndicator);
                klass.IsAdminRegistration = cc.IsAdminRegistration;
            }

            return result;
        }

        /// <summary>
        /// Registers a user for a class
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="classId"></param>
        /// <returns></returns>
        public SingleResult<bool> RegisterForClass(int userId, int classId)
        {
            //TODO: needs waitlist logic here...
            ClassX cls = new ClassX(classId);
            if (cls.IsAdminRegistration)
            {
                throw new ApplicationException("Only administrators can register you for this class.");
            }
            int totalRegistered = Select.AllColumnsFrom<Data.Registration>()
                            .Where(Data.Registration.Columns.ClassID).IsEqualTo(cls.Id)
                            .And(Data.Registration.Columns.RegistrationStatusID).IsEqualTo((int)RegistrationStatusType.Registered)
                            .GetRecordCount();

            if ((totalRegistered >= cls.CapacityOverride) && cls.LockedIndicator)
                throw new ApplicationException("Registration unsuccessful. This class is at full capacity, please select another class.");

            Data.Registration registration = new Data.Registration();
            registration.ClassID = classId;
            registration.RegistrantID = userId;
            registration.RegistrationStatusID = (int)RegistrationStatusType.AwaitingApproval;
            if (!cls.ApprovalRequired)
            {
                if (totalRegistered == cls.CapacityOverride)
                {
                    registration.RegistrationStatusID = (int)RegistrationStatusType.WaitList;
                }
                else
                {
                    registration.RegistrationStatusID = (int)RegistrationStatusType.Registered;
                }
            }
            registration.RegistrationTypeID = (int)RegistrationType.Self;
            registration.Save(Username);

            Data.User user = new Data.User(userId);

            List<string> warnings = new ClassroomController().CheckForConflicts(cls.Id, new List<Data.User>() { user });

            List<NotificationDate> notificationDates = new List<NotificationDate>();
            for (var i = 0; i < cls.ClassDates.Count(); i++)
            {
                var cd = cls.ClassDates[i];

                notificationDates.Add(new NotificationDate()
                {
                    StartDate = cd.StartDateTime,
                    EndDate = cd.StartDateTime.AddMinutes((double)cd.Duration),
                    Type = CalendarType.Timed,
                    InstanceID = cls.Id + "-" + i,
                    Method = CalendarMethod.Create,
                    Name = cls.Name + " (Class " + (i + 1) + " of " + cls.ClassDates.Count() + ")"
                });
            }

            // send out a notification to the user
            if (cls.ApprovalRequired)
            {
                NotificationController.SendNotification(NotificationType.StudentRegistered, new NotificationOptions()
                {
                    TemplateObjects = new object[] { user, cls, registration },
                    NotificationDates = notificationDates.ToArray()
                });
            }
            else
            {
                NotificationController.SendNotification(NotificationType.RegistrationStatusChanged, new NotificationOptions()
                {
                    TemplateObjects = new object[] { user, cls, registration },
                    NotificationDates = notificationDates.ToArray()
                });
            }

            SingleResult<bool> result = new SingleResult<bool>(true);
            if (warnings.Count > 0)
            {
                result.Notice = "The registration request has been processed, but conflicts exist:<br/><br/>" + string.Join("<br/>", warnings.ToArray());
            }

            if (!string.IsNullOrEmpty(cls.WebinarKey))
            {
                // attempt to link to GoToWebinar
                var cc = new ClassroomController();
                WebinarRegistrationAttempt attempt = cc.CreateWebinarRegistration(user, cc.GetOganizer(cls), cls.WebinarKey);
                if (attempt.Success)
                {
                    registration.WebinarRegistrationID = attempt.RegistrantID;
                    registration.Save();
                }
                else
                {
                    if (!string.IsNullOrEmpty(result.Notice))
                    {
                        result.Notice += "<br/><br/>";
                    }
                    result.Notice += "You were successfully registered for the class, but the webinar registration attempt was unsuccessful. Please contact your manager for assistance. <br/><br/>(The error from GoToWebinar was: " + attempt.Error + ")";
                }
            }

            return result;
        }

        /// <summary>
        /// Unregisters a user from a class
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="registrationId"></param>
        /// <returns></returns>
        public SingleResult<bool> UnregisterForClass(int userId, int registrationId)
        {
            SqlQuery q = new SqlQuery()
            .From<Data.Registration>()
            .Where(Data.Registration.Columns.Id).IsEqualTo(registrationId)
            .And(Data.Registration.Columns.RegistrantID).IsEqualTo(userId);

            // select it for notification purposes
            Data.Registration reg = q.ExecuteSingle<Data.Registration>();

            ClassX cls = new ClassX(reg.ClassID);
            if (cls.IsAdminRegistration)
            {
                throw new ApplicationException("Only administrators can unregister you from this class.");
            }

            // delete it
            q.QueryCommandType = SqlQuery.QueryType.Delete;

            if (q.Execute() != 1)
            {
                return new SingleResult<bool>(false);
            }

            // send out a notification to the user
            Data.User user = new Data.User(reg.RegistrantID);
            
            List<NotificationDate> notificationDates = new List<NotificationDate>();
            for (var i = 0; i < cls.ClassDates.Count(); i++)
            {
                var cd = cls.ClassDates[i];

                notificationDates.Add(new NotificationDate()
                {
                    StartDate = cd.StartDateTime,
                    EndDate = cd.StartDateTime.AddMinutes((double)cd.Duration),
                    Type = CalendarType.Timed,
                    Method = CalendarMethod.Delete,
                    InstanceID = cls.Id + "-" + i,
                    Name = cls.Name + " (Class " + (i + 1) + " of " + cls.ClassDates.Count() + ")"
                });
            }

            NotificationController.SendNotification(NotificationType.StudentUnregistered, new NotificationOptions() {
                TemplateObjects = new object[] { user, cls },
                NotificationDates = notificationDates.ToArray()
            });

            return new SingleResult<bool>(true);
        }
         /// <summary>
        /// Gets all the details about a given training program, including files and course details.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="trainingProgramId"></param>
        /// <param name="manage">If true, the user is attempting to manage the TP, and don't mess with the start/due/end dates</param>
        /// <returns></returns>
        public SingleResult<Models.TrainingProgram> GetTrainingProgramDetails(int userId, int trainingProgramId, bool managing)
        {
            return GetTrainingProgramDetails(userId, trainingProgramId, managing, false);
        }
        /// <summary>
        /// Gets all the details about a given training program, including files and course details.
        /// Loads this training program as an admin would see the training program when viewing it for another student
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="trainingProgramId"></param>
        /// <param name="manage">If true, the user is attempting to manage the TP, and don't mess with the start/due/end dates</param>
        /// <returns></returns>
        public SingleResult<Models.TrainingProgram> GetTrainingProgramDetailsAsAdmin(int userId, int trainingProgramId, bool managing)
        {
            return GetTrainingProgramDetails(userId, trainingProgramId, managing, false, 0, true);
        }

        /// <summary>
        /// Gets all the details about a given training program, including files and course details.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="trainingProgramId"></param>
        /// <param name="manage">If true, the user is attempting to manage the TP, and don't mess with the start/due/end dates</param>
        /// <param name="isSalesforce">If true, ensure customer is salesforce enabled</param>
        /// <returns></returns>
        public SingleResult<Models.TrainingProgram> GetTrainingProgramDetails(int userId, int trainingProgramId, bool managing, bool isSalesforce, int targetCustomerId = 0, bool isAdminView = false)
        {
            Data.TrainingProgram tp = new Data.TrainingProgram(trainingProgramId);
            Models.TrainingProgram tpModel = new Models.TrainingProgram();
                
            tpModel.CopyFrom(tp);
            
            // Get the training program rollup
            Data.TrainingProgramRollup tpr = Select.AllColumnsFrom<Data.TrainingProgramRollup>()
                .Where(Data.TrainingProgramRollup.TrainingProgramIDColumn).IsEqualTo(trainingProgramId)
                .And(Data.TrainingProgramRollup.UserIDColumn).IsEqualTo(userId)
                .ExecuteSingle<Data.TrainingProgramRollup>();

            if (tpr != null)
            {
                Models.TrainingProgramRollup tprModel = Model.Create<Models.TrainingProgramRollup>(tpr);
                tpModel.TotalSeconds = tprModel.TotalSeconds;
                tpModel.IsTrainingProgramRollupComplete = tprModel.IsComplete;
                tpModel.TrainingProgramRollup = tprModel;
            }

            List<Models.TrainingProgram> tps = new List<Models.TrainingProgram>() {
                tpModel
            };

            AddTrainingProgramDetails(userId, tps, managing, isSalesforce, targetCustomerId, isAdminView);

            return new SingleResult<Models.TrainingProgram>(tpModel);
        }


        public void AddTrainingProgramDetails(int userId, List<Models.TrainingProgram> trainingPrograms, bool managing, bool isSalesforce, int targetCustomerId = 0, bool isAdminView = false)
        {
            Log.Debug("Gettting Optimized Single TP Details");
            DateTime startOverall = DateTime.Now;

            var user = (new UserController()).GetUser(userId);
            var customer = (new CustomerController()).GetCustomer(CustomerID, UserID, user.CustomerID).Data;

            long todaysCourseWorkDuration = (new CourseAssignmentController()).GetTodaysDurationInCourse(userId);

            // Most likely, when the training program models are passed in they wont have
            // all the data required since it probably will come from GetTrainingProgramsForUser
            // which does not grab all trainingProgramFields (I think) - TODO: Double Check if this
            // will be needed

            //DS: 

            List<Data.TrainingProgram> trainingProgramData = Select.AllColumnsFrom<Data.TrainingProgram>()
               .Where(Data.TrainingProgram.IdColumn).In(trainingPrograms.Select(t => t.Id).ToArray())
               .ExecuteTypedList<Data.TrainingProgram>();

            if (trainingProgramData.Count == 0)
            {
                throw new Exception("Unable to find requested training program id.");
            }
            bool isSingle = trainingPrograms.Count == 1;

            // Look up dictionary to copy training program data over without additional loops
            Dictionary<int, Data.TrainingProgram> trainingProgramDictionary = trainingProgramData.ToDictionary(t => t.Id);

            foreach (Models.TrainingProgram t in trainingPrograms)
            {
                if (trainingProgramDictionary.ContainsKey(t.Id))
                {
                    trainingProgramDictionary[t.Id].Sku = t.Sku;
                }
            }

            // Convert them into an array of ids to make data retrieval easier
            int[] trainingProgramIds = trainingProgramData.Select(t => t.Id).ToArray();
            int[] parentTrainingProgramIds = trainingProgramData.Select(
                        t => t.ParentTrainingProgramID > 0 ? t.ParentTrainingProgramID.Value : t.Id
                    ).ToArray();

            if (trainingPrograms.Count == 0)
            {
                return;
            }

            // For now just check permissions on the first training program.
            // They should all be from the same customer when this function is called
            if (isSalesforce)
            {
                // Open access to all training programs where the customer has Salesforce module
                // enabled. Doesn't matter who the logged in user is that is making the request
                // If it is a salesforce request and customer does not have salesforce
                // but the customer id in training program and user match anyway, allow it. 
                var trainingProgramCustomer = new Data.Customer(trainingPrograms[0].CustomerID);

                var cId = targetCustomerId > 0 ? targetCustomerId : user.CustomerID;

                if (!UserIsSalesChannelAdmin && !Modules.HasModule(trainingProgramCustomer, Modules.Salesforce) && cId != trainingPrograms[0].CustomerID)
                {
                    throw new ApplicationException(string.Format("Permission denied to the training program with id {0}.", trainingPrograms[0].Id));
                }
            }
            else
            {
                if (trainingPrograms[0].CustomerID != user.CustomerID && !UserIsSalesChannelAdmin && trainingPrograms[0].SharedFlag == true)
                {
                    throw new ApplicationException(string.Format("Permission denied to the training program with id {0}.", trainingPrograms[0].Id));
                }
            }
            #region LoadData
            Log.Debug("Loading Data");
            DateTime start = DateTime.Now;
            // We will load all data for all of the training programs
            // in order to avoid looping a trip to the database

            // Survey coppies used for all training programs (Indexed by course id)
            Dictionary<int, Data.OnlineCourse> surveyDictionary = Select.AllColumnsFrom<Data.OnlineCourse>()
                .Where(Data.OnlineCourse.IdColumn).In(trainingProgramData.Select(t => t.SurveyID))
                .WithNoLock()
                .ExecuteTypedList<Data.OnlineCourse>()
                .ToDictionary(c => c.Id);

            // Original surveys used for all the surveys in the training programs (indexed by course id)
            Dictionary<int, Data.OnlineCourse> originalSurveyDictionary = new Dictionary<int, Data.OnlineCourse>();
            if (surveyDictionary.Count > 0)
            {
                originalSurveyDictionary = Select.AllColumnsFrom<Data.OnlineCourse>()
                    .Where(Data.OnlineCourse.IdColumn).In(surveyDictionary.Select(s => s.Value.DuplicateFromID))
                    .WithNoLock()
                    .ExecuteTypedList<Data.OnlineCourse>()
                    .ToDictionary(c => c.Id);
            }

            // Affidavits used in all training programs (indexed by affidavit id)
            List<int> affidavitIds = trainingProgramData.Select(t => t.AffidavitID.HasValue ? t.AffidavitID.Value : 0).Concat(trainingProgramData.Select(t => t.TrainingProgramAffidavitID.HasValue ? t.TrainingProgramAffidavitID.Value : 0).ToList()).ToList();
            Dictionary<int, Data.AffidavitFinalExam> affidavitDictionary = Select.AllColumnsFrom<Data.AffidavitFinalExam>()
                .Where(Data.AffidavitFinalExam.IdColumn).In(affidavitIds)
                .WithNoLock()
                .ExecuteTypedList<Data.AffidavitFinalExam>()
                .ToDictionary(a => a.Id);

            List<int> proctorFormIds = trainingProgramData.Select(t => t.ProctorFormID).ToList();
            Dictionary<int, Data.ProctorForm> proctorFormDictionary = Select.AllColumnsFrom<Data.ProctorForm>()
                .Where(Data.ProctorForm.IdColumn).In(proctorFormIds)
                .WithNoLock()
                .ExecuteTypedList<Data.ProctorForm>()
                .ToDictionary(pf => pf.Id);

            // The affidavit responses for this user for the training programs provided
            List<Data.AffidavitFinalExamResponse> allResponses = Select.AllColumnsFrom<Data.AffidavitFinalExamResponse>()
                .Where(Data.AffidavitFinalExamResponse.TrainingProgramIDColumn).In(trainingProgramIds)
                .And(Data.AffidavitFinalExamResponse.UserIDColumn).IsEqualTo(user.ID)
                .And(Data.AffidavitFinalExamResponse.AffidavitIDColumn).IsGreaterThan(0) // Just for the portal since it was generating
                                                                                         // test data responses with 0 as the id. Exclude
                                                                                         // those to make further testing easier.
                .WithNoLock()
                .ExecuteTypedList<Data.AffidavitFinalExamResponse>();

            // Affidavits done for TP
            Dictionary<int, int> tpAffidavitsCompleted = allResponses
                .Where(r => r.CourseID == 0)
                .DistinctBy(r => r.TrainingProgramID)
                .ToDictionary(rk => rk.TrainingProgramID, rv => rv.AffidavitID);

            // Affidavits done for final
            Dictionary<int, int> finalAffidavitsCompleted = allResponses
                .Where(r => r.CourseID > 0)
                .DistinctBy(r => r.TrainingProgramID)
                .ToDictionary(rk => rk.TrainingProgramID, rv => rv.AffidavitID);

            // All bundles used in the training programs (indexed by bundle id)
            Dictionary<int, Data.TrainingProgramBundle> bundleDictionary = Select.AllColumnsFrom<Data.TrainingProgramBundle>()
                .Where(Data.TrainingProgramBundle.IdColumn).In(trainingProgramData.Select(t => t.PurchasedFromBundleID))
                .WithNoLock()
                .ExecuteTypedList<Data.TrainingProgramBundle>()
                .ToDictionary(b => b.Id);

            // All minimum start times for each training program (indexed by training program id)
            Dictionary<int, DateTime> minimumStartTimesDictionary = new Select(
                new Aggregate(Data.CourseStartTime.TrainingProgramIDColumn.QualifiedName, "TrainingProgramID", AggregateFunction.GroupBy),
                new Aggregate(Data.CourseStartTime.StartTimeColumn.QualifiedName, "StartTime", AggregateFunction.Min))
              .From<Data.CourseStartTime>()
              .Where(Data.CourseStartTime.TrainingProgramIDColumn).In(trainingProgramIds)
              .And(Data.CourseStartTime.UserIDColumn).IsEqualTo(userId)
              .WithNoLock()
              .ExecuteTypedList<Data.CourseStartTime>()
              .DistinctBy(c => c.TrainingProgramID)
              .ToDictionary(c => c.TrainingProgramID.HasValue ? c.TrainingProgramID.Value : 0, c => c.StartTime);

            // All files - since each training program can have multiple files we have to leave it as a list
            // will need to select from this list for each training program
            List<Data.TrainingProgramFile> files = Select.AllColumnsFrom<Data.TrainingProgramFile>()
                .Where(Data.TrainingProgramFile.TrainingProgramIDColumn).In(parentTrainingProgramIds)
                .WithNoLock()
                .ExecuteTypedList<Data.TrainingProgramFile>();

            // Course links - left as a list, will have to select from list for each training program
            List<Data.TrainingProgramToCourseLink> allCourseLinks = Select.AllColumnsFrom<Data.TrainingProgramToCourseLink>()
                .Where(Data.TrainingProgramToCourseLink.TrainingProgramIDColumn).In(trainingProgramIds)
                .WithNoLock()
                .ExecuteTypedList<Data.TrainingProgramToCourseLink>();
            
            // List of ids for each course type throughout all training programs
            List<int> classroomCourseIds = allCourseLinks.Where(l => l.CourseTypeID == (int)CourseType.Classroom).Select(l => l.CourseID).ToList<int>();
            List<int> onlineCourseIds = allCourseLinks.Where(l => l.CourseTypeID == (int)CourseType.Online).Select(l => l.CourseID).ToList<int>();

            // All classroom courses throughout all training programs
            List<string> classroomCourseColumns = Data.Course.Schema.Columns.Select(c => c.QualifiedName).ToList();
            classroomCourseColumns.Add(Data.OnlineCourse.ProctorRequiredIndicatorColumn.QualifiedName);
            classroomCourseColumns.Add(Data.OnlineCourse.IsValidationEnabledColumn.QualifiedName);

            List<Models.TrainingProgramCourse> classroomCourses = classroomCourseIds.Count > 0 ?
                new Select(classroomCourseColumns.ToArray()).From<Data.Course>().Where(Data.Course.Columns.Id).In(classroomCourseIds)
                .LeftOuterJoin(Data.OnlineCourse.IdColumn, Data.Course.OnlineCourseIDColumn)
                .WithNoLock()
                .ExecuteTypedList<Models.TrainingProgramCourse>() : new List<Models.TrainingProgramCourse>();

            // All online courses throughout all training programs
            List<string> columns = Data.OnlineCourse.Schema.Columns.Select(c => c.QualifiedName).ToList();
            columns.Add(Data.ArtisanCourse.ParametersColumn.QualifiedName);
            columns.Add(Data.MessageBoard.IdColumn.QualifiedName + " AS MessageBoardID");

            List<Models.TrainingProgramCourse> onlineCourses = onlineCourseIds.Count > 0 ?
                new Select(columns.ToArray())
                .From<Data.OnlineCourse>()
                .LeftOuterJoin(Data.ArtisanCourse.IdColumn, Data.OnlineCourse.ArtisanCourseIDColumn)
                .LeftOuterJoin(Data.MessageBoard.OnlineCourseIdColumn, Data.OnlineCourse.IdColumn)
                .Where(Data.OnlineCourse.Columns.Id).In(onlineCourseIds)
                .WithNoLock()
                .ExecuteTypedList<Models.TrainingProgramCourse>() : new List<Models.TrainingProgramCourse>();

            // All courses with assignments throughout all training programs
            Dictionary<int, int> coursesWithAssignmentsDictionary = new Dictionary<int, int>();
            IEnumerable<int> artisanCourseIDs = onlineCourses.Select(o => o.PublishedArtisanCourseID.HasValue ? o.PublishedArtisanCourseID.Value : 0);

            if (artisanCourseIDs.Count() > 0)
            {
                List<Data.AssignmentsArtisanCourseDetail> coursesWithAssignments = new Select().From<Data.AssignmentSummary>()
                    .GroupBy(Data.AssignmentSummary.ArtisanCourseIDColumn)
                    .Count(Data.AssignmentSummary.ArtisanSectionIDColumn, Data.AssignmentsArtisanCourseDetail.Columns.TotalAssignments)
                    .Where(Data.AssignmentSummary.ArtisanCourseIDColumn).In(artisanCourseIDs)
                    .WithNoLock()
                    .ExecuteTypedList<Data.AssignmentsArtisanCourseDetail>();
                
                if (coursesWithAssignments.Count() > 0)
                {
                    coursesWithAssignmentsDictionary = coursesWithAssignments.Distinct().ToDictionary(k => k.ArtisanCourseID, v => v.TotalAssignments.HasValue ? v.TotalAssignments.Value : 0);
                }
            }

            // start times for all courses - will have to select from this list for each course
            // indexed as a string trainingprogramid-onlinecourseid
            Dictionary<string, Data.CourseStartTime> onlineCourseStartTimeDictionary = onlineCourseIds.Count > 0 ?
                Select.AllColumnsFrom<Data.CourseStartTime>()
                .Where(Data.CourseStartTime.Columns.OnlineCourseID).In(onlineCourseIds)
                .And(Data.CourseStartTime.TrainingProgramIDColumn).In(trainingProgramIds)
                .And(Data.CourseStartTime.UserIDColumn).IsEqualTo(userId)
                .WithNoLock()
                .ExecuteTypedList<Data.CourseStartTime>()
                .DistinctBy(c => c.TrainingProgramID + "-" + c.OnlineCourseID)
                .ToDictionary(c => c.TrainingProgramID + "-" + c.OnlineCourseID)
                : new Dictionary<string, Data.CourseStartTime>();

            // survey taken flags for all training programs
            Dictionary<int, bool> surveyTakenDictionary = new Dictionary<int,bool>();
            if (surveyDictionary.Count > 0)
            {
                surveyTakenDictionary = Select.AllColumnsFrom<Data.OnlineCourseRollup>()
                    .Where(Data.OnlineCourseRollup.UserIDColumn).IsEqualTo(userId)
                    .And(Data.OnlineCourseRollup.CourseIDColumn).In(surveyDictionary.Select(s => s.Value.Id))
                    .WithNoLock()
                    .ExecuteTypedList<Data.OnlineCourseRollup>()
                    .DistinctBy(o => o.TrainingProgramID)
                    .ToDictionary(o => o.TrainingProgramID, o => (o.Completion.HasValue ? o.Completion.Value : false));
            }

            // All classroom message boards
            List<Models.MessageBoard> messageBoards = new List<Models.MessageBoard>();
            if (classroomCourses.Count > 0)
            {
                messageBoards = Select.AllColumnsFrom<Data.MessageBoard>()
                    .InnerJoin(Data.Tables.TrainingProgram, Data.TrainingProgram.Columns.Id, Data.Tables.MessageBoard, Data.MessageBoard.Columns.TrainingProgramId)
                    .InnerJoin(Data.Tables.Customer, Data.Customer.Columns.Id, Data.Tables.TrainingProgram, Data.TrainingProgram.Columns.CustomerID)
                    .Where(Data.MessageBoard.MessageBoardTypeColumn).IsEqualTo(MessageBoardType.Classroom)
                    .And(Data.MessageBoard.ClassIdColumn).In(classroomCourses.Select(c => c.ClassID))
                    .WithNoLock()
                    .ExecuteTypedList<Models.MessageBoard>();
            }

            Dictionary<int, Models.MessageBoard> messageBoardDictionary = messageBoards.DistinctBy(c => c.ClassId).ToDictionary(m => (int)m.ClassId);


            // All primary assignments - will have to select from this list when loading
            List<Data.HierarchyToTrainingProgramLink> primaryAssignments = Select.AllColumnsFrom<Data.HierarchyToTrainingProgramLink>()
                .Where(Data.HierarchyToTrainingProgramLink.Columns.TrainingProgramID).In(trainingProgramIds)
                .And(Data.HierarchyToTrainingProgramLink.Columns.IsPrimary).IsEqualTo(true)
                .WithNoLock()
                .ExecuteTypedList<Data.HierarchyToTrainingProgramLink>();

            // Secondary assignments, same deal as the primary assignments
            List<Data.HierarchyToTrainingProgramLink> secondaryAssignments = Select.AllColumnsFrom<Data.HierarchyToTrainingProgramLink>()
                .Where(Data.HierarchyToTrainingProgramLink.Columns.TrainingProgramID).In(trainingProgramIds)
                .And(Data.HierarchyToTrainingProgramLink.Columns.IsPrimary).IsEqualTo(false)
                .WithNoLock()
                .ExecuteTypedList<Data.HierarchyToTrainingProgramLink>();

            // Add the training program leaders
            List<Models.TrainingProgramLeader> leaders = new Select(
                Data.TrainingProgramLeader.IdColumn,
                Data.TrainingProgramLeader.UserIDColumn, 
                Data.TrainingProgramLeader.TrainingProgramIDColumn, 
                Data.TrainingProgramLeader.IsPrimaryColumn,
                Data.User.FirstNameColumn, 
                Data.User.LastNameColumn, 
                Data.User.MiddleNameColumn,
                Data.User.AddressLine1Column,
                Data.User.AddressLine2Column,
                Data.User.CityColumn,
                Data.User.StateColumn,
                Data.User.ZipCodeColumn,
                Data.User.AlternateEmail1Column,
                Data.User.AlternateEmail2Column,
                Data.User.HomePhoneColumn,
                Data.User.WorkPhoneColumn,
                Data.User.CellPhoneColumn,
                Data.User.EmailColumn)
                .IncludeColumn(Data.User.CustomerIDColumn.QualifiedName, "CustomerID")
                .From<Data.User>()
                .InnerJoin(Data.TrainingProgramLeader.UserIDColumn, Data.User.IdColumn)
                .Where(Data.TrainingProgramLeader.Columns.TrainingProgramID).In(trainingProgramIds)
                .OrderDesc(Data.TrainingProgramLeader.IsPrimaryColumn.QualifiedName)
                .WithNoLock()
                .ExecuteTypedList<Models.TrainingProgramLeader>();

            // All parameters used in the training programs. This is another one where we will have to lookup in the array later
            List<string> parameterCodeJSON = onlineCourses.Where(o => !String.IsNullOrEmpty(o.Parameters)).Select(o => o.Parameters).ToList<string>();
            List<string> parameterCodes = new List<string>();
            foreach (string json in parameterCodeJSON)
            {
                parameterCodes = parameterCodes.Concat(Utilities.Deserialize<List<string>>(json)).ToList();
            }

            List<Models.TrainingProgramParameterOverride> trainingProgramParameterOverrides = Select.AllColumnsFrom<Data.TrainingProgramParameterOverrideDetail>()
                    .Where(Data.TrainingProgramParameterOverrideDetail.Columns.TrainingProgramID).In(trainingProgramIds)
                    .And(Data.TrainingProgramParameterOverrideDetail.Columns.Code).In(parameterCodes.ToArray())
                    .WithNoLock()
                    .ExecuteTypedList<Models.TrainingProgramParameterOverride>();

            // Training program message board links
            // Respects parent message boards
            // Indexed by parentTrainingProgramID Or training program id
            // To look up, use parent training program id when set
            Dictionary<int, int> trainingProgramMessageBoardDictionary = Select.AllColumnsFrom<Data.MessageBoard>()
                .InnerJoin(Data.Tables.TrainingProgram, Data.TrainingProgram.Columns.Id, Data.Tables.MessageBoard, Data.MessageBoard.Columns.TrainingProgramId)
                .LeftOuterJoin(Data.Tables.CustomerMessageBoardLink, Data.CustomerMessageBoardLink.Columns.CustomerId, Data.Tables.TrainingProgram, Data.TrainingProgram.Columns.CustomerID)
                .Where(Data.MessageBoard.TrainingProgramIdColumn).In(
                    parentTrainingProgramIds
                )
                //.And(Data.TrainingProgram.CustomerIDColumn).IsEqualTo(customer.Id)
                .Or(Data.CustomerMessageBoardLink.CustomerIdColumn).IsEqualTo(customer.ID)
                .WithNoLock()
                .ExecuteTypedList<Data.MessageBoard>()
                .DistinctBy(m => m.TrainingProgramId)
                .ToDictionary(m => m.TrainingProgramId.HasValue ? m.TrainingProgramId.Value : 0, m => m.Id);

            // Indexed by artisan theme id
            Dictionary<int, Models.ArtisanTheme> themesDictionary = Select.AllColumnsFrom<Data.ArtisanTheme>()
                    .Where(Data.ArtisanTheme.Columns.Id).In(trainingProgramData.Select(t => t.SkinOverride))
                    .WithNoLock()
                    .ExecuteTypedList<Models.ArtisanTheme>()
                    .ToDictionary(a => a.Id);

            List<Models.TrainingProgramLicense> licenses = (new LicenseController()).GetLicensesForTrainingProgram(trainingProgramData, "").Data;

            List<Models.Book> books = Select.AllColumnsFrom<Data.Book>().IncludeColumn(Data.TrainingProgramBook.TrainingProgramIdColumn)
                .InnerJoin(Data.Tables.TrainingProgramBook, Data.TrainingProgramBook.Columns.BookId, Data.Tables.Book, Data.Book.Columns.Id)
                .Where(Data.TrainingProgramBook.TrainingProgramIdColumn).In(trainingProgramIds)
                .WithNoLock()
                .ExecuteTypedList<Models.Book>();

            List<Models.TrainingProgramAccreditation> accreditations = Select.AllColumnsFrom<Data.TrainingProgramAccreditation>().IncludeColumn(Data.TrainingProgramAccreditation.TrainingProgramIDColumn)
                .Where(Data.TrainingProgramAccreditation.TrainingProgramIDColumn).In(trainingProgramIds)
                .And(Data.TrainingProgramAccreditation.IsDeletedColumn).IsEqualTo(false)
                .WithNoLock()
                .ExecuteTypedList<Models.TrainingProgramAccreditation>();
             
            Dictionary<int, Data.User> owners = Select.AllColumnsFrom<Data.User>()
                .Where(Data.User.IdColumn).In(trainingPrograms.Select(t => t.OwnerID))
                .WithNoLock()
                .ExecuteTypedList<Data.User>()
                .Distinct()
                .ToDictionary(u => u.Id);

            List<TranscriptEntry> userTranscript = Data.SPs.GetTranscript(userId).ExecuteTypedList<TranscriptEntry>();

            // Because the training programs could have categories assigned from other customers, ignore the 
            // current logged in customer. 
            Dictionary<int, Models.Category> categories = CategoryController.GetCategoriesCrossCustomer(
                trainingProgramData.Select(t => t.CategoryID).ToArray(),
                CategoryType.TrainingProgram)
                .Distinct().ToDictionary(c => c.Id);

            // Load up all registered sessions
            List<int> sessionIds = trainingProgramData.Where(t => t.SessionCourseID > 0).Select(t => t.SessionCourseID.Value).Distinct().ToList();
            List<ClassroomClass> sessions = (new ClassroomController()).GetRegisteredClassesForCourses(userId, sessionIds.ToArray()).Data;
            Dictionary<int, List<ClassroomClass>> sessionDictionary = sessions.DistinctBy(sess => sess.CourseID).ToDictionary(uniqueSess => uniqueSess.CourseID, us => sessions.Where(originalSess => originalSess.CourseID == us.CourseID).ToList());


            List<Models.Author> authors = Select.AllColumnsFrom<Data.User>()
                .IncludeColumn(Data.OnlineCourseAuthorLink.OnlineCourseIdColumn)
                .InnerJoin(Data.OnlineCourseAuthorLink.UserIdColumn, Data.User.IdColumn)
                .Where(Data.OnlineCourseAuthorLink.OnlineCourseIdColumn).In(onlineCourses.Select(o => o.Id))
                .ExecuteTypedList<Models.Author>();
            

            // End loading
            TimeSpan span = DateTime.Now.Subtract(start);
            Log.Debug("Data load complete: " + span.TotalMilliseconds);
            #endregion

            start = DateTime.Now;
            Log.Debug("Looping training programs");
            foreach (Models.TrainingProgram model in trainingPrograms)
            {
                Data.TrainingProgram data = trainingProgramDictionary[model.Id];

                model.CopyFrom(trainingProgramDictionary[model.Id]);

                Data.User owner = owners[model.OwnerID];

                int newHireDuration = 0;
                bool newHireIndicator = false;

                if (user.NewHireIndicator && !managing)
                {
                    // the user is a new hire, and they're not trying to manage the TP itself;
                    // in this case, we want to adjust the start/due/end dates for the user
                    newHireIndicator = true;
                    newHireDuration = customer.NewHireDuration;
                }

                model.UserIDOverride = user.ID;

                // TODO - Cleanup all this copy over - most of it should be unnecessary
                model.Id = data.Id;

                model.Name = data.Name;
                model.Description = data.Description;

                model.NewHireDueDateOffset = data.NewHireDueDateOffset;
                model.NewHireEndDateOffset = data.NewHireEndDateOffset;
                model.NewHireStartDateOffset = data.NewHireStartDateOffset;
                model.NewHireOffsetEnabled = data.NewHireOffsetEnabled;
                model.NewHireTransitionPeriod = data.NewHireTransitionPeriod;

                model.PartnerID = data.PartnerID;
                model.IsSalesforce = data.IsSalesforce;


                // adjust the offsets according to the new hire status
                // by default, new hires get the start date = hire date, end date = hire date + new hire duration;
                // optionally, the offsets can be set at the training program level to override those numbers
                model.StartDate = (newHireIndicator ? user.HireDate.Value.AddDays(data.NewHireOffsetEnabled ? data.NewHireStartDateOffset.Value : 0) : data.StartDate).WithUtcFlag();
                model.DueDate = (newHireIndicator ? user.HireDate.Value.AddDays(data.NewHireOffsetEnabled ? data.NewHireDueDateOffset.Value : newHireDuration) : data.DueDate).WithUtcFlag();
                model.EndDate = (newHireIndicator ? user.HireDate.Value.AddDays(data.NewHireOffsetEnabled ? data.NewHireEndDateOffset.Value : newHireDuration) : data.EndDate).WithUtcFlag();

                // We don't want session logic to apply for new hires as new hires have their own logic applied to start and end date (Sessions shouldn't be allowed for new hires basically)
                if (data.SessionCourseID > 0 && !newHireIndicator)
                {
                    // We also don't want to apply the session logic when editing the training program as that will change the stored start and end dates.
                    if (!managing)
                    {
                        ApplyTrainingProgramSession(model, sessionDictionary.ContainsKey(data.SessionCourseID.Value) ? sessionDictionary[data.SessionCourseID.Value] : null);
                    }
                    else 
                    {
                        // If managing, all we care about is the session course name for the drop down.
                        //
                        // AM - June 17th 2016
                        // Session Dictionary only contains classes that have students registered in the session. If this training program
                        // has no users registered in any sessions, then there will not be a session name available when setting the 
                        // session combo box. 
                        // 
                        // Managing only loads a single training program, so performance is not an issue here
                        // so just going to look up the session course to grab the name for the combo box. 
                        // We can update the original query for training programs to include a left join for
                        // sessions, but since we are very close to release, I don't want to make a change 
                        // that could potentially impact all students. (Even though the change would be fairly simple)
                        //
                        // So for now, just looking up the session.
                        Data.Course sessionCourse = new Data.Course(data.SessionCourseID);
                        if (sessionCourse != null)
                        {
                            model.SessionCourseName = sessionCourse.Name;
                        }
                    }

                }

                model.EnforceRequiredOrder = data.EnforceRequiredOrder;
                model.EnforceCanMoveForwardIndicator = data.EnforceCanMoveForwardIndicator;
                if (data.MinimumElectives.HasValue)
                {
                    model.MinimumElectives = data.MinimumElectives.Value;
                }
                model.IsLive = data.IsLive;
                model.IsNewHire = data.IsNewHire;
                model.Cost = data.Cost;
                model.InternalCode = data.InternalCode;
                model.OwnerName = Model.FormatFullName(owner.FirstName, owner.MiddleName, owner.LastName);
                model.OwnerID = data.OwnerID;
                model.CategoryID = data.CategoryID;
                model.DisableScheduled = data.DisableScheduled;
                model.IsSurveyMandatory = data.IsSurveyMandatory;
                model.IsValidationEnabled = data.IsValidationEnabled.HasValue ? data.IsValidationEnabled.Value : false;
                model.ValidationInterval = data.ValidationInterval.HasValue ? data.ValidationInterval.Value : 0;
                model.MaxCourseWork = data.MaxCourseWork;

                model.SurveyID = data.SurveyID;

                if (data.AffidavitID.HasValue && data.AffidavitID.Value > 0)
                {
                    model.AffidavitID = affidavitDictionary[data.AffidavitID.Value].Id;
                    model.AffidavitName = affidavitDictionary[data.AffidavitID.Value].Name;
                    model.IsFinalAffidavitCompleted = finalAffidavitsCompleted.ContainsKey(model.Id) && finalAffidavitsCompleted[model.Id] == model.AffidavitID; 
                }
                else
                {
                    model.AffidavitID = 0;
                    model.AffidavitName = "None";
                    model.IsFinalAffidavitCompleted = false;
                }

                if (data.ProctorFormID > 0)
                {
                    model.ProctorFormID = data.ProctorFormID;
                    model.ProctorFormName = proctorFormDictionary[data.ProctorFormID].Name;
                }
                else
                {
                    model.ProctorFormID = 1;
                    model.TrainingProgramAffidavitName = "Default";
                }

                if (data.TrainingProgramAffidavitID.HasValue && data.TrainingProgramAffidavitID.Value > 0)
                {
                    model.TrainingProgramAffidavitID = affidavitDictionary[data.TrainingProgramAffidavitID.Value].Id;
                    model.TrainingProgramAffidavitName = affidavitDictionary[data.TrainingProgramAffidavitID.Value].Name;
                    model.IsTrainingProgramAffidavitCompleted = tpAffidavitsCompleted.ContainsKey(model.Id) && tpAffidavitsCompleted[model.Id] == model.TrainingProgramAffidavitID; 
                }
                else
                {
                    model.TrainingProgramAffidavitID = 0;
                    model.TrainingProgramAffidavitName = "None";
                    model.IsTrainingProgramAffidavitCompleted = false;
                }

                if (surveyDictionary.ContainsKey(model.SurveyID))
                {
                    model.SurveyName = surveyDictionary[model.SurveyID].Name;
                    model.OriginalSurveyName = originalSurveyDictionary[surveyDictionary[model.SurveyID].DuplicateFromID].Name;
                    model.OriginalSurveyID = originalSurveyDictionary[surveyDictionary[model.SurveyID].DuplicateFromID].Id;
                }

                int? maxCourseWork = model.MaxCourseWork ?? (categories.ContainsKey(model.CategoryID) ? categories[model.CategoryID].MaxCourseWork : null) ?? customer.MaxCourseWork;

                model.IsCourseWorkAllowed = maxCourseWork.HasValue && maxCourseWork.Value > 0 ?
                    todaysCourseWorkDuration < maxCourseWork.Value * 3600    
                    : true;

                if (model.PurchasedFromBundleId > 0 && bundleDictionary.ContainsKey(model.PurchasedFromBundleId.Value))
                {
                    model.PurchasedFromBundleName = bundleDictionary[model.PurchasedFromBundleId.Value].Name;
                }

                bool userHasValidationParameters = HasValidationParameters;
                bool trainingProgramRequiresValidation = model.IsValidationEnabled.HasValue && model.IsValidationEnabled.Value;

                DateTime now = new DateTime(DateTime.Now.Ticks); // So it wont change later on in the function

                if (minimumStartTimesDictionary.ContainsKey(model.Id)) {
                    model.UserStartTime = minimumStartTimesDictionary[model.Id];
                    model.HasUserStarted = true;
                } else {
                    model.UserStartTime = now;
                    model.HasUserStarted = false;
                }

                model.Files = files.Where(f => f.TrainingProgramID == model.Id || f.TrainingProgramID == model.ParentTrainingProgramID).Select(f => Models.Model.Create<Models.TrainingProgramFile>(f)).ToList();
                
                List<Data.TrainingProgramToCourseLink> courseLinksForTp = allCourseLinks.Where(l => l.TrainingProgramID == model.Id).ToList();

                Dictionary<int, Data.TrainingProgramToCourseLink> classroomCoursesForTp = courseLinksForTp
                    .Where(l => l.CourseTypeID == (int)CourseType.Classroom)
                    .ToDictionary(l => l.CourseID);

                Dictionary<int, List<Models.TrainingProgramCourse>> trainingProgramSyllabus = new Dictionary<int, List<Models.TrainingProgramCourse>>()
                {
                    { (int)SyllabusType.Required, new List<Models.TrainingProgramCourse>() },
                    { (int)SyllabusType.Elective, new List<Models.TrainingProgramCourse>() },
                    { (int)SyllabusType.Optional, new List<Models.TrainingProgramCourse>() },
                    { (int)SyllabusType.Final, new List<Models.TrainingProgramCourse>() }
                };

                List<Models.TrainingProgramCourse> coursesForTrainingProgram = new List<Models.TrainingProgramCourse>();

                classroomCourses.ForEach((c) =>
                {
                    if (classroomCoursesForTp.ContainsKey(c.CourseID))
                    {
                        TrainingProgramToCourseLink link = classroomCoursesForTp[c.CourseID];
                        c.CourseTypeID = (int)CourseType.Classroom;
                        c.SortOrder = link.RequiredSyllabusOrder.HasValue ? link.RequiredSyllabusOrder.Value : 0;
                        c.SyllabusTypeID = link.SyllabusTypeID;

                        c.DueDate = link.DueDate.HasValue ? link.DueDate.Value.WithUtcFlag() : model.DueDate.Value.WithUtcFlag();
                        c.ActiveAfterDate = link.ActiveAfterDate.HasValue ? link.ActiveAfterDate.Value.WithUtcFlag() : model.StartDate.Value.WithUtcFlag();

                        c.ActiveAfterMode = link.ActiveAfterMode.HasValue ? link.ActiveAfterMode.Value : 0;
                        c.DueMode = link.DueMode.HasValue ? link.DueMode.Value : 0;

                        c.EnforceCanMoveForwardIndicator = link.EnforceCanMoveForwardIndicator.HasValue ? link.EnforceCanMoveForwardIndicator.Value : false;
                        c.DenyAccessAfterDueDateIndicator = link.DenyAccessAfterDueDateIndicator;

                        c.IsUsingSession = model.IsUsingSessions;

                        c.HasAssignments = false;

                        // Use TP Override, if not set use course override, if not set use customer override, if not set use course value, if still not set then default to true
                        c.CertificateEnabledOverride = model.CertificateEnabledOverride ?? c.CertificateEnabledOverride ?? customer.CertificateEnabledOverride;
                        c.CertificateEnabled = c.CertificateEnabledOverride.HasValue ?
                                                c.CertificateEnabledOverride.Value :
                                                c.CertificateEnabled.HasValue ?
                                                c.CertificateEnabled.Value : true;

                        // For the online test if one eixsts (IsValidationEnabled refers to it being enabled on the online course linked to this class as test)
                        // Does the user require validation parameters? (DOB, SSN)
                        // True when User has validation parameters set, or training program and course do not require validation
                        // False when user does not have validation parameters set, and training program or course requires validation
                        c.IsUserValidationAllowed = userHasValidationParameters || (!trainingProgramRequiresValidation && !c.IsValidationEnabled);

                        // This is for the online test only if one is applied
                        c.IsCourseWorkAllowed = model.IsCourseWorkAllowed;

                        // Affidavit id - always 0 as these only apply to the Final, not classroom tests
                        // (This could easily be applied if set to the training program affidavit id if required)
                        c.AffidavitID = 0;

                        // If the training program has started or not. Used for course timing
                        c.HasTrainingProgramStarted = model.HasUserStarted;

                        // We will only have a single reference to each course, reset the completed/passed
                        // flags. They will be updated with add transcript details
                        c.Completed = false;
                        c.Passed = 0;

                        trainingProgramSyllabus[c.SyllabusTypeID].Add(c);
                        coursesForTrainingProgram.Add(c);
                    }
                });

                Dictionary<int, Data.TrainingProgramToCourseLink> onlineCoursesForTp = courseLinksForTp
                    .Where(l => l.CourseTypeID == (int)CourseType.Online)
                    .ToDictionary(l => l.CourseID);

                onlineCourses.ForEach((c) =>
                {
                    if (onlineCoursesForTp.ContainsKey(c.CourseID))
                    {
                        TrainingProgramToCourseLink link = onlineCoursesForTp[c.CourseID];
                        c.CourseTypeID = (int)CourseType.Online;
                        c.SortOrder = link.RequiredSyllabusOrder.HasValue ? link.RequiredSyllabusOrder.Value : 0;
                        c.SyllabusTypeID = link.SyllabusTypeID;
                        c.ProctorRequiredIndicator = link.ProctorRequiredIndicator.HasValue ? link.ProctorRequiredIndicator.Value : c.ProctorRequiredIndicator;
                        c.EnforceCanMoveForwardIndicator = link.EnforceCanMoveForwardIndicator.HasValue ? link.EnforceCanMoveForwardIndicator.Value : false;

                        c.DenyAccessAfterDueDateIndicator = link.DenyAccessAfterDueDateIndicator;

                        // Relative expirary should only be active if this is not a session based training program
                        // or this this a session based training program AND the session is active. Otherwise
                        // the dates must correspond to the session dates.
                        if (!model.IsUsingSessions || model.IsSessionActive)
                        {
                            c.ActiveAfterMinutes = link.ActiveAfterMinutes.HasValue ? link.ActiveAfterMinutes.Value : 0;
                            c.ExpiresAfterMinutes = link.ExpiresAfterMinutes.HasValue ? link.ExpiresAfterMinutes.Value : 0;
                        }

                        if (!model.IsUsingSessions)
                        {
                            if (managing)
                            {
                                c.DueDate = link.DueDate.HasValue ? link.DueDate.Value.WithUtcFlag() : link.DueDate;
                                c.ActiveAfterDate = link.ActiveAfterDate.HasValue ? link.ActiveAfterDate.Value.WithUtcFlag() : link.DueDate;
                            }
                            else
                            {
                                c.DueDate = link.DueDate.HasValue && link.DueDate.Value.WithUtcFlag() > model.StartDate.Value.WithUtcFlag() && link.DueDate.Value.WithUtcFlag() < model.EndDate.Value.WithUtcFlag() ?
                                                link.DueDate.Value.WithUtcFlag()
                                                : model.DueDate.Value.WithUtcFlag();
                                c.ActiveAfterDate = link.ActiveAfterDate.HasValue ? link.ActiveAfterDate.Value.WithUtcFlag() : model.StartDate.Value.WithUtcFlag();
                            }
                        }
                        else
                        {
                            if (managing)
                            {
                                c.DueDate = link.DueDate.HasValue ? link.DueDate.Value.WithUtcFlag() : link.DueDate;
                                c.ActiveAfterDate = link.ActiveAfterDate.HasValue ? link.ActiveAfterDate.Value.WithUtcFlag() : link.ActiveAfterDate;
                            }
                            else
                            {
                                c.DueDate = model.DueDate.Value;
                                c.ActiveAfterDate = model.StartDate.Value;
                                if (!model.IsSessionActive && model.Session == null)
                                {
                                    c.DenyAccessAfterDueDateIndicator = true;
                                }
                            }
                        }

                        c.IsUsingSession = model.IsUsingSessions;

                        c.DueMode = link.DueMode.HasValue ? link.DueMode.Value : 0;
                        c.ActiveAfterMode = link.ActiveAfterMode.HasValue ? link.ActiveAfterMode.Value : 0;

                        c.HasAssignments = c.PublishedArtisanCourseID.HasValue ?
                            (coursesWithAssignmentsDictionary.ContainsKey(c.PublishedArtisanCourseID.Value) ? true : false) :
                            false;

                        c.AssignmentsInCourse = c.PublishedArtisanCourseID.HasValue ?
                            (coursesWithAssignmentsDictionary.ContainsKey(c.PublishedArtisanCourseID.Value) ? coursesWithAssignmentsDictionary[c.PublishedArtisanCourseID.Value] : 0) :
                            0;
                        // Assume no assignments have been completed yet. Adding the transcript will override this.
                        c.AssignmentsCompleted = 0;
                        
                        c.IsRelativeActiveAfter = c.ActiveAfterMinutes > 0 && (c.ActiveAfterDate == model.StartDate.Value.WithUtcFlag() || c.ActiveAfterDate.Value.Year <= DefaultDateTime.Year);
                        c.IsRelativeDueDate = c.ExpiresAfterMinutes > 0 && c.DueDate == model.DueDate.Value.WithUtcFlag();

                        // Use TP Override, if not set use course override, if not set use customer override, if not set use course value, if still not set then default to true
                        c.CertificateEnabledOverride = model.CertificateEnabledOverride ?? c.CertificateEnabledOverride ?? customer.CertificateEnabledOverride;
                        c.CertificateEnabled = c.CertificateEnabledOverride.HasValue ?
                                                c.CertificateEnabledOverride.Value :
                                                c.CertificateEnabled.HasValue ?
                                                c.CertificateEnabled.Value : true;

                        // Does the user require validation parameters? (DOB, SSN)
                        // True when User has validation parameters set, or training program and course do not require validation
                        // False when user does not have validation parameters set, and training program or course requires validation
                        c.IsUserValidationAllowed = userHasValidationParameters || (!trainingProgramRequiresValidation && !c.IsValidationEnabled);

                        // Check affidavit
                        // Will only be applied if the course is a final and we use a final affidavit
                        // or it is a regular course, we use a training program affidavit, and the training program affidavit has not yet been completed
                        if (c.SyllabusTypeID == (int)SyllabusType.Final && model.AffidavitID > 0)
                        {
                            c.AffidavitID = model.AffidavitID;
                        }
                        else if (!model.IsTrainingProgramAffidavitCompleted && model.TrainingProgramAffidavitID > 0)
                        {
                            c.AffidavitID = model.TrainingProgramAffidavitID;
                        }
                        else
                        {
                            c.AffidavitID = 0;
                        }


                        c.IsCourseWorkAllowed = model.IsCourseWorkAllowed;

                        // Flag to indicate whether the user has started the training program or not
                        c.HasTrainingProgramStarted = model.HasUserStarted;
 
                        c.Authors = authors.Where(a => a.OnlineCourseID == c.Id).ToList();

                        // We will only have a single reference to each course, reset the completed/passed
                        // flags. They will be updated with add transcript details
                        c.Completed = false;
                        c.Passed = 0;

                        trainingProgramSyllabus[c.SyllabusTypeID].Add(c);
                        coursesForTrainingProgram.Add(c);
                    }
                });

                // We need the TPR to determine which surveys are complete, so we can determine
                // whether or not to show the certificate badge. This is not always populated by
                // this point (ie: in rollups), so we make sure it is.
                if (model.TrainingProgramRollup == null)
                {
                    model.TrainingProgramRollup = Select.AllColumnsFrom<Data.TrainingProgramRollup>()
                        .Where(Data.TrainingProgramRollup.TrainingProgramIDColumn).IsEqualTo(model.Id)
                        .And(Data.TrainingProgramRollup.UserIDColumn).IsEqualTo(userId)
                        .WithNoLock()
                        .ExecuteSingle<Models.TrainingProgramRollup>();
                }

                // Only retrieve actual surveys for single requests to avoid hammering the DB.
                if (isSingle)
                {
                    model.HasAccreditationSurveyData = true;

                    var nowStr = DateTime.Now.ToString();

                    var surveyQuery = new Select()
                        .IncludeColumn(Data.AccreditationBoardSurveyAssignment.OnlineCourseIDColumn.QualifiedName, "Id")
                        .From(Data.TrainingProgramAccreditation.Schema)
                        .InnerJoin(Data.AccreditationBoard.Schema)
                        .InnerJoin(Data.AccreditationBoardSurveyAssignment.AccreditationBoardIDColumn, Data.AccreditationBoard.IdColumn)
                        .InnerJoin(Data.AccreditationBoardProfession.AccreditationBoardIDColumn, Data.AccreditationBoard.IdColumn)
                        .Where(Data.TrainingProgramAccreditation.TrainingProgramIDColumn).IsEqualTo(data.Id)
                        .And(Data.TrainingProgramAccreditation.IsDeletedColumn).IsEqualTo(false)
                        .And(Data.TrainingProgramAccreditation.StatusColumn).IsEqualTo((int)AccreditationStatus.Accredited)
                        .And(Data.TrainingProgramAccreditation.StartDateColumn).IsLessThanOrEqualTo(nowStr)
                        .And(Data.TrainingProgramAccreditation.ExpiryDateColumn).IsGreaterThanOrEqualTo(nowStr)
                        .And(Data.AccreditationBoard.Columns.IsDeleted).IsEqualTo(false);
                    
                    if (model.TrainingProgramRollup != null && model.TrainingProgramRollup.ProfessionID != null) {
                        surveyQuery = surveyQuery.And(Data.AccreditationBoardProfession.ProfessionIDColumn).IsEqualTo(model.TrainingProgramRollup.ProfessionID);
                    }

                    var surveys = surveyQuery.OrderAsc(Data.AccreditationBoardSurveyAssignment.RankColumn.QualifiedName)
                        .ExecuteTypedList<AccreditationBoardSurvey>();

                    if (surveys.Count > 0)
                    {
                        var ids = surveys.Select(s => s.Id).Distinct().ToList();

                        var surveyResponses = Select.AllColumnsFrom<Data.TrainingProgramAccreditationSurveyRollup>()
                            .Where(Data.TrainingProgramAccreditationSurveyRollup.UserIDColumn).IsEqualTo(userId)
                            .And(Data.TrainingProgramAccreditationSurveyRollup.TrainingProgramIDColumn).IsEqualTo(data.Id)
                            .And(Data.TrainingProgramAccreditationSurveyRollup.OnlineCourseIDColumn).In(ids)
                            .WithNoLock()
                            .ExecuteTypedList<Data.TrainingProgramAccreditationSurveyRollup>()
                            .ToDictionary(
                                r => r.OnlineCourseID,
                                r => r.IsSurveyComplete);

                        var surveyCourses = Select.AllColumnsFrom<Data.OnlineCourse>()
                            .Where(Data.OnlineCourse.IdColumn).In(ids)
                            .WithNoLock()
                            .ExecuteTypedList<Models.TrainingProgramCourse>();

                        surveyCourses.ForEach(sc =>
                        {
                            sc.CourseTypeID = (int)CourseType.Online;
                            sc.SyllabusTypeID = (int)SyllabusType.Survey;
                            sc.IsUseAutomaticDuration = true;
                            sc.IsDisplayAssignmentLaunchLink = true;
                            sc.IsSurvey = true;
                            sc.IsSurveyMandatory = true;
                            sc.IsCourseWorkAllowed = true;
                            sc.IsUserValidationAllowed = true;

                            if (surveyResponses.ContainsKey(sc.CourseID))
                            {
                                sc.Completed = surveyResponses[sc.CourseID];
                                if (sc.Completed)
                                {
                                    sc.Passed = 1;
                                }
                            }
                        });

                        model.AccreditationSurveys = surveyCourses;
                    }
                    else
                    {
                        model.AccreditationSurveys = new List<Models.TrainingProgramCourse>();
                    }
                }

                DateTime dtMaxPassedDate = DateTime.MinValue;
                model.Completed = CourseController.AddTranscriptDetails(userId, coursesForTrainingProgram.ToCourseList(), model.Id, out dtMaxPassedDate, userTranscript, data);
                
                // Update the relative start/due date for each course 
                // we have to do this after adding the transcript details as
                // that will give us access to the completion dates
                // if after last course completion is being used.
                Models.TrainingProgramCourse previousCourse = null;
                foreach (Models.TrainingProgramCourse c in coursesForTrainingProgram.OrderBy(c => c.SyllabusTypeID).ThenBy(c => c.SortOrder))
                {
                    if (c.CourseTypeID == (int)CourseType.Online)
                    {
                        if (previousCourse != null && previousCourse.FirstCompletionDate.HasValue)
                        {
                            c.PreviousCourseFirstCompletionDate = previousCourse.FirstCompletionDate;
                        }

                        double timeSinceStart = 0;

                        if (c.ActiveAfterMode == (int)RelativeTimeMode.Session && model.SessionCourseID > 0)
                        {
                            // In the case where we are using a session, start time will be in UTC.
                            timeSinceStart = (DateTime.UtcNow - model.StartDate.Value.WithUtcFlag()).TotalMinutes;
                        }
                        else if (c.ActiveAfterMode == (int)RelativeTimeMode.PreviousCourseEnd && previousCourse != null)
                        {
                            if (previousCourse.FirstCompletionDate.HasValue)
                            {
                                timeSinceStart = (DateTime.UtcNow - previousCourse.FirstCompletionDate.Value.WithUtcFlag()).TotalMinutes;
                            }
                        } 
                        else
                        {
                            // In the case of training program, start time will be recorded using DateTime.Now
                            // so we need to use DateTime.Now to compare
                            timeSinceStart = model.UserStartTime.HasValue ? (now - model.UserStartTime.Value).TotalMinutes : 0;
                        }

                        c.MinutesUntilActive = c.IsRelativeActiveAfter
                                ? (int)(c.ActiveAfterMinutes.Value - Math.Floor(timeSinceStart))
                                : 0;

                        if (c.IsRelativeDueDate)
                        {
                            DateTime startTime = DefaultDateTime;

                            switch (c.DueMode)
                            {
                                case (int)RelativeTimeMode.Session:
                                case (int)RelativeTimeMode.TrainingProgram:
                                    if (model.SessionCourseID > 0 && c.DueMode == (int)RelativeTimeMode.Session)
                                    {
                                        startTime = model.StartDate.Value.WithUtcFlag();
                                    }
                                    else if (model.UserStartTime.HasValue)
                                    {
                                        startTime = model.UserStartTime.Value;
                                    }
                                    break;
                                case (int)RelativeTimeMode.Course:
                                    if (onlineCourseStartTimeDictionary.ContainsKey(model.Id + "-" + c.Id))
                                    {
                                        startTime = onlineCourseStartTimeDictionary[model.Id + "-" + c.Id].StartTime;
                                    }
                                    break;
                                case (int)RelativeTimeMode.FinalUnlocked:
                                    if (model.TrainingProgramRollup != null && model.TrainingProgramRollup.IsFinalAvailable && model.TrainingProgramRollup.FinalUnlockedOn.HasValue)
                                    {
                                        startTime = model.TrainingProgramRollup.FinalUnlockedOn.Value.WithUtcFlag();
                                    }
                                    break;
                            }

                            if (startTime != DefaultDateTime)
                            {
                                DateTime currentTime = c.DueMode == (int)RelativeTimeMode.FinalUnlocked || (model.SessionCourseID > 0 && c.DueMode == (int)RelativeTimeMode.Session) ?
                                    DateTime.UtcNow : // Session start time saved in UTC
                                    now; // User course start time saved in local server time

                                double timeSinceCourseStart = Math.Ceiling((currentTime - startTime).TotalMinutes);

                                c.HasLaunched = true;
                                c.MinutesUntilExpired = (int)(c.ExpiresAfterMinutes.Value - timeSinceCourseStart);
                            }
                            else
                            {
                                c.HasLaunched = false;
                                c.MinutesUntilExpired = (int)c.ExpiresAfterMinutes.Value;
                            }
                        }
                        previousCourse = c;
                    }
                }

                model.RequiredCourses = trainingProgramSyllabus[(int)SyllabusType.Required].OrderBy(c => c.SortOrder).ToList();
                model.ElectiveCourses = trainingProgramSyllabus[(int)SyllabusType.Elective].OrderBy(c => c.SortOrder).ToList();
                model.OptionalCourses = trainingProgramSyllabus[(int)SyllabusType.Optional].OrderBy(c => c.SortOrder).ToList();

                // Strip old failed exams, and future unattempted exams.
                List<Models.TrainingProgramCourse> allFinalAssessments = trainingProgramSyllabus[(int)SyllabusType.Final].OrderBy(c => c.SortOrder).ToList();

                if (!managing && !isAdminView && allFinalAssessments.Count > 0)
                {
                    model.FinalAssessments = allFinalAssessments.Where(c => c.Passed == 1 || !c.Completed).Take(1).ToList();

                    if (model.FinalAssessments.Count == 0)
                    {
                        model.FinalAssessments = new List<Models.TrainingProgramCourse> { allFinalAssessments.Last() };
                    }

                    // Bring the score forward from the last exam so the user can see what they received.
                    Models.TrainingProgramCourse exam = model.FinalAssessments.First();
                    int examIndex = allFinalAssessments.IndexOf(exam);
                    if (examIndex > 0)
                    {
                        string maxScore = "";
                        int currentScore = 0;
                        if (int.TryParse(exam.Score, out currentScore))
                        {
                            maxScore = allFinalAssessments.MaxBy(f =>
                            {
                                if (!string.IsNullOrEmpty(f.Score))
                                {
                                    return int.Parse(f.Score);
                                }
                                else
                                {
                                    return 0;
                                }
                            }).Score;
                        }
                        else
                        {
                            maxScore = allFinalAssessments.MaxBy(f => f.Score).Score;
                        }
                        exam.Score = maxScore;
                    }

                    coursesForTrainingProgram.RemoveAll(c => c.SyllabusTypeID == (int)SyllabusType.Final && c.Id != model.FinalAssessments.First().Id);
                }
                else
                {
                    model.FinalAssessments = allFinalAssessments;
                }

                model.CourseCount = model.RequiredCourses.Count + model.ElectiveCourses.Count + model.OptionalCourses.Count + model.FinalAssessments.Count;

                if (model.Completed)
                {
                    model.DateCompleted = dtMaxPassedDate.WithUtcFlag();
                }

                // Check if the survey has been completed
                if (model.SurveyID > 0 && surveyTakenDictionary.ContainsKey(model.Id))
                {
                    model.SurveyTaken = surveyTakenDictionary[model.Id];
                }

                // Check if any mandatory course surveys are incomplete
                // and if any courses require assignments
                model.SurveysRequired = false;
                model.AssignmentsRequired = false;

                int requiredCoursesComplete = 0;
                int electiveCoursesComplete = 0;
                int finalCoursesComplete = 0;

                foreach (Models.TrainingProgramCourse course in coursesForTrainingProgram)
                {

                    if (course.Passed.HasValue && course.Passed.Value > 0)
                    {
                        course.Completed = true;
                    }

                    if (course.SurveyID > 0 && !course.SurveyTaken && course.IsSurveyMandatory)
                    {
                        model.SurveysRequired = true;
                        course.Completed = false;
                    }
                    if (course.IsAssignmentRequired)
                    {
                        model.AssignmentsRequired = true;
                        course.Completed = false;
                    }

                    if (course.Completed)
                    {
                        switch (course.SyllabusTypeID)
                        {
                            case (int)SyllabusType.Required:
                                requiredCoursesComplete++;
                                break;
                            case (int)SyllabusType.Elective:
                                electiveCoursesComplete++;
                                break;
                            case (int)SyllabusType.Final:
                                finalCoursesComplete++;
                                break;
                        }
                    }
                }


                if (electiveCoursesComplete > model.MinimumElectives)
                {
                    electiveCoursesComplete = model.MinimumElectives;
                }

                int requiredCourseCount = model.RequiredCourses.Count + model.MinimumElectives + model.FinalAssessments.Count;
                int totalCoursesComplete = electiveCoursesComplete + requiredCoursesComplete + finalCoursesComplete;

                model.PercentComplete = requiredCourseCount > 0 ? (int)Math.Round(((double)totalCoursesComplete / requiredCourseCount) * 100) : 0;

                // Add on the message board id for any classes 
                foreach (Models.Course course in coursesForTrainingProgram.ToCourseList())
                {
                    if (course.RegistrationID > 0 && course.ClassID > 0 && messageBoardDictionary.ContainsKey(course.ClassID))
                    {
                        course.MessageBoardID = messageBoardDictionary[course.ClassID].ID;
                    }
                }

                model.PrimaryAssignment = primaryAssignments
                    .Where(a => a.TrainingProgramID == model.Id)
                    .Select(a =>
                    {
                        return new Models.HierarchyLink
                        {
                            HierarchyNodeID = a.HierarchyNodeID,
                            HierarchyTypeID = a.HierarchyTypeID
                        };
                    })
                    .ToList();

                model.SecondaryAssignment = secondaryAssignments
                    .Where(a => a.TrainingProgramID == model.Id)
                    .Select(a =>
                    {
                        return new Models.HierarchyLink
                        {
                            HierarchyNodeID = a.HierarchyNodeID,
                            HierarchyTypeID = a.HierarchyTypeID
                        };
                    })
                    .ToList();

                if (isSalesforce || model.ParentTrainingProgramID > 0)
                {
                    model.Leaders = leaders.Where(l => l.TrainingProgramID == model.Id).ToList();
                }
                else
                {
                    model.Leaders = leaders.Where(l => l.TrainingProgramID == model.Id && l.CustomerID == customer.ID).ToList();
                }

                model.Accreditations = accreditations.Where(a => a.TrainingProgramId == model.Id).ToList();

                model.TrainingProgramParameterOverrides = trainingProgramParameterOverrides.Where(t => t.TrainingProgramID == model.Id).ToList();

                int messageBoardTrainingProgramId = model.ParentTrainingProgramID > 0 ? model.ParentTrainingProgramID : model.Id;
                if (trainingProgramMessageBoardDictionary.ContainsKey(messageBoardTrainingProgramId))
                {
                    model.MessageBoardID = trainingProgramMessageBoardDictionary[messageBoardTrainingProgramId];
                }

                // Get the category text

                if (categories.ContainsKey(model.CategoryID))
                {
                    model.LevelIndentText = categories[model.CategoryID].LevelIndentText;
                }

                if (model.SkinOverride.HasValue && model.SkinOverride.Value > 0 && themesDictionary.ContainsKey(model.SkinOverride.Value))
                {
                    model.Theme = themesDictionary[model.SkinOverride.Value];
                }

                // We should only need the full theme flavor object when managing. Same likely goes for the SkinOverride above
                // as this is really only to show the name in the drop down. 
                // Managing only works with one training program at a time so this will not be a performance hit. 
                if (managing && model.ThemeFlavorOverrideID.HasValue)
                {
                    model.ThemeFlavor = Select.AllColumnsFrom<Data.Theme>()
                        .Where(Data.Theme.IdColumn).IsEqualTo(model.ThemeFlavorOverrideID.Value)
                        .ExecuteSingle<Models.Theme>();
                }


                model.Licenses = licenses.Where(l => l.TrainingProgramID == model.Id || l.TrainingProgramID == model.ParentTrainingProgramID).ToList();

                model.Books = books.Where(b => b.TrainingProgramId == model.Id || b.TrainingProgramId == model.ParentTrainingProgramID).ToList();
            }
            span = DateTime.Now.Subtract(start);
            Log.Debug("Looping complete " + span.TotalMilliseconds);

            span = DateTime.Now.Subtract(startOverall);
            Log.Debug("Single TP Time Optimized: " + span.TotalMilliseconds);
        }

        private void ApplyTrainingProgramSession(ITrainingProgram trainingProgram, List<ClassroomClass> sessionClasses)
        {
            if (trainingProgram.SessionCourseID > 0)
            {
                trainingProgram.IsUsingSessions = true;
                trainingProgram.IsSessionRegistered = false;

                // Find the session we want to use to set this training program's start and end date.
                // If today's date is within the start and end time of a registered session use those dates as the start/end
                // If today's date is not within a registered session, use the closest future session
                // If there are no future registered sessions, use the closest past session.
                // If there are no sessions, use today's date as the end date.
                ClassroomClass lastSession = null; // Last session closest to todays date
                ClassroomClass nextSession = null; // upcoming session closest to todays date
                ClassroomClass activeSession = null; // session containing todays date. If multiple, use the latest start date
                
                if (sessionClasses != null && sessionClasses.Count > 0)
                {
                    trainingProgram.SessionCourseName = sessionClasses[0].CourseName;

                    foreach (ClassroomClass c in sessionClasses)
                    {
                        DateTime minStartDate = c.MinClassDate.Value.WithUtcFlag();
                        DateTime maxEndDate = c.MaxClassDate.Value.AddMinutes(c.DailyDuration).WithUtcFlag();

                        if (maxEndDate < DateTime.UtcNow &&
                            (lastSession == null ||
                             lastSession.MaxClassDate.Value.AddMinutes(lastSession.DailyDuration) < maxEndDate)) // if multiple are expired, use the one with the lastest max end date
                        {
                            lastSession = c;
                        }
                        else if (minStartDate > DateTime.UtcNow &&
                            (nextSession == null ||
                             nextSession.MinClassDate.Value > minStartDate)) // if multiple are coming up, use the earliest upcoming session
                        {
                            nextSession = c;
                        }
                        else if (minStartDate < DateTime.UtcNow && maxEndDate > DateTime.UtcNow &&
                            (activeSession == null ||
                             activeSession.MinClassDate.Value < minStartDate)) // If multiple are active, pick the one with the latest min start date
                        {
                            activeSession = c;
                        }
                    }
                }

                trainingProgram.Session = activeSession ?? nextSession ?? lastSession;

                if (trainingProgram.Session != null)
                {
                    trainingProgram.StartDate = trainingProgram.Session.MinClassDate.Value.WithUtcFlag();
                    trainingProgram.DueDate = trainingProgram.Session.MaxClassDate.Value.AddMinutes(trainingProgram.Session.DailyDuration).WithUtcFlag();
                    trainingProgram.EndDate = trainingProgram.DueDate;

                    trainingProgram.IsSessionActive = trainingProgram.StartDate < DateTime.UtcNow && trainingProgram.EndDate > DateTime.UtcNow;
                    trainingProgram.IsSessionRegistered = true;

                    // subtract 1 second from the end/due date so if it falls on an hour like midnight, we show it ending the day before
                    // We only do this if its a session based training program and we are not managing the tp
                    // as otherwise every save will cause our due/end dates to decrease.
                    trainingProgram.DueDate = trainingProgram.DueDate.HasValue ? trainingProgram.DueDate.Value.Subtract(new TimeSpan(0, 0, 1)) : trainingProgram.DueDate;
                    trainingProgram.EndDate = trainingProgram.EndDate.HasValue ? trainingProgram.EndDate.Value.Subtract(new TimeSpan(0, 0, 1)) : trainingProgram.EndDate;
                } 
                else 
                {
                    trainingProgram.EndDate = DateTime.UtcNow.WithUtcFlag();
                    trainingProgram.DueDate = DateTime.UtcNow.WithUtcFlag();
                    trainingProgram.IsSessionActive = false;
                    trainingProgram.IsSessionRegistered = false;
                }
            }
        }

        /// <summary>
        /// Returns a listing of training programs with enough detail to show the user status in the training program.
        /// Does not include all details such as individual classes
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newHire"></param>
        /// <param name="searchText"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public PagedResult<Models.TrainingProgramSummaryDetail> ListTrainingProgramsForUser(int userId, bool newHire, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, DateTime? startDate = null, DateTime? endDate = null)
        {
            Data.User user = new Data.User(userId);
            Data.Customer customer = new Data.Customer(user.CustomerID);

            /*if (userId != UserID && !UserIsSalesChannelAdmin)
            {
                throw new Exception("You do not have permission to view this users training programs.");
            }*/

            orderBy = string.IsNullOrEmpty(orderBy) ? "Name" : orderBy;

            List<Models.TrainingProgramSummaryDetail> trainingProgramSummaryDetails = Data.SPs
                .GetTrainingProgramsForUserWithInProgressToggle(userId, newHire, searchText, orderBy, orderDir == OrderDirection.Descending ? "desc" : "asc", pageIndex, pageSize, startDate, endDate)
                .ExecuteTypedList<Models.TrainingProgramSummaryDetail>();

            int total = 0;
            int newHireDuration = 0;
            bool newHireIndicator = false;

            if (user.NewHireIndicator)
            {
                // the user is a new hire, and they're not trying to manage the TP itself;
                // in this case, we want to adjust the start/due/end dates for the user
                newHireIndicator = true;
                newHireDuration = customer.NewHireDuration;
            }

            if (trainingProgramSummaryDetails.Count > 0)
            {
                total = trainingProgramSummaryDetails[0].TotalSize;

                // Need to calculate the sessions
                // Load up all registered sessions
                List<int> sessionIds = trainingProgramSummaryDetails.Where(t => t.SessionCourseID > 0).Select(t => t.SessionCourseID.Value).Distinct().ToList();
                List<ClassroomClass> sessions = (new ClassroomController()).GetRegisteredClassesForCourses(userId, sessionIds.ToArray()).Data;
                Dictionary<int, List<ClassroomClass>> sessionDictionary = sessions.DistinctBy(sess => sess.CourseID).ToDictionary(uniqueSess => uniqueSess.CourseID, us => sessions.Where(originalSess => originalSess.CourseID == us.CourseID).ToList());

                foreach (Models.TrainingProgramSummaryDetail tpSummary in trainingProgramSummaryDetails)
                {
                    tpSummary.StartDate = (newHireIndicator ? user.HireDate.Value.AddDays(tpSummary.NewHireOffsetEnabled ? tpSummary.NewHireStartDateOffset.Value : 0) : tpSummary.StartDate);
                    tpSummary.DueDate = (newHireIndicator ? user.HireDate.Value.AddDays(tpSummary.NewHireOffsetEnabled ? tpSummary.NewHireDueDateOffset.Value : newHireDuration) : tpSummary.DueDate);
                    tpSummary.EndDate = (newHireIndicator ? user.HireDate.Value.AddDays(tpSummary.NewHireOffsetEnabled ? tpSummary.NewHireEndDateOffset.Value : newHireDuration) : tpSummary.EndDate);

                    tpSummary.UserID = userId;

                    if (tpSummary.SessionCourseID.HasValue && sessionDictionary.ContainsKey(tpSummary.SessionCourseID.Value))
                    {
                        ApplyTrainingProgramSession(tpSummary, sessionDictionary[tpSummary.SessionCourseID.Value]);
                    }
                }
            }

            return new PagedResult<Models.TrainingProgramSummaryDetail>(trainingProgramSummaryDetails, pageIndex, pageSize, total);
        }

        public PagedResult<Models.TrainingProgram> FindTrainingProgramsForUserNew(int userId, bool newHire, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize, DateTime? startDate = null, DateTime? endDate = null)
        {
            orderBy = string.IsNullOrEmpty(orderBy) ? "Name" : orderBy;

            List<Models.TrainingProgram> result = new List<Models.TrainingProgram>();

            List<Models.TrainingProgram> trainingPrograms = Data.SPs
                .GetTrainingProgramsForUserWithInProgressToggle(userId, newHire, searchText, orderBy, orderDir == OrderDirection.Descending ? "desc" : "asc", pageIndex, pageSize, startDate, endDate)
                .ExecuteTypedList<Models.TrainingProgram>();
            
            int total = 0;
            if (trainingPrograms.Count > 0)
            {
                AddTrainingProgramDetails(userId, trainingPrograms, false, false);
                result = trainingPrograms;
                total = trainingPrograms[0].TotalSize;
            }

            return new Models.PagedResult<Models.TrainingProgram>(trainingPrograms, pageIndex, pageSize, total);
        }

        /// <summary>
        /// For a given user, finds any training programs that match the specified search text and gets all the details about the TP
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="searchText"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedResult<Models.TrainingProgram> FindTrainingProgramsForUser(int userId, bool newHire, string searchText, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            bool isOptimized = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["optimizeFindTrainingProgramsForUser"]);

            if (isOptimized)
            {
                return FindTrainingProgramsForUserNew(userId, newHire, searchText, orderBy, orderDir, pageIndex, pageSize);
            }

            orderBy = string.IsNullOrEmpty(orderBy) ? "Name" : orderBy;

            using (IDataReader dr = Data.SPs
                .GetTrainingProgramsForUserWithInProgressToggle(userId, newHire, searchText, orderBy, orderDir == OrderDirection.Descending ? "desc" : "asc", pageIndex, pageSize, null, null)
                .GetReader())
            {
                List<Models.TrainingProgram> result = new List<Models.TrainingProgram>();
                int total = 0;
                while (dr.Read())
                {
                    // this has to come first, as the Load() call closes the reader at the end, which is right away
                    // if there is only one record
                    if (total == 0)
                    {
                        total = int.Parse(dr["TotalSize"].ToString());
                    }

                    Models.TrainingProgram tpDetails = this.GetTrainingProgramDetails(userId, (int)dr["ID"], false).Data;
                    if (!String.IsNullOrEmpty(dr["MessageBoardID"].ToString())) {
                        tpDetails.MessageBoardID = int.Parse(dr["MessageBoardID"].ToString());
                    }
                    if (!String.IsNullOrEmpty(dr["Sku"].ToString())) {
                        tpDetails.Sku = dr["Sku"].ToString();
                    }
                    result.Add(tpDetails);

                    //result.Add(new Models.TrainingProgram()
                    //{
                    //    StartDate = (DateTime)dr["StartDate"],
                    //    DueDate = (DateTime)dr["DueDate"],
                    //    EndDate = (DateTime)dr["EndDate"],
                    //    Id = (int)dr["ID"],
                    //    Name = (string)dr["Name"],
                    //    Description = (string)dr["Description"],
                    //    CourseCount = (int)dr["CourseCount"]
                    //});
                }
                return new Models.PagedResult<Models.TrainingProgram>(result, pageIndex, pageSize, total);
            }

        }

        /// <summary>
        /// Deletes a single message, validating the userid
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public SingleResult<bool> DeleteMessage(int userId, int messageId)
        {
            Data.Notification n = new Notification(messageId);
            if (n.RecipientID != userId)
            {
                throw new Exception("You don't have permission to delete the specified message.");
            }
            Data.Notification.Delete(messageId);
            return new SingleResult<bool>(true);
        }

        /// <summary>
        /// Deletes a single meeting, validating the user is an organizer
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public SingleResult<bool> DeleteMeeting(int userId, int meetingId)
        {
            Data.UserToGTMMeeting m = Select.AllColumnsFrom<Data.UserToGTMMeeting>()
                .Where(Data.UserToGTMMeeting.Columns.UserID).IsEqualTo(userId)
                .And(Data.UserToGTMMeeting.Columns.GTMMeetingID).IsEqualTo(meetingId)
                .ExecuteSingle<Data.UserToGTMMeeting>();

            if (m == null || m.IsOrganizer == 0)
            {
                throw new Exception("You don't have permission to delete the specified meeting.");
            }


            // send notifications
            GTMInvitationCollection invitations = new GTMInvitationCollection()
                .Where(GTMInvitation.Columns.GTMMeetingID, meetingId)
                .Load();

            GTMMeeting meeting = new GTMMeeting(meetingId);

            // these notifications have to be synchronous or they'll be deleted before we can send them
            foreach (GTMInvitation invitation in invitations)
            {
                string template = NotificationType.MeetingCancelledInternal;
                if (invitation.IsExternal)
                {
                    template = NotificationType.MeetingCancelledExternal;
                }

                List<NotificationDate> notificationDates = new List<NotificationDate>() {
                    new NotificationDate(){
                        StartDate = meeting.StartDateTime,
                        EndDate = meeting.EndDateTime,
                        Type = CalendarType.Timed,
                        InstanceID = meeting.Id.ToString(),
                        Method = CalendarMethod.Delete,
                        Name = "Meeting"
                    }
                };

                NotificationController.SendNotification(template, new NotificationOptions()
                {
                    TemplateObjects = new object[] { meeting, invitation },
                    Users = new int[] { invitation.UserID },
                    NotificationDates = notificationDates.ToArray()
                });

                Data.GTMInvitation.Destroy(invitation.Id);
            }

            // destroy the meeting
            Data.GTMMeeting.Destroy(meetingId);

            return new SingleResult<bool>(true);
        }

        /// <summary>
        /// Deletes all messages for a given user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public SingleResult<bool> DeleteAllMessages(int userId)
        {
            Data.Notification.Delete(Data.Notification.Columns.RecipientID, userId);
            return new SingleResult<bool>(true);
        }

        /// <summary>
        /// Gets a specific training program value. Returns null if the customer ids don't match.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public Stream GetTrainingProgramFile(int customerId, int userId, int fileId)
        {
            Data.TrainingProgramFile file = new Data.TrainingProgramFile(fileId);
            //Data.TrainingProgram tp = new Data.TrainingProgram(file.TrainingProgramID);

            var found = new CourseAssignmentController().FindTrainingPrograms(customerId, userId, string.Empty, string.Empty, OrderDirection.Default,
                0, 1, null, new List<GenericFilter>()
                {
                    new GenericFilter(){ 
                        ComparisonOperator = (int)ComparisonOperators.EqualTo, 
                        Property = Data.TrainingProgram.Columns.Id, 
                        Value = file.TrainingProgramID.ToString() 
                    }
                });
            if (!found.Success || found.TotalSize == 0)
            {
                return null;
            }

            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.FileName.Replace(" ", "_").Replace(";", "_"));
            string path = HttpContext.Current.Server.MapPath(TrainingProgramFilesLocation + fileId);
            if (System.IO.File.Exists(path))
            {
                // grab it from the file system
                return new MemoryStream(System.IO.File.ReadAllBytes(path));
            }
            else
            {
                // grab it from the database
                return new MemoryStream(file.FileBytes);
            }
        }

        /// <summary>
        /// Gets a class and it's associated files
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <param name="UserID"></param>
        /// <param name="classId"></param>
        /// <returns></returns>
        public SingleResult<ClassroomClass> GetClassDetails(int CustomerID, int UserID, int classId)
        {
            SingleResult<ClassroomClass> c = new ClassroomController().GetClass(CustomerID, UserID, classId);
            c.Data.Files = Select.AllColumnsFrom<Data.CourseFile>()
                .Where(Data.CourseFile.Columns.CourseID).IsEqualTo(c.Data.CourseID)
                .ExecuteTypedList<Models.CourseFile>();
            return c;
        }

        /// <summary>
        /// Returns the GTM email and password for a given meeting, provided the organizer is the specified user. Throws
        /// an exception if the user id and meeting organizer id don't match.
        /// </summary>
        /// <param name="CustomerID"></param>
        /// <param name="UserID"></param>
        /// <param name="meetingId"></param>
        /// <returns></returns>
        public SingleResult<GTMLoginInfo> GetGTMLoginInfo(int CustomerID, int UserID, int meetingId)
        {
            Data.GTMOrganizer organizer = Select.AllColumnsFrom<Data.GTMOrganizer>()
                .Where(Data.GTMOrganizer.Columns.UserID).IsEqualTo(UserID)
                .ExecuteSingle<Data.GTMOrganizer>();

            GTMMeeting meeting = new GTMMeeting(meetingId);
            if (organizer.Id == meeting.GTMOrganizerID)
            {
                return new SingleResult<GTMLoginInfo>(new GTMLoginInfo()
                {
                    Email = organizer.Email,
                    Password = organizer.Password,
                });
            }
            else
            {
                return new SingleResult<GTMLoginInfo>(new Exception("You are not the organizer for this meeting."));
            }
        }

        public SingleResult<bool> ChangePassword(PasswordChange passwordChange)
        {
            bool success = (new UserController()).ChangePassword(passwordChange.OldPassword, passwordChange.NewPassword);
            return new SingleResult<bool>(success);
        }

        public SimpleSingleResult<string> SwitchUser(int userId)
        {
            string subdomain = (new UserController()).SwitchUser(userId);
            return new SimpleSingleResult<string>(subdomain);
        }

        public SingleResult<bool> ExitUser()
        {
            bool success = (new UserController()).ExitUser();
            return new SingleResult<bool>(success);
        }

        // notes:
        // remove:
        //   -classes for which you're already registered for one offering of the course
        //   x -non-public, non-registered classes outside the window of all the training programs you're registered for

        /// <summary>
        /// Gets a list of upcoming events for a specific user.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="searchText"></param>
        /// <param name="orderBy"></param>
        /// <param name="orderDir"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PagedResult<Models.UpcomingEvent> FindUpcomingEventsForUser(int userId, string searchText, bool assignedOnly, string orderBy, OrderDirection orderDir, int pageIndex, int pageSize)
        {
            Log.Debug("Find upcoming events for user start");
            DateTime start = DateTime.Now;

            // we'll need some user info
            Data.User user = new Data.User(userId);

            // this handles all the upcoming meetings, public classes, and classes assigned because the person is an instructor
            SqlQuery query = Select.AllColumnsFrom<Data.SimpleUpcomingEvent>()
                .Where(Data.SimpleUpcomingEvent.Columns.UserID).In(new int[] { userId, 0 })
                .And(Data.SimpleUpcomingEvent.Columns.CustomerID).IsEqualTo(user.CustomerID)
                .And(Data.SimpleUpcomingEvent.Columns.StartDate).IsGreaterThan(DateTime.UtcNow.AddDays(-7));

            List<Models.UpcomingEvent> events = _FindUpcomingEventsForUser(query, user, assignedOnly);

            // HACK for OCL
            //var events = query.ExecuteTypedList<Models.UpcomingEvent>();

            if (events.Count == 0)
            {
                return new PagedResult<Models.UpcomingEvent>(events, pageIndex, pageSize, 0);
            }

            if (!string.IsNullOrEmpty(searchText))
            {
                events = events.Where(e => e.EventName.ToLower().Contains(searchText.ToLower())).ToList();
            }

            // stick to the last 2 hours for upcoming
            events = events.Where(e => e.StartDate.WithUtcFlag() > DateTime.UtcNow.AddHours(-2)).ToList();

            TimeSpan span = DateTime.Now.Subtract(start);
            Log.Debug("Get Upcoming Events for user: " + span.TotalMilliseconds);


            // return a paged resultset will all the data we've got
            return new PagedResult<Models.UpcomingEvent>(events.AsQueryable(), pageIndex, pageSize, orderBy, orderDir);
        }

        /// <summary>
        /// Lists all events for the user to help with checking message board access
        /// </summary>
        public List<Models.UpcomingEvent> FindAllEventsForUser(int userId)
        {
            // we'll need some user info
            Data.User user = new Data.User(userId);

            // this handles all the upcoming meetings, public classes, and classes assigned because the person is an instructor
            SqlQuery query = Select.AllColumnsFrom<Data.SimpleUpcomingEvent>()
                .Where(Data.SimpleUpcomingEvent.Columns.UserID).In(new int[] { userId, 0 })
                .And(Data.SimpleUpcomingEvent.Columns.CustomerID).IsEqualTo(user.CustomerID);

            return _FindUpcomingEventsForUser(query, user, true);
        }


        public MultipleResult<CalendarWeek> FindUpcomingEventsForUser(int userId, int year, int month, bool assignedOnly)
        {
            // we'll need some user info
            Data.User user = new Data.User(userId);

            // get the real date range
            DateTime dtStart = new DateTime(year, month, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime dtEnd = dtStart.AddMonths(1).AddSeconds(-1);

            // this handles all the upcoming meetings, public classes, and classes assigned because the person is an instructor
            SqlQuery query = Select.AllColumnsFrom<Data.SimpleUpcomingEvent>()
                .And(Data.SimpleUpcomingEvent.Columns.UserID).In(new int[] { userId, 0 })
                .And(Data.SimpleUpcomingEvent.Columns.CustomerID).IsEqualTo(user.CustomerID)
                .And(Data.SimpleUpcomingEvent.Columns.StartDate).IsGreaterThanOrEqualTo(dtStart)
                .And(Data.SimpleUpcomingEvent.Columns.StartDate).IsLessThanOrEqualTo(dtEnd);

            List<Models.UpcomingEvent> events = _FindUpcomingEventsForUser(query, user, assignedOnly);

            //TODO: validate timezone stuf....events should all get their utc timestamp set
            List<CalendarWeek> weeks = new List<CalendarWeek>();
            DateTime weekStart = dtStart.AddDays(-(int)dtStart.DayOfWeek);
            do
            {
                CalendarWeek calWeek = new CalendarWeek();
                for (int i = 0; i < 7; i++)
                {
                    DateTime date = weekStart.AddDays(i).WithUtcFlag();
                    calWeek[i] = new UpcomingEventsCalendarDay
                    {
                        Year = date.Year,
                        Month = date.Month,
                        Day = date.Day,
                        InCurrentMonth = (date.Month == month && date.Year == year),
                        Events = events.Where(
                            x => x.StartDate.Year == date.Year
                                && x.StartDate.Month == date.Month
                                && x.StartDate.Day == date.Day).ToList()
                    };
                }
                weeks.Add(calWeek);
                weekStart = weekStart.AddDays(7);
            } while (weekStart.Month == month && weekStart.Year == year);

            return new MultipleResult<CalendarWeek>(weeks);
        }

        /// <summary>
        /// Gets all the upcoming events for a user based on their assigned TPs and classes, and assigns appropriate details where available
        /// </summary>
        /// <param name="simpleUpcomingEventsQuery">The query to load up the initial list of upcoming events from the "Simple" view</param>
        /// <param name="searchText">Text to use to filter the results</param>
        /// <param name="user">The current user</param>
        /// <returns></returns>
        private List<Models.UpcomingEvent> _FindUpcomingEventsForUser(SqlQuery simpleUpcomingEventsQuery, Data.User user, bool assignedOnly)
        {
            DateTime start = DateTime.Now;
            Log.Debug("Finding Upcomging Events");
            // we get a full list (no paging) because we have to merge this with the results from the other following queries
            List<Models.UpcomingEvent> events = simpleUpcomingEventsQuery.ExecuteTypedList<Models.UpcomingEvent>();

            TimeSpan span = DateTime.Now.Subtract(start);
            
            Log.Debug("Simple query: " + span.TotalMilliseconds);
            start = DateTime.Now;
            // ok, now we need to deal with all the courses assigned via TPs - get all those classes, along with the TP info
            List<Models.UpcomingEvent> tpEvents = GetTrainingProgramEvents(user.CustomerID, user.Id, user.NewHireIndicator);
            span = DateTime.Now.Subtract(start);
            Log.Debug("Get training program events query: " + span.TotalMilliseconds);

            // merge the results together
            events.AddRange(tpEvents);

            // finally, we need to get a list of all the classes that are *not* in the list of classes assigned via TP, but *are*
            // assigned to this user manually and are also *not* in the list of registered public courses
            start = DateTime.Now;
            List<Models.UpcomingEvent> assignedClassEvents = GetAllClassEventsFiltered(user.CustomerID, user.Id, events);
            span = DateTime.Now.Subtract(start);
            Log.Debug("Get assigned class events: " + span.TotalMilliseconds);

            // merge the results together
            events.AddRange(assignedClassEvents);

            // do a little cleanup;  find all the registered classes, then exclude any class from the events that has the same course id, but the user is not registered
            start = DateTime.Now;

            List<int> registeredPublicCourses =
                events.Where(e => e.EventTypeID == (int)EventType.RegisteredClass || e.EventTypeID == (int)EventType.TrainingProgramClass).Select(e => e.GroupID).ToList();
            events.RemoveAll(e => registeredPublicCourses.Contains(e.GroupID) && e.UserID == 0);
            span = DateTime.Now.Subtract(start);
            Log.Debug("Public courses: " + span.TotalMilliseconds);
            // add meeting info, course details, etc, to the events; 
            // only TPs aren't added here, since we already had to load them up to get the assigned course list anyway
            start = DateTime.Now;

            events = AugmentUpcomingEvents(user.Id, events).ToList();
            span = DateTime.Now.Subtract(start);
            Log.Debug("Augmenting: " + span.TotalMilliseconds);

            start = DateTime.Now;

            if (assignedOnly)
            {
                events = events.Where(e => e.UserID > 0)
                    .Where(e => !(e.EventTypeID == (int)EventType.TrainingProgramClass && e.RegistrationID == 0))
                    .ToList();
            }

            // make sure all the utc flags are set
            foreach (Models.UpcomingEvent @event in events)
            {
                @event.StartDate = @event.StartDate.WithUtcFlag();
                if (@event.EndDate.HasValue)
                {
                    @event.EndDate = @event.EndDate.Value.WithUtcFlag();
                }
            }

            span = DateTime.Now.Subtract(start);
            Log.Debug("Tweaking Events: " + span.TotalMilliseconds);


            return events;
        }

        /// <summary>
        /// Gets all the events for a training program
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="isNewHire"></param>
        /// <returns></returns>
        private List<Models.UpcomingEvent> GetTrainingProgramEvents(int customerId, int userId, bool isNewHire)
        {
            List<Models.UpcomingEvent> events = new List<Models.UpcomingEvent>();

            // first, get all the TPs assigned to this user
            IEnumerable<Models.TrainingProgram> trainingPrograms = FindTrainingProgramsForUser(userId, isNewHire, string.Empty, "ID", OrderDirection.Ascending, 0, 100000).Data;
            CourseAssignmentController c = new CourseAssignmentController();

            // get all the lasses this user is registered for
            string sql = @"
SELECT 
	coalesce([Registration].[ID],0) as RegistrationID,
    coalesce([Registration].[RegistrationStatusID],0) as RegistrationStatusID,
	[Class].[ID] as ClassID,
        -- class params
	    [dbo].[Class].[Name] as ClassName, [dbo].[Class].[Description], [dbo].[Class].[InternalCode], [dbo].[Class].[RoomID], [dbo].[Class].[CourseID], 
        [dbo].[Class].[TimeZoneID], [dbo].[Class].[CapacityOverride], [dbo].[Class].[LockedIndicator], [dbo].[Class].[ModifiedBy], 
        [dbo].[Class].[CreatedBy], [dbo].[Class].[ModifiedOn], [dbo].[Class].[CreatedOn], [dbo].[Class].[AdditionalInstructions], 
        [dbo].[Class].[Notes], [dbo].[Class].[Objective], [dbo].[Class].[CustomerID], [dbo].[Class].[IsVirtual], [dbo].[Class].[SurveyID], 
        [dbo].[Class].[WebinarKey], [dbo].[Class].[IsThirdParty],
	    [dbo].[Class].[ApprovalRequired], 
	StartDateTime as StartDate,
	dateadd(minute, Duration, StartDateTime) as EndDate, 
	[dbo].[Registration].[ID] as RegistrationID
FROM [dbo].[Class]
INNER JOIN [dbo].[ClassDate] 
	ON [dbo].[Class].[ID] = [dbo].[ClassDate].[ClassID] 
INNER JOIN [dbo].[Registration] 
	ON [dbo].[Class].[ID] = [dbo].[Registration].[ClassID] and [dbo].[Registration].[RegistrantID] = @userId
";

            List<Models.Class> registeredClasses = new CodingHorror().ExecuteTypedList<Models.Class>(sql, userId);

            // for each TP, get the list of courses assigned
            foreach (Models.TrainingProgram trainingProgram in trainingPrograms)
            {
                List<List<Models.TrainingProgramCourse>> courseLists = new List<List<Models.TrainingProgramCourse>>()
                {
                    trainingProgram.RequiredCourses,
                    trainingProgram.OptionalCourses,
                    trainingProgram.ElectiveCourses,
                    trainingProgram.FinalAssessments
                };

                foreach (List<Models.TrainingProgramCourse> courseList in courseLists)
                {
                    // convert each list of courses into an upcoming event structure
                    events.AddRange(courseList
                        // only care about classroom for now
                        .Where(course => course.CourseTypeID == (int)CourseType.Classroom)
                        .SelectMany(course =>
                    {
                        List<Models.UpcomingEvent> upcomingClasses = new List<Models.UpcomingEvent>();
                        foreach (Models.Class cls in registeredClasses.Where(cl=> cl.CourseID == course.Id))
                        {
                            upcomingClasses.Add(new Models.UpcomingEvent()
                            {
                                EventID = cls.ClassID,
                                GroupID = course.Id,
                                Details = cls,
                                ExtendedDetails = trainingProgram,
                                StartDate = cls.StartDate.HasValue ? cls.StartDate.Value.WithUtcFlag() : DateTime.UtcNow,
                                EndDate = cls.EndDate.HasValue ? cls.EndDate.Value.WithUtcFlag() : cls.EndDate,
                                EventName = cls.ClassName,
                                EventType = "TP Class",
                                EventTypeID = (int)EventType.TrainingProgramClass,
                                JoinURL = string.Empty,
                                UserID = userId,
                                RegistrationID = cls.RegistrationID,
                                RegistrationStatusID = cls.RegistrationStatusID,
                                WebinarKey = course.WebinarKey,
                                WebinarLaunchURL = course.WebinarLaunchURL,
                                WebinarRegistrationID = course.WebinarRegistrationID,
                                TrainingProgramID = trainingProgram.Id,
                                // used later to ensure we only show a single class instance
                                // this ensures that if 2 TPs have the same course assigned, we don't get the same class showing twice
                                UniqueID = cls.ClassID + cls.StartDate.Value.ToString().ToString() + cls.EndDate.Value.Ticks.ToString()
                            });
                        }
                        return upcomingClasses;
                    }));
                }
            }

            // ok, given the list of events, exclude any that are outside the range of *all* the possible training programs
            List<Models.UpcomingEvent> excludingNonTPDates = new List<Models.UpcomingEvent>();
            foreach (Models.UpcomingEvent @event in events)
            {
                bool shouldInclude = false;
                foreach (Models.TrainingProgram trainingProgram in trainingPrograms)
                {
                    // don't process the date range overlaps for this course/tp if the training program doesn't include this course to begin with
                    if (trainingProgram.Id != @event.TrainingProgramID)
                    {
                        continue;
                    }
                    // if we're missing dates, ignore it
                    if (trainingProgram.StartDate.HasValue && trainingProgram.EndDate.HasValue && @event.EndDate.HasValue)
                    {
                        if (Utilities.Overlaps(trainingProgram.StartDate.Value, trainingProgram.EndDate.Value, @event.StartDate, @event.EndDate.Value))
                        {
                            shouldInclude = true;
                        }
                    }
                }

                if (shouldInclude)
                {
                    excludingNonTPDates.Add(@event);
                }
            }

            // now we need to exclude any classes for courses the user is already registered for
            List<int> removeUnregisteredClassesForTheseCourses = new List<int>();
            foreach (Models.UpcomingEvent @event in excludingNonTPDates)
            {
                if (@event.RegistrationStatusID > 0)
                {
                    removeUnregisteredClassesForTheseCourses.Add(@event.GroupID);
                }
            }

            foreach (int courseId in removeUnregisteredClassesForTheseCourses)
            {
                excludingNonTPDates.RemoveAll(ue => ue.GroupID == courseId && ue.RegistrationStatusID == 0);
            }

            // finally, make sure we don't have any duplicate classes in here - same class id *and* class date is what matters
            excludingNonTPDates = excludingNonTPDates.DistinctBy(ue => ue.UniqueID).ToList();

            // and give the final result back
            return excludingNonTPDates;
        }

        /// <summary>
        /// Gets all the classes assigned to this user in "upcoming event" format, and automatically removes any that already have the same group and eventid as the "existing"
        /// list to avoid duplicates
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="userId"></param>
        /// <param name="existing"></param>
        /// <returns></returns>
        private List<Models.UpcomingEvent> GetAllClassEventsFiltered(int customerId, int userId, List<Models.UpcomingEvent> existing)
        {
            List<Models.UpcomingEvent> events = new List<Models.UpcomingEvent>();

            string sql = @"
select
		c.ID as EventID,
		course.ID as GroupID,
		c.WebinarKey,
		reg.WebinarRegistrationID,
		isnull(p.[Description],'Class') as EventType,
		case when PublicIndicator = 1 then 4 else 1 end as EventTypeID,
		c.Name as EventName,
		reg.RegistrantID as UserID,
		StartDateTime as StartDate,
        IsAdminRegistration as IsAdminRegistration,
        ApprovalRequired as ApprovalRequired,
		DATEADD(minute, Duration, StartDateTime) as EndDate,
		'' as JoinURL
	from
		Registration reg
	join
		Class c
	on
		reg.ClassID = c.ID
	join
		Course course
	on
		course.ID = c.CourseID
	left join
		PresentationType p
	on
		course.PresentationTypeID = p.ID
	-- we can show multiple items in the upcoming events because
	-- each class is...well, upcoming
	join
		ClassDate cd
	on
		cd.ClassID = c.ID
	-- only show classes for which they're actually registered
	where
		RegistrationStatusID = 4
    and
        RegistrantID = @userId
";

            // set UTC flag

            // hide anything public that you're already registered for

            // TODO: the where clause here needs to work on upcoming events with class type of "registered class", "tp class", etc
            return new CodingHorror().ExecuteTypedList<Models.UpcomingEvent>(sql, userId).Where(ue => !existing.Contains(ue)).ToList();
        }

        /// <summary>
        /// Adds details to meeting and class upcoming events
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="events"></param>
        private IEnumerable<Models.UpcomingEvent> AugmentUpcomingEvents(int userId, IEnumerable<Models.UpcomingEvent> events)
        {
            // we'll need to add some additional details for public classes (so the user can register, etc)
            IEnumerable<Models.UpcomingEvent> publicClasses = events.Where(ue => ue.EventTypeID == (int)EventType.PublicClass);
            IEnumerable<Models.PublicCourse> publicClassesCourses = this.FindPublicCoursesForUser(userId, string.Empty, "ID", OrderDirection.Ascending, 0, int.MaxValue, publicClasses.Select(p => p.GroupID).ToArray()).Data;

            IEnumerable<Models.UpcomingEvent> meetings = events.Where(ue => ue.EventTypeID == (int)EventType.Meeting);
            IEnumerable<Models.Meeting> meetingDetails = this.FindMeetingsForUser(userId, string.Empty, "ID", OrderDirection.Ascending, 0, int.MaxValue, meetings.Select(m => m.EventID).ToArray()).Data;

            IEnumerable<Models.UpcomingEvent> registeredClasseses = events.Where(ue => ue.EventTypeID == (int)EventType.RegisteredClass);
            IEnumerable<int> courseIds = registeredClasseses.Select(rc => rc.GroupID);
            if (courseIds.Count() == 0)
            {
                courseIds = new int[] { -1 };
            }
            IEnumerable<string> columns = Data.Course.Schema.Columns.Select(c => c.QualifiedName).Concat(
                Data.Registration.Schema.Columns.Select(c =>
                    c.ColumnName == Data.Registration.Columns.Id ? Data.Registration.IdColumn.QualifiedName + " as RegistrationID" : c.QualifiedName)
                );
            List<Models.Course> registeredClassesCourses = new Select(columns.ToArray())
                .From<Data.Course>()
                .InnerJoin(Data.ClassX.CourseIDColumn, Data.Course.IdColumn)
                .InnerJoin(Data.Registration.ClassIDColumn, Data.ClassX.IdColumn)
                .Where(Data.Registration.Columns.RegistrantID).IsEqualTo(userId)
                .And(Data.Course.Columns.Id).In(courseIds)
                .ExecuteTypedList<Models.Course>();



            List<Models.UpcomingEvent> removeMe = new List<Models.UpcomingEvent>();
            foreach (Models.UpcomingEvent e in events)
            {
                e.StartDate = e.StartDate.WithUtcFlag();
                if (e.EndDate.HasValue)
                {
                    e.EndDate = e.EndDate.Value.WithUtcFlag();
                }
                switch ((EventType)e.EventTypeID)
                {
                    case EventType.TrainingProgramClass:
                        // we've already added the tp details before we get here
                        break;
                    case EventType.InstructedClass:
                        // no add'l details needed
                        break;
                    case EventType.Meeting:
                        // show the meeting details so they can launch it
                        e.Details = meetingDetails.FirstOrDefault(m => m.Id == e.EventID);
                        break;
                    case EventType.PublicClass:
                        // add the course info
                        e.Details = publicClassesCourses.FirstOrDefault(p => p.Id == e.GroupID);

                        // for public classes, if the event is one that i've registered for in the past, exclude it
                        if (e.EventID != ((Models.PublicCourse)e.Details).ClassID && ((Models.PublicCourse)e.Details).ClassID > 0)
                        {
                            removeMe.Add(e);
                        }
                        break;
                    case EventType.RegisteredClass:
                        // add the course info
                        e.Details = registeredClassesCourses.FirstOrDefault(p => p.Id == e.GroupID);
                        break;
                }
            }

            events = events.Where(e => !removeMe.Contains(e));

            return events;
        }





        #region OneOffTrainingProgramRollupBackfill

        // Set of one time use methods to back fill the new training program rollup table
        // Will iterate over all users in the database, call FindTrainingProgramsFor user for each
        // user, then store the completion status of each training program. 
        //
        // This could take a long time!

        public void BackFillTrainingProgramRollup(ILog logger, List<int> customerIds, List<int> userIds)
        {
            StatusLogger log = new StatusLogger(logger);

            log.WriteLine("Kicked off backfill...");

            log.WriteLine("Loading all users...");
            SqlQuery query = Select.AllColumnsFrom<Data.User>()
                .Where(Data.User.CustomerIDColumn).IsGreaterThan(0);

            if (customerIds != null)
            {
                if (query.HasWhere)
                {
                    query.And(Data.User.CustomerIDColumn).In(customerIds);
                }
                else
                {
                    query.Where(Data.User.CustomerIDColumn).In(customerIds);
                }
            }

            if (userIds != null)
            {
                if (query.HasWhere)
                {
                    query.And(Data.User.IdColumn).In(userIds);
                }
                else
                {
                    query.Where(Data.User.IdColumn).In(userIds);
                }
            }

            List<Models.User> users = query.ExecuteTypedList<Models.User>();

            log.WriteLine("Processing training program data for " + users.Count + " users");

            log.totalusers = users.Count;

            foreach (Models.User user in users)
            {
                log.user = user;
                log.currentUserCount++;
                log.currentTrainingProgramCount = 0;

                log.WriteLine("Loading training programs for user: [" + user.ID + "] " + user.FullName + ")");
                IEnumerable<Models.TrainingProgram> trainingPrograms = FindTrainingProgramsForUserNew(user.ID, user.NewHireIndicator, "", "", OrderDirection.Ascending, 0, int.MaxValue).Data;

                log.WriteLine("User has " + trainingPrograms.Count() + " training programs to process.");
                log.currentTrainingProgramCount = trainingPrograms.Count();

                if (trainingPrograms.Count() == 0)
                {
                    log.trainingProgram = null;
                }

                foreach (Models.TrainingProgram trainingProgram in trainingPrograms)
                {
                    log.currentTrainingProgramCount++;
                    log.trainingProgram = trainingProgram;
                    log.WriteLine("Processing training program: [" + trainingProgram.Id + "] " + trainingProgram.Name);
                    log.WriteLine("Checking for existing rollup...");
                    
                    Data.TrainingProgramRollup existingRollup = Select.AllColumnsFrom<Data.TrainingProgramRollup>()
                        .Where(Data.TrainingProgramRollup.UserIDColumn).IsEqualTo(user.ID)
                        .And(Data.TrainingProgramRollup.TrainingProgramIDColumn).IsEqualTo(trainingProgram.Id)
                        .ExecuteSingle<Data.TrainingProgramRollup>();

                    if (existingRollup == null)
                    {
                        existingRollup = new Data.TrainingProgramRollup();
                    }


                    new RollupController().SaveRollup(user.ID, trainingProgram.Id, existingRollup, trainingProgram, true);
                }

                log.WriteLine("Training program processing complete for user.");
            }

            log.WriteLine("Complete!");
            
        }
        private class StatusLogger {
            public int totalusers = 0;
            public int currentTotalTrainingPrograms = 0;
            public int currentUserCount = 0;
            public int currentTrainingProgramCount = 0;
            public Models.User user = new Models.User();
            public Models.TrainingProgram trainingProgram = new Models.TrainingProgram();
            DateTime startTime;
            TimeSpan estimatedCompletion;
            double lastPercentComplete;

            private ILog log;
            
            public StatusLogger(ILog log)
            {
                this.log = log;
                startTime = DateTime.Now;
            }

            public void WriteLine(string message)
            {
                Console.CursorTop = 0;
                Console.CursorLeft = 0;

                TimeSpan duration = (DateTime.Now - startTime);
                double percentComplete = Math.Floor(((double)currentUserCount / (double)totalusers) * 100);
                double estimatedSecondsUntilCompletion = percentComplete > 0 ? Math.Floor((100 / percentComplete) * duration.TotalSeconds) : 0;
                if (percentComplete != lastPercentComplete)
                {
                    estimatedCompletion = new TimeSpan(0, 0, (int)estimatedSecondsUntilCompletion);
                }

                if (totalusers > 0)
                {
                    WriteFullLine("Total Users: " + totalusers + " Current User: " + currentUserCount + " Percent Complete: " + percentComplete + "%");
                    WriteFullLine("Current Training Programs: " + currentTrainingProgramCount + " Current Training Program Count: " + currentTrainingProgramCount);
                    WriteFullLine("Current Customer ID: [" + user.CustomerID + "]");
                    WriteFullLine("Current User: [" + user.ID + "] " + user.FullName);
                    WriteFullLine("Duration: " + duration.ToString(@"hh\:mm\:ss"));
                    WriteFullLine("Time Until Complete: " + estimatedCompletion.ToString(@"hh\:mm\:ss"));
                    WriteFullLine("Current Training Program: [" + (trainingProgram != null ? trainingProgram.Id : 0) + "] " + (trainingProgram != null ? trainingProgram.Name : "None"));
                }

                log.Info(message);
                lastPercentComplete = percentComplete;
            }
            private void WriteFullLine(string message)
            {
                Console.Write(message);
                for (var i = Console.CursorLeft; i < Console.BufferWidth; i++)
                {
                    Console.Write(" ");
                }
            }
        }
        #endregion

    }
}