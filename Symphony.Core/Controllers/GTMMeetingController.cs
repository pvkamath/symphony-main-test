using System;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using BankersEdge.Data.GTM;
using BankersEdge.Data;

namespace BankersEdge.Collaboration
{
    /// <summary>
    /// Summary description for GTMMeetingController
    /// </summary>
    public partial class GTMMeetingController
    {
        private int userID;
        private GTMController gtm;
        private GTMOrganizer gtmOrganizer;

        public GTMMeetingController(int userID)
        {
            this.userID = userID;
            this.BeforeSave += new EventHandler<GTMMeetingSaveEventArgs>(GTMMeetingController_BeforeSave);
            this.BeforeDelete += new EventHandler<GTMMeetingDeleteEventArgs>(GTMMeetingController_BeforeDelete);
        }

        public GTMMeetingController() : this(new Controller().GetUserID()) { }

        public void ValidateOrganizer()
        {
            if (!UserIsGTMOrganizer(userID))
                throw new Exception("Currently logged-in user is not an organizer.");

            this.gtmOrganizer = new GTMOrganizer(GTMOrganizer.Columns.UserID, userID);
            this.gtm = new GTMController(gtmOrganizer.Email, gtmOrganizer.Password);
        }

        void GTMMeetingController_BeforeDelete(object sender, GTMMeetingDeleteEventArgs e)
        {
            try
            {
                this.ValidateOrganizer();

                this.DeleteMeeting(e.GTMMeeting);

                // send delete notifications
                GTMInvitationController gtmInvitationController = new GTMInvitationController(this.gtmOrganizer.UserID);
                GTMInvitationCollection invitations = new GTMInvitationCollection().Where(GTMInvitation.Columns.GTMMeetingID, e.GTMMeeting.Id).Load();
                foreach (GTMInvitation invitation in invitations)
                    gtmInvitationController.Delete(invitation.Id);
            }
            catch
            {
                throw;
            }
        }

        void GTMMeetingController_BeforeSave(object sender, GTMMeetingSaveEventArgs e)
        {
            try
            {
                this.ValidateOrganizer();

                e.GTMMeeting.ApplyDefaults();

                if (e.GTMMeeting.MeetingType.ToLower() == GTMController.MeetingTypeScheduled.ToLower()
                        && e.GTMMeeting.EndDateTime != null && e.GTMMeeting.StartDateTime != null
                        && e.GTMMeeting.StartDateTime >= e.GTMMeeting.EndDateTime)
                    throw new Exception("The end date/time must be later than the start date/time.");

                e.GTMMeeting.GTMOrganizerID = this.gtmOrganizer.Id;

                if (e.GTMMeeting.MeetingType.ToLower() == GTMController.MeetingTypeScheduled.ToLower())
                {
                    if (e.GTMMeeting.Id == 0)
                        this.CreateScheduledMeeting(e.GTMMeeting);
                    else
                        this.UpdateScheduledMeeting(e.GTMMeeting);
                }
                else if (e.GTMMeeting.MeetingType.ToLower() == GTMController.MeetingTypeImmediate.ToLower())
                {
                    if (e.GTMMeeting.Id == 0)
                        this.CreateImmediateMeeting(e.GTMMeeting);
                    else
                        this.UpdateImmediateMeeting(e.GTMMeeting);
                }
                else
                    throw new Exception("Unsupported meeting type.");

                // send update notifications
                GTMInvitationController gtmInvitationController = new GTMInvitationController(this.gtmOrganizer.UserID);
                GTMInvitationCollection invitations = new GTMInvitationCollection().Where(GTMInvitation.Columns.GTMMeetingID, e.GTMMeeting.Id).Load();
                foreach (GTMInvitation invitation in invitations)
                    gtmInvitationController.SendUpdateMeeting(invitation, e.GTMMeeting);
            }
            catch
            {
                throw;
            }
        }

        private void CreateScheduledMeeting(GTMMeeting gtmMeeting)
        {
            try
            {
                MeetingParameters meetingParameters = this.BuildMeetingParameters(GTMController.MeetingTypeScheduled, gtmMeeting);
                MeetingInfo meetingInfo = this.gtm.CreateMeeting(meetingParameters);
                gtmMeeting.AddExtraData(meetingInfo);
            }
            catch
            {
                throw;
            }
        }

        private void CreateImmediateMeeting(GTMMeeting gtmMeeting)
        {
            try
            {
                MeetingParameters meetingParameters = this.BuildMeetingParameters(GTMController.MeetingTypeImmediate, gtmMeeting);
                MeetingInfo meetingInfo = this.gtm.CreateMeeting(meetingParameters);
                gtmMeeting.AddExtraData(meetingInfo);
            }
            catch
            {
                throw;
            }
        }

        private void UpdateScheduledMeeting(GTMMeeting gtmMeeting)
        {
            try
            {
                if (gtmMeeting.Id == 0)
                    throw new Exception("Invalid meeting specified.");

                MeetingParameters meetingParameters = this.BuildMeetingParameters(GTMController.MeetingTypeScheduled, gtmMeeting);
                this.gtm.UpdateMeeting(gtmMeeting.MeetingID, gtmMeeting.UniqueMeetingID, meetingParameters);
            }
            catch
            {
                throw;
            }
        }

        private void UpdateImmediateMeeting(GTMMeeting gtmMeeting)
        {
            try
            {
                if (gtmMeeting.Id == 0)
                    throw new Exception("Invalid meeting specified.");

                MeetingParameters meetingParameters = this.BuildMeetingParameters(GTMController.MeetingTypeImmediate, gtmMeeting);
                this.gtm.UpdateMeeting(gtmMeeting.MeetingID, gtmMeeting.UniqueMeetingID, meetingParameters);
            }
            catch
            {
                throw;
            }
        }

        private void DeleteMeeting(GTMMeeting gtmMeeting)
        {
            try
            {
                if (gtmMeeting.Id == 0)
                    throw new Exception("Invalid meeting specified.");

                this.gtm.DeleteMeeting(gtmMeeting.MeetingID, gtmMeeting.UniqueMeetingID);
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Code.Name.ToLower() != "nosuchmeeting")
                    throw ex;
            }
        }

        public string StartMeeting(int gtmMeetingID)
        {
            try
            {
                this.ValidateOrganizer();

                GTMMeeting gtmMeeting = new GTMMeeting(gtmMeetingID);
                if (gtmMeeting.Id == 0)
                    throw new Exception("Invalid meeting ID specified.");

                return this.gtm.StartMeeting(gtmMeeting.MeetingID, gtmMeeting.UniqueMeetingID);
            }
            catch
            {
                throw;
            }
        }

        public void EndMeeting(int gtmMeetingID)
        {
            try
            {
                this.ValidateOrganizer();

                GTMMeeting gtmMeeting = new GTMMeeting(gtmMeetingID);
                if (gtmMeeting.Id == 0)
                    throw new Exception("Invalid meeting ID specified.");

                this.gtm.EndMeeting(gtmMeeting.MeetingID, gtmMeeting.UniqueMeetingID);
            }
            catch
            {
                throw;
            }
        }

        private MeetingParameters BuildMeetingParameters(string meetingType, GTMMeeting gtmMeeting)
        {
            MeetingParameters meetingParameters = new MeetingParameters();
            meetingParameters.meetingType = meetingType;
            meetingParameters.subject = gtmMeeting.Subject;
            meetingParameters.startTime = gtmMeeting.StartDateTime;
            meetingParameters.endTime = gtmMeeting.EndDateTime;
            meetingParameters.timeZoneKey = gtmMeeting.TimeZoneKey.ToString();
            meetingParameters.conferenceCallInfo = GTMController.ConferenceCallInfoFree; //gtmMeeting.ConferenceCallInfo;
            meetingParameters.passwordRequired = gtmMeeting.PasswordRequired;
            return meetingParameters;
        }
    }
}
