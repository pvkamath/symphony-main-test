﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using log4net;
using System.Text.RegularExpressions;
using System.Collections;
using SubSonic.Utilities;
using System.Reflection;
using SubSonic;
using System.Data;
using System.Configuration;
using Symphony.Core.Data;
using Data = Symphony.Core.Data;
using System.Threading;
using Models = Symphony.Core.Models;
using EntitlementList = Symphony.Core.Models.Salesforce.EntitlementList;
using Entitlement = Symphony.Core.Models.Salesforce.Entitlement;
using System.IO;
using System.Web;
using System.Net;
using DDay.iCal;
using DDay.iCal.Serialization.iCalendar;
using System.Net.Mime;
using Symphony.Core.Extensions;

namespace Symphony.Core.Controllers
{

    /// <summary>
    /// Used to control the sending of notifications to the users.
    /// </summary>
    /// <remarks>
    /// Future enhancements:
    ///  -don't use the Type, but take instances. That way, we can have more than one instance of a specified item referenced. For example,
    ///   when changing statuses, it would be nice to have both the "old" and "new" status available
    ///  -automatically figure out for whom the notification is intended (Done!)
    ///  -record where the message is located (classroom/artisan/training/etc)
    /// </remarks>
    public partial class NotificationController : SymphonyController
    {
        static ILog Log = LogManager.GetLogger(typeof(NotificationController));
        static bool useEmailWhitelist = true;

        static List<string> validEmailDomains = null;

        static int GroupID = -1;
        static object GroupIDLock = new object();

        public static bool IsQAServer = false;

        static NotificationController()
        {
#if DEBUG
            useEmailWhitelist = false;
            IsQAServer = true;
#endif
            string useWhitelist = ConfigurationManager.AppSettings["useEmailWhitelist"];
            string emailWhitelist = ConfigurationManager.AppSettings["emailWhitelist"];
            if (!string.IsNullOrEmpty(useWhitelist))
            {
                // defaults to true, so it has to be explicitly set to false
                bool.TryParse(useWhitelist, out useEmailWhitelist);
            }
            if (!string.IsNullOrEmpty(emailWhitelist))
            {
                // valid domains are anything after the @ sign
                validEmailDomains = emailWhitelist.Split(',').ToList();
            }
            else
            {
                // no valid email addresses if there's no whitelist specified
                validEmailDomains = new List<string>();
            }

            
        }

        private static void SetGroupId()
        {
            lock (GroupIDLock)
            {
                GroupID = new Select(new Aggregate(Data.Notification.Columns.GroupID, AggregateFunction.Max))
                        .From<Data.Notification>()
                        .ExecuteScalar<int>();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messages"></param>
        /// <returns></returns>
        public NotificationCollection QuickSendNotification(int customerId, int userId, List<Models.Message> messages)
        {
            NotificationCollection notifications = new NotificationCollection();
            if (messages.Count > 0)
            {
                // In case messages exceed 2000 in this case
                IEnumerable<int[]> recipientIdGroups = messages.Select(m => m.RecipientID).InGroupsOf(2000);
                List<User> users = new List<User>();

                foreach (int[] recipientIdGroup in recipientIdGroups)
                {
                    users.AddRange(Select
                        .AllColumnsFrom<Data.User>()
                        .Where(Data.User.Columns.Id)
                        .In(recipientIdGroup)
                        .ExecuteTypedList<User>());
                }

                foreach (Models.Message m in messages)
                {
                    Data.User user = new User(m.SenderID);
                    string body = m.Body.Replace("{!name}", user.FirstName + " " + user.LastName)
                                        .Replace("{!email}", user.Email)
                                        .Replace("{!username}", user.Username);

                    _DoSend(customerId, userId, user.Email, null, null, new int[] { m.RecipientID }, m.Subject, body, new Models.NotificationOptions());
                }
                notifications.SaveAll();
            }
            return notifications;
        }

        public NotificationCollection QuickSendNotification(int customerId, int senderId, List<Models.User> users, string subject, string body)
        {
            NotificationCollection notifications = new NotificationCollection();
            Data.User user = new User(senderId);

            _DoSend(customerId, senderId, user.Email, null, null, users.Select(u => u.ID).ToArray(), subject, body, new Models.NotificationOptions());

            return notifications;
        }

        public static Template GetTemplate(string templateCodeName, int customerId, Data.TrainingProgram trainingProgramContext = null)
        {
            return GetTemplateQuery(customerId, templateCodeName, null, trainingProgramContext).OrderAsc(Data.Template.DisplayNameColumn.QualifiedName).ExecuteSingle<Data.Template>();
        }

        private static SqlQuery GetTemplateQuery(int customerId, string templateCodeName = null, string scheduleParameterCodeName = null, Data.TrainingProgram trainingProgramContext = null)
        {
            if (string.IsNullOrWhiteSpace(templateCodeName) && string.IsNullOrWhiteSpace(scheduleParameterCodeName))
            {
                throw new Exception("GetTemplateQuery requires a templateCodeName or a scheduleParameterCodeName");
            }

            SqlQuery templateQuery = Select.AllColumnsFrom<Data.Template>();

            if (!string.IsNullOrWhiteSpace(templateCodeName))
            {
                templateQuery.Where(Data.Template.CodeNameColumn).IsEqualTo(templateCodeName);
            }
            else
            {
                templateQuery.InnerJoin(Data.ScheduleParameter.IdColumn, Data.Template.ScheduleParameterIDColumn)
                    .And(Data.ScheduleParameter.CodeNameColumn).IsEqualTo(scheduleParameterCodeName);
            }

            templateQuery.And(Data.Template.CustomerIDColumn).IsEqualTo(customerId);

            if (trainingProgramContext != null && trainingProgramContext.IsCustomNotificationsEnabled)
            {
                templateQuery.InnerJoin(Data.TrainingProgramTemplate.TemplateIDColumn, Data.Template.IdColumn)
                    .IncludeColumn("1", Data.EnabledTemplate.Columns.IsEnabledByTrainingProgram) // If they are in the TrainingProgramTemplate map, they are enabled
                    .And(Data.TrainingProgramTemplate.TrainingProgramIDColumn).IsEqualTo(
                        trainingProgramContext.ParentTrainingProgramID > 0 ?
                            trainingProgramContext.ParentTrainingProgramID :
                            trainingProgramContext.Id
                    );
            }
            else
            {
                templateQuery.And(Data.Template.EnabledColumn).IsEqualTo(true);
            }

            return templateQuery;
        }

        public static void SendNotification(string templateCodeName, object[] objects)
        {
            Data.TrainingProgram trainingProgramContext = null;

            for (var i = 0; i < objects.Length; i++)
            {
                if (objects[i].GetType() == typeof(Data.TrainingProgram))
                {
                    trainingProgramContext = (Data.TrainingProgram)objects[i];
                }
            }

            Template template = GetTemplate(templateCodeName, (new SymphonyController()).CustomerID, trainingProgramContext);

            if (template == null) return;

            SendNotification(template, new Symphony.Core.Models.NotificationOptions()
            {
                TemplateObjects = objects,
                Sync = false
            });
        }

        /// <summary>
        /// Sends out a batch of notifications with different notification options. This will only re-load the template if the notification options
        /// have changed enough. (Either if there is a training program context and that context has changed from the previous notification, or if
        /// there is a customer id override, and that has changed since the last notification) 
        /// 
        /// By default will bail out as soon as a template cannot be found. Set exitOnFirstNullTemplate to false if the training program context
        /// or the customer id will change in the list of notification options. 
        /// </summary>
        /// <param name="templateCodeName">Code name of the template to send</param>
        /// <param name="options">a list of notification options - will attempt to send one notification per notification option</param>
        /// <param name="exitOnFirstNullTemplate">Default - true - if false, will continue to send notifications after the first null found
        /// (To account for the potential customerid/trainingprogram id change in the options list - if this will not change, leave this as true)</param>
        public static void SendNotification(string templateCodeName, List<Symphony.Core.Models.NotificationOptions> options, bool exitOnFirstNullTemplate = true)
        {
            int customerId = new SymphonyController().CustomerID;
            int lastCustomerId = customerId;
            int lastTrainingProgramId = 0;

            Data.TrainingProgram trainingProgramContext = null;
            Template template = null;

            foreach (Symphony.Core.Models.NotificationOptions option in options)
            {
                for (int i = 0; i < option.TemplateObjects.Length; i++)
                {
                    if (option.TemplateObjects[i].GetType() == typeof(Data.TrainingProgram))
                    {
                        trainingProgramContext = (Data.TrainingProgram)option.TemplateObjects[i];
                    }
                }

                bool customerIdChanged = option.CustomerOverride != null && lastCustomerId != option.CustomerOverride.Id;
                bool trainingProgramChanged = (trainingProgramContext == null && lastTrainingProgramId != 0) ||
                                              (trainingProgramContext != null && lastTrainingProgramId != trainingProgramContext.Id);

                if (customerIdChanged)
                {
                    customerId = option.CustomerOverride.Id;
                }

                // Only lookup a new template if anything has changed from the last set of options
                if (template == null || customerIdChanged || trainingProgramChanged)
                {
                    template = GetTemplate(templateCodeName, customerId, trainingProgramContext);
                }

                if (template == null)
                {
                    if (exitOnFirstNullTemplate)
                    {
                        return;
                    }
                    continue;
                }

                SendNotification(template, option);

                lastCustomerId = customerId;
                lastTrainingProgramId = trainingProgramContext != null ? trainingProgramContext.Id : 0;
                trainingProgramContext = null;
            }
        }

        public static void SendNotification(string templateCodeName, Symphony.Core.Models.NotificationOptions options)
        {
            int customerId = new SymphonyController().CustomerID;
            int tpPrimaryLeaderId = -1;

            if (options.CustomerOverride != null)
            {
                customerId = options.CustomerOverride.Id;
            }

            Data.TrainingProgram trainingProgramContext = null;
            object[] arrCustomer = new object[] { new Data.Customer(customerId) };
            options.TemplateObjects = options.TemplateObjects.Concat(arrCustomer).ToArray();

            for (int i = 0; i < options.TemplateObjects.Length; i++)
            {
                if (options.TemplateObjects[i] == null)
                {
                    // ignore null objects...
                    continue;
                }

                Type objectType = options.TemplateObjects[i].GetType();

                if (objectType == typeof(Data.TrainingProgram))
                {
                    trainingProgramContext = (Data.TrainingProgram)options.TemplateObjects[i];

                    tpPrimaryLeaderId = _GetTrainingProgramPrimaryLeader(trainingProgramContext.Id);
                }
            }

            Template template = GetTemplate(templateCodeName, customerId, trainingProgramContext);

            if (trainingProgramContext != null && trainingProgramContext.ParentTrainingProgramID.HasValue && trainingProgramContext.ParentTrainingProgramID.Value > 0)
            {
                Data.TrainingProgram parentTrainingProgramContext = new Data.TrainingProgram(trainingProgramContext.ParentTrainingProgramID.Value);
                Template parentTemplate = GetTemplate(templateCodeName, parentTrainingProgramContext.CustomerID, parentTrainingProgramContext);

                if (parentTemplate != null && parentTrainingProgramContext.IsCustomNotificationsEnabled)
                {
                    template = parentTemplate;
                }
            }

            if (template == null) return;

            SendNotification(template, options, tpPrimaryLeaderId);
        }

        private static void SendNotification(Template template, Symphony.Core.Models.NotificationOptions options, int tpPrimaryLeaderId = -1)
        {
            // Templates are enabled if the template itself is enabled, it's enabled by the training program, or it's a forced send through the options
            bool isTemplateEnabled = template.Enabled || template.IsEnabledByTrainingProgram || (options.IsForced.HasValue && options.IsForced.Value);

            if (options.IsForced.HasValue && options.IsForced.Value == false)
            {
                Log.WarnFormat("Skipping template '{0}' (Id {1}) because the notification was turned off through the api request.", template.CodeName, template.Id);
                return;
            }
            // bail on non-enabled templates
            if (!isTemplateEnabled)
            {
                Log.WarnFormat("Skipping template '{0}' (Id {1}) because it's not enabled.", template.CodeName, template.Id);
                return;
            }
            if (options.TemplateObjects == null)
            {
                Log.WarnFormat("Skipping template '{0}' (Id {1}) because no objects were defined for the template.", template.CodeName, template.Id);
                return;
            }
            Log.DebugFormat("Processing template '{0}' (Id {1})...", template.CodeName, template.Id);

            // determine the sender; done here so we can pull from the HttpContext if necessary
            int senderId = 0;
            if (tpPrimaryLeaderId > 0)
            {
                senderId = tpPrimaryLeaderId;
            }
            else if (template.IsScheduled)
            {
                senderId = template.OwnerID;
            }
            else
            {
                senderId = template.OwnerID > 0 ? template.OwnerID : new SymphonyController().UserID;
            }

            if (options.Sync)
            {
                ParseAndSendNotification(senderId, template, options);
            }
            else
            {
                HttpContext ctx = HttpContext.Current;

                ThreadPool.QueueUserWorkItem(httpContext =>
                {
                    try
                    {
                        HttpContext.Current = (HttpContext)httpContext;

                        ParseAndSendNotification(senderId, template, options);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                    }
                }, ctx);
            }
        }

        private static void ParseAndSendNotification(int senderId, Template template, Symphony.Core.Models.NotificationOptions options)
        {
            string senderEmail = string.Empty;
            if (senderId > 0)
            {
                senderEmail = new User(senderId).Email;
            }
            
            // parse the subject and body
            string subject = NotificationTemplateController.Parse(template.Subject, options.TemplateObjects);
            string body = NotificationTemplateController.Parse(template.Body, options.TemplateObjects);

            Log.DebugFormat("Locating users for template {0}", template.Id);
            List<int> actualUsers = GetUsersForTemplate(template, options);
            // bail out if we have no users
            if (actualUsers.Count == 0)
            {
                Log.DebugFormat("No users were found for template {0}", template.Id);
                return;
            }

            _DoSend(template.CustomerID, senderId, senderEmail, template, options.Attachment, actualUsers.ToArray(), subject, body, options);
        }

        private static List<int> GetUsersForTemplate(Template template, Symphony.Core.Models.NotificationOptions options)
        {
            // determine who we're sending to; if not set, we use the settings for the template
            List<int> actualUsers = null;
            if (options.Users == null)
            {
                Log.Debug("No users supplied, loading from template.");
                actualUsers = NotificationTemplateController.GetUsersForTemplate(template.Id, options.TemplateObjects);
            }
            else
            {
                Log.Debug("User override supplied, not loading from template.");
                actualUsers = options.Users.ToList();
            }
            if (options.UserModifier != null)
            {
                Log.Debug("User modifier running - Users before filter: " + actualUsers.Count);
                actualUsers = options.UserModifier(actualUsers);
            }

            return actualUsers;
        }

        /// <summary>
        /// Sends a notification from the specified sender id/email. Set templateId to 0 for non-templated messages, and attachment to null if no attachment
        /// </summary>
        private static void _DoSend(int customerId, int senderId, string senderEmail, Template template, byte[] attachment, int[] userIds, string subject, string body, Symphony.Core.Models.NotificationOptions options)
        {
            int templateId = (template == null ? 0 : template.Id);
            bool ccClassroomSupervisors = (template == null ? false : template.CcClassroomSupervisors);
            bool ccReportingSupervisors = (template == null ? false : template.CcReportingSupervisors);

            Dictionary<string, Data.Notification> existingQueuedNotificationDictionary = new Dictionary<string, Notification>();

            /****** Turning off queued notificaitons
             * Avoiding this look up for now
             * Don't want to put any un tested code that could be slow to production
             *
            existingQueuedNotificationDictionary = Select.AllColumnsFrom<Data.Notification>()
                    .Where(Data.Notification.StatusColumn).IsEqualTo((int)NotificationStatus.Queued)
                    .ExecuteTypedList<Data.Notification>()
                    .DistinctBy(n => n.Key)
                    .ToDictionary(n => n.Key);
             */
            /********
             * Make sure Queued is ALWAYS off since not fully tested
             * ******/
            options.IsQueued = false;
            /**** REMOVE ABOVE and UNCOMMENT existingQueuedNotificationDictionary to get queued notifications working again ****/

            try
            {
                Data.Attachment a = new Data.Attachment();
                if (attachment != null)
                {
                    a.Content = attachment;
                    a.Save();
                }

                // don't do anything if we have no targeted users
                if (userIds == null || userIds.Count() == 0)
                {
                    return;
                }

                // get all the details for the users we're about to message
                List<Data.User> users = new List<Data.User>();
                IEnumerable<int[]> userIdGroups = userIds.ToList().InGroupsOf(2000);
                foreach (int[] userIdGroup in userIdGroups)
                {
                    users.AddRange(Select.AllColumnsFrom<Data.User>()
                        .Where(Data.User.Columns.Id).In(userIdGroup)
                        // Allowing sending templates to users in other customers
                        //.And(Data.User.Columns.CustomerID).IsEqualTo(customerId)
                        .ExecuteTypedList<Data.User>());
                }


                List<int> supervisorIds = new List<int>();
                if (ccClassroomSupervisors)
                {
                    supervisorIds = users.Select(u => u.SupervisorID).ToList();
                    supervisorIds = supervisorIds.Union(users.Select(u => u.SecondarySupervisorID)).ToList();
                }
                if (ccReportingSupervisors)
                {
                    supervisorIds = supervisorIds.Union(users.Select(u => u.ReportingSupervisorID)).ToList();
                }
                if (supervisorIds.Count == 0)
                {
                    // ensure we have at least something
                    supervisorIds = new List<int>() { -1 };
                }

                // get all the supervisors of the users we're about to message
                List<Data.User> supervisors = new List<Data.User>();
                IEnumerable<int[]> supervisorIdGroups = supervisorIds.ToList().InGroupsOf(2000);
                foreach (int[] supervisorIdGroup in supervisorIdGroups)
                {
                    supervisors.AddRange(Select.AllColumnsFrom<Data.User>()
                        .Where(Data.User.Columns.Id).In(supervisorIdGroup)
                        // Allowing sending templates to users in other customers
                        //.And(Data.User.Columns.CustomerID).IsEqualTo(customerId)
                        .ExecuteTypedList<Data.User>());
                }

                int groupId = 0;
                lock (GroupIDLock)
                {
                    if (GroupID == -1)
                    {
                        // initialize
                        SetGroupId();
                    }
                    // increment, and assign the new group id
                    GroupID++;
                    groupId = GroupID;
                }

                Data.User sender = new User(senderId);

                NotificationCollection notifications = new NotificationCollection();

                Data.Customer customer = new Customer(customerId);

                foreach (int userId in userIds)
                {
                    // this ensures that no matter what, a message can never go out to a user not in the templates customer
                    // this can occur because we step through the various relations, which does not guarantee that
                    // we only get the items for a given customer id
                    Data.User currentUser = users.FirstOrDefault(u => u.Id == userId);
                    if (currentUser == null || currentUser.Id == 0)
                    {
                        Log.WarnFormat("Skipping sending message for template {2}. The user {0} is not in the customer {1}.", userId, customerId, template.Id);
                        continue;
                    }

                    // do not send notification to inactive users
                    if (currentUser.StatusID != (int)UserStatusType.Active)
                    {
                        continue;
                    }

                    var ccAddresses = new List<string>();
                    if (ccClassroomSupervisors)
                    {
                        var supervisor = supervisors.SingleOrDefault(s => s.Id == currentUser.SupervisorID);
                        var secondarySupervisor = supervisors.SingleOrDefault(s => s.Id == currentUser.SecondarySupervisorID);

                        if (supervisor != null)
                        {
                            ccAddresses.Add(supervisor.Email);
                        }

                        if (secondarySupervisor != null)
                        {
                            ccAddresses.Add(secondarySupervisor.Email);
                        }
                    }
                    if (ccReportingSupervisors)
                    {
                        var reportingSupervisor = supervisors.SingleOrDefault(s => s.Id == currentUser.ReportingSupervisorID);
                        if (reportingSupervisor != null)
                        {
                            ccAddresses.Add(reportingSupervisor.Email);
                        }
                    }

                    // Replace any templated User properties with the corresponding value
                    string parsedSubject = NotificationTemplateController.ParseUserTemplateParameters(subject, currentUser);
                    string parsedBody = NotificationTemplateController.ParseUserTemplateParameters(body, currentUser);

                    Notification n = new Notification();
                    Log.DebugFormat("Parsing template for user {0}.", userId);

                    n.GroupID = groupId;
                    n.RecipientID = userId;
                    n.SenderID = senderId;
                    n.TemplateID = templateId;

                    if (existingQueuedNotificationDictionary.Count() > 0 && existingQueuedNotificationDictionary.ContainsKey(n.Key))
                    {
                        // Set the current notification to the eixsting queued notification
                        // then continue updating it so we always have the most recent queued
                        // notification in the db.
                        n = existingQueuedNotificationDictionary[n.Key];
                    }

                    n.Body = parsedBody;
                    n.Subject = parsedSubject;
                    n.AttachmentID = a.Id; // will nicely default to 0 if there is no attachment
                    n.IsRead = false;
                    n.Priority = 1;
                    n.SendDate = options.SendTime.HasValue && options.SendTime != DateTime.MinValue ? options.SendTime : DateTime.UtcNow;
                    n.Status = options.IsQueued ? (int)NotificationStatus.Queued : (int)NotificationStatus.Sent;

                    string ical = "";

                    // Only send out the email if the notification is not being queued
                    if (!options.IsQueued)
                    {
                        // send out an email
                        if (template != null && template.IncludeCalendarInvite && options.NotificationDates != null && options.NotificationDates.Length > 0)
                        {
                            // send calendar invites
                            foreach (var notificationDate in options.NotificationDates)
                            {
                                var invite = notificationDate.BuildVCalFile(sender, currentUser, n.Subject, n.Body);
                                SendMessageSMTP(customer, n.Body, n.Subject, senderEmail, currentUser.Email, "normal", ccAddresses.ToArray(), invite, null);

                                iCalendarSerializer serializer = new iCalendarSerializer();
                                String icalData = serializer.SerializeToString(invite);

                                ical += ical.Length > 0 ? "|" : icalData;
                            }
                        }
                        else
                        {
                            SendMessageSMTP(customer, n.Body, n.Subject, senderEmail, currentUser.Email, "normal", ccAddresses.ToArray(), null, null);
                        }
                    }

                    n.ICal = ical;
                    n.CCEmails = String.Join(",", ccAddresses);

                    Log.DebugFormat("Created notification, adding to save list.");
                    notifications.Add(n);
                }

                Log.DebugFormat("Saving notifications.");
                notifications.SaveAll();
                Log.DebugFormat("All notifications saved successfully.");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        private static void SendScheduledNewHireNotification(Template template, DateTime? sendDate)
        {
            UserCollection list = new UserCollection()
                .Where(Data.User.Columns.CustomerID, template.CustomerID)
                .Where(Data.User.Columns.NewHireIndicator, true);
            list.Load();

            if (list.Count == 0)
            {
                Log.ErrorFormat("Failed to retrieve any result set for the template " + template.Id);
                return;
            }

            Data.Customer customer = new Customer(template.CustomerID);

            // loop through the collection and determine if any items meets the schedule requirements but
            // hasn't been sent yet. if so, send it out.
            ScheduleParameter sp = new ScheduleParameter(template.ScheduleParameterID);
            foreach (Data.User u in list)
            {
                try
                {
                    // strip out the hours minutes and seconds, set utc flag
                    if (!u.HireDate.HasValue)
                    {
                        continue;
                    }
                    DateTime hireDate = u.HireDate.Value;
                    DateTime expirationDate = new DateTime(hireDate.Ticks)
                        .AddHours(-hireDate.Hour).AddMinutes(-hireDate.Minute).AddSeconds(-hireDate.Second)
                        .AddDays(customer.NewHireDuration)
                        .WithUtcFlag();
                    if (expirationDate.Year == 1900)
                    {
                        continue;
                    }

                    // ok, now the time is midnight UTC; if the company has a timezone, convert to that
                    if (!string.IsNullOrEmpty(u.Customer.TimeZone))
                    {
                        TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(customer.TimeZone);
                        TimeSpan offset = tzi.GetUtcOffset(expirationDate);
                        expirationDate = expirationDate.Subtract(offset);
                    }

                    SendScheduledNotification(template, u.Id.ToString(), expirationDate, (users) =>
                    {
                        return users;
                    }, new object[] { u }, sendDate);
                }
                catch (Exception ex)
                {
                    Log.Error("Error while looping through users in SendScheduledNewHireNotification", ex);
                }
            }
        }

        private static void SendScheduledClassNotification(Template template, DateTime? sendDate = null)
        {
            //TODO: at some point, we could technically join this back to the history table to avoid returning extra data,
            // rather than filtering in the loop below...
            ClassXCollection list = new ClassXCollection()
                .Where(Data.ClassX.Columns.CustomerID, template.CustomerID);
            list.Load();

            if (list.Count == 0)
            {
                Log.ErrorFormat("Failed to retrieve any result set for the template " + template.Id);
                return;
            }

            // loop through the collection and determine if any items meets the schedule requirements but
            // hasn't been sent yet. if so, send it out.
            ScheduleParameter sp = new ScheduleParameter(template.ScheduleParameterID);
            foreach (Data.ClassX c in list)
            {
                try
                {
                    IOrderedEnumerable<ClassDate> dates = c.ClassDates.OrderBy(cd => cd.StartDateTime);
                    ClassDate thisDate = dates.FirstOrDefault();
                    if (thisDate == null)
                    {
                        Log.WarnFormat("No dates found for the class with id {0}, skipping...", c.Id);
                        continue;
                    }
                    DateTime dt = thisDate.StartDateTime.WithUtcFlag();
                    if (sp.CodeName == ScheduleParameterType.ClassEndDate)
                    {
                        dt = dates.Last().StartDateTime.WithUtcFlag();
                    }

                    SendScheduledNotification(template, c.Id.ToString(), dt, (users) =>
                    {
                        List<int> filtered = users;
                        if (template.FilterComplete)
                        {
                            // exclude users show have completed the class
                            filtered = new List<int>();
                            foreach (int user in users)
                            {
                                List<Models.TranscriptEntry> entries = SPs.GetTranscript(user).ExecuteTypedList<Models.TranscriptEntry>();
                                // if the user has not passed the class, they get the notification...should this be course maybe?
                                if (entries.FindAll(e => e.ClassID == c.Id && e.Passed == 1).Count == 0)
                                {
                                    filtered.Add(user);
                                }
                            }

                        }

                        return filtered;
                    }, new object[] { c }, sendDate);
                }
                catch (Exception ex)
                {
                    Log.Error("Error while looping through classes in SendScheduledClassNotification", ex);
                }
            }
        }

        private static void SendScheduledTrainingProgramSessionNotification(Template template, DateTime? sendDate)
        {

            ScheduleParameter sp = new ScheduleParameter(template.ScheduleParameterID);

            DateTime referenceDate = sendDate ?? DateTime.UtcNow;
            referenceDate = referenceDate.AddMinutes(-template.RelativeScheduledMinutes).WithUtcFlag();

            DateTime maxDate = referenceDate.AddDays(1);
            DateTime minDate = referenceDate.AddDays(-1);

            List<string> columns = Data.TrainingProgram.Schema.Columns.Select(c => c.QualifiedName).ToList();
            
            bool isUseStartDate = sp.CodeName == ScheduleParameterType.TrainingProgramSessionStart;

            List<Models.TrainingProgramSession> trainingPrograms = Data.SPs.GetParentTrainingProgramsWithSessions(template.CustomerID, minDate, maxDate, isUseStartDate)
                .ExecuteTypedList<Models.TrainingProgramSession>();

            Dictionary<int, int> trainingProgramIdsForTemplate = GetTrainingProgramIdsForTemplate(template);
            Customer customer = new Customer(template.CustomerID);

            foreach (Models.TrainingProgramSession tpModel in trainingPrograms)
            {
                TrainingProgramSession tp = new TrainingProgramSession();
                tpModel.CopyTo(tp);

                int trainingProgramId = tp.ParentTrainingProgramID.HasValue && tp.ParentTrainingProgramID.Value > 0 ? tp.ParentTrainingProgramID.Value : tp.Id;

                if (tp.IsCustomNotificationsEnabled && !trainingProgramIdsForTemplate.ContainsKey(trainingProgramId))
                {
                    Log.DebugFormat("Skipping program {0} because custom training programs are enabled and the template {1} is not enabled for this training program.", tp.Id, template.Id);
                    continue;
                }
                else if (!tp.IsCustomNotificationsEnabled && !template.Enabled)
                {
                    Log.DebugFormat("Skipping program {0} because custom training program templates are not enabled and the current template {1} is not enabled.", tp.Id, template.Id);
                    continue;
                }

                DateTime dt = tp.SessionStartDate;
                if (sp.CodeName == ScheduleParameterType.TrainingProgramSessionStart)
                {
                    dt = tp.SessionStartDate;
                }
                else if (sp.CodeName == ScheduleParameterType.TrainingProgramSessionEnd)
                {
                    dt = tp.SessionEndDate;
                }
                dt = dt.WithUtcFlag();
                
                // These notifications should go out once per session class training program combination
                // Reasoning:
                //  It is possible to assign the same session course to multiple training programs. Each user in the session could
                //  be assigned to 1 or more of those training programs.
                //
                //  For each training program we get the users for that training program, and filter them by the users
                //  in the associated session. So for each session/trainingprogram pairing we should only send the notification
                //  once. This will also allow us to send to duplicate training programs assigned through salesforce where
                //  the session is reused across all duplicates. 
                SendScheduledNotification(template, tp.Id.ToString() + "-" + tp.SessionClassID.ToString(), dt, (users) =>
                {
                    Dictionary<int, int> userIdsInSession = Data.SPs.GetUsersForTrainingProgramSession(tp.Id, tp.SessionClassID, true)
                        .ExecuteTypedList<int>()
                        .ToDictionary(i => i);

                    List<int> filtered = users.Where(u => userIdsInSession.ContainsKey(u)).ToList();
                    
                    if (users.Count == 0)
                    {
                        users = new List<int>() { -1 };
                    }

                    // always remove new hires from receiving messages about non-new-hire TPs
                    List<Models.User> dataUsers = new List<Models.User>();
                    IEnumerable<int[]> userIdGroups = users.ToList().InGroupsOf(2000);
                    foreach (int[] userIdGroup in userIdGroups)
                    {
                        SqlQuery excludeQuery = Select.AllColumnsFrom<Data.User>()
                            .Where(Data.User.Columns.Id).In(userIdGroup);

                        // If custom notifications enabled on the parent tp
                        // then allow cross customer notifications
                        if (tp.IsCustomNotificationsEnabled)
                        {
                            excludeQuery.And(Data.User.NewHireIndicatorColumn).IsEqualTo(true);
                        }
                        else
                        {
                            // If it's not enabled, ensure the user is in the same customer as the tmplate
                            excludeQuery.AndExpression(Data.User.NewHireIndicatorColumn.QualifiedName).IsEqualTo(true)
                                .Or(Data.User.CustomerIDColumn).IsNotEqualTo(template.CustomerID)
                                .CloseExpression();
                        }

                        dataUsers.AddRange(excludeQuery.ExecuteTypedList<Models.User>());
                    }

                    foreach (Models.User user in dataUsers)
                    {
                        filtered.Remove(user.ID);
                    }

                    // Remove non-new hire users where the new hire end date is greater than tp.EndDate - transitionperiod
                    int transitionPeriod = tp.NewHireTransitionPeriod ?? customer.NewHireTransitionPeriod ?? 0;

                    Log.Debug("Filtering users in transition period. Users prior to filter: " + filtered.Count);
                    if (filtered.Count > 0)
                    {
                        List<int> filteredUserIds = new List<int>();
                        IEnumerable<int[]> filteredIdGroups = filtered.ToList().InGroupsOf(2000);
                        foreach (int[] filteredIdGroup in filteredIdGroups)
                        {
                            filteredUserIds.AddRange(Select.AllColumnsFrom<Data.User>()
                            .Where(Data.User.Columns.NewHireIndicator).IsEqualTo(false)
                            .And(Data.User.Columns.Id).In(filteredIdGroup)
                            .AndExpression(Data.User.Columns.NewHireEndDate).IsNull()
                            .Or(Data.User.Columns.NewHireEndDate).IsLessThanOrEqualTo(tp.EndDate.AddDays(-transitionPeriod))
                            .CloseExpression()
                            .ExecuteTypedList<Models.User>()
                            .Select(u => u.ID)
                            .ToList());
                        }

                        filtered = filteredUserIds;
                    }
                    Log.Debug("Users remaining after transition period filter: " + filtered.Count);

                    return filtered;
                }, new object[] { tp }, sendDate);
            }
        }

        private static void SendScheduledTrainingProgramNotification(Template template, DateTime? sendDate)
        {

            TrainingProgramCollection regularTrainingPrograms = new TrainingProgramCollection()
                .Where(Data.TrainingProgram.Columns.CustomerID, template.CustomerID)
                .Where(Data.TrainingProgram.Columns.IsLive, true)
                .Where(Data.TrainingProgram.Columns.DisableScheduled, false)
                .Where(Data.TrainingProgram.Columns.IsNewHire, false);
            regularTrainingPrograms.Load();

            if (regularTrainingPrograms.Count == 0)
            {
                Log.ErrorFormat("Failed to retrieve any result set for the template " + template.Id);
                return;
            }

            // grab some items we'll need
            Customer customer = new Customer(template.CustomerID);
            PortalController pc = new PortalController();
            ScheduleParameter sp = new ScheduleParameter(template.ScheduleParameterID);

            Dictionary<int, int> trainingProgramIdsForTemplate = GetTrainingProgramIdsForTemplate(template);

            // loop through the collection and determine if any items meets the schedule requirements but
            // hasn't been sent yet. if so, send it out.
            foreach (Data.TrainingProgram tp in regularTrainingPrograms)
            {
                try
                {
                    int trainingProgramId = tp.ParentTrainingProgramID.HasValue && tp.ParentTrainingProgramID.Value > 0 ? tp.ParentTrainingProgramID.Value : tp.Id;

                    if (tp.IsCustomNotificationsEnabled && !trainingProgramIdsForTemplate.ContainsKey(trainingProgramId))
                    {
                        Log.DebugFormat("Skipping program {0} because custom training programs are enabled and the template {1} is not enabled for this training program.", tp.Id, template.Id);
                        continue;
                    }
                    else if (!tp.IsCustomNotificationsEnabled && !template.Enabled)
                    {
                        Log.DebugFormat("Skipping program {0} because custom training program templates are not enabled and the current template {1} is not enabled.", tp.Id, template.Id);
                        continue;
                    }

                    DateTime dt = tp.StartDate;
                    if (sp.CodeName == ScheduleParameterType.TrainingProgramEndDate)
                    {
                        dt = tp.EndDateWithoutTime;
                    }
                    else if (sp.CodeName == ScheduleParameterType.TrainingProgramDueDate)
                    {
                        dt = tp.DueDate;
                    }
                    else if (sp.CodeName == ScheduleParameterType.TrainingProgramStartDate)
                    {
                        dt = tp.StartDateWithoutTime;
                    }
                    dt = dt.WithUtcFlag();

                    SendScheduledNotification(template, tp.Id.ToString(), dt, (users) =>
                    {
                        List<int> filtered = users;
                        if (template.FilterComplete)
                        {
                            // exclude users that have completed the class
                            filtered = new List<int>();
                            foreach (int user in users)
                            {
                                // if the user has not completed the training program, add them to the final list
                                if (!pc.GetTrainingProgramDetails(user, tp.Id, false).Data.Completed)
                                {
                                    filtered.Add(user);
                                }
                            }
                        }

                        if (users.Count == 0)
                        {
                            users = new List<int>() { -1 };
                        }

                        // always remove new hires from receiving messages about non-new-hire TPs
                        List<Models.User> dataUsers = new List<Models.User>();
                        IEnumerable<int[]> userIdGroups = users.ToList().InGroupsOf(2000);
                        foreach (int[] userIdGroup in userIdGroups)
                        {
                            dataUsers.AddRange(Select.AllColumnsFrom<Data.User>()
                                .Where(Data.User.Columns.Id).In(userIdGroup)
                                .And(Data.User.Columns.NewHireIndicator).IsEqualTo(true)
                                .ExecuteTypedList<Models.User>());
                        }

                        foreach (Models.User user in dataUsers)
                        {
                            filtered.Remove(user.ID);
                        }

                        // Remove non-new hire users where the new hire end date is greater than tp.EndDate - transitionperiod
                        int transitionPeriod = tp.NewHireTransitionPeriod ?? customer.NewHireTransitionPeriod ?? 0;

                        Log.Debug("Filtering users in transition period. Users prior to filter: " + filtered.Count);
                        if (filtered.Count > 0)
                        {
                            List<int> filteredUserIds = new List<int>();
                            IEnumerable<int[]> filteredIdGroups = filtered.ToList().InGroupsOf(2000);
                            foreach (int[] filteredIdGroup in filteredIdGroups)
                            {
                                filteredUserIds.AddRange(Select.AllColumnsFrom<Data.User>()
                                .Where(Data.User.Columns.NewHireIndicator).IsEqualTo(false)
                                .And(Data.User.Columns.Id).In(filteredIdGroup)
                                .AndExpression(Data.User.Columns.NewHireEndDate).IsNull()
                                    .Or(Data.User.Columns.NewHireEndDate).IsLessThanOrEqualTo(tp.EndDate.AddDays(-transitionPeriod))
                                .CloseExpression()
                                .ExecuteTypedList<Models.User>()
                                .Select(u => u.ID)
                                .ToList());
                            }

                            filtered = filteredUserIds;
                        }
                        Log.Debug("Users remaining after transition period filter: " + filtered.Count);

                        return filtered;
                    }, new object[] { tp }, sendDate);
                }
                catch (Exception ex)
                {
                    Log.Error("Error while looping through Training Programs in SendScheduledTrainingProgramNotification", ex);
                }
            }


            // we have to handle new hire messages separately, because they have individual dates
            // for every user
            TrainingProgramCollection newHireTrainingPrograms = new TrainingProgramCollection()
                .Where(Data.TrainingProgram.Columns.CustomerID, template.CustomerID)
                .Where(Data.TrainingProgram.Columns.IsNewHire, true);
            newHireTrainingPrograms.Load();

            foreach (Data.TrainingProgram tp in newHireTrainingPrograms)
            {
                List<int> ids = tp.GetAssignedUsers();
                if (ids.Count == 0)
                {
                    continue;
                }
                //List<User> users = Select.AllColumnsFrom<Data.User>().Where(Data.User.Columns.Id).In(ids).ExecuteTypedList<User>();

                //foreach (User user in users)
                //{
                //    // determine if the given user's hire date matches the specified TP date
                //    if (false) // && user.HireDate.AddMinutes(template.RelativeScheduledMinutes) > DateTime.Now)
                //    {
                //        DateTime dtStart = user.HireDate.WithUtcFlag();
                //        DateTime dtEnd = user.HireDate.AddDays(customer.NewHireDuration).WithUtcFlag();
                //        DateTime dtReference = dtStart;
                //        if (sp.CodeName == ScheduleParameterType.TrainingProgramEndDate || sp.CodeName == ScheduleParameterType.TrainingProgramDueDate)
                //        {
                //            dtReference = dtEnd;
                //        }

                //        if (dtReference < template.CreatedOn)
                //        {
                //            Log.DebugFormat("Skipping new hire template {0} for user {1} because the template was created after the scheduled date.", template.Id, user.Id);
                //            return;
                //        }

                //        // if the time has passed, send it out
                //        if (dtReference < DateTime.Now.WithUtcFlag())
                //        {
                //            // if it's valid, set the end date according to each user's hire date and send a notification
                //            tp.StartDate = dtStart;
                //            tp.DueDate = dtEnd;
                //            tp.EndDate = dtEnd;

                //            if (!template.FilterComplete || (template.FilterComplete && !pc.GetTrainingProgramDetails(user.Id, tp.Id).Data.Completed))
                //            {
                //                //SendScheduledNotification(template, new List<int>() { user.Id }, true, new object[]{ tp });
                //            }
                //        }
                //    }
                //}

            }
        }

        public static void SendScheduledTrainingProgramCourseNotification(Template template, DateTime? sendDate)
        {
            //TODO: at some point, we could technically join this back to the history table to avoid returning extra data,
            // rather than filtering in the loop belowTrainingProgramCollection

            //TODO: join to the online course to TP link table to get the dates
            TrainingProgramToCourseLinkCollection list = new TrainingProgramToCourseLinkCollection()
                .Where(Data.TrainingProgramToCourseLink.Columns.CustomerID, template.CustomerID);

            Customer customer = new Customer(template.CustomerID);
            list.Load();

            if (list.Count == 0)
            {
                Log.ErrorFormat("Failed to retrieve any result set for the template " + template.Id);
                return;
            }

            // This may be overkill, but just in case single customer ends up with > 2000 training programs.
            List<Data.TrainingProgram> programs = new List<TrainingProgram>();

            IEnumerable<int[]> trainingProgramIdGroups = list.Select(l => l.TrainingProgramID).Distinct().InGroupsOf(2000);
            foreach (int[] trainingProgramIdGroup in trainingProgramIdGroups)
            {
                programs.AddRange(Select.AllColumnsFrom<Data.TrainingProgram>()
                    .Where(Data.TrainingProgram.Columns.IsLive).IsEqualTo(true)
                    .And(Data.TrainingProgram.Columns.Id).In(trainingProgramIdGroup)
                    .ExecuteTypedList<Data.TrainingProgram>());
            }

            Dictionary<int, int> trainingProgramIdsForTemplate = GetTrainingProgramIdsForTemplate(template);

            // loop through the collection and determine if any items meets the schedule requirements but
            // hasn't been sent yet. if so, send it out.
            ScheduleParameter sp = new ScheduleParameter(template.ScheduleParameterID);
            foreach (Data.TrainingProgramToCourseLink link in list)
            {
                try
                {
                    Data.TrainingProgram tp = programs.FirstOrDefault(p => p.Id == link.TrainingProgramID);
                    if (tp == null || !tp.IsLive || tp.DisableScheduled)
                    {
                        Log.DebugFormat("Skipping program {0} because it's not found or not enabled.", link.TrainingProgramID);
                        continue;
                    }
                    
                    if (!link.DueDate.HasValue || (link.DueDate.HasValue && link.DueDate.Value.Year < 2000))
                    {
                        continue;
                    }

                    // we never send notifications about course due dates to new hires, because they don't have individual due dates
                    // all courses must be due at the end of the TP, since it's based on their hire date
                    if (link.TrainingProgram != null && link.TrainingProgram.IsNewHire)
                    {
                        continue;
                    }

                    int trainingProgramId = tp.ParentTrainingProgramID.HasValue && tp.ParentTrainingProgramID.Value > 0 ? tp.ParentTrainingProgramID.Value : tp.Id;

                    if (tp.IsCustomNotificationsEnabled && !trainingProgramIdsForTemplate.ContainsKey(trainingProgramId))
                    {
                        Log.DebugFormat("Skipping program {0} because custom training program templates are enabled and the template {1} is not enabled for this training program.", link.TrainingProgramID, template.Id);
                        continue;
                    }
                    else if (!tp.IsCustomNotificationsEnabled && !template.Enabled)
                    {
                        Log.DebugFormat("Skipping program {0} becuase custom training program templates are not enabled and the current template {1} is not enabled.", link.TrainingProgramID, template.Id);
                        continue;
                    }

                    // individual course due dates are saved with just a "date" portion;
                    // in reality, we want to use the *customers* timezone as the time portion of a UTC date, so
                    // if they pick 6/12/2011 as the due date, we want to use 6/12/2011 00:00:00 + (utc offset according to the customer timezone) as the time
                    // so we simply need to add the offset according to the specified timezone.
                    TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(customer.TimeZone);
                    TimeSpan offset = tzi.GetUtcOffset(link.DueDate.Value);
                    // so, this becomes, if we assume an offset of 4 hours (for EST during daylight savings):
                    // 6/12/2011 00:00:00 (in the database, retrieved as local time)
                    // 6/12/2011 00:00:00 UTC (because of the WithUtcFlag() call)
                    // 6/12/2011 04:00:00 UTC (because we subtract a negative offset)
                    DateTime dt = link.DueDate.Value.WithUtcFlag().Subtract(offset);

                    Log.DebugFormat("Sending message for training program to course link " + link.Id);

                    SendScheduledNotification(template, link.TrainingProgramID + "-" + link.CourseID + "-" + link.CourseTypeID, dt, (users) =>
                    {
                        List<int> filtered = users;
                        if (template.FilterComplete)
                        {
                            // users who have completed the courses do not get the notification
                            // filtered is the list of users who have *not* completed the course
                            filtered = new List<int>();
                            foreach (int user in users)
                            {
                                List<Models.TranscriptEntry> entries = SPs.GetTranscript(user).ExecuteTypedList<Models.TranscriptEntry>();
                                List<Models.TranscriptEntry> completed = entries.FindAll(e => e.CourseID == link.CourseID && e.CourseTypeID == link.CourseTypeID && e.Passed == 1);

                                if (completed.Count == 0)
                                {
                                    // ok, they've never completed it; always add them to the list
                                    filtered.Add(user);
                                }
                                else
                                {
                                    // they've completed this course at least once; however, we need to see if they completed it within the range of this TP
                                    bool completedForThisTP = false;
                                    foreach (Models.TranscriptEntry entry in completed)
                                    {
                                        if (CourseController.OnlineCourseCountsForTP(tp, entry))
                                        {
                                            completedForThisTP = true;
                                            break;
                                        }
                                    }
                                    if (!completedForThisTP)
                                    {
                                        filtered.Add(user);
                                    }
                                }
                            }
                        }

                        if (link.TrainingProgram != null && link.TrainingProgram.IsNewHire && filtered.Count > 0)
                        {
                            // from the list of users (filtered or not)
                            // select the new hires if the TP is a new hire TP
                            List<int> filteredUserIds = new List<int>();
                            IEnumerable<int[]> filteredUserIdGroups = filtered.InGroupsOf(2000);

                            foreach (int[] filteredUserIdGroup in filteredUserIdGroups)
                            {
                                filteredUserIds.AddRange(Select.AllColumnsFrom<Data.User>()
                                    .Where(Data.User.IdColumn).In(filteredUserIdGroup)
                                    .And(Data.User.NewHireIndicatorColumn).IsEqualTo(true)
                                    .ExecuteTypedList<Data.User>()
                                    .Select(u => u.Id)
                                    .ToList());
                            }

                            filtered = filteredUserIds;
                        }
                        else
                        {
                            // Remove non-new hire users where the new hire end date is greater than tp.EndDate - transitionperiod
                            int transitionPeriod = tp.NewHireTransitionPeriod ?? customer.NewHireTransitionPeriod ?? 0;
                            if (filtered.Count > 0)
                            {
                                Log.Debug("Filtering users in transition period. Users prior to filter: " + filtered.Count);

                                List<int> filteredUserIds = new List<int>();
                                IEnumerable<int[]> filteredUserIdGroups = filtered.InGroupsOf(2000);

                                foreach (int[] filteredUserIdGroup in filteredUserIdGroups)
                                {
                                    filteredUserIds.AddRange(Select.AllColumnsFrom<Data.User>()
                                        .Where(Data.User.Columns.NewHireIndicator).IsEqualTo(false)
                                        .And(Data.User.Columns.Id).In(filteredUserIdGroup)
                                        .AndExpression(Data.User.Columns.NewHireEndDate).IsNull()
                                            .Or(Data.User.Columns.NewHireEndDate).IsLessThanOrEqualTo(tp.EndDate.AddDays(-transitionPeriod))
                                        .CloseExpression()
                                        .ExecuteTypedList<Models.User>()
                                        .Select(u => u.ID)
                                        .ToList());
                                }

                                filtered = filteredUserIds;

                                Log.Debug("Users remaining after transition period filter: " + filtered.Count);
                            }
                        }

                        return filtered;

                    }, new object[] { link }, sendDate);
                }
                catch (Exception ex)
                {
                    Log.Error("Error while looping through TP Courses in SendScheduledTrainingProgramCourseNotification", ex);
                }
            }
        }

        public static void SendScheduledTrainingProgramAfterCompletionNotification(Template template, DateTime? sendDate)
        {
            DateTime today = sendDate.HasValue ? sendDate.Value : DateTime.UtcNow;
            DateTime maxCompletionDate = today.AddMinutes(template.RelativeScheduledMinutes * -1);
            DateTime minCompletionDate = maxCompletionDate.AddDays(-1);

            List<TrainingProgramRollup> rollups = Select.AllColumnsFrom<Data.TrainingProgramRollup>()
                .InnerJoin(Data.User.IdColumn, Data.TrainingProgramRollup.UserIDColumn)
                .Where(Data.TrainingProgramRollup.IsCompleteColumn).IsEqualTo(true)
                .And(Data.TrainingProgramRollup.DateCompletedColumn).IsGreaterThan(minCompletionDate)
                .And(Data.TrainingProgramRollup.DateCompletedColumn).IsLessThan(maxCompletionDate)
                .And(Data.User.CustomerIDColumn).IsEqualTo(template.CustomerID)
                .ExecuteTypedList<Data.TrainingProgramRollup>();

            Dictionary<int, int> trainingProgramIdsForTemplate = GetTrainingProgramIdsForTemplate(template);

            foreach (TrainingProgramRollup rollup in rollups) {

                int trainingProgramId = rollup.TrainingProgram.ParentTrainingProgramID.HasValue && rollup.TrainingProgram.ParentTrainingProgramID.Value > 0 ? rollup.TrainingProgram.ParentTrainingProgramID.Value : rollup.TrainingProgram.Id;
                if (rollup.TrainingProgram.IsCustomNotificationsEnabled && !trainingProgramIdsForTemplate.ContainsKey(trainingProgramId))
                {
                    Log.DebugFormat("Skipping program {0} because custom training programs are enabled and the template {1} is not enabled for this training program.", rollup.TrainingProgramID, template.Id);
                    continue;
                }
                else if (!rollup.TrainingProgram.IsCustomNotificationsEnabled && !template.Enabled)
                {
                    Log.DebugFormat("Skipping program {0} becuase custom training program templates are not enabled and the current template {1} is not enabled.", rollup.TrainingProgramID, template.Id);
                    continue;
                }

                SendScheduledNotification(template, rollup.TrainingProgramID + "-" + rollup.UserID, rollup.DateCompleted.Value, (users) =>
                {
                    return users;
                }, new object[] { rollup }, sendDate);
            }
        }
        /// <summary>
        /// Uses a scheduled template but isn't really scheduled since it's based on the user meeting a certain point
        /// within a training program. 
        /// </summary>
        /// <param name="template"></param>
        /// <param name="rollup"></param>
        public static void SendTrainingProgramPercentCompleteNotification(Data.TrainingProgramRollup rollup)
        {
            Data.Template template = GetTemplateQuery(rollup.TrainingProgram.CustomerID, null, ScheduleParameterType.TrainingProgramPercentComplete, rollup.TrainingProgram)
                .And(Data.Template.PercentageColumn).IsLessThanOrEqualTo(rollup.PercentComplete)
                .OrderDesc(Data.Template.PercentageColumn.QualifiedName)
                .ExecuteSingle<Data.Template>();
            
            if (template != null)
            {
                SendScheduledNotification(template, rollup.TrainingProgramID + "-" + rollup.UserID, DateTime.UtcNow.AddMinutes(-5), (users) =>
                {
                    return users;
                }, new object[] { rollup });
            }
        }

        public static void SendLastLoginNotification(Template template, DateTime? sendDate) {
            SendLastActivityNotification(template, true, sendDate);
        }

        public static void SendLastCourseActivityNotification(Template template, DateTime? sendDate) {
            SendLastActivityNotification(template, false, sendDate);
        }

        private static void SendLastActivityNotification(Template template, bool isLastLogin, DateTime? sendDate)
        {
            DateTime today = sendDate.HasValue ? sendDate.Value : DateTime.UtcNow;
            DateTime maxActivityDate = today.AddMinutes(template.RelativeScheduledMinutes * -1);
            DateTime minActivityDate = maxActivityDate.AddDays(-1);

            SqlQuery query = Select.AllColumnsFrom<Data.UserLastActivity>()
                .Where(Data.UserLastActivity.Columns.CustomerID).IsEqualTo(template.CustomerID);

            if (isLastLogin) {
                query.And(Data.UserLastActivity.Columns.LastLoginDate).IsBetweenAnd(minActivityDate, maxActivityDate);
            } else {
                query.And(Data.UserLastActivity.Columns.LastCourseActivity).IsBetweenAnd(minActivityDate, maxActivityDate);
            }

            List<Data.UserLastActivity> users = query.ExecuteTypedList<Data.UserLastActivity>();

            foreach (Data.UserLastActivity user in users)
            {
                DateTime? lastActivityDate = isLastLogin ? user.LastLoginDate : user.LastCourseActivity;
                if (lastActivityDate.HasValue)
                {
                    SendScheduledNotification(template, user.Id + "-" + lastActivityDate.Value.ToString(), lastActivityDate.Value, (u) =>
                    {
                        return u;
                    }, new object[] { user }, sendDate);
                }
            }          
                
        }

        /// <summary>
        /// Sends a scheduled notfication. NOTE: the reference date time *will have it's UTC flag set*, so make sure you
        /// pass in dates that are in UTC.
        /// </summary>
        /// <param name="template">The template notification to be sent</param>
        /// <param name="pkValue">A unique id for this object in the system. Doesn't have to be the exact primary key, just a unique key</param>
        /// <param name="reference">The reference date, IN UTC</param>
        /// <param name="userModifier">A function that can modify the user list to be sent; the initial list is created based on the template, the user modifier can change it (for example, to filter out users who have "completed" a training program)</param>
        /// <param name="objects">The objects to be used when parsing the template</param>
        /// <param name="currentTime">If set, the notifications will be generated using the specified time. Use a future date to queue up notifications</param>
        /// <param name="isForced">If set to true, this notification will send regardless of the template enabled setting</param>
        private static void SendScheduledNotification(Template template, string pkValue, DateTime reference, Func<List<int>, List<int>> userModifier, object[] objects, DateTime? currentTime = null)
        {
            bool isQueued = false;
            DateTime utcNow = DateTime.UtcNow;
            if (!currentTime.HasValue)
            {
                // If a current time is not specified, this is not a queued notification - will send immediately. 
                isQueued = false;
                currentTime = utcNow;
            }
            else
            {
                // Is queued is true only if the specified time is a future date
                isQueued = currentTime > utcNow;
            }
            // first, confirm that this notification hasn't been sent before
            // we use the id of the schedule along with the object id and the object's date time
            // so, in this way, we can make sure:
            // 1) each schedule entry/object id has a map, but...
            // 2) if the timestamp they're associated with changes, the notifications get rescheduled too
            // For example, if you have a TP scheduled, and the notifications go out 2 weeks ahead of time,
            // but then the TP is pushed out a week, the notifications will be re-sent.
            TemplateScheduleHistoryCollection collection = new TemplateScheduleHistoryCollection()
                .Where(TemplateScheduleHistory.Columns.TemplateScheduleID, template.Id)
                .Where(TemplateScheduleHistory.Columns.ObjectID, pkValue)
                // give a 12-hour window, +/-; anytime in there, and we fail it
                // this lets us account for daylight savings, timezone changes, whatever
                .BetweenAnd(TemplateScheduleHistory.Columns.ObjectDateTime, reference.AddHours(-12), reference.AddHours(12))
                .Load();

            // if we've already sent the message, bail
            if (collection.Count > 0 && !isQueued)
            {
                Log.DebugFormat("Already sent a message for templateID {0}, objectID {1}, objectDateTime {2}.", template.Id, pkValue, reference);
                return;
            }

            // ok, now we need to calculate to see if we *should* send the message
            // the "withutcflag" guarantees that our date time is in UTC, since we're comparing 
            DateTime dtShifted = reference.AddMinutes(template.RelativeScheduledMinutes).WithUtcFlag();

            // if the date we're considering is before the date the schedule was modified (minus 1 day for edge cases), we skip it
            // this makes sure that if they set up a new schedule, it doesn't re-send for all the past
            // items, but instead just considers upcoming events
            if (dtShifted < template.ModifiedOn.WithUtcFlag().AddDays(-1))
            {
                Log.DebugFormat("Skipping template {0} for object {1} because the template was created after the scheduled date.", template.Id, pkValue);
                return;
            }

            // if the time has passed, send it out
            if (dtShifted < currentTime)
            {
                Log.DebugFormat("Sending message for templateID {0}, objectID {1}, objectDateTime {2}.", template.Id, pkValue, reference);

                // the "true" flag indicates synchronous
                SendNotification(template, new Symphony.Core.Models.NotificationOptions()
                {
                    Sync = true,
                    TemplateObjects = objects,
                    UserModifier = userModifier,
                    IsQueued = isQueued,
                    SendTime = dtShifted
                });

                TemplateScheduleHistory history = new TemplateScheduleHistory();

                if (!isQueued)
                {
                    // record that we have sent the message so we don't send it again
                    history.TemplateScheduleID = template.Id;
                    history.ObjectID = pkValue;
                    history.ObjectDateTime = reference;
                    history.Save("TaskRunner");
                }
                if (isQueued)
                {
                    Log.Debug("Message queued successfully. No record has been stored in the history table.");
                }
                else
                {
                    Log.DebugFormat("Message sent successfully. Record is stored as id {0}.", history.Id);
                }
            }
        }

        /// <summary>
        /// Sends out all the queued notifications where the send date is prior to the current date.
        /// </summary>
        public static void SendQueuedScheduledNotifications()
        {
            List<Data.Notification> notifications = Select.AllColumnsFrom<Data.Notification>()
                .Where(Data.Notification.StatusColumn).IsEqualTo((int)NotificationStatus.Queued)
                .And(Data.Notification.SendDateColumn).IsLessThanOrEqualTo(DateTime.UtcNow)
                .ExecuteTypedList<Data.Notification>();

            List<int> senderAndReceivers = notifications.Select(n => n.SenderID).Concat(notifications.Select(n => n.RecipientID)).ToList();


            Dictionary<int, Data.User> userDictionary = Select.AllColumnsFrom<Data.User>()
                .Where(Data.User.IdColumn).In(senderAndReceivers)
                .ExecuteTypedList<User>()
                .DistinctBy(u => u.Id)
                .ToDictionary(u => u.Id);

            List<Data.Customer> customers = Select.AllColumnsFrom<Data.Customer>()
                .Where(Data.Customer.IdColumn).In(userDictionary.Select(ud => ud.Value.CustomerID))
                .ExecuteTypedList<Data.Customer>();

            foreach (Data.Notification n in notifications)
            {
                Data.User sender = userDictionary[n.SenderID];
                Data.User recipient = userDictionary[n.RecipientID];

                Data.Customer customer = customers.Where(c => c.Id == sender.CustomerID && c.Id == recipient.CustomerID).FirstOrDefault();
                if (customer == null)
                {
                    Log.Error("Customer could not be located for user pair " + sender.Id + ", " + recipient.Id);
                }

                string[] iCalStrings = n.ICal.Split('|');

                if (iCalStrings.Count() > 0)
                {
                    if (iCalStrings.Count() > 0)
                    {
                        // Send 1 message for each ical
                        foreach (string ical in iCalStrings)
                        {
                            SendMessageSMTP(customer, n.Body, n.Subject, sender.Email, recipient.Email, "normal", n.CCEmails.Split(','), null, ical);
                        }
                    }
                    else
                    {
                        SendMessageSMTP(customer, n.Body, n.Subject, sender.Email, recipient.Email, "normal", n.CCEmails.Split(','));
                    }
                }
            }
        }

        /// <summary>
        /// Runs through a list of all scheduled notifications and attemps to send them out
        /// </summary>
        public static void SendScheduledNotifications(DateTime? sendDate = null)
        {
            if (!sendDate.HasValue)
            {
                sendDate = DateTime.UtcNow;
            }

            SqlQuery query = Select.AllColumnsFrom<Data.EnabledTemplate>()
                .Where(Data.EnabledTemplate.Columns.IsScheduled).IsEqualTo(true)
                .And(Data.EnabledTemplate.Columns.ScheduleParameterID).IsGreaterThan(0);

#if DEBUG
            query.And(Data.EnabledTemplate.Columns.CustomerID).IsEqualTo(1); // be
#else
            query.And(Data.EnabledTemplate.Columns.CustomerID).IsGreaterThan(0);
#endif

            // Executing the view as a Template as all the column names are the same.
            // EnabledTemplate also has the column IsEnabledByTrainingProgram
            // which is set up as a manual field in Data.Template.
            List<Data.Template> templates = query.ExecuteTypedList<Data.Template>();

            Log.DebugFormat("Sending notifications for {0} templates", templates.Count);

            foreach (Template template in templates)
            {
                try
                {
                    ScheduleParameter sp = new ScheduleParameter(template.ScheduleParameterID);
                    Log.DebugFormat("Sending to template: {0}-{1} Schedule Parameter: {1}-{2}", template.Id, template.DisplayName, sp.Id, sp.DisplayName);
                    switch (sp.CodeName)
                    {
                        case ScheduleParameterType.ClassStartDate:
                        case ScheduleParameterType.ClassEndDate:
                            SendScheduledClassNotification(template, sendDate);
                            break;
                        case ScheduleParameterType.TrainingProgramCourseDueDate:
                            SendScheduledTrainingProgramCourseNotification(template, sendDate);
                            break;
                        case ScheduleParameterType.TrainingProgramStartDate:
                        case ScheduleParameterType.TrainingProgramDueDate:
                        case ScheduleParameterType.TrainingProgramEndDate:
                            SendScheduledTrainingProgramNotification(template, sendDate);
                            break;
                        case ScheduleParameterType.TrainingProgramSessionStart:
                        case ScheduleParameterType.TrainingProgramSessionEnd:
                            SendScheduledTrainingProgramSessionNotification(template, sendDate);
                            break;
                        case ScheduleParameterType.TrainingProgramAfterCompletion:
                            SendScheduledTrainingProgramAfterCompletionNotification(template, sendDate);
                            break;
                        case ScheduleParameterType.NewHireExpiration:
                            SendScheduledNewHireNotification(template, sendDate);
                            break;
                        case ScheduleParameterType.StudentLastLoggedIn:
                            SendLastLoginNotification(template, sendDate);
                            break;
                        case ScheduleParameterType.StudentLastCourseActivity:
                            SendLastCourseActivityNotification(template, sendDate);
                            break;
                        default:
                            Log.Debug("Could not determine type");
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                }
            }
        }

        private static Dictionary<int, int> GetTrainingProgramIdsForTemplate(Data.Template template)
        {
            return new Select(Data.TrainingProgramTemplate.TrainingProgramIDColumn)
                .From<Data.Template>()
                .InnerJoin(Data.TrainingProgramTemplate.TemplateIDColumn, Data.Template.IdColumn)
                .Where(Data.Template.IdColumn).IsEqualTo(template.Id)
                .ExecuteTypedList<int>()
                .Distinct()
                .ToDictionary<int, int>(i => i);
        }

        /// <summary>
        /// Sends an email over SMTP
        /// </summary>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        /// <param name="sSender"></param>
        /// <param name="recipient"></param>
        /// <param name="priority"></param>
        /// <returns></returns>
        public static bool SendMessageSMTP(Customer customer, string body, string subject, string replyTo, string recipient, string priority, string[] ccAddresses = null, iCalendar invite = null, string inviteString = null)
        {
            try
            {
                MailMessage mm = new MailMessage();

                if (string.IsNullOrEmpty(recipient))
                {
                    Log.Warn("No recipient specified, so we could not sent the SMTP message.");
                    return false;
                }

                string[] parts = recipient.Split('@');
                if (parts.Length < 2)
                {
                    Log.WarnFormat("Invalid recipient specified, {0}", recipient);
                    return false;
                }

                if (useEmailWhitelist && !validEmailDomains.Contains(parts[1]))
                {
                    Log.WarnFormat("Email whitelisting is in effect. The domain {0} was not valid.", parts[1]);
                    return false;
                }

                string sender = string.IsNullOrEmpty(customer.Email) ? "symphony@betraining.com" : customer.Email;

                if (string.IsNullOrWhiteSpace(replyTo))
                {
                    replyTo = sender;
                }

                // comes from the system but displays the user
                mm.From = new MailAddress(sender, replyTo);
                try
                {
                    // but reply-to is the user
                    mm.ReplyToList.Add(new MailAddress(replyTo));
                }
                catch (Exception ex)
                {
                    Log.Warn("Adding \"reply to\" failed when sending message with subject \"" + subject + "\" to " + recipient + " using reply to " + replyTo + ": " + ex.Message);
                }

                mm.To.Add(recipient);
                mm.Subject = subject;
                mm.Body = body;
                mm.IsBodyHtml = true;

                //mm.Headers.Add("Content-Type", "multipart/alternative");

                if (invite != null || !String.IsNullOrWhiteSpace(inviteString))
                {
                    iCalendarSerializer serializer = new iCalendarSerializer();
                    String icalData = invite != null ? serializer.SerializeToString(invite) : inviteString;

                    var calAttr = System.Net.Mail.Attachment.CreateAttachmentFromString(icalData, new System.Net.Mime.ContentType("text/calendar; method=" + invite.Method + "; charset=UTF-8;"));
                    calAttr.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                    calAttr.ContentDisposition.DispositionType = "attachment; filename=invite.ics";
                    mm.Attachments.Add(calAttr);

                    //var calAttr1 = System.Net.Mail.Attachment.CreateAttachmentFromString(icalData, );
                    /*var mimeType = new System.Net.Mime.ContentType("text/calendar; method=REQUEST; charset=UTF-8");
                    AlternateView icalView = AlternateView.CreateAlternateViewFromString(@"
BEGIN:VCALENDAR
PRODID:-//Example/ExampleCalendarClient//EN
METHOD:REQUEST
VERSION:2.0
BEGIN:VEVENT
ORGANIZER:mailto:foo1@example.com
ATTENDEE;ROLE=CHAIR;PARTSTAT=ACCEPTED:mailto:foo1@example.com
ATTENDEE;RSVP=YES;CUTYPE=INDIVIDUAL:mailto:foo2@example.com
DTSTAMP:19970611T190000Z
DTSTART:19970701T170000Z
DTEND:19970701T173000Z
SUMMARY:Phone Conference
UID:calsvr.example.com-8739701987387771
SEQUENCE:0
STATUS:CONFIRMED
END:VEVENT
END:VCALENDAR
", mimeType);
                    icalView.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
                    mm.AlternateViews.Add(icalView);

                    //var ics = new System.Net.Mime.ContentType("application/ics; name=\"invite.ics\");
                    /*var calAttr2 = System.Net.Mail.Attachment.CreateAttachmentFromString(icalData, new System.Net.Mime.ContentType("application/ics"));
                    calAttr2.ContentDisposition.FileName = "invite.ics";
                    calAttr2.ContentDisposition.DispositionType = DispositionTypeNames.Attachment;
                    calAttr2.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                    calAttr2.Name = "invite.ics";
                    mm.Attachments.Add(calAttr2);*/
                }

                if (priority.ToLower() == "low")
                    mm.Priority = MailPriority.Low;
                else if (priority.ToLower() == "high")
                    mm.Priority = MailPriority.High;
                else
                    mm.Priority = MailPriority.Normal;

                if (ccAddresses != null)
                {
                    foreach (var address in ccAddresses)
                    {
                        if (string.IsNullOrEmpty(address))
                        {
                            continue;
                        }

                        try
                        {
                            mm.CC.Add(address);
                        }
                        catch { }
                    }
                }

                

                if (IsQAServer) // Just to make sure QA server task runner does not send out a bunch of spam. 
                {
                    using (SmtpClient sc = new SmtpClient())
                    {
                        sc.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                        sc.PickupDirectoryLocation = @"c:\temp\mail";
                        sc.Send(mm);
                    }
                }
                else
                {

                    using (SmtpClient sc = new SmtpClient())
                    {
                        sc.Timeout = 15000;
                        sc.Send(mm);
                        Log.Debug("SendMessageSMTP sent: " + recipient + " " + subject);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Log.Error("SendMessageSMTP FAILED: " + ex.Message + ".  Could not complete SMTP delivery to " + recipient, ex);
                return false;
            }
        }

        private static int _GetTrainingProgramPrimaryLeader(int trainingProgramId)
        {
            int tpPrimaryLeaderId = -1;

            // check to see if there is a dedicate Primary Leader for the TP
            List<Data.TrainingProgramLeader> tpPl = Select.AllColumnsFrom<Data.TrainingProgramLeader>()
                .Where(Data.TrainingProgramLeader.Columns.IsPrimary).IsEqualTo(true)
                .And(Data.TrainingProgramLeader.Columns.TrainingProgramID).IsEqualTo(trainingProgramId)
                .ExecuteTypedList<Data.TrainingProgramLeader>();

            if (tpPl.Count > 0)
            {
                tpPrimaryLeaderId = tpPl[0].UserID;
            }
            return tpPrimaryLeaderId;
        }
    }
}
